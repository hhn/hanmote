﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace DALFactory
{
    public class DALFactoryHelper
    {
        private static readonly string basePath = "Lib.SqlServerDAL";

        /// <summary>
        /// 采用反射创建DAL实例(注意输入类名的完整性)
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="className">类名
        /// (ContractManageDAL.OutlineAgreementDAL.AgreementContractDAL)</param>
        /// <returns></returns>
        public static T CreateNewInstance<T>(string className)
        {
            try
            {
                //完整类名  命名空间.类名
                string classFullName = basePath+ "." + className;
                Assembly assembly = Assembly.Load(basePath);
                object obj = assembly.CreateInstance(classFullName);
                //类型转换并返回
                return (T)obj;
            }
            catch (Exception e)
            {
                Console.Write(e.StackTrace);
                return default(T);
            }
        }

    }
}
