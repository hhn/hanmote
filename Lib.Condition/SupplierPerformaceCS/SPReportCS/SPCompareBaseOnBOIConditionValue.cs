﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.ContionSettings.SupplierPerformaceCS.SPReportCS
{
    /// <summary>
    /// /// <summary>
    /// 基于供应商行业的供应商绩效比较界面
    /// 保存该界面的值
    /// 作为查询条件
    /// </summary>
    /// </summary>
    public class SPCompareBaseOnBOIConditionValue
    {

        private SelectSupplierConditionSettings selectSupplierConditionSettings;

        /// <summary>
        /// 设置查询供应商信息的条件类
        /// 包装类
        /// 此处不用继承方式
        /// </summary>
        public SelectSupplierConditionSettings SelectSupplierConditionSettings
        {
            get { return selectSupplierConditionSettings; }
            set { selectSupplierConditionSettings = value; }
        }

        private string supplierIndustryId;

        /// <summary>
        /// 供应商行业编码
        /// </summary>
        public string SupplierIndustryId
        {
            get { return supplierIndustryId; }
            set { supplierIndustryId = value; }
        }

        private string supplierIndustryName;

        /// <summary>
        /// 供应商行业名称
        /// </summary>
        public string SupplierIndustryName
        {
            get { return supplierIndustryName; }
            set { supplierIndustryName = value; }
        }
    }
}
