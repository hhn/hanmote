﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.ContionSettings.SupplierPerformaceCS.SPReportCS
{
    /// <summary>
    /// 供应商绩效比较界面
    /// 保存该界面的值
    /// 作为查询条件
    /// </summary>
    public class SPCompareConditionValue
    {
        private string purchaseId;

        /// <summary>
        /// 采购组织Id
        /// </summary>
        public string PurchaseId
        {
            get { return purchaseId; }
            set { purchaseId = value; }
        }

        private string purchaseName;

        /// <summary>
        /// 采购组织名称
        /// </summary>
        public string PurchaseName
        {
            get { return purchaseName; }
            set { purchaseName = value; }
        }

        private string year;

        /// <summary>
        /// 年度
        /// </summary>
        public string Year
        {
            get { return year; }
            set { year = value; }
        }

        private string month;

        /// <summary>
        /// 时段
        /// </summary>
        public string Month
        {
            get { return month; }
            set { month = value; }
        }

        private string supplierId;

        /// <summary>
        /// 供应商Id
        /// </summary>
        public string SupplierId
        {
            get { return supplierId; }
            set { supplierId = value; }
        }

        private string supplierName;

        /// <summary>
        /// 供应商名称
        /// </summary>
        public string SupplierName
        {
            get { return supplierName; }
            set { supplierName = value; }
        }

        private string materialId;

        /// <summary>
        /// 物料编码
        /// </summary>
        public string MaterialId
        {
            get { return materialId; }
            set { materialId = value; }
        }

        private string materialName;

        /// <summary>
        /// 物料名称
        /// </summary>
        public string MaterialName
        {
            get { return materialName; }
            set { materialName = value; }
        }
    }
}
