﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Common
{
    /// <summary>
    /// 供应商认证状态类
    /// </summary>
    public class SupplierCertificationState
    {
        /// <summary>
        /// 供应商在门户注册之后
        /// </summary>
        public const int AfterRegister = 1;
        /// <summary>
        /// 供应商已上传自评结果
        /// </summary>
        public const int UploadedSelfassessment = 2;

        /// <summary>
        /// 企业对自评结果决策之后
        /// </summary>
        public const int AfterSelfassessmentDecision = 3;

        /// <summary>
        ///企业对协议签署之后进行决策
        /// </summary>
        public const int ProcessingAgreementDecision = 4;

        /// <summary>
        ///企业对协议签署之后的决策之后
        /// </summary>
        public const int AfterAgreementDecision = 5;
        
        /// <summary>
        /// 企业对供应商等级确认后
        /// </summary>
        public const int AfterGradeConfirm = 6;
        
        /// <summary>
        /// 企业对进行供应商差异识别中
        /// </summary>
        public const int ProcessingDifferenceRecognition = 7;
        
        /// <summary>
        /// 企业对供应商培训之后
        /// </summary>
        public const int AfterTraining = 8;

        /// <summary>
        /// 企业对培训结果决策
        /// </summary>
        public const int AfterTrainningDecision = 9;

        /// <summary>
        /// 供应商被淘汰
        /// </summary>
        public const int SupplierElimination = 0;
    }
}
