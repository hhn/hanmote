﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Lib.Common.CommonUtils
{
    public class FormUtils
    {
        //将数据填充到下拉列表框
        public static void FillCombox(ComboBox combobox, DataTable dt)
        {
            combobox.Items.Clear();
            if (dt != null && dt.Rows.Count > 0)
            {
                List<string> list = new List<string>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    StringBuilder sb = new StringBuilder();
                    for (int j = 0; j < dt.Columns.Count; j++)
                    {
                        sb.Append(dt.Rows[i][j]);
                        if (j != dt.Columns.Count - 1)
                        {
                            sb.Append(" ");
                        }
                    }
                    list.Add(sb.ToString());
                }
                combobox.Items.AddRange(list.ToArray());
                combobox.SelectedIndex = 0;
                combobox.Text = list[0];
            }
        }

        //将数据填充到下拉列表框
        public static void FillCombox(ComboBox combox , List<string> list)
        {
            combox.Items.Clear();
            if(list != null && list.Count > 0)
            {
                combox.Items.AddRange(list.ToArray());
                combox.Text = list[0];
                combox.SelectedIndex = 0;
            }
        }

        //将数据填充到下拉列表框没有默认值
        public static void FillComboxNoDefault(ComboBox combox, List<string> list)
        {
            combox.Items.Clear();
            if (list != null && list.Count > 0)
            {
                combox.Items.AddRange(list.ToArray());
            }
        }
    }
}
