﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Lib.Common.CommonUtils
{
    public class DataGridViewCellTool
    {
        /// <summary>
        /// 获得DataGridViewCell的string值
        /// </summary>
        /// <param name="cell"></param>
        /// <returns></returns>
        public static string getDataGridViewCellValueString(DataGridViewCell cell)
        {
            if (cell.Value == null)
                return "";
            else
                return cell.Value.ToString().Trim();
        }

        /// <summary>
        /// 获得DataGridView的double值
        /// </summary>
        /// <param name="cell"></param>
        /// <returns></returns>
        public static double getDataGridViewCellValueDouble(DataGridViewCell cell)
        {
            if (cell.Value == null || cell.Value.ToString().Trim().Equals(""))
                return 0.0;
            else
                return Convert.ToDouble(cell.Value.ToString());
        }

        /// <summary>
        /// 获得DataGridView的int值
        /// </summary>
        /// <param name="cell"></param>
        /// <returns></returns>
        public static int getDataGridViewCellValueInt(DataGridViewCell cell) {
            if (cell.Value == null || cell.Value.ToString().Trim().Equals(""))
                return 0;
            else
                return Convert.ToInt32(cell.Value.ToString());
        }

        /// <summary>
        /// 把double转化为string
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string convertDoubleToString(double value)
        {
            if (Math.Abs(value) <= 0.00000001)
                return "";
            else
                return Convert.ToString(value);
        }

        /// <summary>
        /// 把string转化为double
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static double convertStringToDouble(string value) {
            if (value.Trim().Equals(""))
                return 0.0;

            return Convert.ToDouble(value);
        }
    }
}
