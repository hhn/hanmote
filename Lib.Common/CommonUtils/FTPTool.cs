﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Net;
using System.Configuration;
using System.ComponentModel;
using System.Windows.Forms;

namespace Lib.Common.CommonUtils
{
    public class FTPTool
    {
        //ftp参数
        private string hostIP;              //FTP服务器IP地址，例如：172.16.142.9
        private string host;                //FTP服务器主机，例如：ftp://172.16.142.9/
        private string user;                //FTP账户用户名
        private string psw;                 //FTP账户密码
        //请求和回应
        private static FtpWebRequest ftpRequest = null;         //FTP请求
        private static FtpWebResponse ftpResponse = null;       //FTP回应
        private static Stream ftpStream = null;                 //FTP数据流
        private static int bufferSize = 2048;                   //缓冲区大小

        //单例实例
        private static readonly FTPTool ftpInstance = new FTPTool();

        // 记录当前处理信息(用于反馈)
        public class CurInfo {
            public double curFilesSize;
            public string curFileName;
        }

        private FTPTool()
        {
            //从配置文件中读取参数
            this.hostIP = ConfigurationManager.AppSettings["ftpServerIP"];
            this.host = "ftp://" + this.hostIP + "/";
            this.user = ConfigurationManager.AppSettings["ftpServerUserID"];
            this.psw = ConfigurationManager.AppSettings["ftpServerPsw"];
        }

        /// <summary>
        /// 获取FTPTool实例
        /// </summary>
        /// <returns></returns>
        public static FTPTool getInstance()
        {
            return ftpInstance;
        }

        /// <summary>
        /// 测试是否可连接
        /// </summary>
        /// <returns></returns>
        public bool serverIsConnect()
        {

            return serverConnectable(this.host, this.user, this.psw);
        }

        /// <summary>
        /// 测试是否可以连接到服务器
        /// </summary>
        /// <param name="ftpServerIP">服务器IP</param>
        /// <param name="user">用户名</param>
        /// <param name="psw">密码</param>
        /// <returns>测试结果</returns>
        private bool serverConnectable(string host, string user, string psw)
        {
            //尝试连接
            try
            {
                //已获取 ftp服务器文件目录 的命令来测试是否可以连接成功
                FtpWebRequest tmpFtpRequest = (FtpWebRequest)FtpWebRequest.Create(host);
                tmpFtpRequest.UseBinary = true;
                tmpFtpRequest.KeepAlive = false;
                tmpFtpRequest.UsePassive = true;
                tmpFtpRequest.Credentials = new NetworkCredential(user, psw);
                tmpFtpRequest.Method = WebRequestMethods.Ftp.ListDirectoryDetails;

                FtpWebResponse tmpFtpResponse = (FtpWebResponse)tmpFtpRequest.GetResponse();

                //可以连接
                tmpFtpResponse.Close();
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// 下载功能(支持以Dictionary的形式下载多个文件)
        /// </summary>
        /// <param name="downloadRLDic">保存ServerLocation和LocalLocation的Dictionary</param>
        /// <param name="allFilesSize">所有文件的大小总和</param>
        /// <returns>下载失败的服务器文件队列</returns>
        public LinkedList<string> download(Dictionary<string, string> downloadRLDic, 
            double allFilesSize, BackgroundWorker worker, DoWorkEventArgs e)
        {
            // 相应时间单位
            int elapsedTime = 20;
            // 记录上次响应时间
            DateTime lastReportDateTime = DateTime.Now;
            // 记录是否取消
            bool isCancel = false;
            // 记录当前信息
            CurInfo curInfo = new CurInfo();

            // 下载失败文件列表
            LinkedList<string> fileDownFailedLinkedList = new LinkedList<string>();

            foreach (KeyValuePair<string, string> kvPair in downloadRLDic)
            {
                string remoteFile = kvPair.Key;
                string localFile = kvPair.Value;
                int indexOfLast = localFile.LastIndexOf('/');
                // 文件名(不包括路径)
                string fileSafeName = localFile.Substring(indexOfLast + 1,
                    localFile.Length - indexOfLast - 1);

                // 是否取消
                if (isCancel)
                {
                    fileDownFailedLinkedList.AddLast(fileSafeName);
                    continue;
                }

                try
                {
                    //创建ftp文件请求
                    ftpRequest = (FtpWebRequest)FtpWebRequest.Create(host + remoteFile);
                    //登陆ftp服务器
                    ftpRequest.Credentials = new NetworkCredential(user, psw);
                    //设置请求属性
                    ftpRequest.UseBinary = true;
                    // 暂时改为false
                    ftpRequest.KeepAlive = false;
                    ftpRequest.UsePassive = true;
                    //设定ftp文件请求的具体类型(下载文件)
                    ftpRequest.Method = WebRequestMethods.Ftp.DownloadFile;
                    ftpResponse = (FtpWebResponse)ftpRequest.GetResponse();
                    /* Get the FTP Server's Response Stream */
                    ftpStream = ftpResponse.GetResponseStream();
                    //创建文件流来写文件
                    FileStream localFileStream = new FileStream(localFile, FileMode.Create);
                    /* Buffer for the Downloaded Data */
                    byte[] byteBuffer = new byte[bufferSize];
                    int bytesRead = ftpStream.Read(byteBuffer, 0, bufferSize);
                    /* Download the File by Writing the Buffered Data Until the Transfer is Complete */
                    try
                    {
                        while (bytesRead > 0)
                        {
                            #region 是否需要响应
                            // 是否取消
                            if (worker.CancellationPending) {
                                isCancel = true;
                                fileDownFailedLinkedList.AddLast(fileSafeName);
                                break;
                            }

                            curInfo.curFilesSize += bytesRead;
                            curInfo.curFileName = fileSafeName;
                            int compare = DateTime.Compare(
                                DateTime.Now, lastReportDateTime.AddMilliseconds(elapsedTime));
                            if (compare > 0) {
                                // 响应一次
                                worker.ReportProgress(0, curInfo);
                                // 时间归为现在
                                lastReportDateTime = System.DateTime.Now;
                            }
                            #endregion

                            localFileStream.Write(byteBuffer, 0, bytesRead);
                            bytesRead = ftpStream.Read(byteBuffer, 0, bufferSize);
                        }
                    }
                    catch (Exception ex)
                    {
                        //下载失败,将下载的错误文件删除
                        fileDownFailedLinkedList.AddLast(fileSafeName);
                        File.Delete(localFile);

                        continue;
                    }

                    /* Resource Cleanup */
                    localFileStream.Close();
                    ftpStream.Close();
                    ftpResponse.Close();
                    ftpRequest = null;
                    break;
                }
                catch (Exception ex)
                {
                    //下载有问题
                    fileDownFailedLinkedList.AddLast("网络异常！");
                }

            }
            // 响应最终结果
            if(!isCancel)
                worker.ReportProgress(0, curInfo);

            return fileDownFailedLinkedList;
        }

        /// <summary>
        /// 上传文件(支持以Dictionary的形式上传多个文件)
        /// </summary>
        /// <param name="uploadRLDic">保存ServerLocation和LocalLocation的Dictionary</param>
        /// <param name="allFilesSize">所有文件的大小总和</param>>
        /// <returns>上传失败的本地文件队列</returns>
        public LinkedList<string> upload(Dictionary<string, string> uploadRLDic, 
            double allFilesSize, BackgroundWorker worker, DoWorkEventArgs e)
        {
            // 相应时间单位
            int elapsedTime = 20;
            // 记录上次响应时间
            DateTime lastReportDateTime = DateTime.Now;
            // 记录是否取消
            bool isCancel = false;
            // 记录当前信息
            CurInfo curInfo = new CurInfo();
            // 上传失败的文件列表
            LinkedList<string> fileUploadFailedLinkedList = new LinkedList<string>();
            // 需要删除的文件
            string fileNeedDelete = "";

            foreach (KeyValuePair<string, string> kvPair in uploadRLDic)
            {
                string remoteFile = kvPair.Key;
                string localFile = kvPair.Value;
                int indexOfLast = localFile.LastIndexOf('/');
                // 文件名(不包括路径)
                string fileSafeName = localFile.Substring(indexOfLast + 1,
                    localFile.Length - indexOfLast - 1);

                //如果取消
                if (isCancel)
                {
                    //记录下未上传的文件
                    fileUploadFailedLinkedList.AddLast(fileSafeName);
                    continue;
                }
                
                //文件夹不存在的话，创建文件夹
                int lastIndex = remoteFile.LastIndexOf('/');
                string folderName = remoteFile.Substring(0, lastIndex);
                if (!directoryIsExist(folderName))
                {
                    createFileCatalog(folderName);
                }

                try
                {
                    /* Create an FTP Request */
                    ftpRequest = (FtpWebRequest)FtpWebRequest.Create(host + remoteFile);
                    /* Log in to the FTP Server with the User Name and Password Provided */
                    ftpRequest.Credentials = new NetworkCredential(user, psw);
                    /* When in doubt, use these options */
                    ftpRequest.UseBinary = true;
                    ftpRequest.UsePassive = true;
                    ftpRequest.KeepAlive = false;
                    /* Specify the Type of FTP Request */
                    ftpRequest.Method = WebRequestMethods.Ftp.UploadFile;
                    /* Establish Return Communication with the FTP Server */
                    ftpStream = ftpRequest.GetRequestStream();
                    /* Open a File Stream to Read the File for Upload */
                    FileStream localFileStream = new FileStream(localFile, FileMode.Open);
                    /* Buffer for the Uploaded Data */
                    byte[] byteBuffer = new byte[bufferSize];
                    int bytesSent = localFileStream.Read(byteBuffer, 0, bufferSize);
                    /* Upload the File by Sending the Buffered Data Until the Transfer is Complete */
                    try
                    {
                        while (bytesSent != 0)
                        {
                            #region 是否需要响应
                            // 是否取消
                            if (worker.CancellationPending)
                            {
                                isCancel = true;
                                //记录下该文件
                                fileUploadFailedLinkedList.AddLast(fileSafeName);
                                //稍后删除已上传的部分文件
                                fileNeedDelete = remoteFile;

                                break;
                            }

                            curInfo.curFilesSize += bytesSent;
                            curInfo.curFileName = fileSafeName;
                            int compare = DateTime.Compare(
                                DateTime.Now, lastReportDateTime.AddMilliseconds(elapsedTime));
                            if (compare > 0)
                            {
                                // 响应一次
                                worker.ReportProgress(0, curInfo);
                                // 时间归为现在
                                lastReportDateTime = System.DateTime.Now;
                            }
                            #endregion

                            ftpStream.Write(byteBuffer, 0, bytesSent);
                            bytesSent = localFileStream.Read(byteBuffer, 0, bufferSize);
                        }
                    }
                    catch (Exception ex)
                    {
                        // 加入 上传失败 队列
                        fileUploadFailedLinkedList.AddFirst(fileSafeName);

                        continue;
                    }

                    /* Resource Cleanup */
                    localFileStream.Close();
                    ftpStream.Close();
                    ftpRequest = null;
                    break;
                }
                catch (Exception ex)
                {
                    throw ex;
                }

            }
            // 响应最终结果
            if(!isCancel)
                worker.ReportProgress(0, curInfo);

            //判断是否有因为取消导致上传的文件，需要删除掉
            if (!fileNeedDelete.Equals("") && checkFileExist(fileNeedDelete)) {
                delete(fileNeedDelete);
            }

            return fileUploadFailedLinkedList;
        }

        /// <summary>
        /// 批量删除文件
        /// </summary>
        /// <param name="fileLinkedList"></param>
        /// <returns>删除失败的文件列表</returns>
        public LinkedList<string> deleteFileList(LinkedList<string> fileLinkedList)
        {
            LinkedList<string> deleteFailedFileList = new LinkedList<string>();
            foreach (string deleteFileName in fileLinkedList)
            {
                if (!checkFileExist(deleteFileName))
                    continue;
                if (!delete(deleteFileName))
                {
                    deleteFailedFileList.AddFirst(deleteFileName);
                }
            }

            return deleteFailedFileList;
        }

        public List<string> deleteFileList(List<string> fileList)
        {
            List<string> deleteFailedFileList = new List<string>();
            foreach (string deleteFileName in fileList)
            {
                if (!checkFileExist(deleteFileName))
                    continue;
                if (!delete(deleteFileName))
                {
                    deleteFailedFileList.Add(deleteFileName);
                }
            }
            return deleteFailedFileList;
        }

        /// <summary>
        /// 删除单个文件
        /// </summary>
        /// <param name="deleteFile"></param>
        /// <returns></returns>
        public bool delete(string deleteFile)
        {
            try
            {
                /* Create an FTP Request */
                FtpWebRequest ftpDeleteRequest = (FtpWebRequest)WebRequest.Create(host + deleteFile);
                /* Log in to the FTP Server with the User Name and Password Provided */
                ftpDeleteRequest.Credentials = new NetworkCredential(user, psw);
                /* When in doubt, use these options */
                ftpDeleteRequest.UseBinary = true;
                ftpDeleteRequest.UsePassive = false;
                ftpDeleteRequest.KeepAlive = true;
                /* Specify the Type of FTP Request */
                ftpDeleteRequest.Method = WebRequestMethods.Ftp.DeleteFile;
                /* Establish Return Communication with the FTP Server */
                FtpWebResponse ftpDeleteResponse = (FtpWebResponse)ftpDeleteRequest.GetResponse();
                /* Resource Cleanup */
                ftpDeleteResponse.Close();
                ftpDeleteRequest = null;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return false;
            }

            return true;
        }

        /// <summary>
        /// 删除文件夹(文件夹必须是空的)
        /// </summary>
        /// <param name="removeDirectory"></param>
        /// <returns></returns>
        public bool removeDirectory(string removeDirectory)
        {
            try
            {
                /* Create an FTP Request */
                FtpWebRequest ftpDeleteRequest = (FtpWebRequest)WebRequest.Create(host + removeDirectory);
                /* Log in to the FTP Server with the User Name and Password Provided */
                ftpDeleteRequest.Credentials = new NetworkCredential(user, psw);
                /* When in doubt, use these options */
                ftpDeleteRequest.UseBinary = true;
                ftpDeleteRequest.UsePassive = false;
                ftpDeleteRequest.KeepAlive = true;
                /* Specify the Type of FTP Request */
                ftpDeleteRequest.Method = WebRequestMethods.Ftp.RemoveDirectory;
                /* Establish Return Communication with the FTP Server */
                FtpWebResponse ftpDeleteResponse = (FtpWebResponse)ftpDeleteRequest.GetResponse();
                /* Resource Cleanup */
                ftpDeleteResponse.Close();
                ftpDeleteRequest = null;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return false;
            }

            return true;
        }

        /// <summary>
        /// 在FTP服务器上创建整个目录
        /// </summary>
        /// <param name="fileSource">文件夹 如：Test/New/Folder</param>
        /// <returns>是否创建成功</returns>
        private bool createFileCatalog(string fileSource)
        {
            //由于一次只能创建一个文件夹，因此需要一层层创建
            string[] fileCatalog = fileSource.Split(new string[] { "/" }, StringSplitOptions.None);
            StringBuilder strBui = new StringBuilder("");

            for (int i = 0; i < fileCatalog.Length; i++)
            {
                if (strBui.ToString().Equals(""))
                {
                    strBui.Append(fileCatalog[i]);
                }
                else
                {
                    strBui.Append("/").Append(fileCatalog[i]);
                }

                if (!directoryIsExist(strBui.ToString()))
                {
                    //创建失败
                    if (!createDirectory(strBui.ToString()))
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// 在FTP服务器上创建单层目录
        /// </summary>
        /// <param name="path">目录名，例如:test/folder</param>
        /// <returns></returns>
        private bool createDirectory(string path)
        {
            try
            {
                FtpWebRequest FTP = (FtpWebRequest)FtpWebRequest.Create(host + path);
                FTP.Credentials = new NetworkCredential(user, psw);
                FTP.Proxy = null;
                FTP.KeepAlive = false;
                FTP.Method = WebRequestMethods.Ftp.MakeDirectory;
                FTP.UseBinary = true;
                FtpWebResponse response = FTP.GetResponse() as FtpWebResponse;
                response.Close();

                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return false;
            }
        }

        /// <summary>
        /// 检测目录是否存在
        /// </summary>
        /// <param name="path">目录路径</param>
        /// <returns>是否存在</returns>
        private bool directoryIsExist(string path)
        {
            string[] value = getFileList(path);
            //如果目录存在, 就算为空，也只是会包括两项: . 和 ..
            if (value == null || value.Length <= 2)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        /// 获取目录下的所有文件名称
        /// </summary>
        /// <param name="path">文件夹路径</param>
        /// <returns></returns>
        private string[] getFileList(string path)
        {
            StringBuilder result = new StringBuilder();
            try
            {
                Uri uri = new Uri(host + path);
                FtpWebRequest reqFTP = (FtpWebRequest)FtpWebRequest.Create(host + path);
                reqFTP.UseBinary = true;
                reqFTP.Credentials = new NetworkCredential(user, psw);
                reqFTP.Method = WebRequestMethods.Ftp.ListDirectoryDetails;

                WebResponse res = reqFTP.GetResponse();
                StreamReader reader = new StreamReader(res.GetResponseStream());
                string line = reader.ReadLine();
                while (line != null)
                {
                    result.Append(line);
                    result.Append("\n");
                    line = reader.ReadLine();
                }
                reader.Close();
                res.Close();

                return result.ToString().Split('\n');
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// 判断文件是否存在
        /// </summary>
        /// <param name="remoteFile">文件地址</param>
        /// <returns>判断结果</returns>
        public bool checkFileExist(string remoteFile) {
            FtpWebRequest reqFTP = (FtpWebRequest)FtpWebRequest.Create(host + remoteFile);
            reqFTP.UseBinary = true;
            reqFTP.Credentials = new NetworkCredential(user, psw);
            reqFTP.Method = WebRequestMethods.Ftp.GetFileSize;
            try
            {
                FtpWebResponse res = (FtpWebResponse)reqFTP.GetResponse();
                res.Close();

                return true;
            }
            catch (Exception) {
                return false;
            }
        }

        /// <summary>
        /// 上传单个文件
        /// 用于上传合同模板——合同条款
        /// </summary>
        /// <param name="localFilePath"></param>
        /// <param name="remoteFilePath"></param>
        /// <returns></returns>
        public bool upload(string localFilePath, string remoteFilePath)
        {
            // bool success = true;
            FileStream localStream = null;
            int lastIndex = remoteFilePath.LastIndexOf("/");
            string folderName = remoteFilePath.Substring(0, lastIndex);
            if (!directoryIsExist(folderName))
            {
                createFileCatalog(folderName);
            }
            try
            {
                ftpRequest = (FtpWebRequest)FtpWebRequest.Create(host + remoteFilePath);
                ftpRequest.Credentials = new NetworkCredential(user, psw);

                ftpRequest.UseBinary = true;
                ftpRequest.UsePassive = true;
                ftpRequest.KeepAlive = false;

                ftpRequest.Method = WebRequestMethods.Ftp.UploadFile;
                ftpStream = ftpRequest.GetRequestStream();
                localStream = new FileStream(localFilePath, FileMode.Open);
                byte[] buffer = new byte[bufferSize];

                int byteSent = localStream.Read(buffer, 0, bufferSize);
                while (byteSent != 0)
                {
                    ftpStream.Write(buffer, 0, byteSent);
                    byteSent = localStream.Read(buffer, 0, bufferSize);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (localStream != null && ftpStream != null)
                {
                    localStream.Close();
                    ftpStream.Close();
                }
            }
            return true;
        }

        public LinkedList<string> download(Dictionary<string, string> downloadFile)
        {
            //下载失败的文件列表
            LinkedList<string> failDownloadFileList = new LinkedList<string>();
            foreach (KeyValuePair<string, string> kvPair in downloadFile)
            {
                string remoteFile = kvPair.Key;
                string localFile = kvPair.Value;
                int indexOfLast = localFile.LastIndexOf("/");
                string fileSafeName = localFile.Substring(indexOfLast + 1, localFile.Length - indexOfLast - 1);

                try
                {
                    //创建ftp文件请求
                    ftpRequest = (FtpWebRequest)FtpWebRequest.Create(host + remoteFile);
                    //登陆ftp服务器
                    ftpRequest.Credentials = new NetworkCredential(user, psw);
                    //设置请求属性
                    ftpRequest.UseBinary = true;
                    // 暂时改为false
                    ftpRequest.KeepAlive = false;
                    ftpRequest.UsePassive = true;
                    //设定ftp文件请求的具体类型(下载文件)
                    ftpRequest.Method = WebRequestMethods.Ftp.DownloadFile;
                    ftpResponse = (FtpWebResponse)ftpRequest.GetResponse();
                    /* Get the FTP Server's Response Stream */
                    ftpStream = ftpResponse.GetResponseStream();
                    //创建文件流来写文件
                    FileStream localFileStream = new FileStream(localFile, FileMode.Create);
                    /* Buffer for the Downloaded Data */
                    byte[] byteBuffer = new byte[bufferSize];
                    int bytesRead = ftpStream.Read(byteBuffer, 0, bufferSize);
                    try
                    {
                        while (bytesRead > 0)
                        {
                            localFileStream.Write(byteBuffer, 0, bytesRead);
                            bytesRead = ftpStream.Read(byteBuffer, 0, bufferSize);
                        }
                    }
                    catch (Exception ex)
                    {
                        failDownloadFileList.AddLast(fileSafeName);
                        File.Delete(localFile);
                        continue;
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            return failDownloadFileList;
        }
    }
}
