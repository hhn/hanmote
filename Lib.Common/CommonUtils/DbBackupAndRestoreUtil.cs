﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.ComponentModel;
using System.IO;
using System.Windows.Forms;
using System.Timers;
using System.Collections;

namespace Lib.Common.CommonUtils
{
    public class DbBackupAndRestoreUtil
    {
        BackgroundWorker backgroundWorker = null;
        DoWorkEventArgs doWorkEventArgs = null;

        /// <summary>
        /// 构造方法
        /// </summary>
        public DbBackupAndRestoreUtil(BackgroundWorker backgroundWorker,DoWorkEventArgs doWorkEventArgs)  
        {
            this.backgroundWorker = backgroundWorker;
            this.doWorkEventArgs = doWorkEventArgs;
        }

        /// <summary>
        /// 存在同名文件时，用dbName加当前时间构成新文件名
        /// </summary>
        /// <param name="dbName">数据库名</param>
        /// <param name="backupFilePath">备份目录</param>
        /// <returns>新全路径名</returns>
        private String correctFilePath(String dbName,String backupFileDir)
        {
            String currentTime = DateTime.Now.ToString("yyyyMMddhhMMss");
            if (!backupFileDir.EndsWith("/"))
            {
                backupFileDir += "/";
            }

            return (backupFileDir + dbName + currentTime + ".bak");
            
        }

        /// <summary>
        /// 备份数据库
        /// </summary>
        /// <param name="databaseName">目标数据库名</param>
        /// <param name="connectionString">数据库连接字符串</param>
        /// <returns></returns>
        public void backupDatabase(String databaseName,String backupFileDir, String connectionString)
        {
            //备份到服务器某目录下，自动构造文件名
            String backupFilePath = correctFilePath(databaseName,backupFileDir);

            //新建数据库连接
            SqlConnection sqlConnection = new SqlConnection(connectionString);
            try
            {
                sqlConnection.Open();
            }
            catch (Exception ex)
            {
                MessageUtil.ShowError(ex.Message);
                return;
            }

            //数据库执行命令
            SqlCommand command = new SqlCommand("use master;backup database @name to disk=@path;", sqlConnection);
            try
            {
                command.Parameters.AddWithValue("@name", databaseName);
                command.Parameters.AddWithValue("@path", backupFilePath);
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageUtil.ShowError(ex.Message);
                return;
            }
            finally
            {
                sqlConnection.Close();
            }

            doWorkEventArgs.Result = 1;     //结果置为1，表示操作正确完成
        }

        /// <summary>
        /// 杀死数据库现连接的进程
        /// </summary>
        /// <param name="dbName"></param>
        /// <param name="connectionString"></param>
        private void killExistDatabaseLinkedProcess(String dbName,String connectionString)
        {
            ///杀死原来所有的数据库连接进程
            SqlConnection conn = new SqlConnection(connectionString);
            conn.Open();

            string sql = String.Format(@"SELECT spid 
                                         FROM sysprocesses ,sysdatabases 
                                         WHERE sysprocesses.dbid=sysdatabases.dbid AND sysdatabases.Name='{0}'", dbName);
            SqlCommand command = new SqlCommand(sql, conn);
            ArrayList list = new ArrayList();
            SqlDataReader dr;
            try
            {
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    list.Add(dr.GetInt16(0));
                }
                dr.Close();
            }
            catch (SqlException exp)
            {
                MessageBox.Show(exp.ToString());
            }
            finally
            {
                conn.Close();
            }
           
            //杀死和当前数据库连接的进程
            for (int i = 0; i < list.Count; i++)
            {
                conn.Open();
                command = new SqlCommand(string.Format("KILL {0}", list[i].ToString()), conn);
                command.ExecuteNonQuery();
                conn.Close();
            }
        }

        /// <summary>
        /// 还原数据库
        /// </summary>
        /// <param name="databaseName">待还原数据库名</param>
        /// <param name="restoreFilePath">备份路径</param>
        /// <param name="connectionString">连接字符串</param>
        public void restoreDatabase(String databaseName, String restoreFilePath,String sysConnection,String dboConnection)
        {
            //开始还原前杀死已连接的进程
            try
            {
                killExistDatabaseLinkedProcess(databaseName, sysConnection);
            }
            catch (Exception e)
            {
                MessageUtil.ShowError(e.Message);
                return;
            }

            SqlConnection sqlConnection = new SqlConnection(dboConnection);
            SqlCommand command = new SqlCommand("use master;restore database @name from disk=@path with replace;", sqlConnection);
            try
            {
                sqlConnection.Open();
                command.Parameters.AddWithValue("@name", databaseName);
                command.Parameters.AddWithValue("@path", restoreFilePath);
                command.ExecuteNonQuery();
            }
            catch (Exception exp)
            {
                MessageUtil.ShowError(exp.Message);
                return;
            }
            finally
            {
                if (ConnectionState.Closed != sqlConnection.State)
                {
                    sqlConnection.Close();
                }
            }

            doWorkEventArgs.Result = 1;     //还原成功
            
        }
        
    }
}
