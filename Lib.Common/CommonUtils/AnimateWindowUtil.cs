﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Lib.Common.CommonUtils
{
    /// <summary>
    /// 窗口渐变的工具类
    /// </summary>
    public class AnitmateWindowUtil
    {
        
        [System.Runtime.InteropServices.DllImport("user32")]
        public static extern bool AnimateWindow(IntPtr hwnd, int dwTime, int dwFlags);
        public const int AW_HOR_POSITIVE = 0x0001;
        public const int AW_HOR_NEGATIVE = 0x0002;
        public const int AW_VER_POSITIVE = 0x0004;
        public const int AW_VER_NEGATIVE = 0x0008;
        public const int AW_CENTER = 0x0010;
        public const int AW_HIDE = 0x10000;
        public const int AW_ACTIVATE = 0x20000;
        public const int AW_SLIDE = 0x40000;
        public const int AW_BLEND = 0x80000;

        public static void AnimateOpen(Form frm,Timer timer1)
        {
            //动画由小渐大
            AnimateWindow(frm.Handle, 1000, AW_CENTER | AW_ACTIVATE);

            //主界面渐变设置
            timer1.Enabled = true;//让jianbian的timer值有效
            frm.Opacity = 0;
        }

        public static void AnimateClose(Form frm)
        {
            //动画窗体调用,关闭时将向上移出屏幕
            AnimateWindow(frm.Handle, 1000, AW_SLIDE | AW_HIDE | AW_VER_NEGATIVE);
        }

        public static void ChangeOpacity(Form frm,Timer timer1)
        {
            //让背景由0变到1
            if (frm.Opacity < 1)
            {
                frm.Opacity = frm.Opacity + 0.05;
            }
            else
            {
                timer1.Enabled = false;
            }
        }
    }
}
