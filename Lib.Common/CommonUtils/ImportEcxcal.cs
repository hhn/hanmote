﻿using Aspose.Cells;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Lib.Common.CommonUtils
{
    class ImportEcxcal
    {
        public void Import()
        {
            //打开excel选择框
            OpenFileDialog frm = new OpenFileDialog();
            frm.Filter = "Excel文件(*.xls,xlsx)|*.xls;*.xlsx";
            if (frm.ShowDialog() == DialogResult.OK)
            {

                string excelName = frm.FileName;
                Workbook excel = new Workbook(excelName);
                List<string[]> importyString = GetImportExcelRoute(excel);
            }
        }
        public List<string[]> GetImportExcelRoute(Workbook excel)
        {
            int icount = excel.Worksheets.Count;

            List<string[]> routList = new List<string[]>();
            for (int i = 0; i < icount; i++)
            {
                Worksheet sheet = excel.Worksheets[i];//工作表
                Cells cells = sheet.Cells;//单元格
                int rowcount = cells.MaxRow;//最大的行
                int columncount = cells.MaxColumn;//最大的列

                int routNameColumn = 0;//
                int routAttachColumn = 0;
                int routDescColumn = 0;
                int routMesgColumn = 0;

                //获取标题所在的列
                if (rowcount > 0 && columncount > 0)
                {
                    //找到对应的列信息
                    int r0 = 2;
                    for (int c = 0; c <= columncount; c++)
                    {
                        string strVal = cells[r0, c].StringValue.Trim();
                        if (strVal == "备注")
                        {
                            routDescColumn = c;
                            break;
                        }
                    }

                    r0 = 3;
                    for (int c = 0; c <= columncount; c++)
                    {
                        //获取文本框内容
                        string strVal = cells[r0, c].StringValue.Trim();
                        if (strVal == "审批明细事项")
                        {
                            routNameColumn = c;
                        }
                        if (strVal == "细项")
                        {
                            routMesgColumn = c;
                        }
                        if (strVal == "前置条件及工作要求")
                        {
                            routAttachColumn = c;
                        }
                    }
                    //找到对应标题列下面的值
                    if (routNameColumn > 0 && routAttachColumn > 0 && routDescColumn > 0)
                    {//在从对应的列中找到对应的值
                        for (int r = 4; r <= rowcount; r++)
                        {
                            string[] str = new string[6];
                            string strRoutName = "";
                            string strRoutMesg = "";
                            string strRoutAttach = "";
                            string strRoutDesc = "";
                            string strRoutRole = "";
                            string strRoutPro = "";
                            for (int c = 0; c <= columncount; c++)
                            {
                                int mergcolumncount = 0;
                                int mergrowcount = 0;

                                bool ismerged = cells[r, c].IsMerged;//是否合并单元格 
                                if (ismerged)
                                {
                                    Range range = cells[r, c].GetMergedRange();

                                    if (range != null)
                                    {
                                        mergcolumncount = range.ColumnCount;
                                        mergrowcount = range.RowCount;
                                    }
                                }
                                //获取文本框内容
                                string strVal = "";
                                strVal = cells[r, c].StringValue.Trim();
                                if (c == routNameColumn)
                                {
                                    strRoutName = strVal;
                                    if (mergrowcount > 1 && string.IsNullOrEmpty(strRoutName))
                                    {
                                        strRoutName = GetRoutName(routList, 0);
                                    }
                                }
                                if (c == routMesgColumn)
                                {
                                    strRoutMesg = strVal;
                                    if (mergrowcount > 1 && string.IsNullOrEmpty(strRoutMesg))
                                    {
                                        strRoutMesg = GetRoutName(routList, 1);
                                    }
                                }
                                if (c == routAttachColumn)
                                {
                                    strRoutAttach = strVal;
                                }
                                if (c == routDescColumn)
                                {
                                    strRoutDesc = strVal;
                                }

                            }
                        }
                    }
                 
                }
            }
            return routList;
        }
        private string GetRoutName(List<string[]> routList, int index)
        {
            if (routList == null)
                return "";
            if (routList.Count == 0)
                return "";
            return routList[routList.Count - 1][index];
        }
    }
}