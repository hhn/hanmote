﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Lib.Common
{
  /// <summary>
  /// 显示行号得类
  /// </summary>
    public class RowPostPaint
    {/// <summary>
    /// 绘制行号
    /// </summary>
    /// <param name="dataGridView">dgv表格</param>
    /// <param name="sender"></param>
    /// <param name="e"></param>
        public static void paintNum(DataGridView dataGridView, object sender, DataGridViewRowPostPaintEventArgs e) {

            Color color = dataGridView.RowHeadersDefaultCellStyle.ForeColor;
            if (dataGridView.Rows[e.RowIndex].Selected)
                color = dataGridView.RowHeadersDefaultCellStyle.SelectionForeColor;
            else
                color = dataGridView.RowHeadersDefaultCellStyle.ForeColor;
            using (SolidBrush b = new SolidBrush(color))
            {
                e.Graphics.DrawString((e.RowIndex + 1).ToString(), e.InheritedRowStyle.Font, b, e.RowBounds.Location.X + 20, e.RowBounds.Location.Y + 6);
            }

        }
    }
}
