﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.Model.CatalogPurchase;

namespace Lib.IDAL.CatalogManageIDAL
{
    public interface ShoppingCartIDAL
    {
        ShoppingCartItem getShoppingCartItem(string shoppingCartID, string materialCatalogID);
        List<ShoppingCartItem> getShoppingCartItems(string shoppingCartID);
        int addToShoppingCart(ShoppingCartItem item);
        int updateShoppingCart(ShoppingCartItem item);
        int deleteShoppingCartItem(string materialCatalogID, string shoppingCartID);
        List<String> getPurchaseBudgetList();
    }
}
