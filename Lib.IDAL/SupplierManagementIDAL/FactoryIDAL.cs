﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Lib.IDAL.SupplierManagementIDAL
{
    /// <summary>
    /// 工厂表数据访问入口
    /// </summary>
    public interface FactoryIDAL
    {
        /// <summary>
        /// 查询所有工厂编码
        /// </summary>
        /// <returns></returns>
        DataTable GetAllFactoryIdDT();
    }
}
