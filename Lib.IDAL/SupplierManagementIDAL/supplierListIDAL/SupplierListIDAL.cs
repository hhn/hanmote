﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Lib.Model.supplierListMode;

namespace Lib.IDAL.SupplierManagementIDAL.supplierListIDAL
{
    /// <summary>
    /// 供应商清单业务处理接口
    /// </summary>
    public interface SupplierListIDAL
    {/// <summary>
    /// 获取采购组织
    /// </summary>
    /// <returns></returns>
        DataTable getPurOrg();
        /// <summary>
        /// 获取供应商信息
        /// </summary>
        /// <param name="queryCondition"></param>
        /// <returns></returns>
        DataTable getSupplierInfo(queryCondition queryCondition);
        /// <summary>
        /// 获取采购组织
        /// </summary>
        /// <param name="purOrgName"></param>
        /// <returns></returns>
        DataTable getMtGroup(string purOrgName);
        /// <summary>
        /// 获取物料名称
        /// </summary>
        /// <param name="purOrgName"></param>
        /// <param name="mtGroupName"></param>
        /// <returns></returns>
        DataTable getMtName(string purOrgName, string mtGroupName);
        /// <summary>
        /// 生成供应商清单
        /// </summary>
        /// <param name="supplierListModel"></param>
        bool saveSupplierListInfo(SupplierListModel supplierListModel);
        /// <summary>
        /// 获取已经存在的供应商清单
        /// </summary>
        /// <param name="purOrg"></param>
        /// <returns></returns>
        DataTable getExistedList(string purOrg);
        /// <summary>
        /// 获取已经存在的清单信息
        /// </summary>
        /// <param name="existedListInfo"></param>
        /// <returns></returns>
        DataTable getExistedListInfo(string existedListInfo);
     
        DataTable getSupplierLevelInfo(queryCondition queryCondition);

        /// <summary>
        /// 保存供应商清单信息
        /// </summary>
        /// <param name="supplierListModel"></param>
        /// <returns></returns>
        bool createSupplierList(SupplierListModel supplierListModel);
        /// <summary>
        /// 删除清单
        /// </summary>
        /// <param name="supplierListModel"></param>
        void deleteSupplierList(SupplierListModel supplierListModel);
        /// <summary>
        /// 获取记录的条数
        /// </summary>
        /// <param name="queryCondition"></param>
        /// <returns></returns>
        int getListCounts(queryCondition queryCondition);
        /// <summary>
        /// 获取物料记录数
        /// </summary>
        /// <param name="queryCondition"></param>
        /// <returns></returns>
        int getMtInfoCount(string queryCondition);
        /// <summary>
        /// 获取供应源信息
        /// </summary>
        /// <param name="queryCondition"></param>
        /// <returns></returns>
        DataTable getSupplySupplierInfo(queryCondition queryCondition);
        /// <summary>
        /// 获取供应源信息记录数
        /// </summary>
        /// <param name="queryCondition"></param>
        /// <returns></returns>
        int getSupplierInfoCount(queryCondition queryCondition);
        /// <summary>
        /// 获取记录条数
        /// </summary>
        /// <param name="queryCondition"></param>
        /// <returns></returns>
        int getSupLevelCount(queryCondition queryCondition);
        /// <summary>
        /// 设置等级
        /// </summary>
        /// <param name="supplier"></param>
        /// <param name="level"></param>
        /// <param name="iD"></param>
        /// <returns></returns>
        bool setLevel(string supplier, string level, string iD);
        /// <summary>
        /// 记录修改历史
        /// </summary>
        /// <param name="supplierId">供应商编号</param>
        /// <param name="modifyMess">修改信息</param>
        /// <param name="reason">修改理由</param>
        /// <returns></returns>
        bool saveModifyInfo(string supplierId, string modifyMess, string reason);
    }
}
