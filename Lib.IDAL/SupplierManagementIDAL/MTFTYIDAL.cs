﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Lib.IDAL.SupplierManagementIDAL
{
    public interface MTFTYIDAL
    {
        DataTable GetAllMaterialIdDTByFactoryId(string factoryId);
    }
}
