﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.SqlIDAL;
using Lib.Model;

namespace Lib.IDAL
{
    public interface OrganizationInfoIDAL : IBaseDAL<OrganizationInfo>
    {
    }
}
