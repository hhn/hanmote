﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.Model;
using System.Data;

namespace Lib.SqlIDAL
{
    /// <summary>
    /// 供应商管理的数据库访问接口
    /// </summary>
    public interface SupplierManagementIDAL
    {
        //获取所有物料的类型(A类 B类等)
        List<String> GetrAllMaterialType();

        //获取所有供应商编号和供应商名称
        //List<SupplierBaseInfo> GetAllSupplier();
        DataTable GetAllSupplier();

        /// <summary>
        /// 获取所有供应商编码id
        /// </summary>
        /// <returns></returns>
        DataTable GetAllSupplierBaseId();

        /// <summary>
        /// 通过供应商编号获取供应商所有物料组
        /// </summary>
        /// <returns></returns>
        DataTable GetAllMaterialGroupBySupplierID(string supplierID);

        /// <summary>
        /// 通过供应商物料组设置支出类型
        /// </summary>
        /// <returns></returns>
        bool SetExpenseTypeByMaterialGroup(string MaterilGroup);

        /// <summary>
        /// 通过供应商物料组设置支出类型
        /// </summary>
        /// <returns></returns>
        bool SetExpenseTypeByMaterialGroup(DataTable dt);

        /// <summary>
        /// 通过供应商编号和物料组设置支出类型  (插入)
        /// </summary>
        /// <returns></returns>
        bool SetExpenseTypeBySupplierIDAndMaterialGroup(string supplierID, string MaterilGroup);

        /// <summary>
        /// 通过供应商编号获取供应商品项信息
        /// </summary>
        /// <param name="SupplierID"></param>
        /// <returns></returns>
        DataTable GetItemsInfoByMaterialGroupID(string ID);

        /// <summary>
        /// 根据供应商的编号更新供应商品项信息
        /// </summary>
        /// <param name="SupplierID"></param>
        /// <param name="materialGroup"></param>
        /// <returns></returns>
        bool UpdateItemsInfoBySupplierIDAndMaterialGroup(string supplierID, string materialGroup);

        ///// <summary>
        ///// 批量更新供应商的品项信息
        ///// </summary>
        ///// <param name="supplierID"></param>
        ///// <returns></returns>
        //bool UpdateItemsInfoBath(string supplierID);

        bool UpdateTempState(string supplierID, int state);

        /// <summary>
        /// 获取所有的物料组
        /// </summary>
        /// <returns></returns>
        DataTable GetAllMaterialGroup();

        /// <summary>
        /// 通过物料ID获取质量特性
        /// </summary>
        /// <param name="materialID"></param>
        /// <returns></returns>
        bool GetQuliatyPropertyByMaterialID(string materialID);

        /// <summary>
        /// 获取所有的供应商认证模板
        /// </summary>
        /// <returns></returns>
        List<CertificationTemplate> GetAllCertificationTemplate();

        bool saveCertificationTemplate(DataTable dt);

        bool saveSelfCerAndBaseInfoDecision(DecisionInfo obj);


        OrganizationInfo getOrganizationInfoByCompanyName(string companyName);

        List<OrgContacts> getOrgContactsByCompanyName(string companyName);

        DataTable getAllCompany();

        DataTable getFirstDegree();
        DataTable getSecondDegree();
        DataTable getThirdDegree();
        bool saveSEMScore(SEMModel obj);
        List<SEMModel> getSEMScoreByCompanyName(string companyName);

        String getSupplierIdBySupplierName(string supplierName);

        DataTable getMaterialName(string type, string filed);

        int getCertificationStatusBySupplierName(string supplierName);

        bool ModifyProcessingStatus(int status, string supplierName);

        string GetFileOnServerPath(string tableName, string supplierName);

        DecisionInfo GetCerAndBaseinfo(string supplierName);
    }
}
