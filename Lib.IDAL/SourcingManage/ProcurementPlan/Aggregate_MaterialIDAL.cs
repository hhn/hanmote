﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.Model.SourcingManage.ProcurementPlan;

namespace Lib.IDAL.SourcingManage.ProcurementPlan
{
    /// <summary>
    /// 添加需求计划对应的物料
    /// </summary>
    public interface Aggregate_MaterialIDAL
    {
        int addAggregateMaterial(Aggregate_Material am);

        List<Aggregate_Material> getAllAggregates();

    }
}
