﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.Model.SourcingManage.ProcurementPlan;
using System.Data;

namespace Lib.IDAL.SourcingManage.ProcurementPlan
{
    public interface DemandIDAL
    {
        int addNewStandard(Summary_Demand summary_Demand);

        int editNewStandard(Summary_Demand summary_Demand);

        Boolean SameStandardByDemand_ID(string demand_ID);

        DataTable FindStandardDemand_ID(String itemName);

        DataTable FindStandardDemand_ID();

        DataTable StandardByWLBH(String item1, String item2, String item3);

        int changeState(String state, String demand_ID);

        int changePrice(String price, String demand_ID);

        DataTable FindMaterial(String itemName, String table);

        DataTable FindMaterialItems(String itemName, String table);
        List<String> FindAddItems(String id, String idName, String itemName, String table);

        DataTable AddItemsToCombobox(String itemName, String table);

        int addNewPrice_Condition(String table, String id, String Condition, String Price_Level, String Rounding_Rule, Boolean Sign, float Value_Rate);

        DataTable Supplier_Base(int item1, String item2, String item3);


    }
}
