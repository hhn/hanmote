﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.Model.SourcingManage.ProcurementPlan;
using System.Data;

namespace Lib.IDAL.SourcingManage.ProcurementPlan
{
    public interface Summary_DemandIDAL
    {
        Summary_Demand findSummaryDemandByDemandID(string demandId);

        int addNewStandard(Summary_Demand summary_Demand);

        DataTable GetAllSummaryDemand();

        int editNewStandard(Summary_Demand summary_Demand);

        int editDemandById(String demand_Id, String supplier_Id, String inquiry_Id);

        Boolean SameStandardByDemand_ID(string demand_ID);

        DataTable FindStandardDemand_ID(String itemName);

        DataTable FindStandardDemand_ID();

        DataTable StandardByWLBH(String item1, String item2, String item3);

        int changeState(String state, String demand_ID);

        int changePrice(String price, String demand_ID);

        int changeReviewTime(String reviewTime, String demand_ID);

        DataTable FindMaterial(String itemName, String table);

        DataTable FindMaterialItems(String itemName, String table);
        List<String> FindAddItems(String id, String idName, String itemName, String table);

        DataTable AddItemsToCombobox(String itemName, String table);

        int addNewPrice_Condition(String table, String id, String Condition, String Price_Level, String Rounding_Rule, Boolean Sign, float Value_Rate);

        DataTable Supplier_Base(int item1, String item2, String item3);

        List<Summary_Demand> findDemand(Dictionary<string, string> conditions);

        int UpdateSummaryDemand(Summary_Demand demand);

        int deleteSummaryDemandById(string demandId);

        int reviewDemandState(Summary_Demand demand);

        List<string> findReviewedDemands(string item);

        List<Summary_Demand> findDemandsInLimitTime(string startTime, string endTime);
    }
}
