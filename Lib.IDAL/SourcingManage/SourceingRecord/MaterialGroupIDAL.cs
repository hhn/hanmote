﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.Model.SourcingManage.SourceingRecord;

namespace Lib.IDAL.SourcingManage.SourceingRecord
{
    public interface MaterialGroupIDAL
    {
        List<MaterialGroup> getAllMaterialGroup();
    }
}
