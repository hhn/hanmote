﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Lib.Model.SourcingManage;
using Lib.Model.SourcingManage.SourcingManagement;

namespace Lib.IDAL.SourcingManage.SourcingManagement
{
    public interface SourceSupplierIDAL
    {
        int addSourceSupplier(SourceSupplier ss);

        List<SourceSupplier> findSuppliersBySId(string sourceId);

        List<SourceSupplier> getBidSupplier();

        List<SourceSupplier> getBidSuppliersByBidId(string p);

        List<SourceSupplier> getBidsBySupplierId(string sid);

        int updateTransState(string bid, string sid, int res);

        List<SourceSupplier> getAvailableSuppliersByBId(string bidId);

        SourceSupplier getSupplierByBidState(string bidId);

        string countTaskNumberByUserId(string user_ID);
    }
}
