﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.Model.SourcingManage.SourcingManagement;

namespace Lib.IDAL.SourcingManage.SourcingManagement
{
    public interface ReadQuotaIDAL
    {

        List<Quota_Arrangement_Item> getInfoByMaterialId(string mid);
    }
}
