﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.Model.SourcingManage.SourcingManagement;

namespace Lib.IDAL.SourcingManage.SourcingManagement
{
    public interface TransactCostIDAL
    {

        int addTransactCost(TransactCost tc);

        List<TransactCost> getTransCostsBySId(string bid,string sid);
    }
}
