﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.Model.SourcingManage.SourcingManagement;

namespace Lib.IDAL.SourcingManage.SourcingManagement
{
     public interface SourceBidIDAL
    {
          int addBid(SourceBid bid);

          List<SourceBid> getBids();

          SourceBid findBidByBidId(string bidId);

          int updateBidState(SourceBid bid);

          List<SourceBid> getSourceBidsByState(int state);

          int updateBidReleaseTime(string bidId);
    }
}
