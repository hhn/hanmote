﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.Model.SourcingManage.SourcingManagement;

namespace Lib.IDAL.SourcingManage.SourcingManagement
{
    public interface TransactItemIDAL
    {
        int addTransactItem(TransactItem tt);

        List<TransactItem> getTransactItemsByBId(string bidId,string supplierId);
    }
}
