﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Lib.Model.SourcingManage;
using Lib.Model.SourcingManage.SourcingManagement;

namespace Lib.IDAL.SourcingManage.SourcingManagement
{
    public interface SourceIDAL
    {
        int addSource(Source source);

        List<Source> findAllSources(string item);

        int updateSourcePath(string sourceId, string path);

        Source findSourceById(string sourceId);


    }
}
