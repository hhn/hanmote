﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Lib.Model.SourcingManage;

namespace Lib.IDAL.SourcingManage.SourcingManagement
{
    public interface SourceItemMaterialIDAL
    {
        int addSourceItemMaterial(SourceItemMaterial sim);

        #region 为创建合同服务
        List<SourceItemMaterial> getSourceItemMaterialByFK(string contractID);
        #endregion    
    }
}
