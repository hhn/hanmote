﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace Lib.IDAL.StockIDAL
{
     public interface StockListIDAL
    {
        /// <summary>
        /// 查询满足条件的物料
        /// </summary>
        /// <param name="dt0"></param>
        /// <returns></returns>
         DataTable stocklist(DataTable dt0);
         
         /// <summary>
        ///  格式处理
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
         DataTable changestyle(DataTable info, bool bl);
    }
}
