﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace Lib.IDAL.StockIDAL
{
    public interface AdjustDifferenceIDAL
    {
        /// <summary>
        /// 写入差异调整凭证
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        bool genAdjustDocument(DataTable dt);

        /// <summary>
        /// 由物料凭证编号获取凭证一般信息
        /// </summary>
        /// <param name="documentid"></param>
        /// <returns></returns>
        DataTable ShowDifferenceAdjustcommonDAL(string documentid);

         /// <summary>
        /// 由凭证编号选择差异调整物料基本信息
        /// </summary>
        /// <param name="documentid"></param>
        /// <returns></returns>
        DataTable ShowDifferenceAdjustDAL(string documentid);

        /// <summary>
        /// 判断凭证是否可冲销
        /// </summary>
        /// <param name="documentid"></param>
        /// <returns></returns>
        bool canceled(string documentid);

        /// <summary>
        /// 更新cancel字段
        /// </summary>
        /// <param name="documentid"></param>
        /// <returns></returns>
        bool updatecancel(string documentid);

         /// <summary>
        /// 产生取消凭证
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        bool genCancelDocument(DataTable dt);

        /// <summary>
        /// 更新Inventory表数据
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        bool updateInventory(DataTable dt);
    }

      
}
