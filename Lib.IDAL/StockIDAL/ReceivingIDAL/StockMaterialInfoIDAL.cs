﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.Model.StockModel;

namespace Lib.IDAL.StockIDAL
{
    public interface StockMaterialInfoIDAL
    {
        /// <summary>
        /// 根据订单id来查找与该订单关联的物料信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        OrderMatReceivingModel findOrderMatInfoById(string orderId);
    }
}
