﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Lib.IDAL.StockIDAL
{
    public interface SaftyInventoryIDAL
    {
        /// <summary>
        /// 初始化待输入界面（手动设置安全库存）
        /// </summary>
        /// <returns></returns>
        DataTable fillInput();

        /// <summary>
        /// 初始化待输入界面（系统计算安全库存）
        /// </summary>
        /// <returns></returns>
        DataTable fillCompute();

        /// <summary>
        /// 更新安全库存表中手动输入值
        /// </summary>
        /// <param name="FactoryID"></param>
        /// <param name="MaterialID"></param>
        /// <param name="count"></param>
        void updateInputCount(string FactoryID, string MaterialID, float count);

        /// <summary>
        /// 更新计算结果
        /// </summary>
        /// <param name="FactoryID"></param>
        /// <param name="MaterialID"></param>
        /// <param name="MAD"></param>
        /// <param name="W"></param>
        /// <param name="R"></param>
        void updateComputeCount(string FactoryID, string MaterialID, double MAD, double W, double R);

        /// <summary>
        /// 将需求信息写入需求表中
        /// </summary>
        /// <param name="dt"></param>
        void inputdemand(DataTable dt);

        /// <summary>
        /// 读取需求量
        /// </summary>
        /// <param name="factoryid"></param>
        /// <param name="materialid"></param>
        /// <returns></returns>
        DataTable selectDemand(string factoryid, string materialid);

        /// <summary>
        /// 计算MAD
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        double computeMAD(DataTable dt);

        /// <summary>
        /// 由订货次数和缺货次数计算安全系数
        /// </summary>
        /// <param name="ordertime"></param>
        /// <param name="shorttime"></param>
        /// <returns></returns>
        double computeSaftyFactor(int ordertime, int shorttime);

        /// <summary>
        /// 判断需求量信息是否填写
        /// </summary>
        /// <param name="factoryid"></param>
        /// <param name="materialid"></param>
        /// <returns></returns>
        bool madExist(string factoryid, string materialid);

        /// <summary>
        ///查询安全库存
        /// </summary>
        /// <param name="factoryid"></param>
        /// <param name="materialid"></param>
        /// <returns></returns>
        DataTable showSaftyStock(string factoryid, string materialid);

        /// <summary>
        /// 获取物料ID
        /// </summary>
        /// <returns></returns>
        DataTable getMaterialID();

        /// <summary>
        /// 获取工厂ID
        /// </summary>
        /// <returns></returns>
        DataTable getFactoryID();
    }
}
