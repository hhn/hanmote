﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Lib.IDAL.StockIDAL
{
    public interface ConsignmentStockIDAL
    {
        DataTable showStock(DataTable dt0);
    }
}
