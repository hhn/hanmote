﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace Lib.IDAL.StockIDAL.StockReport
{
    public interface FactoryStockIDAL
    {
        DataTable showStock(DataTable dt0);
    }
}
