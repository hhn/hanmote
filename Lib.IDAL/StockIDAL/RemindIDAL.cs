﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Lib.IDAL.StockIDAL
{
    public interface RemindIDAL
    {
        /// <summary>
        /// 将待设置的物料选取出来
        /// </summary>
        /// <returns></returns>
        DataTable genTable();

        //写入用户设置的提前提醒天数，并对到期提醒时间进行计算
        void fillPreDay(string factoryID, string stockID, string materialID, string batchID, int days);

        //检查是否有要提醒的物料，若有，写入提醒表中
        DataTable reminder();

         /// <summary>
        /// 处理到期物料
        /// </summary>
        /// <param name="factoryID"></param>
        /// <param name="stockID"></param>
        /// <param name="materialID"></param>
        /// <param name="batchID"></param>
         void handle(string factoryID, string stockID, string materialID, string batchID, string prolongdays);

        /// <summary>
        ///选出当前物料及入库日期
        /// </summary>
        /// <returns></returns>
         DataTable accountManage();

    }
}
