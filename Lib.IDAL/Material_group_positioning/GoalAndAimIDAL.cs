﻿using Lib.Model.MT_GroupModel;
using Lib.Model.PrdModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Lib.IDAL.Material_group_positioning
{
    public interface GoalAndAimIDAL
    {
        #region 生产性物料组
        //获取产品编号
         DataTable GetPrdId();
        //获取产品名称
        DataTable GetPrdName(string text);
        //获取组成产品的物料
        DataTable getMaterialInfo(string text, string goalId, int pageSize, int pageIndex);
        //插入供应目标
        int insertGoalInfo(prdGoalModel goalId);
        //获取供应目标
        DataTable getPrdAim(string text, int pageSize, int pageIndex);
        //插入供应指标
        int insertIndicatorInfo(PrdIndicatorModel prdIndicatorModel);
        /// <summary>
        /// 获取物料组类型编号
        /// </summary>
        /// <returns></returns>
        DataTable getMtGroupTypeId();
        /// <summary>
        /// 根据物料组类型获取物料组信息
        /// </summary>
        /// <param name="mtGroupTypeId">物料组类型</param>
        /// <returns></returns>
        DataTable getMaterialGroupInfo(string mtGroupTypeId, int pageSize, int pageIndex);
        /// <summary>
        /// 根据物料组查询物料的供应目标
        /// </summary>
        /// <param name="mtId"></param>
        /// <returns></returns>
        DataTable getAssertGoal(string mtId, int pageSize, int pageIndex);
        /// <summary>
        /// 插入供应目标信息
        /// </summary>
        /// <param name="groupGoalModel1"></param>
        /// <returns></returns>
        int insertAssertAndMainInfo(GroupGoalModel groupGoalModel1);
        /// <summary>
        /// 获取产品目标条数
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        int getGoalCount(string text);
        /// <summary>
        /// 获取物料组信息
        /// </summary>
        /// <returns></returns>
        DataTable getMtGroupInfo();
        bool insertPrdInfoAndMaterial(PrdModel prdModel);
        bool getGoalId(string goalId);
        bool getMaterials(string mtGroupId1);

        /// <summary>
        /// 查询是否存在该产品
        /// </summary>
        /// <param name="prdId"></param>
        /// <returns></returns>
        bool getPrdId(string prdId);

        /// <summary>
        /// 保存产品数据
        /// </summary>
        /// <param name="prdModel"></param>
        /// <returns></returns>
        bool insertPrdInfo(PrdModel prdModel);

        /// <summary>
        /// 获取产品指标条数
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        int getIndicationCount(string text);
        /// <summary>
        /// 获取特定类型物料组记录总数信息
        /// </summary>
        /// <param name="typeId">类型编号</param>
        /// <returns></returns>
        int getMtGroupCount(string typeId);
        /// <summary>
        //对应物料组指标记录总数信息
        /// </summary>
        /// <param name="mtId">类型编号</param>
        /// <returns></returns>
        int getIndicateCount(string mtId);
        /// <summary>
        /// 获取物料信息
        /// </summary>
        /// <param name="mtGroupTypeId"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageIndex"></param>
        /// <returns></returns>
        DataTable getMaterialInfo(string mtGroupTypeId, int pageSize, int pageIndex);
        /// <summary>
        /// 获取物料记录条数
        /// </summary>
        /// <param name="mtGroupTypeId"></param>
        /// <returns></returns>
        int getMtInfoCount(string mtGroupTypeId);

        #endregion

    }
}
