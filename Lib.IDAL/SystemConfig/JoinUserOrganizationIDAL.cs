﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

using Lib.Model.SystemConfig;

namespace Lib.IDAL.SystemConfig
{
    public interface JoinUserOrganizationIDAL
    {
        /// <summary>
        /// 插入用户组织机构联系
        /// </summary>
        /// <param name="userOrgModel"></param>
        void insertUserOrganizationLink(JoinUserOrganizationModel userOrgModel);

        /// <summary>
        /// 根据组织机构ID删除所有关联用户
        /// </summary>
        /// <param name="userOrgModel"></param>
        void deleteUserOrganizationLinkByOrgId(JoinUserOrganizationModel userOrgModel);

        /// <summary>
        /// 删除组织机构和用户指定的关联
        /// </summary>
        /// <param name="userOrgModel"></param>
        void deleteUserOrganizationLinkByOrgIdAndUserId(JoinUserOrganizationModel userOrgModel);

        /// <summary>
        /// 根据组织机构编号查询其关联的员工编号
        /// </summary>
        /// <param name="organizationID"></param>
        /// <returns></returns>
        DataTable queryUserInforByOrganizationID(String organizationID);
    }
}
