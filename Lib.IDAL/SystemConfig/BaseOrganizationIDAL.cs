﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

using Lib.Model.SystemConfig;

namespace Lib.IDAL.SystemConfig
{
    public interface BaseOrganizationIDAL
    {
        /// <summary>
        /// 查询最大的可用组织机构编号
        /// </summary>
        /// <returns></returns>
        DataTable queryBiggestOrganizationID(String today);

        /// <summary>
        /// 插入新的组织机构
        /// </summary>
        /// <param name="organiztionModel"></param>
        void insertOrganization(BaseOrganizationModel organiztionModel);

        /// <summary>
        /// 查询父级节点的Right_Value
        /// </summary>
        /// <param name="organizationID"></param>
        /// <returns></returns>
        DataTable queryParentRightValue(String organizationID);

        /// <summary>
        /// 查询某组织机构信息
        /// </summary>
        /// <param name="orgModel"></param>
        DataTable queryOrganizationInfor(BaseOrganizationModel orgModel);

        /// <summary>
        /// 更新某组织机构信息
        /// </summary>
        /// <param name="orgModel"></param>
        void updateOrganizationInfor(BaseOrganizationModel orgModel);

        /// <summary>
        /// 查询所有组织机构信息
        /// </summary>
        /// <returns></returns>
        DataTable queryAllOrganizationInfor();

        /// <summary>
        /// 查询当前组织机构的所有下属机构信息
        /// </summary>
        /// <param name="orgModel"></param>
        /// <returns></returns>
        DataTable queryOffspringOrganization(BaseOrganizationModel orgModel);
        
        /// <summary>
        /// 删除当前组织机构及所有下属机构，并更新相应节点的左右值
        /// </summary>
        /// <param name="orgModel"></param>
        void deleteOffspringOrganizationAndUpdateNodes(BaseOrganizationModel orgModel);
    }
}
