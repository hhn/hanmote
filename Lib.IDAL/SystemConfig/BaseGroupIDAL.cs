﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using Lib.Model.SystemConfig;

namespace Lib.IDAL.SystemConfig
{
    public interface BaseGroupIDAL
    {
        /// <summary>
        /// 查询系统用户组、组下面用户信息
        /// </summary>
        /// <returns></returns>
        DataTable queryGroupAndUserName();

        /// <summary>
        /// 获取最大的组编号
        /// </summary>
        /// <param name="today"></param>
        /// <returns></returns>
        DataTable queryBiggestGroupID(String today);

        /// <summary>
        /// 插入一个新的用户组
        /// </summary>
        /// <param name="groupModel"></param>
        void insertNewGroup(BaseGroupModel groupModel);

        /// <summary>
        /// 根据组编号查询用户组信息
        /// </summary>
        /// <param name="groupModel"></param>
        /// <returns></returns>
        DataTable queryGroupInforById(BaseGroupModel groupModel);

        /// <summary>
        /// 根据用户组编号更新用户组信息
        /// </summary>
        /// <param name="groupModel"></param>
        void updateGroupInforById(BaseGroupModel groupModel);

        /// <summary>
        /// 删除用户组
        /// </summary>
        /// <param name="groupModel"></param>
        void deleteGroup(BaseGroupModel groupModel);
    }
}
