﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Lib.SqlIDAL
{
    public interface ConstractManagementIDAL
    {
        DataTable GetConstractInfo(string purchaseOrg, string supplierNameOrID, string constractNameOrID);
    }
}
