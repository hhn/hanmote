﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.Model.MD.Order;

namespace Lib.IDAL.MDIDAL.Order
{
   public interface OrderIDAL
    {
       /// <summary>
       /// 写入订单数据
       /// </summary>
       /// <param name="odif"></param>
       /// <returns></returns>
     bool InsertOrderInfo(OrderInfo odif, List<OrderItem> list);
       /// <summary>
       /// 读取所有完成/未完成的采购申请
       /// </summary>
       /// <param name="finishedmark"></param>
       /// <returns></returns>
     List<PR_Supplier> GetAllPrSupplierRelation(bool finishedmark);
       /// <summary>
       /// 通过物料编码和供应商编码获取对应的采购信息记录
       /// </summary>
       /// <param name="list"></param>
       /// <returns></returns>
       List<InfoRecord> GetInforecordByMaterilSuplier(List<MaterialSupplier> list);
       /// <summary>
       /// 通过订单编码查询订单头数据
       /// </summary>
       /// <param name="OrderId"></param>
       /// <returns></returns>
       OrderInfo GetOrderInfoByID(string Orderid);
       /// <summary>
       /// 通过订单编码查询订单项目信息
       /// </summary>
       /// <param name="orderitem"></param>
       /// <returns></returns>
      List<OrderItem> GetOrderItemByID(string orderid);
       /// <summary>
       /// 通过订单编码查询发票信息
       /// </summary>
       /// <param name="orderid"></param>
       /// <returns></returns>
      List<InvoiceFront> GetInvoiceFrontByID(string orderid);
       /// <summary>
       /// 通过订单编码查询物料入库信息
       /// </summary>
       /// <param name="orderid"></param>
       /// <returns></returns>
      List<MaterialDocumentMM> GetMaterialDocumentMMByID(string orderid);
       


      
    }
}
