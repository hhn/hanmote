﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.Model;
using System.Data;



namespace Lib.SqlIDAL
{
    public interface BatchFeatureIDAL
    {
        //获取所有固定特性
       List<string> GetAllFixedFeatures();
        //向数据库中写入批次信息
       bool InsertBatchFeatures(string BatchID,string name,string value);
        //根据所选固定特性读取其默认值
       List<string> GetAllValue(string name);
        //通过批次编码获取批次特性
       DataTable GetFeaturesByID(string ID);
       //判断是否录入了重复的批次特性信息
       bool RepeatedBatchFeatureInformation(string batchid,string featurename);
        //判断是否录入了重复的批次
       bool RepeatedBatch(string BatchID);
       
    }
}
