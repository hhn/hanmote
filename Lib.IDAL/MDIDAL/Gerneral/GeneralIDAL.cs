﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Lib.Model.MD;
using Lib.Model.MD.GN;
using Lib.Model.MD.SP;
using Lib.Model.MT_GroupModel;

namespace Lib.IDAL.MDIDAL.Gerneral
{
    public interface GeneralIDAL
    {
        #region 付款条件设置
        //获取所有付款条件名称
        List<string> GetAllPaymentClauseName();
        //获取所有付款条件名称及描述
        DataTable GetAllPaymentClause();
        //通过所选付款条件得到条件具体内容
        PaymentClause GetClause(string ID);
        //新建付款条件
        bool NewPaymentClause(string ClauseID, string Description,float FirstDiscount, int FirstDate, float SecondDiscount, int SecondDate, int FinalDate);
        //删除付款条件
        bool DeletePaymentClause(string ID);
        //根据物料组名称查询该物料组下的所有物料
        DataTable GetAllMaterialNameByMaterialGroup(string materialGroupName);
        DataTable getAllTabMtGroupType();
        #endregion

        #region 维护物料信息

        List<string> getAllMaterialIDList();
        DataTable getMaterialInfo(string materialID);

        #endregion

        #region 维护物料组

        //根据采购组织ID获取采购组织名称
        DataTable GetBuyer_OrgNameByBuyer_Org(string Buyer_Org);

        //获取所有评审员信息编号
        List<string> GetAllPinshenName();
        //根据采购组织编码获取该组织下的所有采购组编码
        List<string> GetAllBuyerGroupByBuyer_Org(string Buyer_Org);
        DataTable GetAllSupplier_pinshen();//获取所有评审员信息
        DataTable GetSelectedEval(string fengong);//获取选定评审员的姓名
        string  GetSelectedEvalid(string fengong);//获取选定评审员的id
        DataTable GetAllBuyerOrganizationName();


        //新建物料组
        bool NewSupplier_pinshen(string id, string name, string fengong, string email, string phone, string sphone, string password);
        //删除物料组
        bool DeleteSupplier_pinshen(string ID);
        bool UpdateSupplier_pinshen(string id,string userid, string name, string password, string fengong, string email, string phone, string sphone);
        List<string> GetAllGroupName();//获取所有物料组编号
        //获取所有物料组
        DataTable GetAllMaterialGroup(int pageSize,int pageIndex);

        //新建物料组
        bool NewMaterialGroup(MaterialGroupModel materialGroupModel);
        bool NStatus(string description);
        void Ndelete(string description);
        //删除物料组
        bool DeleteMaterialGroup(string ID);
        bool UpdateMaterialGroup(MaterialGroupModel materialGroupModel);
        #endregion 

        #region 一般设置
        #region 杂项
        //获取所有公司代码
        List<string> GetCompanyCode();
        //获取所有货币类型
        List<string> GetCurrencyType();
        //获取所有评估类
        List<string> GetEvaluationClass();
        //获取所有供应商
        List<string> GetAllSupplier();
        //通过所选供应商获取供应商描述
        string GetSuppplierDescription(string SupplierID);
       

        //获取所有税码
        List<string> GetTaxCode();
        //通过所选税码获取税码描述
        string GetTaxDescription(string TaxID);
        #endregion
        #region 维护工厂
        //获取所有工厂名称
        List<string> GetAllFactory();
        //根据公司名称获取工厂代码
        List<string> GetAllFactory(string companyID);
        //获取所有工厂信息
        DataTable GetAllFactoryInformation();
        //新建工厂信息
        bool NewFactory(string Factory_ID, string factory_name, string company_code);
        //通过所选工厂获取所属库存地
        List<string> GetAllStock(string FactoryID);
        //删除所选工厂
        bool DeleteFactory(String Factory_ID);
        //通过工厂代码获取工厂详细信息
        Factory GetFactoryInformationByID(string FactoryID);

        #endregion
        #region 维护计量单位
        //获取所有计量单位名称
        List<string> GetAllMeasurement();
        //获取所有计量单位信息
        DataTable GetAllMeasurementInformation();
        //新建计量单位信息
        bool NewMeasurement(string meid, string mename, string remename, float ratio,string type);
        //删除所选计量单位
        bool DeleteMeasurement(string meid);
        #endregion
        # region 维护仓库
        //获取所有仓库名称
        List<string> GetAllStock();
        //获取所有仓库信息
        DataTable GetAllStockInformation();
        //新建仓库信息
        bool NewStock(string stockid, string stockname, string factoryid);
        //删除所选仓库
        bool DeleteStock(string stock);
        #endregion
        #region  维护公司代码
        //获取所有公司名称
        List<string> GetAllCompany();
        //获取所有公司详细信息
        DataTable GetAllCompanyInformation();
        //新建公司信息
        bool NewCompany(string cyid, string cyname);
        //删除所选公司信息
        bool DeleteCompany(string Company);
        #endregion
        #region 维护采购组
        //获取所有采购组名称
        List<string> GetAllBuyerGroup();
        //获取所有采购组信息
        DataTable GetAllBuyerGroupInformation();
        //新建采购组信息
        bool NewBuyerGroup(string gpid, string gpname, string mttype);
        //删除采购组信息
        bool DeleteBuyerGroup(string gpid);
        #endregion
        #region 维护采购组织
        //获取所有采购组织名称
        List<string> GetAllBuyerOrganization();
        //获取所有货币,否则返回空
        List<string> GetAllCurrency();
        //获取所有采购组织信息
        DataTable GetAllBuyerOrganizationInformation();
        //新建采购组织信息
        bool NewBuyerOrganization(string boid, string boname);
        //删除采购组织信息
        bool DeleteBuyerOrganization(string boid);
        //读取采购组织-物料组关系信息
        List<PorgMtGropRelation> GetPorgMtgpRelation();
        //写入采购组织-物料组关系信息
        bool NewPorgMtgpRelation(List<PorgMtGropRelation> list);
        //获取采购组织下的物料组-评审员关系
        Dictionary<string,object> getMtGroupAndMasterRelation(string purOrgId);
        //保存采购组织-物料组-评审员关系
        int saveEvalInfo(string mtGroupName, string purOrgName, string evaluaterId);
        #endregion
        #region 维护支付条件
        //获取所有交付条件编号
        List<string> GetAllTradeClauseID();
        //
        DataTable GetTradeClauseByID(string clauseID);
        //获取支付条件的具体信息
        DataTable GetAllTradeClauseInformation();
        //新建支付条件信息
        bool NewTradeClauseInformation(string ClauseID, string ClauseName, string Description);
        //删除支付条件信息
        bool DeleteTradeClause(string ClauseID);
        #endregion 
        #region 维护评估类
        //获取所有评估类名称
        List<string> GetAllEvaluationClass();
        //获取评估类的具体信息
        DataTable GetAllEvaluationClassInformation();
        //新建评估类信息
        bool NewEvaluationClass(string ClassID, string ClassName, string Description);
        //删除评估类信息
        bool DeleteEvaluationClass(string ClassID);
        #endregion
        #region 维护产品组
        //获取所有产品组代码
        List<string> GetAllDivision();
        //获取所有产品组的具体信息
        DataTable GetAllDivisionInformation();
        //新建产品组信息
        bool NewDivision(string DivisionID, string DivisionName, string Description);
        string getUnitByMateriID(string mtName);

        //删除所选产品组
        bool DeleteDivision(string DivisionID);
        #endregion
        #region 维护税码
        //获取所有税码
        List<string> GetAllTaxCode();
        //获取所有税码详细信息
        DataTable GetAllTaxCodeInformation();
        //新建税码信息
        bool NewTaxCode(string TaxID, double tax, string Description);
        //删除所选税码
        bool DeleteTaxCode(string TaxID);
        #endregion
        #region 维护国家
        //获取所有国家(中文）
        List<string> GetAllCountryName();
        //获取所有国家（英文缩写）
        List<string> GetAllCountryENG();
        //获取所有国家代码
        List<string> GetAllCountryCode();
        //根据国家名称选取所属城市
        List<string> GetAllCity(string countryname);
        #endregion
        #region 维护装运评分标准
        //获取所有装运评分标准编号
        List<string> GetAllLoadingCriteriaID();
        //通过装运评分标准编号获取其具体描述
        string GetCriteriaDescriptionByID(string ID);
        //获取装运评分表具体信息
        DataTable GetAllLoadingCriteriaInformation();
        //新建转运评分标准
        bool NewLoadingCriteria(string CriteriaID, string description);
        //删除装运评分标准
        bool DeleteLoadingCriteria(string CriteriaID);
        #endregion
        #region 维护工业标识
        ////获取所有工业标识代码
        //List<string> GetAllIndustryCategoryID();
        ////获取所有工业标识详细信息
        //DataTable GetAllIndustryCategoryInformation();
        ////新建工业标识信息
        //bool NewIndustryCategory(
        #endregion
        #region 维护凭证类型
        //获取所有凭证类型代码
        List<string> GetAllProofType();
        //根据凭证代码获取凭证名称
        string GetProofNameByType(string typeid);
        // 获取供应商的完整信息
        List<SupplierBase> GetAllSuppliers();
        List<SupplierBase> GetAllSuppliersByIdsLimit(List<string> supplierIds);
        List<string> GetAlPurchGroupID();
        List<string> GetAllMtGroupID(String code);
        bool NewPorMtGroup(string porg_ID, string porg_Name, string mtGroup_id, string mtGroup_Name, string mt_Grop_Manager,int isVisible);
        DataTable GetAllPurMtGroupInfo();
        bool MDPorMtGroup(string delpurGroup_id, string delpurGroup_Name, string delmtGroup_id, string delmtGroup_Name,string purGroup_id, string purGroup_Name, string mtGroup_id, string mtGroup_Name);

        List<string> getAllManagerID();
        DataTable getEvalPersonInfo(string evalItemName);

        #endregion
        #endregion

    }
}
