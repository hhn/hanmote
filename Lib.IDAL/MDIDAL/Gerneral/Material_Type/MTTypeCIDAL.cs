﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Lib.IDAL.MDIDAL.Gerneral.Material_Type
{
    public interface MTTypeCIDAL
    {
        //获取所有小分类
        DataTable GetAllLittleClassfy();
        //获取所有大分类名称
        List<string> GetAllBigClassfyName();
        //新建小分类信息
        bool NewLittleClassfy(string lID, string lname, string bname);
        //删除选定小分类
        bool DeleteLittleClassfy(string ID);
        //获取所有类型名称
        List<string> GetAllTypeName();
        
    }
}
