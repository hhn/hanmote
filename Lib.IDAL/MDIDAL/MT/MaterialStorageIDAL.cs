﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.Model.MD.MT;

namespace Lib.IDAL.MDIDAL.MT
{
   public interface MaterialStorageIDAL
    {
       //通过物料编号获取物料无组织机构级别信息
        MaterialBase GetBasicInformation(string MaterialID);
        //通过物料编号和工厂编号获取物料工厂级别信息
        MaterialFactory GetMtFtyInformation(string MaterialID, string FactoryID);
        //更新物料无组织机构级别信息(存储视图)
        bool UpdateBasicInformation(MaterialBase material);
        //更新物料工厂级别信息(存储视图)
        bool UpdateMtFtyInformation(MaterialFactory mtfty);
       //通过物料编号，工厂编号，库存地编号获取物料库存地级别信息
        MaterialStorage GetMTSteInformation(string MaterialID, string FactoryID,string StockID);
       //更新物料的库存地级别信息(存储视图)
        bool UpdateMTSteInformation(MaterialStorage mtste);

    }
}
