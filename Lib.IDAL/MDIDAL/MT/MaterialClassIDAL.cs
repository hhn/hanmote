﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.Model.MD.MT;
using System.Data;
using Lib.Model.MD.SP;

namespace Lib.IDAL.MDIDAL.MT
{
   public interface MaterialClassIDAL
    {
       ///// <summary>
       ///// 根据物料编码读取分类编码
       ///// </summary>
       ///// <param name="materialid"></param>
       ///// <returns></returns>
       //Material_Class GetClassIDByMaterialID(string materialid);
       /// <summary>
       /// 根据分类编码读取特性值信息
       /// </summary>
       /// <param name="classid"></param>
       /// <returns></returns>
       List<Class_Feature> GetFeatureInfoByClassID(string classid);
       /// <summary>
       /// 根据分类编码和物料编码读取物料特性值信息
       /// </summary>
       /// <param name="classid"></param>
       /// <param name="mtid"></param>
       /// <returns></returns>
       List<Material_Feature> GetFeatureInfoByClassIDMtID(string classid, string mtid);
       /// <summary>
       /// 写入物料特性值
       /// </summary>
       /// <param name="listmtf"></param>
       /// <returns></returns>
       bool InsertFeatureInfo(List<Material_Feature> listmtf);
       /// <summary>
       /// 写入特性类对应特性描述
       /// </summary>
       /// <param name="listfea"></param>
       /// <returns></returns>
       bool InsertClassFeature(List<Class_Feature> listfea);
       /// <summary>
       /// 更新物料特性值
       /// </summary>
       /// <param name="listmtf"></param>
       /// <returns></returns>
       bool UpdateFeatureInfo(List<Material_Feature> listmtf);
       /// <summary>
       /// 更新特性类对应特性描述
       /// </summary>
       /// <param name="listfea"></param>
       /// <returns></returns>
       bool UpdateClassFeature(List<Class_Feature> listfea);
       /// <summary>
       /// 查询所有分类编码及名称
       /// </summary>
       /// <returns></returns>
       DataTable GetDistinctClassInfo();
       /// <summary>
       /// 根据分类编码和供应商编码读取供应商特性值信息
       /// </summary>
       /// <param name="classid"></param>
       /// <param name="spid"></param>
       /// <returns></returns>
       List<Supplier_Feature> GetFeatureInfoByClassIDSPID(string classid, string spid);
       /// <summary>
       /// 写入供应商特性值
       /// </summary>
       /// <param name="listmtf"></param>
       /// <returns></returns>
       bool InsertFeatureInfosp(List<Supplier_Feature> listspfe);
       /// <summary>
       /// 更新供应商特性值
       /// </summary>
       /// <param name="listmtf"></param>
       /// <returns></returns>
       bool UpdateFeatureInfosp(List<Supplier_Feature> listspfe);

       
    }
}
