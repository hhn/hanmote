﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.Model.MD.MT;

namespace Lib.IDAL.MDIDAL.MT
{
   public interface MaterialIDAL
    {
        //获取所有物料名称(非MPN)
        List<string> GetAllMaterialName();
        // //获取所有物料名称(MPN)
        List<string> GetAllMaterialNameMPN();
       //获取所有物料编码
        List<string> GetAllMaterialID();
       //更新物料无组织机构级别数据(基本视图）
        bool UpdateBasicInformation(MaterialBase material);
       //通过物料编码获取物料无组织机构级别数据
        MaterialBase GetMaterialBasicInformation(string materialID);


        //获取所有物料信息
        List<MaterialBase> GetMaterialBases(string condition);

        //获取所有（不包含在list中）物料基本信息
        List<MaterialBase> GetMaterialBasesInIDLimit(List<string> materialIds,string condition);

        MaterialBase getMaterialBaseById(string materialId);

    }
}
