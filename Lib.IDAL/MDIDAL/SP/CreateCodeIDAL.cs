﻿using Lib.Model.MD.SP;
using Lib.Model.SystemConfig;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Lib.IDAL.MDIDAL.SP
{
    public interface CreateCodeIDAL
    {
        DataTable getspname(string name);
        DataTable getspname();
        bool checkonce(string loginAccount);
        string updataLoginAccount(string supplierID, string loginAccount);
        DataTable getsupplierinfo(string supplier_name, string supplier_loginaccount_type);
        DataTable getcompanynamecode(string companyname);
        DataTable getbuyernamecode(string buyername);
        void insertSuppliercompanycode(string fCompanyCode, string fCompanyname, string v1, string v2);
        void insertSupplierpurchasingorg(string fBuyerID, string fBuyername, string v1, string v2);
        void insertorupdatesupplierbase(string v1, string v2);
    }
}
