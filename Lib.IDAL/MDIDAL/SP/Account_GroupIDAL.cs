﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Lib.Model.MD.SP;

namespace Lib.IDAL.MDIDAL.SP
{
    public interface Account_GroupIDAL
    {
        bool CheckOnce(string account_Group_ID);
        string insert(Account_Group account_Group);
        Account_Group selectbyid(string account_Group_ID);
        string update(Account_Group account_Group);
        List<Account_Group> selectALL();
        List<Account_Group> selectbydescrption(string aGDC);
        DataTable getAccountGroup(string accountGroupName, string accountGroupSinglestatus);
    }
}
