﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Lib.Model.MD.SP;

namespace Lib.IDAL.MDIDAL.SP
{
    public interface NSIDAL
    {
        Boolean CheckID(string ID);
        string insert(Number_segement number_Segement);
        Number_segement selectbyID(string mNID);
        string update(Number_segement number_Segement);
        DataTable selectbyname(string v);
        DataTable selectAllID();
        string update(string nS_id, string nS_status);
    }
}
