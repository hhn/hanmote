﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.Model.MD.SP;
using System.Data;

namespace Lib.IDAL.MDIDAL.SP
{
   public interface SupplierBaseIDAL
    {
       //通过供应商编码查询供应商无组织机构级别数据
       SupplierBase GetSupplierBasicInformation(string SupplierID);
       //更新供应商无组织机构级别数据
       bool UpdateSupplierBasicInformation(SupplierBase spb);
       //获取所有供应商编码
       List<string> GetAllSupplierID();
       //获取所有供应商基本信息
       DataTable GetAllSupplierBasicInformation();
       //通过供应商编码获取供应商名称
       string GetSupplierNameBySupplierID(string spid);
       //获取所有供应商信息
       List<SupplierBase> GetAllSupplierBase();

       List<SupplierBase> getSupplierBaseByMG(string materialGroup);

       //获取表中的总记录条数
       DataTable GetTotalCount();

        //分页查询供应商基础数据
       DataTable GetSupplierInforByPage(int pageSize, int pageIndex);

    }
}
