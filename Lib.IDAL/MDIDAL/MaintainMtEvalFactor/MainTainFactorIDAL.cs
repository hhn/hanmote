﻿using Lib.Model.MD;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Lib.IDAL.MDIDAL.MaintainMtEvalFactor
{
   public interface MainTainFactorIDAL
    {
        #region
        /// <summary>
        /// 获取主标准库
        /// </summary>
        /// <returns>主标准数据</returns>
        DataTable GetMtFactor();
        /// <summary>
        /// 获取次标准
        /// </summary>
        /// <param name="mainEvalFactor">主标准编码</param>
        /// <returns>次标准数据</returns>
        DataTable getSubEvalFactor(string mainEvalFactor);
        /// <summary>
        /// 添加次标准
        /// </summary>
        /// <param name="mainEvalFactor"></param>
        /// <returns>是否操作成功</returns>
        bool addSubEvalFactor(SubEvalFactor mainEvalFactor);
        /// <summary>
        /// 添加主标准
        /// </summary>
        /// <param name="mainEvalFactor"></param>
        /// <returns>操作是否成功</returns>
        bool addMainEvalFactor(MainEvalFactor mainEvalFactor);
        /// <summary>
        /// 删除主标准
        /// </summary>
        /// <param name="mainEvalFactor"></param>
        /// <returns>操作是否成功</returns>
        bool deleteMainEvalFactor(string mainEvalFactor);
        /// <summary>
        /// 修改主标准
        /// </summary>
        /// <param name="mainEvalFactor"></param>
        /// <param name="code"></param>
        /// <returns></returns>
        bool updateMainEval(MainEvalFactor mainEvalFactor,string code);
        /// <summary>
        /// 删除次标准
        /// </summary>
        /// <param name="subEvalCode"></param>
        /// <returns></returns>
        bool deleteSubEvalFactor(string subEvalCode);
        /// <summary>
        /// 跟新次标准
        /// </summary>
        /// <param name="subEvalFactor"></param>
        /// <param name="code"></param>
        /// <returns></returns>
        bool updateSubEval(SubEvalFactor subEvalFactor, string code,string mainCode);
        #endregion
    }
}
