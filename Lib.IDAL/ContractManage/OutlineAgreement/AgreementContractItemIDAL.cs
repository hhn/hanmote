﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.Model.ContractManage.OutlineAgreement;

namespace Lib.IDAL.ContractManage.OutlineAgreement
{
    public interface AgreementContractItemIDAL
    {
        int addAgreementContractItem(Agreement_Contract_Item item);

        List<Agreement_Contract_Item> getAgreementContractItemListByID(
            string agreementContractID, string agreementContractVersion);

        int updateAgreementContractItem(string agreementContractID,
            string agreementContractVersion, Agreement_Contract_Item item);

        int deleteAgreementContractItem(string agreementContractID,
            string agreementContractVersion);
    }
}
