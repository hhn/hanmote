﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.Model.ContractManage.OutlineAgreement;

namespace Lib.IDAL.ContractManage.OutlineAgreement
{
    public interface AgreementContractIDAL
    {
        Agreement_Contract getAgreementContractByID(string agreementContractID,
            string agreementContractVersion);

        List<Agreement_Contract> getAgreementContractByCondition(
            Dictionary<string, object> conditions, DateTime beginTime, DateTime endTime);

        int updateAgreementContract(string agreementContactID,
            string agreementContractVersion, Agreement_Contract agreementContract);

        int updateAgreementContract(Agreement_Contract agreementContract,
            List<Agreement_Contract_Item> itemList);

        int deleteAgreementContractByID(string agreementContractID,
            string agreementContractVersion);

        int addAgreementContract(Agreement_Contract agreementContract, 
            List<Agreement_Contract_Item> itemList);

        int changeAgreementContract(Agreement_Contract oldAgreementContract,
            Agreement_Contract newAgreementContract, List<Agreement_Contract_Item> itemList);
    }
}
