﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;


namespace Lib.SqlIDAL.SupplierPerformaceIDAL
{
    public interface SupplierPerformanceIDAL
    {
        #region 供应商评估

        #region 价格标准的旧代码
        //价格-价格水平
        DataTable queryPriceLevel(String materialID, String supplierID, DateTime startTime, DateTime endTime, List<double> priceLevelPara);

        //价格-价格历史
        DataTable queryPriceHistory(String materialID, String supplierID, DateTime startTime,DateTime endTime, List<double> priceHistoryPara);
        #endregion 

        #region 价格水平标准的新代码

        //查询订单号（Enquiry_ID）
        DataTable queryOrderID(String materialID, String supplierID,DateTime startTime, DateTime endTime);

        //查询供应商有效价格
        Decimal querySupplierPrice(String materialID, String supplierID, DateTime startTime, DateTime endTime);

        //查询市场价格
        Decimal queryMarketPrice(String materialID,String supplierID,DateTime startTime,DateTime endTime);

        #endregion 

        #region 价格历史标准的新代码

        //供应商历史价格，不需要使用订单ID，只有在查询市场当前价格时需要使用订单ID
        Decimal queryHistorySupplierPrice(String materialID, String supplierID, DateTime startTime, DateTime endTime);

        //市场历史价格，不需要使用订单ID，只有在查询市场当前价格时需要使用订单ID；不需要使用供应商ID
        Decimal queryHistoryMarketPrice(String materialID, DateTime startTime, DateTime endTime);

        Decimal queryHistorySupplierPrice0(String materialID, String supplierID, DateTime startTime, DateTime endTime);

        Decimal queryHistoryMarketPrice0(String materialID, DateTime startTime, DateTime endTime);

        #endregion

        //质量-收货
        DataTable queryReceiveGoods(String materialID, String supplierID, DateTime startTime, DateTime endTime,List<double> PPMPara);

        //质量-质量审计
        DataTable queryQualityAudit(String materialID, String supplierID,DateTime startTime,DateTime endTime);

        //质量-抱怨/拒绝水平
        DataTable queryComplaintReject(String materialID,String supplierID,String purchasingORGID,DateTime startTime,DateTime endTime);
        DataTable queryComplaintReject(String materialID, String supplierID, DateTime startTime, DateTime endTime);

        //交货-按时交货的表现
        DataTable queryDeliverOnTime(String materialID, String supplierID, String purchasingORGID,DateTime startTime,DateTime endTime);
        DataTable queryDeliverOnTime(String materialID, String supplierID, DateTime startTime, DateTime endTime);


        //交货-确认日期
        DataTable queryConfirmDate(String materialID, String supplierID, String purchasingORGID,DateTime startTime,DateTime endTime,double confirmDatePara);
        DataTable queryConfirmDate(String materialID, String supplierID, DateTime startTime, DateTime endTime, double confirmDatePara);
       
        //交货-数量可靠性
        DataTable queryQuantityReliability(String materialID, String supplierID,DateTime startTime,DateTime endTime);

        //交货-对装运须知的遵守
         DataTable queryGoodsShipment(String materialID, String supplierID,DateTime startTime,DateTime endTime);

        //一般服务/支持
         DataTable queryServiceSupport(String materialID, String supplierID,DateTime startTime,DateTime endTime);

        //外部服务
         DataTable queryExternalService(String materialID,String supplierID, DateTime startTime,DateTime endTime);

         void nonQuerySupplierPerformance(String supplierID,String materialID,Decimal score,String createTime,String creatorName);

        //结果显示
         DataTable queryResult(String materialID, String supplierID, String purchasingORGID);

        //根据物料ID和年份查询绩效评估结果记录
         DataTable queryResult(String materialID, String year);

        //详细结果
         DataTable queryDetailResult(String materialID, String supplierID, String purchasingORGID,int idNum);

        //保存
         int saveResult(List<Object> saveRstPara);

        /// <summary>
        /// 计算年度交易总额
        /// </summary>
        /// <param name="materialID"></param>
        /// <param name="supplierID"></param>
        /// <param name="year"></param>
        /// <returns></returns>
         DataTable queryTransactionAmount(String materialID, String supplierID, String year);

        /// <summary>
        /// 保存抱怨/拒绝记录
        /// </summary>
        /// <param name="saveCRPara"></param>
        /// <returns></returns>
         int saveComplaintRejectRecord(List<Object> saveCRPara);

        /// <summary>
        /// 保存一般服务/支持数据
        /// </summary>
        /// <param name="saveSSPara"></param>
        /// <returns></returns>
         int saveServiceSupportRecord(List<Object> saveSSPara);

        /// <summary>
        /// 保存外部服务数据
        /// </summary>
        /// <param name="saveESPara"></param>
        /// <returns></returns>
         int saveExternalServiceRecord(List<Object> saveESPara);

        /// <summary>
        /// 保存质量审核-产品审核数据
        /// </summary>
        /// <param name="savePAPara"></param>
        /// <returns></returns>
         int saveProductAuditRecord(List<Object> savePAPara);

        /// <summary>
         /// 保存过程/体系审核记录
        /// </summary>
        /// <param name="savePSAuditPara"></param>
        /// <param name="chapterInfo"></param>
        /// <returns></returns>
         int savePSAuditRecord(List<Object> savePSAuditPara,List<String> chapterInfo,List<Object> scoreList);

        /// <summary>
        /// 查询供应商主数据中的供应商id、供应商名称
        /// </summary>
        /// <returns></returns>
         DataTable querySupplier();

        /// <summary>
        /// 根据供应商id，查询主数据中的采购组织id、采购组织名称
        /// </summary>
        /// <returns></returns>
         DataTable queryPurchasingORG(String supplierID);
         DataTable queryPurchasingORG();

        /// <summary>
        /// 根据供应商id，查询主数据中的物料id、物料名称
        /// </summary>
        /// <param name="supplier"></param>
        /// <returns></returns>
         DataTable queryMaterial(String supplier);

        /// <summary>
        /// 查询某个物料类型对应的所有物料id列表
        /// </summary>
        /// <param name="materialType"></param>
        /// <returns></returns>
         DataTable queryMaterialList(String materialType);

        /// <summary>
        /// 查询无供应商评估记录的供应商id，供应商名称
        /// </summary>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <returns></returns>
         DataTable queryNoSupplierPerformanceList(DateTime startTime, DateTime endTime);

        /// <summary>
        /// 查询物料类型
        /// </summary>
        /// <param name="materialID"></param>
        /// <returns></returns>
        String queryMaterialType(String materialID);

        /// <summary>
        /// 查询所有供应商类别
        /// </summary>
        /// <returns></returns>
        DataTable queryCategory();

        /// <summary>
        /// 查询物料分类
        /// </summary>
        /// <returns></returns>
        DataTable queryMaterialClassify();

        #endregion

        #region 供应商分级

        /// <summary>
        /// 查询供应商当前年份评估总分的平均值
        /// </summary>
        /// <param name="supplierID"></param>
        /// <returns></returns>
        DataTable queryTotalResult(String supplierID, DateTime start, DateTime end);

        /// <summary>
        /// 保存供应商分级结果
        /// </summary>
        /// <param name="saveClassifyRst"></param>
        /// <returns></returns>
        int saveClassificationResult(List<Object> saveClassifyRst);

        /// <summary>
        /// 查看供应商分级结果
        /// </summary>
        /// <param name="supplierID"></param>
        /// <param name="year"></param>
        /// <returns></returns>
        DataTable queryClassificationResult(String supplierID, String year);

        /// <summary>
        /// 返回最新一次供应商分级的结果
        /// </summary>
        /// <param name="supplierID"></param>
        /// <param name="year"></param>
        /// <returns></returns>
        String queryNewClassificationResult(String supplierID, String timePeriod);

        /// <summary>
        /// 分组查询供应商分级结果
        /// </summary>
        /// <param name="purchasingORGID"></param>
        /// <param name="category"></param>
        /// <param name="year"></param>
        /// <returns></returns>
        DataTable queryClassificationGroupResult(String purchasingORGID, String category, String year);

        #endregion

        #region 供应商批量评估

        /// <summary>
        /// 查询物料ID
        /// </summary>
        /// <returns></returns>
        DataTable queryMaterial();

        /// <summary>
        /// 查询与该物料有过交易的供应商
        /// </summary>
        /// <param name="materialID"></param>
        /// <returns></returns>
        DataTable querySupplier(String materialID);

        #endregion

        #region 数据录入

        /// <summary>
        /// 保存问卷结果，GoodsReceiveSurvey_Scores
        /// </summary>
        /// <param name="saveList"></param>
        /// <returns></returns>
        int saveQuestionaire(List<Object> saveList);

        #endregion

        /// <summary>
        /// 查询供应商名称
        /// </summary>
        /// <param name="supplierID"></param>
        /// <returns></returns>
        String querySupplierName(String supplierID);

        /// <summary>
        /// 查询物料名称
        /// </summary>
        /// <param name="materialID"></param>
        /// <returns></returns>
        String queryMaterialName(String materialID);

        /// <summary>
        /// 查询Supplier_Performance表中记录ID对应的物料ID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        String queryMaterialIDFromSP(String ID);

        /// <summary>
        /// 查询Supplier_Performance表中记录ID对应的供应商ID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        String querySupplierIDFromSP(String ID);

        /// <summary>
        /// 查询Supplier_Performance表中记录ID对应的采购组织ID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        String queryPurchasingORGIDFromSP(String ID);
        /// <summary>
        /// 获取绿色目标和最低目标值
        /// </summary>
        /// <param name="supplierID">供应商的编号</param>
        /// <returns></returns>
        DataTable getPGreenValueAndPLowValue(string supplierID);
    }
}
