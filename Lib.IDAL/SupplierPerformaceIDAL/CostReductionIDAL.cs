﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Lib.IDAL.SupplierPerformaceIDAL
{
    public interface CostReductionIDAL
    {
        /// <summary>
        /// 当月采购额
        /// </summary>
        /// <param name="supplierID"></param>
        /// <param name="materialID"></param>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <returns></returns>
        Decimal queryPurchaseAmountThisMonth(String supplierID, String materialID, String year, String month);

        /// <summary>
        /// 查询上年市场基准价
        /// </summary>
        /// <param name="supplierID"></param>
        /// <param name="materialID"></param>
        /// <param name="year"></param>
        /// <returns></returns>
        Decimal queryMarketPriceLastYear( String materialID, String year);

        /// <summary>
        /// 查询上年末采购价
        /// </summary>
        /// <param name="supplierID"></param>
        /// <param name="materialID"></param>
        /// <param name="year"></param>
        /// <returns></returns>
        Decimal queryPurchasePriceLastYearEnd(String supplierID, String materialID, String year);

        /// <summary>
        /// 查询当月市场基准价
        /// </summary>
        /// <param name="materialID"></param>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <returns></returns>
        Decimal queryMarketPriceThisMonth( String materialID, String year, String month);

        /// <summary>
        /// 查询当月采购价
        /// </summary>
        /// <param name="supplierID"></param>
        /// <param name="materialID"></param>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <returns></returns>
        Decimal queryPurchasePriceThisMonth(String supplierID, String materialID, String year, String month);

        /// <summary>
        /// 判断是否为市场管制品种
        /// </summary>
        /// <param name="materialID"></param>
        /// <returns></returns>
        bool queryControledOrNot(String materialID);

        /// <summary>
        /// 保存降成本计算的记录
        /// </summary>
        /// <param name="savePara"></param>
        /// <returns></returns>
        int saveCostReduction(List<Object> savePara);

        /// <summary>
        /// 查看符合时间条件的所有降成本记录
        /// </summary>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <returns></returns>
        DataTable queryCostReductionResult(String year, String month);
    }
}
