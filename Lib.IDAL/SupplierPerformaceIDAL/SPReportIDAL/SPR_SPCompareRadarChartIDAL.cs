﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using Lib.ContionSettings.SupplierPerformaceCS.SPReportCS;

namespace Lib.IDAL.SupplierPerformaceIDAL
{
    public interface SPR_SPCompareRadarChartIDAL:SPR_BaseIDAL
    {
        /// <summary>
        /// 查询供应商列表
        /// 条件
        /// 采购组织ID、年度、时段
        /// </summary>
        /// <param name="selectSupplierConditionSettings"></param>
        /// <returns></returns>
        DataTable getAllSupplierInfo(SelectSupplierConditionSettings selectSupplierConditionSettings);

        /// <summary>
        /// 查询供应商评分结果
        /// </summary>
        /// <param name="conditionvalue"></param>
        /// <returns></returns>
        DataTable getSupplierEvaluationScore(SPCompareRadarChartConditionValue conditionvalue);
    }
}
