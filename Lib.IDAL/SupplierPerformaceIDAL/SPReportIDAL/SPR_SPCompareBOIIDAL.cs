﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using Lib.ContionSettings.SupplierPerformaceCS.SPReportCS;

namespace Lib.IDAL.SupplierPerformaceIDAL
{
    public interface SPR_SPCompareBOIIDAL:SPR_BaseIDAL
    {
        /// <summary>
        /// 取得供应商行业信息
        /// </summary>
        /// <param name="selectSupplierConditionSettings">查询条件</param>
        /// <returns></returns>
        DataTable getSupplierIndustryInfo(SelectSupplierConditionSettings selectSupplierConditionSettings);

        /// <summary>
        /// 基于行业的供应商评估比较
        /// </summary>
        /// <param name="spcCondition">查询条件</param>
        /// <returns></returns>
        DataTable getEvaluationSupplierIScoreBaseIndustry(SPCompareBaseOnBOIConditionValue condtionValue);
    }
}
