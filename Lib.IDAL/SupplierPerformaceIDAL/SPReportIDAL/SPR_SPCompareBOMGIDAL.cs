﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using Lib.ContionSettings.SupplierPerformaceCS.SPReportCS;

namespace Lib.IDAL.SupplierPerformaceIDAL
{
    /// <summary>
    /// 基于物料组的供应商评估比较
    /// </summary>
    public interface SPR_SPCompareBOMGIDAL:SPR_BaseIDAL
    {
        /// <summary>
        /// 取得物料组信息
        /// </summary>
        /// <param name="selectSupplierConditionSettings">查询条件</param>
        /// <returns></returns>
        DataTable getMaterialGroupInfo(SelectSupplierConditionSettings selectSupplierConditionSettings);


        /// <summary>
        /// 基于物料组的供应商评估比较
        /// </summary>
        /// <param name="spcCondition">查询条件</param>
        /// <returns></returns>
        DataTable getEvaluationSupplierScore(SPCompareBaseOnMGConditionValue spcCondition);

    }
}
