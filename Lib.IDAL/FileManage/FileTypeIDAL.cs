﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.Model.FileManage;

namespace Lib.IDAL.FileManage
{
    public interface FileTypeIDAL
    {
        List<string> getAllFileTypeName();
        File_Type getFileType(string fileTypeName);
        int deleteFileType(string fileTypeName);
        int addFileType(File_Type fileType);
        int updateFileType(File_Type fileType);
    }
}
