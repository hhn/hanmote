﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Lib.Model.CertificationProcess.GN;


namespace Lib.IDAL.CertificationProcess
{
    public interface  GeneralIDAL
    {
        DataTable GetAllInfo(string id);
        DataTable page(string UserID, int pagesize, int currentpage,out int totalSize);//分页
        DataTable GetAllInfoe(String userId);//评审员刷数据

        DataTable GetAllInfo1(String userId,int pageSize,int pageIndex,out int total);
        DataTable GetAllInfo2(string id);
        DataTable GetAllInfo3(string id);
        void updateEvaluate(string obj1, string obj2, string obj3, string obj4, string obj5);
        void deal1(string obj1, string obj2, string obj3, string obj4, string obj5);
        void insert(string id, string thing);
        void inserttask(string id, string thing);
        void Updatetask(string zhuid, string sname);
        void Updatetask1(string zhuid, string sname);
        void updateTask(string id, string thing);
        DataTable GetAll(string id);
        DataTable GetThingLevel(string id);
        DataTable Getzhuid(string id);
        void RestartInfo(string email);
        void adelete(string email);
        void adeletetask(string zzid,string zid);
        void adeletetask1(string zzid);
        void adeletetask2(string zzid);
        void deal(string id, string reason, string score, string sum, string isno,string eid);
        void deal1(string reason,string id);
        void RestartInf(string email);
        void RestartInf1(string email);
        void RestartInf2(string email);
        void RestartInf3(string id, string gid, string User_ID, string level, string reason);
        void pdelete(string id);
        void prdelete(string id,string eid);
        void RejectInf(string email);
        void RejectInf1(string email);
        void deleteInfo(string email);
        void restart(string email);//预评重置
        void deleteInfo1(string email, string eid);
        void Delete(string id);
        void RestartInfo0(string email);//筛选筛选重置
        void RestartInf0(string email);//筛选状态置1和旧的
        void RejectInf0(string email);//筛选拒绝状态置1和旧的
        bool Get(string email);//状态查询
        bool Get1(string email);//状态查询
        bool GetStatus(string email);//状态查询
        bool GetTaskStatus(string email);//状态查询
        bool GetStatus1(string email);//状态查询
        bool GetStatus2(string email, string id);//状态查询
        bool fGetStatus(string id);//状态查询
        bool ffGetStatus(string id);//状态查询
        void GetStatus3(string email, string id);//状态查询
        DataTable GetInfo(string userId, string purchase, string thingGroup, string status, string country, string name);//准入
        DataTable eGetInfo(string uid,string purchase, string thingGroup, string status, string country, string name);//评估员查询
        DataTable GetInfo1(string userId, string purchase, string thingGroup, string status, string country, string name);//预评
        DataTable GetInfo2(string purchase,string name,string id);//建立评估小组查询
        DataTable checkdetail(string id);//评分详情查看
        void back(string id, string eid);
        DataTable getid(string id);
        DataTable getscore(string id);
        DataTable getsum(string id);
        DataTable getthing(string thing);
        void updateAndDeleteTask(string user_ID, string supplierName);
        DataTable getEClassMtName();
    }
}
