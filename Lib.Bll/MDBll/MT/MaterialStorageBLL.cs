﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.IDAL.MDIDAL.MT;
using DALFactory;
using Lib.Model.MD.MT;
namespace Lib.Bll.MDBll.MT
{
   public class MaterialStorageBLL
    {
       private static readonly MaterialStorageIDAL idal = DALFactoryHelper.CreateNewInstance<MaterialStorageIDAL>("MDDAL.MT.MaterialStorageDAL");
       /// <summary>
       /// 通过物料编号获取物料无组织机构信息
       /// </summary>
       /// <param name="MaterialID"></param>
       /// <returns></returns>
      public MaterialBase GetBasicInformation(string MaterialID)
       {
           return idal.GetBasicInformation(MaterialID);
       }
       /// <summary>
       /// 通过物料编号和工厂编号获取物料工厂级别的信息
       /// </summary>
       /// <param name="MaterialID"></param>
       /// <param name="FactoryID"></param>
       /// <returns></returns>
      public MaterialFactory GetMtFtyInformation(string MaterialID, string FactoryID)
       {
           return idal.GetMtFtyInformation(MaterialID, FactoryID);
       }
       /// <summary>
       /// 更新物料无组织机构级别信息(存储视图)
       /// </summary>
       /// <param name="material"></param>
       /// <returns></returns>
      public bool UpdateBasicInformation(MaterialBase material)
       {
           return idal.UpdateBasicInformation(material);
       }
       /// <summary>
       /// 更新物料工厂级别信息(存储视图)
       /// </summary>
       /// <param name="mtfty"></param>
       /// <returns></returns>
      public bool UpdateMtFtyInformation(MaterialFactory mtfty)
       {
           return idal.UpdateMtFtyInformation(mtfty);
       }
       /// <summary>
       /// 通过物料编号，工厂编号，库存地编号获取物料库存地级别信息
       /// </summary>
       /// <param name="MaterialID"></param>
       /// <param name="FactoryID"></param>
       /// <param name="StockID"></param>
       /// <returns></returns>
      public MaterialStorage GetMTSteInformation(string MaterialID, string FactoryID, string StockID)
       {
           return idal.GetMTSteInformation(MaterialID, FactoryID, StockID);
       }
       /// <summary>
       /// 更新物料的库存地级别信息(存储视图)
       /// </summary>
       /// <param name="mtste"></param>
       /// <returns></returns>
      public bool UpdateMTSteInformation(MaterialStorage mtste)
       {
           return idal.UpdateMTSteInformation(mtste);
       }

    }
}
