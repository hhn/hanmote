﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.IDAL.MDIDAL.MT;
using DALFactory;
using Lib.Model.MD.MT;

namespace Lib.Bll.MDBll.MT
{
   public class MaterialBLL
    {

       private static readonly MaterialIDAL idal = DALFactoryHelper.CreateNewInstance<MaterialIDAL>("MDDAL.MT.MaterialDAL");
       /// <summary>
       /// 获取所有物料名称
       /// </summary>
       /// <returns></returns>
      public List<string> GetAllMaterialName()
       {
           return idal.GetAllMaterialName();
       }
       /// <summary>
       /// 获取所有物料编码(非MPN)
       /// </summary>
       /// <returns></returns>
      public List<string> GetAllMaterialID()
      {
          return idal.GetAllMaterialID();
      }
       /// <summary>
      /// 获取所有物料编码(MPN)
       /// </summary>
       /// <returns></returns>
     public List<string> GetAllMaterialNameMPN()
      {
          return idal.GetAllMaterialNameMPN();
      }
       /// <summary>
      /// 更新物料无组织机构级别数据(基本视图）
       /// </summary>
       /// <param name="material"></param>
       /// <returns></returns>
     public bool UpdateBasicInformation(MaterialBase material)
      {
          return idal.UpdateBasicInformation(material);
      }
       /// <summary>
      /// 通过物料编码获取物料无组织机构级别数据
       /// </summary>
       /// <param name="materialID"></param>
       /// <returns></returns>
     public MaterialBase GetMaterialBasicInformation(string materialID)
      {
          return idal.GetMaterialBasicInformation(materialID);
      }

        /// <summary>
        /// 获取所有物料基本信息
        /// </summary>
        /// <returns></returns>
     public List<MaterialBase> GetMaterialBases(string condition)
     {
         return idal.GetMaterialBases(condition);
     }

       /// <summary>
       /// 获取所有物料基本信息（不包括list包含的内容）
       /// </summary>
       /// <param name="materialIds"></param>
       /// <returns></returns>
     public List<MaterialBase> GetMaterialBasesInIDLimit(List<string> materialIds,string condition)
     {
         return idal.GetMaterialBasesInIDLimit(materialIds,condition);
     }

     public MaterialBase getMaterialBaseById(string materialId)
     {
         return idal.getMaterialBaseById(materialId);
     }

    }
}
