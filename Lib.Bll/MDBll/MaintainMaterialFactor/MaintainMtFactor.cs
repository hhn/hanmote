﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using DALFactory;
using Lib.IDAL.MDIDAL.MaintainMtEvalFactor;
using Lib.Model.MD;

/// <summary>
/// 该模块是进行物料评价要素得维护
/// </summary>
namespace Lib.Bll.MDBll.MaintainMaterialFactor
{
    public class MaintainMtFactor
    {
        private static readonly MainTainFactorIDAL idal = DALFactoryHelper.CreateNewInstance<MainTainFactorIDAL>("MaintianEvalFactorDAL");
        /// <summary>
        /// 获取主标准
        /// </summary>
        /// <returns></returns>
        public DataTable getMtEvalFactor() {

            return idal.GetMtFactor();
        }
        /// <summary>
        /// 获取主标准对应得次标准
        /// </summary>
        /// <param name="mainEvalFactor">主标准代码</param>
        /// <returns></returns>
        public DataTable getSubEvalFactor(string mainEvalFactor)
        {
            return idal.getSubEvalFactor(mainEvalFactor);
        }

        public bool addSubEvalFactor(SubEvalFactor subEvalFactor)
        {
            return idal.addSubEvalFactor(subEvalFactor);
        }

        public bool addMainEvalFactor(MainEvalFactor mainEvalFactor)
        {
            return idal.addMainEvalFactor(mainEvalFactor);
        }

        public bool deleteMainEvalFactor(string mainEvalFactor)
        {
            return idal.deleteMainEvalFactor(mainEvalFactor);
        }

        public bool updateMainEval(MainEvalFactor mainEvalFactor,string code)
        {
            return idal.updateMainEval(mainEvalFactor,code);
        }

        public bool deleteSubEvalFactor(string subEvalCode)
        {
            return idal.deleteSubEvalFactor(subEvalCode);
        }
        /// <summary>
        /// 更新次标准
        /// </summary>
        /// <param name="subEvalFactor"></param>
        /// <param name="code"></param>
        ///  /// <param name="code"></param>   
        /// <returns></returns>
        public bool updateSubEval(SubEvalFactor subEvalFactor, string code,string mainCode)
        {
            return idal.updateSubEval(subEvalFactor, code,mainCode);
        }
    }




}
