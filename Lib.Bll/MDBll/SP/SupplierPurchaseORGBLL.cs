﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.IDAL.MDIDAL.SP;
using DALFactory;
using Lib.Model.MD.SP;

namespace Lib.Bll.MDBll.SP
{
   public class SupplierPurchaseORGBLL
    {
       private static readonly SupplierPurchaseORGIDAL idal = DALFactoryHelper.CreateNewInstance<SupplierPurchaseORGIDAL>("MDDAL.SP.SupplierPurchaseORGDAL");
       /// <summary>
       /// 查询供应商采购组织级别数据
       /// </summary>
       /// <param name="supplierID"></param>
       /// <param name="PurchaseORGID"></param>
       /// <returns></returns>
     public  SupplierPurchaseOrganization GetSupplierPurchaseOrganizationInformation(string supplierID, string PurchaseORGID)
       {
           return idal.GetSupplierPurchaseOrganizationInformation(supplierID, PurchaseORGID);
       }
       /// <summary>
     /// 更新供应商采购组织级别数据
       /// </summary>
       /// <param name="sporg"></param>
       /// <returns></returns>
     bool UpdateSupplierPurchaseORGinformation(SupplierPurchaseOrganization sporg)
     {
         return idal.UpdateSupplierPurchaseORGinformation(sporg);
     }

    }
}
