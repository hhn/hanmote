﻿using DALFactory;
using Lib.IDAL.MDIDAL.Gerneral;
using Lib.Model.MD;
using Lib.Model.MD.GN;
using Lib.Model.MT_GroupModel;
using System;
using System.Collections.Generic;
using System.Data;

namespace Lib.Bll.MDBll.General
{
    public class GeneralBLL
    {
        private static readonly GeneralIDAL idal = DALFactoryHelper.CreateNewInstance<GeneralIDAL>("MDDAL.General.GeneralDAL");

        #region 一般设置
        #region 杂项
        /// <summary>
        /// 获取公司代码
        /// </summary>
        /// <returns></returns>
        public List<string> GetCompanyCode()
        {
            return idal.GetCompanyCode();
        }
        /// <summary>
        /// get all  TabMtGroupType
        /// </summary>
        /// <returns></returns>
        public DataTable getAllTabMtGroupType()
        {
            return idal.getAllTabMtGroupType();
        }

        /// <summary>
        /// 获取货币类型
        /// </summary>
        /// <returns></returns>
        public List<string> GetCurrencyType()
        {
            return idal.GetCurrencyType();
        }
        /// <summary>
        /// 获取评估类
        /// </summary>
        /// <returns></returns>
        public List<string> GetEvaluationClass()
        {
            return idal.GetEvaluationClass();
        }

        public DataTable GetAllBuyerOrganizationName()
        {
            return idal.GetAllBuyerOrganizationName();
        }

        /// <summary>
        /// 获取所有供应商
        /// </summary>
        /// <returns></returns>
        public List<string> GetAllSupplier()
        {
            return idal.GetAllSupplier();
        }
        /// <summary>
        /// 通过供应商名称获取描述
        /// </summary>
        /// <param name="SupplierID"></param>
        /// <returns></returns>
        public string GetSuppplierDescription(string SupplierID)
        {
            return idal.GetSuppplierDescription(SupplierID);
        }
        /// <summary>
        /// 获取所有税码
        /// </summary>
        /// <returns></returns>
        public List<string> GetTaxCode()
        {
            return idal.GetTaxCode();
        }
        public string GetTaxDescription(string TaxID)
        {
            return idal.GetTaxDescription(TaxID);
        }
        #endregion
        #region 付款条件设置
        #region 获取所有付款条件名称
        public List<string> GetAllPaymentClauseName()
        {
            return idal.GetAllPaymentClauseName();
        }
        /// <summary>
        /// 保存物料组-采购组织-评审员关系
        /// </summary>
        /// <param name="mtGroupName">物料组名称</param>
        /// <param name="orgId">采购组织名称</param>
        /// <param name="evalItemName">评估员编号</param>
        public int saveEvalInfo(string mtGroupName, string purOrgName, string evaluaterId)
        {
            return idal.saveEvalInfo(mtGroupName, purOrgName, evaluaterId);
        }
        #endregion
        #region 获取所有付款条件
        public DataTable GetAllPaymentClause()
        {
            return idal.GetAllPaymentClause();
        }
        #endregion
        #region 通过所选付款条件得到条件具体内容
        public PaymentClause GetClause(string ID)
        {
            return idal.GetClause(ID);
        }
        #endregion
        #region 新建付款条件
        public bool NewPaymentClause(string ClauseID, string Description, float FirstDiscount, int FirstDate, float SecondDiscount, int SecondDate, int FinalDate)
        {
            return idal.NewPaymentClause(ClauseID, Description, FirstDiscount, FirstDate, SecondDiscount, SecondDate, FinalDate);
        }
        #endregion
        #region 删除付款条件
        public bool DeletePaymentClause(string ID)
        {
            return idal.DeletePaymentClause(ID);
        }
        #endregion
        #endregion
        #region 维护物料组
        //获取所有物料组编号
        public List<string> GetAllGroupName()
        {
            return idal.GetAllGroupName();
        }
        //获取所有物料组具体信息
        public DataTable GetAllMaterialGroup(int pageSize,int pageIndex)
        {
            return idal.GetAllMaterialGroup(pageSize, pageIndex);
        }
        public DataTable GetAllSupplier_pinshen()
        {
            return idal.GetAllSupplier_pinshen();
        }

        public DataTable GetSelectedEval(string fengong)//获取选定评审员的姓名
        {
            return idal.GetSelectedEval(fengong);
        }
        public string GetSelectedEvalid(string fengong)//获取选定评审员的姓名
        {
            return idal.GetSelectedEvalid(fengong);
        }

        /// <summary>
        /// 新建物料组
        /// </summary>
        /// <param name="type">物料组类型</param>
        /// <param name="mtId">物料组编号</param>
        /// <param name="description">物料组名称</param>
        /// <param name="evaluate">等级</param>
        /// <param name="evalid">等级描述</param>
        /// <returns></returns>
        public bool NewMaterialGroup(MaterialGroupModel materialGroupModel)
        {
            return idal.NewMaterialGroup(materialGroupModel);
        }
        public bool NStatus(string description)
        {
            return idal.NStatus(description);
        }
        public void Ndelete(string description)
        {
            idal.Ndelete(description);
        }
        //删除物料组
        public bool DeleteMaterialGroup(string ID)
        {
            return idal.DeleteMaterialGroup(ID);
        }
        public bool UpdateMaterialGroup(MaterialGroupModel materialGroupModel)
        {
            return idal.UpdateMaterialGroup(materialGroupModel);
        }
        public bool NewSupplier_pinshen(string id, string name, string fengong, string email, string phone, string sphone, string password)
        {
            return idal.NewSupplier_pinshen(id, name, fengong, email, phone, sphone, password);
        }
        //删除物料组
        public bool DeleteSupplier_pinshen(string ID)
        {
            return idal.DeleteSupplier_pinshen(ID);
        }
        public bool UpdateSupplier_pinshen(string id, string userid, string name, string password, string fengong, string email, string phone, string sphone)
        {
            return idal.UpdateSupplier_pinshen(id, userid, name, password, fengong, email, phone, sphone);
        }
        #endregion
        #region 维护工厂
        /// <summary>
        /// 获取所有工厂代码
        /// </summary>
        /// <returns></returns>
        public List<string> GetAllFactory()
        {
            return idal.GetAllFactory();
        }
        /// <summary>
        /// 获取所有工厂信息
        /// </summary>
        /// <returns></returns>
        public DataTable GetAllFactoryInformation()
        {
            return idal.GetAllFactoryInformation();
        }

        /// <summary>
        /// 获取属于某一公司的所有工厂编号
        /// </summary>
        /// <param name="companyID"></param>
        /// <returns></returns>
        public List<string> GetAllFactory(string companyID)
        {
            return idal.GetAllFactory(companyID);
        }

        public string getUnitByMateriID(string mtName)
        {
            return idal.getUnitByMateriID(mtName);
        }


        /// <summary>
        /// 获取所有所属仓库代码
        /// </summary>
        /// <param name="FactoryID"></param>
        /// <returns></returns>
        public List<string> GetAllStock(string FactoryID)
        {
            return idal.GetAllStock(FactoryID);
        }
        /// <summary>
        /// 新建工厂
        /// </summary>
        /// <param name="Factory_ID"></param>
        /// <param name="factory_name"></param>
        /// <param name="company_code"></param>
        /// <returns></returns>
        public bool NewFactory(string Factory_ID, string factory_name, string company_code)
        {
            return idal.NewFactory(Factory_ID, factory_name, company_code);
        }
        /// <summary>
        /// 删除工厂
        /// </summary>
        /// <param name="Factory_ID"></param>
        /// <returns></returns>
        public bool DeleteFactory(String Factory_ID)
        {
            return idal.DeleteFactory(Factory_ID);
        }
        /// <summary>
        /// 根据工厂编码获取工厂详细信息
        /// </summary>
        /// <param name="FactoryID"></param>
        /// <returns></returns>
        public Factory GetFactoryInformationByID(string FactoryID)
        {
            return idal.GetFactoryInformationByID(FactoryID);
        }
        #endregion
        #region 维护计量单位
        /// <summary>
        /// 获取所有计量单位名称
        /// </summary>
        /// <returns></returns>
        public List<string> GetAllMeasurement()
        {
            return idal.GetAllMeasurement();
        }
        /// <summary>
        /// 根据采购组织ID获取采购组织名称
        /// </summary>
        /// <param name=""></param>
        /// <returns></returns>
        public string GetBuyer_OrgNameByBuyer_Org(string Buyer_Org)
        {
            DataTable dt = idal.GetBuyer_OrgNameByBuyer_Org(Buyer_Org);
            if(dt != null && dt.Rows.Count > 0)
            {
                return Convert.ToString(dt.Rows[0][0]);
            }
            return null;
        }

        /// <summary>
        /// 获取所有计量单位信息
        /// </summary>
        /// <returns></returns>
        public DataTable GetAllMeasurementInformation()
        {
            return idal.GetAllMeasurementInformation();
        }
        /// <summary>
        /// 根据物料组名称查询该物料组下的所有物料名称列表
        /// </summary>
        /// <param name="materialGroupName"></param>
        /// <returns></returns>
        public DataTable GetAllMaterialNameByMaterialGroup(string materialGroupName)
        {
            return idal.GetAllMaterialNameByMaterialGroup(materialGroupName);
        }
        /// <summary>
        /// 根据采购组织ID获取该采购组织下的所有采购组ID
        /// </summary>
        /// <param name="PurchasingORG_ID"></param>
        /// <returns></returns>
        public List<string> GetAllBuyerGroupByBuyer_Org(string Buyer_Org)
        {
            return idal.GetAllBuyerGroupByBuyer_Org(Buyer_Org);
        }

        /// <summary>
        /// 新建计量单位信息
        /// </summary>
        /// <param name="meid"></param>
        /// <param name="mename"></param>
        /// <param name="remename"></param>
        /// <param name="ratio"></param>
        /// <returns></returns>
        public bool NewMeasurement(string meid, string mename, string remename, float ratio, string type)
        {
            return idal.NewMeasurement(meid, mename, remename, ratio, type);
        }
        /// <summary>
        /// 删除所选计量单位
        /// </summary>
        /// <param name="meid"></param>
        /// <returns></returns>
        public bool DeleteMeasurement(string meid)
        {
            return idal.DeleteMeasurement(meid);
        }
        #endregion
        #region 维护仓库
        /// <summary>
        /// 获取所有仓库名称
        /// </summary>
        /// <returns></returns>
        public List<string> GetAllStock()
        {
            return idal.GetAllStock();
        }
        /// <summary>
        /// 获取所有仓库详细信息
        /// </summary>
        /// <returns></returns>
        public DataTable GetAllStockInformation()
        {
            return idal.GetAllStockInformation();
        }
        /// <summary>
        /// 新建仓库信息
        /// </summary>
        /// <param name="stockid"></param>
        /// <param name="stockname"></param>
        /// <param name="factoryid"></param>
        /// <returns></returns>
        public bool NewStock(string stockid, string stockname, string factoryid)
        {
            return idal.NewStock(stockid, stockname, factoryid);
        }
        /// <summary>
        /// 删除所选仓库
        /// </summary>
        /// <param name="stock"></param>
        /// <returns></returns>
        public bool DeleteStock(string stock)
        {
            return idal.DeleteStock(stock);
        }
        #endregion
        #region 维护公司代码
        /// <summary>
        /// 获取所有公司代码名称
        /// </summary>
        /// <returns></returns>
        public List<string> GetAllCompany()
        {
            return idal.GetAllCompany();
        }
        /// <summary>
        /// 获取所有公司详细信息
        /// </summary>
        /// <returns></returns>
        public DataTable GetAllCompanyInformation()
        {
            return idal.GetAllCompanyInformation();
        }
        /// <summary>
        /// 新建公司信息
        /// </summary>
        /// <param name="cyid"></param>
        /// <param name="cyname"></param>
        /// <returns></returns>
        public bool NewCompany(string cyid, string cyname)
        {
            return idal.NewCompany(cyid, cyname);
        }

        /// <summary>
        /// 删除公司信息
        /// </summary>
        /// <param name="Company"></param>
        /// <returns></returns>
        public bool DeleteCompany(string Company)
        {
            return idal.DeleteCompany(Company);
        }
        #endregion
        #region 维护采购组
        /// <summary>
        /// 获取所有采购组编码
        /// </summary>
        /// <returns></returns>
        public List<string> GetAllBuyerGroup()
        {
            return idal.GetAllBuyerGroup();
        }
        /// <summary>
        /// 获取所有采购组详细信息
        /// </summary>
        /// <returns></returns>
        public DataTable GetAllBuyerGroupInformation()
        {
            return idal.GetAllBuyerGroupInformation();
        }
        /// <summary>
        /// 新建采购组
        /// </summary>
        /// <param name="gpid"></param>
        /// <param name="gpname"></param>
        /// <param name="mttype"></param>
        /// <returns></returns>
        public bool NewBuyerGroup(string gpid, string gpname, string mttype)
        {
            return idal.NewBuyerGroup(gpid, gpname, mttype);
        }
        /// <summary>
        /// 删除选定采购组
        /// </summary>
        /// <param name="gpid"></param>
        /// <returns></returns>
        public bool DeleteBuyerGroup(string gpid)
        {
            return idal.DeleteBuyerGroup(gpid);
        }
        #endregion
        #region 维护采购组织
        /// <summary>
        /// 获取所有采购组织名称
        /// </summary>
        /// <returns></returns>
        public List<string> GetAllBuyerOrganization()
        {
            return idal.GetAllBuyerOrganization();
        }
        /// <summary>
        /// 获取所有货币列表，否则返回空
        /// </summary>
        /// <returns></returns>
        public List<string> GetAllCurrency()
        {
            return idal.GetAllCurrency();
        }


        /// <summary>
        /// 获取所有采购组织详细信息
        /// </summary>
        /// <returns></returns>
        public DataTable GetAllBuyerOrganizationInformation()
        {
            return idal.GetAllBuyerOrganizationInformation();
        }
        /// <summary>
        /// 新建采购组织信息
        /// </summary>
        /// <param name="boid"></param>
        /// <param name="boname"></param>
        /// <returns></returns>
        public bool NewBuyerOrganization(string boid, string boname)
        {
            return idal.NewBuyerOrganization(boid, boname);
        }
        /// <summary>
        /// 删除采购组织信息
        /// </summary>
        /// <param name="boid"></param>
        /// <returns></returns>
        public bool DeleteBuyerOrganization(string boid)
        {
            return idal.DeleteBuyerOrganization(boid);
        }
        /// <summary>
        /// 获取所有采购组织-物料组关系
        /// </summary>
        /// <returns></returns>
        public List<PorgMtGropRelation> GetPorgMtgpRelation()
        {
            return idal.GetPorgMtgpRelation();
        }
        /// <summary>
        /// 写入采购组织-物料组信息
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        public bool NewPorgMtgpRelation(List<PorgMtGropRelation> list)
        {
            return idal.NewPorgMtgpRelation(list);
        }
        /// <summary>
        /// 获取物料组-评审员关系
        /// </summary>
        /// <param name="purOrgId"></param>
        /// <returns></returns>
        public Dictionary<string, object> getMtGroupAndMasterRelation(string purOrgId) {
            return idal.getMtGroupAndMasterRelation(purOrgId);
        }
        /// <summary>
        /// 获取所有评估员信息
        /// </summary>
        /// <param name="evalItemName">评估方面，如质量</param>
        /// <returns></returns>
        public DataTable getEvalPersonInfo(string evalItemName) {
            return idal.getEvalPersonInfo(evalItemName);
        }




        #endregion
        #region 维护支付条件
      /// <summary>
      /// 获取所有支付条件编号
      /// </summary>
      /// <returns></returns>
      public List<string> GetAllTradeClauseID()
       {
           return idal.GetAllTradeClauseID();
       }
        /// <summary>
        /// 获取所有支付条件详细信息
        /// </summary>
        /// <returns></returns>
      public DataTable GetAllTradeClauseInformation()
       {
           return idal.GetAllTradeClauseInformation();
       }
        /// <summary>
        /// 新建支付条件
        /// </summary>
        /// <param name="ClauseID"></param>
        /// <param name="ClauseName"></param>
        /// <param name="Description"></param>
        /// <returns></returns>
      public bool NewTradeClauseInformation(string ClauseID, string ClauseName, string Description)
       {
           return idal.NewTradeClauseInformation(ClauseID, ClauseName, Description);
       }
        /// <summary>
        /// 删除所选支付条件
        /// </summary>
        /// <param name="ClauseID"></param>
        /// <returns></returns>
       public bool DeleteTradeClause(string ClauseID)
       {
           return idal.DeleteTradeClause(ClauseID);
       }
       #endregion
      #region 维护评估类
        /// <summary>
        /// 获取所有评估类名称（返回list）
        /// </summary>
        /// <returns></returns>
      public List<string> GetAllEvaluationClass()
       {
           return idal.GetAllEvaluationClass();
       }
        /// <summary>
        /// 获取所有评估类信息（返回datatable）
        /// </summary>
        /// <returns></returns>
      public DataTable GetAllEvaluationClassInformation()
       {
           return idal.GetAllEvaluationClassInformation();
       }
        /// <summary>
        /// 新建评估类
        /// </summary>
        /// <param name="ClassID"></param>
        /// <param name="ClassName"></param>
        /// <param name="Description"></param>
        /// <returns></returns>
      public bool NewEvaluationClass(string ClassID, string ClassName, string Description)
       {
           return idal.NewEvaluationClass(ClassID, ClassName, Description);
       }
        /// <summary>
        /// 删除所选评估类
        /// </summary>
        /// <param name="ClassID"></param>
        /// <returns></returns>
      public bool DeleteEvaluationClass(string ClassID)
       {
           return idal.DeleteEvaluationClass(ClassID);
       }
       #endregion
      #region 维护产品组
        /// <summary>
        /// 获取所有产品组编码
        /// </summary>
        /// <returns></returns>
     public List<string> GetAllDivision()
      {
          return idal.GetAllDivision();
      }
       /// <summary>
       /// 获取所有产品组详细信息
       /// </summary>
       /// <returns></returns>
    public DataTable GetAllDivisionInformation()
     {
         return idal.GetAllDivisionInformation();
     }
        /// <summary>
        /// 新建产品组
        /// </summary>
        /// <param name="DivisionID"></param>
        /// <param name="DivisionName"></param>
        /// <param name="Description"></param>
        /// <returns></returns>
    public bool NewDivision(string DivisionID, string DivisionName, string Description)
     {
         return idal.NewDivision(DivisionID, DivisionName, Description);
     }
        /// <summary>
        /// 删除所选产品组
        /// </summary>
        /// <param name="DivisionID"></param>
        /// <returns></returns>
    public bool DeleteDivision(string DivisionID)
     {
         return idal.DeleteDivision(DivisionID);
     }
      #endregion
      #region 维护税码
        /// <summary>
        /// 获取所有税码
        /// </summary>
        /// <returns></returns>
   public List<string> GetAllTaxCode()
    {
        return idal.GetAllTaxCode();
    }
        /// <summary>
        /// 获取所有税码的详细信息
        /// </summary>
        /// <returns></returns>
   public DataTable GetAllTaxCodeInformation()
    {
        return idal.GetAllTaxCodeInformation();
    }
        /// <summary>
        /// 新建税码信息
        /// </summary>
        /// <param name="TaxID"></param>
        /// <param name="tax"></param>
        /// <param name="Description"></param>
        /// <returns></returns>
   public bool NewTaxCode(string TaxID, double tax, string Description)
    {
        return idal.NewTaxCode(TaxID, tax, Description);
    }
        /// <summary>
        /// 删除所选税码
        /// </summary>
        /// <param name="TaxID"></param>
        /// <returns></returns>
   public bool DeleteTaxCode(string TaxID)
    {
        return idal.DeleteTaxCode(TaxID);
    }
    #endregion
   #region  维护贸易条件
   public List<string> GetAllTradeClause()
   {
       DataTable dt = idal.GetAllTradeClauseInformation();
       List<string> list = new List<string>();
       for (int i = 0; i < dt.Rows.Count; i++)
       {
           list.Add(dt.Rows[i][0].ToString());
       }
       return list;
   }
        /// <summary>
        /// 获取所有支付条件编号
        /// </summary>
        /// <returns></returns>
        public List<string> GetAllTradeClauseByID()
        {
            return idal.GetAllTradeClauseID();
        }
        public DataTable GetTradeClauseByID(string clauseID)
        {
            return idal.GetTradeClauseByID(clauseID);
        }
        #endregion
        #region 常用方法
        /// <summary>
        /// 判断字符串是在某个列表中,存在返回false，不存在返回true
        /// </summary>
        /// <param name="list"></param>
        /// <param name="ID"></param>
        /// <returns></returns>
        public bool IDInTheList(List<string> list, string ID)
      {
            for (int i = 0; i < list.Count; i++)
            {
                if (ID == list[i])
                    return false;
            }
            return true;
        }
        #endregion
      #region 维护国家,地区，城市
        /// <summary>
     /// 获取所有国家(中文）
        /// </summary>
        /// <returns></returns>
    public List<string> GetAllCountryName()
     {
         return idal.GetAllCountryName();
     }
        /// <summary>
    /// //获取所有国家（英文缩写）
        /// </summary>
        /// <returns></returns>
   public List<string> GetAllCountryENG()
    {
        return idal.GetAllCountryENG();
    }
    /// <summary>
    /// 获取所有国家代码
    /// </summary>
    /// <returns></returns>
   public List<string> GetAllCountryCode()
    {
        return idal.GetAllCountryCode();
    }
        /// <summary>
        /// 根据国家名称选取所属城市
        /// </summary>
        /// <param name="countryname"></param>
        /// <returns></returns>
   public List<string> GetAllCity(string countryname)
   {
       return idal.GetAllCity(countryname);
   }
        #endregion
        #region 维护行业标识

        #endregion
        #region 维护凭证类型
        /// <summary>
        /// 获取所有凭证类型
        /// </summary>
        /// <returns></returns>
   public List<string> GetAllProofType()
   {
       return idal.GetAllProofType();
   }
        /// <summary>
        /// 获取凭证描述
        /// </summary>
        /// <param name="typeid"></param>
        /// <returns></returns>
   public string GetProofNameByType(string typeid)
   {
       return idal.GetProofNameByType(typeid);
   }
        #endregion
        #endregion
        /// <summary>
        /// 获取供应商的完整信息
        /// </summary>
        /// <returns></returns>
        public List<Model.MD.SP.SupplierBase> GetAllSuppliers()
        {
            return idal.GetAllSuppliers();
        }
        public List<string> getAllManagerID()
        {
            return idal.getAllManagerID();
        }

        public List<Model.MD.SP.SupplierBase> GetAllSuppliersByIdsLimit(List<string> supplierIds)
        {
            return idal.GetAllSuppliersByIdsLimit(supplierIds);
        }

        public List<String> getALLPurGroupID() {
            return idal.GetAlPurchGroupID();
        }

        public List<String> getALLMtGroupID(String code_Pur_Organic)
        {
            return idal.GetAllMtGroupID(code_Pur_Organic);
        }

        public bool newPorgAndMtGroup(String code_Pur_Organic, String name__Pur_Organic, String code_Mt_Organic, String name_Mt_Organic, String mt_Grop_Manager,int isVisible)
        {
            return idal.NewPorMtGroup(code_Pur_Organic, name__Pur_Organic, code_Mt_Organic, name_Mt_Organic, mt_Grop_Manager, isVisible);
        }

        public DataTable getAllGroupInfo() {
            return idal.GetAllPurMtGroupInfo();
        }

        public bool MDGroupInfo(string delPurGroup_id, string delPurGroup_Name, string delMtGroup_id, string delMtGroup_Name, string PurGroup_id, string PurGroup_Name, string MtGroup_id, string MtGroup_Name)
        {
            return idal.MDPorMtGroup(delPurGroup_id,delPurGroup_Name, delMtGroup_id, delMtGroup_Name,PurGroup_id, PurGroup_Name, MtGroup_id, MtGroup_Name);
        }
    }
}

     
