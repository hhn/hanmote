﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data;


namespace Lib.Bll.MDBll.General
{
 
    public class ComboBoxItem
    {
        /// <summary>
        /// 模糊查询
        /// </summary>
        /// <param name="combobox"></param>
        /// <param name="list"></param>
        public void FuzzyQury(ComboBox combobox, List<string> list)
        {
            string[] data = null;
            try
            {
                data = list.ToArray();
                combobox.Items.AddRange(data);
            }
            catch (Exception ex)
            {
            }
            try
            {
                combobox.TextUpdate += (a, b) =>
                {
                    var input = combobox.Text.ToUpper();
                    combobox.Items.Clear();
                    if (string.IsNullOrWhiteSpace(input)) combobox.Items.AddRange(data);
                    else
                    {
                        var newList = data.Where(x => x.IndexOf(input, StringComparison.CurrentCultureIgnoreCase) != -1).ToArray();
                        if (newList.Count() != 0)
                            combobox.Items.AddRange(newList);
                        else
                            combobox.Items.Add("");
                    }
                    combobox.Select(combobox.Text.Length, 0);
                    combobox.DroppedDown = true;
                    Cursor.Current = Cursors.Default;
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// 模糊查询并显示ID对应的信息
        /// </summary>
        /// <param name="combobox"></param>
        /// <param name="list"></param>
        /// <param name="dt"></param>
        public void FuzzyQury(ComboBox combobox, List<string> list, DataTable dt)
        {
            if (dt.Rows.Count == 0)
                return;
            int rcount = dt.Rows.Count;
            int ccount = dt.Columns.Count;
            string temp = "";
            List<string> templist = new List<string>();
            int i, j;
            //将datatable中的数据列转化成liststring
            for (i = 0; i < rcount; i++)
            {
                for (j = 0; j < ccount; j++)
                {
                    temp = temp + dt.Rows[i][j].ToString() + ",";
                }
                templist.Add(temp);
                temp = "";

            }
            string[] data = null;
            string[] datatemp = null;
            data = list.ToArray();
            datatemp = templist.ToArray();
            combobox.Items.AddRange(data);
            if (data.Length != datatemp.Length)
            {
                MessageBox.Show("wrong");
                return;
            }
            else
            {
                int k = 0;
                for (k = 0; k < data.Length; k++)
                {
                    data[k] = data[k] + "    " + datatemp[k];
                }
            }
            try
            {
                combobox.TextUpdate += (a, b) =>
                {
                    var input = combobox.Text.ToUpper();
                    combobox.Items.Clear();
                    if (string.IsNullOrWhiteSpace(input)) combobox.Items.AddRange(data);
                    else
                    {
                        var newList = data.Where(x => x.IndexOf(input, StringComparison.CurrentCultureIgnoreCase) != -1).ToArray();
                        if (newList.Count() != 0)
                            combobox.Items.AddRange(newList);
                        else
                            combobox.Items.Add("");
                    }
                    combobox.Select(combobox.Text.Length, 0);
                    combobox.DroppedDown = true;
                    Cursor.Current = Cursors.Default;
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
    }
}
