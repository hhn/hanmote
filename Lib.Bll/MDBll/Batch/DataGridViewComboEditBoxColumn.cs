﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.ComponentModel;
using Lib.Common.CommonUtils;

namespace MDBll.Batch
{
   
         public class DataGridViewComboEditBoxColumn : DataGridViewComboBoxColumn 
    { 
        public DataGridViewComboEditBoxColumn() 
        { 
            DataGridViewComboEditBoxCell obj = new DataGridViewComboEditBoxCell(); 
            this.CellTemplate = obj; 
        } 
    }

    //要加入的类 
    public class DataGridViewComboEditBoxCell : DataGridViewComboBoxCell 
    { 
        public override void InitializeEditingControl(int rowIndex, object initialFormattedValue, DataGridViewCellStyle dataGridViewCellStyle) 
        { 
            base.InitializeEditingControl(rowIndex, initialFormattedValue, dataGridViewCellStyle);

            ComboBox comboBox = (ComboBox)base.DataGridView.EditingControl;

            if (comboBox != null) 
            { 
                comboBox.DropDownStyle = ComboBoxStyle.DropDown; 
                comboBox.AutoCompleteMode = AutoCompleteMode.Suggest; 
                comboBox.Validating += new CancelEventHandler(comboBox_Validating); 
            } 
        }

        protected override object GetFormattedValue(object value, int rowIndex, ref DataGridViewCellStyle cellStyle, TypeConverter valueTypeConverter, TypeConverter formattedValueTypeConverter, DataGridViewDataErrorContexts context) 
        { 
            if (value != null) 
            { 
                if (value.ToString().Trim() != string.Empty) 
                { 
                    if (Items.IndexOf(value) == -1) 
                    {
                        try
                        {
                            Items.Add(value);
                        }
                        catch (Exception ex)
                        {

                        }
                        //DataGridViewComboBoxColumn col = (DataGridViewComboBoxColumn)OwningColumn; 
                        //col.Items.Add(value); 
                    } 
                } 
            } 
            return base.GetFormattedValue(value, rowIndex, ref cellStyle, valueTypeConverter, formattedValueTypeConverter, context); 
        }

        void comboBox_Validating(object sender, System.ComponentModel.CancelEventArgs e) 
        { 
            DataGridViewComboBoxEditingControl cbo = (DataGridViewComboBoxEditingControl)sender; 
            if (cbo.Text.Trim() == string.Empty) return;

            DataGridView grid = cbo.EditingControlDataGridView; 
            object value = cbo.Text;

            // Add value to list if not there 
            if (cbo.Items.IndexOf(value) == -1) 
            {
                try
                {
                    DataGridViewComboBoxColumn cboCol = (DataGridViewComboBoxColumn)grid.Columns[grid.CurrentCell.ColumnIndex];
                    // Must add to both the current combobox as well as the template, to avoid duplicate entries 
                    List<string> dataSource = (List<string>)cbo.DataSource;
                    try
                    {
                        dataSource.Add((string)value);
                        grid.CurrentCell.Value = value;
                    }
                    catch (Exception ex)
                    {
                        MessageUtil.ShowError("请先录入特性名称");
                        grid.CurrentCell.Value = "";
                    }
                }
                catch (Exception ex)
                { }
                //cbo.Items.Add(value); 
                //cboCol.Items.Add(value); 
                
            } 
        } 
    

}
    
}
