﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.Model.PurchaseOrderExecution;
using Lib.IDAL.PurchaseOrderExecution;

namespace Lib.Bll.PurchaseOrderExecutionBLL
{
    public class InvoiceToleranceBLL
    {
        InvoiceToleranceIDAL invoiceToleranceDAL = DALFactory.DALFactoryHelper
            .CreateNewInstance<InvoiceToleranceIDAL>("PurchaseOrderExecution.InvoiceToleranceDAL");

        /// <summary>
        /// 更新发票容差设置
        /// </summary>
        /// <param name="toleranceList"></param>
        /// <returns></returns>
        public int updateInvoiceTolerance(List<Invoice_Tolerance> toleranceList) {
            return invoiceToleranceDAL.updateInvoiceTolerance(toleranceList);
        }

        /// <summary>
        /// 查询容差设置
        /// </summary>
        /// <returns></returns>
        public List<Invoice_Tolerance> getInvoiceTolerance() {
            return invoiceToleranceDAL.getInvoiceTolerance();
        }
    }
}
