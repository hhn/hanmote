﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DALFactory;
using Lib.IDAL.StockIDAL;
using Lib.Model.StockModel;

namespace Lib.Bll.StockBLL
{
    public class MoveTypeInfoBLL
    {
        public static readonly MoveTypeInfoIDAL moveTypeIDAL = DALFactoryHelper.CreateNewInstance<MoveTypeInfoIDAL>("MoveTypeInfoDAL");

        /// <summary>
        /// 查找所有移动类型信息
        /// </summary>
        /// <returns></returns>
        public List<MoveTypeExtendModel> findALLMoveTypeInfo()
        {
            return moveTypeIDAL.findALLMoveTypeInfo();
        }

        /// <summary>
        /// 根据移动类型ID类查找移动类型信息
        /// </summary>
        /// <param name="moveTypeId">移动类型ID</param>
        /// <returns></returns>
        public MoveTypeExtendModel findMoveTypeInfoByID(string moveTypeId)
        {
            return moveTypeIDAL.findMoveTypeInfoByID(moveTypeId);
        }

        /// <summary>
        /// 插入新的移动类型
        /// </summary>
        /// <param name="moveTypeModel"></param>
        /// <returns></returns>
        public int insertNewMoveTypeInfo(MoveTypeModel moveTypeModel)
        {

            return moveTypeIDAL.insertNewMoveTypeInfo(moveTypeModel);
        }

        /// <summary>
        /// 根据移动类型编码来修改数据
        /// </summary>
        /// <param name="moveTypeModel">移动类型类</param>
        /// <returns></returns>
        public int updateMoveTypeInfo(MoveTypeModel moveTypeModel)
        {
            return moveTypeIDAL.updateMoveTypeInfo(moveTypeModel);
        }

        /// <summary>
        /// 根据移动类型编码来删除数据
        /// </summary>
        /// <param name="moveTypeId"></param>
        /// <returns></returns>
        public int deleteMoveTypeInfoById(string moveTypeId)
        {
            return moveTypeIDAL.deleteMoveTypeInfoById(moveTypeId);
        }

        /// <summary>
        /// 通过事务/事件名称来关联查询移动类型信息
        /// </summary>
        /// <param name="transName"></param>
        /// <returns></returns>
        public List<MoveTypeExtendModel> findALLMoveTypeInfoByTransName(string transName)
        {
            return moveTypeIDAL.findALLMoveTypeInfoByTransName(transName);
        }
    }
}
