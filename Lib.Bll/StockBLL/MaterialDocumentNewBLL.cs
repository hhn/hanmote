﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Lib.Model.StockModel;
using DALFactory;
using Lib.IDAL.StockIDAL;

namespace Lib.Bll.StockBLL
{
    public class MaterialDocumentNewBLL
    {
        public static readonly MaterialDocumentNewIDAL materialDocumentNewIDAL = DALFactoryHelper.CreateNewInstance<MaterialDocumentNewIDAL>("MaterialDocumentNewDAL");

        public int insertMaterialNewDocument(MaterialDocumentNewModel materialDocumentNewModel)
        {
            return materialDocumentNewIDAL.insertMaterialNewDocument(materialDocumentNewModel);
        }

        /// <summary>
        /// 根据凭证ID来获取凭证头部信息
        /// </summary>
        /// <param name="mDocId"></param>
        /// <returns></returns>
        public MaterialDocumentNewExtendModel getMaterialNewDocument(string mDocId)
        {
            DataTable dt = materialDocumentNewIDAL.getMaterialNewDocument(mDocId);
            MaterialDocumentNewExtendModel mDocModel = null;
            if (dt != null && dt.Rows.Count > 0)
            {
                mDocModel = new MaterialDocumentNewExtendModel();
                DataRow currentRow = dt.Rows[0];
                mDocModel.StockInDocument_ID = currentRow["StockInDocument_ID"].ToString();
                mDocModel.Posting_Date = Convert.ToDateTime(currentRow["Posting_Date"]);
                mDocModel.Document_Date = Convert.ToDateTime(currentRow["Document_Date"]);
                mDocModel.StockManager = currentRow["StockManager"].ToString();
                mDocModel.Document_Type = currentRow["Move_Type"].ToString();
                mDocModel.Reversred = Convert.ToBoolean(currentRow["Reversed"]);
                mDocModel.Order_ID = currentRow["Order_ID"].ToString();
                mDocModel.Delivery_ID = currentRow["Delivery_ID"].ToString();
                mDocModel.ReceiptNote_ID = currentRow["ReceiptNote_ID"].ToString();
                mDocModel.Total_Number = Convert.ToInt32(currentRow["Total_Number"]);
                mDocModel.Total_Value = (float)Convert.ToDouble(currentRow["Total_Value"]);
            }
            return mDocModel;
        }
    }
}
