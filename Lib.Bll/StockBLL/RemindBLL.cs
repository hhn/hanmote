﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.IDAL.StockIDAL;
using DALFactory;
using System.Data;

namespace Lib.Bll.StockBLL
{
    public class RemindBLL
    {
        public static readonly RemindIDAL remindidal = DALFactoryHelper.CreateNewInstance<RemindIDAL>("StockDAL.remindDAL");

        /// <summary>
        /// 将待设置的物料选取出来
        /// </summary>
        /// <returns></returns>
        public DataTable genTable()
        {
            return remindidal.genTable();
        }

         //写入用户设置的提前提醒天数，并对到期提醒时间进行计算
        public void fillPreDay(string factoryID, string stockID, string materialID, string batchID, int days)
        {
            remindidal.fillPreDay(factoryID, stockID, materialID, batchID, days);
        }

         //检查是否有要提醒的物料，若有，写入提醒表中
        public DataTable reminder()
        {
            return remindidal.reminder();
        }

          /// <summary>
        /// 处理到期物料
        /// </summary>
        /// <param name="factoryID"></param>
        /// <param name="stockID"></param>
        /// <param name="materialID"></param>
        /// <param name="batchID"></param>
        public void handle(string factoryID, string stockID, string materialID, string batchID, string prolongdays)
        {
            remindidal.handle(factoryID, stockID, materialID, batchID, prolongdays);
        }

         /// <summary>
        ///选出当前物料及入库日期
        /// </summary>
        /// <returns></returns>
        public DataTable accountManage()
        {
            return remindidal.accountManage();
        }
    }
}
