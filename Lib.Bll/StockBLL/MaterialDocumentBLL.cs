﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DALFactory;
using Lib.IDAL.StockIDAL;
using Lib.Model.StockModel;

namespace Lib.Bll.StockBLL
{
    public class MaterialDocumentBLL
    {
        public static readonly MaterialDocumentIDAL materialDocumentIDAL = DALFactoryHelper.CreateNewInstance<MaterialDocumentIDAL>("MaterialDocumentDAL");

        /// <summary>
        /// 向数据库中插入物料凭证
        /// </summary>
        /// <param name="materialDocument"></param>
        /// <returns></returns>
        public int insertMaterialDocument(MaterialDocumentModel materialDocument)
        {
            return materialDocumentIDAL.insertMaterialDocument(materialDocument);
        }

        public MaterialDocumentExtendModel findMaterialDocumentByDocumentId(string stockDocumentId)
        {
            return materialDocumentIDAL.findMaterialDocumentByDocumentId(stockDocumentId);
        }

        public string findLatestMaterialDocument()
        {
            return materialDocumentIDAL.findLatestMaterialDocument();
        }
    }
}
