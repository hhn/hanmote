﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DALFactory;
using Lib.IDAL.StockIDAL;
using Lib.Model.StockModel;

namespace Lib.Bll.StockBLL
{
    public class StockFactoryInfoBLL
    {
        public static readonly StockFactoryInfoIDAL stockFactoryInfoIDAL = DALFactoryHelper.CreateNewInstance<StockFactoryInfoIDAL>("StockFactoryInfoDAL");

        /// <summary>
        /// 返回所有工厂信息
        /// </summary>
        /// <returns></returns>
        public List<StockFactoryModel> findAllFactoryInfoBy()
        {
            return stockFactoryInfoIDAL.findAllFactoryInfoBy();
        }

        /// <summary>
        /// 根据工厂ID获取该工厂信息
        /// </summary>
        /// <param name="factoryId"></param>
        /// <returns></returns>
        public StockFactoryModel getFactoryInfoBy(string factoryId)
        {
            DataTable dt = stockFactoryInfoIDAL.getFactoryInfoBy(factoryId);
            StockFactoryModel stockFactoryModel = null;
            if (dt != null && dt.Rows.Count > 0)
            {
                stockFactoryModel = new StockFactoryModel();
                DataRow currentRow = dt.Rows[0];
                stockFactoryModel.Factory_ID = currentRow["Factory_ID"].ToString();
                stockFactoryModel.Factory_Name = currentRow["Factory_Name"].ToString();
            }
            return stockFactoryModel;
        }
    }
}
