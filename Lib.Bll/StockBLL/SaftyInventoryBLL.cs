﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using DALFactory;
using Lib.IDAL.StockIDAL;

namespace Lib.Bll.StockBLL
{

    public class SaftyInventoryBLL
    {
        public static readonly SaftyInventoryIDAL siidal = DALFactoryHelper.CreateNewInstance<SaftyInventoryIDAL>("StockDAL.SaftyInventoryDAL");

        /// <summary>
        /// 初始化待输入界面（手动设置安全库存）
        /// </summary>
        /// <returns></returns>
        public DataTable fillInput()
        {
            return siidal.fillInput();
        }

        /// <summary>
        /// 初始化待输入界面（系统计算安全库存）
        /// </summary>
        /// <returns></returns>
        public DataTable fillCompute()
        {
            return siidal.fillCompute();
        }

        /// <summary>
        /// 更新安全库存表中手动输入值
        /// </summary>
        /// <param name="FactoryID"></param>
        /// <param name="MaterialID"></param>
        /// <param name="count"></param>
        public void updateInputCount(string FactoryID, string MaterialID, float count)
        {
            siidal.updateInputCount(FactoryID, MaterialID, count);
        }

        /// <summary>
        /// 更新计算结果
        /// </summary>
        /// <param name="FactoryID"></param>
        /// <param name="MaterialID"></param>
        /// <param name="MAD"></param>
        /// <param name="W"></param>
        /// <param name="R"></param>
        public void updateComputeCount(string FactoryID, string MaterialID, double MAD, double W, double R)
        {
            siidal.updateComputeCount(FactoryID, MaterialID, MAD, W, R);
        }


        /// <summary>
        /// 将需求信息写入需求表中
        /// </summary>
        /// <param name="dt"></param>
        public void inputdemand(DataTable dt)
        {
            siidal.inputdemand(dt);
        }

        /// <summary>
        /// 读取需求量
        /// </summary>
        /// <param name="factoryid"></param>
        /// <param name="materialid"></param>
        /// <returns></returns>
        public DataTable selectDemand(string factoryid, string materialid)
        {
            return siidal.selectDemand(factoryid, materialid);
        }

        /// <summary>
        /// 计算MAD
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public double computeMAD(DataTable dt)
        {
            return siidal.computeMAD(dt);
        }

        /// <summary>
        /// 由订货次数和缺货次数计算安全系数
        /// </summary>
        /// <param name="ordertime"></param>
        /// <param name="shorttime"></param>
        /// <returns></returns>
        public double computeSaftyFactor(int ordertime, int shorttime)
        {
            return siidal.computeSaftyFactor(ordertime, shorttime);
        }

        /// <summary>
        /// 判断需求量信息是否填写
        /// </summary>
        /// <param name="factoryid"></param>
        /// <param name="materialid"></param>
        /// <returns></returns>
        public bool madExist(string factoryid, string materialid)
        {
            return siidal.madExist(factoryid, materialid);
        }

        /// <summary>
        ///查询安全库存
        /// </summary>
        /// <param name="factoryid"></param>
        /// <param name="materialid"></param>
        /// <returns></returns>
        public DataTable showSaftyStock(string factoryid, string materialid)
        {
            return siidal.showSaftyStock(factoryid, materialid);
        }

        /// <summary>
        /// 获取物料ID
        /// </summary>
        /// <returns></returns>
        public DataTable getMaterialID()
        {
            return siidal.getMaterialID();
        }

        /// <summary>
        /// 获取工厂ID
        /// </summary>
        /// <returns></returns>
        public DataTable getFactoryID()
        {
            return siidal.getFactoryID();

        }
    }
}