﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.Model.StockModel;
using DALFactory;
using Lib.IDAL.StockIDAL;

namespace Lib.Bll.StockBLL
{
    public class StockTransInfoBLL
    {
        public static readonly StockTransInfoIDAL stockTransInfoIDAL = DALFactoryHelper.CreateNewInstance<StockTransInfoIDAL>("StockDAL.StockTransInfoDAL");
        /// <summary>
        /// 查找所有事务/事件信息
        /// </summary>
        /// <returns></returns>
        public List<StockTransExtendModel> findAllStockTransInfo()
        {
            return stockTransInfoIDAL.findAllStockTransInfo();
        }

        /// <summary>
        /// 根据id查找事务/事件信息
        /// </summary>
        /// <param name="stockTransId">事务id</param>
        /// <returns></returns>
        public StockTransExtendModel findStockTransInfoByID(string stockTransId)
        {
            return stockTransInfoIDAL.findStockTransInfoByID(stockTransId);
        }

        /// <summary>
        /// 插入新的事务/事件信息
        /// </summary>
        /// <param name="stockTransModel">事务类</param>
        /// <returns></returns>
        public int insertNewStockTransInfo(StockTransModel stockTransModel)
        {
            return stockTransInfoIDAL.insertNewStockTransInfo(stockTransModel);
        }

        /// <summary>
        /// 修改事务信息
        /// </summary>
        /// <param name="stockTransModel">事务类</param>
        /// <returns></returns>
        public int updateStockTransInfo(StockTransModel stockTransModel)
        {
            return stockTransInfoIDAL.updateStockTransInfo(stockTransModel);
        }

        /// <summary>
        /// 根据id删除事务信息
        /// </summary>
        /// <param name="stockTransModel">事务id</param>
        /// <returns></returns>
        public int deleteStockTransInfo(string stockTransId)
        {
            return stockTransInfoIDAL.deleteStockTransInfo(stockTransId);
        }
    }
}
