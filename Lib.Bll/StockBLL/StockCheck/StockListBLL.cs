﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.IDAL.StockIDAL;
using DALFactory;
using System.Data;
using System.Data.SqlClient;

namespace Lib.Bll.StockBLL
{
    public class StockListBLL
    {
        public static readonly StockListIDAL stockListIDAL = DALFactoryHelper.CreateNewInstance<StockListIDAL>("StockDAL.StockListDAL");

        /// <summary>
        ///  查询满足条件的物料
        /// </summary>
        /// <param name="documentID"></param>
        /// <returns></returns>
        public DataTable stocklist(DataTable dt)
        {
            return stockListIDAL.stocklist(dt);
        }

        /// <summary>
        /// 根据凭证ID读出工厂、库存地ID
        /// </summary>
        /// <param name="documentID"></param>
        /// <returns></returns>
        public DataTable changestyle(DataTable info, bool bl)
        {
            return stockListIDAL.changestyle(info,bl);
        }

    }
}
