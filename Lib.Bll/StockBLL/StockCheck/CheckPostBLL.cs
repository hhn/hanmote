﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DALFactory;
using Lib.IDAL.StockIDAL;
using System.Data;
using System.Data.SqlClient;

namespace Lib.Bll.StockBLL
{
    public class CheckPostBLL
    {
        public static readonly CheckPostIDAL checkpostidal = DALFactoryHelper.CreateNewInstance<CheckPostIDAL>("CheckPostDAL");

        /// <summary>
        /// 根据凭证编号选择满足条件的datatable
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public DataTable selectInfo(string documentid, string accountyear)
        {
            return checkpostidal.selectInfo(documentid, accountyear);
        }

          /// <summary>
        /// 根据数据表（selected,documentid,materialid,batchid,factoryid,stockid,reason）
        /// 进行过账
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        public bool checkPost(DataTable info)
        {
            return checkpostidal.checkPost(info);
        }

        /// <summary>
        /// 根据凭证号读取工厂库存地id
        /// </summary>
        /// <param name="documentid"></param>
        /// <returns></returns>
        public DataTable getfactory(string documentid)
        {
            return checkpostidal.getfactory(documentid);
        }
    }
}
