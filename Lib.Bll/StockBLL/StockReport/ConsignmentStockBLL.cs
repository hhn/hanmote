﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DALFactory;
using Lib.IDAL.StockIDAL;

namespace Lib.Bll.StockBLL
{
    public class ConsignmentStockBLL
    {
        private static readonly ConsignmentStockIDAL stockstock = DALFactoryHelper.CreateNewInstance<ConsignmentStockIDAL>("StockDAL.ConsignmentStockDAL");
        public DataTable showStock(DataTable dt0)
        {
            return stockstock.showStock(dt0);
        }

    }
}
