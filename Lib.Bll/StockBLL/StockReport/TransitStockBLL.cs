﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Lib.IDAL.StockIDAL;
using DALFactory;

namespace Lib.Bll.StockBLL
{
    public class TransitStockBLL
    {
        private static readonly TransitStockIDAL transitstock = DALFactoryHelper.CreateNewInstance<TransitStockIDAL>("StockDAL.TransitStockDAL");

        public DataTable showStock(DataTable dt0)
        {
            return transitstock.showStock(dt0);
        }
    }
}
