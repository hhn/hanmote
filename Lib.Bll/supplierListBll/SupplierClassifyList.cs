﻿using DALFactory;
using Lib.IDAL.SupplierManagementIDAL.supplierListIDAL;
using Lib.Model.supplierListMode;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Lib.Bll.supplierListBll
{
    public class SupplierClassifyList
    {
        private SupplierListIDAL supplierListIDAL = DALFactoryHelper.CreateNewInstance<SupplierListIDAL>("SupplierManagementDAL.supplierListDAL.SupplierListDAL");

        /// <summary>
        /// 获取采购组织信息
        /// </summary>
        /// <returns></returns>
        public DataTable getPurOrg()
        {
           return  supplierListIDAL.getPurOrg();


        }
        /// <summary>
        /// 获取供应商的信息
        /// </summary>
        /// <returns></returns>
        public DataTable getSupplierInfo(queryCondition queryCondition)
        {
            return supplierListIDAL.getSupplierInfo(queryCondition);

        }

        public int getListCounts(queryCondition queryCondition)
        {
            return supplierListIDAL.getListCounts(queryCondition);
        }

        /// <summary>
        /// 获取物料组名称
        /// </summary>
        /// <param name="purOrgName">采购组织名称</param>
        /// <returns></returns>
        public DataTable getMtGroup(string purOrgName)
        {
            return supplierListIDAL.getMtGroup(purOrgName);
        }

        public DataTable getExistedList(string purOrg)
        {
            return supplierListIDAL.getExistedList(purOrg);
        }

        public DataTable getSupplierLevelInfo(queryCondition queryCondition)
        {
            return supplierListIDAL.getSupplierLevelInfo(queryCondition);
        }

        /// <summary>
        /// 获取物料名称
        /// </summary>
        /// <param name="purOrgId">采购组织名称</param>
        /// <param name="MtGroupId">物料组名称</param>
        /// <returns></returns>
        public DataTable getMtName(string purOrgId, string MtGroupId)
        {
            return supplierListIDAL.getMtName(purOrgId, MtGroupId);
        }
        /// <summary>
        /// 保存供应商信息
        /// </summary>
        /// <param name="dgv_SupplierInfo"></param>
        /// <returns></returns>
        public bool saveSupplierListInfo(DataGridView dgv_SupplierInfo, SupplierListModel supplierListModel)
        {

            //插入清单信息
            bool flag = supplierListIDAL.createSupplierList(supplierListModel);
            if (flag) {
                for (int i = 0; i < dgv_SupplierInfo.Rows.Count; i++)
                {
                    supplierListModel.SupplierLevel = dgv_SupplierInfo.Rows[i].Cells["supplierLevel"].Value.ToString();
                    supplierListModel.SupplierID = dgv_SupplierInfo.Rows[i].Cells["supplierId"].Value.ToString();
                    supplierListModel.SupplierName = dgv_SupplierInfo.Rows[i].Cells["supplierName"].Value.ToString();
                    supplierListModel.SupplierType = dgv_SupplierInfo.Rows[i].Cells["supplierType"].Value.ToString();
                    flag &= supplierListIDAL.saveSupplierListInfo(supplierListModel);
                }

            }
            if (!flag) {
                supplierListIDAL.deleteSupplierList(supplierListModel);


            }
           
            return flag;
        }

        public bool setLevel(string supplier, string level, string ID)
        {
            return supplierListIDAL.setLevel(supplier, level,ID);
        }

        public int getSupLevelCount(queryCondition queryCondition)
        {
            return supplierListIDAL.getSupLevelCount(queryCondition);
        }

        /// <summary>
        /// 获取物料记录数
        /// </summary>
        /// <param name="queryCondition"></param>
        /// <returns></returns>
        public int getMtInfoCount(string queryCondition)
        {
          return  supplierListIDAL.getMtInfoCount(queryCondition);
        }


        /// <summary>
        /// 获取物料的供应源信息
        /// </summary>
        /// <param name="queryCondition"></param>
        /// <returns></returns>

        public DataTable getSupplySupplierInfo(queryCondition queryCondition)
        {
            return supplierListIDAL.getSupplySupplierInfo(queryCondition);
        }
        /// <summary>
        /// 获取物料的供应源记录数
        /// </summary>
        /// <param name="queryCondition"></param>
        /// <returns></returns>
        public int getSupplierInfoCount(queryCondition queryCondition)
        {
            return supplierListIDAL.getSupplierInfoCount(queryCondition);

        }
        /// <summary>
        /// 记录设置等级的操作信息
        /// </summary>
        /// <param name="modifyMess"></param>
        /// <param name="reason"></param>
        /// <param name="supplierId"></param>
        /// <returns></returns>
        public bool saveModifyInfo( string supplierId,string modifyMess, string reason)
        {
            return supplierListIDAL.saveModifyInfo(supplierId,modifyMess, reason);

        }

        /// <summary>
        /// 获取已经存在的供应商清单信息
        /// </summary>
        /// <param name="ExistedListInfo"></param>
        /// <returns></returns>
        public DataTable getExistedListInfo(string ExistedListInfo)
        {
            return supplierListIDAL.getExistedListInfo(ExistedListInfo);
        }

    }
}
