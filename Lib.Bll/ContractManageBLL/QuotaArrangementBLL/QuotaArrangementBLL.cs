﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.Model.ContractManage.QuotaArrangement;
using Lib.IDAL.ContractManage.QuotaArrangement;
using Lib.SqlServerDAL.ContractManageDAL.QuotaArrangement;
using DALFactory;

namespace Lib.Bll.ContractManageBLL.QuotaArrangementBLL
{
    public class QuotaArrangementBLL
    {
        QuotaArrangementIDAL quotaArrangementDAL = 
            DALFactoryHelper.CreateNewInstance<QuotaArrangementIDAL>(
            "ContractManageDAL.QuotaArrangement.QuotaArrangementDAL");
        QuotaArrangementItemIDAL quotaArrangementItemDAL = 
            DALFactoryHelper.CreateNewInstance<QuotaArrangementItemIDAL>(
            "ContractManageDAL.QuotaArrangement.QuotaArrangementItemDAL");

        /// <summary>
        /// 查询某一物料ID和工厂ID下的所有配额协议
        /// </summary>
        /// <param name="materialID"></param>
        /// <param name="factoryID"></param>
        /// <returns></returns>
        public List<Quota_Arrangement> getQuotaArrangementList(string materialID,
            string factoryID) {

            return quotaArrangementDAL.getQuotaArrangement(materialID, factoryID);
        }

        /// <summary>
        /// 删除配额协议
        /// </summary>
        /// <param name="quotaArrangementID"></param>
        /// <returns></returns>
        public int deleteQuotaArrangement(string quotaArrangementID) {
            return quotaArrangementDAL.deleteQuotaArrangement(quotaArrangementID);
        }

        /// <summary>
        /// 获取指定配额协议下的所有配额协议项
        /// </summary>
        /// <param name="quotaArrangementID"></param>
        /// <returns></returns>
        public List<Quota_Arrangement_Item> getQuotaArrangementItemList(
            string quotaArrangementID) {
            return quotaArrangementItemDAL.getQuotaArrangementItemList(
                quotaArrangementID);
        }

        /// <summary>
        /// 添加配额协议
        /// </summary>
        /// <param name="quotaArrangement"></param>
        /// <returns></returns>
        public int addQuotaArrangement(Quota_Arrangement quotaArrangement) {

            return quotaArrangementDAL.addQuotaArrangement(quotaArrangement);
        }

        /// <summary>
        /// 更新配额协议
        /// </summary>
        /// <param name="quotaArrangement"></param>
        /// <returns></returns>
        public int updateQuotaArrangement(Quota_Arrangement quotaArrangement) {
            return quotaArrangementDAL.updateQuotaArrangement(quotaArrangement);
        }

        /// <summary>
        /// 添加或刷新配额协议项
        /// </summary>
        /// <param name="quotaArrangementID"></param>
        /// <param name="itemList"></param>
        /// <returns></returns>
        public int addQuotaArrangementItemList(string quotaArrangementID,
            List<Quota_Arrangement_Item> itemList) {

            return quotaArrangementItemDAL.addQuotaArrangementItemList(
                quotaArrangementID, itemList);
        }
    }
}
