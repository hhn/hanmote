﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Bll.ContractManageBLL
{
    /// <summary>
    /// 采购人员类
    /// </summary>
    public class Buyer
    {
        private string buyerID;
        /// <summary>
        /// 采购人员ID
        /// </summary>
        public string BuyerID
        {
            get { return buyerID; }
            set { buyerID = value; }
        }

        private string buyerName;
        /// <summary>
        /// 采购人员姓名
        /// </summary>
        public string BuyerName
        {
            get { return buyerName; }
            set { buyerName = value; }
        }

        private string department;
        /// <summary>
        /// 所属部门
        /// </summary>
        public string Department
        {
            get { return department; }
            set { department = value; }
        }

        private string materialID;
        /// <summary>
        /// 物料ID
        /// </summary>
        public string MaterialID
        {
            get { return materialID; }
            set { materialID = value; }
        }
    }
}
