﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.Model.ContractManage.OutlineAgreement;
using Lib.IDAL.ContractManage.OutlineAgreement;
using Lib.SqlServerDAL.ContractManageDAL.OutlineAgreementDAL;
using DALFactory;

namespace Lib.Bll.ContractManageBLL.OutlineAgreementBLL
{
    public class SchedulingAgreementBLL
    {
        SchedulingAgreementIDAL schedulingAgreementDAL = 
            DALFactoryHelper.CreateNewInstance<SchedulingAgreementIDAL>(
            "ContractManageDAL.OutlineAgreementDAL.SchedulingAgreementDAL");
        SchedulingAgreementItemIDAL schedulingAgreementItemDAL = 
            DALFactoryHelper.CreateNewInstance<SchedulingAgreementItemIDAL>(
            "ContractManageDAL.OutlineAgreementDAL.SchedulingAgreementItemDAL");
        DeliveryItemIDAL deliveryItemDAL = 
            DALFactoryHelper.CreateNewInstance<DeliveryItemIDAL>(
            "ContractManageDAL.OutlineAgreementDAL.DeliveryItemDAL");

        /// <summary>
        /// 根据合同ID和Version获取合同的信息
        /// </summary>
        /// <param name="schedulingAgreementID"></param>
        /// <param name="schedulingAgreementVersion"></param>
        /// <returns></returns>
        public Scheduling_Agreement getSchedulingAgreementByID(string schedulingAgreementID)
        {

            return schedulingAgreementDAL.getSchedulingAgreementByID(schedulingAgreementID);
        }

        /// <summary>
        /// 根据指定的条件获取合同的信息
        /// </summary>
        /// <param name="schedulingAgreementID"></param>
        /// <param name="schedulingAgreementVersion"></param>
        /// <returns></returns>
        public List<Scheduling_Agreement> getSchedulingAgreementByCondition(Dictionary<string, object> conditions,
            DateTime beginTime, DateTime endTime)
        {

            return schedulingAgreementDAL.getSchedulingAgreementByCondition(conditions, beginTime, endTime);
        }

        /// <summary>
        /// 根据合同ID和Version获取所有的框架协议项
        /// </summary>
        /// <param name="schedulingAgreementID"></param>
        /// <param name="schedulingAgreementVersion"></param>
        /// <returns></returns>
        public List<Scheduling_Agreement_Item> getItemsBySchedulingAgreementID(string schedulingAgreementID)
        {

            return schedulingAgreementItemDAL.getSchedulingAgreementItemListByID(schedulingAgreementID);
        }

        /// <summary>
        /// 添加新的协议合同
        /// </summary>
        /// <param name="schedulingAgreement"></param>
        /// <param name="itemList"></param>
        /// <returns></returns>
        public int addNewSchedulingAgreement(Scheduling_Agreement schedulingAgreement,
            List<Scheduling_Agreement_Item> itemList)
        {

            return schedulingAgreementDAL.addSchedulingAgreement(schedulingAgreement, itemList);
        }

        /// <summary>
        /// 批量删除协议合同
        /// </summary>
        /// <param name="agreementIDList"></param>
        /// <returns></returns>
        public int deleteSchedulingAgreementList(List<string> agreementIDList)
        {
            int result = 0;
            for (int i = 0; i < agreementIDList.Count; i++)
            {
                string agreementID = agreementIDList[i];

                result += schedulingAgreementDAL.deleteSchedulingAgreementByID(agreementID);
            }

            return result;
        }

        /// <summary>
        /// 更新合同抬头数据
        /// </summary>
        /// <param name="schedulingAgreementID"></param>
        /// <param name="schedulingAgreement"></param>
        /// <returns></returns>
        public int updateSchedulingAgreement(string schedulingAgreementID,
            Scheduling_Agreement schedulingAgreement)
        {

            return schedulingAgreementDAL.updateSchedulingAgreement(schedulingAgreementID,
                schedulingAgreement);
        }

        /// <summary>
        /// 更新合同全部信息(抬头数据+框架协议项)
        /// </summary>
        /// <param name="agreement"></param>
        /// <param name="itemList"></param>
        /// <returns></returns>
        public int updateSchedulingAgreement(Scheduling_Agreement agreement, List<Scheduling_Agreement_Item> itemList)
        {

            return schedulingAgreementDAL.updateSchedulingAgreement(agreement, itemList);
        }

        /// <summary>
        /// 根据具体的框架协议项获取交货计划
        /// </summary>
        /// <param name="schedulingAgreementID"></param>
        /// <param name="materialItemNum"></param>
        /// <returns></returns>
        public List<Delivery_Item> getDeliveryItemList(string schedulingAgreementID,
            int materialItemNum) {

            return deliveryItemDAL.getDeliveryItemList(schedulingAgreementID,
                materialItemNum);
        }

        /// <summary>
        /// 更新所有的交货计划行
        /// </summary>
        /// <param name="schedulingAgreementID"></param>
        /// <param name="materialItemNum"></param>
        /// <param name="deliveryItemList"></param>
        /// <returns></returns>
        public int updateDeliveryItemList(string schedulingAgreementID, int materialItemNum,
            List<Delivery_Item> deliveryItemList) {

            return deliveryItemDAL.updateDeliveryItemList(schedulingAgreementID, materialItemNum,
                deliveryItemList);
        }
    }
}
