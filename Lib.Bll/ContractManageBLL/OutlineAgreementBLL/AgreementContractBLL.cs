﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.Model.ContractManage.OutlineAgreement;
using Lib.SqlServerDAL.ContractManageDAL.OutlineAgreementDAL;
using Lib.IDAL.ContractManage.OutlineAgreement;
using DALFactory;

namespace Lib.Bll.ContractManageBLL.OutlineAgreementBLL
{
    public class AgreementContractBLL
    {
        AgreementContractIDAL agreementContractDAL = DALFactoryHelper
            .CreateNewInstance<AgreementContractIDAL>(
            "ContractManageDAL.OutlineAgreementDAL.AgreementContractDAL");
        AgreementContractItemIDAL agreementContractItemDAL = DALFactoryHelper
            .CreateNewInstance<AgreementContractItemIDAL>(
            "ContractManageDAL.OutlineAgreementDAL.AgreementContractItemDAL");

        /// <summary>
        /// 根据合同ID和Version获取合同的信息
        /// </summary>
        /// <param name="agreementContractID"></param>
        /// <param name="agreementContractVersion"></param>
        /// <returns></returns>
        public Agreement_Contract getAgreementContractByID(string agreementContractID,
            string agreementContractVersion) {

                return agreementContractDAL.getAgreementContractByID(agreementContractID, agreementContractVersion);
        }

        /// <summary>
        /// 根据指定的条件获取合同的信息
        /// </summary>
        /// <param name="agreementContractID"></param>
        /// <param name="agreementContractVersion"></param>
        /// <returns></returns>
        public List<Agreement_Contract> getAgreementContractByCondition(Dictionary<string, object> conditions,
            DateTime beginTime, DateTime endTime)
        {

            return agreementContractDAL.getAgreementContractByCondition(conditions, beginTime, endTime);
        }

        /// <summary>
        /// 根据合同ID和Version获取所有的框架协议项
        /// </summary>
        /// <param name="agreementContractID"></param>
        /// <param name="agreementContractVersion"></param>
        /// <returns></returns>
        public List<Agreement_Contract_Item> getItemsByAgreementContractID(string agreementContractID,
            string agreementContractVersion) {

            return agreementContractItemDAL.getAgreementContractItemListByID(agreementContractID,
                agreementContractVersion);
        }

        /// <summary>
        /// 添加新的协议合同
        /// </summary>
        /// <param name="agreementContract"></param>
        /// <param name="itemList"></param>
        /// <returns></returns>
        public int addNewAgreementContract(Agreement_Contract agreementContract,
            List<Agreement_Contract_Item> itemList) {

            return agreementContractDAL.addAgreementContract(agreementContract, itemList);
        }

        /// <summary>
        /// 批量删除协议合同
        /// </summary>
        /// <param name="contractIDList"></param>
        /// <param name="contractVersionList"></param>
        /// <returns></returns>
        public int deleteAgreementContractList(List<string> contractIDList, List<string> contractVersionList) {
            int result = 0;
            for (int i = 0; i < contractIDList.Count; i++) {
                string contractID = contractIDList[i];
                string contractVersion = contractVersionList[i];

                result += agreementContractDAL.deleteAgreementContractByID(contractID, contractVersion);
            }

            return result;
        }

        /// <summary>
        /// 更新合同抬头数据
        /// </summary>
        /// <param name="agreementContractID"></param>
        /// <param name="agreementContractVersion"></param>
        /// <param name="agreementContract"></param>
        /// <returns></returns>
        public int updateAgreementContract(string agreementContractID, string agreementContractVersion,
            Agreement_Contract agreementContract) {

                return agreementContractDAL.updateAgreementContract(agreementContractID, agreementContractVersion,
                    agreementContract);
        }

        /// <summary>
        /// 更新合同全部信息(抬头数据+框架协议项)
        /// </summary>
        /// <param name="contract"></param>
        /// <param name="itemList"></param>
        /// <returns></returns>
        public int updateAgreementContract(Agreement_Contract contract, List<Agreement_Contract_Item> itemList) {

            return agreementContractDAL.updateAgreementContract(contract, itemList);
        }

        /// <summary>
        /// 变更合同信息
        /// </summary>
        /// <param name="oldAgreementContract"></param>
        /// <param name="newAgreementContract"></param>
        /// <param name="itemList"></param>
        /// <returns></returns>
        public int changeAgreementContract(Agreement_Contract oldAgreementContract, Agreement_Contract newAgreementContract,
            List<Agreement_Contract_Item> itemList) {

                return agreementContractDAL.changeAgreementContract(oldAgreementContract, newAgreementContract,
                    itemList);
        }
    }
}
