﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.Model.SourcingManage.ProcurementPlan;
using Lib.IDAL.SourcingManage.ProcurementPlan;
using Lib.SqlServerDAL.SourcingManage.ProcurementPlan;
using DALFactory;
using System.Data;
using Lib.Common.MMCException.Bll;
using Lib.Common.MMCException.IDAL;

namespace Lib.Bll.SourcingManage.ProcurementPlan
{
    public class Summary_DemandBLL
    {
        Summary_DemandIDAL idal =
            DALFactoryHelper.CreateNewInstance<Summary_DemandIDAL>(
            "SourcingManage.ProcurementPlan.Summary_DemandDAL");
        //Summary_DemandDAL demandDAL = new Summary_DemandDAL();

        /// <summary>
        /// 根据需求计划编号查找需求计划
        /// </summary>
        /// <param name="demandId"></param>
        /// <returns></returns>
        public Summary_Demand findSummaryDemandByDemandID(string demandId)
        {
            return idal.findSummaryDemandByDemandID(demandId);
        }

        /// <summary>
        /// 获取所有采购需求数据
        /// </summary>
        /// <returns></returns>
        public DataTable GetSummaryDemandViewData()
        {
            try
            {
                //获取采购需求计划数据
                return idal.GetAllSummaryDemand();
            }
            catch (DBException ex)
            {
                throw new BllException("获取采购需求计划失败！", ex);
            }
            
        }

        /// <summary>
        /// 添加新的采购需求
        /// </summary>
        /// <param name="summary_Demand"></param>
        /// <returns></returns>
        public int addNewStandard(Summary_Demand summary_Demand)
        {
            return idal.addNewStandard(summary_Demand);
        }

        /// <summary>
        /// 修改标准需求计划
        /// </summary>
        /// <param name="summary_Demand"></param>
        /// <returns></returns>
        public int editNewStandard(Summary_Demand summary_Demand)
        {
            return idal.editNewStandard(summary_Demand);
        }

        /// <summary>
        /// 查询是否已存在相同的需求单号
        /// </summary>
        /// <param name="demand_ID"></param>
        /// <returns></returns>
        public Boolean SameStandardByDemand_ID(string demand_ID)
        {
            return idal.SameStandardByDemand_ID(demand_ID);
        }

        /// <summary>
        /// 查询已存在的需求单号
        /// </summary>
        /// <param name="demand_ID"></param>
        /// <returns></returns>
        public DataTable FindStandardDemand_ID(String itemName)
        {
            return idal.FindStandardDemand_ID(itemName);
        }

        /// <summary>
        /// 查询最新的需求单号
        /// </summary>
        /// <param name="demand_ID"></param>
        /// <returns></returns>
        public DataTable FindStandardDemand_ID()
        {
            return idal.FindStandardDemand_ID();
        }

        /// <summary>
        /// 查询需求单号
        /// </summary>
        /// <param name="demand_ID"></param>
        /// <returns></returns>
        public DataTable StandardByWLBH(String item1, String item2, String item3)
        {
            return idal.StandardByWLBH(item1, item2, item3);
        }

        /// <summary>
        /// 更改采购单号状态
        /// </summary>
        /// <param name="demand_ID"></param>
        /// <returns></returns>
        public int changeState(String state, String demand_ID)
        {
            return idal.changeState(state, demand_ID);
        }

        /// <summary>
        /// 更改采购单号状态
        /// </summary>
        /// <param name="demand_ID"></param>
        /// <returns></returns>
        public int changePrice(String price, String demand_ID)
        {
            return idal.changePrice(price, demand_ID);
        }

        /// <summary>
        /// 查询物料编号对应的物料信息
        /// </summary>
        /// <param name="demand_ID"></param>
        /// <returns></returns>
        public DataTable FindMaterial(String itemName, String table)
        {
            return idal.FindMaterial(itemName, table);
        }

        /// <summary>
        /// 查询已存在的物料信息
        /// </summary>
        /// <param name="demand_ID"></param>
        /// <returns></returns>
        public DataTable FindMaterialItems(String itemName, String table)
        {
            return idal.FindMaterialItems(itemName, table);
        }
        public List<String> FindAddItems(String id, String idName, String itemName, String table)
        {
            return idal.FindAddItems(id, idName, itemName, table);
        }

        /// <summary>
        /// 根据需要加载items至combobox
        /// </summary>
        /// <param name="demand_ID"></param>
        /// <returns></returns>
        public DataTable AddItemsToCombobox(String itemName, String table)
        {
            return idal.AddItemsToCombobox(itemName, table);
        }

        /// <summary>
        /// 新建条件类型
        /// </summary>
        /// <param name="summary_Demand"></param>
        /// <returns></returns>
        public int addNewPrice_Condition(String table,String id, String Condition, String Price_Level, String Rounding_Rule, Boolean Sign, float Value_Rate)
        {
            return idal.addNewPrice_Condition(table,id,Condition,Price_Level,Rounding_Rule,Sign,Value_Rate);
        }

        /// <summary>
        /// 根据查询条件查询采购单信息
        /// </summary>
        /// <param name="demand_ID"></param>
        /// <returns></returns>
        public List<Summary_Demand> findDemand(Dictionary<string, string> conditions)
        {
            return idal.findDemand(conditions);
        }

        /// <summary>
        /// 更新需求计划
        /// </summary>
        /// <param name="demand"></param>
        /// <returns></returns>
        public int UpdateSummaryDemand(Summary_Demand demand)
        {
            return idal.UpdateSummaryDemand(demand);
        }

        /// <summary>
        /// 根据需求计划单号删除需求计划
        /// </summary>
        /// <returns></returns>
        public int deleteSummaryDemandById(string demandId)
        {
            return idal.deleteSummaryDemandById(demandId);
        }

        /// <summary>
        /// 审核需求计划
        /// </summary>
        /// <param name="demand"></param>
        /// <returns></returns>
        public int reviewDemandState(Summary_Demand demand)
        {
            return idal.reviewDemandState(demand);
        }

        /// <summary>
        /// 获取审核通过的需求计划
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public List<string> findReviewedDemands(string item)
        {
            return idal.findReviewedDemands(item);
        }

        /// <summary>
        /// 返回指定时间内的需求计划信息
        /// </summary>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <returns></returns>
        public List<Summary_Demand> findDemandsInLimitTime(string startTime, string endTime)
        {
            return idal.findDemandsInLimitTime(startTime, endTime);
        }
    }
}
