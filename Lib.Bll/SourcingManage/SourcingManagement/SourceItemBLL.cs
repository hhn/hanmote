﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.Model.SourcingManage;
using Lib.Model.SourcingManage.SourcingManagement;
using Lib.IDAL.SourcingManage.SourcingManagement;
using Lib.SqlServerDAL.SourcingManage.SourcingManagement;
using DALFactory;
using System.Data;

namespace Lib.Bll.SourcingManage.SourcingManagement
{
    public class SourceItemBLL
    {
        SourceItemIDAL idal =
            DALFactoryHelper.CreateNewInstance<SourceItemIDAL>(
            "SourcingManage.SourcingManagement.SourceItemDAL");

        public SourceItem ExistsBySSId(string sourceId, string supplierId)
        {
            return idal.ExistsBySSId(sourceId, supplierId);
        }

        public int addSourceItem(SourceItem sourceItem)
        {
            return idal.addSourceItem(sourceItem);
        }

        /// <summary>
        /// 获取所有未签订合同的寻源子项id
        /// author:liaoyu
        /// </summary>
        /// <returns></returns>
        public List<string> getAllValidItemID()
        {
            return idal.getAllValidItemID();
        }

        /// <summary>
        /// 根据寻源子项id获得对应的寻源子项数据，主要得到合同签订主体的数据
        /// author:liaoyu
        /// </summary>
        /// <param name="itemID"></param>
        /// <returns></returns>
        public SourceItem getSourceItemByID(string itemID)
        {
            return idal.getSourceItemByID(itemID);
        }

        /// <summary>
        /// 获取所有已签订合同的寻源子项id
        /// author:liaoyu
        /// </summary>
        /// <returns></returns>
        public List<string> getAllInvalidItemID()
        {
            return idal.getAllInvalidItemID();
        }
    }
}
