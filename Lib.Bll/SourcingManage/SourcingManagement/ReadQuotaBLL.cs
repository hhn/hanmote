﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using DALFactory;
using Lib.IDAL.SourcingManage.SourcingManagement;
using Lib.Model.SourcingManage.SourcingManagement;

namespace Lib.Bll.SourcingManage.SourcingManagement
{
    public class ReadQuotaBLL
    {
        ReadQuotaIDAL idal =
           DALFactoryHelper.CreateNewInstance<ReadQuotaIDAL>(
           "SourcingManage.SourcingManagement.ReadQuotaDAL");

        public List<Quota_Arrangement_Item> getInfoByMaterialId(string mid)
        {
            return idal.getInfoByMaterialId(mid);
        }
    }
}
