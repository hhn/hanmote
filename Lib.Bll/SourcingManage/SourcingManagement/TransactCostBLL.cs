﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.IDAL.SourcingManage.SourcingManagement;
using DALFactory;
using Lib.Model.SourcingManage.SourcingManagement;

namespace Lib.Bll.SourcingManage.SourcingManagement
{
    public class TransactCostBLL
    {
        TransactCostIDAL idal =
            DALFactoryHelper.CreateNewInstance<TransactCostIDAL>(
            "SourcingManage.SourcingManagement.TransactCostDAL");

        public int addTransactCost(TransactCost tc)
        {
            return idal.addTransactCost(tc);
        }

        public List<TransactCost> getTransCostsBySId(string bid,string sid)
        {
            return idal.getTransCostsBySId(bid,sid);
        }
    }
}
