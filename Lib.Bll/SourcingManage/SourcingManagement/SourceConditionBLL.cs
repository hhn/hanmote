﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DALFactory;
using Lib.IDAL.SourcingManage.SourcingManagement;
using Lib.Model.SourcingManage.SourcingManagement;

namespace Lib.Bll.SourcingManage.SourcingManagement
{
    public class SourceConditionBLL
    {
        SourceConditionIDAL idal =
           DALFactoryHelper.CreateNewInstance<SourceConditionIDAL>(
           "SourcingManage.SourcingManagement.SourceConditionDAL");

        public List<SourceCondition> getSourceConByBidSpMaId(string sourceId, string supplierId, string materialId)
        {
            return idal.getSourceConByBidSpMaId(sourceId, supplierId, materialId);
        }
    }
}
