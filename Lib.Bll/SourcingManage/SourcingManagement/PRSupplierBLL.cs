﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.IDAL.SourcingManage.SourcingManagement;
using DALFactory;
using Lib.Model.SourcingManage.SourcingManagement;

namespace Lib.Bll.SourcingManage.SourcingManagement
{
    public class PRSupplierBLL
    {
        PRSupplierIDAL idal =
           DALFactoryHelper.CreateNewInstance<PRSupplierIDAL>(
           "SourcingManage.SourcingManagement.PRSupplierDAL");

        public int addPRSupplier(PRSupplier prs)
        {
            return idal.addPRSupplier(prs);
        }
    }
}
