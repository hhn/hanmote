﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.IDAL.SourcingManage.SourcingManagement;
using DALFactory;
using Lib.Model.SourcingManage.SourcingManagement;

namespace Lib.Bll.SourcingManage.SourcingManagement
{
    public class TransactItemBLL
    {
        TransactItemIDAL idal =
            DALFactoryHelper.CreateNewInstance<TransactItemIDAL>(
            "SourcingManage.SourcingManagement.TransactItemDAL");

        public int addTransactItem(TransactItem tt)
        {
            return idal.addTransactItem(tt);
        }

        public List<TransactItem> getTransactItemsByBId(string bidId,string supplierId)
        {
            return idal.getTransactItemsByBId(bidId,supplierId);
        }
    }
}
