﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.Model.SourcingManage.SourcingManagement;
using Lib.IDAL.SourcingManage.SourcingManagement;
using DALFactory;

namespace Lib.Bll.SourcingManage.SourcingManagement
{
    public class BidBLL
    {

        BidIDAL idal =
           DALFactoryHelper.CreateNewInstance<BidIDAL>(
           "SourcingManage.SourcingManagement.BidDAL");

        /// <summary>
        /// 添加招标
        /// </summary>
        /// <param name="bid"></param>
        public int addBid(Bid bid)
        {
            return idal.addBid(bid);
        }



        public List<Bid> getBids()
        {
           return  idal.getBids();
        }
    }
}
