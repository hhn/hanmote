﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.Model.SourcingManage.SourcingManagement;
using Lib.IDAL.SourcingManage.SourcingManagement;
using Lib.SqlServerDAL.SourcingManage.SourcingManagement;
using DALFactory;
using System.Data;

namespace Lib.Bll.SourcingManage.SourcingManagement
{
    public class SourceResultBLL
    {
        SourceResultIDAL idal =
           DALFactoryHelper.CreateNewInstance<SourceResultIDAL>(
           "SourcingManage.SourcingManagement.SourceResultDAL");

        public int addSourceResult(SourceResult sim)
        {
            return idal.addSourceResult(sim);
        }

        #region 为创建合同服务
        public List<SourceResult> getSourceItemMaterialByFK(string contractID)
        {
            return idal.getSourceItemMaterialByFK(contractID);
        }
        #endregion
    }
}
