﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.Model.SourcingManage.SourcingManagement;
using Lib.IDAL.SourcingManage.SourcingManagement;
using Lib.SqlServerDAL.SourcingManage.SourcingManagement;
using DALFactory;
using System.Data;

namespace Lib.Bll.SourcingManage.SourcingManagement
{
    public class EnquiryBLL
    {
        EnquiryDAL enquiryDAL = new EnquiryDAL();
        SourceInquiryDAL inquiryDAL = new SourceInquiryDAL();

        /// <summary>
        /// 保存新的询价单
        /// </summary>
        /// <param name="enquiry"></param>
        /// <returns></returns>
        public int addNewEnquiry(List<Enquiry> enquiryList, SourceInquiry inquiry)
        {
            return enquiryDAL.addNewEnquiry(enquiryList, inquiry);
        }

        /// <summary>
        /// 保存新的询价单表头
        /// </summary>
        /// <param name="inquiry_Table"></param>
        /// <returns></returns>
        public int addNewInquiry_Table(SourceInquiry inquiry_Table)
        {
            return inquiryDAL.addNewInquiry_Table(inquiry_Table);
        }

        /// <summary>
        /// 查询单号是否存在
        /// </summary>
        /// <param name="inquiry_Table"></param>
        /// <returns></returns>
        public bool isInquiry_TableExist(string id)
        {
            return inquiryDAL.isInquiry_TableExist(id);
        }

        /// <summary>
        /// 更改表头信息
        /// </summary>
        /// <param name="inquiry_Table"></param>
        /// <returns></returns>
        public int updateInquiry_Table(SourceInquiry inquiry_Table)
        {
            return inquiryDAL.updateInquiry_Table(inquiry_Table);
        }

        /// <summary>
        /// 查询已存在的表头信息
        /// </summary>
        /// <param name="demand_ID"></param>
        /// <returns></returns>
        public SourceInquiry findInquiryTable(Dictionary<string, string> conditions, String table)
        {
            return enquiryDAL.findInquiryTable(conditions, table);
        }

        /// <summary>
        /// 查询已存在的表头信息
        /// </summary>
        /// <param name="demand_ID"></param>
        /// <returns></returns>
        public List<SourceInquiry> findInquiryList(Dictionary<string, string> conditions, String table)
        {
            return enquiryDAL.findInquiryList(conditions, table);
        }

        /// <summary>
        /// 根据询价单号查询已存在的询价单信息
        /// </summary>
        /// <param name="demand_ID"></param>
        /// <returns></returns>
        public List<Enquiry> findEnquiry(Dictionary<string, string> conditions, String table)
        {
            return enquiryDAL.findEnquiry(conditions, table);
        }

        /// <summary>
        /// 更改报价信息
        /// </summary>
        /// <param name="inquiry_Table"></param>
        /// <returns></returns>
        public int updateEnquiryOffer(List<Enquiry> enquiryList, SourceInquiry inquiry)
        {
            return enquiryDAL.updateEnquiryOffer(enquiryList, inquiry);
        }

        /// <summary>
        /// 查询报价单某状态数量
        /// </summary>
        /// <param name="inquiry_Table"></param>
        /// <returns></returns>
        public int enquiryCount(Enquiry enquiry)
        {
            return enquiryDAL.enquiryCount(enquiry);
        }

    }
}
