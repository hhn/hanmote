﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DALFactory;
using Lib.IDAL.SourcingManage.SourcingManagement;
using Lib.Model.SourcingManage.SourcingManagement;

namespace Lib.Bll.SourcingManage.SourcingManagement
{
    public class BidSupplierWeightScoreBLL
    {
        BidSupplierWeightScoreIDAL idal =
           DALFactoryHelper.CreateNewInstance<BidSupplierWeightScoreIDAL>(
           "SourcingManage.SourcingManagement.BidSupplierWeightScoreDAL");

        public int addBidSupplierWeightScore(BidSupplierWeightScore bsws)
        {
            return idal.addBidSupplierWeightScore(bsws);
        }

        public int  getResultByBidSIdWeightName(string bid, string sid,string wn)
        {
            return idal.getResultByBidSIdWeightName(bid, sid,wn);
        }

        public List<BidSupplierWeightScore> getBidSWSByBIdSId(string bidId, string supplierId)
        {
            return idal.getBidSWSByBIdSId(bidId, supplierId);
        }

        public BidSupplierWeightScore getBSWSByBIdSIdWN(string p, string supplierId, string p_2)
        {
            return idal.getBSWSByBIdSIdWN(p, supplierId, p_2);
        }
    }
}
