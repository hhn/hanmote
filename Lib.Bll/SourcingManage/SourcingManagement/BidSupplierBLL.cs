﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.Model.SourcingManage.SourcingManagement;
using Lib.IDAL.SourcingManage.SourcingManagement;
using DALFactory;


namespace Lib.Bll.SourcingManage.SourcingManagement
{
    public class BidSupplierBLL
    {
        BidSupplierIDAL idal = DALFactoryHelper.CreateNewInstance<BidSupplierIDAL>("SourcingManage.SourcingManagement.BidSupplierDAL");

        //添加BidSupplier
        public int addBidSupplier(BidSupplier bs)
        {
            return idal.addBidSupplier(bs);
        }

        public List<BidSupplier> getBidSupplier()
        {
            return idal.getBidSupplier();
        }
    }
}
