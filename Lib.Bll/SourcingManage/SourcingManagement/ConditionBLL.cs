﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.IDAL.SourcingManage.SourcingManagement;
using Lib.Model.SourcingManage.SourcingManagement;
using DALFactory;

namespace Lib.Bll.SourcingManage.SourcingManagement
{
    public class ConditionBLL
    {
        ConditionIDAL idal =
           DALFactoryHelper.CreateNewInstance<ConditionIDAL>(
           "SourcingManage.SourcingManagement.ConditionDAL");

        public int addCondition(ConditionType condition)
        {
            return idal.addCondition(condition);
        }

        public int updateCondition(ConditionType condition)
        {
            return idal.updateCondition(condition);
        }

        public List<ConditionType> getConditions()
        {
            return idal.getConditions();
        }

        public ConditionType findConditionTypeByConditionID(string conditionId)
        {
            return idal.findConditionTypeByConditionID(conditionId);
        }
    }
}
