﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.IDAL.FileManage;
using Lib.SqlServerDAL;
using Lib.Model.FileManage;

namespace Lib.Bll.FileManageBLL
{
    public class FileTypeBLL
    {
        FileTypeIDAL fileTypeDAL = DALFactory.DALFactoryHelper.
            CreateNewInstance<FileTypeIDAL>("FileManageDAL.FileTypeDAL");

        /// <summary>
        /// 获取所有的文件类型名称
        /// </summary>
        /// <returns></returns>
        public List<string> getAllFileTypeName() {
            return fileTypeDAL.getAllFileTypeName();
        }

        /// <summary>
        /// 根据文件类型名称获取文件类型信息
        /// </summary>
        /// <param name="fileTypeName"></param>
        /// <returns></returns>
        public File_Type getFileType(string fileTypeName) {
            return fileTypeDAL.getFileType(fileTypeName);
        }

        /// <summary>
        /// 删除文件类型
        /// </summary>
        /// <param name="fileTypeName"></param>
        /// <returns></returns>
        public int deleteFileType(string fileTypeName) {
            return fileTypeDAL.deleteFileType(fileTypeName);
        }

        /// <summary>
        /// 增加文件类型
        /// </summary>
        /// <param name="fileType"></param>
        /// <returns></returns>
        public int addFileType(File_Type fileType) {
            return fileTypeDAL.addFileType(fileType);
        }
        /// <summary>
        /// 更新文件类型
        /// </summary>
        /// <param name="fileType"></param>
        /// <returns></returns>
        public int updateFileType(File_Type fileType) {
            return fileTypeDAL.updateFileType(fileType);
        }
    }
}
