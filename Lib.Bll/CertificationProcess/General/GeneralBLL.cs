﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.IDAL.CertificationProcess;
using DALFactory;
using System.Data;
using Lib.Model.CertificationProcess.GN;

namespace Lib.Bll.CertificationProcess.General
{
    public class GeneralBLL
    {
        private static readonly GeneralIDAL idal = DALFactoryHelper.CreateNewInstance<GeneralIDAL>("CertificationProcess.GeneralDAL");

        //获取所有注册信息
        public DataTable GetAllInfo(string id)
        {
            return idal.GetAllInfo(id);
        }
        public DataTable page(string UserId, int pagesize, int currentpage,out int totalSize)//分页
        {
            return idal.page(UserId, pagesize, currentpage, out totalSize);
        }
        public int calculateUserNumber()
        {
            string id=null;
            DataTable userInforTable = GetAllInfo(id);
            if (userInforTable == null || (userInforTable.Rows.Count > 0 && userInforTable.Rows[0][0].Equals(DBNull.Value)))
            {
                return 0;
            }
            else
            {
                return userInforTable.Rows.Count;
            }
        }
        public DataTable GetAllInfoe(String userId)
        {
            return idal.GetAllInfoe(userId);
        }
        public DataTable GetAllInfo1(string userId,int PageSize, int PageIndex, out int totalSize)
        {
            return idal.GetAllInfo1(userId, PageSize, PageIndex,out totalSize);
        }
        public DataTable GetAllInfo2(string id)
        {
            return idal.GetAllInfo2(id);
        }
        public DataTable GetAllInfo3(string id)
        {
            return idal.GetAllInfo3(id);
        }
        public void updateEvaluate(string obj1, string obj2, string obj3, string obj4, string obj5)
        {
            idal.updateEvaluate(obj1, obj2, obj3, obj4, obj5);
        }
        public void deal1(string obj1, string obj2, string obj3, string obj4, string obj5)
        {
            idal.deal1(obj1, obj2, obj3, obj4, obj5);
        }
        public void insert(string id, string thing)//插入file_status数据
        {
            idal.insert(id, thing);
        }
        public void inserttask(string id, string thing)//插入file_status数据
        {
            idal.inserttask(id, thing);
        }
        public void Updatetask(string zhuid, string sname)//插入file_status数据
        {
            idal.Updatetask(zhuid, sname);
        }
        public void Updatetask1(string zhuid, string sname)//插入file_status数据
        {
            idal.Updatetask1(zhuid, sname);
        }
        public void updateTask(string id, string thing)//插入file_status数据
        {
            idal.updateTask(id, thing);
        }

        public DataTable GetAll(string id)
        {
            return idal.GetAll(id);
        }
        public DataTable GetThingLevel(string thing)
        {
            return idal.GetThingLevel(thing);
        }
        public DataTable Getzhuid(string thing)
        {
            return idal.Getzhuid(thing);
        }
        public void RestartInfo(string id)//预评重置
        {
            idal.RestartInfo(id);
        }
        public void adelete(string id)//预评重置
        {
            idal.adelete(id);
        }

        public DataTable getEClassMtName()
        {
            return idal.getEClassMtName();
        }

        public void adeletetask(string zid, string zzid)//预评重置
        {
            idal.adeletetask(zid,zzid);
        }
        public void adeletetask1(string zzid)//预评重置
        {
            idal.adeletetask1(zzid);
        }
        public void adeletetask2(string zzid)//预评重置
        {
            idal.adeletetask2(zzid);
        }
        public void deal(string id, string reason, string score, string sum, string isno, string eid)//预评评估员处理
        {
            idal.deal(id, reason, score, sum, isno,eid);
        }
        public void deal1(string reason,string id)//预评评估员处理
        {
            idal.deal1(reason,id);
        }
        public void RestartInf(string email)//预评状态置1和旧的
        {
            idal.RestartInf(email);
        }
        public void RestartInf1(string email)//预评状态置1和旧的
        {
            idal.RestartInf1(email);
        }
        public void RestartInf2(string email)//提交高层
        {
            idal.RestartInf2(email);
        }
        public void RestartInf3(string id,string gid, string User_ID, string level, string reason)//提交高层
        {
            idal.RestartInf3(id, gid, User_ID, level, reason);
        }
        public void pdelete(string id)//提交高层
        {
            idal.pdelete(id);
        }
        public void prdelete(string id,string eid)//提交高层
        {
            idal.prdelete(id,eid);
        }
        public void RejectInf(string email)//预评拒绝状态置1和旧的
        {
            idal.RejectInf(email);
        }
        public void RejectInf1(string email)//预评拒绝状态置1和旧的
        {
            idal.RejectInf1(email);
        }
        public void RestartInfo0(string id)//筛选重置
        {
            idal.RestartInfo(id);
        }
        public void RestartInf0(string email)//筛选状态置1和旧的
        {
            idal.RestartInf(email);
        }
        public void RejectInf0(string email)//筛选拒绝状态置1和旧的
        {
            idal.RejectInf(email);
        }
        public void deleteInfo(string email)//删除已建立的评估员小组
        {
            idal.deleteInfo(email);
        }
        
        public void restart(string email)//删除已建立的评估员小组
        {
            idal.restart(email);
        }
        public void deleteInfo1(string email, string eid)//删除评估员已处理的信息
        {
            idal.deleteInfo1(email,eid);
        }
        public void Delete(string id)//删除评估员已处理的信息
        {
            idal.Delete(id);
        }
        public bool Get(string id)
        {
            return idal.Get(id);
        }
        public bool Get1(string id)
        {
            return idal.Get1(id);
        }
        public bool GetStatus(string id)
        {
            return idal.GetStatus(id);
        }
        public bool GetTaskStatus(string id)
        {
            return idal.GetTaskStatus(id);
        }
        public bool GetStatus1(string id)
        {
            return idal.GetStatus1(id);
        }
        public bool GetStatus2(string id,string eid)
        {
            return idal.GetStatus2(id,eid);
        }
        public bool fGetStatus(string id)
        {
            return idal.fGetStatus(id);
        }
        public bool ffGetStatus(string id)
        {
            return idal.ffGetStatus(id);
        }
        public void GetStatus3(string id, string eid)
        {
            idal.GetStatus3(id, eid);
        }
        public DataTable GetInfo(string UserID, string purchase, string thingGroup, string status,string country,string name)//筛选查询
        {   
            return idal.GetInfo(UserID, purchase, thingGroup, status, country, name);
        }
        public DataTable eGetInfo(string uid,string purchase, string thingGroup, string status, string country, string name)//评估员查询
        {
            return idal.eGetInfo(uid,purchase, thingGroup, status, country, name);
        }
        public DataTable checkdetail(string id)
        {
            return idal.checkdetail(id);
        }
        public DataTable getid(string id)//获取高层id
        {
            return idal.getid(id);
        }
        public DataTable getscore(string id)//获取高层id
        {
            return idal.getscore(id);
        }
        public DataTable getsum(string id)//获取高层id
        {
            return idal.getsum(id);
        }

        public DataTable getthing(string thing)//获取高层id
        {
            return idal.getthing(thing);
        }
        public void back(string id, string eid)
        {
            idal.back(id,eid);
        }
        public DataTable GetInfo1(string userId, string purchase, string thingGroup, string status, string country, string name)
        {
            return idal.GetInfo1(userId, purchase, thingGroup, status, country, name);
        }
        public DataTable GetInfo2(string purchase, string name,string id)
        {
            return idal.GetInfo2(purchase, name,id);
        }

        public void updateAndDeleteTask(string user_ID, string supplierName)
        {
            idal.updateAndDeleteTask(user_ID, supplierName);
        }
    }
}
