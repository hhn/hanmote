﻿using DALFactory;
using Lib.IDAL.CertificationProcess.LocationalEvaluationIDAL;
using Lib.Model.CertificationProcess.LocationEvaluation;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Lib.Bll.CertificationProcess.LocationalEvaluation
{
    public class LocationEvalutionPersonBLL 
    {
        LocationEvaluationPersionIDAL locationEvaluationPersionIDAL = DALFactoryHelper.CreateNewInstance<LocationEvaluationPersionIDAL>("CertificationProcess.LocationEvaluation.LocationEvalutionPersonDAL");
        /// <summary>
        /// all info
        /// </summary>
        /// <param name="pageNum"></param>
        /// <param name="frontNum"></param>
        /// <returns></returns>
        public DataTable findAllLocalPersonList(String MtGroupName,String companyID,int pageNum, int frontNum)
        {
            return locationEvaluationPersionIDAL.findAllEvalutionPersonList(MtGroupName,companyID, pageNum, frontNum);
        }
        /// <summary>
        /// info by condition
        /// </summary>
        /// <param name="model"></param>
        /// <param name="pageNum"></param>
        /// <param name="frontNum"></param>
        /// <returns></returns>
        public DataTable findAllLocalPersionByCondition(String companyID,String name,String evalcode, int pageNum,int frontNum)
        {
            return locationEvaluationPersionIDAL.findAllEvalutionPseronListByCondition(companyID,name,evalcode, pageNum, frontNum);
        }
        /// <summary>
        /// 所有负责某个公司的评审员
        /// </summary>
        /// <param name="companyID"></param>
        /// <returns></returns>
        public DataTable findAllLocalEvalInfoByCompanyId(string companyID)
        {
            return locationEvaluationPersionIDAL.findAllLocalEvalInfoByCompanyId(companyID);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyID"> 注册公司ID</param>
        /// <param name="company_Name"> 注册公司</param>
        /// <param name="contactMan">注册人姓名</param>
        /// <param name="text1"></param>
        /// <param name="text2"></param>
        public void insertSsupplier_MainAccountInfo(string companyID, string company_Name, string contactMan, string acount, string password)
        {
            locationEvaluationPersionIDAL.insertSsupplier_MainAccountInfo(companyID, company_Name, contactMan, acount, password);
        }

        public void setUpLvelBycompanyID(string companyID)
        {
            locationEvaluationPersionIDAL.setUpLvelBycompanyID(companyID);
        }

        public DataTable detailLocalInfoByCompanyIDAndEvaluateID(string companyID, string evaluateID)
        {
            return locationEvaluationPersionIDAL.detailLocalInfoByCompanyIDAndEvaluateID(companyID, evaluateID);
        }

        /// <summary>
        /// 个人评审任务列表
        /// </summary>
        /// <param name="userId">登陆者ID</param>
        /// <param name="pageSize">每页显示条数</param>
        /// <param name="pageIndex"></param>
        /// <returns></returns>
        public DataTable singlePersonEvaluationList(string userId, int pageSize, int pageIndex,out int totalSize)
        {
            return locationEvaluationPersionIDAL.singlePersonEvaluationList(userId, pageSize, pageIndex,out totalSize);
        }

        public int insertSupplierBase(String SupplierId)
        {
           return locationEvaluationPersionIDAL.insertSupplierBase(SupplierId);
        }

        /// <summary>
        /// 得到高层邮箱
        /// </summary>
        /// <returns></returns>
        public string getSenorLeaderMail(String userId)
        {
            return locationEvaluationPersionIDAL.getSenorLeaderMail(userId);
        }
        /// <summary>
        /// 得到评分总分
        /// </summary>
        /// <param name="user_ID"></param>
        /// <param name="companyID"></param>
        /// <returns></returns>
        public DataTable getSumEvaluateScoreByCompanyID(string user_ID, string companyID)
        {
            return locationEvaluationPersionIDAL.getSumEvaluateScoreByCompanyID(user_ID,companyID);
        }

        /// <summary>
        /// 根据ID得到companyinfo
        /// </summary>
        /// <param name="companyID"></param>
        /// <returns></returns>
        public DataTable getCompangInfoByCompanyID(string companyID)
        {
            return locationEvaluationPersionIDAL.getCompangInfoByCompanyID(companyID);
        }

        /// <summary>
        /// 建立评审小组
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        public DataTable getSelectedEvaluationPersonByIds(String ids, string companyID, int pageNum, int frontNum)
        {
            return locationEvaluationPersionIDAL.getSelectedEvaluationPerson(ids,companyID,pageNum,frontNum);
        }
        /// <summary>
        /// 根据useid conpanyID 查看当前信息状态
        /// </summary>
        /// <param name="companyID"></param>
        /// <param name="v"></param>
        public DataTable getSinglePersonEvalStayusByCompanyAndUserID(string companyID, string v)
        {
            return locationEvaluationPersionIDAL.getSinglePersonEvalStayusByCompanyAndUserID(companyID, v);
        }

        public void deleteTaskSpecificationSportToUpper(string mainEvalID, string v)
        {
            locationEvaluationPersionIDAL.deleteTaskSpecificationSportToUpper(mainEvalID, v);
        }

        public void setCompanyIDTagThirdZeroBycompanyID(string companyID)
        {
            locationEvaluationPersionIDAL.setCompanyIDTagThirdZeroBycompanyID(companyID);
        }

        /// <summary>
        /// 更新任务状态
        /// </summary>
        /// <param name="user_ID"></param>
        public void updateTaskSpecification(string user_ID,string companyName)
        {
            locationEvaluationPersionIDAL.updateTaskSpecification(user_ID,companyName);
        }

        /// <summary>
        /// 插入评估信息
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="state"></param>
        /// <param name="score"></param>
        /// <param name="scoreSum"></param>
        public void insertLocalEvalInfoBySingelPerson(string companyID,string UserID,string filePath, string state, string score, string scoreSum,string DenyItems)
        {
            locationEvaluationPersionIDAL.insertLocalEvalInfoBySingelPerson(companyID,UserID,filePath, state, score, scoreSum, DenyItems);
        }

        public DataTable getDetailEvaluateScoreByCompanyID(string User_ID, string companyID)
        {
            return locationEvaluationPersionIDAL.getDetailEvaluateScoreByCompanyID(User_ID,companyID);
        }
        public string getMainEvalIDByMtGroupName(string mtName, string mtPor)
        {
            return locationEvaluationPersionIDAL.getMainEvalIDByMtGroupName(mtName,mtPor);
        }

        public string getMtPorNameByCompanyId(string companyID)
        {
            return locationEvaluationPersionIDAL.getMtPorNameByCompanyId(companyID);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyID"></param>
        /// <returns></returns>
        public string getMtGroupNameByCompanyId(string companyID)
        {
            return locationEvaluationPersionIDAL.getMtGroupNameByCompanyId(companyID);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ids"></param>
        /// <param name="companyID"></param>
        /// <param name="pageNum"></param>
        /// <param name="frontNum"></param>
        /// <returns></returns>
        public DataTable getSelectedEvaluationPersonGroup(string companyID)
        {
            return locationEvaluationPersionIDAL.getSelectedEvaluationPersonGroup(companyID);
        }

        public void setIsFinishedByUserID(string UserID, string companyName)
        {
            locationEvaluationPersionIDAL.setIsFinishedByUserID(UserID, companyName);
        }

        /// <summary>
        /// 
        /// 评审员充值信息
        /// </summary>
        /// <param name="companyID"></param>
        /// <param name="v"></param>
        public void resetInfoEvaluationBySinglePerson(string companyID, string v)
        {
            locationEvaluationPersionIDAL.resetInfoEvaluationBySinglePerson(companyID, v);
        }

        public string getEvalInfoEmaiml(string evaluateID)
        {
            return locationEvaluationPersionIDAL.getEvalInfoEmaiml(evaluateID);
        }

        /// <summary>
        /// 主审将评审员评审信息重置
        /// </summary>
        /// <param name="evaluateID"></param>
        public void backSinglePersonEvalInfo(string UserID,string evaluateID,string companyID)
        {
            locationEvaluationPersionIDAL.backSinglePersonEvalInfo(UserID,evaluateID,companyID);
        }

        /// <summary>
        /// 插入信息
        /// </summary>
        /// <param name="companyID"></param>
        /// <param name="MtGroupName"></param>
        /// <param name="evaluateID"></param>
        public void insertLocalEvalInfo(String evalCode,String companyID, String MtGroupName, String evaluateID)
        {
            locationEvaluationPersionIDAL.insertLocaEvalInfo(evalCode,companyID, MtGroupName, evaluateID);
        }
        /// <summary>
        /// 计算数量
        /// </summary>
        /// <returns></returns>
        public int calculateUserNumber()
        {
            DataTable userInforTable = locationEvaluationPersionIDAL.calculateRecordNumber();
            if (userInforTable == null || (userInforTable.Rows.Count > 0 && userInforTable.Rows[0][0].Equals(DBNull.Value)))
            {
                return 0;
            }
            else
            {
                return userInforTable.Rows.Count;
            }
        }
        /// <summary>
        /// 存入主审意见 
        /// </summary>
        /// <param name="companyID"></param>
        public void saveMainAdvicesOrSSEMFile(string user_ID,String SSEMFilePath, String MainEvalAdvice, string companyID,string mtGroupName)
        {
            locationEvaluationPersionIDAL.saveMainAdvicesOrSSEMFile(user_ID,SSEMFilePath, MainEvalAdvice,companyID, mtGroupName);
        }
        /// <summary>
        /// 插入任务
        /// </summary>
        /// <param name="v1"></param>
        /// <param name="v2"></param>
        public void insertTaskSpecification(string v1, string v2)
        {
            locationEvaluationPersionIDAL.insertTaskSpecification(v1, v2);
        }

        /// <summary>
        /// 查找评审小组
        /// </summary>
        /// <param name="companyID"></param>
        /// <returns></returns>
        public DataTable getLocalEvalGroupStatByCompanyID(String companyID)
        {
            return locationEvaluationPersionIDAL.getLocalEvalInfoStatBySupplierID(companyID);
        }
        /// <summary>
        /// 删除已选评审员信息
        /// </summary>
        /// <param name="companyID"></param>
        /// <param name="iD"></param>
        public void delLocalEvalInfo(string companyID, string iD)
        {
            locationEvaluationPersionIDAL.delEvalInfoByCompanyIDAndID(companyID, iD);
        }
        /// <summary>
        /// 通过Id得到评审状态
        /// </summary>
        /// <param name="companyID"></param>
        /// <returns></returns>
        public int getEndResultStatusBYCompanyID(String companyID)
        {
            return locationEvaluationPersionIDAL.getEndResultStatusBYCompanyID(companyID);
        }
        /// <summary>
        /// 查看状态是未完成通知
        /// </summary>
        /// <param name="companyID"></param>
        /// <returns></returns>
        public DataTable getunfinishedEvalPersonInfo(String companyID)
        {
            return locationEvaluationPersionIDAL.getunfinishedEvalPersonInfo(companyID);
        }
        /// <summary>
        /// 建立小组是通知得到邮箱
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        public String getEvaluatorMailByEvalID(string v)
        {
            return locationEvaluationPersionIDAL.getEvaluatorMailByEvalID(v);
        }
        /// <summary>
        /// 重置
        /// </summary>
        /// <param name="companyID"></param>
        public void deleteEvalGroupByCompanyID(string companyID)
        {
            locationEvaluationPersionIDAL.deleteEvalGroupByCompanyID(companyID);
        }

        public DataTable findAllTaskByUserID(string user_ID)
        {
            return locationEvaluationPersionIDAL.findAllTaskByUserID(user_ID);
        }

        public void deleteTaskSpecification(string userID,string companyName)
        {
             locationEvaluationPersionIDAL.deleteTaskSpecification(userID,companyName);
        }
    }
}
