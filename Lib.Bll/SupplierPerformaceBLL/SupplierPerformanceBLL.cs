﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Lib.SqlIDAL.SupplierPerformaceIDAL;
using DALFactory;
using System.Windows.Forms;
using Lib.Common.CommonUtils;

namespace Lib.Bll.SupplierPerformaceBLL
{
    public class SupplierPerformanceBLL
    {
        private static readonly SupplierPerformanceIDAL supplierPerformanceIDAL = DALFactoryHelper.CreateNewInstance<SupplierPerformanceIDAL>("SupplierPerformaceDAL.SupplierPerformanceDAL");

        #region 供应商评估

        #region 价格标准的旧代码
        /// <summary>
        /// 价格-价格水平
        /// </summary>
        /// <param name="materialID"></param>
        /// <param name="supplierID"></param>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <param name="priceLevelPara"></param>
        /// <returns></returns>
        public Decimal queryPriceLevel(String materialID, String supplierID, DateTime startTime, DateTime endTime, List<double> priceLevelPara)
        {

            DataTable priceLevelTable = supplierPerformanceIDAL.queryPriceLevel(materialID, supplierID, startTime, endTime, priceLevelPara);
            double rst = 0.0;
            double priceLevelPer;
            //如果在数据库中未查到数据，则返回数值101，作为异常的标志
            if ((priceLevelTable.Rows[0][0] == DBNull.Value) || (priceLevelTable.Rows[0][1] == DBNull.Value))
            {
                return 101;
            }
            else
            {
                foreach (DataRow row in priceLevelTable.Rows)
                {
                    priceLevelPer = (Convert.ToDouble(row[1]) - Convert.ToDouble(row[0])) / Convert.ToDouble(row[0]);
                    if (priceLevelPer <= priceLevelPara[0]) //小于P LOW
                    {
                        rst = rst + 100;
                    }
                    else if (priceLevelPer >= priceLevelPara[1]) //大于P HIGH
                    {
                        rst = rst + 0;
                    }
                    else
                    {
                        rst = rst + (100 / (priceLevelPara[1] - priceLevelPara[0])) * (priceLevelPara[1] - priceLevelPer);
                    }
                }

                return Convert.ToDecimal(rst / priceLevelTable.Rows.Count);
            }
        }

        /// <summary>
        /// 价格-价格历史
        /// </summary>
        /// <param name="materialID"></param>
        /// <param name="supplierID"></param>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <param name="priceHistoryPara"></param>
        /// <returns></returns>
        public Decimal queryPriceHistory(String materialID, String supplierID, DateTime startTime, DateTime endTime, List<double> priceHistoryPara)
        {
            DataTable priceHistoryTable = supplierPerformanceIDAL.queryPriceHistory(materialID, supplierID, startTime, endTime, priceHistoryPara);
            double rst = 0.0;
            double priceHistoryPer;
            //如果在数据库中未查到数据，则返回数值101，作为异常的标志
            if(priceHistoryTable.Rows[0][0] == DBNull.Value)
            {
                MessageBox.Show("MCP数据缺失，无法完成评估");
                return 101;
            }
            if(priceHistoryTable.Rows[0][1] == DBNull.Value)
            {
                MessageBox.Show("MPP数据缺失，无法完成评估");
                return 101;
            }
            if(priceHistoryTable.Rows[0][2] == DBNull.Value)
            {
                MessageBox.Show("ASP数据缺失，无法完成评估");
                return 101;
            }
            if(priceHistoryTable.Rows[0][3] == DBNull.Value)
            {
                MessageBox.Show("SPP数据缺失，无法完成评估");
                return 101;
            }
            foreach (DataRow row in priceHistoryTable.Rows)
            {
                //每行存放的数据依次为MCP，MPP，ASP，SPP（市场当前价，市场历史价，供应商当前价，供应商历史价）
                
                priceHistoryPer = ((Convert.ToDouble(row[1]) * Convert.ToDouble(row[2])) / (Convert.ToDouble(row[0]) * Convert.ToDouble(row[3]))) - 1;
                if (priceHistoryPer <= priceHistoryPara[0])
                {
                    rst = rst + 100;
                }
                else if (priceHistoryPer >= priceHistoryPara[1])
                {
                    rst = rst + 0;
                }
                else
                {
                    rst = rst + (100 / (priceHistoryPara[1] - priceHistoryPara[0])) * (priceHistoryPara[1] - priceHistoryPer);
                }
            }

            return Convert.ToDecimal(rst / priceHistoryTable.Rows.Count);
        }
        #endregion

        #region 价格水平标准的新代码（修改价格数据表后的代码）

        /// <summary>
        /// 计算每一个采购订单对应的价格水平差额百分比
        /// </summary>
        /// <returns></returns>
        public Decimal calculatePriceLevelPer(String orderID, String materialID, String supplierID, DateTime startTime, DateTime endTime, List<double> priceLevelPara)
        {
            // 价格水平 NetPrice right
            Decimal marketPrice = supplierPerformanceIDAL.queryMarketPrice(materialID, supplierID,startTime,endTime);
            //历史价格水平 right
            Decimal supplierPrice = supplierPerformanceIDAL.querySupplierPrice(materialID,supplierID,startTime,endTime);
            if (marketPrice < 0)
            {
                return 101;
            }
            else if (supplierPrice < 0)
            {
                return 101;
            }
            else
            {
                //计算价格水平差额百分比
                Decimal percentage = (supplierPrice - marketPrice) / marketPrice;
                Decimal score = 0M;


                if (percentage <= Convert.ToDecimal(priceLevelPara[0])) //小于P LOW
                {
                    score = 100;
                }
                else if (percentage >= Convert.ToDecimal(priceLevelPara[1])) //大于P HIGH
                {
                    score =  0;
                }
                else
                {
                    score = (100 / (Convert.ToDecimal(priceLevelPara[1]) - Convert.ToDecimal(priceLevelPara[0]))) * (Convert.ToDecimal(priceLevelPara[1]) - percentage);
                }
                return score;
            }

        }

        /// <summary>
        /// 计算所有符合要求的采购订单在价格水平次标准上的分数,可直接在form的代码中调用
        /// </summary>
        /// <param name="materialID"></param>
        /// <param name="supplierID"></param>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <returns></returns>
        public Decimal calculatePriceLevel(String materialID, String supplierID, DateTime startTime, DateTime endTime, List<double> priceLevelPara)
        {
            //
            DataTable orderList = supplierPerformanceIDAL.queryOrderID(materialID,supplierID,startTime,endTime);
            Decimal score = 0M;
            if (orderList.Rows.Count > 0)
            {
                //用于统计总共计算了多少条订单的记录（因为有的订单的记录没有评估数据，所以不能直接用rows.count）
                int count = orderList.Rows.Count;
                foreach (DataRow row in orderList.Rows)
                {
                    String orderID = row[0].ToString();
                    //循环调用计算每一个订单对应分数的函数
                    Decimal newScore = this.calculatePriceLevelPer(orderID, materialID, supplierID, startTime, endTime,priceLevelPara);
                    
                    //这一条订单没有相关的评估数据，从而没有分数
                    if ((newScore <= 100) && (newScore > 0))
                    {
                        score = score + newScore;
                    }
                    else
                    {
                        count--;
                    }
                }

                //保证分母不为零
                if (count != 0)
                {
                    score = score / count;
                    return score;
                }
                else
                {
                    return 101;
                }
            }
            else
            {
                return 101;
            }
        }

        /// <summary>
        /// 计算价格水平分数
        /// </summary>
        /// <param name="materialID"></param>
        /// <param name="supplierID"></param>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <param name="priceLevelPara"></param>
        /// <returns></returns>
        public Decimal calculatePriceLevel_1(String materialID, String supplierID, DateTime startTime, DateTime endTime, List<double> priceLevelPara)
        {
            Decimal marketPrice = supplierPerformanceIDAL.queryMarketPrice(materialID, supplierID, startTime, endTime);
            Decimal supplierPrice = supplierPerformanceIDAL.querySupplierPrice(materialID, supplierID, startTime, endTime);
            if (marketPrice < 0)
            {
                return 101;
            }
            else if (supplierPrice < 0)
            {
                return 101;
            }
            else
            {
                //计算价格水平差额百分比
                Decimal percentage = (supplierPrice - marketPrice) / marketPrice;
                Decimal score = 0M;


                if (percentage <= Convert.ToDecimal(priceLevelPara[0])) //小于P LOW
                {
                    score = 100;
                }
                else if (percentage >= Convert.ToDecimal(priceLevelPara[1])) //大于P HIGH
                {
                    score = 0;
                }
                else
                {
                    score = (100 / (Convert.ToDecimal(priceLevelPara[1]) - Convert.ToDecimal(priceLevelPara[0]))) * (Convert.ToDecimal(priceLevelPara[1]) - percentage);
                }
                return score;
            }
        }

        #endregion

        #region 价格历史标准的新代码（修改价格数据表后的代码）

        /// <summary>
        /// 计算每一个采购订单对应的价格历史 OK
        /// </summary>
        /// <param name="orderID"></param>
        /// <param name="materialID"></param>
        /// <param name="supplierID"></param>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <param name="priceHistoryPara"></param>
        /// <returns></returns>
        public Decimal calculatePriceHistoryPer(String orderID,String materialID,String supplierID,DateTime startTime,DateTime endTime,List<double> priceHistoryPara)
        {
            //MaterialID
            Decimal marketPrice = supplierPerformanceIDAL.queryMarketPrice(materialID, supplierID,startTime,endTime);
            //MaterialID + supplierID
            Decimal supplierPrice = supplierPerformanceIDAL.querySupplierPrice(materialID, supplierID, startTime, endTime);
            //year + MaterialID
            Decimal historyMarketPrice = supplierPerformanceIDAL.queryHistoryMarketPrice( materialID,  startTime,  endTime);
            //year + MaterialID + supplierID
            Decimal historySupplierPrice = supplierPerformanceIDAL.queryHistorySupplierPrice( materialID,  supplierID,  startTime,  endTime);
            
            if (marketPrice < 0)
            {
                return 101;
            }
            else if (supplierPrice < 0)
            {
                return 101;
            }
            else if(historyMarketPrice < 0)
            {
                return 101;
            }
            else if (historySupplierPrice < 0)
            {
                return 101;
            }
            else
            {
                //计算价格水平差额百分比
                Decimal percentage = ((historyMarketPrice*supplierPrice)/(marketPrice*historySupplierPrice))-1;
                Decimal score = 0M;


                if (percentage <= Convert.ToDecimal(priceHistoryPara[0])) //小于P LOW
                {
                    score = 100;
                }
                else if (percentage >= Convert.ToDecimal(priceHistoryPara[1])) //大于P HIGH
                {
                    score = 0;
                }
                else
                {
                    score = (100 / (Convert.ToDecimal(priceHistoryPara[1]) - Convert.ToDecimal(priceHistoryPara[0]))) * (Convert.ToDecimal(priceHistoryPara[1]) - percentage);
                }
                return score;
            }

        }

        /// <summary>
        /// 计算所有符合要求的采购订单在价格历史次标准上的分数,可直接在form的代码中调用
        /// </summary>
        /// <param name="materialID"></param>
        /// <param name="supplierID"></param>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <param name="priceHistoryPara"></param>
        /// <returns></returns>
        public Decimal calculatePriceHistory(String materialID, String supplierID, DateTime startTime, DateTime endTime, List<double> priceHistoryPara)
        {
            //得到信息记录ID
            //SQL DataTable RecordInfo
            DataTable orderList = supplierPerformanceIDAL.queryOrderID(materialID, supplierID, startTime, endTime);
            Decimal score = 0M;
            if (orderList.Rows.Count > 0)
            {
                //用于统计总共计算了多少条订单的记录（因为有的订单的记录没有评估数据，所以不能直接用rows.count）
                int count = orderList.Rows.Count;
                foreach (DataRow row in orderList.Rows)
                {
                    String orderID = row[0].ToString();
                    //循环调用计算每一个订单对应分数的函数

                    Decimal newScore = this.calculatePriceHistoryPer(orderID, materialID, supplierID, startTime, endTime, priceHistoryPara);

                    //这一条订单没有相关的评估数据，从而没有分数
                    if ((newScore <= 100) && (newScore > 0))
                    {
                        score = score + newScore;
                    }
                    else
                    {
                        count--;
                    }
                }

                //保证分母不为零
                if (count != 0)
                {
                    score = score / count;
                    return score;
                }
                else
                {
                    return 101;
                }
            }
            else
            {
                return 101;
            }
        }

        #region 主次标准的计算方法
        /// <summary>
        /// 计算价格主标准分数
        /// </summary>
        /// <param name="priceLevelScore"></param>
        /// <param name="priceHistoryScore"></param>
        /// <param name="priceSSWeight"></param>
        /// <returns></returns>
        public Decimal calculatePriceScore(Decimal priceLevelScore, Decimal priceHistoryScore, List<double> priceSSWeight)
        {
            Decimal rst = 0M;
            rst = (priceLevelScore * Convert.ToDecimal(priceSSWeight[0]) + priceHistoryScore * Convert.ToDecimal(priceSSWeight[1])) / (Convert.ToDecimal(priceSSWeight[0]) + Convert.ToDecimal(priceSSWeight[1]));
            return rst;
        }

        /// <summary>
        /// 质量-收货
        /// </summary>
        /// <param name="materialID"></param>
        /// <param name="supplierID"></param>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <param name="PPMPara"></param>
        /// <returns></returns>
        public Decimal queryReceiveGoods(String materialID, String supplierID, DateTime startTime, DateTime endTime, List<double> PPMPara)
        {
            //SQL DataTable : NonRaw_Material_Receive_Check
            DataTable receiveGoodsTable = supplierPerformanceIDAL.queryReceiveGoods(materialID, supplierID, startTime, endTime, PPMPara);
            double rst = 0.0;
            if(receiveGoodsTable == null)
            {
                return 101;
            }
            if (receiveGoodsTable.Rows.Count == 0)
            {
                return 101;
            }
            int count = receiveGoodsTable.Rows.Count;

            //判断是否为非原材料型物料
            //如果是非原材料型物料
            if (this.queryMaterialType(materialID) == "0")
            {
                //需要修改
                //PPMPara[0]为100分对应的PPM值（即PPM LOW），PPMPara[1]为0分下对应的PPM值（即PPM HIGH）
                double perPPMScore = 100 / (PPMPara[0] - PPMPara[1]);
                foreach (DataRow row in receiveGoodsTable.Rows)
                {
                    //PPM值小于PPM LOW
                    if (Convert.ToDouble(row[0]) <= PPMPara[0])
                    {
                        row[0] = 100;
                    }
                    else if (Convert.ToDouble(row[0]) >= PPMPara[1])
                    {
                        row[0] = 0;
                    }
                    else
                    {
                        row[0] = perPPMScore * (Convert.ToDouble(row[0]) - PPMPara[0]);
                    }
                    try
                    {
                        rst = rst + Convert.ToDouble(row[0]);
                    }
                    catch
                    {
                        count--;
                    }

                }
            }
            //若为原材料型物料
            else
            {

                foreach (DataRow row in receiveGoodsTable.Rows)
                {
                    try
                    {
                        rst = rst + Convert.ToDouble(row[0]);
                    }
                    catch
                    {
                        //当该项值无记录时（即为null时），不把这条记录的数据算在内
                        count--;
                    }
                }


            }

            Decimal finalRst = 101;
            try
            {
                finalRst = Convert.ToDecimal(rst / count);
            }
            catch
            {
                return 101;
            }
            return finalRst;


        }

        /// <summary>
        /// 质量-质量审计
        /// </summary>
        /// <param name="materialID"></param>
        /// <param name="supplierID"></param>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <returns></returns>
        public Decimal queryQualityAudit(String materialID, String supplierID, DateTime startTime, DateTime endTime)
        {
            double rst = 0.0;
            //SQL DataTable :  System_Process_Audit  /  Product_Audit
            DataTable qualityAuditTable = supplierPerformanceIDAL.queryQualityAudit(materialID, supplierID, startTime, endTime);
            //该dataTable仅有三行，分别为体系、过程、产品审核分数
            if (qualityAuditTable.Rows.Count != 3)
            {
                return 101;
            }
            else
            {
                try
                {
                    foreach (DataRow row in qualityAuditTable.Rows)
                    {
                        //判断Audit_Type
                        if (Convert.ToString(row[0]) == "过程审核")
                        {
                            rst = rst + Convert.ToDouble(row[1]) * 0.65;
                        }
                        else if (row[0].ToString() == "体系审核")
                        {
                            rst = rst + Convert.ToDouble(row[1]) * 0.15;
                        }
                        else
                        {
                            rst = rst + Convert.ToDouble(row[1]) * 0.2;
                        }
                    }
                }
                catch
                {
                    return 101;
                }
                
            }
            return Convert.ToDecimal(rst);
        }

        /// <summary>
        /// 质量-抱怨/拒绝水平
        /// </summary>
        /// <param name="materialID"></param>
        /// <param name="supplierID"></param>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <returns></returns>
        public Decimal queryComplaintReject(String materialID, String supplierID, DateTime startTime, DateTime endTime)
        {
            //SQL DataTable : Complaint_Reject
            DataTable complaintRejectTable = supplierPerformanceIDAL.queryComplaintReject(materialID, supplierID, startTime, endTime);
            double rst = 0.0;
            double tmp;//保存计算出的百分比

            if (complaintRejectTable == null)
            {
                rst = rst + 100;
            }
            foreach (DataRow row in complaintRejectTable.Rows)
            {
                //如果业务量参数为空，则使用默认的业务量参数即10%
                if (row[0] == null)
                {
                    row[0] = 0.1;
                }
                //判断有缺陷或不合格的交货的成本是否超过了在用户设置系统中对于的业务量的比例（即n*c>=PT*p） 
                if (Convert.ToDouble(row[3]) * Convert.ToDouble(row[2]) >= Convert.ToDouble(row[1]) * Convert.ToDouble(row[0]))
                {
                    rst = rst + 1;
                }
                else
                {
                    tmp = 1 - Convert.ToDouble(row[3]) * Convert.ToDouble(row[2]) / (Convert.ToDouble(row[1]) * Convert.ToDouble(row[0]));
                    rst = rst + (1 + tmp * 99);
                }
            }

            if (complaintRejectTable.Rows.Count == 0)
            {
                return 100;
            }
            else
            {
                return Convert.ToDecimal(rst / complaintRejectTable.Rows.Count);
            }

        }

        /// <summary>
        /// 质量
        /// </summary>
        /// <param name="receiveGoodsScore"></param>
        /// <param name="qualityAuditScore"></param>
        /// <param name="complaintRejectScore"></param>
        /// <param name="qualitySSWeight"></param>
        /// <returns></returns>
        public Decimal calculateQualityScore(Decimal receiveGoodsScore, Decimal qualityAuditScore, Decimal complaintRejectScore, List<double> qualitySSWeight)
        {
            Decimal rst = 0M;
            rst = (receiveGoodsScore * Convert.ToDecimal(qualitySSWeight[0]) + qualityAuditScore * Convert.ToDecimal(qualitySSWeight[1]) + complaintRejectScore * Convert.ToDecimal(qualitySSWeight[2])) / (Convert.ToDecimal(qualitySSWeight[0]) + Convert.ToDecimal(qualitySSWeight[1]) + Convert.ToDecimal(qualitySSWeight[2]));
            return rst;
        }

        /// <summary>
        /// 收货-按时交货的表现
        /// </summary>
        /// <param name="materialID"></param>
        /// <param name="supplierID"></param>
        /// <param name="purchasingORGID"></param>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <returns></returns>
        public Decimal queryDeliverOnTime(String materialID, String supplierID, DateTime startTime, DateTime endTime)
        {

            //SQL DataTable  [Enquiry],[GoodsReceive_Note]
            DataTable deliverOnTimeTable = supplierPerformanceIDAL.queryDeliverOnTime(materialID, supplierID,startTime, endTime);
            double rst = 0.0;
            DateTime plannedDeliverDate;
            DateTime stockInDate;
            TimeSpan ts;
            int days;
            double ot;//日期偏差差额百分比

            foreach (DataRow row in deliverOnTimeTable.Rows)
            {
                plannedDeliverDate = DateTime.Parse(Convert.ToString(row[0]));
                stockInDate = DateTime.Parse(Convert.ToString(row[1]));
                ts = stockInDate.Subtract(plannedDeliverDate);
                days = ts.Days;
                ot = (double)days / 30.0;

                if (ot >= -0.1 && ot <= 0)
                {
                    rst = rst + (200 * ot + 100);
                }
                else if (ot > 0 && ot <= 0.1)
                {
                    rst = rst + (100 - 200 * ot);
                }
                else if (ot >= 0.55 || ot <= -0.56)
                {
                    rst = rst + 0;
                }
                else
                {
                    rst = rst + (-255 * ot * ot - 1.023 * ot + 80.4);
                }
            }
            if (deliverOnTimeTable.Rows.Count == 0)
            {
                return 101;
            }
            else
            {
                return Convert.ToDecimal(rst / deliverOnTimeTable.Rows.Count);
            }


        }

        /// <summary>
        /// 收货-确认日期
        /// </summary>
        /// <param name="materialID"></param>
        /// <param name="supplierID"></param>
        /// <param name="purchasingORGID"></param>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <param name="confirmDatePara"></param>
        /// <returns></returns>
        public Decimal queryConfirmDate(String materialID, String supplierID,DateTime startTime, DateTime endTime, double confirmDatePara)
        {
            //SQL DataTable : [Enquiry],[GoodsReceive_Note]
            DataTable confirmDateTable = supplierPerformanceIDAL.queryConfirmDate(materialID, supplierID, startTime, endTime, confirmDatePara);
            double rst = 0.0;
            DateTime confirmDate;
            DateTime stockInDate;
            TimeSpan ts;
            int days;
            double ot;//日期偏差差额百分比
            int count = confirmDateTable.Rows.Count;//记录有有效数据用于计算的记录的条数（防止出现null，抛异常）
            //首先需要判断是否达到最小收货数量（待完成）

            //确认日期偏差与分数的对应关系计算需要修改
            foreach (DataRow row in confirmDateTable.Rows)
            {
                try
                {
                    confirmDate = DateTime.Parse(Convert.ToString(row[0]));
                    stockInDate = DateTime.Parse(Convert.ToString(row[1]));
                }
                catch
                {
                    count--;
                    continue;
                }
                ts = stockInDate.Subtract(confirmDate);
                days = ts.Days;
                if (confirmDatePara == 0)
                {
                    if (days == 0)
                    {
                        rst = rst + 100;
                    }
                    else
                    {
                        rst = rst + 0;
                    }
                }
                else
                {
                    ot = Convert.ToDouble(days) / confirmDatePara;

                    if ((-1 < ot) && (ot < 0))
                    {
                        rst = rst + 100 * (1 + ot);
                    }
                    else if ((ot > 0) && (ot < 1))
                    {
                        rst = rst + 100 * (1 - ot);
                    }
                    else
                    {
                        rst = rst + 0;
                    }
                }

            }

            if (confirmDateTable.Rows.Count == 0)
            {
                return 101;
            }
            else
            {
                return Convert.ToDecimal(rst / count);
            }

        }

        /// <summary>
        /// 收货-数量可靠性
        /// </summary>
        /// <param name="materialID"></param>
        /// <param name="supplierID"></param>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <returns></returns>
        public Decimal queryQuantityReliability(String materialID, String supplierID, DateTime startTime, DateTime endTime)
        {
            //SQL DataTable :GoodsReceive_Note
            DataTable quantityReliabilityTable = supplierPerformanceIDAL.queryQuantityReliability(materialID, supplierID, startTime, endTime);
            double rst = 0.0;
            double mr = 0.0;//差额百分比

            foreach (DataRow row in quantityReliabilityTable.Rows)
            {
                if ((Convert.ToDouble(row[0]) - Convert.ToDouble(row[1])) >= 0)
                {
                    rst = rst + 100;
                }
                else
                {
                    mr = (Convert.ToDouble(row[0]) - Convert.ToDouble(row[1])) / Convert.ToDouble(row[1]);
                    rst = rst + 100 * (1 + mr);
                }

                //double receive_count = Convert.ToDouble(row[0]);
                //double count_in_order = Convert.ToDouble(row[1]);

            }

            if (quantityReliabilityTable.Rows.Count == 0)
            {
                return 101;
            }
            else
            {
                return Convert.ToDecimal(rst / quantityReliabilityTable.Rows.Count);
            }
        }

        /// <summary>
        /// 收货-对装运须知的遵守，计算并返回分数
        /// </summary>
        /// <param name="materialID"></param>
        /// <param name="supplierID"></param>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <returns></returns>
        public Decimal queryGoodsShipment(String materialID, String supplierID, DateTime startTime, DateTime endTime)
        {
            //SQL DataTable:GoodsReceive_Note
            DataTable goodsShipmentTable = supplierPerformanceIDAL.queryGoodsShipment(materialID, supplierID, startTime, endTime);
            double rst = 0.0;

            foreach (DataRow row in goodsShipmentTable.Rows)
            {
                if (String.IsNullOrEmpty(row[0].ToString()))
                {
                    continue;
                }
                else
                {
                    rst = rst + Convert.ToDouble(row[0]);
                }    
            }


            if (goodsShipmentTable.Rows.Count == 0)
            {
                return 101;
            }
            else
            {
                return Convert.ToDecimal(rst / goodsShipmentTable.Rows.Count);
            }

        }

        /// <summary>
        /// 收货
        /// </summary>
        /// <param name="dilverOnTimeScore"></param>
        /// <param name="confirmDateScore"></param>
        /// <param name="quantityReliabilityScore"></param>
        /// <param name="goodsShipmentScore"></param>
        /// <param name="deliverySSWeight"></param>
        /// <returns></returns>
        public Decimal calculateDeliverScore(Decimal dilverOnTimeScore, Decimal confirmDateScore, Decimal quantityReliabilityScore, Decimal goodsShipmentScore, List<double> deliverySSWeight)
        {
            Decimal rst = 0M;
            rst = (dilverOnTimeScore * Convert.ToDecimal(deliverySSWeight[0]) + confirmDateScore * Convert.ToDecimal(deliverySSWeight[1]) + quantityReliabilityScore * Convert.ToDecimal(deliverySSWeight[2]) + goodsShipmentScore * Convert.ToDecimal(deliverySSWeight[3])) / (Convert.ToDecimal(deliverySSWeight[0]) + Convert.ToDecimal(deliverySSWeight[1]) + Convert.ToDecimal(deliverySSWeight[2]) + Convert.ToDecimal(deliverySSWeight[3]));
            return rst;
        }

        /// <summary>
        /// 查询Service_Support数据表，计算并返回一般服务/支持主标准分数
        /// </summary>
        /// <param name="materialID"></param>
        /// <param name="supplierID"></param>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>0
        /// <returns></returns>
        public Decimal queryServiceSupport(String materialID, String supplierID, DateTime startTime, DateTime endTime)
        {
            //SQL DataTable : Service_Support
            DataTable serviceSupportTable = supplierPerformanceIDAL.queryServiceSupport(materialID, supplierID, startTime, endTime);
            //遍历DataTable，求平均值
            double rst = 0.0;
            if (serviceSupportTable.Rows.Count == 0)
            {
                return 101;
            }
            else
            {
                foreach (DataRow row in serviceSupportTable.Rows)
                {
                    rst = rst + Convert.ToDouble(row[0]) + Convert.ToDouble(row[1]) + Convert.ToDouble(row[2]);
                }

                return Convert.ToDecimal(rst / (3 * serviceSupportTable.Rows.Count));
            }

        }

        /// <summary>
        /// 查询External_Service数据表，计算并返回外部服务主标准分数
        /// </summary>
        /// <param name="materialID"></param>
        /// <param name="supplierID"></param>
        /// <returns></returns>
        public Decimal queryExternalService(String materialID, String supplierID, DateTime startTime, DateTime endTime)
        {
            //SQL DataTable : External_Service 
            DataTable externalServiceTable = supplierPerformanceIDAL.queryExternalService(materialID, supplierID, startTime, endTime);
            double rst = 0.0;
            if (externalServiceTable.Rows.Count == 0)
            {
                return 101;
            }
            else
            {
                foreach (DataRow row in externalServiceTable.Rows)
                {
                    rst = rst + Convert.ToDouble(row[0]) + Convert.ToDouble(row[1]);
                }
                return Convert.ToDecimal(rst / (2 * externalServiceTable.Rows.Count));
            }

        }

        /// <summary>
        /// 总分
        /// </summary>
        /// <param name="priceScore"></param>
        /// <param name="qualityScore"></param>
        /// <param name="deliverScore"></param>
        /// <param name="serviceSupportScore"></param>
        /// <param name="externalServiceScore"></param>
        /// <param name="mainStandardWeight"></param>
        /// <returns></returns>
        public Decimal calculateTotalScore(Decimal priceScore, Decimal qualityScore, Decimal deliverScore, Decimal serviceSupportScore, Decimal externalServiceScore, List<double> mainStandardWeight)
        {
            Decimal rst = 0M;
            //各主标准分数和各标准权重（数组）以参数的形式传递进来
            //计算5个主标准的加权平均值，作为总分rst的值
            rst = priceScore * Convert.ToDecimal(mainStandardWeight[0]) + qualityScore * Convert.ToDecimal(mainStandardWeight[1]);
            rst = rst + deliverScore * Convert.ToDecimal(mainStandardWeight[2]) + serviceSupportScore * Convert.ToDecimal(mainStandardWeight[3]);
            rst = rst + externalServiceScore * Convert.ToDecimal(mainStandardWeight[4]);

            rst = rst / (Convert.ToDecimal(mainStandardWeight[0]) + Convert.ToDecimal(mainStandardWeight[1]) + Convert.ToDecimal(mainStandardWeight[2]) + Convert.ToDecimal(mainStandardWeight[3]) + Convert.ToDecimal(mainStandardWeight[4]));
            return rst;
        }
        #endregion

        #region 结果查询和保存
        /// <summary>
        /// 结果显示
        /// </summary>
        /// <param name="materialID"></param>
        /// <param name="supplierID"></param>
        /// <param name="purchasingORGID"></param>
        /// <returns></returns>
        public DataTable queryResult(String materialID, String supplierID, String purchasingORGID)
        {
            DataTable resultTable = supplierPerformanceIDAL.queryResult(materialID, supplierID, purchasingORGID);
            int i = 1;

            foreach (DataRow row in resultTable.Rows)
            {
                //该值为编号
                row[0] = i;
                i++;

            }

            return resultTable;
        }

        /// <summary>
        /// 根据物料ID和年份查询绩效评估结果
        /// </summary>
        /// <param name="materialID"></param>
        /// <param name="year"></param>
        /// <returns></returns>
        public DataTable queryResult(String materialID, String year)
        {
            return(supplierPerformanceIDAL.queryResult(materialID,year));
        }

        /// <summary>
        /// 详细结果，查询
        /// </summary>
        /// <param name="materialID"></param>
        /// <param name="supplierID"></param>
        /// <param name="purchasingORGID"></param>
        /// <param name="idNum"></param>
        /// <returns></returns>
        public DataTable queryDetailResult(String materialID, String supplierID, String purchasingORGID, int idNum)
        {
            DataTable resultTable = supplierPerformanceIDAL.queryDetailResult(materialID, supplierID, purchasingORGID, idNum);
            return resultTable;
        }

        /// <summary>
        /// 保存结果,返回受影响的行数
        /// </summary>
        /// <param name="saveRstPara"></param>
        /// <returns></returns>
        public int saveResult(List<Object> saveRstPara)
        {
            int lines;
            lines = supplierPerformanceIDAL.saveResult(saveRstPara);
            return lines;
        }
  
        /// <summary>
        /// 计算年度交易总额
        /// </summary>
        /// <param name="materialID"></param>
        /// <param name="supplierID"></param>
        /// <param name="year"></param>
        /// <returns></returns>
        public Decimal queryTransactionAmount(String materialID, String supplierID, String year)
        {
            DataTable transactionTable = supplierPerformanceIDAL.queryTransactionAmount(materialID, supplierID, year);
            Double rst = 0.0;

            if (transactionTable.Rows.Count == 0)
            {
                return 0;
            }
            else
            {
                foreach (DataRow row in transactionTable.Rows)
                {
                    rst = rst + Convert.ToDouble(row[0]);
                }
                return Convert.ToDecimal(rst);
            }

        }

        /// <summary>
        /// 抱怨/拒绝情况保存
        /// </summary>
        /// <param name="saveCRPara"></param>
        /// <returns></returns>
        public int saveComplaintRejectRecord(List<Object> saveCRPara)
        {
            int lines;
            lines = supplierPerformanceIDAL.saveComplaintRejectRecord(saveCRPara);
            return lines;
        }

        /// <summary>
        /// 一般服务/支持保存（将记录写入数据表Service_Support）
        /// </summary>
        /// <param name="saveSSPara"></param>
        /// <returns></returns>
        public int saveServiceSupportRecord(List<Object> saveSSPara)
        {
            int lines;
            lines = supplierPerformanceIDAL.saveServiceSupportRecord(saveSSPara);
            return lines;
        }

        /// <summary>
        /// 外部服务保存（将记录写入数据表External_Service）
        /// </summary>
        /// <param name="saveESPara"></param>
        /// <returns></returns>
        public int saveExternalServiceRecord(List<Object> saveESPara)
        {
            int lines;
            lines = supplierPerformanceIDAL.saveExternalServiceRecord(saveESPara);
            return lines;
        }

        /// <summary>
        /// 质量审核-产品审核保存（将记录写入数据表Product_Audit）
        /// </summary>
        /// <param name="savePAPara"></param>
        /// <returns></returns>
        public int saveProductAuditRecord(List<Object> savePAPara)
        {
            int lines;
            lines = supplierPerformanceIDAL.saveProductAuditRecord(savePAPara);
            return lines;
        }

        /// <summary>
        /// 体系/过程审核保存（将记录写入数据表System_Process_Audit）
        /// </summary>
        /// <param name="savePSAuditPara"></param>
        /// <param name="chapterInfo"></param>
        /// <param name="scoreList"></param>
        /// <returns></returns>
        public int savePSAuditRecord(List<Object> savePSAuditPara, List<String> chapterInfo, List<Object> scoreList)
        {
            int lines;
            lines = supplierPerformanceIDAL.savePSAuditRecord(savePSAuditPara, chapterInfo, scoreList);
            return lines;
        }
        #endregion

        #region 基本信息获取
        /// <summary>
        /// 查询供应商ID
        /// </summary>
        /// <returns></returns>
        public DataTable querySupplier()
        {
            return (supplierPerformanceIDAL.querySupplier());
        }

        /// <summary>
        /// 根据特定的供应商ID，查询采购组织ID
        /// </summary>
        /// <param name="supplierID"></param>
        /// <returns></returns>
        public DataTable queryPurchasingORG(String supplierID)
        {
            return (supplierPerformanceIDAL.queryPurchasingORG(supplierID));
        }

        public DataTable queryPurchasingORG()
        {
            return (supplierPerformanceIDAL.queryPurchasingORG());
        }

        /// <summary>
        /// 根据特定的供应商ID，查询物料ID
        /// </summary>
        /// <returns></returns>
        public DataTable queryMaterialID(String supplierID)
        {
            return (supplierPerformanceIDAL.queryMaterial(supplierID));
        }

        /// <summary>
        /// 查询无评估供应商清单
        /// </summary>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <returns></returns>
        public DataTable queryNoSupplierPerformanceList(DateTime startTime,DateTime endTime)
        {
            return (supplierPerformanceIDAL.queryNoSupplierPerformanceList(startTime, endTime));
        }

        /// <summary>
        /// 查询物料类型
        /// </summary>
        /// <param name="materialID"></param>
        /// <returns></returns>
        public String queryMaterialType(String materialID)
        {
            return (supplierPerformanceIDAL.queryMaterialType(materialID));
        }

        /// <summary>
        /// 查询某个物料类型对应的所有物料ID列表
        /// </summary>
        /// <param name="typeList"></param>
        /// <returns></returns>
        public DataTable queryMaterialList(String materialType)
        {
            DataTable materialList = supplierPerformanceIDAL.queryMaterialList(materialType);
            return materialList;
        }

        #endregion 

        #region 供应商分级

        /// <summary>
        /// 执行供应商分级
        /// </summary>
        /// <param name="supplierID"></param>
        /// <param name="year"></param>
        /// <returns></returns>
        public String executeSupplierClassify(String supplierID, DateTime start, DateTime end)
        {
            DataTable avgTotalScore;
            avgTotalScore = supplierPerformanceIDAL.queryTotalResult(supplierID, start,end);
            Double score = 0.0;
            String rst;

            //获取绿色目标值和最低目标值
            DataTable dt= supplierPerformanceIDAL.getPGreenValueAndPLowValue(supplierID);

            if (dt == null)
            {
                return "N";
            }
            else {
                float PGreenValue = (float)dt.Rows[0]["PGreenValue"];
                float PLowValue = (float)dt.Rows[0]["PLowValue"];
                //该datatable仅有一行
                foreach (DataRow row in avgTotalScore.Rows)
                {
                    try
                    {
                        score = Convert.ToDouble(row[0]);
                    }
                    catch
                    {
                        //表示无可用于分级的记录，返回N
                        return "N";
                    }
                }

                //分级
                if (score >= PGreenValue)
                {
                    rst = "A";
                }
                else if (score>= PLowValue)
                {
                    rst = "B";
                }
                else
                {
                    rst = "C";
                }
            }
            return rst;

        }

        /// <summary>
        /// 保存供应商分级结果
        /// </summary>
        /// <param name="saveClassifyRst"></param>
        /// <returns></returns>
        public int saveClassificationResult(List<Object> saveClassifyRst)
        {
            int lines = supplierPerformanceIDAL.saveClassificationResult(saveClassifyRst);
            return lines;
        }

        /// <summary>
        /// 查询供应商分级结果
        /// </summary>
        /// <param name="supplierID"></param>
        /// <param name="year"></param>
        /// <returns></returns>
        public DataTable queryClassificationResult(String supplierID, String year)
        {
            DataTable rst = supplierPerformanceIDAL.queryClassificationResult(supplierID, year);
            return rst;
        }

        /// <summary>
        /// 返回最新一次的分级结果
        /// </summary>
        /// <param name="supplierID"></param>
        /// <param name="year"></param>
        /// <returns></returns>
        public String queryNewClassificationResult(String supplierID, String timePeriod)
        {
            String rst = supplierPerformanceIDAL.queryNewClassificationResult(supplierID,timePeriod);
            return rst;
        }

        /// <summary>
        /// 批量查询供应商分级结果
        /// </summary>
        /// <param name="purchasingORGID"></param>
        /// <param name="category"></param>
        /// <param name="year"></param>
        /// <returns></returns>
        public DataTable queryClassificationGroupResult(String purchasingORGID, String category, String year)
        {
            DataTable rst = supplierPerformanceIDAL.queryClassificationGroupResult( purchasingORGID,  category, year);
            return rst;
        }

        #endregion

        #region 供应商批量评估
        
        /// <summary>
        /// 查询物料ID
        /// </summary>
        /// <returns></returns>
        public DataTable queryMaterial()
        {
            return (supplierPerformanceIDAL.queryMaterial());
        }

        /// <summary>
        /// 根据物料ID，查询有过交易的供应商ID
        /// </summary>
        /// <param name="materialID"></param>
        /// <returns></returns>
        public List<String> querySupplier(String materialID)
        {
            List<String> supplierIDList = new List<String>();
            DataTable supplierIDTable = supplierPerformanceIDAL.querySupplier(materialID);
            int count = supplierIDTable.Rows.Count;
            if(count <= 0)
            {
                return supplierIDList;
            }
            else
            {
                foreach(DataRow row in supplierIDTable.Rows)
                {
                    supplierIDList.Add(row[0].ToString());
                }
                return supplierIDList;
            }
        }

        /// <summary>
        /// 查询所有供应商类别
        /// </summary>
        /// <returns></returns>
        public DataTable queryCategory()
        {
            return (supplierPerformanceIDAL.queryCategory());
        }
        #endregion

        #region 数据录入

        /// <summary>
        /// 保存问卷结果，写入数据表GoodsReceiveSurvey_Score
        /// </summary>
        /// <param name="saveList"></param>
        /// <returns></returns>
        public int saveQuestionaire(List<Object> saveList)
        {
            int lines = supplierPerformanceIDAL.saveQuestionaire(saveList);
            return lines;
        }

        #endregion

        /// <summary>
        /// 查询供应商名称
        /// </summary>
        /// <param name="supplierID"></param>
        /// <returns></returns>
        public String querySupplierName(String supplierID)
        {
            return (supplierPerformanceIDAL.querySupplierName(supplierID));
        }

        /// <summary>
        /// 查看物料名称
        /// </summary>
        /// <param name="materialID"></param>
        /// <returns></returns>
        public String queryMaterialName(String materialID)
        { 
            return(supplierPerformanceIDAL.queryMaterialName(materialID));
        }

        /// <summary>
        /// 从Supplier_Performance表中查询ID对应的记录，相应的物料ID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public String queryMaterialIDFromSP(String ID)
        { 
            return(supplierPerformanceIDAL.queryMaterialIDFromSP(ID));
        }

        /// <summary>
        /// 从Supplier_Performance表中查询ID对应的记录，相应的供应商ID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public String querySupplierIDFromSP(String ID)
        {
            return (supplierPerformanceIDAL.querySupplierIDFromSP(ID));
        }

        /// <summary>
        /// 从Supplier_Performance表中查询ID对应的记录，相应的采购组织ID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public String queryPurchasingORGIDFromSP(String ID)
        {
            return (supplierPerformanceIDAL.queryPurchasingORGIDFromSP(ID));
        }
    }
}
#endregion
#endregion