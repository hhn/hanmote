﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using Lib.IDAL.SupplierPerformaceIDAL;
using DALFactory;
using Lib.ContionSettings.SupplierPerformaceCS.SPReportCS;

namespace Lib.Bll.SupplierPerformaceBLL
{
    public class SPR_SPCompareBOMGBLL
    {

        private static readonly SPR_SPCompareBOMGIDAL sPR_SPCompareBOMGIDAL = DALFactoryHelper.CreateNewInstance<SPR_SPCompareBOMGIDAL>("SPR_SPCompareBOMGDAL");

        /// <summary>
        /// 获取所有采购组织信息
        /// </summary>
        /// <returns></returns>
        public DataTable getAllBuyerOrgInfo()
        {
            return sPR_SPCompareBOMGIDAL.getAllBuyerOrgInfo();
        }

        /// <summary>
        /// 取得物料组信息
        /// </summary>
        /// <param name="selectSupplierConditionSettings">查询条件</param>
        /// <returns></returns>
        public DataTable getMaterialGroupInfo(SelectSupplierConditionSettings selectSupplierConditionSettings)
        {
            return sPR_SPCompareBOMGIDAL.getMaterialGroupInfo(selectSupplierConditionSettings);
        }

        /// <summary>
        /// 基于物料组的供应商评估比较
        /// </summary>
        /// <param name="spcCondition">查询条件</param>
        /// <returns></returns>
        public  DataTable getEvaluationSupplierScore(SPCompareBaseOnMGConditionValue spcCondition)
        {
            return sPR_SPCompareBOMGIDAL.getEvaluationSupplierScore(spcCondition);
        }

    }
}
