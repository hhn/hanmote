﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using Lib.IDAL.SupplierPerformaceIDAL;
using Lib.Model.SPReportModel;
using Lib.ContionSettings.SupplierPerformaceCS.SPReportCS;
using DALFactory;

namespace Lib.Bll.SupplierPerformaceBLL
{
    public class SPR_SPCompareBubbleCharBLL
    {
        private static readonly SPR_SPCompareBubbleChartIDAL sPR_SPCompareBubbleChartIDAL = DALFactoryHelper.CreateNewInstance<SPR_SPCompareBubbleChartIDAL>("SPR_SPCompareBubbleCharDAL");

        /// <summary>
        /// 查询供应商列表
        /// 条件
        /// 采购组织ID、年度、时段
        /// </summary>
        /// <param name="selectSupplierConditionSettings"></param>
        /// <returns></returns>
        public DataTable getAllSupplierInfo(SelectSupplierConditionSettings selectSupplierConditionSettings)
        {
            return sPR_SPCompareBubbleChartIDAL.getAllSupplierInfo(selectSupplierConditionSettings);
        }
    }
}
