﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DALFactory;
using System.Windows.Forms;
using Lib.IDAL.SupplierPerformaceIDAL;
using Lib.Model.SupplierManagementModel;

namespace Lib.Bll.SupplierPerformaceBLL
{
    public class SupplierValueBLL
    {
        private static readonly SupplierValueIDAL businessValueIDAL = DALFactoryHelper.CreateNewInstance<SupplierValueIDAL>("SupplierPerformaceDAL.SupplierValueDAL");

        #region 业务价值

        /// <summary>
        /// 查询 采购方的采购业务额
        /// </summary>
        /// <param name="supplierID"></param>
        /// <param name="year"></param>
        /// <returns></returns>
        public Decimal queryPurchasingTurnover(String supplierID, String year)
        {
            DataTable turnoverList = businessValueIDAL.queryPurchasingTurnover(supplierID, year);
            if (turnoverList.Rows.Count > 0)
            {
                Decimal sum = 0M;
                foreach (DataRow row in turnoverList.Rows)
                {
                    try
                    {
                        sum = sum + Convert.ToDecimal(row[0]);
                    }
                    catch
                    {
                        return -1;
                    }
                }
                return sum;
            }
            else
            {
                //返回-1表示当前暂时无数据
                return -1;
            }

        }

        public DataTable getExsitStrategyBySupplierId(string supplierId)
        {
            return businessValueIDAL.getExsitStrategyBySupplierId(supplierId);

        }

        /// <summary>
        /// 获取策略类型
        /// </summary>
        /// <returns></returns>
        public DataTable getStrategyType()
        {
            return businessValueIDAL.getStrategyType();

        }

        public DataTable getStrategyInfo(string levelType)
        {
            return businessValueIDAL.getStrategyInfo(levelType);
        }

        //TODO：获取评估结果
        public DataTable getClassifyResult(conditionInfo conditionInfo)
        {
          return businessValueIDAL.queryClassifyResult(conditionInfo);
        }
        /// <summary>
        /// 获取记录条数
        /// </summary>
        /// <param name="conditionInfo"></param>
        /// <returns></returns>
        public int getRecordCount(conditionInfo conditionInfo)
        {
            return businessValueIDAL.queryRecordCount(conditionInfo);
        }

        public bool delExsitedStrategyInfo(string supplierId)
        {
            return businessValueIDAL.delExsitedStrategyInfo(supplierId);
        }

        /// <summary>
        /// 保存策略信息
        /// </summary>
        /// <param name="strategyId"></param>
        /// <param name="supplierId"></param>
        /// <returns></returns>
        public bool insertStategyInfo(string strategyId, string supplierId)
        {
            return businessValueIDAL.insertStategyInfo(strategyId, supplierId);

        }

        /// <summary>
        /// 查询供应商全年营业额
        /// </summary>
        /// <param name="supplier"></param>
        /// <param name="year"></param>
        /// <returns></returns>
        public Decimal querySupplierTurnover(String supplierID, String year)
        {
            DataTable turnoverList = businessValueIDAL.querySupplierTurnover(supplierID, year);
            if (turnoverList.Rows.Count > 0)
            {
                Decimal turnover = 0M;
                foreach (DataRow row in turnoverList.Rows)
                {
                    try
                    {
                        turnover = turnover + Convert.ToDecimal(row[0]);
                    }
                    catch
                    {
                        return -1;
                    }

                }
                return turnover;
            }
            else
            {
                //返回负数表示没有数据
                return -1M;
            }
        }

        /// <summary>
        /// 定位公司业务价值，返回H\M\L\N
        /// </summary>
        /// <param name="portion"></param>
        /// <returns></returns>
        public String positionCompanyPortion(Decimal portion)
        {
            Double portionScore = Convert.ToDouble(portion);
            if (portionScore >= 0.15)
            {
                return "H";
            }
            else if ((portionScore < 0.15) && (portionScore >= 0.05))
            {
                return "M";
            }
            else if ((portionScore < 0.05) && (portionScore >= 0.008))
            {
                return "L";
            }
            else
            {
                return "N";
            }
        }

        /// <summary>
        /// 定位公司吸引力水平，返回H\M\L\N
        /// </summary>
        /// <param name="appeal"></param>
        /// <returns></returns>
        public String positionAppealLevel(Decimal appeal)
        {
            Double appealScore = Convert.ToDouble(appeal);

            if (appealScore >= 8)
            {
                return "H";
            }
            else if ((appealScore < 8) && (appealScore >= 4))
            {
                return "M";
            }
            else if ((appealScore < 4) && (appealScore >= 0))
            {
                return "L";
            }
            else
            {
                return "N";
            }
        }

        /// <summary>
        /// 供应商定位
        /// </summary>
        /// <param name="turnover"></param>
        /// <param name="appeal"></param>
        /// <returns></returns>
        public String positionSupplier(String turnoverRst, String appealRst)
        {
            String rst = "";
            if ((turnoverRst == "N") || (turnoverRst == "L"))
            {
                if ((appealRst == "H") || (appealRst == "M"))
                {
                    rst = "发展型";
                }
                else if ((appealRst == "L") || (appealRst == "N"))
                {
                    rst = "边缘型";
                }
                else
                {
                    return "";
                }
            }
            else if ((turnoverRst == "M") || (turnoverRst == "H"))
            {
                if ((appealRst == "H") || (appealRst == "M"))
                {
                    rst = "核心型";
                }
                else if ((appealRst == "L") || (appealRst == "N"))
                {
                    rst = "盘剥型";
                }
                else
                {
                    return "";
                }
            }
            return rst;

        }

        /// <summary>
        /// 保存业务价值相关字段 保存到数据表Supplier_Business_Value中
        /// </summary>
        /// <param name="savePara"></param>
        /// <returns></returns>
        public int saveBusinessValue(List<Object> savePara)
        {
            int lines = businessValueIDAL.saveBusinessValue(savePara);
            return lines;
        }

        #endregion

        #region 品项定位结果查询

        /// <summary>
        /// 查询品项定位结果
        /// </summary>
        /// <param name="supplierID"></param>
        /// <returns></returns>
        public String querySupplyType(String supplierID)
        {
            String rst = businessValueIDAL.querySupplyType(supplierID);
            return rst;
        }

        #endregion

        #region 供应商细分

        /// <summary>
        /// 供应商细分
        /// </summary>
        /// <param name="supplierPosition"></param>
        /// <param name="supplyType"></param>
        /// <returns></returns>
        public String segmentSupplier(String supplierPosition, String supplyType)
        {
            String rst = "101";
            if ((supplierPosition == "边缘型") || (supplyType == "一般"))
            {
                rst = "T";
            }
            else if (supplierPosition == "盘剥型")
            {
                if (supplyType == "关键")
                {
                    rst = "C";
                }
                else if ((supplyType == "瓶颈") || (supplyType == "杠杆"))
                {
                    rst = "T";
                }
            }
            else if (supplierPosition == "发展型")
            {
                if (supplyType == "杠杆")
                {
                    rst = "T";
                }
                else if ((supplyType == "瓶颈") || (supplyType == "关键"))
                {
                    rst = "C";
                }
            }
            else if (supplierPosition == "核心型")
            {
                if (supplyType == "关键")
                {
                    rst = "S";
                }
                else if ((supplyType == "瓶颈") || (supplyType == "杠杆"))
                {
                    rst = "C";
                }
            }

            return rst;
        }

        #endregion

        #region 供应商区分

        /// <summary>
        /// 供应商区分 （供应商定位最终结果）
        /// </summary>
        /// <param name="classifyRst"></param>
        /// <param name="supplierRelationship"></param>
        /// <returns></returns>
        public String supplierPositionRst(String classifyRst, String supplierRelationship)
        {
            String rst = "101";
            if (classifyRst == "A")
            {
                if (supplierRelationship == "S")
                {
                    rst = "A";
                }
                else if ((supplierRelationship == "C") || (supplierRelationship == "T"))
                {
                    rst = "B";
                }
            }
            else if (classifyRst == "B")
            {
                rst = "B";
            }
            else if ((classifyRst == "C") || (classifyRst == "D"))
            {
                if (supplierRelationship == "S")
                {
                    rst = "C";
                }
                else if ((supplierRelationship == "C") || (supplierRelationship == "T"))
                {
                    rst = "D";
                }
            }

            return rst;
        }

        /// <summary>
        /// 保存供应商区分结果 Supplier_Position
        /// </summary>
        /// <param name="savePara"></param>
        /// <returns></returns>
        public int saveSupplierPosition(List<Object> savePara)
        {
            int lines = businessValueIDAL.saveSupplierPosition(savePara);
            return lines;
        }
        #endregion

        #region 供应商状态跟踪

        /// <summary>
        /// 当前年份所有供应商状态查询
        /// </summary>
        /// <param name="year"></param>
        /// <returns></returns>
        public DataTable querySupplierState(String year, String industry)
        {
            DataTable rst = businessValueIDAL.querySupplierState(year, industry);
            return rst;
        }

        /// <summary>
        /// 修改供应商状态，数据表Supplier_Base
        /// </summary>
        /// <param name="supplierID"></param>
        /// <param name="state"></param>
        /// <returns></returns>
        public int changeSupplierState(String supplierID, String state)
        {
            int lines = businessValueIDAL.changeSupplierState(supplierID, state);
            return lines;
        }

        /// <summary>
        /// 查询行业类别，Supplier_Base数据表中的Industry_Category字段
        /// </summary>
        /// <returns></returns>
        public DataTable queryIndustry()
        {
            return (businessValueIDAL.queryIndustry());
        }

        /// <summary>
        /// 保存供应商营业额
        /// </summary>
        /// <param name="supplierID"></param>
        /// <param name="year"></param>
        /// <param name="turnover"></param>
        /// <returns></returns>
        public int saveSupplierTurnover(String supplierID, String year, String turnover)
        {
            int lines = businessValueIDAL.saveSupplierTurnover(supplierID, year, turnover);
            return lines;
        }
        /// <summary>
        /// 查询采购组织
        /// </summary>
        /// <returns></returns>
        public List<String> getPurOrg()
        {
            DataTable dt = businessValueIDAL.getPurOrg();
            List<String> purOrg = new List<string>();
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    purOrg.Add(dt.Rows[i][0].ToString());
                }
            }
            return purOrg;
        }
        #endregion
    }
}
