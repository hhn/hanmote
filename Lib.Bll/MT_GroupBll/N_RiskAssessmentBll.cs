﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Model.MT_GroupModel;
using Lib.SqlServerDAL.MT_GroupDAL;
/// <summary>
/// 物料定位业务逻辑类
/// </summary>

namespace Lib.Bll.MT_GroupBll
{

    public class N_RiskAssessmentBll
    {

        N_RiskAssDAL n_RiskAssDAL = new N_RiskAssDAL();
        public DataTable GetDefinedMt_Group(String purOrgName) {
            
            return n_RiskAssDAL.GetDefinedGroup(purOrgName);
        }

        public DataTable getPurCostAndItem(string purName)
        {
            return n_RiskAssDAL.getPurCostAndItem(purName);
        }

        /// <summary>
        /// 获取评分信息
        /// </summary>
        /// <param name="id">物料组id</param>
        /// <param name="purOrgName">采购组名称</param>
        /// <returns></returns>
        public DataTable getMtGroupData(String id,String purOrgName)
        {
            return n_RiskAssDAL.getMtGroupData(id, purOrgName);
        }

        public DataTable getMtInfo(string purOrgId, string  mtName)
        {
            return n_RiskAssDAL.getMtInfo(purOrgId,mtName);
        }

        /// <summary>
        /// 获取导入的数据
        /// </summary>
        /// <param name="mtId"></param>
        /// <param name="purOrgName"></param>
        /// <returns></returns>
        public DataTable getMtGroupImportedData(string purOrgName)
        {
            return n_RiskAssDAL.getMtGroupImportedData(purOrgName);
        }

        /// <summary>
        /// 插入分数信息
        /// </summary>
        /// <param name="mtItemScoreInfoModle"></param>
        /// <returns></returns>
        public Boolean updateSingleMtSubScoreInfo(MtItemScoreInfoModle mtItemScoreInfoModle)
        {
            return n_RiskAssDAL.updateMtItemScoreInfo(mtItemScoreInfoModle);
        }

        public DataTable getMtGroupFinishData(string id,string purOrgName)
        {
            return n_RiskAssDAL.getMtGroupFinishData(id, purOrgName);
        }

        public DataTable getMtGroupResultData(string id, string mtGroupID)
        {
           return  n_RiskAssDAL.getMtGroupResultData(id, mtGroupID);
        }
        /// <summary>
        /// 获取物料评估项
        /// </summary>
        /// <param name="id">物料编号</param>
        /// <param name="purOrgId">采购组织编号</param>
        /// /// <param name="type"></param>
        /// <returns></returns>
        public DataTable getMtItemInfo(string id, string purOrgId,string type)
        {
            return n_RiskAssDAL.getMtItemInfo(id, purOrgId, type);

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="resultPosScore">定位得分</param>
        /// <param name="resultLevel">定位等级</param>
        /// <param name="id">定位物料组id</param>
        /// <param name="purOrgName">物料组所属采购部名称</param>
        /// <returns></returns>
        public int insertIntoPosResInfo(double resultPosScore, string resultLevel, string id, string purOrgName,string riskScore, string influenceScore,string mtName,float chanceScore)
        {
            return n_RiskAssDAL.insertIntoPosResInfo(resultPosScore, resultLevel,id,purOrgName, riskScore, influenceScore,mtName, chanceScore);
        }

        public DataTable getMtGroupFinished(String purOrgName)
        {
            return n_RiskAssDAL.getMtGroupFinished(purOrgName);
        }


        /// <summary>
        /// 保存物料组最后评估结果
        /// </summary>
        /// <param name="posResultInfoModel"></param>
        /// <returns></returns>
        public int insertLastPosResultInfo(PosResultInfoModel posResultInfoModel)
        {
            return n_RiskAssDAL.insertLastPosResultInfo(posResultInfoModel);
        }

        public DataTable mtGroupSearch(string mtName, string purOrgName)
        {
            return n_RiskAssDAL.mtGroupSearch(mtName, purOrgName);
        }

        public DataTable SearchMtGroupByName(string mtName,string purOrgId)
        {
            return n_RiskAssDAL.SearchMtGroupByName(mtName, purOrgId);
        }
        /// <summary>
        /// 根据采购组织编号加载物料组信息
        /// </summary>
        /// <param name="purOrgId">采购组织编号</param>
        /// <returns></returns>
        public DataTable getMtGroupInfo(string purOrgId)
        {
            return n_RiskAssDAL.getMtGroupInfo(purOrgId);

        }

        public int getIndicateCount(string v)
        {
            //TODO：待实现
            return 10;
        }

        public string getQualityClass(string purOrgId)
        {
            return n_RiskAssDAL.getQualityClass(purOrgId);
        }
    }
}
