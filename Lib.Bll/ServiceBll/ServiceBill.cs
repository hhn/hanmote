﻿using Lib.Common.MMCException.Bll;
using Lib.Common.MMCException.IDAL;
using Lib.IDAL.ServiceIDAL;
using Lib.Model.CommonModel;
using Lib.Model.ServiceEvaluation;
using Lib.Model.ServiceModel;
using Lib.SqlServerDAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

namespace Lib.Bll.ServiceBll
{
    public class ServiceBill : ServiceIDAL
    {
        private ServiceSqlIDAL serviceSqlIDAL=new ServiceSqlIDAL();

        /// <summary>
        /// 更新询价单
        /// </summary>
        /// <param name="inqueryPrice"></param>
        public void UpdateInqueryPrice(InqueryPrice inqueryPrice)
        {
            serviceSqlIDAL.UpdateInqueryPrice(inqueryPrice);
        }

        /// <summary>
        /// 根据物料Id获取审核物料信息
        /// </summary>
        /// <param name="materialId"></param>
        /// <returns></returns>
        public ApproveMaterial GetApproveMaterialByMaterialId(string materialId)
        {
            DataTable dt = serviceSqlIDAL.FindApproveMaterialByMaterialId(materialId);
            if(dt != null && dt.Rows.Count > 0)
            {
                ApproveMaterial approveMaterial = new ApproveMaterial();
                approveMaterial.Material_Id = dt.Rows[0]["Material_Id"].ToString();
                approveMaterial.Verify = Convert.ToInt32(dt.Rows[0]["Verify"].ToString());
                approveMaterial.Approve_Id = dt.Rows[0]["Approve_Id"].ToString();
                approveMaterial.Verify_Id1 = dt.Rows[0]["Verify_Id1"].ToString();
                approveMaterial.Verify_Id2 = dt.Rows[0]["Verify_Id2"].ToString();
                approveMaterial.Material_Location = dt.Rows[0]["Material_Location"].ToString();
                approveMaterial.Verify_Advice1 = dt.Rows[0]["Verify_Advice1"].ToString();
                approveMaterial.Verify_Advice2 = dt.Rows[0]["Verify_Advice2"].ToString();
                approveMaterial.Approve = dt.Rows[0]["Approve"].ToString();
                approveMaterial.Verify_Result1 = dt.Rows[0]["Verify_Result1"].ToString();
                approveMaterial.Verify_Result2 = dt.Rows[0]["Verify_Result2"].ToString();
                return approveMaterial;
            }
            return null;
        }

        /// <summary>
        /// 根据物料Id获取询价信息
        /// </summary>
        /// <param name="materialId"></param>
        /// <returns></returns>
        public DataTable GetInqueryPriceByMaterialId(string materialId)
        {
            DataTable dt = GetSourceMaterialByMaterialId(materialId);
            if(dt != null && dt.Rows.Count > 0)
            {
                string SourceId = dt.Rows[0]["Source_ID"].ToString();
                return serviceSqlIDAL.FindInqueryPriceById(SourceId);
            }
            return null;
        }

        /// <summary>
        /// 根据物料Id查询寻源物料信息
        /// </summary>
        /// <param name="materialId"></param>
        /// <returns></returns>
        public DataTable GetSourceMaterialByMaterialId(string materialId)
        {
            return serviceSqlIDAL.ListSourceMaterialByMaterialId(materialId);
        }

        /// <summary>
        /// 根据审核通过的次数和物料定位查询待处理的信息
        /// </summary>
        /// <param name="verify"></param>
        /// <param name="materialLocation"></param>
        /// <returns></returns>
        public string GetApproveMaterialByMaterialLocationAndVerify(int verify, string materialLocation,int approve)
        {
            DataTable dt = serviceSqlIDAL.ListApproveMaterialByMaterialLocationAndVerify(verify, materialLocation, approve);
            if(dt != null && dt.Rows.Count > 0)
            {
                return dt.Rows[0][0].ToString();
            }
            return null;
        }

        /// <summary>
        /// 根据角色Id获取角色名称
        /// </summary>
        /// <param name="roleId"></param>
        /// <returns></returns>
        public string GetRoleNameById(string roleId)
        {
            DataTable dt = serviceSqlIDAL.FindRoleNameById(roleId);
            if(dt != null && dt.Rows.Count > 0)
            {
                return dt.Rows[0][0].ToString();
            }
            return null;
        }

        /// <summary>
        /// 查询所有供应商
        /// </summary>
        /// <returns></returns>
        public DataTable GetAllSupplier()
        {
            return serviceSqlIDAL.ListAllSupplier();
        }

        /// <summary>
        /// 根据供应商Id列表查询所有供应商
        /// </summary>
        /// <param name="supplierList"></param>
        /// <returns></returns>
        public DataTable GetAllSupplierByList(List<string> supplierList)
        {
            if(supplierList != null)
            {
                List<DataTable> list = new List<DataTable>(supplierList.Count);
                foreach(string supplierId in supplierList)
                {
                    DataTable dt = serviceSqlIDAL.FindSupplierById(supplierId);
                    list.Add(dt);
                }
                if(list.Count > 0)
                {
                    return mergerDataTable(list);
                }
            }
            return null;
        }


        /// <summary>
        /// 根据采购组织Id获取该采购组织下所有采购组
        /// </summary>
        /// <param name="purOrgId"></param>
        /// <returns></returns>
        public List<string> GetAllBuyGroupByPurId(string purOrgId)
        {
            DataTable dt = serviceSqlIDAL.ListBuyGroupByOrgId(purOrgId);
            if(dt != null && dt.Rows.Count > 0)
            {
                List<string> list = new List<string>(dt.Rows.Count);
                for(int i = 0; i < dt.Rows.Count; i++)
                {
                    list.Add(dt.Rows[i][0].ToString());
                }
                return list;
            }
            return null;
        }

        /// <summary>
        /// 根据采购组织Id查询采购组织名称
        /// </summary>
        /// <param name="purOrgId"></param>
        /// <returns></returns>
        public string GetPurOrgNameById(string purOrgId)
        {
            DataTable dt = serviceSqlIDAL.FindPurOrgNameById(purOrgId);
            if(dt != null && dt.Rows.Count > 0)
            {
                return dt.Rows[0][0].ToString();
            }
            return null;
        }

        /// <summary>
        /// 根据工厂Id获取采购组织Id
        /// </summary>
        /// <param name="factoryId"></param>
        /// <returns></returns>
        public string GetPurOrgIdByFactoryId(string factoryId)
        {
            DataTable dt = serviceSqlIDAL.FindPurOrgIdByFactoryId(factoryId);
            if(dt != null && dt.Rows.Count > 0)
            {
                return dt.Rows[0][0].ToString();
            }
            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Demand_ID"></param>
        /// <returns></returns>
        public SummaryDemand GetSummaryDemandById(string Demand_ID)
        {
            DataTable dt = serviceSqlIDAL.GetSummaryDemandObjById(Demand_ID);
            return DataTalbeToEntity<SummaryDemand>(dt);
        }

        public static T DataTalbeToEntity<T>(DataTable dataTable)
        {
            T element = Activator.CreateInstance<T>();
            Type targetType = typeof(T);
            PropertyInfo[] allPropertyArray = targetType.GetProperties();
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                DataRow rowElement = dataTable.Rows[0];
                foreach (DataColumn columnElement in dataTable.Columns)
                {
                    foreach (PropertyInfo property in allPropertyArray)
                    {
                        if (property.Name.Equals(columnElement.ColumnName))
                        {
                            if (rowElement[columnElement.ColumnName] == DBNull.Value)
                            {
                                property.SetValue(element, null, null);
                            }
                            else
                            {
                                property.SetValue(element, rowElement
                                [columnElement.ColumnName], null);
                            }
                        }
                    }
                }
            }
            else
            {
                return default(T);//返回null
            }
            return element;
        }

        /// <summary>
        /// 获取对应ID的所有需求
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        public DataTable GetSummaryDemandByIdList(List<string> list)
        {
            if(list != null && list.Count > 0)
            {
                List<DataTable> dataTableList = new List<DataTable>();
                foreach(string demandId in list)
                {
                    DataTable dt = serviceSqlIDAL.GetSummaryDemandById(demandId);
                    dataTableList.Add(dt);
                }
                return mergerDataTable(dataTableList);
            }
            return null;
        }

        //合并DataTable
        private DataTable mergerDataTable(List<DataTable> list)
        {
            DataTable newDataTable = null;
            foreach (DataTable dataTable in list)
            {
                if (newDataTable == null)
                {
                    newDataTable = dataTable.Clone();
                }
                object[] obj = new object[dataTable.Columns.Count];
                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    //将object[]所有元素置空
                    this.SetNullToArr(obj);
                    dataTable.Rows[i].ItemArray.CopyTo(obj, 0);
                    newDataTable.Rows.Add(obj);
                }
            }
            return newDataTable;
        }

        /// <summary>
        /// 获取所有需求
        /// </summary>
        /// <returns></returns>
        public DataTable GetSummaryDemandByState(string state)
        {
            return serviceSqlIDAL.ListSummaryDemandByState(state);
        }

        /// <summary>
        /// 获取所有需求计划的状态
        /// </summary>
        /// <returns></returns>
        public List<string> GetAllState()
        {
            DataTable dt = serviceSqlIDAL.ListAllState();
            if(dt != null && dt.Rows.Count > 0)
            {
                List<string> list = new List<string>(dt.Rows.Count);
                for(int i = 0;i < dt.Rows.Count; i++)
                {
                    string state = dt.Rows[i][0].ToString();
                    list.Add(state);
                }
                return list;
            }
            return null;
        }

        /// <summary>
        /// 根据询价单号查询该询价下的所有物料
        /// </summary>
        /// <param name=""></param>
        /// <returns></returns>
        public List<string> GetAllMaterialIdByInqueryPriceId(string inqueryPriceId)
        {
            DataTable dt = serviceSqlIDAL.ListMaterialByInqueryPriceId(inqueryPriceId);
            if(dt != null && dt.Rows.Count > 0)
            {
                List<string> list = new List<string>(dt.Rows.Count);
                for(int i = 0;i < dt.Rows.Count; i++)
                {
                    string Material_ID = dt.Rows[i]["Material_ID"].ToString();
                    list.Add(Material_ID);
                }
                return list;
            }
            return null;
        }

        /// <summary>
        /// 获取所有询价单id
        /// </summary>
        /// <returns></returns>
        public List<string> GetAllInqueryId()
        {
            DataTable dt = this.GetAllInqueryPriceInfo();
            if(dt != null && dt.Rows.Count > 0)
            {
                List<string> list = new List<string>(dt.Rows.Count);
                for(int i = 0;i < dt.Rows.Count; i++)
                {
                    string Source_ID = dt.Rows[i]["Source_ID"].ToString();
                    list.Add(Source_ID);
                }
                return list;
            }
            return null;
        }

        /// <summary>
        /// 删除指定的报价单
        /// </summary>
        /// <param name="offerPrice"></param>
        public void RemoveOfferPrice(OfferPrice offerPrice)
        {
            serviceSqlIDAL.DeleteOfferPrice(offerPrice);
        }

        /// <summary>
        /// 根据物料编码模糊查询报价单信息
        /// </summary>
        /// <param name="materialId"></param>
        /// <returns></returns>
        public DataTable GetOfferPriceLikeMaterialId(string materialId)
        {
            return serviceSqlIDAL.ListOfferPriceLikeMaterialId(materialId);
        }

        /// <summary>
        /// 获取所有报价信息
        /// </summary>
        /// <returns></returns>
        public DataTable GetAllOfferPrice()
        {
            return serviceSqlIDAL.ListAllOfferPrice();
        }

        /// <summary>
        /// 根据询价单号和物料ID查询寻源物料
        /// </summary>
        /// <param name="Material_ID"></param>
        /// <param name="Source_ID"></param>
        /// <returns></returns>
        public SourceMaterial GetSourceMaterialByMaterialIdAndInqueryId(string Material_ID, string InqueryId)
        {
            DataTable dt = serviceSqlIDAL.FindSourceMaterialByMaterialIdAndInqueryId(Material_ID, InqueryId);
            if(dt != null && dt.Rows.Count > 0)
            {
                SourceMaterial sourceMaterial = new SourceMaterial();
                DataRow dataRow = dt.Rows[0];
                sourceMaterial.Material_ID = dataRow["Material_ID"].ToString();
                sourceMaterial.Material_Name = dataRow["Material_Name"].ToString();
                sourceMaterial.Demand_Count = Convert.ToInt32(dataRow["Demand_Count"].ToString());
                sourceMaterial.Demand_ID = dataRow["Demand_ID"].ToString();
                sourceMaterial.Unit = dataRow["Unit"].ToString();
                sourceMaterial.Factory_ID = dataRow["Factory_ID"].ToString();
                sourceMaterial.Stock_ID = dataRow["Stock_ID"].ToString();
                sourceMaterial.Source_ID = dataRow["Source_ID"].ToString();
                return sourceMaterial;
            }
            return null;
        }

        /// <summary>
        /// 根据询价单号获取该询价单号下的所有物料信息
        /// </summary>
        /// <param name="Inquery_ID"></param>
        /// <returns></returns>
        public DataTable GetSourceMaterialByInqueryId(string Inquery_ID)
        {
            DataTable sourceMaterialDt = serviceSqlIDAL.FindSourceMaterialByInqueryId(Inquery_ID);
            if(sourceMaterialDt != null && sourceMaterialDt.Rows.Count > 0)
            {
                ISet<string> materialSet = new HashSet<string>();
                for(int i = 0; i < sourceMaterialDt.Rows.Count; i++)
                {
                    string Material_ID = sourceMaterialDt.Rows[i]["Material_ID"].ToString();
                    materialSet.Add(Material_ID);
                }
                return GetMaterialByMaterialIdList(materialSet);
            }
            return null;
        }

        /// <summary>
        /// 根据询价单ID查询询价单基本信息
        /// </summary>
        /// <param name="Inquery_ID"></param>
        /// <returns></returns>
        public InqueryPrice GetInqueryPriceById(string Inquery_ID)
        {
            DataTable dt = serviceSqlIDAL.FindInqueryPriceById(Inquery_ID);
            if(dt != null && dt.Rows.Count > 0)
            {
                DataRow dataRow = dt.Rows[0];
                InqueryPrice inqueryPrice = new InqueryPrice();
                inqueryPrice.Source_ID = dataRow["Source_ID"].ToString();
                inqueryPrice.Purchase_Org = dataRow["Purchase_Org"].ToString();
                inqueryPrice.Buyer_Name = dataRow["Buyer_Name"].ToString();
                inqueryPrice.CreateTime = Convert.ToDateTime(dataRow["CreateTime"].ToString());
                inqueryPrice.Offer_Time = Convert.ToDateTime(dataRow["Offer_Time"].ToString());
                inqueryPrice.State = dataRow["State"].ToString();
                inqueryPrice.Purchase_Group = dataRow["Purchase_Group"].ToString();
                inqueryPrice.Inquiry_Name = dataRow["Inquiry_Name"].ToString();
                inqueryPrice.Material_Location = dataRow["Material_Location"].ToString();
                inqueryPrice.Approve_Count = Convert.ToInt32(dataRow["Approve_Count"].ToString());
                return inqueryPrice;
            }
            return null;
        }

        /// <summary>
        /// 根据询价单名称模糊查询询价单
        /// </summary>
        /// <param name="Inquiry_Name"></param>
        /// <returns></returns>
        public DataTable GetInqueryPriceLikeName(string Inquiry_Name)
        {
            return serviceSqlIDAL.FindInqueryPriceLikeName(Inquiry_Name);
        }

        /// <summary>
        /// 保存寻源物料
        /// </summary>
        /// <param name="sourceMaterial"></param>
        public void SaveSourceMaterial(SourceMaterial sourceMaterial)
        {
            try
            {
                serviceSqlIDAL.InsertSourceMaterial(sourceMaterial);
            }
            catch (DBException e)
            {
                throw new BllException("保存寻源物料失败！",e);
            }
        }

        /// <summary>
        /// 获取所有询价单信息
        /// </summary>
        /// <returns></returns>
        public DataTable GetAllInqueryPriceInfo()
        {
            try
            {
                return serviceSqlIDAL.ListAllInqueryPriceInfo();
            }
            catch (DBException e)
            {
                throw new BllException("查询询价单失败！", e);
            }
        }

        /// <summary>
        /// 根据询价单状态，审核次数，物料定位获取所有询价单
        /// </summary>
        /// <param name="state"></param>
        /// <returns></returns>
        public DataTable GetInqueryPriceByState(string state , int approveCount , string materialLocation)
        {
            try
            {
                return serviceSqlIDAL.ListInqueryPriceByState(state, approveCount ,materialLocation);
            }
            catch (DBException e)
            {
                throw new BllException("根据指定询价单状态获取相应询价单失败！",e);
            }
        }

        /// <summary>
        /// 保存询价记录
        /// </summary>
        /// <param name="inqueryPrice"></param>
        /// <returns></returns>
        public void SaveInqueryPrice(InqueryPrice inqueryPrice)
        {
            try
            {
                serviceSqlIDAL.InsertInqueryPrice(inqueryPrice);
            }
            catch(DBException e)
            {
                throw new BllException("创建询价失败！", e);
            }
        }

        /// <summary>
        /// 根据供应商id列表获取对应供应商信息
        /// </summary>
        /// <param name="gysbhSet"></param>
        /// <returns></returns>
        public DataTable GetSupplierBySupplierIdList(ISet<string> gysbhSet)
        {
            try
            {
                if (gysbhSet != null && gysbhSet.Count > 0)
                {
                    DataTable newDataTable = null;
                    foreach (string gysbh in gysbhSet)
                    {
                        DataTable dt = serviceSqlIDAL.GetSupplierBySupplierId(gysbh);
                        if (newDataTable == null)
                        {
                            newDataTable = dt.Clone();
                        }
                        object[] obj = new object[dt.Columns.Count];
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            //将object[]所有元素置空
                            this.SetNullToArr(obj);
                            dt.Rows[i].ItemArray.CopyTo(obj, 0);
                            newDataTable.Rows.Add(obj);
                        }
                    }
                    return newDataTable;
                }
                return null;
            }
            catch (DBException ex)
            {
                throw new BllException("根据供应商id列表获取对应供应商信息失败！",ex);
            }
        }

        //将object[]置空
        private void SetNullToArr(object[] obj)
        {
            if(obj != null && obj.Length > 0)
            {
                for(int i = 0; i < obj.Length; i++)
                {
                    obj[i] = null;
                }
            }
        }

        /// <summary>
        /// 根据物料id列表获取对应物料信息
        /// </summary>
        /// <param name="wlbhItems"></param>
        /// <returns></returns>
        public DataTable GetMaterialByMaterialIdList(ISet<string> wlbhItems)
        {
            try
            {
                if (wlbhItems != null && wlbhItems.Count > 0)
                {
                    DataTable newDataTable = null;
                    foreach (string materialId in wlbhItems)
                    {
                        DataTable dt = serviceSqlIDAL.GetMaterialByMaterialId(materialId);
                        if(newDataTable == null)
                        {
                            newDataTable = dt.Clone();
                        }
                        object[] obj = new object[dt.Columns.Count];
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            this.SetNullToArr(obj);
                            dt.Rows[i].ItemArray.CopyTo(obj, 0);
                            newDataTable.Rows.Add(obj);
                        }
                    }
                    return newDataTable;
                }
                return null;
            }
            catch (DBException ex)
            {
                throw new BllException("根据物料编码列表获取物料信息列表失败！", ex);
            }
        }

        /// <summary>
        /// 获取供应商编号
        /// </summary>
        /// <param name="loginId"></param>
        /// <returns></returns>
        public DataTable getSupplierId(string loginId)
        {
            return serviceSqlIDAL.getSupplierId(loginId);
        }

        /// <summary>
        /// 获取所有物料信息
        /// </summary>
        /// <returns></returns>
        public DataTable GetAllMaterialInfo()
        {
            try
            {
                return serviceSqlIDAL.GetAllMaterialInfo();
            }
            catch (DBException ex)
            {
                throw new BllException("获取物料信息失败！",ex);
            }
        }

        /// <summary>
        /// 根据相应条件查询市场价格记录信息
        /// </summary>
        /// <param name="PurGroupName"></param>
        /// <param name="MtGroupName"></param>
        /// <param name="BusType"></param>
        /// <returns></returns>
        public DataTable GetMarketPrice(string PurGroupName, string MtGroupName, string BusType)
        {
            return serviceSqlIDAL.GetMarketPrice(PurGroupName, MtGroupName, BusType);
        }

        public List<string> getGenMateName(string text1, string text2)
        {
            return serviceSqlIDAL.getGenMateName(text1, text2);
        }

        public List<string> getGenMateGroupName(string text)
        {
            return serviceSqlIDAL.getGenMateGroupName(text);
        }

        public List<string> getGenSupplierName()
        {
            return serviceSqlIDAL.getGenSupplierName();
        }

        public List<string> getMateName(string supplierName, string mateGroupName)
        {
            return serviceSqlIDAL.getMateName(supplierName, mateGroupName);
        }

        public DataTable getExServiceData(string supplierName, string MtGroupName, string mtName,string startTime,string endTime)
        {
            return serviceSqlIDAL.getExServiceData(supplierName, MtGroupName, mtName, startTime, endTime);
        }

        public List<string> getMateGroupName(string supplierName)
        {
            return serviceSqlIDAL.getMateGroupName(supplierName);
        }

        public DataTable getGenServiceData(string supplierName, string mtGroupName, string mtName, string startTime, string endTime)
        {
            return serviceSqlIDAL.getGenServiceData(supplierName, mtGroupName, mtName, startTime, endTime);
        }

        public List<string> getSupplierName()
        {
            return serviceSqlIDAL.getSupplierName();
        }

        public DataTable getGeneServiceRate(string mtGroupId)
        {
            return serviceSqlIDAL.getGeneServiceRate(mtGroupId);
        }

        public DataTable getExServiceRate(string mtGroupId)
        {
            return serviceSqlIDAL.getExServiceRate(mtGroupId);
        }

        public DataTable getServiceRate()
        {
            return serviceSqlIDAL.getServiceRate();
        }

        public DataTable getAllMtGroupidAndName()
        {
            return serviceSqlIDAL.getAllMtGroupidAndName();
        }

        public DataTable getServiceRate(string MTGName)
        {
            return serviceSqlIDAL.getServiceRate(MTGName);
        }

        public bool insertServiceRate(DataGridView mtGroupRate)
        {
            return serviceSqlIDAL.insertServiceRate(mtGroupRate);
        }

        public DataTable getGenServiceData()
        {
            return serviceSqlIDAL.getGenServiceData();
        }

        public DataTable getExServiceData()
        {
            return  serviceSqlIDAL.getExServiceData();
        }

        public Boolean insertServiceModel(string sQs, string sTs, string modelName)
        {
            return serviceSqlIDAL.insertServiceModel(sQs,sTs, modelName);
        }

        public bool insertGenServiceModel(string innos, string res, string uSs, string modelName)
        {
            return serviceSqlIDAL.insertGenServiceModel(innos, res, uSs, modelName);
        }

        public List<String> getGenServiceModelName()
        {
            return serviceSqlIDAL.getGenServiceModelName();
        }

        public DataTable getGenServiceModel(string Name)
        {
            return serviceSqlIDAL.getGenServiceModel(Name);
        }

        public Boolean insertServiceData(ServiceModel serviceData)
        {
            return serviceSqlIDAL.insertServiceData(serviceData);
        }

        public Boolean insertGenServiceData(GeneralServiceModle serviceData)
        {
            return serviceSqlIDAL.insertGenServiceData(serviceData);
        }

        public List<String> getServiceModelName()
        {
              return serviceSqlIDAL.getServiceModelName();
        }

        public DataTable getServiceModel(string Name)
        {
            return serviceSqlIDAL.getServiceModel(Name);
        }
       /// <summary>
       /// 
       /// </summary>
       /// <param name="MtGroupId">物料组编号</param>
       /// <param name="user_ID">供应商编号</param>
       /// <returns></returns>
        public DataTable getMtidAndName(string MtGroupId, string user_ID)
        {
            DataTable dt = serviceSqlIDAL.getMtidAndName(MtGroupId, user_ID);
            return dt;
        }
        //获取物料组编号
        public DataTable getMtGroupidAndName(string supplierId)
        {
            DataTable dt = serviceSqlIDAL.getMtGroupidAndName(supplierId);
            
            return dt;
        }

        public DataTable getSupplierIdByPurId( string text)
        {
           

            DataTable dt = serviceSqlIDAL.getSupplierIdByPurId(text);
            return dt;
        }

        Dictionary<string, string> ServiceIDAL.getSupplierId(string loginId)
        {
            throw new NotImplementedException();
        }
    }
}
