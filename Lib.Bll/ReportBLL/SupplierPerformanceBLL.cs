﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.SqlIDAL;
using DALFactory;
using System.Data;

namespace Lib.Bll
{
    public class SupplierPerformanceBLL
    {
        /// <summary>
        /// 取得该对象的实例
        /// </summary>
        public static readonly SupplierPerformanceIDAL sPerIDAL = DALFactoryHelper.CreateNewInstance<SupplierPerformanceIDAL>("SupplierPerformanceDAL");

        public DataTable GetSupplierPerformance(string purchaseOrg, string materialGroup)
        {
            return sPerIDAL.GetSupplierPerformance(purchaseOrg, materialGroup);
        }

    }
}
