﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Lib.SqlIDAL;
using DALFactory;
using Lib.Common.MMCException.IDAL;
using Lib.Common.MMCException.Bll;

namespace Lib.Bll
{
    public class BuyerOrganizationBLL
    {
        public static readonly BuyerOrganizationIDAL buyerOrgIDAL = DALFactoryHelper.CreateNewInstance<BuyerOrganizationIDAL>("BuyerOrganizationDAL");

        /// <summary>
        /// 获取所有采购组织编码
        /// </summary>
        /// <returns></returns>
        public List<string> GetAllBuyerOrgId()
        {
            try
            {
                DataTable dt = buyerOrgIDAL.GetAllBuyerOrganizationId();
                if (dt != null && dt.Rows.Count > 0)
                {
                    List<string> list = new List<string>(dt.Rows.Count);
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        list.Add(dt.Rows[i][0].ToString());
                    }
                    return list;
                }
                return null;
            }
            catch (DBException ex)
            {
                throw new BllException("查询采购组织编码列表失败！",ex);
            }
            
        }

        /// <summary>
        /// 获取所有采购组织名称
        /// </summary>
        /// <returns></returns>
        public DataTable GetAllBuyerOrgName()
        {
            try
            {
                return buyerOrgIDAL.GetAllBuyerOrganizationName();
            }
            catch (DBException ex)
            {
                throw new BllException("查询采购组织名称列表失败！",ex);
            }
        }


    }
}
