﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.SqlIDAL;
using DALFactory;
using Lib.Model;


namespace Lib.Bll
{
    public class SupplierBaseInformationBLL
    {
        /// <summary>
        /// 取得实例
        /// </summary>
        private static readonly SupplierBaseInformationIDAL sBaseInforIDAL = DALFactoryHelper.CreateNewInstance<SupplierBaseInformationIDAL>("SupplierBaseInformationDAL");

        /// <summary>
        /// 根据供应商商名称查找供应商信息
        /// </summary>
        /// <param name="SupplierName"></param>
        /// <returns></returns>
        public SupplierBaseInformation FindBySupplierName(string SupplierName)
        {
            return sBaseInforIDAL.FindBySupplierName(SupplierName);
        }

        /// <summary>
        /// 根据供应商ID查找供应商信息
        /// </summary>
        /// <param name="SupplierID"></param>
        /// <returns></returns>
        public SupplierBaseInformation FindBySupplierID(string SupplierID)
        {
            return sBaseInforIDAL.FindBySupplierID(SupplierID);
        }
    }
}
