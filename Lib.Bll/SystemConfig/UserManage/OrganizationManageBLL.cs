﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

using Lib.IDAL.SystemConfig;
using Lib.SqlServerDAL.SystemConfig;
using Lib.Model.SystemConfig;
using DALFactory;

namespace Lib.Bll.SystemConfig.UserManage
{
    public class OrganizationManageBLL
    {
        BaseOrganizationIDAL organizationDAL = DALFactory.DALFactoryHelper.CreateNewInstance<BaseOrganizationDAL>("SystemConfig.BaseOrganizationDAL");
        JoinUserOrganizationIDAL userOrganizationDAL = DALFactory.DALFactoryHelper.CreateNewInstance<JoinUserOrganizationDAL>("SystemConfig.JoinUserOrganizationDAL");

        /// <summary>
        /// 插入新的组织机构
        /// </summary>
        /// <param name="organizationModel"></param>
        public void insertOrganization(BaseOrganizationModel organizationModel)
        {
            organizationDAL.insertOrganization(organizationModel);
        }

        /// <summary>
        /// 查询父级节点的Right_Value
        /// </summary>
        /// <param name="organizationID"></param>
        /// <returns></returns>
        public int queryParentRightValue(String organizationID)
        {
            DataTable orgTable = organizationDAL.queryParentRightValue(organizationID);
            if (orgTable != null && orgTable.Rows.Count > 0)
            {
                return Convert.ToInt32(orgTable.Rows[0][0]);
            }
            else
            {
                throw new Exception("查询父级节点信息错误！");
            }
        }

        /// <summary>
        /// 根据Organization_ID查询组织机构信息
        /// </summary>
        /// <param name="orgModel"></param>
        public void queryOrganizationInfor(BaseOrganizationModel orgModel)
        {
            DataTable orgTable = organizationDAL.queryOrganizationInfor(orgModel);
            if (orgTable != null && orgTable.Rows.Count > 0)
            {
                DataRow row = orgTable.Rows[0];
                orgModel.Organization_Name = Convert.ToString(row["Organization_Name"]);
                orgModel.Left_Value = Convert.ToInt32(row["Left_Value"]);
                orgModel.Right_Value = Convert.ToInt32(row["Right_Value"]);
                orgModel.Level = Convert.ToInt32(row["Level"]);
                orgModel.Description = Convert.ToString(row["Description"]);
            }
            else
            {
                throw new Exception("查询组织机构信息错误！");
            }
        }

        /// <summary>
        /// 更新组织机构信息
        /// </summary>
        /// <param name="orgModel"></param>
        public void updateOrganizationInfor(BaseOrganizationModel orgModel)
        {
            organizationDAL.updateOrganizationInfor(orgModel);
        }

        /// <summary>
        /// 查询所有组织机构信息
        /// </summary>
        /// <returns></returns>
        public DataTable queryAllOrganizationInfor()
        {
            return organizationDAL.queryAllOrganizationInfor();
        }

        /// <summary>
        /// 插入用户和组织机构关系
        /// </summary>
        /// <param name="orgModel"></param>
        public void insertUserOrganizationLink(JoinUserOrganizationModel orgModel)
        {
            userOrganizationDAL.insertUserOrganizationLink(orgModel);
        }

        /// <summary>
        /// 查询某组织机构关联的所有员工信息
        /// </summary>
        /// <param name="organizationID"></param>
        /// <returns></returns>
        public DataTable queryUserInforByOrganizationID(String organizationID)
        {
            return userOrganizationDAL.queryUserInforByOrganizationID(organizationID);
        }

        /// <summary>
        /// 将用户移除某组织机构级别
        /// </summary>
        /// <param name="orgModel"></param>
        public void removeUserOrganizationLink(JoinUserOrganizationModel orgModel)
        {
            userOrganizationDAL.deleteUserOrganizationLinkByOrgIdAndUserId(orgModel);
        }

        /// <summary>
        /// 删除某组织机构所关联的全部员工
        /// </summary>
        public void removeUserOrganizationLinkByOrgId(JoinUserOrganizationModel orgModel)
        {
            userOrganizationDAL.deleteUserOrganizationLinkByOrgId(orgModel);
        }

        /// <summary>
        /// 查询当前组织机构的所有下属机构编号
        /// </summary>
        /// <param name="orgModel"></param>
        /// <returns></returns>
        public DataTable queryOffspringOrganization(BaseOrganizationModel orgModel)
        {
            return organizationDAL.queryOffspringOrganization(orgModel);
        }

        /// <summary>
        /// 删除当前组织机构及所有下属机构，并更新相应节点的左右值
        /// </summary>
        /// <param name="orgModel"></param>
        public void deleteOffspringOrganizationAndUpdateNodes(BaseOrganizationModel orgModel)
        {
            organizationDAL.deleteOffspringOrganizationAndUpdateNodes(orgModel);
        }

    }
}
