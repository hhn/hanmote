﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

using Lib.IDAL.SystemConfig;
using Lib.Model.SystemConfig;
using Lib.SqlServerDAL.SystemConfig;
using DALFactory;

namespace Lib.Bll.SystemConfig.UserManage
{
    public class UserRoleDetailInforBLL
    {
        BaseRoleIDAL baseRoleIDAL = DALFactoryHelper.CreateNewInstance<BaseRoleIDAL>("SystemConfig.BaseRoleDAL");

        /// <summary>
        /// 获取可用最大角色编号
        /// </summary>
        /// <returns></returns>
        public String getBiggestRoleId()
        {
            String today = DateTime.Now.ToString("yyyyMMdd");
            DataTable roleTable = baseRoleIDAL.queryBiggestRoleID(today);
            if (roleTable == null)
            {
                throw new Exception("生成角色编号失败！");
            }

            if (roleTable.Rows.Count > 0 && !roleTable.Rows[0][0].Equals(DBNull.Value))
            {
                String currentId = Convert.ToString(roleTable.Rows[0][0]);
                int seq = Convert.ToInt32(currentId.Substring(8, 5));
                String newSeq = Convert.ToString(seq + 1);
                if (seq >= 1 && seq < 9)
                    return today + "0000" + newSeq;
                else if (seq >= 9 && seq < 99)
                    return today + "000" + newSeq;
                else if (seq >= 99 && seq < 999)
                    return today + "00" + newSeq;
                else if (seq >= 999 && seq < 9999)
                    return today + "0" + newSeq;
                else if (seq >= 9999 && seq < 99999)
                    return today + newSeq;
                else
                    throw new Exception("今日可用编号已达到上限，请择日重试！");
            }
            else
            {
                return today + "00001";
            }
        }

        /// <summary>
        /// 根据角色编号查询角色信息
        /// </summary>
        /// <param name="roleModel"></param>
        public void queryRoleInforById(BaseRoleModel roleModel)
        {
            DataTable roleTable = baseRoleIDAL.queryRoleInforById(roleModel);
            if (roleTable != null && roleTable.Rows.Count > 0)
            {
                roleModel.Role_Name = Convert.ToString(roleTable.Rows[0]["Role_Name"]);
                roleModel.Description = Convert.ToString(roleTable.Rows[0]["Description"]);
                roleModel.Left_Value = Convert.ToInt32(roleTable.Rows[0]["Left_Value"]);
                roleModel.Right_Value = Convert.ToInt32(roleTable.Rows[0]["Right_Value"]);
                roleModel.Level = Convert.ToInt32(roleTable.Rows[0]["Level"]);
            }
            else
            {
                throw new Exception("查询角色信息失败！");
            }
        }

        /// <summary>
        /// 更新角色信息
        /// </summary>
        /// <param name="roleModel"></param>
        public void updateRoleInforById(BaseRoleModel roleModel)
        {
            baseRoleIDAL.updateRoleInforById(roleModel);
        }

    }
}
