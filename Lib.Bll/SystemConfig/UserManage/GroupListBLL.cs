﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

using Lib.IDAL.SystemConfig;
using Lib.SqlServerDAL.SystemConfig;
using Lib.Model.SystemConfig;
using DALFactory;

namespace Lib.Bll.SystemConfig.UserManage
{
    public class GroupListBLL
    {
        JoinUserGroupIDAL joinUserGroupIDAL = DALFactoryHelper.CreateNewInstance<JoinUserGroupIDAL>("SystemConfig.JoinUserGroupDAL");

        /// <summary>
        /// 查询用户的所有组信息
        /// </summary>
        /// <param name="joinUserGroupModel"></param>
        /// <returns></returns>
        public DataTable queryGroupByUserId(JoinUserGroupModel joinUserGroupModel)
        {
            return joinUserGroupIDAL.queryGroupByUserId(joinUserGroupModel);
        }
    }
}
