﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

using Lib.Model.SystemConfig;
using Lib.IDAL.SystemConfig;
using Lib.SqlServerDAL.SystemConfig;

namespace Lib.Bll.SystemConfig.UserManage
{
    public class LoginBLL
    {
        BaseUserIDAL baseUserIDAL = DALFactory.DALFactoryHelper.CreateNewInstance<BaseUserIDAL>("SystemConfig.BaseUserDAL");

        /// <summary>
        /// 填充用户Model
        /// </summary>
        /// <param name="userModel"></param>
        /// <param name="userInfor"></param>
        private void fillBaseUserModel(BaseUserModel userModel, DataTable userInfor)
        {
            if (userModel == null || userInfor == null)
            {
                return;
            }
            //对部分目前有用的字段赋值
            userModel.User_ID = Convert.ToString(userInfor.Rows[0]["User_ID"]);
            userModel.Username = Convert.ToString(userInfor.Rows[0]["Username"]);
            userModel.Gender = Convert.ToInt32(userInfor.Rows[0]["Gender"]);
           // userModel.Birthday = Convert.ToString(userInfor.Rows[0]["Birthday"]);
         //   userModel.Birth_Place = Convert.ToString(userInfor.Rows[0]["Birth_Place"]);
         //   userModel.ID_Number = Convert.ToString(userInfor.Rows[0]["ID_Number"]);
         //   userModel.Mobile = Convert.ToString(userInfor.Rows[0]["Mobile"]);
         //   userModel.Email = Convert.ToString(userInfor.Rows[0]["Email"]);
        //    userModel.User_Description = Convert.ToString(userInfor.Rows[0]["User_Description"]);
         //   userModel.Question = Convert.ToString(userInfor.Rows[0]["Answer_Question"]);
            userModel.Enabled = Convert.ToInt32(userInfor.Rows[0]["Enabled"]);
            userModel.Manager_Flag = Convert.ToInt32(userInfor.Rows[0]["Manager_flag"]);
            userModel.Role_ID = userInfor.Rows[0]["Role_ID"].ToString();
        }

        /// <summary>
        /// 验证用户登录信息
        /// </summary>
        /// <param name="userModel"></param>
        /// <returns></returns>
        public bool validateLoginInfor(BaseUserModel userModel)
        {
            DataTable userInfor = baseUserIDAL.validateUserInformation(userModel);
            if (userInfor == null || userInfor.Rows.Count <= 0)
            {
                return false;
            }
            else
            {
                fillBaseUserModel(userModel, userInfor);
                return true;
            }
        }


    }
}
