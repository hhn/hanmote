﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using Lib.IDAL.SystemConfig;
using Lib.SqlServerDAL.SystemConfig;
using Lib.Model.SystemConfig;
using DALFactory;

namespace Lib.Bll.SystemConfig.UserManage
{
    public class UserGroupManageBLL
    {
        BaseGroupIDAL baseGroupIDAL = DALFactoryHelper.CreateNewInstance<BaseGroupIDAL>("SystemConfig.BaseGroupDAL");
        JoinUserGroupIDAL joinUserGroupIDAL = DALFactoryHelper.CreateNewInstance<JoinUserGroupIDAL>("SystemConfig.JoinUserGroupDAL");
        JoinGroupFunctionPermissionObjectIDAL joinGroupFunction = DALFactoryHelper.CreateNewInstance<JoinGroupFunctionPermissionObjectDAL>("SystemConfig.JoinGroupFunctionPermissionObjectDAL");

        /// <summary>
        /// 查询系统全部组和用户信息
        /// </summary>
        /// <returns></returns>
        public DataTable queryAllGroupAndUser()
        {
            return baseGroupIDAL.queryGroupAndUserName();
        }

        /// <summary>
        /// 添加用户到组
        /// </summary>
        /// <param name="joinUserGroupModel"></param>
        public void addUserToGroup(JoinUserGroupModel joinUserGroupModel)
        {
            DataTable userGroupTable = joinUserGroupIDAL.checkUserGroupLinkExist(joinUserGroupModel);
            if(userGroupTable != null && userGroupTable.Rows.Count <= 0)
            {
                joinUserGroupIDAL.addUserToGroup(joinUserGroupModel);
            }
        }

        /// <summary>
        /// 从组中移除用户
        /// </summary>
        /// <param name="joinUserGroupModel"></param>
        public void removeUserFromGroup(JoinUserGroupModel joinUserGroupModel)
        {
            joinUserGroupIDAL.removeUserFromGroup(joinUserGroupModel);
        }

        /// <summary>
        /// 删除一个用户组中的所有用户
        /// </summary>
        /// <param name="joinUserGroupModel"></param>
        public void deleteAllUserOfGroup(JoinUserGroupModel joinUserGroupModel)
        {
            joinUserGroupIDAL.deleteAllUserOfGroup(joinUserGroupModel);
        }

        /// <summary>
        /// 删除某用户组的所有权限
        /// </summary>
        /// <param name="objModel"></param>
        public void revokeGroupAllPermission(JoinGroupFunctionPermissionObjectModel objModel)
        {
            joinGroupFunction.revokeGroupAllPermission(objModel);
        }

        /// <summary>
        /// 根据用户组编号删除用户组
        /// </summary>
        /// <param name="baseGroupModel"></param>
        public void deleteGroupById(BaseGroupModel baseGroupModel)
        {
            baseGroupIDAL.deleteGroup(baseGroupModel);
        }
    }
}
