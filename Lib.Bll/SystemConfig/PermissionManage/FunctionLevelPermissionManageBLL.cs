﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

using Lib.SqlServerDAL.SystemConfig;
using Lib.IDAL.SystemConfig;
using Lib.Model.SystemConfig;
using DALFactory;

namespace Lib.Bll.SystemConfig.PermissionManage
{
    public class FunctionLevelPermissionManageBLL
    {
        private BaseFunctionPermissionObjectIDAL baseFunction = DALFactoryHelper.CreateNewInstance<BaseFunctionPermissionObjectIDAL>("SystemConfig.BaseFunctionPermissionObjectDAL");
        private JoinUserFunctionPermissionObjectIDAL joinUserFunction = DALFactoryHelper.CreateNewInstance<JoinUserFunctionPermissionObjectIDAL>("SystemConfig.JoinUserFunctionPermissionObjectDAL");
        private JoinRoleFunctionPermissionObjectIDAL joinRoleFunction = DALFactoryHelper.CreateNewInstance<JoinRoleFunctionPermissionObjectIDAL>("SystemConfig.JoinRoleFunctionPermissionObjectDAL");
        private JoinGroupFunctionPermissionObjectIDAL joinGroupFunction = DALFactoryHelper.CreateNewInstance<JoinGroupFunctionPermissionObjectIDAL>("SystemConfig.JoinGroupFunctionPermissionObjectDAL");

        private BaseRoleIDAL baseRole = DALFactoryHelper.CreateNewInstance<BaseRoleDAL>("SystemConfig.BaseRoleDAL");
        private JoinUserRoleIDAL joinUserRole = DALFactoryHelper.CreateNewInstance<JoinUserRoleDAL>("SystemConfig.JoinUserRoleDAL");

        /// <summary>
        /// 查询某一个用户所具有的角色、及这些角色的下属角色是否具有某个权限项（对应某个Permission_ID）
        /// </summary>
        /// <param name="userModel"></param>
        /// <param name="basePermissionModel"></param>
        /// <returns></returns>
        public DataTable queryRolePermissionRelatedUserByPermissionID(BaseUserModel userModel, BaseFunctionPermissionObjectModel basePermissionModel)
        {
            DataTable rolePermissionTable = null;

            JoinUserRoleModel joinUserRoleModel = new JoinUserRoleModel();
            joinUserRoleModel.User_ID = userModel.User_ID;

            //查询用户具有的所有角色
            DataTable userTable = joinUserRole.queryRoleByUserId(joinUserRoleModel);
            BaseRoleModel parentRoleModel = new BaseRoleModel();
            BaseRoleModel childRoleModel = new BaseRoleModel();
            foreach (DataRow row in userTable.Rows)
            {
                parentRoleModel.Role_ID = Convert.ToString(row["Role_ID"]);
                parentRoleModel.Left_Value = Convert.ToInt32(row["Left_Value"]);
                parentRoleModel.Right_Value = Convert.ToInt32(row["Right_Value"]);
                //查询每个角色所具有的下属角色
                DataTable offspringTable = baseRole.queryOffspringInfor(parentRoleModel);
                foreach (DataRow rowItem in offspringTable.Rows)
                {
                    childRoleModel.Role_ID = Convert.ToString(rowItem["Role_ID"]);
                    if (rolePermissionTable == null)
                    {
                        rolePermissionTable = joinRoleFunction.queryPermissionFromSingleRoleOfPermissionID(childRoleModel, basePermissionModel);
                    }
                    else
                    {
                        rolePermissionTable.Merge(joinRoleFunction.queryPermissionFromSingleRoleOfPermissionID(childRoleModel, basePermissionModel));
                    }
                }
            }

            return rolePermissionTable;
        }

        /// <summary>
        /// 查询某一个用户所具有的角色、及这些角色的下属角色所具有的权限项（对应某个Menu_Name）
        /// </summary>
        /// <returns></returns>
        public DataTable queryRolePermissionRelatedUserByMenuName(BaseUserModel userModel, BaseFunctionPermissionObjectModel basePermissionModel)
        {
            DataTable rolePermissionTable = null;

            JoinUserRoleModel joinUserRoleModel = new JoinUserRoleModel();
            joinUserRoleModel.User_ID = userModel.User_ID;

            //查询用户具有的所有角色
            DataTable userTable = joinUserRole.queryRoleByUserId(joinUserRoleModel);
            BaseRoleModel parentRoleModel = new BaseRoleModel();
            BaseRoleModel childRoleModel = new BaseRoleModel();
            foreach (DataRow row in userTable.Rows)
            {
                parentRoleModel.Role_ID = Convert.ToString(row["Role_ID"]);
                parentRoleModel.Left_Value = Convert.ToInt32(row["Left_Value"]);
                parentRoleModel.Right_Value = Convert.ToInt32(row["Right_Value"]);
                //查询每个角色所具有的下属角色
                DataTable offspringTable = baseRole.queryOffspringInfor(parentRoleModel);
                foreach (DataRow rowItem in offspringTable.Rows)
                {
                    childRoleModel.Role_ID = Convert.ToString(rowItem["Role_ID"]);
                    if (rolePermissionTable == null)
                    {
                        rolePermissionTable = queryCurrentRolePermission(childRoleModel, basePermissionModel);
                    }
                    else
                    {
                        rolePermissionTable.Merge(queryCurrentRolePermission(childRoleModel, basePermissionModel));
                    }
                }
            }

            return rolePermissionTable;
        }

        /// <summary>
        /// 查询一个用户在某个节点对应窗口中的权限信息
        /// </summary>
        /// <param name="userModel"></param>
        /// <param name="basePermissionModel"></param>
        /// <returns></returns>
        public DataTable queryBaseFunctionPermission(BaseUserModel userModel,BaseFunctionPermissionObjectModel basePermissionModel)
        {
            //查询一个菜单对应窗口的所有权限项
            DataTable allPermissionOfWindowTable = baseFunction.queryPermissionByMenuName(basePermissionModel);
            //查询用户直接权限项
            DataTable baseUserPermissionTable = joinUserFunction.queryUserFunctionByID(userModel,basePermissionModel);
            //查询所具有的角色的所有权限项
            DataTable joinRolePermissionTable = joinRoleFunction.queryPermissionFromRoleOfMenuName(userModel, basePermissionModel);
            //查询所在用户组权限项
            DataTable joinGroupPermissionTable = joinGroupFunction.queryPermissionFromGroup(userModel, basePermissionModel);

            //统计所有具有的权限对象
            Dictionary<String, String> hasPermissionDict = new Dictionary<string, string>();
            Dictionary<String, String> groupPermissionDict = new Dictionary<string, string>();
            Dictionary<String, String> rolePermissionDict = new Dictionary<string, string>();

            foreach (DataRow row in baseUserPermissionTable.Rows)
            {
                String key = Convert.ToString(row[0]);
                String value = Convert.ToString(row[1]);
                if(!hasPermissionDict.ContainsKey(key))
                {
                    hasPermissionDict.Add(key,value);
                }
            }

            foreach (DataRow row in joinGroupPermissionTable.Rows)
            {
                String key = Convert.ToString(row[0]);
                String value = Convert.ToString(row[1]);
                if (!groupPermissionDict.ContainsKey(key))
                {
                    groupPermissionDict.Add(key, value);
                }
            }

            if (joinRolePermissionTable != null)
            {
                foreach (DataRow row in joinRolePermissionTable.Rows)
                {
                    String key = Convert.ToString(row[0]);
                    String value = Convert.ToString(row[1]);
                    if (!rolePermissionDict.ContainsKey(key))
                    {
                        rolePermissionDict.Add(key, value);
                    }
                }
            }

            //结果集增加3列
            DataColumn newColumn = new DataColumn();
            newColumn.DataType = typeof(bool);
            newColumn.ColumnName = "hasPermission";

            DataColumn groupColumn = new DataColumn();
            groupColumn.DataType = typeof(bool);
            groupColumn.ColumnName = "groupPermission";

            DataColumn roleColumn = new DataColumn();
            roleColumn.DataType = typeof(bool);
            roleColumn.ColumnName = "rolePermission";

            allPermissionOfWindowTable.Columns.Add(newColumn);
            allPermissionOfWindowTable.Columns.Add(groupColumn);
            allPermissionOfWindowTable.Columns.Add(roleColumn);

            foreach (DataRow row in allPermissionOfWindowTable.Rows)
            {
                //当前域权限是否具有
                String permission_id = Convert.ToString(row["Permission_ID"]);
                row["hasPermission"] = hasPermissionDict.ContainsKey(permission_id);
                row["groupPermission"] = groupPermissionDict.ContainsKey(permission_id);
                row["rolePermission"] = rolePermissionDict.ContainsKey(permission_id);
            }

            return allPermissionOfWindowTable;
        }

        /// <summary>
        /// 收回用户被授予的权限
        /// </summary>
        /// <param name="objModel"></param>
        public void revokePermission(JoinUserFunctionPermissionObjectModel objModel)
        {
            joinUserFunction.revokePermission(objModel);
        }

        /// <summary>
        /// 授予用户新的权限
        /// </summary>
        /// <param name="objModel"></param>
        public void authorizePermission(JoinUserFunctionPermissionObjectModel objModel)
        {
            joinUserFunction.authorizePermission(objModel);
        }

        /// <summary>
        /// 查询一个组在某个节点对应窗口中的权限信息
        /// </summary>
        /// <param name="groupModel"></param>
        /// <param name="basePermissionModel"></param>
        public DataTable queryBaseGroupPermission(BaseGroupModel groupModel, BaseFunctionPermissionObjectModel basePermissionModel)
        {
            //查询一个菜单对应窗口的所有权限项
            DataTable allPermissionOfWindowTable = baseFunction.queryPermissionByMenuName(basePermissionModel);
            //查询一个组所具有的权限项
            DataTable groupPermission = joinGroupFunction.queryPermissionFromSingleGroup(groupModel, basePermissionModel);

            //统计所有具有的权限对象
            Dictionary<String, String> hasPermissionDict = new Dictionary<string, string>();
            foreach (DataRow row in groupPermission.Rows)
            {
                hasPermissionDict.Add(Convert.ToString(row[0]), Convert.ToString(row[1]));
            }

            //结果集增加一列hasPermission
            DataColumn newColumn = new DataColumn();
            newColumn.DataType = typeof(bool);
            newColumn.ColumnName = "hasPermission";
            allPermissionOfWindowTable.Columns.Add(newColumn);

            foreach (DataRow row in allPermissionOfWindowTable.Rows)
            {
                //当前域权限是否具有
                row["hasPermission"] = hasPermissionDict.ContainsKey(Convert.ToString(row["Permission_ID"]));
            }

            return allPermissionOfWindowTable;
        }

        /// <summary>
        /// 收回组权限
        /// </summary>
        /// <param name="objModel"></param>
        public void revokeGroupPermission(JoinGroupFunctionPermissionObjectModel objModel)
        {
            joinGroupFunction.revokeGroupPermission(objModel);
        }

        /// <summary>
        /// 授予组权限
        /// </summary>
        /// <param name="objModel"></param>
        public void authorizeGroupPermission(JoinGroupFunctionPermissionObjectModel objModel)
        {
            joinGroupFunction.authorizeGroupPermission(objModel);
        }

        /// <summary>
        /// 查询某个角色所具有的对应某窗口的权限信息
        /// </summary>
        /// <param name="roleModel"></param>
        /// <param name="basePermissionModel"></param>
        /// <returns></returns>
        private DataTable queryCurrentRolePermission(BaseRoleModel roleModel, BaseFunctionPermissionObjectModel basePermissionModel)
        {
            return joinRoleFunction.queryPermissionFromSingleRoleOfMenuName(roleModel, basePermissionModel);
        }

        /// <summary>
        /// 查询某个角色的所有下属角色所具有的对应某窗口的权限信息
        /// </summary>
        /// <param name="roleModel"></param>
        /// <param name="basePermissionModel"></param>
        /// <returns></returns>
        private DataTable queryOffspringRolePermission(BaseRoleModel roleModel, BaseFunctionPermissionObjectModel basePermissionModel)
        {
            DataTable offspringPermissionTable = null;
            //根据角色编号获取该角色左、右值等其他信息
            DataTable roleTable = baseRole.queryRoleInforById(roleModel);
            if (roleTable != null && roleTable.Rows.Count >= 0)
            {
                roleModel.Left_Value = Convert.ToInt32(roleTable.Rows[0]["Left_Value"]);
                roleModel.Right_Value = Convert.ToInt32(roleTable.Rows[0]["Right_Value"]);

                //查询当前角色的所有下属角色信息
                DataTable offspringTable = baseRole.queryOffspringInforWithoutSelf(roleModel);
                BaseRoleModel offspringModel = new BaseRoleModel();
                bool firstFlag = false;
                //查询所有下属角色的权限，并进行合并
                foreach (DataRow row in offspringTable.Rows)
                {
                    offspringModel.Role_ID = Convert.ToString(row["Role_ID"]);
                    if (!firstFlag)
                    {
                        offspringPermissionTable = queryCurrentRolePermission(offspringModel, basePermissionModel);
                    }
                    else
                    {
                        offspringPermissionTable.Merge(queryCurrentRolePermission(offspringModel, basePermissionModel));
                    }
                }
            }
            return offspringPermissionTable;
        }

        /// <summary>
        /// 查询一个角色在某个节点对应窗口中的权限信息
        /// </summary>
        /// <param name="roleModel"></param>
        /// <param name="basePermissionModel"></param>
        public DataTable queryBaseRolePermission(BaseRoleModel roleModel, BaseFunctionPermissionObjectModel basePermissionModel)
        {
            //查询一个菜单对应窗口的所有权限项
            DataTable allPermissionOfWindowTable = baseFunction.queryPermissionByMenuName(basePermissionModel);
            //查询一个角色本身所具有的权限项
            DataTable currentRolePermission = queryCurrentRolePermission(roleModel, basePermissionModel);

            Dictionary<String, String> currentRoleDict = new Dictionary<string, string>();

            foreach (DataRow row in currentRolePermission.Rows)
            {
                String key = Convert.ToString(row[0]);
                String value = Convert.ToString(row[1]);
                if (!currentRoleDict.ContainsKey(key))
                {
                    currentRoleDict.Add(key,value);
                }
            }

            DataColumn col1 = new DataColumn();
            col1.DataType = typeof(bool);
            col1.ColumnName = "hasPermission";
            allPermissionOfWindowTable.Columns.Add(col1);

            foreach (DataRow row in allPermissionOfWindowTable.Rows)
            {
                //当前域权限是否具有
                String permission_id = Convert.ToString(row["Permission_ID"]);
                row["hasPermission"] = currentRoleDict.ContainsKey(permission_id);
            }

            return allPermissionOfWindowTable;
        }

        /// <summary>
        /// 收回角色权限
        /// </summary>
        /// <param name="objModel"></param>
        public void revokeRolePermission(JoinRoleFunctionPermissionObjectModel objModel)
        {
            joinRoleFunction.revokeRolePermission(objModel);
        }

        /// <summary>
        /// 授予角色权限
        /// </summary>
        /// <param name="objModel"></param>
        public void authorizeRolePermission(JoinRoleFunctionPermissionObjectModel objModel)
        {
            joinRoleFunction.authorizeRolePermission(objModel);
        }

    }
}
