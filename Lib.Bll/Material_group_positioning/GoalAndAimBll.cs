﻿using DALFactory;
using Lib.IDAL.Material_group_positioning;
using Lib.Model.MT_GroupModel;
using Lib.Model.PrdModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Lib.Bll.Material_group_positioning
{
    public class GoalAndAimBll
    {

        
        private static readonly GoalAndAimIDAL idal = DALFactoryHelper.CreateNewInstance<GoalAndAimIDAL>("Material_group_positioning.GoalAndAimDAL");


        //维护性物料组

        //资产性物料组


        #region 生产性物料组

        public List<String> getPrdId()
        {
            List<String> list = new List<string>();
            DataTable dt =idal.GetPrdId();

            for (int i = 0; i < dt.Rows.Count; i++) {
                list.Add(dt.Rows[i][0].ToString()); 
            }
            return list;
        }



        /// <summary>
        /// 通过产品id查询产品名称
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>

        public String getPrdName(string text)
        {
           
            DataTable dt = idal.GetPrdName(text);
            return dt.Rows[0][0].ToString();
        }

 
        /// <summary>
        /// 获取物料组类型
        /// </summary>
        /// <returns></returns>
        public List<ListInfo> getMtGroupTypeId()
        {
            List<ListInfo> list = new List<ListInfo>();
            DataTable dt = idal.getMtGroupTypeId();
           
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                ListInfo listInfo = new ListInfo();
                listInfo.Id = dt.Rows[i]["Id"].ToString();
                listInfo.Value = dt.Rows[i]["Name"].ToString();
                list.Add(listInfo);
            }
            return list;

        }
        /// <summary>
        /// 判定是否存在物料
        /// </summary>
        /// <param name="mtGroupId1">物料编号</param>
        /// <returns></returns>
        public bool getMaterials(string mtGroupId1)
        {
            return idal.getMaterials(mtGroupId1);
        }

        public bool getPrdId(string prdId)
        {
            return idal.getPrdId(prdId);
        }

        public bool getGoalId(string goalId)
        {
            return idal.getGoalId(goalId);
        }

        public int insertAssertAndMainInfo(GroupGoalModel prdIndicatorModel)
        {
           return idal.insertAssertAndMainInfo(prdIndicatorModel);
        }

        public bool insertPrdInfoAndMaterial(PrdModel prdModel)
        {
            return idal.insertPrdInfoAndMaterial(prdModel);
        }

        /// <summary>
        /// 新建产品信息
        /// </summary>
        /// <param name="prdModel"></param>
        /// <returns></returns>
        public bool insertPrdInfo(PrdModel prdModel)
        {
            return idal.insertPrdInfo(prdModel);
        }

        public object getMtGroupInfo()
        {
            DataTable dt = idal.getMtGroupInfo();

            return dt;
        }

        /// <summary>
        /// 根据物料类型显示对应物料
        /// </summary>
        /// <param name="mtGroupType">物料类型编号</param>
        /// <returns></returns>
        public DataTable getAssertMaterial(string mtGroupTypeId, int pageSize, int pageIndex)
        {
            DataTable dt = null;
            try
            {
                 dt = idal.getMaterialGroupInfo(mtGroupTypeId, pageSize, pageIndex);
            }
            catch (Exception)
            {
                dt = null;
            }
            return dt;
        }

        public bool insertAssertAndMainInfo(System.Windows.Forms.DataGridView dgv_goal, String mtGroupId)
        {
            GroupGoalModel groupGoalModel1 = new GroupGoalModel();
            groupGoalModel1.MtGroupId1 = mtGroupId;
            int flag = 0;
            for (int i = 0; i < dgv_goal.Rows.Count-1; i++)
            {

                groupGoalModel1.GoalId1 = dgv_goal.Rows[i].Cells["GoalId"].Value.ToString();
                groupGoalModel1.GoalText1 = dgv_goal.Rows[i].Cells["supplierGoal"].Value.ToString();
                groupGoalModel1.GoalValue1 = float.Parse(dgv_goal.Rows[i].Cells["GoalValue"].Value.ToString());
                groupGoalModel1.GoalLevelValue1 = float.Parse(dgv_goal.Rows[i].Cells["levelValue"].Value.ToString());
                groupGoalModel1.GoalPipLevel1 = dgv_goal.Rows[i].Cells["level"].Value.ToString();
                groupGoalModel1.Unit1 = dgv_goal.Rows[i].Cells["Unit"].Value.ToString();
                groupGoalModel1.Domain= dgv_goal.Rows[i].Cells["domain"].Value.ToString();
                try
                {
                    
                    flag+=idal.insertAssertAndMainInfo(groupGoalModel1);
                }
                catch (Exception ex)
                {
                    continue;
                }

            }
            if (flag == dgv_goal.Rows.Count - 1)
            {
                return true;
            }
            else {
                return false;
            }

        }
        /// <summary>
        /// 获取物料数据
        /// </summary>
        /// <param name="mtGroupTypeId"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageIndex"></param>
        /// <returns></returns>
        public DataTable getMaterialInfo(string mtGroupTypeId, int pageSize, int pageIndex)
        {
            return idal.getMaterialInfo(mtGroupTypeId, pageSize, pageIndex);
        }

        public int getMtInfoCount(string mtGroupTypeId)
        {
            return idal.getMtInfoCount(mtGroupTypeId);
        }

        /// <summary>
        /// 获取特定类型物料组记录数
        /// </summary>
        /// <param name="v">物料组类型参数</param>
        /// <returns></returns>
        public int getMtGroupCount(string typeId)
        {
            int count = idal.getMtGroupCount(typeId);
            return count;
        }

        /// <summary>
        /// 获取组成产品的物料信息
        /// </summary>
        /// <param name="text">产品编号</param>
        /// <param name="goalId">供应目标编号</param>
        /// <returns></returns>
        public DataTable getMaterialInfo(string text, string goalId, int pageSize, int pageIndex)
        {
            DataTable dt = idal.getMaterialInfo(text, goalId, pageSize, pageIndex);
            dt.Columns.Remove("materialId");
            return dt;
        }
       
        public DataTable getAssertGoal(string mtId, int pageSize, int pageIndex)
        {
            DataTable dt = null;
            try
            {
                 dt = idal.getAssertGoal(mtId, pageSize, pageIndex);

            }
            catch (Exception)
            {

                dt = null;
            }
            return dt;
        }
        /// <summary>
        /// 获取对应指标数
        /// </summary>
        /// <param name="mtId"></param>
        /// <returns></returns>
        public int getIndicateCount(string mtId)
        {
            int count = 0;
            try
            {
                 count = idal.getIndicateCount(mtId);
            }
            catch (Exception)
            {

                count = 0;
            }
            
            return count;
        }

        public int insertGoalInfo(prdGoalModel prdGoalModel)
        {
          return  idal.insertGoalInfo(prdGoalModel);
            
        }

        public DataTable getPrdAim(string text, int pageSize, int pageIndex)
        {
            return idal.getPrdAim(text,pageSize,pageIndex);
        }
        //插入供应指标
        public int insertIndicatorInfo(PrdIndicatorModel prdIndicatorModel)
        {
            return idal.insertIndicatorInfo(prdIndicatorModel);
        }
        /// <summary>
        /// 获取供应目标的总条数
        /// </summary>
        /// <param name="text">产品编号</param>
        /// <returns></returns>
        public int getGoalCount(string text)
        {
            return idal.getGoalCount(text);
        }

        public int getIndicationCount(string text)
        {
            return idal.getIndicationCount(text);
        }
        #endregion

    }

    public class ListInfo {
        private string id;
        private string value;

        public string Id { get => id; set => id = value; }
        public string Value { get => value; set => this.value = value; }
    }


}
