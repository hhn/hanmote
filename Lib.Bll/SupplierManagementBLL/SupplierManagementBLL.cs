﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.SqlIDAL;
using DALFactory;
using System.Data;
using Lib.Model;
using Lib.Common.MMCException.IDAL;
using Lib.Common.MMCException.Bll;

namespace Lib.Bll
{
    public class SupplierManagementBLL
    {
        private static readonly SupplierManagementIDAL idal = DALFactoryHelper.CreateNewInstance<SupplierManagementIDAL>("SupplierManagementDAL");

        public List<String> GetrAllMaterialType()
        {
            return idal.GetrAllMaterialType();
        }

        public DataTable GetAllSupplier()
        {
            return idal.GetAllSupplier();
        }

        public DataTable GetAllMaterialGroupBySupplierID(string supplierID)
        {
            return idal.GetAllMaterialGroupBySupplierID(supplierID);
        }


        /// <summary>
        /// 通过供应商编号获取供应商品项信息
        /// </summary>
        /// <param name="SupplierID"></param>
        /// <returns></returns>
        public DataTable GetItemsInfoByMaterialGroupID(string supplierID)
        {
            return idal.GetItemsInfoByMaterialGroupID(supplierID);
        }
        /// <summary>
        ///  通过供应商物料组设置支出类型
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public bool SetExpenseTypeByMaterialGroup(DataTable dt)
        {
            return idal.SetExpenseTypeByMaterialGroup(dt);
        }

        public bool UpdateTempState(string supplierID,int state)
        {
            return idal.UpdateTempState(supplierID,state);
        }

        public DataTable GetAllMaterialGroup() {
            DataTable dt = idal.GetAllMaterialGroup();
            return dt;
        }

        /// <summary>
        /// 获取所有供应商编码列表
        /// </summary>
        /// <returns></returns>
        public List<string> GetAllSupplierBaseId()
        {
            try
            {
                DataTable dt = idal.GetAllSupplierBaseId();
                if (dt != null && dt.Rows.Count > 0)
                {
                    //行数
                    int rows = dt.Rows.Count;
                    List<string> list = new List<string>(rows);
                    for (int i = 0; i < rows; i++)
                    {
                        list.Add(dt.Rows[i][0].ToString());
                    }
                    return list;
                }
                return null;
            }
            catch (DBException ex)
            {
                throw new BllException("查询供应商编码列表失败！", ex);
            }
        }

        public List<CertificationTemplate> GetAllCertificationTemplate()
        {
            return idal.GetAllCertificationTemplate();
        }

        public bool saveCertificationTemplate(DataTable dt)
        {
            return idal.saveCertificationTemplate(dt);
        }
        /// <summary>
        /// 保存自评和供应商基本信息评估的决策结果
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool saveSelfCerAndBaseInfoDecision(DecisionInfo obj)
        {
            return idal.saveSelfCerAndBaseInfoDecision(obj);
        }

        public OrganizationInfo getOrganizationInfoByCompanyName(string companyName)
        {
            return idal.getOrganizationInfoByCompanyName(companyName);
        }

        public List<OrgContacts> getOrgContactsByCompanyName(string companyName)
        {
            return idal.getOrgContactsByCompanyName(companyName);
        }

        public DataTable getAllCompany()
        {
            return idal.getAllCompany();
        }

        public DataTable getFirstDegree()
        {
            return idal.getFirstDegree();
        }
        public DataTable getSecondDegree()
        {
            return idal.getSecondDegree();
        }
        public DataTable getThirdDegree()
        {
            return idal.getThirdDegree();
        }

        public bool saveSEMScore(SEMModel obj) 
        {
            return idal.saveSEMScore(obj);
        }

        public List<SEMModel> getSEMScoreByCompanyName(string companyName)
        { 
            return idal.getSEMScoreByCompanyName(companyName);
        }

        public String getSupplierIdBySupplierName(string supplierName)
        {
            return idal.getSupplierIdBySupplierName(supplierName);
        }

        public DataTable getMaterialName(string type, string filed)
        {
            return idal.getMaterialName(type,filed);
        }

        /// <summary>
        /// 在流程之前返回-1 在流程中 0 在流程之后 1
        /// </summary>
        /// <param name="status"></param>
        /// <param name="supplierName"></param>
        /// <returns></returns>
        public int CheckProcessing(int status,string supplierName)
        {
            int flag = 0;
            int curStatus = idal.getCertificationStatusBySupplierName(supplierName);
            if (curStatus < status)
            {
                if (curStatus + 1 == status)
                    flag = 0;
                else
                    flag = 1;
            }
            else if (curStatus > status)
            {
                flag = -1;
            }
            return flag;

        }

        public bool ModifyProcessingStatus(int status ,string supplierName)
        {
            return idal.ModifyProcessingStatus(status,supplierName);
        }

        public string GetFileOnServerPath(string tableName,string supplierName)
        {
            return idal.GetFileOnServerPath(tableName,supplierName);
        }

    }
}
