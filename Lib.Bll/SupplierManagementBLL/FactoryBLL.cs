﻿using Lib.Common.MMCException.Bll;
using Lib.Common.MMCException.IDAL;
using Lib.IDAL.SupplierManagementIDAL;
using Lib.SqlServerDAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Lib.Bll
{
    public class FactoryBLL
    {
        private FactoryIDAL idal = new FactoryDAL();
        /// <summary>
        /// 获取所有工厂id列表
        /// </summary>
        /// <returns></returns>
        public List<string> GetAllFactoryId()
        {
            try
            {
                DataTable dt = idal.GetAllFactoryIdDT();
                if (dt != null && dt.Rows.Count > 0)
                {
                    int rows = dt.Rows.Count;
                    List<string> list = new List<string>(rows);
                    for (int i = 0; i < rows; i++)
                    {
                        list.Add(dt.Rows[i][0].ToString());
                    }
                    return list;
                }
                return null;
            }
            catch (DBException ex)
            {
                throw new BllException("获取工厂编码列表失败！",ex);
            }
        }
    }
}
