﻿using Lib.Common.MMCException.Bll;
using Lib.Common.MMCException.IDAL;
using Lib.IDAL.SupplierManagementIDAL;
using Lib.SqlServerDAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Lib.Bll
{
    public class MTFTYBLL
    {
        private MTFTYIDAL idal = new MTFTYDAL();

        public List<string> GetAllMaterialIdByFactoryId(string factoryId)
        {
            try
            {
                DataTable dt = idal.GetAllMaterialIdDTByFactoryId(factoryId);
                if (dt != null && dt.Rows.Count > 0)
                {
                    int rows = dt.Rows.Count;
                    List<string> list = new List<string>(rows);
                    for (int i = 0; i < rows; i++)
                    {
                        list.Add(dt.Rows[i][0].ToString());
                    }
                    return list;
                }
                return null;
            }
            catch (DBException ex)
            {
                throw new BllException("根据工厂编码获取物料物料编码失败！", ex);
            }
        }
    }
}
