﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.Model;
using Lib.Common.CommonUtils;


namespace Lib.Bll
{
    /// <summary>
    /// 供应商业务价值模型类
    /// 包含依赖度与PIP等级的对应关系
    /// </summary>
    public class BusinessValueEvaluateModel
    {
        #region  依赖度和PIP对应关系
        /// <summary>
        /// 映射依赖度和PIP的对应关系
        /// </summary>
        /// <param name="dependence">依赖度</param>
        /// <returns>PIP等级</returns>
        public static string mappingDependencePIP(double dependence) 
        {
            string PIP = "";


            return PIP;
        }

        #endregion

        #region 计算依赖度
        /// <summary>
        /// 
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public double calculateDenpendence(double a,double b) 
        {
            double result = 0.0;
            try
            {
                result = MathUtil.division(a,b) * 100;
            }
            catch (Exception e) {
                
            }
            return result;
        }

        #endregion
    }
}
