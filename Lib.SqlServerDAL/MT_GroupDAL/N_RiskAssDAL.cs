﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Lib.IDAL.MT_GroupPositionIDAL;
using Lib.Model.MT_GroupModel;

namespace Lib.SqlServerDAL.MT_GroupDAL
{
    public class N_RiskAssDAL : N_RiskAssessmentIDAL
    {
        DataTable dt = null;
        String sql = "";
        /// <summary>
        /// 获取已经定义好的物料组数据
        /// </summary>
        /// <returns></returns>
        public DataTable GetDefinedGroup(String purOrgName)
        {

            sql = "select distinct id as 物料组编号,MTGName as 物料组名称,Category as 类别 from MTGAttributeDefinition where PurOrgName='"+ purOrgName + "'";
            dt = DBHelper.ExecuteQueryDT(sql);
            return dt;
        }
        //获取支出情况
        public DataTable getPurCostAndItem(string purName)
        {
            sql = @"select AddProportionCount as cost from  Hanmote_MtGroupPositionSheet where  PurGroupName='"+purName+"' order by AddProportionCount;";
            dt = DBHelper.ExecuteQueryDT(sql);
            return dt;


        }

        public DataTable getMtGroupImportedData(string purOrgId)
        {
            sql = @"SELECT
	                        mt.mtLevel AS 等级,
	                        mt.mtScore AS '评分',
							mt.Material_ID,
	                        mt.Material_Name AS 'MtGroupName',
	                        sheet.Proportion AS '支出金额',
	                        sheet.AddProportionCount AS '累计支出' 
                        FROM
	                        (
                        SELECT
													info.PurOrgId,
	                        info.mtLevel,
	                        m.Material_Group,
	                        m.Material_ID,
	                        m.Material_Name,
	                        info.mtScore 
                        FROM
	                        Han_MtPosInfo info,
	                        Material m 
                        WHERE
	                        info.PurOrgId=@purOrgId 
	                        AND info.MtId= m.Material_ID 
	                        ) mt,
	                        Hanmote_MtGroupPositionSheet sheet 
                        WHERE
	                        sheet.MtGroupCode = mt.Material_Group and sheet.PurGroupCode=mt.PurOrgId";
            SqlParameter[] sqlParameter = new SqlParameter[] {
                new SqlParameter("@purOrgId",purOrgId),
                
            };
            dt = DBHelper.ExecuteQueryDT(sql, sqlParameter);
            return dt;
        }

        public DataTable getMtInfo(string purOrgId, string mtGroupId)
        {
            sql = @"SELECT
	                    Material_ID AS '物料编号',
	                    Material_Name AS '物料名称',
                    CASE
	                    Material_Purchase_Type 
	                    WHEN '1' THEN
	                    '生产性物料' 
	                    WHEN '0' THEN
	                    '非生产性物料' ELSE '其他' 
                    END 
                    as '类别'
                    FROM
	                    Material where Material_Group='"+ mtGroupId + "'";
            dt = DBHelper.ExecuteQueryDT(sql);
            return dt;
        }

        /// <summary>
        /// 物料组评估子项
        /// </summary>
        /// <param name="mtItemScoreInfoModle"></param>
        /// <returns></returns>
        public Boolean updateMtItemScoreInfo(MtItemScoreInfoModle mtItemScoreInfoModle)
        {
            Boolean flag = false;
            sql = @"IF EXISTS ( SELECT * FROM [dbo].[MTGAttributeDefinition] WHERE aimId=@aimId and PurOrgID=@purOrgID ) UPDATE MTGAttributeDefinition 
                    SET Goals = @Goals,
                    AimsScore = @AimsScore,
                    aimLevel = @aimLevel,
                    RisKScore =@RisKScore,
                    riskText = @riskText,
                    RiskLevel = @RiskLevel,
                    chanceText = @chanceText,
                    chanceScore = @chanceScore,
                    chanceleve = @chanceleve
                    WHERE
	                   aimId=@aimId and PurOrgID=@purOrgID
                    ELSE 
                        INSERT INTO MTGAttributeDefinition ( aimId,purOrgID, PurOrgName, id, MTGName,Category,Aims,Goals, AimsScore, aimLevel, RisKScore, riskText, RiskLevel, chanceText, chanceScore, chanceLeve )
                            VALUES
	                    ( @aimId,@purOrgID, @PurOrgName, @id, @MTGName, @Category, @Aims, @Goals, @AimsScore, @aimLevel, @RisKScore, @riskText, @RiskLevel, @chanceText,@chanceScore,@chanceLeve )";

            SqlParameter[] sqlParameter = new SqlParameter[] {
                new SqlParameter("@AutoId",mtItemScoreInfoModle.AutoId1),
                new SqlParameter("@purOrgID",mtItemScoreInfoModle.PurOrgId),
                new SqlParameter("@PurOrgName",mtItemScoreInfoModle.PurOrgName),
                new SqlParameter("@id",mtItemScoreInfoModle.Id),
                new SqlParameter("@MTGName",mtItemScoreInfoModle.MtName),
                new SqlParameter("@Category",mtItemScoreInfoModle.Category1),
                new SqlParameter("@Aims",mtItemScoreInfoModle.Aim),
                new SqlParameter("@Goals",mtItemScoreInfoModle.Global),
                new SqlParameter("@AimsScore",mtItemScoreInfoModle.InfluenScore),
                new SqlParameter("@aimLevel",mtItemScoreInfoModle.InfluenLevel),
                new SqlParameter("@RisKScore",mtItemScoreInfoModle.RiskScore),
                new SqlParameter("@riskText",mtItemScoreInfoModle.RiskText),
                new SqlParameter("@RiskLevel",mtItemScoreInfoModle.RiskLevel),
                new SqlParameter("@chanceText",mtItemScoreInfoModle.ChanceText),
                new SqlParameter("@chanceScore",mtItemScoreInfoModle.ChanceScore),
                new SqlParameter("@chanceLeve",mtItemScoreInfoModle.ChanceLevel),
                new SqlParameter("@aimId",mtItemScoreInfoModle.AimId),
            };


            int n = DBHelper.ExecuteNonQuery(sql,sqlParameter);
            if (n > 0) {
                flag = true;

            }
            return flag;
        }

        public string getQualityClass(string mtId)
        {
            string str = @"select qualityClass from Material WHERE Material_ID='"+ mtId + "'";
           DataTable dt= DBHelper.ExecuteQueryDT(str);
            if (dt.Rows.Count != 0) {
                return dt.Rows[0][0].ToString();
            }
            return "";
        }

        /// <summary>
        /// 获取物料的供应目标和指标
        /// </summary>
        /// <param name="id">物料编号</param>
        /// <param name="purOrgName"></param>
        /// <returns></returns>
        public DataTable getMtItemInfo(string id, string purOrgId, string type)
        {
            if (type.Equals("生产性物料")) {
                sql = @"SELECT
                    domain as '领域',
	                indicatorId as '供应指标编号',
	                indicatorText AS '供应目标',
	                indicatorScore AS '指标',
	                levelValue AS '影响力得分',
	                pipLevel AS '影响力PIP等级',
	                riskText AS '风险',
	                RisKScore AS '供应风险得分',
	                RiskLevel AS '供应风险等级',
	                chanceText,
	                chanceScore,
	                chanceLeve 
                FROM
	                TabIndicator s
	                LEFT JOIN ( SELECT * FROM MTGAttributeDefinition WHERE purOrgID = @purOrgId AND id = @id ) m ON s.indicatorId = m.aimId 
                WHERE
	                s.materialId =@id ";
            }
            if (type.Equals("非生产性物料"))
            {
                sql = @"SELECT
                    domain as '领域',
                    GoalId as '供应指标编号',
	                GoalText AS '供应目标',
	                GoalValue AS '指标',
	                GoalLevelValue AS '影响力得分',
	                GoalPipLevel AS '影响力PIP等级',
	                riskText AS '风险',
	                RisKScore AS '供应风险得分',
	                RiskLevel AS '供应风险等级',
	                chanceText ,
	                chanceScore,
	                chanceLeve 
                FROM
                TabAssertAndMainMtGoal s
	                LEFT JOIN ( SELECT * FROM MTGAttributeDefinition WHERE purOrgID = @purOrgId AND id = @id ) m ON s.GoalId = m.aimId 
                WHERE
	                s.MtGroupId = @id";
            }
            SqlParameter[] sqlParameters = new SqlParameter[] {
                new SqlParameter("@purOrgId",purOrgId),
                new SqlParameter("@id",id),
            };
            dt = DBHelper.ExecuteQueryDT(sql, sqlParameters);
            return dt;
        }



        public DataTable getMtGroupFinished(string purOrgName)
        {
            sql = @"select riskScore,influenceScore,mtName  from Han_MtPosInfo  where PurOrgName='"+purOrgName+"' and  riskScore is not null and influenceScore is not null";
            dt = DBHelper.ExecuteQueryDT(sql);
            return dt;

        }

        public DataTable mtGroupSearch(string mtName, string purOrgName)
        {
            sql = @"select distinct id as 物料组编号,MTGName as 物料组名称,Category as 类别 from MTGAttributeDefinition where PurOrgName='" + purOrgName + "' and MTGName like'%"+mtName+"%'";
            dt = DBHelper.ExecuteQueryDT(sql);
            return dt;
        }
        /// <summary>
        /// 加载物料组信息
        /// </summary>
        /// <param name="purOrgId"></param>
        /// <returns></returns>
        public DataTable getMtGroupInfo(string purOrgId)
        {
            sql = @"select  MtGroup_ID,MtGroup_Name from Porg_MtGroup_Relationship  where Porg_ID='"+ purOrgId + "'";
            dt = DBHelper.ExecuteQueryDT(sql);
            return dt;
        }

        public DataTable SearchMtGroupByName(string mtName,string purOrgId)
        {
            string str = "";
            if (!mtName.Equals("")) {

                str = " and MtGroupName like'%" + mtName + "%' ";

            } 
            sql = @"select mtId as Material_ID, MtGroupName as 物料,InfluAndRisk as 影响风险评分,Cost as 支出金额,PosLevel as '优先级',ClassifyType as '区分类型' from Han_MtPosResult  where PurOrgId='" + purOrgId + "'" + str+ " ORDER BY InfluAndRisk desc";
           
            dt = DBHelper.ExecuteQueryDT(sql);
            return dt;
        }

        public int insertLastPosResultInfo(PosResultInfoModel posResultInfoModel)
        {

            int flag = 0;
            SqlParameter[] sqlParameter = new SqlParameter[] {
                new SqlParameter("@MtId",posResultInfoModel.MtId),
                new SqlParameter("@Material_Name",posResultInfoModel.MtName),
                new SqlParameter("@PurOrgId",posResultInfoModel.PurOrgId),
                new SqlParameter("@Cost",posResultInfoModel.Cost),
                new SqlParameter("@InfluAndRisk",posResultInfoModel.InfluAndRiskScore),
                new SqlParameter("@PosLevel",posResultInfoModel.PosResult),
                new SqlParameter("@ClassifyType",posResultInfoModel.ClassifyType)
            };
            sql = @" if EXISTS (select * from Han_MtPosResult WHERE  PurOrgId=@PurOrgId and MtId=@MtId)
		update Han_MtPosResult set InfluAndRisk=@InfluAndRisk,Cost=@Cost,PosLevel=@PosLevel,ClassifyType=@ClassifyType  where PurOrgId=@PurOrgId  and MtId=@MtId
else 
	insert into Han_MtPosResult(PurOrgId,MtId,MtGroupName,InfluAndRisk,Cost,PosLevel,ClassifyType) values(@PurOrgId,@MtId,@Material_Name,@InfluAndRisk,@Cost,@PosLevel,@ClassifyType)";

            flag = DBHelper.ExecuteNonQuery(sql, sqlParameter);

            return flag;

        }

        public DataTable getMtGroupResultData(string id, string PurOrgID)
        {
            sql = @"SELECT
	                                * 
                                FROM
	                                (
                                SELECT DISTINCT
	                                aimLevel,
	                                AimsScore 
                                FROM
	                                MTGAttributeDefinition 
                                WHERE
	                                AimsScore IN ( SELECT MAX ( AimsScore ) FROM MTGAttributeDefinition WHERE id =@id AND PurOrgID =@PurOrgID ) 
	                                AND id =@id 
	                                AND PurOrgID =@PurOrgID 
	                                ) t1,
	                                (
                                SELECT DISTINCT
	                                RiskLevel,
	                                RisKScore 
                                FROM
	                                MTGAttributeDefinition 
                                WHERE
	                                RisKScore IN ( SELECT MAX ( RisKScore ) FROM MTGAttributeDefinition WHERE id =@id AND PurOrgID =@PurOrgID ) 
	                                AND id =@id 
	                                AND PurOrgID =@PurOrgID 
	                                ) t2,
	                                (
                                SELECT DISTINCT
	                                chanceScore,
	                                chanceLeve 
                                FROM
	                                MTGAttributeDefinition 
                                WHERE
	                                RisKScore IN ( SELECT MAX ( chanceScore ) FROM MTGAttributeDefinition WHERE id =@id AND PurOrgID =@PurOrgID ) 
	                                AND id =@id 
	                                AND PurOrgID =@PurOrgID 
	                                ) t3";
            SqlParameter[] sqlParameter = new SqlParameter[] {
                new SqlParameter("@id",id),
                new SqlParameter("@PurOrgID",PurOrgID)
            };

            dt = DBHelper.ExecuteQueryDT(sql, sqlParameter);
            return dt;
        }

        public int insertIntoPosResInfo(double resultPosScore, string resultLevel, string mtId, string purOrgId, string riskScore, string influenceScore,string mtName,float chanceScore)
        {
       
            SqlParameter[] sqlParameter = new SqlParameter[] {
                new SqlParameter("@mtId",mtId),
                new SqlParameter("@purOrgId",purOrgId),
                new SqlParameter("@resultPosScore",resultPosScore),
                new SqlParameter("@resultLevel",resultLevel),
                new SqlParameter("@riskScore",riskScore),
                new SqlParameter("@influenceScore",influenceScore),
                new SqlParameter("@mtName",mtName),
                new SqlParameter("@chanceScore",chanceScore)
            };
            sql = @"IF EXISTS ( SELECT * FROM  Han_MtPosInfo WHERE purOrgId = @purOrgId AND MtId = @mtId ) UPDATE Han_MtPosInfo 
                        SET mtScore =@resultPosScore ,
                        mtLevel = @resultLevel,
                        riskScore = @riskScore,
                        influenceScore = @influenceScore, chanceScore =@chanceScore
                        WHERE
	                        purOrgId = @purOrgId 
	                        AND MtId = @mtId ELSE INSERT INTO Han_MtPosInfo ( purOrgId, MtId, mtName,mtScore, mtLevel, riskScore, influenceScore, chanceScore )
                        VALUES
	                        ( @purOrgId, @mtId, @mtName, @resultPosScore, @resultLevel, @riskScore, @influenceScore, @chanceScore)";
            int flag = DBHelper.ExecuteNonQuery(sql, sqlParameter);
            return flag;
        }

        /// <summary>
        /// 获取评估子项
        /// </summary>
        /// <param name="id">物料组id</param>
        /// <param name="purOrgName">采购组名称</param>
        /// <returns></returns>
        public DataTable getMtGroupData(String id,String purOrgName)
        {
            sql = @"SELECT Area as 领域, Aims AS 供应目标, Goals AS 指标,riskText as 风险,
                        (CASE WHEN AimsScore IS NULL THEN '待评估' else AimsScore END) AS 影响力得分, 
                        (CASE WHEN RiskScore IS NULL THEN '待评估' else RiskScore END) AS 供应风险得分,
                        (CASE WHEN aimLevel IS NULL THEN '待评估' else aimLevel END) AS 影响力PIP等级,
                        (CASE WHEN RiskLevel IS NULL THEN '待评估' else RiskLevel END) AS 供应风险等级
                    FROM      MTGAttributeDefinition
                    WHERE   (id = '" + id+"')  and PurOrgName='"+purOrgName+"'";
            dt = DBHelper.ExecuteQueryDT(sql);
            return dt;
        }

        public DataTable getMtGroupFinishData(String id,String purOrgName)
        {
            // sql = @"select * from MTGAttributeDefinition where (id = '" + id + "') and PurOrgName='"+ purOrgName + "' and AimsScore is not null and RisKScore is not null";
            sql = @"select * from MTGAttributeDefinition where (id = '" + id + "') and PurOrgName='" + purOrgName + "'";
            dt = DBHelper.ExecuteQueryDT(sql);
            return dt;
        }



    }
}
