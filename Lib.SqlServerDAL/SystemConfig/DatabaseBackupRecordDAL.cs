﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Sql;
using System.Data;
using System.Data.SqlClient;

using Lib.IDAL.SystemConfig;
using Lib.Model.SystemConfig;

namespace Lib.SqlServerDAL.SystemConfig
{
    public class DatabaseBackupRecordDAL : DatabaseBackupRecordIDAL
    {
        /// <summary>
        /// 查询所有的备份记录
        /// </summary>
        /// <returns></returns>
        public DataTable queryAllBackupRecord()
        {
            String sqlText = @"select A.ID,A.DB_Name,A.Backup_Time,A.Backup_Operator,A.Backup_Path,B.Username as Backup_Operator_Name
                               from DatabaseBackupRecord A,Base_User B
                               where A.Backup_Operator=B.User_ID
                               order by A.Backup_Time DESC";
            return DBHelper.ExecuteQueryDT(sqlText);
        }

        /// <summary>
        /// 根据记录ID查询备份记录
        /// </summary>
        /// <param name="recordModel"></param>
        /// <returns></returns>
        public DataTable queryBackupRecordById(DatabaseBackupRecordModel recordModel)
        {
            String sqlText = @"select A.ID,A.DB_Name,A.Backup_Time,A.Backup_Operator,A.Backup_Path,B.Username as Backup_Operator_Name
                               from DatabaseBackupRecord A,Base_User B
                               where A.Backup_Operator=@ID AND A.Backup_Operator=B.User_ID
                               order by A.Backup_Time DESC";
            SqlParameter[] sqlParameter = new SqlParameter[] { 
                new SqlParameter("@ID",SqlDbType.Int)
            };
            sqlParameter[0].Value = recordModel.ID;

            return DBHelper.ExecuteQueryDT(sqlText,sqlParameter);
        }

        /// <summary>
        /// 插入新备份记录到DB
        /// </summary>
        /// <param name="recordModel"></param>
        public void insertBackupRecord(DatabaseBackupRecordModel recordModel)
        {
            String sqlText = @"insert into DatabaseBackupRecord(DB_Name,Backup_Time,Backup_Operator,Backup_Path,Backup_Size,Backup_Type,Backup_Status) 
                               values(@DB_Name,@Backup_Time,@Backup_Operator,@Backup_Path,@Backup_Size,@Backup_Type,@Backup_Status)";
            SqlParameter[] sqlParameter = new SqlParameter[] { 
                new SqlParameter("@DB_Name",SqlDbType.VarChar),
                new SqlParameter("@Backup_Time",SqlDbType.DateTime),
                new SqlParameter("@Backup_Operator",SqlDbType.VarChar),
                new SqlParameter("@Backup_Path",SqlDbType.NVarChar),
                new SqlParameter("@Backup_Size",SqlDbType.BigInt),
                new SqlParameter("@Backup_Type",SqlDbType.Int),
                new SqlParameter("@Backup_Status",SqlDbType.Int)
            };
            sqlParameter[0].Value = recordModel.DB_Name;
            sqlParameter[1].Value = recordModel.Backup_Time;
            sqlParameter[2].Value = recordModel.Backup_Operator;
            sqlParameter[3].Value = recordModel.Backup_Path;
            sqlParameter[4].Value = recordModel.Backup_Size;
            sqlParameter[5].Value = recordModel.Backup_Type;
            sqlParameter[6].Value = recordModel.Backup_Status;

            DBHelper.ExecuteNonQuery(sqlText,sqlParameter);
        }

        /// <summary>
        /// 更新备份记录
        /// </summary>
        /// <param name="recordModel"></param>
        public void updateBackupRecordById(DatabaseBackupRecordModel recordModel)
        {
            String sqlText = @"update DatabaseBackupRecord set DB_Name=@DB_Name,
                                                               Backup_Time=@Backup_Time,
                                                               Backup_Operator=@Backup_Operator,
                                                               Backup_Path=@Backup_Path,
                                                               Backup_Size=@Backup_Size,
                                                               Backup_Type=@Backup_Type,
                                                               Backup_Status=@Backup_Status where ID=@ID";
            SqlParameter[] sqlParameter = new SqlParameter[] {
                new SqlParameter("@DB_Name",SqlDbType.VarChar),
                new SqlParameter("@Backup_Time",SqlDbType.DateTime),
                new SqlParameter("@Backup_Operator",SqlDbType.Int),
                new SqlParameter("@Backup_Path",SqlDbType.NVarChar),
                new SqlParameter("@Backup_Size",SqlDbType.BigInt),
                new SqlParameter("@Backup_Type",SqlDbType.Int),
                new SqlParameter("@Backup_Status",SqlDbType.Int),
                new SqlParameter("@ID",SqlDbType.Int)
            };
            sqlParameter[0].Value = recordModel.DB_Name;
            sqlParameter[1].Value = recordModel.Backup_Time;
            sqlParameter[2].Value = recordModel.Backup_Operator;
            sqlParameter[3].Value = recordModel.Backup_Path;
            sqlParameter[4].Value = recordModel.Backup_Size;
            sqlParameter[5].Value = recordModel.Backup_Type;
            sqlParameter[6].Value = recordModel.Backup_Status;
            sqlParameter[7].Value = recordModel.ID;

            DBHelper.ExecuteNonQuery(sqlText, sqlParameter);
        }

        /// <summary>
        /// 删除备份记录
        /// </summary>
        /// <param name="recordModel"></param>
        public void deleteBackupRecordById(DatabaseBackupRecordModel recordModel)
        {
            String sqlText = "delete from DatabaseBackupRecord where ID=@ID";
            SqlParameter[] sqlParameter = new SqlParameter[] {
                new SqlParameter("@ID",SqlDbType.Int)
            };
            sqlParameter[0].Value = recordModel.ID;

            DBHelper.ExecuteNonQuery(sqlText,sqlParameter);
        }
    }
}
