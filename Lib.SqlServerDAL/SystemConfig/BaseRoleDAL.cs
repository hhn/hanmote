﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using Lib.IDAL.SystemConfig;
using Lib.Model.SystemConfig;

namespace Lib.SqlServerDAL.SystemConfig
{
    public class BaseRoleDAL : BaseRoleIDAL
    {
        /// <summary>
        /// 查找最大的角色编号
        /// </summary>
        /// <returns></returns>
        public DataTable queryBiggestRoleID(String today)
        {
            String sqlText = "select MAX(Role_ID) from Base_Role where Role_ID LIKE @Role_ID";
            SqlParameter[] sqlParameter = new SqlParameter[] {
                new SqlParameter("@Role_ID",SqlDbType.VarChar)
            };
            sqlParameter[0].Value = today + "%";

            return DBHelper.ExecuteQueryDT(sqlText,sqlParameter);
        }

        /// <summary>
        /// 根据角色编号查询角色信息
        /// </summary>
        /// <param name="roleModel"></param>
        public DataTable queryRoleInforById(BaseRoleModel roleModel)
        {
            String sqlText = "select * from Base_Role where Role_ID=@Role_ID";
            SqlParameter[] sqlParameter = new SqlParameter[] {
                new SqlParameter("@Role_ID",SqlDbType.VarChar)
            };
            sqlParameter[0].Value = roleModel.Role_ID;

            return DBHelper.ExecuteQueryDT(sqlText,sqlParameter);
        }

        /// <summary>
        /// 更新角色信息
        /// </summary>
        /// <param name="roleModel"></param>
        public void updateRoleInforById(BaseRoleModel roleModel)
        {
            String sqlText = @"update Base_Role set Role_Name=@Role_Name,Description=@Description
                                                where Role_ID=@Role_ID";
            SqlParameter[] sqlParameter = new SqlParameter[] {
                new SqlParameter("@Role_Name",SqlDbType.VarChar),
                new SqlParameter("@Description",SqlDbType.VarChar),
                new SqlParameter("@Role_ID",SqlDbType.VarChar)
            };
            sqlParameter[0].Value = roleModel.Role_Name;
            sqlParameter[1].Value = roleModel.Description;
            sqlParameter[2].Value = roleModel.Role_ID;

            DBHelper.ExecuteNonQuery(sqlText,sqlParameter);
        }

        /// <summary>
        /// 插入角色信息
        /// </summary>
        /// <param name="roleModel"></param>
        public void insertRole(BaseRoleModel roleModel)
        {
            String sql1 = "update Base_Role set Right_Value=Right_Value+2 where Right_Value>=@Right_Value";
            String sql2 = "update Base_Role set Left_Value=Left_Value+2 where Left_Value>=@Left_Value";
            String sql3 = @"insert into Base_Role(Role_ID,Role_Name,Description,Left_Value,Right_Value,Level,Compang_ID)
                                       values(@Role_ID,@Role_Name,@Description,@Left_Value,@Right_Value,@Level,@Compang_ID)";

            SqlParameter[] sqlParameter1 = new SqlParameter[] {
                new SqlParameter("@Right_Value",SqlDbType.Int)
            };
            sqlParameter1[0].Value = roleModel.Left_Value;

            SqlParameter[] sqlParameter2 = new SqlParameter[] {
                new SqlParameter("@Left_Value",SqlDbType.Int)
            };
            sqlParameter2[0].Value = roleModel.Left_Value;

            SqlParameter[] sqlParameter3 = new SqlParameter[] { 
                new SqlParameter("@Role_ID",SqlDbType.VarChar),
                new SqlParameter("@Role_Name",SqlDbType.VarChar),
                new SqlParameter("@Description",SqlDbType.VarChar),
                new SqlParameter("@Left_Value",SqlDbType.Int),
                new SqlParameter("@Right_Value",SqlDbType.Int),
                new SqlParameter("@Level",SqlDbType.Int),
                new SqlParameter("@Compang_ID",SqlDbType.VarChar)
            };
            sqlParameter3[0].Value = roleModel.Role_ID;
            sqlParameter3[1].Value = roleModel.Role_Name;
            sqlParameter3[2].Value = roleModel.Description;
            sqlParameter3[3].Value = roleModel.Left_Value;
            sqlParameter3[4].Value = roleModel.Right_Value;
            sqlParameter3[5].Value = roleModel.Level;
            sqlParameter3[6].Value = roleModel.Compang_ID;

            List<String> sqlList = new List<String> {sql1,sql2,sql3 };
            List<SqlParameter[]> sqlParameterList = new List<SqlParameter[]>{sqlParameter1,sqlParameter2,sqlParameter3};

            DBHelper.ExecuteNonQuery(sqlList, sqlParameterList);
        }

        /// <summary>
        /// 查询所有的角色信息
        /// </summary>
        /// <returns></returns>
        public DataTable queryAllRoleInfor()
        {
            String sql = "select * from Base_Role order by Level asc";
            return DBHelper.ExecuteQueryDT(sql);
        }

        /// <summary>
        /// 查询当前角色及所有子角色信息
        /// </summary>
        /// <param name="roleModel"></param>
        /// <returns></returns>
        public DataTable queryOffspringInfor(BaseRoleModel roleModel)
        {
            String sql = "select * from Base_Role where Left_Value>=@Left_Value and Right_Value<=@Right_Value";
            SqlParameter[] sqlParameter = new SqlParameter[] {
                new SqlParameter("@Left_Value",SqlDbType.Int),
                new SqlParameter("@Right_Value",SqlDbType.Int)
            };
            sqlParameter[0].Value = roleModel.Left_Value;
            sqlParameter[1].Value = roleModel.Right_Value;
            return DBHelper.ExecuteQueryDT(sql,sqlParameter);
        }

        /// <summary>
        /// 查询当前角色的子角色信息（不包括当前角色）
        /// </summary>
        /// <param name="roleModel"></param>
        /// <returns></returns>
        public DataTable queryOffspringInforWithoutSelf(BaseRoleModel roleModel)
        {
            String sql = "select * from Base_Role where Left_Value>@Left_Value and Right_Value<@Right_Value";
            SqlParameter[] sqlParameter = new SqlParameter[] {
                new SqlParameter("@Left_Value",SqlDbType.Int),
                new SqlParameter("@Right_Value",SqlDbType.Int)
            };
            sqlParameter[0].Value = roleModel.Left_Value;
            sqlParameter[1].Value = roleModel.Right_Value;
            return DBHelper.ExecuteQueryDT(sql, sqlParameter);
        }

        /// <summary>
        /// 根据角色编号查询其关联的用户信息
        /// </summary>
        /// <param name="roleModel"></param>
        /// <returns></returns>
        public DataTable queryUserInforByRoleId(BaseRoleModel roleModel)
        {
            String sql = @"SELECT
	                            A.User_ID,
	                            A.Username,
	                            CASE
                            WHEN A.Gender = 0
                            THEN '男'
                            WHEN A.Gender = 1
                            THEN '女'
                            END AS Gender,
                             A.Birthday,
                             A.Mobile
                            FROM
	                            Hanmote_Base_User A,
	                            Join_User_Role B
                            WHERE B.Role_ID=@Role_ID and A.User_ID=B.User_ID";
            SqlParameter[] sqlParameter = new SqlParameter[] {
                new SqlParameter("@Role_ID",SqlDbType.VarChar)
            };
            sqlParameter[0].Value = roleModel.Role_ID;
            return DBHelper.ExecuteQueryDT(sql,sqlParameter);
        }

        /// <summary>
        /// 删除某角色和用户的关联
        /// </summary>
        /// <param name="userRoleModel"></param>
        public void deleteUserRoleLinkByRoleId(JoinUserRoleModel userRoleModel)
        {
            String sql = "delete from Join_User_Role where Role_ID=@Role_ID";
            SqlParameter[] sqlParameter = new SqlParameter[] {
                new SqlParameter("@Role_ID",SqlDbType.VarChar)
            };
            sqlParameter[0].Value = userRoleModel.Role_ID;
            DBHelper.ExecuteNonQuery(sql,sqlParameter);
        }

        /// <summary>
        /// 删除数据库中当前节点及所有子节点并更新受影响节点的左右值
        /// </summary>
        /// <param name="roleModel"></param>
        public void deleteOffspringOrganizationAndUpdateNodes(BaseRoleModel roleModel)
        {
            String sql1 = "delete from Base_Role where Left_Value>=@Left_Value and Right_Value<=@Right_Value";
            String sql2 = "update Base_Role set Right_Value=Right_Value-@width where Right_Value>@Right_Value";
            String sql3 = "update Base_Role set Left_Value=Left_Value-@width where Left_Value>@Right_Value";
        
            SqlParameter []sqlParameter1 = new SqlParameter[]{
                new SqlParameter("@Left_Value",SqlDbType.Int),
                new SqlParameter("@Right_Value",SqlDbType.Int)
            };
            sqlParameter1[0].Value = roleModel.Left_Value;
            sqlParameter1[1].Value = roleModel.Right_Value;

            SqlParameter[] sqlParameter2 = new SqlParameter[]{
                new SqlParameter("@width",SqlDbType.Int),
                new SqlParameter("@Right_Value",SqlDbType.Int)
            };
            sqlParameter2[0].Value = roleModel.Right_Value - roleModel.Left_Value + 1;
            sqlParameter2[1].Value = roleModel.Right_Value;

            SqlParameter[] sqlParameter3 = new SqlParameter[]{
                new SqlParameter("@width",SqlDbType.Int),
                new SqlParameter("@Right_Value",SqlDbType.Int)
            };
            sqlParameter3[0].Value = roleModel.Right_Value - roleModel.Left_Value + 1;
            sqlParameter3[1].Value = roleModel.Right_Value;

            List<String> sqlList = new List<String> {sql1,sql2,sql3 };
            List<SqlParameter[]> sqlParameterList = new List<SqlParameter[]> { sqlParameter1,sqlParameter2,sqlParameter3 };

            DBHelper.ExecuteNonQuery(sqlList,sqlParameterList);
        }

        /// <summary>
        /// 插入用户和角色关联
        /// </summary>
        /// <param name="userRoleModel"></param>
        public void insertUserRoleLink(JoinUserRoleModel userRoleModel)
        {
            String sql = @"insert into Join_User_Role(Role_ID,Role_Name,User_ID,Username)
                                       values(@Role_ID,@Role_Name,@User_ID,@Username)";
            SqlParameter[] sqlParameter = new SqlParameter[] {
                new SqlParameter("@Role_ID",SqlDbType.VarChar),
                new SqlParameter("@Role_Name",SqlDbType.VarChar),
                new SqlParameter("@User_ID",SqlDbType.VarChar),
                new SqlParameter("@Username",SqlDbType.VarChar)
            };
            sqlParameter[0].Value = userRoleModel.Role_ID;
            sqlParameter[1].Value = userRoleModel.Role_Name;
            sqlParameter[2].Value = userRoleModel.User_ID;
            sqlParameter[3].Value = userRoleModel.Username;
            DBHelper.ExecuteNonQuery(sql,sqlParameter);
        }

    }
}
