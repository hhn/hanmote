﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using Lib.Model.SystemConfig;
using Lib.IDAL.SystemConfig;

namespace Lib.SqlServerDAL.SystemConfig
{
    public class JoinUserRoleDAL : JoinUserRoleIDAL
    {
        /// <summary>
        /// 检查用户和角色联系是否已存在
        /// </summary>
        /// <param name="userRoleModel"></param>
        public DataTable checkUserRoleLinkExist(JoinUserRoleModel userRoleModel)
        {
            String sqlText = "select User_Role_ID from Join_User_Role where User_ID=@User_ID and Role_ID=@Role_ID";
            SqlParameter[] sqlParameter = new SqlParameter[] {
                new SqlParameter("@User_ID",SqlDbType.VarChar),
                new SqlParameter("@Role_ID",SqlDbType.VarChar)
            };
            sqlParameter[0].Value = userRoleModel.User_ID;
            sqlParameter[1].Value = userRoleModel.Role_ID;

            return DBHelper.ExecuteQueryDT(sqlText,sqlParameter);
        }

        /// <summary>
        /// 添加角色给用户，即写入用户和角色的联系
        /// </summary>
        /// <param name="userRoleModel"></param>
        public void addRoleToUser(JoinUserRoleModel userRoleModel)
        {
            String sqlText = "insert into Join_User_Role(User_ID,Username,Role_ID,Role_Name) values(@User_ID,@Username,@Role_ID,@Role_Name)";
            SqlParameter[] sqlParameter = new SqlParameter[] {
                new SqlParameter("@User_ID",SqlDbType.VarChar),
                new SqlParameter("@Username",SqlDbType.VarChar),
                new SqlParameter("@Role_ID",SqlDbType.VarChar),
                new SqlParameter("@Role_Name",SqlDbType.VarChar)
            };
            sqlParameter[0].Value = userRoleModel.User_ID;
            sqlParameter[1].Value = userRoleModel.Username;
            sqlParameter[2].Value = userRoleModel.Role_ID;
            sqlParameter[3].Value = userRoleModel.Role_Name;

            DBHelper.ExecuteNonQuery(sqlText,sqlParameter);
        }

        /// <summary>
        /// 从用户收回角色，即移除用户和角色间联系
        /// </summary>
        /// <param name="userRoleModel"></param>
        public void removeRoleFromUser(JoinUserRoleModel userRoleModel)
        {
            String sqlText = "delete from Join_User_Role where User_ID=@User_ID and Role_ID=@Role_ID";
            SqlParameter[] sqlParameter = new SqlParameter[] {
                new SqlParameter("@User_ID",SqlDbType.VarChar),
                new SqlParameter("@Role_ID",SqlDbType.VarChar)
            };
            sqlParameter[0].Value = userRoleModel.User_ID;
            sqlParameter[1].Value = userRoleModel.Role_ID;

            DBHelper.ExecuteNonQuery(sqlText, sqlParameter);
        }

        /// <summary>
        /// 查询用户所具有的角色
        /// </summary>
        /// <param name="userRoleModel"></param>
        public DataTable queryRoleByUserId(JoinUserRoleModel userRoleModel)
        {
            String sqlText = "select * from Join_User_Role A,Base_Role B where A.User_ID=@User_ID and A.Role_ID=B.Role_ID";
            SqlParameter[] sqlParameter = new SqlParameter[] {
                new SqlParameter("@User_ID",SqlDbType.VarChar)
            };
            sqlParameter[0].Value = userRoleModel.User_ID;

            return DBHelper.ExecuteQueryDT(sqlText,sqlParameter);
        }

    }
}
