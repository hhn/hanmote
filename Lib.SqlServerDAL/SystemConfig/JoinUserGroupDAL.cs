﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using Lib.Model.SystemConfig;
using Lib.IDAL.SystemConfig;

namespace Lib.SqlServerDAL.SystemConfig
{
    public class JoinUserGroupDAL : JoinUserGroupIDAL
    {
        /// <summary>
        /// 检查用户和组联系是否已存在
        /// </summary>
        /// <returns></returns>
        public DataTable checkUserGroupLinkExist(JoinUserGroupModel joinUserGroupModel)
        {
            String sqlText = "select User_Group_ID from Join_User_Group where User_ID=@User_ID and Group_ID=@Group_ID";
            SqlParameter[] sqlParameter = new SqlParameter[] {
                new SqlParameter("@User_ID",SqlDbType.VarChar),
                new SqlParameter("@Group_ID",SqlDbType.VarChar)
            };
            sqlParameter[0].Value = joinUserGroupModel.User_ID;
            sqlParameter[1].Value = joinUserGroupModel.Group_ID;

            return DBHelper.ExecuteQueryDT(sqlText,sqlParameter);
        }

        /// <summary>
        /// 添加用户和组的联系
        /// </summary>
        /// <param name="joinUserGroupModel"></param>
        public void addUserToGroup(JoinUserGroupModel joinUserGroupModel)
        {
            String sqlText = @"insert into Join_User_Group(User_ID,Username,Group_ID,Group_Name)
                               values(@User_ID,@Username,@Group_ID,@Group_Name)";
            SqlParameter[] sqlParameter = new SqlParameter[] {
                new SqlParameter("@User_ID",SqlDbType.VarChar),
                new SqlParameter("@Username",SqlDbType.VarChar),
                new SqlParameter("@Group_ID",SqlDbType.VarChar),
                new SqlParameter("@Group_Name",SqlDbType.VarChar)
            };
            sqlParameter[0].Value = joinUserGroupModel.User_ID;
            sqlParameter[1].Value = joinUserGroupModel.Username;
            sqlParameter[2].Value = joinUserGroupModel.Group_ID;
            sqlParameter[3].Value = joinUserGroupModel.Group_Name;

            DBHelper.ExecuteNonQuery(sqlText,sqlParameter);
        }

        /// <summary>
        /// 从组中删除用户
        /// </summary>
        /// <param name="joinUserGroupModel"></param>
        public void removeUserFromGroup(JoinUserGroupModel joinUserGroupModel)
        {
            String sqlText = "delete from Join_User_Group where User_ID=@User_ID and Group_ID=@Group_ID";
            SqlParameter[] sqlParameter = new SqlParameter[] {
                new SqlParameter("@User_ID",SqlDbType.VarChar),
                new SqlParameter("@Group_ID",SqlDbType.VarChar)
            };
            sqlParameter[0].Value = joinUserGroupModel.User_ID;
            sqlParameter[1].Value = joinUserGroupModel.Group_ID;

            DBHelper.ExecuteNonQuery(sqlText,sqlParameter);
        }

        /// <summary>
        /// 查询用户的所有组信息
        /// </summary>
        /// <param name="joinUserGroupModel"></param>
        /// <returns></returns>
        public DataTable queryGroupByUserId(JoinUserGroupModel joinUserGroupModel)
        {
            String sqlText = "select * from Join_User_Group A,Base_Group B where A.User_ID=@User_ID and A.Group_ID=B.Group_ID";
            SqlParameter[] sqlParameter = new SqlParameter[] {
                new SqlParameter("@User_ID",SqlDbType.VarChar)
            };
            sqlParameter[0].Value = joinUserGroupModel.User_ID;

            return DBHelper.ExecuteQueryDT(sqlText,sqlParameter);
        }

        /// <summary>
        /// 删除一个用户组中的所有用户
        /// </summary>
        /// <param name="joinUserGroupModel"></param>
        public void deleteAllUserOfGroup(JoinUserGroupModel joinUserGroupModel)
        {
            String sql = "delete from Join_User_Group where Group_ID=@Group_ID";
            SqlParameter[] sqlParameter = new SqlParameter[] {
                new SqlParameter("@Group_ID",SqlDbType.VarChar)
            };
            sqlParameter[0].Value = joinUserGroupModel.Group_ID;
            DBHelper.ExecuteNonQuery(sql,sqlParameter);
        }
    }
}
