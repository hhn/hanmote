﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using Lib.IDAL.SystemConfig;
using Lib.Model.SystemConfig;

namespace Lib.SqlServerDAL.SystemConfig
{
    /// <summary>
    /// author:hezl
    /// </summary>
    class CompanyUserDAL : CompanyUserIDAL
    {
       
        CompanyUserModel companyUserModel = new CompanyUserModel();
        /// <summary>
        /// 计算条数
        /// </summary>
        /// <returns></returns>
        public DataTable calculateRecordNumber()
        {
            String sqlText = "select count(User_ID) from Base_User";
            return DBHelper.ExecuteQueryDT(sqlText);
        }
        /// <summary>
        /// add info
        /// </summary>
        /// <param name="companyUserModel"></param>
        public void addCompanyUserInfo(CompanyUserModel companyUserModel)
        {

            String sqlText = @"INSERT INTO [Base_User]([User_ID],[Login_Name],[Login_Password],[Username]
                                                      ,[Gender],[Birthday],[Birth_Place],[ID_Number]
                                                      ,[Mobile],[Email],[User_Description]
                                                      ,[Question],[Answer_Question],[Enabled],[Manager_Flag]
                                                      ,[Generate_Time],[Generate_User_ID],[Generate_Username],[Modify_Time]
                                                      ,[Modify_User_ID],[Modify_Username],[companyID],[companyName])
                               VALUES(@User_ID,@Login_Name,@Login_Password,@Username,
                                      @Gender,@Birthday,@Birth_Place,@ID_Number,
                                      @Mobile,@Email,@User_Description,
                                      @Question,@Answer_Question,@Enabled,@Manager_Flag,
                                      @Generate_Time,@Generate_User_ID,@Generate_Username,@Modify_Time,
                                      @Modify_User_ID,@Modify_Username,@Supplier_Id,@Supplier_Name)";
            SqlParameter[] sqlParameter = new SqlParameter[] {
                new SqlParameter("@User_ID",SqlDbType.VarChar),
                new SqlParameter("@Login_Name",SqlDbType.VarChar),
                new SqlParameter("@Login_Password",SqlDbType.VarChar),
                new SqlParameter("@Username",SqlDbType.VarChar),
                new SqlParameter("@Gender",SqlDbType.Int),
                new SqlParameter("@Birthday",SqlDbType.Date),
                new SqlParameter("@Birth_Place",SqlDbType.VarChar),
                new SqlParameter("@ID_Number",SqlDbType.VarChar),
                new SqlParameter("@Mobile",SqlDbType.VarChar),
                new SqlParameter("@Email",SqlDbType.VarChar),
                new SqlParameter("@User_Description",SqlDbType.VarChar),
                new SqlParameter("@Question",SqlDbType.VarChar),
                new SqlParameter("@Answer_Question",SqlDbType.VarChar),
                new SqlParameter("@Enabled",SqlDbType.Int),
                new SqlParameter("@Manager_Flag",SqlDbType.Int),
                new SqlParameter("@Generate_Time",SqlDbType.DateTime),
                new SqlParameter("@Generate_User_ID",SqlDbType.VarChar),
                new SqlParameter("@Generate_Username",SqlDbType.VarChar),
                new SqlParameter("@Modify_Time",SqlDbType.DateTime),
                new SqlParameter("@Modify_User_ID",SqlDbType.VarChar),
                new SqlParameter("@Modify_Username",SqlDbType.VarChar),
                new SqlParameter("@Supplier_Id",SqlDbType.VarChar),
                new SqlParameter("@Supplier_Name",SqlDbType.VarChar),
              
            };
            sqlParameter[0].Value = companyUserModel.User_ID;
            sqlParameter[1].Value = companyUserModel.Login_Name;
            sqlParameter[2].Value = companyUserModel.Login_Password;
            sqlParameter[3].Value = companyUserModel.Username;
            sqlParameter[4].Value = companyUserModel.Gender;
            sqlParameter[5].Value = companyUserModel.Birthday;
            sqlParameter[6].Value = companyUserModel.Birth_Place;
            sqlParameter[7].Value = companyUserModel.ID_Number;
            sqlParameter[8].Value = companyUserModel.Mobile;
            sqlParameter[9].Value = companyUserModel.Email;
            sqlParameter[10].Value = companyUserModel.User_Description;
            sqlParameter[11].Value = companyUserModel.Question;
            sqlParameter[12].Value = companyUserModel.Answer_Question;
            sqlParameter[13].Value = companyUserModel.Enabled;
            sqlParameter[14].Value = CompanyUserModel.Manager_Flag;
            sqlParameter[15].Value = companyUserModel.Generate_Time;
            sqlParameter[16].Value = companyUserModel.Generate_User_ID;
            sqlParameter[17].Value = companyUserModel.Generate_Username;
            sqlParameter[18].Value = companyUserModel.Modify_Time;
            sqlParameter[19].Value = companyUserModel.Modify_User_ID;
            sqlParameter[20].Value = companyUserModel.Modify_Username;
            sqlParameter[21].Value = CompanyUserModel.Supplier_Id;
            sqlParameter[22].Value = CompanyUserModel.Supplier_Name;
         
            DBHelper.ExecuteNonQuery(sqlText, sqlParameter);
        }
        /// <summary>
        /// del info 逻辑删除
        /// </summary>
        /// <param name="User_ID"></param>
        public void delCompanyUserInfo(String Supplier_Id,string User_ID)
        {
            String sqlText = "delete from  Base_User where User_ID='"+User_ID+"' and companyID='"+Supplier_Id+"'";
            DBHelper.ExecuteNonQuery(sqlText);
        }
        /// <summary>
        /// edit info
        /// </summary>
        /// <param name="companyUserModel"></param>
        public void editCompanyUserIndo(CompanyUserModel companyUserModel)
        {
            String sqlText = @"update Base_User set [Login_Name]=@Login_Name,[Login_Password]=@Login_Password,[Username]=@Username,[Gender]=@Gender,
                                                    [Birthday]=@Birthday,[Birth_Place]=@Birth_Place,[ID_Number]=@ID_Number,[Mobile]=@Mobile,
                                                    [Email]=@Email,[User_Description]=@User_Description,[Question]=@Question,[Answer_Question]=@Answer_Question,
                                                    [Enabled]=@Enabled,[Manager_Flag]=@Manager_Flag,[Modify_Time]=@Modify_Time,[Modify_User_ID]=@Modify_User_ID,[Modify_Username]=@Modify_Username
                               where [User_ID]=@User_ID";
            SqlParameter[] sqlParameter = new SqlParameter[] {
                new SqlParameter("@Login_Name",SqlDbType.VarChar),
                new SqlParameter("@Login_Password",SqlDbType.VarChar),
                new SqlParameter("@Username",SqlDbType.VarChar),
                new SqlParameter("@Gender",SqlDbType.Int),
                new SqlParameter("@Birthday",SqlDbType.VarChar),
                new SqlParameter("@Birth_Place",SqlDbType.VarChar),
                new SqlParameter("@ID_Number",SqlDbType.VarChar),
                new SqlParameter("@Mobile",SqlDbType.VarChar),
                new SqlParameter("@Email",SqlDbType.VarChar),
                new SqlParameter("@User_Description",SqlDbType.VarChar),
                new SqlParameter("@Question",SqlDbType.VarChar),
                new SqlParameter("@Answer_Question",SqlDbType.VarChar),
                new SqlParameter("@Enabled",SqlDbType.Int),
                new SqlParameter("@Manager_Flag",SqlDbType.Int),
                new SqlParameter("@Modify_Time",SqlDbType.DateTime),
                new SqlParameter("@Modify_User_ID",SqlDbType.VarChar),
                new SqlParameter("@Modify_Username",SqlDbType.VarChar),
                new SqlParameter("@User_ID",SqlDbType.VarChar)
            };
            sqlParameter[0].Value = companyUserModel.Login_Name;
            sqlParameter[1].Value = companyUserModel.Login_Password;
            sqlParameter[2].Value = companyUserModel.Username;
            sqlParameter[3].Value = companyUserModel.Gender;
            sqlParameter[4].Value = companyUserModel.Birthday;
            sqlParameter[5].Value = companyUserModel.Birth_Place;
            sqlParameter[6].Value = companyUserModel.ID_Number;
            sqlParameter[7].Value = companyUserModel.Mobile;
            sqlParameter[8].Value = companyUserModel.Email;
            sqlParameter[9].Value = companyUserModel.User_Description;
            sqlParameter[10].Value = companyUserModel.Question;
            sqlParameter[11].Value = companyUserModel.Answer_Question;
            sqlParameter[12].Value = companyUserModel.Enabled;
            sqlParameter[13].Value = CompanyUserModel.Manager_Flag;
            sqlParameter[14].Value = companyUserModel.Modify_Time;
            sqlParameter[15].Value = companyUserModel.Modify_User_ID;
            sqlParameter[16].Value = companyUserModel.Modify_Username;
            sqlParameter[17].Value = companyUserModel.User_ID;

            DBHelper.ExecuteNonQuery(sqlText, sqlParameter);
        }
        /// <summary>
        /// list info
        /// </summary>
        /// <param name="companyID"></param>
        /// <param name="pageNum"></param>
        /// <param name="frontNum"></param>
        /// <returns></returns>
        public DataTable findAllCompanyuUserList(string companyID, int pageNum, int frontNum)
        {
            String sqlText = String.Format(@" select
                                            User_ID,Username ,Login_Name,Email ,Birth_Place,
                                            case 
                                            when Gender ='0' then '男'  
                                            when Gender ='1' then '女'
                                            end as Gender,
                                            Mobile,
                                            case 
                                            when Enabled ='0' then '有效'  
                                            when Enabled ='1' then '无效'
                                            end as Enabled
                                            from Base_User where Manager_Flag = 1 and  companyID='" + companyID+"'", pageNum, frontNum);

            return DBHelper.ExecuteQueryDT(sqlText);
        }
        /// <summary>
        /// 
        /// condition list info
        /// </summary>
        /// <param name="companyID"></param>
        /// <param name="Username"></param>
        /// <param name="Login_Name"></param>
        /// <param name="pageNum"></param>
        /// <param name="frontNum"></param>
        /// <returns></returns>
        public DataTable findAllCompanyListByCondition(string companyName, int pageNum, int frontNum)
        {
            String sqlText = String.Format(@" SELECT
	                                                Supplier_Id AS Supplier_Id,
	                                                Supplier_Name AS Supplier_Name,
	                                                email AS Email,
	                                                ManagerName AS Username,
	                                                mobile AS Mobile,
	                                                CASE
                                                WHEN Enable = '0' THEN
	                                                '在籍'
                                                WHEN Enable = '1' THEN
	                                                '已注销'
                                                END AS Enabled
                                                FROM
	                                                Supplier_MainAccount
");
            if (!String.IsNullOrEmpty(companyName))
            {
                sqlText += " and Supplier_Name like '%" + companyName + "%'";
            }

            return DBHelper.ExecuteQueryDT(sqlText);
        }

        /// <summary>
        /// 统计用户记录数目
        /// </summary>
        /// <returns></returns>
        public int calculateUserNumber()
        {
            DataTable userInforTable = calculateRecordNumber();
            if (userInforTable == null || (userInforTable.Rows.Count > 0 && userInforTable.Rows[0][0].Equals(DBNull.Value)))
            {
                return 0;
            }
            else
            {
                return userInforTable.Rows.Count;
            }
        }
    }
}
