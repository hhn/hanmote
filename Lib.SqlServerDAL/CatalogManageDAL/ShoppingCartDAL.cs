﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.IDAL.CatalogManageIDAL;
using Lib.Model.CatalogPurchase;
using System.Data;
using System.Data.SqlClient;

namespace Lib.SqlServerDAL.CatalogManageDAL
{
    public class ShoppingCartDAL : ShoppingCartIDAL
    {
        public ShoppingCartItem getShoppingCartItem(string shoppingCartID, string materialCatalogID) {
            ShoppingCartItem item = null;


            StringBuilder strBui = new StringBuilder("select * from Shopping_Cart_Item where ")
                .Append("Shopping_Cart_ID = '").Append(shoppingCartID).Append("' and Material_Catalog_ID = '")
                .Append(materialCatalogID).Append("'");
            DataTable dt = DBHelper.ExecuteQueryDT(strBui.ToString());
            foreach (DataRow row in dt.Rows) {
                item = new ShoppingCartItem();
                item.Shopping_Cart_ID = row["Shopping_Cart_ID"].ToString();
                item.Material_Catalog_ID = row["Material_Catalog_ID"].ToString();
                item.Total_Quantity = Convert.ToDouble(row["Total_Quantity"].ToString());
                item.Net_Price = Convert.ToDouble(row["Net_Price"].ToString());
                item.Total_Amount = Convert.ToDouble(row["Total_Amount"].ToString());
                item.Material_Name = row["Material_Name"].ToString();
                item.Supplier_ID = row["Supplier_ID"].ToString();
                item.Is_Valid = row["Is_Valid"].ToString();
                item.Currency = row["Currency"].ToString();
            }

            return item;
        }

        /// <summary>
        /// 根据购物车编号获取所有项目内容
        /// </summary>
        /// <param name="shoppingCartID"></param>
        /// <returns></returns>
        public List<ShoppingCartItem> getShoppingCartItems(string shoppingCartID) {
            List<ShoppingCartItem> items = new List<ShoppingCartItem>();

            StringBuilder strBui = new StringBuilder("select * from Shopping_Cart_Item where ")
                .Append("Shopping_Cart_ID = '").Append(shoppingCartID).Append("'");
            DataTable dt = DBHelper.ExecuteQueryDT(strBui.ToString());
            foreach (DataRow row in dt.Rows)
            {
                ShoppingCartItem item = new ShoppingCartItem();
                item = new ShoppingCartItem();
                item.Shopping_Cart_ID = row["Shopping_Cart_ID"].ToString();
                item.Material_Catalog_ID = row["Material_Catalog_ID"].ToString();
                item.Total_Quantity = Convert.ToDouble(row["Total_Quantity"].ToString());
                item.Net_Price = Convert.ToDouble(row["Net_Price"].ToString());
                item.Total_Amount = Convert.ToDouble(row["Total_Amount"].ToString());
                item.Material_Name = row["Material_Name"].ToString();
                item.Supplier_ID = row["Supplier_ID"].ToString();
                item.Is_Valid = row["Is_Valid"].ToString();
                item.Currency = row["Currency"].ToString();

                items.Add(item);
            }

            return items;
        }

        /// <summary>
        /// 向购物车添加新项
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public int addToShoppingCart(ShoppingCartItem item){
            StringBuilder strBui = new StringBuilder("insert into Shopping_Cart_Item (")
                .Append("Shopping_Cart_ID, Material_Catalog_ID, Total_Quantity, Net_Price, ")
                .Append("Total_Amount, Material_Name, Supplier_ID, Is_Valid, Currency) values (")
                .Append("@Shopping_Cart_ID, @Material_Catalog_ID, @Total_Quantity, @Net_Price, ")
                .Append("@Total_Amount, @Material_Name, @Supplier_ID, @Is_Valid, @Currency)");

            SqlParameter[] sqlParams = new SqlParameter[]{
                new SqlParameter("@Shopping_Cart_ID", SqlDbType.VarChar),
                new SqlParameter("@Material_Catalog_ID", SqlDbType.VarChar),
                new SqlParameter("@Total_Quantity", SqlDbType.Decimal),
                new SqlParameter("@Net_Price", SqlDbType.Decimal),
                new SqlParameter("@Total_Amount", SqlDbType.Decimal),
                new SqlParameter("@Material_Name", SqlDbType.VarChar),
                new SqlParameter("@Supplier_ID", SqlDbType.VarChar),
                new SqlParameter("@Is_Valid", SqlDbType.VarChar),
                new SqlParameter("@Currency", SqlDbType.VarChar)
            };

            sqlParams[0].Value = item.Shopping_Cart_ID;
            sqlParams[1].Value = item.Material_Catalog_ID;
            sqlParams[2].Value = item.Total_Quantity;
            sqlParams[3].Value = item.Net_Price;
            sqlParams[4].Value = item.Total_Amount;
            sqlParams[5].Value = item.Material_Name;
            sqlParams[6].Value = item.Supplier_ID;
            sqlParams[7].Value = item.Is_Valid;
            sqlParams[8].Value = item.Currency;

            return DBHelper.ExecuteNonQuery(strBui.ToString(), sqlParams);
        }

        /// <summary>
        /// 更新购物车
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public int updateShoppingCart(ShoppingCartItem item) {
            StringBuilder strBui = new StringBuilder("update Shopping_Cart_Item set ")
                .Append("Total_Quantity = @Total_Quantity, Net_Price = @Net_Price, ")
                .Append("Total_Amount = @Total_Amount, Is_Valid = @Is_Valid where ")
                .Append("Shopping_Cart_ID = @Shopping_Cart_ID and Material_Catalog_ID = @Material_Catalog_ID");

            SqlParameter[] sqlParams = new SqlParameter[]{
                new SqlParameter("@Total_Quantity", SqlDbType.Decimal),
                new SqlParameter("@Net_Price", SqlDbType.Decimal),
                new SqlParameter("@Total_Amount", SqlDbType.Decimal),
                new SqlParameter("@Is_Valid", SqlDbType.VarChar),
                new SqlParameter("@Shopping_Cart_ID", SqlDbType.VarChar),
                new SqlParameter("@Material_Catalog_ID", SqlDbType.VarChar)
            };

            sqlParams[0].Value = item.Total_Quantity;
            sqlParams[1].Value = item.Net_Price;
            sqlParams[2].Value = item.Total_Amount;
            sqlParams[3].Value = item.Is_Valid;
            sqlParams[4].Value = item.Shopping_Cart_ID;
            sqlParams[5].Value = item.Material_Catalog_ID;

            return DBHelper.ExecuteNonQuery(strBui.ToString(), sqlParams);
        }
        
        /// <summary>
        /// 删除购物车项目
        /// </summary>
        /// <param name="materialCatalogID">物料编号</param>
        /// <param name="shoppingCartID">购物车编号</param>
        /// <returns></returns>
        public int deleteShoppingCartItem(string materialCatalogID, string shoppingCartID)
        {
            StringBuilder strBui = new StringBuilder("delete from Shopping_Cart_Item where ")
                .Append("Shopping_Cart_ID = '").Append(shoppingCartID).Append("' and ")
                .Append("Material_Catalog_ID = '").Append(materialCatalogID).Append("'");

            return DBHelper.ExecuteNonQuery(strBui.ToString());
        }

        /// <summary>
        /// 获得所有的预算编号
        /// </summary>
        /// <returns></returns>
        public List<string> getPurchaseBudgetList() {
            string sql = "SELECT Budget_ID FROM Purchase_Budget WHERE Budget_Type = '单次采购' OR Budget_Type = '持续采购' AND End_Time > GETDATE()";
            List<string> result = new List<string>();
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            foreach (DataRow row in dt.Rows) {
                result.Add(row["Budget_ID"].ToString());
            }

            return result;
        }
    }
}
