﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.IDAL.FileManage;
using Lib.Model.FileManage;
using System.Data;
using System.Data.SqlClient;

namespace Lib.SqlServerDAL.FileManageDAL
{
    public class FileTypeDAL : FileTypeIDAL
    {
        /// <summary>
        /// 获取所有的文件类型名称
        /// </summary>
        /// <returns></returns>
        public List<string> getAllFileTypeName(){
            string sqlText = "select File_Type_Name from File_Type order by File_Type_Name";
            
            DataTable dt = DBHelper.ExecuteQueryDT(sqlText);
            List<string> fileTypeNameList = new List<string>();
            foreach (DataRow row in dt.Rows) {
                string fileTypeName = row[0].ToString();
                fileTypeNameList.Add(fileTypeName);
            }

            return fileTypeNameList;
        }

        public File_Type getFileType(string fileTypeName) {
            StringBuilder strBui = new StringBuilder("select * from File_Type ")
                .Append(" where File_Type_Name = @File_Type_Name");
            
            SqlParameter[] sqlParams = new SqlParameter[]{
                new SqlParameter("@File_Type_Name", SqlDbType.VarChar)
            };
            sqlParams[0].Value = fileTypeName;

            DataTable dt = DBHelper.ExecuteQueryDT(strBui.ToString(), sqlParams);
            if (dt.Rows.Count == 0)
                return null;
            DataRow row = dt.Rows[0];
            File_Type fileType = new File_Type();
            fileType.File_Type_Name = row["File_Type_Name"].ToString();
            fileType.Description = row["Description"].ToString();
            fileType.Creator_ID = row["Creator_ID"].ToString();
            fileType.Create_Time = Convert.ToDateTime(row["Create_Time"].ToString());

            return fileType;
        }

        /// <summary>
        /// 删除文件类型
        /// </summary>
        /// <param name="fileTypeName"></param>
        /// <returns></returns>
        public int deleteFileType(string fileTypeName) {
            StringBuilder strBui = new StringBuilder("delete from File_Type ")
                .Append("where File_Type_Name = '").Append(fileTypeName).Append("'");

            return DBHelper.ExecuteNonQuery(strBui.ToString());
        }

        /// <summary>
        /// 增加文件类型
        /// </summary>
        /// <param name="fileType"></param>
        /// <returns></returns>
        public int addFileType(File_Type fileType) {
            StringBuilder strBui = new StringBuilder("insert into File_Type ( ")
                .Append("File_Type_Name, Description, Creator_ID, Creator_Type, Create_Time )")
                .Append(" values (@File_Type_Name, @Description, @Creator_ID, @Creator_Type, @Create_Time )");

            SqlParameter[] sqlParams = new SqlParameter[]{
                new SqlParameter("@File_Type_Name", SqlDbType.VarChar),
                new SqlParameter("@Description", SqlDbType.VarChar),
                new SqlParameter("@Creator_ID", SqlDbType.VarChar),
                new SqlParameter("@Creator_Type", SqlDbType.VarChar),
                new SqlParameter("@Create_Time", SqlDbType.DateTime)
            };
            sqlParams[0].Value = fileType.File_Type_Name;
            sqlParams[1].Value = fileType.Description;
            sqlParams[2].Value = fileType.Creator_ID;
            sqlParams[3].Value = fileType.Creator_Type;
            sqlParams[4].Value = fileType.Create_Time;

            return DBHelper.ExecuteNonQuery(strBui.ToString(), sqlParams);
        }

        /// <summary>
        /// 更新文件类型
        /// </summary>
        /// <param name="fileType"></param>
        /// <returns></returns>
        public int updateFileType(File_Type fileType) {
            StringBuilder strBui = new StringBuilder("update File_Type set Description = '")
                .Append(fileType.Description).Append("'");

            return DBHelper.ExecuteNonQuery(strBui.ToString());
        }
    }
}
