﻿using Lib.IDAL.MDIDAL.MaintainMtEvalFactor;
using Lib.Model.MD;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace Lib.SqlServerDAL
{
    class MaintianEvalFactorDAL : MainTainFactorIDAL
    {
        /// <summary>
        /// 保存主标准数据
        /// </summary>
        /// <param name="mainEvalFactor"></param>
        /// <returns></returns>
        public bool addMainEvalFactor(MainEvalFactor mainEvalFactor)
        {
            bool isSuccess = false;
            string sqlStr = @"INSERT into TabEvalItem values(@desc,@code)";
            SqlParameter[] sqlParameters = new SqlParameter[]
            {
                new SqlParameter("@code",mainEvalFactor.Code),
                new SqlParameter("@desc",mainEvalFactor.Desc),
            };
            if (DBHelper.ExecuteNonQuery(sqlStr, sqlParameters) == 1)
            {
                isSuccess = true;

            }
            return isSuccess;
        }

        /// <summary>
        /// 添加次标准
        /// </summary>
        /// <param name="mainEvalFactor"></param>
        /// <returns></returns>
        public bool addSubEvalFactor(SubEvalFactor subEvalFactor)
        {
            bool isSuccess = false;
            string sqlStr = @"INSERT into TabSubStandard values(@mainEvalCode,@SubCode,@Desc)";
            SqlParameter[] sqlParameters = new SqlParameter[]
            {
                new SqlParameter("@mainEvalCode",subEvalFactor.MainEvalCode),
                new SqlParameter("@SubCode",subEvalFactor.SubCode),
                new SqlParameter("@Desc",subEvalFactor.Desc),
            };
            if (DBHelper.ExecuteNonQuery(sqlStr, sqlParameters) == 1) {
                isSuccess = true;

            }
            return isSuccess;
        }
        /// <summary>
        /// 删除主标准
        /// </summary>
        /// <param name="mainEvalFactor">主标准编号</param>
        /// <returns></returns>
        public bool deleteMainEvalFactor(string mainEvalFactor)
        {
            bool isSuccess = false;
            string sqlStr = @"delete from TabEvalItem where EvalItemCode=@mainEvalFactor";
            SqlParameter[] sqlParameters = new SqlParameter[]
            {
                new SqlParameter("@mainEvalFactor",mainEvalFactor)
             
            };
            if (DBHelper.ExecuteNonQuery(sqlStr, sqlParameters) == 1)
            {
                isSuccess = true;

            }
            return isSuccess;
        }

        /// <summary>
        /// 获取主标准
        /// </summary>
        /// <returns></returns>
        public DataTable GetMtFactor()
        {
            string sqlStr = "select EvalItemCode as 代码,Description as 描述 from TabEvalItem";
            SqlParameter[] sqlParameters = new SqlParameter[]
            {
                new SqlParameter("43242",sqlStr)

            };

            return DBHelper.ExecuteQueryDT(sqlStr);
        }
        /// <summary>
        /// 获取主标准对应得次标准
        /// </summary>
        /// <param name="mainEvalFactor">主标准代码</param>
        /// <returns></returns>
        public DataTable getSubEvalFactor(string mainEvalFactor)
        {
            string sqlStr = @"SELECT SubStandardCode,SubStandardDes FROM [dbo].[TabSubStandard] where MainStandardCode=@mainEvalFactor";
            SqlParameter[] sqlParameters = new SqlParameter[]
            {
                new SqlParameter("@mainEvalFactor",mainEvalFactor)

            };
            return DBHelper.ExecuteQueryDT(sqlStr,sqlParameters);
        }
        /// <summary>
        /// 修改主标准
        /// </summary>
        /// <param name="mainEvalFactor">主标准对象</param>
        /// <returns></returns>
        public bool updateMainEval(MainEvalFactor mainEvalFactor,string beforeCode)
        {
            bool isSuccess = false;
            string sqlStr = @"update TabEvalItem set EvalItemCode=@code,Description=@desc where EvalItemCode=@beforeCode";
            SqlParameter[] sqlParameters = new SqlParameter[]
            {
                new SqlParameter("@code",mainEvalFactor.Code),
                new SqlParameter("@desc",mainEvalFactor.Desc),
                new SqlParameter("@beforeCode",beforeCode)


            };
            if (DBHelper.ExecuteNonQuery(sqlStr, sqlParameters) == 1)
            {
                isSuccess = true;

            }
            return isSuccess;
        }
        /// <summary>
        /// 删除主标准
        /// </summary>
        /// <param name="mainEvalFactor">主标准编号</param>
        /// <returns></returns>
        public bool deleteSubEvalFactor(string subEvalFactor)
        {
            bool isSuccess = false;
            string sqlStr = @"delete from TabSubStandard where SubStandardCode=@subEvalFactor";
            SqlParameter[] sqlParameters = new SqlParameter[]
            {
                new SqlParameter("@subEvalFactor",subEvalFactor)

            };
            if (DBHelper.ExecuteNonQuery(sqlStr, sqlParameters) == 1)
            {
                isSuccess = true;

            }
            return isSuccess;
        }
        /// <summary>
        /// 更新次标准
        /// </summary>
        /// <param name="subEvalFactor"></param>
        /// <param name="code"></param>
        /// <returns></returns>
        public bool updateSubEval(SubEvalFactor subEvalFactor, string code,string mainCode)
        {
            bool isSuccess = false;
            string sqlStr = @"update TabSubStandard set SubStandardCode=@code, SubStandardDes=@desc where  MainStandardCode=@mainCode and SubStandardCode=@beforeCode";
            SqlParameter[] sqlParameters = new SqlParameter[]
            {
                new SqlParameter("@code",subEvalFactor.SubCode),
                new SqlParameter("@desc",subEvalFactor.Desc),
                new SqlParameter("@beforeCode",code),
                new SqlParameter("@mainCode",mainCode)
            };
            if (DBHelper.ExecuteNonQuery(sqlStr, sqlParameters) == 1)
            {
                isSuccess = true;

            }
            return isSuccess;
        }
    }
}
