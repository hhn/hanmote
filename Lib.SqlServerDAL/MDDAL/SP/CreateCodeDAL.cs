﻿using Lib.Common.MMCException.IDAL;
using Lib.IDAL.MDIDAL.SP;
using Lib.Model.MD.SP;
using Lib.Model.SystemConfig;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Lib.SqlServerDAL.MDDAL.SP
{
    public class CreateCodeDAL : CreateCodeIDAL
    {
        public bool checkonce(string loginAccount)
        {
            string sql = "SELECT LoginAccount from Supplier_MainAccount WHERE LoginAccount ='" + loginAccount + "'";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            bool account = checkOnlyOne(dt, loginAccount);
            return account;
        }

        public DataTable getbuyernamecode(string buyername)
        {
            string sql = "SELECT Buyer_Org,Buyer_Org_Name FROM Buyer_Org";
            if (!buyername.Equals("null"))
            {
                sql += " where Buyer_Org_Name like '%" + buyername + "%' ";
            }
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            return dt;
        }

        public DataTable getcompanynamecode(string companyname)
        {
            string sql = "SELECT Company_ID,Company_Name FROM Company";
            if (!companyname.Equals("null"))
            {
                sql += " where Company_Name like '%" + companyname + "%' ";
            }
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            return dt;
        }

        public DataTable getspname()
        {
            string sql = "SELECT Supplier_Name,Supplier_Id from Supplier_MainAccount where LoginAccount is null";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            return dt;
        }

        public DataTable getspname(string name)
        {
            string sql = "SELECT Supplier_Name,Supplier_Id from Supplier_MainAccount where Supplier_Name like '%"+name+"%'";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            return dt;
        }

        public DataTable getsupplierinfo(string supplier_name, string supplier_loginaccount_type)
        {
            string sql = "SELECT Supplier_Id,Supplier_Name,ManagerName,LoginAccount FROM Supplier_MainAccount WHERE ";
            if (!supplier_name.Equals("null"))
            {
                sql += "Supplier_Name like '%" + supplier_name + "%' and ";
            }
            sql += "LoginAccount is " + supplier_loginaccount_type + "";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            return dt;
        }

        public void insertorupdatesupplierbase(string v1, string v2)
        {
            try {
                string sql = "UPDATE Supplier_Base set Supplier_AccountGroup = '" + v2 + "' where Supplier_ID='" + v1 + "'";
                DBHelper.ExecuteNonQuery(sql);
            } catch (DBException ex)
            {
                throw ex;
            }
        }

        public void insertSuppliercompanycode(string fCompanyCode, string fCompanyname, string v1, string v2)
        {
            try
            {
                string sql = "INSERT INTO Supplier_CompanyCode (Company_ID,Company_Name,Supplier_ID,Supplier_Name) VALUES('"+fCompanyCode+"','"+fCompanyname+"','"+v1+"','"+v2+"')";
                DBHelper.ExecuteNonQuery(sql);
            }
            catch (DBException ex)
            {
                throw ex;
            }
        }

        public void insertSupplierpurchasingorg(string fBuyerID, string fBuyername, string v1, string v2)
        {
            try
            {
                string sql = "INSERT INTO Supplier_Purchasing_Org (PurchasingORG_ID,PurchasingORG_Name,Supplier_ID,Supplier_Name) VALUES('"+fBuyerID+"','"+fBuyername+"','"+v1+ "','" + v2 + "')";
                DBHelper.ExecuteNonQuery(sql);
            }
            catch (DBException ex)
            {
                throw ex;
            }
        }

        public string updataLoginAccount(string supplierID, string loginAccount)
        {
            try
            {
                string sql = " UPDATE Supplier_MainAccount SET LoginAccount = '"+loginAccount+"' WHERE Supplier_Id='"+supplierID+"'";
                DBHelper.ExecuteNonQuery(sql);
                return "账户更新成功";
            }
            catch (DBException ex)
            {
                return "账户更新失败";
            }
        }

        /// <summary>
        /// 唯一性检查
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="Id"></param>
        private bool checkOnlyOne(DataTable dt, string Id)
        {
            if (dt.Rows.Count == 0)
            {
                return true;
            }
            else
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (Id == dt.Rows[i][0].ToString().Trim() || Id.Equals(dt.Rows[i][0].ToString().Trim()))
                    {
                        return false;
                    }
                }
            }
            return true;
        }
    }
}
