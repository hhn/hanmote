﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.IDAL.MDIDAL.SP;
using Lib.Model.MD.SP;
using System.Data;

namespace Lib.SqlServerDAL.MDDAL.SP
{
   public class SupplierPurchaseORGDAL:SupplierPurchaseORGIDAL
    {

        public Model.MD.SP.SupplierPurchaseOrganization GetSupplierPurchaseOrganizationInformation(string supplierID, string PurchaseORGID)
        {
            SupplierPurchaseOrganization sporg = new SupplierPurchaseOrganization();
            string sql = "SELECT * FROM [Supplier_Purchasing_Org] WHERE Supplier_ID='" + supplierID + "' AND PurchasingORG_ID='" + PurchaseORGID + "'";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            if (dt.Rows.Count == 0)
                return sporg;
            else
            {
                DataRow row = dt.Rows[0];
                sporg.Supplier_ID = row["Supplier_ID"].ToString();
                sporg.Supplier_Name = row["Supplier_Name"].ToString();
                sporg.PurchasingORG_ID = row["PurchasingORG_ID"].ToString();
                sporg.PurchasingORG_Name = row["PurchasingORG_Name"].ToString();
                sporg.Order_Units = row["Order_Units"].ToString();
                sporg.Payment_Clause = row["Payment_Clause"].ToString();
                sporg.Trade_Clause = row["Trade_Clause"].ToString();
                sporg.MIN_OrderValue = row["MIN_OrderValue"].ToString();
                sporg.Supplier_Group = row["Supplier_Group"].ToString();
                sporg.PricingDate_Control = row["PricingDate_Control"].ToString();
                sporg.Order_Limit = row["Order_Limit"].ToString();
                sporg.Supplier_Has = row["Supplier_Has"].ToString();
                sporg.Invoice_Verification = Convert.ToBoolean(row["Invoice_Verification"].ToString());
                sporg.Delivery_Settlement = Convert.ToBoolean(row["Delivery_Settlement"].ToString());
                sporg.Settlement_Reserves = Convert.ToBoolean(row["Settlement_Reserves"].ToString());
                sporg.ReturnReceipt_Requirements = Convert.ToBoolean(row["ReturnReceipt_Requirements"].ToString());
                sporg.Automatic_PurchaseOrder = Convert.ToBoolean(row["Automatic_PurchaseOrder"].ToString());
                sporg.Subsequent_Settlement = Convert.ToBoolean(row["Subsequent_Settlement"].ToString());
                sporg.Settlement_Index = Convert.ToBoolean(row["Settlement_Index"].ToString());
                sporg.Business_Comparison = Convert.ToBoolean(row["Business_Comparison"].ToString());
                sporg.Document_Indexin = Convert.ToBoolean(row["Document_Indexin"].ToString());
                sporg.Return_Supplier = Convert.ToBoolean(row["Return_Supplier"].ToString());
                sporg.Service_Verification = Convert.ToBoolean(row["Service_Verification"].ToString());
                sporg.Allow_Reevaluation = Convert.ToBoolean(row["Allow_Reevaluation"].ToString());
                sporg.Allow_Discount = Convert.ToBoolean(row["Allow_Discount"].ToString());
                sporg.Price_Determination = Convert.ToBoolean(row["Price_Determination"].ToString());
                sporg.Agent_Service = Convert.ToBoolean(row["Agent_Service"].ToString());
                sporg.Shipment_Terms = row["Shipment_Terms"].ToString();
                sporg.ABC_Identify = row["ABC_Identify"].ToString();
                sporg.Transportation_Mode = row["Transportation_Mode"].ToString();
                sporg.Input_ManagementOffice = row["Input_ManagementOffice"].ToString();
                sporg.Sort_Criteria = row["Sort_Criteria"].ToString();
                sporg.PRO_ControlData = row["PRO_ControlData"].ToString();
                sporg.Buyer_Group = row["Buyer_Group"].ToString();
                sporg.Delivery_Time = Convert.ToDateTime(row["Delivery_Time"].ToString());
                sporg.Control_Confirm = row["Control_Confirm"].ToString();
                sporg.Measurment_Group = row["Measurment_Group"].ToString();
                sporg.Rounding_Parameters = row["Rounding_Parameters"].ToString();
                sporg.Price_TagAgreement = row["Price_TagAgreement"].ToString();
                try
                {
                    sporg.Services_Agreement = Convert.ToBoolean(row["Services_Agreement"].ToString());
                }
                catch(Exception ex)
                {
                }
                try
                {
                    sporg.Order_Entry = Convert.ToBoolean(row["Order_Entry"].ToString());
                }
                catch
                {
                }
                sporg.Service_Grade = row["Service_Grade"].ToString();
                return sporg;
            }




        }

        public bool UpdateSupplierPurchaseORGinformation(Model.MD.SP.SupplierPurchaseOrganization sporg)
        {
            string sql1 = "SELECT * FROM [Supplier_Purchasing_Org]";
            DataTable dt = DBHelper.ExecuteQueryDT(sql1);
            if (dt.Rows.Count == 0)
            {
                string sql2 = "INSERT INTO [Supplier_Purchasing_Org](Supplier_ID,Supplier_Name,PurchasingORG_ID,PurchasingORG_Name,";
                sql2 += "Order_Units,Payment_Clause,Trade_Clause,MIN_OrderValue,Supplier_Group,PricingDate_Control,Order_Limit,";
                sql2 += "Supplier_Has,Invoice_Verification,Delivery_Settlement,Settlement_Reserves,ReturnReceipt_Requirements,";
                sql2 += "Automatic_PurchaseOrder ,Subsequent_Settlement,Settlement_Index,Business_Comparison,Document_Indexin,";
                sql2 += "Return_Supplier,Service_Verification,Allow_Reevaluation,Allow_Discount,Price_Determination,Agent_Service,";
                sql2 += "Shipment_Terms,ABC_Identify,Transportation_Mode,Input_ManagementOffice,Sort_Criteria,PRO_ControlData,";
                sql2 += "Buyer_Group,Delivery_Time,Control_Confirm,Measurment_Group,Rounding_Parameters,Price_TagAgreement,";
                sql2 += "Services_Agreement,Order_Entry,Service_Grade)VALUES('" + sporg.Supplier_ID + "','" + sporg.Supplier_Name + "',";
                sql2 += "'" + sporg.PurchasingORG_ID + "','" + sporg.PurchasingORG_Name + "','" + sporg.Order_Units + "','" + sporg.Payment_Clause + "',";
                sql2 += "'" + sporg.Trade_Clause + "','" + sporg.MIN_OrderValue + "','" + sporg.Supplier_Group + "','" + sporg.PricingDate_Control + "',";
                sql2 += "'" + sporg.Order_Limit + "','" + sporg.Supplier_Has + "','" + sporg.Invoice_Verification + "','" + sporg.Delivery_Settlement + "',";
                sql2 += "'" + sporg.Settlement_Reserves + "','" + sporg.ReturnReceipt_Requirements + "','" + sporg.Automatic_PurchaseOrder + "',";
                sql2 += "'" + sporg.Subsequent_Settlement + "','" + sporg.Settlement_Index + "','" + sporg.Business_Comparison + "',";
                sql2 += "'" + sporg.Document_Indexin + "','" + sporg.Return_Supplier + "','" + sporg.Service_Verification + "',";
                sql2 += "'" + sporg.Allow_Reevaluation + "','" + sporg.Allow_Discount + "','" + sporg.Price_Determination + "',";
                sql2 += "'" + sporg.Agent_Service + "','" + sporg.Shipment_Terms + "','" + sporg.ABC_Identify + "',";
                sql2 += "'" + sporg.Transportation_Mode + "','" + sporg.Input_ManagementOffice + "','" + sporg.Sort_Criteria + "',";
                sql2 += "'" + sporg.PRO_ControlData + "','" + sporg.Buyer_Group + "','" + sporg.Delivery_Time + "','" + sporg.Control_Confirm + "',";
                sql2 += "'" + sporg.Measurment_Group + "','" + sporg.Rounding_Parameters + "','" + sporg.Price_TagAgreement + "',";
                sql2 += "'" + sporg.Services_Agreement + "','" + sporg.Order_Entry + "','" + sporg.Service_Grade + "')";
                DBHelper.ExecuteNonQuery(sql2);
                return true;


            }
            else
            {
                string sql3 = "UPDATE [Supplier_Purchasing_Org] SET Order_Units='"+sporg.Order_Units+"',";
                sql3 += "Payment_Clause='" + sporg.Payment_Clause + "',Trade_Clause='" + sporg.Trade_Clause + "',";
                sql3 += "MIN_OrderValue='" + sporg.MIN_OrderValue + "',Supplier_Group='" + sporg.Supplier_Group + "',";
                sql3 += "PricingDate_Control='" + sporg.PricingDate_Control + "',Order_Limit='" + sporg.Order_Limit + "',";
                sql3 += "Supplier_Has='" + sporg.Supplier_Has + "',Invoice_Verification='" + sporg.Invoice_Verification + "',";
                sql3 += "Delivery_Settlement='" + sporg.Delivery_Settlement + "',Settlement_Reserves='" + sporg.Settlement_Reserves + "',";
                sql3 += "ReturnReceipt_Requirements='" + sporg.ReturnReceipt_Requirements + "',Automatic_PurchaseOrder='" + sporg.Automatic_PurchaseOrder + "',";
                sql3 += "Subsequent_Settlement='" + sporg.Subsequent_Settlement + "',Settlement_Index='" + sporg.Settlement_Index + "',";
                sql3 += "Business_Comparison='" + sporg.Business_Comparison + "',Document_Indexin='" + sporg.Document_Indexin + "',";
                sql3 += "Return_Supplier='" + sporg.Return_Supplier + "',Service_Verification='" + sporg.Service_Verification + "',";
                sql3 += "Allow_Reevaluation='" + sporg.Allow_Reevaluation + "',Allow_Discount='" + sporg.Allow_Discount + "',";
                sql3 += "Price_Determination='" + sporg.Price_Determination + "',Agent_Service='" + sporg.Agent_Service + "',";
                sql3 += "Shipment_Terms='" + sporg.Shipment_Terms + "',ABC_Identify='" + sporg.ABC_Identify + "',";
                sql3 += "Transportation_Mode='" + sporg.Transportation_Mode + "',Input_ManagementOffice='" + sporg.Input_ManagementOffice + "',";
                sql3 += "Sort_Criteria='" + sporg.Sort_Criteria + "',PRO_ControlData='" + sporg.PRO_ControlData + "',";
                sql3 += "Buyer_Group='" + sporg.Buyer_Group + "',Delivery_Time='" + sporg.Delivery_Time + "',Control_Confirm='" + sporg.Control_Confirm + "',";
                sql3 += "Measurment_Group='" + sporg.Measurment_Group + "',Rounding_Parameters='" + sporg.Rounding_Parameters + "',";
                sql3 += "Price_TagAgreement='" + sporg.Price_TagAgreement + "',Services_Agreement='" + sporg.Services_Agreement + "',";
                sql3 += "Order_Entry='" + sporg.Order_Entry + "',Service_Grade='" + sporg.Service_Grade + "'";
                sql3 += "WHERE Supplier_ID='" + sporg.Supplier_ID + "' AND PurchasingORG_ID='" + sporg.PurchasingORG_ID + "'";
                DBHelper.ExecuteNonQuery(sql3);
                return true;
            }
        }
    }
}
