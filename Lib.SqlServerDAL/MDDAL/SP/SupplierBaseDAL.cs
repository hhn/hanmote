﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.IDAL.MDIDAL.SP;
using System.Data;
using Lib.Model.MD.SP;
using Lib.Common.MMCException.IDAL;

namespace Lib.SqlServerDAL.MDDAL.SP
{
   public class SupplierBaseDAL:SupplierBaseIDAL
    {
        public Model.MD.SP.SupplierBase GetSupplierBasicInformation(string SupplierID)
        {
            string sql = "SELECT * FROM [Supplier_Base] WHERE Supplier_ID='" + SupplierID + "'";
           DataTable dt = DBHelper.ExecuteQueryDT(sql);
            DataRow row;
            SupplierBase spb = new SupplierBase();
            if (dt.Rows.Count != 0)
            {
                row = dt.Rows[0];

                spb.Supplier_ID = row["Supplier_ID"].ToString();
                spb.Supplier_Name = row["Supplier_Name"].ToString();
                spb.Supplier_Account = row["Supplier_AccountGroup"].ToString();
                spb.Supplier_Title = row["Supplier_Title"].ToString();
                spb.Nation = row["Nation"].ToString();
                spb.Address = row["Address"].ToString();
                spb.House_Number = row["House_Number"].ToString();
                spb.Street_postalCode = row["Street_postalCode"].ToString();
                spb.City = row["City"].ToString();
                spb.Region = row["Region"].ToString();
                spb.Post_OfficeBox = row["Post_OfficeBox"].ToString();
                spb.Company_ZipCode = row["Company_ZipCode"].ToString();
                spb.Language = row["Language"].ToString();
                spb.Supplier_OName = row["Supplier_OName"].ToString();
                spb.Zip_Code = row["Zip_Code"].ToString();
                spb.Fax = row["Fax"].ToString();
                spb.Mobile_Phone1 = row["Mobile_Phone1"].ToString();
                spb.Mobile_Phone2 = row["Mobile_Phone2"].ToString();
                spb.Client = row["Client"].ToString();
                spb.Trade_Partner = row["Trade_Partner"].ToString();
                spb.Permissions_Set = row["Permissions_Set"].ToString();
                spb.Group_Code = row["Group_Code"].ToString();
                spb.Contact_phone1 = row["Contact_phone1"].ToString();
                spb.Contact_phone2 = row["Contact_phone2"].ToString();
                spb.Email = row["Email"].ToString();
                spb.Tax_Code1 = row["Tax_Code1"].ToString();
                spb.Tax_Code2 = row["Tax_Code2"].ToString();
                spb.Tax_Code3 = row["Tax_Code3"].ToString();
                spb.Tax_Code4 = row["Tax_Code4"].ToString();
                spb.EIN_Type = row["EIN_Type"].ToString();
                spb.Tax_Type = row["Tax_Type"].ToString();
                spb.Financial_Address = row["Financial_Address"].ToString();
                spb.Area_TaxCode = row["Area_TaxCode"].ToString();
                spb.Representor_Name = row["Representor_Name"].ToString();
                spb.Tax_Brurau = row["Tax_Brurau"].ToString();
                spb.Tax_Code = row["Tax_Code"].ToString();
                spb.Tax_Base = row["Tax_Base"].ToString();
                spb.Social_CnsuranceCode = row["Social_CnsuranceCode"].ToString();
                spb.VAT_Registration_Number = row["VAT_Registration_Number"].ToString();
                spb.Business_Type = row["Business_Type"].ToString();
                spb.Industry_Type = row["Industry_Type"].ToString();
                spb.International_AreaCode1 = row["International_AreaCode1"].ToString();
                spb.International_AreaCode2 = row["International_AreaCode2"].ToString();
                spb.Credit_Information = row["Credit_Information"].ToString();
                spb.Industry = row["Industry"].ToString();
                spb.SCAC_Cod = row["SCAC_Code"].ToString();
                spb.PCO_Code = row["PCO_Code"].ToString();
                spb.Contact_phone1 = row["Contact_phone1"].ToString();
                spb.Contact_phone2 = row["Contact_phone2"].ToString();
                spb.Supplier_Class = row["Supplier_Class"].ToString();
                return spb;
            }
            else
                return spb;



            
        }

        public bool UpdateSupplierBasicInformation(Model.MD.SP.SupplierBase spb)
        {
            string sql = "UPDATE [Supplier_Base] SET Supplier_Name='" + spb.Supplier_Name + "',Supplier_AccountGroup='" + spb.Supplier_Account + "',";
            sql += "Supplier_Title='" + spb.Supplier_Title + "',Nation='" + spb.Nation + "',Address='" + spb.Address + "',";
            sql += "House_Number='" + spb.House_Number + "',Street_postalCode='" + spb.Street_postalCode + "',City='" + spb.City + "',";
            sql += "Region='" + spb.Region + "',Post_OfficeBox='" + spb.Post_OfficeBox + "',Company_ZipCode='" + spb.Company_ZipCode + "',";
            sql += "Language='" + spb.Language + "',Supplier_OName='" + spb.Supplier_OName + "',Zip_Code='" + spb.Zip_Code + "',";
            sql += "Fax='" + spb.Fax + "',Mobile_Phone1='" + spb.Mobile_Phone1 + "',Mobile_Phone2='" + spb.Mobile_Phone2 + "',";
            sql += "Contact_phone1='" + spb.Contact_phone1 + "',Contact_phone2='" + spb.Contact_phone2 + "',";
            sql += "Client='" + spb.Client + "',Trade_Partner='" + spb.Trade_Partner + "',Permissions_Set='" + spb.Permissions_Set + "',";
            sql += "Group_Code='" + spb.Group_Code + "',";
            sql += "Email='" + spb.Email + "',Tax_Code1='" + spb.Tax_Code1 + "',Tax_Code2='" + spb.Tax_Code2 + "',Tax_Code3='" + spb.Tax_Code3 + "',";
            sql += "Tax_Code4='" + spb.Tax_Code4 + "',EIN_Type='" + spb.EIN_Type + "',Tax_Type='" + spb.Tax_Type + "',Financial_Address='" + spb.Financial_Address + "',";
            sql += "Area_TaxCode='" + spb.Area_TaxCode + "',Representor_Name='" + spb.Representor_Name + "',Tax_Brurau='" + spb.Tax_Brurau + "',";
            sql += "Tax_Code='" + spb.Tax_Code + "',Tax_Base='" + spb.Tax_Base + "',Social_CnsuranceCode='" + spb.Social_CnsuranceCode + "',";
            sql += "VAT_Registration_Number='" + spb.VAT_Registration_Number + "',Business_Type='" + spb.Business_Type + "',Industry_Type='" + spb.Industry_Type + "',";
            sql += "International_AreaCode1='" + spb.International_AreaCode1 + "',International_AreaCode2='" + spb.International_AreaCode2 + "',Credit_Information='" + spb.Credit_Information + "',";
            sql += "Industry='" + spb.Industry + "',SCAC_Code='" + spb.SCAC_Cod + "',Transport_Area='" + spb.Transport_Area + "',PCO_Code='" + spb.PCO_Code + "'";
            sql += "WHERE Supplier_ID = '" + spb.Supplier_ID + "'";
            DBHelper.ExecuteNonQuery(sql);
            return true;
        }


     public List<string> GetAllSupplierID()
        {
            string sql = "SELECT * FROM [Supplier_Base]";
          DataTable dt =  DBHelper.ExecuteQueryDT(sql);
          if (dt == null || dt.Rows.Count == 0)
          {
              return null;
          }
          List<string> list = new List<string>();
          string obj;
          for (int i = 0; i < dt.Rows.Count; i++)
          {
              obj = dt.Rows[i][1].ToString();
              list.Add(obj);
              obj = null;
          }
          return list;
        }


     public DataTable GetAllSupplierBasicInformation()
     {
         string sql = "SELECT Supplier_Name,Supplier_AccountGroup,Supplier_Title,Nation FROM [Supplier_Base]";
         DataTable dt = DBHelper.ExecuteQueryDT(sql);
         return dt;
     }


     public string GetSupplierNameBySupplierID(string spid)
     {
         throw new NotImplementedException();
     }

        public List<SupplierBase> GetAllSupplierBase()
        {
            List<SupplierBase> list = new List<SupplierBase>();
            string tableName = "dbo.Supplier_Base";
            StringBuilder sb = new StringBuilder("select * from "+tableName);
            DataTable dt = DBHelper.ExecuteQueryDT(sb.ToString());
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    SupplierBase supb = new SupplierBase();
                    supb.Supplier_ID = dr["Supplier_ID"].ToString();
                    supb.Supplier_Name = dr["Supplier_Name"].ToString();
                    //联系人
                    supb.Email = dr["Email"].ToString();
                    supb.Address = dr["Address"].ToString();
                    list.Add(supb);
                }
            }
            return list;
        }
        public List<SupplierBase> getSupplierBaseByMG(string materialGroup)
        {
            List<SupplierBase> list = new List<SupplierBase>();
            string tableName = "dbo.Supplier_Base";
            StringBuilder sb = new StringBuilder("select * from " + tableName + " where Industry_Category='" + materialGroup + "'");
            DataTable dt = DBHelper.ExecuteQueryDT(sb.ToString());
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    SupplierBase supb = new SupplierBase();
                    supb.Supplier_ID = dr["Supplier_ID"].ToString();
                    supb.Supplier_Name = dr["Supplier_Name"].ToString();
                    //联系人
                    supb.Email = dr["Email"].ToString();
                    supb.Address = dr["Address"].ToString();
                    list.Add(supb);
                }
            }
            return list;
        }

        public DataTable GetTotalCount()
        {
            string sql = "SELECT COUNT(*) FROM [Supplier_Base]";
            return DBHelper.ExecuteQueryDT(sql);
        }

        public DataTable GetSupplierInforByPage(int pageSize, int pageIndex)
        {
                string sql = @"SELECT top " + pageSize + " Supplier_ID,Supplier_AccountGroup,Supplier_Name,Supplier_Title,Nation,Supplier_OName,Client,Trade_Partner,Group_Code FROM [Supplier_Base] where Supplier_ID not in(select top " + pageSize * (pageIndex - 1) + " Supplier_ID from Supplier_Base ORDER BY Supplier_ID ASC)ORDER BY Supplier_ID";
                return DBHelper.ExecuteQueryDT(sql);
        }
    }
}
