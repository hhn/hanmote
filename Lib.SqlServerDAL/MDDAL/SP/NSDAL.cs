﻿using Lib.Common.MMCException.IDAL;
using Lib.IDAL.MDIDAL.SP;
using Lib.Model.MD.SP;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;

namespace Lib.SqlServerDAL.MDDAL.SP
{
    public class NSDAL : NSIDAL
    {
        public bool CheckID(string Id)
        {
            string sql = "SELECT Number_ID from Number_Segment WHERE Number_ID ='"+Id+"'";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            bool ID = checkOnlyOne(dt,Id);
            return ID;
        }

        public string insert(Number_segement number_Segement)
        {
            try
            {
                string sql = " INSERT INTO [Number_Segment](Number_ID,Description,Status,NS_Begin,NS_End,NS_status) VALUES ('" + number_Segement.Number_ID+ "','" + number_Segement.Description + "','" + number_Segement.Status + "','" + number_Segement.NS_Begin + "','" + number_Segement.NS_End + "','" + number_Segement.NS_status + "')";
                DBHelper.ExecuteNonQuery(sql);
                return "创建成功";
            }
            catch (DBException ex)
            {
                return "创建失败";
            }
        }

        public DataTable selectAllID()
        {
            string sql = "SELECT Number_ID,Description from Number_Segment ";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            return dt;
        }

        public Number_segement selectbyID(string mNID)
        {
                string sql = "SELECT * from Number_Segment WHERE Number_ID ='" + mNID + "'";
                DataTable dt = DBHelper.ExecuteQueryDT(sql);
                DataRow row;
                Number_segement number_Segement = new Number_segement();
                if (dt.Rows.Count != 0)
                {
                    row = dt.Rows[0];
                    number_Segement.Number_ID =row["Number_ID"].ToString();
                    number_Segement.Description = row["Description"].ToString();
                    number_Segement.Status = row["Status"].ToString();
                    number_Segement.NS_Begin = row["NS_Begin"].ToString();
                    number_Segement.NS_End = row["NS_End"].ToString();
                    number_Segement.NS_status = row["NS_status"].ToString();
                }
                return number_Segement;
        }

        public DataTable selectbyname(string v)
        {
            string sql = "SELECT Number_ID,Description from Number_Segment WHERE Description like '%" + v+ "%'";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            return dt;
        }

        public string update(Number_segement number_Segement)
        {
            try
            {
                string sql = " UPDATE Number_Segment SET Description = '"+number_Segement.Description+"', NS_Begin = '"+number_Segement.NS_Begin+"' ,NS_End='"+number_Segement.NS_End+"',NS_status='"+number_Segement.NS_status+"' WHERE Number_ID = '"+number_Segement.Number_ID+"'";
                DBHelper.ExecuteNonQuery(sql);
                return "更新成功";
            }
            catch (DBException ex)
            {
                return "更新失败";
            }
        }

        public string update(string nS_id, string nS_status)
        {
            try
            {
                string sql = " UPDATE Number_Segment SET NS_status = '"+nS_status+"' where Number_ID = '" + nS_id + "'";
                DBHelper.ExecuteNonQuery(sql);
                return "号码段已更新";
            }
            catch (DBException ex)
            {
                return "号码段更新失败";
            }
        }

        /// <summary>
        /// 唯一性检查
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="Id"></param>
        private bool checkOnlyOne(DataTable dt, string Id)
        {
            if (dt.Rows.Count == 0)
            {
                return true;
            }
            else
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (Id == dt.Rows[i][0].ToString().Trim() || Id.Equals(dt.Rows[i][0].ToString().Trim()))
                    {
                        return false;
                    }
                }
            }
            return true;
        }

    }
}