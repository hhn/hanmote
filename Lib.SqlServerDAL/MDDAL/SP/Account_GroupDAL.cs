﻿using Lib.Common.MMCException.IDAL;
using Lib.IDAL.MDIDAL.SP;
using Lib.Model.MD.SP;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Lib.SqlServerDAL.MDDAL.SP
{
    public class Account_GroupDAL : Account_GroupIDAL
    {
        public bool CheckOnce(string account_Group_ID)
        {
            string sql = "SELECT Account_Group_ID from Account_Group WHERE Account_Group_ID ='" + account_Group_ID + "'";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            bool ID = checkOnlyOne(dt, account_Group_ID);
            return ID;
        }

        public DataTable getAccountGroup(string accountGroupName, string accountGroupSinglestatus)
        {
            string sql = "SELECT Account_Group_ID,a.Description,SingleStatus,NS_id,Status,NS_Begin,NS_End,NS_status FROM Account_Group a LEFT JOIN Number_Segment b ON(a.NS_id = b.Number_ID) WHERE ";
            if (!accountGroupName.Equals("null"))
            {
                sql += "a.Description LIKE '%" + accountGroupName + "%' and ";
            }
            sql += "a.SingleStatus = '"+ accountGroupSinglestatus +"'";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            return dt;
        }

        public string insert(Account_Group account_Group)
        {
            try
            {
                string sql = " INSERT INTO [Account_Group](Account_Group_ID,Description,SingleStatus,NS_id) VALUES ('" + account_Group.Account_Group_ID + "','" + account_Group.Description + "','" + account_Group.SingleStatus + "','" + account_Group.NS_id + "')";
                DBHelper.ExecuteNonQuery(sql);
                return "创建成功";
            }
            catch (DBException ex)
            {
                return "创建失败";
            }
        }

        public List<Account_Group> selectALL()
        {
            List<Account_Group> list = new List<Account_Group>();
            string sql = "SELECT * FROM Account_Group a LEFT JOIN Number_Segment b ON a.NS_id = b.Number_ID";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            foreach(DataRow dr in dt.Rows){
                Account_Group account_Group = new Account_Group();
                account_Group.Number_Segement = new Number_segement();
                account_Group.Account_Group_ID=dr["Account_Group_ID"].ToString();
                account_Group.Description = dr["Description"].ToString();
                account_Group.SingleStatus = dr["SingleStatus"].ToString();
                account_Group.NS_id = dr["NS_id"].ToString();              
                account_Group.Number_Segement.Status = dr["Status"].ToString();
                account_Group.Number_Segement.NS_Begin = dr["NS_Begin"].ToString();
                account_Group.Number_Segement.NS_End = dr["NS_End"].ToString();
                account_Group.Number_Segement.NS_status = dr["NS_status"].ToString();
                list.Add(account_Group);
            }
            return list;

        }

        public List<Account_Group> selectbydescrption(string aGDC)
        {
            List<Account_Group> list = new List<Account_Group>();
            string sql = "SELECT * FROM Account_Group a LEFT JOIN Number_Segment b ON (a.NS_id = b.Number_ID) WHERE a.Description LIKE '%"+aGDC+"%'";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            foreach (DataRow dr in dt.Rows)
            {
                Account_Group account_Group = new Account_Group();
                account_Group.Number_Segement = new Number_segement();
                account_Group.Account_Group_ID = dr["Account_Group_ID"].ToString();
                account_Group.Description = dr["Description"].ToString();
                account_Group.SingleStatus = dr["SingleStatus"].ToString();
                account_Group.NS_id = dr["NS_id"].ToString();
                account_Group.Number_Segement.Status = dr["Status"].ToString();
                account_Group.Number_Segement.NS_Begin = dr["NS_Begin"].ToString();
                account_Group.Number_Segement.NS_End = dr["NS_End"].ToString();
                account_Group.Number_Segement.NS_status = dr["NS_status"].ToString();
                list.Add(account_Group);
            }
            return list;
        }

        public Account_Group selectbyid(string account_Group_ID)
        {
            string sql = "SELECT * from Account_Group WHERE Account_Group_ID ='" + account_Group_ID + "'";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            DataRow row;
            Account_Group account_Group = new Account_Group();
            if (dt.Rows.Count != 0)
            {
                row = dt.Rows[0];
                account_Group.Account_Group_ID = row["Account_Group_ID"].ToString();
                account_Group.Description = row["Description"].ToString();
                account_Group.SingleStatus = row["SingleStatus"].ToString();
                account_Group.NS_id = row["NS_id"].ToString();
            }
            return account_Group;
        }

        public string update(Account_Group account_Group)
        {
            try
            {
                string sql = " UPDATE Account_Group SET Description = '" + account_Group.Description + "', SingleStatus = '" + account_Group.SingleStatus + "' ,NS_id='" + account_Group.NS_id + "' WHERE Account_Group_ID = '" + account_Group.Account_Group_ID + "'";
                DBHelper.ExecuteNonQuery(sql);
                return "更新成功";
            }
            catch (DBException ex)
            {
                return "更新失败";
            }

        }



        /// <summary>
        /// 唯一性检查
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="Id"></param>
        private bool checkOnlyOne(DataTable dt, string Id)
        {
            if (dt.Rows.Count == 0)
            {
                return true;
            }
            else
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (Id == dt.Rows[i][0].ToString().Trim() || Id.Equals(dt.Rows[i][0].ToString().Trim()))
                    {
                        return false;
                    }
                }
            }
            return true;
        }

    }
}
