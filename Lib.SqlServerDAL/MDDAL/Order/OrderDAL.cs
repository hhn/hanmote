﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.IDAL.MDIDAL.Order;
using System.Data;
using Lib.Common.CommonUtils;
using Lib.Model.MD.Order;

namespace Lib.SqlServerDAL.MDDAL.Order
{
   public class OrderDAL:OrderIDAL
    {
  public  bool InsertOrderInfo(Model.MD.Order.OrderInfo odif, List<Model.MD.Order.OrderItem> list)
        {

            StringBuilder strBui = new StringBuilder("insert into Order_Info (Order_ID,Order_Type,Purchase_Organization,")
            .Append("Company_Code,Supplier_ID,Create_Time,Delivery_Points,Delivery_Type,Total_Value,Order_Status,Value_Received,Value_Paid)")
            .Append("values ('" + odif.Order_ID + "','" + odif.Order_Type + "','" + odif.Purchase_Organization + "','" + odif.Company_Code + "',")
            .Append("'" + odif.Supplier_ID + "','" + odif.Create_Time.ToString("yyyyMMdd") + "','" + odif.Delivery_Points + "','" + odif.Delivery_Type + "','" + odif.Total_Value + "',")
            .Append("'" + odif.Order_Status + "','" + odif.Value_Received + "','" + odif.Value_Paid + "');");
            for (int i = 0; i < list.Count; i++)
            {
                strBui.Append("insert into order_Item (Order_ID,Mterial_ID,Factory_ID,Stock_ID,Material_Group,net_Price,Number,")
                    .Append("Unit,Total_price,Batch_ID,PR_ID,Delivery_Time,Info_Type,Info_Number) values ('" + list[i].Order_ID + "',")
                    .Append("'" + list[i].Material_ID + "','" + list[i].Factory_ID + "','" + list[i].Stock_ID + "','" + list[i].Material_Group + "',")
                    .Append("'" + list[i].Net_Price + "','" + list[i].Number + "','" + list[i].Unit + "','" + list[i].Total_Price + "','" + list[i].Batch_ID + "',")
                    .Append("'" + list[i].PR_ID + "','" + list[i].Delivery_Time.ToString("yyyyMMdd") + "','" + list[i].Info_Type + "','" + list[i].Info_Number + "');");

            }
            string delSQL = @"delete from Order_Info where Order_ID = '"+ odif.Order_ID + "';delete from order_Item where Order_ID = '"+ odif.Order_ID + "';";
            DBHelper.ExecuteNonQuery(delSQL);
            DBHelper.ExecuteNonQuery(strBui.ToString());
            return true;
        }


  public List<PR_Supplier> GetAllPrSupplierRelation(bool finishedmark)
  {
      List<PR_Supplier> list = new List<PR_Supplier>();
     
      StringBuilder strBui = new StringBuilder("SELECT * FROM [PR_Supplier] WHERE Finished_Mark='"+finishedmark+"'");
     DataTable dt = DBHelper.ExecuteQueryDT(strBui.ToString());
     if (dt.Rows.Count == 0)
     {
         MessageUtil.ShowError("没有符合条件的记录");
         return list;
     }
     else
     {
         for(int i = 0;i<dt.Rows.Count;i++)
         {
           PR_Supplier prs = new PR_Supplier();
         prs.Finished_Mark = false;
         prs.Material_ID = dt.Rows[i]["Material_ID"].ToString();
         prs.Number =Convert.ToInt64( dt.Rows[i]["Number"].ToString());
         prs.PR_ID = dt.Rows[i]["PR_ID"].ToString();
         prs.Supplier_ID = dt.Rows[i]["Supplier_ID"].ToString();
         list.Add(prs);
         }
         return list;
     }

  }


  public List<InfoRecord> GetInforecordByMaterilSuplier(List<MaterialSupplier> list)
  {
      List<InfoRecord> listinfo = new List<InfoRecord>();
      string SupplierID;
      string MaterialID;
      DataTable dt = new DataTable();
      for (int i = 0; i < list.Count; i++)
      {
          
          MaterialID = list[i].Material_ID;
          SupplierID = list[i].Supplier_ID;
          string sql = "SELECT * FROM [RecordInfo] WHERE MaterialId='" + MaterialID + "' AND SupplierId='" + SupplierID + "'";
          dt = DBHelper.ExecuteQueryDT(sql);
          if (dt.Rows.Count == 0)
          {
              MessageUtil.ShowTips("没有该项信息记录");
          }
          else
          {
              for (int j = 0; j < dt.Rows.Count; j++)
              {
                  InfoRecord info = new InfoRecord();
                  info.Material_ID = dt.Rows[j]["MaterialId"].ToString();
                  info.Material_Name = dt.Rows[j]["MaterialName"].ToString();
                  info.NetPrice = (float)Convert.ToDouble(dt.Rows[j]["NetPrice"].ToString());
                  info.Record_ID = dt.Rows[j]["RecordId"].ToString();
                  info.Supplier_ID = dt.Rows[j]["SupplierId"].ToString();
                  info.Supplier_Name = dt.Rows[j]["SupplierName"].ToString();
                  info.Factory_ID = dt.Rows[j]["FactoryId"].ToString();
                  info.Stock_ID = dt.Rows[j]["StockId"].ToString();
                  info.Price_Determine = dt.Rows[j]["PriceDetermineId"].ToString();
                  listinfo.Add(info);
              }
          }
          
      }
      return listinfo;
  }


  public OrderInfo GetOrderInfoByID(string Orderid)
  {
      OrderInfo odif = new OrderInfo();
      string sql = "select * from [Order_Info] where Order_ID='" + Orderid + "'";
      DataTable dt = DBHelper.ExecuteQueryDT(sql);
      if (dt.Rows.Count == 0)
      {
          MessageUtil.ShowError("没有任何行");

      }
      else
      {
          odif.Company_Code = dt.Rows[0]["Company_Code"].ToString();
          odif.Create_Time =Convert.ToDateTime( dt.Rows[0]["Create_Time"].ToString());
          odif.Delivery_Points = dt.Rows[0]["Delivery_Points"].ToString();
          odif.Delivery_Type = dt.Rows[0]["Delivery_Type"].ToString();
          odif.Order_ID = Orderid;
          odif.Order_Status = dt.Rows[0]["Order_Status"].ToString();
          odif.Order_Type = dt.Rows[0]["Order_Type"].ToString();
          odif.Purchase_Organization = dt.Rows[0]["Purchase_Organization"].ToString();
          odif.Supplier_ID = dt.Rows[0]["Supplier_ID"].ToString();
          odif.Total_Value =(float)Convert.ToDouble( dt.Rows[0]["Total_Value"].ToString());
                try {
                    odif.Total_Number = Convert.ToInt16(dt.Rows[0]["Total_Number"].ToString());
                }
                catch (Exception ex)
                {
                    odif.Total_Number = 0;
                }
          
      }
      return odif;

  }

  public List<OrderItem> GetOrderItemByID(string orderid)
  {
      List<OrderItem> list = new List<OrderItem>();
      string sql = "SELECT * FROM [Order_Item] WHERE Order_ID = '" + orderid + "'";
      DataTable dt = DBHelper.ExecuteQueryDT(sql);
      if (dt.Rows.Count == 0)
      {
          MessageUtil.ShowError("没有任何行");
          return list;

      }
      else
      {
          for (int i = 0; i < dt.Rows.Count; i++)
          {
              OrderItem odim = new OrderItem();
              odim.Batch_ID = dt.Rows[i]["Batch_ID"].ToString();
              odim.Delivery_Time =Convert.ToDateTime( dt.Rows[i]["Delivery_Time"].ToString());
              odim.Factory_ID = dt.Rows[i]["Factory_ID"].ToString();
              odim.Info_Number = dt.Rows[i]["Info_Number"].ToString();
              odim.Info_Type = dt.Rows[i]["Info_Type"].ToString();
              odim.Material_Group = dt.Rows[i]["Material_Group"].ToString();
              odim.Material_ID = dt.Rows[i]["Mterial_ID"].ToString();
              odim.Net_Price =(float)Convert.ToDouble( dt.Rows[i]["Net_Price"].ToString());
              odim.Number =Convert.ToInt16( dt.Rows[i]["Number"].ToString());
              odim.Order_ID = orderid;
              odim.PR_ID = dt.Rows[i]["PR_ID"].ToString();
              odim.Stock_ID = dt.Rows[i]["Stock_ID"].ToString();
              odim.Total_Price =(float)Convert.ToDouble( dt.Rows[i]["Total_Price"].ToString());
              odim.Unit = dt.Rows[i]["Unit"].ToString();
              odim.Info_Number = dt.Rows[i]["Info_Number"].ToString();
              list.Add(odim);



          }
          return list;
      }

  }

  public List<InvoiceFront> GetInvoiceFrontByID(string orderid)
  {
      List<InvoiceFront> list = new List<InvoiceFront>();
      string sql = "SELECT * FROM [Invoice] WHERE Certificate_Code = '" + orderid + "'";
    DataTable dt =  DBHelper.ExecuteQueryDT(sql);
    if (dt.Rows.Count == 0)
    {
        //MessageUtil.ShowError("没有任何行");
        return list;

    }
    else
    {
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            InvoiceFront invo = new InvoiceFront();
            invo.Certificate_Code = dt.Rows[i]["Certificate_Code"].ToString();
            invo.Certificate_Type = dt.Rows[i]["Certificate_Type"].ToString();
            invo.Create_Time = Convert.ToDateTime(dt.Rows[i]["Create_Time"].ToString());
            invo.Currency = dt.Rows[i]["Currency"].ToString();
            invo.Invoice_Code = dt.Rows[i]["Invoice_Code"].ToString();
            invo.Invoice_Makeout_Time =Convert.ToDateTime( dt.Rows[i]["Invoice_MakeOut_Time"].ToString());
            invo.Invoice_Number = dt.Rows[i]["Invoice_Number"].ToString();
            invo.Payment_Type = dt.Rows[i]["Payment_Type"].ToString();
            invo.sum = dt.Rows[i]["Sum"].ToString();
           // invo.Sum_Number =Convert.ToInt16( dt.Rows[i]["Sum_Number"].ToString());
            list.Add(invo);
            
        }
        return list;

    }

  }

  public List<MaterialDocumentMM> GetMaterialDocumentMMByID(string orderid)
  {
      List<MaterialDocumentMM> list = new List<MaterialDocumentMM>();
      string sql = "SELECT * FROM [MaterialDocument] WHERE Order_ID = '" + orderid + "'";
      DataTable dt = DBHelper.ExecuteQueryDT(sql);
      if (dt.Rows.Count == 0)
      {
          //MessageUtil.ShowError("没有任何行");
          return list;

      }
      else
      {
          for (int i = 0; i < dt.Rows.Count; i++)
          {
              MaterialDocumentMM mtdc = new MaterialDocumentMM();
              mtdc.Delivery_ID = dt.Rows[i]["Delivery_ID"].ToString();
              mtdc.Document_Date =Convert.ToDateTime( dt.Rows[i]["Document_Date"].ToString());
              mtdc.Move_Type = dt.Rows[i]["Move_Type"].ToString();
              mtdc.Order_ID = orderid;
              mtdc.Posting_Date =Convert.ToDateTime( dt.Rows[i]["Posting_Date"].ToString());
              mtdc.ReceiptNote_ID = dt.Rows[i]["ReceiptNote_ID"].ToString();
              mtdc.Reversed =Convert.ToBoolean( dt.Rows[i]["Reversed"].ToString());
              mtdc.StockDocumentId = dt.Rows[i]["StockInDocument_ID"].ToString();
              mtdc.StockManager = dt.Rows[i]["StockManager"].ToString();
              mtdc.Total_Number =Convert.ToInt16(  dt.Rows[i]["Total_Number"].ToString());
              mtdc.Total_Value = (float)Convert.ToDouble(dt.Rows[i]["Total_Value"].ToString());
              list.Add(mtdc);


          }
          return list;
      }

  }
    }
}
