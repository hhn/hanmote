﻿using Lib.IDAL.MDIDAL.MT;
using Lib.Model.MD.MT;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Lib.SqlServerDAL.MDDAL.MT
{
    class MaterialDAL:MaterialIDAL
    {

        public List<string> GetAllMaterialName()
        {
            string sql = "SELECT * FROM [Material]";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            if (dt == null || dt.Rows.Count == 0)
            {
                return null;
            }
            List<string> list = new List<string>();
            string obj;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                obj = dt.Rows[i][2].ToString();
                list.Add(obj);
                obj = null;
            }
            return list;
            
        }

        //非MPN物料
        public List<string> GetAllMaterialID()
        {
            string sql = "SELECT * FROM [Material] WHERE MPN_NO <>'true' or MPN_NO is null";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            if (dt == null || dt.Rows.Count == 0)
            {
                return null;
            }
            List<string> list = new List<string>();
            string obj;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                obj = dt.Rows[i][1].ToString();
                list.Add(obj);
                obj = null;
            }
            return list;
        }


        public bool UpdateBasicInformation(Model.MD.MT.MaterialBase material)
        {
            string sql = "UPDATE [Material] SET Material_Name='" + material.Material_Name + "',";
            sql += "Measurement='" + material.Measurement + "',Division='" + material.Division + "',Material_Status='" + material.Material_Status + "',";
            sql += "EI_Period='" + material.EI_Period.ToString("yyyyMMdd") + "',Material_Standard='" + material.Material_Standard + "',Gross_Weight='" + material.Gross_Weight + "',";
            sql += "Net_Weight='" + material.Net_Weight + "',Order_Unit='"+material.Order_Unit+"',Variable_Unit='"+material.Variable_Unit+"',";
            sql += "Market_Regulation_Identity='" + material.Market_Regulation_Identity + "',Material_Level='" + material.Material_Level + "',Status_Information='"+material.Status_Information+ "',Material_Purchase_Type='" + material.Material_Purchase_Type + "'";
            sql += "WHERE Material_ID = '" + material.Material_ID + "'";
            DBHelper.ExecuteNonQuery(sql);
            return true;
        }


        public Model.MD.MT.MaterialBase GetMaterialBasicInformation(string materialID)
        {
            string sql = "SELECT * FROM [Material] WHERE Material_ID='" + materialID + "'";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            MaterialBase mt = new MaterialBase();
            if (dt.Rows.Count == 0)
                return mt;
            DataRow row = dt.Rows[0];
            mt.ID = Convert.ToInt16(dt.Rows[0][0].ToString());
            mt.Material_ID = dt.Rows[0][1].ToString();
            mt.Material_Name = dt.Rows[0][2].ToString();
            mt.Material_Type = dt.Rows[0][3].ToString();
            mt.Measurement = dt.Rows[0][4].ToString();
            mt.In_Identification = dt.Rows[0][5].ToString();
            mt.Type_Name = dt.Rows[0][6].ToString();
            mt.Condition_Code = dt.Rows[0][7].ToString();
            mt.Material_Standard = dt.Rows[0][8].ToString();
            mt.Status_Information = dt.Rows[0][9].ToString();
            mt.Purpose = dt.Rows[0][10].ToString();
            mt.Material_Status = dt.Rows[0][11].ToString();
            mt.Special_Requirements = dt.Rows[0][12].ToString();
            mt.Temperature_Condition = dt.Rows[0][13].ToString();
            mt.Container_Demand = dt.Rows[0][14].ToString();
            mt.Division = dt.Rows[0][15].ToString();
            mt.Material_Group = dt.Rows[0][16].ToString();
            mt.Label_Type = dt.Rows[0][17].ToString();
            mt.Storage_Condition = dt.Rows[0][18].ToString();
            mt.Dangerous_MTNumber = dt.Rows[0][19].ToString();
            if (dt.Rows[0][20].ToString() == "")
                mt.Document_Number = 0;
            else
                mt.Document_Number = Convert.ToInt16(dt.Rows[0][20].ToString());
            mt.MIN_SLlife = dt.Rows[0][21].ToString();
            mt.Total_SLlife = dt.Rows[0][22].ToString();
            mt.SLED_Identify = dt.Rows[0][23].ToString();
            mt.SLED_Rule = dt.Rows[0][24].ToString();
            mt.Storge_Percentage = dt.Rows[0][25].ToString();
            if (dt.Rows[0][26].ToString() == "")
                mt.Gross_Weight = 0;
            else
                mt.Gross_Weight = Convert.ToDecimal(dt.Rows[0][26].ToString());
            if (dt.Rows[0][27].ToString() == "")
                mt.Net_Weight = 0;
            else
                mt.Net_Weight = Convert.ToDecimal(dt.Rows[0][27].ToString());
            mt.Weight_Unit = dt.Rows[0][28].ToString();
            mt.Volume_Unit = dt.Rows[0][29].ToString();
            mt.Dimension = dt.Rows[0][30].ToString();
            mt.Discount_Qualifications = dt.Rows[0][31].ToString();
            mt.Value_Code = dt.Rows[0][32].ToString();
            mt.MRP_Group = dt.Rows[0][33].ToString();
            mt.ABC_Identify = dt.Rows[0][34].ToString();
            mt.Order_Unit = dt.Rows[0][35].ToString();
            mt.Variable_Unit = dt.Rows[0][36].ToString();
            if(dt.Rows[0]["EI_Period"].ToString()!="")
            mt.EI_Period =Convert.ToDateTime( dt.Rows[0]["EI_Period"].ToString());
            if (row["Market_Regulation_Identity"].ToString() == "")
                mt.Market_Regulation_Identity = false;
            else
            mt.Market_Regulation_Identity =Convert.ToBoolean( row["Market_Regulation_Identity"].ToString());
            mt.Material_Level = row["Material_Level"].ToString();
            mt.Material_Class = row["Material_Class"].ToString();
            if (row["Material_Purchase_Type"].ToString() == "")
                mt.Material_Purchase_Type = false;
            else
            mt.Material_Purchase_Type = Convert.ToBoolean(row["Material_Purchase_Type"].ToString());
            return mt;
        }


        public List<string> GetAllMaterialNameMPN()
        {
            string sql = "SELECT * FROM [Material] WHERE MPN_NO ='true' ";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            if (dt == null || dt.Rows.Count == 0)
            {
                return null;
            }
            List<string> list = new List<string>();
            string obj;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                obj = dt.Rows[i][1].ToString();
                list.Add(obj);
                obj = null;
            }
            return list;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="materialId"></param>
        /// <returns></returns>
        public MaterialBase getMaterialBaseById(string materialId)
        {
            MaterialBase mb = new MaterialBase();
            StringBuilder sb = new StringBuilder("select * from Material where Material_ID='" + materialId + "'");
            string sqlText = sb.ToString();
            DataTable dt = DBHelper.ExecuteQueryDT(sqlText);
            if (dt != null && dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                mb.Material_Name = dr["Material_Name"].ToString();
                mb.Material_Group = dr["Material_Group"].ToString();
                mb.Measurement = dr["Measurement"].ToString();
            }
            return mb;
        }
        /// <summary>
        /// 获取所有（不包含在list中）物料信息
        /// </summary>
        /// <param name="materialIds"></param>
        /// <returns></returns>
        public List<MaterialBase> GetMaterialBasesInIDLimit(List<string> materialIds,string condition)
        {
            //如果List为空
            if (materialIds == null || materialIds.Count == 0)
            {
                return GetMaterialBases(condition);
            }
            if(!string.IsNullOrEmpty(condition))
            {
                condition += " AND  ";
            }
            else
            {
                condition = " where ";
            }
            List<MaterialBase> limitMaterialBases = new List<MaterialBase>();
            StringBuilder sb = new StringBuilder("select * from Material ")
                    .Append( condition +"  Material_ID not in ( ");
            foreach (string s in materialIds)
            {
                sb.Append("'" + s + "',");
            }
            string sql = sb.ToString();
            string sqlText = sql.Substring(0, sql.Length - 1) + ")";
            DataTable dt = DBHelper.ExecuteQueryDT(sqlText);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        MaterialBase materialBase = new MaterialBase();
                        materialBase.Material_ID = dr["Material_ID"].ToString();
                        materialBase.Material_Name = dr["Material_Name"].ToString();
                        materialBase.Material_Group = dr["Material_Group"].ToString();
                        materialBase.Measurement = dr["Measurement"].ToString();
                        limitMaterialBases.Add(materialBase);
                    }
                }
            }
            return limitMaterialBases;
        }
        /// <summary>
        /// 获取所有物料基本信息
        /// </summary>
        /// <returns></returns>
        public List<MaterialBase> GetMaterialBases(string condition)
        {
            List<MaterialBase> materialBases = new List<MaterialBase>();
            string sqlText = "SELECT * FROM Material " + condition;
            DataTable dt = DBHelper.ExecuteQueryDT(sqlText);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        MaterialBase materialBase = new MaterialBase();
                        materialBase.Material_ID = dr["Material_ID"].ToString();
                        materialBase.Material_Name = dr["Material_Name"].ToString();
                        materialBase.Material_Group = dr["Material_Group"].ToString();
                        materialBase.Measurement = dr["Measurement"].ToString();
                        materialBases.Add(materialBase);
                    }
                }
            }
            return materialBases;
        }

    }
}
