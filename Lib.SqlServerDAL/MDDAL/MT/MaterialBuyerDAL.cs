﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.IDAL.MDIDAL.MT;
using System.Data;
using Lib.Model.MD.MT;

namespace Lib.SqlServerDAL.MDDAL.MT
{
   public class MaterialBuyerDAL:MaterialBuyerIDAL
    {
        public Model.MD.MT.MaterialBase GetBasicInformation(string MaterialID)
        {
            string sql = "SELECT * FROM [Material] WHERE Material_ID='" + MaterialID + "'";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            MaterialBase mt = new MaterialBase();
            if (dt != null)
            {
                mt.ID = Convert.ToInt16(dt.Rows[0][0].ToString());
                mt.Material_ID = dt.Rows[0][1].ToString();
                mt.Material_Name = dt.Rows[0][2].ToString();
                mt.Material_Type = dt.Rows[0][3].ToString();
                mt.Measurement = dt.Rows[0][4].ToString();
                mt.In_Identification = dt.Rows[0][5].ToString();
                mt.Type_Name = dt.Rows[0][6].ToString();
                mt.Condition_Code = dt.Rows[0][7].ToString();
                mt.Material_Standard = dt.Rows[0][8].ToString();
                mt.Status_Information = dt.Rows[0][9].ToString();
                mt.Purpose = dt.Rows[0][10].ToString();
                mt.Material_Status = dt.Rows[0][11].ToString();
                mt.Special_Requirements = dt.Rows[0][12].ToString();
                mt.Temperature_Condition = dt.Rows[0][13].ToString();
                mt.Container_Demand = dt.Rows[0][14].ToString();
                mt.Division = dt.Rows[0][15].ToString();
                mt.Material_Group = dt.Rows[0][16].ToString();
                mt.Label_Type = dt.Rows[0][17].ToString();
                mt.Storage_Condition = dt.Rows[0][18].ToString();
                mt.Dangerous_MTNumber = dt.Rows[0][19].ToString();
                if (dt.Rows[0][20].ToString() == "")
                    mt.Document_Number = 0;
                else
                    mt.Document_Number = Convert.ToInt16(dt.Rows[0][20].ToString());
                mt.MIN_SLlife = dt.Rows[0][21].ToString();
                mt.Total_SLlife = dt.Rows[0][22].ToString();
                mt.SLED_Identify = dt.Rows[0][23].ToString();
                mt.SLED_Rule = dt.Rows[0][24].ToString();
                mt.Storge_Percentage = dt.Rows[0][25].ToString();
                if (dt.Rows[0][26].ToString() == "")
                    mt.Gross_Weight = 0;
                else
                    mt.Gross_Weight = Convert.ToDecimal(dt.Rows[0][26].ToString());
                if (dt.Rows[0][27].ToString() == "")
                    mt.Net_Weight = 0;
                else
                    mt.Net_Weight = Convert.ToDecimal(dt.Rows[0][27].ToString());
                mt.Weight_Unit = dt.Rows[0][28].ToString();
                mt.Volume_Unit = dt.Rows[0][29].ToString();
                mt.Dimension = dt.Rows[0][30].ToString();
                mt.Discount_Qualifications = dt.Rows[0][31].ToString();
                mt.Value_Code = dt.Rows[0][32].ToString();
                mt.MRP_Group = dt.Rows[0][33].ToString();
                mt.ABC_Identify = dt.Rows[0][34].ToString();
                mt.Order_Unit = dt.Rows[0][35].ToString();
                mt.Variable_Unit = dt.Rows[0][36].ToString();
                try
                {
                    mt.EI_Period = Convert.ToDateTime(dt.Rows[0]["EI_Period"].ToString());
                }
                catch(Exception e)
                {
                    //
                }
               
                if (dt.Rows[0][38].ToString() == "")
                    mt.Batch_Mark = false;
                else
                    mt.Batch_Mark = Convert.ToBoolean(dt.Rows[0][38].ToString());
                if (dt.Rows[0][39].ToString() == "")
                    mt.MPN_Mark = false;
                else
                    mt.MPN_Mark = Convert.ToBoolean(dt.Rows[0][39].ToString());
            }
            return mt;
        }

        public Model.MD.MT.MaterialFactory GetMtFtyInformation(string MaterialID, string FactoryID)
        {
            string sql = "SELECT * FROM [MT_FTY] WHERE Material_ID='" + MaterialID + "'AND Factory_ID='" + FactoryID + "'";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            MaterialFactory mtfty = new MaterialFactory();
            if (dt.Rows.Count == 0)
            {
                mtfty.Assessment_Category = "";
                mtfty.Buyer_Group = "";
                mtfty.Check_Mark = false;
                mtfty.Currency_Unit = "";
                mtfty.Current_Price = 0;
                mtfty.Delivery_Unit = "";
                mtfty.Evaluation_Class = "";
                mtfty.Factory_ID = "";
                mtfty.Factory_Name = "";
                mtfty.Future_Price = 0;
                mtfty.Gross_Inventory = 0;
                mtfty.Hourly_Basis = "";
                mtfty.ID = -1;
                mtfty.Material_ID = "";
                mtfty.Material_Name = "";
                mtfty.MAX_STPeriod = 0;
                mtfty.Moving_AGPrice = 0;
                mtfty.MTtax_Status = "";
                mtfty.MTtransport_Group = "";
                mtfty.Normal_Price = 0;
                mtfty.Previous_Price = 0;
                mtfty.Price_Unit = "";
                mtfty.Ptmtl_Status = "";
            }
            else
            {
                mtfty.ID = Convert.ToInt16(dt.Rows[0][0].ToString());
                mtfty.Material_ID = dt.Rows[0][1].ToString();
                mtfty.Material_Name = dt.Rows[0][2].ToString();
                mtfty.Factory_ID = dt.Rows[0][3].ToString();
                mtfty.Factory_Name = dt.Rows[0][4].ToString();
                mtfty.Buyer_Group = dt.Rows[0][5].ToString();
                mtfty.Ptmtl_Status = dt.Rows[0][6].ToString();
                mtfty.MTtax_Status = dt.Rows[0][7].ToString();
                mtfty.MTtransport_Group = dt.Rows[0][8].ToString();
                mtfty.Currency_Unit = dt.Rows[0][10].ToString();
                mtfty.Assessment_Category = dt.Rows[0][11].ToString();
                mtfty.Evaluation_Class = dt.Rows[0][14].ToString();
                if (dt.Rows[0][17].ToString() == "")
                    mtfty.Moving_AGPrice = 0;
                else
                    mtfty.Moving_AGPrice = (float)Convert.ToDouble(dt.Rows[0][17].ToString());
                if (dt.Rows[0][18].ToString() == "")
                    mtfty.Gross_Inventory = 0;
                else
                    mtfty.Gross_Inventory = Convert.ToInt16(dt.Rows[0][18].ToString());
                mtfty.Price_Unit = dt.Rows[0][20].ToString();
                if (dt.Rows[0][21].ToString() == "")
                    mtfty.Normal_Price = 0;
                else
                    mtfty.Normal_Price = (float)Convert.ToDouble(dt.Rows[0][21].ToString());
                if (dt.Rows[0][22].ToString() == "")
                    mtfty.TotalValue = 0;
                else
                    mtfty.TotalValue = (float)Convert.ToDouble(dt.Rows[0][22].ToString());
                if (dt.Rows[0][23].ToString() == "")
                    mtfty.Future_Price = 0;
                else
                    mtfty.Future_Price = (float)Convert.ToDouble(dt.Rows[0][23].ToString());
                if (dt.Rows[0][24].ToString() == "")
                    mtfty.Current_Price = 0;
                else
                    mtfty.Current_Price = (float)Convert.ToDouble(dt.Rows[0][24].ToString());
                if (dt.Rows[0][25].ToString() == "")
                    mtfty.Previous_Price = 0;
                else
                    mtfty.Previous_Price = (float)Convert.ToDouble(dt.Rows[0][25].ToString());
                if (dt.Rows[0][26].ToString() == "")
                    mtfty.Check_Mark = false;
                else
                    mtfty.Check_Mark = Convert.ToBoolean(dt.Rows[0][26].ToString());
                mtfty.Delivery_Unit = dt.Rows[0][27].ToString();
                if (dt.Rows[0][28].ToString() == "")
                    mtfty.MAX_STPeriod = 0;
                else
                    mtfty.MAX_STPeriod = (float)Convert.ToDouble(dt.Rows[0][28].ToString());
                mtfty.Hourly_Basis = dt.Rows[0][29].ToString();
                if (dt.Rows[0]["Source_List"].ToString() == "")
                    mtfty.Source_List = false;
                else
                    mtfty.Source_List = Convert.ToBoolean(dt.Rows[0]["Source_List"].ToString());


            }
            return mtfty;
        }

        public bool UpdateBasicInformation(Model.MD.MT.MaterialBase material)
        {
            string sql = "UPDATE [Material] SET Material_Name = '" + material.Material_Name + "',";
            sql += "Material_Group='" + material.Material_Group + "',Measurement='" + material.Measurement + "',";
            sql += "Order_Unit='" + material.Order_Unit + "',Variable_Unit='" + material.Variable_Unit + "',Discount_Qualifications='" + material.Discount_Qualifications + "',";
            sql += "Batch_Mark='" + material.Batch_Mark + "',MPN_Mark='" + material.MPN_Mark + "'" + "',minDeliveryNum='" + material.minDeliveryNum + "'";
            sql += "WHERE Material_ID='" + material.Material_ID + "'";
            DBHelper.ExecuteNonQuery(sql);
            return true;
        }

        public bool UpdateMtFtyInformation(Model.MD.MT.MaterialFactory mtfty)
        {
           string sql = "SELECT * FROM [MT_FTY] WHERE Material_ID='" + mtfty.Material_ID + "'AND Factory_ID='" + mtfty.Factory_ID + "'";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            if (dt.Rows.Count == 0)
            {
                string sql1 = "INSERT INTO [MT_FTY] (Material_ID,Material_Name,Factory_ID,Factory_Name,";
                sql1 += "Buyer_Group,Ptmtl_Status,MTtax_Status,MTtransport_Group,Source_List)";
                sql1 += "VALUES('" + mtfty.Material_ID + "','" + mtfty.Material_Name + "','" + mtfty.Factory_ID + "','" + mtfty.Factory_Name + "',";
                sql1 += "'" + mtfty.Buyer_Group + "','" + mtfty.Ptmtl_Status + "','" + mtfty.MTtax_Status + "','" + mtfty.MTtransport_Group + "','"+mtfty.Source_List+"')";
                DBHelper.ExecuteNonQuery(sql1);
            }
            else
            {
                string sql2 = " UPDATE [MT_FTY] SET Material_Name='"+mtfty.Material_Name+"',Factory_Name='"+mtfty.Factory_Name+"',";
                sql2 += "Buyer_Group='" + mtfty.Buyer_Group + "',qualityClass = '"+ mtfty.qualityClass + "',Ptmtl_Status='" + mtfty.Ptmtl_Status + "',MTtax_Status='" + mtfty.MTtax_Status + "',";
                sql2 += "MTtransport_Group='" + mtfty.MTtransport_Group + "',Source_List='" + mtfty.Source_List + "' WHERE Material_ID='" + mtfty.Material_ID + "' AND Factory_ID='" + mtfty.Factory_ID + "'";
                DBHelper.ExecuteNonQuery(sql2);
            }
            return true;

            }
            }
        }

    

