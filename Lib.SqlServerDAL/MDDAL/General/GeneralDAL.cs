﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.IDAL.MDIDAL.Gerneral;
using System.Data;
using Lib.Model.MD;
using Lib.Common.CommonUtils;
using Lib.Model.MD.GN;
using Lib.Model.MD.SP;
using System.Data.SqlClient;
using Lib.Model.MT_GroupModel;

namespace Lib.SqlServerDAL.MDDAL.General
{
   public class GeneralDAL:GeneralIDAL
   {
       #region  付款条件信息
       List<string> GeneralIDAL.GetAllPaymentClauseName()
        {
            string sql = "SELECT * FROM [Payment_Clause]";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            if (dt == null || dt.Rows.Count == 0)
            {
                return null;
            }
            List<string> list = new List<string>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                list.Add(Convert.ToString(dt.Rows[i][0]));
                
            }
            return list;
        }

        DataTable GeneralIDAL.GetAllPaymentClause()
        {
            string sql = "SELECT Clause_ID as '条件编号',Description as '描述' FROM [Payment_Clause]";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            return dt;
        }

        PaymentClause GeneralIDAL.GetClause(string ID)
        {
            string sql = "SELECT * FROM [Payment_Clause] WHERE Clause_ID='" + ID + "'";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            if (dt.Rows.Count == 0)
                return null;
            DataRow row = dt.Rows[0];
            PaymentClause clasue = new PaymentClause();
            clasue.Clause_ID = row["Clause_ID"].ToString();
            clasue.Description = row["Description"].ToString();
            clasue.First_Discount =(float)Convert.ToDouble(row["First_Discount"].ToString());
            clasue.First_Date = Convert.ToInt16(row["First_Date"].ToString());
            clasue.Second_Discount = (float)Convert.ToDouble(row["Second_Discount"].ToString());
            clasue.Second_Date = Convert.ToInt16(row["Second_Date"].ToString());
            clasue.Final_Date = Convert.ToInt16(row["Final_Date"].ToString());
            return clasue ;
        }

        bool GeneralIDAL.NewPaymentClause(string ClauseID, string Description, float FirstDiscount, int FirstDate, float SecondDiscount, int SecondDate, int FinalDate)
        {
            string sql = "INSERT INTO [Payment_Clause] (Clause_ID,Description,First_Discount,First_Date,Second_Discount,Second_Date,Final_Date)VALUES('" + ClauseID + "','" + Description + "','" + FirstDiscount + "','" + FirstDate + "','" + SecondDiscount + "','" + SecondDate + "','" + FinalDate + "')";
            DBHelper.ExecuteNonQuery(sql);
            return true;
        }

        bool GeneralIDAL.DeletePaymentClause(string ID)
        {
            string sql = "DELETE FROM [Payment_Clause] WHERE Clause_ID='" + ID + "'";
            DBHelper.ExecuteNonQuery(sql);
            return true;
        }
       #endregion

       #region 维护物料组

        DataTable GeneralIDAL.GetAllMaterialGroup(int pageSize,int pageIndex)
        {
            string sql = @"SELECT top " + pageSize + " Material_Group as '编号',Category as '物料组类型',Description as '名称',MtGroupClass as '分级',Evallevel as '分级描述' FROM [Material_Group] where Material_Group not in(select top " + pageSize * (pageIndex - 1) + " Material_Group from Material_Group ORDER BY Material_Group ASC)ORDER BY Material_Group";
            // string sql = @"SELECT top "+pageSize+" Material_Group as '编号',Category as '物料组类型',Description as '名称',MtGroupClass as '分级',Evallevel as '分级描述',CGreenValue as '认证绿色目标',CLowValue as '认证最低目标',PGreenValue as '绩效绿色目标',PLowValue as '绩效最低目标' FROM [Material_Group] where Material_Group not in(select top "+pageSize*(pageIndex-1)+" Material_Group from Material_Group ORDER BY Material_Group ASC)ORDER BY Material_Group";
            SqlParameter[] sqlParams ={
                new SqlParameter("@pageSize", pageSize),
                new SqlParameter("@pageIndex", pageIndex)

            };

            DataTable dt = DBHelper.ExecuteQueryDT(sql, sqlParams);
            return dt;
        }
        DataTable GeneralIDAL.GetAllSupplier_pinshen()
        {
            string sql = "SELECT ID as '用户编码',name as '用户名',password as '密码',evalCode as '分工',email as '邮箱',mobile as '联系方式',telPhone as '固定电话' FROM [EvaluateInfo]";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            return dt;
        }
        DataTable GeneralIDAL.GetSelectedEval(string fengong)
        {
            string sql = "SELECT * FROM [Supplier_pinshen] where Fengong="+"'"+fengong +"'";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            return dt;
        }

        string  GeneralIDAL.GetSelectedEvalid(string fengong)
        {
            string sql = "SELECT User_ID FROM [Supplier_pinshen] where User_Name=" + "'" + fengong + "'";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            string d = "";
            d = dt.Rows[0][0].ToString() ;
            return d;
        }

        bool GeneralIDAL.NewMaterialGroup(MaterialGroupModel materialGroupModel)
        {
            bool flag = false;
            //string sql = "insert into Material_Group (Material_Group,Description,MtGroupClass,Evallevel,Category)values('" + materialGroupModel.MtGroupId + "','" + materialGroupModel.MtGroupName + "','" + materialGroupModel.MtGroupLevel + "','" + materialGroupModel.MtGroupLevelValue + "','" + materialGroupModel.MtGroupClass + "'" + ") ";
            /*string sql = @"
                 IF EXISTS ( SELECT * FROM [Material_Group] WHERE Description =@description  ) UPDATE [Material_Group]  SET Material_Group = @mtId, MtGroupClass =@evaluate,
                    Evallevel = @evalid,
                    Category = @type 
                    WHERE
	                    Description = @description;
                    ELSE INSERT INTO Material_Group (Material_Group, Description, MtGroupClass, Evallevel, Category,CGreenValue,CLowValue,PGreenValue,PLowValue )
                    VALUES
	                    (@mtId,@description,@evaluate ,@evalid, @type,@CGreenValue,@CLowValue,@PGreenValue,@PLowValue)";*/


            string sql = @"IF EXISTS ( SELECT * FROM [Material_Group] WHERE Description =@description  ) UPDATE [Material_Group]  SET Material_Group = @mtId, MtGroupClass =@evaluate,
                    Evallevel = @evalid,
                    Category = @type 
                    WHERE
	                    Description = @description;
                    ELSE INSERT INTO Material_Group (Material_Group, Description, MtGroupClass, Evallevel, Category)
                    VALUES
	                    (@mtId,@description,@evaluate ,@evalid, @type)";
            SqlParameter[] sqlparams = {
                new SqlParameter("@mtId",materialGroupModel.MtGroupId),
                new SqlParameter("@description",materialGroupModel.MtGroupName),
                new SqlParameter("@evaluate",materialGroupModel.MtGroupLevel),
                new SqlParameter("@evalid",materialGroupModel.MtGroupLevelValue),
                new SqlParameter("@type",materialGroupModel.MtGroupClass)
               // new SqlParameter("@CGreenValue",materialGroupModel.CGreenValue),
                //new SqlParameter("@CLowValue",materialGroupModel.CLowValue),
              //  new SqlParameter("@PGreenValue",materialGroupModel.PGreenValue),
               // new SqlParameter("@PLowValue",materialGroupModel.PLowValue)
            };

            if (DBHelper.ExecuteNonQuery(sql, sqlparams) == 1) {

                flag = true;
            }
            return flag;
        }
        bool GeneralIDAL.NStatus(string description)
        {
            string sql = "select * from Material_Group where Description='"+ description + "'";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            if (dt == null || dt.Rows.Count == 0)
                return true;
            else
                return false;
        }
        void GeneralIDAL.Ndelete(string description)
        {
            string sql = "delete from Material_Group where Description='" + description + "'";
           DBHelper.ExecuteQueryDT(sql);
            
        }
        bool GeneralIDAL.NewSupplier_pinshen(string id, string name, string fengong, string email, string phone, string sphone, string password)
        {
            string sql = "INSERT INTO [Supplier_pinshen] Values ('" + id + "','" + name + "','" + fengong + "','" + email + "','" + phone + "','" +sphone + "','" + password + "')";
            DBHelper.ExecuteNonQuery(sql);
            return true;
        }

        bool GeneralIDAL.DeleteMaterialGroup(string ID)
        {
             string sql = "delete FROM [Material_Group] WHERE Material_Group = '" + ID + "'";
            DBHelper.ExecuteNonQuery(sql);
            return true;
        }

        bool GeneralIDAL.DeleteSupplier_pinshen(string ID)
        {
            string sql = "delete FROM [EvaluateInfo] WHERE ID = '" + ID + "'";
            DBHelper.ExecuteNonQuery(sql);
            return true;
        }
        //更新物料组数据
        bool GeneralIDAL.UpdateMaterialGroup(MaterialGroupModel materialGroupModel)
        {
            string sql = @"update Material_Group set Description = '" + materialGroupModel.MtGroupName + "'," + "Factory_ID = '" + materialGroupModel.FactoryId + "'," + "MtGroupClass = '" + materialGroupModel.MtGroupLevel + "'," +  "Evallevel = '" + materialGroupModel.MtGroupLevelValue + "'," + "Category = '"+materialGroupModel.MtGroupClass+"',CGreenValue = '"+materialGroupModel.CGreenValue + "',CLowValue = '" + materialGroupModel.CLowValue + "',PGreenValue = '" + materialGroupModel.PGreenValue + "',PLowValue = '" + materialGroupModel.PLowValue + "' where Material_Group='" + materialGroupModel.MtGroupId+"'";
            DBHelper.ExecuteNonQuery(sql);
            if (DBHelper.ExecuteNonQuery(sql) > 0) {
                return true;
            }
            return false;
        }
        bool GeneralIDAL.UpdateSupplier_pinshen(string id,string userid, string name, string password, string fengong, string email, string phone, string sphone)
        {
            string sql = "update EvaluateInfo set ID='" + userid + "'," + "name = '" + name + "'," + "password = '" + password  + "'," + "evalCode = '" + fengong  + "'," + "email = '" + email  + "'," + "mobile = '" + phone + "'," + "telPhone = '" + sphone + "'where ID='" + userid + "'";
            DBHelper.ExecuteNonQuery(sql);
            return true;
        }
        List<string> GeneralIDAL.GetAllGroupName()
        {
            string sql = "SELECT * FROM [Material_Group]";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            if (dt == null || dt.Rows.Count == 0)
            {
                return null;
            }
            List<string> list = new List<string>();
            string obj;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                obj = dt.Rows[i][1].ToString();
                list.Add(obj);
                obj = null;
            }
            return list;
        }

        public DataTable GetBuyer_OrgNameByBuyer_Org(string Buyer_Org)
        {
            //Supplier_Purchasing_Org表
            string sql = "select Buyer_Org_Name from Buyer_Org where Buyer_Org = '" + Buyer_Org + "'";
            return DBHelper.ExecuteQueryDT(sql);
        }

        List<string> GeneralIDAL.GetAllPinshenName()
        {
            //Supplier_pinshen表
            string sql = "SELECT * FROM [Supplier_pinshen]";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            if(dt != null && dt.Rows.Count > 0)
            {
                List<string> list = new List<string>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    list.Add(dt.Rows[i][1].ToString());
                }
                return list;
            }
            return null;
        }
        #endregion

        public List<string> GetAllBuyerGroupByBuyer_Org(string Buyer_Org)
        {
            //Buyer_Group表
            string sql = "select Buyer_Group from Buyer_Group where Buyer_Org = '" + Buyer_Org + "'";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            if(dt != null && dt.Rows.Count > 0)
            {
                List<string> list = new List<string>(dt.Rows.Count);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    list.Add(Convert.ToString(dt.Rows[i][0]));
                }
                return list;
            }
            return null;
        }

        #region 维护物料信息

        public DataTable GetAllMaterialNameByMaterialGroup(string materialGroupName)
        {
            string sql = "SELECT Material_Name as name , Material_ID as id from [Material] WHERE Material_Group = '" + materialGroupName + "'";
            return DBHelper.ExecuteQueryDT(sql);
        }

        /// <summary>
        /// 获取所有的物料ID
        /// </summary>
        /// <returns>物料ID List</returns>
        public List<string> getAllMaterialIDList() {
            string sqlText = "select Material_ID from Material";

            List<string> materialIDList = new List<string>();
            DataTable result = DBHelper.ExecuteQueryDT(sqlText);
            foreach (DataRow row in result.Rows) {
                string materialID = row["Material_ID"].ToString();
                materialIDList.Add(materialID);
            }

            return materialIDList;
        }

       /// <summary>
       /// 查询物料信息
       /// </summary>
       /// <param name="materialID"></param>
       /// <returns></returns>
        public DataTable getMaterialInfo(string materialID) {
            string sql = "select * from Material where Material_ID = '"
                + materialID + "'";

            return DBHelper.ExecuteQueryDT(sql);
        }

       #endregion
       
       //获取公司代码
        public List<string> GetCompanyCode()
        {
            string sql = " SELECT * FROM [Company] ";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            if (dt == null || dt.Rows.Count == 0)
            {
                return null;
            }
            List<string> list = new List<string>();
            string obj;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                obj = dt.Rows[i][0].ToString();
                list.Add(obj);
                obj = null;
            }
            return list;
        }
        //获取所有货币类型
        public List<string> GetCurrencyType()
        {
            string sql = " SELECT * FROM [Currency] ";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            if (dt == null || dt.Rows.Count == 0)
            {
                return null;
            }
            List<string> list = new List<string>();
            string obj;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                obj = dt.Rows[i][2].ToString();
                list.Add(obj);
                obj = null;
            }
            return list;
        }
        //获取所有评估类
        public List<string> GetEvaluationClass()
        {
            return null;
        }
        //获取所有供应商
        public List<string> GetAllSupplier()
        {
            string sql = " SELECT * FROM [Supplier_Base] ";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            if (dt == null || dt.Rows.Count == 0)
            {
                return null;
            }
            List<string> list = new List<string>();
            string obj;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                obj = dt.Rows[i][1].ToString();
                list.Add(obj);
                obj = null;
            }
            return list;
        }
        //根据供应商ID获取供应商描述
        public string GetSuppplierDescription(string SupplierID)
        {
            string sql = " SELECT * FROM [Supplier_Base] WHERE Supplier_ID='" + SupplierID + "'";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            if (dt == null || dt.Rows.Count == 0)
            {
                return null;
            }
            string obj = dt.Rows[0][3].ToString();
            return obj;
        }
        //获取所有税码
        public List<string> GetTaxCode()
        {
            string sql = " SELECT * FROM [Tax_Code] ";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            if (dt == null || dt.Rows.Count == 0)
            {
                return null;
            }
            List<string> list = new List<string>();
            string obj;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                obj = dt.Rows[i][0].ToString();
                list.Add(obj);
                obj = null;
            }
            return list;
        }
        //通过所选税码获取税码描述
        public string GetTaxDescription(string TaxID)
        {
            string sql = " SELECT * FROM [Tax_Code] WHERE Tax_Code='" + TaxID + "'";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            if (dt == null || dt.Rows.Count == 0)
            {
                return null;
            }
            string obj = dt.Rows[0][1].ToString();
            return obj;
        }
        #region 维护工厂
        //获取所有工厂名称
        public List<string> GetAllFactory()
        {
            string sql = " SELECT * FROM [Factory] ";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            if (dt == null || dt.Rows.Count == 0)
            {
                return null;
            }
            List<string> list = new List<string>();
            string obj;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                obj = dt.Rows[i][1].ToString();
                list.Add(obj);
                obj = null;
            }
            return list;
        }
        //根据公司名称获取工厂代码
        public List<string> GetAllFactory(string companyID)
        {
            string sql = " SELECT * FROM [Factory] where Company_ID = '" + companyID + "'";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            if (dt == null || dt.Rows.Count == 0)
            {
                return null;
            }
            List<string> list = new List<string>();
            string obj;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                obj = dt.Rows[i][1].ToString();
                list.Add(obj);
                obj = null;
            }
            return list;
        }
        //通过所选工厂获取其所属库存地
        public List<string> GetAllStock(string FactoryID)
        {
            string sql = " SELECT * FROM [Stock] WHERE Factory_ID='" + FactoryID + "'";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            if (dt == null || dt.Rows.Count == 0)
            {
                return null;
            }
            List<string> list = new List<string>();
            string obj;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                obj = dt.Rows[i][1].ToString();
                list.Add(obj);
                obj = null;
            }
            return list;
        }
       //获取所有工厂信息
        DataTable GeneralIDAL.GetAllFactoryInformation()
        {
            string sql = "SELECT Factory_ID AS '工厂代码',Factory_Name AS '工厂名称',Company_ID AS '公司代码' FROM [Factory]";
           DataTable dt =  DBHelper.ExecuteQueryDT(sql);
           return dt;
            
        }
       //新建工厂
        bool GeneralIDAL.NewFactory(string Factory_ID, string factory_name, string company_code)
        {
            string sql = "INSERT INTO [Factory](Factory_ID,Factory_Name,Company_ID)VALUES('" + Factory_ID + "','" + factory_name + "','" + company_code + "')";
            DBHelper.ExecuteNonQuery(sql);
            return true;
        }
       //删除工厂
        bool GeneralIDAL.DeleteFactory(String Factory_ID)
        {
            string sql = "DELETE From [Factory] WHERE Factory_ID='" + Factory_ID + "'";
            DBHelper.ExecuteNonQuery(sql);
            return true;
        }
       /// <summary>
       /// 根据工厂编码读取工厂信息
       /// </summary>
       /// <param name="FactoryID"></param>
       /// <returns></returns>
        public Model.MD.GN.Factory GetFactoryInformationByID(string FactoryID)
        {
            Factory fac = new Factory();
            string sql = "SELECT * FROM [Factory] WHERE Factory_ID='" + FactoryID + "'";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            if(dt.Rows.Count ==0)
                MessageUtil.ShowError("所选工厂编码不存在");
            else
            {
                fac.FactoryID = dt.Rows[0][1].ToString();
                fac.FactoryName = dt.Rows[0][2].ToString();
                fac.CompanyCode = dt.Rows[0][3].ToString();
            }
            return fac;
        }
        #endregion
        #region 维护计量单位
        //获取所有计量单位名称
        List<string> GeneralIDAL.GetAllMeasurement()
        {
            string sql = " SELECT * FROM [Measurement] ";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            if (dt == null || dt.Rows.Count == 0)
            {
                return null;
            }
            List<string> list = new List<string>();
            string obj;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                obj = dt.Rows[i][1].ToString();
                list.Add(obj);
                obj = null;
            }
            return list;
        }
       //获取所有计量单位信息
        DataTable GeneralIDAL.GetAllMeasurementInformation()
        {
            string sql = "SELECT Measurement_ID AS '计量单位代码',Measurement_Name AS '计量单位名称',Relevant_Measurement AS '相关计量单位',Ratio AS '比率' FROM [Measurement]";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            return dt;
        }
       //新建计量单位
        bool GeneralIDAL.NewMeasurement(string meid, string mename, string remename, float ratio,string type)
        {
            string sql = "INSERT INTO [Measurement](Measurement_ID,Measurement_Name ,Relevant_Measurement ,Ratio,Measurement_Type)VALUES('" + meid + "','" + mename + "','" + remename + "','" + ratio + "','"+type+"')";
            DBHelper.ExecuteNonQuery(sql);
            return true;
        }
       //删除所选计量单位
        bool GeneralIDAL.DeleteMeasurement(string meid)
        {
            string sql = "DELETE From [Measurement] WHERE Measurement_ID='" + meid + "'";
            DBHelper.ExecuteNonQuery(sql);
            return true;
        }
        #endregion
        #region 维护仓库
       //获取所有仓库名称
        public List<string> GetAllStock()
        {
            string sql = " SELECT * FROM [Stock] ";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            if (dt == null || dt.Rows.Count == 0)
            {
                return null;
            }
            List<string> list = new List<string>();
            string obj;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                obj = dt.Rows[i][1].ToString();
                list.Add(obj);
                obj = null;
            }
            return list;
        }
       //获取所有仓库信息
        public DataTable GetAllStockInformation()
        {
            string sql = "SELECT Stock_ID AS '仓库代码',Stock_Name AS '仓库名称',Factory_ID AS '所属工厂代码' FROM [Stock]";
           DataTable dt = DBHelper.ExecuteQueryDT(sql);
            return dt;
        }
       //新建仓库
        public bool NewStock(string stockid, string stockname, string factoryid)
        {
            string sql = "INSERT INTO [Stock] (Stock_ID,Stock_Name,Factory_ID) VALUES ('" + stockid + "','" + stockname + "','" + factoryid + "')";
            DBHelper.ExecuteNonQuery(sql);
            return true;
        }
       //删除仓库
        public bool DeleteStock(string stock)
        {
            string sql = "DELETE FROM [Stock] WHERE Stock_ID='" + stock + "'";
            DBHelper.ExecuteNonQuery(sql);
            return true;
        }
        #endregion
        #region 维护公司
        //获取所有公司名称
        public List<string> GetAllCompany()
        {
            string sql = " SELECT * FROM [Company] ";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            if (dt == null || dt.Rows.Count == 0)
            {
                return null;
            }
            List<string> list = new List<string>();
            string obj;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                obj = dt.Rows[i][0].ToString();
                list.Add(obj);
                obj = null;
            }
            return list;
        }

        public DataTable GetAllCompanyInformation()
        {
            string sql = " SELECT Company_ID AS '公司代码',Company_Name AS '公司名称' FROM [Company] ";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            return dt;
        }

        public bool NewCompany(string cyid, string cyname)
        {
            string sql = "INSERT INTO [Company] (Company_ID,Company_Name)VALUES('" + cyid + "','" + cyname + "')";
            DBHelper.ExecuteNonQuery(sql);
            return true;
        }

        public bool DeleteCompany(string Company)
        {
            string sql = "DELETE FROM [Company] WHERE Company_ID='" + Company + "'";
            DBHelper.ExecuteNonQuery(sql);
            return true;
        }
        #endregion
        #region 维护采购组
        public List<string> GetAllBuyerGroup()
        {

            string sql = " SELECT * FROM [Buyer_Group] ";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            if (dt == null || dt.Rows.Count == 0)
            {
                return null;
            }
            List<string> list = new List<string>();
            string obj;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                obj = dt.Rows[i][0].ToString();
                list.Add(obj);
                obj = null;
            }
            return list;
        }

        public DataTable GetAllBuyerGroupInformation()
        {
            string sql = "SELECT Buyer_Group AS '采购组编码',Buyer_Group_Name AS '采购组名称',Material_Type AS '负责的物料类型' FROM [Buyer_Group]";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            return dt;
        }

        public bool NewBuyerGroup(string gpid, string gpname, string mttype)
        {
            string sql = "INSERT INTO [Buyer_Group] (Buyer_Group,Buyer_Group_Name,Material_Type)VALUES('" + gpid + "','" + gpname + "','" + mttype + "')";
            DBHelper.ExecuteNonQuery(sql);
            return true;
        }

        public bool DeleteBuyerGroup(string gpid)
        {
            string sql = "DELETE FORM [Buyer_Group] WHERE Buyer_Group = '" + gpid + "'";
               DBHelper.ExecuteNonQuery(sql);
                    return true;
        }
        #endregion
        #region 维护采购组织
        public List<string> GetAllBuyerOrganization()
        {
            string sql = "SELECT Buyer_Org AS '采购组织代码' FROM [Buyer_Org]";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            if (dt != null && dt.Rows.Count > 0)
            {
                List<string> list = new List<string>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    list.Add(dt.Rows[i][0].ToString());
                }
                return list;
            }
            return null;
        }
        //public List<string> GetAllBuyerOrganization(string BuyerGroupCode)
        //{
        //    string sql = "SELECT Buyer_Group AS '采购组织代码',Buyer_Org_Name AS '采购组织名称' FROM [Buyer_Group] WHERE PurchasingORG_ID = '" + BuyerGroupCode + "'";
        //    DataTable dt = DBHelper.ExecuteQueryDT(sql);
        //    if(dt != null && dt.Rows.Count > 0)
        //    {
        //        List<string> list = new List<string>();
        //        for (int i = 0; i < dt.Rows.Count; i++)
        //        {
        //            list.Add(dt.Rows[i][0].ToString());
        //        }
        //    }
        //    return null;
        //}


        public List<string> GetAllCurrency()
        {
            string sql = "SELECT * FROM [Currency]";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            if(dt == null || dt.Rows.Count == 0)
            {
                return null;
            }
            List<string> list = new List<string>();
            for (int k = 0; k < Convert.ToInt32(dt.Rows.Count.ToString()); k++)
            {
                list.Add(Convert.ToString(dt.Rows[k][2]));
            }
            return list;
        }

        public DataTable GetAllBuyerOrganizationInformation()
        {
            string sql = "SELECT Buyer_Org AS '采购组织代码',Buyer_Org_Name AS '采购组织名称' FROM [Buyer_Org]";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            return dt;
        }

        public bool NewBuyerOrganization(string boid, string boname)
        {
            string sql = "INSERT INTO [Buyer_Org](Buyer_Org,Buyer_Org_Name)VALUES('" + boid + "','" + boname + "')";
            DBHelper.ExecuteNonQuery(sql);
            return true;
        }

        public bool DeleteBuyerOrganization(string boid)
        {
            string sql = "DELETE FROM [Buyer_Org] WHERE Buyer_Org='" + boid + "'";
            DBHelper.ExecuteNonQuery(sql);
            return true;
        }
       /// <summary>
       /// 获取采购组织-物料组信息
       /// </summary>
       /// <returns></returns>
       public List<PorgMtGropRelation> GetPorgMtgpRelation()
        {
            List<PorgMtGropRelation> list = new List<PorgMtGropRelation>();
           // PorgMtGropRelation pmt = new PorgMtGropRelation();
            string sql = "SELECT * FROM [Porg_MtGroup_Relationship]";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            if (dt == null || dt.Rows.Count == 0)
            {
                MessageUtil.ShowError("没有相关信息");
                return null;
            }
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                PorgMtGropRelation pmt = new PorgMtGropRelation();
                pmt.Porg_ID = dt.Rows[i][1].ToString();
                pmt.Porg_Name = dt.Rows[i][2].ToString();
                pmt.MtGroup_ID = dt.Rows[i][3].ToString();
                pmt.MtGroup_Name = dt.Rows[i][4].ToString();
                list.Add(pmt);
            }
            return list;
        }
       /// <summary>
       /// 写入采购组织-物料组信息
       /// </summary>
       /// <param name="list"></param>
       /// <returns></returns>
        public bool NewPorgMtgpRelation(List<PorgMtGropRelation> list)
        {
            string sql="";
            for (int i = 0; i < list.Count; i++)
            {
                sql = sql + "INSERT INTO [Porg_MtGroup_Relationship] VALUES ('" + list[i].Porg_ID + "','" + list[i].Porg_Name + "','" + list[i].MtGroup_ID + "','" + list[i].MtGroup_Name + "');";
            }
            DBHelper.ExecuteNonQuery(sql);
            return true;
        }
        #endregion


        public List<string> GetAllTradeClauseID()
        {
            string sql = "SELECT * FROM [Trade_Clause]";
           DataTable dt =  DBHelper.ExecuteQueryDT(sql);
           if (dt == null || dt.Rows.Count == 0)
           {
               return null;
           }
           List<string> list = new List<string>();
           for (int i = 0; i < dt.Rows.Count; i++)
           {
               list.Add(Convert.ToString(dt.Rows[i][0]));
           }
           return list;
        }

        public DataTable GetTradeClauseByID(string clauseID)
        {
            string sql = "SELECT * From [Trade_Clause] WHERE TradeClause_ID = '" + clauseID +"'";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            return dt;
        }

        public DataTable GetAllTradeClauseInformation()
        {
            string sql = "SELECT TradeClause_ID AS '交付条件编码',TradeClause_Name AS '交付条件名称',Description AS '交付条件描述' FROM [Trade_Clause]";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            return dt;
        }

        public bool NewTradeClauseInformation(string ClauseID, string ClauseName, string Description)
        {
            string sql = "INSERT INTO [Trade_Clause] (TradeClause_ID,TradeClause_Name,Description) VALUES ('" + ClauseID + "',";
            sql += "'" + ClauseName + "','" + Description + "')";
            DBHelper.ExecuteNonQuery(sql);
            return true;
        }

        public bool DeleteTradeClause(string ClauseID)
        {
            string sql = "DELETE FROM [Trade_Clause] WHERE TradeClause_ID='" + ClauseID + "'";
            DBHelper.ExecuteNonQuery(sql);
            return true;
        }


        public List<string> GetAllEvaluationClass()
        {
            string sql = "SELECT * FROM [Evaluation_Class]";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            if (dt == null || dt.Rows.Count == 0)
            {
                return null;
            }
            List<string> list = new List<string>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                list.Add(Convert.ToString(dt.Rows[i][1]));
            }
            return list;
        }

        public DataTable GetAllEvaluationClassInformation()
        {
            string sql = "SELECT Class_ID AS '评估类编码',Class_Name AS '评估类名称',Description AS '评估类描述' FROM [Evaluation_Class] ";
            return DBHelper.ExecuteQueryDT(sql);
        }

        public bool NewEvaluationClass(string ClassID, string ClassName, string Description)
        {
            string sql = "INSERT INTO [Evaluation_Class] (Class_ID,Class_Name,Description) VALUES ('"+ClassID+"','"+ClassName+"','"+Description+"')";
            DBHelper.ExecuteNonQuery(sql);
            return true;
        }

        public bool DeleteEvaluationClass(string ClassID)
        {
            string sql = "DELETE FROM [Evaluation_Class] WHERE Class_ID='" + ClassID + "'";
            DBHelper.ExecuteNonQuery(sql);
            return true;
        }


        public List<string> GetAllDivision()
        {
            string sql = "SELECT * FROM [Division]";
           DataTable dt =  DBHelper.ExecuteQueryDT(sql);
           if (dt == null || dt.Rows.Count == 0)
           {
               return null;
           }
           List<string> list = new List<string>();
           for (int i = 0; i < dt.Rows.Count; i++)
           {
               list.Add(Convert.ToString(dt.Rows[i][0]));
           }
           return list;
        }

        public DataTable GetAllDivisionInformation()
        {
            string sql = "SELECT Division_ID AS '产品组编码',Division_Name AS '产品组名称',Description AS '描述' FROM [Division]";
           DataTable dt = DBHelper.ExecuteQueryDT(sql);
           return dt;
        }

        public bool NewDivision(string DivisionID, string DivisionName, string Description)
        {
            string sql = "INSERT INTO [Division] (Division_ID,Division_Name,Description) VALUES ('" + DivisionID + "',";
            sql += "'" + DivisionName + "','" + Description + "')";
            DBHelper.ExecuteNonQuery(sql);
            return true;
        }

        public bool DeleteDivision(string DivisionID)
        {
            string sql = "Delete FROM [Division] WHERE Division_ID='" + DivisionID + "'";
            DBHelper.ExecuteNonQuery(sql);
            return true;
        }


        public List<string> GetAllTaxCode()
        {
            string sql = "SELECT * FROM [Tax_Code]";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            if (dt == null || dt.Rows.Count == 0)
            {
                return null;
            }
            List<string> list = new List<string>();
            string obj;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                obj = dt.Rows[i][0].ToString();
                list.Add(obj);
                obj = null;
            }
            return list;

        }

        public DataTable GetAllTaxCodeInformation()
        {
            string sql = "SELECT Tax_Code ,Tax ,Description  FROM [Tax_Code]";
           DataTable dt =  DBHelper.ExecuteQueryDT(sql);
            return dt;
        }

        public bool NewTaxCode(string TaxID, double tax, string Description)
        {
            string sql = "INSERT INTO [Tax_Code] (Tax_Code,Tax,Description) VALUES ('" + TaxID + "','" + tax + "','" + Description + "')";
            DBHelper.ExecuteNonQuery(sql);
            return true;
        }

        public bool DeleteTaxCode(string TaxID)
        {
            string sql = "DELETE FROM [Tax_Code] WHERE Tax_Code='" + TaxID + "'";
            DBHelper.ExecuteNonQuery(sql);
            return true;
        }


        public List<string> GetAllCountryName()
        {
            string sql = "SELECT * FROM [Country] ";
          DataTable dt =  DBHelper.ExecuteQueryDT(sql);
          if (dt == null || dt.Rows.Count == 0)
          {
              return null;
          }
          List<string> list = new List<string>();
          string obj;
          for (int i = 0; i < dt.Rows.Count; i++)
          {
              obj = dt.Rows[i][1].ToString();
              list.Add(obj);
              obj = null;
          }
          return list;

        }

        public List<string> GetAllCountryENG()
        {
            string sql = "SELECT * FROM [Country] ";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            if (dt == null || dt.Rows.Count == 0)
            {
                return null;
            }
            List<string> list = new List<string>();
            string obj;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                obj = dt.Rows[i][2].ToString();
                list.Add(obj);
                obj = null;
            }
            return list;
        }


        public List<string> GetAllCountryCode()
        {
            string sql = "SELECT * FROM [Country] ";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            if (dt == null || dt.Rows.Count == 0)
            {
                return null;
            }
            List<string> list = new List<string>();
            string obj;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                obj = dt.Rows[i][0].ToString();
                list.Add(obj);
                obj = null;
            }
            return list;
        }





        public List<string> GetAllLoadingCriteriaID()
        {
            throw new NotImplementedException();
        }

        public string GetCriteriaDescriptionByID(string ID)
        {
            throw new NotImplementedException();
        }

        public DataTable GetAllLoadingCriteriaInformation()
        {
            throw new NotImplementedException();
        }

        public bool NewLoadingCriteria(string CriteriaID, string description)
        {
            throw new NotImplementedException();
        }

        public bool DeleteLoadingCriteria(string CriteriaID)
        {
            throw new NotImplementedException();
        }


        public List<string> GetAllCity(string countryname)
        {
            string sql = "SELECT Country_ID FROM [Country] WHERE Country_Name = '" + countryname + "'";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            string countryID = dt.Rows[0][0].ToString();
            string sql1 = "SELECT City_Name FROM [City] WHERE Country_ID = '" + countryID + "'";
            DataTable dt1 = DBHelper.ExecuteQueryDT(sql1);
            if (dt1 == null || dt1.Rows.Count == 0)
            {
               
                return null;
            }
            List<string> list = new List<string>();
            string obj;
            for (int i = 0; i < dt1.Rows.Count; i++)
            {
                obj = dt1.Rows[i][0].ToString();
                list.Add(obj);
                obj = null;
            }
            return list;
        }


        public List<string> GetAllProofType()
        {
            string sql = "SELECT * FROM [Proof_Type] ";
          DataTable dt =  DBHelper.ExecuteQueryDT(sql);
            List<string> list = new List<string>();
            string obj;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                obj = dt.Rows[i][0].ToString();
                list.Add(obj);
                obj = null;
            }
            return list;
        }

        public string  GetProofNameByType(string typeid)
        {
            string sql = "SELECT Description FROM [Proof_Type] WHERE Proof_TypeID='" + typeid + "'";
          DataTable dt =  DBHelper.ExecuteQueryDT(sql);
          string proofname = dt.Rows[0][0].ToString();
          return proofname;

        }

        public List<SupplierBase> GetAllSuppliers()
        {
            List<SupplierBase> supplierBaseList = new List<SupplierBase>();
            StringBuilder sb = new StringBuilder("select * from Supplier_Base");
            string sqlText = sb.ToString();
            DataTable dt = DBHelper.ExecuteQueryDT(sqlText);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        SupplierBase spb = new SupplierBase();
                        spb.Supplier_ID = dr["Supplier_ID"].ToString();
                        spb.Supplier_Name = dr["Supplier_Name"].ToString();
                        spb.Mobile_Phone1 = dr["Mobile_Phone1"].ToString();
                        spb.Email = dr["Email"].ToString();
                        spb.Nation = dr["Nation"].ToString();
                        spb.Address = dr["Address"].ToString();
                        supplierBaseList.Add(spb);
                    }
                }
            }
            return supplierBaseList;
        }

        /// <summary>
        /// 根据供应商编号获取供应商完整信息
        /// </summary>
        /// <param name="supplierIds"></param>
        /// <returns></returns>
        public List<SupplierBase> GetAllSuppliersByIdsLimit(List<string> supplierIds)
        {
            List<SupplierBase> supplierBaseList = new List<SupplierBase>();
            StringBuilder sb = new StringBuilder(" select * from Supplier_Base where Supplier_ID in(");
            for (int i = 0; i < supplierIds.Count; i++)
            {
                if (i < supplierIds.Count - 1)
                {
                    sb.Append("'" + supplierIds.ElementAt(i).ToString() + "',");
                }
                else
                {
                    sb.Append("'" + supplierIds.ElementAt(i).ToString() + "')");
                }
            }
            string sqlText = sb.ToString();
            DataTable dt = DBHelper.ExecuteQueryDT(sqlText);
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    SupplierBase spb = new SupplierBase();
                    spb.Supplier_ID = dr["Supplier_ID"].ToString();
                    spb.Supplier_Name = dr["Supplier_Name"].ToString();
                    spb.Mobile_Phone1 = dr["Mobile_Phone1"].ToString();
                    spb.Email = dr["Email"].ToString();
                    spb.Nation = dr["Nation"].ToString();
                    spb.Address = dr["Address"].ToString();
                    supplierBaseList.Add(spb);
                }
            }
            return supplierBaseList;
        }
        /// <summary>
        /// 查找采购组物料组中物组的id
        /// </summary>
        /// <param name="null"></param>
        /// <returns></returns>
        public List<String> GetAlPurchGroupID()
        {
            List<String> porg_ID = new List<string>();
            String sql = "SELECT Buyer_Org AS '采购组织代码' FROM[Buyer_Org]";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    porg_ID.Add(dt.Rows[i][0].ToString());
                }
            }
            return porg_ID;
        }

        /// <summary>
        /// 负责人
        /// </summary>
        /// <returns></returns>
        List<string> GeneralIDAL.getAllManagerID()
        {


            string sql = "SELECT hu.User_ID AS 员工编号, hu.Username AS 姓名 FROM Hanmote_Base_User hu,Base_Role r WHERE r.Role_ID = hu.Role_ID  and hu.Manager_Flag != 1 and r.Role_Name = '主审员'";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            if (dt == null || dt.Rows.Count == 0)
            {
                return null;
            }

            List<string> list = new List<string>();
            string obj;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                obj = dt.Rows[i][1].ToString() + "(" + dt.Rows[i][0].ToString() + ")";
                list.Add(obj);
                obj = null;
            }
            return list;
        }
        /// <summary>
        /// 查找物料组中物组的id
        /// </summary>
        /// <param name="null"></param>
        /// <returns></returns>
        public List<String> GetAllMtGroupID(String Prog_ID)
        {
            List<String> MtGroup_ID = new List<string>();

            String sql = " select Material_Group as 物料组编码  from Material_Group a, (select Description from Material_Group EXCEPT  select MtGroup_Name from Porg_MtGroup_Relationship where Porg_ID = '" + Prog_ID + "') b where a.Description = b.Description";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    MtGroup_ID.Add(dt.Rows[i][0].ToString());
                }
            }
            return MtGroup_ID;
        }

        public bool NewPorMtGroup(string Porg_ID, string Porg_Name, string MtGroup_id, string MtGroup_Name, string mt_Grop_Manager,int isVisible)
        {
            String delsql = "DELETE FROM [Porg_MtGroup_Relationship] WHERE Porg_ID='" + Porg_ID + "'AND MtGroup_ID='" + MtGroup_id + "'";
            DBHelper.ExecuteNonQuery(delsql);    //避免重复插入
            String sql = " INSERT INTO[Porg_MtGroup_Relationship] (Porg_ID, Porg_Name, MtGroup_ID,MtGroup_Name,isVisible) VALUES('" + Porg_ID + "', '" + Porg_Name + "', ";
            sql += "'" + MtGroup_id + "','" + MtGroup_Name + "',"+isVisible+")";
            DBHelper.ExecuteNonQuery(sql);
            //插入负责人信息
            delsql = @"delete from Hanmote_User_MtGroupName where Mt_Group_Name='"+ MtGroup_Name + "' and Pur_Group_Name='"+ Porg_Name + "'";
            DBHelper.ExecuteNonQuery(delsql);
            string sqlstr = "insert into Hanmote_User_MtGroupName(User_ID,Mt_Group_Name,Description,Pur_Group_Name) values('" + mt_Grop_Manager + "','" + MtGroup_Name + "','关系说明','" + Porg_Name + "')";
            DBHelper.ExecuteNonQuery(sqlstr);
            return true;

        }
        /// <summary>
        /// 获取评估员信息
        /// </summary>
        /// <param name="evalItem">评估方面</param>
        /// <returns></returns>
        public DataTable getEvalPersonInfo(string evalItem) {
            string sql = @"select hbu.User_ID as evalPersonCode ,hbu.Username as evalPersonName,hbe.Description as evalItem from HanmoteBaseUser_EvalCode hbe, Hanmote_Base_User hbu where hbe.UserID = hbu.User_ID and hbe.Description='"+evalItem+"'";
            return DBHelper.ExecuteQueryDT(sql); ;   
        }



        public DataTable GetAllPurMtGroupInfo()
        {
            string sql = "SELECT Porg_ID AS '采购组织编码',Porg_Name AS '采购组织名称',MtGroup_ID AS '物料组代码',MtGroup_Name AS '物料组名称'FROM [Porg_MtGroup_Relationship]";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            return dt;
        }

        public bool MDPorMtGroup(string delPurGroup_id, string delPurGroup_Name, string delMtGroup_id, string delMtGroup_Name,string PurGroup_id, string PurGroup_Name,string MtGroup_id, string MtGroup_Name)
        {
            //String sql = "update Porg_MtGroup_Relationship set MtGroup_ID = '"+ MtGroup_id + "',MtGroup_Name = '"+ MtGroup_Name + "'  where Porg_ID='"+PurGroup_id+ "' and Porg_Name = '"+PurGroup_Name+"'";
            //  DBHelper.ExecuteNonQuery(sql);
            //  return true;
            String delsql = "DELETE FROM [Porg_MtGroup_Relationship] WHERE Porg_ID='" + delPurGroup_id + "'AND MtGroup_ID='" + delMtGroup_id + "'";
            DBHelper.ExecuteNonQuery(delsql);    //避免重复插入
            String sql = " INSERT INTO[Porg_MtGroup_Relationship] (Porg_ID, Porg_Name, MtGroup_ID,MtGroup_Name) VALUES('" + delPurGroup_id + "', '" + delPurGroup_Name + "', ";
            sql += "'" + MtGroup_id + "','" + MtGroup_Name + "')";
            DBHelper.ExecuteNonQuery(sql);
            return true;

        }
        /// <summary>
        /// 获取采购部门名称
        /// </summary>
        /// <returns></returns>
        public DataTable GetAllBuyerOrganizationName()
        {
            List<String> porg_ID = new List<string>();
            String sql = "SELECT *  FROM[Buyer_Org]";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
           
            return dt;
        }

        public string getUnitByMateriID(string mtName)
        {
            string sql = "select Measurement from Material where Material_ID ='" + mtName + "'";
            try
            {
                string str = DBHelper.ExecuteQueryDT(sql).Rows[0][0].ToString();
                return str;
            }
            catch
            {
                return "err";
            }
        }
        /// <summary>
        /// 显示该采购组织下的所有物料评审详情
        /// </summary>
        /// <param name="purOrgId">采购部名称</param>
        /// <returns></returns>
        public Dictionary<string, Object> getMtGroupAndMasterRelation(string purOrgId)
        {
            //查询采购组织的物料组与评审之间的关系
            string sql = @"SELECT distinct
	                            sur.User_ID as 评审负责人,
	                            mtsur.Mt_Group_Name as 物料组名称,
								mt.Material_Group as 物料组代码
                            FROM
	                            Hanmote_User_MtGroupName mtsur,
	                            Hanmote_Base_User sur,
								Material_Group mt
                            WHERE
	                            mtsur.User_ID = sur.User_ID and mtsur.Type = 1 and Pur_Group_Name = '"+purOrgId+"'  and mt.Description=mtsur.Mt_Group_Name";

                       DataTable dt= DBHelper.ExecuteQueryDT(sql);

            if (dt.Rows.Count == 0) { return null; };

            DataTable temp =  DBHelper.ExecuteQueryDT("select * from TabEvalItem");
            Dictionary<string, Object> datas = new Dictionary<string, Object>();
            
            Dictionary<string, Dictionary<string, List<string>>> evalPersonss = new Dictionary<string, Dictionary<string, List<string>>>();
            List<string> evalPerson = null;

            foreach (DataRow dataRow in dt.Rows) {

                Dictionary<string, List<string>> evalPersons = new Dictionary<string, List<string>>();

                for (int i = 0; i < temp.Rows.Count; i++)
                {
                    evalPerson = new List<string>();
                    string code = temp.Rows[i]["EvalItemCode"].ToString();
                    sql = @"select be.UserID,be.Description from  HanmoteBaseUser_EvalCode be,Hanmote_User_MtGroupName hum where   be.codeID='" + code + "' and  hum.User_ID =be.UserID and hum.Mt_Group_Name='"+dataRow["物料组名称"].ToString() +"' and hum.Type=0 and hum.Pur_Group_Name='" + purOrgId + "'";
                    DataTable personData = DBHelper.ExecuteQueryDT(sql);
                    if (personData.Rows.Count == 0)
                    {
                        evalPerson.Add("暂无评审员");

                    }
                    else
                    {
                        for (int j = 0; j < personData.Rows.Count; j++)
                        {
                            evalPerson.Add(personData.Rows[j]["UserID"].ToString());
                        }
                    }

                    evalPersons.Add(temp.Rows[i]["Description"].ToString(), evalPerson);

                }

                evalPersonss.Add(dataRow["物料组名称"].ToString(), evalPersons);


            }


         
            datas.Add("dataTable", dt);
            datas.Add("dc", evalPersonss);

            Dictionary<string, string> leader = new Dictionary<string, string>();

            

            string User_Id = dt.Rows[0]["评审负责人"].ToString();
            for (int i = 0; i < 10; i++) {
                sql = "select pid from Hanmote_Base_User  where User_ID='" + User_Id + "'";
                if (DBHelper.ExecuteQueryDT(sql).Rows.Count == 0)
                {
                    break;
                }
                string pid = DBHelper.ExecuteQueryDT(sql).Rows[0][0].ToString();
                if (pid.Equals("")) {
                    break;

                }
                leader.Add(i+1+ "level", pid);
                User_Id = pid;
            }   
            datas.Add("leader", leader);

                        return datas;
        }
        /// <summary>
        /// 保存采购组织-评估员-物料组关系
        /// </summary>
        /// <param name="mtGroupName">物料组名称</param>
        /// <param name="purOrgName">采购组织名称</param>
        /// <param name="evaluterId">评估员编号</param>
        /// <returns></returns>
        public int saveEvalInfo(string mtGroupName, string purOrgName, string evaluterId) {

            string sql = @"INSERT into [dbo].[Hanmote_User_MtGroupName](User_ID,Pur_Group_Name,Mt_Group_Name,Description,Type) VALUES('"+ evaluterId + "','"+ purOrgName + "','"+ mtGroupName + "','关系说明','0')";
            return DBHelper.ExecuteNonQuery(sql); 
        }

        public DataTable getAllTabMtGroupType()
        {
            string sql = "select MaterialGroupTypeId as id , MaterialGroupType as Type from TabMtGroupType";
            return DBHelper.ExecuteQueryDT(sql); 
        }
    }
}
