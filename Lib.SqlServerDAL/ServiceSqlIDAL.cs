﻿using Lib.Common.MMCException.IDAL;
using Lib.Model.ServiceEvaluation;
using Lib.Model.ServiceModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Lib.SqlServerDAL
{
    public class ServiceSqlIDAL
    {
        //更新询价单信息
        public void UpdateInqueryPrice(InqueryPrice inqueryPrice)
        {
            string sql = "update Source_Inquiry set Purchase_Org = '" + inqueryPrice.Purchase_Org + "', Buyer_Name = '" + inqueryPrice.Buyer_Name + "', Offer_Time = '" + inqueryPrice.Offer_Time + "', State = '" + inqueryPrice.State + "', Purchase_Group = '" + inqueryPrice.Purchase_Group + "', Inquiry_Name = '" + inqueryPrice.Inquiry_Name + "', CreateTime = '" + inqueryPrice.CreateTime + "', Material_Location = '" + inqueryPrice.Material_Location + "', Approve_Count = " + inqueryPrice.Approve_Count + " where Source_ID = '" + inqueryPrice.Source_ID + "'";
            DBHelper.ExecuteNonQuery(sql);
        }

        //根据物料ID查询审核物料信息
        public DataTable FindApproveMaterialByMaterialId(string materialId)
        {
            string sql = "select * from Approve_Material";
            return DBHelper.ExecuteQueryDT(sql);
        }

        //通过物料Id查询寻源物料
        public DataTable ListSourceMaterialByMaterialId(string materialId)
        {
            string sql = "select * from Source_Material Material_ID = '" + materialId + "'";
            return DBHelper.ExecuteQueryDT(sql);
        }

        //根据审核通过的次数和物料定位查询待处理的物料审批信息
        public DataTable ListApproveMaterialByMaterialLocationAndVerify(int verify, string materialLocation,int approve)
        {
            string sql = "select * from Approve_Material where Verify = " + verify + " and Material_Location = '" + materialLocation + "' and Approve = " + approve;
            return DBHelper.ExecuteQueryDT(sql);
        }

        //根据角色Id查询角色名称
        public DataTable FindRoleNameById(string roleId)
        {
            string sql = "select Role_Name from Base_Role where Role_ID = '" + roleId + "'";
            return DBHelper.ExecuteQueryDT(sql);
        }

        //查询所有供应商
        public DataTable ListAllSupplier()
        {
            string sql = "select Supplier_ID,Supplier_Name,Supplier_AccountGroup,Mobile_Phone1,Email from Supplier_Base";
            return DBHelper.ExecuteQueryDT(sql);
        }

        //根据供应商Id查询供应商
        public DataTable FindSupplierById(string supplierId)
        {
            string sql = "select Supplier_ID,Supplier_Name,Supplier_AccountGroup,Mobile_Phone1,Email from Supplier_Base where Supplier_ID = '" + supplierId + "'";
            return DBHelper.ExecuteQueryDT(sql);
        }

        //根据采购组织Id获取该采购组织下的所有采购组
        public DataTable ListBuyGroupByOrgId(string purOrgId)
        {
            string sql = "select Buyer_Group from Buyer_Group where Buyer_Org = '" + purOrgId + "'";
            return DBHelper.ExecuteQueryDT(sql);
        }

        //根据采购组织Id查询采购组织名称
        public DataTable FindPurOrgNameById(string purOrgId)
        {
            string sql = "select Buyer_Org_Name from Buyer_Org where Buyer_Org = '" + purOrgId + "'";
            return DBHelper.ExecuteQueryDT(sql);
        }

        //根据工厂Id查询采购组织Id
        public DataTable FindPurOrgIdByFactoryId(string factoryId)
        {
            string sql = "select PurcharseOrgazationID from Factory_Purcharse_Organization where FactoryID = '" + factoryId + "'";
            return DBHelper.ExecuteQueryDT(sql);
        }

        //根据需求ID获取完整需求单数据
        public DataTable GetSummaryDemandObjById(string Demand_ID)
        {
            string sql = "select * from Summary_Demand where Demand_ID = '" + Demand_ID + "'";
            return DBHelper.ExecuteQueryDT(sql);
        }

        //根据ID获取需求单
        public DataTable GetSummaryDemandById(string demandId)
        {
            string sql = "select Demand_ID,Purchase_Type,Proposer_ID,State,PhoneNum,Department,ReviewAdvice,Demand_Count,Material_Name,Supplier_ID,Factory_ID from Summary_Demand where Demand_ID = '" + demandId + "'";
            return DBHelper.ExecuteQueryDT(sql);
        }

        //获取所有需求计划
        public DataTable ListSummaryDemandByState(string state)
        {
            string sql = "select Demand_ID,Purchase_Type,Proposer_ID,State,PhoneNum,Department,ReviewAdvice,Demand_Count,Material_Name,Supplier_ID,Factory_ID from Summary_Demand";
            if (!"显示所有".Equals(state))
            {
                sql += " where state = '" + state + "'";
            }
            return DBHelper.ExecuteQueryDT(sql);
        }
        //查询所有需求状态
        public DataTable ListAllState()
        {
            string sql = "select distinct state from Summary_Demand";
            return DBHelper.ExecuteQueryDT(sql);
        }

        //查询询价单下的所有物料
        public DataTable ListMaterialByInqueryPriceId(string inqueryPriceId)
        {
            string sql = "select * from Source_Material where Source_ID = '" + inqueryPriceId + "'";
            return DBHelper.ExecuteQueryDT(sql);
        }

        //删除指定的报价单记录
        public void DeleteOfferPrice(OfferPrice offerPrice)
        {
            string sql = "delete from Source_Supplier_Material where Source_ID = @Source_ID and Supplier_ID = @Supplier_ID and Material_ID = @Material_ID";
            SqlParameter[] sqlParameters = new SqlParameter[]
                    {
                    new SqlParameter("@Source_ID",offerPrice.Source_ID),
                    new SqlParameter("@Supplier_ID",offerPrice.Supplier_ID),
                    new SqlParameter("@Material_ID",offerPrice.Material_ID)
                    };

            DBHelper.ExecuteNonQuery(sql, sqlParameters);
        }

        //根据物料编码模糊查询所有报价单
        public DataTable ListOfferPriceLikeMaterialId(string materialId)
        {
            string sql = "select * from Source_Supplier_Material where Material_ID like '%" + materialId + "%'";
            return DBHelper.ExecuteQueryDT(sql);
        }

        //获取所有报价信息
        public DataTable ListAllOfferPrice()
        {
            string sql = "select * from Source_Supplier_Material";
            return DBHelper.ExecuteQueryDT(sql);
        }

        //根据询价单号和物料ID查询寻源物料
        public DataTable FindSourceMaterialByMaterialIdAndInqueryId(string Material_ID,string InqueryId)
        {
            string sql = "select * from Source_Material where Material_ID = '" + Material_ID + "' and Source_ID = '" + InqueryId + "'";
            return DBHelper.ExecuteQueryDT(sql);
        }

        //查询某一询价单号下的所有物料
        public DataTable FindSourceMaterialByInqueryId(string Inquery_ID)
        {
            string sql = "select * from Source_Material where Source_ID = '" + Inquery_ID + "'";
            return DBHelper.ExecuteQueryDT(sql);
        }

        //根据询价单号查询询价信息
        public DataTable FindInqueryPriceById(string Inquery_ID)
        {
            string sql = "select * from Source_Inquiry where Source_ID = '" + Inquery_ID + "'";
            return DBHelper.ExecuteQueryDT(sql);
        }

        //根据询价单名称模糊查询询价单
        public DataTable FindInqueryPriceLikeName(string Inquiry_Name)
        {
            string sql = "select * from Source_Inquiry where Inquiry_Name like '%" + Inquiry_Name + "%'";
            return DBHelper.ExecuteQueryDT(sql);
        }

        //插入寻源物料数据
        public void InsertSourceMaterial(SourceMaterial sourceMaterial)
        {
            try
            {
                string sql = "insert into Source_Material(Material_ID,Material_Name,Demand_Count,Demand_ID,Unit,Factory_ID,Stock_ID,Source_ID) values(@Material_ID,@Material_Name,@Demand_Count,@Demand_ID,@Unit,@Factory_ID,@Stock_ID,@Source_ID)";
                SqlParameter[] sqlParameters = new SqlParameter[]
                    {
                    new SqlParameter("@Material_ID",sourceMaterial.Material_ID),
                    new SqlParameter("@Material_Name",sourceMaterial.Material_Name),
                    new SqlParameter("@Demand_Count",sourceMaterial.Demand_Count),
                    new SqlParameter("@Demand_ID",sourceMaterial.Demand_ID),
                    new SqlParameter("@Unit",sourceMaterial.Unit),
                    new SqlParameter("@Factory_ID",sourceMaterial.Factory_ID),
                    new SqlParameter("@Stock_ID",sourceMaterial.Stock_ID),
                    new SqlParameter("@Source_ID",sourceMaterial.Source_ID)
                    };
                DBHelper.ExecuteNonQuery(sql, sqlParameters);
            }
            catch (Exception e)
            {
                throw new DBException("插入寻源物料时数据库执行异常！",e);
            }
        }
        //获取供应商编号
        public DataTable getSupplierId(String loginId) {

            string sql = @"select distinct S1.Supplier_ID as 供应商编号,s1.Supplier_Name as 供应商名称 from Hanmote_User_MtGroupName m, Supplier_Base S1,SR_Info S2  where  S1.Supplier_ID=S2.SupplierId and S2.SelectedDep=m.Pur_Group_Name and s2.SelectedMType =m.Mt_Group_Name ";
            SqlParameter[] sqlParameter = new SqlParameter[] {
                new SqlParameter("@id",loginId)
            };
            return DBHelper.ExecuteQueryDT(sql, sqlParameter);
        }

        //根据询价单的状态，审核次数，物料定位获取询价单
        public DataTable ListInqueryPriceByState(string state , int approveCount , string materialLocation)
        {
            try
            {
                if (state == null)
                {
                    return null;
                }
                string sql = null;
                if(approveCount == -1 && materialLocation == null)
                {
                    sql = "select Source_ID,Purchase_Org,Buyer_Name,Offer_Time,State,Purchase_Group,Inquiry_Name,CreateTime from Source_Inquiry where State = '" + state + "'";
                }
                else
                {
                    sql = "select Source_ID,Purchase_Org,Buyer_Name,Offer_Time,State,Purchase_Group,Inquiry_Name,CreateTime,Material_Location from Source_Inquiry where 1 =1 ";
                    if (approveCount != -1)
                    {
                        sql += " and Approve_Count = " + approveCount ;
                    }
                    if(materialLocation != null)
                    {
                        char[] charArr = materialLocation.ToCharArray();
                        if (charArr[0].Equals('!'))
                        {
                            materialLocation = materialLocation.Substring(1);
                            sql += " and Material_Location != '" + materialLocation + "'";
                        }
                        else
                        {
                            sql += " and Material_Location = '" + materialLocation + "'";
                        }
                        
                    }
                }
                return DBHelper.ExecuteQueryDT(sql);
            }
            catch (Exception e)
            {
                throw new DBException("根据询价单状态查询询价单时数据库执行异常！",e);
            }
        }

        //获取所有询价单信息
        public DataTable ListAllInqueryPriceInfo()
        {
            try
            {
                string sql = "select * from Source_Inquiry";
                return DBHelper.ExecuteQueryDT(sql);
            }
            catch (Exception e)
            {
                throw new DBException("获取所有询价单信息时数据库执行异常！", e);
            }
        }

        //插入询价信息
        public int InsertInqueryPrice(InqueryPrice inqueryPrice)
        {
            try
            {
                string sql = "insert into Source_Inquiry values(@Source_ID,@Purchase_Org,@Buyer_Name,@Offer_Time,@State,@Purchase_Group,@Inquiry_Name,@CreateTime,@Material_Location,@Approve_Count)";
                SqlParameter[] sqlParameters = new SqlParameter[]
                {
                    new SqlParameter("@Source_ID",inqueryPrice.Source_ID),
                    new SqlParameter("@Purchase_Org",inqueryPrice.Purchase_Org),
                    new SqlParameter("@Buyer_Name",inqueryPrice.Buyer_Name),
                    new SqlParameter("@Offer_Time",inqueryPrice.Offer_Time),
                    new SqlParameter("@State",inqueryPrice.State),
                    new SqlParameter("@Purchase_Group",inqueryPrice.Purchase_Group),
                    new SqlParameter("@Inquiry_Name",inqueryPrice.Inquiry_Name),
                    new SqlParameter("@CreateTime",inqueryPrice.CreateTime),
                    new SqlParameter("@Material_Location",inqueryPrice.Material_Location),
                    new SqlParameter("@Approve_Count",inqueryPrice.Approve_Count),
                };
                return DBHelper.ExecuteNonQuery(sql,sqlParameters);
            }
            catch (Exception e)
            {
                throw new DBException("插入询价信息时数据库执行异常！", e);
            }
        }

        //根据供应商id查询供应商
        public DataTable GetSupplierBySupplierId(string gysbh)
        {
            try
            {
                string sql = "select Supplier_ID,Supplier_Name,Mobile_Phone1,Email,Nation,Address,Language,Trade_Partner from Supplier_Base where Supplier_ID = '" + gysbh + "'";
                return DBHelper.ExecuteQueryDT(sql);
            }
            catch (Exception ex)
            {
                throw new DBException("根据供应商编号查询供应商信息时数据库执行异常！",ex);
            }
        }

        //根据物料Id查询物料信息
        public DataTable GetMaterialByMaterialId(string MaterialId)
        {
            try
            {
                string sql = "select Material_ID,Material_Name,Material_Group,Measurement,Material_Type,Type_Name from Material where Material_ID = '" + MaterialId + "'";
                return DBHelper.ExecuteQueryDT(sql);
            }
            catch (Exception ex)
            {
                throw new DBException("根据物料Id查询物料信息时数据库执行异常！",ex);
            }
        }

        //查询所有物料信息
        public DataTable GetAllMaterialInfo()
        {
            try
            {
                string sql = "select Material_ID,Material_Name,Material_Type,Material_Group,Measurement from Material";
                return DBHelper.ExecuteQueryDT(sql);
            }
            catch (Exception ex)
            {
                throw new DBException("查询所有供应商信息时数据库执行异常！",ex);
            }
        }

        //根据条件获取市场价格记录信息
        public DataTable GetMarketPrice(string PurGroupName, string MtGroupName, string BusType)
        {
            //作为标识
            int flag = 0;
            string sql = "select * from New_MarketPrice where 1 = 1";
            if(PurGroupName != null && PurGroupName.Length > 0)
            {
                flag++;
                sql += "and PurGroupName = '" + PurGroupName + "'";
            }
            if(MtGroupName != null && MtGroupName.Length > 0)
            {
                flag++;
                sql += "and MtGroupName = '" + MtGroupName + "'";
            }
            if(BusType != null && BusType.Length > 0)
            {
                flag++;
                sql += "and BusType = '" + BusType + "'";
            }
            if(flag == 0)
            {
                return null;
            }
            return DBHelper.ExecuteQueryDT(sql);
        }

        public DataTable getExServiceData()
        {
            string sql = @"select MtGroupName as 物料组名称,mtName as 物料名称,  supplierName as 供应商名称,TimeRate as 及时性权重,QualifyRate as 质量权重, exServiceId as 服务工单,supplierId as 供应商代码, serviceTime as 时间,serviceTotalScore as 综合,serviceQualifyScore as 质量评分,serviceTimeScore as 及时性评分,danWei as 单位 from Hanmote_ExtService";
          
            return DBHelper.ExecuteQueryDT(sql);
        }
        /// <summary>
        /// 获取一般服务的供应商
        /// </summary>
        /// <returns></returns>
        public List<string> getGenSupplierName()
        {
            List<string> list = new List<string>();
            string sql = @"select distinct supplierName from Hanmote_GenService";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            list.Add("");
            foreach (DataRow row in dt.Rows)
            {
                list.Add(row[0].ToString());
            }

            return list;
        }
        /// <summary>
        /// 获取一般服务所有物料组
        /// </summary>
        /// <param name="supplierName"></param>
        /// <returns></returns>
        public List<string> getGenMateGroupName(string supplierName)
        {
            List<string> list = new List<string>();
            string sql = @"select distinct MtGroupName from Hanmote_GenService where supplierName='" + supplierName + "'";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            list.Add("");
            foreach (DataRow row in dt.Rows)
            {
                list.Add(row[0].ToString());
            }

            return list;
        }

        /// <summary>
        /// 一般服务物料
        /// </summary>
        /// <param name="supplierName"></param>
        /// <param name="mateGroupName"></param>
        /// <returns></returns>
        public List<string> getGenMateName(string supplierName, string mateGroupName)
        {
            List<string> list = new List<string>();
            string sql = @"select distinct mtName from Hanmote_GenService where supplierName='" + supplierName + "' and MtGroupName='" + mateGroupName + "'";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            list.Add("");
            foreach (DataRow row in dt.Rows)
            {
                list.Add(row[0].ToString());
            }

            return list;
        }





        public List<string> getMateName(string supplierName, string mateGroupName)
        {
            List<string> list = new List<string>();
            string sql = @"select distinct mtName from Hanmote_ExtService where supplierName='"+ supplierName + "' and MtGroupName='"+ mateGroupName + "'";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            list.Add("");
            foreach (DataRow row in dt.Rows) {
                list.Add(row[0].ToString());
            }

            return list;
        }

        public DataTable getGenServiceData(string supplierName, string mtGroupName, string mtName, string startTime, string endTime)
        {
            string sql = @"select * from Hanmote_GenService ";

            if (!startTime.Equals(""))
                sql += " where ServiceTime >='" + startTime + "'";

            if (!endTime.Equals(""))
                sql += " and ServiceTime< ='" + endTime + "'";

            if (!supplierName.Equals(""))
            {
                sql += " and supplierName='" + supplierName + "'";
            }
            if (!mtGroupName.Equals(""))
                sql += " and MtGroupName='" + mtGroupName + "'";
            if (!mtName.Equals(""))
                sql += " and mtName='" + mtName + "'";


            return DBHelper.ExecuteQueryDT(sql);
        }

        public DataTable getExServiceData(string supplierName, string mtGroupName, string mtName,string startTime,string endTime)
        { 
            string sql = @"select MtGroupName as 物料组名称,mtName as 物料名称,  supplierName as 供应商名称,TimeRate as 及时性权重,QualifyRate as 质量权重, exServiceId as 服务工单,supplierId as 供应商代码, serviceTime as 时间,serviceTotalScore as 综合,serviceQualifyScore as 质量评分,serviceTimeScore as 及时性评分,danWei as 单位 from Hanmote_ExtService ";

            if (!startTime.Equals(""))
                sql += " where serviceTime >='" + startTime + "'";

            if (!endTime.Equals(""))
                sql += " and serviceTime< ='" + endTime + "'";

            if (!supplierName.Equals("")) {
                sql += " and supplierName='" + supplierName + "'";
            }
            if (!mtGroupName.Equals(""))
                sql += " and MtGroupName='"+ mtGroupName + "'";
            if (!mtName.Equals(""))
                sql += " and mtName='" + mtName + "'";
          

            return DBHelper.ExecuteQueryDT(sql);
        }
     
        public DataTable getSupplierIdByPurId( string text)
        {
            string sqlText = @"SELECT Supplier_ID,Supplier_Name FROM [dbo].[Supplier_Purchasing_Org] where PurchasingORG_ID='"+text+"';";
            return DBHelper.ExecuteQueryDT(sqlText);
        }

        /// <summary>
        /// 获取服务汇总物料组
        /// </summary>
        /// <param name="supplierName"></param>
        /// <returns></returns>
        public List<string> getMateGroupName(string supplierName)
        {
            List<string> list = new List<string>();
            string sql = @"select distinct MtGroupName from Hanmote_ExtService where supplierName='"+ supplierName + "'";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            list.Add("");
            foreach (DataRow row in dt.Rows)
            {
                list.Add(row[0].ToString());
            }

            return list;
        }

        public List<string> getSupplierName()
        {
            List<string> list = new List<string>();
            string sql = @"select distinct supplierName from Hanmote_ExtService";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            list.Add("");
            foreach (DataRow row in dt.Rows)
            {
                list.Add(row[0].ToString());
            }

            return list;
        }

        public DataTable getExServiceRate(string mtGroupId)
        {
            string sql = @"select serviceQuality as 服务质量,serviceTimeRate as 服务及时性 from Hanmote_MtGroup_serviceRate where mtGroupId='" + mtGroupId + "'";
            return DBHelper.ExecuteQueryDT(sql);
        }

        public DataTable getGeneServiceRate(string mtGroupId)
        {
            string sql = @"select serviceReliable as 可靠性,serviceInnovate as 创新性,userService as 用户服务 from   Hanmote_MtGroup_serviceRate where  mtGroupId='"+ mtGroupId + "'";
            return DBHelper.ExecuteQueryDT(sql);
        }

        public DataTable getServiceRate(string mTGName)
        {
            string sql = @"select * from Hanmote_MtGroup_serviceRate where mtGroupName like '%" + mTGName + "%'";
            return DBHelper.ExecuteQueryDT(sql);
        }

        public DataTable getServiceRate()
        {
            string sql = @"select   a.Material_Group as mtGroupId,a.Description as mtGroupName ,b.serviceQuality,b.serviceTimeRate,b.serviceInnovate,b.serviceReliable,b.userService   from  Material_Group a   left   join  Hanmote_MtGroup_serviceRate b     on   a.Material_Group=b.mtGroupId";
            return DBHelper.ExecuteQueryDT(sql);
        }

        public DataTable getAllMtGroupidAndName()
        {
            string sql = @"select Material_Group as mtGroupId,Description as mtGroupName from Material_Group";

            return DBHelper.ExecuteQueryDT(sql);
        }

        //自定义服务
        public bool insertServiceRate(DataGridView mtGroupRate)
        {
            int flag = 0;
       
        
            foreach (DataGridViewRow row in mtGroupRate.Rows) {

                string sql = @"DECLARE @isexist INT;
                    select @isexist=count(*) from Hanmote_MtGroup_serviceRate  where mtGroupId='"+ row.Cells["mtGroupId"].Value.ToString()+ "'" +
                    " if(@isexist>0)" +
                    " update Hanmote_MtGroup_serviceRate set serviceQuality='"+ row.Cells["serviceQuality"].Value.ToString() + "',serviceTimeRate='" + row.Cells["serviceTimeRate"].Value.ToString() + "',serviceInnovate='" + row.Cells["serviceInnovate"].Value.ToString() + "',serviceReliable='" + row.Cells["serviceReliable"].Value.ToString() + "',userService='" + row.Cells["userService"].Value.ToString() + "' where mtGroupId='" + row.Cells["mtGroupId"].Value.ToString() 
                         + "'else " +
                    "insert into Hanmote_MtGroup_serviceRate(mtGroupId,mtGroupName,serviceQuality,serviceTimeRate,serviceInnovate,serviceReliable,userService) values('" + row.Cells["mtGroupId"].Value.ToString() + "','" + row.Cells["mtGroupName"].Value.ToString() + "','" + row.Cells["serviceQuality"].Value.ToString() + "','" + row.Cells["serviceTimeRate"].Value.ToString() + "','" + row.Cells["serviceInnovate"].Value.ToString() + "','" + row.Cells["serviceReliable"].Value.ToString() + "','" + row.Cells["userService"].Value.ToString() + "')";
                 flag+=DBHelper.ExecuteNonQuery(sql);

            }
            if (flag == mtGroupRate.Rows.Count)
            {

                return true;
            }
            else {
                return false;
            }
            
            
        }



        public DataTable getGenServiceData()
        {
            string sql = @"select * from Hanmote_GenService";

            return DBHelper.ExecuteQueryDT(sql);
        }

        public bool insertGenServiceModel(string innos, string res, string uSs, object modelName)
        {
            Boolean flag = false;
            string sql = @"select * from Hanmote_genServicemodel where ServiceModelName='" + modelName + "'";
            if (DBHelper.ExecuteQueryDT(sql).Rows.Count > 0)
            {

                sql = @"update Hanmote_genServicemodel set Innos=@innos,Res=@res,USs=@uSs where ServiceModelName=@modelName";
            }
            else
            {
                sql = @"insert into Hanmote_genServicemodel(ServiceModelName,Innos,Res,USs) values(@modelName,@innos,@res,@uSs)";

            }

            SqlParameter[] sqlParameter = new SqlParameter[] {
                new SqlParameter("@innos",innos),
                new SqlParameter("@res",res),
                new SqlParameter("@uSs",uSs),
                new SqlParameter("@modelName",modelName),

            };

            if (DBHelper.ExecuteNonQuery(sql, sqlParameter) > 0)
            {
                flag = true;
            }
            return flag;
        }

        //获取物料信息
        public DataTable getMtidAndName(string mtGroupid,string SupplierId)
        {
            string sql = "SELECT mtid as 物料编号,mtName as 物料名称 FROM [dbo].[TabSupplyInfo] where SupplierId='"+ SupplierId + "' and mtGroupId='"+mtGroupid+"'";
            return DBHelper.ExecuteQueryDT(sql);
        }

        public DataTable getGenServiceModel(string name)
        {
            string sql = "select * from Hanmote_genServicemodel where ServiceModelName='" + name + "'";
            return DBHelper.ExecuteQueryDT(sql);
        }

        public List<string> getGenServiceModelName()
        {
            string sql = @"select ServiceModelName from Hanmote_genServicemodel";
            List<String> modelName = new List<string>();
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            foreach (DataRow row in dt.Rows)
            {
                modelName.Add(row[0].ToString());
            }
            return modelName;
        }
        /// <summary>
        /// 获取物料组的信息
        /// </summary>
        /// <param name="supplierId"></param>
        /// <returns></returns>
        public DataTable getMtGroupidAndName(string supplierId)
        {
            string sql = "select distinct mtGroupId,mtGroupName from TabSupplyInfo where SupplierId='" + supplierId + "'";
            DataTable dt= DBHelper.ExecuteQueryDT(sql);
            return dt;
        }

        public Boolean insertServiceModel(string sQs, string sTs, string modelName)
        {
            Boolean flag = false;
            string sql = @"select *from Hanmote_exServiceModel where modelName='"+ modelName + "'";
            if (DBHelper.ExecuteQueryDT(sql).Rows.Count > 0)
            {

                sql = @"update Hanmote_exServiceModel set sQs=@sQs,sTs=@sTs where modelName=@modelName";
            }
            else {
                sql = @"insert into Hanmote_exServiceModel values(@modelName,@sQs,@sTs)";

            }

            SqlParameter[] sqlParameter = new SqlParameter[] {
                new SqlParameter("@sQs",sQs),
                new SqlParameter("@sTs",sTs),
                new SqlParameter("@modelName",modelName),

            };

            if (DBHelper.ExecuteNonQuery(sql, sqlParameter) > 0)
            {
                flag = true;
            }
            return flag;
        }

        public DataTable getServiceModel(string name)
        {

           string sql = "select * from Hanmote_exServiceModel where modelName='" + name+"'";
           return DBHelper.ExecuteQueryDT(sql);
        }

        /// <summary>
        /// 获取服务模板名称
        /// </summary>
        /// <returns></returns>
        public List<string> getServiceModelName()
        {
            string sql = @"select modelName from Hanmote_exServiceModel";
            List<String> modelName = new List<string>();
            DataTable dt =DBHelper.ExecuteQueryDT(sql);
            foreach (DataRow row in dt.Rows) {
                modelName.Add(row[0].ToString());
            }
            return modelName;
        }


        /// <summary>
        /// 保存服务数据
        /// </summary>
        /// <param name="serviceModel">服务数据对象</param>
        /// <returns></returns>
        public Boolean insertServiceData(ServiceModel serviceModel) {

            string sql = @"insert into  Hanmote_ExtService(mtName,MtGroupName,TimeRate,QualifyRate,supplierName,supplierId,mtId,exServiceId,servicePlace,creator,serviceTime,serviceTimeScore,serviceTimeReason,serviceQualifyScore,serviceQualifyReson,serviceTotalScore)
                                                values(@mtName,@mtGroupName,@STRate,@SQRate,@supplierName,@supplierId,@mtId,@exServiceId,@servicePlace,@creator,getDate(),@serviceTimeScore,@serviceTimeReason,@serviceQualifyScore,@serviceQualifyReson,@serviceTotalScore)";
            Boolean flag = false;
            SqlParameter[] sqlParameter = new SqlParameter[] {
                new SqlParameter("@supplierId",serviceModel.SupplierId),
                new SqlParameter("@mtId",serviceModel.MtID),
                new SqlParameter("@mtName",serviceModel.MtName),
                new SqlParameter("@exServiceId",serviceModel.ExtServiceId),
                new SqlParameter("@servicePlace",serviceModel.ServicePlace),
                new SqlParameter("@creator",serviceModel.Creator),
                new SqlParameter("@serviceTimeScore",serviceModel.ServiceTimeScore),
                new SqlParameter("@serviceTimeReason",serviceModel.ServiceTimeReason),
                new SqlParameter("@serviceQualifyScore",serviceModel.ServiceQualityScroce),
                new SqlParameter("@serviceQualifyReson",serviceModel.ServiceQualityReason),
                new SqlParameter("@serviceTotalScore",serviceModel.ServiceTotalScore),
                new SqlParameter("@supplierName",serviceModel.SupplierName),
                new SqlParameter("@STRate",serviceModel.STRate1),
                new SqlParameter("@SQRate",serviceModel.SQRate1),
                new SqlParameter("@mtGroupName",serviceModel.MtGroupName)
            };
            if (DBHelper.ExecuteNonQuery(sql, sqlParameter) > 0) {
                flag = true;
            }
            return flag;
        }

        public bool insertGenServiceData(GeneralServiceModle serviceData)
        {
            string sql = @"insert into  Hanmote_GenService(mtName,MtGroupName,InRate,ReRate,UserRate,supplierName,supplierId,mtId,GenServiceId,FactoryId,creator,ServiceTime,InnovationSco,InReason,RealizableSco,ReReson,UserServiceSco,UserSerReson,serviceTotalScore)
                                                values(@MtName,@MtGroupName,@InnoRate,@ReRate,@UserRate,@supplierName,@supplierId,@mtId,@GenServiceId,@FactoryId,@creator,@ServiceTime,@InnovationSco,@InReason,@RealizableSco,@ReReson,@UserServiceSco,@UserSerReson,@serviceTotalScore)";
            Boolean flag = false;
            SqlParameter[] sqlParameter = new SqlParameter[] {
                new SqlParameter("@supplierId",serviceData.SupplierId),
                new SqlParameter("@mtId",serviceData.MtID),
                new SqlParameter("@MtName",serviceData.MtName),
                new SqlParameter("@GenServiceId",serviceData.GenServiceId),
                new SqlParameter("@FactoryId",serviceData.FactoryId),
                new SqlParameter("@creator",serviceData.Creator),
                new SqlParameter("@ServiceTime",serviceData.ServiceTime),
                new SqlParameter("@InnovationSco",serviceData.InnovationSco),
                new SqlParameter("@InReason",serviceData.InReason1),
                new SqlParameter("@RealizableSco",serviceData.RealizableSco),
                new SqlParameter("@ReReson",serviceData.ReReson),
                new SqlParameter("@UserServiceSco",serviceData.UserServiceSco),
                new SqlParameter("@UserSerReson",serviceData.UserSerReson),
                new SqlParameter("@serviceTotalScore",serviceData.ServiceTotalScore),
                new SqlParameter("@supplierName",serviceData.SupplierName),
                new SqlParameter("@InnoRate",serviceData.InnoRate1),
                new SqlParameter("@ReRate",serviceData.ReRate1),
                new SqlParameter("@UserRate",serviceData.UserRate1),
                new SqlParameter("@MtGroupName",serviceData.MtGroupName),

            };
            if (DBHelper.ExecuteNonQuery(sql, sqlParameter) > 0)
            {
                flag = true;
            }
            return flag;
        }
    }
}
