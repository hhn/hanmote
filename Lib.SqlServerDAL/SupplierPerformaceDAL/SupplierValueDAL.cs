﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using Lib.SqlServerDAL;
using Lib.IDAL.SupplierPerformaceIDAL;
using Lib.Model.SupplierManagementModel;

namespace Lib.SqlServerDAL.SupplierPerformaceDAL
{
    class SupplierValueDAL: SupplierValueIDAL
    {
        #region 业务价值

        /// <summary>
        /// 查询全年采购业务量
        /// </summary>
        /// <param name="supplierID"></param>
        /// <param name="year"></param>
        /// <returns></returns>
        public DataTable queryPurchasingTurnover(String supplierID, String year)
        {
            //SQL语句查询起始时间
            DateTime yearStart = Convert.ToDateTime(year + "-" + "1-1");
            DateTime yearEnd = Convert.ToDateTime(year + "-" + "12-31" + " 23:59:59");
            String yearStartStr = yearStart.ToString("yyyyMMdd");
            String yearEndStr = yearEnd.ToString("yyyyMMdd");
            
            StringBuilder sqlText = new StringBuilder();
            sqlText.Append("SELECT SUM(Number * Price) FROM")
                .Append("(SELECT ISNUMERIC(DemandCount) AS isNumber, ISNUMERIC(NetPrice) AS isPrice, DemandCount AS ")
                .Append("Number, NetPrice AS Price FROM RecordInfo")
                .Append(" WHERE SupplierId = '"+supplierID+"'")
                .Append(" AND CreateTime BETWEEN '"+ yearStartStr + "' AND '"+ yearEndStr + "'")
                .Append(" ) AS tmpTable")
                .Append(" WHERE isNumber = 1 AND isPrice = 1");

            DataTable rst = DBHelper.ExecuteQueryDT(sqlText.ToString());
            return rst;
        }

        /// <summary>
        /// 查询供应商全年营业额
        /// </summary>
        /// <param name="supplierID"></param>
        /// <param name="year"></param>
        /// <returns></returns>
        public DataTable querySupplierTurnover(String supplierID, String year)
        {
            //String sqlText = "SELECT SUM(Turnover) FROM Supplier_Turnover WHERE Supplier_ID = '"+supplierID+"' AND [Year] = '"+year+"'";
            String sqlText = "SELECT SUM(Annual_Sales) FROM Supplier_Base WHERE Supplier_ID = '" + supplierID + "'";

            DataTable rst = DBHelper.ExecuteQueryDT(sqlText);
            return rst;
        }

        /// <summary>
        /// 保存业务价值，写入数据表Supplier_Business_Value中
        /// </summary>
        /// <param name="savePara"></param>
        /// <returns></returns>
        public int saveBusinessValue(List<Object> savePara)
        {
            DateTime time = Convert.ToDateTime(savePara[7]);
            String timeStr = time.ToString("yyyyMMdd");
            StringBuilder sqlText = new StringBuilder();
            sqlText.Append("INSERT INTO Supplier_Position")
                .Append(" (Supplier_ID,Company_Portion,Appeal_Level,Business_Value,Relationship_Segment,Supplier_Position,[Year],Record_Time,Record_Name)")
                .Append(" VALUES('" + Convert.ToString(savePara[0]) + "','" + Convert.ToString(savePara[1]) + "'")
                .Append(" ,'" + Convert.ToString(savePara[2]) + "','" + Convert.ToString(savePara[3]) + "'")
                .Append(" ,'" + Convert.ToString(savePara[4]) + "','" + Convert.ToString(savePara[5]) + "','" + Convert.ToString(savePara[6]) + "'")
                .Append(",'" +timeStr)
                .Append("','" + Convert.ToString(savePara[8]) + "')");
            int lines = DBHelper.ExecuteNonQuery(sqlText.ToString());
            return lines;
        }

        #endregion

        #region 品项定位结果查询

        /// <summary>
        /// 查询供应商提供过的所有物料ID
        /// </summary>
        /// <param name="supplierID"></param>
        /// <returns></returns>
        public DataTable querySuppliedMaterial(String supplierID)
        {
            String sqlText = "SELECT DISTINCT(MaterialId) from RecordInfo WHERE SupplierId = '"+supplierID+"'";
            DataTable rstTable = DBHelper.ExecuteQueryDT(sqlText);
            return rstTable;
        }

        /// <summary>
        /// 查询物料ID对应的物料组ID
        /// </summary>
        /// <param name="materialID"></param>
        /// <returns></returns>
        public String queryMaterialGroup(String materialID)
        {
            String sqlText = "SELECT Material_Group FROM Material WHERE Material_ID = '"+materialID+"'";
            DataTable rstTable = DBHelper.ExecuteQueryDT(sqlText);
            String rst = "101";
            foreach (DataRow row in rstTable.Rows)
            {
                rst = row[0].ToString();
            }
            return rst;
        }

        /// <summary>
        /// 查询品项定位结果
        /// </summary>
        /// <param name="supplierID"></param>
        /// <returns></returns>
        public String querySupplyType(String supplierID)
        {
            DataTable materialTable = this.querySuppliedMaterial(supplierID);
            if (materialTable.Rows.Count <= 0)
            {
                return "101";
            }

            String rst = "101";
            String PIP = "101";
            foreach (DataRow row in materialTable.Rows)
            {
                if(String.IsNullOrEmpty(row.ToString()))
                {
                    continue;
                }
                String materialID = row[0].ToString();
                String materialGroup = this.queryMaterialGroup(materialID);
                //查询不到对应的物料组信息，则进行下一轮循环
                if (String.IsNullOrEmpty(materialGroup))
                {
                    continue;
                }
                else if (materialGroup == "101")
                {
                    continue;
                }

                String sqlText = "SELECT PIP_Level FROM Admission_Base WHERE Material_Group = '"+materialGroup+"'";
                DataTable tmp = DBHelper.ExecuteQueryDT(sqlText);
                String tmpStr="101";
                if (tmp.Rows.Count <= 0)
                {
                    continue;
                }
                else
                {
                    foreach (DataRow row1 in tmp.Rows)
                    {
                        tmpStr = row1[0].ToString();
                    }
                }

                //判断是否改变当前PIP值
                if (PIP == "H")
                {
                    continue;
                }
                else if (PIP == "M")
                {
                    if (tmpStr == "H")
                    {
                        PIP = tmpStr;
                    }
                    continue;
                }
                else if (PIP == "L")
                {
                    if ((tmpStr == "H")||(tmpStr == "M"))
                    {
                        PIP = tmpStr;
                    }
                    continue;
                }
                else
                {
                    PIP = tmpStr;
                    continue;
                }
            }
            return PIP;
        }
        
        #endregion

        #region 供应商区分（供应商定位最终结果）

        /// <summary>
        /// 保存供应商定位结果 写入数据表Supplier_Position
        /// </summary>
        /// <param name="savePara"></param>
        /// <returns></returns>
        public int saveSupplierPosition(List<Object> savePara)
        {
            StringBuilder sqlText = new StringBuilder();
            sqlText.Append(" INSERT INTO Supplier_Position (Supplier_ID,Company_Portion,Appeal_Level,Business_Value,Relationship_Segment,Supplier_Position")
                .Append(" ,[Year],Record_Time,Record_Name)")
                .Append("VALUES('" + Convert.ToString(savePara[0]) + "','" + Convert.ToString(savePara[1]) + "','" + Convert.ToString(savePara[2]) + "'")
                .Append(",'" + Convert.ToString(savePara[3]) + "','" + Convert.ToString(savePara[4]) + "','" + Convert.ToString(savePara[5]) + "'")
                .Append(",'" + Convert.ToString(savePara[6]) + "','" + Convert.ToString(savePara[7]) + "','" + Convert.ToString(savePara[8]) + "')");
            int lines = DBHelper.ExecuteNonQuery(sqlText.ToString());
            return lines;
        }

        #endregion

        #region 供应商跟踪
        /// <summary>
        /// 查询当前年份所有供应商状态
        /// </summary>
        /// <param name="year"></param>
        /// <returns></returns>
        public DataTable querySupplierState(String year,String industry)
        {
            StringBuilder sqlText = new StringBuilder();
            sqlText.Append("SELECT table1.Supplier_ID,Supplier_Base.Supplier_Name,table1.Supplier_Position,Supplier_Base.Industry_Category FROM")
                .Append(" (SELECT *, Row_Number() OVER (partition by Supplier_ID ORDER BY Record_Time desc) rank FROM Supplier_Position  WHERE [Year] = '" + year + "'")
                .Append(" ) AS table1,Supplier_Base")
                .Append(" WHERE table1.rank = 1 AND table1.Supplier_ID = Supplier_Base.Supplier_ID AND Supplier_Base.Industry_Category = '"+industry+"'");
            
            return (DBHelper.ExecuteQueryDT(sqlText.ToString()));
        }

        /// <summary>
        /// 修改供应商状态，数据表Supplier_Base
        /// </summary>
        /// <param name="supplierID"></param>
        /// <param name="state"></param>
        /// <returns></returns>
        public int changeSupplierState(String supplierID, String state)
        {
            StringBuilder sqlText = new StringBuilder();
            sqlText.Append("UPDATE Supplier_Base SET State = '"+state+"' WHERE Supplier_ID = '"+supplierID+"'");
            return(DBHelper.ExecuteNonQuery(sqlText.ToString()));
        }

        /// <summary>
        /// 查询行业类别
        /// </summary>
        /// <returns></returns>
        public DataTable queryIndustry()
        {
            String sqlText = "SELECT DISTINCT Industry_Category FROM Supplier_Base WHERE Industry_Category is not NULL";
            return (DBHelper.ExecuteQueryDT(sqlText));
        }

        /// <summary>
        /// 保存供应商营业额
        /// </summary>
        /// <param name="supplierID"></param>
        /// <param name="year"></param>
        /// <param name="turnover"></param>
        /// <returns></returns>
        public int saveSupplierTurnover(String supplierID, String year, String turnover)
        {
            String sqlText = "INSERT INTO Supplier_Turnover(Supplier_ID,Turnover,[year]) VALUES('"+supplierID+"', '"+turnover+"', '"+year+"')";
            int lines = DBHelper.ExecuteNonQuery(sqlText.ToString());
            return lines;
        }
        /// <summary>
        /// 获取供应商区分结果
        /// </summary>
        /// <param name="conditionInfo"></param>
        /// <returns></returns>
        public DataTable queryClassifyResult(conditionInfo conditionInfo)
        {
            StringBuilder str = new StringBuilder();
            if (conditionInfo.PurId != null && !conditionInfo.PurId.Equals("")) {

                str.Append(" and sr.SelectedDep=@PurId ");

            }
            if (conditionInfo.SupplierName != null && !conditionInfo.SupplierName.Equals(""))
            {
                str.Append(" and sb.Supplier_Name LIKE '%" + conditionInfo.SupplierName + "%' ");

            }
            if (conditionInfo.STime != null && !conditionInfo.STime.Equals(""))
            {
                str.Append(" and sp.Record_Time>@sTime  ");

            }
            if (conditionInfo.ETime != null && !conditionInfo.ETime.Equals(""))
            {
                str.Append(" and sp.Record_Time<@eTime ");

            }
            if (conditionInfo.Level != null && !conditionInfo.Level.Equals(""))
            {
                str.Append(" and sp.Supplier_Position=@level ");

            }

            String sqlText = @"select top "+ conditionInfo.PageSize + " * from (select sp.Supplier_ID,sb.Supplier_Name,  case  sp.Supplier_Position when 'A' then '优选' when 'B' then '有价值' when 'C' then '待改善' when 'D' then '可删除' ELSE '其他' END  as Supplier_Position,sp.Record_Time,ROW_NUMBER () OVER ( ORDER BY sp.Supplier_ID ) AS RowNumber from Supplier_Position sp,Supplier_Base sb,SR_Info sr  where  sp.Supplier_ID=sb.Supplier_ID  and sb.Supplier_ID=sr.SupplierId " + str+" ) as tb  where RowNumber>"+conditionInfo.PageSize*(conditionInfo.PageIndex -1);

            SqlParameter[] sqlParameter = new SqlParameter[] {
                new SqlParameter("@PurId",conditionInfo.PurId),
                new SqlParameter("@Supplier_Name",conditionInfo.SupplierName),
                new SqlParameter("@sTime",conditionInfo.STime),
                new SqlParameter("@eTime",conditionInfo.ETime),
                new SqlParameter("@level",conditionInfo.Level)
    
            };

           return DBHelper.ExecuteQueryDT(sqlText,sqlParameter); 
        }
        /// <summary>
        /// 查询记录条数
        /// </summary>
        /// <param name="conditionInfo"></param>
        /// <returns></returns>
        public int queryRecordCount(conditionInfo conditionInfo)
        {
            StringBuilder str = new StringBuilder();
            if (conditionInfo.PurId != null && !conditionInfo.PurId.Equals(""))
            {

                str.Append(" and sr.SelectedDep=@PurId ");

            }
            if (conditionInfo.SupplierName != null && !conditionInfo.SupplierName.Equals(""))
            {
                str.Append(" and sb.Supplier_Name LIKE '%"+ conditionInfo.SupplierName + "%' ");

            }
            if (conditionInfo.STime != null && !conditionInfo.STime.Equals(""))
            {
                str.Append(" and sp.Record_Time>@sTime  ");

            }
            if (conditionInfo.ETime != null && !conditionInfo.ETime.Equals(""))
            {
                str.Append(" and sp.Record_Time<@eTime ");

            }
            if (conditionInfo.Level != null && !conditionInfo.Level.Equals(""))
            {
                str.Append(" and sp.Supplier_Position=@level ");

            }

            SqlParameter[] sqlParameter = new SqlParameter[] {
                new SqlParameter("@PurId",conditionInfo.PurId),
                new SqlParameter("@Supplier_Name",conditionInfo.SupplierName),
                new SqlParameter("@sTime",conditionInfo.STime),
                new SqlParameter("@eTime",conditionInfo.ETime),
                new SqlParameter("@level",conditionInfo.Level),
                new SqlParameter("@pageSize",conditionInfo.PageSize),
                new SqlParameter("@pageIndex",conditionInfo.PageIndex)
            };

            String sqlText = @"select count(*) from Supplier_Position sp,Supplier_Base sb,SR_Info sr  where  sp.Supplier_ID=sb.Supplier_ID  and sb.Supplier_ID=sr.SupplierId " + str;
            return int.Parse(DBHelper.ExecuteQueryDT(sqlText,sqlParameter).Rows[0][0].ToString());
        }
        /// <summary>
        /// 获取采购组织
        /// </summary>
        /// <returns></returns>
        public DataTable getPurOrg()
        {
            String sqlText = @"select Buyer_Org_Name from Buyer_Org";
            return DBHelper.ExecuteQueryDT(sqlText);

        }
        /// <summary>
        /// 获取策略信息
        /// </summary>
        /// <param name="levelType"></param>
        /// <returns></returns>
        public DataTable getStrategyInfo(string levelType)
        {
          
            String sqlText = @"SELECT strategyId,strategyInfo from TabStrategyInfo where typeId='" + levelType + "'";
            DataTable dt = DBHelper.ExecuteQueryDT(sqlText);
            return dt;
        }

        public DataTable getStrategyType()
        {
            String sqlText = @"SELECT * from TabstrategyType";
            DataTable dt = DBHelper.ExecuteQueryDT(sqlText);
            return dt;
        }
        /// <summary>
        /// 保存策略信息
        /// </summary>
        /// <param name="strategyId"></param>
        /// <param name="supplierId"></param>
        /// <returns></returns>
        public bool insertStategyInfo(string strategyId, string supplierId)
        {
           
            //插入新策略
          string  sqlText = @" INSERT INTO TabSupplierAndStrategy ( supplierId, strategyId )
                        VALUES
	                    ( '"+supplierId+"', '"+strategyId+"' )";
            if (DBHelper.ExecuteNonQuery(sqlText) == 1) {

                return true;
            }
            return false;
        }
        /// <summary>
        /// 获取已经存在的策略信息
        /// </summary>
        /// <param name="supplierId"></param>
        /// <returns></returns>
        public DataTable getExsitStrategyBySupplierId(string supplierId)
        {
            String sqlText = @"select strategyId from TabSupplierAndStrategy where supplierId='" + supplierId + "'";
            DataTable dt = DBHelper.ExecuteQueryDT(sqlText);
            return dt;
        }

        public bool delExsitedStrategyInfo(string supplierId)
        {
            //删除已经存在的策略
            String sqlText = @"delete from TabSupplierAndStrategy where supplierId='" + supplierId + "'";
            int count = DBHelper.ExecuteNonQuery(sqlText);
            if (count == 1) {

                return true;
            }
            return false;
        }
        #endregion
    }
}
