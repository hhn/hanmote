﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using Lib.SqlServerDAL;
using Lib.IDAL.SupplierPerformaceIDAL;

namespace Lib.SqlServerDAL.SupplierPerformaceDAL
{
    class CostReductionDAL:CostReductionIDAL
    {
        /// <summary>
        /// 当月采购额
        /// </summary>
        /// <param name="supplierID"></param>
        /// <param name="materialID"></param>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <returns></returns>
        public Decimal queryPurchaseAmountThisMonth (String supplierID, String materialID, String year, String month)
        {
            StringBuilder sqlText = new StringBuilder();
            sqlText.Append("SELECT SUM(Number) FROM Enquiry ")
                .Append(" WHERE Supplier_ID = '"+supplierID+"' AND Material_ID = '"+materialID+"'")
                .Append(" AND MONTH(Update_Time) = '"+month+"' AND YEAR(Update_Time) = '"+year+"'");
            
            DataTable rstTable = DBHelper.ExecuteQueryDT(sqlText.ToString());
            try
            {
                Decimal rst = Convert.ToDecimal(rstTable.Rows[0][0]);
                return rst;
            }
            catch
            {
                return 0;
            } 
        }

        /// <summary>
        /// 查询 上年市场基准价
        /// </summary>
        /// <param name="supplierID"></param>
        /// <param name="materialID"></param>
        /// <param name="year"></param>
        /// <returns></returns>
        public Decimal queryMarketPriceLastYear( String materialID, String year)
        {
            StringBuilder sqlText = new StringBuilder();
            sqlText.Append("SELECT AVG(Market_Price) FROM Market_Price ")
                .Append(" WHERE Material_ID = '"+materialID+"'")
                .Append(" AND YEAR(End_Time) = YEAR(DATEADD(YEAR, -1, '" + year + "'))");

            DataTable rstTable = DBHelper.ExecuteQueryDT(sqlText.ToString());
            try
            {
                Decimal rst = Convert.ToDecimal(rstTable.Rows[0][0]);
                return rst;
            }
            catch
            {
                return 0;
            }

        }

        /// <summary>
        /// 查询上年末采购价
        /// </summary>
        /// <param name="supplierID"></param>
        /// <param name="materialID"></param>
        /// <param name="year"></param>
        /// <returns></returns>
        public Decimal queryPurchasePriceLastYearEnd(String supplierID, String materialID, String year)
        {
            StringBuilder sqlText = new StringBuilder();
            sqlText.Append("SELECT AVG(CONVERT(DECIMAL,Price)) FROM Enquiry")
                .Append(" WHERE Supplier_ID = '"+supplierID+"' AND Material_ID = '"+materialID+"'")
                .Append(" AND State = '已完成' AND YEAR(Update_Time) =  YEAR(DATEADD(YEAR, -1, '"+year+"'))")
                .Append(" AND MONTH(Update_Time) = '12'");

            DataTable rstTable = DBHelper.ExecuteQueryDT(sqlText.ToString());
            try
            {
                Decimal rst = Convert.ToDecimal(rstTable.Rows[0][0]);
                return rst;
            }
            catch
            {
                return 0;
            }
           
        }

        /// <summary>
        /// 查询当月市场基准价
        /// </summary>
        /// <param name="supplierID"></param>
        /// <param name="materialID"></param>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <returns></returns>
        public Decimal queryMarketPriceThisMonth( String materialID, String year, String month)
        {
            StringBuilder sqlText = new StringBuilder();
            sqlText.Append("SELECT AVG(Market_Price) FROM Market_Price")
                .Append(" WHERE Material_ID = '"+materialID+"'")
                .Append(" AND YEAR(End_Time) = '" + year + "' AND MONTH(End_Time) = '" + month + "'");
            DataTable rstTable = DBHelper.ExecuteQueryDT(sqlText.ToString());
            try
            {
                Decimal rst = Convert.ToDecimal(rstTable.Rows[0][0]);
                return rst;
            }
            catch
            {
                return 0;
            }

        }

        /// <summary>
        /// 查询当月采购价
        /// </summary>
        /// <param name="supplierID"></param>
        /// <param name="materialID"></param>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <returns></returns>
        public Decimal queryPurchasePriceThisMonth(String supplierID, String materialID, String year, String month)
        {
            StringBuilder sqlText = new StringBuilder();
            sqlText.Append("SELECT AVG(CONVERT(DECIMAL,Price)) FROM Enquiry")
                .Append(" WHERE Supplier_ID = '"+supplierID+"' AND Material_ID = '"+materialID+"'")
                .Append(" AND State = '已完成' AND YEAR(Update_Time) = '"+year+"' AND MONTH(Update_Time) = '"+month+"'");
            DataTable rstTable = DBHelper.ExecuteQueryDT(sqlText.ToString());
            try
            {
                Decimal rst = Convert.ToDecimal(rstTable.Rows[0][0]);
                return rst;
            }
            catch
            {
                return 0;
            }
            
        }

        /// <summary>
        /// 判断是否为市场管制品种
        /// </summary>
        /// <param name="materialID"></param>
        /// <returns></returns>
        public bool queryControledOrNot(String materialID)
        {
            String sqlText = "SELECT Market_Regulation_Identity FROM Material WHERE Material_ID = '"+materialID+"'";
            DataTable rstTable = DBHelper.ExecuteQueryDT(sqlText);
            if (rstTable.Rows.Count == 0)
            {
                return false;
            }
            else
            {
                //TODO:bug
                bool rst = Convert.ToBoolean(rstTable.Rows[0][0]);
                return rst;
            }
        }

        /// <summary>
        /// 保存降成本计算的记录
        /// </summary>
        /// <param name="savePara"></param>
        /// <returns></returns>
        public int saveCostReduction(List<Object> savePara)
        {
            StringBuilder sqlText = new StringBuilder();
            sqlText.Append(" INSERT INTO Cost_Reduction")
                .Append(" (Supplier_ID,Material_ID,Material_Type,Cost_Reduction,CostReduction_Rate")
                .Append(" ,[Year],[Month],Create_Time,Creator_Name)")
                .Append(" VALUES('"+savePara[0]+"','"+savePara[1]+"','"+savePara[2]+"','"+savePara[3]+"','"+savePara[4]+"','"+savePara[5]+"','"+savePara[6]+"','"+savePara[7]+"','"+savePara[8]+"')");

            int lines = DBHelper.ExecuteNonQuery(sqlText.ToString());
            return lines;
        }

        /// <summary>
        /// 查看符合时间条件的所有降成本记录
        /// </summary>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <returns></returns>
        public DataTable queryCostReductionResult(String year, String month)
        {
            String sqlText = "SELECT Supplier_ID,Material_ID,Material_Type,Cost_Reduction,CostReduction_Rate,Create_Time,Creator_Name FROM Cost_Reduction WHERE [Year] = '"+year+"' AND [Month] = '"+month+"'";
            return (DBHelper.ExecuteQueryDT(sqlText));
        }
    }
}
