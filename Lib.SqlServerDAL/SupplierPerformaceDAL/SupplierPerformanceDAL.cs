﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using Lib.SqlServerDAL;
using Lib.SqlIDAL.SupplierPerformaceIDAL;
using Lib.Common.MMCException.IDAL;
using System.Windows.Forms;

namespace Lib.SqlServerDAL.SupplierPerformaceDAL
{
    class SupplierPerformanceDAL : SupplierPerformanceIDAL
    {
        #region 供应商评估

        #region 价格标准的旧代码
        /// <summary>
        /// 次标准：价格-价格水平，查询数据表，得到市场价格和供应商有效价格，供BLL层计算
        /// </summary>
        /// <param name="materialID"></param>
        /// <param name="supplierID"></param>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <param name="priceLevelPara"></param>
        /// <returns></returns>
        public DataTable queryPriceLevel(String materialID, String supplierID, DateTime startTime, DateTime endTime, List<double> priceLevelPara)
        {
            String startTimeStr = startTime.ToString("yyyyMMdd");
            String endTimeStr = endTime.ToString("yyyyMMdd");
            DataTable resultTable;
            //检查是否维护了市场价格
            String sqlMarketPrice;
            sqlMarketPrice = "SELECT NetPrice FROM[RecordInfo] WHERE MaterialId = '" + materialID + "' AND CreateTime BETWEEN '" + startTimeStr + "' AND '" + endTimeStr + "'";
            //已经维护了市场价格
            if (DBHelper.ExecuteQueryDT(sqlMarketPrice) != null)
            {
                StringBuilder queryPrice1 = new StringBuilder();
                queryPrice1.Append("SELECT AVG(MarketPrice.NetPrice),AVG(Convert(decimal,SupplierPrice.Price))")
                    .Append(" FROM(SELECT NetPrice FROM [RecordInfo]")
                    .Append(" WHERE MaterialId = '" + materialID + "' AND CreateTime BETWEEN '" + startTimeStr + "' AND '" + endTimeStr + "')")
                    .Append(" AS MarketPrice ,")
                    .Append(" (SELECT Price FROM [Enquiry] ")
                    .Append(" WHERE Supplier_ID = '" + supplierID + "' AND Material_ID = '" + materialID + "' AND [State] = '已完成'")
                    .Append(" AND Update_Time BETWEEN '"+startTimeStr+"' AND '"+endTimeStr+"') AS SupplierPrice");

                resultTable = DBHelper.ExecuteQueryDT(queryPrice1.ToString());
                return (resultTable);
            }
            else //未维护市场价格
            {
                StringBuilder queryPrice2 = new StringBuilder();
                queryPrice2.Append("SELECT MarketPrice.MP,SupplierPrice.SP FROM ")
                    .Append(" (SELECT Material_ID,AVG(Convert(decimal,Price)) AS MP FROM [Enquiry] ")
                    .Append(" WHERE [State] = '已完成'")
                    .Append(" AND Update_Time BETWEEN '" + startTime + "' AND '" + endTime + "'")
                    .Append(" GROUP BY Material_ID ) AS MarketPrice, ")
                    .Append(" (SELECT Material_ID,Supplier_ID,AVG(Convert(decimal,Price)) AS SP   FROM [Enquiry] ")
                    .Append("  WHERE  [State] = '报价后'")
                    .Append(" AND Update_Time BETWEEN '" + startTimeStr + "' AND '" + endTimeStr + "'")
                    .Append("  Group BY Supplier_ID , Material_ID) AS SupplierPrice ")
                    .Append(" WHERE MarketPrice.Material_ID = '" + materialID + "' AND SupplierPrice.Material_ID = '" + materialID + "' ")
                    .Append("  AND SupplierPrice.Supplier_ID = '" + supplierID + "'");

                return (DBHelper.ExecuteQueryDT(queryPrice2.ToString()));
            }
        }

        /// <summary>
        /// 次标准：价格-价格历史
        /// </summary>
        /// <param name="materialID"></param>
        /// <param name="supplierID"></param>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <param name="priceHistoryPara"></param>
        /// <returns></returns>
        public DataTable queryPriceHistory(String materialID, String supplierID, DateTime startTime, DateTime endTime, List<double> priceHistoryPara)
        {
            String startTimeStr = startTime.ToString("yyyyMMdd");
            String endTimeStr = endTime.ToString("yyyyMMdd");
            //检查是否维护了市场价格
            String sqlMarketPrice;
            sqlMarketPrice = "SELECT NetPrice FROM [RecordInfo] WHERE MaterialId  = '" + materialID + "' AND CreateTime BETWEEN '" + startTimeStr + "' AND '" + endTimeStr + "'";

            //lastYearStart和lastYearEnd用于查询市场历史价格和供应商历史价格（使用上一年的平均价格）
            String lastYear = Convert.ToString(Convert.ToInt16(startTime.Year.ToString()) - 1);
            DateTime lastYearStart = Convert.ToDateTime(lastYear + "-" + "1-1");
            DateTime lastYearEnd = Convert.ToDateTime(lastYear + "-" + "12-31" + " 23:59:59");

            //已经维护了市场价格和市场历史价格
            StringBuilder priceQueryText = new StringBuilder();
            priceQueryText.Append(" SELECT* FROM")
                .Append(" (SELECT AVG(NetPrice) AS MCP FROM [RecordInfo]")
                .Append("  WHERE MaterialId = '" + materialID + "' AND CreateTime BETWEEN '" + startTimeStr + "' AND '" + endTimeStr + "')")
                .Append("  AS MCPTable  , ")
                .Append(" (SELECT AVG(NetPrice) AS MPP FROM [RecordInfo]")
                .Append("  WHERE MaterialId = '" + materialID + "' AND CreateTime BETWEEN '" + lastYearStart + "' AND '" + lastYearEnd + "')")
                .Append("  AS MPPTable,")
                .Append(" (SELECT AVG(Convert(decimal,Price))AS ASP FROM [Enquiry] ")
                .Append(" WHERE Supplier_ID = '" + supplierID + "' AND Material_ID = '" + materialID + "' AND [State] = '报价后'")
                .Append(" AND Update_Time BETWEEN '" + startTimeStr + "' AND '" + endTimeStr + "')")
                .Append("  AS ASPTable ,")
                .Append("  (SELECT AVG(Convert(decimal,Price))AS SPP FROM [Enquiry] ")
                .Append(" WHERE Supplier_ID = '" + supplierID + "' AND Material_ID = '" + materialID + "' AND [State] = '报价后'")
                .Append("  AND Update_Time BETWEEN '" + lastYearStart + "' AND '" + lastYearEnd + "')  AS SSPTable");

            DataTable dt = DBHelper.ExecuteQueryDT(priceQueryText.ToString());
            //查询出来的dt只有1行，每一列分别为MCP，MPP，ASP，SPP（市场当前价，市场历史价，供应商当前价，供应商历史价）
            return (dt);

        }
        #endregion
         
        #region 价格数据来源非采购信息记录

        #region 价格水平标准的新代码

        public DataTable queryOrderID0(String materialID, String supplierID,DateTime startTime,DateTime endTime)
        {
            String startTimeStr = startTime.ToString("yyyyMMdd");
            String endTimeStr = endTime.ToString("yyyyMMdd");
            String sqlText = "SELECT Inquiry_ID FROM Enquiry WHERE State = '已完成' AND Update_Time  BETWEEN '"+startTimeStr+"' AND '"+endTimeStr+"'";
            DataTable rstTable = DBHelper.ExecuteQueryDT(sqlText);
            return rstTable;
        }

        /// <summary>
        /// 查询某个订单对应的供应商有效价格
        /// </summary>
        /// <param name="orderID"></param>
        /// <param name="materialID"></param>
        /// <param name="supplierID"></param>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <returns></returns>
        public Decimal querySupplierPrice0(String orderID, String materialID, String supplierID, DateTime startTime, DateTime endTime)
        {
            String startTimeStr = startTime.ToString("yyyyMMdd");
            String endTimeStr = endTime.ToString("yyyyMMdd");
            StringBuilder sqlText = new StringBuilder();
            sqlText.Append("SELECT Price FROM Enquiry WHERE State = '已完成'")
                .Append("AND Update_Time  BETWEEN '"+startTimeStr+"' AND '"+endTimeStr+"'")
                .Append("AND Inquiry_ID = '"+orderID+"' AND Material_ID = '"+materialID+"'")
                .Append("AND Supplier_ID = '"+supplierID+"'");

            DataTable supplierPriceList = DBHelper.ExecuteQueryDT(sqlText.ToString());
            Decimal supplierPrice = 0M;
            if (supplierPriceList.Rows.Count > 0)
            {
                foreach (DataRow row in supplierPriceList.Rows)
                {
                    supplierPrice = supplierPrice + Convert.ToDecimal(row[0]);
                }
                supplierPrice = supplierPrice / (supplierPriceList.Rows.Count);
                return supplierPrice;
            }
            else
            {
                //返回-1表示暂时无有效数据
                return -1M;
            }
            
        }
        
        /// <summary>
        /// 查询市场价格
        /// </summary>
        /// <param name="orderID"></param>
        /// <param name="materialID"></param>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <returns></returns>
        public Decimal queryMarketPrice0(String orderID, String materialID,String supplierID)
        {
            StringBuilder sqlText = new StringBuilder();
            sqlText.Append("SELECT Market_Price FROM Market_Price ,(")
                .Append(" SELECT Update_Time FROM Enquiry WHERE Inquiry_ID = '"+orderID+"' ")
                .Append(" AND Material_ID = '"+materialID+"' AND Supplier_ID = '"+supplierID+"' ) as Table1")
                .Append(" WHERE  YEAR(Table1.Update_Time) = YEAR(Market_Price.Start_Time)")
                .Append(" AND MONTH(Table1.Update_Time) = MONTH(Market_Price.Start_Time)")
                .Append(" AND DAY(Table1.Update_Time) > DAY(Market_Price.Start_Time)")
                .Append(" AND DAY(Table1.Update_Time) < DAY(Market_Price.End_Time)");

            DataTable priceList = DBHelper.ExecuteQueryDT(sqlText.ToString());
            Decimal price = 0M;
            if (priceList.Rows.Count > 0)
            {
                foreach (DataRow row in priceList.Rows)
                {
                    price = price + Convert.ToDecimal(row[0]);
                }
                price = price / (priceList.Rows.Count);
                return price;
            }
            else
            {
                //返回-1表示暂无数据
                return -1M;
            }
                
        }

        #endregion

        #region 价格历史标准的新代码
        /// <summary>
        /// 查询供应商历史价格
        /// </summary>
        /// <param name="materialID"></param>
        /// <param name="supplierID"></param>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <returns></returns>
        public Decimal queryHistorySupplierPrice0(String materialID, String supplierID, DateTime startTime, DateTime endTime)
        {
            //lastYearStart和lastYearEnd用于查询供应商历史价格（使用上一年的平均价格）
            String lastYear = Convert.ToString(Convert.ToInt16(startTime.Year.ToString()) - 1);
            DateTime lastYearStart = Convert.ToDateTime(lastYear + "-" + "1-1");
            DateTime lastYearEnd = Convert.ToDateTime(lastYear + "-" + "12-31" + " 23:59:59");

            StringBuilder sqlText = new StringBuilder();
            sqlText.Append("SELECT Price FROM Enquiry WHERE Material_ID = '" + materialID + "' AND Supplier_ID='" + supplierID + "'")
                .Append("AND State = '已完成' AND Update_Time BETWEEN '" + lastYearStart + "' AND '" + lastYearEnd + "' ");

            DataTable rstList = DBHelper.ExecuteQueryDT(sqlText.ToString());
            Decimal rst = 0M;
            if (rstList.Rows.Count > 0)
            {
                foreach (DataRow row in rstList.Rows)
                {
                    rst = rst + Convert.ToDecimal(row[0]);
                }
                rst = rst / (rstList.Rows.Count);
                return rst;
            }
            else
            {
                return -1M;
            }
        }

        /// <summary>
        /// 查询市场历史价格
        /// </summary>
        /// <param name="materialID"></param>
        /// <param name="supplierID"></param>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <returns></returns>
        public Decimal queryHistoryMarketPrice0(String materialID, DateTime startTime, DateTime endTime)
        {
            //lastYearStart和lastYearEnd用于查询市场历史价格（使用上一年的平均价格）
            String lastYear = Convert.ToString(Convert.ToInt16(startTime.Year.ToString()) - 1);
            DateTime lastYearStart = Convert.ToDateTime(lastYear + "-" + "1-1");
            DateTime lastYearEnd = Convert.ToDateTime(lastYear + "-" + "12-31" + " 23:59:59");

            StringBuilder sqlText = new StringBuilder();
            sqlText.Append("SELECT Market_Price from Market_Price WHERE Material_ID = '"+materialID+"' ")
                .Append(" AND Start_Time BETWEEN '"+lastYearStart+"' AND '"+lastYearEnd+"'")
                .Append(" AND End_Time BETWEEN '"+lastYearStart+"' AND '"+lastYearEnd+"'");

            DataTable rstList = DBHelper.ExecuteQueryDT(sqlText.ToString());
            Decimal rst = 0M;

            if (rstList.Rows.Count > 0)
            {
                foreach (DataRow row in rstList.Rows)
                {
                    rst = rst + Convert.ToDecimal(row[0]);
                }
                rst = rst / rstList.Rows.Count;
                return rst;
            }
            else
            {
                //无相关数据返回-1
                return -1M;
            }
        }

        #endregion
        #endregion

        #region 价格数据来源于采购信息记录

        /// <summary>
        /// 查询采购信息记录id
        /// </summary>
        /// <param name="materialID"></param>
        /// <param name="supplierID"></param>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <returns></returns>
        public DataTable queryOrderID(String materialID, String supplierID, DateTime startTime, DateTime endTime)
        {
            String startTimeStr =  startTime.ToString("yyyyMMdd");
            String endTimeStr = endTime.ToString("yyyyMMdd");
            String sqlText = "SELECT recordId FROM RecordInfo WHERE CreateTime BETWEEN '" + startTimeStr+ "' AND '"+ endTimeStr + "' AND MaterialId = '" + materialID+"' AND SupplierId = '"+supplierID+"'";
            DataTable rstTable = DBHelper.ExecuteQueryDT(sqlText);
            return rstTable;
        }

        /// <summary>
        /// 查询供应商有效价格
        /// </summary>
        /// <param name="materialID"></param>
        /// <param name="supplierID"></param>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <returns></returns>
        public Decimal querySupplierPrice(String materialID, String supplierID, DateTime startTime, DateTime endTime)
        {
            String startTimeStr = startTime.ToString("yyyyMMdd");
            String endTimeStr = endTime.ToString("yyyyMMdd");
            StringBuilder sqlText = new StringBuilder();
            sqlText.Append("SELECT AVG(NetPrice) FROM RecordInfo WHERE SupplierId = '" + supplierID + "' ")
                .Append("AND MaterialId = '" + materialID + "' ")
                .Append(" AND CreateTime BETWEEN '"+startTimeStr+"' AND '"+endTimeStr+"'");

            DataTable supplierPriceList = DBHelper.ExecuteQueryDT(sqlText.ToString());
            Decimal supplierPrice = -1M;
            if (supplierPriceList.Rows.Count > 0)
            {
                foreach (DataRow row in supplierPriceList.Rows)
                {
                    if (String.IsNullOrEmpty(row[0].ToString()))
                    {
                        supplierPrice = -1M;
                        return supplierPrice;
                    }
                    else
                    {
                        supplierPrice = Convert.ToDecimal(row[0]);
                    }
                }
            }
            return supplierPrice;
        }

        /// <summary>
        /// 查询市场价格
        /// </summary>
        /// <param name="materialID"></param>
        /// <returns></returns>
        public Decimal queryMarketPrice(String materialID, String supplierID,DateTime startTime,DateTime endTime)
        {
            StringBuilder sqlText = new StringBuilder();
            String startTimeStr = startTime.ToString("yyyyMMdd");
            String endTimeStr = endTime.ToString("yyyyMMdd");
            //自动评估
            sqlText.Append("SELECT AVG(NetPrice) FROM RecordInfo WHERE MaterialId = '" + materialID + "' AND CreateTime BETWEEN '" + startTimeStr + "' AND '" + endTimeStr + "'");

            DataTable priceList = DBHelper.ExecuteQueryDT(sqlText.ToString());
            Decimal price = -1M;
            if (priceList.Rows.Count > 0)
            {
                foreach (DataRow row in priceList.Rows)
                {
                    String priceStr = row[0].ToString();
                    if (String.IsNullOrEmpty(priceStr))
                    {
                        price = -1M;
                        return price;
                    }
                    else
                    {
                        price = Convert.ToDecimal(priceStr);
                    }
                }
                    
            }
            return price;
        }

        /// <summary>
        /// 查询供应商历史价格
        /// </summary>
        /// <param name="materialID"></param>
        /// <param name="supplierID"></param>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <returns></returns>
        public Decimal queryHistorySupplierPrice(String materialID, String supplierID, DateTime startTime, DateTime endTime)
        {
            //lastYearStart和lastYearEnd用于查询市场历史价格（使用上一年的平均价格）
            String lastYear = Convert.ToString(Convert.ToInt16(startTime.Year.ToString()) - 1);
            DateTime lastYearStart = Convert.ToDateTime(lastYear + "-" + "1-1");
            DateTime lastYearEnd = Convert.ToDateTime(lastYear + "-" + "12-31" + " 23:59:59");
            String startTimeStr = startTime.ToString("yyyyMMdd");
            String endTimeStr = endTime.ToString("yyyyMMdd");


            StringBuilder sqlText = new StringBuilder(); 
            sqlText.Append("SELECT AVG(NetPrice) FROM RecordInfo WHERE SupplierId = '" + supplierID + "' ")
           .Append("AND MaterialId = '" + materialID + "' ")
           .Append(" AND CreateTime BETWEEN '" + startTimeStr + "' AND '" + endTimeStr + "'");

            DataTable priceList = DBHelper.ExecuteQueryDT(sqlText.ToString());
            Decimal price = 0M;
            if (priceList.Rows.Count > 0)
            {
                foreach (DataRow row in priceList.Rows)
                {
                    price = price + Convert.ToDecimal(row[0]);
                }
                price = price / (priceList.Rows.Count);
                return price;
            }
            else
            {
                //返回-1表示暂无数据
                return -1M;
            }
        }

        /// <summary>
        /// 查询市场历史价格
        /// </summary>
        /// <param name="materialID"></param>
        /// <param name="supplierID"></param>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <returns></returns>
        public Decimal queryHistoryMarketPrice(String materialID, DateTime startTime, DateTime endTime)
        {
            //lastYearStart和lastYearEnd用于查询市场历史价格（使用上一年的平均价格）
            String lastYear = Convert.ToString(Convert.ToInt16(startTime.Year.ToString()) - 1);
            DateTime lastYearStart = Convert.ToDateTime(lastYear + "-" + "1-1");
            DateTime lastYearEnd = Convert.ToDateTime(lastYear + "-" + "12-31" + " 23:59:59");

            String lastYearStartStr = lastYearStart.ToString("yyyyMMdd");
            String lastYearEndStr = lastYearEnd.ToString("yyyyMMdd");
            StringBuilder sqlText = new StringBuilder();
            sqlText.Append("SELECT AVG(NetPrice) FROM RecordInfo WHERE MaterialId = '" + materialID + "' AND CreateTime BETWEEN '" + lastYearStartStr + "' AND '" + lastYearEndStr + "'");

            DataTable priceList = DBHelper.ExecuteQueryDT(sqlText.ToString());
            Decimal price = -1M;
            if (priceList.Rows.Count > 0)
            {
                foreach (DataRow row in priceList.Rows)
                {
                    if (String.IsNullOrEmpty(row[0].ToString()))
                    {
                        price = -1M;
                        return price;
                    }
                    else
                    {
                        price = Convert.ToDecimal(row[0].ToString());
                    }
                }
                return price;
            }
            else
            {
                //返回-1表示暂无数据
                return -1M;
            }
        }

        #endregion

        /// <summary>
        /// 次标准：质量-收货
        /// </summary>
        /// <param name="materialID"></param>
        /// <param name="supplierID"></param>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <param name="PPMPara"></param>
        /// <returns></returns>
        public DataTable queryReceiveGoods(String materialID, String supplierID, DateTime startTime, DateTime endTime, List<double> PPMPara)
        {
            String sqlText;
            String startTimeStr = startTime.ToString("yyyyMMdd");
            String endTimeStr = endTime.ToString("yyyyMMdd");

            //判断物料为原材料还是非原材料
            // String isRawMaterial = "";
            if (this.queryMaterialType(materialID) == "0" )  
            {
                //当该物料为非原材料时，计算PPM分数，并将该分数更新到数据表NonRaw_Material_Receive_Check中
                StringBuilder updatePPM = new StringBuilder();
                updatePPM.Append("UPDATE [NonRaw_Material_Receive_Check] ")
                    .Append(" SET [NonRaw_Material_Receive_Check].PPM = (RejNumTable.Reject_Num/TotalNumTable.Total_Num)*1000000 ")
                    .Append(" FROM(SELECT  [NonRaw_Material_Receive_Check].NonRawReceiveCheck_ID  ,[NonRaw_Material_Receive_Check].Supplier_ID ")
                    .Append(" ,[NonRaw_Material_Receive_Check].Material_ID,[NonRaw_Material_Receive_Check].Reject_Num ")
                    .Append("  FROM [NonRaw_Material_Receive_Check], [GoodsReceive_Note] ")
                    .Append("  WHERE [NonRaw_Material_Receive_Check].Supplier_ID='" + supplierID + "' AND [NonRaw_Material_Receive_Check].Material_ID= '" + materialID)
                    .Append("' AND [NonRaw_Material_Receive_Check].GoodsReceiveNote_ID = [GoodsReceive_Note].GoodsReceiveNote_ID ")
                    .Append("  AND Create_Time BETWEEN '" + startTimeStr + "' AND '" + endTimeStr + "' ) AS RejNumTable")
                    .Append(" ,(SELECT [GoodsReceive_Note].Supplier_ID,[GoodsReceive_Note].Material_ID ")
                    .Append("  ,SUM([GoodsReceive_Note].Receive_Count) as Total_Num ")
                    .Append("  FROM [GoodsReceive_Note] WHERE [GoodsReceive_Note].StockIn_Date")
                    .Append("  BETWEEN '" + startTimeStr + "' AND '" + endTimeStr + "'")
                    .Append("  GROUP BY [GoodsReceive_Note].Material_ID , [GoodsReceive_Note].Supplier_ID ) AS TotalNumTable")
                    .Append("  WHERE RejNumTable.Material_ID = TotalNumTable.Material_ID  AND RejNumTable.Supplier_ID = TotalNumTable.Supplier_ID  ")
                    .Append(" AND RejNumTable.NonRawReceiveCheck_ID = [NonRaw_Material_Receive_Check].NonRawReceiveCheck_ID ");
                DBHelper.ExecuteNonQuery(updatePPM.ToString());

                sqlText = " SELECT PPM FROM [NonRaw_Material_Receive_Check] WHERE Supplier_ID = '" + supplierID + "' AND Material_ID = '" + materialID + "'  AND Create_Time between '" + startTime + "' AND  '" + endTime + "'";
            }
            else
            {
                sqlText = " SELECT RawMaterialReceiveCheck_Score FROM [Raw_Material_Receive_Check]  WHERE Supplier_ID = '" + supplierID + "' AND Material_ID = '" + materialID + "'";
            }
            try
            {
                return DBHelper.ExecuteQueryDT(sqlText);
            }
            catch(DBException ex)
            {
                MessageBox.Show("数据加载失败");
                return null;
            }
            
        }

        /// <summary>
        /// 次标准：质量-质量审计
        /// </summary>
        /// <param name="materialID"></param>
        /// <param name="supplierID"></param>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <returns></returns>
        public DataTable queryQualityAudit(String materialID, String supplierID, DateTime startTime, DateTime endTime)
        {
            String startTimeStr = startTime.ToString("yyyyMMdd");
            String endTimeStr = endTime.ToString("yyyyMMdd");
            //获得审核类型分数（审核类型为过程审核或体系审核）
            String sqlText1 = "SELECT Audit_Type,AVG(Score) FROM [System_Process_Audit] WHERE Supplier_ID ='" + supplierID + "' AND Material_ID ='" + materialID + "' AND Score is not null  AND Create_Time BETWEEN '" + startTimeStr + "' AND '" + endTimeStr + "' GROUP BY Audit_Type";
            DataTable table1 = DBHelper.ExecuteQueryDT(sqlText1);

            //获得产品审核的分数
            String sqlText2 = "SELECT avg(ProductAudit_Score) FROM [Product_Audit] WHERE Supplier_ID = '"+supplierID+"' AND Material_ID = '"+materialID+"' AND ProductAudit_Score is not null AND Create_Time BETWEEN '"+startTimeStr+"' AND '"+endTimeStr+"'";
            DataTable table2 = DBHelper.ExecuteQueryDT(sqlText2);

            //合并之后，前两行未审核类型、分数、空值，最后一行为空值、空值、分数（产品审核）
            table1.Merge(table2);

            return table1;
        }
        
        /// <summary>
        /// 次标准：质量-抱怨/拒绝水平，查询Complaint_Reject数据表
        /// </summary>
        /// <param name="materialID"></param>
        /// <param name="supplierID"></param>
        /// <param name="purchasingORGID"></param>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <returns></returns>
        public DataTable queryComplaintReject(String materialID, String supplierID, String purchasingORGID, DateTime startTime, DateTime endTime)
        {
            String startTimeStr = startTime.ToString("yyyyMMdd");
            String endTimeStr = endTime.ToString("yyyyMMdd");
            String sqlText = " SELECT Ratio,Transaction_Amount,Complaint_Reject_Cost,Complaint_Reject_Count FROM [Complaint_Reject] WHERE Supplier_ID = '"+supplierID+"' AND Material_ID = '"+materialID+"' AND PurchasingORG_ID = '"+purchasingORGID+"' AND Create_Time  BETWEEN '"+startTimeStr+"' AND '"+endTimeStr+"'";

            return DBHelper.ExecuteQueryDT(sqlText);
        }

        public DataTable queryComplaintReject(String materialID, String supplierID, DateTime startTime, DateTime endTime)
        {
            String startTimeStr = startTime.ToString("yyyyMMdd");
            String endTimeStr = endTime.ToString("yyyyMMdd");
            String sqlText = " SELECT Ratio,Transaction_Amount,Complaint_Reject_Cost,Complaint_Reject_Count FROM [Complaint_Reject] WHERE Supplier_ID = '" + supplierID + "' AND Material_ID = '" + materialID + "' AND Create_Time  BETWEEN '" + startTimeStr + "' AND '" + endTimeStr + "'";

            return DBHelper.ExecuteQueryDT(sqlText);
        }

        /// <summary>
        /// 次标准：交货-按时交货的表现，查询GoodsReceive_Note数据表和Enquiry数据表
        /// </summary>
        /// <param name="materialID"></param>
        /// <param name="supplierID"></param>
        /// <param name="purchasingORGID"></param>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <returns></returns>
        public DataTable queryDeliverOnTime(String materialID, String supplierID, String purchasingORGID, DateTime startTime, DateTime endTime)
        {
            StringBuilder sqlText = new StringBuilder();
            String startTimeStr = startTime.ToString("yyyyMMdd");
            String endTimeStr = endTime.ToString("yyyyMMdd");
            sqlText.Append("SELECT [Enquiry].Delivery_Time, [GoodsReceive_Note].StockIn_Date  FROM ")
                .Append(" [Enquiry],[GoodsReceive_Note]  WHERE [Enquiry].Inquiry_ID = [GoodsReceive_Note].Order_ID ")
                .Append(" AND [Enquiry].Material_ID = [GoodsReceive_Note].Material_ID")
                .Append(" AND [Enquiry].[State] = '询价项'")
                .Append(" AND [Enquiry].Material_ID ='" + materialID + "'")
                .Append(" AND [GoodsReceive_Note].StockIn_Date BETWEEN '" + startTimeStr + "' AND '" + endTimeStr + "'");
            
            return DBHelper.ExecuteQueryDT(sqlText.ToString());
        }

        public DataTable queryDeliverOnTime(String materialID, String supplierID, DateTime startTime, DateTime endTime)
        {
            StringBuilder sqlText = new StringBuilder();
            String startTimeStr = startTime.ToString("yyyyMMdd");
            String endTimeStr = endTime.ToString("yyyyMMdd");

            sqlText.Append("SELECT [Enquiry].Delivery_Time, [GoodsReceive_Note].StockIn_Date  FROM ")
                .Append(" [Enquiry],[GoodsReceive_Note]  WHERE [Enquiry].Inquiry_ID = [GoodsReceive_Note].Order_ID ")
                .Append(" AND [Enquiry].Material_ID = [GoodsReceive_Note].Material_ID")
                .Append(" AND [Enquiry].[State] = '询价项'")
                .Append(" AND [Enquiry].Material_ID ='" + materialID + "'")
                .Append(" AND [Enquiry].Supplier_ID = [GoodsReceive_Note].Supplier_ID AND [Enquiry].Supplier_ID = '"+supplierID+"'")
                .Append(" AND [GoodsReceive_Note].StockIn_Date BETWEEN '" + startTimeStr + "' AND '" + endTimeStr + "'");

            return DBHelper.ExecuteQueryDT(sqlText.ToString());
        }

        /// <summary>
        /// 次标准：交货-确认日期，查询GoodsReceive_Note数据表和Enquiry数据表
        /// </summary>
        /// <param name="materialID"></param>
        /// <param name="supplierID"></param>
        /// <param name="purchasingORGID"></param>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <param name="confirmDataPara"></param>
        /// <returns></returns>
        public DataTable queryConfirmDate(String materialID, String supplierID, String purchasingORGID, DateTime startTime, DateTime endTime, double confirmDataPara)
        {
            StringBuilder sqlText = new StringBuilder();
            String startTimeStr = startTime.ToString("yyyyMMdd");
            String endTimeStr = endTime.ToString("yyyyMMdd");
            sqlText.Append(" SELECT [Enquiry].Delivery_Time, [GoodsReceive_Note].StockIn_Date FROM [Enquiry],[GoodsReceive_Note]")
               .Append(" WHERE [Enquiry].Inquiry_ID = [GoodsReceive_Note].Order_ID AND")
               .Append(" [Enquiry].Material_ID = [GoodsReceive_Note].Material_ID AND [Enquiry].Supplier_ID = [GoodsReceive_Note].Supplier_ID")
               .Append(" AND [Enquiry].Material_ID ='" + materialID + "'  AND [Enquiry].Supplier_ID = '" + supplierID + "'")
               .Append(" AND [Enquiry].[State] = '已完成'")
               .Append(" AND  [GoodsReceive_Note].StockIn_Date BETWEEN '" + startTimeStr + "' AND '" + endTimeStr + "'");
            return DBHelper.ExecuteQueryDT(sqlText.ToString());
        }
        public DataTable queryConfirmDate(String materialID, String supplierID, DateTime startTime, DateTime endTime, double confirmDataPara)
        {
            String startTimeStr = startTime.ToString("yyyyMMdd");
            String endTimeStr = endTime.ToString("yyyyMMdd");
            StringBuilder sqlText = new StringBuilder();
            sqlText.Append(" SELECT [Enquiry].Delivery_Time, [GoodsReceive_Note].StockIn_Date FROM [Enquiry],[GoodsReceive_Note]")
               .Append(" WHERE [Enquiry].Inquiry_ID = [GoodsReceive_Note].Order_ID AND")
               .Append(" [Enquiry].Material_ID = [GoodsReceive_Note].Material_ID AND [Enquiry].Supplier_ID = [GoodsReceive_Note].Supplier_ID")
               .Append(" AND [Enquiry].Material_ID ='" + materialID + "'  AND [Enquiry].Supplier_ID = '" + supplierID + "'")
               .Append(" AND [Enquiry].[State] = '已完成'")
               .Append(" AND  [GoodsReceive_Note].StockIn_Date BETWEEN '" + startTimeStr + "' AND '" + endTimeStr + "'");
            return DBHelper.ExecuteQueryDT(sqlText.ToString());
        }

        /// <summary>
        /// 次标准:交货-数量可靠性，查询GoodsReceive_Note数据表
        /// </summary>
        /// <param name="materialID"></param>
        /// <param name="supplierID"></param>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <returns></returns>
        public DataTable queryQuantityReliability(String materialID, String supplierID, DateTime startTime, DateTime endTime)
        {
            String startTimeStr = startTime.ToString("yyyyMMdd");
            String endTimeStr = endTime.ToString("yyyyMMdd");
            String sqlText = " SELECT SUM(Receive_Count),SUM(CountInOrder)/COUNT(CountInOrder) FROM [GoodsReceive_Note]  WHERE Material_ID = '"+materialID+"' AND Supplier_ID ='"+supplierID+"' AND StockIn_Date BETWEEN '"+startTimeStr+"' AND '"+endTimeStr+"'  GROUP BY [Order_ID]";
            return DBHelper.ExecuteQueryDT(sqlText);
        }

        /// <summary>
        /// 次标准：收货-对装运须知的遵守，查询Goods_Shipment数据表
        /// </summary>
        /// <param name="materialID"></param>
        /// <param name="supplierID"></param>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <returns></returns>
        public DataTable queryGoodsShipment(String materialID, String supplierID, DateTime startTime, DateTime endTime)
        {
            String startTimeStr = startTime.ToString("yyyyMMdd");
            String endTimeStr = endTime.ToString("yyyyMMdd");
            String sqlText = "SELECT GoodsShipment_Score From [GoodsReceive_Note] WHERE Material_ID='"+materialID+"' AND Supplier_ID='"+supplierID+"' AND StockIn_Date BETWEEN '"+startTimeStr+"' AND '"+endTimeStr+"'";
            return DBHelper.ExecuteQueryDT(sqlText);
        }

        /// <summary>
        /// 主标准：一般服务/支持
        /// </summary>
        /// <param name="materialID"></param>
        /// <param name="supplierID"></param>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <returns></returns>
        public DataTable queryServiceSupport(String materialID, String supplierID, DateTime startTime, DateTime endTime)
        {
            String startTimeStr = startTime.ToString("yyyyMMdd");
            String endTimeStr = endTime.ToString("yyyyMMdd");
            String sqlText = " SELECT InnovationSco as Innovation_Score,RealizableSco  as Reliability_Score,UserServiceSco as CustomerService_Score FROM Hanmote_GenService WHERE mtId='" + materialID + "' AND supplierId='" + supplierID + "' AND ServiceTime BETWEEN '" + startTimeStr + "' AND '" + endTimeStr + "'";

            return DBHelper.ExecuteQueryDT(sqlText);
        }

        /// <summary>
        /// 主标准：外部服务
        /// </summary>
        /// <param name="materialID"></param>
        /// <param name="supplierID"></param>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <returns></returns>
        public DataTable queryExternalService(String materialID, String supplierID, DateTime startTime, DateTime endTime)
        {
            String startTimeStr = startTime.ToString("yyyyMMdd");
            String endTimeStr = endTime.ToString("yyyyMMdd");
            String sqlText = "SELECT serviceQualifyScore as ServiceQuality_Score, serviceTimeScore as TimelinessOfService_Score FROM Hanmote_ExtService WHERE mtId='" + materialID + "' AND supplierId='" + supplierID + "' AND serviceTime BETWEEN '" + startTimeStr+"' AND '"+endTimeStr+"'";
            return DBHelper.ExecuteQueryDT(sqlText);
        }

        /// <summary>
        /// 结果保存
        /// 将绩效评估结果插入数据表Supplier_Performance中(需要修改)
        /// </summary>
        public void nonQuerySupplierPerformance(String supplierID, String materialID, Decimal score, String createTime, String creatorName)
        {
            LinkedList<string> sqlList = new LinkedList<String>();
            //插入“一般服务支持分数”
            String sqlText = "INSERT INTO [Supplier_Performance](Supplier_ID,Material_ID,GeneralServiceAndSupport_Score,Evaluation_Time,Creator_Name) VALUES (@supplierID,@materialID,@score,@createTime,@creatorName)";
            SqlParameter[] sqlParameter = new SqlParameter[]{
                new SqlParameter("@supplierID",SqlDbType.VarChar),
                new SqlParameter("@materialID",SqlDbType.VarChar),
                new SqlParameter("@score",SqlDbType.Decimal),
                new SqlParameter("@createTime",SqlDbType.DateTime),
                new SqlParameter("@creatorName",SqlDbType.VarChar)
            };
            sqlParameter[0].Value = supplierID;
            sqlParameter[1].Value = materialID;
            sqlParameter[2].Value = score;
            sqlParameter[3].Value = Convert.ToDateTime(createTime);
            sqlParameter[4].Value = creatorName;

            DBHelper.ExecuteNonQuery(sqlText, sqlParameter);
        }

        /// <summary>
        /// 查询所有物料类别
        /// </summary>
        /// <returns></returns>
        public DataTable queryMaterialClassify()
        {
            String sqlText;
            sqlText = "SELECT DISTINCT(Bigclassfy_Name) FROM Bigclassfy";
            return (DBHelper.ExecuteQueryDT(sqlText));
        }

        /// <summary>
        /// 结果显示，查询Supplier_Performance数据表主标准分数和总分等字段
        /// </summary>
        /// <param name="materialID"></param>
        /// <param name="supplierID"></param>
        /// <param name="purchasingORGID"></param>
        /// <returns></returns>
        public DataTable queryResult(String materialID, String supplierID, String purchasingORGID)
        {
            String sqlText = "  SELECT IsCulmunativeAssess,Evaluation_Period,Creator_Name,Evaluation_Time, Price_Score,Quality_Score,Delivery_Score,GeneralServiceAndSupport_Score,ExternalService_Score, Total_Score,Supplier_ID,Material_ID,PurchasingORG_ID,ID   FROM [Supplier_Performance] WHERE Supplier_ID = @supplierID AND PurchasingORG_ID = @purchasingORGID AND Material_ID = @materialID";
            SqlParameter[] sqlParameter = new SqlParameter[]{
                new SqlParameter("@materialID",SqlDbType.VarChar),
                new SqlParameter("@supplierID",SqlDbType.VarChar),
                new SqlParameter("@purchasingORGID",SqlDbType.VarChar)
            };
            sqlParameter[0].Value = materialID;
            sqlParameter[1].Value = supplierID;
            sqlParameter[2].Value = purchasingORGID;

            return DBHelper.ExecuteQueryDT(sqlText, sqlParameter);
        }


        /// <summary>
        /// 根据物料ID和年份查询绩效评估结果记录
        /// </summary>
        /// <param name="materialID"></param>
        /// <param name="year"></param>
        /// <returns></returns>
        public DataTable queryResult(String materialID, String year)
        {
            StringBuilder sqlText = new StringBuilder();
            sqlText.Append("SELECT IsCulmunativeAssess,Evaluation_Period,Creator_Name,Evaluation_Time, Price_Score,")
                .Append("Quality_Score,Delivery_Score,GeneralServiceAndSupport_Score,ExternalService_Score")
                .Append(", Total_Score,Supplier_ID,Material_ID,PurchasingORG_ID,ID,Supplier_ID,Material_ID")
                .Append(" FROM Supplier_Performance WHERE Material_ID = '" + materialID + "' AND   SUBSTRING(Evaluation_Period,1,4) = '" + year + "'");
            return(DBHelper.ExecuteQueryDT(sqlText.ToString()));
        }

        /// <summary>
        /// 详细结果查询（查询Supplier_Performance所有字段）
        /// </summary>
        /// <param name="materialID"></param>
        /// <param name="supplierID"></param>
        /// <param name="purchasingORGID"></param>
        /// <param name="idNum"></param>
        /// <returns></returns>
        public DataTable queryDetailResult(String materialID, String supplierID, String purchasingORGID,int idNum)
        {
            String sqlText = "    SELECT *  FROM [Supplier_Performance]   WHERE ID = @idNum AND Supplier_ID = @supplierID AND PurchasingORG_ID = @purchasingORGID AND Material_ID =@materialID";
            SqlParameter[] sqlParameter = new SqlParameter[]{
                new SqlParameter("@materialID",SqlDbType.VarChar),
                new SqlParameter("@supplierID",SqlDbType.VarChar),
                new SqlParameter("@purchasingORGID",SqlDbType.VarChar),
                new SqlParameter("@idNum",SqlDbType.VarChar)
            };
            sqlParameter[0].Value = materialID;
            sqlParameter[1].Value = supplierID;
            sqlParameter[2].Value = purchasingORGID;
            sqlParameter[3].Value = idNum;

            return DBHelper.ExecuteQueryDT(sqlText, sqlParameter);
        }

        /// <summary>
        /// 保存结果(saveRstPara索引为0到21)
        /// </summary>
        /// <param name="saveRstPara"></param>
        /// <returns></returns>
        public int saveResult(List<Object> saveRstPara)
        {
            LinkedList<String> sqlList = new LinkedList<string>();
            StringBuilder sqltext = new StringBuilder();
            int lines = 0;

            DateTime creatTime = Convert.ToDateTime(saveRstPara[21]);
            String creatTimeStr =creatTime.ToString("yyyyMMdd");

            sqltext.Append("INSERT INTO [Supplier_Performance]")
                .Append(" ([Supplier_ID]")
                .Append("  ,[PurchasingORG_ID]")
                .Append(" ,[Material_ID]")
                .Append("  ,[Total_Score]")
                .Append("  ,[Price_Score]")
                .Append("  ,[PriceLevel_Score]")
                .Append("  ,[PriceHistory_Score]")
                .Append("  ,[Quality_Score]")
                .Append(" ,[GoodReceipt_Score]")
                .Append(" ,[QualityAudit_Score]")
                .Append(" ,[ComplaintAndReject_Score]")
                .Append("  ,[Delivery_Score]")
                .Append(" ,[OnTimeDelivery_Score]")
                .Append(" ,[ConfirmDate_Score]")
                .Append(" ,[QuantityReliability_Score]")
                .Append(" ,[Shipment_Score]")
                .Append(" ,[GeneralServiceAndSupport_Score]")
                .Append(" ,[ExternalService_Score]")
                .Append(" ,[IsCulmunativeAssess]")
                .Append(" ,[Evaluation_Period]")
                .Append(" ,[Creator_Name]")
                .Append(" ,[Evaluation_Time] ) VALUES ('")
                .Append(saveRstPara[0].ToString() + "','" + saveRstPara[1].ToString() + "','")
                .Append(saveRstPara[2].ToString() + "'," + Convert.ToString(saveRstPara[3]) + "," + Convert.ToString(saveRstPara[4]) + ",")
                .Append(Convert.ToString(saveRstPara[5]) + "," + Convert.ToString(saveRstPara[6]) + "," + Convert.ToString(saveRstPara[7]) + "," + Convert.ToString(saveRstPara[8]) + ",")
                .Append(Convert.ToString(saveRstPara[9]) + "," + Convert.ToString(saveRstPara[10]) + "," + Convert.ToString(saveRstPara[11]) + "," + Convert.ToString(saveRstPara[12]) + ",")
                .Append(Convert.ToString(saveRstPara[13]) + "," + Convert.ToString(saveRstPara[14]) + "," + Convert.ToString(saveRstPara[15]) + "," + Convert.ToString(saveRstPara[16]) + ",")
                .Append(Convert.ToString(saveRstPara[17]) + ",'" + saveRstPara[18].ToString() + "', '" + saveRstPara[19].ToString() + "' , '" + saveRstPara[20].ToString() + "' ,")
                .Append("'"+ creatTimeStr.ToString() +"'")
                .Append(")");

            sqlList.AddLast(sqltext.ToString());
            lines = DBHelper.ExecuteNonQuery(sqlList);

            return lines;

        }

        /// <summary>
       /// 计算年度交易总额
       /// </summary>
       /// <param name="materialID"></param>
       /// <param name="supplierID"></param>
       /// <param name="year"></param>
       /// <returns></returns>
        public DataTable queryTransactionAmount(String materialID, String supplierID, String year)
        {
            String sqlText;
            DateTime startTime = Convert.ToDateTime(year + "-" + "1-1" + " 0:0:0");
            DateTime endTime = Convert.ToDateTime(year + "-" + "12-31" + " 23:59:59");

            String startTimeStr = startTime.ToString("yyyyMMdd");
            String endTimeStr = endTime.ToString("yyyyMMdd");
            sqlText = " SELECT convert(decimal,DemandCount) FROM RecordInfo WHERE MaterialId ='"+ materialID + "' AND SupplierId = '"+supplierID+"' AND CreateTime BETWEEN '"+ startTimeStr + "' AND '"+ endTimeStr + "'";

            return DBHelper.ExecuteQueryDT(sqlText);
        }

        /// <summary>
       /// 保存抱怨/拒绝相关记录（saveCRPara索引为0到8）
       /// </summary>
       /// <param name="saveCRPara"></param>
       /// <returns></returns>
        public int saveComplaintRejectRecord(List<Object> saveCRPara)
       {
           LinkedList<String> sqlList = new LinkedList<string>();
           StringBuilder sqltext = new StringBuilder();
           int lines = 0;
            DateTime creatTime = Convert.ToDateTime(saveCRPara[7]);
            String creatTimeStr = creatTime.ToString("yyyyMMdd");

           sqltext.Append("INSERT INTO [Complaint_Reject]")
               .Append(" ([Supplier_ID]")
               .Append("  ,[Material_ID]")
               .Append(" ,[PurchasingORG_ID]")
               .Append("  ,[Ratio]")
               .Append("  ,[Transaction_Amount]")
               .Append("  ,[Complaint_Reject_Cost]")
               .Append("  ,[Complaint_Reject_Count]")
               .Append("  ,[Create_Time]")
               .Append(" ,[Creator_Name] ) VALUES ('")
               .Append(Convert.ToString(saveCRPara[0]) + "','" + Convert.ToString(saveCRPara[1]) + "','")
               .Append(Convert.ToString(saveCRPara[2]) + "','" + Convert.ToString(saveCRPara[3]) + "','" + Convert.ToString(saveCRPara[4]) + "','")
               .Append(Convert.ToString(saveCRPara[5]) + "','" + Convert.ToString(saveCRPara[6]) + "', '" + creatTimeStr + "' ,")
               .Append("'" + Convert.ToString(saveCRPara[8]) + "' )");

           sqlList.AddLast(sqltext.ToString());
           lines = DBHelper.ExecuteNonQuery(sqlList);

           return lines;
       }

        /// <summary>
        /// 保存一般服务/支持记录（数据表Service_Support），saveSSPara索引为0到6
        /// </summary>
        /// <param name="saveSSPara"></param>
        /// <returns></returns>
        public int saveServiceSupportRecord(List<Object> saveSSPara)
       {
           LinkedList<String> sqlList = new LinkedList<string>();
           StringBuilder sqltext = new StringBuilder();
           int lines = 0;
            DateTime createTime = Convert.ToDateTime(saveSSPara[5]);
            String createTimeStr = createTime.ToString("yyyyMMdd");

           sqltext.Append("INSERT INTO [Service_Support] (")
               .Append(" [Supplier_ID] ")
               .Append(" ,[Material_ID] ")
               .Append(" ,[Innovation_Score] ")
               .Append("  ,[Reliability_Score] ")
               .Append(" ,[CustomerService_Score] ")
               .Append(" ,[Create_Time] ")
               .Append("  ,[Creator_Name]) VALUES( '")
               .Append(saveSSPara[0].ToString() + "' , '" + saveSSPara[1].ToString() + "' , '" + saveSSPara[2].ToString() + "' , '")
               .Append(saveSSPara[3].ToString() + "' , '" + saveSSPara[4].ToString() + "' , '" + createTimeStr + "' , '")
               .Append(saveSSPara[6].ToString() + "')");
              
           sqlList.AddLast(sqltext.ToString());
           lines = DBHelper.ExecuteNonQuery(sqlList);

           return lines;
       }

        /// <summary>
        /// 保存外部服务记录（数据表External_Service）,saveESPara索引为0到5
        /// </summary>
        /// <param name="saveESPara"></param>
        /// <returns></returns>
        public int saveExternalServiceRecord(List<Object> saveESPara)
       {
           LinkedList<String> sqlList = new LinkedList<string>();
           StringBuilder sqltext = new StringBuilder();
           int lines = 0;
            DateTime createTime = Convert.ToDateTime(saveESPara[4]);
            String createTimeStr = createTime.ToString("yyyyMMdd");

           sqltext.Append("INSERT INTO [External_Service] (")
               .Append(" [Supplier_ID] ")
               .Append(" ,[Material_ID] ")
               .Append(" ,[ServiceQuality_Score] ")
               .Append("  ,[TimelinessOfService_Score] ")
               .Append(" ,[Create_Time] ")
               .Append("  ,[Creator_Name]) VALUES( '")
               .Append(saveESPara[0].ToString() + "' , '" + saveESPara[1].ToString() + "' , '" + saveESPara[2].ToString() + "' , '")
               .Append(saveESPara[3].ToString() + "' , '" + createTimeStr + "' , '" + saveESPara[5].ToString() + "' ) ");

           sqlList.AddLast(sqltext.ToString());
           lines = DBHelper.ExecuteNonQuery(sqlList);

           return lines;
       }

        /// <summary>
        /// 保存质量审核-产品审核记录（数据表Product_Audit），savePAPara索引为0到10
        /// </summary>
        /// <param name="savePAPara"></param>
        /// <returns></returns>
        public int saveProductAuditRecord(List<Object> savePAPara)
       {
           LinkedList<String> sqlList = new LinkedList<string>();
           StringBuilder sqltext = new StringBuilder();
           int lines = 0;
            DateTime createTime = Convert.ToDateTime(savePAPara[9]);
            String createTimeStr = createTime.ToString("yyyyMMdd");

           sqltext.Append("INSERT INTO [Product_Audit] (")
               .Append(" [Supplier_ID] ")
               .Append(" ,[Material_ID] ")
               .Append(" ,[KeySample_Count] ")
               .Append("  ,[MainSample_Count] ")
               .Append(" ,[SecondarySample_Count] ")
               .Append(" ,[KeyDefect_Count] ")
               .Append(" ,[MainDefect_Count] ")
               .Append(" ,[SecondaryDefect_Count] ")
               .Append(" ,[ProductAudit_Score] ")
               .Append(" ,[Create_Time] ")
               .Append("  ,[Creator_Name]) VALUES( '")
               .Append(savePAPara[0].ToString() + "' , '" + savePAPara[1].ToString() + "' , ' " + savePAPara[2].ToString() + "' , '")
               .Append(savePAPara[3].ToString() + "' , ' " + savePAPara[4].ToString() + "' , ' " + savePAPara[5].ToString() + "' ,' ")
               .Append(savePAPara[6].ToString() + "' , ' " + savePAPara[7].ToString() + "' , ' " + savePAPara[8].ToString() + "' ,' ")
               .Append(createTimeStr + "' , ' " + savePAPara[10].ToString() + "' ) ");

           sqlList.AddLast(sqltext.ToString());
           lines = DBHelper.ExecuteNonQuery(sqlList);

           return lines;
       }

        /// <summary>
        /// 保存体系/过程审核记录
        /// </summary>
        /// <param name="savePSAuditPara"></param>
        /// <returns></returns>
        public int savePSAuditRecord(List<Object> savePSAuditPara,List<String> chapterInfo,List<Object> scoreList)
       {
           List<String> sqlList = new List<string>();
           StringBuilder sqltext = new StringBuilder();
           int lines = 0;
           String auditType;
           String insertScore;

           List<SqlParameter[]> sqlParamsList = new List<SqlParameter[]>();

           for (int i = 0; i < 14; i++)
           {
               if (i <= 5)
               {
                   auditType = "过程审核";
               }
               else
               {
                   auditType = "体系审核";
               }

               if(scoreList[i] == null)
               {
                   insertScore = "null";
               }
               else
               {
                   insertScore = "'"+scoreList[i].ToString()+"'";
               }

               //由于章节名称可能出现单引号，因此使用sql传递参数
               SqlParameter[] sqlParameter = new SqlParameter[]{
                new SqlParameter("@chapterName",SqlDbType.VarChar)
            };
               sqlParameter[0].Value = chapterInfo[i].ToString();
               sqlParamsList.Add(sqlParameter);

                DateTime createTime = Convert.ToDateTime(savePSAuditPara[2]);
                String createTimeStr = createTime.ToString("yyyyMMdd");
               sqltext.Clear();
               sqltext.Append("INSERT INTO [System_Process_Audit] (")
                  .Append(" [Supplier_ID] ")
                  .Append(" ,[Material_ID] ")
                  .Append(" ,[Audit_Type] ")
                  .Append(" ,[Chapter_Name] ")
                  .Append(" ,[Score] ")
                  .Append(" ,[Create_Time] ")
                  .Append("  ,[Creator_Name]) VALUES( '")
                  .Append(savePSAuditPara[0].ToString() + "' , '" + savePSAuditPara[1].ToString() + "' , '" + auditType + "' , @chapterName")
                  .Append( "  , " + insertScore + " , ' " +createTimeStr + "' ,' ")
                  .Append( savePSAuditPara[3].ToString() + "' ) ");
               sqlList.Add(sqltext.ToString());
           }
           lines = DBHelper.ExecuteNonQuery(sqlList, sqlParamsList);
           return lines;
       }

        /// <summary>
        /// 查询数据表Supplier_Base中的所有供应商ID
        /// </summary>
        /// <returns></returns>
        public DataTable querySupplier()
        {
            String sqlText;
            sqlText = "SELECT DISTINCT(Supplier_ID),Supplier_Name FROM Supplier_Base";
            return(DBHelper.ExecuteQueryDT(sqlText));
        }

        /// <summary>
        /// 查询数据表Supplier_Purchasing_Org中，特定供应商ID对应的采购组织ID
        /// </summary>
        /// <param name="supplierID"></param>
        /// <returns></returns>
        public DataTable queryPurchasingORG(String supplierID)
        {
            String sqlText;
            sqlText = "SELECT PurchasingORG_ID,PurchasingORG_Name FROM Supplier_Purchasing_Org WHERE Supplier_ID = '" + supplierID + "'";
            return (DBHelper.ExecuteQueryDT(sqlText));
        }

        public DataTable queryPurchasingORG()
        {
            String sqlText;
            sqlText = "SELECT DISTINCT(PurchasingORG_ID),PurchasingORG_Name FROM Supplier_Purchasing_Org ";
            return (DBHelper.ExecuteQueryDT(sqlText));
        }

        /// <summary>
        /// ，特定供应商ID对应的物料ID
        /// </summary>
        /// <param name="supplierID"></param>
        /// <returns></returns>
        public DataTable queryMaterial(String supplierID)
        {
            String sqlText;
            sqlText = "SELECT DISTINCT RecordInfo.MaterialId AS Material_ID, Material.Material_Name AS Material_Name FROM RecordInfo, Material WHERE SupplierId = '" + supplierID + "' AND Material.Material_ID = RecordInfo.MaterialId ";
            return (DBHelper.ExecuteQueryDT(sqlText));
        }

        /// <summary>
        /// 查询无评估供应商
        /// </summary>
        /// <param name="timePeriod"></param>
        /// <returns></returns>
        public DataTable queryNoSupplierPerformanceList(DateTime startTime,DateTime endTime)
        {
            String sqlText;
            sqlText = "SELECT Supplier_ID,Supplier_Name FROM Supplier_Base WHERE NOT EXISTS (SELECT* FROM Supplier_Performance WHERE Supplier_Base.Supplier_ID = Supplier_Performance.Supplier_ID AND Evaluation_Period   BETWEEN '"+startTime+"' And ' "+endTime+" ')";
            return (DBHelper.ExecuteQueryDT(sqlText));
        }

        /// <summary>
        /// 查询物料对应的物料类型
        /// </summary>
        /// <param name="materialID"></param>
        /// <returns></returns>
        public String queryMaterialType(String materialID)
        {
            String sqlText;
            DataTable resultTable;
            sqlText = "SELECT b.isRowMaterial from Material as m , Bigclassfy as b where m.Material_Type = b.ClassfyID and m.Material_ID = '" + materialID + "'";
            resultTable = DBHelper.ExecuteQueryDT(sqlText);
            if( resultTable.Rows[0][0]== null || string.IsNullOrEmpty(resultTable.Rows[0][0].ToString()))
            {
                return "0";
            }
            return resultTable.Rows[0][0].ToString();
        }

        /// <summary>
        /// 查询某一个物料类型对应的所有物料列表
        /// </summary>
        /// <param name="materialType"></param>
        /// <returns></returns>
        public DataTable queryMaterialList(String materialType)
        {
            String sqlText = "SELECT DISTINCT Material_ID FROM Material WHERE Material_Type = '"+materialType+"'";
            return(DBHelper.ExecuteQueryDT(sqlText));
        }
        #endregion

        #region 供应商分级

        /// <summary>
        /// 查询供应商在指定年度内的总分的平均值
        /// </summary>
        /// <param name="supplierID"></param>
        /// <param name="year"></param>
        /// <returns></returns>
        public DataTable queryTotalResult(String supplierID, DateTime start,DateTime end)
        {
            String sqlText;
            sqlText = "SELECT AVG(Total_Score) FROM Supplier_Performance WHERE Supplier_ID = '" + supplierID + "'AND  Evaluation_Period  between '" + start  + "' AND '"+ end + "'";

            return (DBHelper.ExecuteQueryDT(sqlText));
        }

        /// <summary>
        /// 保存供应商分级结果
        /// </summary>
        /// <param name="saveClassifyRst"></param>
        /// <returns></returns>
        public int saveClassificationResult(List<Object> saveClassifyRst)
        {
            StringBuilder sqltext = new StringBuilder();
            int lines = 0;
            DateTime createTime = Convert.ToDateTime(saveClassifyRst[3]);
            String createTimeStr = createTime.ToString("yyyyMMdd");

            sqltext.Append("INSERT INTO Supplier_Classification ")
                .Append(" (Supplier_ID,Classification_Result,[Year],Create_Time,Creator_Name,Classify_Period) ")
                .Append(" VALUES( '")
                .Append(saveClassifyRst[0].ToString() + "' , '" + saveClassifyRst[1].ToString() + "' , '" + saveClassifyRst[2].ToString() + "' , '")
                .Append(createTimeStr + "' , '" + saveClassifyRst[4].ToString() + "' , '" + saveClassifyRst[5].ToString()  +"' )");

            lines = DBHelper.ExecuteNonQuery(sqltext.ToString());

            return lines;
        }

        /// <summary>
        /// 查看供应商分级结果
        /// </summary>
        /// <param name="supplierID"></param>
        /// <param name="year"></param>
        /// <returns></returns>
        public DataTable queryClassificationResult(String supplierID, String year)
        {
            String sqlText;
            sqlText = "SELECT Supplier_ID,Classification_Result,Year,Create_Time,Creator_Name FROM Supplier_Classification WHERE Supplier_ID = '"+supplierID+"' AND [Year] = '"+year+"'";
            return (DBHelper.ExecuteQueryDT(sqlText));
        }

        /// <summary>
        /// 返回供应商分级最新的结果
        /// </summary>
        /// <param name="supplierID"></param>
        /// <param name="year"></param>
        /// <returns></returns>
        public String queryNewClassificationResult(String supplierID, String timePeriod)
        {
            String sqlText = "SELECT TOP 1 Classification_Result FROM Supplier_Classification where Supplier_ID = '"+supplierID+ "' AND year = '" + timePeriod+"' ORDER BY Create_Time DESC";
            DataTable rstTable = DBHelper.ExecuteQueryDT(sqlText);
            String rst = "101";
            if (rstTable.Rows.Count > 0)
            {
                //只有一行查询结果，因为是top1
                foreach (DataRow row in rstTable.Rows)
                {
                    rst = row[0].ToString();
                }
                return rst;
            }
            else
            {
                //返回101即无相关结果
                return rst;
            }
            
        }

        /// <summary>
        /// 分组查询供应商分级结果
        /// </summary>
        /// <param name="purchasingORGID"></param>
        /// <param name="category"></param>
        /// <param name="year"></param>
        /// <returns></returns>
        public DataTable queryClassificationGroupResult(String purchasingORGID, String category, String year)
        {
            String sqlText = "SELECT Supplier_Classification.Supplier_ID,Classification_Result,Supplier_Classification.[Year],Supplier_Classification.Classify_Period FROM Supplier_Classification,Supplier_Base WHERE Supplier_Classification.[Year] = '"+year+"' AND Supplier_Classification.Supplier_ID=Supplier_Base.Supplier_ID AND Supplier_Base.Supplier_Class='"+category+"' ";
            return (DBHelper.ExecuteQueryDT(sqlText));
        }
        #endregion

        #region 供应商批量评估

        /// <summary>
        /// 查询所有物料ID
        /// </summary>
        /// <returns></returns>
        public DataTable queryMaterial()
        {
            String sqlText;
            sqlText = "SELECT DISTINCT(Material_ID),Material_Name FROM Material";
            return (DBHelper.ExecuteQueryDT(sqlText));
        }

        /// <summary>
        /// 查询跟该物料有过交易的供应商
        /// </summary>
        /// <param name="materialID"></param>
        /// <returns></returns>
        public DataTable querySupplier(String materialID)
        {
            String sqlText;
            sqlText = "SELECT DISTINCT(Supplier_ID) FROM Enquiry WHERE Material_ID = '"+materialID+"' AND State = '已完成' ";
            return (DBHelper.ExecuteQueryDT(sqlText));
        }

        /// <summary>
        /// 查询所有供应商类别
        /// </summary>
        /// <returns></returns>
        public DataTable queryCategory()
        {
            String sqlText;
            sqlText = "SELECT DISTINCT(Supplier_Class) FROM Supplier_Base";
            return (DBHelper.ExecuteQueryDT(sqlText));
        }

        #endregion

        #region 数据录入

        /// <summary>
        /// 将问卷结果保存到数据表GoodsReceiveSurvey_Score中
        /// </summary>
        /// <param name="saveList"></param>
        /// <returns></returns>
        public int saveQuestionaire(List<Object> saveList)
        {
            StringBuilder sqlText = new StringBuilder();
            DateTime createTime = Convert.ToDateTime(saveList[3]);
            String createTimeStr = createTime.ToString("yyyyMMdd");

            sqlText.Append("INSERT INTO GoodsReceiveSurvey_Score(Supplier_ID,Material_ID,")
                .Append(" Questionaire_Score,Create_Time,Creator_Name)")
                .Append(" VALUES ('" + Convert.ToString(saveList[0]) + "','" + Convert.ToString(saveList[1]) + "'")
                .Append(" ,'" + Convert.ToString(saveList[2]) + "','" + createTimeStr+ "','" + Convert.ToString(saveList[4]) + "')");
            int lines;
            lines = DBHelper.ExecuteNonQuery(sqlText.ToString());
            return lines;
        }

        #endregion

        /// <summary>
        /// 查询供应商名称
        /// </summary>
        /// <param name="supplierID"></param>
        /// <returns></returns>
        public String querySupplierName(String supplierID)
        {
            String sqlText = "select Supplier_Name from Supplier_Base where Supplier_ID = '"+supplierID+"'";
            DataTable rst = DBHelper.ExecuteQueryDT(sqlText);
            return (rst.Rows[0][0].ToString());
        }

        /// <summary>
        /// 查询物料名称
        /// </summary>
        /// <param name="materialID"></param>
        /// <returns></returns>
        public String queryMaterialName(String materialID)
        {
            String sqlText = "select Material_Name from Material where Material_ID = '" + materialID + "'";
            DataTable rst = DBHelper.ExecuteQueryDT(sqlText);
            return (rst.Rows[0][0].ToString());
        }

        /// <summary>
        /// 从Supplier_Performance表中读取记录ID对应的物料ID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public String queryMaterialIDFromSP(String ID)
        {
            String sqlText = "SELECT Material_ID FROM Supplier_Performance WHERE ID = '"+ID+"'";
            DataTable rst = DBHelper.ExecuteQueryDT(sqlText);
            return (rst.Rows[0][0].ToString());
        }

        /// <summary>
        /// 从Supplier_Performance表中读取记录ID对应的供应商ID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public String querySupplierIDFromSP(String ID)
        {
            String sqlText = "SELECT Supplier_ID FROM Supplier_Performance WHERE ID = '" + ID + "'";
            DataTable rst = DBHelper.ExecuteQueryDT(sqlText);
            return (rst.Rows[0][0].ToString());
        }

        /// <summary>
        /// 从Supplier_Performance表中读取记录ID对应的采购组织ID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public String queryPurchasingORGIDFromSP(String ID)
        {
            String sqlText = "SELECT PurchasingORG_ID FROM Supplier_Performance WHERE ID = '" + ID + "'";
            DataTable rst = DBHelper.ExecuteQueryDT(sqlText);
            return (rst.Rows[0][0].ToString());
        }
        /// <summary>
        /// 获取绩效绿色目标和最低目标
        /// </summary>
        /// <returns></returns>
        public DataTable getPGreenValueAndPLowValue(string supplierId) {
            //查询供应商所选物料组
            /* string sqlText = "select SelectedMType from SR_Info WHERE SupplierId='"+ supplierId + "'";
             DataTable dt = DBHelper.ExecuteQueryDT(sqlText);
             if (dt.Rows.Count==0) {
                 return null;
             }
             string[] mtGroupName = DBHelper.ExecuteQueryDT(sqlText).Rows[0][0].ToString().Split(',');*/

            //查询物料组对应的绿色目标和最低目标
            string sqlText = @"	select PGoalValue as PGreenValue,LowValue as PLowValue from TabGoalAndLowVal";
           DataTable dt = DBHelper.ExecuteQueryDT(sqlText);
            if (dt.Rows.Count == 0) {
                return null;
            }
            return dt;
        }



    } 
}
