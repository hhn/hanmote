﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using System.Data.SqlClient;
using Lib.IDAL.SupplierPerformaceIDAL;
using Lib.ContionSettings.SupplierPerformaceCS.SPReportCS;

namespace Lib.SqlServerDAL
{
    public class SPR_SPCompareBOMGDAL:SPR_BaseDAL,SPR_SPCompareBOMGIDAL
    {
        /// <summary>
        /// 取得物料组信息
        /// </summary>
        /// <param name="selectSupplierConditionSettings">查询条件</param>
        /// <returns></returns>
        public DataTable getMaterialGroupInfo(SelectSupplierConditionSettings selectSupplierConditionSettings)
        {
            //编写SQL语句文本
            string sqlText = @"SELECT DISTINCT a.Material_Group AS MaterialGroupId,a.Description AS MaterialGroupName  FROM Material_Group a INNER JOIN Material b ON a.Material_Group=b.Material_Group INNER JOIN Supplier_Performance c ON b.Material_ID=c.Material_ID WHERE c.Evaluation_Period=@Evaluation_Period";
            //设置参数
            SqlParameter[] sqlParas = new SqlParameter[] { 
                new SqlParameter("@Evaluation_Period",SqlDbType.VarChar)
            };

            //为参数赋值
            string evaluationPeriod = selectSupplierConditionSettings.Year + "年" + selectSupplierConditionSettings.Month;
            sqlParas[0].Value = evaluationPeriod;

            //执行查询操作并返回结果集
            return DBHelper.ExecuteQueryDT(sqlText, sqlParas);
        }

        /// <summary>
        /// 基于物料组的供应商评估比较
        /// </summary>
        /// <param name="spcCondition">查询条件</param>
        /// <returns></returns>
        public DataTable getEvaluationSupplierScore(SPCompareBaseOnMGConditionValue spcCondition)
        {
            //编写SQL语句文本
            //嵌套查询
            StringBuilder sqlText = new StringBuilder("");
            sqlText.Append("    SELECT tb.Supplier_ID,d.Supplier_Name, tb.Price_Score,tb.Delivery_Score, tb.Quality_Score,tb.GeneralServiceAndSupport_Score ");
            sqlText.Append("    FROM  (");
            sqlText.Append("                    SELECT a.Supplier_ID, AVG(a.Price_Score) Price_Score,AVG(a.Delivery_Score) Delivery_Score,AVG(a.Quality_Score) Quality_Score,AVG(a.GeneralServiceAndSupport_Score) GeneralServiceAndSupport_Score");
            sqlText.Append("                    FROM Supplier_Performance a INNER JOIN Supplier_Base b ON a.Supplier_ID=b.Supplier_ID INNER JOIN Material c ON a.Material_ID=c.Material_ID ");
            sqlText.Append("                    WHERE  a.PurchasingORG_ID=@PurchasingORG_ID   AND a.Evaluation_Period=@Evaluation_Period AND c.Material_Group=@MaterialGroupId");
            sqlText.Append("                    GROUP BY a.Supplier_ID) tb");
            sqlText.Append("    INNER JOIN Supplier_Base d ON tb.Supplier_ID=d.Supplier_ID");

            //设置参数
            SqlParameter[] sqlParas = new SqlParameter[] { 
                new SqlParameter("@PurchasingORG_ID",SqlDbType.VarChar),
                new SqlParameter("@Evaluation_Period",SqlDbType.VarChar),
                new SqlParameter("@MaterialGroupId",SqlDbType.VarChar)
            };

            //为参数赋值
            sqlParas[0].Value = spcCondition.SelectSupplierConditionSettings.PurchaseId;
            string evaluationPeriod = spcCondition.SelectSupplierConditionSettings.Year + "年" + spcCondition.SelectSupplierConditionSettings.Month;
            sqlParas[1].Value = evaluationPeriod;
            sqlParas[2].Value = spcCondition.MaterialGroupId;

            //执行查询操作并返回结果集
            return DBHelper.ExecuteQueryDT(sqlText.ToString(), sqlParas);
        }
    }
}
