﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using System.Data.SqlClient;
using Lib.IDAL.SupplierPerformaceIDAL;
using Lib.Model.SPReportModel;
using Lib.ContionSettings.SupplierPerformaceCS.SPReportCS;

namespace Lib.SqlServerDAL
{
    public class SPR_SPCompareDAL:SPR_BaseDAL,SPR_SPCompareIDAL
    {
        /// <summary>
        /// 查询物料信息
        /// </summary>
        /// <param name="selectMaterialConditionSettings">查询条件</param>
        /// <returns></returns>
        public DataTable getAllMaterialInfoBySelectMaterialConditon(SelectMaterialConditionSettings selectMaterialConditionSettings)
        {
            //编写SQL语句文本
            StringBuilder sqlText = new StringBuilder("");
            sqlText.Append("    SELECT  a.Material_ID,b.Material_Name");
            sqlText.Append("    FROM  Supplier_Performance a INNER JOIN Material b ON a.Material_ID=b.Material_ID");
            sqlText.Append("    WHERE  a.PurchasingORG_ID=@PurchasingORG_ID AND a.Supplier_ID=@Supplier_ID  AND a.Evaluation_Period=@Evaluation_Period ");

            //设置参数
            SqlParameter[] sqlParas = new SqlParameter[] { 
                new SqlParameter("@PurchasingORG_ID",SqlDbType.VarChar),
                new SqlParameter("@Supplier_ID",SqlDbType.VarChar),
                new SqlParameter("@Evaluation_Period",SqlDbType.VarChar)
            };

            //为参数赋值
            sqlParas[0].Value = selectMaterialConditionSettings.SelectSupplierConditionSettings.PurchaseId;
            sqlParas[1].Value = selectMaterialConditionSettings.SupplierId;
            string evaluationPeriod = selectMaterialConditionSettings.SelectSupplierConditionSettings.Year + "年" + selectMaterialConditionSettings.SelectSupplierConditionSettings.Month;
            sqlParas[2].Value = evaluationPeriod;

            //执行查询操作并返回结果集
            return DBHelper.ExecuteQueryDT(sqlText.ToString(), sqlParas);
        }


        /// <summary>
        /// 获得物料评分结果
        /// </summary>
        /// <param name="sPCompareConditionValue">查询条件</param>
        /// <returns></returns>
        public DataTable getMaterialEvaluatedScore(SPCompareConditionValue sPCompareConditionValue)
        {
            //编写SQL语句文本
            StringBuilder sqlText = new StringBuilder("");
            sqlText.Append("    SELECT Total_Score,Price_Score,PriceLevel_Score,PriceHistory_Score, Quality_Score,GoodReceipt_Score,QualityAudit_Score,ComplaintAndReject_Score,Delivery_Score,OnTimeDelivery_Score,ConfirmDate_Score,QuantityReliability_Score,Shipment_Score,GeneralServiceAndSupport_Score,ExternalService_Score,Creator_Name,Evaluation_Time ");
            sqlText.Append("    FROM  Supplier_Performance");
            sqlText.Append("    WHERE  PurchasingORG_ID=@PurchasingORG_ID AND Supplier_ID=@Supplier_ID  AND Evaluation_Period=@Evaluation_Period AND Material_ID=@Material_ID ");

            //设置参数
            SqlParameter[] sqlParas = new SqlParameter[] { 
                new SqlParameter("@PurchasingORG_ID",SqlDbType.VarChar),
                new SqlParameter("@Supplier_ID",SqlDbType.VarChar),
                new SqlParameter("@Evaluation_Period",SqlDbType.VarChar),
                new SqlParameter("@Material_ID",SqlDbType.VarChar)
            };

            //为参数赋值
            sqlParas[0].Value = sPCompareConditionValue.PurchaseId;
            sqlParas[1].Value = sPCompareConditionValue.SupplierId;
            string evaluationPeriod = sPCompareConditionValue.Year + "年" + sPCompareConditionValue.Month;
            sqlParas[2].Value = evaluationPeriod;
            sqlParas[3].Value = sPCompareConditionValue.MaterialId;

            //执行查询操作并返回结果集
            return DBHelper.ExecuteQueryDT(sqlText.ToString(), sqlParas);
        }

        /// <summary>
        /// 获得供应商评分结果
        /// </summary>
        /// <param name="sPCompareConditionValue">查询条件</param>
        /// <returns></returns>
        public DataTable getSupplierEvaluatedScore(SPCompareConditionValue sPCompareConditionValue)
        {
            //编写SQL语句文本
            StringBuilder sqlText = new StringBuilder("");
            sqlText.Append("    SELECT AVG(Total_Score) AS Total_Score,AVG(Price_Score) AS Price_Score,AVG(PriceLevel_Score) AS PriceLevel_Score,AVG(PriceHistory_Score) AS PriceHistory_Score,AVG(Quality_Score) AS Quality_Score,AVG(GoodReceipt_Score) AS GoodReceipt_Score,AVG(QualityAudit_Score) AS QualityAudit_Score,AVG(ComplaintAndReject_Score) AS ComplaintAndReject_Score,AVG(Delivery_Score) AS Delivery_Score,AVG(OnTimeDelivery_Score) AS OnTimeDelivery_Score,AVG(ConfirmDate_Score) AS ConfirmDate_Score,AVG(QuantityReliability_Score) AS QuantityReliability_Score,AVG(Shipment_Score) AS Shipment_Score,AVG(GeneralServiceAndSupport_Score) AS GeneralServiceAndSupport_Score,AVG(ExternalService_Score) AS ExternalService_Score ");
            sqlText.Append("    FROM  Supplier_Performance");
            sqlText.Append("    WHERE  PurchasingORG_ID=@PurchasingORG_ID AND Supplier_ID=@Supplier_ID  AND Evaluation_Period=@Evaluation_Period ");

            //设置参数
            SqlParameter[] sqlParas = new SqlParameter[] { 
                new SqlParameter("@PurchasingORG_ID",SqlDbType.VarChar),
                new SqlParameter("@Supplier_ID",SqlDbType.VarChar),
                new SqlParameter("@Evaluation_Period",SqlDbType.VarChar)
            };

            //为参数赋值
            sqlParas[0].Value = sPCompareConditionValue.PurchaseId;
            sqlParas[1].Value = sPCompareConditionValue.SupplierId;
            string evaluationPeriod = sPCompareConditionValue.Year + "年" + sPCompareConditionValue.Month;
            sqlParas[2].Value = evaluationPeriod;

            //执行查询操作并返回结果集
            return DBHelper.ExecuteQueryDT(sqlText.ToString(), sqlParas);
        }

    }
}
