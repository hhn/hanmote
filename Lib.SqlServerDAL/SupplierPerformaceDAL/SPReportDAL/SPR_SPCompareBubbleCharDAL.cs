﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using System.Data.SqlClient;
using Lib.IDAL.SupplierPerformaceIDAL;
using Lib.Model.SPReportModel;
using Lib.ContionSettings.SupplierPerformaceCS.SPReportCS;

namespace Lib.SqlServerDAL
{
    public class SPR_SPCompareBubbleCharDAL : SPR_BaseDAL, SPR_SPCompareBubbleChartIDAL
    {
        /// <summary>
        /// 查询供应商列表
        /// 条件
        /// 采购组织ID、年度、时段
        /// </summary>
        /// <param name="selectSupplierConditionSettings"></param>
        /// <returns></returns>
        public DataTable getAllSupplierInfo(SelectSupplierConditionSettings selectSupplierConditionSettings)
        {
            //编写SQL语句文本
            StringBuilder sqlText = new StringBuilder("");
            sqlText.Append("    SELECT DISTINCT a.Supplier_ID,b.Supplier_Name,c.SupplierIndustry_Id,b.Address ");
            sqlText.Append("    FROM Supplier_Performance a INNER JOIN Supplier_Base b ON a.Supplier_ID=b.Supplier_ID INNER JOIN Supplier_Industry c ON b.Industry =c.SupplierIndustry_Id");
            sqlText.Append("    WHERE  a.PurchasingORG_ID=@PurchasingORG_ID AND a.Evaluation_Period=@Evaluation_Period ");

            //设置参数
            SqlParameter[] sqlParas = new SqlParameter[] { 
                new SqlParameter("@PurchasingORG_ID",SqlDbType.VarChar),
                new SqlParameter("@Evaluation_Period",SqlDbType.VarChar)
            };

            //为参数赋值
            sqlParas[0].Value = selectSupplierConditionSettings.PurchaseId;
            string evaluationPeriod = selectSupplierConditionSettings.Year + "年" + selectSupplierConditionSettings.Month;
            sqlParas[1].Value = evaluationPeriod;

            //执行查询操作并返回结果集
            return DBHelper.ExecuteQueryDT(sqlText.ToString(), sqlParas);
        }
    }
}
