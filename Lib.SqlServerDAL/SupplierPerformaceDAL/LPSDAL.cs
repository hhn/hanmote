﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using Lib.SqlServerDAL;
using Lib.IDAL.SupplierPerformaceIDAL;

namespace Lib.SqlServerDAL.SupplierPerformaceDAL
{
    class LPSDAL:LPSIDAL
    {
        #region 供应商警告

        /// <summary>
        /// 根据5个主标准的分数，查询低效能供应商
        /// </summary>
        /// <param name="scoreThresholdList"></param>
        /// <param name="timePeriod"></param>
        /// <returns></returns>
        public DataTable queryLPSByScore(List<Decimal> scoreThresholdList,String timePeriod)
        {
           StringBuilder sqlText = new StringBuilder();
           sqlText.Append("SELECT ResultTable.Supplier_ID,Supplier_Base.Supplier_Name FROM")
               .Append(" (SELECT DISTINCT(Supplier_ID) FROM ")
               .Append(" (SELECT Supplier_ID,Price_Score,Quality_Score,Delivery_Score,GeneralServiceAndSupport_Score,")
               .Append(" ExternalService_Score FROM Supplier_Performance WHERE Evaluation_Period = '" + timePeriod + "') AS tb1")
               .Append(" WHERE (Price_Score < " + scoreThresholdList[0] + " AND Price_Score IS NOT NULL) OR (Quality_Score < " + scoreThresholdList[1] + " AND Quality_Score IS NOT NULL) ")
               .Append(" OR (Delivery_Score < " + scoreThresholdList[2] + " AND Delivery_Score IS NOT NULL) ")
               .Append(" OR (GeneralServiceAndSupport_Score <" + scoreThresholdList[3] + " AND GeneralServiceAndSupport_Score IS NOT NULL) ")
               .Append(" OR (ExternalService_Score < " + scoreThresholdList[4] + " AND ExternalService_Score IS NOT NULL)")
               .Append(" ) AS ResultTable ,Supplier_Base")
               .Append(" WHERE ResultTable.Supplier_ID = Supplier_Base.Supplier_ID ");

            return (DBHelper.ExecuteQueryDT(sqlText.ToString()));
        }

        /// <summary>
        /// 根据供应商分级，查询进入低效能供应商流程的供应商
        /// </summary>
        /// <param name="year"></param>
        /// <returns></returns>
        public DataTable queryLPSByClassification(String year)
        {
            StringBuilder sqlText = new StringBuilder();
            sqlText.Append(" SELECT TMP2.Supplier_ID,Supplier_Base.Supplier_Name FROM ")
                .Append(" (SELECT * FROM(")
                .Append(" SELECT *, ROW_NUMBER()")
                .Append("  OVER (partition BY Supplier_ID ORDER BY Create_Time DESC)")
                .Append(" AS rownum")
                .Append(" FROM Supplier_Classification WHERE [Year] = '" + year + "'")
                .Append(" )AS TMP1")
                .Append(" WHERE rownum <=1")
                .Append(" ) AS TMP2")
                .Append(" , Supplier_Base")
                .Append(" WHERE (TMP2.Classification_Result = 'C' OR TMP2.Classification_Result = 'D')")
                .Append("  AND Supplier_Base.Supplier_ID = TMP2.Supplier_ID ");
            return (DBHelper.ExecuteQueryDT(sqlText.ToString()));
        }

        /// <summary>
        /// 查询是否已经存在LPS记录
        /// </summary>
        /// <param name="supplierID"></param>
        /// <returns></returns>
        public bool queryRecordOrNot(String supplierID)
        {
            String sqlText = "SELECT * FROM Low_Performance_Supplier WHERE Supplier_ID = '" + supplierID + "'";
            DataTable rst = DBHelper.ExecuteQueryDT(sqlText);
            if (rst.Rows.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 记录LPS流程状态
        /// </summary>
        /// <param name="supplierID"></param>
        /// <param name="LPSState"></param>
        /// <param name="recordTime"></param>
        /// <param name="recordName"></param>
        /// <returns></returns>
        public int saveLPSState(String supplierID, String LPSState, DateTime recordTime, String recordName)
        {
            StringBuilder sqltext = new StringBuilder();
            int lines = 0;
            String recordTimeStr = recordTime.ToString("yyyyMMdd");
            sqltext.Append("INSERT INTO Low_Performance_Supplier(Supplier_ID,LPS_Degree,Record_Time,Record_Name) ")
                .Append(" VALUES('"+supplierID+"','"+LPSState+"','"+recordTimeStr+"','"+recordName+"')");
            lines = DBHelper.ExecuteNonQuery(sqltext.ToString());

            return lines;
        }

        /// <summary>
        /// 查询LPS流程中的所有供应商
        /// </summary>
        /// <returns></returns>
        public DataTable queryLPSSupplier()
        {
            String sqlText;
            sqlText = "SELECT DISTINCT(Low_Performance_Supplier.Supplier_ID),Supplier_Base.Supplier_Name FROM Low_Performance_Supplier,Supplier_Base WHERE Low_Performance_Supplier.Supplier_ID = Supplier_Base.Supplier_ID";
            return (DBHelper.ExecuteQueryDT(sqlText));
        }

        /// <summary>
        /// 查询LPS流程状态
        /// </summary>
        /// <param name="SupplierID"></param>
        /// <returns></returns>
        public String queryLPSDegree(String supplierID)
        {
            String sqlText;
            sqlText = "SELECT TOP 1 LPS_Degree FROM Low_Performance_Supplier WHERE Supplier_ID = '"+supplierID+"' ORDER BY Record_Time DESC";
            DataTable dt = DBHelper.ExecuteQueryDT(sqlText);
            String lpsDegree;
            try
            {
                lpsDegree = dt.Rows[0][0].ToString();
            }
            catch
            {
                lpsDegree = "";
            }
            return (lpsDegree);
        }

        /// <summary>
        /// 保存关键指数原因及陈述
        /// </summary>
        /// <param name="suppler"></param>
        /// <param name="kpiList"></param>
        /// <param name="statement"></param>
        /// <param name="time"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public int saveKPIStatement(String supplierID, List<String> kpiList, String statement, DateTime time, String name)
        {
            StringBuilder kpiString = new StringBuilder();
            int count = kpiList.Count();
            for(int i = 0;i < count;i++)
            {
                kpiString.Append(kpiList[i]+";");
            }

            StringBuilder sqltext = new StringBuilder();
            int lines = 0;
            String timeStr = time.ToString("yyyyMMdd");
            sqltext.Append("INSERT INTO LPS_KPI_Statement(Supplier_ID,KPI,Statement,Record_Time,Record_Name) ")
                .Append("VALUES ('"+supplierID+"','"+kpiString+"','"+statement+"','"+ timeStr + "','"+name+"')");

            try
            {
                lines = DBHelper.ExecuteNonQuery(sqltext.ToString());
            }
            catch
            {
                return -1;
            }

            return lines;
        }
        #endregion

        #region LPS三级流程

        /// <summary>
        /// 查询供应商在特定时间范围的绩效表现
        /// </summary>
        /// <param name="supplierID"></param>
        /// <param name="timePeriod"></param>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <returns></returns>
        public DataTable querySupplierPerformance(String supplierID, String timePeriod, DateTime startTime, DateTime endTime)
        {
            String startTimeStr = startTime.ToString("yyyyMMdd");
            String endTimeStr = endTime.ToString("yyyyMMdd");
            StringBuilder sqlText = new StringBuilder();
            sqlText.Append("SELECT AVG(Price_Score) AS Price_Score,AVG(Quality_Score) AS Quality_Score, ")
                .Append(" AVG(Delivery_Score) AS Delivery_Score,AVG(GeneralServiceAndSupport_Score) AS ServiceSupport_Score")
                .Append(" ,AVG(ExternalService_Score) AS ExternalService_Score ,AVG(Total_Score) AS Total_Score")
                .Append(" FROM Supplier_Performance")
                .Append(" WHERE Supplier_ID = '" + supplierID + "' AND Evaluation_Period = '" + timePeriod + "' ")
                .Append(" AND Evaluation_Time BETWEEN '" + startTimeStr + "' AND '" + endTimeStr + "'")
                .Append(" GROUP BY Supplier_ID");
            return (DBHelper.ExecuteQueryDT(sqlText.ToString()));
        }

        /// <summary>
        /// 退出LPS流程，即在数据表Low_Performance_Supplier中删除记录
        /// </summary>
        /// <param name="supplierID"></param>
        /// <returns></returns>
        public int deleteLPSRecord(String supplierID)
        {
            String sqlText = "DELETE FROM Low_Performance_Supplier WHERE Supplier_ID = '"+supplierID+"'";
            return (DBHelper.ExecuteNonQuery(sqlText));
        }

        /// <summary>
        /// 更新LPS状态
        /// </summary>
        /// <param name="supplierID"></param>
        /// <param name="state"></param>
        /// <returns></returns>
        public int updateLPSState(String supplierID, String state)
        {
            String sqlText = "UPDATE Low_Performance_Supplier SET LPS_Degree = '"+state+"' WHERE Supplier_ID = '"+supplierID+"'";
            int lines = DBHelper.ExecuteNonQuery(sqlText);
            return lines;
        }

        /// <summary>
        /// 添加至待淘汰供应商列表
        /// </summary>
        /// <param name="supplierID"></param>
        /// <param name="outIdentity"></param>
        /// <param name="time"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public int saveLPSOutList(String supplierID, String outIdentity, DateTime time, String name)
        {
            StringBuilder sqltext = new StringBuilder();
            int lines = 0;
            String timeStr = time.ToString("yyyyMMdd");
            sqltext.Append("INSERT INTO LPS_Out_List(Supplier_ID,Record_Time,Record_Name) ")
                .Append("  VALUES('"+supplierID+"','"+timeStr+"','"+name+"')");
            lines = DBHelper.ExecuteNonQuery(sqltext.ToString());

            return lines;
        }

        /// <summary>
        /// 检查该供应商是否已经在LPS_Out_List数据表中有记录
        /// </summary>
        /// <param name="supplierID"></param>
        /// <returns></returns>
        public int checkLPSOutList(String supplierID)
        {
            StringBuilder sqltext = new StringBuilder();
            DataTable rst = new DataTable();

            sqltext.Append("SELECT * FROM LPS_Out_List WHERE Supplier_ID = '"+supplierID+"'");
            rst = DBHelper.ExecuteQueryDT(sqltext.ToString());
            int count;
            try
            {
                count = rst.Rows.Count;
            }
            catch
            {
                count = 0;
            }
            return count;
        }

      #endregion

        #region 供应商淘汰

        /// <summary>
        /// 查询待淘汰的供应商列表
        /// </summary>
        /// <returns></returns>
        public DataTable queryOutSupplier()
        {
            String sqlText;
            sqlText = "SELECT DISTINCT(LPS_Out_List.Supplier_ID),Supplier_Base.Supplier_Name FROM LPS_Out_List,Supplier_Base WHERE Supplier_Base.Supplier_ID = LPS_Out_List.Supplier_ID";
            return (DBHelper.ExecuteQueryDT(sqlText));
        }

        /// <summary>
        /// 淘汰供应商(将供应商从LPS_Out_List中删除)
        /// </summary>
        /// <param name="supplierID"></param>
        /// <returns></returns>
        public int deleteLPSupplier(String supplierID)
        {
            String sqlText;
            sqlText = "DELETE FROM LPS_Out_List WHERE Supplier_ID ='"+supplierID+"'";
            return (DBHelper.ExecuteNonQuery(sqlText));
        }

        /// <summary>
        /// 在Source_List数据表中将供应商的状态改为淘汰
        /// </summary>
        /// <param name="supplierID"></param>
        /// <returns></returns>
        public int updateSupplierOutInSourceList(String supplierID)
        {
            String sqlText;
            sqlText = "UPDATE SourceList SET blk = '1'  WHERE Supplier_ID = '"+supplierID+"'";
            return (DBHelper.ExecuteNonQuery(sqlText));
        }

        /// <summary>
        /// 在Supplier_Base数据表中将供应商的状态改为淘汰
        /// </summary>
        /// <param name="supplierID"></param>
        /// <returns></returns>
        public int updateSupplierOutInSupplierBase(String supplierID)
        {
            String sqlText;
            sqlText = "UPDATE Supplier_Base SET Eliminate_Mark = 1 WHERE Supplier_ID = '"+supplierID+"'";
            return(DBHelper.ExecuteNonQuery(sqlText));
        }

        /// <summary>
        /// 在Eliminate_Supplier_Record数据表中记录淘汰供应商记录
        /// </summary>
        /// <param name="supplierID"></param>
        /// <param name="time"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public int insertEliminateSupplierRecord(String supplierID, String time, String name)
        {
            String sqlText;
            sqlText = "INSERT INTO Eliminate_Supplier_Record(Supplier_ID,Record_Time,Record_Name) VALUES ('"+supplierID+"','"+time+"','"+name+"')";
            return (DBHelper.ExecuteNonQuery(sqlText));
        }

        #endregion
    }
}
