﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.Model.PurchaseOrderExecution;
using System.Data;
using System.Data.SqlClient;
using Lib.IDAL.PurchaseOrderExecution;

namespace Lib.SqlServerDAL.PurchaseOrderExecution
{
    public class InvoiceDAL : InvoiceIDAL
    {
        /// <summary>
        /// 添加新的发票
        /// </summary>
        /// <param name="invoice"></param>
        /// <param name="itemList"></param>
        /// <returns></returns>
        public int addInvoice(Invoice invoice, List<Invoice_Item> itemList) {
            List<string> sqlList = new List<string>();
            List<SqlParameter[]> sqlParamsList = new List<SqlParameter[]>();

            #region 添加基本数据

            StringBuilder strBui = new StringBuilder("insert into Invoice (Invoice_ID, Invoice_Code, Invoice_Number, ")
                .Append("Certificate_Type, Certificate_Code, Invoice_MakeOut_Time, Create_Time, Payer_Name, Payer_Identify_Number, ")
                .Append("Recipient_Name, Recipient_Identify_Number, Payment_Type, Sum, Currency, Invoice_Maker, ")
                .Append("Creator, Status, Supplier_ID, Supplier_Name")
                .Append(") values(@Invoice_ID, @Invoice_Code, @Invoice_Number, ")
                .Append("@Certificate_Type, @Certificate_ID, @Invoice_MakeOut_Time, @Create_Time, @Payer_Name, @Payer_Identify_Number, ")
                .Append("@Recipient_Name, @Recipient_Identify_Number, @Payment_Type, @Sum, @Currency, @Invoice_Maker, ")
                .Append("@Creator, @Status, @Supplier_ID, @Supplier_Name")
                .Append(")");

            SqlParameter[] sqlParas = new SqlParameter[]{
                new SqlParameter("@Invoice_ID", SqlDbType.VarChar),
                new SqlParameter("@Invoice_Code", SqlDbType.VarChar),
                new SqlParameter("@Invoice_Number", SqlDbType.VarChar),
                new SqlParameter("@Certificate_Type", SqlDbType.VarChar),
                new SqlParameter("@Certificate_ID", SqlDbType.VarChar),
                new SqlParameter("@Invoice_MakeOut_Time", SqlDbType.DateTime),
                new SqlParameter("@Create_Time", SqlDbType.DateTime),
                new SqlParameter("@Payer_Name", SqlDbType.VarChar),
                new SqlParameter("@Payer_Identify_Number", SqlDbType.VarChar),
                new SqlParameter("@Recipient_Name", SqlDbType.VarChar),
                new SqlParameter("@Recipient_Identify_Number", SqlDbType.VarChar),
                new SqlParameter("@Payment_Type", SqlDbType.VarChar),
                new SqlParameter("@Sum", SqlDbType.Decimal),
                new SqlParameter("@Currency", SqlDbType.VarChar),
                new SqlParameter("@Invoice_Maker", SqlDbType.VarChar),
                new SqlParameter("@Creator", SqlDbType.VarChar),
                new SqlParameter("@Status", SqlDbType.VarChar),
                new SqlParameter("@Supplier_ID", SqlDbType.VarChar),
                new SqlParameter("@Supplier_Name", SqlDbType.VarChar)
            };
            sqlParas[0].Value = invoice.Invoice_ID;
            sqlParas[1].Value = invoice.Invoice_Code;
            sqlParas[2].Value = invoice.Invoice_Number;
            sqlParas[3].Value = invoice.Certificate_Type;
            sqlParas[4].Value = invoice.Certificate_ID;
            sqlParas[5].Value = invoice.Invoice_MakeOut_Time;
            sqlParas[6].Value = invoice.Create_Time;
            sqlParas[7].Value = invoice.Payer_Name;
            sqlParas[8].Value = invoice.Payer_Identify_Number;
            sqlParas[9].Value = invoice.Recipient_Name;
            sqlParas[10].Value = invoice.Recipient_Identify_Number;
            sqlParas[11].Value = invoice.Payment_Type;
            sqlParas[12].Value = invoice.Sum;
            sqlParas[13].Value = invoice.Currency;
            sqlParas[14].Value = invoice.Invoice_Maker;
            sqlParas[15].Value = invoice.Creator;
            sqlParas[16].Value = invoice.Status;
            sqlParas[17].Value = invoice.Supplier_ID;
            sqlParas[18].Value = invoice.Supplier_Name;

            sqlList.Add(strBui.ToString());
            sqlParamsList.Add(sqlParas);

            #endregion

            #region 添加发票项目

            foreach (Invoice_Item item in itemList) {
                strBui = new StringBuilder("insert into Invoice_Item (")
                    .Append("Invoice_ID, Material_ID, Material_Name, Unit_Price, Quantity, Sum, Currency")
                    .Append(") values (")
                    .Append("@Invoice_ID, @Material_ID, @Material_Name, @Unit_Price, @Quantity, @Sum, @Currency)");

                SqlParameter[] sqlParas2 = new SqlParameter[]{
                    new SqlParameter("@Invoice_ID", SqlDbType.VarChar),
                    new SqlParameter("@Material_ID", SqlDbType.VarChar),
                    new SqlParameter("@Material_Name", SqlDbType.VarChar),
                    new SqlParameter("@Unit_Price", SqlDbType.Decimal),
                    new SqlParameter("@Quantity", SqlDbType.Decimal),
                    new SqlParameter("@Sum", SqlDbType.Decimal),
                    new SqlParameter("@Currency", SqlDbType.VarChar)
                };
                sqlParas2[0].Value = item.Invoice_ID;
                sqlParas2[1].Value = item.Material_ID;
                sqlParas2[2].Value = item.Material_Name;
                sqlParas2[3].Value = item.Unit_Price;
                sqlParas2[4].Value = item.Quantity;
                sqlParas2[5].Value = item.Sum;
                sqlParas2[6].Value = item.Currency;

                sqlList.Add(strBui.ToString());
                sqlParamsList.Add(sqlParas2);
            }

            #endregion

            return DBHelper.ExecuteNonQuery(sqlList, sqlParamsList);
        }

        /// <summary>
        /// 删除发票信息
        /// </summary>
        /// <param name="invoiceID"></param>
        /// <returns></returns>
        public int deleteInvoice(string invoiceID) {
            LinkedList<string> sqlList = new LinkedList<string>();

            StringBuilder strBui = new StringBuilder("delete from Invoice where Invoice_ID = '")
                .Append(invoiceID).Append("'");
            sqlList.AddLast(strBui.ToString());
            strBui = new StringBuilder("delete from Invoice_Item where Invoice_ID = '")
                .Append(invoiceID).Append("'");
            sqlList.AddLast(strBui.ToString());

            return DBHelper.ExecuteNonQuery(sqlList);
        }

        /// <summary>
        /// 更新发票信息
        /// </summary>
        /// <param name="invoice"></param>
        /// <param name="itemList"></param>
        /// <returns></returns>
        public int updateInvoice(Invoice invoice, List<Invoice_Item> itemList) {
            List<string> sqlList = new List<string>();
            List<SqlParameter[]> sqlParamsList = new List<SqlParameter[]>();

            #region 更新基本数据

            StringBuilder strBui = new StringBuilder("update Invoice set Invoice_Code = @Invoice_Code, Invoice_Number = @Invoice_Number, ")
                .Append("Certificate_Type = @Certificate_Type, Certificate_Code = @Certificate_ID, Invoice_MakeOut_Time = @Invoice_MakeOut_Time, ")
                .Append("Payer_Name = @Payer_Name, Payer_Identify_Number = @Payer_Identify_Number, ")
                .Append("Recipient_Name = @Recipient_Name, Recipient_Identify_Number = @Recipient_Identify_Number, ")
                .Append("Payment_Type = @Payment_Type, Sum = @Sum, Currency = @Currency, Invoice_Maker = @Invoice_Maker, Status = @Status, ")
                .Append("Supplier_ID = @Supplier_ID, Supplier_Name = @Supplier_Name")
                .Append("   where Invoice_ID = @Invoice_ID");
                
            SqlParameter[] sqlParas = new SqlParameter[]{
                new SqlParameter("@Invoice_ID", SqlDbType.VarChar),
                new SqlParameter("@Invoice_Code", SqlDbType.VarChar),
                new SqlParameter("@Invoice_Number", SqlDbType.VarChar),
                new SqlParameter("@Certificate_Type", SqlDbType.VarChar),
                new SqlParameter("@Certificate_ID", SqlDbType.VarChar),
                new SqlParameter("@Invoice_MakeOut_Time", SqlDbType.DateTime),
                new SqlParameter("@Payer_Name", SqlDbType.VarChar),
                new SqlParameter("@Payer_Identify_Number", SqlDbType.VarChar),
                new SqlParameter("@Recipient_Name", SqlDbType.VarChar),
                new SqlParameter("@Recipient_Identify_Number", SqlDbType.VarChar),
                new SqlParameter("@Payment_Type", SqlDbType.VarChar),
                new SqlParameter("@Sum", SqlDbType.Decimal),
                new SqlParameter("@Currency", SqlDbType.VarChar),
                new SqlParameter("@Invoice_Maker", SqlDbType.VarChar),
                new SqlParameter("@Status", SqlDbType.VarChar),
                new SqlParameter("@Supplier_ID", SqlDbType.VarChar),
                new SqlParameter("@Supplier_Name", SqlDbType.VarChar)
            };
            sqlParas[0].Value = invoice.Invoice_ID;
            sqlParas[1].Value = invoice.Invoice_Code;
            sqlParas[2].Value = invoice.Invoice_Number;
            sqlParas[3].Value = invoice.Certificate_Type;
            sqlParas[4].Value = invoice.Certificate_ID;
            sqlParas[5].Value = invoice.Invoice_MakeOut_Time;
            sqlParas[6].Value = invoice.Payer_Name;
            sqlParas[7].Value = invoice.Payer_Identify_Number;
            sqlParas[8].Value = invoice.Recipient_Name;
            sqlParas[9].Value = invoice.Recipient_Identify_Number;
            sqlParas[10].Value = invoice.Payment_Type;
            sqlParas[11].Value = invoice.Sum;
            sqlParas[12].Value = invoice.Currency;
            sqlParas[13].Value = invoice.Invoice_Maker;
            sqlParas[14].Value = invoice.Status;
            sqlParas[15].Value = invoice.Supplier_ID;
            sqlParas[16].Value = invoice.Supplier_Name;

            sqlList.Add(strBui.ToString());
            sqlParamsList.Add(sqlParas);

            #endregion

            #region 删除发票项目

            strBui = new StringBuilder("delete from Invoice_Item where Invoice_ID = @Invoice_ID");
            SqlParameter[] sqlParams3 = new SqlParameter[]{
                new SqlParameter("@Invoice_ID", SqlDbType.VarChar)
            };
            sqlParams3[0].Value = invoice.Invoice_ID;
            sqlList.Add(strBui.ToString());
            sqlParamsList.Add(sqlParams3);

            #endregion

            #region 添加发票项目

            foreach (Invoice_Item item in itemList)
            {
                strBui = new StringBuilder("insert into Invoice_Item (")
                    .Append("Invoice_ID, Material_ID, Material_Name, Unit_Price, Quantity, Sum, Currency")
                    .Append(") values (")
                    .Append("@Invoice_ID, @Material_ID, @Material_Name, @Unit_Price, @Quantity, @Sum, @Currency)");

                SqlParameter[] sqlParas2 = new SqlParameter[]{
                    new SqlParameter("@Invoice_ID", SqlDbType.VarChar),
                    new SqlParameter("@Material_ID", SqlDbType.VarChar),
                    new SqlParameter("@Material_Name", SqlDbType.VarChar),
                    new SqlParameter("@Unit_Price", SqlDbType.Decimal),
                    new SqlParameter("@Quantity", SqlDbType.Decimal),
                    new SqlParameter("@Sum", SqlDbType.Decimal),
                    new SqlParameter("@Currency", SqlDbType.VarChar)
                };
                sqlParas2[0].Value = item.Invoice_ID;
                sqlParas2[1].Value = item.Material_ID;
                sqlParas2[2].Value = item.Material_Name;
                sqlParas2[3].Value = item.Unit_Price;
                sqlParas2[4].Value = item.Quantity;
                sqlParas2[5].Value = item.Sum;
                sqlParas2[6].Value = item.Currency;

                sqlList.Add(strBui.ToString());
                sqlParamsList.Add(sqlParas2);
            }

            #endregion

            return DBHelper.ExecuteNonQuery(sqlList, sqlParamsList);
        }

        /// <summary>
        /// 根据发票编号查询发票信息
        /// </summary>
        /// <param name="invoiceID"></param>
        /// <returns></returns>
        public Invoice getInvoice(string invoiceID) {
            Invoice invoice = null;
            StringBuilder strBui = new StringBuilder("select * from Invoice where Invoice_ID = '")
                .Append(invoiceID).Append("'");

            DataTable dt = DBHelper.ExecuteQueryDT(strBui.ToString());
            if (dt.Rows.Count > 0) {
                DataRow row = dt.Rows[0];
                invoice = new Invoice();
                invoice.Invoice_ID = row["Invoice_ID"].ToString();
                invoice.Invoice_Code = row["Invoice_Code"].ToString();
                invoice.Invoice_Number = row["Invoice_Number"].ToString();
                invoice.Certificate_Type = row["Certificate_Type"].ToString();
                invoice.Certificate_ID = row["Certificate_Code"].ToString();
                invoice.Invoice_MakeOut_Time = Convert.ToDateTime(row["Invoice_MakeOut_Time"].ToString());
                invoice.Create_Time = Convert.ToDateTime(row["Create_Time"].ToString());
                invoice.Payer_Name = row["Payer_Name"].ToString();
                invoice.Payer_Identify_Number = row["Payer_Identify_Number"].ToString();
                invoice.Recipient_Name = row["Recipient_Name"].ToString();
                invoice.Recipient_Identify_Number = row["Recipient_Identify_Number"].ToString();
                invoice.Payment_Type = row["Payment_Type"].ToString();
                invoice.Sum = Convert.ToDouble(row["Sum"].ToString());
                invoice.Currency = row["Currency"].ToString();
                invoice.Invoice_Maker = row["Invoice_Maker"].ToString();
                invoice.Creator = row["Creator"].ToString();
                invoice.Status = row["Status"].ToString();
                invoice.Supplier_ID = row["Supplier_ID"].ToString();
                invoice.Supplier_Name = row["Supplier_Name"].ToString();
            }

            return invoice;
        }

        /// <summary>
        /// 查询满足条件的发票
        /// </summary>
        /// <param name="conditions"></param>
        /// <param name="beginTime"></param>
        /// <param name="endTime"></param>
        /// <returns></returns>
        public List<Invoice> getInvoiceListByConditions(Dictionary<string, object> conditions,
            DateTime beginTime, DateTime endTime) {

            List<Invoice> invoiceList = new List<Invoice>();
            StringBuilder strBui = new StringBuilder("select * from Invoice where Create_Time >= @Begin_Time and ")
                .Append("Create_Time <= @End_Time ");

            foreach (string key in conditions.Keys)
            {
                string value = (string)conditions[key];
                if (value != null && !value.Equals(""))
                {
                    strBui.Append(" and ").Append(key).Append(" = @")
                        .Append(key);
                }
            }

            //这里只是定义变量，可以定义前面的sql语句用不到的变量
            //所以这里声明所有的变量
            SqlParameter[] sqlParas = new SqlParameter[]{
                new SqlParameter("@Begin_Time", SqlDbType.DateTime),
                new SqlParameter("@End_Time", SqlDbType.DateTime),
                new SqlParameter("@Invoice_Code", SqlDbType.VarChar),
                new SqlParameter("@Invoice_Number", SqlDbType.VarChar),
                new SqlParameter("@Certificate_ID", SqlDbType.VarChar),
                new SqlParameter("@Status", SqlDbType.VarChar)
            };

            sqlParas[0].Value = beginTime;
            sqlParas[1].Value = endTime;
            sqlParas[2].Value = (string)conditions["Invoice_Code"];
            sqlParas[3].Value = (string)conditions["Invoice_Number"];
            sqlParas[4].Value = (string)conditions["Certificate_Code"];
            sqlParas[5].Value = (string)conditions["Status"];

            DataTable dt = DBHelper.ExecuteQueryDT(strBui.ToString(), sqlParas);
            foreach (DataRow row in dt.Rows) {
                Invoice invoice = new Invoice();
                invoice.Invoice_ID = row["Invoice_ID"].ToString();
                invoice.Invoice_Code = row["Invoice_Code"].ToString();
                invoice.Invoice_Number = row["Invoice_Number"].ToString();
                invoice.Certificate_Type = row["Certificate_Type"].ToString();
                invoice.Certificate_ID = row["Certificate_Code"].ToString();
                invoice.Invoice_MakeOut_Time = Convert.ToDateTime(row["Invoice_MakeOut_Time"].ToString());
                invoice.Create_Time = Convert.ToDateTime(row["Create_Time"].ToString());
                invoice.Payer_Name = row["Payer_Name"].ToString();
                invoice.Payer_Identify_Number = row["Payer_Identify_Number"].ToString();
                invoice.Recipient_Name = row["Recipient_Name"].ToString();
                invoice.Recipient_Identify_Number = row["Recipient_Identify_Number"].ToString();
                invoice.Payment_Type = row["Payment_Type"].ToString();
                invoice.Sum = Convert.ToDouble(row["Sum"].ToString());
                invoice.Currency = row["Currency"].ToString();
                invoice.Invoice_Maker = row["Invoice_Maker"].ToString();
                invoice.Creator = row["Creator"].ToString();
                invoice.Status = row["Status"].ToString();
                invoice.Supplier_ID = row["Supplier_ID"].ToString();
                invoice.Supplier_Name = row["Supplier_Name"].ToString();

                invoiceList.Add(invoice);
            }

            return invoiceList;
        }

        /// <summary>
        /// 查询发票项
        /// </summary>
        /// <param name="invoiceID"></param>
        /// <returns></returns>
        public List<Invoice_Item> getInvoiceItemByInvoiceID(string invoiceID) {
            string sql = "select * from Invoice_Item where Invoice_ID = '"
                + invoiceID + "'";

            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            List<Invoice_Item> items = new List<Invoice_Item>();
            foreach (DataRow row in dt.Rows) {
                Invoice_Item item = new Invoice_Item();
                item.Invoice_ID = invoiceID;
                item.Currency = row["Currency"].ToString();
                item.Material_ID = row["Material_ID"].ToString();
                item.Material_Name = row["Material_Name"].ToString();
                item.Quantity = Convert.ToDouble(row["Quantity"].ToString());
                item.Sum = Convert.ToDouble(row["Sum"].ToString());
                item.Unit_Price = Convert.ToDouble(row["Unit_Price"].ToString());

                items.Add(item);
            }

            return items;
        }
    }
}
