﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.IDAL.SourcingManage.SourceingRecord;
using Lib.Model.SourcingManage.SourceingRecord;
using System.Data;

namespace Lib.SqlServerDAL.SourcingManage.SourceingRecord
{
    class PurchaseOrgDAL:PurchaseOrgIDAL
    {
        private string tableName = "dbo.Buyer_Org";

        public List<PurchaseOrg> getAllPurchaseOrg()
        {
            List<PurchaseOrg> list = new List<PurchaseOrg>();
            StringBuilder sb = new StringBuilder("select * from "+tableName);
            string sqlText = sb.ToString();
            DataTable dt =  DBHelper.ExecuteQueryDT(sqlText);
            if (dt!=null && dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    PurchaseOrg po = new PurchaseOrg();
                    po.PurchaseOrgId = dr["Buyer_Org"].ToString();
                    po.PurchaseOrgName = dr["Buyer_Org_Name"].ToString();
                    list.Add(po);
                }
            }
            return list;
        }
    }
}
