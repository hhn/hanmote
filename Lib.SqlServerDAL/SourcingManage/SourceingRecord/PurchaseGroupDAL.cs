﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.IDAL.SourcingManage.SourceingRecord;
using Lib.Model.SourcingManage.SourceingRecord;
using System.Data;

namespace Lib.SqlServerDAL.SourcingManage.SourceingRecord
{
    public class PurchaseGroupDAL:PurchaseGroupIDAL
    {
        private string tableName = "dbo.Buyer_Group";
        public List<PurchaseGroup> getAllPurchaseGroup()
        {
            List<PurchaseGroup> list = new List<PurchaseGroup>();
            StringBuilder sb = new StringBuilder("select * from "+tableName);
            string sqlText = sb.ToString();
            DataTable dt = DBHelper.ExecuteQueryDT(sqlText);
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    PurchaseGroup pg = new PurchaseGroup();
                    pg.PurchaseGroupId = dr["Buyer_Group"].ToString();
                    pg.PurchaseGroupName = dr["Buyer_Group_Name"].ToString();
                    pg.MaterialType = dr["Material_Type"].ToString();
                    pg.Telephone = dr["Telephone"].ToString();
                    pg.Fax = dr["Fax"].ToString();
                    list.Add(pg);
                }
            }
            return list;
        }
    }
}
