﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.IDAL.SourcingManage.SourceingRecord;
using Lib.Model.SourcingManage.SourceingRecord;
using System.Data.SqlClient;
using System.Data;

namespace Lib.SqlServerDAL.SourcingManage.SourceingRecord
{
    public class RecordInfoDAL: RecordInfoIDAL
    {
        private string tableName = "dbo.RecordInfo";

        public int addRecordInfo(RecordInfo ri)
        {
            StringBuilder sb = new StringBuilder("insert into " + tableName)
                .Append("(RecordId,SupplierId,SupplierName,PurchaseOrg,PurchaseGroup,MaterialId,MaterialName,FactoryId,StockId,DemandCount,FromType,FromId,ContractState,CreateTime,PaymentClause,PriceDetermineId,NetPrice,TradeClause,StartTime,EndTime) values(")
                .Append("@RecordId,@SupplierId,@SupplierName,@PurchaseOrg,@PurchaseGroup,@MaterialId,@MaterialName,@FactoryId,@StockId,@DemandCount,@FromType,@FromId,@ContractState,@CreateTime,@PaymentClause,@PriceDetermineId,@NetPrice,@TradeClause,@StartTime,@EndTime)");
            SqlParameter[] sqlParas = new SqlParameter[]
            {
                new SqlParameter("@RecordId",SqlDbType.VarChar),
                new SqlParameter("@SupplierId",SqlDbType.VarChar),
                new SqlParameter("@SupplierName",SqlDbType.VarChar),
                new SqlParameter("@PurchaseOrg",SqlDbType.VarChar),
                new SqlParameter("@PurchaseGroup",SqlDbType.VarChar),
                new SqlParameter("@MaterialId",SqlDbType.VarChar),
                new SqlParameter("@MaterialName",SqlDbType.VarChar),
                new SqlParameter("@FactoryId",SqlDbType.VarChar),
                new SqlParameter("@StockId",SqlDbType.VarChar),
                new SqlParameter("@DemandCount",SqlDbType.Int),
                new SqlParameter("@FromType",SqlDbType.Int),
                new SqlParameter("@FromId",SqlDbType.VarChar),
                new SqlParameter("@ContractState",SqlDbType.VarChar),
                new SqlParameter("@CreateTime",SqlDbType.DateTime),
                new SqlParameter("@PaymentClause",SqlDbType.VarChar),
                new SqlParameter("@PriceDetermineId",SqlDbType.VarChar),
                new SqlParameter("@NetPrice",SqlDbType.Float),
                new SqlParameter("@TradeClause",SqlDbType.VarChar),
                new SqlParameter("@StartTime",SqlDbType.DateTime),
                new SqlParameter("@EndTime",SqlDbType.DateTime)
            };
            sqlParas[0].Value = ri.RecordInfoId;
            sqlParas[1].Value = ri.SupplierId;
            sqlParas[2].Value = ri.SupplierName;
            sqlParas[3].Value = ri.PurchaseOrg;
            sqlParas[4].Value = ri.PurchaseGroup;
            sqlParas[5].Value = ri.MaterialId;
            sqlParas[6].Value = ri.MaterialName;
            sqlParas[7].Value = ri.FactoryId;
            sqlParas[8].Value = ri.StockId;
            sqlParas[9].Value = ri.DemandCount;
            sqlParas[10].Value = ri.FromType;
            sqlParas[11].Value = ri.FromId;
            sqlParas[12].Value = ri.ContactState;
            sqlParas[13].Value = DateTime.Now;
            sqlParas[14].Value = ri.PaymentClause;
            sqlParas[16].Value = ri.NetPrice;
            sqlParas[17].Value = ri.TradeClause;
            sqlParas[18].Value = ri.StartTime;
            sqlParas[19].Value = ri.EndTime;
            string sqlText = sb.ToString();
            return DBHelper.ExecuteNonQuery(sqlText, sqlParas);
        }

        public List<RecordInfo> getAllRecordInfos()
        {
            List<RecordInfo> list = new List<RecordInfo>();
            StringBuilder sb = new StringBuilder("select * from "+tableName);
            string sqlText = sb.ToString();
            DataTable dt = DBHelper.ExecuteQueryDT(sqlText);
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    RecordInfo ri = new RecordInfo();
                    ri.RecordInfoId = dr["RecordId"].ToString();
                    ri.SupplierId = dr["SupplierId"].ToString();
                    ri.SupplierName = dr["SupplierName"].ToString();
                    ri.MaterialId = dr["MaterialId"].ToString();
                    ri.MaterialName = dr["MaterialName"].ToString();
                    /*
                    if (dr["NetPrice"].ToString() == null ||dr["NetPrice"].ToString().Equals(" "))
                    {
                        ri.NetPrice = 0;
                    }
                    else
                    {
                        ri.NetPrice = float.Parse(dr["NetPrice"].ToString());
                    }
                     * */
                    ri.NetPrice = float.Parse(dr["NetPrice"].ToString());
                    ri.FromType = int.Parse(dr["FromType"].ToString());
                    ri.FromId = dr["FromId"].ToString();
                    ri.CreatTime = DateTime.Parse(dr["CreateTime"].ToString());
                    list.Add(ri);
                }
            }
            return list;
        }

        public List<RecordInfo> getRecordsByMaterialId(string materialId)
        {
            List<RecordInfo> list = new List<RecordInfo>();
            StringBuilder sb = new StringBuilder("select * from "+tableName+" where MaterialId='"+materialId+"'");
            DataTable dt = DBHelper.ExecuteQueryDT(sb.ToString());
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    RecordInfo ri = new RecordInfo();
                    ri.RecordInfoId = dr["RecordId"].ToString();
                    ri.SupplierId = dr["SupplierId"].ToString();
                    ri.SupplierName = dr["SupplierName"].ToString();
                    ri.NetPrice = float.Parse(dr["NetPrice"].ToString());
                    ri.CreatTime = DateTime.Parse(dr["CreateTime"].ToString());
                    list.Add(ri);
                }
            }
            return list;
        }  
    }
}
