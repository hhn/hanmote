﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.IDAL.SourcingManage.SourcingManagement;
using Lib.Model.SourcingManage.SourcingManagement;
using System.Data.SqlClient;
using System.Data;

namespace Lib.SqlServerDAL.SourcingManage.SourcingManagement
{
    public class BidWeightScoreDAL : BidWeightScoreIDAL
    {
        private string tableName = "dbo.BidWeightScore";
        public int addBidWeightScore(BidWeightScore bws)
        {
            StringBuilder sb = new StringBuilder("insert into " + tableName+" (")
                .Append("bidId,weightScoreName,weightScoreNum) values (")
                .Append("@BidId,@WeightScoreName,@WeightScoreNum)");
            SqlParameter[] sqlParas = new SqlParameter[] 
            { 
                new SqlParameter("@BidId",SqlDbType.VarChar),
                new SqlParameter("@WeightScoreName",SqlDbType.VarChar),
                new SqlParameter("@WeightScoreNum",SqlDbType.Int)
            };
            sqlParas[0].Value = bws.BidId;
            sqlParas[1].Value = bws.WeightScoreName;
            sqlParas[2].Value = bws.WeightScoreNum;
            string sqlText = sb.ToString();
            return DBHelper.ExecuteNonQuery(sqlText, sqlParas);
        }

        public List<BidWeightScore> findWeightScoresByBidId(string bidId)
        {
            List<BidWeightScore> list = new List<BidWeightScore>();
            StringBuilder sb = new StringBuilder("select * from " + tableName+" where bidId = '"+ bidId +"'");
            string sqlText = sb.ToString();
            DataTable dt = DBHelper.ExecuteQueryDT(sqlText);
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    BidWeightScore bws = new BidWeightScore();
                    bws.WeightScoreName = row["weightScoreName"].ToString();
                    bws.WeightScoreNum = Convert.ToInt32(row["weightScoreNum"].ToString());
                    list.Add(bws);
                }
            }
            return list;
        }
    }
}
