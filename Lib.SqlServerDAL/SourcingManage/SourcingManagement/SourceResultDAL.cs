﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Lib.IDAL.SourcingManage;
using Lib.IDAL.SourcingManage.SourcingManagement;
using Lib.Model.SourcingManage.SourcingManagement;

namespace Lib.SqlServerDAL.SourcingManage.SourcingManagement
{
    public class SourceResultDAL:SourceResultIDAL
    {
        private string tableName = "dbo.Source_Result";

        public int addSourceResult(SourceResult sim)
        {
            StringBuilder sb = new StringBuilder("insert into " + tableName + " (")
                .Append(" Item_ID,Source_ID,Supplier_ID,Material_ID,Material_Name,Material_Group,Measurement,Net_Price,Net_price_Unit,Buy_Number,Provider_Number,Factory_ID,Stock_ID,Update_Time) values(")
                .Append("@Item_ID,@Source_ID,@Supplier_ID,@Material_ID,@Material_Name,@Material_Group,@Measurement,@Net_Price,@Net_price_Unit,@Buy_Number,@Provider_Number,@Factory_ID,@Stock_ID,@Update_Time)");
            SqlParameter[] sqlParas = new SqlParameter[]{
                new SqlParameter("@Item_ID",SqlDbType.VarChar),
                new SqlParameter("@Source_ID",SqlDbType.VarChar),
                new SqlParameter("@Supplier_ID",SqlDbType.VarChar),
                new SqlParameter("@Material_ID",SqlDbType.VarChar),
                new SqlParameter("@Material_Name",SqlDbType.VarChar),
                new SqlParameter("@Material_Group",SqlDbType.VarChar),
                new SqlParameter("@Measurement",SqlDbType.VarChar),
                new SqlParameter("@Net_Price",SqlDbType.Float),
                new SqlParameter("@Net_price_Unit",SqlDbType.VarChar),
                new SqlParameter("@Buy_Number",SqlDbType.Int),
                new SqlParameter("@Provider_Number",SqlDbType.Int),
                new SqlParameter("@Factory_ID",SqlDbType.VarChar),
                new SqlParameter("@Stock_ID",SqlDbType.VarChar),
                new SqlParameter("@Update_Time",SqlDbType.DateTime)
            };
            sqlParas[0].Value = sim.Item_ID;
            sqlParas[1].Value = sim.Source_ID;
            sqlParas[2].Value = sim.Supplier_ID;
            sqlParas[3].Value = sim.Material_ID;
            sqlParas[4].Value = sim.Material_Name;
            sqlParas[5].Value = sim.Material_Group;
            sqlParas[6].Value = sim.Measurement;
            sqlParas[7].Value = sim.Net_Price;
            sqlParas[8].Value = sim.Net_Price_Unit;
            sqlParas[9].Value = sim.Buy_Number;
            sqlParas[10].Value = sim.Provider_Number;
            sqlParas[11].Value = sim.Factory_ID;
            sqlParas[12].Value = sim.Stock_ID;
            sqlParas[13].Value = DateTime.Now;
            string sqlText = sb.ToString();
            return DBHelper.ExecuteNonQuery(sqlText,sqlParas);
        }

        #region
        public List<SourceResult> getSourceItemMaterialByFK(string contractID)
        {
            string sql = "SELECT * FROM Source_Item_Material WHERE Item_ID = '" + contractID + "'";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            if (dt == null || dt.Rows.Count == 0)
                return null;
            List<SourceResult> resultList = new List<SourceResult>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                SourceResult material = new SourceResult();
                material.Item_ID = dt.Rows[i]["Item_ID"].ToString();
                material.Material_Group = dt.Rows[i]["Material_Group"].ToString();
                material.Material_ID = dt.Rows[i]["Material_ID"].ToString();
                material.Material_Name = dt.Rows[i]["Material_Name"].ToString();
                material.Measurement = dt.Rows[i]["Measurement"].ToString();
                material.Net_Price = float.Parse(dt.Rows[i]["Net_Price"].ToString());
                material.Net_Price_Unit = dt.Rows[i]["Net_Price_Unit"].ToString();
                material.Provider_Number = Convert.ToInt32(dt.Rows[i]["Provider_Number"]);
                material.Buy_Number = Convert.ToInt32(dt.Rows[i]["Buy_Number"]);
                material.Supplier_ID = dt.Rows[i]["Supplier_ID"].ToString();
                resultList.Add(material);
            }
            return resultList;
        }
        #endregion

    }
}
