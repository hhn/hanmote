﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Lib.IDAL.SourcingManage;
using Lib.IDAL.SourcingManage.SourcingManagement;
using Lib.Model.SourcingManage;

namespace Lib.SqlServerDAL.SourcingManage.SourcingManagement
{
    public class SourceItemMaterialDAL:SourceItemMaterialIDAL
    {
        private string tableName = "dbo.Source_Item_Material";

        public int addSourceItemMaterial(SourceItemMaterial sim)
        {
            StringBuilder sb = new StringBuilder("insert into " + tableName + " (")
                .Append(" Item_ID,Supplier_ID,Material_ID,Material_Name,Material_Group,Measurement,Net_Price,Net_price_Unit,Buy_Number,Provider_Number) values(")
                .Append("'" + sim.Item_ID + "',")
                .Append("'" + sim.Supplier_ID + "',")
                .Append("'" + sim.Material_ID + "',")
                .Append("'" + sim.Material_Name + "',")
                .Append("'" + sim.Material_Group + "',")
                .Append("'" + sim.Measurement + "',")
                .Append(+sim.Net_Price + ",")
                .Append("'" + sim.Net_Price_Unit + "',")
                .Append(+sim.Buy_Number + ",")
                .Append(+sim.Provider_Number + ")");
//                 .Append("'" + sim.Start_Time.ToString() + "',")
//                 .Append("'" + sim.End_Time.ToString() + "')");
                
            string sqlText = sb.ToString();
            int result = DBHelper.ExecuteNonQuery(sqlText);
            return result;
        }

        #region
        public List<SourceItemMaterial> getSourceItemMaterialByFK(string contractID)
        {
            string sql = "SELECT * FROM Source_Item_Material WHERE Item_ID = '" + contractID + "'";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            if (dt == null || dt.Rows.Count == 0)
                return null;
            List<SourceItemMaterial> resultList = new List<SourceItemMaterial>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                SourceItemMaterial material = new SourceItemMaterial();
                material.Item_ID = dt.Rows[i]["Item_ID"].ToString();
                material.Material_Group = dt.Rows[i]["Material_Group"].ToString();
                material.Material_ID = dt.Rows[i]["Material_ID"].ToString();
                material.Material_Name = dt.Rows[i]["Material_Name"].ToString();
                material.Measurement = dt.Rows[i]["Measurement"].ToString();
                material.Net_Price = float.Parse(dt.Rows[i]["Net_Price"].ToString());
                material.Net_Price_Unit = dt.Rows[i]["Net_Price_Unit"].ToString();
                material.Provider_Number = Convert.ToInt32(dt.Rows[i]["Provider_Number"]);
                material.Buy_Number = Convert.ToInt32(dt.Rows[i]["Buy_Number"]);
                material.Supplier_ID = dt.Rows[i]["Supplier_ID"].ToString();
                resultList.Add(material);
            }
            return resultList;
        }
        #endregion

    }
}
