﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Lib.IDAL.SourcingManage;
using Lib.IDAL.SourcingManage.SourcingManagement;
using Lib.Model.SourcingManage.SourcingManagement;

namespace Lib.SqlServerDAL.SourcingManage.SourcingManagement
{
    public class SourceItemDAL:SourceItemIDAL
    {
        private string tableName = "dbo.Source_Item";

        /// <summary>
        /// 根据寻源编号和供应商编号查找该供应商记录是否已经添加成功
        /// </summary>
        /// <param name="sourceId"></param>
        /// <param name="supplierId"></param>
        /// <returns></returns>
        public SourceItem ExistsBySSId(string sourceId,string supplierId)
        {
            SourceItem si = null;
            StringBuilder sb = new StringBuilder("select * from "+tableName+" where Source_ID='"+sourceId+"' and Supplier_ID='"+supplierId+"'");
            string sqlText = sb.ToString();
            DataTable dt = DBHelper.ExecuteQueryDT(sqlText);
            if (dt != null && dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                si = new SourceItem();
                si.Item_ID = dr["Item_ID"].ToString();
                si.Source_ID = dr["Source_ID"].ToString();
                si.Supplier_ID = dr["Supplier_ID"].ToString();
            }
            return si;
        }

        /// <summary>
        /// 新添寻源子项
        /// </summary>
        /// <param name="sourceItem"></param>
        /// <returns></returns>
        public int addSourceItem(SourceItem sourceItem)
        {
            StringBuilder sb = new StringBuilder("insert into " + tableName + " (")
                .Append(" Item_ID,Source_ID,Supplier_ID,Factory_ID,State) values(")
                .Append("'" + sourceItem.Item_ID + "',")
                .Append("'" + sourceItem.Source_ID + "',")
                .Append("'" + sourceItem.Supplier_ID + "',")
                .Append("'"+sourceItem.Factory_ID+"',")
                .Append(+sourceItem.State + ")");
            string sqlText = sb.ToString();
            int result = DBHelper.ExecuteNonQuery(sqlText);
            return result;
        }

        #region 为创建合同服务
        public List<string> getAllValidItemID()
        {
            string sql = "SELECT Item_ID FROM Source_Item WHERE State = 0";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            if (dt == null || dt.Rows.Count == 0)
                return null;
            List<string> resultList = new List<string>();
            string obj;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                obj = dt.Rows[i][0].ToString();
                resultList.Add(obj);
                obj = null;
            }
            return resultList;
        }

        public SourceItem getSourceItemByID(string itemID)
        {
            string sql = "SELECT * FROM Source_Item WHERE Item_ID = '" + itemID + "'";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            if (dt == null || dt.Rows.Count == 0)
                return null;
            DataRow row = dt.Rows[0];
            SourceItem sourceItem = new SourceItem();
            sourceItem.Item_ID = row["Item_ID"].ToString();
            sourceItem.Source_ID = row["Source_ID"].ToString();
            sourceItem.Supplier_ID = row["Supplier_ID"].ToString();
            sourceItem.Factory_ID = row["Factory_ID"].ToString();
            sourceItem.State = Convert.ToInt32(row["State"].ToString());
            return sourceItem;
        }

        public List<string> getAllInvalidItemID()
        {
            string sql = "SELECT Item_ID FROM Source_Item WHERE State = 1";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            if (dt == null || dt.Rows.Count == 0)
                return null;
            List<string> resultList = new List<string>();
            string obj;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                obj = dt.Rows[i][0].ToString();
                resultList.Add(obj);
                obj = null;
            }
            return resultList;
        }
        #endregion

    }
}
