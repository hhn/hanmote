﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.IDAL.SourcingManage.SourcingManagement;
using Lib.Model.SourcingManage.SourcingManagement;
using System.Data.SqlClient;
using System.Data;

namespace Lib.SqlServerDAL.SourcingManage.SourcingManagement
{
    public class BidDAL : BidIDAL
    {
        private string tableName = "dbo.Bid";
        //添加招标
        public int addBid(Bid bid)
        {
            StringBuilder sb = new StringBuilder(" insert into " + tableName)
                .Append("(bidId,bidName,createTime,serviceType,catalogue,displayType,purchaseOrg,purchaseGroup,startTime, endTime,startBeginTime,limitTime,timezone,currency,commBidStartTime,commBidStartDate,techBidStartTime,techBidStartDate,buyEndTime,buyEndDate,enrollEndTime,enrollEndDate,enrollStartTime,enrollStartDate,bidCost,bidState,evalMethId,payItem) values(")
                .Append("@BidId,@BidName,@CreateTime,@ServiceType,@Catalogue,@DisplayType,@PurchaseOrg,@PurchaseGroup,@StartTime,@EndTime,@StartBeginTime,@LimitTime,@TimeZone,@Currency,@CommBidStartTime,@CommBidStartDate,@TechBidStartTime,@TechBidStartDate,@BuyEndTime,@BuyEndDate,@EnrollEndTime,@EnrollEndDate,@EnrollStartTime,@EnrollStartDate,@BidCost,@BidState,@EvalMethId,@PayItem)");
            SqlParameter[] sqlParas = new SqlParameter []{ 
                new SqlParameter("@BidId",SqlDbType.VarChar),
                new SqlParameter("@BidName",SqlDbType.VarChar),
                new SqlParameter("@CreateTime",SqlDbType.DateTime),
                new SqlParameter("@ServiceType",SqlDbType.VarChar),
                new SqlParameter("@Catalogue",SqlDbType.VarChar),
                new SqlParameter("@DisplayType",SqlDbType.VarChar),
                new SqlParameter("@PurchaseOrg",SqlDbType.Int),
                new SqlParameter("@PurchaseGroup",SqlDbType.Int),
                new SqlParameter("@StartTime",SqlDbType.DateTime),
                new SqlParameter("@EndTime",SqlDbType.DateTime),
                new SqlParameter("@StartBeginTime",SqlDbType.DateTime),
                new SqlParameter("@LimitTime",SqlDbType.DateTime),
                new SqlParameter("@TimeZone",SqlDbType.VarChar),
                new SqlParameter("@Currency",SqlDbType.VarChar),
                new SqlParameter("@CommBidStartTime",SqlDbType.DateTime),
                new SqlParameter("@CommBidStartDate",SqlDbType.DateTime),
                new SqlParameter("@TechBidStartTime",SqlDbType.DateTime),
                new SqlParameter("@TechBidStartDate",SqlDbType.DateTime),
                new SqlParameter("@BuyEndTime",SqlDbType.DateTime),
                new SqlParameter("@BuyEndDate",SqlDbType.DateTime),
                new SqlParameter("@EnrollEndTime",SqlDbType.DateTime),
                new SqlParameter("@EnrollEndDate",SqlDbType.DateTime),
                new SqlParameter("@EnrollStartTime",SqlDbType.DateTime),
                new SqlParameter("@EnrollStartDate",SqlDbType.DateTime),
                new SqlParameter("@BidCost",SqlDbType.Float),
                new SqlParameter("@BidState",SqlDbType.Int),
                new SqlParameter("@EvalMethId",SqlDbType.Int),
                new SqlParameter("@PayItem",SqlDbType.VarChar)
            };

            sqlParas[0].Value = bid.BidId;
            sqlParas[1].Value = bid.BidName;
            sqlParas[2].Value = DateTime.Now;
            sqlParas[3].Value = bid.ServiceType;
            sqlParas[4].Value = bid.Catalogue;
            sqlParas[5].Value = bid.DisplayType;
            sqlParas[6].Value = bid.PurchaseOrg;
            sqlParas[7].Value = bid.PurchaseGroup;
            sqlParas[8].Value = bid.StartTime;
            sqlParas[9].Value = bid.EndTime;
            sqlParas[10].Value = bid.StartBeginTime;
            sqlParas[11].Value = bid.LimitTime;
            sqlParas[12].Value = bid.TimeZone;
            sqlParas[13].Value = bid.Currency;
            sqlParas[14].Value = bid.CommBidStartTime;
            sqlParas[15].Value = bid.CommBidStartDate;
            sqlParas[16].Value = bid.TechBidStartTime;
            sqlParas[17].Value = bid.TechBidStartDate;
            sqlParas[18].Value = bid.BuyEndTime;
            sqlParas[19].Value = bid.BuyEndDate;
            sqlParas[20].Value = bid.EnrollEndTime;
            sqlParas[21].Value = bid.EnrollEndDate;
            sqlParas[22].Value = bid.EnrollStartTime;
            sqlParas[23].Value = bid.EnrollStartDate;
            sqlParas[24].Value = bid.BidCost;
            sqlParas[25].Value = bid.BidState;
            sqlParas[26].Value = bid.EvalMethId;
            sqlParas[27].Value = bid.PayItem;

            string sqlText = sb.ToString();
            return DBHelper.ExecuteNonQuery(sqlText, sqlParas);
        }

        public List<Bid> getBids()
        {
            List<Bid> result = new List<Bid>();
            StringBuilder sb = new StringBuilder("select * from " + tableName);
            string sqlText = sb.ToString();
            DataTable dt =  DBHelper.ExecuteQueryDT(sqlText);
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    Bid bid = new Bid();
                    bid.BidId = row["bidId"].ToString();
                    bid.BidName = row["bidName"].ToString();
                    bid.CreateTime = DateTime.Parse(row["createTime"].ToString());
                    bid.BidState = Convert.ToInt32(row["bidState"].ToString());
                    result.Add(bid); 
                }
            }
            return result;
        }
    }
}
