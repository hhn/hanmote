﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.IDAL.SourcingManage.SourcingManagement;
using Lib.Model.SourcingManage.SourcingManagement;
using System.Data.SqlClient;
using System.Data;

namespace Lib.SqlServerDAL.SourcingManage.SourcingManagement
{
    public class TransactItemDAL : TransactItemIDAL
    {
        private string tableName = "dbo.Transact_Item";

        public int addTransactItem(TransactItem tt)
        {
            StringBuilder sb = new StringBuilder("insert into " + tableName + " (BidId,SupplierId,ItemName,GoalDescription) values(")
                .Append("@BidId,@SupplierId,@ItemName,@GoalDescription)");
            SqlParameter[] sqlParas = new SqlParameter[]{
                new SqlParameter("@BidId",SqlDbType.VarChar),
                new SqlParameter("@SupplierId",SqlDbType.VarChar),
                new SqlParameter("@ItemName",SqlDbType.VarChar),
                new SqlParameter("@GoalDescription",SqlDbType.VarChar),
            };
            sqlParas[0].Value = tt.BidId;
            sqlParas[1].Value = tt.SupplierId;
            sqlParas[2].Value = tt.ItemName;
            sqlParas[3].Value = tt.GoalDescription;
            return DBHelper.ExecuteNonQuery(sb.ToString(), sqlParas);
        }

        public List<TransactItem> getTransactItemsByBId(string bidId,string supplierId)
        {
            List<TransactItem> list = new List<TransactItem>();
            StringBuilder sb = new StringBuilder("select * from " +tableName+" where BidId='"+bidId+"' and SupplierId='"+supplierId+"'" );
            DataTable dt = DBHelper.ExecuteQueryDT(sb.ToString());
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    TransactItem tt = new TransactItem();
                    tt.BidId = bidId;
                    tt.ItemName = dr["ItemName"].ToString();
                    tt.GoalDescription = dr["GoalDescription"].ToString();
                    tt.SupplierAnswer = dr["SupplierAnswer"].ToString();
                    tt.Note = dr["Note"].ToString();
                    list.Add(tt);
                }
            }


            return list;
        }
    }
}
