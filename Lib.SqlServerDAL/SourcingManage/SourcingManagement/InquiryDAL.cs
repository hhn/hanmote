﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Lib.IDAL.SourcingManage;
using Lib.IDAL.SourcingManage.SourcingManagement;
using Lib.Model.SourcingManage;

namespace Lib.SqlServerDAL.SourcingManage.SourcingManagement
{
    public class InquiryDAL:InquiryIDAL
    {
        private string tableName = "";

        /// <summary>
        /// 构造函数，设为私有
        /// </summary>
        public InquiryDAL()
        {
            tableName = "dbo.Inquiry_Table";
        }

        /// <summary>
        /// 保存新的询价单表头
        /// </summary>
        /// <param name="inquiry_Table"></param>
        /// <returns></returns>
        public int addNewInquiry_Table(Inquiry_Table inquiry_Table)
        {
            SqlParameter[] sqlParas = new SqlParameter[]{
                new SqlParameter("@Inquiry_ID", SqlDbType.VarChar),
                new SqlParameter("@Inquiry_Name", SqlDbType.VarChar),
                new SqlParameter("@Transaction_Type", SqlDbType.VarChar),
                new SqlParameter("@Buyer_Name", SqlDbType.VarChar),
                new SqlParameter("@Inquiry_Time", SqlDbType.DateTime),
                new SqlParameter("@Offer_Time", SqlDbType.DateTime),
                new SqlParameter("@Create_Time", SqlDbType.DateTime),
                new SqlParameter("@State", SqlDbType.VarChar),
                new SqlParameter("@Inquiry_Path", SqlDbType.VarChar),
                new SqlParameter("@Example3", SqlDbType.VarChar)
            };
            StringBuilder stringBuilder = new StringBuilder("insert into ");
            stringBuilder.Append(tableName + "([Inquiry_ID],[Inquiry_Name],[Transaction_Type],[Buyer_Name],[Inquiry_Time],[Offer_Time],[Create_Time],[State],[Inquiry_Path],[Example3])VALUES(");
            stringBuilder.Append("@Inquiry_ID, @Inquiry_Name, @Transaction_Type, @Buyer_Name, @Inquiry_Time, @Offer_Time, @Create_Time, @State, @Inquiry_Path, @Example3)");
            
            sqlParas[0].Value = inquiry_Table.Inquiry_ID;
            sqlParas[1].Value = inquiry_Table.Inquiry_Name;
            sqlParas[2].Value = inquiry_Table.Transaction_Type;
            sqlParas[3].Value = inquiry_Table.Buyer_Name;
            sqlParas[4].Value = inquiry_Table.Inquiry_Time;
            sqlParas[5].Value = inquiry_Table.Offer_Time;
            //sqlParas[6].Value = DBNull.Value;
            sqlParas[6].Value = DateTime.Now;
            sqlParas[7].Value = inquiry_Table.State;
            sqlParas[8].Value = inquiry_Table.Inquiry_Path;
            sqlParas[9].Value = inquiry_Table.Example3;
            string sqlText = stringBuilder.ToString();
            return DBHelper.ExecuteNonQuery(sqlText, sqlParas);
        }

        /// <summary>
        /// 查询单号是否存在
        /// </summary>
        /// <param name="inquiry_Table"></param>
        /// <returns></returns>
        public bool isInquiry_TableExist(string id)
        {
            StringBuilder stringBuilder = new StringBuilder("select * from ")
                .Append(tableName).Append(" where Inquiry_ID = '")
                .Append(id).Append("'");
            string sqlText = stringBuilder.ToString();
            DataTable dt = DBHelper.ExecuteQueryDT(sqlText);
            if (dt.Rows.Count > 0)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// 更改表头信息
        /// </summary>
        /// <param name="inquiry_Table"></param>
        /// <returns></returns>
        public int updateInquiry_Table(Inquiry_Table inquiry_Table)
        {
            StringBuilder stringBuilder = new StringBuilder("update Inquiry_Table set Transaction_Type = '")
                .Append(inquiry_Table.Transaction_Type).Append("', Buyer_Name = '")
                .Append(inquiry_Table.Buyer_Name).Append("', State = '")
                .Append(inquiry_Table.State).Append("', Inquiry_Path = '")
                .Append(inquiry_Table.Inquiry_Path).Append("', Inquiry_Time = '")
                .Append(inquiry_Table.Inquiry_Time).Append("', Offer_Time = '")
                .Append(inquiry_Table.Offer_Time).Append("'")
                .Append("where Inquiry_ID = '").Append(inquiry_Table.Inquiry_ID).Append("'");

            string sqlText = stringBuilder.ToString();
            return DBHelper.ExecuteNonQuery(sqlText);
        }

        /// <summary>
        /// 更新询价项状态
        /// </summary>
        /// <param name="inquiry_id"></param>
        /// <param name="state"></param>
        /// <returns></returns>
        public int updateInquiryState(String inquiry_id,String state)
        {
            StringBuilder sb = new StringBuilder("update Inquiry_Table set State= '")
                .Append(state).Append("' where Inquiry_ID ='")
                .Append(inquiry_id).Append("'");
            return DBHelper.ExecuteNonQuery(sb.ToString());
        }


    }
}
