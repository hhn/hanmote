﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.IDAL.SourcingManage.SourcingManagement;
using Lib.Model.SourcingManage.SourcingManagement;
using System.Data.SqlClient;
using System.Data;

namespace Lib.SqlServerDAL.SourcingManage.SourcingManagement
{
    public class TransactCostDAL : TransactCostIDAL
    {
        private string tableName = "dbo.TransactCost";

        public int addTransactCost(TransactCost tc)
        {
            StringBuilder sb = new StringBuilder("insert into "+tableName+" (bidId,supplierId,materialId,materialName,count,unit,targetPrice,targetTotal) values(")
                .Append("@BidId,@SupplierId,@MaterialId,@MaterialName,@Count,@Unit,@TargetPrice,@TargetTotal)");
            SqlParameter[] sqlParas = new SqlParameter[]{
                new SqlParameter("@BidId",SqlDbType.VarChar),
                new SqlParameter("@SupplierId",SqlDbType.VarChar),
                new SqlParameter("@MaterialId",SqlDbType.VarChar),
                new SqlParameter("@MaterialName",SqlDbType.VarChar),
                new SqlParameter("@Count",SqlDbType.Int),
                new SqlParameter("@Unit",SqlDbType.VarChar),
                new SqlParameter("@TargetPrice",SqlDbType.Float),
                new SqlParameter("@TargetTotal",SqlDbType.Float)
            };
            sqlParas[0].Value = tc.BidId;
            sqlParas[1].Value = tc.SupplierId;
            sqlParas[2].Value = tc.MaterialId;
            sqlParas[3].Value = tc.MaterialName;
            sqlParas[4].Value = tc.Count;
            sqlParas[5].Value = tc.Unit;
            sqlParas[6].Value = tc.TargetPrice;
            sqlParas[7].Value = tc.TargetTotal;
            return DBHelper.ExecuteNonQuery(sb.ToString(), sqlParas);
        }

        public List<TransactCost> getTransCostsBySId(string bid,string sid)
        {
            List<TransactCost> list = new List<TransactCost>();
            StringBuilder sb = new StringBuilder("select * from " + tableName + " where bidId='" + bid + "' and SupplierId='"+sid+"'");
            DataTable dt = DBHelper.ExecuteQueryDT(sb.ToString());
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach(DataRow dr in dt.Rows)
                {
                    TransactCost tc = new TransactCost();
                    tc.BidId = bid;
                    tc.SupplierId = dr["supplierId"].ToString();
                    tc.MaterialId = dr["materialId"].ToString();
                    tc.MaterialName = dr["materialName"].ToString();
                    tc.Count = int.Parse(dr["count"].ToString());
                    tc.Unit = dr["unit"].ToString();
                    tc.TargetPrice = float.Parse(dr["targetPrice"].ToString());
                    tc.TargetTotal = float.Parse(dr["targetTotal"].ToString());
                    list.Add(tc);
                }
            }
            return list;
        }
    }
}
