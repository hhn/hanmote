﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Lib.IDAL.SourcingManage;
using Lib.IDAL.SourcingManage.SourcingManagement;
using Lib.Model.SourcingManage.SourcingManagement;

namespace Lib.SqlServerDAL.SourcingManage.SourcingManagement
{
    public class SourceMaterialDAL:SourceMaterialIDAL
    {
        private string tableName = "dbo.Source_Material";

        /// <summary>
        /// 添加寻源-物料信息
        /// </summary>
        /// <param name="sm"></param>
        /// <returns></returns>
        public int addSourceMaterials(SourceMaterial sm)
        {

            StringBuilder sb = new StringBuilder("insert into " + tableName + "(")
                   .Append(" Source_ID,Material_ID,Material_Name,Demand_Count,Demand_ID,Unit,Factory_ID,Stock_ID,DeliveryStartTime,DeliveryEndTime) values(")
                   .Append(" @Source_ID,@Material_ID,@Material_Name,@Demand_Count,@Demand_ID,@Unit,@Factory_ID,@Stock_ID,@DeliveryStartTime,@DeliveryEndTime)");
            SqlParameter[] sqlParas = new SqlParameter[]{
                new SqlParameter("@Source_ID",SqlDbType.VarChar),
                new SqlParameter("@Material_ID",SqlDbType.VarChar),
                new SqlParameter("@Material_Name",SqlDbType.VarChar),
                new SqlParameter("@Demand_Count",SqlDbType.Int),
                new SqlParameter("@Demand_ID",SqlDbType.VarChar),
                new SqlParameter("@Unit",SqlDbType.VarChar),
                new SqlParameter("@Factory_ID",SqlDbType.VarChar),
                new SqlParameter("@Stock_ID",SqlDbType.VarChar),
                new SqlParameter("@DeliveryStartTime",SqlDbType.DateTime),
                new SqlParameter("@DeliveryEndTime",SqlDbType.DateTime)
            };
            sqlParas[0].Value = sm.Source_ID;
            sqlParas[1].Value = sm.Material_ID;
            sqlParas[2].Value = sm.Material_Name;
            sqlParas[3].Value = sm.Demand_Count;
            sqlParas[4].Value = sm.Demand_ID;
            sqlParas[5].Value = sm.Unit;
            sqlParas[6].Value = sm.FactoryId;
            sqlParas[7].Value = sm.StockId;
            sqlParas[8].Value = sm.DeliveryStartTime;
            sqlParas[9].Value = sm.DeliveryEndTime;
            string sqlText = sb.ToString();
            int result = DBHelper.ExecuteNonQuery(sqlText,sqlParas);
            return result;
            
        }

        /// <summary>
        /// 根据寻源单号查找对应的物料信息
        /// </summary>
        /// <param name="sourceId"></param>
        /// <returns></returns>
        public List<SourceMaterial> findMaterialsBySId(string sourceId)
        {
            List<SourceMaterial> smList = new List<SourceMaterial>();
            StringBuilder sb = new StringBuilder("select * from "+tableName+" where Source_ID='"+sourceId+"'");
            string sqlText = sb.ToString();
            DataTable dt = DBHelper.ExecuteQueryDT(sqlText);
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    SourceMaterial sm = new SourceMaterial();
                    sm.Material_ID = dr["Material_ID"].ToString();
                    sm.Material_Name = dr["Material_Name"].ToString();
                    sm.Demand_Count = int.Parse(dr["Demand_Count"].ToString());
                    sm.Unit = dr["Unit"].ToString();
                    sm.Demand_ID = dr["Demand_ID"].ToString();
                    sm.StockId = dr["Stock_ID"].ToString();
                    sm.DeliveryStartTime = DateTime.Parse(dr["DeliveryStartTime"].ToString());
                    sm.DeliveryEndTime = DateTime.Parse(dr["DeliveryEndTime"].ToString());
                    smList.Add(sm);
                }
            }
            return smList;
        }       

        public SourceMaterial findSMBySIdMId(string sourceId,string materialId)
        {
            SourceMaterial sm = new SourceMaterial();
            StringBuilder sb = new StringBuilder("select * from " + tableName + " where Source_ID='" + sourceId + "' and Material_ID='" + materialId + "'");
            string sqlText = sb.ToString();
            DataTable dt = DBHelper.ExecuteQueryDT(sqlText);
            if (dt != null && dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                sm.Demand_ID = dr["Demand_ID"].ToString();
                sm.FactoryId = dr["Factory_ID"].ToString();
                sm.DeliveryStartTime = DateTime.Parse(dr["DeliveryStartTime"].ToString());
                sm.DeliveryEndTime = DateTime.Parse(dr["DeliveryEndTime"].ToString());
            }
            return sm;
        }



        public List<SourceMaterial> findSourceMaterialsBySourceId(string p)
        {
            List<SourceMaterial> list = new List<SourceMaterial>();
            StringBuilder sb = new StringBuilder("select * from " + tableName + " where Source_ID ='" + p + "'");
            string sqlText = sb.ToString();
            DataTable dt = DBHelper.ExecuteQueryDT(sqlText);
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    SourceMaterial bm = new SourceMaterial();
                    bm.Material_ID = row["Material_ID"].ToString();
                    bm.Material_Name = row["Material_Name"].ToString();
                    bm.Demand_Count = Convert.ToInt32(row["Demand_Count"].ToString());
                    bm.Unit = row["Unit"].ToString();
                    bm.FactoryId = row["Factory_ID"].ToString();
                    bm.StockId = row["Stock_ID"].ToString();
                    bm.DeliveryStartTime = DateTime.Parse(row["DeliveryStartTime"].ToString());
                    bm.DeliveryEndTime = DateTime.Parse(row["DeliveryEndTime"].ToString());
                    list.Add(bm);
                }
            }
            return list;
        }

        public List<SourceMaterial> findSourcesByMaterialId(string materialId)
        {
            List<SourceMaterial> list = new List<SourceMaterial>();
            StringBuilder sb = new StringBuilder("select * from "+tableName+" where Material_ID='"+materialId+"' and Source_ID like 'B%'");
            string sqlText = sb.ToString();
            DataTable dt = DBHelper.ExecuteQueryDT(sqlText);
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    SourceMaterial sm = new SourceMaterial();
                    sm.Source_ID = row["Source_ID"].ToString();
                    list.Add(sm);
                }
            }
            return list;
        }
    }
}
