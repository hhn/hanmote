﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Lib.IDAL.SourcingManage;
using Lib.IDAL.SourcingManage.SourcingManagement;
using Lib.Model.SourcingManage.SourcingManagement;

namespace Lib.SqlServerDAL.SourcingManage.SourcingManagement
{
    public class SourceInquiryDAL:SourceInquiryIDAL
    {
        private string tableName = "dbo.Source_Inquiry";

        /// <summary>
        /// 构造函数，设为私有
        /// </summary>
        public SourceInquiryDAL()
        {
            
        }

        /// <summary>
        /// 保存新的询价单表头
        /// </summary>
        /// <param name="inquiry_Table"></param>
        /// <returns></returns>
        public int addNewInquiry_Table(SourceInquiry inquiry_Table)
        {
            SqlParameter[] sqlParas = new SqlParameter[]{
                new SqlParameter("@Source_ID", SqlDbType.VarChar),
                new SqlParameter("@Transaction_Type", SqlDbType.VarChar),
                new SqlParameter("@Offer_Time", SqlDbType.DateTime),
                new SqlParameter("@State", SqlDbType.VarChar),
                new SqlParameter("@Inquiry_Path", SqlDbType.VarChar)
            };
            StringBuilder stringBuilder = new StringBuilder("insert into ");
            stringBuilder.Append(tableName + "(Source_ID,Transaction_Type,Offer_Time,State,Inquiry_Path) VALUES(");
            stringBuilder.Append("@Source_ID,@Transaction_Type, @Offer_Time,@State, @Inquiry_Path)");
            
            sqlParas[0].Value = inquiry_Table.Source_ID;
            sqlParas[1].Value = inquiry_Table.Transaction_Type;
            sqlParas[2].Value = inquiry_Table.Offer_Time;
            sqlParas[3].Value = inquiry_Table.State;
            sqlParas[4].Value = inquiry_Table.Inquiry_Path;
            string sqlText = stringBuilder.ToString();
            return DBHelper.ExecuteNonQuery(sqlText, sqlParas);
        }

        /// <summary>
        /// 查询单号是否存在
        /// </summary>
        /// <param name="inquiry_Table"></param>
        /// <returns></returns>
        public bool isInquiry_TableExist(string id)
        {
            StringBuilder stringBuilder = new StringBuilder("select * from ")
                .Append(tableName).Append(" where Inquiry_ID = '")
                .Append(id).Append("'");
            string sqlText = stringBuilder.ToString();
            DataTable dt = DBHelper.ExecuteQueryDT(sqlText);
            if (dt.Rows.Count > 0)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// 更改表头信息
        /// </summary>
        /// <param name="inquiry_Table"></param>
        /// <returns></returns>
        public int updateInquiry_Table(SourceInquiry inquiry_Table)
        {
            StringBuilder stringBuilder = new StringBuilder("update Inquiry_Table set Transaction_Type = '")
                .Append(inquiry_Table.Transaction_Type).Append("', State = '")
                .Append(inquiry_Table.State).Append("', Inquiry_Path = '")
                .Append(inquiry_Table.Inquiry_Path).Append("', Offer_Time = '")
                .Append(inquiry_Table.Offer_Time).Append("'")
                .Append("where Inquiry_ID = '").Append(inquiry_Table.Source_ID).Append("'");

            string sqlText = stringBuilder.ToString();
            return DBHelper.ExecuteNonQuery(sqlText);
        }

        /// <summary>
        /// 更新询价项状态
        /// </summary>
        /// <param name="inquiry_id"></param>
        /// <param name="state"></param>
        /// <returns></returns>
        public int updateInquiryState(String inquiry_id,String state)
        {
            StringBuilder sb = new StringBuilder("update Inquiry_Table set State= '")
                .Append(state).Append("' where Inquiry_ID ='")
                .Append(inquiry_id).Append("'");
            return DBHelper.ExecuteNonQuery(sb.ToString());
        }

        public List<SourceInquiry> findSourcesByState(int state)
        {
            List<SourceInquiry> list = new List<SourceInquiry>();
            StringBuilder sb = new StringBuilder("select * from " +tableName+" where State="+state+"");
            string sqlText = sb.ToString();
            DataTable dt = DBHelper.ExecuteQueryDT(sqlText);
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    SourceInquiry inquiry = new SourceInquiry();
                    inquiry.Source_ID = dr["Source_ID"].ToString();
                    inquiry.State = int.Parse(dr["State"].ToString());
                    list.Add(inquiry);
                }
            }
            return list;
        }

        /// <summary>
        /// 更新询价单状态
        /// </summary>
        /// <param name="sourceId"></param>
        /// <param name="newState"></param>
        /// <returns></returns>
        public int updateSourceState(string sourceId, int newState)
        {
            StringBuilder sb = new StringBuilder("update " + tableName + " set ")
                    .Append("State=" + newState + " ")
                    .Append(" where Source_ID='" + sourceId + "'");
            string sqlText = sb.ToString();
            int result = DBHelper.ExecuteNonQuery(sqlText);
            return result;
        }

        public SourceInquiry findSourceById(string sourceId)
        {
            SourceInquiry sourceInquiry = new SourceInquiry();
            StringBuilder sb = new StringBuilder("select * from " + tableName + " where Source_ID = '" + sourceId + "'");
            string sqlText = sb.ToString();
            DataTable dt = DBHelper.ExecuteQueryDT(sqlText);
            if (dt != null && dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                sourceInquiry.Source_ID = sourceId;
                //sourceInquiry.Source_Type = dr["Source_Type"].ToString();
                //sourceInquiry.Create_Time = DateTime.Parse(dr["Start_Time"].ToString());
                sourceInquiry.Offer_Time = DateTime.Parse(dr["Offer_Time"].ToString());
                sourceInquiry.State = int.Parse(dr["State"].ToString());
            }
            return sourceInquiry;
        }
    }
}
