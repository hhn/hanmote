﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.IDAL.SourcingManage.SourcingManagement;
using Lib.Model.SourcingManage.SourcingManagement;
using System.Data.SqlClient;
using System.Data;

namespace Lib.SqlServerDAL.SourcingManage.SourcingManagement
{
    class LowestCostDAL : LowestCostIDAL
    {
        private string tableName = "dbo.LowestCost";
        public int addLowestCost(LowestCost ls)
        {
            StringBuilder sb = new StringBuilder("insert into "+tableName+" (SourceId,CostItem,First,Second,Third,Forth,Fifth,Sixth,Seventh,Eighth,Ninth,Tenth) values(")
                .Append("@SourceId,@CostItem,@First,@Second,@Third,@Forth,@Fifth,@Sixth,@Seventh,@Eighth,@Ninth,@Tenth)");
            SqlParameter[] sqlParas = new SqlParameter[]{
                new SqlParameter("@SourceId",SqlDbType.VarChar),
                new SqlParameter("@CostItem",SqlDbType.VarChar),
                new SqlParameter("@First",SqlDbType.Float),
                new SqlParameter("@Second",SqlDbType.Float),
                new SqlParameter("@Third",SqlDbType.Float),
                new SqlParameter("@Forth",SqlDbType.Float),
                new SqlParameter("@Fifth",SqlDbType.Float),
                new SqlParameter("@Sixth",SqlDbType.Float),
                new SqlParameter("@Seventh",SqlDbType.Float),
                new SqlParameter("@Eighth",SqlDbType.Float),
                new SqlParameter("@Ninth",SqlDbType.Float),
                new SqlParameter("@Tenth",SqlDbType.Float)
            };
            sqlParas[0].Value = ls.SourceId;
            sqlParas[1].Value = ls.CostItem;
            sqlParas[2].Value = ls.First;
            sqlParas[3].Value = ls.Second;
            sqlParas[4].Value = ls.Third;
            sqlParas[5].Value = ls.Forth;
            sqlParas[6].Value = ls.Fifth;
            sqlParas[7].Value = ls.Sixth; 
            sqlParas[8].Value = ls.Seventh;
            sqlParas[9].Value = ls.Eighth;
            sqlParas[10].Value = ls.Ninth;
            sqlParas[11].Value = ls.Tenth; 
            string sqlText = sb.ToString();
            return DBHelper.ExecuteNonQuery(sqlText, sqlParas);
        }

        public List<LowestCost> getAllLowestBySId(string sid)
        {
            List<LowestCost> list = new List<LowestCost>();
            StringBuilder sb = new StringBuilder(" select * from " +tableName+" where SourceId='"+sid+"'");
            DataTable dt = DBHelper.ExecuteQueryDT(sb.ToString());
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    LowestCost lc = new LowestCost();
                    lc.SourceId = dr["SourceId"].ToString();
                    lc.CostItem = dr["CostItem"].ToString();
                    lc.First = float.Parse(dr["First"].ToString());
                    lc.Second = float.Parse(dr["Second"].ToString());
                    lc.Third = float.Parse(dr["Third"].ToString());
                    lc.Forth = float.Parse(dr["Forth"].ToString());
                    lc.Fifth = float.Parse(dr["Fifth"].ToString());
                    lc.Sixth = float.Parse(dr["Sixth"].ToString());
                    lc.Seventh = float.Parse(dr["Seventh"].ToString());
                    lc.Eighth = float.Parse(dr["Eighth"].ToString());
                    lc.Ninth = float.Parse(dr["Ninth"].ToString());
                    lc.Tenth = float.Parse(dr["Tenth"].ToString());
                    list.Add(lc);
                }
            }
          
            return list;
        }
    }
}
