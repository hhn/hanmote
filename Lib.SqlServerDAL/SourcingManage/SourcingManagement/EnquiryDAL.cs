﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Lib.IDAL.SourcingManage;
using Lib.Model.SourcingManage.SourcingManagement;
using System.IO;
using Lib.IDAL.SourcingManage.SourcingManagement;

namespace Lib.SqlServerDAL.SourcingManage.SourcingManagement
{
    public class EnquiryDAL : EnquiryIDAL

    {
        private string tableName = "";

        /// <summary>
        /// 构造函数，设为私有
        /// </summary>
        public EnquiryDAL()
        {
            tableName = "dbo.Enquiry";
        }

        /// <summary>
        /// 保存新的询价单表头
        /// </summary>
        /// <param name="inquiry_Table"></param>
        /// <returns></returns>
        public int addNewEnquiry(List<Enquiry> enquiryList,SourceInquiry inquiry)
        {

            LinkedList<string> sqlList = new LinkedList<string>();
            #region 更新询价单表头状态信息

            StringBuilder strBui = new StringBuilder("update Inquiry_Table set State = '")
                .Append(inquiry.State).Append("' ")
                .Append("where Inquiry_ID = '").Append(inquiry.Source_ID).Append("'");
            sqlList.AddLast(strBui.ToString());

            #endregion

            #region 删除旧的附件信息

            strBui = new StringBuilder("delete from ")
                .Append(tableName).Append(" where Inquiry_ID = '")
                .Append(inquiry.Source_ID).Append("'");
            sqlList.AddLast(strBui.ToString());

            #endregion

            #region 插入新的询价信息

            foreach (Enquiry enquiry in enquiryList)
            {
                strBui = new StringBuilder("insert into ")
                    .Append(tableName + "([Inquiry_ID],[Material_ID],[Supplier_ID],[Number],[Price],[Clause_ID],[Delivery_Time],[Factory_ID],[Stock_ID],[Condition_ID],[State],[Update_Time],[demand_Id])VALUES('")
                    .Append(enquiry.Inquiry_ID).Append("', '")
                    .Append(enquiry.Material_ID).Append("', '")
                    .Append(enquiry.Supplier_ID).Append("', ")
                    .Append(enquiry.Number).Append(", '")
                    .Append(enquiry.Price).Append("', '")
                    .Append(enquiry.Clause_ID).Append("', '")
                    .Append(DBNull.Value).Append("', '")
                    .Append(enquiry.Factory_ID).Append("', '")
                    .Append(enquiry.Stock_ID).Append("', '")
                    .Append(enquiry.Condition_ID.ToString()).Append("', '")
                    .Append(enquiry.State).Append("', '")
                    .Append(enquiry.Update_Time.ToString()).Append("', '")
                    .Append(enquiry.Demand_id).Append("') ");
                sqlList.AddLast(strBui.ToString());
            }
            
            #endregion

            return DBHelper.ExecuteNonQuery(sqlList);
        }

        /// <summary>
        /// 查询已存在的表头信息
        /// </summary>
        /// <param name="demand_ID"></param>
        /// <returns></returns>
        public SourceInquiry findInquiryTable(Dictionary<string, string> conditions, String table)
        {
            SourceInquiry result = new SourceInquiry();

            StringBuilder stringBuilder = new StringBuilder("select * from ");
            stringBuilder.Append(table).Append(" where ");
            bool isfirst = true;
            foreach (KeyValuePair<string, string> kvPair in conditions)
            {
                string key = kvPair.Key;
                string value = kvPair.Value;
                if(isfirst)
                    stringBuilder.Append(key).Append(" = '").Append(value).Append("'");
                else
                    stringBuilder.Append(" and ").Append(key).Append(" = '").Append(value).Append("'");
            }
            string sqlText = stringBuilder.ToString();
            DataTable dt = DBHelper.ExecuteQueryDT(sqlText);
            if (dt.Rows.Count > 0)
            {
                DataRow row = dt.Rows[0];
                result.Source_ID = row["Inquiry_ID"].ToString();
                result.Transaction_Type = row["Transaction_Type"].ToString();
                result.Offer_Time = DateTime.Parse(row["Offer_Time"].ToString());
                result.State = int.Parse(row["State"].ToString());
                result.Inquiry_Path = row["Inquiry_Path"].ToString();

                return result;
            }
            else
                return null;
        }


        /// <summary>
        /// 查询已存在的表头信息
        /// </summary>
        /// <param name="demand_ID"></param>
        /// <returns></returns>
        public List<SourceInquiry> findInquiryList(Dictionary<string, string> conditions, String table)
        {
            List<SourceInquiry> result = new List<SourceInquiry>();

            StringBuilder stringBuilder = new StringBuilder("select * from ");
            stringBuilder.Append(table);
            if (conditions.Count != 0)
                stringBuilder.Append(" where ");
            bool isfirst = true;
            foreach (KeyValuePair<string, string> kvPair in conditions)
            {
                string key = kvPair.Key;
                string value = kvPair.Value;
                if (isfirst && key.Length > 1)
                {
                    stringBuilder.Append(key).Append(" = '").Append(value).Append("' ");
                    isfirst = false;
                }
                else if (isfirst && key.Length <= 1)
                {
                    stringBuilder.Append(value);
                    isfirst = false;
                }
                else if (key.Length > 1)
                    stringBuilder.Append(" and ").Append(key).Append(" = '").Append(value).Append("'");
                else
                    stringBuilder.Append(" and ").Append(value);
            }
            stringBuilder.Append(" order by Create_Time desc");
            string sqlText = stringBuilder.ToString();
            DataTable dt = DBHelper.ExecuteQueryDT(sqlText);
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    SourceInquiry inquiry = new SourceInquiry();
                    inquiry.Source_ID = row["Inquiry_ID"].ToString();
                    inquiry.Transaction_Type = row["Transaction_Type"].ToString();
                    inquiry.Offer_Time = DateTime.Parse(row["Offer_Time"].ToString());
                    inquiry.State = int.Parse(row["State"].ToString());
                    inquiry.Inquiry_Path = row["Inquiry_Path"].ToString();

                    result.Add(inquiry);
                }
                return result;
            }
            else
                return null;
        }

        /// <summary>
        /// 根据询价单号查询已存在的询价单信息
        /// </summary>
        /// <param name="demand_ID"></param>
        /// <returns></returns>
        public List<Enquiry> findEnquiry(Dictionary<string, string> conditions, String table)
        {
            List<Enquiry> result = new List<Enquiry>();

            StringBuilder stringBuilder = new StringBuilder("select * from ");
            stringBuilder.Append(table).Append(" where ");
            bool isfirst = true;
            foreach (KeyValuePair<string, string> kvPair in conditions)
            {
                string key = kvPair.Key;
                string value = kvPair.Value;
                if (isfirst)
                {
                    stringBuilder.Append(key).Append(" = '").Append(value).Append("' ");
                    isfirst = false;
                } 
                else if(!key.Equals(""))
                    stringBuilder.Append(" and ").Append(key).Append(" = '").Append(value).Append("'");
                else 
                    stringBuilder.Append(" and ").Append(value);
            }
            stringBuilder.Append(" order by Update_Time desc");
            string sqlText = stringBuilder.ToString();
            DataTable dt = DBHelper.ExecuteQueryDT(sqlText);
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    Enquiry enquiry = new Enquiry();
                    enquiry.Inquiry_ID = row["Inquiry_ID"].ToString();
                    enquiry.Material_ID = row["Material_ID"].ToString();
                    enquiry.Supplier_ID = row["Supplier_ID"].ToString();
                    enquiry.Number = Convert.ToInt32(row["Number"].ToString());
                    enquiry.Price = row["Price"].ToString();
                    enquiry.Clause_ID = row["Clause_ID"].ToString();
                    enquiry.Delivery_Time = DateTime.Parse(row["Delivery_Time"].ToString());
                    enquiry.Factory_ID = row["Factory_ID"].ToString();
                    enquiry.Stock_ID = row["Stock_ID"].ToString();
                    enquiry.Condition_ID = row["Condition_ID"].ToString();
                    enquiry.State = row["State"].ToString();
                    enquiry.Update_Time = DateTime.Parse(row["Update_Time"].ToString());
                    enquiry.Demand_id = row["demand_Id"].ToString();
                    result.Add(enquiry);
                }
                return result;
            }
            else
                return result;
        }

        /// <summary>
        /// 更改报价信息
        /// </summary>
        /// <param name="inquiry_Table"></param>
        /// <returns></returns>
        public int updateEnquiryOffer(List<Enquiry> enquiryList, SourceInquiry inquiry)
        {

            LinkedList<string> sqlList = new LinkedList<string>();
            #region 更新询价单表头状态信息

            StringBuilder strBui = new StringBuilder("update Inquiry_Table set State = '")
                .Append(inquiry.State).Append("' ")
                .Append("where Inquiry_ID = '").Append(inquiry.Source_ID ).Append("'");
            sqlList.AddLast(strBui.ToString());

            #endregion

            #region 更改报价信息

            foreach (Enquiry enquiry in enquiryList)
            {
                strBui = new StringBuilder("update Enquiry set Price = '")
                    .Append(enquiry.Price).Append("', Delivery_Time ='")
                    .Append(DBNull.Value).Append("', Clause_ID='")
                    .Append(enquiry.Clause_ID).Append("', Condition_ID='")
                    .Append(enquiry.Condition_ID).Append("', State='")
                    .Append(enquiry.State).Append("', Update_Time='")
                    .Append(enquiry.Update_Time.ToString()).Append("' where Inquiry_ID='")
                    .Append(enquiry.Inquiry_ID).Append("' and Supplier_ID='")
                    .Append(enquiry.Supplier_ID).Append("'and Material_ID='")
                    .Append(enquiry.Material_ID).Append("'");
                sqlList.AddLast(strBui.ToString());
            }

            #endregion

            return DBHelper.ExecuteNonQuery(sqlList);
        }

        /// <summary>
        /// 查询报价单某状态数量
        /// </summary>
        /// <param name="inquiry_Table"></param>
        /// <returns></returns>
        public int enquiryCount(Enquiry enquiry)
        {
            StringBuilder stringBuilder = new StringBuilder("select * from Enquiry where Inquiry_ID='")
                    .Append(enquiry.Inquiry_ID).Append("' and State ='")
                    .Append("待报价'");
            DataTable dt = DBHelper.ExecuteQueryDT(stringBuilder.ToString());

            return dt.Rows.Count;
        }

        public DataTable FindSupplierById(String itemId)
        {
            StringBuilder stringBuilder = new StringBuilder("select * from Supplier_Base where supplier_ID = '")
                         .Append(itemId).Append(" '");
            DataTable dt = DBHelper.ExecuteQueryDT(stringBuilder.ToString());
            return dt;
        }
    }
}
