﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.Model.SourcingManage.ProcurementPlan;
using Lib.IDAL.SourcingManage.ProcurementPlan;
using System.Data.SqlClient;

namespace Lib.SqlServerDAL.SourcingManage.ProcurementPlan
{
    class Demand_MaterialDAL:Demand_MaterialIDAL
    {
        private string tableName = "dbo.Demand_Material";

        public Demand_Material findDemandMaterial(string demandId, string materialId)
        {
            
            StringBuilder sb = new StringBuilder(" select * from "+tableName+" where Demand_ID='"+demandId+"' and Material_ID='"+materialId+"'");
            string sqlText = sb.ToString();
            DataTable dt = DBHelper.ExecuteQueryDT(sqlText);
            if (dt!=null&& dt.Rows.Count>0)
            {
                DataRow dr = dt.Rows[0];
                Demand_Material demandmaterial = new Demand_Material();
                demandmaterial.Demand_ID = demandId;
                demandmaterial.Material_ID = materialId;
                demandmaterial.Material_Name = dr["Material_Name"].ToString();
                demandmaterial.Material_Group = dr["Material_Group"].ToString();
                demandmaterial.Demand_Count = Convert.ToInt32(dr["Demand_Count"].ToString());
                demandmaterial.Measurement = dr["Measurement"].ToString();
                demandmaterial.DeliveryStartTime = DateTime.Parse(dr["DeliveryStartTime"].ToString());
                demandmaterial.DeliveryEndTime = DateTime.Parse(dr["DeliveryEndTime"].ToString());
                return demandmaterial;
            }
            return null;
        }
        /// <summary> 
        /// 添加需求计划对应的物料
        /// </summary>
        /// <param name="demandMaterial"></param>
        /// <returns></returns>
        public int addSummaryMaterials(Demand_Material demandMaterial)
        {
            //查找是否存在
            Demand_Material demandmaterial = findDemandMaterial(demandMaterial.Demand_ID, demandMaterial.Material_ID);
            int tmp = 0;
            if (demandmaterial != null)
            {
                tmp = deleteDemandMaterial(demandmaterial.Demand_ID, demandmaterial.Material_ID);
            }
            StringBuilder sb = new StringBuilder("insert into " + tableName + "(");
            sb.Append(" Demand_ID,Material_ID,Material_Name,Material_Group,Demand_Count,Measurement,Stock_ID,Factory_ID,DeliveryStartTime,DeliveryEndTime) values(@Demand_ID,@Material_ID,@Material_Name,@Material_Group,@Demand_Count,@Measurement,@Stock_ID,@Factory_ID,@DeliveryStartTime,@DeliveryEndTime)");
            SqlParameter[] sqlParas = new SqlParameter[]
           {
                new SqlParameter("@Demand_ID",SqlDbType.VarChar),
                new SqlParameter("@Material_ID",SqlDbType.VarChar),
                new SqlParameter("@Material_Name",SqlDbType.VarChar),
                new SqlParameter("@Material_Group",SqlDbType.VarChar),
                new SqlParameter("@Demand_Count",SqlDbType.Int),
                new SqlParameter("@Measurement",SqlDbType.VarChar),
                new SqlParameter("@Stock_ID",SqlDbType.VarChar),
                new SqlParameter("@Factory_ID",SqlDbType.VarChar),
                new SqlParameter("@DeliveryStartTime",SqlDbType.DateTime),
                new SqlParameter("@DeliveryEndTime",SqlDbType.DateTime)
           };


            sqlParas[0].Value = demandMaterial.Demand_ID;
            sqlParas[1].Value = demandMaterial.Material_ID;
            sqlParas[2].Value = demandMaterial.Material_Name;
            sqlParas[3].Value = demandMaterial.Material_Group;
            sqlParas[4].Value = demandMaterial.Demand_Count;
            sqlParas[5].Value = demandMaterial.Measurement;
            sqlParas[6].Value = demandMaterial.Stock_ID;
            sqlParas[7].Value = demandMaterial.Factory_ID;
            sqlParas[8].Value = demandMaterial.DeliveryStartTime;
            sqlParas[9].Value = demandMaterial.DeliveryEndTime;
            string sqlText = sb.ToString();
            return DBHelper.ExecuteNonQuery(sqlText,sqlParas);
        }

        /// <summary>
        /// 根据需求单号查找对应的所有物料
        /// </summary>
        /// <param name="demandid"></param>
        /// <returns></returns>
        public List<Demand_Material> findDemandMaterials(string demandid)
        {
            List<Demand_Material> demandMaterialList = new List<Demand_Material>();
            string sql = "select * from " + tableName + " where Demand_ID='" + demandid + "'";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        Demand_Material demandmaterial = new Demand_Material();
                        demandmaterial.Material_ID = dr["Material_ID"].ToString();
                        demandmaterial.Material_Name = dr["Material_Name"].ToString();
                        demandmaterial.Material_Group = dr["Material_Group"].ToString();
                        demandmaterial.Demand_Count = Convert.ToInt32(dr["Demand_Count"].ToString());
                        demandmaterial.Measurement = dr["Measurement"].ToString();
                        demandmaterial.Factory_ID = dr["Factory_ID"].ToString();
                        demandmaterial.Stock_ID = dr["Stock_ID"].ToString();
                        demandmaterial.DeliveryStartTime = DateTime.Parse(dr["DeliveryStartTime"].ToString());
                        demandmaterial.DeliveryEndTime = DateTime.Parse(dr["DeliveryEndTime"].ToString());
                        demandMaterialList.Add(demandmaterial);
                    }
                }
            }
            return demandMaterialList;
        }
    
        /// <summary>
        /// 根据需求计划号，物料编号删除需求-物料记录
        /// </summary>
        /// <param name="demandId"></param>
        /// <param name="materialId"></param>
        /// <returns></returns>
        public int deleteDemandMaterial(string demandId, string materialId)
        {
            StringBuilder sb = new StringBuilder("delete from " + tableName);
            if(demandId!=null){
                sb.Append(" where Demand_ID='"+demandId+"'");
            }
            if (materialId != null)
            {
                sb.Append("and Material_ID='"+materialId+"'");
            }
            string sqlText = sb.ToString();
            int result = DBHelper.ExecuteNonQuery(sqlText);
            return result;
        }

        /// <summary>
        /// 查找未汇总的需求单号
        /// </summary>
        /// <returns></returns>
        public List<Demand_Material> findDemandMaterialNonAggregate(List<string> demandIDs)
        {
            List<Demand_Material> demandMaterialList = new List<Demand_Material>();
            StringBuilder sb = new StringBuilder("select * from " + tableName + " where Aggregate_ID is null and Demand_ID in(");
            for (int i = 0; i < demandIDs.Count; i++)
            {
                if (i < demandIDs.Count - 1)
                {
                    sb.Append("'" + demandIDs.ElementAt(i) + "',");
                }
                else
                {
                    sb.Append("'" + demandIDs.ElementAt(i) + "')");
                }
            }
            string sqlText = sb.ToString();
            DataTable dt = DBHelper.ExecuteQueryDT(sqlText);
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    Demand_Material demandmaterial = new Demand_Material();
                    demandmaterial.Material_ID = dr["Material_ID"].ToString();
                    demandmaterial.Material_Name = dr["Material_Name"].ToString();
                    demandmaterial.Material_Group = dr["Material_Group"].ToString();
                    demandmaterial.Demand_Count = Convert.ToInt32(dr["Demand_Count"].ToString());
                    demandmaterial.Measurement = dr["Measurement"].ToString();
                    demandmaterial.Factory_ID = dr["Factory_ID"].ToString();
                    demandmaterial.Stock_ID = dr["Stock_ID"].ToString();
                    demandMaterialList.Add(demandmaterial);
                }
            }
            return demandMaterialList;
        }

        /// <summary>
        /// 更新需求-物料的汇总编号
        /// </summary>
        /// <param name="am"></param>
        /// <returns></returns>
        public int updateDemandMaterialAggregateID(Aggregate_Material am,List<string> demandIDs){
            StringBuilder sb = new StringBuilder("update " + tableName + " set")
                          .Append(" Aggregate_ID='" + am.Aggregate_ID + "'")
                          .Append(" where Aggregate_ID is null and Material_ID='" + am.Material_ID + "' and Demand_ID in(");
            for (int i = 0; i < demandIDs.Count; i++)
            {
                if (i < demandIDs.Count - 1)
                {
                    sb.Append("'" + demandIDs.ElementAt(i) + "',");
                }
                else
                {
                    sb.Append("'" + demandIDs.ElementAt(i) + "')");
                }
            }
            string sqlText = sb.ToString();
            int result = DBHelper.ExecuteNonQuery(sqlText);
            return result;
        }

        public int updateSummaryMaterials(Demand_Material demand_material,string materialId)
        {
            string stime = demand_material.DeliveryStartTime.ToString("yyyy-MM-dd hh:mm:ss");
            string etime = demand_material.DeliveryEndTime.ToString("yyyy-MM-dd hh:mm:ss");
            StringBuilder sb = new StringBuilder("update " + tableName + " set ")
                .Append("Demand_Count=" + demand_material.Demand_Count+",")
                .Append("DeliveryStartTime='"+stime+"',")
                .Append("DeliveryEndTime='"+etime+"'")
                .Append(" where Demand_ID='"+demand_material.Demand_ID+"' and Material_ID='"+materialId+"'");
            string sqlText = sb.ToString();
            int result = DBHelper.ExecuteNonQuery(sqlText);
            return result;
        }

    }   
}
