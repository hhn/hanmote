﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.Model.SourcingManage.ProcurementPlan;
using Lib.IDAL.SourcingManage.ProcurementPlan;

namespace Lib.SqlServerDAL.SourcingManage.ProcurementPlan
{
    class Aggregate_MaterialDAL:Aggregate_MaterialIDAL
    {
        private string tableName = "dbo.Aggregate_Material";

        /// <summary>
        /// 插入汇总计划
        /// </summary>
        /// <param name="am"></param>
        /// <returns></returns>
        public int addAggregateMaterial(Aggregate_Material am)
        {
            StringBuilder sb = new StringBuilder("insert into " + tableName +"(");
            //Aggregate_ID,
            sb.Append("Material_ID,Material_Name,Material_Group,Aggregate_Count,Measurement,Create_Time) values(")
               // .Append("'" + am.Aggregate_ID + "',")
                .Append("'" + am.Material_ID + "',")
                .Append("'" + am.Material_Name + "',")
                .Append("'" + am.Material_Group + "',")
                .Append(am.Aggregate_Count + ",")
                .Append("'" + am.Measurement + "',")
                .Append("'" + am.Create_Time + "')");
            string sqlText = sb.ToString();
            int result = DBHelper.ExecuteNonQuery(sqlText);
            return result;
        }

        /// <summary>
        /// 获取所有汇总信息
        /// </summary>
        /// <returns></returns>
        public List<Aggregate_Material> getAllAggregates()
        {
            List<Aggregate_Material> list = new List<Aggregate_Material>();
            StringBuilder sb = new StringBuilder("select * from "+tableName);
            string sqlText = sb.ToString();
            DataTable dt = DBHelper.ExecuteQueryDT(sqlText);
            if (dt != null && dt.Rows.Count>0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    Aggregate_Material am = new Aggregate_Material();
                    am.Aggregate_ID = dr["Aggregate_ID"].ToString();
                    am.Material_ID = dr["Material_ID"].ToString();
                    am.Material_Name = dr["Material_Name"].ToString();
                    am.Material_Group = dr["Material_Group"].ToString();
                    am.Aggregate_Count = Convert.ToInt32(dr["Aggregate_Count"]);
                    am.Measurement = dr["Measurement"].ToString();
                    am.Create_Time = DateTime.Parse(dr["Create_Time"].ToString());
                    list.Add(am);
                }
            }
            return list;
        }

    }   
}
