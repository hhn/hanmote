﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Lib.IDAL.SourcingManage.ProcurementPlan;
using Lib.Model.SourcingManage.ProcurementPlan;
using Lib.Common.MMCException.IDAL;
using Lib.Common.MMCException.Bll;

namespace Lib.SqlServerDAL.SourcingManage.ProcurementPlan
{

    /// <summary>
    /// 采购计划类操作
    /// </summary>
    public partial class Summary_DemandDAL : Summary_DemandIDAL
    {
        
        private string tableName = "";

        SqlParameter[] sqlParas = new SqlParameter[]{
                new SqlParameter("@Demand_ID", SqlDbType.VarChar),
                new SqlParameter("@Material_ID", SqlDbType.VarChar),
                new SqlParameter("@Material_Name", SqlDbType.VarChar),
                new SqlParameter("@Purchase_Type", SqlDbType.VarChar),
                new SqlParameter("@Material_Type", SqlDbType.VarChar),
                new SqlParameter("@Stock_ID", SqlDbType.VarChar),
                new SqlParameter("@Factory_ID", SqlDbType.VarChar),
                new SqlParameter("@Mini_Purchase_Count", SqlDbType.Int),
                new SqlParameter("@Demand_Count", SqlDbType.Int),
                new SqlParameter("@Proposer_ID", SqlDbType.VarChar),
                new SqlParameter("@Supplier_ID", SqlDbType.VarChar),
                new SqlParameter("@Contract_ID", SqlDbType.VarChar),
                new SqlParameter("@Remarks", SqlDbType.VarChar),
                new SqlParameter("@State", SqlDbType.VarChar),
                new SqlParameter("@Purchase_Price", SqlDbType.VarChar),
                new SqlParameter("@Inquiry_ID", SqlDbType.VarChar),
                new SqlParameter("@Follow_Certificate", SqlDbType.VarChar),
                new SqlParameter("@Create_Time", SqlDbType.DateTime),
                new SqlParameter("@Update_Time", SqlDbType.DateTime),
                new SqlParameter("@PhoneNum", SqlDbType.VarChar),
                new SqlParameter("@Department",SqlDbType.VarChar),
                new SqlParameter("@LogisticsMode",SqlDbType.VarChar),
                new SqlParameter("@Measurement",SqlDbType.VarChar)
            };

        /// <summary>
        /// 构造函数，设为私有
        /// </summary>
        public Summary_DemandDAL()
        {
            tableName = "dbo.Summary_Demand";
        }

        public Summary_Demand findSummaryDemandByDemandID(string demandId)
        {
            Summary_Demand demand = new Summary_Demand();
            StringBuilder sb = new StringBuilder("select * from " + tableName)
                .Append(" where Demand_ID = '" + demandId + "'");
            string sqlText = sb.ToString();
            DataTable dt = DBHelper.ExecuteQueryDT(sqlText);
            if(dt.Rows.Count>0){
                DataRow dr = dt.Rows[0];
                demand.Demand_ID = dr["Demand_ID"].ToString();
                demand.Purchase_Type = dr["Purchase_Type"].ToString();
                demand.LogisticsMode = dr["LogisticsMode"].ToString();
                demand.Factory_ID = dr["Factory_ID"].ToString();
                demand.Department = dr["Department"].ToString();
                demand.Proposer_ID = dr["Proposer_ID"].ToString();
                demand.PhoneNum = dr["PhoneNum"].ToString();
                demand.Create_Time = DateTime.Parse(dr["Create_Time"].ToString());
                demand.State = dr["state"].ToString();
                demand.ReviewAdvice = dr["ReviewAdvice"].ToString();
            } 
            return demand;
        }

        /// <summary>
        /// 增加新建标准需求计划
        /// </summary>
        /// <param name="summary_Demand"></param>
        /// <returns></returns>
        public int addNewStandard(Summary_Demand summary_Demand)
        {
            StringBuilder stringBuilder = new StringBuilder("insert into ");
            stringBuilder.Append(tableName + "(Demand_ID, Purchase_Type,State,Create_Time,PhoneNum,Department,LogisticsMode) values(");
            stringBuilder.Append("@Demand_ID, @Purchase_Type, @State,@Create_Time,@PhoneNum,@Department,@LogisticsMode)");
            SqlParameter[] sqlParam = new SqlParameter[]{
                new SqlParameter("@Demand_ID", SqlDbType.VarChar),
                new SqlParameter("@Purchase_Type", SqlDbType.VarChar), 
                new SqlParameter("@State", SqlDbType.VarChar),   
                new SqlParameter("@Create_Time", SqlDbType.DateTime), 
                new SqlParameter("@PhoneNum", SqlDbType.VarChar),
                new SqlParameter("@Department",SqlDbType.VarChar),
                new SqlParameter("@LogisticsMode",SqlDbType.VarChar)
            };
            sqlParas[0].Value = summary_Demand.Demand_ID;  
            sqlParas[1].Value = summary_Demand.Purchase_Type;  
            sqlParas[2].Value = summary_Demand.State;
            sqlParas[3].Value = summary_Demand.Create_Time;
            sqlParas[4].Value = summary_Demand.PhoneNum;
            sqlParas[5].Value = summary_Demand.Department;
            sqlParas[6].Value = summary_Demand.LogisticsMode; 

            string sqlText = stringBuilder.ToString();
            return DBHelper.ExecuteNonQuery(sqlText, sqlParas);
        }

        /// <summary>
        /// 获取所有采购计划数据
        /// </summary>
        /// <returns></returns>
        public DataTable GetAllSummaryDemand()
        {
            string sql = "select Demand_ID ,Purchase_Type,LogisticsMode,Material_Name,Mini_Purchase_Count,Proposer_ID,Create_Time,ReviewTime,ReviewAdvice from Summary_Demand";
            return DBHelper.ExecuteQueryDT(sql);
        }

        /// <summary>
        /// 修改标准需求计划
        /// </summary>
        /// <param name="summary_Demand"></param>
        /// <returns></returns>
        public int editNewStandard(Summary_Demand summary_Demand)
        {
            StringBuilder stringBuilder = new StringBuilder("update " + tableName + " set ");
            stringBuilder.Append(" Material_ID= '" + summary_Demand.Material_ID + "',"
                + " Material_Name= '" + summary_Demand.Material_Name + "',"
                + " Purchase_Type= '" + summary_Demand.Purchase_Type + "',"
                + " Material_Type= '" + summary_Demand.Material_Type + "',"
                + " Stock_ID= '" + summary_Demand.Stock_ID + "',"
                + " Factory_ID= '" + summary_Demand.Factory_ID + "',"
                + " Mini_Purchase_Count= " + summary_Demand.Mini_Purchase_Count + ","
                + " Demand_Count= " + summary_Demand.Demand_Count + ","
                + " Proposer_ID= '" + summary_Demand.Proposer_ID + "',"
                + " Supplier_ID= '" + summary_Demand.Supplier_ID + "',"
                + " Contract_ID= '" + summary_Demand.Contract_ID + "',"
                + " Remarks= '" + summary_Demand.Remarks + "',"
                + " State= '" + summary_Demand.State + "',"
                + " Update_Time= '" + summary_Demand.Update_Time + "'"
                + " where  Demand_ID= '" +summary_Demand.Demand_ID+ "'"
                );
            string sqlText = stringBuilder.ToString();
            int dt = DBHelper.ExecuteNonQuery(sqlText);
            return dt;
        }

        public int editDemandById(String demand_Id, String supplier_Id, String inquiry_Id)
        {
            StringBuilder stringBuilder = new StringBuilder("update " + tableName + " set ");
            stringBuilder.Append(" Supplier_ID= '" + supplier_Id + "',"
                + " Inquiry_ID='" + inquiry_Id + "'"
                + " where Demand_ID= '" + demand_Id + "'");
            string sqlText = stringBuilder.ToString();
            int result = DBHelper.ExecuteNonQuery(sqlText);
            return result;
                
        }

        /// <summary>
        /// 查询是否已存在相同的需求单号
        /// </summary>
        /// <param name="demand_ID"></param>
        /// <returns></returns>
        public Boolean SameStandardByDemand_ID(string demand_ID)
        {
            StringBuilder stringBuilder = new StringBuilder("select * from ");
            stringBuilder.Append(tableName + " where Demand_ID = '" + demand_ID + "'");
            string sqlText = stringBuilder.ToString();
            DataTable dt = DBHelper.ExecuteQueryDT(sqlText);
            if (dt.Rows.Count > 0){
                return true;
            }
            else{
                return false;
            }
        }

        /// <summary>
        /// 查询已存在的需求单号
        /// </summary>
        /// <param name="demand_ID"></param>
        /// <returns></returns>
        public DataTable FindStandardDemand_ID(String itemName)
        {
            StringBuilder stringBuilder = new StringBuilder("select distinct ");
            stringBuilder.Append(itemName + " as id  from " + tableName );
            if (itemName.Equals("state")) 
            {
                stringBuilder.Append(" where state != '0' ");
            }
            string sqlText = stringBuilder.ToString();
            DataTable dt = DBHelper.ExecuteQueryDT(sqlText);
            if (dt.Rows.Count > 0)
            {
                    return dt;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 查询最新的需求单号
        /// </summary>
        /// <param name="demand_ID"></param>
        /// <returns></returns>
        public DataTable FindStandardDemand_ID()
        {
            StringBuilder stringBuilder = new StringBuilder("select top 1 * from ");
            stringBuilder.Append(tableName + " order by Create_Time desc");
            string sqlText = stringBuilder.ToString();
            DataTable dt = DBHelper.ExecuteQueryDT(sqlText);
            if (dt.Rows.Count > 0)
            {
                return dt;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 查询需求单号
        /// </summary>
        /// <param name="demand_ID"></param>
        /// <returns></returns>
        public DataTable StandardByWLBH(String item1, String item2, String item3)
        {
            StringBuilder stringBuilder = new StringBuilder("select * from ");
            stringBuilder.Append(tableName);
            if (!string.IsNullOrWhiteSpace(item1) || !string.IsNullOrWhiteSpace(item2) || !string.IsNullOrWhiteSpace(item3))
            {
                //stringBuilder.Append(" and ");
                stringBuilder.Append(" where ");
                if (!string.IsNullOrWhiteSpace(item1))
                {
                    stringBuilder.Append(item1);
                    if (!string.IsNullOrWhiteSpace(item2))
                    {
                        stringBuilder.Append(" and " + item2);
                        if (!string.IsNullOrWhiteSpace(item3))
                        {
                            stringBuilder.Append(" and " + item3);
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrWhiteSpace(item3))
                        {
                            stringBuilder.Append(" and " + item3);
                        }
                    }
                }
                else
                {
                    if (!string.IsNullOrWhiteSpace(item2))
                    {
                        stringBuilder.Append(item2);
                        if (!string.IsNullOrWhiteSpace(item3))
                        {
                            stringBuilder.Append(" and " + item3);
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrWhiteSpace(item3))
                        {
                            stringBuilder.Append(item3);
                        }
                    }
                }
            }
            stringBuilder.Append(" order by Create_Time desc");
            string sqlText = stringBuilder.ToString();
            
            DataTable dt = DBHelper.ExecuteQueryDT(sqlText);
            if (dt.Rows.Count > 0)
            {
                if (item3!=null && item3.Equals(" state = '0'"))
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        dt.Rows[i][14] = "已删除";
                    }
                }
                return dt;
            }
            else
            {
                return null;
            }
        }


        /// <summary>
        /// 更改采购计划状态
        /// </summary>
        /// <param name="demand_ID"></param>
        /// <returns></returns>
        public int changeState(String state, String demand_ID)
        {
            StringBuilder stringBuilder = new StringBuilder("update ");
            stringBuilder.Append(tableName + " set state = '" + state + "' where Demand_ID = '" + demand_ID + "'");
            string sqlText = stringBuilder.ToString();
            int dt = DBHelper.ExecuteNonQuery(sqlText);
            return dt;
        }

        public int changeReviewTime(String reviewTime, String demand_ID)
        {
            StringBuilder stringBuilder = new StringBuilder("update ");
            stringBuilder.Append(tableName + " set reviewTime = '" + reviewTime + "' where Demand_ID = '" + demand_ID + "'");
            string sqlText = stringBuilder.ToString();
            int dt = DBHelper.ExecuteNonQuery(sqlText);
            return dt;
          
        }


        /// <summary>
        /// 更改采购单号状态
        /// </summary>
        /// <param name="demand_ID"></param>
        /// <returns></returns>
        public int changePrice(String price, String demand_ID)
        {
            StringBuilder stringBuilder = new StringBuilder("update ");
            stringBuilder.Append(tableName + " set Purchase_Price = '" + price + "' where Demand_ID = '" + demand_ID + "'");
            string sqlText = stringBuilder.ToString();
            int dt = DBHelper.ExecuteNonQuery(sqlText);
            return dt;
        }

        /// <summary>
        /// 查询物料编号对应的物料信息
        /// </summary>
        /// <param name="demand_ID"></param>
        /// <returns></returns>
        public DataTable FindMaterial(String itemName, String table)
        {
            StringBuilder stringBuilder = new StringBuilder("select * from ");
            stringBuilder.Append(table + " where Material_ID = '" + itemName +"'");
            string sqlText = stringBuilder.ToString();
            DataTable dt = DBHelper.ExecuteQueryDT(sqlText);
            if (dt.Rows.Count > 0)
            {
                return dt;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 查询已存在的物料信息
        /// </summary>
        /// <param name="demand_ID"></param>
        /// <returns></returns>
        public DataTable FindMaterialItems(String itemName, String table)
        {
            StringBuilder stringBuilder = new StringBuilder("select distinct ");
            stringBuilder.Append(itemName + " from " + table);
            string sqlText = stringBuilder.ToString();
            DataTable dt = DBHelper.ExecuteQueryDT(sqlText);
            if (dt.Rows.Count > 0)
            {
                return dt;
            }
            else
            {
                return null;
            }
        }
        
        public List<String> FindAddItems(String value ,String key ,String itemName, String tableName)
        {
            StringBuilder stringBuilder = new StringBuilder("select distinct ");
            stringBuilder.Append(itemName + " from " + tableName);
            if (value.Length > 0 && key.Length>0)
            {
                stringBuilder.Append(" where " + key + " = '" + value + "'");
            }
            string sqlText = stringBuilder.ToString();
            DataTable dt = DBHelper.ExecuteQueryDT(sqlText);
            if (dt.Rows.Count > 0)
            {
                List<string> list = new List<string>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    list.Add(dt.Rows[i][0].ToString());
                }
                return list;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 根据需要加载items至combobox
        /// </summary>
        /// <param name="demand_ID"></param>
        /// <returns></returns>
        public DataTable AddItemsToCombobox(String itemName, String table)
        {
            StringBuilder stringBuilder = new StringBuilder("select distinct ");
            stringBuilder.Append(itemName + " from " + table);
            string sqlText = stringBuilder.ToString();
            DataTable dt = DBHelper.ExecuteQueryDT(sqlText);
            if (dt.Rows.Count > 0)
            {
                return dt;
            }
            else
            {
                return null;
            }
        }
         
        /// <summary>
        /// 新建条件类型
        /// </summary>
        /// <param name="summary_Demand"></param>
        /// <returns></returns>
        public int addNewPrice_Condition(String table,String id, String Condition, String Price_Level, String Rounding_Rule, Boolean Sign, float Value_Rate)
        {
            StringBuilder stringBuilder = new StringBuilder("insert into ");
            stringBuilder.Append(table + " ([Demand_ID],[Condition],[Price_Level],[Rounding_Rule],[Sign],[Value_Rate]) values(");
            stringBuilder.Append("@1, @2, @3, @4, @5, @6)");
            SqlParameter[] sqlParas = new SqlParameter[]{
                new SqlParameter("@1", SqlDbType.VarChar),
                new SqlParameter("@2", SqlDbType.VarChar),
                new SqlParameter("@3", SqlDbType.VarChar),
                new SqlParameter("@4", SqlDbType.VarChar),
                new SqlParameter("@5", SqlDbType.VarChar),
                new SqlParameter("@6", SqlDbType.Float)
            };
            sqlParas[0].Value = id;
            sqlParas[1].Value = Condition;
            sqlParas[2].Value = Price_Level;
            sqlParas[3].Value = Rounding_Rule;
            sqlParas[4].Value = Sign;
            sqlParas[5].Value = Value_Rate;

            string sqlText = stringBuilder.ToString();
            
            return DBHelper.ExecuteNonQuery(sqlText, sqlParas);
        }

        /// <summary>
        /// 查询供应商信息
        /// </summary>
        /// <param name="demand_ID"></param>
        /// <returns></returns>
        public DataTable Supplier_Base(int item1, String item2, String item3)
        {
            StringBuilder stringBuilder = new StringBuilder("select top ");
            stringBuilder.Append(item1);
            if (item2.Length == 0)
                stringBuilder.Append(" * from Supplier_Base order by ID ");
            else
                stringBuilder.Append(" * from Supplier_Base where Supplier_ID = '" + item2 + " '");
            string sqlText = stringBuilder.ToString();
            DataTable dt = DBHelper.ExecuteQueryDT(sqlText);
            if (dt.Rows.Count > 0)
            {
                return dt;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 根据查询条件查询采购单信息
        /// </summary>
        /// <param name="demand_ID"></param>
        /// <returns></returns>
        public List<Summary_Demand> findDemand(Dictionary<string, string> conditions)
        {
            List<Summary_Demand> result = new List<Summary_Demand>();

            StringBuilder stringBuilder = new StringBuilder("select * from "+tableName);
            if (conditions.Count != 0)
                stringBuilder.Append(" where ");
            bool isfirst = true;
            foreach (KeyValuePair<string, string> kvPair in conditions)
            {
                string key = kvPair.Key;
                string value = kvPair.Value;
                if (isfirst)
                {
                    stringBuilder.Append(value);
                    isfirst = false;
                }
                else
                    stringBuilder.Append(" and ").Append(value);
            }
            stringBuilder.Append(" order by Update_Time desc");
            string sqlText = stringBuilder.ToString();
            DataTable dt = DBHelper.ExecuteQueryDT(sqlText);
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    Summary_Demand demand = new Summary_Demand();
                    demand.Demand_ID = row["Demand_ID"].ToString();
                    demand.Material_ID = row["Material_ID"].ToString();
                    demand.Material_Name = row["Material_Name"].ToString();
                    demand.Purchase_Type = row["Purchase_Type"].ToString();
                    demand.Material_Type = row["Material_Type"].ToString();
                    demand.Stock_ID = row["Stock_ID"].ToString();
                    demand.Factory_ID = row["Factory_ID"].ToString();
                    demand.Mini_Purchase_Count = Convert.ToInt32(row["Mini_Purchase_Count"].ToString());
                    demand.Demand_Count = Convert.ToInt32(row["Demand_Count"].ToString());
                    /*
                    try
                    {
                        demand.Delivery_Date = DateTime.Parse(row["Delivery_Date"].ToString());
                    }
                    catch (Exception e) { }
                     * */
                    demand.Proposer_ID = row["Proposer_ID"].ToString();
                    demand.Supplier_ID = row["Supplier_ID"].ToString();
                    demand.Contract_ID = row["Contract_ID"].ToString();
                    demand.PhoneNum = row["PhoneNum"].ToString();
                    demand.Department = row["Department"].ToString();
                    demand.Remarks = row["Remarks"].ToString();
                    demand.State = row["State"].ToString();
                    //try
                    //{
                    //    demand.Pass_Date = DateTime.Parse(row["Pass_Date"].ToString());
                    //}
                    //catch (Exception e) { }
                    demand.Purchase_Price = row["Purchase_Price"].ToString();
                    demand.Inquiry_ID = row["Inquiry_ID"].ToString();
                    demand.Follow_Certificate = row["Follow_Certificate"].ToString();
                    try
                    {
                        demand.Create_Time = DateTime.Parse(row["Create_Time"].ToString());
                    }
                    catch (Exception e) { }
                    //try
                    //{
                    //    demand.Update_Time = DateTime.Parse(row["Update_Time"].ToString());
                    //}
                    //catch (Exception e) { }
                    //demand.Spare1 = row["Spare1"].ToString();
                    //demand.Spare2 = row["Spare2"].ToString();
                    //demand.Spare3 = row["Spare3"].ToString();

                    result.Add(demand);
                }
                return result;
            }
            else
                return result;
        }
        
        /// <summary>
        /// 更改需求计划信息
        /// </summary>
        /// <param name="demand"></param>
        /// <returns></returns>
        public int UpdateSummaryDemand(Summary_Demand demand)
        {
            string strNow = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss");
            StringBuilder sb = new StringBuilder("Update " + tableName + " set")
                    .Append(" Purchase_Type='" + demand.Purchase_Type + "',")
                    .Append(" LogisticsMode='" + demand.LogisticsMode + "',")
                    .Append(" Department='" + demand.Department + "',")
                    .Append(" Proposer_ID='" + demand.Proposer_ID + "',")
                    .Append(" PhoneNum='" + demand.PhoneNum + "',")
                    .Append(" Update_Time='" + strNow+"',")
                    .Append(" State='"+demand.State+"'")
                    .Append(" where Demand_ID='"+demand.Demand_ID+"'");
            string sqlText = sb.ToString();
            int result = DBHelper.ExecuteNonQuery(sqlText);
            return result;
        }

        /// <summary>
        /// 根据需求计划单号删除需求计划
        /// </summary>
        /// <param name="demandId"></param>
        /// <returns></returns>
        public int deleteSummaryDemandById(string demandId)
        {
            StringBuilder sb = new StringBuilder("delete from "+ tableName+" where Demand_ID='"+demandId+"'");
            string sqlText = sb.ToString();
            int result = DBHelper.ExecuteNonQuery(sqlText);
            return result;
        }

        /// <summary>
        /// 审核需求计划
        /// </summary>
        /// <param name="demand"></param>
        /// <returns></returns>
        public int reviewDemandState(Summary_Demand demand)
        {
            String str = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss");

            StringBuilder sb = new StringBuilder("update " + tableName + " set")
                .Append(" ReviewAdvice=@ReviewAdvice,State=@State,ReviewTime=@ReviewTime ,flag=@flag,Purchase_Price=@Purchase_Price")//ReviewAdvice='" + demand.ReviewAdvice + "',")
                .Append(" where Demand_ID=@Demand_ID");
            SqlParameter[] sqlPara = new SqlParameter[] { 
                new SqlParameter("@ReviewAdvice",SqlDbType.VarChar),
                new SqlParameter("@State",SqlDbType.VarChar),
                new SqlParameter("@ReviewTime",SqlDbType.DateTime),
                new SqlParameter("@flag",SqlDbType.VarChar),
                new SqlParameter("@Purchase_Price",SqlDbType.VarChar),
                new SqlParameter("@Demand_ID",SqlDbType.VarChar)

            };
            sqlPara[0].Value = demand.ReviewAdvice;
            sqlPara[1].Value = demand.State;
            sqlPara[2].Value = str;
            sqlPara[3].Value = demand.flag;
            sqlPara[4].Value = demand.Purchase_Price;
            sqlPara[5].Value = demand.Demand_ID;
            String sqlText = sb.ToString();
            int result = DBHelper.ExecuteNonQuery(sqlText,sqlPara);
            return result;
        }

        /// <summary>
        /// 获取审核通过的需求计划
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public List<string> findReviewedDemands(string item)
        {
            List<string> demandList = new List<string>();
            StringBuilder sb = new StringBuilder("select * from "+tableName+" where State='"+item+"'");
            string sqlText = sb.ToString();
            DataTable dt = DBHelper.ExecuteQueryDT(sqlText);
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    demandList.Add(dr["Demand_ID"].ToString());
                }
            }
            return demandList;
        }

        /// <summary>
        /// 返回指定时间内的需求计划信息
        /// </summary>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <returns></returns>
        public List<Summary_Demand> findDemandsInLimitTime(string startTime, string endTime)
        {
            DateTime ds = DateTime.Parse(startTime);
            DateTime de = DateTime.Parse(endTime);
            List<Summary_Demand> summaryDemandList = new List<Summary_Demand>();
            StringBuilder sb = new StringBuilder("select * from "+tableName+" where Create_Time>='"+ds+"' and Create_Time<='"+de+"' order by Create_Time desc");
            string sqlText = sb.ToString();
            DataTable dt = DBHelper.ExecuteQueryDT(sqlText);
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    Summary_Demand summaryDemand = new Summary_Demand();
                    summaryDemand.Demand_ID = dr["Demand_ID"].ToString();
                    summaryDemand.Purchase_Type = dr["Purchase_Type"].ToString();
                    summaryDemand.Proposer_ID = dr["Proposer_ID"].ToString();
                    summaryDemand.State = dr["State"].ToString();
                    summaryDemand.Create_Time = DateTime.Parse(dr["Create_Time"].ToString());
                    summaryDemand.flag = dr["flag"].ToString();
                    summaryDemandList.Add(summaryDemand);
                }
            }
            return summaryDemandList;
        }
    }
}
