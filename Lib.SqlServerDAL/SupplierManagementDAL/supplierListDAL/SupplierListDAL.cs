﻿using Lib.Common.CommonUtils;
using Lib.IDAL.SupplierManagementIDAL.supplierListIDAL;
using Lib.Model.supplierListMode;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace Lib.SqlServerDAL.SupplierManagementDAL.supplierListDAL
{
    public class SupplierListDAL : SupplierListIDAL
    {
        /// <summary>
        /// 创建清单信息
        /// </summary>
        /// <param name="supplierListModel"></param>
        /// <returns></returns>
        public bool createSupplierList(SupplierListModel supplierListModel)
        {
            bool flag = false;
            SqlParameter[] sqlParam = new SqlParameter[] {

                new SqlParameter("@SupplierListId",supplierListModel.SupplierListid1),
                new SqlParameter("@remaker",supplierListModel.Remaker),
                new SqlParameter("@purOrgId",supplierListModel.PurOrgId),
                new SqlParameter("@mtId",supplierListModel.MtId),
                new SqlParameter("@description",supplierListModel.Description),
                new SqlParameter("@createTime",supplierListModel.CreateTime)
            }; 
            try
            {
                string str = @"insert into Tab_SupplierListInfo(SupplierListId,remaker,purOrgId,mtId,description,createTime) VALUES (@SupplierListId,@remaker,@purOrgId,@mtId,@description,@createTime)";
                if (DBHelper.ExecuteNonQuery(str,sqlParam) == 1) 
                {
                    flag = true;

                }
            }
            catch (Exception ex)
            {
                LogHelper.WriteMySqlLog(ex.Message);
                flag = false;
            }
            return flag;
        }
        /// <summary>
        /// 删除清单
        /// </summary>
        /// <param name="supplierListModel"></param>
        public void deleteSupplierList(SupplierListModel supplierListModel)
        {
            DBHelper.ExecuteNonQuery("DELETE from Tab_SupplierListInfo where SupplierListid='"+supplierListModel.SupplierListid1+"'");
        }

        /// <summary>
        /// 获取已经存在的清单
        /// </summary>
        /// <param name="purOrgId">采购组织编号</param>
        /// <returns></returns>
        public DataTable getExistedList(string purOrgId)
        {
            string str = @"select SupplierListid from Tab_SupplierListInfo where purOrgId='" + purOrgId+"'";
            return DBHelper.ExecuteQueryDT(str);
        }
        /// <summary>
        /// 获取已经存在的清单的供应商信息
        /// </summary>
        /// <param name="existedListId"></param>
        /// <returns></returns>
        public DataTable getExistedListInfo(string existedListId)
        {
            string str = @"select s.SupplierId as Supplier_ID,s.SupplierLevel as Supplier_Position,s.levelDescription,s.supplierType,sb.Supplier_Name from Tab_supplierListAndSupplier_Base s,Supplier_Base sb where  s.SupplierListId ='" + existedListId + "' and s.SupplierId=sb.Supplier_ID ";
            return DBHelper.ExecuteQueryDT(str);
        }

        public DataTable getMtGroup(string purOrgId)
        {
            string str = @"select MtGroup_ID,MtGroup_Name from Porg_MtGroup_Relationship where Porg_ID='"+ purOrgId + "'";
            DataTable dt = DBHelper.ExecuteQueryDT(str);
            return dt;
        }

        public DataTable getMtName(string purOrgId, string mtGroupId)
        {
            string str = @"SELECT Material_ID,Material_Name from Material where Material_Group='"+ mtGroupId + "'";
            return DBHelper.ExecuteQueryDT(str);
        }

        public DataTable getPurOrg()
        {
            string str = @"select * from Buyer_Org";
            
            return DBHelper.ExecuteQueryDT(str);
        }


        /// <summary>
        /// 获取满足采购组织、物料组、物料的供应商
        /// </summary>
        /// <param name="queryCondition"></param>
        /// <returns></returns>
        public int getListCounts(queryCondition queryCondition)
        {
            string str = @"SELECT
	                       count(*)
                        FROM
	                        SR_Info S,
	                        Supplier_Base Sb,
	                        Material M,
	                        Supplier_Position Sp 
                        WHERE
	                        Sb.Supplier_ID = S.SupplierId 
	                        AND S.SelectedDep = @purOrgName 
	                        AND S.SelectedMType = @MtGroupName 
	                        AND M.Material_Group = ( SELECT Material_Group FROM Material_Group WHERE Description =@MtGroupName) 
	                        AND M.Material_ID = @Material_ID
	                        AND Sb.Supplier_ID = Sp.Supplier_ID";
            SqlParameter[] sqlParams = new SqlParameter[] {
                new SqlParameter("@purOrgName",queryCondition.PurOrg),
                new SqlParameter("@MtGroupName",queryCondition.MtGroupName),
                new SqlParameter("@Material_ID",queryCondition.MtId)
            };
           return  int.Parse(DBHelper.ExecuteQueryDT(str, sqlParams).Rows[0][0].ToString());


        }
            /// <summary>
            /// 获取满足采购组织、物料组、物料的供应商
            /// </summary>
            /// <param name="queryCondition"></param>
            /// <returns></returns>
        public DataTable getSupplierInfo(queryCondition queryCondition)
        {



            string str = @"	select top "+ queryCondition.PageSize + "  * from (SELECT Sb.Supplier_ID,Sb.Supplier_Name,sp.Supplier_Position,ROW_NUMBER () OVER ( ORDER BY sb.Supplier_ID ) AS RowNumber  FROM SR_Info S, Supplier_Base Sb,Material M,Supplier_Position Sp WHERE Sb.Supplier_ID = S.SupplierId AND S.SelectedDep = @purOrgName AND S.SelectedMType = @MtGroupName AND M.Material_Group = ( SELECT Material_Group FROM Material_Group WHERE Description =@MtGroupName)  AND  M.Material_ID = @Material_ID  AND  Sb.Supplier_ID = Sp.Supplier_ID) as tb where  RowNumber>"+queryCondition.PageSize*(queryCondition.PageIndex-1);
            SqlParameter[] sqlParams = new SqlParameter[] {
                new SqlParameter("@purOrgName",queryCondition.PurOrg),
                new SqlParameter("@MtGroupName",queryCondition.MtGroupName),
                new SqlParameter("@Material_ID",queryCondition.MtId)
          
            };
            DataTable dt = DBHelper.ExecuteQueryDT(str, sqlParams);

            dt.Columns.Add("levelDescription", typeof(object));
            dt.Columns.Add("supplierType", typeof(object));

            string levelDescription = "";
            for (int i = 0; i < dt.Rows.Count; i++)
            {

                switch (dt.Rows[i]["Supplier_Position"].ToString())
                {
                    case "A":levelDescription = "优选";break;
                    case "B": levelDescription = "有价值"; break;
                    case "C": levelDescription = "待改善"; break;
                    case "D": levelDescription = "可删除"; break;
                    default:
                        break;
                }
                dt.Rows[i]["levelDescription"] = levelDescription;
                dt.Rows[i]["supplierType"] = "现有供应商";
            }
            return dt;
        }

        /// <summary>
        /// 获取满足采购组织、物料组、物料的供应商
        /// </summary>
        /// <param name="queryCondition"></param>
        /// <returns></returns>
        public DataTable getSupplierLevelInfo(queryCondition queryCondition)
        {
            string condition="" ;

            if (queryCondition.PurOrg != null && !queryCondition.PurOrg.Equals("")) {

                condition += "  and sr.SelectedDep='"+ queryCondition.PurOrg + "'";
            }
            if (queryCondition.SupplierId != null && !queryCondition.SupplierId.Equals(""))
            {

                condition += "  and sp.Supplier_ID like'%"+ queryCondition.SupplierId + "%'" ;
            }
            if (queryCondition.STime != null && !queryCondition.STime.Equals(""))
            {

                condition += "  and sp.Record_Time>='"+ queryCondition.STime + "'";
            }

            if (queryCondition.ETime != null && !queryCondition.ETime.Equals(""))
            {

                condition += "  and sp.Record_Time<='" + queryCondition.ETime +"'";
            }

            if (queryCondition.SupplierName != null && !queryCondition.SupplierName.Equals(""))
            {

                condition += "  and sr.companyName like'%" + queryCondition.SupplierName + "%'";
            }




            string str = @"	select top " + queryCondition.PageSize + "  * from (SELECT sp.ID,sp.Supplier_ID,sr.companyName,sp.Supplier_Position,ROW_NUMBER () OVER ( ORDER BY sp.Supplier_ID ) AS RowNumber  from Supplier_Position sp,SR_Info sr where   sr.SupplierId=sp.Supplier_ID " + condition+"  ) as tb where  RowNumber>" + queryCondition.PageSize * (queryCondition.PageIndex - 1);
    
            DataTable dt = DBHelper.ExecuteQueryDT(str);
            dt.Columns.Add("levelDescription", typeof(object));
            dt.Columns.Add("supplierType", typeof(object));
            string levelDescription = "";
            for (int i = 0; i < dt.Rows.Count; i++)
            {

                switch (dt.Rows[i]["Supplier_Position"].ToString())
                {
                    case "A": levelDescription = "优选"; break;
                    case "B": levelDescription = "有价值"; break;
                    case "C": levelDescription = "待改善"; break;
                    case "D": levelDescription = "可删除"; break;
                    default:
                        break;
                }
                dt.Rows[i]["levelDescription"] = levelDescription;
                dt.Rows[i]["supplierType"] = "现有供应商";
            }
            return dt;
        }

        public int getSupLevelCount(queryCondition queryCondition) {

            string condition = "";

            if (queryCondition.PurOrg != null && !queryCondition.PurOrg.Equals(""))
            {

                condition += "  and sr.SelectedDep='" + queryCondition.PurOrg + "'";
            }
            if (queryCondition.SupplierId != null && !queryCondition.SupplierId.Equals(""))
            {

                condition += "  and sp.Supplier_ID='" + queryCondition.SupplierId + "'";
            }
            if (queryCondition.STime != null && !queryCondition.STime.Equals(""))
            {

                condition += "  and sp.Record_Time>='" + queryCondition.STime + "'";
            }

            if (queryCondition.ETime != null && !queryCondition.ETime.Equals(""))
            {

                condition += "  and sp.Record_Time<='" + queryCondition.ETime + "'";
            }

            string str = @"	SELECT count(*) from Supplier_Position sp,SR_Info sr where   sr.SupplierId=sp.Supplier_ID " + condition;
            DataTable dt = DBHelper.ExecuteQueryDT(str);

            return int.Parse(dt.Rows[0][0].ToString());

        }




        public bool saveSupplierListInfo(SupplierListModel supplierListModel)
        {
            SqlParameter[] sqlParam = new SqlParameter[] {
                new SqlParameter("@SupplierListId",supplierListModel.SupplierListid1),
                new SqlParameter("@SupplierId",supplierListModel.SupplierID),
                new SqlParameter("@SupplierLevel",supplierListModel.SupplierLevel),
                new SqlParameter("@levelDescription","有价值的供应商"),
               new SqlParameter("@suppLierType",supplierListModel.SupplierType)
            };
            bool flag = false;

            string  str = @"INSERT INTO Tab_supplierListAndSupplier_Base ( supplierListId, SupplierId, SupplierLevel, levelDescription, suppLierType )
                                VALUES
	                       ( @SupplierListId,@SupplierId,@SupplierLevel,@levelDescription,@suppLierType )";
            try
            {
                flag = DBHelper.ExecuteNonQuery(str, sqlParam)==1?true:false;
            }
            catch (Exception ex)
            {
                LogHelper.WriteExceptionLog(ex);
                flag = false;
            }

            return flag;
        }
        /// <summary>
        /// 获取物料记录数
        /// </summary>
        /// <param name="queryCondition"></param>
        /// <returns></returns>
        public int getMtInfoCount(string MtId )
        {
            string str = @"SELECT count(*) from Material where Material_Group='" + MtId + "'";
            return int.Parse(DBHelper.ExecuteQueryDT(str).Rows[0][0].ToString());
        }
        /// <summary>
        /// 获取物料的供应源信息
        /// </summary>
        /// <param name="queryCondition"></param>
        /// <returns></returns>
        public DataTable getSupplySupplierInfo(queryCondition queryCondition)
        {
            string str = @"select top "+queryCondition.PageSize+" *  from (SELECT  SI.SupplierId,sb.Supplier_Name,sp.Supplier_Position,ROW_NUMBER() OVER (ORDER BY SI.SupplierId ) AS RowNumber FROM [dbo].[TabSupplyInfo] SI,Supplier_Base sb,Supplier_Position sp where SI.mtId='"+queryCondition.MtId+"' and SI.SupplierId= sb.Supplier_ID and sp.Supplier_ID=SI.SupplierId) as tb where RowNumber >"+queryCondition.PageSize*(queryCondition.PageIndex-1);
            return DBHelper.ExecuteQueryDT(str);
        }
        /// <summary>
        /// 获取供应源记录数
        /// </summary>
        /// <param name="queryCondition"></param>
        /// <returns></returns>
        public int getSupplierInfoCount(queryCondition queryCondition)
        {
            string str = @"SELECT count(*) FROM [dbo].[TabSupplyInfo] SI,Supplier_Base sb,Supplier_Position sp where SI.mtId='" + queryCondition.MtId + "' and SI.SupplierId= sb.Supplier_ID and sp.Supplier_ID=SI.SupplierId";
            return int.Parse(DBHelper.ExecuteQueryDT(str).Rows[0][0].ToString());
        }
        /// <summary>
        /// 设置供应商的等级
        /// </summary>
        /// <param name="supplier"></param>
        /// <param name="level"></param>
        /// <returns></returns>
        public bool setLevel(string supplier, string level,string ID)
        {
            string str = @"update Supplier_Position set Supplier_Position='"+ level + "' where Supplier_ID='"+supplier+"' and ID='"+ID+"' ";
            if (DBHelper.ExecuteNonQuery(str) == 1) {
                return true;
            }
            return false;
        }
        /// <summary>
        /// 记录操作信息
        /// </summary>
        /// <param name="supplierId"></param>
        /// <param name="modifyMess"></param>
        /// <param name="reason"></param>
        /// <returns></returns>
        public bool saveModifyInfo(string supplierId, string modifyMess, string reason)
        {
            string str = @"insert into TabsetLevelInfo(supplierId,modifyResult,operateReson) values('" + supplierId + "','" + modifyMess + "','" + reason + "')";
            if (DBHelper.ExecuteNonQuery(str) == 1)
            {
                return true;
            }
            return false;
        }
    }
}
