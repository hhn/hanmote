﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.SqlIDAL;
using System.Data.SqlClient;
using System.Data;
using Lib.Model;
using Lib.Common.MMCException.IDAL;

namespace Lib.SqlServerDAL.SupplierManagementDAL
{
    public class SupplierManagementDAL : SupplierManagementIDAL
    {
        public List<String> GetrAllMaterialType()
        {
            return null;
        }

        public DataTable GetAllSupplierBaseId()
        {
            try
            {
                string sql = "select Supplier_ID from Supplier_Base";
                return DBHelper.ExecuteQueryDT(sql);
            }
            catch (Exception ex)
            {
                throw new DBException("查询供应商信息异常！", ex);
            }
            
        }

        public bool InsertMaterialType()
        {
            string sql = "insert into Material_Type() value()";

            return false;
        }

        ////获取所有供应商编号和供应商名称
        //public List<SupplierBaseInfo> GetAllSupplier() 
        //{
        //    StringBuilder stringBuilder = new StringBuilder("SELECT Supplier_ID,Supplier_Name FROM Supplier_Base WHERE Supplier_ID IN(SELECT Supplier_ID FROM dbo.Material_Supplier)");
        //    string sql = stringBuilder.ToString();
        //    DataTable dt = DBHelper.ExecuteQueryDT(sql);
        //    if (dt == null || dt.Rows.Count == 0)
        //    {
        //        return null;
        //    }
        //    List<SupplierBaseInfo> list = new List<SupplierBaseInfo>();
        //    SupplierBaseInfo obj;
        //    for (int i = 0; i < dt.Rows.Count; i++)
        //    {
        //        obj = new SupplierBaseInfo();
        //        DataRow dr = dt.Rows[i];
        //        obj.SupplierID = dr["Supplier_ID"].ToString();
        //        obj.SupplierName = dr["Supplier_Name"].ToString();
        //        list.Add(obj);
        //    }
        //    return list;
        //}

        //获取所有供应商编号和供应商名称
        public DataTable GetAllSupplier()
        {
            StringBuilder stringBuilder = new StringBuilder("SELECT DISTINCT(Supplier_ID),Supplier_Name FROM Supplier_Base WHERE Supplier_ID IN(SELECT Supplier_ID FROM dbo.Material_Supplier)");
            string sql = stringBuilder.ToString();
            DataTable dt = DBHelper.ExecuteQueryDT(sql); 
            return dt;
        }

        /// <summary>
        /// 通过供应商编号获取供应商所有物料组
        /// </summary>
        /// <returns></returns>
        public DataTable GetAllMaterialGroupBySupplierID(string supplierID)
        {
            string sql = "SELECT DISTINCT(Material_Group) FROM Material WHERE Material_ID IN(SELECT Material_ID FROM dbo.Material_Supplier WHERE Supplier_ID='" + supplierID + "')";
            DataTable dt = DBHelper.ExecuteQueryDT(sql); 
            return dt;
        }

        /// <summary>
        /// 通过供应商编号和物料组设置支出类型  (插入)
        /// </summary>
        /// <returns></returns>
        public bool SetExpenseTypeBySupplierIDAndMaterialGroup(string supplierID,string MaterilGroup)
        {
            string sql = "";
            int n = DBHelper.ExecuteNonQuery(sql);
            return n > 0 ? true : false;
        }

        /// <summary>
        ///  通过供应商物料组设置支出类型
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public bool SetExpenseTypeByMaterialGroup(DataTable dt)
        {
            string sql = "SELECT Supplier_ID,Expense_Type,Quailty_Score,Cost_Score,Deliver_Score,Service_Score,Total_Score,PIP_Level,Supply_Type,Material_Group FROM Admission_Base WHERE 1=2";
            int n = DBHelper.ExecuteBatch(sql,dt);
            return n > 0 ? true : false;
        }


        public bool SetExpenseTypeByMaterialGroup(string materilGroup)
        {
            string sql = "SELECT Material_Group,Extense_Type,Quailty_Score,Cost_Score,Deliver_Score,Service,Total_Score,PIP_Level,Supply_Type FROM Admission_Base WHERE Material_Group ='" + materilGroup + "'";
            int n = DBHelper.ExecuteNonQuery(sql);
            return n > 0 ? true : false;
        }

        /// <summary>
        /// 通过供应商编号获取供应商品项信息
        /// </summary>
        /// <param name="SupplierID"></param>
        /// <returns></returns>
        public DataTable GetItemsInfoByMaterialGroupID(string id)
        {
            DataTable dt = null;
            string sql = "SELECT Material_Group,Expense_Type,Quailty_Score,Cost_Score,Deliver_Score,Service_Score,Total_Score,PIP_Level,Supply_Type FROM Admission_Base WHERE Material_Group = '" + id + "';";
            dt = DBHelper.ExecuteQueryDT(sql);
            return dt;
        }

        /// <summary>
        /// 更新供应商的编号
        /// </summary>
        /// <param name="SupplierID"></param>
        /// <param name="materialGroup"></param>
        /// <returns></returns>
        public bool UpdateItemsInfoBySupplierIDAndMaterialGroup(string SupplierID, string materialGroup)
        {
            string sql = "UPDATE Admission_Base SET(Expense_Type,Quailty_Score,Cost_Score,Deliver_Score,Service_Score,Total_Score,Supply_Type)";
            int n = DBHelper.ExecuteNonQuery(sql);
            return n > 0 ? true : false;
        }
        /// <summary>
        ///  更新供应商的状态
        /// </summary>
        /// <param name="supplierID"></param>
        /// <returns></returns>
        public bool UpdateTempState(string supplierID,int state)
        {
            string sql = "UPDATE temp SET state="+state+"WHERE Supplier_ID='"+supplierID+"'";
            int n = DBHelper.ExecuteNonQuery(sql);
            return n > 0 ? true : false;
        }
        /// <summary>
        /// 获取所有的物料组
        /// </summary>
        /// <returns></returns>
        public DataTable GetAllMaterialGroup() {
            string sql = "SELECT DISTINCT(Material_Group) From Material";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            return dt;
        }

        /// <summary>
        /// 通过物料ID获取质量特性
        /// </summary>
        /// <param name="materialID"></param>
        /// <returns></returns>
        public bool GetQuliatyPropertyByMaterialID(string materialID) {
            string sql = "Select * from Material";
            int n = DBHelper.ExecuteNonQuery(sql);
            return n > 0 ? true : false;
        }


        public List<CertificationTemplate> GetAllCertificationTemplate()
        {
            StringBuilder stringBuilder = new StringBuilder("SELECT tid,pid,text,others,isUse FROM Admission_Certification_Item where isUse=0");
            string sql = stringBuilder.ToString();
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            if (dt == null || dt.Rows.Count == 0)
            {
                return null;
            }
            List<CertificationTemplate> list = new List<CertificationTemplate>();
            CertificationTemplate obj;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                obj = new CertificationTemplate();
                DataRow dr = dt.Rows[i];
                obj.tid = dr["tid"] == null ? null : dr["tid"].ToString();
                obj.pid = dr["pid"] == null ? null : dr["pid"].ToString();
                obj.text = dr["text"] == null ? null : dr["text"].ToString();
                obj.others = dr["others"] == null ? null : dr["others"].ToString();
                obj.isUse = dr["isUse"] == null ? 0 : Convert.ToInt32(dr["isUse"].ToString());
                list.Add(obj);
            }
            return list;
        }

        public bool saveCertificationTemplate(DataTable dt) 
        {
            string sql = "SELECT material_group,type,templatename FROM Admission_Certification_Template WHERE 1=2";
            int n = DBHelper.ExecuteBatch(sql,dt);
            return n > 0 ? true : false;
        }

        public bool saveSelfCerAndBaseInfoDecision(DecisionInfo obj)
        {
            string sql = "insert into admission_decision_result (supplier_name,decision_result,descriptions,decision_time,status) values (@supplier_name,@decision_result,@descriptions,@decision_time,@status)";
            SqlParameter[] sqlParameter = new SqlParameter[]{
                new SqlParameter("@supplier_name",SqlDbType.NVarChar),
                new SqlParameter("@decision_result",SqlDbType.NVarChar),
                new SqlParameter("@descriptions",SqlDbType.NVarChar),
                new SqlParameter("@decision_time",SqlDbType.DateTime),
                new SqlParameter("@status",SqlDbType.Int)
            };
            sqlParameter[0].Value = obj.SupplierName;
            sqlParameter[1].Value = obj.DecisionResult;
            sqlParameter[2].Value = obj.Descriptions;
            sqlParameter[3].Value = Convert.ToDateTime(System.DateTime.Now);
            sqlParameter[4].Value = obj.Status;
            int n = DBHelper.ExecuteNonQuery(sql, sqlParameter);
            return n > 0 ? true : false;
        }

        public DataTable getAllCompany()
        {
            StringBuilder stringBuilder = new StringBuilder("SELECT DISTINCT(supplierChineseName) FROM Org_company_baseinfo");
            string sql = stringBuilder.ToString();
            DataTable dt = DBHelper.ExecuteQueryDT(sql);

            return dt;
        }

        public OrganizationInfo getOrganizationInfoByCompanyName(string companyName)
        {
            OrganizationInfo obj = new OrganizationInfo();
            string sql = "Select * from Org_company_baseinfo where supplierChineseName ='"+companyName+"'";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            if (dt == null || dt.Rows.Count == 0)
            {
                return null;
            }
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataRow dr = dt.Rows[i];
                obj.SupplierChineseName = dr["supplierChineseName"] == null ? null : dr["supplierChineseName"].ToString();
                obj.SupplierEnglishName = dr["supplierEnglishName"] == null ? null : dr["supplierEnglishName"].ToString();
                obj.CompanyProperty = dr["companyProperty"] == null ? null : dr["companyProperty"].ToString();
                obj.CompanyUsedName = dr["companyUsedname"] == null ? null : dr["companyUsedname"].ToString();
                obj.CompanyRegisterCountry = dr["companyRegisterCountry"] == null ? null : dr["companyRegisterCountry"].ToString();
                obj.CompanyWebsite = dr["companyWebsite"] == null ? null : dr["companyWebsite"].ToString();
                obj.CompanyCode = dr["companyCode"] == null ? null : dr["companyCode"].ToString();
                obj.CompanyRegisterDate = dr["companyRegisterDate"] == null ? System.DateTime.Now : Convert.ToDateTime(dr["companyRegisterDate"].ToString());
                obj.IsListed = dr["isListed"] == null ? 0 : Convert.ToInt32(dr["isListed"].ToString());
                obj.CompanyRepresent = dr["companyRepresent"] == null ? null : dr["companyRepresent"].ToString();
                obj.StockCode = dr["stockCode"] == null ? null : dr["stockCode"].ToString();
                obj.IsExport = dr["isExport"] == null ? 0 : Convert.ToInt32(dr["isExport"].ToString());
                obj.Country = dr["country"] == null ? null : dr["country"].ToString();
                obj.City = dr["city"] == null ? null : dr["city"].ToString();
                obj.DoorNum = dr["doorNum"] == null ? null : dr["doorNum"].ToString();
                obj.ZipCode = dr["zipCode"] == null ? null : dr["zipCode"].ToString();
                obj.Id = dr["cid"] == null ? 0 : Convert.ToInt32(dr["cid"].ToString());
            }
            string sql2 = "Select * from Org_contacts where conid =" + obj.Id;
            DataTable dt2 = DBHelper.ExecuteQueryDT(sql2);
            OrgContacts oc = null;
            if (dt2 == null || dt2.Rows.Count == 0)
            {
                return null;
            }
            for (int i = 0; i < dt2.Rows.Count; i++)
            {
                DataRow dr = dt2.Rows[i];
                oc = new OrgContacts();
                oc.ContactMan = dr["contactMan"] == null ? null : dr["contactMan"].ToString();
                oc.Phonenum1 = dr["phonenum1"] == null ? null : dr["phonenum1"].ToString();
                oc.Phonenum2 = dr["phonenum2"] == null ? null : dr["phonenum2"].ToString();
                oc.Email = dr["email"] == null ? null : dr["email"].ToString();
            }
            obj.orgContacts = new List<OrgContacts>();
            obj.orgContacts.Add(oc);
            return obj;
        }

        public List<OrgContacts> getOrgContactsByCompanyName(string companyName)
        {
            List<OrgContacts> list = new List<OrgContacts>();
            OrgContacts oc = null;
            OrganizationInfo obj = new OrganizationInfo();
            string sql = "Select * from Org_contacts where conid in(Select cid from Org_company_baseInfo where supplierChineseName ='" + companyName + "')";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            if (dt == null || dt.Rows.Count == 0)
            {
                return null;
            }
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataRow dr = dt.Rows[i];
                oc = new OrgContacts();
                oc.ContactMan = dr["contactMan"] == null ? null : dr["contactMan"].ToString();
                oc.Phonenum1 = dr["phonenum1"] == null ? null : dr["phonenum1"].ToString();
                oc.Phonenum2 = dr["phonenum2"] == null ? null : dr["phonenum2"].ToString();
                oc.Email = dr["email"] == null ? null : dr["email"].ToString();
                list.Add(oc);
            }
            //string sql2 = "Select * from Org_contacts where cid =" + obj.Id;
            //DataTable dt2 = DBHelper.ExecuteQueryDT(sql2);
            //if (dt == null || dt.Rows.Count == 0)
            //{
            //    return null;
            //}
            //for (int i = 0; i < dt.Rows.Count; i++)
            //{
            //    DataRow dr = dt.Rows[i];
            //    oc = new OrgContacts();
            //    oc.contactMan = dr["contactMan"] == null ? null : dr["contactMan"].ToString();
            //    oc.phonenum1 = dr["phonenum1"] == null ? null : dr["phonenum1"].ToString();
            //    oc.phonenum2 = dr["phonenum2"] == null ? null : dr["phonenum2"].ToString();
            //    oc.email = dr["email"] == null ? null : dr["email"].ToString();
            //}
            return list;
        }

        public DataTable getFirstDegree()
        {
            StringBuilder stringBuilder = new StringBuilder("SELECT DISTINCT(Bigclassfy_Name) FROM Bigclassfy");
            string sql = stringBuilder.ToString();
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            return dt;
        }
        public DataTable getSecondDegree()
        {
            StringBuilder stringBuilder = new StringBuilder("SELECT DISTINCT(Littleclassfy_Name) FROM Littleclassfy");
            string sql = stringBuilder.ToString();
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            return dt;
        }
        public DataTable getThirdDegree()
        {
            StringBuilder stringBuilder = new StringBuilder("SELECT DISTINCT(Type_Name) FROM Typeclassfy");
            string sql = stringBuilder.ToString();
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            return dt;
        }

        public bool saveSEMScore(SEMModel obj)
        {
            string sql = "insert into Admission_SEM (supplierName,totalScore,others,filePath,fileName,uploadDate,level) values (@supplierName,@totalScore,@others,@filePath,@fileName,@uploadDate,@level)";
            SqlParameter[] sqlParameter = new SqlParameter[]{
                new SqlParameter("@supplierName",SqlDbType.NVarChar),
                new SqlParameter("@totalScore",SqlDbType.Real),
                new SqlParameter("@others",SqlDbType.NVarChar),
                new SqlParameter("@filePath",SqlDbType.NVarChar),
                new SqlParameter("@fileName",SqlDbType.NVarChar),
                new SqlParameter("@uploadDate",SqlDbType.DateTime),
                new SqlParameter("@level",SqlDbType.NVarChar)
            };
            sqlParameter[0].Value = obj.SupplierName;
            sqlParameter[1].Value = obj.TotalSocre;
            sqlParameter[2].Value = obj.Others;
            sqlParameter[3].Value = obj.FilePath;
            sqlParameter[4].Value = obj.FileName;
            sqlParameter[5].Value = Convert.ToDateTime(System.DateTime.Now);
            sqlParameter[6].Value = obj.Level;
            int n = DBHelper.ExecuteNonQuery(sql, sqlParameter);
            bool flag = n > 0 ? true : false;
            string sql1 = "update status set @status where supplierName =@supplierName";
            SqlParameter[] sqlParameter1 = new SqlParameter[]{
                new SqlParameter("@status",SqlDbType.Int),
                new SqlParameter("@supplierName",SqlDbType.NVarChar)
            };
            sqlParameter[0].Value = obj.Status;
            sqlParameter[1].Value = obj.SupplierName;
            int n1 = DBHelper.ExecuteNonQuery(sql1, sqlParameter1);
            bool flag1 = n1 > 0 ? true : false;

            return flag && flag1;
        }

        public List<SEMModel> getSEMScoreByCompanyName(string companyName)
        {
            List<SEMModel> list = new List<SEMModel>();
            SEMModel obj = new SEMModel();
            string sql = "Select * from Admission_SEM where supplierName ='" + companyName + "' order by uploadDate";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            if (dt == null || dt.Rows.Count == 0)
            {
                return null;
            }
            DataRow dr = dt.Rows[0];
            obj.FileName = dr["fileName"] == null ? null : dr["fileName"].ToString();
            obj.FilePath = dr["filePath"] == null ? null : dr["filePath"].ToString();
            obj.Others = dr["others"] == null ? null : dr["others"].ToString();
            obj.TotalSocre = dr["totalScore"] == null ? 0 : Convert.ToDouble(dr["totalScore"].ToString());
            list.Add(obj);
            return list;
        }
        public String getSupplierIdBySupplierName(string supplierName)
        {
            StringBuilder stringBuilder = new StringBuilder("SELECT Supplier_ID FROM Supplier_Base WHERE Supplier_Name = '"+ supplierName+"'");
            string sql = stringBuilder.ToString();
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            if (dt == null || dt.Rows.Count == 0)
            {
                return null;
            }
            DataRow dr = dt.Rows[0];
            string supplierId = dr["Supplier_ID"] == null ? null : dr["Supplier_ID"].ToString();
            return supplierId;
        }

        public DataTable getMaterialName(string type,string filed)
        {
            StringBuilder stringBuilder = new StringBuilder("SELECT * FROM Material WHERE "+filed+" = '" + type + "'");
            string sql = stringBuilder.ToString();
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            return dt;
        }

        public int getCertificationStatusBySupplierName(string supplierName)
        {
            string sql = "select max(status) from Admission_Decision_Result where supplier_name ='" + supplierName + "'";
            return (int)DBHelper.ExecuteScalar(sql);
        }

        public bool ModifyProcessingStatus(int status, string supplierName)
        {
            string sql = "update Admission_Decision_Result SET status=" + status + "WHERE supplier_name='" + supplierName + "'";
            int n = DBHelper.ExecuteNonQuery(sql);
            return n > 0 ? true : false; 
        }

        public string GetFileOnServerPath(string tableName, string supplierName)
        {
            string sql = "select filePath from "+tableName+" where supplierName ='" + supplierName + "'";
            return (string)DBHelper.ExecuteScalar(sql);
        }

        public DecisionInfo GetCerAndBaseinfo(string supplierName)
        {
            DecisionInfo di = new DecisionInfo();
            string sql = "SELECT * FROM Admission_Decision_Result";


            return di;
        }
    }
}
