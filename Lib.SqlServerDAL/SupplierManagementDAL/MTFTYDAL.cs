﻿using Lib.Common.MMCException.IDAL;
using Lib.IDAL.SupplierManagementIDAL;
using System;
using System.Data;
using System.Data.Common;


namespace Lib.SqlServerDAL
{
    public class MTFTYDAL : MTFTYIDAL
    {
        DataTable MTFTYIDAL.GetAllMaterialIdDTByFactoryId(string factoryId)
        {
            try
            {
                string sql = "select Material_ID from MT_FTY where Factory_ID = '" + factoryId + "'";
                return DBHelper.ExecuteQueryDT(sql);
            }
            catch (Exception ex)
            {
                throw new DBException("通过工厂id查询物料id列表时数据库执行异常！", ex);
            }
        }
    }
}
