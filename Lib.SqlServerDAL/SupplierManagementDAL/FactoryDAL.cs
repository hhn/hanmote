﻿using Lib.Common.MMCException.IDAL;
using Lib.IDAL.SupplierManagementIDAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Lib.SqlServerDAL
{
    public class FactoryDAL : FactoryIDAL
    {
        DataTable FactoryIDAL.GetAllFactoryIdDT()
        {
            try
            {
                string sql = "select Factory_ID from Factory";
                return DBHelper.ExecuteQueryDT(sql);
            }
            catch (Exception ex)
            {
                throw new DBException("查询工厂编码时数据库操作异常！", ex);
            }
        }
    }
}
