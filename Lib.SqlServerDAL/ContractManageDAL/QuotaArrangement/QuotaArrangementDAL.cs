﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Lib.IDAL.ContractManage.QuotaArrangement;
using Lib.Model.ContractManage.QuotaArrangement;

namespace Lib.SqlServerDAL.ContractManageDAL.QuotaArrangement
{
    public class QuotaArrangementDAL : QuotaArrangementIDAL
    {
        /// <summary>
        /// 获取指定Material_ID和Factory_ID的所有配额协议
        /// </summary>
        /// <param name="materialID"></param>
        /// <param name="factoryID"></param>
        /// <returns></returns>
        public List<Quota_Arrangement> getQuotaArrangement(string materialID,
            string factoryID) {

            StringBuilder strBui = new StringBuilder("select * from Quota_Arrangement ")
                .Append("where Material_ID = @Material_ID and Factory_ID = @Factory_ID ")
                .Append(" order by Create_Time");

            SqlParameter[] sqlParams = new SqlParameter[]{
                new SqlParameter("@Material_ID", SqlDbType.VarChar),
                new SqlParameter("@Factory_ID", SqlDbType.VarChar)
            };
            sqlParams[0].Value = materialID;
            sqlParams[1].Value = factoryID;

            DataTable dt = DBHelper.ExecuteQueryDT(strBui.ToString(), sqlParams);
            List<Quota_Arrangement> resultList = new List<Quota_Arrangement>();
            foreach (DataRow dr in dt.Rows) {
                Quota_Arrangement quota = new Quota_Arrangement();
                quota.Material_ID = materialID;
                quota.Factory_ID = factoryID;
                quota.Quota_Arrangement_ID = dr["Quota_Arrangement_ID"].ToString();
                quota.Material_Name = dr["Material_Name"].ToString();
                quota.Factory_Name = dr["Factory_Name"].ToString();
                quota.Begin_Time = Convert.ToDateTime(dr["Begin_Time"].ToString());
                quota.End_Time = Convert.ToDateTime(dr["End_Time"].ToString());
                quota.Create_Time = Convert.ToDateTime(dr["Create_Time"].ToString());
                quota.Creator_ID = dr["Creator_ID"].ToString();
                quota.Min_Quantity_Resolve = Convert.ToDouble(dr["Min_Quantity_Resolve"].ToString());
                quota.OUn = dr["OUn"].ToString();
                quota.Cal_Type = dr["Cal_Type"].ToString();

                resultList.Add(quota);
            }

            return resultList;
        }

        /// <summary>
        /// 删除指定ID的配额协议
        /// </summary>
        /// <param name="quotaArrangementID">配额协议编号</param>
        /// <returns>受影响行数</returns>
        public int deleteQuotaArrangement(string quotaArrangementID) {
            LinkedList<string> sqlList = new LinkedList<string>();

            StringBuilder strBui = new StringBuilder("delete from Quota_Arrangement where ")
                .Append("Quota_Arrangement_ID = '").Append(quotaArrangementID).Append("'");
            sqlList.AddLast(strBui.ToString());

            strBui = new StringBuilder("delete from Quota_Arrangement_Item where ")
                .Append("Quota_Arrangement_ID = '").Append(quotaArrangementID).Append("'");
            sqlList.AddLast(strBui.ToString());

            return DBHelper.ExecuteNonQuery(sqlList);
        }

        /// <summary>
        /// 添加新配额协议
        /// </summary>
        /// <param name="quotaArrangement"></param>
        /// <returns></returns>
        public int addQuotaArrangement(Quota_Arrangement quotaArrangement) {
            StringBuilder strBui = new StringBuilder("insert into Quota_Arrangement values ( ")
                .Append("@Material_ID, @Factory_ID, @Quota_Arrangement_ID, @Material_Name, ")
                .Append("@Factory_Name, @Begin_Time, @End_Time, @Create_Time, @Creator_ID, ")
                .Append("@Min_Quantity_Resolve, @OUn, @Cal_Type , @Status)");

            SqlParameter[] sqlParams = new SqlParameter[]{
                new SqlParameter("@Material_ID", SqlDbType.VarChar),
                new SqlParameter("@Factory_ID", SqlDbType.VarChar),
                new SqlParameter("@Quota_Arrangement_ID", SqlDbType.VarChar),
                new SqlParameter("@Material_Name", SqlDbType.VarChar),
                new SqlParameter("@Factory_Name", SqlDbType.VarChar),
                new SqlParameter("@Begin_Time", SqlDbType.DateTime),
                new SqlParameter("@End_Time", SqlDbType.DateTime),
                new SqlParameter("@Create_Time", SqlDbType.DateTime),
                new SqlParameter("@Creator_ID", SqlDbType.VarChar),
                new SqlParameter("@Min_Quantity_Resolve", SqlDbType.Decimal),
                new SqlParameter("@OUn", SqlDbType.VarChar),
                new SqlParameter("@Cal_Type", SqlDbType.VarChar),
                new SqlParameter("@Status", SqlDbType.VarChar)
            };
            sqlParams[0].Value = quotaArrangement.Material_ID;
            sqlParams[1].Value = quotaArrangement.Factory_ID;
            sqlParams[2].Value = quotaArrangement.Quota_Arrangement_ID;
            sqlParams[3].Value = quotaArrangement.Material_Name;
            sqlParams[4].Value = quotaArrangement.Factory_Name;
            sqlParams[5].Value = quotaArrangement.Begin_Time;
            sqlParams[6].Value = quotaArrangement.End_Time;
            sqlParams[7].Value = quotaArrangement.Create_Time;
            sqlParams[8].Value = quotaArrangement.Creator_ID;
            sqlParams[9].Value = quotaArrangement.Min_Quantity_Resolve;
            sqlParams[10].Value = quotaArrangement.OUn;
            sqlParams[11].Value = quotaArrangement.Cal_Type;
            sqlParams[12].Value = quotaArrangement.Status;

            return DBHelper.ExecuteNonQuery(strBui.ToString(), sqlParams);
        }

        /// <summary>
        /// 更新配额协议
        /// </summary>
        /// <param name="quotaArrangement"></param>
        /// <returns></returns>
        public int updateQuotaArrangement(Quota_Arrangement quotaArrangement) {
            StringBuilder strBui = new StringBuilder("update Quota_Arrangement ")
                .Append("set Begin_Time = @Begin_Time, End_Time = @End_Time, ")
                .Append("Min_Quantity_Resolve = @Min_Quantity_Resolve, Cal_Type = @Cal_Type ")
                .Append("where Quota_Arrangement_ID = @Quota_Arrangement_ID");
            SqlParameter[] sqlParams = new SqlParameter[]{
                new SqlParameter("@Begin_Time", SqlDbType.DateTime),
                new SqlParameter("@End_Time", SqlDbType.DateTime),
                new SqlParameter("@Min_Quantity_Resolve", SqlDbType.VarChar),
                new SqlParameter("@Cal_Type", SqlDbType.VarChar),
                new SqlParameter("@Quota_Arrangement_ID", SqlDbType.VarChar)
            };
            sqlParams[0].Value = quotaArrangement.Begin_Time;
            sqlParams[1].Value = quotaArrangement.End_Time;
            sqlParams[2].Value = quotaArrangement.Min_Quantity_Resolve;
            sqlParams[3].Value = quotaArrangement.Cal_Type;
            sqlParams[4].Value = quotaArrangement.Quota_Arrangement_ID;

            return DBHelper.ExecuteNonQuery(strBui.ToString(), sqlParams);
        }
    }
}
