﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.Model.ContractManage.ContractDocs;
using System.Data;
using System.Data.SqlClient;
using Lib.IDAL.ContractManage.ContractDocs;

namespace Lib.SqlServerDAL.ContractManageDAL
{
    public class ContractTemplateSectionDAL : ContractTemplateSectionIDAL
    {
        /// <summary>
        /// 增加新的合同模板章节
        /// </summary>
        /// <param name="contractTemplate"></param>
        /// <returns>受影响的行数</returns>
        public int addNewContractTemplateSection(Contract_Template_Section newSection) {
            if (newSection == null)
                return 0;
            StringBuilder stringBuilder = new StringBuilder("insert into Contract_Template_Section (ID, Contract_Template_ID, Name, Detail, Annotation, Order_Number) values(")
                .Append("@ID, @Contract_Template_ID, @Name, @Detail, @Annotation, @Order_Number")
                .Append(")");
            SqlParameter[] sqlParas = new SqlParameter[]{
                new SqlParameter("@ID", SqlDbType.VarChar),
                new SqlParameter("@Contract_Template_ID", SqlDbType.VarChar),
                new SqlParameter("@Name", SqlDbType.VarChar),
                new SqlParameter("@Detail", SqlDbType.VarChar),
                new SqlParameter("@Annotation", SqlDbType.VarChar),
                new SqlParameter("@Order_Number", SqlDbType.Int)
            };
            sqlParas[0].Value = newSection.ID;
            sqlParas[1].Value = newSection.Contract_Template_ID;
            sqlParas[2].Value = newSection.Name;
            sqlParas[3].Value = newSection.Detail;
            sqlParas[4].Value = newSection.Annotation;
            sqlParas[5].Value = newSection.Order_Number;

            return DBHelper.ExecuteNonQuery(stringBuilder.ToString(), sqlParas);
        }

        /// <summary>
        /// 根据Contact_Template_ID获取所有的section
        /// </summary>
        /// <param name="contractTemplateID"></param>
        /// <returns></returns>
        public LinkedList<Contract_Template_Section> getContractTemplateSectionByContractTemplateID(string contractTemplateID) { 
            LinkedList<Contract_Template_Section> result = new LinkedList<Contract_Template_Section>();
            if(contractTemplateID == null || contractTemplateID.Equals(""))
                return result;

            StringBuilder sb = new StringBuilder("select * from Contract_Template_Section where Contract_Template_ID = '");
            sb.Append(contractTemplateID)
                .Append("' order by Order_Number");

            DataTable dt = DBHelper.ExecuteQueryDT(sb.ToString());
            for (int i = 0; i < dt.Rows.Count; i++) {
                DataRow dr = dt.Rows[i];

                Contract_Template_Section section = new Contract_Template_Section();
                section.ID = dr["ID"].ToString();
                section.Order_Number = Convert.ToInt32(dr["Order_Number"].ToString());
                section.Name = dr["Name"].ToString();
                section.Contract_Template_ID = dr["Contract_Template_ID"].ToString();
                section.Detail = dr["Detail"].ToString();
                section.Annotation = dr["Annotation"].ToString();

                result.AddLast(section);
            }

            return result;
        }
    }
}
