﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.Model.ContractManage.ContractDocs;
using System.Data;
using System.Data.SqlClient;

namespace Lib.SqlServerDAL.ContractManageDAL
{
    public class ContractModelDAL
    {
        private string tableName = "";

        public ContractModelDAL() {
            tableName = "dbo.Contract_Model";
        }

        /// <summary>
        /// 增加新建的合同模板
        /// </summary>
        /// <param name="contractTemplate"></param>
        /// <returns></returns>
        public int addContractTemplate(ContractTemplate contractTemplate) {
            StringBuilder stringBuilder = new StringBuilder("insert into ");
            stringBuilder.Append(tableName + "(Contract_Model_ID, Creater_ID, Contract_Context, Contract_Model_Name, Contract_Model_Ins, State, Begin_Time, End_Time) values(");
            stringBuilder.Append("@Contract_Model_ID, @Creater_ID, @Contract_Context, @Contract_Model_Name, @Contract_Model_Ins, ");
            stringBuilder.Append("@State, @Begin_Time, @End_Time)");
            SqlParameter[] sqlParas = new SqlParameter[]{
                new SqlParameter("@Contract_Model_ID", SqlDbType.VarChar),
                new SqlParameter("@Creater_ID", SqlDbType.VarChar),
                new SqlParameter("@Contract_Context", SqlDbType.VarChar),
                new SqlParameter("@Contract_Model_Name", SqlDbType.VarChar),
                new SqlParameter("@Contract_Model_Ins", SqlDbType.VarChar),
                new SqlParameter("@State", SqlDbType.Bit),
                new SqlParameter("@Begin_Time", SqlDbType.DateTime),
                new SqlParameter("@End_Time", SqlDbType.DateTime)
            };
            sqlParas[0].Value = contractTemplate.ContractTemplateID;
            sqlParas[1].Value = contractTemplate.CreaterID;
            sqlParas[2].Value = contractTemplate.ContractTemplateContext;
            sqlParas[3].Value = contractTemplate.ContractTemplateName;
            sqlParas[4].Value = contractTemplate.ContractTemplateIns;
            sqlParas[5].Value = contractTemplate.ContractTemplateState;
            sqlParas[6].Value = contractTemplate.BeginTime;
            sqlParas[7].Value = contractTemplate.EndTime;

            string sqlText = stringBuilder.ToString();

            return DBHelper.ExecuteNonQuery(sqlText, sqlParas);
        }

        /// <summary>
        /// 根据合同模板ID查询合同
        /// 查询没有结果就返回null，否则返回ContractTemplate的一个实例
        /// </summary>
        /// <param name="contractTemplateID"></param>
        /// <returns></returns>
        public ContractTemplate GetContractTemplateByContractTemplateID(string contractTemplateID) {
            if (contractTemplateID == null || contractTemplateID.Equals(""))
                return null;
            StringBuilder stringBuilder = new StringBuilder("select * from ");
            stringBuilder.Append(tableName + " where Contract_Model_ID = '" + contractTemplateID + "'");
            string sqlText = stringBuilder.ToString();
            DataTable dt = DBHelper.ExecuteQueryDT(sqlText);
            if (dt == null || dt.Rows.Count <= 0)
                return null;
            ContractTemplate contractTemplate = new ContractTemplate();
            DataRow dr = dt.Rows[0];
            contractTemplate.ContractTemplateID = dr["Contract_Model_ID"].ToString();
            contractTemplate.CreaterID = dr["Creater_ID"].ToString();
            contractTemplate.ContractTemplateName = dr["Contract_Model_Name"].ToString();
            contractTemplate.ContractTemplateContext = dr["Contract_Context"].ToString();
            contractTemplate.ContractTemplateIns = dr["Contract_Model_Ins"].ToString();
            contractTemplate.ContractTemplateState = Convert.ToBoolean(dr["State"].ToString());
            contractTemplate.BeginTime = Convert.ToDateTime(dr["Begin_Time"].ToString());
            contractTemplate.EndTime = Convert.ToDateTime(dr["End_Time"].ToString());

            return contractTemplate;
        }

        /// <summary>
        /// 根据采购人员ID获取合同模板
        /// </summary>
        /// <param name="BuyerID">创建人员ID</param>
        /// <param name="state">模板状态，0：未通过审核，1：通过审核，2：全部状态</param>
        /// <returns></returns>
        public LinkedList<ContractTemplate> GetContractTemplateByBuyerID(string BuyerID, int state) { 
            StringBuilder stringBuilder;
            StringBuilder stateStringBuilder = new StringBuilder("");
            //判断查询的模板状态
            //if (state == 0) {
            //    stateStringBuilder.Append(" State = 0 ");
            //}
            //else if (state == 1) {
            //    stateStringBuilder.Append(" State = 1 ");
            //}

            if (state == 0 || state == 1) {
                stateStringBuilder.Append(" State = ").Append(state).Append(" ");
            }

            //判断查询的人员
            if (BuyerID.Equals("all"))
            {
                stringBuilder = new StringBuilder("select * from dbo.Contract_Model ");
                if (state == 0 || state == 1) {
                    stringBuilder.Append(" where ").Append(stateStringBuilder);
                }
                stringBuilder.Append(" order by Creater_ID ");
            }
            else {
                stringBuilder = new StringBuilder("select * from dbo.Contract_Model where Creater_ID = '");
                stringBuilder.Append(BuyerID);
                stringBuilder.Append("'");
                if (state == 0 || state == 1)
                {
                    stringBuilder.Append(" and ").Append(stateStringBuilder);
                }
            }
            string sql = stringBuilder.ToString();
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            if (dt == null || dt.Rows.Count == 0) {
                return null;
            }
            LinkedList<ContractTemplate> list = new LinkedList<ContractTemplate>();
            ContractTemplate contractTemplate;
            for (int i = 0; i < dt.Rows.Count; i++ )
            {
                contractTemplate = new ContractTemplate();
                DataRow dr = dt.Rows[i];
                contractTemplate.ContractTemplateID = dr["Contract_Model_ID"].ToString();
                contractTemplate.CreaterID = dr["Creater_ID"].ToString();
                contractTemplate.ContractTemplateName = dr["Contract_Model_Name"].ToString();
                contractTemplate.ContractTemplateContext = dr["Contract_Context"].ToString();
                contractTemplate.ContractTemplateIns = dr["Contract_Model_Ins"].ToString();
                contractTemplate.ContractTemplateState = Convert.ToBoolean(dr["State"].ToString());
                contractTemplate.CreateTime = Convert.ToDateTime(dr["Create_Time"].ToString());
                contractTemplate.BeginTime = Convert.ToDateTime(dr["Begin_Time"].ToString());
                contractTemplate.EndTime = Convert.ToDateTime(dr["End_Time"].ToString());

                list.AddFirst(contractTemplate);
            }

            return list;
        }

        /// <summary>
        /// 根据ID来更新合同模板的状态
        /// </summary>
        /// <param name="IDList">包含合同ID的list</param>
        /// <param name="changeToState">改变后的状态</param>>
        /// <returns></returns>
        public int UpdateContractTemplateState(LinkedList<string> IDList, bool changeToState) {
            if (IDList == null || IDList.Count == 0)
            {
                return 0;
            }

            int state;
            if (changeToState)
                state = 1;
            else
                state = 0;

            StringBuilder strBui = new StringBuilder("update Contract_Model set State = " + state);
            strBui.Append(" where Contract_Model_ID in (");
            foreach (string id in IDList) {
                strBui.Append(" '" + id + "',");
            }
            //去掉最后一个    ","
            strBui.Remove(strBui.Length - 1, 1);
            strBui.Append(")");

            int result = 0;
            string sql = strBui.ToString();
            result = DBHelper.ExecuteNonQuery(sql);

            return result;
        }

        /// <summary>
        /// 删除指定ID的合同模板
        /// </summary>
        /// <param name="IDList"></param>
        /// <returns></returns>
        public int DeleteContractTemplate(LinkedList<string> IDList) {
            StringBuilder strBui = new StringBuilder("delete Contract_Model ");
            strBui.Append(" where Contract_Model_ID in (");
            foreach (string id in IDList)
            {
                strBui.Append(" '" + id + "',");
            }
            //去掉最后一个    ","
            strBui.Remove(strBui.Length - 1, 1);
            strBui.Append(")");

            int result = DBHelper.ExecuteNonQuery(strBui.ToString());

            return result;
        }

        /// <summary>
        /// 根据SQL语句返回具体结果, LinkedList<ContractTemplate>
        /// </summary>
        /// <param name="sql"></param>
        /// <returns>ContractTemplateList</returns>
        public LinkedList<ContractTemplate> getContractTemplateBySQL(string sql) {
            LinkedList<ContractTemplate> contractTemplateList = new LinkedList<ContractTemplate>();
            DataTable result = DBHelper.ExecuteQueryDT(sql);
            ContractTemplate template;
            for (int i = 0; i < result.Rows.Count; i++) {
                DataRow dr = result.Rows[i];
                template = new ContractTemplate();
                template.ContractTemplateID = dr["Contract_Model_ID"].ToString();
                template.CreaterID = dr["Creater_ID"].ToString();
                template.ContractTemplateName = dr["Contract_Model_Name"].ToString();
                template.ContractTemplateContext = dr["Contract_Context"].ToString();
                template.ContractTemplateIns = dr["Contract_Model_Ins"].ToString();
                template.ContractTemplateState = Convert.ToBoolean(dr["State"].ToString());
                template.CreateTime = Convert.ToDateTime(dr["Create_Time"].ToString());
                template.BeginTime = Convert.ToDateTime(dr["Begin_Time"].ToString());
                template.EndTime = Convert.ToDateTime(dr["End_Time"].ToString());

                contractTemplateList.AddLast(template);
            }

            return contractTemplateList;
        }

        /// <summary>
        /// 根据SQL语句返回具体结果, DataTable
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public DataTable getContractTemplateDataTableBySQL(string sql)
        {
            DataTable result = DBHelper.ExecuteQueryDT(sql);

            return result;
        }
    }
}
