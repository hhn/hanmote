﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.Model.ContractManage.ContractDocs;
using System.Data;
using System.Data.SqlClient;
using Lib.IDAL.ContractManage.ContractDocs;

namespace Lib.SqlServerDAL.ContractManageDAL
{
    //与 合同文本 数据库表交互
    public class ContractTextDAL : ContractTextIDAL
    {
        public ContractTextDAL() { 
        }

        /// <summary>
        /// 获取所有的合同文本信息
        /// </summary>
        /// <returns></returns>
        public DataTable GetAllContractTextInfo(DataTable dtIn) {
            if(dtIn == null){
                return null;
            }

            string sql = "select * from dbo.Contract";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            if (dt == null || dt.Rows.Count == 0)
            {
                return null;
            }
            //目的DR
            DataRow drDes;
            //源DR
            DataRow drSrc;
            for (int i = 0; i < dt.Rows.Count; i++) {
                drDes = dtIn.NewRow();
                drSrc = dt.Rows[i];
                drDes["合同编号"] = drSrc["Contract_ID"].ToString();
                drDes["合同名称"] = drSrc["Contract_Name"].ToString();
                drDes["供应商编号"] = drSrc["Supplier_ID"].ToString();
                drDes["合同类型"] = drSrc["Contract_Type"].ToString();
                drDes["合同生成方式"] = drSrc["Contract_Generate_Type"].ToString();
                drDes["创建时间"] = drSrc["Begin_Time"].ToString();
                drDes["文本状态"] = drSrc["Contract_ID"].ToString();
                drDes["采购员ID"] = drSrc["Contract_ID"].ToString();

                dtIn.Rows.Add(drDes);
            }

            return dtIn;
        }

        public int addContractText(ContractText contractText) {
            if (contractText == null)
                return 0;

            StringBuilder strBui = new StringBuilder("insert into Contract (Contract_ID, Contract_Name, Supplier_ID, Create_Mode, Contract_Type, Status, File_Location, Begin_Time, End_Time, Create_Time, Creater_ID) ")
                .Append("values(@Contract_ID, @Contract_Name, @Supplier_ID, @Create_Mode, @Contract_Type, @Status, @File_Location, @Begin_Time, @End_Time, @Create_Time, @Creater_ID)");
            
            SqlParameter[] sqlParas = new SqlParameter[]{
                new SqlParameter("@Contract_ID", SqlDbType.VarChar),
                new SqlParameter("@Contract_Name", SqlDbType.VarChar),
                new SqlParameter("@Supplier_ID", SqlDbType.VarChar),
                new SqlParameter("@Create_Mode", SqlDbType.VarChar),
                new SqlParameter("@Contract_Type", SqlDbType.VarChar),
                new SqlParameter("@Status", SqlDbType.VarChar),
                new SqlParameter("@File_Location", SqlDbType.VarChar),
                new SqlParameter("@Begin_Time", SqlDbType.DateTime),
                new SqlParameter("@End_Time", SqlDbType.DateTime),
                new SqlParameter("@Create_Time", SqlDbType.DateTime),
                new SqlParameter("@Creater_ID", SqlDbType.VarChar)
            };

            sqlParas[0].Value = contractText.ContractID;
            sqlParas[1].Value = contractText.ContractName;
            sqlParas[2].Value = contractText.SupplierID;
            sqlParas[3].Value = contractText.CreateMode;
            sqlParas[4].Value = contractText.ContractType;
            sqlParas[5].Value = contractText.Status;
            sqlParas[6].Value = contractText.FileLocation;
            sqlParas[7].Value = contractText.BeginTime;
            sqlParas[8].Value = contractText.EndTime;
            sqlParas[9].Value = contractText.CreateTime;
            sqlParas[10].Value = contractText.CreaterID;

            return DBHelper.ExecuteNonQuery(strBui.ToString(), sqlParas);
        }

        /// <summary>
        /// 根据采购人员ID获取合同文本
        /// </summary>
        /// <param name="BuyerID">创建人员ID</param>
        /// <param name="state">模板状态，0：未通过审核，1：通过审核，2：全部状态</param>
        /// <returns></returns>
        public LinkedList<ContractText> GetContractTextByBuyerID(string BuyerID, string state)
        {
            StringBuilder stringBuilder;
            StringBuilder stateStringBuilder = new StringBuilder("");

            if (!state.Equals("全部状态"))
            {
                stateStringBuilder.Append(" State = '").Append(state).Append("' ");
            }

            //判断查询的人员
            if (BuyerID.Equals("all"))
            {
                stringBuilder = new StringBuilder("select * from dbo.Contract ");
                if (!state.Equals("全部状态"))
                {
                    stringBuilder.Append(" where ").Append(stateStringBuilder);
                }
                stringBuilder.Append(" order by Creater_ID ");
            }
            else
            {
                stringBuilder = new StringBuilder("select * from dbo.Contract where Creater_ID = '");
                stringBuilder.Append(BuyerID);
                stringBuilder.Append("'");
                if (!state.Equals("全部状态"))
                {
                    stringBuilder.Append(" and ").Append(stateStringBuilder);
                }
            }
            string sql = stringBuilder.ToString();
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            if (dt == null || dt.Rows.Count == 0)
            {
                return null;
            }
            LinkedList<ContractText> list = new LinkedList<ContractText>();
            ContractText contractText;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                contractText = new ContractText();
                DataRow dr = dt.Rows[i];
                contractText.ContractID = dr["Contract_ID"].ToString();
                contractText.CreaterID = dr["Creater_ID"].ToString();
                contractText.ContractName = dr["Contract_Name"].ToString();
                contractText.SupplierID = dr["Supplier_ID"].ToString();
                contractText.ContractType = dr["Contract_Type"].ToString();
                contractText.CreateMode = dr["Create_Mode"].ToString();
                contractText.Status = dr["Status"].ToString();
                contractText.BeginTime = Convert.ToDateTime(dr["Begin_Time"].ToString());
                contractText.EndTime = Convert.ToDateTime(dr["End_Time"].ToString());

                list.AddFirst(contractText);
            }

            return list;
        }

        /// <summary>
        /// 根据ID来更新合同模板的状态
        /// </summary>
        /// <param name="IDList">包含合同ID的list</param>
        /// <param name="changeToState">改变后的状态</param>>
        /// <returns></returns>
        public int UpdateContractTextState(LinkedList<string> IDList, bool changeToState)
        {
            if (IDList == null || IDList.Count == 0)
            {
                return 0;
            }

            int state;
            if (changeToState)
                state = 0;
            else
                state = 1;

            StringBuilder strBui = new StringBuilder("update Contract set State = " + state);
            strBui.Append(" where Contract_ID in (");
            foreach (string id in IDList)
            {
                strBui.Append(" '" + id + "',");
            }
            //去掉最后一个    ","
            strBui.Remove(strBui.Length - 1, 1);
            strBui.Append(")");

            int result = 0;
            string sql = strBui.ToString();
            result = DBHelper.ExecuteNonQuery(sql);

            return result;
        }

        /// <summary>
        /// 根据SQL语句返回具体结果, LinkedList<ContractText>
        /// </summary>
        /// <param name="sql"></param>
        /// <returns>ContractTextList</returns>
        public LinkedList<ContractText> getContractTextBySQL(string sql)
        {
            LinkedList<ContractText> contractTextList = new LinkedList<ContractText>();
            DataTable result = DBHelper.ExecuteQueryDT(sql);
            ContractText text;
            for (int i = 0; i < result.Rows.Count; i++)
            {
                DataRow dr = result.Rows[i];
                text = new ContractText();
                text.ContractID = dr["Contract_ID"].ToString();
                text.CreaterID = dr["Creater_ID"].ToString();
                text.SupplierID = dr["Supplier_ID"].ToString();
                text.ContractName = dr["Contract_Name"].ToString();
                text.ContractType = dr["Contract_Type"].ToString();
                text.Status = dr["Status"].ToString();
                text.CreaterID = dr["Creater_ID"].ToString();
                text.CreateMode = dr["Create_Mode"].ToString();
                text.CreateTime = Convert.ToDateTime(dr["Create_Time"].ToString());
                text.BeginTime = Convert.ToDateTime(dr["Begin_Time"].ToString());
                text.EndTime = Convert.ToDateTime(dr["End_Time"].ToString());

                contractTextList.AddLast(text);
            }

            return contractTextList;
        }

        /// <summary>
        /// 删除指定ID的合同文本
        /// </summary>
        /// <param name="IDList"></param>
        /// <returns></returns>
        public int DeleteContractTemplate(LinkedList<string> IDList)
        {
            StringBuilder strBui = new StringBuilder("delete Contract ");
            strBui.Append(" where Contract_ID in (");
            foreach (string id in IDList)
            {
                strBui.Append(" '" + id + "',");
            }
            //去掉最后一个    ","
            strBui.Remove(strBui.Length - 1, 1);
            strBui.Append(")");

            int result = DBHelper.ExecuteNonQuery(strBui.ToString());

            return result;
        }
    }
}
