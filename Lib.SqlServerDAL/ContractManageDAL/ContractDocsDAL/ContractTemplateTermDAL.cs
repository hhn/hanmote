﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.Model.ContractManage.ContractDocs;
using System.Data;
using System.Data.SqlClient;
using Lib.IDAL.ContractManage.ContractDocs;

namespace Lib.SqlServerDAL.ContractManageDAL
{
    public class ContractTemplateTermDAL : ContractTemplateTermIDAL
    {
        /// <summary>
        /// 获得所有的Contract_Template_Term_Sample
        /// </summary>
        /// <returns></returns>
        public LinkedList<Contract_Template_Term_Sample> getAllTermSample() {
            LinkedList<Contract_Template_Term_Sample> result = new LinkedList<Contract_Template_Term_Sample>();
            StringBuilder sb = new StringBuilder();
            sb.Append("SELECT * from Contract_Template_Term_Sample");
            DataTable dt = DBHelper.ExecuteQueryDT(sb.ToString());

            if (dt != null) {
                for (int i = 0; i < dt.Rows.Count; i++) {
                    DataRow dr = dt.Rows[i];
                    Contract_Template_Term_Sample term_sample = new Contract_Template_Term_Sample();
                    term_sample.Name = dr["Name"].ToString();
                    term_sample.Class = dr["Class"].ToString();
                    term_sample.FileName = dr["FileName"].ToString();
                    term_sample.Detail = dr["Detail"].ToString();

                    result.AddLast(term_sample);
                }
            }

            return result;
        }

        /// <summary>
        /// 增加新的合同模板条款
        /// </summary>
        /// <param name="contractTemplate"></param>
        /// <returns>受影响的行数</returns>
        public int addNewContractTemplateTerm(Contract_Template_Term newTerm)
        {
            if (newTerm == null)
                return 0;
            StringBuilder stringBuilder = new StringBuilder("insert into Contract_Template_Term (Order_Number, Term_Sample_ID, Section_ID, Detail, Annotation) values(")
                .Append("@Order_Number, @Term_Sample_ID, @Section_ID, @Detail, @Annotation")
                .Append(")");
            SqlParameter[] sqlParas = new SqlParameter[]{
                new SqlParameter("@Order_Number", SqlDbType.Int),
                new SqlParameter("@Term_Sample_ID", SqlDbType.VarChar),
                new SqlParameter("@Section_ID", SqlDbType.VarChar),
                new SqlParameter("@Detail", SqlDbType.VarChar),
                new SqlParameter("@Annotation", SqlDbType.VarChar)
            };
            sqlParas[0].Value = newTerm.Order_Number;
            sqlParas[1].Value = newTerm.Term_Sample_ID;
            sqlParas[2].Value = newTerm.Section_ID;
            sqlParas[3].Value = newTerm.Detail;
            sqlParas[4].Value = newTerm.Annotation;

            return DBHelper.ExecuteNonQuery(stringBuilder.ToString(), sqlParas);
        }

        /// <summary>
        /// 根据Contact_Template_Section_ID获取所有的term
        /// </summary>
        /// <param name="contractTemplateID"></param>
        /// <returns></returns>
        public LinkedList<Contract_Template_Term> getContractTemplateTermByContractTemplateID(string contractTemplateSectionID)
        {
            LinkedList<Contract_Template_Term> result = new LinkedList<Contract_Template_Term>();
            if (contractTemplateSectionID == null || contractTemplateSectionID.Equals(""))
                return result;

            StringBuilder sb = new StringBuilder("select * from Contract_Template_Term where Section_ID = '");
            sb.Append(contractTemplateSectionID)
                .Append("' order by Order_Number");

            DataTable dt = DBHelper.ExecuteQueryDT(sb.ToString());
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataRow dr = dt.Rows[i];

                Contract_Template_Term term = new Contract_Template_Term();
                term.Term_Sample_ID = dr["Term_Sample_ID"].ToString();
                term.Order_Number = Convert.ToInt32(dr["Order_Number"].ToString());
                term.Section_ID = dr["Section_ID"].ToString();
                term.Detail = dr["Detail"].ToString();
                term.Annotation = dr["Annotation"].ToString();

                result.AddLast(term);
            }

            return result;
        }

        ///
        public Contract_Template_Term_Sample getTermSampleByName(string name)
        {
            Contract_Template_Term_Sample result = new Contract_Template_Term_Sample();
            StringBuilder sb = new StringBuilder();
            sb.Append("Select * From Contract_Template_Term_Sample Where Name = '");
            sb.Append(name);
            sb.Append("'");
            DataTable dt = DBHelper.ExecuteQueryDT(sb.ToString());
            if (dt != null)
            {
                DataRow dr = dt.Rows[0];
                result.Name = dr["Name"].ToString();
                result.Class = dr["Class"].ToString();
                result.FileName = dr["FileName"].ToString();
                result.Detail = dr["Detail"].ToString();
            }
            return result;
        }
    }
}
