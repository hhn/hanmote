﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.Model.ContractManage.OutlineAgreement;
using Lib.IDAL.ContractManage.OutlineAgreement;
using System.Data;
using System.Data.SqlClient;

namespace Lib.SqlServerDAL.ContractManageDAL.OutlineAgreementDAL
{
    public class SchedulingAgreementDAL : SchedulingAgreementIDAL
    {
        /// <summary>
        /// 添加协议合同
        /// </summary>
        /// <param name="schedulingAgreement"></param>
        /// <returns></returns>
        private int addSchedulingAgreement(Scheduling_Agreement schedulingAgreement)
        {
            # region 添加计划协议基本数据
            StringBuilder strBui = new StringBuilder("insert into Scheduling_Agreement (")
                .Append("Scheduling_Agreement_ID, Supplier_ID, Supplier_Name, ")
                .Append("Scheduling_Agreement_Type, Purchase_Group, Purchase_Organization, Create_Time, ")
                .Append("Creater_ID, Status, Factory, Storage_Location, Begin_Time, End_Time, ")
                .Append("IT_Term_Code, IT_Term_Text, Payment_Type, Days1, Discount_Rate1, ")
                .Append("Days2, Discount_Rate2, Days3, Target_Value, Currency_Type, Exchange_Rate")
                .Append(") values(")
                .Append("@Scheduling_Agreement_ID, @Supplier_ID, @Supplier_Name, ")
                .Append("@Scheduling_Agreement_Type, @Purchase_Group, @Purchase_Organization, @Create_Time, ")
                .Append("@Creater_ID, @Status, @Factory, @Storage_Location, @Begin_Time, @End_Time, ")
                .Append("@IT_Term_Code, @IT_Term_Text, @Payment_Type, @Days1, @Discount_Rate1, ")
                .Append("@Days2, @Discount_Rate2, @Days3, @Target_Value, @Currency_Type, @Exchange_Rate")
                .Append(")");

            SqlParameter[] sqlParas = new SqlParameter[]{
                    new SqlParameter("@Scheduling_Agreement_ID", SqlDbType.VarChar),
                    new SqlParameter("@Supplier_ID", SqlDbType.VarChar),
                    new SqlParameter("@Supplier_Name", SqlDbType.VarChar),
                    new SqlParameter("@Scheduling_Agreement_Type", SqlDbType.VarChar),
                    new SqlParameter("@Purchase_Group", SqlDbType.VarChar),
                    new SqlParameter("@Purchase_Organization", SqlDbType.VarChar),
                    new SqlParameter("@Create_Time", SqlDbType.DateTime),
                    new SqlParameter("@Creater_ID", SqlDbType.VarChar),
                    new SqlParameter("@Status", SqlDbType.VarChar),
                    new SqlParameter("@Factory", SqlDbType.VarChar),
                    new SqlParameter("@Storage_Location", SqlDbType.VarChar),
                    new SqlParameter("@Begin_Time", SqlDbType.DateTime),
                    new SqlParameter("@End_Time", SqlDbType.DateTime),
                    new SqlParameter("@IT_Term_Code", SqlDbType.VarChar),
                    new SqlParameter("@IT_Term_Text", SqlDbType.VarChar),
                    new SqlParameter("@Payment_Type", SqlDbType.VarChar),
                    new SqlParameter("@Days1", SqlDbType.Int),
                    new SqlParameter("@Discount_Rate1", SqlDbType.Decimal),
                    new SqlParameter("@Days2", SqlDbType.Int),
                    new SqlParameter("@Discount_Rate2", SqlDbType.Decimal),
                    new SqlParameter("@Days3", SqlDbType.Int),
                    new SqlParameter("@Target_Value", SqlDbType.Decimal),
                    new SqlParameter("@Currency_Type", SqlDbType.VarChar),
                    new SqlParameter("@Exchange_Rate", SqlDbType.Decimal)
            };

            sqlParas[0].Value = schedulingAgreement.Scheduling_Agreement_ID;
            sqlParas[1].Value = schedulingAgreement.Supplier_ID;
            sqlParas[2].Value = schedulingAgreement.Supplier_Name;
            sqlParas[3].Value = schedulingAgreement.Scheduling_Agreement_Type;
            sqlParas[4].Value = schedulingAgreement.Purchase_Group;
            sqlParas[5].Value = schedulingAgreement.Purchase_Organization;
            sqlParas[6].Value = schedulingAgreement.Create_Time;
            sqlParas[7].Value = schedulingAgreement.Creater_ID;
            sqlParas[8].Value = schedulingAgreement.Status;
            sqlParas[9].Value = schedulingAgreement.Factory;
            sqlParas[10].Value = schedulingAgreement.Storage_Location;
            sqlParas[11].Value = schedulingAgreement.Begin_Time;
            sqlParas[12].Value = schedulingAgreement.End_Time;
            sqlParas[13].Value = schedulingAgreement.IT_Term_Code;
            sqlParas[14].Value = schedulingAgreement.IT_Term_Text;
            sqlParas[15].Value = schedulingAgreement.Payment_Type;
            sqlParas[16].Value = schedulingAgreement.Days1;
            sqlParas[17].Value = schedulingAgreement.Discount_Rate1;
            sqlParas[18].Value = schedulingAgreement.Days2;
            sqlParas[19].Value = schedulingAgreement.Discount_Rate2;
            sqlParas[20].Value = schedulingAgreement.Days3;
            sqlParas[21].Value = schedulingAgreement.Target_Value;
            sqlParas[22].Value = schedulingAgreement.Currency_Type;
            sqlParas[23].Value = schedulingAgreement.Exchange_Rate;

            # endregion

            return DBHelper.ExecuteNonQuery(strBui.ToString(), sqlParas);
        }

        /// <summary>
        /// 根据ID获取Scheduling_Agreement，为空返回null
        /// </summary>
        /// <param name="schedulingAgreementID"></param>
        /// <param name="schedulingAgreementVersion"></param>
        /// <returns></returns>
        public Scheduling_Agreement getSchedulingAgreementByID(string schedulingAgreementID)
        {

            Scheduling_Agreement schedulingAgreement = new Scheduling_Agreement();
            StringBuilder strBui = new StringBuilder("select * from Scheduling_Agreement where Scheduling_Agreement_ID = ")
                .Append("@Scheduling_Agreement_ID");
            SqlParameter[] sqlParas = new SqlParameter[]{
                new SqlParameter("@Scheduling_Agreement_ID", SqlDbType.VarChar)
            };
            sqlParas[0].Value = schedulingAgreementID;

            DataTable dt = DBHelper.ExecuteQueryDT(strBui.ToString(), sqlParas);
            if (dt.Rows.Count <= 0)
                return null;

            DataRow dr = dt.Rows[0];
            schedulingAgreement.Scheduling_Agreement_ID = dr["Scheduling_Agreement_ID"].ToString();
            schedulingAgreement.Supplier_ID = dr["Supplier_ID"].ToString();
            schedulingAgreement.Supplier_Name = dr["Supplier_Name"].ToString();
            schedulingAgreement.Scheduling_Agreement_Type = dr["Scheduling_Agreement_Type"].ToString();
            schedulingAgreement.Purchase_Group = dr["Purchase_Group"].ToString();
            schedulingAgreement.Purchase_Organization = dr["Purchase_Organization"].ToString();
            schedulingAgreement.Create_Time = Convert.ToDateTime(dr["Create_Time"].ToString());
            schedulingAgreement.Creater_ID = dr["Creater_ID"].ToString();
            schedulingAgreement.Status = dr["Status"].ToString();
            schedulingAgreement.Factory = dr["Factory"].ToString();
            schedulingAgreement.Storage_Location = dr["Storage_Location"].ToString();
            schedulingAgreement.Begin_Time = Convert.ToDateTime(dr["Begin_Time"].ToString());
            schedulingAgreement.End_Time = Convert.ToDateTime(dr["End_Time"].ToString());
            schedulingAgreement.IT_Term_Code = dr["IT_Term_Code"].ToString();
            schedulingAgreement.IT_Term_Text = dr["IT_Term_Text"].ToString();
            schedulingAgreement.Payment_Type = dr["Payment_Type"].ToString();
            schedulingAgreement.Days1 = Convert.ToInt32(dr["Days1"].ToString());
            schedulingAgreement.Discount_Rate1 = Convert.ToDouble(dr["Discount_Rate1"].ToString());
            schedulingAgreement.Days2 = Convert.ToInt32(dr["Days2"].ToString());
            schedulingAgreement.Discount_Rate2 = Convert.ToDouble(dr["Discount_Rate2"].ToString());
            schedulingAgreement.Days3 = Convert.ToInt32(dr["Days3"].ToString());
            schedulingAgreement.Target_Value = Convert.ToDouble(dr["Target_Value"].ToString());
            schedulingAgreement.Currency_Type = dr["Currency_Type"].ToString();
            schedulingAgreement.Exchange_Rate = Convert.ToDouble(dr["Exchange_Rate"].ToString());

            return schedulingAgreement;
        }

        /// <summary>
        /// 根据指定的条件获取Scheduling_Agreement
        /// </summary>
        /// <param name="conditions"></param>
        /// <param name="beginTime"></param>
        /// <param name="endTime"></param>
        /// <returns></returns>
        public List<Scheduling_Agreement> getSchedulingAgreementByCondition(Dictionary<string, object> conditions,
            DateTime beginTime, DateTime endTime)
        {
            List<Scheduling_Agreement> agreementList = new List<Scheduling_Agreement>();

            StringBuilder strBui = new StringBuilder("select * from Scheduling_Agreement where Create_Time >= @Begin_Time and ")
                .Append("Create_Time <= @End_Time ");

            //因为变量数目不确定，采用list
            //List<SqlParameter> sqlParasList = new List<SqlParameter>();
            //SqlParameter sqlPara = new SqlParameter("@Begin_Time", SqlDbType.DateTime);
            //sqlPara.Value = beginTime;
            //sqlParasList.Add(sqlPara);
            //sqlPara = new SqlParameter("@End_Time", SqlDbType.DateTime);
            //sqlPara.Value = endTime;
            //sqlParasList.Add(sqlPara);

            foreach (string key in conditions.Keys)
            {
                string value = (string)conditions[key];
                if (value != null && !value.Equals(""))
                {
                    strBui.Append(" and ").Append(key).Append(" = @")
                        .Append(key);
                    //sqlPara = new SqlParameter("@" + key, SqlDbType.VarChar);
                    //sqlPara.Value = value;
                    //sqlParasList.Add(sqlPara);
                }
            }

            //这里只是定义变量，可以定义前面的sql语句用不到的变量
            //所以这里声明所有的变量
            SqlParameter[] sqlParas = new SqlParameter[]{
                new SqlParameter("@Begin_Time", SqlDbType.DateTime),
                new SqlParameter("@End_Time", SqlDbType.DateTime),
                new SqlParameter("@Scheduling_Agreement_ID", SqlDbType.VarChar),
                new SqlParameter("@Supplier_ID", SqlDbType.VarChar),
                new SqlParameter("@Scheduling_Agreement_Type", SqlDbType.VarChar),
                new SqlParameter("@Status", SqlDbType.VarChar)
            };

            sqlParas[0].Value = beginTime;
            sqlParas[1].Value = endTime;
            sqlParas[2].Value = (string)conditions["Scheduling_Agreement_ID"];
            sqlParas[3].Value = (string)conditions["Supplier_ID"];
            sqlParas[4].Value = (string)conditions["Scheduling_Agreement_Type"];
            sqlParas[5].Value = (string)conditions["Status"];

            DataTable dt = DBHelper.ExecuteQueryDT(strBui.ToString(), sqlParas);
            foreach (DataRow dr in dt.Rows)
            {
                Scheduling_Agreement schedulingAgreement = new Scheduling_Agreement();
                schedulingAgreement.Scheduling_Agreement_ID = dr["Scheduling_Agreement_ID"].ToString();
                schedulingAgreement.Supplier_ID = dr["Supplier_ID"].ToString();
                schedulingAgreement.Scheduling_Agreement_Type = dr["Scheduling_Agreement_Type"].ToString();
                schedulingAgreement.Purchase_Group = dr["Purchase_Group"].ToString();
                schedulingAgreement.Purchase_Organization = dr["Purchase_Organization"].ToString();
                schedulingAgreement.Create_Time = Convert.ToDateTime(dr["Create_Time"].ToString());
                schedulingAgreement.Creater_ID = dr["Creater_ID"].ToString();
                schedulingAgreement.Status = dr["Status"].ToString();
                schedulingAgreement.Factory = dr["Factory"].ToString();
                schedulingAgreement.Storage_Location = dr["Storage_Location"].ToString();
                schedulingAgreement.Begin_Time = Convert.ToDateTime(dr["Begin_Time"].ToString());
                schedulingAgreement.End_Time = Convert.ToDateTime(dr["End_Time"].ToString());
                schedulingAgreement.IT_Term_Code = dr["IT_Term_Code"].ToString();
                schedulingAgreement.IT_Term_Text = dr["IT_Term_Text"].ToString();
                schedulingAgreement.Payment_Type = dr["Payment_Type"].ToString();
                schedulingAgreement.Days1 = Convert.ToInt32(dr["Days1"].ToString());
                schedulingAgreement.Discount_Rate1 = Convert.ToDouble(dr["Discount_Rate1"].ToString());
                schedulingAgreement.Days2 = Convert.ToInt32(dr["Days2"].ToString());
                schedulingAgreement.Discount_Rate2 = Convert.ToDouble(dr["Discount_Rate2"].ToString());
                schedulingAgreement.Days3 = Convert.ToInt32(dr["Days3"].ToString());
                schedulingAgreement.Target_Value = Convert.ToDouble(dr["Target_Value"].ToString());
                schedulingAgreement.Currency_Type = dr["Currency_Type"].ToString();
                schedulingAgreement.Exchange_Rate = Convert.ToDouble(dr["Exchange_Rate"].ToString());

                agreementList.Add(schedulingAgreement);
            }

            return agreementList;
        }

        /// <summary>
        /// 添加新的计划协议
        /// </summary>
        /// <param name="schedulingAgreement"></param>
        /// <param name="itemList"></param>
        /// <returns></returns>
        public int addSchedulingAgreement(Scheduling_Agreement schedulingAgreement, List<Scheduling_Agreement_Item> itemList)
        {
            List<string> sqlList = new List<string>();
            List<SqlParameter[]> sqlParamsList = new List<SqlParameter[]>();

            #region 添加计划协议基本数据

            StringBuilder strBui = new StringBuilder("insert into Scheduling_Agreement (")
                    .Append("Scheduling_Agreement_ID, Supplier_ID, ")
                    .Append("Scheduling_Agreement_Type, Purchase_Group, Purchase_Organization, Create_Time, ")
                    .Append("Creater_ID, Status, Factory, Storage_Location, Begin_Time, End_Time, ")
                    .Append("IT_Term_Code, IT_Term_Text, Payment_Type, Days1, Discount_Rate1, ")
                    .Append("Days2, Discount_Rate2, Days3, Target_Value, Currency_Type, Exchange_Rate")
                    .Append(") values (")
                    .Append("@Scheduling_Agreement_ID, @Supplier_ID, ")
                    .Append("@Scheduling_Agreement_Type, @Purchase_Group, @Purchase_Organization, @Create_Time, ")
                    .Append("@Creater_ID, @Status, @Factory, @Storage_Location, @Begin_Time, @End_Time, ")
                    .Append("@IT_Term_Code, @IT_Term_Text, @Payment_Type, @Days1, @Discount_Rate1, ")
                    .Append("@Days2, @Discount_Rate2, @Days3, @Target_Value, @Currency_Type, @Exchange_Rate")
                    .Append(")");
            SqlParameter[] sqlParas = new SqlParameter[]{
                    new SqlParameter("@Scheduling_Agreement_ID", SqlDbType.VarChar),
                    new SqlParameter("@Supplier_ID", SqlDbType.VarChar),
                    new SqlParameter("@Scheduling_Agreement_Type", SqlDbType.VarChar),
                    new SqlParameter("@Purchase_Group", SqlDbType.VarChar),
                    new SqlParameter("@Purchase_Organization", SqlDbType.VarChar),
                    new SqlParameter("@Create_Time", SqlDbType.DateTime),
                    new SqlParameter("@Creater_ID", SqlDbType.VarChar),
                    new SqlParameter("@Status", SqlDbType.VarChar),
                    new SqlParameter("@Factory", SqlDbType.VarChar),
                    new SqlParameter("@Storage_Location", SqlDbType.VarChar),
                    new SqlParameter("@Begin_Time", SqlDbType.DateTime),
                    new SqlParameter("@End_Time", SqlDbType.DateTime),
                    new SqlParameter("@IT_Term_Code", SqlDbType.VarChar),
                    new SqlParameter("@IT_Term_Text", SqlDbType.VarChar),
                    new SqlParameter("@Payment_Type", SqlDbType.VarChar),
                    new SqlParameter("@Days1", SqlDbType.Int),
                    new SqlParameter("@Discount_Rate1", SqlDbType.Decimal),
                    new SqlParameter("@Days2", SqlDbType.Int),
                    new SqlParameter("@Discount_Rate2", SqlDbType.Decimal),
                    new SqlParameter("@Days3", SqlDbType.Int),
                    new SqlParameter("@Target_Value", SqlDbType.Decimal),
                    new SqlParameter("@Currency_Type", SqlDbType.VarChar),
                    new SqlParameter("@Exchange_Rate", SqlDbType.Decimal)
            };

            sqlParas[0].Value = schedulingAgreement.Scheduling_Agreement_ID;
            sqlParas[1].Value = schedulingAgreement.Supplier_ID;
            sqlParas[2].Value = schedulingAgreement.Scheduling_Agreement_Type;
            sqlParas[3].Value = schedulingAgreement.Purchase_Group;
            sqlParas[4].Value = schedulingAgreement.Purchase_Organization;
            sqlParas[5].Value = schedulingAgreement.Create_Time;
            sqlParas[6].Value = schedulingAgreement.Creater_ID;
            sqlParas[7].Value = schedulingAgreement.Status;
            sqlParas[8].Value = schedulingAgreement.Factory;
            sqlParas[9].Value = schedulingAgreement.Storage_Location;
            sqlParas[10].Value = schedulingAgreement.Begin_Time;
            sqlParas[11].Value = schedulingAgreement.End_Time;
            sqlParas[12].Value = schedulingAgreement.IT_Term_Code;
            sqlParas[13].Value = schedulingAgreement.IT_Term_Text;
            sqlParas[14].Value = schedulingAgreement.Payment_Type;
            sqlParas[15].Value = schedulingAgreement.Days1;
            sqlParas[16].Value = schedulingAgreement.Discount_Rate1;
            sqlParas[17].Value = schedulingAgreement.Days2;
            sqlParas[18].Value = schedulingAgreement.Discount_Rate2;
            sqlParas[19].Value = schedulingAgreement.Days3;
            sqlParas[20].Value = schedulingAgreement.Target_Value;
            sqlParas[21].Value = schedulingAgreement.Currency_Type;
            sqlParas[22].Value = schedulingAgreement.Exchange_Rate;

            //加入到List中
            sqlList.Add(strBui.ToString());
            sqlParamsList.Add(sqlParas);

            #endregion

            #region 添加框架协议项

            foreach (Scheduling_Agreement_Item item in itemList)
            {
                StringBuilder strBui2 = new StringBuilder("insert into Scheduling_Agreement_Item ( ")
                .Append("Scheduling_Agreement_ID, Item_Num, I, A, ")
                .Append("Material_ID, Short_Text, Target_Quantity, OUn, Net_Price, ")
                .Append("Every, OPU, Material_Group, Factory ")
                .Append(") values( ")
                .Append("@Scheduling_Agreement_ID, @Item_Num, @I, @A, ")
                .Append("@Material_ID, @Short_Text, @Target_Quantity, @OUn, @Net_Price, ")
                .Append("@Every, @OPU, @Material_Group, @Factory )");

                SqlParameter[] sqlParas2 = new SqlParameter[]{
                    new SqlParameter("@Scheduling_Agreement_ID", SqlDbType.VarChar),
                    new SqlParameter("@Item_Num", SqlDbType.Int),
                    new SqlParameter("@I", SqlDbType.VarChar),
                    new SqlParameter("@A", SqlDbType.VarChar),
                    new SqlParameter("@Material_ID", SqlDbType.VarChar),
                    new SqlParameter("@Short_Text", SqlDbType.VarChar),
                    new SqlParameter("@Target_Quantity", SqlDbType.Decimal),
                    new SqlParameter("@OUn", SqlDbType.VarChar),
                    new SqlParameter("@Net_Price", SqlDbType.Decimal),
                    new SqlParameter("@Every", SqlDbType.Decimal),
                    new SqlParameter("@OPU", SqlDbType.VarChar),
                    new SqlParameter("@Material_Group", SqlDbType.VarChar),
                    new SqlParameter("@Factory", SqlDbType.VarChar)
                };

                sqlParas2[0].Value = item.Scheduling_Agreement_ID;
                sqlParas2[1].Value = item.Item_Num;
                sqlParas2[2].Value = item.I;
                sqlParas2[3].Value = item.A;
                sqlParas2[4].Value = item.Material_ID;
                sqlParas2[5].Value = item.Short_Text;
                sqlParas2[6].Value = item.Target_Quantity;
                sqlParas2[7].Value = item.OUn;
                sqlParas2[8].Value = item.Net_Price;
                sqlParas2[9].Value = item.Every;
                sqlParas2[10].Value = item.OPU;
                sqlParas2[11].Value = item.Material_Group;
                sqlParas2[12].Value = item.Factory;

                //加入到List
                sqlList.Add(strBui2.ToString());
                sqlParamsList.Add(sqlParas2);
            }

            #endregion

            return DBHelper.ExecuteNonQuery(sqlList, sqlParamsList);
        }

        /// <summary>
        /// 更新计划协议(基本数据+框架协议项)
        /// </summary>
        /// <param name="schedulingAgreement"></param>
        /// <param name="itemList"></param>
        /// <returns></returns>
        public int updateSchedulingAgreement(Scheduling_Agreement schedulingAgreement, List<Scheduling_Agreement_Item> itemList)
        {
            List<string> sqlList = new List<string>();
            List<SqlParameter[]> sqlParamsList = new List<SqlParameter[]>();

            # region 更新协议合同基本数据

            StringBuilder strBui = new StringBuilder("update Scheduling_Agreement set ")
                .Append("Purchase_Group = @Purchase_Group, Purchase_Organization = @Purchase_Organization, ")
                .Append("Status = @Status, Factory = @Factory, Storage_Location = @Storage_Location, ")
                .Append("Begin_Time = @Begin_Time, End_Time = @End_Time, IT_Term_Code = @IT_Term_Code, ")
                .Append("IT_Term_Text = @IT_Term_Text, Payment_Type = @Payment_Type, Days1 = @Days1, ")
                .Append("Discount_Rate1 = @Discount_Rate1, Days2 = @Days2, Discount_Rate2 = @Discount_Rate2, ")
                .Append("Days3 = @Days3, Target_Value = @Target_Value, Currency_Type = @Currency_Type, ")
                .Append("Exchange_Rate = @Exchange_Rate ")
                .Append("where Scheduling_Agreement_ID = @Scheduling_Agreement_ID");

            SqlParameter[] sqlParas = new SqlParameter[]{
                new SqlParameter("@Purchase_Group", SqlDbType.VarChar),
                new SqlParameter("@Purchase_Organization", SqlDbType.VarChar),
                new SqlParameter("@Status", SqlDbType.VarChar),
                new SqlParameter("@Factory", SqlDbType.VarChar),
                new SqlParameter("@Storage_Location", SqlDbType.VarChar),
                new SqlParameter("@Begin_Time", SqlDbType.DateTime),
                new SqlParameter("@End_Time", SqlDbType.DateTime),
                new SqlParameter("@IT_Term_Code", SqlDbType.VarChar),
                new SqlParameter("@IT_Term_Text", SqlDbType.VarChar),
                new SqlParameter("@Payment_Type", SqlDbType.VarChar),
                new SqlParameter("@Days1", SqlDbType.Int),
                new SqlParameter("@Discount_Rate1", SqlDbType.Decimal),
                new SqlParameter("@Days2", SqlDbType.Int),
                new SqlParameter("@Discount_Rate2", SqlDbType.Decimal),
                new SqlParameter("@Days3", SqlDbType.Int),
                new SqlParameter("@Target_Value", SqlDbType.Decimal),
                new SqlParameter("@Currency_Type", SqlDbType.VarChar),
                new SqlParameter("@Exchange_Rate", SqlDbType.Decimal),
                new SqlParameter("@Scheduling_Agreement_ID", SqlDbType.VarChar)
            };

            sqlParas[0].Value = schedulingAgreement.Purchase_Group;
            sqlParas[1].Value = schedulingAgreement.Purchase_Organization;
            sqlParas[2].Value = schedulingAgreement.Status;
            sqlParas[3].Value = schedulingAgreement.Factory;
            sqlParas[4].Value = schedulingAgreement.Storage_Location;
            sqlParas[5].Value = schedulingAgreement.Begin_Time;
            sqlParas[6].Value = schedulingAgreement.End_Time;
            sqlParas[7].Value = schedulingAgreement.IT_Term_Code;
            sqlParas[8].Value = schedulingAgreement.IT_Term_Text;
            sqlParas[9].Value = schedulingAgreement.Payment_Type;
            sqlParas[10].Value = schedulingAgreement.Days1;
            sqlParas[11].Value = schedulingAgreement.Discount_Rate1;
            sqlParas[12].Value = schedulingAgreement.Days2;
            sqlParas[13].Value = schedulingAgreement.Discount_Rate2;
            sqlParas[14].Value = schedulingAgreement.Days3;
            sqlParas[15].Value = schedulingAgreement.Target_Value;
            sqlParas[16].Value = schedulingAgreement.Currency_Type;
            sqlParas[17].Value = schedulingAgreement.Exchange_Rate;
            sqlParas[18].Value = schedulingAgreement.Scheduling_Agreement_ID;

            sqlList.Add(strBui.ToString());
            sqlParamsList.Add(sqlParas);

            # endregion

            # region 删除协议合同项

            StringBuilder strBui2 = new StringBuilder("delete from Scheduling_Agreement_Item ")
                .Append("where Scheduling_Agreement_ID = @Scheduling_Agreement_ID");

            SqlParameter[] sqlParas2 = new SqlParameter[]{
                new SqlParameter("@Scheduling_Agreement_ID", SqlDbType.VarChar)
            };

            sqlParas2[0].Value = schedulingAgreement.Scheduling_Agreement_ID;

            sqlList.Add(strBui2.ToString());
            sqlParamsList.Add(sqlParas2);

            # endregion

            # region 添加新的协议合同项

            foreach (Scheduling_Agreement_Item item in itemList)
            {
                StringBuilder strBui3 = new StringBuilder("insert into Scheduling_Agreement_Item ( ")
                    .Append("Scheduling_Agreement_ID, Scheduling_Agreement_Version, Item_Num, I, A, ")
                    .Append("Material_ID, Short_Text, Target_Quantity, OUn, Net_Price, ")
                    .Append("Every, OPU, Material_Group, Factory ")
                    .Append(") values( ")
                    .Append("@Scheduling_Agreement_ID, @Item_Num, @I, @A, ")
                    .Append("@Material_ID, @Short_Text, @Target_Quantity, @OUn, @Net_Price, ")
                    .Append("@Every, @OPU, @Material_Group, @Factory )");

                SqlParameter[] sqlParas3 = new SqlParameter[]{
                    new SqlParameter("@Scheduling_Agreement_ID", SqlDbType.VarChar),
                    new SqlParameter("@Item_Num", SqlDbType.Int),
                    new SqlParameter("@I", SqlDbType.VarChar),
                    new SqlParameter("@A", SqlDbType.VarChar),
                    new SqlParameter("@Material_ID", SqlDbType.VarChar),
                    new SqlParameter("@Short_Text", SqlDbType.VarChar),
                    new SqlParameter("@Target_Quantity", SqlDbType.Decimal),
                    new SqlParameter("@OUn", SqlDbType.VarChar),
                    new SqlParameter("@Net_Price", SqlDbType.Decimal),
                    new SqlParameter("@Every", SqlDbType.Decimal),
                    new SqlParameter("@OPU", SqlDbType.VarChar),
                    new SqlParameter("@Material_Group", SqlDbType.VarChar),
                    new SqlParameter("@Factory", SqlDbType.VarChar)
                };

                sqlParas3[0].Value = item.Scheduling_Agreement_ID;
                sqlParas3[1].Value = item.Item_Num;
                sqlParas3[2].Value = item.I;
                sqlParas3[3].Value = item.A;
                sqlParas3[4].Value = item.Material_ID;
                sqlParas3[5].Value = item.Short_Text;
                sqlParas3[6].Value = item.Target_Quantity;
                sqlParas3[7].Value = item.OUn;
                sqlParas3[8].Value = item.Net_Price;
                sqlParas3[9].Value = item.Every;
                sqlParas3[10].Value = item.OPU;
                sqlParas3[11].Value = item.Material_Group;
                sqlParas3[12].Value = item.Factory;

                sqlList.Add(strBui3.ToString());
                sqlParamsList.Add(sqlParas3);
            }

            # endregion

            return DBHelper.ExecuteNonQuery(sqlList, sqlParamsList);
        }



        /// <summary>
        /// 更新计划协议抬头信息
        /// </summary>
        /// <param name="schedulingAgreementID"></param>
        /// <param name="schedulingAgreement"></param>
        /// <returns></returns>
        public int updateSchedulingAgreement(string schedulingAgreementID,
            Scheduling_Agreement schedulingAgreement)
        {

            StringBuilder strBui = new StringBuilder("update Scheduling_Agreement set ")
                .Append("Purchase_Group = @Purchase_Group, Purchase_Organization = @Purchase_Organization, ")
                .Append("Status = @Status, Factory = @Factory, Storage_Location = @Storage_Location, ")
                .Append("Begin_Time = @Begin_Time, End_Time = @End_Time, IT_Term_Code = @IT_Term_Code, ")
                .Append("IT_Term_Text = @IT_Term_Text, Payment_Type = @Payment_Type, Days1 = @Days1, ")
                .Append("Discount_Rate1 = @Discount_Rate1, Days2 = @Days2, Discount_Rate2 = @Discount_Rate2, ")
                .Append("Days3 = @Days3, Target_Value = @Target_Value, Currency_Type = @Currency_Type, ")
                .Append("Exchange_Rate = @Exchange_Rate ")
                .Append("where Scheduling_Agreement_ID = @Scheduling_Agreement_ID");

            SqlParameter[] sqlParas = new SqlParameter[]{
                new SqlParameter("@Purchase_Group", SqlDbType.VarChar),
                new SqlParameter("@Purchase_Organization", SqlDbType.VarChar),
                new SqlParameter("@Status", SqlDbType.VarChar),
                new SqlParameter("@Factory", SqlDbType.VarChar),
                new SqlParameter("@Storage_Location", SqlDbType.VarChar),
                new SqlParameter("@Begin_Time", SqlDbType.DateTime),
                new SqlParameter("@End_Time", SqlDbType.DateTime),
                new SqlParameter("@IT_Term_Code", SqlDbType.VarChar),
                new SqlParameter("@IT_Term_Text", SqlDbType.VarChar),
                new SqlParameter("@Payment_Type", SqlDbType.VarChar),
                new SqlParameter("@Days1", SqlDbType.Int),
                new SqlParameter("@Discount_Rate1", SqlDbType.Decimal),
                new SqlParameter("@Days2", SqlDbType.Int),
                new SqlParameter("@Discount_Rate2", SqlDbType.Decimal),
                new SqlParameter("@Days3", SqlDbType.Int),
                new SqlParameter("@Target_Value", SqlDbType.Decimal),
                new SqlParameter("@Currency_Type", SqlDbType.VarChar),
                new SqlParameter("@Exchange_Rate", SqlDbType.Decimal),
                new SqlParameter("@Scheduling_Agreement_ID", SqlDbType.VarChar)
            };

            sqlParas[0].Value = schedulingAgreement.Purchase_Group;
            sqlParas[1].Value = schedulingAgreement.Purchase_Organization;
            sqlParas[2].Value = schedulingAgreement.Status;
            sqlParas[3].Value = schedulingAgreement.Factory;
            sqlParas[4].Value = schedulingAgreement.Storage_Location;
            sqlParas[5].Value = schedulingAgreement.Begin_Time;
            sqlParas[6].Value = schedulingAgreement.End_Time;
            sqlParas[7].Value = schedulingAgreement.IT_Term_Code;
            sqlParas[8].Value = schedulingAgreement.IT_Term_Text;
            sqlParas[9].Value = schedulingAgreement.Payment_Type;
            sqlParas[10].Value = schedulingAgreement.Days1;
            sqlParas[11].Value = schedulingAgreement.Discount_Rate1;
            sqlParas[12].Value = schedulingAgreement.Days2;
            sqlParas[13].Value = schedulingAgreement.Discount_Rate2;
            sqlParas[14].Value = schedulingAgreement.Days3;
            sqlParas[15].Value = schedulingAgreement.Target_Value;
            sqlParas[16].Value = schedulingAgreement.Currency_Type;
            sqlParas[17].Value = schedulingAgreement.Exchange_Rate;
            sqlParas[18].Value = schedulingAgreement.Scheduling_Agreement_ID;

            return DBHelper.ExecuteNonQuery(strBui.ToString(), sqlParas);
        }

        /// <summary>
        /// 删除指定的框架协议
        /// </summary>
        /// <param name="schedulingAgreementID"></param>
        /// <param name="schedulingAgreementVersion"></param>
        /// <returns></returns>
        public int deleteSchedulingAgreementByID(string schedulingAgreementID)
        {

            List<string> sqlList = new List<string>();
            List<SqlParameter[]> sqlParamsList = new List<SqlParameter[]>();

            # region 删除框架协议项

            StringBuilder strBui2 = new StringBuilder("delete from Scheduling_Agreement_Item ")
                .Append("where Scheduling_Agreement_ID = @Scheduling_Agreement_ID");

            SqlParameter[] sqlParas2 = new SqlParameter[]{
                new SqlParameter("@Scheduling_Agreement_ID", SqlDbType.VarChar)
            };

            sqlParas2[0].Value = schedulingAgreementID;

            sqlList.Add(strBui2.ToString());
            sqlParamsList.Add(sqlParas2);

            # endregion

            # region 删除框架协议抬头信息

            StringBuilder strBui = new StringBuilder("delete from Scheduling_Agreement ")
                .Append("where Scheduling_Agreement_ID = @Scheduling_Agreement_ID");

            SqlParameter[] sqlParas = new SqlParameter[]{
                new SqlParameter("@Scheduling_Agreement_ID", SqlDbType.VarChar)
            };

            sqlParas[0].Value = schedulingAgreementID;

            sqlList.Add(strBui.ToString());
            sqlParamsList.Add(sqlParas);

            # endregion

            return DBHelper.ExecuteNonQuery(sqlList, sqlParamsList);
        }
    }
}
