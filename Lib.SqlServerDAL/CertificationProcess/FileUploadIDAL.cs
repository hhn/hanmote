﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace Lib.SqlServerDAL.CertificationProcess
{
    public class FileUploadIDAL
    {
        /// <summary>
        /// 插入文件信息
        /// </summary>
        /// <param name="supplierId"></param>
        /// <param name="fileName"></param>
        /// <param name="fileType"></param>
        /// <param name="filePerson"></param>
        /// <param name="personType"></param>
        /// <returns></returns>
        public static int InsertFileInfo(String supplierId,String fileName,String fileType,String filePerson,String personType,String filePath) {
           string sql = @" insert into Hanmote_SupplierFileInfo(supplierId,File_Name, File_Type, CreatePerson, PersonType, CreateTime,filePath) 
                        values(@supplierId,@fileName, @fileType, @filePerson,@personType, GETDATE(),@filePath)";

            SqlParameter[] parameter = {
                new SqlParameter("@fileName",fileName),
                new SqlParameter("@fileType",fileType),
                new SqlParameter("@filePerson",filePerson),
                new SqlParameter("@personType",personType),
                new SqlParameter("@supplierId",supplierId),
                new SqlParameter("@filePath",filePath)
            };
            int a = 0;
            try
            {
                a = DBHelper.ExecuteNonQuery(sql, parameter);


            }
            catch(SqlException e)
            {
                a = 0;

            }

            return a;
        }
        /// <summary>
        /// 获取文件信息
        /// </summary>
        /// <param name="supplierID"></param>
        /// <returns></returns>

        public static DataTable getFileInfo(String supplierID) {

            DataTable dt = new DataTable();
            string sql = @"select File_Name as 文档名称,
                                   case File_Type 
								   when 'localEvalFile' then '现场评估' 
								   when 'supplier' then '供应商文件' 
								   when 'Leader' then '决策文件' 
								   when 'Evaluation' then '预评文件' 
                                    ELSE '其他'
								   end as 文档类型,CreatePerson as 上传者,
                                    PersonType as 角色,
                                    CreateTime as 创建时间,
                                    filePath as 文件路径 
                         from Hanmote_SupplierFileInfo where supplierId=@supplierID";
            SqlParameter parameter = new SqlParameter("@supplierID", supplierID);
            dt=DBHelper.ExecuteQueryDT(sql,parameter);
            return dt;


        }


        /// <summary>
        /// 搜索文件
        /// </summary>
        /// <param name="fileType"></param>
        /// <returns></returns>

        public DataTable searchFile(String fileType,String supplierId,string start,string end,string createPerson) {
           
            DataTable dt = new DataTable();
            switch (fileType) {
                case "现场评估文件":
                    fileType = "localEvalFile"; break;
                case "供应商文件":
                    fileType = "supplier"; break;
                case "决策文件":
                    fileType = "Leader"; break;
                case "预评文件":
                    fileType = "Evaluation"; break;
                case "全部文件":
                    fileType = "";break;
            }


            string sql = @"select File_Name as 文档名称,
                                   case File_Type 
								   when 'localEvalFile' then '现场评估' 
								   when 'supplier' then '供应商文件' 
								   when 'Leader' then '决策文件' 
								   when 'Evaluation' then '预评文件' 
								   end as 文档类型,CreatePerson 上传者,
                                    PersonType as 角色,
                                    CreateTime as 创建时间,
                                    filePath as 文件路径 
                            from Hanmote_SupplierFileInfo where supplierId='"+supplierId+"'";


            if (!fileType.Equals("")) 
                sql += " and File_Type='" + fileType + "'";
            if (!(start.Equals("") && end.Equals("")))
                sql += " and CreateTime>='"+start+"' and CreateTime<='"+end+"' ";
            if (!createPerson.Equals(""))
                sql += " and CreatePerson like '%" + createPerson + "%'";


            dt = DBHelper.ExecuteQueryDT(sql);
            return dt;
        }





    }
}
