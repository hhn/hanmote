﻿using Lib.IDAL.CertificationProcess.LocationalEvaluationIDAL;
using Lib.Model.CertificationProcess.LocationEvaluation;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace Lib.SqlServerDAL.CertificationProcess.LocationEvaluation
{
    class LocationEvalutionPersonDAL : LocationEvaluationPersionIDAL
    {
        /// <summary>
        /// 计算条数总数
        /// </summary>
        /// <returns></returns>
        public int calculateEvalutionPersonNumber()
        {
            DataTable userInforTable = calculateRecordNumber();
            if (userInforTable == null || (userInforTable.Rows.Count > 0 && userInforTable.Rows[0][0].Equals(DBNull.Value)))
            {
                return 0;
            }
            else
            {
                return userInforTable.Rows.Count;
            }
        }
        /// <summary>
        /// 统计总数
        /// </summary>
        /// <returns></returns>
        public DataTable calculateRecordNumber()
        {
            String sqlText = "select count(User_ID) from Hanmote_Base_User";
            return DBHelper.ExecuteQueryDT(sqlText);
        }
        /// <summary>
        /// 查找所有评审员信息
        /// </summary>
        /// <param name="pageNum"></param>
        /// <param name="frontNum"></param>
        /// <returns></returns>
        public DataTable findAllEvalutionPersonList(String MtGroupName,String companyID,int pageNum, int frontNum)
        {


            String sqlText = String.Format(@"SELECT
	                                            u.User_ID AS ID,
	                                            u.Username AS name,
	                                            u.Email AS email,
	                                            u.Mobile AS mobile,
	                                            u.Birth_Place AS address,
	                                            mt.Pur_Group_Name AS PurGroupName,
                                                e.Description as evalCodeName
                                            FROM
	                                            Hanmote_User_MtGroupName mt,
	                                            Hanmote_Base_User u,
	                                            SR_Info sr,
                                            HanmoteBaseUser_EvalCode e
         
                                            WHERE
	                                            Type = 0
                                            AND u.User_ID = mt.User_ID
                                            and e.UserID = mt.User_ID
                                            AND mt.Mt_Group_Name = sr.SelectedMType
                                            AND mt.Pur_Group_Name = sr.SelectedDep
                                            AND sr.SupplierId = '" + companyID+"'", pageNum, frontNum);
            return DBHelper.ExecuteQueryDT(sqlText);
        }

        public DataTable findAllEvalutionPseronListByCondition(String companyID,String name,String evalcode, int pageNum, int frontNum)
        {
            StringBuilder tmpText = new StringBuilder("");
            if (!String.IsNullOrEmpty(name))
            {
                tmpText.Append(" and u.Username like " + " '%" + name + "%' ");
            }
            if (!String.IsNullOrEmpty(evalcode))
            {
                tmpText.Append(" and  c.Name like " + " '%" + evalcode + "%' ");
            }
            String sqlText = String.Format(@"SELECT
	                                            u.User_ID AS ID,
	                                            u.Username AS name,
	                                            u.Email AS email,
	                                            u.Mobile AS mobile,
	                                            u.Birth_Place AS address,
	                                            mt.Pur_Group_Name AS PurGroupName,
                                                 e.Description as evalCodeName
                                            FROM
	                                            Hanmote_User_MtGroupName mt,
	                                            Hanmote_Base_User u,
	                                            SR_Info sr,
                                            HanmoteBaseUser_EvalCode e,
                                            LocalEvalClass_1 c
                                            WHERE
	                                            Type = 0
                                            and c.Id = e.codeID
                                            AND u.User_ID = mt.User_ID
                                            and e.UserID = mt.User_ID
                                            AND mt.Mt_Group_Name = sr.SelectedMType
                                            AND mt.Pur_Group_Name = sr.SelectedDep
                                            AND sr.SupplierId = '" + companyID + "'" + tmpText, pageNum, frontNum);

            return DBHelper.ExecuteQueryDT(sqlText);
        }
        /// <summary>
        /// 得到评审小组状态列表
        /// </summary>
        /// <param name="companyID"></param>
        /// <returns></returns>
        public DataTable getLocalEvalInfoStatBySupplierID(string companyID)
        {
            /* String sqlText = String.Format(@"SELECT
                                                 sr.SelectedDep AS PurGroupName,
                                                 code.name AS evalCodeName,
                                                 info.mobile,
                                                 info.email,
                                                 info.Username AS name,
                                                 info.User_ID AS ID,
                                                 CASE
                                             WHEN lol.localEvalstat = '0' THEN
                                                 '未完成'
                                             WHEN lol.localEvalstat = '1' THEN
                                                 '已完成'
                                             END AS localEvalstat
                                             FROM
                                                 Hanmote_Base_User info,
                                                 LocalEvalClass_1 code,
                                                 SR_Info sr,
                                                 HanmoteBaseUser_EvalCode lo,
                                                 LocalEval_Info lol
                                             WHERE
                                                 sr.TagRefuse=0  and   
                                                 lol.evaluateID = info.User_ID
                                             AND lo.UserID = info.User_ID
                                             AND lo.codeID = code.Id
                                             AND sr.SupplierId  = '" + companyID + "' and lol.SupplierID= '" + companyID + "'");*/
            String sqlText = String.Format(@"SELECT
	                                            sr.SelectedDep AS PurGroupName,
	                                            lo.Description AS evalCodeName,
	                                            info.mobile,
	                                            info.email,
	                                            info.Username AS name,
	                                            info.User_ID AS ID,
	                                            CASE
                                            WHEN lol.localEvalstat = '0' THEN
	                                            '未完成'
                                            WHEN lol.localEvalstat = '1' THEN
	                                            '已完成'
                                            END AS localEvalstat
                                            FROM
	                                            Hanmote_Base_User info,
	                                            SR_Info sr,
	                                            HanmoteBaseUser_EvalCode lo,
	                                            LocalEval_Info lol
                                            WHERE
                                                sr.TagRefuse=0  and   
	                                            lol.evaluateID = info.User_ID
                                            AND lo.UserID = info.User_ID
                                            AND sr.SupplierId  = '" + companyID + "' and lol.SupplierID= '" + companyID + "'");

            return DBHelper.ExecuteQueryDT(sqlText);
        }

        public DataTable getSelectedEvaluationPerson(string ids, string companyID, int pageNum, int frontNum)
        {
            /* String sqlText = String.Format(@"SELECT
                                                 u.User_ID AS ID,
                                                 u.Username AS name,
                                                 u.Email AS email,
                                                 u.Mobile AS mobile,
                                                 u.Birth_Place AS address,
                                                 mt.Pur_Group_Name AS PurGroupName,
                                                 c.Name as evalCodeName
                                             FROM
                                                 Hanmote_User_MtGroupName mt,
                                                 Hanmote_Base_User u,
                                                 SR_Info sr,
                                             HanmoteBaseUser_EvalCode e,
                                             LocalEvalClass_1 c
                                             WHERE
                                                 Type = 0
                                             AND u.User_ID = mt.User_ID
                                             and e.UserID = mt.User_ID
                                             and c.Id = e.codeID
                                             AND mt.Mt_Group_Name = sr.SelectedMType
                                             AND mt.Pur_Group_Name = sr.SelectedDep
                                             AND sr.SupplierId = '" + companyID + "' and u.User_ID = '" + ids + "' ", pageNum, frontNum);*/
            String sqlText = String.Format(@"SELECT
	                                            u.User_ID AS ID,
	                                            u.Username AS name,
	                                            u.Email AS email,
	                                            u.Mobile AS mobile,
	                                            u.Birth_Place AS address,
	                                            mt.Pur_Group_Name AS PurGroupName,
                                                e.Description as evalCodeName
                                            FROM
	                                            Hanmote_User_MtGroupName mt,
	                                            Hanmote_Base_User u,
	                                            SR_Info sr,
                                            HanmoteBaseUser_EvalCode e
                                            WHERE
	                                            Type = 0
                                            AND u.User_ID = mt.User_ID
                                            and e.UserID = mt.User_ID
                                            AND mt.Mt_Group_Name = sr.SelectedMType
                                            AND mt.Pur_Group_Name = sr.SelectedDep
                                            AND sr.SupplierId = '" + companyID + "' and u.User_ID = '" + ids + "' ", pageNum, frontNum);
            return DBHelper.ExecuteQueryDT(sqlText);
        }





        public DataTable getSelectedEvaluationPersonGroup(string companyID)
        {
            /*String sqlText = String.Format(@"SELECT
	                                        hub.User_ID as ID,
	                                        cl.Name AS evalCodeName,
	                                        hub.Username AS name,
	                                        hub.Email as email,
	                                        hub.Mobile as mobile,
	                                        hub.Birth_Place as address,
	                                        sr.SelectedDep AS PurGroupName
                                        FROM
	                                        Hanmote_Base_User hub,
	                                        LocalEvalClass_1 cl,
	                                        HanmoteBaseUser_EvalCode hue,
	                                        SR_Info sr
                                        WHERE
                                            sr.TagRefuse = 0 and 
	                                        hue.codeID = cl.Id
                                        AND hub.User_ID = hue.UserID
                                        AND sr.SupplierId ='" + companyID + "' and lol.supplierID = '" + companyID + "' ");*/


            String sqlText = String.Format(@"SELECT
	                                        hub.User_ID as ID,
	                                        hue.Description AS evalCodeName,
	                                        hub.Username AS name,
	                                        hub.Email as email,
	                                        hub.Mobile as mobile,
	                                        hub.Birth_Place as address,
	                                        sr.SelectedDep AS PurGroupName
                                        FROM
	                                        Hanmote_Base_User hub,
	                                        HanmoteBaseUser_EvalCode hue,
	                                        SR_Info sr
                                        WHERE
                                            sr.TagRefuse = 0 and 
                                        AND hub.User_ID = hue.UserID
                                        AND sr.SupplierId ='" + companyID + "' and lol.supplierID = '" + companyID + "' ");
            return DBHelper.ExecuteQueryDT(sqlText);
        }

        /// <summary>
        /// 插入信息到表LocalEval_Info
        /// </summary>
        /// <param name="companyID"></param>
        /// <param name="MtGroupName"></param>
        /// <param name="evaluateID"></param>
        public void insertLocaEvalInfo(string evalCode, string companyID, string MtGroupName, string evaluateID)
        {
            String sqlstr = "insert into LocalEval_Info (supplierID,mT_GroupID,evaluateID,localEvalstat ,localEvalItem ) values ('" + companyID+"','"+MtGroupName+"','"+evaluateID+"',0 , '"+evalCode+"')";
            DBHelper.ExecuteNonQuery(sqlstr);
        }

        public void delEvalInfoByCompanyIDAndID(string companyID, object iD)
        {
            String sqlstr= "delete from LocalEval_Info where supplierID = '" + companyID + "'" + "  and  evaluateID ='" + iD + "'";
            DBHelper.ExecuteNonQuery(sqlstr);
        }
        /// <summary>
        /// 查看评审结果状态
        /// </summary>
        /// <param name="companyID"></param>
        /// <returns></returns>
        public int getEndResultStatusBYCompanyID(string companyID)
        {
            int status = 0;
            String sqlstr = "select localEvalstat  from LocalEval_Info where supplierID = '" + companyID + "'";
            status =  DBHelper.ExecuteNonQuery(sqlstr);
            return status;
        }
        /// <summary>
        /// getunfinishedEvalPersonInfo
        /// </summary>
        /// <param name="companyID"></param>
        /// <returns></returns>
        public DataTable getunfinishedEvalPersonInfo(String companyID)
        {
            String sqlText = @"SELECT
                                    info.User_ID AS ID,
	                                info.Username AS name,
	                                info.Email
                                FROM

                                    Hanmote_Base_User info,
                                    LocalEval_Info lo
                                WHERE
                                    lo.evaluateID = info.User_ID
                                AND lo.localEvalstat = 0
                                AND lo.supplierID = '" + companyID + "'";
            return DBHelper.ExecuteQueryDT(sqlText);
        }
        /// <summary>
        /// 根据ID得到评审员邮箱
        /// </summary>
        /// <param name="evalID"></param>
        /// <returns></returns>
        public String getEvaluatorMailByEvalID(String evalID)
        {
            String sqlstr = "select email from Hanmote_Base_User where User_ID = '" + evalID + "'";
            return DBHelper.ExecuteQueryDT(sqlstr).Rows[0][0].ToString();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyID"></param>
        /// <returns></returns>
        public DataTable getCompangInfoByCompanyID(string companyID)
        {
            String sqlstr = "SELECT DbsCode,  CompanyName, ContactName, ContactPosition, PhoneNumber, CompanyFox, ContactMail, CompanyWebsite, Address, PostalCode FROM  SR_Info where SupplierId= '"+companyID+"'";
            return DBHelper.ExecuteQueryDT(sqlstr);
        }
        public DataTable getSumEvaluateScoreByCompanyID(string user_ID, string companyID)
        {
            string sql = "select sum(localEvalScore) as score ,sum(localEvalSum) as scoresum  from LocalEval_Info WHERE supplierID='" + companyID + "' and evaluateID !='" + user_ID + "'";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            return dt;
        }
        public DataTable getDetailEvaluateScoreByCompanyID(string User_ID ,String companyID)
        {
            String sqlstr = @"SELECT
	                            linfo.evaluateID AS ID,
	                            info.Username AS name,
	                            linfo.localEvalScore AS Eval_score,
	                            linfo.localEvalSum AS EvalSumScore,
	                            linfo.localEvalItem AS Eval_Item,
                                linfo.DenyItems AS DenyItems,
	                            linfo.localEvalReasonInfo AS Eval_ReasonInfo
                            FROM
	                            LocalEval_Info linfo,
	                            Hanmote_Base_User info
                            WHERE
	                            linfo.evaluateID = info.User_ID
                            AND linfo.localEvalstat = 1
                            And linfo.evaluateID !=  '" + User_ID + @"' 
                            AND linfo.supplierID =  '" + companyID + "'";
            return DBHelper.ExecuteQueryDT(sqlstr);
        }
        /// <summary>
        /// 插入主审意见 并计算总分
        /// </summary>
        /// <param name="SSEMFilePath"></param>
        /// <param name="MainEvalAdvice"></param>
        /// <param name="companyID"></param>
        public void saveMainAdvicesOrSSEMFile(string userID,String SSEMFilePath,String MainEvalAdvice, String companyID,string mtGroupName)
        {
            //一供应商ID 查询各评分并计算总分
            string sql = "select sum(localEvalScore) as score ,sum(localEvalSum) as scoresum  from LocalEval_Info WHERE supplierID='" + companyID + "' and evaluateID !='"+ userID + "'";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            if(dt.Rows.Count>0)
            {
                String sqlstr = @"INSERT INTO LocalEval_Info (
	                                [supplierID],
	                                [mT_GroupID],
	                                [evaluateID],
	                                [localEvalScore],
	                                [localEvalstat],
	                                [localEvalSum],
	                                [SSEMFilePath],
	                                [MainEvalAdvice]
                                )
                                VALUES
	                                ('"+companyID+"','"+ mtGroupName + "','"+userID+"','"+dt.Rows[0][0].ToString()+"','1' ,'"+dt.Rows[0][1].ToString()+"','"+ SSEMFilePath + "','"+ MainEvalAdvice + "')";
                DBHelper.ExecuteNonQuery(sqlstr);
            }
        }
        /// <summary>
        /// 有没有建立过评审小组
        /// </summary>
        /// <param name="comapnyID"></param>
        /// <returns></returns>
        public DataTable findAllLocalEvalInfoByCompanyId(String comapnyID)
        {
            String sqlstr = "select *  from LocalEval_Info where supplierID = '"+comapnyID+"'";
            return DBHelper.ExecuteQueryDT(sqlstr);
        }
        /// <summary>
        /// 得到高层邮箱
        /// </summary>
        /// <returns></returns>
        public String getSenorLeaderMail(string userId)
        {
            String sqlstr = "select Email from Hanmote_Base_User where User_ID = '" + userId+"' ";
            return DBHelper.ExecuteQueryDT(sqlstr).Rows[0][0].ToString();
        }
        /// <summary>
        /// 删除评审小组
        /// </summary>
        /// <param name="companyID"></param>
        public void deleteEvalGroupByCompanyID(String companyID)
        {
            String sqlstr = "delete from LocalEval_Info  where  supplierID='" + companyID + "'";
            DBHelper.ExecuteNonQuery(sqlstr);
        }

        public DataTable singlePersonEvaluationList(string userId, int pageSize, int pageIndex,out int totalSize)
        {
            
            String sqlstr = @"SELECT
                                  case
	                              when info.localEvalstat='1' then '已完成'
                                  when info.localEvalstat='0' then '未完成'
                                  END as status,
                                  info.supplierID ,
                                  sr.CompanyName,
                                  sr.Country,
                                  sr.ContactName,
                                  sr.PhoneNumber,
                                  sr.ContactMail,
                                  sr.SelectedMType as Mt_GroupName,
                                  sr.SelectedDep  as Pur_GroupName,
                                  info.evaluateID
                                FROM
	                                LocalEval_Info info,
	                                SR_Info sr
                                WHERE
	                                info.supplierID = sr.SupplierId and info.evaluateID =@userId";

            string sql = @"EXECUTE Proc_SR_InfoMainLocation @pageSize, @pageIndex, @userId,null";
            SqlParameter[] parameter = {
                new SqlParameter("@userId",userId),
                new SqlParameter("@pageSize",pageSize),
                new SqlParameter("@pageIndex",pageIndex)

            };
            DataTable dt = DBHelper.ExecuteQueryDT(sql, parameter);
            
            if (dt.Rows.Count == 0)
            {
                totalSize = 0;
            }
            else
            {
                totalSize = (int)dt.Rows[0]["pageCount"];
            }
            dt.Columns.Remove("pageCount");
            return dt;

        }
        /// <summary>
        /// 插入现场评估信息
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="state"></param>
        /// <param name="score"></param>
        /// <param name="scoreSum"></param>
        public void insertLocalEvalInfoBySingelPerson(string companyID, string UserID, string filePath, string state, string score, string scoreSum,string DenyItrems)
        {
            String sqlstr = @" UPDATE LocalEval_Info SET localEvalScore = '" + score + "', localEvalReasonInfo = '" + state + "', localEvalstat = 1, localEvalSum = '" + scoreSum + "',DenyItems='"+DenyItrems+"', filePath = '" + filePath + "' WHERE supplierID = '" + companyID + "' AND evaluateID = '" + UserID + "'";

            DBHelper.ExecuteNonQuery(sqlstr);
        }
        /// <summary>
        /// 评审员重置信息
        /// </summary>
        /// <param name="companyID"></param>
        /// <param name="v"></param>
        public void resetInfoEvaluationBySinglePerson(string companyID, string v)
        {
            String sqlstr = @" UPDATE LocalEval_Info SET localEvalScore = '', localEvalReasonInfo = '', localEvalstat = 0, localEvalSum = '', filePath = '' WHERE supplierID = '" + companyID + "' AND evaluateID = '" + v + "'";

            DBHelper.ExecuteNonQuery(sqlstr);
        }
        public DataTable getSinglePersonEvalStayusByCompanyAndUserID(string companyID, string v)
        {
            String sqlstr = @"select localEvalstat from  LocalEval_Info WHERE supplierID = '" + companyID + "' AND evaluateID = '" + v + "'";
           // dweagerr
            return  DBHelper.ExecuteQueryDT(sqlstr);
        }
        public DataTable detailLocalInfoByCompanyIDAndEvaluateID(string companyID, string evaluateID)
        {
            String sqlstr = @" select localEvalScore,localEvalReasonInfo,localEvalSum,filePath from  LocalEval_Info WHERE supplierID = '" + companyID + "' AND evaluateID = '" + evaluateID + "'";
            // dweagerr
            return DBHelper.ExecuteQueryDT(sqlstr);
        }
        /// <summary>
        /// 主审重置评审员信息
        /// </summary>
        /// <param name="evaluateID"></param>
        public void backSinglePersonEvalInfo(string UserID,string evaluateID,string companyID)
        {
            String sqlstr = @" UPDATE LocalEval_Info SET localEvalScore = '', localEvalReasonInfo = '', localEvalstat = 0, localEvalSum = '', filePath = '' WHERE supplierID = '" + companyID + "' AND evaluateID = '" + evaluateID + "'";
            DBHelper.ExecuteNonQuery(sqlstr);
            string sqldel = @"delete from LocalEval_Info WHERE supplierID = '" + companyID + "' AND evaluateID = '" + UserID + "'";
            DBHelper.ExecuteNonQuery(sqldel);
        }
        public string getEvalInfoEmaiml(string evaluateID)
        {
            String sqlstr = "select email from Hanmote_Base_User where User_ID = '" + evaluateID+"'";
            return DBHelper.ExecuteQueryDT(sqlstr).Rows[0][0].ToString();
        }

        public void insertSsupplier_MainAccountInfo(string companyID, string company_Name, string contactMan, string acount, string password)
        {
            string sql1 = "select ContactMail from SR_Info where SupplierId='" + companyID + "'";
            string sql2 = "select PhoneNumber from SR_Info where SupplierId='" + companyID + "'";
            string sql3 = "select ContactName from SR_Info where SupplierId='" + companyID + "'";
            string ContactMail = DBHelper.ExecuteQueryDT(sql1).Rows[0][0].ToString();
            string PhoneNumber = DBHelper.ExecuteQueryDT(sql2).Rows[0][0].ToString();
            string ContactName = DBHelper.ExecuteQueryDT(sql3).Rows[0][0].ToString();
            String sqlstr = @"insert into Supplier_MainAccount (
                                                                [Supplier_Id], 
                                                                [Password], 
                                                                [email], 
                                                                [mobile], 
                                                                [Enable], 
                                                                [Supplier_Name], 
                                                                [ManagerName], 
                                                                [LoginAccount]) 
                                                                values ('"
                                                                + companyID +
                                                                 "','"
                                                                + password +
                                                                "','"
                                                                + ContactMail +
                                                                "','"
                                                                + PhoneNumber +
                                                                 "','"
                                                                + 0 +
                                                                 "','"
                                                                + company_Name +
                                                                "','"
                                                                + contactMan +
                                                                "','"
                                                                + acount +
                                                                "')";
            DBHelper.ExecuteNonQuery(sqlstr);

  
        }
        public void setUpLvelBycompanyID(string companyID)
        {
            string sql = @"SELECT
	                            CASE
                            WHEN (
	                             SUM (localEvalScore)/SUM (localEvalSum)
                            ) >= 0.9 THEN
	                            'A'
                            WHEN (
	                             SUM (localEvalScore)/SUM (localEvalSum)
                            ) < 0.9
                            AND (
	                            SUM (localEvalScore)/SUM (localEvalSum)
                            ) >= 0.8 THEN
	                            'B'
                            WHEN (
	                             SUM (localEvalScore)/SUM (localEvalSum)
                            ) < 0.8
                            AND (
	                             SUM (localEvalScore)/SUM (localEvalSum)
                            ) >= 0.7 THEN
	                            'c'
                            WHEN (
	                             SUM (localEvalScore)/SUM (localEvalSum)
                            ) < 0.7
                            AND (
	                             SUM (localEvalScore)/SUM (localEvalSum)
                            ) >= 0.6 THEN
	                            'D'
                            WHEN (
	                            SUM (localEvalSum) / SUM (localEvalScore)
                            ) < 0.6 THEN
	                            'E'
                            END AS status
                            FROM
	                            LocalEval_Info
                            WHERE
	                            supplierID = '" + companyID+"'";
            string lass = DBHelper.ExecuteQueryDT(sql).Rows[0][0].ToString();

            string sqlstr = "update SR_Info set SupLevel ='"+lass+ "',TagPermission=1  where  SupplierId = '" + companyID+"'";
            DBHelper.ExecuteNonQuery(sqlstr);
        }
        public void insertTaskSpecification(string UserID, string state)
        {
            string sqlstr = "insert into Hanmote_TaskSpecification(UserID,TaskSpecification,isFinished,DateTime) values('"+UserID+"','"+state+"','0',getdate())";
            DBHelper.ExecuteNonQuery(sqlstr);
        }
       

        public string getMtGroupNameByCompanyId(string companyID)
        {
            string sql = "select  SelectedMType  from SR_Info where SupplierId='"+companyID+"' ";
            return DBHelper.ExecuteQueryDT(sql).Rows[0][0].ToString();
        }

        public string getMainEvalIDByMtGroupName(string mtName,string mtPor)
        {
            string sql = "select User_ID from Hanmote_User_MtGroupName where Mt_Group_Name='"+mtName+ "' and Pur_Group_Name='"+mtPor+"'";
            return DBHelper.ExecuteQueryDT(sql).Rows[0][0].ToString();
        }
        public void updateTaskSpecification(string user_ID, string companyName)
        {
            string sqlstr = "update Hanmote_TaskSpecification set isFinished = '1', DateTime=getdate()  where UserID='" + user_ID + "' and TaskSpecification like '%" + companyName + "%'";
            DBHelper.ExecuteNonQuery(sqlstr);
        }
        public void setIsFinishedByUserID(string UserID, string companyName)
        {
            string sqlstr = "update Hanmote_TaskSpecification set isFinished = '0', DateTime=getdate()  where UserID='" + UserID + "' and TaskSpecification like '%" + companyName + "待现场评审%'";
            string delsql = "delete from Hanmote_TaskSpecification where TaskSpecification like '%" + companyName + "现场评审已完结待提交上级审批%'";
            DBHelper.ExecuteNonQuery(sqlstr);
            DBHelper.ExecuteNonQuery(delsql);

        }
        public DataTable findAllTaskByUserID(string user_ID)
        {
            string sql = "select TaskSpecification from Hanmote_TaskSpecification where UserID='"+user_ID+ "' and isFinished ='0' order by DateTime asc ";
            return DBHelper.ExecuteQueryDT(sql);
        }
        public void deleteTaskSpecification(string userID,string companyName)
        {
            string sqlstr1 = "update Hanmote_TaskSpecification set isFinished = '0', DateTime=getdate()  where UserID='" + userID + "' and TaskSpecification like '%" + companyName + "预评审已完结待提交现场评审%'";
            DBHelper.ExecuteNonQuery(sqlstr1);
            string sqlstr = "delete from Hanmote_TaskSpecification where TaskSpecification like '%"+companyName+ "待现场评审%'";
            DBHelper.ExecuteNonQuery(sqlstr);
        }

        public string getMtPorNameByCompanyId(string companyID)
        {
            string sql = "select  SelectedDep  from SR_Info where SupplierId='" + companyID + "' ";
            return DBHelper.ExecuteQueryDT(sql).Rows[0][0].ToString();
        }

        public void deleteTaskSpecificationSportToUpper(string mainEvalID, string v)
        {
            //现场评审已完结待提交上级审批
            //现场评审已完结待审批
            //待审批
            string sqlstr = "delete from Hanmote_TaskSpecification where TaskSpecification like '%" + v + "现场评审已完结待提交上级审批%'";
            DBHelper.ExecuteNonQuery(sqlstr);
            string sqlstr2 = "delete from Hanmote_TaskSpecification where TaskSpecification like '%" + v + "现场评审已完结待审批%'";
            DBHelper.ExecuteNonQuery(sqlstr2);
            string sqlstr3= "delete from Hanmote_TaskSpecification where TaskSpecification like '%" + v + "待审批%'";
            DBHelper.ExecuteNonQuery(sqlstr3);
        }

        public void setCompanyIDTagThirdZeroBycompanyID(string companyID)
        {
            string sqlstr = "update SR_Info set TAgThird = 0 where SupplierId='"+companyID+"'";
            DBHelper.ExecuteNonQuery(sqlstr);
        }

        public int insertSupplierBase(String supplierId)
        {
            String sql = @"select* from SR_Info where SupplierId='"+ supplierId + "'";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            SqlParameter[] sqlparameter = new SqlParameter[]
            {
                new SqlParameter("@supplierId",supplierId),
                new SqlParameter("@companyName",dt.Rows[0]["CompanyName"].ToString()),
                new SqlParameter("@country",dt.Rows[0]["Country"].ToString()),
                new SqlParameter("@address",dt.Rows[0]["Address"].ToString()),
             
                new SqlParameter("@city",dt.Rows[0]["City"].ToString()),
                new SqlParameter("@postalCode",dt.Rows[0]["PostalCode"].ToString()),
                new SqlParameter("@companyFox",dt.Rows[0]["CompanyFox"].ToString()),
                new SqlParameter("@phoneNumber",dt.Rows[0]["PhoneNumber"].ToString()),
                new SqlParameter("@contactMail",dt.Rows[0]["ContactMail"].ToString())
            };
            sql = @"insert into Supplier_Base(Supplier_ID,Supplier_Name,Nation,Address,City,Zip_Code,Fax,Mobile_Phone1,Email)  
		    values(@supplierId,@companyName,@country,@address,@city,@postalCode,@companyFox,@phoneNumber,@contactMail)";
            return  DBHelper.ExecuteNonQuery(sql,sqlparameter);
        
        }
    }
}
