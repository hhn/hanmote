﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Lib.SqlIDAL;
using System.Data.SqlClient;
using Lib.Common.MMCException.IDAL;

namespace Lib.SqlServerDAL
{
    public class BuyerOrganizationDAL:BuyerOrganizationIDAL
    {
        public DataTable GetAllBuyerOrganizationId()
        {
            try
            {
                string sql = "SELECT Buyer_Org FROM Buyer_Org";
                return DBHelper.ExecuteQueryDT(sql);
            }
            catch (Exception ex)
            {
                throw new DBException("查询采购组织编码列表数据数据库异常！",ex);
            }

        }

        public DataTable GetAllBuyerOrganizationName()
        {
            try
            {
                string sql = "select Buyer_Org_Name from Buyer_Org";
                return DBHelper.ExecuteQueryDT(sql);
            }
            catch (Exception ex)
            {
                throw new DBException("查询采购组织名称列表数据库异常！",ex);
            }
            
        }
    }
}
