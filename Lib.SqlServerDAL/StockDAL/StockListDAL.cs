﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Lib.IDAL.StockIDAL;

namespace Lib.SqlServerDAL.StockDAL
{
    public class StockListDAL:StockListIDAL
    {
        /// <summary>
        /// 查询满足条件的物料
        /// </summary>
        /// <param name="dt0"></param>
        /// <returns></returns>
        public DataTable stocklist(DataTable dt0)
        {
            int num = 0;
            int j = 0;

            if (!dt0.Rows[0][0].ToString().Equals(""))
                num++;

            if (!dt0.Rows[0][1].ToString().Equals(""))
                num++;

            if (!dt0.Rows[0][2].ToString().Equals(""))
                num++;

            if (!dt0.Rows[0][3].ToString().Equals(""))
                num++;

            if (!dt0.Rows[0][4].ToString().Equals(""))
                num++;

            if (!dt0.Rows[0][5].ToString().Equals(""))
                num++;


            StringBuilder strbld = new StringBuilder("select Factory_ID,Stock_ID,")
            .Append("Material_ID,Inventory_State,SKU,sum(Count) from Inventory where");

            SqlParameter[] sqlpara = new SqlParameter[num];

            if (!dt0.Rows[0][0].ToString().Equals(""))
            {
                strbld.Append(" Material_ID = @mid ");
                sqlpara[j] = new SqlParameter("@mid", SqlDbType.VarChar);
                sqlpara[j].Value = dt0.Rows[0][0].ToString();
                j++;
            }

            if (!dt0.Rows[0][0].ToString().Equals("")&&dt0.Rows[0][1].ToString() != "")
            {
                strbld.Append(" and Factory_ID=@fid ");
                sqlpara[j] = new SqlParameter("@fid", SqlDbType.VarChar);
                sqlpara[j].Value = dt0.Rows[0][1].ToString();
                j++;
            }
            if (dt0.Rows[0][0].ToString().Equals("") && dt0.Rows[0][1].ToString() != "")
            {
                strbld.Append(" Factory_ID=@fid ");
                sqlpara[j] = new SqlParameter("@fid", SqlDbType.VarChar);
                sqlpara[j].Value = dt0.Rows[0][1].ToString();
                j++;
            }

            if ((!dt0.Rows[0][0].ToString().Equals("") || !dt0.Rows[0][1].ToString().Equals("")) && dt0.Rows[0][2].ToString() != "")
            {
                strbld.Append(" and Stock_ID=@sid ");
                sqlpara[j] = new SqlParameter("@sid", SqlDbType.VarChar);
                sqlpara[j].Value = dt0.Rows[0][2].ToString();
                j++;
            }
            if ((dt0.Rows[0][0].ToString().Equals("") &&dt0.Rows[0][1].ToString().Equals("")) && dt0.Rows[0][2].ToString() != "")
            {
                strbld.Append(" Stock_ID=@sid ");
                sqlpara[j] = new SqlParameter("@sid", SqlDbType.VarChar);
                sqlpara[j].Value = dt0.Rows[0][2].ToString();
                j++;
            }

            if ((!dt0.Rows[0][0].ToString().Equals("") || !dt0.Rows[0][1].ToString().Equals("") || !dt0.Rows[0][2].ToString().Equals("")) && dt0.Rows[0][3].ToString() != "")
            {
                strbld.Append(" and Batch_ID=@bid ");
                sqlpara[j] = new SqlParameter("@bid", SqlDbType.VarChar);
                sqlpara[j].Value = dt0.Rows[0][3].ToString();
                j++;
            }
            if ((dt0.Rows[0][0].ToString().Equals("") && dt0.Rows[0][1].ToString().Equals("") &&dt0.Rows[0][2].ToString().Equals("")) && dt0.Rows[0][3].ToString() != "")
            {
                strbld.Append(" Batch_ID=@bid ");
                sqlpara[j] = new SqlParameter("@bid", SqlDbType.VarChar);
                sqlpara[j].Value = dt0.Rows[0][3].ToString();
                j++;
            }


            if ((!dt0.Rows[0][0].ToString().Equals("") || !dt0.Rows[0][1].ToString().Equals("") || !dt0.Rows[0][2].ToString().Equals("") || !dt0.Rows[0][3].ToString().Equals("")) && dt0.Rows[0][4].ToString() != "")
            {
                strbld.Append(" and Evaluate_Type=@et ");
                sqlpara[j] = new SqlParameter("@et", SqlDbType.VarChar);
                sqlpara[j].Value = dt0.Rows[0][4].ToString();
                j++;
            }
            if ((dt0.Rows[0][0].ToString().Equals("") && dt0.Rows[0][1].ToString().Equals("") && dt0.Rows[0][2].ToString().Equals("") && dt0.Rows[0][3].ToString().Equals("")) && dt0.Rows[0][4].ToString() != "")
            {
                strbld.Append(" Evaluate_Type=@et ");
                sqlpara[j] = new SqlParameter("@et", SqlDbType.VarChar);
                sqlpara[j].Value = dt0.Rows[0][4].ToString();
                j++;
            }

            if ((!dt0.Rows[0][0].ToString().Equals("") || !dt0.Rows[0][1].ToString().Equals("") || !dt0.Rows[0][2].ToString().Equals("") || !dt0.Rows[0][3].ToString().Equals("") || !dt0.Rows[0][4].ToString().Equals("")) && dt0.Rows[0][5].ToString() != "")
            {
                strbld.Append(" and Material_Group=@mg ");
                sqlpara[j] = new SqlParameter("@mg", SqlDbType.VarChar);
                sqlpara[j].Value = dt0.Rows[0][5].ToString();
                j++;
            }
            if ((dt0.Rows[0][0].ToString().Equals("") && dt0.Rows[0][1].ToString().Equals("") && dt0.Rows[0][2].ToString().Equals("") && dt0.Rows[0][3].ToString().Equals("") && dt0.Rows[0][4].ToString().Equals("")) && dt0.Rows[0][5].ToString() != "")
            {
                strbld.Append(" Material_Group=@mg ");
                sqlpara[j] = new SqlParameter("@mg", SqlDbType.VarChar);
                sqlpara[j].Value = dt0.Rows[0][5].ToString();
                j++;
            }

            if (Convert.ToBoolean(dt0.Rows[0][6].ToString()) == true)
            {
                strbld.Append(" and (Inventory_State='非限制库存' ");
            }

            if (Convert.ToBoolean(dt0.Rows[0][6].ToString()) == false && Convert.ToBoolean(dt0.Rows[0][7].ToString()) == true)
            {
                strbld.Append(" and (Inventory_State='质量检查' ");
            }
            if (Convert.ToBoolean(dt0.Rows[0][6].ToString()) == false && Convert.ToBoolean(dt0.Rows[0][7].ToString()) == false && Convert.ToBoolean(dt0.Rows[0][8].ToString()) == true)
            {
                strbld.Append(" and (Inventory_State='冻结库存' ");
            }
            if (Convert.ToBoolean(dt0.Rows[0][6].ToString()) == true && Convert.ToBoolean(dt0.Rows[0][7].ToString()) == true)
            {
                strbld.Append(" or Inventory_State='质量检查' ");
            }

            if (Convert.ToBoolean(dt0.Rows[0][6].ToString()) == true || Convert.ToBoolean(dt0.Rows[0][7].ToString()) == true && Convert.ToBoolean(dt0.Rows[0][8].ToString()) == true)
            {
                strbld.Append(" or Inventory_State='冻结库存' ");
            }


            if (Convert.ToBoolean(dt0.Rows[0][6].ToString()) == true || Convert.ToBoolean(dt0.Rows[0][7].ToString()) == true|| Convert.ToBoolean(dt0.Rows[0][8].ToString()) == true)
            {
                strbld.Append(") ");
            }

            strbld.Append(" and  Inventory_State !='在途库存'");

            
            if (Convert.ToBoolean(dt0.Rows[0][10].ToString()) == true)
            {
                strbld.Append(" and Count<0 ");
            }
            if (Convert.ToBoolean(dt0.Rows[0][9].ToString()) == true)
            {
                strbld.Append(" and Batch_ID!='' ");
            }

            if (Convert.ToBoolean(dt0.Rows[0][12].ToString()) == true)
            {
                strbld.Append(" or Count<>0");
            }

            strbld.Append(" group by Factory_ID,Stock_ID,Material_ID,Inventory_State,SKU");

            DataTable info = DBHelper.ExecuteQueryDT(strbld.ToString(), sqlpara);
            return info;
        }

        /// <summary>
        ///  选择显示格式
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        public DataTable changestyle(DataTable info,bool bl)
        {            
           
            //显示值
            if (bl == false)
            {
                DataTable dt = new DataTable("dt");
                dt.Columns.Add("物料编号", typeof(string));
                dt.Columns.Add("工厂编号", typeof(string));
                dt.Columns.Add("库存地", typeof(string));
                dt.Columns.Add("冻结库存", typeof(double));
                dt.Columns.Add("质检库存", typeof(double));
                dt.Columns.Add("非限制库存", typeof(double));
                dt.Columns.Add("单位", typeof(string));
                int n = info.Rows.Count;
                
                if (n > 0)
                {
                    DataRow dr = dt.NewRow();
                    dr["物料编号"] = info.Rows[0][2].ToString();
                    dr["工厂编号"] = info.Rows[0][0].ToString();
                    dr["库存地"] = info.Rows[0][1].ToString();
                    if (info.Rows[0][3].ToString() == "冻结库存")
                    {
                        dr["冻结库存"] = Convert.ToDouble(info.Rows[0][5].ToString());
                    }
                    if (info.Rows[0][3].ToString() == "质量检查")
                    {
                        dr["质检库存"] = Convert.ToDouble(info.Rows[0][5].ToString());
                    }
                    if (info.Rows[0][3].ToString() == "非限制库存")
                    {
                        dr["非限制库存"] = Convert.ToDouble(info.Rows[0][5].ToString());
                    }
                    dr["单位"] = info.Rows[0][4].ToString();
                    dt.Rows.Add(dr);
                }
                int dtn = dt.Rows.Count;
                for (int i = 1; i < n; i++)
                {
                    for (int k = 0; k < dtn; k++)
                    {
                        if (dt.Rows[k][0].ToString() == info.Rows[i][0].ToString() && dt.Rows[k][1].ToString() == info.Rows[i][1].ToString() && dt.Rows[k][2].ToString() == info.Rows[i][2].ToString())
                        {
                            DataRow currentrow = dt.Rows[k];
                            currentrow.BeginEdit();

                            if (info.Rows[i][3].ToString() == "冻结库存")
                            {
                                currentrow["冻结库存"] = Convert.ToDouble(info.Rows[i][5].ToString());
                            }
                            if (info.Rows[i][3].ToString() == "质量检查")
                            {
                                currentrow["质检库存"] = Convert.ToDouble(info.Rows[i][5].ToString());
                            }
                            if (info.Rows[i][3].ToString() == "非限制库存")
                            {
                                currentrow["非限制库存"] = Convert.ToDouble(info.Rows[i][5].ToString());
                            }
                            currentrow.EndEdit();
                            break;
                        }  
                    }

                    DataRow dr = dt.NewRow();
                    dr["物料编号"] = info.Rows[i][2].ToString();
                    dr["工厂编号"] = info.Rows[i][0].ToString();
                    dr["库存地"] = info.Rows[i][1].ToString();
                    if (info.Rows[i][3].ToString() == "冻结库存")
                    {
                        dr["冻结库存"] = Convert.ToDouble(info.Rows[i][5].ToString());
                    }
                    if (info.Rows[i][3].ToString() == "质量检查")
                    {
                        dr["质检库存"] = Convert.ToDouble(info.Rows[i][5].ToString());
                    }
                    if (info.Rows[i][3].ToString() == "非限制库存")
                    {
                        dr["非限制库存"] = Convert.ToDouble(info.Rows[i][5].ToString());
                    }
                    dr["单位"] = info.Rows[i][4].ToString();
                    dt.Rows.Add(dr);
                    dtn = dt.Rows.Count;

                }

                for (int k = 0; k < dt.Rows.Count; k++)
                {
                    DataRow currentrow = dt.Rows[k];
                    currentrow.BeginEdit();

                    if (dt.Rows[k][3].ToString().Equals(""))
                    {
                        currentrow["冻结库存"] = 0;
                    }
                    if (dt.Rows[k][4].ToString().Equals(""))
                    {
                        currentrow["质检库存"] = 0;
                    }
                    if (dt.Rows[k][5].ToString().Equals(""))
                    {
                        currentrow["非限制库存"] = 0;
                    }
                    currentrow.EndEdit();
                }
                return dt;

            }

                //不显示值
            else
            {
                DataTable dt1 = new DataTable("dt1");
                int n = info.Rows.Count;
                dt1.Columns.Add("物料编号", typeof(string));
                dt1.Columns.Add("工厂编号", typeof(string));
                dt1.Columns.Add("库存地", typeof(string));
                dt1.Columns.Add("库存状态", typeof(string));
                dt1.Columns.Add("单位", typeof(string));
                for (int i = 0; i < n; i++)
                {
                    DataRow dr = dt1.NewRow();
                    dr["物料编号"] = info.Rows[i][2].ToString();
                    dr["工厂编号"] = info.Rows[i][0].ToString();
                    dr["库存地"] = info.Rows[i][1].ToString();
                    dr["库存状态"] = info.Rows[i][3].ToString();
                    dr["单位"] = info.Rows[i][4].ToString();
                    dt1.Rows.Add(dr);
                }
                return dt1;
            }
            

           
           
        }
    }

}
