﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.IDAL.StockIDAL;
using Lib.Model.StockModel;
using System.Data;
using System.Data.SqlClient;

namespace Lib.SqlServerDAL
{
    public class InventoryInfoDAL : InventoryInfoIDAL
    {
        /// <summary>
        /// 查找库存信息
        /// </summary>
        /// <returns></returns>
        public List<InventoryExtendModel> findInventoryInfo(string materialIdOrName, string factoryIdOrName, string inventoryState, string stock)
        {
            List<InventoryExtendModel> inventoryExtendModelList = new List<InventoryExtendModel>();
            StringBuilder sqlText = new StringBuilder("SELECT a.Material_ID,b.Material_Name,a.Factory_ID,a.Stock_ID,a.SKU,a.[Count],a.Inventory_State,a.Batch_ID ");
            sqlText.Append(" FROM Inventory a JOIN Material b ON a.Material_ID=b.Material_ID JOIN Factory c ON a.Factory_ID=c.Factory_ID JOIN Stock d ON a.Stock_ID=d.Stock_ID WHERE 1=1  ");
            //StringBuilder sqlTextJ = new StringBuilder("");
            if (materialIdOrName != null)
            {
                sqlText.Append("AND (a.Material_ID='");
                sqlText.Append(materialIdOrName);
                sqlText.Append("' OR b.Material_Name='");
                sqlText.Append(materialIdOrName);
                sqlText.Append("') ");
            }
            if (factoryIdOrName != null)
            {
                //sqlText.Append(" AND (a.Factory_ID=@factoryId OR c.Factory_Name=factoryName)");
                sqlText.Append("AND (a.Factory_ID='");
                sqlText.Append(factoryIdOrName);
                sqlText.Append("' OR c.Factory_Name='");
                sqlText.Append(factoryIdOrName);
                sqlText.Append("') ");
            }
            if (stock != null)
            {
                //sqlText.Append(" AND (a.Stock_ID=@stockId OR c.Stock_Name=stockName)");
                sqlText.Append("AND (a.Stock_ID='");
                sqlText.Append(stock);
                sqlText.Append("' OR d.Stock_Name='");
                sqlText.Append(stock);
                sqlText.Append("') ");
            }
           
            if (inventoryState != null)
            {
                //sqlText.Append(" AND a.Inventory_State=@inventoryState");
                sqlText.Append("AND a.Inventory_State='");
                sqlText.Append(inventoryState);
                sqlText.Append("' ");
            }
            /*
            string sqlTextT = sqlTextJ.ToString();
            if ("AND".Equals(sqlTextT.TrimStart().Substring(0, 3)))
            {
                sqlTextT = sqlTextT.Substring(3);
            }
            sqlText.Append(sqlTextT);
             * */
            //执行查询
            DataTable dt = DBHelper.ExecuteQueryDT(sqlText.ToString());
            if (dt != null)
            {
                if(dt.Rows.Count > 0)
                {
                    foreach(DataRow dr in dt.Rows)
                    {
                        InventoryExtendModel inventoryExtendModel = new InventoryExtendModel();
                        inventoryExtendModel.Material_ID = dr["Material_ID"].ToString();
                        inventoryExtendModel.Material_Name = dr["Material_Name"].ToString();
                        inventoryExtendModel.Factory_ID = dr["Factory_ID"].ToString();
                        inventoryExtendModel.Stock_ID = dr["Stock_ID"].ToString();
                        inventoryExtendModel.SKU = dr["SKU"].ToString();
                        inventoryExtendModel.Count = Convert.ToInt32(dr["Count"].ToString());
                        inventoryExtendModel.Inventory_State = dr["Inventory_State"].ToString();
                        inventoryExtendModel.Batch_ID = dr["Batch_ID"].ToString();
                        inventoryExtendModelList.Add(inventoryExtendModel);
                    }
                    return inventoryExtendModelList;
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 通过物料Id来查找库存信息
        /// </summary>
        /// <param name="materialId">库存Id</param>
        /// <returns></returns>
        public InventoryExtendModel findInventoryInfoByMaterialId(string materialId, string inventoryState, string stockId, string batchId, string factoryId)
        {
            InventoryExtendModel inventoryExtendModel = new InventoryExtendModel();
            //编写sql语句
            StringBuilder sqlText = new StringBuilder("SELECT a.Batch_ID,a.[Count],a.Evaluate_Type,a.Factory_ID,a.ID,a.Inventory_State,a.Inventory_Type, a.Material_Group,a.Material_ID,a.SKU,a.Stocktaking,a.Stock_ID, b.Material_Name,c.Factory_Name,d.Stock_Name");
            sqlText.Append(" FROM Inventory a JOIN Material b ON a.Material_ID=b.Material_ID JOIN Factory c ON a.Factory_ID=c.Factory_ID JOIN Stock d ON a.Stock_ID=d.Stock_ID");
            sqlText.Append(" WHERE a.Material_ID=@Material_ID AND a.Factory_ID=@Factory_ID AND a.Inventory_State=@Inventory_State AND a.Batch_ID=@Batch_ID AND  a.Stock_ID=@Stock_ID");

            SqlParameter[] sqlParas = new SqlParameter[] { 
                new SqlParameter("@Material_ID",SqlDbType.VarChar),
                new SqlParameter("@Factory_ID",SqlDbType.VarChar),
                new SqlParameter("@Inventory_State",SqlDbType.VarChar),
                new SqlParameter("@Batch_ID",SqlDbType.VarChar),
                new SqlParameter("@Stock_ID",SqlDbType.VarChar)
            };
            sqlParas[0].Value = materialId;
            sqlParas[1].Value = factoryId;
            sqlParas[2].Value = inventoryState;
            sqlParas[3].Value = batchId;
            sqlParas[4].Value = stockId;

            //执行查询操作
            DataTable dt = DBHelper.ExecuteQueryDT(sqlText.ToString(),sqlParas);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    DataRow dr = dt.Rows[0];
                    inventoryExtendModel.Material_ID = dr["Material_ID"].ToString();
                    inventoryExtendModel.Material_Name = dr["Material_Name"].ToString();
                    inventoryExtendModel.Factory_ID = dr["Factory_ID"].ToString();
                    inventoryExtendModel.Factory_Name = dr["Factory_Name"].ToString();
                    inventoryExtendModel.Stock_ID = dr["Stock_ID"].ToString();
                    inventoryExtendModel.Stock_Name = dr["Stock_Name"].ToString();
                    inventoryExtendModel.Material_Group = dr["Material_Group"].ToString();
                    inventoryExtendModel.Inventory_State = dr["Inventory_State"].ToString();
                    inventoryExtendModel.Stocktaking = Convert.ToBoolean(dr["Stocktaking"].ToString());
                    inventoryExtendModel.Evaluate_Type = dr["Evaluate_Type"].ToString();
                    inventoryExtendModel.SKU = dr["SKU"].ToString();
                    inventoryExtendModel.Count = Convert.ToInt32(dr["Count"].ToString());
                    inventoryExtendModel.Inventory_State = dr["Inventory_State"].ToString();
                    inventoryExtendModel.Batch_ID = dr["Batch_ID"].ToString();
                    return inventoryExtendModel;
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 通过物料Id来查找库存信息
        /// </summary>
        /// <param name="materialId">库存Id</param>
        /// <returns></returns>
        public InventoryExtendModel findInventoryInfoByMaterialId(string materialId, string inventoryState, string batchId)
        {
            InventoryExtendModel inventoryExtendModel = new InventoryExtendModel();
            //编写sql语句
            StringBuilder sqlText = new StringBuilder("SELECT a.Batch_ID,a.[Count],a.Evaluate_Type,a.Factory_ID,a.ID,a.Inventory_State,a.Inventory_Type, a.Material_Group,a.Material_ID,a.SKU,a.Stocktaking,a.Stock_ID, b.Material_Name,c.Factory_Name,d.Stock_Name");
            sqlText.Append(" FROM Inventory a JOIN Material b ON a.Material_ID=b.Material_ID JOIN Factory c ON a.Factory_ID=c.Factory_ID JOIN Stock d ON a.Stock_ID=d.Stock_ID");
            sqlText.Append(" WHERE a.Material_ID=@Material_ID AND a.Inventory_State=@Inventory_State AND a.Batch_ID=@Batch_ID");

            SqlParameter[] sqlParas = new SqlParameter[] { 
                new SqlParameter("@Material_ID",SqlDbType.VarChar),
                new SqlParameter("@Inventory_State",SqlDbType.VarChar),
                new SqlParameter("@Batch_ID",SqlDbType.VarChar),
            };
            sqlParas[0].Value = materialId;
            sqlParas[1].Value = inventoryState;
            sqlParas[2].Value = batchId;

            //执行查询操作
            DataTable dt = DBHelper.ExecuteQueryDT(sqlText.ToString(), sqlParas);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    DataRow dr = dt.Rows[0];
                    inventoryExtendModel.Material_ID = dr["Material_ID"].ToString();
                    inventoryExtendModel.Material_Name = dr["Material_Name"].ToString();
                    inventoryExtendModel.Factory_ID = dr["Factory_ID"].ToString();
                    inventoryExtendModel.Factory_Name = dr["Factory_Name"].ToString();
                    inventoryExtendModel.Stock_ID = dr["Stock_ID"].ToString();
                    inventoryExtendModel.Stock_Name = dr["Stock_Name"].ToString();
                    inventoryExtendModel.Material_Group = dr["Material_Group"].ToString();
                    inventoryExtendModel.Inventory_State = dr["Inventory_State"].ToString();
                    inventoryExtendModel.Stocktaking = Convert.ToBoolean(dr["Stocktaking"].ToString());
                    inventoryExtendModel.Evaluate_Type = dr["Evaluate_Type"].ToString();
                    inventoryExtendModel.SKU = dr["SKU"].ToString();
                    inventoryExtendModel.Count = Convert.ToInt32(dr["Count"].ToString());
                    inventoryExtendModel.Inventory_State = dr["Inventory_State"].ToString();
                    inventoryExtendModel.Batch_ID = dr["Batch_ID"].ToString();
                    return inventoryExtendModel;
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }
    }
}
