﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.IDAL.StockIDAL;
using Lib.Model.StockModel;
using System.Collections;
using System.Data.SqlClient;
using System.Data;

namespace Lib.SqlServerDAL
{
    public class StockFactoryInfoDAL : StockFactoryInfoIDAL
    {
        /// <summary>
        /// 返回所有工厂信息
        /// </summary>
        /// <returns></returns>
        public List<StockFactoryModel> findAllFactoryInfoBy()
        {
            List<StockFactoryModel> stockFactoryList = new List<StockFactoryModel>();
            //StringBuilder sqlText = new StringBuilder ("SELECT ID, Factory_ID,Factory_Name, Factory_Address,Stock_ID FROM Factory");
            string sqlText = @"SELECT ID, Factory_ID,Factory_Name FROM Factory";

            //执行数据库查询语句，返回一个DataTable
            DataTable dt = DBHelper.ExecuteQueryDT(sqlText);
            if(dt !=null)
            {
                if(dt.Rows.Count > 0)
                {
                    foreach(DataRow dr in dt.Rows)
                    {
                        StockFactoryModel stockFactoryModel = new StockFactoryModel ();
                        stockFactoryModel.ID = dr["ID"].ToString();
                        stockFactoryModel.Factory_ID = dr["Factory_ID"].ToString();
                        stockFactoryModel.Factory_Name = dr["Factory_Name"].ToString();
                        //stockFactoryModel.Factory_Address = dr["Factory_Address"].ToString();
                        //stockFactoryModel.Stock_ID = dr["Stock_ID"].ToString();
                        stockFactoryList.Add(stockFactoryModel);
                    }
                    return stockFactoryList;
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 根据工厂ID，获取工厂名称
        /// </summary>
        /// <param name="factoryId"></param>
        /// <returns></returns>
        public DataTable getFactoryInfoBy(string factoryId)
        {
            //拼接SQL语句
            StringBuilder sqlText = new StringBuilder("");
            sqlText.Append("SELECT Factory_ID,Factory_Name");
            sqlText.Append("    FROM Factory");
            sqlText.Append("    WHERE Factory_ID=@factoryId ");
            //设置参数
            SqlParameter[] sqlParas = new SqlParameter[] { 
                new SqlParameter("@factoryId",SqlDbType.VarChar)
            };
            sqlParas[0].Value = factoryId;

            return DBHelper.ExecuteQueryDT(sqlText.ToString(), sqlParas);    //返回DataTable
        }
    }
}
