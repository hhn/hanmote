﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace Lib.SqlServerDAL.StockDAL
{
    public class ReceiveScoreDAL
    {
        public DataTable filltable()
        {
            StringBuilder strbld = new StringBuilder("select RawReceiveCheck_ID as 检验单号,GoodsReceiveNote_ID as 入库单号,")
               .Append("Supplier_ID as 供应商,Material_ID as 物料编号,Check_Result as 质检结果, RawMaterialReceiveCheck_Score as 原材料检验收货得分,Create_Time as 维护时间,Creator_Name as 维护人")
               .Append(" from Raw_Material_Receive_Check where RawMaterialReceiveCheck_Score is null");
            
             DataTable dt = DBHelper.ExecuteQueryDT(strbld.ToString());
             return dt;

        }

        public void updatetable1(int i,decimal score,DateTime createtime,string cn)
        {
            StringBuilder strbld = new StringBuilder("update Raw_Material_Receive_Check set RawMaterialReceiveCheck_Score=@rmrc,Create_Time=@ct,")
                .Append("Creator_Name=@cn where RawReceiveCheck_ID=@i");
            SqlParameter[] sqlpara = new SqlParameter[]
            {
                new SqlParameter("@i",SqlDbType.Int),
                new SqlParameter("@rmrc",SqlDbType.Decimal),
                new SqlParameter("@ct",SqlDbType.Date),
                new SqlParameter("@cn",SqlDbType.VarChar),

            };

            sqlpara[0].Value = i;
            sqlpara[1].Value = score;
            sqlpara[2].Value = createtime;
            sqlpara[3].Value = cn;

            DBHelper.ExecuteNonQuery(strbld.ToString(),sqlpara);
 
        }


        public DataTable filltable2()
        {
            StringBuilder strbld = new StringBuilder("select GoodsReceiveNote_ID as 入库单号,Order_ID as 订单号,")
             .Append("Supplier_ID as 供应商,Material_ID as 物料编号,GoodsShipment_Score as 装运评分,StockIn_Date as 入库时间")
             .Append(" from GoodsReceive_Note where GoodsShipment_Score is null");

            DataTable dt = DBHelper.ExecuteQueryDT(strbld.ToString());
            return dt;
 
        }


        public void updatetable2(decimal i, string cn)
        {
            StringBuilder strbld = new StringBuilder("update GoodsReceive_Note set GoodsShipment_Score=@gss")
                .Append(" where GoodsReceiveNote_ID=@grn");
            SqlParameter[] sqlpara = new SqlParameter[]
            {
                new SqlParameter("@gss",SqlDbType.Decimal),
                new SqlParameter("@grn",SqlDbType.VarChar),

            };

            sqlpara[0].Value = i;
            sqlpara[1].Value = cn;

            DBHelper.ExecuteNonQuery(strbld.ToString(), sqlpara);

        }


        /// <summary>
        /// 生成非原材料拒收数量表
        /// </summary>
        /// <returns></returns>
        public DataTable filltable3()
        {
            StringBuilder strbld = new StringBuilder("select NonRawReceiveCheck_ID as 编号,GoodsReceiveNote_ID as 入库单号,")
             .Append("Supplier_ID as 供应商,Material_ID as 物料编号,Reject_Num as 拒收数量,Create_Time as 维护时间,Creator_Name as 维护人")
             .Append(" from NonRaw_Material_Receive_Check where Reject_Num is null");

            DataTable dt = DBHelper.ExecuteQueryDT(strbld.ToString());
            return dt;

        }

        /// <summary>
        /// 维护非原材料表
        /// </summary>
        /// <param name="i"></param>
        /// <param name="score"></param>
        /// <param name="createtime"></param>
        /// <param name="cn"></param>
        public void updatetable3(int i, decimal score, DateTime createtime, string cn)
        {
            StringBuilder strbld = new StringBuilder("update NonRaw_Material_Receive_Check set Reject_Num=@rmrc,Create_Time=@ct,")
                .Append("Creator_Name=@cn where NonRawReceiveCheck_ID=@i");
            SqlParameter[] sqlpara = new SqlParameter[]
            {
                new SqlParameter("@i",SqlDbType.Int),
                new SqlParameter("@rmrc",SqlDbType.Decimal),
                new SqlParameter("@ct",SqlDbType.Date),
                new SqlParameter("@cn",SqlDbType.VarChar),

            };

            sqlpara[0].Value = i;
            sqlpara[1].Value = score;
            sqlpara[2].Value = createtime;
            sqlpara[3].Value = cn;

            DBHelper.ExecuteNonQuery(strbld.ToString(), sqlpara);

        }

    }
}
