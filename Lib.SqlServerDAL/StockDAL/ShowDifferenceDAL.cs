﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Lib.IDAL.StockIDAL;

namespace Lib.SqlServerDAL.StockDAL
{
    public class ShowDifferenceDAL:ShowDifferenceIDAL
    {
        /// <summary>
        /// 根据用户指定的要求筛选出符合要求的数据，按照
        /// stocktakingDocument_ID,Material_ID,batch_ID,Factory_ID,Stock_ID,StockCount,Stocktaking_count,Difference,SKU排列
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public DataTable selectInfo(DataTable dt)
        {
            StringBuilder strbld = new StringBuilder("select stocktakingDocument_ID as 凭证编号, Material_ID as 物料编码,batch_ID as 批次,")
            .Append("Factory_ID as 工厂,Stock_ID as 库存地,StockCount as 账面数量,")
                .Append("Stocktaking_count as 盘点数量,CountDifference as 差额 from  stocktakingDocument where ");

            int num = 0;
            int i = 0;

            if (!dt.Rows[0][0].ToString().Equals(""))
                num++;
            if (!dt.Rows[0][1].ToString().Equals(""))
                num++;
            if (!dt.Rows[0][2].ToString().Equals(""))
                num++;
            if (!dt.Rows[0][3].ToString().Equals(""))
                num++;
            if (!dt.Rows[0][4].ToString().Equals(""))
                num++;
            if (!dt.Rows[0][5].ToString().Equals(""))
                num++;
            if (!dt.Rows[0][6].ToString().Equals(""))
                num++;
            if (!dt.Rows[0][7].ToString().Equals(""))
                num++;
            if (!dt.Rows[0][8].ToString().Equals(""))
                num++;
            if (!dt.Rows[0][9].ToString().Equals(""))
                num++;
            if (!dt.Rows[0][10].ToString().Equals(""))
                num++;

            SqlParameter[] sqlpara = new SqlParameter[num];

            if (!dt.Rows[0][0].ToString().Equals(""))
            {
                strbld.Append(" Material_ID = @mid ");
                sqlpara[i] = new SqlParameter("@mid", SqlDbType.VarChar);
                sqlpara[i].Value = dt.Rows[0][0].ToString();
                i++;
            }

            if (dt.Rows[0][0].ToString() != "" && dt.Rows[0][1].ToString() != "")
            {
                strbld.Append(" and Factory_ID=@fid ");
                sqlpara[i] = new SqlParameter("@fid", SqlDbType.VarChar);
                sqlpara[i].Value = dt.Rows[0][1].ToString();
                i++;
            }
            if (dt.Rows[0][0].ToString() == "" && dt.Rows[0][1].ToString() != "")
            {
                strbld.Append(" Factory_ID=@fid ");
                sqlpara[i] = new SqlParameter("@fid", SqlDbType.VarChar);
                sqlpara[i].Value = dt.Rows[0][1].ToString();
                i++;
            }

            if ((dt.Rows[0][0].ToString() != "" || dt.Rows[0][1].ToString() != "") && dt.Rows[0][2].ToString() != "")
            {
                strbld.Append(" and Stock_ID=@sid ");
                sqlpara[i] = new SqlParameter("@sid", SqlDbType.VarChar);
                sqlpara[i].Value = dt.Rows[0][2].ToString();
                i++;
            }
            if ((dt.Rows[0][0].ToString() == "" && dt.Rows[0][1].ToString() == "") && dt.Rows[0][2].ToString() != "")
            {
                strbld.Append(" Stock_ID=@sid ");
                sqlpara[i] = new SqlParameter("@sid", SqlDbType.VarChar);
                sqlpara[i].Value = dt.Rows[0][2].ToString();
                i++;
            }

            if ((dt.Rows[0][0].ToString() != "" || dt.Rows[0][1].ToString() != "" || dt.Rows[0][2].ToString() != "") && dt.Rows[0][3].ToString() != "")
            {
                strbld.Append(" and batch_ID=@bid ");
                sqlpara[i] = new SqlParameter("@bid", SqlDbType.VarChar);
                sqlpara[i].Value = dt.Rows[0][3].ToString();
                i++;
            }
            if ((dt.Rows[0][0].ToString() == "" && dt.Rows[0][1].ToString() == "" && dt.Rows[0][2].ToString() == "") && dt.Rows[0][3].ToString() != "")
            {
                strbld.Append(" batch_ID=@bid ");
                sqlpara[i] = new SqlParameter("@bid", SqlDbType.VarChar);
                sqlpara[i].Value = dt.Rows[0][3].ToString();
                i++;
            }
            if ((dt.Rows[0][0].ToString() != "" || dt.Rows[0][1].ToString() != "" || dt.Rows[0][2].ToString() != "" || dt.Rows[0][3].ToString() !="") && dt.Rows[0][4].ToString() != "")
            {
                strbld.Append(" and stocktakingDocument_ID=@stktkdid ");
                sqlpara[i] = new SqlParameter("@stktkdid", SqlDbType.VarChar);
                sqlpara[i].Value = dt.Rows[0][4].ToString();
                i++;
            }
            if ((dt.Rows[0][0].ToString() == "" && dt.Rows[0][1].ToString() == "" && dt.Rows[0][2].ToString() == "" && dt.Rows[0][3].ToString() == "") && dt.Rows[0][4].ToString() != "")
            {
                strbld.Append(" stocktakingDocument_ID=@stktkdid ");
                sqlpara[i] = new SqlParameter("@stktkdid", SqlDbType.VarChar);
                sqlpara[i].Value = dt.Rows[0][4].ToString();
                i++;
            }


            if ((dt.Rows[0][0].ToString() != "" || dt.Rows[0][1].ToString() != "" || dt.Rows[0][2].ToString() != "" || dt.Rows[0][3].ToString() != "" || dt.Rows[0][4].ToString() != "") && dt.Rows[0][5].ToString() != "")
            {
                strbld.Append(" and stocktaking_ID=@stktkid ");
                sqlpara[i] = new SqlParameter("@stktkid", SqlDbType.VarChar);
                sqlpara[i].Value = dt.Rows[0][5].ToString();
                i++;
            }
            if ((dt.Rows[0][0].ToString() == "" && dt.Rows[0][1].ToString() == "" && dt.Rows[0][2].ToString() == "" && dt.Rows[0][3].ToString() == "" && dt.Rows[0][4].ToString() == "") && dt.Rows[0][5].ToString() != "")
            {
                strbld.Append(" stocktaking_ID=@stktkid ");
                sqlpara[i] = new SqlParameter("@stktkid", SqlDbType.VarChar);
                sqlpara[i].Value = dt.Rows[0][5].ToString();
                i++;
            }

            if (!dt.Rows[0][6].ToString().Equals(""))
            {
                strbld.Append(" and Inventory_State= @is");
                sqlpara[i] = new SqlParameter("@is", SqlDbType.VarChar);
                sqlpara[i].Value = dt.Rows[0][6].ToString();
                i++;
            }

            if (!dt.Rows[0][7].ToString().Equals(""))
            {
                strbld.Append(" and planStocktaking_Date = @pstkd");
                sqlpara[i] = new SqlParameter("@pstkd", SqlDbType.Date);
                sqlpara[i].Value = Convert.ToDateTime(dt.Rows[0][7].ToString());
                i++;
            }

            if (!dt.Rows[0][8].ToString().Equals(""))
            {
                strbld.Append(" and Account_Year = @ay");
                sqlpara[i] = new SqlParameter("@ay", SqlDbType.VarChar);
                sqlpara[i].Value = dt.Rows[0][8].ToString();
                i++;
            }


            if (!dt.Rows[0][9].ToString().Equals(""))
            {
                strbld.Append(" and stocktakingrefer_ID = @stkrid");
                sqlpara[i] = new SqlParameter("@stkrid", SqlDbType.VarChar);
                sqlpara[i].Value = dt.Rows[0][9].ToString();
                i++;
            }

            if (!dt.Rows[0][10].ToString().Equals(""))
            {
                strbld.Append(" and Stocktaking_Date = @stkd");
                sqlpara[i] = new SqlParameter("@stkd", SqlDbType.Date);
                sqlpara[i].Value = Convert.ToDateTime(dt.Rows[0][10].ToString());
                i++;
            }


            if (Convert.ToBoolean(dt.Rows[0][11].ToString()) == true)
            {
                strbld.Append(" and State = '创建盘点凭证'");
            }

            if (Convert.ToBoolean(dt.Rows[0][11].ToString()) == true && Convert.ToBoolean(dt.Rows[0][12].ToString()) == true)
            {
                strbld.Append(" or State = '部分盘点'");
            }

            if (Convert.ToBoolean(dt.Rows[0][11].ToString()) == false && Convert.ToBoolean(dt.Rows[0][12].ToString()) == true)
            {
                strbld.Append(" and State = '部分盘点'");
            }

            if ((Convert.ToBoolean(dt.Rows[0][11].ToString()) == true || Convert.ToBoolean(dt.Rows[0][12].ToString()) == true) && Convert.ToBoolean(dt.Rows[0][13].ToString())==true)
            {
                strbld.Append(" or State = '完成盘点'");
            }

            if ((Convert.ToBoolean(dt.Rows[0][11].ToString()) == false && Convert.ToBoolean(dt.Rows[0][12].ToString()) == false) && Convert.ToBoolean(dt.Rows[0][13].ToString()) == true)
            {
                strbld.Append(" and State = '完成盘点'");
            }

            if (Convert.ToBoolean(dt.Rows[0][14].ToString()) == true)
            {
                strbld.Append(" and Item_State = '还未计数'");
            }

            if (Convert.ToBoolean(dt.Rows[0][14].ToString()) == true && Convert.ToBoolean(dt.Rows[0][15].ToString()) == true)
            {
                strbld.Append(" or Item_State = '仅记值'");
            }

            if (Convert.ToBoolean(dt.Rows[0][14].ToString()) == false && Convert.ToBoolean(dt.Rows[0][15].ToString()) == true)
            {
                strbld.Append(" and Item_State = '仅记值'");
            }

            if ((Convert.ToBoolean(dt.Rows[0][14].ToString()) == true || Convert.ToBoolean(dt.Rows[0][15].ToString()) == true) && Convert.ToBoolean(dt.Rows[0][16].ToString()) == true)
            {
                strbld.Append(" or Item_State = '已过账'");
            }

            if ((Convert.ToBoolean(dt.Rows[0][14].ToString()) == false && Convert.ToBoolean(dt.Rows[0][15].ToString()) == false) && Convert.ToBoolean(dt.Rows[0][16].ToString()) == true)
            {
                strbld.Append(" and Item_State = '已过账'");
            }

            if ((Convert.ToBoolean(dt.Rows[0][14].ToString()) == true || Convert.ToBoolean(dt.Rows[0][15].ToString()) == true || Convert.ToBoolean(dt.Rows[0][16].ToString()) == true) && Convert.ToBoolean(dt.Rows[0][17].ToString()) == true)
            {
                strbld.Append(" or Item_State = '重新盘点'");
            }

            if ((Convert.ToBoolean(dt.Rows[0][14].ToString()) == false && Convert.ToBoolean(dt.Rows[0][15].ToString()) == false && Convert.ToBoolean(dt.Rows[0][16].ToString()) == false) && Convert.ToBoolean(dt.Rows[0][17].ToString()) == true)
            {
                strbld.Append(" and Item_State = '重新盘点'");
            }

            strbld.Append(" group by stocktakingDocument_ID,Material_ID,batch_ID,Factory_ID,Stock_ID,StockCount,Stocktaking_count,CountDifference,SKU");
            DataTable dt0 = DBHelper.ExecuteQueryDT(strbld.ToString(), sqlpara);

            return dt0;
        }
    }
}
