﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Lib.IDAL.StockIDAL;

namespace Lib.SqlServerDAL.StockDAL
{
    public class ConsignmentStockDAL:ConsignmentStockIDAL
    {
        public DataTable showStock(DataTable dt0)
        {
             
                int num = 0;
                int j = 0;

                if (!dt0.Rows[0][0].ToString().Equals(""))
                    num++;

                if (!dt0.Rows[0][1].ToString().Equals(""))
                    num++;

                if (!dt0.Rows[0][2].ToString().Equals(""))
                    num++;

                if (!dt0.Rows[0][3].ToString().Equals(""))
                    num++;

                if (!dt0.Rows[0][4].ToString().Equals(""))
                    num++;

                if (!dt0.Rows[0][5].ToString().Equals(""))
                    num++;

                StringBuilder strbld = new StringBuilder("select Company_ID as 公司,Inventory.Factory_ID  as 工厂,Stock_ID as 库存地,")
                .Append("Material_ID as 物料,Batch_ID as 批次,Inventory_Type as 库存类型,sum(Count) as 数量,SKU as 单位 from Inventory,Factory where")
                .Append(" Factory.Factory_ID = Inventory.Factory_ID");

                SqlParameter[] sqlpara = new SqlParameter[num];

                strbld.Append(" and Inventory.Factory_ID = @fid");
                sqlpara[j] = new SqlParameter("@fid", SqlDbType.VarChar);
                sqlpara[j].Value = dt0.Rows[0][1].ToString().Trim();
                j++;

                if (!dt0.Rows[0][0].ToString().Equals(""))
                {
                    strbld.Append(" AND Material_ID = @mid ");
                    sqlpara[j] = new SqlParameter("@mid", SqlDbType.VarChar);
                    sqlpara[j].Value = dt0.Rows[0][0].ToString().Trim();
                    j++;
                }

                if (dt0.Rows[0][2].ToString() != "")
                {
                    strbld.Append(" and Stock_ID=@sid ");
                    sqlpara[j] = new SqlParameter("@sid", SqlDbType.VarChar);
                    sqlpara[j].Value = dt0.Rows[0][2].ToString();
                    j++;
                }


                if (dt0.Rows[0][3].ToString() != "")
                {
                    strbld.Append(" and Batch_ID=@bid ");
                    sqlpara[j] = new SqlParameter("@bid", SqlDbType.VarChar);
                    sqlpara[j].Value = dt0.Rows[0][3].ToString();
                    j++;
                }

                if (dt0.Rows[0][4].ToString() != "")
                {
                    strbld.Append(" and Evaluate_Type=@et ");
                    sqlpara[j] = new SqlParameter("@et", SqlDbType.VarChar);
                    sqlpara[j].Value = dt0.Rows[0][4].ToString();
                    j++;
                }

                if (dt0.Rows[0][5].ToString() != "")
                {
                    strbld.Append(" and Material_Group=@mg ");
                    sqlpara[j] = new SqlParameter("@mg", SqlDbType.VarChar);
                    sqlpara[j].Value = dt0.Rows[0][5].ToString();
                    j++;
                }

                if (Convert.ToBoolean(dt0.Rows[0][6].ToString()) == true)
                {
                    strbld.Append(" and (Inventory_Type='普通库存' ");
                }

                if (Convert.ToBoolean(dt0.Rows[0][6].ToString()) == false && Convert.ToBoolean(dt0.Rows[0][7].ToString()) == true)
                {
                    strbld.Append(" and (Inventory_Type='供应商寄售' ");
                }
                if (Convert.ToBoolean(dt0.Rows[0][6].ToString()) == false && Convert.ToBoolean(dt0.Rows[0][7].ToString()) == false && Convert.ToBoolean(dt0.Rows[0][8].ToString()) == true)
                {
                    strbld.Append(" and (Inventory_Type='供应商分包' ");
                }

                if (Convert.ToBoolean(dt0.Rows[0][6].ToString()) == true && Convert.ToBoolean(dt0.Rows[0][7].ToString()) == true)
                {
                    strbld.Append(" or Inventory_Type='供应商寄售' ");
                }

                if (Convert.ToBoolean(dt0.Rows[0][6].ToString()) == true || Convert.ToBoolean(dt0.Rows[0][7].ToString()) == true && Convert.ToBoolean(dt0.Rows[0][8].ToString()) == true)
                {
                    strbld.Append(" or Inventory_Type='供应商分包' ");
                }
                if (Convert.ToBoolean(dt0.Rows[0][6].ToString()) == true || Convert.ToBoolean(dt0.Rows[0][7].ToString()) == true || Convert.ToBoolean(dt0.Rows[0][8].ToString()) == true)
                {
                    strbld.Append(")");
                }

                if (Convert.ToBoolean(dt0.Rows[0][9].ToString()) == true)
                {
                    strbld.Append(" and Count<0 ");
                }
                if (Convert.ToBoolean(dt0.Rows[0][10].ToString()) == true)
                {
                    strbld.Append(" and Batch_ID!='' ");
                }

                if (Convert.ToBoolean(dt0.Rows[0][11].ToString()) == true)
                {
                    strbld.Append(" and Count<>0");
                }

                strbld.Append(" group by Company_ID ,Inventory.Factory_ID ,Stock_ID, Material_ID,Batch_ID,Inventory_Type,SKU ");
                DataTable info = DBHelper.ExecuteQueryDT(strbld.ToString(), sqlpara);
                return info;
             
        }
    }
}
