﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Lib.IDAL.StockIDAL.StockReport;

namespace Lib.SqlServerDAL.StockDAL.StockReportDAL
{
    public class StockOverviewDAL:StockOverviewIDAL
    {
        public DataTable selectInfo(DataTable dt)
        {
            #region 参考
            /*StringBuilder strbld0 = new StringBuilder("select Company_ID ,Inventory.Factory_ID,Inventory.Stock_ID ,")
            .Append("Inventory.Material_ID,Inventory.Batch_ID ,Inventory.Count, Inventory.Inventory_Type from Inventory,Factory where")
                .Append("  Inventory.Factory_ID=Factory.Factory_ID ");
            DataTable tb = DBHelper.ExecuteQueryDT(strbld0.ToString());
            if (tb.Rows.Count > 0)
            {
                for (int i = 0; i < tb.Rows.Count; i++)
                {
                    StringBuilder strbld01 = new StringBuilder("insert into StockOverview(Company_ID,Factory_ID,Stock_ID,Material_ID,Batch_ID,Count,Inventory_Type)")
                        .Append("values(@cid,@fid,@sid,@mid,@bid,@c,@it)");

                    SqlParameter[] sqlpa = new SqlParameter[] {
            
                new SqlParameter("@cid",SqlDbType.VarChar),
                new SqlParameter("@fid",SqlDbType.VarChar),
                new SqlParameter("@sid",SqlDbType.VarChar),
                new SqlParameter("@mid",SqlDbType.VarChar),
                new SqlParameter("@bid",SqlDbType.VarChar),
                new SqlParameter("@c",SqlDbType.Float),
                new SqlParameter("@it",SqlDbType.VarChar),

            };

                    sqlpa[0].Value = tb.Rows[i][0].ToString();
                    sqlpa[1].Value = tb.Rows[i][1].ToString();
                    sqlpa[2].Value = tb.Rows[i][2].ToString();
                    sqlpa[3].Value = tb.Rows[i][3].ToString();
                    sqlpa[4].Value = tb.Rows[i][4].ToString();
                    sqlpa[5].Value = Convert.ToDouble(tb.Rows[i][5].ToString());
                    sqlpa[6].Value = tb.Rows[i][6].ToString();

                    DBHelper.ExecuteNonQuery(strbld01.ToString(), sqlpa);
                }
            }*/
            #endregion

            int num = 0;
            int j = 0;

            if (!dt.Rows[0][0].ToString().Equals(""))
                num++;

            if (!dt.Rows[0][1].ToString().Equals(""))
                num++;

            if (!dt.Rows[0][2].ToString().Equals(""))
                num++;

            if (!dt.Rows[0][3].ToString().Equals(""))
                num++;

            SqlParameter[] sqlpara = new SqlParameter[num];

            StringBuilder strbld = new StringBuilder("select Company_ID as 公司,Inventory.Factory_ID as 工厂,Stock_ID as 库存地,")
            .Append("Material_ID as 物料,Batch_ID as 批次,sum(Count) as 数量,Inventory_Type as 库存类型,SKU as 单位  from Inventory,Factory where")
            .Append (" Factory.Factory_ID = Inventory.Factory_ID");

            if (!dt.Rows[0][0].ToString().Equals(""))
            {
                strbld.Append(" AND Material_ID = @mid ");
                sqlpara[j] = new SqlParameter("@mid", SqlDbType.VarChar);
                sqlpara[j].Value = dt.Rows[0][0].ToString();
                j++;
            }

           
            if ( !dt.Rows[0][0].ToString().Equals("")&&dt.Rows[0][1].ToString() != "")
            {
                strbld.Append(" and Inventory.Factory_ID=@fid ");
                sqlpara[j] = new SqlParameter("@fid", SqlDbType.VarChar);
                sqlpara[j].Value = dt.Rows[0][1].ToString();
                j++;
            }

           /* if (dt.Rows[0][0].ToString().Equals("") && dt.Rows[0][1].ToString() != "")
            {
                strbld.Append(" Inventory.Factory_ID=@fid ");
                sqlpara[j] = new SqlParameter("@fid", SqlDbType.VarChar);
                sqlpara[j].Value = dt.Rows[0][1].ToString();
                j++;
            }*/

            if ((!dt.Rows[0][0].ToString().Equals("") || dt.Rows[0][1].ToString() != "") && dt.Rows[0][2].ToString() != "")
            {
                strbld.Append(" and Stock_ID=@sid ");
                sqlpara[j] = new SqlParameter("@sid", SqlDbType.VarChar);
                sqlpara[j].Value = dt.Rows[0][2].ToString();
                j++;
            }

           /* if ((dt.Rows[0][0].ToString().Equals("") && dt.Rows[0][1].ToString() == "") && dt.Rows[0][2].ToString() != "")
            {
                strbld.Append(" Stock_ID=@sid ");
                sqlpara[j] = new SqlParameter("@sid", SqlDbType.VarChar);
                sqlpara[j].Value = dt.Rows[0][2].ToString();
                j++;
            }
            */
            if ((!dt.Rows[0][0].ToString().Equals("") || dt.Rows[0][1].ToString() != ""|| dt.Rows[0][2].ToString() != "")&&dt.Rows[0][3].ToString() != "")
            {
                strbld.Append(" and Batch_ID=@bid ");
                sqlpara[j] = new SqlParameter("@bid", SqlDbType.VarChar);
                sqlpara[j].Value = dt.Rows[0][3].ToString();
                j++;
            }
           /* if ((dt.Rows[0][0].ToString().Equals("") && dt.Rows[0][1].ToString() == "" && dt.Rows[0][2].ToString() == "") && dt.Rows[0][3].ToString() != "")
            {
                strbld.Append(" Batch_ID=@bid ");
                sqlpara[j] = new SqlParameter("@bid", SqlDbType.VarChar);
                sqlpara[j].Value = dt.Rows[0][3].ToString();
                j++;
            }*/

            if (Convert.ToBoolean(dt.Rows[0][4].ToString()) == false)
            {
                strbld.Append(" and Inventory_Type='普通库存' ");
            }

            if (Convert.ToBoolean(dt.Rows[0][5].ToString()) == true)
            {
                strbld.Append(" and Count<>0 ");
            }

            strbld.Append(" group by Company_ID ,Inventory.Factory_ID ,Stock_ID, Material_ID ,Batch_ID  ,Inventory_Type,SKU ");

            if (Convert.ToBoolean(dt.Rows[0][6].ToString()) == true)
            {
                strbld.Append(" order by Company_ID ");
            }

            if (Convert.ToBoolean(dt.Rows[0][6].ToString()) == false && Convert.ToBoolean(dt.Rows[0][7].ToString()) == true)
            {
                strbld.Append(" order by Inventory.Factory_ID ");
            }
            if (Convert.ToBoolean(dt.Rows[0][6].ToString()) == true && Convert.ToBoolean(dt.Rows[0][7].ToString()) == true)
            {
                strbld.Append(" ,Inventory.Factory_ID ");
            }

            if ((Convert.ToBoolean(dt.Rows[0][6].ToString()) == true || Convert.ToBoolean(dt.Rows[0][7].ToString()) == true) && Convert.ToBoolean(dt.Rows[0][8].ToString()) == true)
            {
                strbld.Append(" ,Stock_ID ");
            }
            if ((Convert.ToBoolean(dt.Rows[0][6].ToString()) == false && Convert.ToBoolean(dt.Rows[0][7].ToString()) == false) && Convert.ToBoolean(dt.Rows[0][8].ToString()) == true)
            {
                strbld.Append(" order by Stock_ID ");
            }

            if ((Convert.ToBoolean(dt.Rows[0][6].ToString()) == true || Convert.ToBoolean(dt.Rows[0][7].ToString()) == true || Convert.ToBoolean(dt.Rows[0][8].ToString()) == true) && Convert.ToBoolean(dt.Rows[0][9].ToString()) == true)
            {
                strbld.Append(" ,batch_ID ");
            }
            if ((Convert.ToBoolean(dt.Rows[0][6].ToString()) == false && Convert.ToBoolean(dt.Rows[0][7].ToString()) == false && Convert.ToBoolean(dt.Rows[0][8].ToString()) == false) && Convert.ToBoolean(dt.Rows[0][9].ToString()) == true)
            {
                strbld.Append(" order by batch_ID ");
            }


            if ((Convert.ToBoolean(dt.Rows[0][6].ToString()) == true || Convert.ToBoolean(dt.Rows[0][7].ToString()) == true || Convert.ToBoolean(dt.Rows[0][8].ToString()) == true || Convert.ToBoolean(dt.Rows[0][9].ToString()) == true) && Convert.ToBoolean(dt.Rows[0][10].ToString()) == true)
            {
                strbld.Append(" ,Inventory_Type ");
            }
            if ((Convert.ToBoolean(dt.Rows[0][6].ToString()) == false && Convert.ToBoolean(dt.Rows[0][7].ToString()) == false && Convert.ToBoolean(dt.Rows[0][8].ToString()) == false && Convert.ToBoolean(dt.Rows[0][9].ToString()) == false) && Convert.ToBoolean(dt.Rows[0][10].ToString()) == true)
            {
                strbld.Append(" order by Inventory_Type ");
            }

            DataTable info = DBHelper.ExecuteQueryDT(strbld.ToString(), sqlpara);
            /*StringBuilder str = new StringBuilder("delete from StockOverview");
            DBHelper.ExecuteNonQuery(str.ToString());*/
            return info;
 
        }
    }
}
