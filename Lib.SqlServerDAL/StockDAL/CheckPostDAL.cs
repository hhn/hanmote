﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Lib.IDAL.StockIDAL;

namespace Lib.SqlServerDAL
{
    public class CheckPostDAL:CheckPostIDAL
    {
        /// <summary>
        /// 根据凭证编号选择满足条件的datatable
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public DataTable selectInfo(string documentid, string accountyear)
        {
            StringBuilder strbld = new StringBuilder("select Material_ID as 物料编码,Batch_ID as 批次,Inventory_State as 库存状态,")
                .Append("StockCount as 账面数量,Stocktaking_count as 盘点数量,CountDifference as 差额,SKU as 计量单位,Reason as 原因,Evaluate_Type as 评估类型, Material_Group as 物料组,Inventory_Type as 库存类型 ")
                .Append(" from stocktakingDocument where stocktakingDocument_ID = @sdid and Account_Year=@ay and Item_State = '仅记值'");
            SqlParameter[] sqlpara = new SqlParameter[] { 
                new SqlParameter("@sdid",SqlDbType.VarChar),
                new SqlParameter("@ay",SqlDbType.VarChar),
            };

            sqlpara[0].Value = documentid;
            sqlpara[1].Value = accountyear;

            DataTable dt = DBHelper.ExecuteQueryDT(strbld.ToString(),sqlpara);
            return dt;
        }


        /// <summary>
        /// 根据数据表（selected,documentid,materialid,batchid,factoryid,stockid,reason）
        /// 进行过账
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        public bool checkPost(DataTable info)
        {
            int n = info.Rows.Count;
            for (int i = 0; i < n; i++)
            {
                if (Convert.ToBoolean(info.Rows[i][0].ToString()) == true)
                {
                    StringBuilder strbld = new StringBuilder("update stocktakingDocument set ")
                        .Append("Reason=@r,Account_Date=@ad,Item_State=@ist ")
                        .Append(" where stocktakingDocument_ID=@sdid and Material_ID=@mid and batch_ID=@bid and Factory_ID=@fid")
                        .Append(" and Stock_ID=@sid");

                    SqlParameter[] sqlpara = new SqlParameter[] { 
                    
                        new SqlParameter("@r",SqlDbType.VarChar),
                        new SqlParameter("@ad",SqlDbType.DateTime),
                        new SqlParameter("@ist",SqlDbType.VarChar),
                        new SqlParameter("@sdid",SqlDbType.VarChar),
                        new SqlParameter("@mid",SqlDbType.VarChar),
                        new SqlParameter("@bid",SqlDbType.VarChar),
                        new SqlParameter("@fid",SqlDbType.VarChar),
                        new SqlParameter("@sid",SqlDbType.VarChar),
                    };

                    sqlpara[0].Value = info.Rows[i][6].ToString();
                    sqlpara[1].Value = System.DateTime.Now.ToShortDateString();
                    sqlpara[2].Value = "已过账";
                    sqlpara[3].Value = info.Rows[i][1].ToString();
                    sqlpara[4].Value = info.Rows[i][2].ToString();
                    sqlpara[5].Value = info.Rows[i][3].ToString();
                    sqlpara[6].Value = info.Rows[i][4].ToString();
                    sqlpara[7].Value = info.Rows[i][5].ToString();

                    updateInventory(info);
                    DBHelper.ExecuteNonQuery(strbld.ToString(),sqlpara);

                    

                }
            }
                return true;
        }

        /// <summary>
        /// 根据物料凭证号读取工厂编号和库存地编号
        /// </summary>
        /// <param name="documentid"></param>
        /// <returns></returns>
        public DataTable getfactory(string documentid)
        {
            StringBuilder strbld = new StringBuilder("select Factory_ID,Stock_ID from stocktakingDocument where stocktakingDocument_ID=@sdid");
            SqlParameter sqlpara = new SqlParameter("@sdid", SqlDbType.VarChar);
            sqlpara.Value = documentid;
            DataTable dt = DBHelper.ExecuteQueryDT(strbld.ToString(), sqlpara);
            return dt;
        }


        /// <summary>
        /// 更新Inventory表数据
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public bool updateInventory(DataTable dt)
        {
            try
            {
                int n = dt.Rows.Count;
                for (int i = 0; i < n; i++)
                {

                    StringBuilder strbld = new StringBuilder("update Inventory set Count = Count+@c where Material_ID = @mid and Factory_ID =@fid and ")
                    .Append("Stock_ID=@sid  and Batch_ID=@bid and Inventory_State=@is and Evaluate_Type=@et")
                    .Append(" and Material_Group=@mg and Inventory_Type = @it");
                    SqlParameter[] sqlpara = new SqlParameter[] { 
            
                new SqlParameter("@mid",SqlDbType.VarChar),
                new SqlParameter("@fid",SqlDbType.VarChar),
                new SqlParameter("@sid",SqlDbType.VarChar),
                new SqlParameter("@bid",SqlDbType.VarChar),
                new SqlParameter("@is",SqlDbType.VarChar),
                new SqlParameter("@et",SqlDbType.VarChar),
                new SqlParameter("@mg",SqlDbType.VarChar),
                new SqlParameter("@it",SqlDbType.VarChar),
                new SqlParameter("@c",SqlDbType.Float),

            };

                    sqlpara[0].Value = dt.Rows[i][2].ToString();
                    sqlpara[1].Value = dt.Rows[i][4].ToString();
                    sqlpara[2].Value = dt.Rows[i][5].ToString();
                    sqlpara[3].Value = dt.Rows[i][3].ToString();
                    sqlpara[4].Value = dt.Rows[i][7].ToString();
                    sqlpara[5].Value = dt.Rows[i][8].ToString();
                    sqlpara[6].Value = dt.Rows[i][9].ToString();
                    sqlpara[7].Value = dt.Rows[i][10].ToString();
                    sqlpara[8].Value = Convert.ToDouble(dt.Rows[i][13].ToString());
                                    
                    DBHelper.ExecuteNonQuery(strbld.ToString(), sqlpara);
                }
                return true;
            }
            catch
            {
                return false;
            }

        }
    }
}
