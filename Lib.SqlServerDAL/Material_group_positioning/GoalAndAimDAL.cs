﻿using Lib.IDAL.Material_group_positioning;
using Lib.Model.MT_GroupModel;
using Lib.Model.PrdModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace Lib.SqlServerDAL.Material_group_positioning
{


    public class GoalAndAimDAL : GoalAndAimIDAL
    {
    
        /// <summary>
        /// 根据物料组获取目标
        /// </summary>
        /// <param name="mtId"></param>
        /// <returns></returns>
        public DataTable getAssertGoal(string mtId,int pageSize,int pageIndex )
        {
            string sqlText = @"select top "+pageSize+ "  * from  (select goalId,goalText,GoalValue,Unit,GoalLevelValue,GoalPipLevel,createTime,domain, ROW_NUMBER () OVER ( ORDER BY goalId ) AS RowNumber from TabAssertAndMainMtGoal  where MtGroupId='" + mtId+"') as tb where RowNumber>"+pageSize*(pageIndex-1);
            return DBHelper.ExecuteQueryDT(sqlText);
        }
        /// <summary>
        /// 查询产品供应目标总条数
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public int getGoalCount(string text)
        {
            string sqlText = @"select count(*) as num from TabprdGoalAndPrd where prdId='"+text+"'";
            int count = int.Parse(DBHelper.ExecuteQueryDT(sqlText).Rows[0][0].ToString());

            return count;
        }

        public bool getGoalId(string goalId)
        {
            string sqlText = @"SELECT * FROM TabprdGoalAndPrd where goalId='"+ goalId + "'";
            if (DBHelper.ExecuteQueryDT(sqlText).Rows.Count == 1) {
                return true;

            }
            return false;
        }

        /// <summary>
        /// 非产品
        /// </summary>
        /// <param name="mtId"></param>
        /// <returns></returns>
        public int getIndicateCount(string mtId)
        {
            string sqlText = @"select count(*) as num from TabAssertAndMainMtGoal where MtGroupId='" + mtId + "'";
            int count = int.Parse(DBHelper.ExecuteQueryDT(sqlText).Rows[0][0].ToString());
            return count;
        }
        /// <summary>
        /// 生产性物料
        /// </summary>
        /// <param name="mtId"></param>
        /// <returns></returns>
        public int getIndicationCount(string text)
        {
            string sqlText = @"select count(*) as num from TabPrdAndMaterial where prdId='" + text + "'";
            int count = int.Parse(DBHelper.ExecuteQueryDT(sqlText).Rows[0][0].ToString());

            return count;
        }
 
        /// <summary>
        /// 获取物料组信息
        /// </summary>
        /// <param name="mtGroupTypeId"></param>
        /// <returns></returns>
        public DataTable getMaterialGroupInfo(string mtGroupTypeId,int pageSize,int pageIndex)
        {
            string sqlText = @"select top "+pageSize+ " * from  (select Material_Group,Description,type.MaterialGroupType,ROW_NUMBER () OVER ( ORDER BY Material_Group ) AS RowNumber  from Material_Group Mg,TabMtGroupType type  where Category='" + mtGroupTypeId + "' and Mg.Category = type.MaterialGroupTypeId) as tb where RowNumber>"+pageSize*(pageIndex-1);
            return DBHelper.ExecuteQueryDT(sqlText);
        }

        /// <summary>
        /// 获取物料信息
        /// </summary>
        /// <param name="mtGroupTypeId">物料组编号</param>
        /// <param name="pageSize">显示记录条数</param>
        /// <param name="pageIndex">当前页码条数</param>
        /// <returns></returns>
        public DataTable getMaterialInfo(string mtGroupTypeId, int pageSize, int pageIndex)
        {
            string sqlText = @"select top " + pageSize + " * from  (select Material_ID as Material_Group, Material_Name as Description,ROW_NUMBER() OVER(ORDER BY Material_Group) AS RowNumber from Material  where Material_Group = '"+mtGroupTypeId+"' and Material_Purchase_Type = 0) as tb where RowNumber>" + pageSize * (pageIndex - 1);
            return DBHelper.ExecuteQueryDT(sqlText);
        }



        //获取组成产品的物料信息
        //text:产品编号
        public DataTable getMaterialInfo(string prdId,string goalId,int pageSize,int pageIndex)
        {
            StringBuilder sqlText = new StringBuilder();
            sqlText.Append("SELECT top  "+pageSize+ " Material_ID,Material_Name,indicatorId,indicatorText,indicatorScore,materialId,pipLevel,levelValue,Unit FROM (SELECT *,ROW_NUMBER () OVER ( ORDER BY Material_ID ) AS RowNumber   FROM");

            sqlText.Append(" (SELECT Material_ID, Material_Name FROM TabPrdAndMaterial pm, Material m WHERE prdId = '"+prdId+"' AND pm.materialId = m.Material_ID ) a");
            sqlText.Append(" LEFT JOIN ( SELECT indicatorId, indicatorText, indicatorScore, materialId, pipLevel,levelValue,Unit  FROM TabIndicator WHERE goalId = '" + goalId+"' ) b ON a.Material_ID= b.materialId ");
            sqlText.Append(" ) AS d  where RowNumber >"+pageSize*(pageIndex-1));

            SqlParameter[] sqlParameter = new SqlParameter[] {
                new SqlParameter("@pageSize",pageSize),
                new SqlParameter("@pageIndex",pageIndex),
                new SqlParameter("@prdId",prdId),
                new SqlParameter("@goalId",goalId)

            };

            return DBHelper.ExecuteQueryDT(sqlText.ToString());
           
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="mtGroupId1"></param>
        /// <returns></returns>
        public bool getMaterials(string mtGroupId1)
        {
            string sql = "select * from Material where Material_ID='"+ mtGroupId1+ "'";
            if (DBHelper.ExecuteQueryDT(sql).Rows.Count == 1)
            {
                return true;
            }

            return false;
        }

        public int getMtGroupCount(string typeId)
        {
            string sqlText = @"select count(*) from Material_Group Mg,TabMtGroupType type  where Category='" + typeId + "' and Mg.Category = type.MaterialGroupTypeId";
            int count = int.Parse(DBHelper.ExecuteQueryDT(sqlText).Rows[0][0].ToString());

            return count;
        }
        /// <summary>
        /// 加载物料组信息 
        /// </summary>
        /// <returns></returns>
        public DataTable getMtGroupInfo()
        {
            string sqlText = @"SELECT Material_Group as Id,Description as Value from Material_Group";
            return DBHelper.ExecuteQueryDT(sqlText);
        }

        public DataTable getMtGroupTypeId()
        {
            string sqlText = @"select MaterialGroupTypeId  as Id,MaterialGroupType  as Name FROM TabMtGroupType";
            return DBHelper.ExecuteQueryDT(sqlText);
        }

        public int getMtInfoCount(string mtGroupTypeId)
        {
            string sqlText = @"select Count(*) from Material  where Material_Group = '" + mtGroupTypeId + "' and Material_Purchase_Type = 0";
            return int.Parse(DBHelper.ExecuteQueryDT(sqlText).Rows[0][0].ToString());
        }

        //获取对应产品供应目标
        public DataTable getPrdAim(string text,int pageSize,int pageIndex)
        {
            string sqlText = @"SELECT top  "+pageSize+" goalId,goalText,goalSocre,createTime,pipLevel,domain FROM [dbo].[TabprdGoalAndPrd] where prdId='"+text+"'  and goalId not in (select top "+pageSize*(pageIndex-1)+" goalId from TabprdGoalAndPrd  ORDER BY goalId ASC) ORDER BY goalId";
            return DBHelper.ExecuteQueryDT(sqlText);
        }

        //获取产品组编号
        public DataTable GetPrdId()
        {
            string sqlText = @"select prdId from TabPrdInfo";
            return DBHelper.ExecuteQueryDT(sqlText);
        }
        /// <summary>
        /// 查询是否存在产品
        /// </summary>
        /// <param name="prdId"></param>
        /// <returns></returns>
        public bool getPrdId(string prdId)
        {
            string sql = "SELECT * FROM TabPrdInfo where prdId='" + prdId + "'";
            if (DBHelper.ExecuteQueryDT(sql).Rows.Count == 1) {
                return true;
            }

            return false;
             
        }

        //获取产品组编号
        public DataTable GetPrdName(string text)
        {
            string sqlText = @"select prdName from TabPrdInfo where prdId='"+text+"'";
            return DBHelper.ExecuteQueryDT(sqlText);
        }
        /// <summary>
        /// 保存物料组目标信息
        /// </summary>
        /// <param name="groupGoalModel1"></param>
        /// <returns></returns>
        public int insertAssertAndMainInfo(GroupGoalModel groupGoalModel)
        {
            string sqlText = @"if EXISTS (SELECT * from TabAssertAndMainMtGoal where goalId=@goalId or  GoalText=@GoalText)
                                        update TabAssertAndMainMtGoal set 
                                        createTime=GETDATE(),
                                        GoalText=@GoalText ,
                                        GoalValue=@GoalValue,
                                        Unit=@Unit,
                                        GoalLevelValue=@GoalLevelValue,
                                        GoalPipLevel=@GoalPipLevel,
                                        domain=@domain
                                    where MtGroupId=@MtGroupId  and (goalId=@goalId or  GoalText=@GoalText)
                                else 
                                    insert into TabAssertAndMainMtGoal(GoalText,GoalValue,Unit,GoalLevelValue,GoalPipLevel,MtGroupId,createTime,domain)
                                            VALUES(@GoalText,@GoalValue,@Unit,@GoalLevelValue,@GoalPipLevel,@MtGroupId,GETDATE(),@domain) ";
            SqlParameter[] sqlParam = new SqlParameter[]{
                new SqlParameter("@goalId",groupGoalModel.GoalId1),
                new SqlParameter("@GoalText",groupGoalModel.GoalText1),
                new SqlParameter("@GoalValue",groupGoalModel.GoalValue1),
                new SqlParameter("@Unit",groupGoalModel.Unit1),
                new SqlParameter("@GoalLevelValue",groupGoalModel.GoalLevelValue1),
                new SqlParameter("@GoalPipLevel",groupGoalModel.GoalPipLevel1),
                new SqlParameter("@MtGroupId",groupGoalModel.MtGroupId1),
                new SqlParameter("@domain",groupGoalModel.Domain),

            };

            return DBHelper.ExecuteNonQuery(sqlText, sqlParam);
            
        }

        public int insertGoalInfo(prdGoalModel prdGoalModel)
        {

            string sqlText = @"if EXISTS(select * from TabprdGoalAndPrd where goalId=@goalId or goalText=@goalText)
	                    UPDATE TabprdGoalAndPrd set goalText=@goalText,goalSocre=@goalSocre,prdId=@prdId,pipLevel=@pipLevel,createTime=GETDATE(),domain=@domain where  goalId=@goalId or goalText=@goalText
                    else
		                insert into TabprdGoalAndPrd(goalText,goalSocre,prdId,pipLevel,createTime,domain) values(@goalText,@goalSocre,@prdId,@pipLevel,GETDATE(),@domain)";

            SqlParameter[] sqlParams = new SqlParameter[]
            {
                new SqlParameter("@goalId",prdGoalModel.GoalId),
                new SqlParameter("@goalText",prdGoalModel.GoalText),
                new SqlParameter("@goalSocre",prdGoalModel.GoalSocre),
                new SqlParameter("@prdId",prdGoalModel.PrdId),
                new SqlParameter("@pipLevel",prdGoalModel.GoalPipLevel),
                new SqlParameter("@domain",prdGoalModel.Domain)

            };
            return DBHelper.ExecuteNonQuery(sqlText,sqlParams);
        }

        //插入供应指标信息
        public int insertIndicatorInfo(PrdIndicatorModel prdIndicatorModel)
        {
            int count;

            string sqlText = @"IF EXISTS ( SELECT * FROM TabIndicator WHERE (IndicatorId =@IndicatorId or IndicatorText =@IndicatorText) AND GoalId =@GoalId AND MaterialId =@MaterialId ) UPDATE TabIndicator 
                                        SET IndicatorText =@IndicatorText,
                                        IndicatorScore =@IndicatorScore,
                                        pipLevel =@pipLevel,
                                        levelValue=@levelValue,
                                        Unit=@Unit,
                                        domain=@domain
                                        WHERE
	                                        (IndicatorId =@IndicatorId or IndicatorText =@IndicatorText)     
	                                        AND GoalId =@GoalId 
	                                        AND MaterialId =@MaterialId 
	                                        ELSE INSERT INTO TabIndicator ( IndicatorText, IndicatorScore, GoalId, MaterialId, pipLevel,levelValue,Unit,domain)
                                        VALUES
	                                        (
	                                        @IndicatorText,@IndicatorScore,@GoalId,@MaterialId,@pipLevel,@levelValue,@Unit,@domain)";
            SqlParameter[] sqlParams = new SqlParameter[]
            {
                new SqlParameter("@IndicatorId",prdIndicatorModel.IndicatorId),
                new SqlParameter("@IndicatorText",prdIndicatorModel.IndicatorText),
                new SqlParameter("@IndicatorScore",prdIndicatorModel.IndicatorScore),
                new SqlParameter("@GoalId",prdIndicatorModel.GoalId),
                new SqlParameter("@MaterialId",prdIndicatorModel.MaterialId),
                new SqlParameter("@pipLevel",prdIndicatorModel.PipLevel),
                new SqlParameter("@levelValue",prdIndicatorModel.LevelValue),
                new SqlParameter("@Unit",prdIndicatorModel.Unit1),
                new SqlParameter("@domain",prdIndicatorModel.Domain)

            };
            try
            {
               count= DBHelper.ExecuteNonQuery(sqlText, sqlParams);
            }
            catch (Exception ex)
            {

                return 0;
            }

            return count;
        }

        /// <summary>
        /// 插入产品信息
        /// </summary>
        /// <param name="prdModel"></param>
        /// <returns></returns>
        public bool insertPrdInfo(PrdModel prdModel)
        {
            string sql = @" if  NOT EXISTS (select * from TabPrdInfo where prdId=@prdId )
                                INSERT into TabPrdInfo(prdId, prdName) VALUES(@prdId, @prdName) ";

            SqlParameter[] sqlParameters = new SqlParameter[] {
                new SqlParameter("@prdId",prdModel.PrdId),
                new SqlParameter("@prdName",prdModel.PrdName)
            };
            try
            {
                DBHelper.ExecuteNonQuery(sql, sqlParameters);
            }
            catch (Exception ex)
            {

                return false;
            }

            return true;
        }

        public bool insertPrdInfoAndMaterial(PrdModel prdModel)
        {
            string sql = @" if  NOT EXISTS (select * from TabPrdAndMaterial where prdId=@prdId and materialId=@materialId)
                                    INSERT into TabPrdAndMaterial(prdId, materialId) VALUES(@prdId, @materialId) ";


            SqlParameter[] sqlParameters = new SqlParameter[] {
                new SqlParameter("@prdId",prdModel.PrdId),
                new SqlParameter("@materialId",prdModel.MaterialId)
            };
            try
            {
                DBHelper.ExecuteNonQuery(sql, sqlParameters);

            }
            catch (Exception ex)
            {

                return false;
            }
            return true;
        }

      
    }
}
