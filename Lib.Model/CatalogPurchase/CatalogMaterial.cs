﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace Lib.Model.CatalogPurchase
{
    public class CatalogMaterial
    {
        /// <summary>
        /// 唯一性编号
        /// </summary>
        public string ID { get; set; }
        
        /// <summary>
        /// 物料编号
        /// </summary>
        public string Material_ID { get; set; }

        /// <summary>
        /// 供应商编号
        /// </summary>
        public string Supplier_ID { get; set; }

        /// <summary>
        /// 物料名称
        /// </summary>
        public string Material_Name { get; set; }

        /// <summary>
        /// 供应商名称
        /// </summary>
        public string Supplier_Name { get; set; }

        /// <summary>
        /// 物料类型
        /// </summary>
        public string Material_Type { get; set; }

        /// <summary>
        /// 物料标准
        /// </summary>
        public string Material_Standard { get; set; }

        /// <summary>
        /// 短文本
        /// </summary>
        public string Short_Text { get; set; }

        /// <summary>
        /// 价格
        /// </summary>
        public double Price { get; set; }

        /// <summary>
        /// 计量单位
        /// </summary>
        public string Material_Unit { get; set; }

        /// <summary>
        /// 货币类型
        /// </summary>
        public string Currency_Type { get; set; }

        /// <summary>
        /// 自助采购协议编号
        /// </summary>
        public string Catalog_Purchase_Contract_ID { get; set; }

        /// <summary>
        /// 详细描述
        /// </summary>
        public string Detail { get; set; }

        /// <summary>
        /// 图片
        /// </summary>
        public Bitmap Image { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime Create_Time { get; set; }

        /// <summary>
        /// 失效时间
        /// </summary>
        public DateTime End_Time { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        public string Status { get; set; }
    }
}
