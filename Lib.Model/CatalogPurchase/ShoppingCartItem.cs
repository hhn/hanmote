﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.CatalogPurchase
{
    public class ShoppingCartItem
    {
        /// <summary>
        /// 购物车编号
        /// </summary>
        public string Shopping_Cart_ID { get; set; }

        /// <summary>
        /// 自助采购物料编号
        /// </summary>
        public string Material_Catalog_ID { get; set; }

        /// <summary>
        /// 采购总数量
        /// </summary>
        public double Total_Quantity { get; set; }

        /// <summary>
        /// 净价
        /// </summary>
        public double Net_Price { get; set; }

        /// <summary>
        /// 采购总金额
        /// </summary>
        public double Total_Amount { get; set; }

        /// <summary>
        /// 货币类型
        /// </summary>
        public string Currency { get; set; }

        /// <summary>
        /// 物料名称
        /// </summary>
        public string Material_Name { get; set; }

        /// <summary>
        /// 供应商编号
        /// </summary>
        public string Supplier_ID { get; set; }

        /// <summary>
        /// 物料状态
        /// </summary>
        public string Is_Valid { get; set; }
    }
}
