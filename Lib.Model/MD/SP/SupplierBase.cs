﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.MD.SP
{
   public class SupplierBase
    {
       /// <summary>
       /// 流水码
       /// </summary>
        public int ID { get; set; }
       /// <summary>
       /// 供应商编码
       /// </summary>
        public string Supplier_ID { get; set; }
       /// <summary>
       /// 供应商账户组
       /// </summary>
        public string Supplier_Account { get; set; }
       /// <summary>
       /// 供应商名称
       /// </summary>
        public string Supplier_Name { get; set; }
       /// <summary>
       /// 供应商标题
       /// </summary>
        public string Supplier_Title { get; set; }
       /// <summary>
       /// 所在国家
       /// </summary>
        public string Nation { get; set; }
       /// <summary>
       /// 街道
       /// </summary>
        public string Address { get; set; }
       /// <summary>
       /// 门牌号
       /// </summary>
        public string House_Number { get; set; }
       /// <summary>
       /// 街道邮政编码
       /// </summary>
        public string Street_postalCode { get; set; }
       /// <summary>
       /// 城市
       /// </summary>
        public string City { get; set; }
       /// <summary>
       /// 地区
       /// </summary>
        public string Region { get; set; }
       /// <summary>
        /// 邮政信箱
       /// </summary>
        public string Post_OfficeBox { get; set; }
       /// <summary>
        /// 公司邮政编码
       /// </summary>
        public string Company_ZipCode { get; set; }
       /// <summary>
       /// 语言
       /// </summary>
        public string Language { get; set; }
       /// <summary>
       /// 供应商原名称
       /// </summary>
        public string Supplier_OName { get; set; }
       /// <summary>
        /// 邮政编码
       /// </summary>
        public string Zip_Code { get; set; }
       /// <summary>
       /// 传真
       /// </summary>
        public string Fax { get; set; }
       /// <summary>
       /// 移动电话1
       /// </summary>
        public string Mobile_Phone1 { get; set; }
       /// <summary>
       /// 移动电话2
       /// </summary>
        public string Mobile_Phone2 { get; set; }
       /// <summary>
       /// 客户
       /// </summary>
        public string Client { get; set; }
       /// <summary>
        /// 贸易伙伴
       /// </summary>
        public string Trade_Partner { get; set; }
       /// <summary>
       /// 权限组
       /// </summary>
        public string Permissions_Set { get; set; }
       /// <summary>
       /// 组代码
       /// </summary>
        public string Group_Code { get; set; }
       /// <summary>
       /// 联系电话1
       /// </summary>
        public string Contact_phone1 { get; set; }
       /// <summary>
       /// 联系电话2
       /// </summary>
        public string Contact_phone2 { get; set; }
       /// <summary>
       /// 电子邮箱
       /// </summary>
        public string Email { get; set; }
       /// <summary>
       /// 税号1
       /// </summary>
        public string Tax_Code1 { get; set; }
       /// <summary>
       /// 税号2
       /// </summary>
        public string Tax_Code2 { get; set; }
       /// <summary>
       /// 税号3
       /// </summary>
        public string Tax_Code3 { get; set; }
       /// <summary>
       /// 税号4
       /// </summary>
        public string Tax_Code4 { get; set; }
       /// <summary>
       /// 税号类型
       /// </summary>
        public string EIN_Type { get; set; }
       /// <summary>
       /// 税类型
       /// </summary>
        public string Tax_Type { get; set; }
       /// <summary>
       /// 财务地址
       /// </summary>
        public string Financial_Address { get; set; }
       /// <summary>
       /// 地区税务代码
       /// </summary>
        public string Area_TaxCode { get; set; }
       /// <summary>
       /// 代表名称
       /// </summary>
        public string Representor_Name { get; set; }
       /// <summary>
       /// 税务局负责
       /// </summary>
        public string Tax_Brurau { get; set; }
       /// <summary>
       /// 税号
       /// </summary>
        public string Tax_Code { get; set; }
       /// <summary>
       /// 计税基数
       /// </summary>
        public string Tax_Base { get; set; }
       /// <summary>
       /// 社会保险代码
       /// </summary>
        public string Social_CnsuranceCode { get; set; }
       /// <summary>
       /// 增值税登记号
       /// </summary>
        public string VAT_Registration_Number { get; set; }
       /// <summary>
       /// 业务类型
       /// </summary>
        public string Business_Type { get; set; }
       /// <summary>
       /// 工业类型
       /// </summary>
        public string Industry_Type { get; set; }
       /// <summary>
       /// 国际区位码1
       /// </summary>
        public string International_AreaCode1 { get; set; }
       /// <summary>
       /// 国际区位码2
       /// </summary>
        public string International_AreaCode2 { get; set; }
       /// <summary>
       /// 信用信息吗
       /// </summary>
        public string Credit_Information { get; set; }
       /// <summary>
       /// 行业
       /// </summary>
        public string Industry { get; set; }
       /// <summary>
       /// SCAC码
       /// </summary>
        public string SCAC_Cod { get; set; }
       /// <summary>
        /// 运输区域
       /// </summary>
        public string Transport_Area { get; set; }
       /// <summary>
        /// PCO相关
       /// </summary>
        public string PCO_Code { get; set; }
       /// <summary>
       /// 供应商分类
       /// </summary>
        public string Supplier_Class { get; set; }





    }
}
