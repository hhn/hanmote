﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.MD.SP
{
    public class Number_segement
    {
        //号码段ID
        public string Number_ID { get; set; }
        //号码段描述
        public string Description { get; set; }
        //内外部标识
        public string Status { get; set; }
        //开始号码段
        public string NS_Begin { get; set; }
        //结束号码段
        public string NS_End { get; set; }
        //号码段范围状态
        public string NS_status { get; set; }
    }
}
