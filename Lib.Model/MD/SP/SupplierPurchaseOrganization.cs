﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.MD.SP
{
   public class SupplierPurchaseOrganization
    {
       /// <summary>
       /// 流水码
       /// </summary>
        public int ID { get; set; }
       /// <summary>
        /// 供应商编码（主键）
       /// </summary>
        public string Supplier_ID { get; set; }
       /// <summary>
       /// 供应商名称
       /// </summary>
        public string Supplier_Name { get; set; }
       /// <summary>
        /// 采购组织编码（主键）
       /// </summary>
        public string PurchasingORG_ID { get; set; }
       /// <summary>
       /// 采购组织名称
       /// </summary>
        public string PurchasingORG_Name { get; set; }
       /// <summary>
       /// 订单单位
       /// </summary>
        public string Order_Units { get; set; }
       /// <summary>
       /// 付款条件
       /// </summary>
        public string Payment_Clause { get; set; }
       /// <summary>
       /// 收付条件
       /// </summary>
        public string Trade_Clause { get; set; }
       /// <summary>
       /// 最小订单值
       /// </summary>
        public string MIN_OrderValue { get; set; }
       /// <summary>
       /// 方案组(供应商)
       /// </summary>
        public string Supplier_Group { get; set; }
       /// <summary>
       /// 定价日期控制
       /// </summary>
        public string PricingDate_Control { get; set; }
       /// <summary>
       /// 订单优化限制
       /// </summary>
        public string Order_Limit { get; set; }
       /// <summary>
       /// 供应商累计
       /// </summary>
        public string Supplier_Has { get; set; }
       /// <summary>
        /// 基于收货的发票验证
       /// </summary>
        public bool Invoice_Verification { get; set; }
       /// <summary>
        /// 自动评估GR结算交货
       /// </summary>
        public bool Delivery_Settlement { get; set; }
       /// <summary>
        /// 自动评估GR结算保留
       /// </summary>
        public bool Settlement_Reserves { get; set; }
       /// <summary>
       /// 回执需求
       /// </summary>
        public bool ReturnReceipt_Requirements { get; set; }
       /// <summary>
        /// 自动建立采购订单
       /// </summary>
        public bool Automatic_PurchaseOrder  { get; set; }
       /// <summary>
        /// 后续结算
       /// </summary>
        public bool Subsequent_Settlement { get; set; }
       /// <summary>
        /// 后续结算索引
       /// </summary>
        public bool Settlement_Index { get; set; }
       /// <summary>
        /// 所需业务量比较/协议
       /// </summary>
        public bool Business_Comparison { get; set; }
       /// <summary>
        /// 单据索引有效的
       /// </summary>
        public bool Document_Indexin { get; set; }
       /// <summary>
        /// 退货供应商
       /// </summary>
        public bool Return_Supplier { get; set; }
       /// <summary>
        /// 基于服务的开票校验
       /// </summary>
        public bool Service_Verification { get; set; }
       /// <summary>
        /// 允许重新评估
       /// </summary>
        public bool Allow_Reevaluation { get; set; }
       /// <summary>
        /// 准许折扣
       /// </summary>
        public bool Allow_Discount { get; set; }
       /// <summary>
        /// 相关价格确定
       /// </summary>
        public bool Price_Determination{ get; set; }
       /// <summary>
        /// 相关代理业务
       /// </summary>
        public bool Agent_Service { get; set; }
       /// <summary>
        /// 装运条件
       /// </summary>
        public string Shipment_Terms { get; set; }
       /// <summary>
        /// ABC标识
       /// </summary>
        public string ABC_Identify { get; set; }
       /// <summary>
        /// 运输方式
       /// </summary>
        public string Transportation_Mode { get; set; }
       /// <summary>
        /// 输入管理处
       /// </summary>
        public string Input_ManagementOffice { get; set; }
       /// <summary>
        /// 排序标准
       /// </summary>
        public string Sort_Criteria { get; set; }
       /// <summary>
        /// PROACT控制参数文件
       /// </summary>
        public string PRO_ControlData { get; set; }
       /// <summary>
       /// 采购组
       /// </summary>
        public string Buyer_Group { get; set; }
       /// <summary>
        /// 计划交货时间
       /// </summary>
        public DateTime Delivery_Time { get; set; }
       /// <summary>
        /// 确认控制
       /// </summary>
        public string Control_Confirm { get; set; }
       /// <summary>
        /// 计量单位组
       /// </summary>
        public string Measurment_Group { get; set; }
       /// <summary>
        /// 舍入参数文件
       /// </summary>
        public string Rounding_Parameters { get; set; }
       /// <summary>
        /// 价格标记协议
       /// </summary>
        public string Price_TagAgreement { get; set; }
       /// <summary>
        /// 展示商品服务协议
       /// </summary>
        public bool Services_Agreement { get; set; }
       /// <summary>
        /// 按供应商的订单输入项
       /// </summary>
        public bool Order_Entry { get; set; }
       /// <summary>
        /// 服务等级
       /// </summary>
        public string Service_Grade { get; set; }




    }
}
