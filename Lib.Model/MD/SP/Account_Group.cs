﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.MD.SP
{
   public class Account_Group
    {
        //账户组ID
        public string Account_Group_ID { get; set; }

        //账户组描述
        public string Description { get; set; }

        //一次性供应商标识
        public string SingleStatus { get; set; }

        //号码段表ID
        public string NS_id { get; set; }

        //号码段详细信息
        public Number_segement Number_Segement { get; set; }

    }
}
