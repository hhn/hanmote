﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.MD.MT
{
   public class MaterialFactory
    {
        public string qualityClass;

        /// <summary>
        /// 标识符
        /// </summary>
        public int ID { get; set; }
       /// <summary>
       /// 物料编码
       /// </summary>
        public string Material_ID { get; set; }
       /// <summary>
       /// 物料名称
       /// </summary>
        public string Material_Name { get; set; }
       /// <summary>
       /// 工厂编码
       /// </summary>
        public string Factory_ID { get; set; }
       /// <summary>
       /// 工厂名称
       /// </summary>
        public string Factory_Name { get; set; }
       /// <summary>
       /// 采购组
       /// </summary>
        public string  Buyer_Group { get; set; }
       /// <summary>
       /// 工厂物料状态
       /// </summary>
        public string Ptmtl_Status { get; set; }
       /// <summary>
       /// 物料税收状态
       /// </summary>
        public string MTtax_Status { get; set; }
       /// <summary>
        /// 物料运输组
       /// </summary>
        public string MTtransport_Group { get; set; }
       /// <summary>
       /// 货币单位
       /// </summary>
        public string Currency_Unit { get; set; }
       /// <summary>
       /// 评估类别
       /// </summary>
        public string Assessment_Category { get; set; }
       /// <summary>
       /// 评估类
       /// </summary>
        public string Evaluation_Class { get; set; }
       /// <summary>
       /// 移动平均价
       /// </summary>
        public float Moving_AGPrice { get; set; }
       /// <summary>
       /// 总库存
       /// </summary>
        public int Gross_Inventory { get; set; }
       /// <summary>
       /// 价格单位
       /// </summary>
        public string Price_Unit { get; set; }
       /// <summary>
       /// 标准价格
       /// </summary>
        public float Normal_Price { get; set; }
       /// <summary>
       /// 总价值
       /// </summary>
        public float TotalValue { get; set; }
       /// <summary>
       /// 未来价格
       /// </summary>
        public float Future_Price { get; set; }
       /// <summary>
       /// 当前价格
       /// </summary>
        public float Current_Price { get; set; }
       /// <summary>
       /// 上期价格
       /// </summary>
        public float Previous_Price { get; set; }
       /// <summary>
       /// 周期盘点标识
       /// </summary>
        public bool Check_Mark { get; set; }
       /// <summary>
       /// 发货单位
       /// </summary>
        public string Delivery_Unit { get; set; }
      /// <summary>
      /// 最大仓储时间
      /// </summary>
        public float MAX_STPeriod { get; set; }
       /// <summary>
       /// 时间单位
       /// </summary>
        public string Hourly_Basis { get; set; }

       /// <summary>
       /// 价格控制
       /// </summary>
        public string Price_Control { get; set; }
       /// <summary>
       /// 源清单标识
       /// </summary>
        public bool Source_List { get; set; }
    }
}
