﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.MD.MT
{
    /// <summary>
    /// 绩效评估模板类
    /// </summary>
    public class PerformanceModel
    {
        //模板创建者编号
        private string creator;
        //物料类型编号
        private string MaterialType;
        //主标准
        private MainEvalFactor MainEvalFactor;
        //次标准
        private SubEvalFactor SubEvalFactor;
        //评估类型
        private string modelType;
        public string MaterialType1 { get => MaterialType; set => MaterialType = value; }
        public MainEvalFactor MainEvalFactor1 { get => MainEvalFactor; set => MainEvalFactor = value; }
        public SubEvalFactor SubEvalFactor1 { get => SubEvalFactor; set => SubEvalFactor = value; }
        public string ModelType { get => modelType; set => modelType = value; }
        public string Creator { get => creator; set => creator = value; }
    }
}
