﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.MD.MT
{
   public class Batch_Feature
    {
        /// <summary>
        /// 特性名称
        /// </summary>
        public string  Feature_Name { get; set; }
        /// <summary>
        /// 特性值
        /// </summary>
        public string Feature_Value { get; set; }
    }
}
