﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.MD.MT
{
   public class MaterialStorage
    {
       /// <summary>
       /// 物料编码
       /// </summary>
        public string Material_ID { get; set; }
       /// <summary>
       /// 物料名称
       /// </summary>
        public string Material_Name { get; set; }
       /// <summary>
       /// 工厂编码
       /// </summary>
        public string Factory_ID { get; set; }
       /// <summary>
       /// 工厂名称
       /// </summary>
        public string Factory_Name { get; set; }
       /// <summary>
       /// 仓库编码
       /// </summary>
        public String Stock_ID { get; set; }
       /// <summary>
       /// 仓库名称
       /// </summary>
        public string Stock_Name { get; set; }
       /// <summary>
       /// 周期盘点标识
       /// </summary>
        public bool Check_Mark { get; set; }
       /// <summary>
       /// 发货单位
       /// </summary>
        public string Delivery_Unit { get; set; }
       /// <summary>
       /// 最大仓储时间
       /// </summary>
        public string MAX_STPeriod { get; set; }
       /// <summary>
       /// 时间单位 
       /// </summary>
        public string Hourly_Basis { get; set; }
       /// <summary>
       /// 库存仓位
       /// </summary>
        public string Inventory_Position { get; set; }
       /// <summary>
       /// 领货范围
       /// </summary>
        public string Collar_Range { get; set; }

    }
}
