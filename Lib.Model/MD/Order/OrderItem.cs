﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.MD.Order
{
   public class OrderItem
    {
        /// <summary>
        /// 订单编码
        /// </summary>
        public string Order_ID { get; set; }
        /// <summary>
        /// 物料编码
        /// </summary>
        public string Material_ID { get; set; }
        /// <summary>
        /// 工厂编码
        /// </summary>
        public string Factory_ID { get; set; }
        /// <summary>
        /// 仓库编码
        /// </summary>
        public string Stock_ID { get; set; }
        /// <summary>
        /// 物料组
        /// </summary>
        public string Material_Group { get; set; }
        /// <summary>
        /// 净价
        /// </summary>
        public float Net_Price { get; set; }
        /// <summary>
        /// 数量
        /// </summary>
        public int Number { get; set; }
        /// <summary>
        /// 单位
        /// </summary>
        public string Unit { get; set; }
        /// <summary>
        /// 总价
        /// </summary>
        public float Total_Price { get; set; }
        /// <summary>
        /// 批次编码
        /// </summary>
        public string Batch_ID { get; set; }
        /// <summary>
        /// 货源确定号
        /// </summary>
        public string PR_ID { get; set; }
       /// <summary>
       /// 交货时间
       /// </summary>
        public DateTime Delivery_Time { get; set; }
        /// <summary>
        /// 信息记录类型
        /// </summary>
        public string Info_Type { get; set; }
        /// <summary>
        /// 记录编码
        /// </summary>
        public string Info_Number { get; set; }
       /// <summary>
       /// 采购申请编号
       /// </summary>
        public string Purchase_ID { get; set; }

    }
}
