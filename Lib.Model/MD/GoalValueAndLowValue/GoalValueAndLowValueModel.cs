﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model
{
    public class GoalValueAndLowValueModel
    {
        //物料组认证绿色目标值
        private string cGreenValue;
        //物料组认证最低目标值
        private string cLowValue;
        //物料组绩效绿色目标值
        private string pGreenValue;
        //物料组绩效最低目标值
        private string pLowValue;

        public string CGreenValue { get => cGreenValue; set => cGreenValue = value; }
        public string CLowValue { get => cLowValue; set => cLowValue = value; }
        public string PGreenValue { get => pGreenValue; set => pGreenValue = value; }
        public string PLowValue { get => pLowValue; set => pLowValue = value; }
    }
}
