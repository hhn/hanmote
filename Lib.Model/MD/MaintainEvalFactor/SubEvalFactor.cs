﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.MD
{
    /// <summary>
    /// 主标准
    /// </summary>
    public class SubEvalFactor
    {
        #region 属性
        //主标准代码
        private string mainEvalCode;
        //次标准代码
        private string subCode;
        //名称
        private string desc;
        //权重
        private string rate;
        #endregion


        #region 方法

        public string Desc { get => desc; set => desc = value; }
        public string MainEvalCode { get => mainEvalCode; set => mainEvalCode = value; }
        public string SubCode { get => subCode; set => subCode = value; }
        public string Rate { get => rate; set => rate = value; }
        #endregion
    }
}
