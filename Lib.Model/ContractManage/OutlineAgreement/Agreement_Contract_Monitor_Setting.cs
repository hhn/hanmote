﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.ContractManage.OutlineAgreement
{
    public class Agreement_Contract_Monitor_Setting
    {
        /// <summary>
        /// 协议合同ID
        /// </summary>
        public string Agreement_Contract_ID { get; set; }

        /// <summary>
        /// 协议合同版本号
        /// </summary>
        public int Agreement_Contract_Version { get; set; }

        /// <summary>
        /// 协议合同类型
        /// </summary>
        public string Agreement_Contract_Type { get; set; }

        /// <summary>
        /// 是否开启总价值预警
        /// </summary>
        public bool Enable_Alert_Value { get; set; }

        /// <summary>
        /// 预警价值
        /// </summary>
        public double Alert_Value { get; set; }

        /// <summary>
        /// 是否开启总数量预警
        /// </summary>
        public bool Enable_Alert_Quantity { get; set; }

        /// <summary>
        /// 预警数量
        /// </summary>
        public double Alert_Quantity { get; set; }

        /// <summary>
        /// 是否开启日期预警
        /// </summary>
        public bool Enable_Alert_DateTime { get; set; }

        /// <summary>
        /// 预警日期
        /// </summary>
        public DateTime Alert_DateTime { get; set; }
    }
}
