﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.ContractManage.OutlineAgreement
{
    public class Scheduling_Agreement
    {

        private string scheduling_Agreement_ID;
        /// <summary>
        /// 协议合同ID
        /// </summary>
        public string Scheduling_Agreement_ID
        {
            get { return scheduling_Agreement_ID; }
            set { scheduling_Agreement_ID = value; }
        }

        private string supplier_ID;
        /// <summary>
        /// 供应商编号
        /// </summary>
        public string Supplier_ID
        {
            get { return supplier_ID; }
            set { supplier_ID = value; }
        }

        /// <summary>
        /// 供应商名称
        /// </summary>
        public string Supplier_Name { get; set; }

        private string scheduling_Agreement_Type;
        /// <summary>
        /// 计划协议类型(LP、LPA)
        /// </summary>
        public string Scheduling_Agreement_Type
        {
            get { return scheduling_Agreement_Type; }
            set { scheduling_Agreement_Type = value; }
        }

        private string purchase_Group;
        /// <summary>
        /// 采购组
        /// </summary>
        public string Purchase_Group
        {
            get { return purchase_Group; }
            set { purchase_Group = value; }
        }

        private string purchase_Organization;
        /// <summary>
        /// 采购组织
        /// </summary>
        public string Purchase_Organization
        {
            get { return purchase_Organization; }
            set { purchase_Organization = value; }
        }

        private string creater_ID;
        /// <summary>
        /// 创建者ID
        /// </summary>
        public string Creater_ID
        {
            get { return creater_ID; }
            set { creater_ID = value; }
        }

        private DateTime create_Time;
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime Create_Time
        {
            get { return create_Time; }
            set { create_Time = value; }
        }

        private string status;
        /// <summary>
        /// 协议状态(未审核、已审核、执行完成)
        /// </summary>
        public string Status
        {
            get { return status; }
            set { status = value; }
        }

        private string factory;
        /// <summary>
        /// 工厂编号
        /// </summary>
        public string Factory
        {
            get { return factory; }
            set { factory = value; }
        }

        private string storage_Location;
        /// <summary>
        /// 库存位置
        /// </summary>
        public string Storage_Location
        {
            get { return storage_Location; }
            set { storage_Location = value; }
        }

        private DateTime begin_Time;
        /// <summary>
        /// 有效开始时间
        /// </summary>
        public DateTime Begin_Time
        {
            get { return begin_Time; }
            set { begin_Time = value; }
        }

        private DateTime end_Time;
        /// <summary>
        /// 有效结束时间
        /// </summary>
        public DateTime End_Time
        {
            get { return end_Time; }
            set { end_Time = value; }
        }

        private string payment_Type;
        /// <summary>
        /// 付款条件
        /// </summary>
        public string Payment_Type
        {
            get { return payment_Type; }
            set { payment_Type = value; }
        }

        private int days1;
        /// <summary>
        /// 天数1
        /// </summary>
        public int Days1
        {
            get { return days1; }
            set { days1 = value; }
        }

        private double discount_Rate1;
        /// <summary>
        /// 天数1前的折扣比
        /// </summary>
        public double Discount_Rate1
        {
            get { return discount_Rate1; }
            set { discount_Rate1 = value; }
        }

        private int days2;
        /// <summary>
        /// 天数2
        /// </summary>
        public int Days2
        {
            get { return days2; }
            set { days2 = value; }
        }

        private double discount_Rate2;
        /// <summary>
        /// 天数2前的折扣比
        /// </summary>
        public double Discount_Rate2
        {
            get { return discount_Rate2; }
            set { discount_Rate2 = value; }
        }

        private int days3;
        /// <summary>
        /// days3前付款净额
        /// </summary>
        public int Days3
        {
            get { return days3; }
            set { days3 = value; }
        }

        private string iT_Term_Code;
        /// <summary>
        /// 国贸术语编号
        /// </summary>
        public string IT_Term_Code
        {
            get { return iT_Term_Code; }
            set { iT_Term_Code = value; }
        }

        private string iT_Term_Text;
        /// <summary>
        /// 国贸术语文本
        /// </summary>
        public string IT_Term_Text
        {
            get { return iT_Term_Text; }
            set { iT_Term_Text = value; }
        }

        private double target_Value;
        /// <summary>
        /// 目标值
        /// </summary>
        public double Target_Value
        {
            get { return target_Value; }
            set { target_Value = value; }
        }

        private string currency_Type;
        /// <summary>
        /// 货币类型
        /// </summary>
        public string Currency_Type
        {
            get { return currency_Type; }
            set { currency_Type = value; }
        }

        private double exchange_Rate;
        /// <summary>
        /// 汇率(暂时不考虑)
        /// </summary>
        public double Exchange_Rate
        {
            get { return exchange_Rate; }
            set { exchange_Rate = value; }
        }
    }
}
