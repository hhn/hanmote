﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.ContractManage.OutlineAgreement
{
    public class Outline_Agreement_Item
    {
        private string outline_Agreement_ID;
        ///<summary>
        ///框架协议id
        ///</summary>
        public string Outline_Agreement_ID
        {
            get { return outline_Agreement_ID; }
            set { outline_Agreement_ID = value; }
        }

        private string material_ID;
        /// <summary>
        /// 物料编号
        /// </summary>
        public string Material_ID
        {
            get { return material_ID; }
            set { material_ID = value; }
        }

        private string material_Name;
        /// <summary>
        /// 物料名称
        /// </summary>
        public string Material_Name
        {
            get { return material_Name; }
            set { material_Name = value; }
        }

        private string demand_Count;
        /// <summary>
        /// 需求数量
        /// </summary>
        public string Demand_Count
        {
            get { return demand_Count; }
            set { demand_Count = value; }
        }

        private string net_Price;
        /// <summary>
        /// 单价
        /// </summary>
        public string Net_Price
        {
            get { return net_Price; }
            set { net_Price = value; }
        }

        private string factory_ID;
        /// <summary>
        /// 工厂编号
        /// </summary>
        public string Factory_ID
        {
            get { return factory_ID; }
            set { factory_ID = value; }
        }

        private string stock_ID;
        /// <summary>
        /// 库存地编号
        /// </summary>
        public string Stock_ID
        {
            get { return stock_ID; }
            set { stock_ID = value; }
        }

        private string price_Determine_ID;
        /// <summary>
        /// 价格确定编号
        /// </summary>
        public string Price_Determine_ID
        {
            get { return price_Determine_ID; }
            set { price_Determine_ID = value; }
        }
    }
}
