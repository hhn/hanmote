﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.ContractManage.ContractDocs
{
    public class Contract_Template
    {
        /// <summary>
        /// 合同模板编号
        /// </summary>
        private string iD;

        public string ID
        {
            get { return iD; }
            set { iD = value; }
        }
        
        /// <summary>
        /// 合同模板名称
        /// </summary>
        private string name;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        /// <summary>
        /// 合同模板状态
        /// </summary>
        private string status;

        public string Status
        {
            get { return status; }
            set { status = value; }
        }

        /// <summary>
        /// 合同模板创建时间
        /// </summary>
        private DateTime create_Time;

        public DateTime Create_Time
        {
            get { return create_Time; }
            set { create_Time = value; }
        }
    }
}
