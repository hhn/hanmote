﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.ContractManage.ContractDocs
{
    public class ContractText
    {
        private string contractID;
        /// <summary>
        /// 合同文本ID
        /// </summary>
        public string ContractID
        {
            get { return contractID; }
            set { contractID = value; }
        }

        private string contractName;
        /// <summary>
        /// 合同名称
        /// </summary>
        public string ContractName
        {
            get { return contractName; }
            set { contractName = value; }
        }

        private string supplierID;
        /// <summary>
        /// 供应商ID
        /// </summary>
        public string SupplierID
        {
            get { return supplierID; }
            set { supplierID = value; }
        }

        private string createrID;
        /// <summary>
        /// 创建者ID
        /// </summary>
        public string CreaterID
        {
            get { return createrID; }
            set { createrID = value; }
        }

        private string contractType;
        /// <summary>
        /// 合同类型
        /// 现货、定期、无定额、定额
        /// </summary>
        public string ContractType
        {
            get { return contractType; }
            set { contractType = value; }
        }

        private string status;
        /// <summary>
        /// 合同状态
        /// </summary>
        public string Status
        {
            get { return status; }
            set { status = value; }
        }

        private string fileLocation;
        /// <summary>
        /// 合同文本的位置
        /// </summary>
        public string FileLocation
        {
            get { return fileLocation; }
            set { fileLocation = value; }
        }

        private string createMode;
        /// <summary>
        /// 合同的生成类型(直接上传、手工创建)
        /// </summary>
        public string CreateMode
        {
            get { return createMode; }
            set { createMode = value; }
        }

        private DateTime createTime;
        /// <summary>
        /// 合同文本创建时间
        /// </summary>
        public DateTime CreateTime
        {
            get { return createTime; }
            set { createTime = value; }
        }

        private DateTime beginTime;
        /// <summary>
        /// 合同开始时间
        /// </summary>
        public DateTime BeginTime
        {
            get { return beginTime; }
            set { beginTime = value; }
        }

        private DateTime endTime;
        /// <summary>
        /// 合同结束时间
        /// </summary>
        public DateTime EndTime
        {
            get { return endTime; }
            set { endTime = value; }
        }
    }
}
