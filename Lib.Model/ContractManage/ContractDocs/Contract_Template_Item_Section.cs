﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.ContractManage.ContractDocs
{
    public class Contract_Template_Item_Section
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string name;

        /// <summary>
        /// 类型(章节和条款)
        /// </summary>
        public string type;

        /// <summary>
        /// 说明
        /// </summary>
        public string detail;

        /// <summary>
        /// 自定义注释
        /// </summary>
        public string annotation;
    }
}
