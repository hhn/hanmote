﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.ContractManage.ContractDocs
{
    public class Contract_Template_Section
    {
        /// <summary>
        /// 序号
        /// </summary>
        public int Order_Number;

        /// <summary>
        /// 编号(Name_Contract_Template_ID)作为主键
        /// </summary>
        public string ID;

        /// <summary>
        /// 合同模板编号
        /// </summary>
        public string Contract_Template_ID;

        /// <summary>
        /// 名称
        /// </summary>
        public string Name;

        /// <summary>
        /// 说明
        /// </summary>
        public string Detail;

        /// <summary>
        /// 自定义注释
        /// </summary>
        public string Annotation;
    }
}
