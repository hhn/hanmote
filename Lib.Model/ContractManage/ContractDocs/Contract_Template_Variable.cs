﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.ContractManage.ContractDocs
{
    public class Contract_Template_Variable
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name;

        /// <summary>
        /// 对应的合同模板条款Sample的ID
        /// </summary>
        public string Contract_Template_Term_Sample_ID;
        
        /// <summary>
        /// 序号
        /// </summary>
        public int Order_Number;
        
        /// <summary>
        /// 类别(只有文本框、时间选择框)
        /// </summary>
        public string Type;

        /// <summary>
        /// 令牌文本
        /// </summary>
        public string Token_Text;

        /// <summary>
        /// 说明
        /// </summary>
        public string Detail;
    }
}
