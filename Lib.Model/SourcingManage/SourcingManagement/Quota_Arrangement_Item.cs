﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.SourcingManage.SourcingManagement
{
    public class Quota_Arrangement_Item
    {
        /// <summary>
        /// 配额协议编号
        /// </summary>
        public string Quota_Arrangement_ID { get; set; }

        /// <summary>
        /// 项目号
        /// </summary>
        public int Item_Num { get; set; }

        /// <summary>
        /// 物料编号
        /// </summary>
        public string Material_ID { get; set; }

        /// <summary>
        /// 工厂编号
        /// </summary>
        public string Factory_ID { get; set; }

        /// <summary>
        /// P
        /// </summary>
        public string P { get; set; }

        /// <summary>
        /// S
        /// </summary>
        public string S { get; set; }

        /// <summary>
        /// 供应商编号
        /// </summary>
        public string Supplier_ID { get; set; }

        /// <summary>
        /// 生产工厂
        /// </summary>
        public string PP1 { get; set; }

        /// <summary>
        /// 配额
        /// </summary>
        public double Quota_Value { get; set; }

        /// <summary>
        /// 配额比
        /// </summary>
        public double Quota_Percentage { get; set; }

        /// <summary>
        /// 已分配的数量
        /// </summary>
        public double Allocate_Amount { get; set; }

        /// <summary>
        /// 分配给供应商的最大数量
        /// </summary>
        public double Max_Amount { get; set; }

        /// <summary>
        /// 配额基本数量
        /// </summary>
        public double Quota_Base_Amount { get; set; }

        /// <summary>
        /// 最大批量尺寸
        /// </summary>
        public double Max_Batch_Size { get; set; }

        /// <summary>
        /// 最小批量尺寸
        /// </summary>
        public double Min_Batch_Size { get; set; }

        /// <summary>
        /// 最小包装数
        /// </summary>
        public double RPro { get; set; }

        /// <summary>
        /// 是否只能单次分配
        /// </summary>
        public string One_Time { get; set; }

        /// <summary>
        /// 某一时间范围内最大交货数量
        /// </summary>
        public double Max_Delivery_Amount { get; set; }

        /// <summary>
        /// 时间范围
        /// </summary>
        public string Period { get; set; }

        /// <summary>
        /// 优先级
        /// </summary>
        public int Pr { get; set; }

        /// <summary>
        /// 配额计算结果
        /// 该属性只用于配额计算，存储中间结果
        /// </summary>
        public double Quota_Cal_Value { get; set; }
    }
}
