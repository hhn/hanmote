﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.SourcingManage.SourcingManagement
{
    /// <summary>
    /// 询价单表头类
    /// </summary>
    public class SourceInquiry
    {
        private string source_ID;
        /// <summary>
        /// 询价编号
        /// </summary>
        public string Source_ID
        {
            get { return source_ID; }
            set { source_ID = value; }
        }

        private string transaction_Type;
        /// <summary>
        /// 交易类型
        /// </summary>
        public string Transaction_Type
        {
            get { return transaction_Type; }
            set { transaction_Type = value; }
        }

        private DateTime offer_Time;
        /// <summary>
        /// 报价日期
        /// </summary>
        public DateTime Offer_Time
        {
            get { return offer_Time; }
            set { offer_Time = value; }
        }
        

        private int state;
        /// <summary>
        /// 字段1
        /// </summary>
        public int State
        {
            get { return state; }
            set { state = value; }
        }

        private string inquiry_Path;
        /// <summary>
        /// 字段2
        /// </summary>
        public string Inquiry_Path
        {
            get { return inquiry_Path; }
            set { inquiry_Path = value; }
        }

    }
}
