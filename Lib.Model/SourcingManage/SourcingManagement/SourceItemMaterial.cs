﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.SourcingManage
{
    /// <summary>
    /// 寻源子项-物料类
    /// </summary>
    public class SourceItemMaterial
    {

        private string item_ID;
        /// <summary>
        /// 子项ID
        /// </summary>
        public string Item_ID
        {
            get { return item_ID; }
            set { item_ID = value; }
        }

       
        private string supplier_ID;
        /// <summary>
        /// 供应商编号
        /// </summary>
        public string Supplier_ID
        {
            get { return supplier_ID; }
            set { supplier_ID = value; }
        }

        private string material_ID;
        /// <summary>
        /// 物料编号
        /// </summary>
        public string Material_ID
        {
            get { return material_ID; }
            set { material_ID = value; }
        }

        private string material_Name;
        /// <summary>
        /// 物料名称
        /// </summary>
        public string Material_Name
        {
            get { return material_Name; }
            set { material_Name = value; }
        }

        private string material_Group;
        /// <summary>
        /// 物料组
        /// </summary>
        public string Material_Group
        {
            get { return material_Group; }
            set { material_Group = value; }
        }

        private string measurement;
        /// <summary>
        /// 计量单位
        /// </summary>
        public string Measurement
        {
            get { return measurement; }
            set { measurement = value; }
        }

        private float net_Price;
        /// <summary>
        /// 净价
        /// </summary>
        public float Net_Price
        {
            get { return net_Price; }
            set { net_Price = value; }
        }

        private string net_Price_Unit;
        /// <summary>
        /// 净价单位
        /// </summary>
        public string Net_Price_Unit
        {
            get { return net_Price_Unit; }
            set { net_Price_Unit = value; }
        }

        private int buy_Number;
        /// <summary>
        /// 购买数量
        /// </summary>
        public int Buy_Number
        {
            get { return buy_Number; }
            set { buy_Number = value; }
        }

        private int provider_Number;
        /// <summary>
        /// 供应商提供数量
        /// </summary>
        public int Provider_Number
        {
            get { return provider_Number; }
            set { provider_Number = value; }
        }

        private DateTime start_Time;
        /// <summary>
        /// 询价开始时间
        /// </summary>
        public DateTime Start_Time
        {
            get { return Start_Time; }
            set { Start_Time = value; }
        }

        private DateTime end_Time;
        /// <summary>
        /// 报价结束时间
        /// </summary>
        public DateTime End_Time
        {
            get { return End_Time; }
            set { End_Time = value; }
        }
    }
}
