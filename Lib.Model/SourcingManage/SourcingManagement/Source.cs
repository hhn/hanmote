﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.SourcingManage.SourcingManagement
{
    /// <summary>
    /// 寻源类
    /// </summary>
    public class Source
    {
        private string source_ID;
        /// <summary>
        /// 寻源编号
        /// </summary>
        public string Source_ID
        {
            get { return source_ID; }
            set { source_ID = value; }
        }

        private string source_Name;
        /// <summary>
        /// 寻缘名称
        /// </summary>
        public string Source_Name
        {
            get { return source_Name; }
            set { source_Name = value; }
        }

        private string source_Type;
        /// <summary>
        /// 寻源类型
        /// </summary>
        public string Source_Type
        {
            get { return source_Type; }
            set { source_Type = value; }
        }

        private string purchaseOrg;
        /// <summary>
        /// 采购组织
        /// </summary>
        public string PurchaseOrg
        {
            get { return purchaseOrg; }
            set { purchaseOrg = value; }
        }

        private string purchaseGroup;
        /// <summary>
        /// 采购组
        /// </summary>
        public string PurchaseGroup
        {
            get { return purchaseGroup; }
            set { purchaseGroup = value; }
        }

        private DateTime create_Time;

        public DateTime Create_Time
        {
            get { return create_Time; }
            set { create_Time = value; }
        }


        private DateTime delivery_Time;

        public DateTime Delivery_Time
        {
            get { return delivery_Time; }
            set { delivery_Time = value; }
        }
 

        private string source_State;
        /// <summary>
        /// 寻源（询价方式）的状态
        /// </summary>
        public string Source_State
        {
            get { return source_State; }
            set { source_State = value; }
        }

        private string source_Path;
        /// <summary>
        /// 寻源（寻源方式）的寻源文件保存路径
        /// </summary>
        public string Source_Path
        {
            get { return source_Path; }
            set { source_Path = value; }
        }
    }
}
