﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.SourcingManage.SourcingManagement
{
    public class Bid
    {
        //招标编号
        private string bidId;

        public string BidId
        {
            get { return bidId; }
            set { bidId = value; }
        }

        //招标名称
        private string bidName;

        public string BidName
        {
            get { return bidName; }
            set { bidName = value; }
        }

        //创建时间
        private DateTime createTime;

        public DateTime CreateTime
        {
            get { return createTime; }
            set { createTime = value; }
        }

        //业务类型
        private string serviceType;

        public string ServiceType
        {
            get { return serviceType; }
            set { serviceType = value; }
        }

        //产品目录
        private string catalogue;

        public string Catalogue
        {
            get { return catalogue; }
            set { catalogue = value; }
        }

        //发布的类型
        private int displayType;

        public int DisplayType
        {
            get { return displayType; }
            set { displayType = value; }
        }

        //采购组织
        private int purchaseOrg;

        public int PurchaseOrg
        {
            get { return purchaseOrg; }
            set { purchaseOrg = value; }
        }


        //采购组
        private int purchaseGroup;

        public int PurchaseGroup
        {
            get { return purchaseGroup; }
            set { purchaseGroup = value; }
        }


        //开始时间
        private DateTime startTime;

        public DateTime StartTime
        {
            get { return startTime; }
            set { startTime = value; }
        }


        //结束时间
        private DateTime endTime;

        public DateTime EndTime
        {
            get { return endTime; }
            set { endTime = value; }
        }

        //起始时间
        private DateTime startBeginTime;

        public DateTime StartBeginTime
        {
            get { return startBeginTime; }
            set { startBeginTime = value; }
        }





        //限定期段
        private DateTime limitTime;

        public DateTime LimitTime
        {
            get { return limitTime; }
            set { limitTime = value; }
        }


        //时区
        private string timeZone;

        public string TimeZone
        {
            get { return timeZone; }
            set { timeZone = value; }
        }


        //货币
        private string currency;

        public string Currency
        {
            get { return currency; }
            set { currency = value; }
        }


        //商务标开标时间
        private DateTime commBidStartTime;

        public DateTime CommBidStartTime
        {
            get { return commBidStartTime; }
            set { commBidStartTime = value; }
        }


        //商务标开标日期
        private DateTime commBidStartDate;

        public DateTime CommBidStartDate
        {
            get { return commBidStartDate; }
            set { commBidStartDate = value; }
        }


        //技术标开标时间
        private DateTime techBidStartTime;

        public DateTime TechBidStartTime
        {
            get { return techBidStartTime; }
            set { techBidStartTime = value; }
        }


        //技术标开标日期
        private DateTime techBidStartDate;

        public DateTime TechBidStartDate
        {
            get { return techBidStartDate; }
            set { techBidStartDate = value; }
        }


        //购买标书的截至时间
        private DateTime buyEndTime;

        public DateTime BuyEndTime
        {
            get { return buyEndTime; }
            set { buyEndTime = value; }
        }


        //购买标书的截至日期
        private DateTime buyEndDate;

        public DateTime BuyEndDate
        {
            get { return buyEndDate; }
            set { buyEndDate = value; }
        }


        //报名结束时间
        private DateTime enrollEndTime;

        public DateTime EnrollEndTime
        {
            get { return enrollEndTime; }
            set { enrollEndTime = value; }
        }


        //报名结束日期
        private DateTime enrollEndDate;

        public DateTime EnrollEndDate
        {
            get { return enrollEndDate; }
            set { enrollEndDate = value; }
        }


        //报名开始时间
        private DateTime enrollStartTime;

        public DateTime EnrollStartTime
        {
            get { return enrollStartTime; }
            set { enrollStartTime = value; }
        }


        //报名开始日期
        private DateTime enrollStartDate;

        public DateTime EnrollStartDate
        {
            get { return enrollStartDate; }
            set { enrollStartDate = value; }
        }


        //投标费
        private double bidCost;

        public double BidCost
        {
            get { return bidCost; }
            set { bidCost = value; }
        }


        //招标状态
        private int bidState;

        public int BidState
        {
            get { return bidState; }
            set { bidState = value; }
        }


        //评估方法编号
        private int evalMethId;

        public int EvalMethId
        {
            get { return evalMethId; }
            set { evalMethId = value; }
        }

        //支付条款
        private string payItem;

        public string PayItem
        {
            get { return payItem; }
            set { payItem = value; }
        }

    }
}
