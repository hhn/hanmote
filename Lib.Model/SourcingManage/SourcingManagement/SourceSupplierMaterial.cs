﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.SourcingManage.SourcingManagement
{
    /// <summary>
    /// 寻源类
    /// </summary>
    public class SourceSupplierMaterial
    {
        private string source_ID;
        /// <summary>
        /// 寻源编号
        /// </summary>
        public string Source_ID
        {
            get { return source_ID; }
            set { source_ID = value; }
        }

        private string supplier_ID;
        /// <summary>
        /// 供应商编号
        /// </summary>
        public string Supplier_ID
        {
            get { return supplier_ID; }
            set { supplier_ID = value; }
        }

        private string material_ID;
        /// <summary>
        /// 物料编号
        /// </summary>
        public string Material_ID
        {
            get { return material_ID; }
            set { material_ID = value; }
        }

        private float price;
        /// <summary>
        /// 供应商提供的价格
        /// </summary>
        public float Price
        {
            get { return price; }
            set { price = value; }
        }

    }
}
