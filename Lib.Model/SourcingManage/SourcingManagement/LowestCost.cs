﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.SourcingManage.SourcingManagement
{
    public class LowestCost
    {
        private string sourceId;

        public string SourceId
        {
            get { return sourceId; }
            set { sourceId = value; }
        }

        private string costItem;

        public string CostItem
        {
            get { return costItem; }
            set { costItem = value; }
        }

        private float first;

        public float First
        {
            get { return first; }
            set { first = value; }
        }

        private float second;

        public float Second
        {
            get { return second; }
            set { second = value; }
        }

        private float third;

        public float Third
        {
            get { return third; }
            set { third = value; }
        }

        private float forth;

        public float Forth
        {
            get { return forth; }
            set { forth = value; }
        }

        private float fifth;

        public float Fifth
        {
            get { return fifth; }
            set { fifth = value; }
        }

        private float sixth;

        public float Sixth
        {
            get { return sixth; }
            set { sixth = value; }
        }

        private float seventh;

        public float Seventh
        {
            get { return seventh; }
            set { seventh = value; }
        }

        private float eighth;

        public float Eighth
        {
            get { return eighth; }
            set { eighth = value; }
        }

        private float ninth;

        public float Ninth
        {
            get { return ninth; }
            set { ninth = value; }
        }

        private float tenth;

        public float Tenth
        {
            get { return tenth; }
            set { tenth = value; }
        }


    }
}
