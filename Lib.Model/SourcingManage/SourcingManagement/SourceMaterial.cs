﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.SourcingManage.SourcingManagement
{
    /// <summary>
    /// 寻源类
    /// </summary>
    public class SourceMaterial
    {
        private string source_ID;
        /// <summary>
        /// 寻源编号
        /// </summary>
        public string Source_ID
        {
            get { return source_ID; }
            set { source_ID = value; }
        }

        private string material_ID;
        /// <summary>
        /// 物料编号
        /// </summary>
        public string Material_ID
        {
            get { return material_ID; }
            set { material_ID = value; }
        }

        private string material_Name;
        /// <summary>
        /// 物料名称
        /// </summary>
        public string Material_Name
        {
            get { return material_Name; }
            set { material_Name = value; }
        }

        private int demand_Count;
        /// <summary>
        /// 需求数量
        /// </summary>
        public int Demand_Count
        {
            get { return demand_Count; }
            set { demand_Count = value; }
        }

        private string demand_ID;
        /// <summary>
        /// 需求单号
        /// </summary>
        public string Demand_ID
        {
            get { return demand_ID; }
            set { demand_ID = value; }
        }

        private string unit;
        /// <summary>
        /// 单位
        /// </summary>
        public string Unit
        {
            get { return unit; }
            set { unit = value; }
        }

        private string factoryId;
        /// <summary>
        /// 工厂编号
        /// </summary>
        public string FactoryId
        {
            get { return factoryId; }
            set { factoryId = value; }
        }

        private string stockId;
        /// <summary>
        /// 仓库编号
        /// </summary>
        public string StockId
        {
            get { return stockId; }
            set { stockId = value; }
        }

        private DateTime deliveryStartTime;
        /// <summary>
        /// 交易开始日期
        /// </summary>
        public DateTime DeliveryStartTime
        {
            get { return deliveryStartTime; }
            set { deliveryStartTime = value; }
        }

        private DateTime deliveryEndTime;
        /// <summary>
        /// 交易结束日期-
        /// </summary>
        public DateTime DeliveryEndTime
        {
            get { return deliveryEndTime; }
            set { deliveryEndTime = value; }
        }


    }
}
