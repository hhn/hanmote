﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.SourcingManage.SourcingManagement
{
    public class TransactItem
    {
        private string bidId;

        public string BidId
        {
            get { return bidId; }
            set { bidId = value; }
        }

        private string supplierId;

        public string SupplierId
        {
            get { return supplierId; }
            set { supplierId = value; }
        }

        private string itemName;

        public string ItemName
        {
            get { return itemName; }
            set { itemName = value; }
        }

        private string goalDescription;

        public string GoalDescription
        {
            get { return goalDescription; }
            set { goalDescription = value; }
        }

        private string supplierAnswer;

        public string SupplierAnswer
        {
            get { return supplierAnswer; }
            set { supplierAnswer = value; }
        }

        private string note;

        public string Note
        {
            get { return note; }
            set { note = value; }
        }
    }
}
