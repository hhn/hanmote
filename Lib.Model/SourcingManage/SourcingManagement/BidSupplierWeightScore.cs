﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.SourcingManage.SourcingManagement
{
    public class BidSupplierWeightScore
    {
        private string bidId;

        public string BidId
        {
            get { return bidId; }
            set { bidId = value; }
        }

        private string supplierId;

        public string SupplierId
        {
            get { return supplierId; }
            set { supplierId = value; }
        }

        private string weightName;

        public string WeightName
        {
            get { return weightName; }
            set { weightName = value; }
        }

        private int weightNum;

        public int WeightNum
        {
            get { return weightNum; }
            set { weightNum = value; }
        }


        private string supplierAnswer;

        public string SupplierAnswer
        {
            get { return supplierAnswer; }
            set { supplierAnswer = value; }
        }



        private int score;

        public int Score
        {
            get { return score; }
            set { score = value; }
        }

        private string cause;
        /// <summary>
        /// 得分原因
        /// </summary>
        public string Cause
        {
            get { return cause; }
            set { cause = value; }
        }

    }
}
