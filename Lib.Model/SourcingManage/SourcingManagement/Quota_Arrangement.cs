﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.SourcingManage.SourcingManagement
{
    public class Quota_Arrangement
    {
        /// <summary>
        /// 物料编号
        /// </summary>
        public string Material_ID { get; set; }

        /// <summary>
        /// 工厂编号
        /// </summary>
        public string Factory_ID { get; set; }

        /// <summary>
        /// 配额协议编号
        /// </summary>
        public string Quota_Arrangement_ID { get; set; }

        /// <summary>
        /// 物料名称
        /// </summary>
        public string Material_Name { get; set; }

        /// <summary>
        /// 工厂名称
        /// </summary>
        public string Factory_Name { get; set; }

        /// <summary>
        /// 有效开始时间
        /// </summary>
        public DateTime Begin_Time { get; set; }

        /// <summary>
        /// 有效截止时间
        /// </summary>
        public DateTime End_Time { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime Create_Time { get; set; }

        /// <summary>
        /// 创建者编号
        /// </summary>
        public string Creator_ID { get; set; }

        /// <summary>
        /// 最小数量分解
        /// </summary>
        public double Min_Quantity_Resolve { get; set; }

        /// <summary>
        /// 基本单位
        /// </summary>
        public string OUn { get; set; }

        /// <summary>
        /// 计算类型(NotSet、Quota、Priority)
        /// </summary>
        public string Cal_Type { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        public string Status { get; set; }
    }
}
