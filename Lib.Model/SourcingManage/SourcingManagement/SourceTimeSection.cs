﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.SourcingManage.SourcingManagement
{
    public class SourceTimeSection
    {
        private string sourceId;
        /// <summary>
        /// 寻源编号
        /// </summary>
        public string SourceId
        {
            get { return sourceId; }
            set { sourceId = value; }
        }

        private string supplierId;
        /// <summary>
        /// 供应商编号
        /// </summary>
        public string SupplierId
        {
            get { return supplierId; }
            set { supplierId = value; }
        }

        private string materialId;
        /// <summary>
        /// 物料编号
        /// </summary>
        public string MaterialId
        {
            get { return materialId; }
            set { materialId = value; }
        }

        private DateTime startTime;
        /// <summary>
        /// 开始时间
        /// </summary>
        public DateTime StartTime
        {
            get { return startTime; }
            set { startTime = value; }
        }

        private DateTime endTime;
        /// <summary>
        /// 结束时间
        /// </summary>
        public DateTime EndTime
        {
            get { return endTime; }
            set { endTime = value; }
        }

        private float num;
        /// <summary>
        /// 区间值
        /// </summary>
        public float Num
        {
            get { return num; }
            set { num = value; }
        }
    }
}
