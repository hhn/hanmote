﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.SourcingManage.SourcingManagement
{
    /// <summary>
    /// 寻源类
    /// </summary>
    public class SourceSupplier
    {
        private string source_ID;
        /// <summary>
        /// 寻源编号
        /// </summary>
        public string Source_ID
        {
            get { return source_ID; }
            set { source_ID = value; }
        }

        private string supplier_ID;
        /// <summary>
        /// 供应商编号
        /// </summary>
        public string Supplier_ID
        {
            get { return supplier_ID; }
            set { supplier_ID = value; }
        }

        private string supplier_Name;
        /// <summary>
        /// 供应商名称
        /// </summary>
        public string Supplier_Name
        {
            get { return supplier_Name; }
            set { supplier_Name = value; }
        }

        private string classificationResult;
        /// <summary>
        /// 供应商评级
        /// </summary>
        public string ClassificationResult
        {
            get { return classificationResult; }
            set { classificationResult = value; }
        }

        

        //联系人
        private string contact;

        public string Contact
        {
            get { return contact; }
            set { contact = value; }
        }

        //邮箱
        private string email;

        public string Email
        {
            get { return email; }
            set { email = value; }
        }

        //地址
        private string address;

        public string Address
        {
            get { return address; }
            set { address = value; }
        }

        //状态
        private int state;

        public int State
        {
            get { return state; }
            set { state = value; }
        }


        private int isTrans;
        /// <summary>
        /// 谈判标识  0 未谈判；1 谈判中；  2 谈判成功；3 谈判失败
        /// </summary>
        public int IsTrans
        {
            get { return isTrans; }
            set { isTrans = value; }
        }


    }
}
