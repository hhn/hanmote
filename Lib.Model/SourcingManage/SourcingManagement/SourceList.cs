﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.SourcingManage.SourcingManagement
{
    /// <summary>
    /// 货源清单类
    /// </summary>
    public class SourceList
    {

        private string sourceListId;
        /// <summary>
        /// 货源清单编号
        /// </summary>
        public string SourceListId
        {
            get { return sourceListId; }
            set { sourceListId = value; }
        }


        private string material_ID;
        /// <summary>
        /// 物料编号
        /// </summary>
        public string Material_ID
        {
            get { return material_ID; }
            set { material_ID = value; }
        }

        private string factory_ID;
        /// <summary>
        /// 工厂编号
        /// </summary>
        public string Factory_ID
        {
            get { return factory_ID; }
            set { factory_ID = value; }
        }

        private string supplier_ID;
        /// <summary>
        /// 供应商编号
        /// </summary>
        public string Supplier_ID
        {
            get { return supplier_ID; }
            set { supplier_ID = value; }
        }

        //创建时间
        private DateTime createTime;

        public DateTime CreateTime
        {
            get { return createTime; }
            set { createTime = value; }
        }

        //生效时间
        private DateTime startTime;

        public DateTime StartTime
        {
            get { return startTime; }
            set { startTime = value; }
        }

        //失效时间
        private DateTime endTime;

        public DateTime EndTime
        {
            get { return endTime; }
            set { endTime = value; }
        }

        private string department;
        /// <summary>
        /// 采购组织
        /// </summary>
        public string Department
        {
            get { return department; }
            set { department = value; }
        }

        //协议号
        private string protocolId;

        public string ProtocolId
        {
            get { return protocolId; }
            set { protocolId = value; }
        }

        private string purchaseOrg;
        /// <summary>
        /// 采购组织
        /// </summary>
        public string PurchaseOrg
        {
            get { return purchaseOrg; }
            set { purchaseOrg = value; }
        }


        //ppl（工厂内调拨）
        private string pPL;

        public string PPL
        {
            get { return pPL; }
            set { pPL = value; }
        }

        private int fix;

        //固定供应商
        public int Fix
        {
            get { return fix; }
            set { fix = value; }
        }

        //冻结供应商
        private int blk;

        public int Blk
        {
            get { return blk; }
            set { blk = value; }
        }
        //和MRP相关
        private int mRP;

        public int MRP
        {
            get { return mRP; }
            set { mRP = value; }
        }
    }
}
