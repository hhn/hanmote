﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.SourcingManage
{
    /// <summary>
    /// 询价单类
    /// </summary>
    public class Enquiry
    {
        private string inquiry_ID;
        /// <summary>
        /// 询价单号
        /// </summary>
        public string Inquiry_ID
        {
            get { return inquiry_ID; }
            set { inquiry_ID = value; }
        }

        private string material_ID;
        /// <summary>
        /// 物料编号
        /// </summary>
        public string Material_ID
        {
            get { return material_ID; }
            set { material_ID = value; }
        }

        private string supplier_ID;
        /// <summary>
        /// 供应商编号
        /// </summary>
        public string Supplier_ID
        {
            get { return supplier_ID; }
            set { supplier_ID = value; }
        }

        private int number;
        /// <summary>
        /// 采购数量
        /// </summary>
        public int Number
        {
            get { return number; }
            set { number = value; }
        }

        private string price;
        /// <summary>
        /// 价格
        /// </summary>
        public string Price
        {
            get { return price; }
            set { price = value; }
        }

        private string clause_ID;
        /// <summary>
        /// 支付条款
        /// </summary>
        public string Clause_ID
        {
            get { return clause_ID; }
            set { clause_ID = value; }
        }

        private DateTime delivery_Time;
        /// <summary>
        /// 交货日期
        /// </summary>
        public DateTime Delivery_Time
        {
            get { return delivery_Time; }
            set { delivery_Time = value; }
        }

        private string factory_ID;
        /// <summary>
        /// 工厂编号
        /// </summary>
        public string Factory_ID
        {
            get { return factory_ID; }
            set { factory_ID = value; }
        }

        private string stock_ID;
        /// <summary>
        /// 仓库编号
        /// </summary>
        public string Stock_ID
        {
            get { return stock_ID; }
            set { stock_ID = value; }
        }

        private string condition_ID;
        /// <summary>
        /// 条件类型编号
        /// </summary>
        public string Condition_ID
        {
            get { return condition_ID; }
            set { condition_ID = value; }
        }

        private string state;
        /// <summary>
        /// 询价状态
        /// </summary>
        public string State
        {
            get { return state; }
            set { state = value; }
        }

        private DateTime update_Time;
        /// <summary>
        /// 更新时间
        /// </summary>
        public DateTime Update_Time
        {
            get { return update_Time; }
            set { update_Time = value; }
        }

        private string demand_Id;

        public string Demand_id
        {
            get { return demand_Id; }
            set { demand_Id = value; }
        }


    }
}
