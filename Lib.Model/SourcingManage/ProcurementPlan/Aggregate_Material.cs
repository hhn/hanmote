﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.SourcingManage.ProcurementPlan
{
    /// <summary>
    /// 需求计划对应的物料
    /// </summary>
    public class Aggregate_Material
    {
        private string aggregate_ID;
        /// <summary>
        /// 汇总单号
        /// </summary>
        public string Aggregate_ID
        {
            get { return aggregate_ID; }
            set { aggregate_ID = value; }
        }

        private string material_ID;
        /// <summary>
        /// 物料编号
        /// </summary>
        public string Material_ID
        {
            get { return material_ID; }
            set { material_ID = value; }
        }

        private string material_Name;
        /// <summary>
        /// 物料名称
        /// </summary>
        public string Material_Name
        {
            get { return material_Name; }
            set { material_Name = value; }
        }

        private string material_Group;
        /// <summary>
        /// 物料组
        /// </summary>
        public string Material_Group
        {
            get { return material_Group; }
            set { material_Group = value; }
        }



        private int aggregate_Count;
        /// <summary>
        /// 汇总数量
        /// </summary>
        public int Aggregate_Count
        {
            get { return aggregate_Count; }
            set { aggregate_Count = value; }
        }

        private string measurement;
        /// <summary>
        /// 计量单位
        /// </summary>
        public string Measurement
        {
            get { return measurement; }
            set { measurement = value; }
        }

        private DateTime create_Time;
        /// <summary>
        /// 汇总时间
        /// </summary>
        public DateTime Create_Time
        {
            get { return create_Time; }
            set { create_Time = value; }
        }

        private string factory_ID;
        /// <summary>
        /// 工厂编号
        /// </summary>
        public string Factory_ID
        {
            get { return factory_ID; }
            set { factory_ID = value; }
        }



    }
}
