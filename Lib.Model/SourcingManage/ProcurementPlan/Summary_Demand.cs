﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.SourcingManage.ProcurementPlan
{
    /// <summary>
    /// 采购计划类
    /// </summary>
    public class Summary_Demand
    {
        private string demand_ID;
        /// <summary>
        /// 需求单号
        /// </summary>
        public string Demand_ID
        {
            get { return demand_ID; }
            set { demand_ID = value; }
        }

        private string material_ID;
        /// <summary>
        /// 物料编码
        /// </summary>
        public string Material_ID
        {
            get { return material_ID; }
            set { material_ID = value; }
        }

        private string material_Name;
        /// <summary>
        /// 物料名称
        /// </summary>
        public string Material_Name
        {
            get { return material_Name; }
            set { material_Name = value; }
        }

        private string purchase_Type;
        /// <summary>
        /// 采购类型
        /// </summary>
        public string Purchase_Type
        {
            get { return purchase_Type; }
            set { purchase_Type = value; }
        }

        private string material_Type;
        /// <summary>
        /// 物料组
        /// </summary>
        public string Material_Type
        {
            get { return material_Type; }
            set { material_Type = value; }
        }

        private string stock_ID;
        /// <summary>
        /// 存货仓库
        /// </summary>
        public string Stock_ID
        {
            get { return stock_ID; }
            set { stock_ID = value; }
        }

        private string factory_ID;
        /// <summary>
        /// 工厂编号
        /// </summary>
        public string Factory_ID
        {
            get { return factory_ID; }
            set { factory_ID = value; }
        }

        private int mini_Purchase_Count;
        /// <summary>
        /// 最小订货批量
        /// </summary>
        public int Mini_Purchase_Count
        {
            get { return mini_Purchase_Count; }
            set { mini_Purchase_Count = value; }
        }

        private int demand_Count;
        /// <summary>
        /// 需求数量
        /// </summary>
        public int Demand_Count
        {
            get { return demand_Count; }
            set { demand_Count = value; }
        }

        private DateTime delivery_Date;
        /// <summary>
        /// 申请交货日期
        /// </summary>
        public DateTime Delivery_Date
        {
            get { return delivery_Date; }
            set { delivery_Date = value; }
        }

        private string proposer_ID;
        /// <summary>
        /// 申请人编号
        /// </summary>
        public string Proposer_ID
        {
            get { return proposer_ID; }
            set { proposer_ID = value; }
        }

        private string supplier_ID;
        /// <summary>
        /// 供应商编号
        /// </summary>
        public string Supplier_ID
        {
            get { return supplier_ID; }
            set { supplier_ID = value; }
        }

        private string contract_ID;
        /// <summary>
        /// 协议编号
        /// </summary>
        public string Contract_ID
        {
            get { return contract_ID; }
            set { contract_ID = value; }
        }

        private string remarks;
        /// <summary>
        /// 其他要求
        /// </summary>
        public string Remarks
        {
            get { return remarks; }
            set { remarks = value; }
        }

        private string state;
        /// <summary>
        /// 采购订单状态
        /// </summary>
        public string State
        {
            get { return state; }
            set { state = value; }
        }

        private DateTime pass_Date;
        /// <summary>
        /// 采购订单结束时间
        /// </summary>
        public DateTime Pass_Date
        {
            get { return pass_Date; }
            set { pass_Date = value; }
        }

        private string purchase_Price;
        /// <summary>
        /// 采购价格
        /// </summary>
        public string Purchase_Price
        {
            get { return purchase_Price; }
            set { purchase_Price = value; }
        }

        private string inquiry_ID;
        /// <summary>
        /// 分配原则
        /// </summary>
        public string Inquiry_ID
        {
            get { return inquiry_ID; }
            set { inquiry_ID = value; }
        }

        private DateTime begin_Time;
        /// <summary>
        /// 采购开始时间
        /// </summary>
        public DateTime Begin_Time
        {
            get { return begin_Time; }
            set { begin_Time = value; }
        }

        private DateTime end_Time;
        /// <summary>
        /// 采购结束时间
        /// </summary>
        public DateTime End_Time
        {
            get { return end_Time; }
            set { end_Time = value; }
        }

        private string follow_Certificate;
        /// <summary>
        /// 后续采购凭证
        /// </summary>
        public string Follow_Certificate
        {
            get { return follow_Certificate; }
            set { follow_Certificate = value; }
        }

        private DateTime create_Time;
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime Create_Time
        {
            get { return create_Time; }
            set { create_Time = value; }
        }

        private DateTime update_Time;
        /// <summary>
        /// 更新时间
        /// </summary>
        public DateTime Update_Time
        {
            get { return update_Time; }
            set { update_Time = value; }
        }

        private string phoneNum;
        /// <summary>
        /// 电话号码
        /// </summary>
        /// 
        public string PhoneNum
        {
            get { return phoneNum; }
            set { phoneNum = value; }
        }

        private string reviewTime;
        /// <summary>
        /// 审核时间
        /// </summary>
        public string ReviewTime
        {
            get { return reviewTime; }
            set { reviewTime = value; }
        }

        private string department;
        /// <summary>
        /// 申请部门
        /// </summary>
        public string Department
        {
            get { return department; }
            set { department = value; }
        }

        private string logisticsMode;
        /// <summary>
        /// 物流方式
        /// </summary>
        public string LogisticsMode
        {
            get { return logisticsMode; }
            set { logisticsMode = value; }
        }

        private string measurement;
        /// <summary>
        /// 计量单位
        /// </summary>
        public string Measurement
        {
            get { return measurement; }
            set { measurement = value; }
        }

        private string reviewAdvice;
        public string flag;

        /// <summary>
        /// 审核意见
        /// </summary>
        public string ReviewAdvice
        {
            get { return reviewAdvice; }
            set { reviewAdvice = value; }
        }
    }
}
