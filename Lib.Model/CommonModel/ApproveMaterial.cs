﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.CommonModel
{
    public class ApproveMaterial
    {
        //物料id
        public string Material_Id { get; set; }
        //审核次数
        public int Verify { get; set; }
        //审批人id
        public string Approve_Id { get; set; }
        //审批人意见
        public string Approve_Advice { get; set; }
        //审批结果
        public string Approve { get; set; }
        //第一次审核人id
        public string Verify_Id1 { get; set; }
        //第二次审核人Id
        public string Verify_Id2 { get; set; }
        //第一次审核结果
        public string Verify_Result1 { get; set; }
        //第二次审核结果
        public string Verify_Result2 { get; set; }
        //第一次审核意见
        public string Verify_Advice1 { get; set; }
        //第二次审核意见
        public string Verify_Advice2 { get; set; }
        //物料定位
        public string Material_Location { get; set; }
    }
}
