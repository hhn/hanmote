﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.CommonModel
{
    public class SummaryDemand
    {
        public string Demand_ID{ get; set; }
        public string Purchase_Type { get; set; }
        public string Proposer_ID { get; set; }
        public string Remarks { get; set; }
        public string State { get; set; }
        public string Inquiry_ID { get; set; }
        public string Follow_Certificate { get; set; }
        public DateTime Create_Time { get; set; }
        public DateTime Update_Time { get; set; }
        public string PhoneNum { get; set; }
        public DateTime ReviewTime { get; set; }
        public string Department { get; set; }
        public string LogisticsMode { get; set; }
        public string ReviewAdvice { get; set; }
        public string Contract_ID { get; set; }
        public DateTime Apply_Date { get; set; }
        public DateTime Begin_Time { get; set; }
        public DateTime Delivery_Date { get; set; }
        public int Demand_Count { get; set; }
        public string Distribution_Principle { get; set; }
        public string Distribution_Type { get; set; }
        public DateTime End_Time { get; set; }
        public string Material_Name { get; set; }
        public int Mini_Purchase_Count { get; set; }
        public DateTime Pass_Date { get; set; }
        public string Purchase_Price { get; set; }
        public string Supplier_ID { get; set; }
        public string flag { get; set; }
        public string Factory_ID { get; set; }
    }
}
