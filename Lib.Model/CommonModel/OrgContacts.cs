﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model
{
    /// <summary>
    /// 组织信息中联系人的实体类
    /// </summary>
    public class OrgContacts:BaseEntity
    {
        public string ContactMan { get; set; }

        public string Phonenum1 { get; set; }

        public string Phonenum2 { get; set; }

        public string Email { get; set; }

    }
}
