﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.StockModel
{
    public class DocumentDetailInfoModel
    {
        private string stockInDocument_ID;

        public string StockInDocument_ID
        {
            get { return stockInDocument_ID; }
            set { stockInDocument_ID = value; }
        }
        private string sMaterial_ID;

        public string SMaterial_ID
        {
            get { return sMaterial_ID; }
            set { sMaterial_ID = value; }
        }
        private string order_ID;

        public string Order_ID
        {
            get { return order_ID; }
            set { order_ID = value; }
        }
        private string delivery_ID;

        public string Delivery_ID
        {
            get { return delivery_ID; }
            set { delivery_ID = value; }
        }
        private string receiptNote_ID;

        public string ReceiptNote_ID
        {
            get { return receiptNote_ID; }
            set { receiptNote_ID = value; }
        }
        private string sFactory_ID;

        public string SFactory_ID
        {
            get { return sFactory_ID; }
            set { sFactory_ID = value; }
        }
        private string sStock_ID;

        public string SStock_ID
        {
            get { return sStock_ID; }
            set { sStock_ID = value; }
        }
        private string supplier_ID;

        public string Supplier_ID
        {
            get { return supplier_ID; }
            set { supplier_ID = value; }
        }
        private string inventory_Type;

        public string Inventory_Type
        {
            get { return inventory_Type; }
            set { inventory_Type = value; }
        }
        private int receive_Count;

        public int Receive_Count
        {
            get { return receive_Count; }
            set { receive_Count = value; }
        }
        private int order_Count;

        public int Order_Count
        {
            get { return order_Count; }
            set { order_Count = value; }
        }

        private string unit;
        public string Unit
        {
            get { return unit; }
            set { unit = value; }
        }

        private float unitPrice;

        public float UnitPrice
        {
            get { return unitPrice; }
            set { unitPrice = value; }
        }


        private int input_Count;

        public int Input_Count
        {
            get { return input_Count; }
            set { input_Count = value; }
        }
        private int sKU_Count;

        public int SKU_Count
        {
            get { return sKU_Count; }
            set { sKU_Count = value; }
        }
        private int notes_Count;

        public int Notes_Count
        {
            get { return notes_Count; }
            set { notes_Count = value; }
        }
        private string batch_ID;

        public string Batch_ID
        {
            get { return batch_ID; }
            set { batch_ID = value; }
        }
        private string supplierMaterial_ID;

        public string SupplierMaterial_ID
        {
            get { return supplierMaterial_ID; }
            set { supplierMaterial_ID = value; }
        }
        private string material_Group;

        public string Material_Group
        {
            get { return material_Group; }
            set { material_Group = value; }
        }
        private string evaluate_Type;

        public string Evaluate_Type
        {
            get { return evaluate_Type; }
            set { evaluate_Type = value; }
        }
        private int tolerance;

        public int Tolerance
        {
            get { return tolerance; }
            set { tolerance = value; }
        }
        private string loading_Place;

        public string Loading_Place
        {
            get { return loading_Place; }
            set { loading_Place = value; }
        }
        private string receiving_Place;

        public string Receiving_Place
        {
            get { return receiving_Place; }
            set { receiving_Place = value; }
        }
        private string tMaterial_ID;

        public string TMaterial_ID
        {
            get { return tMaterial_ID; }
            set { tMaterial_ID = value; }
        }
        private string tFactory_ID;

        public string TFactory_ID
        {
            get { return tFactory_ID; }
            set { tFactory_ID = value; }
        }
        private string tStock_ID;

        public string TStock_ID
        {
            get { return tStock_ID; }
            set { tStock_ID = value; }
        }
        private string tMaterial_Name;

        public string TMaterial_Name
        {
            get { return tMaterial_Name; }
            set { tMaterial_Name = value; }
        }
        private string tFactory_Name;

        public string TFactory_Name
        {
            get { return tFactory_Name; }
            set { tFactory_Name = value; }
        }
        private string tStock_Name;

        public string TStock_Name
        {
            get { return tStock_Name; }
            set { tStock_Name = value; }
        }
        private string specialStock;

        public string SpecialStock
        {
            get { return specialStock; }
            set { specialStock = value; }
        }
        private string explainText;

        public string ExplainText
        {
            get { return explainText; }
            set { explainText = value; }
        }

        private string storehouseUnit;

        public string StorehouseUnit
        {
            get { return storehouseUnit; }
            set { storehouseUnit = value; }
        }

        private string storehouse_ID;

        public string Storehouse_ID
        {
            get { return storehouse_ID; }
            set { storehouse_ID = value; }
        }
    }
}
