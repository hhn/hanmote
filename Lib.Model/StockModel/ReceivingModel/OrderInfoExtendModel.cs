﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.StockModel
{
     public class OrderInfoExtendModel:OrderInfoModel
    {
         /// <summary>
        /// 供应商编码
         /// </summary>
        private string supplierId;

        public string SupplierId
        {
            get { return supplierId; }
            set { supplierId = value; }
        }

         /// <summary>
         /// 供应商名称
         /// </summary>
        private string supplierName;

        public string SupplierName
        {
            get { return supplierName; }
            set { supplierName = value; }
        }

         /// <summary>
         /// 供应商地址
         /// </summary>
        private string address;

        public string Address
        {
            get { return address; }
            set { address = value; }
        }

         /// <summary>
         /// 供应商邮编
         /// </summary>
        private string zip_Code;

        public string Zip_Code
        {
            get { return zip_Code; }
            set { zip_Code = value; }
        }

    }
}
