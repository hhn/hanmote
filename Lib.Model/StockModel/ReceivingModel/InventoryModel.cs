﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.StockModel
{
    public class InventoryModel
    {
        /// <summary>
        /// id；
        /// </summary>
        private int iD;

        public int ID
        {
            get { return iD; }
            set { iD = value; }
        }

        /// <summary>
        /// 物料编码
        /// </summary>
        private string material_ID;

        public string Material_ID
        {
            get { return material_ID; }
            set { material_ID = value; }
        }

        /// <summary>
        /// 工厂编码
        /// </summary>
        private string factory_ID;

        public string Factory_ID
        {
            get { return factory_ID; }
            set { factory_ID = value; }
        }

        /// <summary>
        /// 库存编码
        /// </summary>
        private string stock_ID;

        public string Stock_ID
        {
            get { return stock_ID; }
            set { stock_ID = value; }
        }

        /// <summary>
        /// 库存类型
        /// </summary>
        private string inventory_Type;

        public string Inventory_Type
        {
            get { return inventory_Type; }
            set { inventory_Type = value; }
        }

        /// <summary>
        /// 批次id
        /// </summary>
        private string batch_ID;

        public string Batch_ID
        {
            get { return batch_ID; }
            set { batch_ID = value; }
        }

        /// <summary>
        /// 库存状态
        /// </summary>
        private string inventory_State;

        public string Inventory_State
        {
            get { return inventory_State; }
            set { inventory_State = value; }
        }

        /// <summary>
        /// 数量
        /// </summary>
        private int count;

        public int Count
        {
            get { return count; }
            set { count = value; }
        }

        /// <summary>
        /// 评估类型
        /// </summary>
        private string evaluate_Type;

        public string Evaluate_Type
        {
            get { return evaluate_Type; }
            set { evaluate_Type = value; }
        }

        /// <summary>
        /// 物料组
        /// </summary>
        private string material_Group;

        public string Material_Group
        {
            get { return material_Group; }
            set { material_Group = value; }
        }

        /// <summary>
        /// 是否在盘点
        /// </summary>
        private Boolean stocktaking;

        public Boolean Stocktaking
        {
            get { return stocktaking; }
            set { stocktaking = value; }
        }

        /// <summary>
        /// 单位
        /// </summary>
        private string sKU;

        public string SKU
        {
            get { return sKU; }
            set { sKU = value; }
        }
    }
}
