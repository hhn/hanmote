﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.StockModel
{
    public class OrderInfoModel
    {
        private string order_ID;

        public string Order_ID
        {
            get { return order_ID; }
            set { order_ID = value; }
        }

        private string order_Type;

        public string Order_Type
        {
            get { return order_Type; }
            set { order_Type = value; }
        }

        private string purchase_Organization;

        public string Purchase_Organization
        {
            get { return purchase_Organization; }
            set { purchase_Organization = value; }
        }

        private string company_Code;

        public string Company_Code
        {
            get { return company_Code; }
            set { company_Code = value; }
        }

        private string supplier_ID;

        public string Supplier_ID
        {
            get { return supplier_ID; }
            set { supplier_ID = value; }
        }

        private string create_Time;

        public string Create_Time
        {
            get { return create_Time; }
            set { create_Time = value; }
        }

        private string delivery_Points;

        public string Delivery_Points
        {
            get { return delivery_Points; }
            set { delivery_Points = value; }
        }

        private string delivery_Type;

        public string Delivery_Type
        {
            get { return delivery_Type; }
            set { delivery_Type = value; }
        }

        private string total_Value;

        public string Total_Value
        {
            get { return total_Value; }
            set { total_Value = value; }
        }

        private string order_Status;

        public string Order_Status
        {
            get { return order_Status; }
            set { order_Status = value; }
        }

        private string value_Received;

        public string Value_Received
        {
            get { return value_Received; }
            set { value_Received = value; }
        }

        private string value_Paid;

        public string Value_Paid
        {
            get { return value_Paid; }
            set { value_Paid = value; }
        }

        private string total_Number;

        public string Total_Number
        {
            get { return total_Number; }
            set { total_Number = value; }
        }

        private string pur_phone;

        public string Pur_phone
        {
            get { return pur_phone; }
            set { pur_phone = value; }
        }

        private string pur_mail;

        public string Pur_mail
        {
            get { return pur_mail; }
            set { pur_mail = value; }
        }

        private string explain;

        public string Explain
        {
            get { return explain; }
            set { explain = value; }
        }
    }
}
