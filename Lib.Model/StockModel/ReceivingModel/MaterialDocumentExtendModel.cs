﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.StockModel
{
    public class MaterialDocumentExtendModel : MaterialDocumentModel
    {
        /// <summary>
        /// 物料名称
        /// </summary>
        private string material_Name;

        public string Material_Name
        {
            get { return material_Name; }
            set { material_Name = value; }
        }

        /// <summary>
        /// 采购订单数量
        /// </summary>
        private int material_Count;

        public int Material_Count
        {
            get { return material_Count; }
            set { material_Count = value; }
        }

        /// <summary>
        /// 物料组
        /// </summary>
        private string material_Group;

        public string Material_Group
        {
            get { return material_Group; }
            set { material_Group = value; }
        }

        /// <summary>
        /// 工厂名称
        /// </summary>
        private string factory_Name;

        public string Factory_Name
        {
            get { return factory_Name; }
            set { factory_Name = value; }
        }


        /// <summary>
        /// 计量单位
        /// </summary>
        private string measurement;

        public string Measurement
        {
            get { return measurement; }
            set { measurement = value; }
        }

        /// <summary>
        /// 供应商名称
        /// </summary>
        private string supplier_Name;

        public string Supplier_Name
        {
            get { return supplier_Name; }
            set { supplier_Name = value; }
        }

        /// <summary>
        /// 供应商所在地址
        /// </summary>
        private string address;

        public string Address
        {
            get { return address; }
            set { address = value; }
        }

        /// <summary>
        /// 供应商邮政编号
        /// </summary>
        private string company_ZipCode;

        public string Company_ZipCode
        {
            get { return company_ZipCode; }
            set { company_ZipCode = value; }
        }


        /// <summary>
        /// 联系人
        /// </summary>
        private string contact_Person;

        public string Contact_Person
        {
            get { return contact_Person; }
            set { contact_Person = value; }
        }

        /// <summary>
        /// 联系方式
        /// </summary>
        private string contact_phone1;

        public string Contact_phone1
        {
            get { return contact_phone1; }
            set { contact_phone1 = value; }
        }
    }
}
