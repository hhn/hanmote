﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.StockModel
{
    public class StockFactoryModel
    {
        //流水码
        private string iD;

        public string ID
        {
            get { return iD; }
            set { iD = value; }
        }

        //工厂代码
        private string factory_ID;

        public string Factory_ID
        {
            get { return factory_ID; }
            set { factory_ID = value; }
        }

        //工厂名称
        private string factory_Name;

        public string Factory_Name
        {
            get { return factory_Name; }
            set { factory_Name = value; }
        }

        /*
        //工厂地址
        private string factory_Address;

        public string Factory_Address
        {
            get { return factory_Address; }
            set { factory_Address = value; }
        }

        
        //下属仓库编码
        private string stock_ID;

        public string Stock_ID
        {
            get { return stock_ID; }
            set { stock_ID = value; }
        }
         * */
    }
}
