﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.StockModel
{
    public class StockTypeModel
    {
        /// <summary>
        /// 类型编号
        /// </summary>
        private int id;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        /// <summary>
        /// 类型名称
        /// </summary>
        private string type;

        public string Type
        {
            get { return type; }
            set { type = value; }
        }

        /// <summary>
        /// 是否有效
        /// 标识删除
        /// </summary>
        private int valid;

        public int Valid
        {
            get { return valid; }
            set { valid = value; }
        }

        /// <summary>
        /// 创建时间
        /// </summary>
        private DateTime create_ts;

        public DateTime Create_ts
        {
            get { return create_ts; }
            set { create_ts = value; }
        }

        /// <summary>
        /// 修改时间
        /// </summary>
        private DateTime update_ts;

        public DateTime Update_ts
        {
            get { return update_ts; }
            set { update_ts = value; }
        }
    }
}
