﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.CertificationProcess.LocationEvaluation
{
    public class LocationalEvaluationSupperModel
    {
                 
        /// 供应商编号
        public String SupplierId { get; set; }
        /// 公司名称
        public String companyName { get; set; }
        ///区域
        public String Country { get; set; }
        ///联系人
        public String ContactName { get; set; }
        ///联系方式
        public String PhoneNumber { get; set; }
        ///电子邮件
        public String ContactMail { get; set; }
        ///物料组
        public String MtGroupName { get; set; }
        ///采购组织
        public String SelectedDep { get; set; }

        public String evalID { get; set; }

    }
}
