﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.supplierListMode
{
    public class SupplierListModel
    {
        #region 供应商信息
        private string supplierID;
        private string supplierName;
        private string supplierLevel;
        private string levelDescription;
        private string supplierType;
        #endregion
        #region 清单信息
        private string SupplierListid;
        private string description;
        private string remaker;
        private string purOrgId;
        private string mtId;
        private DateTime createTime;

        public string SupplierID { get => supplierID; set => supplierID = value; }
        public string SupplierName { get => supplierName; set => supplierName = value; }
        public string SupplierLevel { get => supplierLevel; set => supplierLevel = value; }
        public string SupplierListid1 { get => SupplierListid; set => SupplierListid = value; }
        public string Description { get => description; set => description = value; }
        public string Remaker { get => remaker; set => remaker = value; }
        public string PurOrgId { get => purOrgId; set => purOrgId = value; }
        public string MtId { get => mtId; set => mtId = value; }
        public DateTime CreateTime { get => createTime; set => createTime = value; }
        public string LevelDescription { get => levelDescription; set => levelDescription = value; }
        public string SupplierType { get => supplierType; set => supplierType = value; }
        #endregion



    }
}
