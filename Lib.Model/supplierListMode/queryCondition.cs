﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.supplierListMode
{
    public class queryCondition
    {
        //采购组织名称
        private string purOrg;
        /// <summary>
        /// 物料组名称
        /// </summary>
        private string mtGroupName;
        /// <summary>
        /// 供应商编号
        /// </summary>
        private string supplierId;
        /// <summary>
        /// 供应商名称
        /// </summary>
        private string supplierName;
        /// <summary>
        /// 物料编号
        /// </summary>
        private string mtId;
        /// <summary>
        /// 显示记录条数
        /// </summary>
        private int pageSize;
        /// <summary>
        /// 当前页码
        /// </summary>
        private int pageIndex;
        /// <summary>
        /// 起止时间
        /// </summary>
        private DateTime sTime;
        private DateTime eTime;

        public string PurOrg { get => purOrg; set => purOrg = value; }
        public string MtGroupName { get => mtGroupName; set => mtGroupName = value; }
        public string MtId { get => mtId; set => mtId = value; }
        public int PageSize { get => pageSize; set => pageSize = value; }
        public int PageIndex { get => pageIndex; set => pageIndex = value; }
        public string SupplierId { get => supplierId; set => supplierId = value; }
        public string SupplierName { get => supplierName; set => supplierName = value; }
        public DateTime STime { get => sTime; set => sTime = value; }
        public DateTime ETime { get => eTime; set => eTime = value; }
    }
}
