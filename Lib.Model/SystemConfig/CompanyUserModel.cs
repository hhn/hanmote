﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.SystemConfig
{   /// <summary>
    /// author：hezl
    /// </summary>
    public class CompanyUserModel
    {

        ///
        ///公司名称、编号
        ///
        public static String companyID { get; set; }

        public static String companyName { get; set; }
        /// <summary>
        /// 用户编号
        /// </summary>
        public String User_ID { get; set; }

        /// <summary>
        /// 登录名
        /// </summary>
        public String Login_Name { get; set; }

        /// <summary>
        /// 登录密码
        /// </summary>
        public String Login_Password { get; set; }

        /// <summary>
        /// 用户姓名
        /// </summary>
        public String Username { get; set; }

        /// <summary>
        /// 性别 0为男、1为女
        /// </summary>
        public int Gender { get; set; }

        /// <summary>
        /// 出生日期
        /// </summary>
        public String Birthday { get; set; }

        /// <summary>
        /// 籍贯
        /// </summary>
        public String Birth_Place { get; set; }

        /// <summary>
        /// 身份证号
        /// </summary>
        public String ID_Number { get; set; }

        /// <summary>
        /// 用户联系方式
        /// </summary>
        public String Mobile { get; set; }

        /// <summary>
        /// 用户邮箱
        /// </summary>
        public String Email { get; set; }

        /// <summary>
        /// 用户备注
        /// </summary>
        public String User_Description { get; set; }

        /// <summary>
        /// 用户密保问题
        /// </summary>
        public String Question { get; set; }

        /// <summary>
        /// 用户密保答案
        /// </summary>
        public String Answer_Question { get; set; }

        /// <summary>
        /// 用户是否有效
        /// </summary>
        public int Enabled { get; set; }

        /// <summary>
        /// 是否为管理员
        /// </summary>
        public static int Manager_Flag { get; set; }

        /// <summary>
        /// 用户产生时间
        /// </summary>
        public String Generate_Time { get; set; }

        /// <summary>
        /// 建用户者编号
        /// </summary>
        public String Generate_User_ID { get; set; }

        /// <summary>
        /// 建用户者姓名
        /// </summary>
        public String Generate_Username { get; set; }

        /// <summary>
        /// 用户更新时间
        /// </summary>
        public String Modify_Time { get; set; }

        /// <summary>
        /// 更新用户者编号
        /// </summary>
        public String Modify_User_ID { get; set; }

        /// <summary>
        /// 更新用户者姓名
        /// </summary>
        public String Modify_Username { get; set; }
        /// <summary>
        /// 公司名称
        /// </summary>
        public static  string Supplier_Id { get; set; }
        /// <summary>
        /// 公司编号
        /// </summary>
        public static string Supplier_Name { get; set; }
    }
}
