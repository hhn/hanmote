﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.SystemConfig
{
    public class JoinUserOrganizationModel
    {
        /// <summary>
        /// 自增ID
        /// </summary>
        public int ID { get; set; }
       
        /// <summary>
        /// 用户编号
        /// </summary>
        public String User_ID { get; set; }

        /// <summary>
        /// 组织机构编号
        /// </summary>
        public String Organization_ID { get; set; }
    }
}
