﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.SystemConfig
{
    /// <summary>
    /// 组、权限对象联系Model
    /// </summary>
    public class JoinGroupFunctionPermissionObjectModel
    {
        /// <summary>
        /// 自增标志
        /// </summary>
        public int Group_Permission_ID { get; set; }

        /// <summary>
        /// 组编号
        /// </summary>
        public String Group_ID { get; set; }

        /// <summary>
        /// 权限对象编号
        /// </summary>
        public int Permission_ID { get; set; }
    }
}
