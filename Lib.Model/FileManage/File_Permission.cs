﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.FileManage
{
    public class File_Permission
    {
        /// <summary>
        /// 文件编号
        /// </summary>
        public string File_ID { get; set; }

        /// <summary>
        /// 用户编号
        /// </summary>
        public string User_ID { get; set; }

        /// <summary>
        /// 用户类型
        /// </summary>
        public string User_Type { get; set; }

        /// <summary>
        /// 权限类型
        /// </summary>
        public PermissionType Permission_Type { get; set; }
    }
}
