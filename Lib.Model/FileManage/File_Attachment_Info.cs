﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.FileManage
{
    public class File_Attachment_Info
    {
        /// <summary>
        /// 文件编号
        /// </summary>
        public string File_ID { get; set; }

        /// <summary>
        /// 附件名称
        /// </summary>
        public string Attachment_Name { get; set; }

        /// <summary>
        /// 附件位置(服务器端)
        /// </summary>
        public string Attachment_Location { get; set; }

        /// <summary>
        /// 附件本地位置(用于上传时记录信息)
        /// </summary>
        public string Attachment_Local_Location { get; set; }

        /// <summary>
        /// 附件大小
        /// </summary>
        public double Attachment_Size { get; set; }

        /// <summary>
        /// 附件上传时间
        /// </summary>
        public DateTime Upload_Time { get; set; }

        /// <summary>
        /// 附件下载次数
        /// </summary>
        public int Attachment_Download_Times { get; set; }
    }
}
