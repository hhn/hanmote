﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.ServiceEvaluation
{
    public class mtGroupServiceModel
    {
        private String mtGroupName;
        private String mtGroupId;
        private String serviceTimeRate;
        private String serviceQualityRate;
        private String serviceInnovateRate;
        private String serviceReliableRate;
        private String serviceUserService;

        public string MtGroupName { get => mtGroupName; set => mtGroupName = value; }
        public string MtGroupId { get => mtGroupId; set => mtGroupId = value; }
        public string ServiceTimeRate { get => serviceTimeRate; set => serviceTimeRate = value; }
        public string ServiceQualityRate { get => serviceQualityRate; set => serviceQualityRate = value; }
        public string ServiceInnovateRate { get => serviceInnovateRate; set => serviceInnovateRate = value; }
        public string ServiceReliableRate { get => serviceReliableRate; set => serviceReliableRate = value; }
        public string ServiceUserService { get => serviceUserService; set => serviceUserService = value; }
    }
}
