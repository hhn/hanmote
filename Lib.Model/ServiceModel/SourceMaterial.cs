﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.ServiceModel
{
    //寻源物料
    public class SourceMaterial
    {
        public string Material_ID { get; set; }
        public string Material_Name { get; set; }
        public int Demand_Count { get; set; }
        public string Demand_ID { get; set; }
        public string Unit { get; set; }
        public string Factory_ID { get; set; }
        public string Stock_ID { get; set; }
        public string Source_ID { get; set; }

        public SourceMaterial()
        {
        }

        public SourceMaterial(string material_ID, string material_Name, int demand_Count, string demand_ID, string unit, string factory_ID, string stock_ID, DateTime deliveryStartTime, DateTime deliveryEndTime, string source_ID)
        {
            Material_ID = material_ID;
            Material_Name = material_Name;
            Demand_Count = demand_Count;
            Demand_ID = demand_ID;
            Unit = unit;
            Factory_ID = factory_ID;
            Stock_ID = stock_ID;
            Source_ID = source_ID;
        }

        public override bool Equals(object obj)
        {
            if(obj == null)
            {
                return false;
            }
            if(obj is SourceMaterial)
            {
                SourceMaterial sourceMaterial = (SourceMaterial)obj;
                return this.Material_ID.Equals(sourceMaterial.Material_ID);
            }
            return false;
        }

        public override int GetHashCode()
        {
            return this.Material_ID.GetHashCode() + this.Material_Name.GetHashCode();
        }
    }
}
