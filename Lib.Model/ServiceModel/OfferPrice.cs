﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.ServiceModel
{
    public class OfferPrice
    {
        public string Source_ID { get; set; }
        public string Supplier_ID { get; set; }
        public string Material_ID { get; set; }
        public float Price { get; set; }

        public OfferPrice()
        {
        }

        public OfferPrice(string source_ID, string supplier_ID, string material_ID, float price)
        {
            Source_ID = source_ID;
            Supplier_ID = supplier_ID;
            Material_ID = material_ID;
            Price = price;
        }

        public override bool Equals(object obj)
        {
            if(obj != null && obj is OfferPrice)
            {
                OfferPrice offerPrice = (OfferPrice)obj;
                return this.Material_ID.Equals(offerPrice.Material_ID) && this.Source_ID.Equals(offerPrice.Source_ID) && this.Supplier_ID.Equals(offerPrice.Supplier_ID) && this.Price == offerPrice.Price;
            }
            return false;
        }

        public override int GetHashCode()
        {
            return this.Source_ID.GetHashCode() + this.Material_ID.GetHashCode() + this.Supplier_ID.GetHashCode() + this.Price.GetHashCode();
        }
    }
}
