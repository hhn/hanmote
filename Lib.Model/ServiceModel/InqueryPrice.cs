﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.ServiceModel
{
    public class InqueryPrice
    {
        public string Source_ID { get; set; }
        public string Purchase_Org { get; set; }
        public string Buyer_Name { get; set; }
        public DateTime CreateTime { get; set; }
        public DateTime Offer_Time { get; set; }
        public string State { get; set; }
        public string Purchase_Group { get; set; }
        public string Inquiry_Name { get; set; }
        public string Material_Location { get; set; }
        public int Approve_Count { get; set; }

        public InqueryPrice()
        {
        }

        public InqueryPrice(string source_ID, string purchase_Org, string buyer_Name, DateTime createTime, DateTime offer_Time, string state, string purchase_Group, string inquiry_Name,string material_Location,int approve_Count)
        {
            Source_ID = source_ID;
            Purchase_Org = purchase_Org;
            Buyer_Name = buyer_Name;
            CreateTime = createTime;
            Offer_Time = offer_Time;
            State = state;
            Purchase_Group = purchase_Group;
            Inquiry_Name = inquiry_Name;
            Material_Location = material_Location;
            Approve_Count = approve_Count;
        }
    }
}
