﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model
{
    /// <summary>
    /// 认证状态
    /// </summary>
    public class CertificationStatus
    {
        /// <summary>
        /// 未做任何事情
        /// </summary>
        public const int Undo = 0;
        /// <summary>
        ///  物料品项定位
        /// </summary>
        public const int MaterialLocation = 1;

        /// <summary>
        /// 设置评估模板
        /// </summary>
        public const int SetTemplate = 2;

        /// <summary>
        ///  经过初选
        /// </summary>
        public const int AfterPresselection = 3;

        /// <summary>
        /// 协议与承诺
        /// </summary>
        public const int AgreementAndCommitement = 4;

        /// <summary>
        /// SEM评价
        /// </summary>
        public const int SEMEvaluation = 5;

        /// <summary>
        /// 改进验收
        /// </summary>
        public const int ImproveAndCheck = 6;



    }
}
