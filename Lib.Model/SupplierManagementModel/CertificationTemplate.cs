﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model
{
    /// <summary>
    /// 供应商认证模板实体类
    /// </summary>
    public class CertificationTemplate
    {
        public string tid { get; set; }

        public string pid { get; set; }

        public int isUse { get; set; }

        public string text { get; set; }

        public string others { get; set; }
    }
}
