﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.SupplierManagementModel
{
    /// <summary>
    /// 查询条件对象
    /// </summary>
    public class conditionInfo
    {
        private string purId;
        private string level;
        private DateTime sTime;
        private DateTime eTime;
        private string supplierName;
        private int pageIndex;
        private int pageSize;


        public string PurId { get => purId; set => purId = value; }
        public string SupplierName { get => supplierName; set => supplierName = value; }
        public int PageSize { get => pageSize; set => pageSize = value; }
        public int PageIndex { get => pageIndex; set => pageIndex = value; }
        public DateTime STime { get => sTime; set => sTime = value; }
        public DateTime ETime { get => eTime; set => eTime = value; }
        public string Level { get => level; set => level = value; }
    }
}
