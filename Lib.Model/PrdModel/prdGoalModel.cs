﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.PrdModel
{
    /// <summary>
    /// 产品供应目标类
    /// </summary>
    public class prdGoalModel
    {
        //供应目标编号
        private string goalId;
        //供应目标内容
        private string goalText;
        //供应目标分数
        private string goalSocre;
        //供应目标所属产品编号
        private string prdId;
        //供应目标pip等级
        private string goalPipLevel;
        //创建时间
        private DateTime createTime;
        //供应目标领域
        private string domain;

        public string GoalId { get => goalId; set => goalId = value; }
        public string GoalText { get => goalText; set => goalText = value; }
        public string GoalSocre { get => goalSocre; set => goalSocre = value; }
        public string PrdId { get => prdId; set => prdId = value; }
        public string GoalPipLevel { get => goalPipLevel; set => goalPipLevel = value; }
        public DateTime CreateTime { get => createTime; set => createTime = value; }
        public string Domain { get => domain; set => domain = value; }
    }
}
