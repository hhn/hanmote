﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.PrdModel
{
    /// <summary>
    /// 产品对象
    /// </summary>
    public class PrdModel
    {
        //产品编号
        private string prdId;
        //产品名称
        private string prdName;
        //组成产品的物料编号
        private string materialId;
        //组成产品的物料名称
        private string materialName;
        //产品目标内容
        private string goalText;
        //产品目标价值
        private string goalValue;
        //产品目标内容
        private string goalLevel;
        //产品目标领域
        private string domain;



        public string PrdId { get => prdId; set => prdId = value; }
        public string PrdName { get => prdName; set => prdName = value; }
        public string MaterialName { get => materialName; set => materialName = value; }
        public string MaterialId { get => materialId; set => materialId = value; }
        public string GoalValue { get => goalValue; set => goalValue = value; }
        public string GoalLevel { get => goalLevel; set => goalLevel = value; }
        public string Domain { get => domain; set => domain = value; }
        public string GoalText { get => goalText; set => goalText = value; }
    }
}
