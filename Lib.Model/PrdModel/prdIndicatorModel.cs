﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.PrdModel
{
    public class PrdIndicatorModel
    {
        //供应指标编号
        private string indicatorId;
        //供应指标内容
        private string indicatorText;
        //供应指标目标值
        private float indicatorScore;
        //指标所属物料
        private string materialId;
        //供应目标物料
        private string goalId;
        //指标等级值
        private string levelValue;
        //指标单位
        private string Unit;
        //指标pip等级
        private string pipLevel;
        //指标领域
        private string domain;


        public string IndicatorId { get => indicatorId; set => indicatorId = value; }
        public string IndicatorText { get => indicatorText; set => indicatorText = value; }
        public float IndicatorScore { get => indicatorScore; set => indicatorScore = value; }
        public string MaterialId { get => materialId; set => materialId = value; }
        public string GoalId { get => goalId; set => goalId = value; }
        public string PipLevel { get => pipLevel; set => pipLevel = value; }
        public string LevelValue { get => levelValue; set => levelValue = value; }
        public string Unit1 { get => Unit; set => Unit = value; }
        public string Domain { get => domain; set => domain = value; }
    }
}
