﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.MT_GroupModel
{
    public class GroupGoalModel
    {
        private string GoalId;
        private string GoalText;
        private float GoalValue;
        private string Unit;
        private float GoalLevelValue;
        private string GoalPipLevel;
        private string MtGroupId;
        private string domain;


        public string GoalId1 { get => GoalId; set => GoalId = value; }
        public string GoalText1 { get => GoalText; set => GoalText = value; }
        public float GoalValue1 { get => GoalValue; set => GoalValue = value; }
        public string Unit1 { get => Unit; set => Unit = value; }
        public float GoalLevelValue1 { get => GoalLevelValue; set => GoalLevelValue = value; }
        public string GoalPipLevel1 { get => GoalPipLevel; set => GoalPipLevel = value; }
        public string MtGroupId1 { get => MtGroupId; set => MtGroupId = value; }
        public string Domain { get => domain; set => domain = value; }
    }
}