﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.MT_GroupModel
{
    public class MtItemScoreInfoModle  
    {
        //采购组织编号
        private string purOrgId;
        //采购组织名称
        private string purOrgName;
        //物料编号
        private string id;
        //物料名称
        private string mtName;
        //物料目标编号
        private string aimId;
        //物料目标
        private string aim;
        //物料类别
        private string Category;
        //
        //物料指标
        private string global;
        //影响力得分
        private string influenScore;
        //得分
        private string riskScore;
        //影响力等级
        private string influenLevel;
        //风险等级
        private string riskLevel;
        //风险内容
        private string riskText;
        //机会等级
        private string chanceLevel;
        //机会得分
        private string chanceScore;
        //机会内容
        private string chanceText;
        //记录编号
        private string AutoId;


        public string Global { get => global; set => global = value; }
        public string Id { get => id; set => id = value; }
        public string Aim { get => aim; set => aim = value; }
        public string RiskScore { get => riskScore; set => riskScore = value; }
      
        public string RiskLevel { get => riskLevel; set => riskLevel = value; }
        public string InfluenScore { get => influenScore; set => influenScore = value; }
        public string InfluenLevel { get => influenLevel; set => influenLevel = value; }
        public string RiskText { get => riskText; set => riskText = value; }
        public string ChanceLevel { get => chanceLevel; set => chanceLevel = value; }
        public string ChanceText { get => chanceText; set => chanceText = value; }
        public string ChanceScore { get => chanceScore; set => chanceScore = value; }
        public string MtName { get => mtName; set => mtName = value; }
        public string Category1 { get => Category; set => Category = value; }
        public string PurOrgId { get => purOrgId; set => purOrgId = value; }
        public string PurOrgName { get => purOrgName; set => purOrgName = value; }
        public string AutoId1 { get => AutoId; set => AutoId = value; }
        public string AimId { get => aimId; set => aimId = value; }
    }
}
