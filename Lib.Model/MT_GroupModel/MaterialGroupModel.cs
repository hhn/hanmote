﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.MT_GroupModel
{
    /// <summary>
    /// 物料组类
    /// </summary>
   public class MaterialGroupModel
    {
        #region 物料组字段
        //物料组编号
        private string mtGroupId;
        //物料组名称
        private string mtGroupName;
        //物料组类型
        private string mtGroupClass;
        //物料组优先级
        private string mtGroupLevel;
        //物料组优先级值
        private string mtGroupLevelValue;
        //物料组认证绿色目标值
        private string cGreenValue;

        //物料组认证最低目标值
        private string cLowValue;
        //物料组绩效绿色目标值
        private string pGreenValue;
        //物料组绩效最低目标值
        private string pLowValue;
        //物料组所属工厂
        private string factoryId;

        public string MtGroupId { get => mtGroupId; set => mtGroupId = value; }
        public string MtGroupName { get => mtGroupName; set => mtGroupName = value; }
        public string MtGroupClass { get => mtGroupClass; set => mtGroupClass = value; }
        public string MtGroupLevel { get => mtGroupLevel; set => mtGroupLevel = value; }
        public string CGreenValue { get => cGreenValue; set => cGreenValue = value; }
        public string CLowValue { get => cLowValue; set => cLowValue = value; }
        public string PGreenValue { get => pGreenValue; set => pGreenValue = value; }
        public string PLowValue { get => pLowValue; set => pLowValue = value; }
        public string MtGroupLevelValue { get => mtGroupLevelValue; set => mtGroupLevelValue = value; }
        public string FactoryId { get => factoryId; set => factoryId = value; }
        #endregion



    }
}
