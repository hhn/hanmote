﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Model.MT_GroupModel
{
   public  class PosResultInfoModel
    {
        #region 定位结果模型
        private String purOrgId;//采购组织编号
        private String mtId;//物料组编号
        private String mtName;//物料组名称
        private float influAndRiskScore;//影响/供应风险得分
        private string cost;//支出占比
        private String posResult;//定位结果
        private string classifyType;

        public string MtName { get => mtName; set => mtName = value; }
        public string PosResult { get => posResult; set => posResult = value; }
        public float InfluAndRiskScore { get => influAndRiskScore; set => influAndRiskScore = value; }
        public string Cost { get => cost; set => cost = value; }
        public string ClassifyType { get => classifyType; set => classifyType = value; }
        public string PurOrgId { get => purOrgId; set => purOrgId = value; }
        public string MtId { get => mtId; set => mtId = value; }

        #endregion
    }
}
