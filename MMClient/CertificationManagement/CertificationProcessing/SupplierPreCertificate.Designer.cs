﻿namespace MMClient.CertificationManagement.CertificationProcessing
{
    partial class SupplierPreCertificate
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.select_button = new System.Windows.Forms.Button();
            this.state_comboBox = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.PurchaseOrg_textBox = new System.Windows.Forms.TextBox();
            this.物料组 = new System.Windows.Forms.Label();
            this.thing_textBox = new System.Windows.Forms.TextBox();
            this.pageNext1 = new pager.pagetool.pageNext();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.ControlLight;
            this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(0, 61);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersWidth = 20;
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.Size = new System.Drawing.Size(1138, 551);
            this.dataGridView1.TabIndex = 23;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.ThylxBtn_CellContentClick);
            this.dataGridView1.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.dataGridView1_RowPostPaint);
            // 
            // select_button
            // 
            this.select_button.Location = new System.Drawing.Point(732, 32);
            this.select_button.Name = "select_button";
            this.select_button.Size = new System.Drawing.Size(75, 23);
            this.select_button.TabIndex = 22;
            this.select_button.Text = "查询";
            this.select_button.UseVisualStyleBackColor = true;
            this.select_button.Click += new System.EventHandler(this.select_button_Click);
            // 
            // state_comboBox
            // 
            this.state_comboBox.FormattingEnabled = true;
            this.state_comboBox.Items.AddRange(new object[] {
            "中国",
            "美国",
            "日本",
            "德国"});
            this.state_comboBox.Location = new System.Drawing.Point(399, 33);
            this.state_comboBox.Name = "state_comboBox";
            this.state_comboBox.Size = new System.Drawing.Size(121, 20);
            this.state_comboBox.TabIndex = 21;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(397, 18);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(29, 12);
            this.label4.TabIndex = 20;
            this.label4.Text = "国家";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(588, 33);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 21);
            this.textBox1.TabIndex = 19;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(586, 19);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 12);
            this.label3.TabIndex = 18;
            this.label3.Text = "名称";
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(89, 16);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 12;
            this.label1.Text = "采购组织";
            // 
            // PurchaseOrg_textBox
            // 
            this.PurchaseOrg_textBox.Location = new System.Drawing.Point(91, 32);
            this.PurchaseOrg_textBox.Name = "PurchaseOrg_textBox";
            this.PurchaseOrg_textBox.Size = new System.Drawing.Size(100, 21);
            this.PurchaseOrg_textBox.TabIndex = 13;
            // 
            // 物料组
            // 
            this.物料组.AutoSize = true;
            this.物料组.Location = new System.Drawing.Point(242, 15);
            this.物料组.Name = "物料组";
            this.物料组.Size = new System.Drawing.Size(41, 12);
            this.物料组.TabIndex = 14;
            this.物料组.Text = "物料组";
            // 
            // thing_textBox
            // 
            this.thing_textBox.Location = new System.Drawing.Point(244, 32);
            this.thing_textBox.Name = "thing_textBox";
            this.thing_textBox.Size = new System.Drawing.Size(100, 21);
            this.thing_textBox.TabIndex = 15;
            // 
            // pageNext1
            // 
            this.pageNext1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.pageNext1.Location = new System.Drawing.Point(0, 618);
            this.pageNext1.Name = "pageNext1";
            this.pageNext1.PageIndex = 1;
            this.pageNext1.PageSize = 100;
            this.pageNext1.RecordCount = 0;
            this.pageNext1.Size = new System.Drawing.Size(660, 37);
            this.pageNext1.TabIndex = 24;
            this.pageNext1.OnPageChanged += new System.EventHandler(this.pageNext1_OnPageChanged);
            // 
            // SupplierPreCertificate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1137, 657);
            this.Controls.Add(this.pageNext1);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.select_button);
            this.Controls.Add(this.state_comboBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.thing_textBox);
            this.Controls.Add(this.物料组);
            this.Controls.Add(this.PurchaseOrg_textBox);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "SupplierPreCertificate";
            this.Text = "供应商预评";
            this.Load += new System.EventHandler(this.SupplierPreCertificate_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button select_button;
        private System.Windows.Forms.ComboBox state_comboBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox PurchaseOrg_textBox;
        private System.Windows.Forms.Label 物料组;
        private System.Windows.Forms.TextBox thing_textBox;
        private pager.pagetool.pageNext pageNext1;
    }
}