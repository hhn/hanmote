﻿namespace MMClient
{
    partial class SelfCerAndBaseInfoDecision_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label3 = new System.Windows.Forms.Label();
            this.save_btn = new System.Windows.Forms.Button();
            this.decision_cmb = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.describe_rtb = new System.Windows.Forms.RichTextBox();
            this.sendDecision_btn = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.suppliername_txt = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("宋体", 9F);
            this.label3.Location = new System.Drawing.Point(68, 233);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 12);
            this.label3.TabIndex = 168;
            this.label3.Text = "评审结果";
            // 
            // save_btn
            // 
            this.save_btn.Location = new System.Drawing.Point(554, 283);
            this.save_btn.Name = "save_btn";
            this.save_btn.Size = new System.Drawing.Size(89, 24);
            this.save_btn.TabIndex = 167;
            this.save_btn.Text = "保存";
            this.save_btn.UseVisualStyleBackColor = true;
            this.save_btn.Click += new System.EventHandler(this.save_btn_Click);
            // 
            // decision_cmb
            // 
            this.decision_cmb.FormattingEnabled = true;
            this.decision_cmb.ItemHeight = 12;
            this.decision_cmb.Items.AddRange(new object[] {
            "通过，进入公司评审",
            "不通过"});
            this.decision_cmb.Location = new System.Drawing.Point(159, 233);
            this.decision_cmb.Name = "decision_cmb";
            this.decision_cmb.Size = new System.Drawing.Size(200, 20);
            this.decision_cmb.TabIndex = 166;
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("宋体", 9F);
            this.label1.Location = new System.Drawing.Point(68, 72);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 169;
            this.label1.Text = "评审描述";
            // 
            // describe_rtb
            // 
            this.describe_rtb.Location = new System.Drawing.Point(159, 72);
            this.describe_rtb.Name = "describe_rtb";
            this.describe_rtb.Size = new System.Drawing.Size(344, 124);
            this.describe_rtb.TabIndex = 170;
            this.describe_rtb.Text = "";
            // 
            // sendDecision_btn
            // 
            this.sendDecision_btn.Location = new System.Drawing.Point(434, 283);
            this.sendDecision_btn.Name = "sendDecision_btn";
            this.sendDecision_btn.Size = new System.Drawing.Size(89, 24);
            this.sendDecision_btn.TabIndex = 171;
            this.sendDecision_btn.Text = "发送决策通知";
            this.sendDecision_btn.UseVisualStyleBackColor = true;
            this.sendDecision_btn.Click += new System.EventHandler(this.sendDecision_btn_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("宋体", 9F);
            this.label2.Location = new System.Drawing.Point(68, 24);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 12);
            this.label2.TabIndex = 172;
            this.label2.Text = "供应商名称";
            // 
            // suppliername_txt
            // 
            this.suppliername_txt.Enabled = false;
            this.suppliername_txt.Location = new System.Drawing.Point(159, 24);
            this.suppliername_txt.Name = "suppliername_txt";
            this.suppliername_txt.Size = new System.Drawing.Size(234, 21);
            this.suppliername_txt.TabIndex = 173;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(434, 24);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(89, 24);
            this.button1.TabIndex = 174;
            this.button1.Text = "查看自评结果";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("宋体", 9F);
            this.label4.Location = new System.Drawing.Point(68, 283);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(305, 12);
            this.label4.TabIndex = 175;
            this.label4.Text = "注：对供应商的自评结果和供应商基本信息的评估决策。";
            // 
            // SelfCerAndBaseInfoDecision_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(673, 330);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.suppliername_txt);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.sendDecision_btn);
            this.Controls.Add(this.describe_rtb);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.save_btn);
            this.Controls.Add(this.decision_cmb);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SelfCerAndBaseInfoDecision_Form";
            this.Text = "评估决策";
            this.Load += new System.EventHandler(this.SelfCerAndBaseInfoDecision_Form_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button save_btn;
        private System.Windows.Forms.ComboBox decision_cmb;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RichTextBox describe_rtb;
        private System.Windows.Forms.Button sendDecision_btn;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox suppliername_txt;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label4;
    }
}