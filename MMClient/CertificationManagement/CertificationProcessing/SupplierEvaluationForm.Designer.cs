﻿namespace MMClient
{
    partial class SupplierEvaluationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.companyname_cmb = new System.Windows.Forms.ComboBox();
            this.label17 = new System.Windows.Forms.Label();
            this.filename_txt = new System.Windows.Forms.TextBox();
            this.importFile_btn = new System.Windows.Forms.Button();
            this.totalScore_txt = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.save_btn = new System.Windows.Forms.Button();
            this.others_rtb = new System.Windows.Forms.RichTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tips_lbl = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // companyname_cmb
            // 
            this.companyname_cmb.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.companyname_cmb.FormattingEnabled = true;
            this.companyname_cmb.Location = new System.Drawing.Point(120, 24);
            this.companyname_cmb.Name = "companyname_cmb";
            this.companyname_cmb.Size = new System.Drawing.Size(213, 25);
            this.companyname_cmb.TabIndex = 50;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label17.Location = new System.Drawing.Point(46, 30);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(56, 17);
            this.label17.TabIndex = 49;
            this.label17.Text = "公司名称";
            // 
            // filename_txt
            // 
            this.filename_txt.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.filename_txt.Location = new System.Drawing.Point(374, 24);
            this.filename_txt.Name = "filename_txt";
            this.filename_txt.Size = new System.Drawing.Size(249, 23);
            this.filename_txt.TabIndex = 51;
            // 
            // importFile_btn
            // 
            this.importFile_btn.Location = new System.Drawing.Point(667, 24);
            this.importFile_btn.Name = "importFile_btn";
            this.importFile_btn.Size = new System.Drawing.Size(75, 23);
            this.importFile_btn.TabIndex = 52;
            this.importFile_btn.Text = "导入...";
            this.importFile_btn.UseVisualStyleBackColor = true;
            this.importFile_btn.Click += new System.EventHandler(this.importFile_btn_Click);
            // 
            // totalScore_txt
            // 
            this.totalScore_txt.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.totalScore_txt.Location = new System.Drawing.Point(120, 80);
            this.totalScore_txt.Name = "totalScore_txt";
            this.totalScore_txt.Size = new System.Drawing.Size(213, 23);
            this.totalScore_txt.TabIndex = 53;
            this.totalScore_txt.TextChanged += new System.EventHandler(this.totalScore_txt_TextChanged);
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(46, 83);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(70, 17);
            this.label1.TabIndex = 54;
            this.label1.Text = "SEM总得分";
            // 
            // save_btn
            // 
            this.save_btn.Location = new System.Drawing.Point(374, 280);
            this.save_btn.Name = "save_btn";
            this.save_btn.Size = new System.Drawing.Size(75, 23);
            this.save_btn.TabIndex = 56;
            this.save_btn.Text = "保存并上传";
            this.save_btn.UseVisualStyleBackColor = true;
            this.save_btn.Click += new System.EventHandler(this.save_btn_Click);
            // 
            // others_rtb
            // 
            this.others_rtb.Location = new System.Drawing.Point(120, 124);
            this.others_rtb.Name = "others_rtb";
            this.others_rtb.Size = new System.Drawing.Size(231, 124);
            this.others_rtb.TabIndex = 178;
            this.others_rtb.Text = "";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("宋体", 9F);
            this.label2.Location = new System.Drawing.Point(49, 124);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 177;
            this.label2.Text = "其他说明";
            // 
            // tips_lbl
            // 
            this.tips_lbl.AutoSize = true;
            this.tips_lbl.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tips_lbl.Location = new System.Drawing.Point(371, 86);
            this.tips_lbl.Name = "tips_lbl";
            this.tips_lbl.Size = new System.Drawing.Size(0, 17);
            this.tips_lbl.TabIndex = 180;
            // 
            // SupplierEvaluationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(874, 341);
            this.Controls.Add(this.tips_lbl);
            this.Controls.Add(this.others_rtb);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.save_btn);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.totalScore_txt);
            this.Controls.Add(this.importFile_btn);
            this.Controls.Add(this.filename_txt);
            this.Controls.Add(this.companyname_cmb);
            this.Controls.Add(this.label17);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "SupplierEvaluationForm";
            this.Text = "供应商能力评估";
            this.Load += new System.EventHandler(this.SupplierEvaluationForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox companyname_cmb;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox filename_txt;
        private System.Windows.Forms.Button importFile_btn;
        private System.Windows.Forms.TextBox totalScore_txt;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button save_btn;
        private System.Windows.Forms.RichTextBox others_rtb;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label tips_lbl;

    }
}