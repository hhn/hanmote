﻿using Lib.Common.CommonUtils;
using Lib.SqlServerDAL;
using Lib.SqlServerDAL.CertificationProcess;
using MMClient.CommFileShow;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using WeifenLuo.WinFormsUI.Docking;
using Lib.Bll.CertificationProcess.LeadDecide;
namespace MMClient.CertificationManagement.CertificationProcessing
{
    public partial class PerimissionSupplierInfo2 : DockContent
    {
        FileUploadIDAL fileHelper = new FileUploadIDAL();
        string supplierID;
        FTPHelper fTPHelper = new FTPHelper("");
        FileShowForm fileShowForm = null;
        LeadDecidebll leadDecidebll = new LeadDecidebll();
        public PerimissionSupplierInfo2(string id)
        {
            InitializeComponent();
            supplierID = id;
            intial(id);


        }

        private void PerimissionSupplierInfo2_Load(object sender, EventArgs e)
        {
            dgvFileInfo.DataSource = FileUploadIDAL.getFileInfo(supplierID);
            this.tbFileID.Text = supplierID;
            this.cbxFileType.Text = "全部文件";
        }
        private string[] getFile(string filepath)
        {

            string[] filenames = fTPHelper.GetFileList(filepath);
            return filenames;

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        private void intial(string id)
        {
            supplierID = id;
            DataTable dt = leadDecidebll.SelectById(id);
            if (dt != null)
            {
                this.LabelDBsCode.Text = dt.Rows[0][0].ToString();
                this.labelCompanyNAme.Text = dt.Rows[0][1].ToString();
                this.labelContactMan.Text = dt.Rows[0][2].ToString();
                this.LabelPosition.Text = dt.Rows[0][3].ToString();
                this.labelMobile.Text = dt.Rows[0][4].ToString();
                this.labelTelPhone.Text = dt.Rows[0][5].ToString();
                this.labelEmail.Text = dt.Rows[0][6].ToString();
                this.labelUrlAddress.Text = dt.Rows[0][7].ToString();
                this.labelAddress.Text = dt.Rows[0][8].ToString();
                this.labelPostCode.Text = dt.Rows[0][9].ToString();
            }
        }
            /// <summary>
            /// 搜索文档
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            private void btnSearch_Click_1(object sender, EventArgs e)
        {
            String start = dtpBeginTime.Value.ToString("yyyy-MM-dd 00:00:00");
            String end = dtpEndTime.Value.ToString("yyyy-MM-dd 23:59:59");
           dgvFileInfo.DataSource= fileHelper.searchFile(this.cbxFileType.Text, supplierID,start,end,tbCreatorName.Text);
        }
        /// <summary>
        /// 查看文档
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void fileViewBtn_Click_1(object sender, EventArgs e)
        {
            List<string> fileIDList = new List<string>();
            foreach (DataGridViewRow row in this.dgvFileInfo.Rows)
            {
                string isSelected = row.Cells[0].EditedFormattedValue.ToString();
                if (isSelected.Equals("True"))
                {
                    fileIDList.Add(row.Cells[1].Value.ToString());
                    fileIDList.Add(row.Cells["filePath"].Value.ToString());
                }
            }

            if (fileIDList.Count == 0)
            {
                MessageUtil.ShowError("请选择查看文件");
                return;
            }
            if (fileIDList.Count > 2)
            {
                MessageUtil.ShowError("不要选择多个文件");
                return;
            }

            string fileName = fileIDList[0];
            string filePath = fileIDList[1];
            string selectedPath = System.Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            selectedPath = selectedPath.Replace("\\", "/") + "/";
            try {
                if (this.fileShowForm == null || this.fileShowForm.IsDisposed)
                {
                    this.fileShowForm =  new FileShowForm(selectedPath + fileName);
                }

             

            } catch (Exception) {

                fTPHelper.Download(selectedPath, fileName, filePath + fileName);
                if (this.fileShowForm == null || this.fileShowForm.IsDisposed)
                {
                    this.fileShowForm = new FileShowForm(selectedPath + fileName);
                }
            }
            SingletonUserUI.addToUserUI(fileShowForm);

        }

        /// <summary>
        /// 多文件下载
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="fileName"></param>

        public String downLoad(List<string> filePath, List<string> fileName)
        {
            string mess = "请选择要下载的文件";

            FolderBrowserDialog BrowDialog = new FolderBrowserDialog();
            BrowDialog.ShowNewFolderButton = true;
            BrowDialog.Description = "请选择保存位置";

            if (BrowDialog.ShowDialog() == DialogResult.OK)
            {
                //此处表示用户点击了“确认”

                string pathname = BrowDialog.SelectedPath;

                if (pathname != null)
                {
                    for (int i = 0; i < fileName.Count; i++)
                    {

                        mess = fTPHelper.Download(pathname, fileName[i], filePath[i] + fileName[i]);

                    }

                }


            }
            else
            {
                mess = "请选择目录";
            }
            return mess;
            
        }
        /// <summary>
        /// 文件下载
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click_1(object sender, EventArgs e)
        {
            List<string> fileIDList = new List<string>();
            List<string> filePath = new List<string>();

            foreach (DataGridViewRow row in this.dgvFileInfo.Rows)
            {
                string isSelected = row.Cells[0].EditedFormattedValue.ToString();
                if (isSelected.Equals("True"))
                {
                    fileIDList.Add(row.Cells[1].Value.ToString());
                    filePath.Add(row.Cells["filePath"].Value.ToString());
 
                }
            }

            if (fileIDList.Count == 0)
            {
                MessageUtil.ShowError("请选择查看文件");
                return;
            }

           string mess = downLoad(filePath, fileIDList);
            MessageUtil.ShowTips(mess);
        }

     
    }
}
