﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using System.Text;

using Lib.Bll.CertificationProcess.General;
using Lib.Common.CommonUtils;
using System.Net.Mail;

namespace MMClient.CertificationManagement.CertificationProcessing
{
    public partial class CheckStatus1 : Form
    {
        private string id;
        public CheckStatus1(string id)
        {
            this.id = id;
            InitializeComponent();
        }

        private void CheckStatus1_Load(object sender, EventArgs e)
        {
            GeneralBLL gn = new GeneralBLL();
            DataTable dt = gn.GetAllInfo3(this.id);
            dataGridView1.DataSource = dt;
            /*DataGridViewCheckBoxColumn ChCol = new DataGridViewCheckBoxColumn();
            ChCol.Name = "CheckBoxRow";
            ChCol.HeaderText = "操作";
            ChCol.Width = 50;
            ChCol.TrueValue = "1";
            ChCol.FalseValue = "0";
            //dataGridView1.Columns.Insert(0, ChCol);
            dataGridView1.Columns.Add(ChCol);*/
        }
        private void dataGridView1_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)//自动编号的一列
        {
            Rectangle rectangle = new Rectangle(e.RowBounds.Location.X,
                e.RowBounds.Location.Y,
                dataGridView1.RowHeadersWidth - 4,
                e.RowBounds.Height);
            TextRenderer.DrawText(e.Graphics, (e.RowIndex + 1).ToString(),
                dataGridView1.RowHeadersDefaultCellStyle.Font,
                rectangle,
                dataGridView1.RowHeadersDefaultCellStyle.ForeColor,
                TextFormatFlags.VerticalCenter | TextFormatFlags.Right);
        }
        private void button1_Click(object sender, EventArgs e)//查看评估结果
        {
            SupplierPreCertificationResult nm = new SupplierPreCertificationResult(this.id);
            nm.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
