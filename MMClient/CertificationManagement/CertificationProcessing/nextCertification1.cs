﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Lib.Bll.CertificationProcess.General;
using Lib.Common.CommonUtils;
using System.Net.Mail;
using System.Text.RegularExpressions;

namespace MMClient.CertificationManagement.CertificationProcessing
{
    public partial class nextCertification1 : Form
    {
        string User_ID = SingleUserInstance.getCurrentUserInstance().User_ID;//主审id
        private string email;
        private string id;
        private string reason;
        private string thing;
        private string sname;
        public nextCertification1(string email, string id,string reason,string thing, string sname)
        {
            InitializeComponent();
            this.email = email;
            this.id = id;
            this.reason = reason;
            this.thing = thing;
            this.sname = sname;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string filename1 = null;
            filename1 = label2.Text;
            if (filename1 == null|| filename1 == "")
            {
                MessageUtil.ShowWarning("请添加邀请函！");
                return;
            }
            else
            {
                GeneralBLL gn = new GeneralBLL();
                gn.deal1(reason, this.id);
                string body = this.richTextBox1.Text;
                List<MailAddress> addresses = new List<MailAddress>();
                string subject = "告知用户进入现场评估";
                if (string.IsNullOrWhiteSpace(this.richTextBox1.Text))
                {
                    MessageUtil.ShowWarning("通知理由不能为空！");
                    return;
                }

                string toAddresses = this.email;

                MailMessage message = new MailMessage();
                message.From = new MailAddress("342706245@qq.com", "342706245");//必须是提供smtp服务的邮件服务器 
                                                                                /*if (toAddresses != null && toAddresses.Count > 0)
                                                                                    for (int i = 0; i < toAddresses.Count; i++)*/
                message.To.Add(toAddresses);
                message.Subject = subject;

                message.IsBodyHtml = true;
                message.BodyEncoding = System.Text.Encoding.UTF8;
                message.Body = body;
                Attachment att = new Attachment(filename1);
                message.Attachments.Add(att);//添加附件

                message.Priority = System.Net.Mail.MailPriority.High;
                SmtpClient client = new SmtpClient("smtp.qq.com", 25); // 587;//Gmail使用的端口 
                client.Credentials = new System.Net.NetworkCredential("342706245@qq.com", "fcjreozibygqcbbf"); //这里是申请的邮箱和密码 
                client.EnableSsl = true; //必须经过ssl加密 
                try
                {
                    client.Send(message);

                    MessageUtil.ShowTips("邮件已经成功发送" + message.To.ToString());

                }
                catch (Exception ee)
                {
                    MessageUtil.ShowError(ee.Message  /* + ee.InnerException.Message*/ );

                }

                string zzid = sname + "预评审已完结待现场评审";
                //string sname1 = sname + "待主审预评";
                string sname2 = sname + "预评审已完结待提交现场评审";
                string zhuid = null;
                DataTable dt = gn.Getzhuid(sname + "待筛选");
                zhuid = dt.Rows[0][0].ToString();
                gn.adeletetask(zhuid, zzid);
                gn.inserttask(zhuid, zzid);
                //gn.Updatetask(zhuid, sname1);
                gn.Updatetask(zhuid, sname2);


                gn.RestartInf1(id);//处理状态置1
                //插一条主审数据
                gn.prdelete(id, User_ID);
                int score = 0;
                int sum = 0;
                DataTable dt2 = gn.getscore(this.id);
                for (int i = 0; i < dt2.Rows.Count; i++)
                    score = score + int.Parse(dt2.Rows[i][0].ToString());
                DataTable dt3 = gn.getsum(this.id);
                for (int i = 0; i < dt2.Rows.Count; i++)
                    sum = sum + int.Parse(dt3.Rows[i][0].ToString());

                string ascore;
                string bsum;
                ascore = Convert.ToString(score);
                bsum = Convert.ToString(sum);
                gn.deal1(this.id,this.thing, User_ID, ascore, bsum);

            }
        }

        private void button2_Click(object sender, EventArgs e)//添加邀请函
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.Multiselect = true;
            openFileDialog1.InitialDirectory = "D:\\Patch";
            openFileDialog1.Filter = "All files (*.*)|*.*|txt files (*.txt)|*.txt";
            openFileDialog1.FilterIndex = 1;
            openFileDialog1.RestoreDirectory = true;
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                string[] sArray5 = Regex.Split(openFileDialog1.FileName, @"\\", RegexOptions.IgnoreCase);
                filename.Text = sArray5[sArray5.Length - 1];
                label2.Text = openFileDialog1.FileName;

            }
           // filename.Text = openFileDialog.FileName;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
