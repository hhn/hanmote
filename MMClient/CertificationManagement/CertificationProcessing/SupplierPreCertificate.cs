﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using System.Text;

using Lib.Bll.CertificationProcess.General;
using Lib.Common.CommonUtils;

namespace MMClient.CertificationManagement.CertificationProcessing
{
    public partial class SupplierPreCertificate : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        public SupplierPreCertificate()
        {
            InitializeComponent();
        }

        private void select_button_Click(object sender, EventArgs e)
        {
            string status = "";
            //status = status_comboBox.Text;
            string country = "";
            country = state_comboBox.Text;
            string name = "";
            string purchase = "";
            purchase = PurchaseOrg_textBox.Text;
            string thingGroup = "";
            thingGroup = thing_textBox.Text;
            name = textBox1.Text;
            GeneralBLL gn = new GeneralBLL();
            DataTable dt = gn.GetInfo1(SingleUserInstance.getCurrentUserInstance().User_ID, purchase, thingGroup, status, country, name);
            if (dt.Rows.Count == 0)
            {
                this.pageNext1.PageIndex = 0;
            }
            else {

                this.pageNext1.PageIndex = 1;
            }
            pageNext1.DrawControl(dt.Rows.Count);
            dataGridView1.DataSource = dt;
        }

        private void SupplierPreCertificate_Load(object sender, EventArgs e)
        {
            LoadData();
            DataGridViewButtonColumn button1 = new DataGridViewButtonColumn();
            button1.Name = "button1";
            button1.Text = "选择评审员";//加上这两个就能显示
            button1.UseColumnTextForButtonValue = true;//
            button1.Width = 100;
            button1.HeaderText = "选择评审员";
            this.dataGridView1.Columns.AddRange(button1);

            DataGridViewButtonColumn button2 = new DataGridViewButtonColumn();
            button2.Name = "button2";
            button2.Text = "查看评审状态";//加上这两个就能显示
            button2.UseColumnTextForButtonValue = true;//
            button2.Width = 100;
            button2.HeaderText = "查看评审状态";
            this.dataGridView1.Columns.AddRange(button2);



            /*DataGridViewButtonColumn button3 = new DataGridViewButtonColumn();
            button3.Name = "button3";
            button3.Text = "发送邮件";//加上这两个就能显示
            button3.UseColumnTextForButtonValue = true;//
            button3.Width = 100;
            button3.HeaderText = "发送邮件";
            this.dataGridView1.Columns.AddRange(button3);*/

            DataGridViewButtonColumn button4 = new DataGridViewButtonColumn();
            button4.Name = "button4";
            button4.Text = "拒绝";//加上这两个就能显示
            button4.UseColumnTextForButtonValue = true;//
            button4.Width = 50;
            button4.HeaderText = "拒绝";
            this.dataGridView1.Columns.AddRange(button4);
        }

        private void dataGridView1_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)//自动编号的一列
        {
            Rectangle rectangle = new Rectangle(e.RowBounds.Location.X,
                e.RowBounds.Location.Y,
                dataGridView1.RowHeadersWidth - 4,
                e.RowBounds.Height);
            TextRenderer.DrawText(e.Graphics, (e.RowIndex + 1).ToString(),
                dataGridView1.RowHeadersDefaultCellStyle.Font,
                rectangle,
                dataGridView1.RowHeadersDefaultCellStyle.ForeColor,
                TextFormatFlags.VerticalCenter | TextFormatFlags.Right);
        }

        private void ThylxBtn_CellContentClick(object sender, DataGridViewCellEventArgs e)//三个操作之一对应的事件跳转代码
        {

            if (e.RowIndex >= 0)
            {

                if (dataGridView1.Columns[e.ColumnIndex].Name == "button1")//建立评估小组
                {
                    string id = dataGridView1.CurrentRow.Cells["供应商编号"].Value.ToString();
                    GeneralBLL gn = new GeneralBLL();
                    if (gn.fGetStatus(id))
                    {
                        
                        if (gn.GetStatus1(id))
                        {
                            BuildCertificationGroup nm = new BuildCertificationGroup(id, dataGridView1.CurrentRow.Cells["供应商名称"].Value.ToString());
                            nm.Show();
                        }
                        else
                        {
                            string zzid = dataGridView1.CurrentRow.Cells["供应商名称"].Value.ToString() + "待预评审";
                            MessageBoxButtons messButton = MessageBoxButtons.OKCancel;
                            //"确定要退出吗？"是对话框的显示信息，"退出系统"是对话框的标题
                            //默认情况下，如MessageBox.Show("确定要退出吗？")只显示一个“确定”按钮。
                            DialogResult dr = MessageBox.Show("确定要重新建立评估员小组吗吗?", "重新建立评估员小组", messButton);
                            if (dr == DialogResult.OK)//如果点击“确定”按钮
                            {
                                string sname1 = dataGridView1.CurrentRow.Cells["供应商名称"].Value.ToString() + "待主审预评";
                                gn.Updatetask1(SingleUserInstance.getCurrentUserInstance().User_ID, sname1);
                                gn.deleteInfo(id);
                                gn.adeletetask1(zzid);
                                BuildCertificationGroup nm = new BuildCertificationGroup(id, dataGridView1.CurrentRow.Cells["供应商名称"].Value.ToString());
                                nm.Show();
                            }
                            else//如果点击“取消”按钮
                            {

                            }
                        }
                    }
                    else
                    {
                        MessageUtil.ShowWarning("供应商未上传文件！");
                        return;
                    }
                }
                if (dataGridView1.Columns[e.ColumnIndex].Name == "button2")//查看评审状态
                {

                    string id = dataGridView1.CurrentRow.Cells["供应商编号"].Value.ToString();
                    GeneralBLL gn = new GeneralBLL();
                    if (gn.ffGetStatus(id))
                    {
                        if (gn.GetStatus(id))
                        {
                            CheckStatus1 nm = new CheckStatus1(id);
                            nm.Show();
                        }
                        else
                        {
                            CheckStatus nm = new CheckStatus(id);
                            nm.Show();
                        }
                    }
                    else
                    {
                        MessageUtil.ShowWarning("请先建立评估小组！");
                        return;
                    }

                }
                if (dataGridView1.Columns[e.ColumnIndex].Name == "button3")//单击发送邮件对应事件
                {
                    string email = dataGridView1.CurrentRow.Cells["电子邮件"].Value.ToString();
                   
                        sendemail nm = new sendemail(email);
                        nm.Show();
                    
                }
                if (dataGridView1.Columns[e.ColumnIndex].Name == "button4")//单击拒绝对应事件
                {
                    string email = dataGridView1.CurrentRow.Cells["电子邮件"].Value.ToString();
                    string id = dataGridView1.CurrentRow.Cells["供应商编号"].Value.ToString();
                    GeneralBLL gn = new GeneralBLL();
                    denyFormprez nm = new denyFormprez(email, id);
                    nm.Show();

                    /* string id = dataGridView1.CurrentRow.Cells[3].Value.ToString();
                     GeneralBLL gn = new GeneralBLL();
                     gn.RestartInfo(id);
                     DataTable dt = gn.GetAllInfo();
                     dataGridView1.DataSource = dt;*/

                }
            }

        }

        private void bindingNavigator1_RefreshItems(object sender, EventArgs e)
        {

        }

        private void pageNext1_OnPageChanged(object sender, EventArgs e)
        {
            LoadData();
        }


        public void LoadData()
        {
            GeneralBLL gn = new GeneralBLL();
            int totalSize = 0;
            DataTable dt = gn.GetAllInfo1(SingleUserInstance.getCurrentUserInstance().User_ID, this.pageNext1.PageSize, this.pageNext1.PageIndex, out totalSize);
            dataGridView1.DataSource = dt;
            pageNext1.DrawControl(totalSize);
        }
    }
}
