﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WHC.Pager.Entity;
using WHC.Pager.WinControl;
using Lib.SqlServerDAL;
using Lib.Common.CommonUtils;
using Lib.Bll;
using WeifenLuo.WinFormsUI.Docking;
using Lib.Model;

namespace MMClient
{
    public partial class SupplierPreselectionForm : DockContent
    {

        public SupplierPreselectionForm()
        {
            InitializeComponent();
        }

        private void SupplierPreselectionForm_Load(object sender, EventArgs e)
        {
            //this.winGridViewPager1.ProgressBar = this.toolStripProgressBar1.ProgressBar;
            this.winGridViewPager1.OnPageChanged += new EventHandler(winGridViewPager1_OnPageChanged);
            this.winGridViewPager1.OnStartExport += new EventHandler(winGridViewPager1_OnStartExport);
            this.winGridViewPager1.OnEditSelected += new EventHandler(winGridViewPager1_OnEditSelected);
            this.winGridViewPager1.OnDeleteSelected += new EventHandler(winGridViewPager1_OnDeleteSelected);
            this.winGridViewPager1.OnRefresh += new EventHandler(winGridViewPager1_OnRefresh);
            this.winGridViewPager1.OnAddNew += new EventHandler(winGridViewPager1_OnAddNew);
            //this.winGridViewPager1.AppendedMenu = this.contextMenuStrip1;//追加额外菜单项目
            this.winGridViewPager1.ShowLineNumber = true;//显示行号
            this.winGridViewPager1.PagerInfo.PageSize = 10;//页面大小
            this.winGridViewPager1.EventRowBackColor = Color.LightCyan;//间隔颜色
            this.BindData();
        }

        private void InitCompanyName()
        {
            this.companyname_txt.Text = "";
        }

        private void InitCountry()
        {
            this.country_cmb.Text = "";
            this.country_cmb.Items.Clear();
            this.country_cmb.Items.Add("中国");
            this.country_cmb.Items.Add("日本");
            this.country_cmb.Items.Add("德国");
            this.country_cmb.Items.Add("美国");
        }

        private void InitCompanyProperty()
        {
            this.companyProperty_cmb.Text = "";
            this.companyProperty_cmb.Items.Clear();
            this.companyProperty_cmb.Items.Add("国有企业");
            this.companyProperty_cmb.Items.Add("私营企业");
        }

        private void BindData()
        {
            #region 添加别名解析
            //DisplayColumns与显示的字段名或者实体属性一致，大小写不敏感，顺序代表显示顺序，用逗号或者|分开
            this.winGridViewPager1.DisplayColumns = "supplierChineseName,country,companyProperty,zipCode,city,phonenum1,phonenum2";
            this.winGridViewPager1.AddColumnAlias("ID", "编号");
            this.winGridViewPager1.AddColumnAlias("supplierChineseName", "供应商名称");
            this.winGridViewPager1.AddColumnAlias("country","国家");
            this.winGridViewPager1.AddColumnAlias("companyProperty", "公司性质");
            this.winGridViewPager1.AddColumnAlias("zipCode","邮编");
            this.winGridViewPager1.AddColumnAlias("city","城市");
            this.winGridViewPager1.AddColumnAlias("phonenum1","联系电话1");
            this.winGridViewPager1.AddColumnAlias("phonenum2","联系电话2");
            
            #endregion
            
            string where = GetSearchSql();
            this.winGridViewPager1.DataSource = BLLFactory<OrganizationInfoBLL>.Instance.Find(where, this.winGridViewPager1.PagerInfo);
            this.winGridViewPager1.dataGridView1.Columns[1].Width = 200;
        }


        private void winGridViewPager1_OnRefresh(object sender, EventArgs e)
        {
            BindData();
        }

        private void winGridViewPager1_OnDeleteSelected(object sender, EventArgs e)
        {
            if (MessageUtil.ShowYesNoAndTips("您确定删除选定的记录么？") == DialogResult.No)
            {
                return;
            }

            DataGridView grid = this.winGridViewPager1.dataGridView1;
            if (grid != null)
            {
                foreach (DataGridViewRow row in grid.SelectedRows)
                {
                    //BLLFactory<Customer>.Instance.Delete(curRow.Cells[0].Value.ToString());
                }
                BindData();
            }
        }

        private void winGridViewPager1_OnEditSelected(object sender, EventArgs e)
        {
            DataGridView grid = this.winGridViewPager1.dataGridView1;
            if (grid != null)
            {
                foreach (DataGridViewRow row in grid.SelectedRows)
                {
                    SupplierDetailsForm dlg = new SupplierDetailsForm();
                    dlg.ID = row.Cells[0].Value.ToString();
                    if (DialogResult.OK == dlg.ShowDialog())
                    {
                        BindData();
                    }

                    break;
                }
            }
        }



        private void winGridViewPager1_OnAddNew(object sender, EventArgs e)
        {
            //btnAddNew_Click(null, null);
        }

        private void winGridViewPager1_OnStartExport(object sender, EventArgs e)
        {
            string where = GetSearchSql();
            this.winGridViewPager1.AllToExport = FindToDataTable(where, this.winGridViewPager1.PagerInfo);
        }

        private void winGridViewPager1_OnPageChanged(object sender, EventArgs e)
        {
            BindData();
        }


        /// <summary>
        /// 标准的记录查询函数
        /// </summary>
        /// <param name="where"></param>
        /// <param name="pagerInfo"></param>
        /// <returns></returns>
        private DataTable FindToDataTable(string where, PagerInfo pagerInfo)
        {
            WHC.Pager.WinControl.PagerHelper helper = new WHC.Pager.WinControl.PagerHelper("Org_company_baseinfo", "*", "supplierChineseName", pagerInfo.PageSize, pagerInfo.CurrenetPageIndex, true, where);
            string countSql = helper.GetPagingSql(WHC.Pager.WinControl.DatabaseType.SqlServer, true);
            string dataSql = helper.GetPagingSql(WHC.Pager.WinControl.DatabaseType.SqlServer, false);

            string value = SqlValueList(countSql);
            pagerInfo.RecordCount = Convert.ToInt32(value);//为了显示具体的信息，需要设置总记录数
            DataTable dt = SqlTable(dataSql);
            return dt;
        }

        /// <summary>    
        /// 执行SQL查询语句，返回查询结果的所有记录的第一个字段,用逗号分隔。    
        /// </summary>    
        /// <param name="sql">SQL语句</param>    
        /// <returns>    
        /// 返回查询结果的所有记录的第一个字段,用逗号分隔。    
        /// </returns>    
        public string SqlValueList(string sql)
        {
            StringBuilder result = new StringBuilder();
            using (IDataReader dr = DBHelper.ExecuteReader(sql))
            {
                while (dr.Read())
                {
                    result.AppendFormat("{0},", dr[0].ToString());
                }
            }
            string strResult = result.ToString().Trim(',');
            return strResult;
        }

        /// <summary>    
        /// 执行SQL查询语句，返回所有记录的DataTable集合。    
        /// </summary>    
        /// <param name="sql">SQL查询语句</param>    
        /// <returns></returns>    
        public DataTable SqlTable(string sql)
        {
            //Database db = DatabaseFactory.CreateDatabase();
            //DbCommand command = db.GetSqlStringCommand(sql);
            //return db.ExecuteDataSet(command).Tables[0];
            return DBHelper.ExecuteQueryDT(sql);
        }

        /// <summary>
        /// 根据查询条件构造查询语句
        /// </summary>
        /// <returns></returns>
        private string GetSearchSql()
        {
            
            SearchCondition condition = new SearchCondition();
            condition.AddCondition("supplierChineseName", this.companyname_txt.Text, SqlOperator.Like)
                .AddCondition("country", this.country_cmb.Text, SqlOperator.Equal)
                .AddCondition("companyProperty", this.companyProperty_cmb.Text, SqlOperator.Equal);
            string where = condition.BuildConditionSql().Replace("Where","");

            return where;
        }

        /// <summary>
        /// 查询按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            BindData();
        }
        /// <summary>
        /// 查看详情
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void seeDetails_btn_Click(object sender, EventArgs e)
        {
            DataGridView grid = this.winGridViewPager1.dataGridView1;
            if (grid != null)
            {
                int count = grid.SelectedRows.Count;
                int index = grid.CurrentRow.Index;
                if (count == 1)
                {
                    List<OrganizationInfo> list = (List<OrganizationInfo>)grid.SelectedRows[0].DataGridView.DataSource;
                    SupplierDetailsForm sdFrm = new SupplierDetailsForm(list[index]);
                    sdFrm.StartPosition = FormStartPosition.CenterScreen;
                    sdFrm.ShowDialog();
                }
                else
                {
                    MessageUtil.ShowError("请选中一条记录！！！");
                }
                
            }
            else
            {
                MessageUtil.ShowError("数据为空！！！");
            }
        }
        /// <summary>
        /// 清除条件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            InitCompanyName();
            InitCountry();
            InitCompanyProperty();

            BindData();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            string companyName = "";
            DataGridView grid = this.winGridViewPager1.dataGridView1;
            if (grid != null)
            {
                int count = grid.SelectedRows.Count;
                int index = grid.CurrentRow.Index;
                if (count == 1)
                {
                    List<OrganizationInfo> list = (List<OrganizationInfo>)grid.SelectedRows[0].DataGridView.DataSource;
                    companyName = list[index].SupplierChineseName;
                }
                else
                {
                    MessageUtil.ShowError("请选中一条记录！！！");
                }

            }
            else
            {
                MessageUtil.ShowError("数据为空！！！");
            }
            List<OrgContacts> listContacts = BLLFactory<SupplierManagementBLL>.Instance.getOrgContactsByCompanyName(companyName);
            SelfCerAndBaseInfoDecision_Form scFrm = new SelfCerAndBaseInfoDecision_Form(listContacts, companyName);
            scFrm.StartPosition = FormStartPosition.CenterScreen;
            scFrm.ShowDialog();
        }

        private void winGridViewPager1_Load(object sender, EventArgs e)
        {

        }
    }
}
