﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;
using Lib.Model;
using Lib.Bll;
using System.IO;
using Lib.Common.CommonUtils;
using MMClient.CommonForms;


namespace MMClient
{
    public partial class SupplierEvaluationForm : DockContent
    {

        private static SupplierManagementBLL sbll = new SupplierManagementBLL();
        private DataTable companyDT = sbll.getAllCompany();
        private string safeFileName;
        private string localFileName;

        public SupplierEvaluationForm()
        {
            InitializeComponent();
        }

        private void SupplierEvaluationForm_Load(object sender, EventArgs e)
        {
            FormHelper.combox_bind(this.companyname_cmb, companyDT, "supplierChineseName", "supplierChineseName");
            string companyName = this.companyname_cmb.Text;
            List<SEMModel> list = sbll.getSEMScoreByCompanyName(companyName);
            if (list != null&& list.Count > 0)
            {
                this.others_rtb.Text = list[0].Others;
                this.totalScore_txt.Text = list[0].TotalSocre.ToString();
                this.filename_txt.Text = list[0].FilePath;
            }
        }

        private void importFile_btn_Click(object sender, EventArgs e)
        {
            //打开文件选择框
            OpenFileDialog ofd = new OpenFileDialog();
            if (ofd.ShowDialog() == DialogResult.Cancel)
            {
                return;
            }
            string fileName = ofd.FileName;
            this.localFileName = fileName;
            safeFileName = ofd.SafeFileName;
            this.filename_txt.Text = fileName;
            string result = ExcelUtil.ReadCellValueToString(fileName, "详细结果-Detailed scoring", "Applicable\npoints");
            this.totalScore_txt.Text = result;
        }

        private void save_btn_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(this.companyname_cmb.Text))
            {
                MessageUtil.ShowError("供应商名称不能为空");
                return;
            }

            if (string.IsNullOrWhiteSpace(this.totalScore_txt.Text))
            {
                MessageUtil.ShowError("得分不能为空");
                return;
            }
            string supplierName =  this.companyname_cmb.Text;
            int flag = sbll.CheckProcessing(CertificationStatus.SEMEvaluation, supplierName);
            if (flag == 1)
            {
                MessageUtil.ShowWarning("请先完成前面操作！！！");
                return;
            }
            else if (flag == -1)
            {
                if (MessageUtil.ShowYesNoAndWarning("是否重新评估？") == DialogResult.No)
                {
                    return;
                }
                else
                {
                    if (!sbll.ModifyProcessingStatus(CertificationStatus.SEMEvaluation, supplierName))
                    {
                        MessageUtil.ShowWarning("操作失败！！！");
                        return;
                    }

                }
            }
            SEMModel obj = new SEMModel();
            obj.SupplierName = supplierName;
            obj.TotalSocre = Convert.ToDouble(this.totalScore_txt.Text);
            if (string.IsNullOrWhiteSpace(this.others_rtb.Text))
            {
                obj.Others = "";
            }
            obj.Others = this.others_rtb.Text;
            obj.FileName = this.safeFileName;
            string filePath = createFilePath(this.companyname_cmb.Text,this.safeFileName);
            obj.FilePath = filePath;
            double score = Convert.ToDouble(this.totalScore_txt.Text);
            if (score < SEMScore.FirstLadderScore && score >= SEMScore.SecondLadderScore)
            {
                obj.Level = SEMLevel.B;
            }
            else if (score >= SEMScore.FirstLadderScore)
            {
                obj.Level = SEMLevel.A;
            }
            else
            {
                obj.Level = SEMLevel.C;
            }
            obj.Status = CertificationStatus.SEMEvaluation;
            if(sbll.saveSEMScore(obj)&&saveFileOnServer(filePath))
            {
                MessageUtil.ShowTips("保存成功！！！");
            }
            else {
                MessageUtil.ShowTips("保存失败！！！");
            }
        }

        private String createFilePath(string supplierName,string safeFileName)
        {
            string supplierID = sbll.getSupplierIdBySupplierName(supplierName);
            string filePath = supplierID + "/" + safeFileName;
            return filePath;
        }

        

        private bool saveFileOnServer(string filePath)
        {
            FTPTool ftp = FTPTool.getInstance();
            bool isConnect = ftp.serverIsConnect();
            if (!isConnect)
            {
                MessageUtil.ShowError("无法连接服务器!");
                return false;
            }
            Dictionary<string, string> uploadRLDic = new Dictionary<string, string>();
            double allFilesSize = 0.0;
            uploadRLDic.Add(filePath, this.localFileName);
            allFilesSize = FileUtils.GetFileSizeToDouble(this.localFileName);
            LinkedList<string> uploadFailList = new LinkedList<string>();
            ProgressForm progress = new ProgressForm(uploadFailList);
            progress.RLFileDic = uploadRLDic;
            progress.AllFilesSize = allFilesSize;
            progress.OpType = "upload";
            progress.ShowDialog();
            //LinkedList<string> list = ftp.upload(uploadRLDic, allFilesSize, new BackgroundWorker(), new DoWorkEventArgs(""));
            //bool flag = list == null || list.Count <= 0 ? false : true;
            return true;
        }

        private void readExcel()
        { 
            
        }

        private void totalScore_txt_TextChanged(object sender, EventArgs e)
        {
            double score = Convert.ToDouble(this.totalScore_txt.Text);
            if (score < 90.0 && score >= 60.0)
            {
                this.tips_lbl.ForeColor = Color.Red;
                this.tips_lbl.Text = "请及时审查行动计划";
            }
        }

        

    }
}
