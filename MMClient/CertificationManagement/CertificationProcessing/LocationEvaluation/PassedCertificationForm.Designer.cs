﻿namespace MMClient.CertificationManagement.CertificationProcessing.LocationEvaluation
{
    partial class PassedCertificationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.topPanel = new System.Windows.Forms.Panel();
            this.contentPanel = new System.Windows.Forms.Panel();
            this.LocationalcertificatedTable = new System.Windows.Forms.DataGridView();
            this.pageNext1 = new pager.pagetool.pageNext();
            this.headerPanel = new System.Windows.Forms.Panel();
            this.PurGroupName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.ResetInfoList = new System.Windows.Forms.Button();
            this.NationalName = new System.Windows.Forms.TextBox();
            this.CompanyNameInput = new System.Windows.Forms.TextBox();
            this.MtGroupName = new System.Windows.Forms.TextBox();
            this.ReseachByCondition = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.Reset = new System.Windows.Forms.Button();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Agree = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SupplierId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.companyName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Country = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ContactName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PhoneNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ContactMail = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Mt_GroupName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.准入 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.拒绝 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.topPanel.SuspendLayout();
            this.contentPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LocationalcertificatedTable)).BeginInit();
            this.headerPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // topPanel
            // 
            this.topPanel.Controls.Add(this.contentPanel);
            this.topPanel.Controls.Add(this.headerPanel);
            this.topPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.topPanel.Location = new System.Drawing.Point(0, 0);
            this.topPanel.Name = "topPanel";
            this.topPanel.Size = new System.Drawing.Size(1193, 482);
            this.topPanel.TabIndex = 45;
            // 
            // contentPanel
            // 
            this.contentPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.contentPanel.Controls.Add(this.LocationalcertificatedTable);
            this.contentPanel.Controls.Add(this.pageNext1);
            this.contentPanel.Location = new System.Drawing.Point(0, 67);
            this.contentPanel.Name = "contentPanel";
            this.contentPanel.Size = new System.Drawing.Size(1193, 412);
            this.contentPanel.TabIndex = 1;
            // 
            // LocationalcertificatedTable
            // 
            this.LocationalcertificatedTable.AllowUserToAddRows = false;
            this.LocationalcertificatedTable.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LocationalcertificatedTable.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.LocationalcertificatedTable.BackgroundColor = System.Drawing.SystemColors.ControlLight;
            this.LocationalcertificatedTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.LocationalcertificatedTable.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Agree,
            this.SupplierId,
            this.companyName,
            this.Country,
            this.ContactName,
            this.PhoneNumber,
            this.ContactMail,
            this.Mt_GroupName,
            this.准入,
            this.拒绝});
            this.LocationalcertificatedTable.Location = new System.Drawing.Point(3, 3);
            this.LocationalcertificatedTable.Name = "LocationalcertificatedTable";
            this.LocationalcertificatedTable.RowTemplate.Height = 23;
            this.LocationalcertificatedTable.Size = new System.Drawing.Size(1187, 359);
            this.LocationalcertificatedTable.TabIndex = 2;
            this.LocationalcertificatedTable.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.LocationalcertificatedTable_CellContentClick);
            // 
            // pageNext1
            // 
            this.pageNext1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pageNext1.Location = new System.Drawing.Point(3, 368);
            this.pageNext1.Name = "pageNext1";
            this.pageNext1.PageIndex = 1;
            this.pageNext1.PageSize = 100;
            this.pageNext1.RecordCount = 0;
            this.pageNext1.Size = new System.Drawing.Size(649, 37);
            this.pageNext1.TabIndex = 1;
            // 
            // headerPanel
            // 
            this.headerPanel.Controls.Add(this.PurGroupName);
            this.headerPanel.Controls.Add(this.label1);
            this.headerPanel.Controls.Add(this.ResetInfoList);
            this.headerPanel.Controls.Add(this.NationalName);
            this.headerPanel.Controls.Add(this.CompanyNameInput);
            this.headerPanel.Controls.Add(this.MtGroupName);
            this.headerPanel.Controls.Add(this.ReseachByCondition);
            this.headerPanel.Controls.Add(this.label3);
            this.headerPanel.Controls.Add(this.label4);
            this.headerPanel.Controls.Add(this.label6);
            this.headerPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.headerPanel.Location = new System.Drawing.Point(0, 0);
            this.headerPanel.Name = "headerPanel";
            this.headerPanel.Size = new System.Drawing.Size(1193, 61);
            this.headerPanel.TabIndex = 0;
            // 
            // PurGroupName
            // 
            this.PurGroupName.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.PurGroupName.Location = new System.Drawing.Point(654, 19);
            this.PurGroupName.Name = "PurGroupName";
            this.PurGroupName.Size = new System.Drawing.Size(82, 23);
            this.PurGroupName.TabIndex = 63;
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.label1.Location = new System.Drawing.Point(602, 21);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(56, 17);
            this.label1.TabIndex = 62;
            this.label1.Text = "采购组：";
            // 
            // ResetInfoList
            // 
            this.ResetInfoList.Location = new System.Drawing.Point(1021, 30);
            this.ResetInfoList.Name = "ResetInfoList";
            this.ResetInfoList.Size = new System.Drawing.Size(74, 23);
            this.ResetInfoList.TabIndex = 61;
            this.ResetInfoList.Text = "重  置";
            this.ResetInfoList.UseVisualStyleBackColor = true;
            this.ResetInfoList.Click += new System.EventHandler(this.ResetInfoList_Click);
            // 
            // NationalName
            // 
            this.NationalName.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.NationalName.Location = new System.Drawing.Point(251, 16);
            this.NationalName.Name = "NationalName";
            this.NationalName.Size = new System.Drawing.Size(85, 23);
            this.NationalName.TabIndex = 60;
            // 
            // CompanyNameInput
            // 
            this.CompanyNameInput.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.CompanyNameInput.Location = new System.Drawing.Point(444, 16);
            this.CompanyNameInput.Name = "CompanyNameInput";
            this.CompanyNameInput.Size = new System.Drawing.Size(85, 23);
            this.CompanyNameInput.TabIndex = 59;
            // 
            // MtGroupName
            // 
            this.MtGroupName.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.MtGroupName.Location = new System.Drawing.Point(80, 17);
            this.MtGroupName.Name = "MtGroupName";
            this.MtGroupName.Size = new System.Drawing.Size(82, 23);
            this.MtGroupName.TabIndex = 58;
            // 
            // ReseachByCondition
            // 
            this.ReseachByCondition.Location = new System.Drawing.Point(917, 30);
            this.ReseachByCondition.Name = "ReseachByCondition";
            this.ReseachByCondition.Size = new System.Drawing.Size(69, 23);
            this.ReseachByCondition.TabIndex = 57;
            this.ReseachByCondition.Text = "查  询";
            this.ReseachByCondition.UseVisualStyleBackColor = true;
            this.ReseachByCondition.Click += new System.EventHandler(this.ReseachByCondition_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.label3.Location = new System.Drawing.Point(203, 20);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 17);
            this.label3.TabIndex = 56;
            this.label3.Text = "国  家：";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.label4.Location = new System.Drawing.Point(379, 20);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(68, 17);
            this.label4.TabIndex = 55;
            this.label4.Text = "公司名称：";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.label6.Location = new System.Drawing.Point(28, 19);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(56, 17);
            this.label6.TabIndex = 54;
            this.label6.Text = "物料组：";
            // 
            // Reset
            // 
            this.Reset.Location = new System.Drawing.Point(289, 25);
            this.Reset.Name = "Reset";
            this.Reset.Size = new System.Drawing.Size(83, 23);
            this.Reset.TabIndex = 44;
            this.Reset.Text = "重  置";
            this.Reset.UseVisualStyleBackColor = true;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.dataGridViewTextBoxColumn1.DataPropertyName = "SupplierId";
            this.dataGridViewTextBoxColumn1.Frozen = true;
            this.dataGridViewTextBoxColumn1.HeaderText = "供应商编号";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Width = 102;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.dataGridViewTextBoxColumn2.DataPropertyName = "companyName";
            this.dataGridViewTextBoxColumn2.Frozen = true;
            this.dataGridViewTextBoxColumn2.HeaderText = "公司名称";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.Width = 102;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.dataGridViewTextBoxColumn3.DataPropertyName = "Country";
            this.dataGridViewTextBoxColumn3.Frozen = true;
            this.dataGridViewTextBoxColumn3.HeaderText = "区  域";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.Width = 102;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.dataGridViewTextBoxColumn4.DataPropertyName = "ContactName";
            this.dataGridViewTextBoxColumn4.Frozen = true;
            this.dataGridViewTextBoxColumn4.HeaderText = "联系人";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.Width = 102;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.dataGridViewTextBoxColumn5.DataPropertyName = "PhoneNumber";
            this.dataGridViewTextBoxColumn5.Frozen = true;
            this.dataGridViewTextBoxColumn5.HeaderText = "联系方式";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.Width = 102;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.dataGridViewTextBoxColumn6.DataPropertyName = "ContactMail";
            this.dataGridViewTextBoxColumn6.Frozen = true;
            this.dataGridViewTextBoxColumn6.HeaderText = "电子邮箱";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.Width = 102;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.dataGridViewTextBoxColumn7.DataPropertyName = "MtGroupName";
            this.dataGridViewTextBoxColumn7.Frozen = true;
            this.dataGridViewTextBoxColumn7.HeaderText = "物料组名称";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.Width = 102;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.dataGridViewTextBoxColumn8.DataPropertyName = "MtGroupName";
            this.dataGridViewTextBoxColumn8.Frozen = true;
            this.dataGridViewTextBoxColumn8.HeaderText = "操    作";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.Width = 103;
            // 
            // Agree
            // 
            this.Agree.DataPropertyName = "Agree";
            this.Agree.HeaderText = "状态";
            this.Agree.Name = "Agree";
            this.Agree.ToolTipText = "状态";
            // 
            // SupplierId
            // 
            this.SupplierId.DataPropertyName = "SupplierId";
            this.SupplierId.HeaderText = "供应商编号";
            this.SupplierId.Name = "SupplierId";
            this.SupplierId.ToolTipText = "供应商编号";
            // 
            // companyName
            // 
            this.companyName.DataPropertyName = "companyName";
            this.companyName.HeaderText = "公司名称";
            this.companyName.Name = "companyName";
            this.companyName.ToolTipText = "公司名称";
            // 
            // Country
            // 
            this.Country.DataPropertyName = "Country";
            this.Country.HeaderText = "区域";
            this.Country.Name = "Country";
            this.Country.ToolTipText = "区域";
            // 
            // ContactName
            // 
            this.ContactName.DataPropertyName = "ContactName";
            this.ContactName.HeaderText = "联系人";
            this.ContactName.Name = "ContactName";
            this.ContactName.ToolTipText = "联系人";
            // 
            // PhoneNumber
            // 
            this.PhoneNumber.DataPropertyName = "PhoneNumber";
            this.PhoneNumber.HeaderText = "联系方式";
            this.PhoneNumber.Name = "PhoneNumber";
            this.PhoneNumber.ToolTipText = "联系方式";
            // 
            // ContactMail
            // 
            this.ContactMail.DataPropertyName = "ContactMail";
            this.ContactMail.HeaderText = "电子邮箱";
            this.ContactMail.Name = "ContactMail";
            this.ContactMail.ToolTipText = "电子邮箱";
            // 
            // Mt_GroupName
            // 
            this.Mt_GroupName.DataPropertyName = "Mt_GroupName";
            this.Mt_GroupName.HeaderText = "物料组";
            this.Mt_GroupName.Name = "Mt_GroupName";
            this.Mt_GroupName.ToolTipText = "物料组";
            // 
            // 准入
            // 
            this.准入.HeaderText = "操作";
            this.准入.Name = "准入";
            this.准入.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.准入.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.准入.ToolTipText = "操作";
            // 
            // 拒绝
            // 
            this.拒绝.HeaderText = "拒绝";
            this.拒绝.Name = "拒绝";
            // 
            // PassedCertificationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1193, 482);
            this.Controls.Add(this.topPanel);
            this.Controls.Add(this.Reset);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "PassedCertificationForm";
            this.Text = "供应商准入列表";
            this.topPanel.ResumeLayout(false);
            this.contentPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LocationalcertificatedTable)).EndInit();
            this.headerPanel.ResumeLayout(false);
            this.headerPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel topPanel;
        private System.Windows.Forms.Panel contentPanel;
        private System.Windows.Forms.Panel headerPanel;
        private System.Windows.Forms.Button ResetInfoList;
        private System.Windows.Forms.TextBox NationalName;
        private System.Windows.Forms.TextBox CompanyNameInput;
        private System.Windows.Forms.Button ReseachByCondition;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button Reset;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.TextBox PurGroupName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox MtGroupName;
        private System.Windows.Forms.Label label6;
        private pager.pagetool.pageNext pageNext1;
        private System.Windows.Forms.DataGridView LocationalcertificatedTable;
        private System.Windows.Forms.DataGridViewTextBoxColumn Agree;
        private System.Windows.Forms.DataGridViewTextBoxColumn SupplierId;
        private System.Windows.Forms.DataGridViewTextBoxColumn companyName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Country;
        private System.Windows.Forms.DataGridViewTextBoxColumn ContactName;
        private System.Windows.Forms.DataGridViewTextBoxColumn PhoneNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn ContactMail;
        private System.Windows.Forms.DataGridViewTextBoxColumn Mt_GroupName;
        private System.Windows.Forms.DataGridViewButtonColumn 准入;
        private System.Windows.Forms.DataGridViewButtonColumn 拒绝;
    }
}