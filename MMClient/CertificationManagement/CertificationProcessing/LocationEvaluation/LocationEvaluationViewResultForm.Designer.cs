﻿namespace MMClient.CertificationManagement.CertificationProcessing.LocationEvaluation
{
    partial class LocationEvaluationViewResultForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.headerPanel = new System.Windows.Forms.Panel();
            this.labelPostCode = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.labelAddress = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.labelUrlAddress = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.labelEmail = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.labelTelPhone = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.labelMobile = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.LabelPosition = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.labelContactMan = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.labelCompanyNAme = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.LabelDBsCode = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.BackBtn = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.LocationalCompanyTable = new System.Windows.Forms.DataGridView();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.companyName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Eval_score = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EvalSumScore = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Eval_Item = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DenyItems = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Eval_ReasonInfo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.backEval = new System.Windows.Forms.DataGridViewButtonColumn();
            this.label14 = new System.Windows.Forms.Label();
            this.submitBtn = new System.Windows.Forms.Button();
            this.label33 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.getFileName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.saveBtn = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.upload = new System.Windows.Forms.Button();
            this.checkedListBox1 = new System.Windows.Forms.CheckedListBox();
            this.button1 = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.ViewBtn = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.label15 = new System.Windows.Forms.Label();
            this.textBoxAdvice = new System.Windows.Forms.TextBox();
            this.headerPanel.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LocationalCompanyTable)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // headerPanel
            // 
            this.headerPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.headerPanel.Controls.Add(this.labelPostCode);
            this.headerPanel.Controls.Add(this.label8);
            this.headerPanel.Controls.Add(this.labelAddress);
            this.headerPanel.Controls.Add(this.label12);
            this.headerPanel.Controls.Add(this.labelUrlAddress);
            this.headerPanel.Controls.Add(this.label6);
            this.headerPanel.Controls.Add(this.labelEmail);
            this.headerPanel.Controls.Add(this.label9);
            this.headerPanel.Controls.Add(this.labelTelPhone);
            this.headerPanel.Controls.Add(this.label11);
            this.headerPanel.Controls.Add(this.labelMobile);
            this.headerPanel.Controls.Add(this.label13);
            this.headerPanel.Controls.Add(this.LabelPosition);
            this.headerPanel.Controls.Add(this.label5);
            this.headerPanel.Controls.Add(this.labelContactMan);
            this.headerPanel.Controls.Add(this.label7);
            this.headerPanel.Controls.Add(this.labelCompanyNAme);
            this.headerPanel.Controls.Add(this.label4);
            this.headerPanel.Controls.Add(this.LabelDBsCode);
            this.headerPanel.Controls.Add(this.label2);
            this.headerPanel.Controls.Add(this.label1);
            this.headerPanel.Location = new System.Drawing.Point(0, 1);
            this.headerPanel.Name = "headerPanel";
            this.headerPanel.Size = new System.Drawing.Size(959, 146);
            this.headerPanel.TabIndex = 1;
            // 
            // labelPostCode
            // 
            this.labelPostCode.AutoSize = true;
            this.labelPostCode.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labelPostCode.Location = new System.Drawing.Point(684, 94);
            this.labelPostCode.Name = "labelPostCode";
            this.labelPostCode.Size = new System.Drawing.Size(41, 12);
            this.labelPostCode.TabIndex = 20;
            this.labelPostCode.Text = "label3";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(613, 94);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(41, 12);
            this.label8.TabIndex = 19;
            this.label8.Text = "邮编：";
            // 
            // labelAddress
            // 
            this.labelAddress.AutoSize = true;
            this.labelAddress.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labelAddress.Location = new System.Drawing.Point(151, 120);
            this.labelAddress.Name = "labelAddress";
            this.labelAddress.Size = new System.Drawing.Size(77, 12);
            this.labelAddress.TabIndex = 18;
            this.labelAddress.Text = "labelAddress";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(68, 120);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(41, 12);
            this.label12.TabIndex = 17;
            this.label12.Text = "地址：";
            // 
            // labelUrlAddress
            // 
            this.labelUrlAddress.AutoSize = true;
            this.labelUrlAddress.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labelUrlAddress.Location = new System.Drawing.Point(418, 94);
            this.labelUrlAddress.Name = "labelUrlAddress";
            this.labelUrlAddress.Size = new System.Drawing.Size(41, 12);
            this.labelUrlAddress.TabIndex = 16;
            this.labelUrlAddress.Text = "label3";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(347, 94);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 12);
            this.label6.TabIndex = 15;
            this.label6.Text = "网址：";
            // 
            // labelEmail
            // 
            this.labelEmail.AutoSize = true;
            this.labelEmail.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labelEmail.Location = new System.Drawing.Point(696, 64);
            this.labelEmail.Name = "labelEmail";
            this.labelEmail.Size = new System.Drawing.Size(41, 12);
            this.labelEmail.TabIndex = 14;
            this.labelEmail.Text = "label3";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(613, 64);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(41, 12);
            this.label9.TabIndex = 13;
            this.label9.Text = "邮箱：";
            // 
            // labelTelPhone
            // 
            this.labelTelPhone.AutoSize = true;
            this.labelTelPhone.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labelTelPhone.Location = new System.Drawing.Point(139, 94);
            this.labelTelPhone.Name = "labelTelPhone";
            this.labelTelPhone.Size = new System.Drawing.Size(41, 12);
            this.labelTelPhone.TabIndex = 12;
            this.labelTelPhone.Text = "label3";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(68, 94);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(41, 12);
            this.label11.TabIndex = 11;
            this.label11.Text = "传真：";
            // 
            // labelMobile
            // 
            this.labelMobile.AutoSize = true;
            this.labelMobile.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labelMobile.Location = new System.Drawing.Point(151, 64);
            this.labelMobile.Name = "labelMobile";
            this.labelMobile.Size = new System.Drawing.Size(41, 12);
            this.labelMobile.TabIndex = 10;
            this.labelMobile.Text = "label3";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(68, 64);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(65, 12);
            this.label13.TabIndex = 9;
            this.label13.Text = "电话号码：";
            // 
            // LabelPosition
            // 
            this.LabelPosition.AutoSize = true;
            this.LabelPosition.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.LabelPosition.Location = new System.Drawing.Point(418, 64);
            this.LabelPosition.Name = "LabelPosition";
            this.LabelPosition.Size = new System.Drawing.Size(41, 12);
            this.LabelPosition.TabIndex = 8;
            this.LabelPosition.Text = "label3";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(347, 64);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 12);
            this.label5.TabIndex = 7;
            this.label5.Text = "职位：";
            // 
            // labelContactMan
            // 
            this.labelContactMan.AutoSize = true;
            this.labelContactMan.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labelContactMan.Location = new System.Drawing.Point(696, 34);
            this.labelContactMan.Name = "labelContactMan";
            this.labelContactMan.Size = new System.Drawing.Size(41, 12);
            this.labelContactMan.TabIndex = 6;
            this.labelContactMan.Text = "label3";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(613, 34);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 12);
            this.label7.TabIndex = 5;
            this.label7.Text = "联系人：";
            // 
            // labelCompanyNAme
            // 
            this.labelCompanyNAme.AutoSize = true;
            this.labelCompanyNAme.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labelCompanyNAme.Location = new System.Drawing.Point(418, 34);
            this.labelCompanyNAme.Name = "labelCompanyNAme";
            this.labelCompanyNAme.Size = new System.Drawing.Size(41, 12);
            this.labelCompanyNAme.TabIndex = 4;
            this.labelCompanyNAme.Text = "label3";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(347, 34);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 12);
            this.label4.TabIndex = 3;
            this.label4.Text = "公司名称：";
            // 
            // LabelDBsCode
            // 
            this.LabelDBsCode.AutoSize = true;
            this.LabelDBsCode.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.LabelDBsCode.Location = new System.Drawing.Point(151, 34);
            this.LabelDBsCode.Name = "LabelDBsCode";
            this.LabelDBsCode.Size = new System.Drawing.Size(41, 12);
            this.LabelDBsCode.TabIndex = 2;
            this.LabelDBsCode.Text = "label3";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(68, 34);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 12);
            this.label2.TabIndex = 1;
            this.label2.Text = "邓白氏编码：";
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("华文新魏", 15F);
            this.label1.Location = new System.Drawing.Point(22, 8);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(110, 21);
            this.label1.TabIndex = 0;
            this.label1.Text = "基本信息：";
            // 
            // BackBtn
            // 
            this.BackBtn.Location = new System.Drawing.Point(605, 826);
            this.BackBtn.Name = "BackBtn";
            this.BackBtn.Size = new System.Drawing.Size(85, 30);
            this.BackBtn.TabIndex = 26;
            this.BackBtn.Text = "退  出";
            this.BackBtn.UseVisualStyleBackColor = true;
            this.BackBtn.Click += new System.EventHandler(this.button1_Click);
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.Controls.Add(this.LocationalCompanyTable);
            this.panel2.Location = new System.Drawing.Point(0, 174);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(959, 127);
            this.panel2.TabIndex = 3;
            // 
            // LocationalCompanyTable
            // 
            this.LocationalCompanyTable.AllowUserToAddRows = false;
            this.LocationalCompanyTable.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LocationalCompanyTable.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.LocationalCompanyTable.BackgroundColor = System.Drawing.SystemColors.ControlLight;
            this.LocationalCompanyTable.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.LocationalCompanyTable.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.LocationalCompanyTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.LocationalCompanyTable.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ID,
            this.companyName,
            this.Eval_score,
            this.EvalSumScore,
            this.Eval_Item,
            this.DenyItems,
            this.Eval_ReasonInfo,
            this.backEval});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.LocationalCompanyTable.DefaultCellStyle = dataGridViewCellStyle3;
            this.LocationalCompanyTable.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.LocationalCompanyTable.GridColor = System.Drawing.SystemColors.ControlLight;
            this.LocationalCompanyTable.Location = new System.Drawing.Point(8, 8);
            this.LocationalCompanyTable.Name = "LocationalCompanyTable";
            this.LocationalCompanyTable.RowTemplate.Height = 23;
            this.LocationalCompanyTable.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.LocationalCompanyTable.Size = new System.Drawing.Size(948, 127);
            this.LocationalCompanyTable.TabIndex = 3;
            this.LocationalCompanyTable.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.LocationalCompanyTable_CellContentClick);
            // 
            // ID
            // 
            this.ID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.ID.DataPropertyName = "ID";
            this.ID.Frozen = true;
            this.ID.HeaderText = "编号";
            this.ID.Name = "ID";
            this.ID.Width = 85;
            // 
            // companyName
            // 
            this.companyName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.companyName.DataPropertyName = "name";
            this.companyName.Frozen = true;
            this.companyName.HeaderText = "评审员名称";
            this.companyName.Name = "companyName";
            // 
            // Eval_score
            // 
            this.Eval_score.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.Eval_score.DataPropertyName = "Eval_score";
            this.Eval_score.Frozen = true;
            this.Eval_score.HeaderText = "评估分数";
            this.Eval_score.Name = "Eval_score";
            this.Eval_score.Width = 80;
            // 
            // EvalSumScore
            // 
            this.EvalSumScore.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.EvalSumScore.DataPropertyName = "EvalSumScore";
            this.EvalSumScore.Frozen = true;
            this.EvalSumScore.HeaderText = "该项总分";
            this.EvalSumScore.Name = "EvalSumScore";
            this.EvalSumScore.Width = 80;
            // 
            // Eval_Item
            // 
            this.Eval_Item.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.Eval_Item.DataPropertyName = "Eval_Item";
            this.Eval_Item.Frozen = true;
            this.Eval_Item.HeaderText = "评估方面";
            this.Eval_Item.Name = "Eval_Item";
            this.Eval_Item.Width = 80;
            // 
            // DenyItems
            // 
            this.DenyItems.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.DenyItems.DataPropertyName = "DenyItems";
            this.DenyItems.Frozen = true;
            this.DenyItems.HeaderText = "否决项";
            this.DenyItems.Name = "DenyItems";
            // 
            // Eval_ReasonInfo
            // 
            this.Eval_ReasonInfo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.Eval_ReasonInfo.DataPropertyName = "Eval_ReasonInfo";
            this.Eval_ReasonInfo.Frozen = true;
            this.Eval_ReasonInfo.HeaderText = "评估理由";
            this.Eval_ReasonInfo.Name = "Eval_ReasonInfo";
            this.Eval_ReasonInfo.Width = 300;
            // 
            // backEval
            // 
            this.backEval.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.NullValue = "撤回";
            this.backEval.DefaultCellStyle = dataGridViewCellStyle2;
            this.backEval.HeaderText = "驳   回";
            this.backEval.Name = "backEval";
            this.backEval.Width = 80;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("华文新魏", 15F);
            this.label14.Location = new System.Drawing.Point(23, 150);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(110, 21);
            this.label14.TabIndex = 4;
            this.label14.Text = "评分详情：";
            // 
            // submitBtn
            // 
            this.submitBtn.Location = new System.Drawing.Point(486, 826);
            this.submitBtn.Name = "submitBtn";
            this.submitBtn.Size = new System.Drawing.Size(85, 30);
            this.submitBtn.TabIndex = 25;
            this.submitBtn.Text = "提  交";
            this.submitBtn.UseVisualStyleBackColor = true;
            this.submitBtn.Click += new System.EventHandler(this.submitBtn_Click);
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("华文新魏", 15F);
            this.label33.Location = new System.Drawing.Point(24, 11);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(150, 21);
            this.label33.TabIndex = 0;
            this.label33.Text = "企业评估报告：";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("宋体", 10F);
            this.label32.Location = new System.Drawing.Point(52, 42);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(140, 14);
            this.label32.TabIndex = 1;
            this.label32.Text = "1、评审员补充文件：";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("宋体", 10F);
            this.label30.Location = new System.Drawing.Point(52, 184);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(126, 14);
            this.label30.TabIndex = 3;
            this.label30.Text = "2、SSEM文件上传：";
            // 
            // getFileName
            // 
            this.getFileName.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.getFileName.Location = new System.Drawing.Point(184, 179);
            this.getFileName.Multiline = true;
            this.getFileName.Name = "getFileName";
            this.getFileName.ReadOnly = true;
            this.getFileName.Size = new System.Drawing.Size(434, 21);
            this.getFileName.TabIndex = 21;
            this.getFileName.Click += new System.EventHandler(this.getFileName_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("宋体", 10F);
            this.label3.Location = new System.Drawing.Point(52, 211);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(98, 14);
            this.label3.TabIndex = 22;
            this.label3.Text = "3、主审意见：";
            // 
            // saveBtn
            // 
            this.saveBtn.Location = new System.Drawing.Point(361, 335);
            this.saveBtn.Name = "saveBtn";
            this.saveBtn.Size = new System.Drawing.Size(85, 30);
            this.saveBtn.TabIndex = 24;
            this.saveBtn.Text = "保存并提交";
            this.saveBtn.UseVisualStyleBackColor = true;
            this.saveBtn.Click += new System.EventHandler(this.saveBtn_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.ForeColor = System.Drawing.Color.Tomato;
            this.label10.Location = new System.Drawing.Point(167, 185);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(11, 12);
            this.label10.TabIndex = 25;
            this.label10.Text = "*";
            // 
            // upload
            // 
            this.upload.Location = new System.Drawing.Point(672, 179);
            this.upload.Name = "upload";
            this.upload.Size = new System.Drawing.Size(67, 23);
            this.upload.TabIndex = 27;
            this.upload.Text = "上  传";
            this.upload.UseVisualStyleBackColor = true;
            this.upload.Click += new System.EventHandler(this.upload_Click);
            // 
            // checkedListBox1
            // 
            this.checkedListBox1.FormattingEnabled = true;
            this.checkedListBox1.Location = new System.Drawing.Point(190, 20);
            this.checkedListBox1.Name = "checkedListBox1";
            this.checkedListBox1.Size = new System.Drawing.Size(418, 148);
            this.checkedListBox1.TabIndex = 28;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(623, 138);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 29;
            this.button1.Text = "下载";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.ViewBtn);
            this.panel1.Controls.Add(this.button2);
            this.panel1.Controls.Add(this.label15);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.checkedListBox1);
            this.panel1.Controls.Add(this.upload);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.saveBtn);
            this.panel1.Controls.Add(this.textBoxAdvice);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.getFileName);
            this.panel1.Controls.Add(this.label30);
            this.panel1.Controls.Add(this.label32);
            this.panel1.Controls.Add(this.label33);
            this.panel1.Location = new System.Drawing.Point(0, 307);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(959, 368);
            this.panel1.TabIndex = 2;
            // 
            // ViewBtn
            // 
            this.ViewBtn.Location = new System.Drawing.Point(704, 138);
            this.ViewBtn.Name = "ViewBtn";
            this.ViewBtn.Size = new System.Drawing.Size(75, 23);
            this.ViewBtn.TabIndex = 32;
            this.ViewBtn.Text = "查看";
            this.ViewBtn.UseVisualStyleBackColor = true;
            this.ViewBtn.Click += new System.EventHandler(this.ViewBtn_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(840, 335);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(85, 30);
            this.button2.TabIndex = 31;
            this.button2.Text = "返  回";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.ForeColor = System.Drawing.Color.Tomato;
            this.label15.Location = new System.Drawing.Point(142, 212);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(11, 12);
            this.label15.TabIndex = 30;
            this.label15.Text = "*";
            // 
            // textBoxAdvice
            // 
            this.textBoxAdvice.Location = new System.Drawing.Point(116, 235);
            this.textBoxAdvice.Multiline = true;
            this.textBoxAdvice.Name = "textBoxAdvice";
            this.textBoxAdvice.Size = new System.Drawing.Size(729, 75);
            this.textBoxAdvice.TabIndex = 23;
            // 
            // LocationEvaluationViewResultForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(959, 677);
            this.Controls.Add(this.submitBtn);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.BackBtn);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.headerPanel);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "LocationEvaluationViewResultForm";
            this.Text = "主审评估";
            this.headerPanel.ResumeLayout(false);
            this.headerPanel.PerformLayout();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LocationalCompanyTable)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel headerPanel;
        private System.Windows.Forms.Label labelPostCode;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label labelAddress;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label labelUrlAddress;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label labelEmail;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label labelTelPhone;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label labelMobile;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label LabelPosition;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label labelContactMan;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label labelCompanyNAme;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label LabelDBsCode;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button submitBtn;
        private System.Windows.Forms.Button BackBtn;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.CheckedListBox checkedListBox1;
        private System.Windows.Forms.Button upload;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button saveBtn;
        private System.Windows.Forms.TextBox textBoxAdvice;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox getFileName;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.DataGridView LocationalCompanyTable;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn companyName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Eval_score;
        private System.Windows.Forms.DataGridViewTextBoxColumn EvalSumScore;
        private System.Windows.Forms.DataGridViewTextBoxColumn Eval_Item;
        private System.Windows.Forms.DataGridViewTextBoxColumn DenyItems;
        private System.Windows.Forms.DataGridViewTextBoxColumn Eval_ReasonInfo;
        private System.Windows.Forms.DataGridViewButtonColumn backEval;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button ViewBtn;
    }
}