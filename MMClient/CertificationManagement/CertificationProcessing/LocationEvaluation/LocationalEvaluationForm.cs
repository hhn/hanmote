﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using System.Text;

using Lib.Bll.CertificationProcess.General;
using Lib.Common.CommonUtils;
using MMClient.SystemConfig.UserManage;
using Lib.Bll.CertificationProcess.LocationalEvaluation;
using Lib.Model.CertificationProcess.LocationEvaluation;
using MMClient.CertificationManagement.CertificationProcessing.LocationEvaluation;
using WeifenLuo.WinFormsUI.Docking;

namespace MMClient.CertificationManagement.CertificationProcessing
{
    public partial class LocationalEvaluationForm : DockContent
    {
        private const int DEFAULT_PAGE_SIZE = 20;

        //每页显示的记录条数
        private int pageSize;

        //总记录条数
        private int sumSize;

        //总页数
        private int pageCount;

        //当前页号
        private int currentPage;

        //当前页最后一条记录在总记录中位置
        private int currentRow;

        //存储查询结果
        private DataTable userInforTable;

        //条件查询窗口对象
        private LocationalEvaluationSupperModel searchCondition;

        // 选评审员窗口
        private LocationEvaluationPersonForm locationEvaluationPersonForm;
        //查看评审状态窗口对象
        private CheckEvaluationStatusForm checkEvaluationStatusForm;
        //邮件通知窗口对象
        private LocaltionEvaluationKeyPointForm localtionEvaluationKeyPointForm;

        public LocationalEvaluationForm()
        {
            InitializeComponent();
        }
        

        private void init()
        {
            this.pageSize = DEFAULT_PAGE_SIZE;
            this.sumSize = 0;
            this.pageCount = 0;
            this.currentPage = 0;
            this.currentRow = 0;
        }
        

        /// <summary>
        /// 查询当前选中的行号
        /// </summary>
        /// <returns></returns>
        private int getCurrentSelectedRowIndex()
        {
            if (this.LocationalCompanyTable.Rows.Count <= 0)
            {
                MessageUtil.ShowWarning("用户表为空，无法执行操作");
                return -1;
            }

            if (this.LocationalCompanyTable.CurrentRow.Index < 0)
            {
                MessageUtil.ShowWarning("请选择一行记录，然后进行操作！");
                return -1;
            }

            return this.LocationalCompanyTable.CurrentRow.Index;
        }

        private void ReseachByCondition_Click(object sender, EventArgs e)
        {
            if(searchCondition==null)
            {
                searchCondition = new LocationalEvaluationSupperModel();
            }
            searchCondition.companyName = this.CompanyNameInput.Text;
            searchCondition.MtGroupName = this.MtGroupName.Text;
            searchCondition.Country = this.NationalName.Text;
            searchCondition.SelectedDep = this.PurGroupName.Text;
            LocationalEvaluationBLL locationalEvaluationBLL = new LocationalEvaluationBLL();
            //查询当前页大小的记录
            userInforTable = locationalEvaluationBLL.findCompanyListByCondition(SingleUserInstance.getCurrentUserInstance().User_ID,searchCondition, this.pageSize, 0);
            this.LocationalCompanyTable.AutoGenerateColumns = false;
            this.LocationalCompanyTable.DataSource = userInforTable;
            if (LocationalCompanyTable.Rows.Count == 0)
            {
                this.pageNext1.PageIndex = 0;

            }
            else {
                this.pageNext1.PageIndex = 1;

            }

            this.pageNext1.DrawControl(LocationalCompanyTable.Rows.Count);
            //记录总数
           // this.sumSize = locationalEvaluationBLL.calculateUserNumber();

        }

        private void ResetInfoList_Click(object sender, EventArgs e)
        {
            this.CompanyNameInput.Text = "";
            this.MtGroupName.Text="";
            this.NationalName.Text="";
            this.PurGroupName.Text="";
            loadData();
            if (LocationalCompanyTable.Rows.Count == 0)
            {
                this.pageNext1.PageIndex = 0;

            }
            else
            {
                this.pageNext1.PageIndex = 1;

            }
        }

        private void pageNext1_OnPageChanged(object sender, EventArgs e)
        {
            loadData();
        }

        private void loadData()
        {
            int totalSize = 0;
            LocationalEvaluationBLL locationalEvaluationBLL = new LocationalEvaluationBLL();
            DataTable dt = locationalEvaluationBLL.findAllLocalCompanyList(SingleUserInstance.getCurrentUserInstance().User_ID, this.pageNext1.PageSize, 1, out totalSize);
            this.LocationalCompanyTable.AutoGenerateColumns = false;
            this.LocationalCompanyTable.DataSource = dt;
            this.pageNext1.DrawControl(totalSize);

        }
    }
}
