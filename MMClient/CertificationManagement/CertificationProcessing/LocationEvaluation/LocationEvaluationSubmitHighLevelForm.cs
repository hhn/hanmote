﻿using Lib.Bll.CertificationProcess.LocationalEvaluation;
using Lib.Bll.SystemConfig.UserManage;
using Lib.Common.CommonUtils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MMClient.CertificationManagement.CertificationProcessing.LocationEvaluation
{
    public partial class LocationEvaluationSubmitHighLevelForm : Form
    {
        private LocationalEvaluationBLL locationalEvaluationBLL = new LocationalEvaluationBLL();
        private string companyID;
        private SystemUserOrganicRelationshipBLL systemUserOrganicRelationshipBLL = new SystemUserOrganicRelationshipBLL(); 
        public LocationEvaluationSubmitHighLevelForm(string companyID)
        {
            InitializeComponent();
            this.companyID = companyID;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.comboBox1.Text != null)
            {
                DataTable dataTable = systemUserOrganicRelationshipBLL.getUserInfoByRoleName(this.comboBox1.Text);
                this.userTableEvaluators.AutoGenerateColumns = false;
                this.userTableEvaluators.DataSource = dataTable;
            }
        }
        public void LoadJobPostionByRoleUserGroup(object sender, EventArgs e)
        {
            DataTable dataTable = systemUserOrganicRelationshipBLL.getInfoByRoleUserGroup();
            this.comboBox1.DataSource = dataTable;
            this.comboBox1.DisplayMember = "name";
        }

        private void button1_Click(object sender, EventArgs e)
        {
             int currentIndex = getCurrentSelectedRowIndex();
             DataGridViewRow row = this.userTableEvaluators.Rows[currentIndex];
             String ID = Convert.ToString(row.Cells["ID"].Value);
            String naame = Convert.ToString(row.Cells["namae"].Value);
            //现场评估完结,邮件通知搞成并修改标志位......
            locationalEvaluationBLL.updateTagSuperSRInfoByCompanyID(this.companyID);
            locationalEvaluationBLL.insertHanmoteDecisionTable(ID,this.companyID,this.comboBox1.Text.ToString(),"suggession");
            MessageBox.Show("选择成功");
            this.Hide();
        }
        /// <summary>
        /// 查询当前选中的行号
        /// </summary>
        /// <returns></returns>
        private int getCurrentSelectedRowIndex()
        {
            if (this.userTableEvaluators.Rows.Count <= 0)
            {
                MessageUtil.ShowWarning("用户表为空，无法执行操作");
                return -1;
            }

            if (this.userTableEvaluators.CurrentRow.Index < 0)
            {
                MessageUtil.ShowWarning("请选择一行记录，然后进行操作！");
                return -1;
            }

            return this.userTableEvaluators.CurrentRow.Index;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
