﻿namespace MMClient.CertificationManagement.CertificationProcessing.LocationEvaluation
{
    partial class SupplierPassCertificationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.LoginAccount = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.PasswordTxt = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.companyName = new System.Windows.Forms.Label();
            this.ContactMan = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.Emailinfo = new System.Windows.Forms.TextBox();
            this.PassConcel = new System.Windows.Forms.Button();
            this.PassConcern = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(31, 26);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(83, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "公 司 名 称：";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("宋体", 9F);
            this.label2.Location = new System.Drawing.Point(351, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(83, 12);
            this.label2.TabIndex = 2;
            this.label2.Text = "注册人姓名 ：";
            // 
            // LoginAccount
            // 
            this.LoginAccount.Location = new System.Drawing.Point(133, 117);
            this.LoginAccount.Name = "LoginAccount";
            this.LoginAccount.Size = new System.Drawing.Size(123, 21);
            this.LoginAccount.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("宋体", 10F);
            this.label3.Location = new System.Drawing.Point(30, 74);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(105, 14);
            this.label3.TabIndex = 4;
            this.label3.Text = "分配账号密码：";
            // 
            // PasswordTxt
            // 
            this.PasswordTxt.Location = new System.Drawing.Point(432, 114);
            this.PasswordTxt.Name = "PasswordTxt";
            this.PasswordTxt.Size = new System.Drawing.Size(123, 21);
            this.PasswordTxt.TabIndex = 7;
            this.PasswordTxt.Text = "123456";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("宋体", 9F);
            this.label4.Location = new System.Drawing.Point(349, 120);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(77, 12);
            this.label4.TabIndex = 6;
            this.label4.Text = "密      码：";
            // 
            // companyName
            // 
            this.companyName.AutoSize = true;
            this.companyName.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.companyName.Location = new System.Drawing.Point(120, 26);
            this.companyName.Name = "companyName";
            this.companyName.Size = new System.Drawing.Size(83, 12);
            this.companyName.TabIndex = 8;
            this.companyName.Text = "邮 箱 地 址：";
            // 
            // ContactMan
            // 
            this.ContactMan.AutoSize = true;
            this.ContactMan.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.ContactMan.Location = new System.Drawing.Point(455, 30);
            this.ContactMan.Name = "ContactMan";
            this.ContactMan.Size = new System.Drawing.Size(83, 12);
            this.ContactMan.TabIndex = 9;
            this.ContactMan.Text = "邮 箱 地 址：";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("宋体", 9F);
            this.label5.Location = new System.Drawing.Point(52, 120);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(77, 12);
            this.label5.TabIndex = 10;
            this.label5.Text = "账      号：";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(31, 181);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 12);
            this.label6.TabIndex = 11;
            this.label6.Text = "准入通告";
            // 
            // Emailinfo
            // 
            this.Emailinfo.Location = new System.Drawing.Point(54, 211);
            this.Emailinfo.Multiline = true;
            this.Emailinfo.Name = "Emailinfo";
            this.Emailinfo.Size = new System.Drawing.Size(591, 185);
            this.Emailinfo.TabIndex = 12;
            // 
            // PassConcel
            // 
            this.PassConcel.Location = new System.Drawing.Point(546, 432);
            this.PassConcel.Name = "PassConcel";
            this.PassConcel.Size = new System.Drawing.Size(87, 31);
            this.PassConcel.TabIndex = 13;
            this.PassConcel.Text = "取  消";
            this.PassConcel.UseVisualStyleBackColor = true;
            this.PassConcel.Click += new System.EventHandler(this.PassConcel_Click);
            // 
            // PassConcern
            // 
            this.PassConcern.Location = new System.Drawing.Point(399, 432);
            this.PassConcern.Name = "PassConcern";
            this.PassConcern.Size = new System.Drawing.Size(82, 31);
            this.PassConcern.TabIndex = 14;
            this.PassConcern.Text = "确  认";
            this.PassConcern.UseVisualStyleBackColor = true;
            this.PassConcern.Click += new System.EventHandler(this.PassConcern_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.ForeColor = System.Drawing.Color.Red;
            this.label7.Location = new System.Drawing.Point(262, 123);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(11, 12);
            this.label7.TabIndex = 15;
            this.label7.Text = "*";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.ForeColor = System.Drawing.Color.Red;
            this.label8.Location = new System.Drawing.Point(561, 120);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(11, 12);
            this.label8.TabIndex = 16;
            this.label8.Text = "*";
            // 
            // SupplierPassCertificationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(689, 475);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.PassConcern);
            this.Controls.Add(this.PassConcel);
            this.Controls.Add(this.Emailinfo);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.ContactMan);
            this.Controls.Add(this.companyName);
            this.Controls.Add(this.PasswordTxt);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.LoginAccount);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "SupplierPassCertificationForm";
            this.Text = "分配账号";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox LoginAccount;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox PasswordTxt;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label companyName;
        private System.Windows.Forms.Label ContactMan;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox Emailinfo;
        private System.Windows.Forms.Button PassConcel;
        private System.Windows.Forms.Button PassConcern;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
    }
}