﻿using Lib.Bll.CertificationProcess.LocationalEvaluation;
using Lib.Common.CommonUtils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Windows.Forms;

namespace MMClient.CertificationManagement.CertificationProcessing.LocationEvaluation
{
    public partial class CheckEvaluationStatusForm : Form
    {
        private const int DEFAULT_PAGE_SIZE = 20;

        //每页显示的记录条数
        private int pageSize;

        //总记录条数
        private int sumSize;

        //总页数
        private int pageCount;

        //当前页号
        private int currentPage;

        //当前页最后一条记录在总记录中位置
        private int currentRow;

        //存储查询结果
        private DataTable userInforTable;
        //传入的评审员id
        private StringBuilder tmpTextids;
        //传入供应商ID
        private string companyID;
        //传入供应商Name
        private string companyName;
        //传入物料组名称
        private string mtGroupName;
        //评审员工作完结状态
        private bool result = true ;

        //查看结果窗口对象
        private LocationEvaluationViewResultForm locationEvaluationViewResultForm;

        private LocationEvalutionPersonBLL locationEvalutionPersonBLL = new LocationEvalutionPersonBLL();

        public CheckEvaluationStatusForm(string company_ID, string company_Name, string mtGroup_Name)
        {
            this.companyID = company_ID;
            this.companyName = company_Name;
            this.mtGroupName = mtGroup_Name;
            InitializeComponent();
            fillCheckEvaluationStatusTable();
            this.Text = "当前处理公司：" + company_Name + "  物料组：" + mtGroup_Name;
            if (result == false)
                {
                  this.checkResult.Text = "通    知";
                  this.endTime.Show();
                  this.labelTimeState1.Show();
                  this.labelTimeState2.Show();
                  this.labelTimeState3.Show();
                }
             else
                {
                  this.checkResult.Text = "查看评审结果";
                  this.endTime.Hide();
                  this.labelTimeState1.Hide();
                  this.labelTimeState2.Hide();
                  this.labelTimeState3.Hide();
                }
        }
        /// <summary>
        /// 从数据库获取数据，填充用户表格
        /// </summary>
        public void fillCheckEvaluationStatusTable()
        {
            //查询当前页大小的记录
            userInforTable = locationEvalutionPersonBLL.getLocalEvalGroupStatByCompanyID(this.companyID);
            for (int k=0;k<userInforTable.Rows.Count;k++)
            {
                if(userInforTable.Rows[k][6].ToString().Equals("未完成"))
                {
                    this.result = false;
                }
            }
            this.CheckEvaluationStatus.AutoGenerateColumns = false;
            this.CheckEvaluationStatus.DataSource = userInforTable;
            
        }

        private void checkResult_Click(object sender, EventArgs e)
        {
            //评审工作完结，则进入查看评审阶段
            if (result==true)
            {
                //出现主审结果状态查看
                if (locationEvaluationViewResultForm == null || locationEvaluationViewResultForm.IsDisposed)
                {
                    locationEvaluationViewResultForm = new LocationEvaluationViewResultForm(this.companyID, this.mtGroupName);
                }
                locationEvaluationViewResultForm.Show();
                this.Close();
            }
            //未完结则处于发通知状态
            else
            {
                //得到未处理人邮箱发送通知
                userInforTable = locationEvalutionPersonBLL.getunfinishedEvalPersonInfo(this.companyID);
                StringBuilder person  = new StringBuilder();
                person.Append(":");
                for (int k = 0;k<userInforTable.Rows.Count;k++)
                {
                    String email = userInforTable.Rows[k][2].ToString();
                    String name =  userInforTable.Rows[k][1].ToString();
                    person.Append(name);
                    //添加邮件发送功能.....
                    List<MailAddress> list = new List<MailAddress>();
                    MailAddress mailAddress = new MailAddress(email);
                    list.Add(mailAddress);
                    EmailUtil.SendEmail("主审通知", "请"+name +"尽快录入、上传"+ this.companyName + "公司的现场评估分数及报告，截止时间："+this.endTime.Value.ToString("yyyy-MM-dd"), list);
                    person.Append("、");
                }
                MessageBox.Show("已成功通知"+ person);
                this.Hide();
                //this.checkResult.Enabled = false;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
