﻿using Lib.Bll.CertificationProcess.LocationalEvaluation;
using Lib.Common.CommonUtils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MMClient.CertificationManagement.CertificationProcessing.LocationEvaluation
{
    public partial class SupplierPassCertificationForm : Form
    {
        //公司名称
        private string company_Name;
        //公司注册人
        private string contactMan;
        //公司ID
        private string companyID;
        //注册人邮箱
        private string email;
        LocationEvalutionPersonBLL supplier_MainAccountInfo = new LocationEvalutionPersonBLL();
        public SupplierPassCertificationForm(string companyID, string companyName, string contactMan, string email)
        {
            this.companyID = companyID;
            this.company_Name = companyName;
            this.contactMan = contactMan;
            this.email = email;
            InitializeComponent();
            this.companyName.Text = companyName;
            this.ContactMan.Text = contactMan;
            this.LoginAccount.Text = companyID;
            this.Text = "当前处理公司 " + companyName; 
        }

        private void PassConcel_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void PassConcern_Click(object sender, EventArgs e)
        {
            if(this.LoginAccount.Text==null || this.LoginAccount.Text.ToString().Trim().Equals("") || this.PasswordTxt.Text == null || this.PasswordTxt.Text.ToString().Trim().Equals("") ||this.Emailinfo.Text==null || this.Emailinfo.Text.ToString().Trim().Equals("") )
            {
                MessageBox.Show("请将完整信息补充完整");

            }
            else
            {
                //记录数据到表Supplier_MainAccount；并发送邮件
                supplier_MainAccountInfo.insertSsupplier_MainAccountInfo(this.companyID,this.company_Name,this.contactMan,this.LoginAccount.Text,this.PasswordTxt.Text);
                List<MailAddress> list = new List<MailAddress>();
                MailAddress mailAddress = new MailAddress(email);
                list.Add(mailAddress);
                //插入等级SR_Info
                supplier_MainAccountInfo.setUpLvelBycompanyID(companyID);
                string text = "准入通知："+ this.Emailinfo.Text + "<p>您的账号:" + this.LoginAccount.Text + "<br>您的密码为：" + this.PasswordTxt.Text;
                EmailUtil.SendEmail("汉默特准入通知", text, list);
               
                LocationEvalutionPersonBLL locationEvalutionPersonBLL = new LocationEvalutionPersonBLL();
                locationEvalutionPersonBLL.updateTaskSpecification(SingleUserInstance.getCurrentUserInstance().User_ID, this.company_Name);
                locationEvalutionPersonBLL.insertTaskSpecification(SingleUserInstance.getCurrentUserInstance().User_ID, this.company_Name+"待改善");
                //插入供应商主数据信息
                if (locationEvalutionPersonBLL.insertSupplierBase(companyID) > 0)
                {
                    MessageUtil.ShowTips("邮件发送成功");
                }
                else {
                    MessageUtil.ShowError("准入失败！");
                }


                this.Close();
            }
        }
    }
}
