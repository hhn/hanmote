﻿using Lib.Bll.CertificationProcess.LocationalEvaluation;
using Lib.Common.CommonUtils;
using Lib.Model.CertificationProcess.LocationEvaluation;
using Lib.Model.SystemConfig;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MMClient.CertificationManagement.CertificationProcessing.LocationEvaluation
{
    public partial class LocalEvaluationSingleForm :  WeifenLuo.WinFormsUI.Docking.DockContent
    {
        
        private const int DEFAULT_PAGE_SIZE = 20;

        //每页显示的记录条数
        private int pageSize;

        //总记录条数
        private int sumSize;

        //总页数
        private int pageCount;

        //当前页号
        private int currentPage;

        //当前页最后一条记录在总记录中位置
        private int currentRow;

        //存储查询结果
        private DataTable userInforTable;

        //用户登陆判断ID
        private String UserID;

        // 条件model

        private LocationalEvaluationSupperModel searchCondition;
        private LocationEvalutionPersonBLL locationEvalutionPersonBLL = new LocationEvalutionPersonBLL();
        //处理窗口

        private LocationEvaluateSinglePersonForm LocationEvaluateSinglePersonForm;


        public LocalEvaluationSingleForm()
        {
            InitializeComponent();
        
        }

        private void init()
        {
            this.pageSize = DEFAULT_PAGE_SIZE;
            this.sumSize = 0;
            this.pageCount = 0;
            this.currentPage = 0;
            this.currentRow = 0;
        }
        /// <summary>
        /// 从数据库获取数据，填充用户表格
        /// </summary>
    

        /// <summary>
        /// 查询当前选中的行号
        /// </summary>
        /// <returns></returns>
        private int getCurrentSelectedRowIndex()
        {
            if (this.singleListSupplierTable.Rows.Count <= 0)
            {
                MessageUtil.ShowWarning("用户表为空，无法执行操作");
                return -1;
            }

            if (this.singleListSupplierTable.CurrentRow.Index < 0)
            {
                MessageUtil.ShowWarning("请选择一行记录，然后进行操作！");
                return -1;
            }

            return this.singleListSupplierTable.CurrentRow.Index;
        }
        /// <summary>
        /// 处理信息数据
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void singleListSupplierTable_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            //判断是处理还是重置
            if (e.RowIndex >= 0)
            {
                int currentIndex = getCurrentSelectedRowIndex();
                DataGridViewRow row = this.singleListSupplierTable.Rows[currentIndex];
                String companyID = Convert.ToString(row.Cells["SupplierId"].Value);
                String evaluateID = Convert.ToString(row.Cells["evaluateID"].Value);
                String companyName = Convert.ToString(row.Cells["companyName"].Value);
                //处理
                if (this.singleListSupplierTable.Columns[e.ColumnIndex].Name == "处理")
                {
                    //判断是否已经处理过了
                    DataTable table = locationEvalutionPersonBLL.getSinglePersonEvalStayusByCompanyAndUserID(companyID, evaluateID);
                    for (int i = 0; i < table.Rows.Count; i++)
                    {
                        if(table.Rows[0][0].ToString().Equals("1"))
                        {
                            MessageBoxButtons messButton = MessageBoxButtons.OKCancel;
                            DialogResult dr = MessageBox.Show("已经处理过，是否需要修改", "选 择", messButton);
                            if (dr == DialogResult.OK)
                            {
                                //删除任务列表已有的“XXXX现场评审已完结待审批”信息
                                //根据供应商ID得到物料组
                                string mtGroup = locationEvalutionPersonBLL.getMtGroupNameByCompanyId(companyID);
                                //根据供应商ID得到采购组
                                string mtPor = locationEvalutionPersonBLL.getMtPorNameByCompanyId(companyID);
                                //根据物料组得到主审ID
                                string MainEvalID = locationEvalutionPersonBLL.getMainEvalIDByMtGroupName(mtGroup, mtPor);
                                //删除原有的任务信息
                                locationEvalutionPersonBLL.deleteTaskSpecificationSportToUpper(MainEvalID, companyName);

                                //将现场评审完结的状态重新修改为未完成状态TagThird
                                locationEvalutionPersonBLL.setCompanyIDTagThirdZeroBycompanyID(companyID);

                                //弹出页面
                                LocationEvaluateSinglePersonForm = null;
                                if (LocationEvaluateSinglePersonForm == null || LocationEvaluateSinglePersonForm.IsDisposed)
                                {
                                    LocationEvaluateSinglePersonForm = new LocationEvaluateSinglePersonForm(companyName, companyID, 2, evaluateID);
                                }
                                LocationEvaluateSinglePersonForm.Show();
                            }
                                
                        }
                        else
                        {
                            this.singleListSupplierTable.Columns[e.ColumnIndex].Name = "处理";
                            //弹出页面
                            LocationEvaluateSinglePersonForm = null;
                            if (LocationEvaluateSinglePersonForm == null || LocationEvaluateSinglePersonForm.IsDisposed)
                            {
                                LocationEvaluateSinglePersonForm = new LocationEvaluateSinglePersonForm(companyName,companyID, 1,evaluateID);
                            }
                            LocationEvaluateSinglePersonForm.Show();
                        }
                    }
                }
                if (this.singleListSupplierTable.Columns[e.ColumnIndex].Name == "重置")
                {
                    MessageBoxButtons messButton = MessageBoxButtons.OKCancel;
                    DialogResult dr = MessageBox.Show("您确定重置吗？", "警 告", messButton);
                    if (dr == DialogResult.OK)
                    {
                        //删除信息并刷新数据
                        locationEvalutionPersonBLL.resetInfoEvaluationBySinglePerson(companyID, evaluateID);
                        //更新任务状态同时删除主审审批通知
                        locationEvalutionPersonBLL.setIsFinishedByUserID(SingleUserInstance.getCurrentUserInstance().User_ID, companyName);
                        loadData();
                        MessageBox.Show("重置成功");
                    }
                }
            }
            else
            {
                //....
            }
        }

        private void ReseachByCondition_Click(object sender, EventArgs e)
        {
            if (searchCondition == null)
            {
                searchCondition = new LocationalEvaluationSupperModel();
            }
            searchCondition.companyName = this.CompanyNameInput.Text;
            searchCondition.MtGroupName = this.MtGroupNameIN.Text;
            searchCondition.Country = this.NationalNameIN.Text;
            searchCondition.SelectedDep = this.PurGroupNameIN.Text;
            searchCondition.evalID = this.evalID.Text;
            LocationalEvaluationBLL locationalEvaluationBLL = new LocationalEvaluationBLL();
            //查询当前页大小的记录
            userInforTable = locationalEvaluationBLL.findSimplePersonEvalListByCondition(SingleUserInstance.getCurrentUserInstance().User_ID,searchCondition, this.pageSize, 0);
            this.singleListSupplierTable.AutoGenerateColumns = false;
            this.singleListSupplierTable.DataSource = userInforTable;
            //记录总数
            this.pageNext1.DrawControl(userInforTable.Rows.Count);
        }

        private void ResetInfoList_Click(object sender, EventArgs e)
        {
            this.CompanyNameInput.Text = "";
            this.MtGroupNameIN.Text = "";
            this.NationalNameIN.Text = "";
            this.PurGroupNameIN.Text = "";
            loadData();
        }

        private void pageNext1_OnPageChanged(object sender, EventArgs e)
        {
            loadData();
        }
        /// <summary>
        /// 加载数据
        /// </summary>
        private void loadData()
        {
            int totalSize = 0;
            DataTable dt = locationEvalutionPersonBLL.singlePersonEvaluationList(SingleUserInstance.getCurrentUserInstance().User_ID, this.pageNext1.PageSize, this.pageNext1.PageIndex, out totalSize);
            singleListSupplierTable.DataSource = dt;
            this.pageNext1.DrawControl(totalSize);
        }

        private void LocalEvaluationSingleForm_Load(object sender, EventArgs e)
        {
            loadData();

        }

      
    }
}
