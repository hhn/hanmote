﻿namespace MMClient.CertificationManagement.CertificationProcessing
{
    partial class LeaderDecide_subPage
    { /// <summary>
      /// Required designer variable.
      /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.headerPanel = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.button3 = new System.Windows.Forms.Button();
            this.RefuseButton = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.suggession = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.agree_comboBox = new System.Windows.Forms.ComboBox();
            this.label17 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.MainAdviceLable = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.button4 = new System.Windows.Forms.Button();
            this.checkedListBox1 = new System.Windows.Forms.CheckedListBox();
            this.button2 = new System.Windows.Forms.Button();
            this.label16 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.labelPostCode = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.labelAddress = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.labelUrlAddress = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.labelEmail = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.labelTelPhone = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.labelMobile = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.LabelPosition = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.labelContactMan = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.labelCompanyNAme = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.LabelDBsCode = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.headerPanel.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // headerPanel
            // 
            this.headerPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.headerPanel.AutoSize = true;
            this.headerPanel.Controls.Add(this.panel2);
            this.headerPanel.Controls.Add(this.labelPostCode);
            this.headerPanel.Controls.Add(this.label8);
            this.headerPanel.Controls.Add(this.labelAddress);
            this.headerPanel.Controls.Add(this.label12);
            this.headerPanel.Controls.Add(this.labelUrlAddress);
            this.headerPanel.Controls.Add(this.label6);
            this.headerPanel.Controls.Add(this.labelEmail);
            this.headerPanel.Controls.Add(this.label9);
            this.headerPanel.Controls.Add(this.labelTelPhone);
            this.headerPanel.Controls.Add(this.label11);
            this.headerPanel.Controls.Add(this.labelMobile);
            this.headerPanel.Controls.Add(this.label13);
            this.headerPanel.Controls.Add(this.LabelPosition);
            this.headerPanel.Controls.Add(this.label5);
            this.headerPanel.Controls.Add(this.labelContactMan);
            this.headerPanel.Controls.Add(this.label7);
            this.headerPanel.Controls.Add(this.labelCompanyNAme);
            this.headerPanel.Controls.Add(this.label4);
            this.headerPanel.Controls.Add(this.LabelDBsCode);
            this.headerPanel.Controls.Add(this.label2);
            this.headerPanel.Controls.Add(this.label1);
            this.headerPanel.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.headerPanel.Location = new System.Drawing.Point(0, 0);
            this.headerPanel.Name = "headerPanel";
            this.headerPanel.Size = new System.Drawing.Size(899, 710);
            this.headerPanel.TabIndex = 2;
            // 
            // panel2
            // 
            this.panel2.AutoScrollMargin = new System.Drawing.Size(1, 1);
            this.panel2.AutoSize = true;
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Controls.Add(this.panel1);
            this.panel2.Controls.Add(this.label16);
            this.panel2.Controls.Add(this.label18);
            this.panel2.Controls.Add(this.label19);
            this.panel2.Location = new System.Drawing.Point(12, 165);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(884, 503);
            this.panel2.TabIndex = 18;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.button3);
            this.panel3.Controls.Add(this.RefuseButton);
            this.panel3.Controls.Add(this.groupBox3);
            this.panel3.Controls.Add(this.label17);
            this.panel3.Controls.Add(this.button1);
            this.panel3.Location = new System.Drawing.Point(49, 281);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(822, 219);
            this.panel3.TabIndex = 21;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(626, 179);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(87, 31);
            this.button3.TabIndex = 7;
            this.button3.Text = "返  回";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // RefuseButton
            // 
            this.RefuseButton.Location = new System.Drawing.Point(389, 179);
            this.RefuseButton.Name = "RefuseButton";
            this.RefuseButton.Size = new System.Drawing.Size(87, 31);
            this.RefuseButton.TabIndex = 25;
            this.RefuseButton.Text = "拒绝";
            this.RefuseButton.UseVisualStyleBackColor = true;
            this.RefuseButton.Click += new System.EventHandler(this.RefuseButton_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.suggession);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this.agree_comboBox);
            this.groupBox3.Location = new System.Drawing.Point(10, 23);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(803, 156);
            this.groupBox3.TabIndex = 24;
            this.groupBox3.TabStop = false;
            // 
            // suggession
            // 
            this.suggession.Location = new System.Drawing.Point(23, 40);
            this.suggession.Multiline = true;
            this.suggession.Name = "suggession";
            this.suggession.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.suggession.Size = new System.Drawing.Size(714, 103);
            this.suggession.TabIndex = 18;
            this.suggession.Text = "决策意见输入决策意见";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(21, 14);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(29, 12);
            this.label10.TabIndex = 17;
            this.label10.Text = "决策";
            // 
            // agree_comboBox
            // 
            this.agree_comboBox.Enabled = false;
            this.agree_comboBox.FormattingEnabled = true;
            this.agree_comboBox.Items.AddRange(new object[] {
            "未审核",
            "同意",
            "不同意"});
            this.agree_comboBox.Location = new System.Drawing.Point(57, 11);
            this.agree_comboBox.Name = "agree_comboBox";
            this.agree_comboBox.Size = new System.Drawing.Size(134, 20);
            this.agree_comboBox.TabIndex = 17;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("宋体", 10F);
            this.label17.Location = new System.Drawing.Point(9, 6);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(56, 14);
            this.label17.TabIndex = 4;
            this.label17.Text = "2、决策";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(261, 179);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(87, 31);
            this.button1.TabIndex = 14;
            this.button1.Text = "提  交";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // panel1
            // 
            this.panel1.AutoSize = true;
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Controls.Add(this.groupBox2);
            this.panel1.Location = new System.Drawing.Point(49, 63);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(832, 216);
            this.panel1.TabIndex = 19;
            // 
            // supplierInforGroupBox
            // 
            this.groupBox1.Controls.Add(this.MainAdviceLable);
            this.groupBox1.Location = new System.Drawing.Point(381, 3);
            this.groupBox1.Name = "supplierInforGroupBox";
            this.groupBox1.Size = new System.Drawing.Size(432, 209);
            this.groupBox1.TabIndex = 22;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "下级意见";
            // 
            // MainAdviceLable
            // 
            this.MainAdviceLable.AutoSize = true;
            this.MainAdviceLable.Location = new System.Drawing.Point(45, 27);
            this.MainAdviceLable.Name = "MainAdviceLable";
            this.MainAdviceLable.Size = new System.Drawing.Size(53, 12);
            this.MainAdviceLable.TabIndex = 0;
            this.MainAdviceLable.Text = "主审意见";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.button4);
            this.groupBox2.Controls.Add(this.checkedListBox1);
            this.groupBox2.Controls.Add(this.button2);
            this.groupBox2.Location = new System.Drawing.Point(3, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(362, 209);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "报告文件";
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(103, 167);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 2;
            this.button4.Text = "查看";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // checkedListBox1
            // 
            this.checkedListBox1.BackColor = System.Drawing.SystemColors.Control;
            this.checkedListBox1.ColumnWidth = 10;
            this.checkedListBox1.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkedListBox1.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.checkedListBox1.FormattingEnabled = true;
            this.checkedListBox1.Location = new System.Drawing.Point(4, 17);
            this.checkedListBox1.Name = "checkedListBox1";
            this.checkedListBox1.Size = new System.Drawing.Size(322, 132);
            this.checkedListBox1.TabIndex = 0;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(7, 167);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 1;
            this.button2.Text = "下载";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.ForeColor = System.Drawing.Color.DarkOrange;
            this.label16.Location = new System.Drawing.Point(123, 39);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(11, 12);
            this.label16.TabIndex = 7;
            this.label16.Text = "*";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("宋体", 10F);
            this.label18.Location = new System.Drawing.Point(46, 34);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(84, 14);
            this.label18.TabIndex = 1;
            this.label18.Text = "1、评估报告";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("华文新魏", 15F);
            this.label19.Location = new System.Drawing.Point(22, 8);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(50, 21);
            this.label19.TabIndex = 0;
            this.label19.Text = "决策";
            // 
            // labelPostCode
            // 
            this.labelPostCode.AutoSize = true;
            this.labelPostCode.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labelPostCode.Location = new System.Drawing.Point(626, 143);
            this.labelPostCode.Name = "labelPostCode";
            this.labelPostCode.Size = new System.Drawing.Size(41, 12);
            this.labelPostCode.TabIndex = 20;
            this.labelPostCode.Text = "label3";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(555, 143);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(41, 12);
            this.label8.TabIndex = 19;
            this.label8.Text = "邮编：";
            // 
            // labelAddress
            // 
            this.labelAddress.AutoSize = true;
            this.labelAddress.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labelAddress.Location = new System.Drawing.Point(150, 143);
            this.labelAddress.Name = "labelAddress";
            this.labelAddress.Size = new System.Drawing.Size(77, 12);
            this.labelAddress.TabIndex = 18;
            this.labelAddress.Text = "labelAddress";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(67, 143);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(41, 12);
            this.label12.TabIndex = 17;
            this.label12.Text = "地址：";
            // 
            // labelUrlAddress
            // 
            this.labelUrlAddress.AutoSize = true;
            this.labelUrlAddress.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labelUrlAddress.Location = new System.Drawing.Point(626, 118);
            this.labelUrlAddress.Name = "labelUrlAddress";
            this.labelUrlAddress.Size = new System.Drawing.Size(41, 12);
            this.labelUrlAddress.TabIndex = 16;
            this.labelUrlAddress.Text = "label3";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(555, 118);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 12);
            this.label6.TabIndex = 15;
            this.label6.Text = "网址：";
            // 
            // labelEmail
            // 
            this.labelEmail.AutoSize = true;
            this.labelEmail.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labelEmail.Location = new System.Drawing.Point(150, 118);
            this.labelEmail.Name = "labelEmail";
            this.labelEmail.Size = new System.Drawing.Size(41, 12);
            this.labelEmail.TabIndex = 14;
            this.labelEmail.Text = "label3";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(67, 118);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(41, 12);
            this.label9.TabIndex = 13;
            this.label9.Text = "邮箱：";
            // 
            // labelTelPhone
            // 
            this.labelTelPhone.AutoSize = true;
            this.labelTelPhone.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labelTelPhone.Location = new System.Drawing.Point(626, 92);
            this.labelTelPhone.Name = "labelTelPhone";
            this.labelTelPhone.Size = new System.Drawing.Size(41, 12);
            this.labelTelPhone.TabIndex = 12;
            this.labelTelPhone.Text = "label3";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(555, 92);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(41, 12);
            this.label11.TabIndex = 11;
            this.label11.Text = "传真：";
            // 
            // labelMobile
            // 
            this.labelMobile.AutoSize = true;
            this.labelMobile.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labelMobile.Location = new System.Drawing.Point(149, 92);
            this.labelMobile.Name = "labelMobile";
            this.labelMobile.Size = new System.Drawing.Size(41, 12);
            this.labelMobile.TabIndex = 10;
            this.labelMobile.Text = "label3";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(67, 92);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(65, 12);
            this.label13.TabIndex = 9;
            this.label13.Text = "电话号码：";
            // 
            // LabelPosition
            // 
            this.LabelPosition.AutoSize = true;
            this.LabelPosition.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.LabelPosition.Location = new System.Drawing.Point(627, 64);
            this.LabelPosition.Name = "LabelPosition";
            this.LabelPosition.Size = new System.Drawing.Size(41, 12);
            this.LabelPosition.TabIndex = 8;
            this.LabelPosition.Text = "label3";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(556, 64);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 12);
            this.label5.TabIndex = 7;
            this.label5.Text = "职位：";
            // 
            // labelContactMan
            // 
            this.labelContactMan.AutoSize = true;
            this.labelContactMan.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labelContactMan.Location = new System.Drawing.Point(151, 64);
            this.labelContactMan.Name = "labelContactMan";
            this.labelContactMan.Size = new System.Drawing.Size(41, 12);
            this.labelContactMan.TabIndex = 6;
            this.labelContactMan.Text = "label3";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(68, 64);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 12);
            this.label7.TabIndex = 5;
            this.label7.Text = "联系人：";
            // 
            // labelCompanyNAme
            // 
            this.labelCompanyNAme.AutoSize = true;
            this.labelCompanyNAme.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labelCompanyNAme.Location = new System.Drawing.Point(627, 34);
            this.labelCompanyNAme.Name = "labelCompanyNAme";
            this.labelCompanyNAme.Size = new System.Drawing.Size(41, 12);
            this.labelCompanyNAme.TabIndex = 4;
            this.labelCompanyNAme.Text = "label3";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(556, 34);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 12);
            this.label4.TabIndex = 3;
            this.label4.Text = "公司名称：";
            // 
            // LabelDBsCode
            // 
            this.LabelDBsCode.AutoSize = true;
            this.LabelDBsCode.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.LabelDBsCode.Location = new System.Drawing.Point(151, 34);
            this.LabelDBsCode.Name = "LabelDBsCode";
            this.LabelDBsCode.Size = new System.Drawing.Size(41, 12);
            this.LabelDBsCode.TabIndex = 2;
            this.LabelDBsCode.Text = "label3";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(68, 34);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 12);
            this.label2.TabIndex = 1;
            this.label2.Text = "邓白氏编码：";
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("华文新魏", 15F);
            this.label1.Location = new System.Drawing.Point(22, 8);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(110, 21);
            this.label1.TabIndex = 0;
            this.label1.Text = "基本信息：";
            // 
            // LeaderDecide_subPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(874, 689);
            this.Controls.Add(this.headerPanel);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "LeaderDecide_subPage";
            this.Text = "决策界面";
            this.headerPanel.ResumeLayout(false);
            this.headerPanel.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel headerPanel;
        private System.Windows.Forms.Label labelPostCode;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label labelAddress;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label labelUrlAddress;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label labelEmail;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label labelTelPhone;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label labelMobile;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label LabelPosition;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label labelContactMan;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label labelCompanyNAme;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label LabelDBsCode;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.CheckedListBox checkedListBox1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label MainAdviceLable;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox suggession;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button RefuseButton;
        private System.Windows.Forms.ComboBox agree_comboBox;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
    }
}