﻿namespace MMClient
{
    partial class SelfAssessmentForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.sendmessagebtn = new System.Windows.Forms.Button();
            this.gysbh_cmb = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.gysmc_cmb = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // sendmessagebtn
            // 
            this.sendmessagebtn.Location = new System.Drawing.Point(664, 56);
            this.sendmessagebtn.Name = "sendmessagebtn";
            this.sendmessagebtn.Size = new System.Drawing.Size(106, 24);
            this.sendmessagebtn.TabIndex = 159;
            this.sendmessagebtn.Text = "发送自评通知";
            this.sendmessagebtn.UseVisualStyleBackColor = true;
            this.sendmessagebtn.Click += new System.EventHandler(this.sendmessagebtn_Click);
            // 
            // gysbh_cmb
            // 
            this.gysbh_cmb.FormattingEnabled = true;
            this.gysbh_cmb.ItemHeight = 12;
            this.gysbh_cmb.Location = new System.Drawing.Point(119, 56);
            this.gysbh_cmb.Name = "gysbh_cmb";
            this.gysbh_cmb.Size = new System.Drawing.Size(200, 20);
            this.gysbh_cmb.TabIndex = 161;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("宋体", 9F);
            this.label2.Location = new System.Drawing.Point(28, 59);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 12);
            this.label2.TabIndex = 160;
            this.label2.Text = "供应商编号";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.ItemHeight = 12;
            this.comboBox1.Items.AddRange(new object[] {
            "通过，进入公司评审",
            "不通过"});
            this.comboBox1.Location = new System.Drawing.Point(119, 110);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(200, 20);
            this.comboBox1.TabIndex = 162;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(796, 57);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(89, 24);
            this.button1.TabIndex = 163;
            this.button1.Text = "查看自评结果";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(363, 107);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(89, 24);
            this.button2.TabIndex = 164;
            this.button2.Text = "保存";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("宋体", 9F);
            this.label3.Location = new System.Drawing.Point(28, 110);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 12);
            this.label3.TabIndex = 165;
            this.label3.Text = "评审结果";
            // 
            // gysmc_cmb
            // 
            this.gysmc_cmb.FormattingEnabled = true;
            this.gysmc_cmb.ItemHeight = 12;
            this.gysmc_cmb.Location = new System.Drawing.Point(443, 60);
            this.gysmc_cmb.Name = "gysmc_cmb";
            this.gysmc_cmb.Size = new System.Drawing.Size(200, 20);
            this.gysmc_cmb.TabIndex = 167;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("宋体", 9F);
            this.label4.Location = new System.Drawing.Point(361, 62);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 12);
            this.label4.TabIndex = 166;
            this.label4.Text = "供应商名称";
            // 
            // SelfAssessmentForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(897, 358);
            this.Controls.Add(this.gysmc_cmb);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.gysbh_cmb);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.sendmessagebtn);
            this.Name = "SelfAssessmentForm";
            this.Text = "自评管理";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button sendmessagebtn;
        private System.Windows.Forms.ComboBox gysbh_cmb;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox gysmc_cmb;
        private System.Windows.Forms.Label label4;
    }
}