﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Common.CommonUtils;
using Lib.Bll;

namespace MMClient
{
    public partial class SetTemplateForm : Form
    {
        private SupplierManagementBLL bll = new SupplierManagementBLL();
        private SupplierCertificationForm sFrm = null;
        private string expensetype;
        public SetTemplateForm()
        {
            InitializeComponent();
        }

        public SetTemplateForm(SupplierCertificationForm sFrm,string expensetype)
        {
            InitializeComponent();
            this.sFrm = sFrm;
            this.expensetype = expensetype;
        }
        private void button4_Click(object sender, EventArgs e)
        {
            //遍历树形模板是否有没有选中的
            if (!checkNothing(this.treeView4))
            {
                List<String> list = new List<string>();
                TreeView t = this.treeView4;
                for (int i = 0; i < treeView4.Nodes.Count; i++)
                {
                    if (t.Nodes[i].Checked)
                    {
                        list.Add(t.Nodes[i].Text);
                    }
                }
                if (list != null && list.Count > 0)
                {
                    DataTable dt = GetDataStructure();
                    DataRow dr = null;
                    for (int i = 0; i < list.Count; i++)
                    {
                        dr = dt.NewRow();
                        dr["material_group"] = this.sFrm.getMaterialCmb().Text;
                        dr["type"] = "关键型";
                        dr["templatename"] = list[i].ToString();
                        dt.Rows.Add(dr);
                    }
                    bool success = bll.saveCertificationTemplate(dt);
                    if (success)
                    {
                        MessageUtil.ShowTips("保存成功");
                    }
                    else
                    {
                        MessageUtil.ShowTips("保存失败");
                    }
                }
            }
            else
            {
                MessageUtil.ShowTips("至少选中一个子项！");
            }
        }

        /// <summary>
        /// 遍历是否没有选中 都没选中return true
        /// </summary>
        /// <param name="tree"></param>
        /// <returns></returns>
        private bool checkNothing(TreeView tree)
        {
            bool noCheck = true;
            for (int i = 0; i < tree.Nodes.Count; i++)
            {
                for (int j = 0; j < tree.Nodes[i].Nodes.Count; j++)
                {
                    if (tree.Nodes[i].Nodes[j].Checked)
                    {
                        noCheck = false;
                        return noCheck;
                    }
                }

                if (tree.Nodes[i].Checked)
                {
                    noCheck = false;
                    return noCheck;
                }
            }

            return noCheck;
        }

        private DataTable GetDataStructure()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("material_group", typeof(string));
            dt.Columns.Add("type", typeof(string));
            dt.Columns.Add("templatename", typeof(string));
            return dt;
        }

        /// <summary>
        /// 全选
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void selectAll_cb_CheckedChanged(object sender, EventArgs e)
        {
            bool check = this.selectAll_cb.Checked;
            //treeView1.Focus();
            for (int i = 0; i < treeView1.Nodes.Count; i++)
            {
                treeView1.Nodes[i].Checked = check;  //父级选中
                treeView1.Nodes[i].Expand();//展开父级
                for (int j = 0; j < treeView1.Nodes[i].Nodes.Count; j++)
                {
                    treeView1.Nodes[i].Nodes[j].Checked = check;
                }
            }
        }

        private TreeView getTreeView()
        {
            TreeView tv = null;
            switch (this.expensetype)
            {
                case "一般":
                    tv = this.treeView1; break;
                case "杠杆":
                    tv = this.treeView2; break;
                case "瓶颈":
                    tv = this.treeView3; break;
                case "关键":
                    tv = this.treeView4; break;
                default:
                    tv = this.treeView1; break;
            }
            return tv;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            TreeView tv = this.treeView1;
            //遍历树形模板是否有没有选中的
            if (!checkNothing(tv))
            {
                List<String> list = new List<string>();
                TreeView t = this.treeView1;
                for (int i = 0; i < t.Nodes.Count; i++)
                {
                    if (t.Nodes[i].Checked)
                    {
                        list.Add(t.Nodes[i].Text);
                    }
                }
                if (list != null && list.Count > 0)
                {
                    DataTable dt = GetDataStructure();
                    DataRow dr = null;
                    for (int i = 0; i < list.Count; i++)
                    {
                        dr = dt.NewRow();
                        dr["material_group"] = this.sFrm.getMaterialCmb().Text;
                        dr["type"] = "一般型";
                        dr["templatename"] = list[i].ToString();
                        dt.Rows.Add(dr);
                    }
                    bool success = bll.saveCertificationTemplate(dt);
                    if (success)
                    {
                        MessageUtil.ShowTips("保存成功");
                    }
                    else
                    {
                        MessageUtil.ShowTips("保存失败");
                    }
                }
            }
            else
            {
                MessageUtil.ShowTips("至少选中一个子项！");
            }
        }

        private void SetTemplateForm_Load(object sender, EventArgs e)
        {
            //设置选项卡
            if (!string.IsNullOrWhiteSpace(this.expensetype))
            { 
                int n = 0;
                switch(this.expensetype)
                {
                    case "一般":
                        n = 0;break;
                    case "杠杆":
                        n = 1;break;
                    case "瓶颈":
                        n = 2;break;
                    case "关键":
                        n = 3; break;
                    default:
                        n = 0;break;
                }
                this.tabControl1.SelectedIndex = n;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            //遍历树形模板是否有没有选中的
            if (!checkNothing(this.treeView3))
            {
                List<String> list = new List<string>();
                TreeView t = this.treeView3;
                for (int i = 0; i < t.Nodes.Count; i++)
                {
                    if (t.Nodes[i].Checked)
                    {
                        list.Add(t.Nodes[i].Text);
                    }
                }
                if (list != null && list.Count > 0)
                {
                    DataTable dt = GetDataStructure();
                    DataRow dr = null;
                    for (int i = 0; i < list.Count; i++)
                    {
                        dr = dt.NewRow();
                        dr["material_group"] = this.sFrm.getMaterialCmb().Text;
                        dr["type"] = "瓶颈型";
                        dr["templatename"] = list[i].ToString();
                        dt.Rows.Add(dr);
                    }
                    bool success = bll.saveCertificationTemplate(dt);
                    if (success)
                    {
                        MessageUtil.ShowTips("保存成功");
                    }
                    else
                    {
                        MessageUtil.ShowTips("保存失败");
                    }
                }
            }
            else
            {
                MessageUtil.ShowTips("至少选中一个子项！");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //遍历树形模板是否有没有选中的
            if (!checkNothing(this.treeView2))
            {
                List<String> list = new List<string>();
                TreeView t = this.treeView2;
                for (int i = 0; i < t.Nodes.Count; i++)
                {
                    if (t.Nodes[i].Checked)
                    {
                        list.Add(t.Nodes[i].Text);
                    }
                }
                if (list != null && list.Count > 0)
                {
                    DataTable dt = GetDataStructure();
                    DataRow dr = null;
                    for (int i = 0; i < list.Count; i++)
                    {
                        dr = dt.NewRow();
                        dr["material_group"] = this.sFrm.getMaterialCmb().Text;
                        dr["type"] = "杠杆型";
                        dr["templatename"] = list[i].ToString();
                        dt.Rows.Add(dr);
                    }
                    bool success = bll.saveCertificationTemplate(dt);
                    if (success)
                    {
                        MessageUtil.ShowTips("保存成功");
                    }
                    else
                    {
                        MessageUtil.ShowTips("保存失败");
                    }
                }
            }
            else
            {
                MessageUtil.ShowTips("至少选中一个子项！");
            }
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            bool check = this.selectAll_cb.Checked;
            //treeView1.Focus();
            for (int i = 0; i < treeView2.Nodes.Count; i++)
            {
                treeView2.Nodes[i].Checked = check;  //父级选中
                treeView2.Nodes[i].Expand();//展开父级
                for (int j = 0; j < treeView2.Nodes[i].Nodes.Count; j++)
                {
                    treeView2.Nodes[i].Nodes[j].Checked = check;
                }
            }
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            bool check = this.selectAll_cb.Checked;
            //treeView1.Focus();
            for (int i = 0; i < treeView3.Nodes.Count; i++)
            {
                treeView3.Nodes[i].Checked = check;  //父级选中
                treeView3.Nodes[i].Expand();//展开父级
                for (int j = 0; j < treeView3.Nodes[i].Nodes.Count; j++)
                {
                    treeView3.Nodes[i].Nodes[j].Checked = check;
                }
            }
        }

        private void checkBox3_CheckedChanged(object sender, EventArgs e)
        {
            bool check = this.selectAll_cb.Checked;
            //treeView1.Focus();
            for (int i = 0; i < treeView4.Nodes.Count; i++)
            {
                treeView4.Nodes[i].Checked = check;  //父级选中
                treeView4.Nodes[i].Expand();//展开父级
                for (int j = 0; j < treeView4.Nodes[i].Nodes.Count; j++)
                {
                    treeView4.Nodes[i].Nodes[j].Checked = check;
                }
            }
        }

    }
}

