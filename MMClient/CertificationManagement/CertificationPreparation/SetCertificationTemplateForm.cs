﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Bll;
using Lib.Model;
using Lib.Common.CommonUtils;

namespace MMClient
{
    public partial class SetCertificationTemplateForm : Form
    {
        private SupplierManagementBLL bll = new SupplierManagementBLL();
        private SupplierCertificationForm sFrm = null;
        public SetCertificationTemplateForm()
        {
            InitializeComponent();
        }

        public SetCertificationTemplateForm(SupplierCertificationForm sFrm)
        {
            InitializeComponent();
            this.sFrm = sFrm;
        }

        private void SetCertificationTemplateForm_Load(object sender, EventArgs e)
        {
            InitialTreeNode();
        }
        /// <summary>
        /// 初始化所有树形节点
        /// </summary>
        private void InitialTreeNode()
        {
            List<CertificationTemplate> list = bll.GetAllCertificationTemplate();
            if (list != null && list.Count > 0)
            {
                BindTreeData(list,this.treeView1);
                BindTreeData(list, this.treeView2);
                BindTreeData(list, this.treeView3);
                BindTreeData(list, this.treeView4);
            }
            
        }

        private void BindTreeData(List<CertificationTemplate> list,TreeView treeView)
        {
            List<CertificationTemplate> pList = new List<CertificationTemplate>();
            List<CertificationTemplate> cList = new List<CertificationTemplate>();
            foreach (CertificationTemplate c in list)
            {
                if (string.IsNullOrWhiteSpace(c.pid))
                {
                    pList.Add(c);
                }
                else
                {
                    cList.Add(c);
                }
            }

            foreach (CertificationTemplate d in pList)
            {
                TreeNode pNode = new TreeNode(d.text);

                foreach (CertificationTemplate child in cList)
                {
                    //如果子节点在父节点下
                    if (child.pid.Equals(d.tid))
                    {
                        TreeNode node = new TreeNode(child.text);
                        node.Name = child.tid;
                        pNode.Nodes.Add(node);
                    }
                }
                treeView.Nodes.Add(pNode);
            }
        }

        /// <summary>
        /// 全选
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void selectAll_cb_CheckedChanged(object sender, EventArgs e)
        {
            bool check = this.selectAll_cb.Checked;
            treeView1.Focus();
            for (int i = 0; i < treeView1.Nodes.Count; i++)
            {
                treeView1.Nodes[i].Checked = check;  //父级选中
                treeView1.Nodes[i].Expand();//展开父级
                for (int j = 0; j < treeView1.Nodes[i].Nodes.Count; j++)
                {
                    treeView1.Nodes[i].Nodes[j].Checked = check;
                }
            }
        }

        /// <summary>
        /// 保存一般模板
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            //遍历树形模板是否有没有选中的
            if (!checkNothing(this.treeView1))
            {
                List<String> list = new List<string>();
                for (int i = 0; i < treeView1.Nodes.Count; i++)
                {
                    for (int j = 0; j < treeView1.Nodes[i].Nodes.Count; j++)
                    {
                        if (treeView1.Nodes[i].Nodes[j].Checked)
                        {
                            list.Add(treeView1.Nodes[i].Nodes[j].Text);
                        }
                    }
                }
                if (list != null && list.Count > 0)
                {
                    DataTable dt = GetDataStructure();
                    DataRow dr = null;
                        for (int i = 0; i < list.Count; i++)
                        {
                            dr = dt.NewRow();
                            dr["material_group"] = this.sFrm.getMaterialCmb().Text;
                            dr["type"] = "一般型";
                            dr["templatename"] = list[i].ToString();
                            dt.Rows.Add(dr);
                        }
                        bool success = bll.saveCertificationTemplate(dt);
                        if (success)
                        {
                            MessageUtil.ShowTips("保存成功");
                        }
                        else {
                            MessageUtil.ShowTips("保存失败");
                        }
                }
            }
            else
            {
                MessageUtil.ShowTips("至少选中一个子项！");
            }
        }
        private DataTable GetDataStructure()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("material_group",typeof(string));
            dt.Columns.Add("type", typeof(string));
            dt.Columns.Add("templatename", typeof(string));
            return dt;
        }


        /// <summary>
        /// 遍历是否没有选中 都没选中return true
        /// </summary>
        /// <param name="tree"></param>
        /// <returns></returns>
        private bool checkNothing(TreeView tree) 
        {
            bool noCheck = true;
            for (int i = 0; i < tree.Nodes.Count; i++)
            {
                for (int j = 0; j < tree.Nodes[i].Nodes.Count; j++)
                {
                    if (tree.Nodes[i].Nodes[j].Checked)
                    {
                        noCheck = false;
                        return noCheck;
                    }
                }
            }

            return noCheck;
        }
        /// <summary>
        /// 保存杠杆模板
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            //遍历树形模板是否有没有选中的
            if (!checkNothing(this.treeView2))
            {
                List<String> list = new List<string>();
                for (int i = 0; i < treeView2.Nodes.Count; i++)
                {
                    for (int j = 0; j < treeView2.Nodes[i].Nodes.Count; j++)
                    {
                        if (treeView2.Nodes[i].Nodes[j].Checked)
                        {
                            list.Add(treeView2.Nodes[i].Nodes[j].Text);
                        }
                    }
                }
                if (list != null && list.Count > 0)
                {
                    DataTable dt = GetDataStructure();
                    DataRow dr = null;
                    for (int i = 0; i < list.Count; i++)
                    {
                        dr = dt.NewRow();
                        dr["material_group"] = this.sFrm.getMaterialCmb().Text;
                        dr["type"] = "一般型";
                        dr["templatename"] = list[i].ToString();
                        dt.Rows.Add(dr);
                    }
                    bool success = bll.saveCertificationTemplate(dt);
                    if (success)
                    {
                        MessageUtil.ShowTips("保存成功");
                    }
                    else
                    {
                        MessageUtil.ShowTips("保存失败");
                    }
                }
            }
            else
            {
                MessageUtil.ShowTips("至少选中一个子项！");
            }
        }
        /// <summary>
        /// 保存瓶颈模板
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button3_Click(object sender, EventArgs e)
        {
            //遍历树形模板是否有没有选中的
            if (!checkNothing(this.treeView3))
            {
                List<String> list = new List<string>();
                for (int i = 0; i < treeView3.Nodes.Count; i++)
                {
                    for (int j = 0; j < treeView3.Nodes[i].Nodes.Count; j++)
                    {
                        if (treeView3.Nodes[i].Nodes[j].Checked)
                        {
                            list.Add(treeView1.Nodes[i].Nodes[j].Text);
                        }
                    }
                }
                if (list != null && list.Count > 0)
                {
                    DataTable dt = GetDataStructure();
                    DataRow dr = null;
                    for (int i = 0; i < list.Count; i++)
                    {
                        dr = dt.NewRow();
                        dr["material_group"] = this.sFrm.getMaterialCmb().Text;
                        dr["type"] = "一般型";
                        dr["templatename"] = list[i].ToString();
                        dt.Rows.Add(dr);
                    }
                    bool success = bll.saveCertificationTemplate(dt);
                    if (success)
                    {
                        MessageUtil.ShowTips("保存成功");
                    }
                    else
                    {
                        MessageUtil.ShowTips("保存失败");
                    }
                }
            }
            else
            {
                MessageUtil.ShowTips("至少选中一个子项！");
            }
        }
        /// <summary>
        /// 保存关键模板
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button4_Click(object sender, EventArgs e)
        {
            //遍历树形模板是否有没有选中的
            if (!checkNothing(this.treeView4))
            {
                List<String> list = new List<string>();
                for (int i = 0; i < treeView4.Nodes.Count; i++)
                {
                    for (int j = 0; j < treeView4.Nodes[i].Nodes.Count; j++)
                    {
                        if (treeView4.Nodes[i].Nodes[j].Checked)
                        {
                            list.Add(treeView4.Nodes[i].Nodes[j].Text);
                        }
                    }
                }
                if (list != null && list.Count > 0)
                {
                    DataTable dt = GetDataStructure();
                    DataRow dr = null;
                    for (int i = 0; i < list.Count; i++)
                    {
                        dr = dt.NewRow();
                        dr["material_group"] = this.sFrm.getMaterialCmb().Text;
                        dr["type"] = "一般型";
                        dr["templatename"] = list[i].ToString();
                        dt.Rows.Add(dr);
                    }
                    bool success = bll.saveCertificationTemplate(dt);
                    if (success)
                    {
                        MessageUtil.ShowTips("保存成功");
                    }
                    else
                    {
                        MessageUtil.ShowTips("保存失败");
                    }
                }
            }
            else
            {
                MessageUtil.ShowTips("至少选中一个子项！");
            }
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            bool check = this.checkBox1.Checked;
            treeView2.Focus();
            for (int i = 0; i < treeView2.Nodes.Count; i++)
            {
                treeView2.Nodes[i].Checked = check;  //父级选中
                treeView2.Nodes[i].Expand();//展开父级
                for (int j = 0; j < treeView1.Nodes[i].Nodes.Count; j++)
                {
                    treeView2.Nodes[i].Nodes[j].Checked = check;
                }
            }
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            bool check = this.checkBox2.Checked;
            treeView3.Focus();
            for (int i = 0; i < treeView2.Nodes.Count; i++)
            {
                treeView3.Nodes[i].Checked = check;  //父级选中
                treeView3.Nodes[i].Expand();//展开父级
                for (int j = 0; j < treeView1.Nodes[i].Nodes.Count; j++)
                {
                    treeView3.Nodes[i].Nodes[j].Checked = check;
                }
            }
        }

        private void checkBox3_CheckedChanged(object sender, EventArgs e)
        {
            bool check = this.checkBox3.Checked;
            treeView4.Focus();
            for (int i = 0; i < treeView2.Nodes.Count; i++)
            {
                treeView4.Nodes[i].Checked = check;  //父级选中
                treeView4.Nodes[i].Expand();//展开父级
                for (int j = 0; j < treeView1.Nodes[i].Nodes.Count; j++)
                {
                    treeView4.Nodes[i].Nodes[j].Checked = check;
                }
            }
        }

    }
}
