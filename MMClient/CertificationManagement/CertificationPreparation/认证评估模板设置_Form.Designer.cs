﻿namespace MMClient
{
    partial class 认证评估模板设置_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(认证评估模板设置_Form));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.pnlCommon = new System.Windows.Forms.Panel();
            this.pnlPurchaseCommon = new System.Windows.Forms.Panel();
            this.checkBox36 = new System.Windows.Forms.CheckBox();
            this.checkBox37 = new System.Windows.Forms.CheckBox();
            this.label9 = new System.Windows.Forms.Label();
            this.pnlProductivityCommon = new System.Windows.Forms.Panel();
            this.checkBox39 = new System.Windows.Forms.CheckBox();
            this.checkBox40 = new System.Windows.Forms.CheckBox();
            this.label10 = new System.Windows.Forms.Label();
            this.pnlFinanceCommon = new System.Windows.Forms.Panel();
            this.checkBox25 = new System.Windows.Forms.CheckBox();
            this.checkBox26 = new System.Windows.Forms.CheckBox();
            this.label7 = new System.Windows.Forms.Label();
            this.pnlTechnicalAbilityCommon = new System.Windows.Forms.Panel();
            this.checkBox33 = new System.Windows.Forms.CheckBox();
            this.checkBox31 = new System.Windows.Forms.CheckBox();
            this.checkBox32 = new System.Windows.Forms.CheckBox();
            this.checkBox30 = new System.Windows.Forms.CheckBox();
            this.checkBox27 = new System.Windows.Forms.CheckBox();
            this.checkBox28 = new System.Windows.Forms.CheckBox();
            this.checkBox29 = new System.Windows.Forms.CheckBox();
            this.label8 = new System.Windows.Forms.Label();
            this.pnlAftersalesCommon = new System.Windows.Forms.Panel();
            this.checkBox17 = new System.Windows.Forms.CheckBox();
            this.checkBox18 = new System.Windows.Forms.CheckBox();
            this.checkBox19 = new System.Windows.Forms.CheckBox();
            this.checkBox20 = new System.Windows.Forms.CheckBox();
            this.label5 = new System.Windows.Forms.Label();
            this.pnlLogisticsCommon = new System.Windows.Forms.Panel();
            this.checkBox21 = new System.Windows.Forms.CheckBox();
            this.checkBox22 = new System.Windows.Forms.CheckBox();
            this.checkBox23 = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.pnlQualityCommon = new System.Windows.Forms.Panel();
            this.checkBox15 = new System.Windows.Forms.CheckBox();
            this.checkBox8 = new System.Windows.Forms.CheckBox();
            this.checkBox9 = new System.Windows.Forms.CheckBox();
            this.checkBox10 = new System.Windows.Forms.CheckBox();
            this.checkBox11 = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.pnlEnvironmentCommon = new System.Windows.Forms.Panel();
            this.checkBox12 = new System.Windows.Forms.CheckBox();
            this.checkBox13 = new System.Windows.Forms.CheckBox();
            this.checkBox14 = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.pnlManageCommon = new System.Windows.Forms.Panel();
            this.checkBox7 = new System.Windows.Forms.CheckBox();
            this.checkBox4 = new System.Windows.Forms.CheckBox();
            this.checkBox5 = new System.Windows.Forms.CheckBox();
            this.checkBox6 = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.pnlCompanyProfileCommon = new System.Windows.Forms.Panel();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnPreviewCommon = new System.Windows.Forms.Button();
            this.btnSaveCommon = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.pnlLeveraged = new System.Windows.Forms.Panel();
            this.pnlPurchaseLeveraged = new System.Windows.Forms.Panel();
            this.checkBox16 = new System.Windows.Forms.CheckBox();
            this.checkBox24 = new System.Windows.Forms.CheckBox();
            this.label11 = new System.Windows.Forms.Label();
            this.pnlProductivityLeveraged = new System.Windows.Forms.Panel();
            this.checkBox34 = new System.Windows.Forms.CheckBox();
            this.checkBox35 = new System.Windows.Forms.CheckBox();
            this.label12 = new System.Windows.Forms.Label();
            this.pnlFinanceLeveraged = new System.Windows.Forms.Panel();
            this.checkBox38 = new System.Windows.Forms.CheckBox();
            this.checkBox41 = new System.Windows.Forms.CheckBox();
            this.label13 = new System.Windows.Forms.Label();
            this.pnlTechnicalAbilityLeveraged = new System.Windows.Forms.Panel();
            this.checkBox42 = new System.Windows.Forms.CheckBox();
            this.checkBox43 = new System.Windows.Forms.CheckBox();
            this.checkBox44 = new System.Windows.Forms.CheckBox();
            this.checkBox45 = new System.Windows.Forms.CheckBox();
            this.checkBox46 = new System.Windows.Forms.CheckBox();
            this.checkBox47 = new System.Windows.Forms.CheckBox();
            this.checkBox48 = new System.Windows.Forms.CheckBox();
            this.label14 = new System.Windows.Forms.Label();
            this.pnlAftersalesLeveraged = new System.Windows.Forms.Panel();
            this.checkBox49 = new System.Windows.Forms.CheckBox();
            this.checkBox50 = new System.Windows.Forms.CheckBox();
            this.checkBox51 = new System.Windows.Forms.CheckBox();
            this.checkBox52 = new System.Windows.Forms.CheckBox();
            this.label15 = new System.Windows.Forms.Label();
            this.pnlLogisticsLeveraged = new System.Windows.Forms.Panel();
            this.checkBox53 = new System.Windows.Forms.CheckBox();
            this.checkBox54 = new System.Windows.Forms.CheckBox();
            this.checkBox55 = new System.Windows.Forms.CheckBox();
            this.label16 = new System.Windows.Forms.Label();
            this.pnlQualityLeveraged = new System.Windows.Forms.Panel();
            this.checkBox56 = new System.Windows.Forms.CheckBox();
            this.checkBox57 = new System.Windows.Forms.CheckBox();
            this.checkBox58 = new System.Windows.Forms.CheckBox();
            this.checkBox59 = new System.Windows.Forms.CheckBox();
            this.checkBox60 = new System.Windows.Forms.CheckBox();
            this.label17 = new System.Windows.Forms.Label();
            this.pnlEnvironmentLeveraged = new System.Windows.Forms.Panel();
            this.checkBox61 = new System.Windows.Forms.CheckBox();
            this.checkBox62 = new System.Windows.Forms.CheckBox();
            this.checkBox63 = new System.Windows.Forms.CheckBox();
            this.label18 = new System.Windows.Forms.Label();
            this.pnlManageLeveraged = new System.Windows.Forms.Panel();
            this.checkBox64 = new System.Windows.Forms.CheckBox();
            this.checkBox65 = new System.Windows.Forms.CheckBox();
            this.checkBox66 = new System.Windows.Forms.CheckBox();
            this.checkBox67 = new System.Windows.Forms.CheckBox();
            this.label19 = new System.Windows.Forms.Label();
            this.pnlCompanyProfileLeveraged = new System.Windows.Forms.Panel();
            this.checkBox68 = new System.Windows.Forms.CheckBox();
            this.checkBox69 = new System.Windows.Forms.CheckBox();
            this.checkBox70 = new System.Windows.Forms.CheckBox();
            this.label20 = new System.Windows.Forms.Label();
            this.btnPreviewLeveraged = new System.Windows.Forms.Button();
            this.btnSaveLeveraged = new System.Windows.Forms.Button();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.pnlBottleneck = new System.Windows.Forms.Panel();
            this.pnlPurchaseBottleneck = new System.Windows.Forms.Panel();
            this.checkBox71 = new System.Windows.Forms.CheckBox();
            this.checkBox72 = new System.Windows.Forms.CheckBox();
            this.label21 = new System.Windows.Forms.Label();
            this.pnlProductivityBottleneck = new System.Windows.Forms.Panel();
            this.checkBox73 = new System.Windows.Forms.CheckBox();
            this.checkBox74 = new System.Windows.Forms.CheckBox();
            this.label22 = new System.Windows.Forms.Label();
            this.pnlFinanceBottleneck = new System.Windows.Forms.Panel();
            this.checkBox75 = new System.Windows.Forms.CheckBox();
            this.checkBox76 = new System.Windows.Forms.CheckBox();
            this.label23 = new System.Windows.Forms.Label();
            this.pnlTechnicalAbilityBottleneck = new System.Windows.Forms.Panel();
            this.checkBox77 = new System.Windows.Forms.CheckBox();
            this.checkBox78 = new System.Windows.Forms.CheckBox();
            this.checkBox79 = new System.Windows.Forms.CheckBox();
            this.checkBox80 = new System.Windows.Forms.CheckBox();
            this.checkBox81 = new System.Windows.Forms.CheckBox();
            this.checkBox82 = new System.Windows.Forms.CheckBox();
            this.checkBox83 = new System.Windows.Forms.CheckBox();
            this.label24 = new System.Windows.Forms.Label();
            this.pnlAftersalesBottleneck = new System.Windows.Forms.Panel();
            this.checkBox84 = new System.Windows.Forms.CheckBox();
            this.checkBox85 = new System.Windows.Forms.CheckBox();
            this.checkBox86 = new System.Windows.Forms.CheckBox();
            this.checkBox87 = new System.Windows.Forms.CheckBox();
            this.label25 = new System.Windows.Forms.Label();
            this.pnlLogisticsBottleneck = new System.Windows.Forms.Panel();
            this.checkBox88 = new System.Windows.Forms.CheckBox();
            this.checkBox89 = new System.Windows.Forms.CheckBox();
            this.checkBox90 = new System.Windows.Forms.CheckBox();
            this.label26 = new System.Windows.Forms.Label();
            this.pnlQualityBottleneck = new System.Windows.Forms.Panel();
            this.checkBox91 = new System.Windows.Forms.CheckBox();
            this.checkBox92 = new System.Windows.Forms.CheckBox();
            this.checkBox93 = new System.Windows.Forms.CheckBox();
            this.checkBox94 = new System.Windows.Forms.CheckBox();
            this.checkBox95 = new System.Windows.Forms.CheckBox();
            this.label27 = new System.Windows.Forms.Label();
            this.pnlEnvironmentBottleneck = new System.Windows.Forms.Panel();
            this.checkBox96 = new System.Windows.Forms.CheckBox();
            this.checkBox97 = new System.Windows.Forms.CheckBox();
            this.checkBox98 = new System.Windows.Forms.CheckBox();
            this.label28 = new System.Windows.Forms.Label();
            this.pnlManageBottleneck = new System.Windows.Forms.Panel();
            this.checkBox99 = new System.Windows.Forms.CheckBox();
            this.checkBox100 = new System.Windows.Forms.CheckBox();
            this.checkBox101 = new System.Windows.Forms.CheckBox();
            this.checkBox102 = new System.Windows.Forms.CheckBox();
            this.label29 = new System.Windows.Forms.Label();
            this.pnlCompanyProfileBottleneck = new System.Windows.Forms.Panel();
            this.checkBox103 = new System.Windows.Forms.CheckBox();
            this.checkBox104 = new System.Windows.Forms.CheckBox();
            this.checkBox105 = new System.Windows.Forms.CheckBox();
            this.label30 = new System.Windows.Forms.Label();
            this.btnPreviewBottleneck = new System.Windows.Forms.Button();
            this.btnSaveBottleneck = new System.Windows.Forms.Button();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.pnlKey = new System.Windows.Forms.Panel();
            this.pnlPurchaseKey = new System.Windows.Forms.Panel();
            this.checkBox106 = new System.Windows.Forms.CheckBox();
            this.checkBox107 = new System.Windows.Forms.CheckBox();
            this.label31 = new System.Windows.Forms.Label();
            this.pnlProductivityKey = new System.Windows.Forms.Panel();
            this.checkBox108 = new System.Windows.Forms.CheckBox();
            this.checkBox109 = new System.Windows.Forms.CheckBox();
            this.label32 = new System.Windows.Forms.Label();
            this.pnlFinanceKey = new System.Windows.Forms.Panel();
            this.checkBox110 = new System.Windows.Forms.CheckBox();
            this.checkBox111 = new System.Windows.Forms.CheckBox();
            this.label33 = new System.Windows.Forms.Label();
            this.pnlTechnicalAbilityKey = new System.Windows.Forms.Panel();
            this.checkBox112 = new System.Windows.Forms.CheckBox();
            this.checkBox113 = new System.Windows.Forms.CheckBox();
            this.checkBox114 = new System.Windows.Forms.CheckBox();
            this.checkBox115 = new System.Windows.Forms.CheckBox();
            this.checkBox116 = new System.Windows.Forms.CheckBox();
            this.checkBox117 = new System.Windows.Forms.CheckBox();
            this.checkBox118 = new System.Windows.Forms.CheckBox();
            this.label34 = new System.Windows.Forms.Label();
            this.pnlAftersalesKey = new System.Windows.Forms.Panel();
            this.checkBox119 = new System.Windows.Forms.CheckBox();
            this.checkBox120 = new System.Windows.Forms.CheckBox();
            this.checkBox121 = new System.Windows.Forms.CheckBox();
            this.checkBox122 = new System.Windows.Forms.CheckBox();
            this.label35 = new System.Windows.Forms.Label();
            this.pnlLogisticsKey = new System.Windows.Forms.Panel();
            this.checkBox123 = new System.Windows.Forms.CheckBox();
            this.checkBox124 = new System.Windows.Forms.CheckBox();
            this.checkBox125 = new System.Windows.Forms.CheckBox();
            this.label36 = new System.Windows.Forms.Label();
            this.pnlQualityKey = new System.Windows.Forms.Panel();
            this.checkBox126 = new System.Windows.Forms.CheckBox();
            this.checkBox127 = new System.Windows.Forms.CheckBox();
            this.checkBox128 = new System.Windows.Forms.CheckBox();
            this.checkBox129 = new System.Windows.Forms.CheckBox();
            this.checkBox130 = new System.Windows.Forms.CheckBox();
            this.label37 = new System.Windows.Forms.Label();
            this.pnlEnvironmentKey = new System.Windows.Forms.Panel();
            this.checkBox131 = new System.Windows.Forms.CheckBox();
            this.checkBox132 = new System.Windows.Forms.CheckBox();
            this.checkBox133 = new System.Windows.Forms.CheckBox();
            this.label38 = new System.Windows.Forms.Label();
            this.pnlManageKey = new System.Windows.Forms.Panel();
            this.checkBox134 = new System.Windows.Forms.CheckBox();
            this.checkBox135 = new System.Windows.Forms.CheckBox();
            this.checkBox136 = new System.Windows.Forms.CheckBox();
            this.checkBox137 = new System.Windows.Forms.CheckBox();
            this.label39 = new System.Windows.Forms.Label();
            this.pnlCompanyProfileKey = new System.Windows.Forms.Panel();
            this.checkBox138 = new System.Windows.Forms.CheckBox();
            this.checkBox139 = new System.Windows.Forms.CheckBox();
            this.checkBox140 = new System.Windows.Forms.CheckBox();
            this.label40 = new System.Windows.Forms.Label();
            this.btnPreviewKey = new System.Windows.Forms.Button();
            this.btnSaveKey = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.pnlCommon.SuspendLayout();
            this.pnlPurchaseCommon.SuspendLayout();
            this.pnlProductivityCommon.SuspendLayout();
            this.pnlFinanceCommon.SuspendLayout();
            this.pnlTechnicalAbilityCommon.SuspendLayout();
            this.pnlAftersalesCommon.SuspendLayout();
            this.pnlLogisticsCommon.SuspendLayout();
            this.pnlQualityCommon.SuspendLayout();
            this.pnlEnvironmentCommon.SuspendLayout();
            this.pnlManageCommon.SuspendLayout();
            this.pnlCompanyProfileCommon.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.pnlLeveraged.SuspendLayout();
            this.pnlPurchaseLeveraged.SuspendLayout();
            this.pnlProductivityLeveraged.SuspendLayout();
            this.pnlFinanceLeveraged.SuspendLayout();
            this.pnlTechnicalAbilityLeveraged.SuspendLayout();
            this.pnlAftersalesLeveraged.SuspendLayout();
            this.pnlLogisticsLeveraged.SuspendLayout();
            this.pnlQualityLeveraged.SuspendLayout();
            this.pnlEnvironmentLeveraged.SuspendLayout();
            this.pnlManageLeveraged.SuspendLayout();
            this.pnlCompanyProfileLeveraged.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.pnlBottleneck.SuspendLayout();
            this.pnlPurchaseBottleneck.SuspendLayout();
            this.pnlProductivityBottleneck.SuspendLayout();
            this.pnlFinanceBottleneck.SuspendLayout();
            this.pnlTechnicalAbilityBottleneck.SuspendLayout();
            this.pnlAftersalesBottleneck.SuspendLayout();
            this.pnlLogisticsBottleneck.SuspendLayout();
            this.pnlQualityBottleneck.SuspendLayout();
            this.pnlEnvironmentBottleneck.SuspendLayout();
            this.pnlManageBottleneck.SuspendLayout();
            this.pnlCompanyProfileBottleneck.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.pnlKey.SuspendLayout();
            this.pnlPurchaseKey.SuspendLayout();
            this.pnlProductivityKey.SuspendLayout();
            this.pnlFinanceKey.SuspendLayout();
            this.pnlTechnicalAbilityKey.SuspendLayout();
            this.pnlAftersalesKey.SuspendLayout();
            this.pnlLogisticsKey.SuspendLayout();
            this.pnlQualityKey.SuspendLayout();
            this.pnlEnvironmentKey.SuspendLayout();
            this.pnlManageKey.SuspendLayout();
            this.pnlCompanyProfileKey.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tabControl1.Location = new System.Drawing.Point(7, 44);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(931, 656);
            this.tabControl1.TabIndex = 1;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.pnlCommon);
            this.tabPage1.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(923, 627);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "一般型";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // pnlCommon
            // 
            this.pnlCommon.Controls.Add(this.pnlPurchaseCommon);
            this.pnlCommon.Controls.Add(this.pnlProductivityCommon);
            this.pnlCommon.Controls.Add(this.pnlFinanceCommon);
            this.pnlCommon.Controls.Add(this.pnlTechnicalAbilityCommon);
            this.pnlCommon.Controls.Add(this.pnlAftersalesCommon);
            this.pnlCommon.Controls.Add(this.pnlLogisticsCommon);
            this.pnlCommon.Controls.Add(this.pnlQualityCommon);
            this.pnlCommon.Controls.Add(this.pnlEnvironmentCommon);
            this.pnlCommon.Controls.Add(this.pnlManageCommon);
            this.pnlCommon.Controls.Add(this.pnlCompanyProfileCommon);
            this.pnlCommon.Controls.Add(this.btnPreviewCommon);
            this.pnlCommon.Controls.Add(this.btnSaveCommon);
            this.pnlCommon.Location = new System.Drawing.Point(5, 8);
            this.pnlCommon.Name = "pnlCommon";
            this.pnlCommon.Size = new System.Drawing.Size(913, 625);
            this.pnlCommon.TabIndex = 1;
            // 
            // pnlPurchaseCommon
            // 
            this.pnlPurchaseCommon.Controls.Add(this.checkBox36);
            this.pnlPurchaseCommon.Controls.Add(this.checkBox37);
            this.pnlPurchaseCommon.Controls.Add(this.label9);
            this.pnlPurchaseCommon.Location = new System.Drawing.Point(19, 517);
            this.pnlPurchaseCommon.Name = "pnlPurchaseCommon";
            this.pnlPurchaseCommon.Size = new System.Drawing.Size(355, 50);
            this.pnlPurchaseCommon.TabIndex = 59;
            this.pnlPurchaseCommon.Tag = "9";
            // 
            // checkBox36
            // 
            this.checkBox36.AutoSize = true;
            this.checkBox36.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox36.Location = new System.Drawing.Point(151, 28);
            this.checkBox36.Name = "checkBox36";
            this.checkBox36.Size = new System.Drawing.Size(110, 18);
            this.checkBox36.TabIndex = 50;
            this.checkBox36.Text = "分包方的业绩";
            this.checkBox36.UseVisualStyleBackColor = true;
            // 
            // checkBox37
            // 
            this.checkBox37.AutoSize = true;
            this.checkBox37.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox37.Location = new System.Drawing.Point(16, 29);
            this.checkBox37.Name = "checkBox37";
            this.checkBox37.Size = new System.Drawing.Size(82, 18);
            this.checkBox37.TabIndex = 49;
            this.checkBox37.Text = "采购程序";
            this.checkBox37.UseVisualStyleBackColor = true;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label9.Location = new System.Drawing.Point(4, 6);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(42, 16);
            this.label9.TabIndex = 48;
            this.label9.Text = "采购";
            // 
            // pnlProductivityCommon
            // 
            this.pnlProductivityCommon.Controls.Add(this.checkBox39);
            this.pnlProductivityCommon.Controls.Add(this.checkBox40);
            this.pnlProductivityCommon.Controls.Add(this.label10);
            this.pnlProductivityCommon.Location = new System.Drawing.Point(19, 465);
            this.pnlProductivityCommon.Name = "pnlProductivityCommon";
            this.pnlProductivityCommon.Size = new System.Drawing.Size(355, 50);
            this.pnlProductivityCommon.TabIndex = 58;
            this.pnlProductivityCommon.Tag = "8";
            // 
            // checkBox39
            // 
            this.checkBox39.AutoSize = true;
            this.checkBox39.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox39.Location = new System.Drawing.Point(154, 28);
            this.checkBox39.Name = "checkBox39";
            this.checkBox39.Size = new System.Drawing.Size(82, 18);
            this.checkBox39.TabIndex = 46;
            this.checkBox39.Text = "成本目标";
            this.checkBox39.UseVisualStyleBackColor = true;
            // 
            // checkBox40
            // 
            this.checkBox40.AutoSize = true;
            this.checkBox40.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox40.Location = new System.Drawing.Point(14, 28);
            this.checkBox40.Name = "checkBox40";
            this.checkBox40.Size = new System.Drawing.Size(138, 18);
            this.checkBox40.TabIndex = 45;
            this.checkBox40.Text = "内部成本降低程序";
            this.checkBox40.UseVisualStyleBackColor = true;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label10.Location = new System.Drawing.Point(3, 4);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(76, 16);
            this.label10.TabIndex = 44;
            this.label10.Text = "生产效率";
            // 
            // pnlFinanceCommon
            // 
            this.pnlFinanceCommon.Controls.Add(this.checkBox25);
            this.pnlFinanceCommon.Controls.Add(this.checkBox26);
            this.pnlFinanceCommon.Controls.Add(this.label7);
            this.pnlFinanceCommon.Location = new System.Drawing.Point(19, 409);
            this.pnlFinanceCommon.Name = "pnlFinanceCommon";
            this.pnlFinanceCommon.Size = new System.Drawing.Size(355, 51);
            this.pnlFinanceCommon.TabIndex = 57;
            this.pnlFinanceCommon.Tag = "7";
            // 
            // checkBox25
            // 
            this.checkBox25.AutoSize = true;
            this.checkBox25.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox25.Location = new System.Drawing.Point(146, 27);
            this.checkBox25.Name = "checkBox25";
            this.checkBox25.Size = new System.Drawing.Size(82, 18);
            this.checkBox25.TabIndex = 37;
            this.checkBox25.Text = "财款期限";
            this.checkBox25.UseVisualStyleBackColor = true;
            // 
            // checkBox26
            // 
            this.checkBox26.AutoSize = true;
            this.checkBox26.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox26.Location = new System.Drawing.Point(11, 28);
            this.checkBox26.Name = "checkBox26";
            this.checkBox26.Size = new System.Drawing.Size(82, 18);
            this.checkBox26.TabIndex = 36;
            this.checkBox26.Text = "财务评价";
            this.checkBox26.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label7.Location = new System.Drawing.Point(4, 5);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(42, 16);
            this.label7.TabIndex = 35;
            this.label7.Text = "财务";
            // 
            // pnlTechnicalAbilityCommon
            // 
            this.pnlTechnicalAbilityCommon.Controls.Add(this.checkBox33);
            this.pnlTechnicalAbilityCommon.Controls.Add(this.checkBox31);
            this.pnlTechnicalAbilityCommon.Controls.Add(this.checkBox32);
            this.pnlTechnicalAbilityCommon.Controls.Add(this.checkBox30);
            this.pnlTechnicalAbilityCommon.Controls.Add(this.checkBox27);
            this.pnlTechnicalAbilityCommon.Controls.Add(this.checkBox28);
            this.pnlTechnicalAbilityCommon.Controls.Add(this.checkBox29);
            this.pnlTechnicalAbilityCommon.Controls.Add(this.label8);
            this.pnlTechnicalAbilityCommon.Location = new System.Drawing.Point(19, 326);
            this.pnlTechnicalAbilityCommon.Name = "pnlTechnicalAbilityCommon";
            this.pnlTechnicalAbilityCommon.Size = new System.Drawing.Size(572, 80);
            this.pnlTechnicalAbilityCommon.TabIndex = 56;
            this.pnlTechnicalAbilityCommon.Tag = "6";
            // 
            // checkBox33
            // 
            this.checkBox33.AutoSize = true;
            this.checkBox33.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox33.Location = new System.Drawing.Point(245, 55);
            this.checkBox33.Name = "checkBox33";
            this.checkBox33.Size = new System.Drawing.Size(82, 18);
            this.checkBox33.TabIndex = 48;
            this.checkBox33.Text = "设计更改";
            this.checkBox33.UseVisualStyleBackColor = true;
            // 
            // checkBox31
            // 
            this.checkBox31.AutoSize = true;
            this.checkBox31.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox31.Location = new System.Drawing.Point(147, 55);
            this.checkBox31.Name = "checkBox31";
            this.checkBox31.Size = new System.Drawing.Size(54, 18);
            this.checkBox31.TabIndex = 47;
            this.checkBox31.Text = "设计";
            this.checkBox31.UseVisualStyleBackColor = true;
            // 
            // checkBox32
            // 
            this.checkBox32.AutoSize = true;
            this.checkBox32.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox32.Location = new System.Drawing.Point(8, 55);
            this.checkBox32.Name = "checkBox32";
            this.checkBox32.Size = new System.Drawing.Size(54, 18);
            this.checkBox32.TabIndex = 46;
            this.checkBox32.Text = "研发";
            this.checkBox32.UseVisualStyleBackColor = true;
            // 
            // checkBox30
            // 
            this.checkBox30.AutoSize = true;
            this.checkBox30.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox30.Location = new System.Drawing.Point(438, 31);
            this.checkBox30.Name = "checkBox30";
            this.checkBox30.Size = new System.Drawing.Size(82, 18);
            this.checkBox30.TabIndex = 45;
            this.checkBox30.Text = "试制样件";
            this.checkBox30.UseVisualStyleBackColor = true;
            // 
            // checkBox27
            // 
            this.checkBox27.AutoSize = true;
            this.checkBox27.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox27.Location = new System.Drawing.Point(245, 31);
            this.checkBox27.Name = "checkBox27";
            this.checkBox27.Size = new System.Drawing.Size(110, 18);
            this.checkBox27.TabIndex = 44;
            this.checkBox27.Text = "产品工程技术";
            this.checkBox27.UseVisualStyleBackColor = true;
            // 
            // checkBox28
            // 
            this.checkBox28.AutoSize = true;
            this.checkBox28.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox28.Location = new System.Drawing.Point(147, 31);
            this.checkBox28.Name = "checkBox28";
            this.checkBox28.Size = new System.Drawing.Size(82, 18);
            this.checkBox28.TabIndex = 43;
            this.checkBox28.Text = "工程经验";
            this.checkBox28.UseVisualStyleBackColor = true;
            // 
            // checkBox29
            // 
            this.checkBox29.AutoSize = true;
            this.checkBox29.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox29.Location = new System.Drawing.Point(8, 31);
            this.checkBox29.Name = "checkBox29";
            this.checkBox29.Size = new System.Drawing.Size(124, 18);
            this.checkBox29.TabIndex = 42;
            this.checkBox29.Text = "产品和工程技术";
            this.checkBox29.UseVisualStyleBackColor = true;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label8.Location = new System.Drawing.Point(5, 7);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(76, 16);
            this.label8.TabIndex = 41;
            this.label8.Text = "技术能力";
            // 
            // pnlAftersalesCommon
            // 
            this.pnlAftersalesCommon.Controls.Add(this.checkBox17);
            this.pnlAftersalesCommon.Controls.Add(this.checkBox18);
            this.pnlAftersalesCommon.Controls.Add(this.checkBox19);
            this.pnlAftersalesCommon.Controls.Add(this.checkBox20);
            this.pnlAftersalesCommon.Controls.Add(this.label5);
            this.pnlAftersalesCommon.Location = new System.Drawing.Point(19, 269);
            this.pnlAftersalesCommon.Name = "pnlAftersalesCommon";
            this.pnlAftersalesCommon.Size = new System.Drawing.Size(572, 54);
            this.pnlAftersalesCommon.TabIndex = 55;
            this.pnlAftersalesCommon.Tag = "5";
            // 
            // checkBox17
            // 
            this.checkBox17.AutoSize = true;
            this.checkBox17.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox17.Location = new System.Drawing.Point(441, 29);
            this.checkBox17.Name = "checkBox17";
            this.checkBox17.Size = new System.Drawing.Size(54, 18);
            this.checkBox17.TabIndex = 32;
            this.checkBox17.Text = "赔偿";
            this.checkBox17.UseVisualStyleBackColor = true;
            // 
            // checkBox18
            // 
            this.checkBox18.AutoSize = true;
            this.checkBox18.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox18.Location = new System.Drawing.Point(248, 28);
            this.checkBox18.Name = "checkBox18";
            this.checkBox18.Size = new System.Drawing.Size(96, 18);
            this.checkBox18.TabIndex = 31;
            this.checkBox18.Text = "合作和支持";
            this.checkBox18.UseVisualStyleBackColor = true;
            // 
            // checkBox19
            // 
            this.checkBox19.AutoSize = true;
            this.checkBox19.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox19.Location = new System.Drawing.Point(150, 28);
            this.checkBox19.Name = "checkBox19";
            this.checkBox19.Size = new System.Drawing.Size(82, 18);
            this.checkBox19.TabIndex = 30;
            this.checkBox19.Text = "服务读物";
            this.checkBox19.UseVisualStyleBackColor = true;
            // 
            // checkBox20
            // 
            this.checkBox20.AutoSize = true;
            this.checkBox20.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox20.Location = new System.Drawing.Point(11, 29);
            this.checkBox20.Name = "checkBox20";
            this.checkBox20.Size = new System.Drawing.Size(138, 18);
            this.checkBox20.TabIndex = 29;
            this.checkBox20.Text = "技术文件（售后）";
            this.checkBox20.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label5.Location = new System.Drawing.Point(4, 6);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(42, 16);
            this.label5.TabIndex = 28;
            this.label5.Text = "售后";
            // 
            // pnlLogisticsCommon
            // 
            this.pnlLogisticsCommon.Controls.Add(this.checkBox21);
            this.pnlLogisticsCommon.Controls.Add(this.checkBox22);
            this.pnlLogisticsCommon.Controls.Add(this.checkBox23);
            this.pnlLogisticsCommon.Controls.Add(this.label6);
            this.pnlLogisticsCommon.Location = new System.Drawing.Point(19, 213);
            this.pnlLogisticsCommon.Name = "pnlLogisticsCommon";
            this.pnlLogisticsCommon.Size = new System.Drawing.Size(572, 52);
            this.pnlLogisticsCommon.TabIndex = 54;
            this.pnlLogisticsCommon.Tag = "4";
            // 
            // checkBox21
            // 
            this.checkBox21.AutoSize = true;
            this.checkBox21.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox21.Location = new System.Drawing.Point(314, 29);
            this.checkBox21.Name = "checkBox21";
            this.checkBox21.Size = new System.Drawing.Size(166, 18);
            this.checkBox21.TabIndex = 26;
            this.checkBox21.Text = "环境审核--产品和服务";
            this.checkBox21.UseVisualStyleBackColor = true;
            // 
            // checkBox22
            // 
            this.checkBox22.AutoSize = true;
            this.checkBox22.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox22.Location = new System.Drawing.Point(129, 29);
            this.checkBox22.Name = "checkBox22";
            this.checkBox22.Size = new System.Drawing.Size(159, 18);
            this.checkBox22.TabIndex = 25;
            this.checkBox22.Text = "交付准确性/服务水平";
            this.checkBox22.UseVisualStyleBackColor = true;
            // 
            // checkBox23
            // 
            this.checkBox23.AutoSize = true;
            this.checkBox23.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox23.Location = new System.Drawing.Point(13, 29);
            this.checkBox23.Name = "checkBox23";
            this.checkBox23.Size = new System.Drawing.Size(82, 18);
            this.checkBox23.TabIndex = 24;
            this.checkBox23.Text = "物流体系";
            this.checkBox23.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label6.Location = new System.Drawing.Point(5, 5);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(42, 16);
            this.label6.TabIndex = 23;
            this.label6.Text = "物流";
            // 
            // pnlQualityCommon
            // 
            this.pnlQualityCommon.Controls.Add(this.checkBox15);
            this.pnlQualityCommon.Controls.Add(this.checkBox8);
            this.pnlQualityCommon.Controls.Add(this.checkBox9);
            this.pnlQualityCommon.Controls.Add(this.checkBox10);
            this.pnlQualityCommon.Controls.Add(this.checkBox11);
            this.pnlQualityCommon.Controls.Add(this.label3);
            this.pnlQualityCommon.Location = new System.Drawing.Point(19, 155);
            this.pnlQualityCommon.Name = "pnlQualityCommon";
            this.pnlQualityCommon.Size = new System.Drawing.Size(634, 56);
            this.pnlQualityCommon.TabIndex = 53;
            this.pnlQualityCommon.Tag = "3";
            // 
            // checkBox15
            // 
            this.checkBox15.AutoSize = true;
            this.checkBox15.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox15.Location = new System.Drawing.Point(537, 31);
            this.checkBox15.Name = "checkBox15";
            this.checkBox15.Size = new System.Drawing.Size(82, 18);
            this.checkBox15.TabIndex = 24;
            this.checkBox15.Text = "问题处理";
            this.checkBox15.UseVisualStyleBackColor = true;
            // 
            // checkBox8
            // 
            this.checkBox8.AutoSize = true;
            this.checkBox8.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox8.Location = new System.Drawing.Point(453, 31);
            this.checkBox8.Name = "checkBox8";
            this.checkBox8.Size = new System.Drawing.Size(68, 18);
            this.checkBox8.TabIndex = 23;
            this.checkBox8.Text = "可靠性";
            this.checkBox8.UseVisualStyleBackColor = true;
            // 
            // checkBox9
            // 
            this.checkBox9.AutoSize = true;
            this.checkBox9.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox9.Location = new System.Drawing.Point(325, 30);
            this.checkBox9.Name = "checkBox9";
            this.checkBox9.Size = new System.Drawing.Size(110, 18);
            this.checkBox9.TabIndex = 22;
            this.checkBox9.Text = "交付质量业绩";
            this.checkBox9.UseVisualStyleBackColor = true;
            // 
            // checkBox10
            // 
            this.checkBox10.AutoSize = true;
            this.checkBox10.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox10.Location = new System.Drawing.Point(139, 30);
            this.checkBox10.Name = "checkBox10";
            this.checkBox10.Size = new System.Drawing.Size(180, 18);
            this.checkBox10.TabIndex = 21;
            this.checkBox10.Text = "质量策划--部品质量保证";
            this.checkBox10.UseVisualStyleBackColor = true;
            // 
            // checkBox11
            // 
            this.checkBox11.AutoSize = true;
            this.checkBox11.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox11.Location = new System.Drawing.Point(23, 31);
            this.checkBox11.Name = "checkBox11";
            this.checkBox11.Size = new System.Drawing.Size(110, 18);
            this.checkBox11.TabIndex = 20;
            this.checkBox11.Text = "质量管理体系";
            this.checkBox11.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label3.Location = new System.Drawing.Point(5, 6);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 16);
            this.label3.TabIndex = 19;
            this.label3.Text = "质量";
            // 
            // pnlEnvironmentCommon
            // 
            this.pnlEnvironmentCommon.Controls.Add(this.checkBox12);
            this.pnlEnvironmentCommon.Controls.Add(this.checkBox13);
            this.pnlEnvironmentCommon.Controls.Add(this.checkBox14);
            this.pnlEnvironmentCommon.Controls.Add(this.label4);
            this.pnlEnvironmentCommon.Location = new System.Drawing.Point(19, 106);
            this.pnlEnvironmentCommon.Name = "pnlEnvironmentCommon";
            this.pnlEnvironmentCommon.Size = new System.Drawing.Size(520, 47);
            this.pnlEnvironmentCommon.TabIndex = 52;
            this.pnlEnvironmentCommon.Tag = "2";
            // 
            // checkBox12
            // 
            this.checkBox12.AutoSize = true;
            this.checkBox12.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox12.Location = new System.Drawing.Point(294, 23);
            this.checkBox12.Name = "checkBox12";
            this.checkBox12.Size = new System.Drawing.Size(166, 18);
            this.checkBox12.TabIndex = 15;
            this.checkBox12.Text = "环境审核--产品和服务";
            this.checkBox12.UseVisualStyleBackColor = true;
            // 
            // checkBox13
            // 
            this.checkBox13.AutoSize = true;
            this.checkBox13.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox13.Location = new System.Drawing.Point(125, 23);
            this.checkBox13.Name = "checkBox13";
            this.checkBox13.Size = new System.Drawing.Size(152, 18);
            this.checkBox13.TabIndex = 14;
            this.checkBox13.Text = "环境审核--公司水平";
            this.checkBox13.UseVisualStyleBackColor = true;
            // 
            // checkBox14
            // 
            this.checkBox14.AutoSize = true;
            this.checkBox14.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox14.Location = new System.Drawing.Point(7, 23);
            this.checkBox14.Name = "checkBox14";
            this.checkBox14.Size = new System.Drawing.Size(110, 18);
            this.checkBox14.TabIndex = 13;
            this.checkBox14.Text = "环境管理体系";
            this.checkBox14.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label4.Location = new System.Drawing.Point(3, 4);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(42, 16);
            this.label4.TabIndex = 9;
            this.label4.Text = "环境";
            // 
            // pnlManageCommon
            // 
            this.pnlManageCommon.Controls.Add(this.checkBox7);
            this.pnlManageCommon.Controls.Add(this.checkBox4);
            this.pnlManageCommon.Controls.Add(this.checkBox5);
            this.pnlManageCommon.Controls.Add(this.checkBox6);
            this.pnlManageCommon.Controls.Add(this.label2);
            this.pnlManageCommon.Location = new System.Drawing.Point(19, 58);
            this.pnlManageCommon.Name = "pnlManageCommon";
            this.pnlManageCommon.Size = new System.Drawing.Size(400, 47);
            this.pnlManageCommon.TabIndex = 51;
            this.pnlManageCommon.Tag = "1";
            // 
            // checkBox7
            // 
            this.checkBox7.AutoSize = true;
            this.checkBox7.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox7.Location = new System.Drawing.Point(294, 24);
            this.checkBox7.Name = "checkBox7";
            this.checkBox7.Size = new System.Drawing.Size(82, 18);
            this.checkBox7.TabIndex = 13;
            this.checkBox7.Text = "风险管理";
            this.checkBox7.UseVisualStyleBackColor = true;
            // 
            // checkBox4
            // 
            this.checkBox4.AutoSize = true;
            this.checkBox4.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox4.Location = new System.Drawing.Point(185, 24);
            this.checkBox4.Name = "checkBox4";
            this.checkBox4.Size = new System.Drawing.Size(103, 18);
            this.checkBox4.TabIndex = 12;
            this.checkBox4.Text = "TQM工作程序";
            this.checkBox4.UseVisualStyleBackColor = true;
            // 
            // checkBox5
            // 
            this.checkBox5.AutoSize = true;
            this.checkBox5.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox5.Location = new System.Drawing.Point(97, 24);
            this.checkBox5.Name = "checkBox5";
            this.checkBox5.Size = new System.Drawing.Size(82, 18);
            this.checkBox5.TabIndex = 11;
            this.checkBox5.Text = "客户满意";
            this.checkBox5.UseVisualStyleBackColor = true;
            // 
            // checkBox6
            // 
            this.checkBox6.AutoSize = true;
            this.checkBox6.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox6.Location = new System.Drawing.Point(8, 24);
            this.checkBox6.Name = "checkBox6";
            this.checkBox6.Size = new System.Drawing.Size(54, 18);
            this.checkBox6.TabIndex = 10;
            this.checkBox6.Text = "管理";
            this.checkBox6.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.Location = new System.Drawing.Point(4, 3);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 16);
            this.label2.TabIndex = 9;
            this.label2.Text = "管理";
            // 
            // pnlCompanyProfileCommon
            // 
            this.pnlCompanyProfileCommon.Controls.Add(this.checkBox3);
            this.pnlCompanyProfileCommon.Controls.Add(this.checkBox2);
            this.pnlCompanyProfileCommon.Controls.Add(this.checkBox1);
            this.pnlCompanyProfileCommon.Controls.Add(this.label1);
            this.pnlCompanyProfileCommon.Location = new System.Drawing.Point(19, 4);
            this.pnlCompanyProfileCommon.Name = "pnlCompanyProfileCommon";
            this.pnlCompanyProfileCommon.Size = new System.Drawing.Size(309, 52);
            this.pnlCompanyProfileCommon.TabIndex = 50;
            this.pnlCompanyProfileCommon.Tag = "0";
            // 
            // checkBox3
            // 
            this.checkBox3.AutoSize = true;
            this.checkBox3.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox3.Location = new System.Drawing.Point(185, 30);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(68, 18);
            this.checkBox3.TabIndex = 7;
            this.checkBox3.Text = "依附性";
            this.checkBox3.UseVisualStyleBackColor = true;
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox2.Location = new System.Drawing.Point(97, 30);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(82, 18);
            this.checkBox2.TabIndex = 6;
            this.checkBox2.Text = "全球能力";
            this.checkBox2.UseVisualStyleBackColor = true;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox1.Location = new System.Drawing.Point(8, 30);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(82, 18);
            this.checkBox1.TabIndex = 5;
            this.checkBox1.Text = "股权关系";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(4, 6);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(76, 16);
            this.label1.TabIndex = 4;
            this.label1.Text = "公司概况";
            // 
            // btnPreviewCommon
            // 
            this.btnPreviewCommon.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnPreviewCommon.Location = new System.Drawing.Point(482, 573);
            this.btnPreviewCommon.Name = "btnPreviewCommon";
            this.btnPreviewCommon.Size = new System.Drawing.Size(75, 32);
            this.btnPreviewCommon.TabIndex = 49;
            this.btnPreviewCommon.Text = "预览";
            this.btnPreviewCommon.UseVisualStyleBackColor = true;
            this.btnPreviewCommon.Click += new System.EventHandler(this.btnPreviewCommon_Click);
            // 
            // btnSaveCommon
            // 
            this.btnSaveCommon.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnSaveCommon.Location = new System.Drawing.Point(288, 573);
            this.btnSaveCommon.Name = "btnSaveCommon";
            this.btnSaveCommon.Size = new System.Drawing.Size(75, 32);
            this.btnSaveCommon.TabIndex = 48;
            this.btnSaveCommon.Text = "保存";
            this.btnSaveCommon.UseVisualStyleBackColor = true;
            this.btnSaveCommon.Click += new System.EventHandler(this.btnSaveCommon_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.pnlLeveraged);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(923, 627);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "杠杆型";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // pnlLeveraged
            // 
            this.pnlLeveraged.Controls.Add(this.pnlPurchaseLeveraged);
            this.pnlLeveraged.Controls.Add(this.pnlProductivityLeveraged);
            this.pnlLeveraged.Controls.Add(this.pnlFinanceLeveraged);
            this.pnlLeveraged.Controls.Add(this.pnlTechnicalAbilityLeveraged);
            this.pnlLeveraged.Controls.Add(this.pnlAftersalesLeveraged);
            this.pnlLeveraged.Controls.Add(this.pnlLogisticsLeveraged);
            this.pnlLeveraged.Controls.Add(this.pnlQualityLeveraged);
            this.pnlLeveraged.Controls.Add(this.pnlEnvironmentLeveraged);
            this.pnlLeveraged.Controls.Add(this.pnlManageLeveraged);
            this.pnlLeveraged.Controls.Add(this.pnlCompanyProfileLeveraged);
            this.pnlLeveraged.Controls.Add(this.btnPreviewLeveraged);
            this.pnlLeveraged.Controls.Add(this.btnSaveLeveraged);
            this.pnlLeveraged.Location = new System.Drawing.Point(5, 8);
            this.pnlLeveraged.Name = "pnlLeveraged";
            this.pnlLeveraged.Size = new System.Drawing.Size(913, 625);
            this.pnlLeveraged.TabIndex = 2;
            // 
            // pnlPurchaseLeveraged
            // 
            this.pnlPurchaseLeveraged.Controls.Add(this.checkBox16);
            this.pnlPurchaseLeveraged.Controls.Add(this.checkBox24);
            this.pnlPurchaseLeveraged.Controls.Add(this.label11);
            this.pnlPurchaseLeveraged.Location = new System.Drawing.Point(19, 517);
            this.pnlPurchaseLeveraged.Name = "pnlPurchaseLeveraged";
            this.pnlPurchaseLeveraged.Size = new System.Drawing.Size(355, 50);
            this.pnlPurchaseLeveraged.TabIndex = 59;
            // 
            // checkBox16
            // 
            this.checkBox16.AutoSize = true;
            this.checkBox16.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox16.Location = new System.Drawing.Point(151, 28);
            this.checkBox16.Name = "checkBox16";
            this.checkBox16.Size = new System.Drawing.Size(110, 18);
            this.checkBox16.TabIndex = 50;
            this.checkBox16.Text = "分包方的业绩";
            this.checkBox16.UseVisualStyleBackColor = true;
            // 
            // checkBox24
            // 
            this.checkBox24.AutoSize = true;
            this.checkBox24.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox24.Location = new System.Drawing.Point(16, 29);
            this.checkBox24.Name = "checkBox24";
            this.checkBox24.Size = new System.Drawing.Size(82, 18);
            this.checkBox24.TabIndex = 49;
            this.checkBox24.Text = "采购程序";
            this.checkBox24.UseVisualStyleBackColor = true;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label11.Location = new System.Drawing.Point(4, 6);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(42, 16);
            this.label11.TabIndex = 48;
            this.label11.Text = "采购";
            // 
            // pnlProductivityLeveraged
            // 
            this.pnlProductivityLeveraged.Controls.Add(this.checkBox34);
            this.pnlProductivityLeveraged.Controls.Add(this.checkBox35);
            this.pnlProductivityLeveraged.Controls.Add(this.label12);
            this.pnlProductivityLeveraged.Location = new System.Drawing.Point(19, 465);
            this.pnlProductivityLeveraged.Name = "pnlProductivityLeveraged";
            this.pnlProductivityLeveraged.Size = new System.Drawing.Size(355, 50);
            this.pnlProductivityLeveraged.TabIndex = 58;
            // 
            // checkBox34
            // 
            this.checkBox34.AutoSize = true;
            this.checkBox34.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox34.Location = new System.Drawing.Point(154, 28);
            this.checkBox34.Name = "checkBox34";
            this.checkBox34.Size = new System.Drawing.Size(82, 18);
            this.checkBox34.TabIndex = 46;
            this.checkBox34.Text = "成本目标";
            this.checkBox34.UseVisualStyleBackColor = true;
            // 
            // checkBox35
            // 
            this.checkBox35.AutoSize = true;
            this.checkBox35.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox35.Location = new System.Drawing.Point(14, 28);
            this.checkBox35.Name = "checkBox35";
            this.checkBox35.Size = new System.Drawing.Size(138, 18);
            this.checkBox35.TabIndex = 45;
            this.checkBox35.Text = "内部成本降低程序";
            this.checkBox35.UseVisualStyleBackColor = true;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label12.Location = new System.Drawing.Point(3, 4);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(76, 16);
            this.label12.TabIndex = 44;
            this.label12.Text = "生产效率";
            // 
            // pnlFinanceLeveraged
            // 
            this.pnlFinanceLeveraged.Controls.Add(this.checkBox38);
            this.pnlFinanceLeveraged.Controls.Add(this.checkBox41);
            this.pnlFinanceLeveraged.Controls.Add(this.label13);
            this.pnlFinanceLeveraged.Location = new System.Drawing.Point(19, 409);
            this.pnlFinanceLeveraged.Name = "pnlFinanceLeveraged";
            this.pnlFinanceLeveraged.Size = new System.Drawing.Size(355, 51);
            this.pnlFinanceLeveraged.TabIndex = 57;
            // 
            // checkBox38
            // 
            this.checkBox38.AutoSize = true;
            this.checkBox38.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox38.Location = new System.Drawing.Point(146, 27);
            this.checkBox38.Name = "checkBox38";
            this.checkBox38.Size = new System.Drawing.Size(82, 18);
            this.checkBox38.TabIndex = 37;
            this.checkBox38.Text = "财款期限";
            this.checkBox38.UseVisualStyleBackColor = true;
            // 
            // checkBox41
            // 
            this.checkBox41.AutoSize = true;
            this.checkBox41.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox41.Location = new System.Drawing.Point(11, 28);
            this.checkBox41.Name = "checkBox41";
            this.checkBox41.Size = new System.Drawing.Size(82, 18);
            this.checkBox41.TabIndex = 36;
            this.checkBox41.Text = "财务评价";
            this.checkBox41.UseVisualStyleBackColor = true;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label13.Location = new System.Drawing.Point(4, 5);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(42, 16);
            this.label13.TabIndex = 35;
            this.label13.Text = "财务";
            // 
            // pnlTechnicalAbilityLeveraged
            // 
            this.pnlTechnicalAbilityLeveraged.Controls.Add(this.checkBox42);
            this.pnlTechnicalAbilityLeveraged.Controls.Add(this.checkBox43);
            this.pnlTechnicalAbilityLeveraged.Controls.Add(this.checkBox44);
            this.pnlTechnicalAbilityLeveraged.Controls.Add(this.checkBox45);
            this.pnlTechnicalAbilityLeveraged.Controls.Add(this.checkBox46);
            this.pnlTechnicalAbilityLeveraged.Controls.Add(this.checkBox47);
            this.pnlTechnicalAbilityLeveraged.Controls.Add(this.checkBox48);
            this.pnlTechnicalAbilityLeveraged.Controls.Add(this.label14);
            this.pnlTechnicalAbilityLeveraged.Location = new System.Drawing.Point(19, 326);
            this.pnlTechnicalAbilityLeveraged.Name = "pnlTechnicalAbilityLeveraged";
            this.pnlTechnicalAbilityLeveraged.Size = new System.Drawing.Size(572, 80);
            this.pnlTechnicalAbilityLeveraged.TabIndex = 56;
            // 
            // checkBox42
            // 
            this.checkBox42.AutoSize = true;
            this.checkBox42.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox42.Location = new System.Drawing.Point(245, 55);
            this.checkBox42.Name = "checkBox42";
            this.checkBox42.Size = new System.Drawing.Size(82, 18);
            this.checkBox42.TabIndex = 48;
            this.checkBox42.Text = "设计更改";
            this.checkBox42.UseVisualStyleBackColor = true;
            // 
            // checkBox43
            // 
            this.checkBox43.AutoSize = true;
            this.checkBox43.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox43.Location = new System.Drawing.Point(147, 55);
            this.checkBox43.Name = "checkBox43";
            this.checkBox43.Size = new System.Drawing.Size(54, 18);
            this.checkBox43.TabIndex = 47;
            this.checkBox43.Text = "设计";
            this.checkBox43.UseVisualStyleBackColor = true;
            // 
            // checkBox44
            // 
            this.checkBox44.AutoSize = true;
            this.checkBox44.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox44.Location = new System.Drawing.Point(8, 55);
            this.checkBox44.Name = "checkBox44";
            this.checkBox44.Size = new System.Drawing.Size(54, 18);
            this.checkBox44.TabIndex = 46;
            this.checkBox44.Text = "研发";
            this.checkBox44.UseVisualStyleBackColor = true;
            // 
            // checkBox45
            // 
            this.checkBox45.AutoSize = true;
            this.checkBox45.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox45.Location = new System.Drawing.Point(438, 31);
            this.checkBox45.Name = "checkBox45";
            this.checkBox45.Size = new System.Drawing.Size(82, 18);
            this.checkBox45.TabIndex = 45;
            this.checkBox45.Text = "试制样件";
            this.checkBox45.UseVisualStyleBackColor = true;
            // 
            // checkBox46
            // 
            this.checkBox46.AutoSize = true;
            this.checkBox46.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox46.Location = new System.Drawing.Point(245, 31);
            this.checkBox46.Name = "checkBox46";
            this.checkBox46.Size = new System.Drawing.Size(110, 18);
            this.checkBox46.TabIndex = 44;
            this.checkBox46.Text = "产品工程技术";
            this.checkBox46.UseVisualStyleBackColor = true;
            // 
            // checkBox47
            // 
            this.checkBox47.AutoSize = true;
            this.checkBox47.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox47.Location = new System.Drawing.Point(147, 31);
            this.checkBox47.Name = "checkBox47";
            this.checkBox47.Size = new System.Drawing.Size(82, 18);
            this.checkBox47.TabIndex = 43;
            this.checkBox47.Text = "工程经验";
            this.checkBox47.UseVisualStyleBackColor = true;
            // 
            // checkBox48
            // 
            this.checkBox48.AutoSize = true;
            this.checkBox48.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox48.Location = new System.Drawing.Point(8, 31);
            this.checkBox48.Name = "checkBox48";
            this.checkBox48.Size = new System.Drawing.Size(124, 18);
            this.checkBox48.TabIndex = 42;
            this.checkBox48.Text = "产品和工程技术";
            this.checkBox48.UseVisualStyleBackColor = true;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label14.Location = new System.Drawing.Point(5, 7);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(76, 16);
            this.label14.TabIndex = 41;
            this.label14.Text = "技术能力";
            // 
            // pnlAftersalesLeveraged
            // 
            this.pnlAftersalesLeveraged.Controls.Add(this.checkBox49);
            this.pnlAftersalesLeveraged.Controls.Add(this.checkBox50);
            this.pnlAftersalesLeveraged.Controls.Add(this.checkBox51);
            this.pnlAftersalesLeveraged.Controls.Add(this.checkBox52);
            this.pnlAftersalesLeveraged.Controls.Add(this.label15);
            this.pnlAftersalesLeveraged.Location = new System.Drawing.Point(19, 269);
            this.pnlAftersalesLeveraged.Name = "pnlAftersalesLeveraged";
            this.pnlAftersalesLeveraged.Size = new System.Drawing.Size(572, 54);
            this.pnlAftersalesLeveraged.TabIndex = 55;
            // 
            // checkBox49
            // 
            this.checkBox49.AutoSize = true;
            this.checkBox49.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox49.Location = new System.Drawing.Point(441, 29);
            this.checkBox49.Name = "checkBox49";
            this.checkBox49.Size = new System.Drawing.Size(54, 18);
            this.checkBox49.TabIndex = 32;
            this.checkBox49.Text = "赔偿";
            this.checkBox49.UseVisualStyleBackColor = true;
            // 
            // checkBox50
            // 
            this.checkBox50.AutoSize = true;
            this.checkBox50.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox50.Location = new System.Drawing.Point(248, 28);
            this.checkBox50.Name = "checkBox50";
            this.checkBox50.Size = new System.Drawing.Size(96, 18);
            this.checkBox50.TabIndex = 31;
            this.checkBox50.Text = "合作和支持";
            this.checkBox50.UseVisualStyleBackColor = true;
            // 
            // checkBox51
            // 
            this.checkBox51.AutoSize = true;
            this.checkBox51.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox51.Location = new System.Drawing.Point(150, 28);
            this.checkBox51.Name = "checkBox51";
            this.checkBox51.Size = new System.Drawing.Size(82, 18);
            this.checkBox51.TabIndex = 30;
            this.checkBox51.Text = "服务读物";
            this.checkBox51.UseVisualStyleBackColor = true;
            // 
            // checkBox52
            // 
            this.checkBox52.AutoSize = true;
            this.checkBox52.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox52.Location = new System.Drawing.Point(11, 29);
            this.checkBox52.Name = "checkBox52";
            this.checkBox52.Size = new System.Drawing.Size(138, 18);
            this.checkBox52.TabIndex = 29;
            this.checkBox52.Text = "技术文件（售后）";
            this.checkBox52.UseVisualStyleBackColor = true;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label15.Location = new System.Drawing.Point(4, 6);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(42, 16);
            this.label15.TabIndex = 28;
            this.label15.Text = "售后";
            // 
            // pnlLogisticsLeveraged
            // 
            this.pnlLogisticsLeveraged.Controls.Add(this.checkBox53);
            this.pnlLogisticsLeveraged.Controls.Add(this.checkBox54);
            this.pnlLogisticsLeveraged.Controls.Add(this.checkBox55);
            this.pnlLogisticsLeveraged.Controls.Add(this.label16);
            this.pnlLogisticsLeveraged.Location = new System.Drawing.Point(19, 213);
            this.pnlLogisticsLeveraged.Name = "pnlLogisticsLeveraged";
            this.pnlLogisticsLeveraged.Size = new System.Drawing.Size(572, 52);
            this.pnlLogisticsLeveraged.TabIndex = 54;
            // 
            // checkBox53
            // 
            this.checkBox53.AutoSize = true;
            this.checkBox53.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox53.Location = new System.Drawing.Point(314, 29);
            this.checkBox53.Name = "checkBox53";
            this.checkBox53.Size = new System.Drawing.Size(166, 18);
            this.checkBox53.TabIndex = 26;
            this.checkBox53.Text = "环境审核--产品和服务";
            this.checkBox53.UseVisualStyleBackColor = true;
            // 
            // checkBox54
            // 
            this.checkBox54.AutoSize = true;
            this.checkBox54.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox54.Location = new System.Drawing.Point(129, 29);
            this.checkBox54.Name = "checkBox54";
            this.checkBox54.Size = new System.Drawing.Size(159, 18);
            this.checkBox54.TabIndex = 25;
            this.checkBox54.Text = "交付准确性/服务水平";
            this.checkBox54.UseVisualStyleBackColor = true;
            // 
            // checkBox55
            // 
            this.checkBox55.AutoSize = true;
            this.checkBox55.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox55.Location = new System.Drawing.Point(13, 29);
            this.checkBox55.Name = "checkBox55";
            this.checkBox55.Size = new System.Drawing.Size(82, 18);
            this.checkBox55.TabIndex = 24;
            this.checkBox55.Text = "物流体系";
            this.checkBox55.UseVisualStyleBackColor = true;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label16.Location = new System.Drawing.Point(5, 5);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(42, 16);
            this.label16.TabIndex = 23;
            this.label16.Text = "物流";
            // 
            // pnlQualityLeveraged
            // 
            this.pnlQualityLeveraged.Controls.Add(this.checkBox56);
            this.pnlQualityLeveraged.Controls.Add(this.checkBox57);
            this.pnlQualityLeveraged.Controls.Add(this.checkBox58);
            this.pnlQualityLeveraged.Controls.Add(this.checkBox59);
            this.pnlQualityLeveraged.Controls.Add(this.checkBox60);
            this.pnlQualityLeveraged.Controls.Add(this.label17);
            this.pnlQualityLeveraged.Location = new System.Drawing.Point(19, 155);
            this.pnlQualityLeveraged.Name = "pnlQualityLeveraged";
            this.pnlQualityLeveraged.Size = new System.Drawing.Size(634, 56);
            this.pnlQualityLeveraged.TabIndex = 53;
            // 
            // checkBox56
            // 
            this.checkBox56.AutoSize = true;
            this.checkBox56.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox56.Location = new System.Drawing.Point(537, 31);
            this.checkBox56.Name = "checkBox56";
            this.checkBox56.Size = new System.Drawing.Size(82, 18);
            this.checkBox56.TabIndex = 24;
            this.checkBox56.Text = "问题处理";
            this.checkBox56.UseVisualStyleBackColor = true;
            // 
            // checkBox57
            // 
            this.checkBox57.AutoSize = true;
            this.checkBox57.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox57.Location = new System.Drawing.Point(453, 31);
            this.checkBox57.Name = "checkBox57";
            this.checkBox57.Size = new System.Drawing.Size(68, 18);
            this.checkBox57.TabIndex = 23;
            this.checkBox57.Text = "可靠性";
            this.checkBox57.UseVisualStyleBackColor = true;
            // 
            // checkBox58
            // 
            this.checkBox58.AutoSize = true;
            this.checkBox58.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox58.Location = new System.Drawing.Point(325, 30);
            this.checkBox58.Name = "checkBox58";
            this.checkBox58.Size = new System.Drawing.Size(110, 18);
            this.checkBox58.TabIndex = 22;
            this.checkBox58.Text = "交付质量业绩";
            this.checkBox58.UseVisualStyleBackColor = true;
            // 
            // checkBox59
            // 
            this.checkBox59.AutoSize = true;
            this.checkBox59.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox59.Location = new System.Drawing.Point(139, 30);
            this.checkBox59.Name = "checkBox59";
            this.checkBox59.Size = new System.Drawing.Size(180, 18);
            this.checkBox59.TabIndex = 21;
            this.checkBox59.Text = "质量策划--部品质量保证";
            this.checkBox59.UseVisualStyleBackColor = true;
            // 
            // checkBox60
            // 
            this.checkBox60.AutoSize = true;
            this.checkBox60.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox60.Location = new System.Drawing.Point(23, 31);
            this.checkBox60.Name = "checkBox60";
            this.checkBox60.Size = new System.Drawing.Size(110, 18);
            this.checkBox60.TabIndex = 20;
            this.checkBox60.Text = "质量管理体系";
            this.checkBox60.UseVisualStyleBackColor = true;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label17.Location = new System.Drawing.Point(5, 6);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(42, 16);
            this.label17.TabIndex = 19;
            this.label17.Text = "质量";
            // 
            // pnlEnvironmentLeveraged
            // 
            this.pnlEnvironmentLeveraged.Controls.Add(this.checkBox61);
            this.pnlEnvironmentLeveraged.Controls.Add(this.checkBox62);
            this.pnlEnvironmentLeveraged.Controls.Add(this.checkBox63);
            this.pnlEnvironmentLeveraged.Controls.Add(this.label18);
            this.pnlEnvironmentLeveraged.Location = new System.Drawing.Point(19, 106);
            this.pnlEnvironmentLeveraged.Name = "pnlEnvironmentLeveraged";
            this.pnlEnvironmentLeveraged.Size = new System.Drawing.Size(520, 47);
            this.pnlEnvironmentLeveraged.TabIndex = 52;
            // 
            // checkBox61
            // 
            this.checkBox61.AutoSize = true;
            this.checkBox61.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox61.Location = new System.Drawing.Point(294, 23);
            this.checkBox61.Name = "checkBox61";
            this.checkBox61.Size = new System.Drawing.Size(166, 18);
            this.checkBox61.TabIndex = 15;
            this.checkBox61.Text = "环境审核--产品和服务";
            this.checkBox61.UseVisualStyleBackColor = true;
            // 
            // checkBox62
            // 
            this.checkBox62.AutoSize = true;
            this.checkBox62.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox62.Location = new System.Drawing.Point(125, 23);
            this.checkBox62.Name = "checkBox62";
            this.checkBox62.Size = new System.Drawing.Size(152, 18);
            this.checkBox62.TabIndex = 14;
            this.checkBox62.Text = "环境审核--公司水平";
            this.checkBox62.UseVisualStyleBackColor = true;
            // 
            // checkBox63
            // 
            this.checkBox63.AutoSize = true;
            this.checkBox63.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox63.Location = new System.Drawing.Point(7, 23);
            this.checkBox63.Name = "checkBox63";
            this.checkBox63.Size = new System.Drawing.Size(110, 18);
            this.checkBox63.TabIndex = 13;
            this.checkBox63.Text = "环境管理体系";
            this.checkBox63.UseVisualStyleBackColor = true;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label18.Location = new System.Drawing.Point(3, 4);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(42, 16);
            this.label18.TabIndex = 9;
            this.label18.Text = "环境";
            // 
            // pnlManageLeveraged
            // 
            this.pnlManageLeveraged.Controls.Add(this.checkBox64);
            this.pnlManageLeveraged.Controls.Add(this.checkBox65);
            this.pnlManageLeveraged.Controls.Add(this.checkBox66);
            this.pnlManageLeveraged.Controls.Add(this.checkBox67);
            this.pnlManageLeveraged.Controls.Add(this.label19);
            this.pnlManageLeveraged.Location = new System.Drawing.Point(19, 58);
            this.pnlManageLeveraged.Name = "pnlManageLeveraged";
            this.pnlManageLeveraged.Size = new System.Drawing.Size(400, 47);
            this.pnlManageLeveraged.TabIndex = 51;
            // 
            // checkBox64
            // 
            this.checkBox64.AutoSize = true;
            this.checkBox64.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox64.Location = new System.Drawing.Point(294, 24);
            this.checkBox64.Name = "checkBox64";
            this.checkBox64.Size = new System.Drawing.Size(82, 18);
            this.checkBox64.TabIndex = 13;
            this.checkBox64.Text = "风险管理";
            this.checkBox64.UseVisualStyleBackColor = true;
            // 
            // checkBox65
            // 
            this.checkBox65.AutoSize = true;
            this.checkBox65.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox65.Location = new System.Drawing.Point(185, 24);
            this.checkBox65.Name = "checkBox65";
            this.checkBox65.Size = new System.Drawing.Size(103, 18);
            this.checkBox65.TabIndex = 12;
            this.checkBox65.Text = "TQM工作程序";
            this.checkBox65.UseVisualStyleBackColor = true;
            // 
            // checkBox66
            // 
            this.checkBox66.AutoSize = true;
            this.checkBox66.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox66.Location = new System.Drawing.Point(97, 24);
            this.checkBox66.Name = "checkBox66";
            this.checkBox66.Size = new System.Drawing.Size(82, 18);
            this.checkBox66.TabIndex = 11;
            this.checkBox66.Text = "客户满意";
            this.checkBox66.UseVisualStyleBackColor = true;
            // 
            // checkBox67
            // 
            this.checkBox67.AutoSize = true;
            this.checkBox67.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox67.Location = new System.Drawing.Point(8, 24);
            this.checkBox67.Name = "checkBox67";
            this.checkBox67.Size = new System.Drawing.Size(54, 18);
            this.checkBox67.TabIndex = 10;
            this.checkBox67.Text = "管理";
            this.checkBox67.UseVisualStyleBackColor = true;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label19.Location = new System.Drawing.Point(4, 3);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(42, 16);
            this.label19.TabIndex = 9;
            this.label19.Text = "管理";
            // 
            // pnlCompanyProfileLeveraged
            // 
            this.pnlCompanyProfileLeveraged.Controls.Add(this.checkBox68);
            this.pnlCompanyProfileLeveraged.Controls.Add(this.checkBox69);
            this.pnlCompanyProfileLeveraged.Controls.Add(this.checkBox70);
            this.pnlCompanyProfileLeveraged.Controls.Add(this.label20);
            this.pnlCompanyProfileLeveraged.Location = new System.Drawing.Point(19, 4);
            this.pnlCompanyProfileLeveraged.Name = "pnlCompanyProfileLeveraged";
            this.pnlCompanyProfileLeveraged.Size = new System.Drawing.Size(309, 52);
            this.pnlCompanyProfileLeveraged.TabIndex = 50;
            // 
            // checkBox68
            // 
            this.checkBox68.AutoSize = true;
            this.checkBox68.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox68.Location = new System.Drawing.Point(185, 30);
            this.checkBox68.Name = "checkBox68";
            this.checkBox68.Size = new System.Drawing.Size(68, 18);
            this.checkBox68.TabIndex = 7;
            this.checkBox68.Text = "依附性";
            this.checkBox68.UseVisualStyleBackColor = true;
            // 
            // checkBox69
            // 
            this.checkBox69.AutoSize = true;
            this.checkBox69.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox69.Location = new System.Drawing.Point(97, 30);
            this.checkBox69.Name = "checkBox69";
            this.checkBox69.Size = new System.Drawing.Size(82, 18);
            this.checkBox69.TabIndex = 6;
            this.checkBox69.Text = "全球能力";
            this.checkBox69.UseVisualStyleBackColor = true;
            // 
            // checkBox70
            // 
            this.checkBox70.AutoSize = true;
            this.checkBox70.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox70.Location = new System.Drawing.Point(8, 30);
            this.checkBox70.Name = "checkBox70";
            this.checkBox70.Size = new System.Drawing.Size(82, 18);
            this.checkBox70.TabIndex = 5;
            this.checkBox70.Text = "股权关系";
            this.checkBox70.UseVisualStyleBackColor = true;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label20.Location = new System.Drawing.Point(4, 6);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(76, 16);
            this.label20.TabIndex = 4;
            this.label20.Text = "公司概况";
            // 
            // btnPreviewLeveraged
            // 
            this.btnPreviewLeveraged.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnPreviewLeveraged.Location = new System.Drawing.Point(482, 573);
            this.btnPreviewLeveraged.Name = "btnPreviewLeveraged";
            this.btnPreviewLeveraged.Size = new System.Drawing.Size(75, 32);
            this.btnPreviewLeveraged.TabIndex = 49;
            this.btnPreviewLeveraged.Text = "预览";
            this.btnPreviewLeveraged.UseVisualStyleBackColor = true;
            // 
            // btnSaveLeveraged
            // 
            this.btnSaveLeveraged.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnSaveLeveraged.Location = new System.Drawing.Point(288, 573);
            this.btnSaveLeveraged.Name = "btnSaveLeveraged";
            this.btnSaveLeveraged.Size = new System.Drawing.Size(75, 32);
            this.btnSaveLeveraged.TabIndex = 48;
            this.btnSaveLeveraged.Text = "保存";
            this.btnSaveLeveraged.UseVisualStyleBackColor = true;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.pnlBottleneck);
            this.tabPage3.Location = new System.Drawing.Point(4, 25);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(923, 627);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "瓶颈型";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // pnlBottleneck
            // 
            this.pnlBottleneck.Controls.Add(this.pnlPurchaseBottleneck);
            this.pnlBottleneck.Controls.Add(this.pnlProductivityBottleneck);
            this.pnlBottleneck.Controls.Add(this.pnlFinanceBottleneck);
            this.pnlBottleneck.Controls.Add(this.pnlTechnicalAbilityBottleneck);
            this.pnlBottleneck.Controls.Add(this.pnlAftersalesBottleneck);
            this.pnlBottleneck.Controls.Add(this.pnlLogisticsBottleneck);
            this.pnlBottleneck.Controls.Add(this.pnlQualityBottleneck);
            this.pnlBottleneck.Controls.Add(this.pnlEnvironmentBottleneck);
            this.pnlBottleneck.Controls.Add(this.pnlManageBottleneck);
            this.pnlBottleneck.Controls.Add(this.pnlCompanyProfileBottleneck);
            this.pnlBottleneck.Controls.Add(this.btnPreviewBottleneck);
            this.pnlBottleneck.Controls.Add(this.btnSaveBottleneck);
            this.pnlBottleneck.Location = new System.Drawing.Point(5, 8);
            this.pnlBottleneck.Name = "pnlBottleneck";
            this.pnlBottleneck.Size = new System.Drawing.Size(913, 625);
            this.pnlBottleneck.TabIndex = 3;
            // 
            // pnlPurchaseBottleneck
            // 
            this.pnlPurchaseBottleneck.Controls.Add(this.checkBox71);
            this.pnlPurchaseBottleneck.Controls.Add(this.checkBox72);
            this.pnlPurchaseBottleneck.Controls.Add(this.label21);
            this.pnlPurchaseBottleneck.Location = new System.Drawing.Point(19, 517);
            this.pnlPurchaseBottleneck.Name = "pnlPurchaseBottleneck";
            this.pnlPurchaseBottleneck.Size = new System.Drawing.Size(355, 50);
            this.pnlPurchaseBottleneck.TabIndex = 59;
            // 
            // checkBox71
            // 
            this.checkBox71.AutoSize = true;
            this.checkBox71.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox71.Location = new System.Drawing.Point(151, 28);
            this.checkBox71.Name = "checkBox71";
            this.checkBox71.Size = new System.Drawing.Size(110, 18);
            this.checkBox71.TabIndex = 50;
            this.checkBox71.Text = "分包方的业绩";
            this.checkBox71.UseVisualStyleBackColor = true;
            // 
            // checkBox72
            // 
            this.checkBox72.AutoSize = true;
            this.checkBox72.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox72.Location = new System.Drawing.Point(16, 29);
            this.checkBox72.Name = "checkBox72";
            this.checkBox72.Size = new System.Drawing.Size(82, 18);
            this.checkBox72.TabIndex = 49;
            this.checkBox72.Text = "采购程序";
            this.checkBox72.UseVisualStyleBackColor = true;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label21.Location = new System.Drawing.Point(4, 6);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(42, 16);
            this.label21.TabIndex = 48;
            this.label21.Text = "采购";
            // 
            // pnlProductivityBottleneck
            // 
            this.pnlProductivityBottleneck.Controls.Add(this.checkBox73);
            this.pnlProductivityBottleneck.Controls.Add(this.checkBox74);
            this.pnlProductivityBottleneck.Controls.Add(this.label22);
            this.pnlProductivityBottleneck.Location = new System.Drawing.Point(19, 465);
            this.pnlProductivityBottleneck.Name = "pnlProductivityBottleneck";
            this.pnlProductivityBottleneck.Size = new System.Drawing.Size(355, 50);
            this.pnlProductivityBottleneck.TabIndex = 58;
            // 
            // checkBox73
            // 
            this.checkBox73.AutoSize = true;
            this.checkBox73.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox73.Location = new System.Drawing.Point(154, 28);
            this.checkBox73.Name = "checkBox73";
            this.checkBox73.Size = new System.Drawing.Size(82, 18);
            this.checkBox73.TabIndex = 46;
            this.checkBox73.Text = "成本目标";
            this.checkBox73.UseVisualStyleBackColor = true;
            // 
            // checkBox74
            // 
            this.checkBox74.AutoSize = true;
            this.checkBox74.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox74.Location = new System.Drawing.Point(14, 28);
            this.checkBox74.Name = "checkBox74";
            this.checkBox74.Size = new System.Drawing.Size(138, 18);
            this.checkBox74.TabIndex = 45;
            this.checkBox74.Text = "内部成本降低程序";
            this.checkBox74.UseVisualStyleBackColor = true;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label22.Location = new System.Drawing.Point(3, 4);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(76, 16);
            this.label22.TabIndex = 44;
            this.label22.Text = "生产效率";
            // 
            // pnlFinanceBottleneck
            // 
            this.pnlFinanceBottleneck.Controls.Add(this.checkBox75);
            this.pnlFinanceBottleneck.Controls.Add(this.checkBox76);
            this.pnlFinanceBottleneck.Controls.Add(this.label23);
            this.pnlFinanceBottleneck.Location = new System.Drawing.Point(19, 409);
            this.pnlFinanceBottleneck.Name = "pnlFinanceBottleneck";
            this.pnlFinanceBottleneck.Size = new System.Drawing.Size(355, 51);
            this.pnlFinanceBottleneck.TabIndex = 57;
            // 
            // checkBox75
            // 
            this.checkBox75.AutoSize = true;
            this.checkBox75.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox75.Location = new System.Drawing.Point(146, 27);
            this.checkBox75.Name = "checkBox75";
            this.checkBox75.Size = new System.Drawing.Size(82, 18);
            this.checkBox75.TabIndex = 37;
            this.checkBox75.Text = "财款期限";
            this.checkBox75.UseVisualStyleBackColor = true;
            // 
            // checkBox76
            // 
            this.checkBox76.AutoSize = true;
            this.checkBox76.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox76.Location = new System.Drawing.Point(11, 28);
            this.checkBox76.Name = "checkBox76";
            this.checkBox76.Size = new System.Drawing.Size(82, 18);
            this.checkBox76.TabIndex = 36;
            this.checkBox76.Text = "财务评价";
            this.checkBox76.UseVisualStyleBackColor = true;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label23.Location = new System.Drawing.Point(4, 5);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(42, 16);
            this.label23.TabIndex = 35;
            this.label23.Text = "财务";
            // 
            // pnlTechnicalAbilityBottleneck
            // 
            this.pnlTechnicalAbilityBottleneck.Controls.Add(this.checkBox77);
            this.pnlTechnicalAbilityBottleneck.Controls.Add(this.checkBox78);
            this.pnlTechnicalAbilityBottleneck.Controls.Add(this.checkBox79);
            this.pnlTechnicalAbilityBottleneck.Controls.Add(this.checkBox80);
            this.pnlTechnicalAbilityBottleneck.Controls.Add(this.checkBox81);
            this.pnlTechnicalAbilityBottleneck.Controls.Add(this.checkBox82);
            this.pnlTechnicalAbilityBottleneck.Controls.Add(this.checkBox83);
            this.pnlTechnicalAbilityBottleneck.Controls.Add(this.label24);
            this.pnlTechnicalAbilityBottleneck.Location = new System.Drawing.Point(19, 326);
            this.pnlTechnicalAbilityBottleneck.Name = "pnlTechnicalAbilityBottleneck";
            this.pnlTechnicalAbilityBottleneck.Size = new System.Drawing.Size(572, 80);
            this.pnlTechnicalAbilityBottleneck.TabIndex = 56;
            // 
            // checkBox77
            // 
            this.checkBox77.AutoSize = true;
            this.checkBox77.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox77.Location = new System.Drawing.Point(245, 55);
            this.checkBox77.Name = "checkBox77";
            this.checkBox77.Size = new System.Drawing.Size(82, 18);
            this.checkBox77.TabIndex = 48;
            this.checkBox77.Text = "设计更改";
            this.checkBox77.UseVisualStyleBackColor = true;
            // 
            // checkBox78
            // 
            this.checkBox78.AutoSize = true;
            this.checkBox78.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox78.Location = new System.Drawing.Point(147, 55);
            this.checkBox78.Name = "checkBox78";
            this.checkBox78.Size = new System.Drawing.Size(54, 18);
            this.checkBox78.TabIndex = 47;
            this.checkBox78.Text = "设计";
            this.checkBox78.UseVisualStyleBackColor = true;
            // 
            // checkBox79
            // 
            this.checkBox79.AutoSize = true;
            this.checkBox79.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox79.Location = new System.Drawing.Point(8, 55);
            this.checkBox79.Name = "checkBox79";
            this.checkBox79.Size = new System.Drawing.Size(54, 18);
            this.checkBox79.TabIndex = 46;
            this.checkBox79.Text = "研发";
            this.checkBox79.UseVisualStyleBackColor = true;
            // 
            // checkBox80
            // 
            this.checkBox80.AutoSize = true;
            this.checkBox80.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox80.Location = new System.Drawing.Point(438, 31);
            this.checkBox80.Name = "checkBox80";
            this.checkBox80.Size = new System.Drawing.Size(82, 18);
            this.checkBox80.TabIndex = 45;
            this.checkBox80.Text = "试制样件";
            this.checkBox80.UseVisualStyleBackColor = true;
            // 
            // checkBox81
            // 
            this.checkBox81.AutoSize = true;
            this.checkBox81.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox81.Location = new System.Drawing.Point(245, 31);
            this.checkBox81.Name = "checkBox81";
            this.checkBox81.Size = new System.Drawing.Size(110, 18);
            this.checkBox81.TabIndex = 44;
            this.checkBox81.Text = "产品工程技术";
            this.checkBox81.UseVisualStyleBackColor = true;
            // 
            // checkBox82
            // 
            this.checkBox82.AutoSize = true;
            this.checkBox82.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox82.Location = new System.Drawing.Point(147, 31);
            this.checkBox82.Name = "checkBox82";
            this.checkBox82.Size = new System.Drawing.Size(82, 18);
            this.checkBox82.TabIndex = 43;
            this.checkBox82.Text = "工程经验";
            this.checkBox82.UseVisualStyleBackColor = true;
            // 
            // checkBox83
            // 
            this.checkBox83.AutoSize = true;
            this.checkBox83.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox83.Location = new System.Drawing.Point(8, 31);
            this.checkBox83.Name = "checkBox83";
            this.checkBox83.Size = new System.Drawing.Size(124, 18);
            this.checkBox83.TabIndex = 42;
            this.checkBox83.Text = "产品和工程技术";
            this.checkBox83.UseVisualStyleBackColor = true;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label24.Location = new System.Drawing.Point(5, 7);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(76, 16);
            this.label24.TabIndex = 41;
            this.label24.Text = "技术能力";
            // 
            // pnlAftersalesBottleneck
            // 
            this.pnlAftersalesBottleneck.Controls.Add(this.checkBox84);
            this.pnlAftersalesBottleneck.Controls.Add(this.checkBox85);
            this.pnlAftersalesBottleneck.Controls.Add(this.checkBox86);
            this.pnlAftersalesBottleneck.Controls.Add(this.checkBox87);
            this.pnlAftersalesBottleneck.Controls.Add(this.label25);
            this.pnlAftersalesBottleneck.Location = new System.Drawing.Point(19, 269);
            this.pnlAftersalesBottleneck.Name = "pnlAftersalesBottleneck";
            this.pnlAftersalesBottleneck.Size = new System.Drawing.Size(572, 54);
            this.pnlAftersalesBottleneck.TabIndex = 55;
            // 
            // checkBox84
            // 
            this.checkBox84.AutoSize = true;
            this.checkBox84.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox84.Location = new System.Drawing.Point(441, 29);
            this.checkBox84.Name = "checkBox84";
            this.checkBox84.Size = new System.Drawing.Size(54, 18);
            this.checkBox84.TabIndex = 32;
            this.checkBox84.Text = "赔偿";
            this.checkBox84.UseVisualStyleBackColor = true;
            // 
            // checkBox85
            // 
            this.checkBox85.AutoSize = true;
            this.checkBox85.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox85.Location = new System.Drawing.Point(248, 28);
            this.checkBox85.Name = "checkBox85";
            this.checkBox85.Size = new System.Drawing.Size(96, 18);
            this.checkBox85.TabIndex = 31;
            this.checkBox85.Text = "合作和支持";
            this.checkBox85.UseVisualStyleBackColor = true;
            // 
            // checkBox86
            // 
            this.checkBox86.AutoSize = true;
            this.checkBox86.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox86.Location = new System.Drawing.Point(150, 28);
            this.checkBox86.Name = "checkBox86";
            this.checkBox86.Size = new System.Drawing.Size(82, 18);
            this.checkBox86.TabIndex = 30;
            this.checkBox86.Text = "服务读物";
            this.checkBox86.UseVisualStyleBackColor = true;
            // 
            // checkBox87
            // 
            this.checkBox87.AutoSize = true;
            this.checkBox87.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox87.Location = new System.Drawing.Point(11, 29);
            this.checkBox87.Name = "checkBox87";
            this.checkBox87.Size = new System.Drawing.Size(138, 18);
            this.checkBox87.TabIndex = 29;
            this.checkBox87.Text = "技术文件（售后）";
            this.checkBox87.UseVisualStyleBackColor = true;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label25.Location = new System.Drawing.Point(4, 6);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(42, 16);
            this.label25.TabIndex = 28;
            this.label25.Text = "售后";
            // 
            // pnlLogisticsBottleneck
            // 
            this.pnlLogisticsBottleneck.Controls.Add(this.checkBox88);
            this.pnlLogisticsBottleneck.Controls.Add(this.checkBox89);
            this.pnlLogisticsBottleneck.Controls.Add(this.checkBox90);
            this.pnlLogisticsBottleneck.Controls.Add(this.label26);
            this.pnlLogisticsBottleneck.Location = new System.Drawing.Point(19, 213);
            this.pnlLogisticsBottleneck.Name = "pnlLogisticsBottleneck";
            this.pnlLogisticsBottleneck.Size = new System.Drawing.Size(572, 52);
            this.pnlLogisticsBottleneck.TabIndex = 54;
            // 
            // checkBox88
            // 
            this.checkBox88.AutoSize = true;
            this.checkBox88.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox88.Location = new System.Drawing.Point(314, 29);
            this.checkBox88.Name = "checkBox88";
            this.checkBox88.Size = new System.Drawing.Size(166, 18);
            this.checkBox88.TabIndex = 26;
            this.checkBox88.Text = "环境审核--产品和服务";
            this.checkBox88.UseVisualStyleBackColor = true;
            // 
            // checkBox89
            // 
            this.checkBox89.AutoSize = true;
            this.checkBox89.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox89.Location = new System.Drawing.Point(129, 29);
            this.checkBox89.Name = "checkBox89";
            this.checkBox89.Size = new System.Drawing.Size(159, 18);
            this.checkBox89.TabIndex = 25;
            this.checkBox89.Text = "交付准确性/服务水平";
            this.checkBox89.UseVisualStyleBackColor = true;
            // 
            // checkBox90
            // 
            this.checkBox90.AutoSize = true;
            this.checkBox90.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox90.Location = new System.Drawing.Point(13, 29);
            this.checkBox90.Name = "checkBox90";
            this.checkBox90.Size = new System.Drawing.Size(82, 18);
            this.checkBox90.TabIndex = 24;
            this.checkBox90.Text = "物流体系";
            this.checkBox90.UseVisualStyleBackColor = true;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label26.Location = new System.Drawing.Point(5, 5);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(42, 16);
            this.label26.TabIndex = 23;
            this.label26.Text = "物流";
            // 
            // pnlQualityBottleneck
            // 
            this.pnlQualityBottleneck.Controls.Add(this.checkBox91);
            this.pnlQualityBottleneck.Controls.Add(this.checkBox92);
            this.pnlQualityBottleneck.Controls.Add(this.checkBox93);
            this.pnlQualityBottleneck.Controls.Add(this.checkBox94);
            this.pnlQualityBottleneck.Controls.Add(this.checkBox95);
            this.pnlQualityBottleneck.Controls.Add(this.label27);
            this.pnlQualityBottleneck.Location = new System.Drawing.Point(19, 155);
            this.pnlQualityBottleneck.Name = "pnlQualityBottleneck";
            this.pnlQualityBottleneck.Size = new System.Drawing.Size(634, 56);
            this.pnlQualityBottleneck.TabIndex = 53;
            // 
            // checkBox91
            // 
            this.checkBox91.AutoSize = true;
            this.checkBox91.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox91.Location = new System.Drawing.Point(537, 31);
            this.checkBox91.Name = "checkBox91";
            this.checkBox91.Size = new System.Drawing.Size(82, 18);
            this.checkBox91.TabIndex = 24;
            this.checkBox91.Text = "问题处理";
            this.checkBox91.UseVisualStyleBackColor = true;
            // 
            // checkBox92
            // 
            this.checkBox92.AutoSize = true;
            this.checkBox92.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox92.Location = new System.Drawing.Point(453, 31);
            this.checkBox92.Name = "checkBox92";
            this.checkBox92.Size = new System.Drawing.Size(68, 18);
            this.checkBox92.TabIndex = 23;
            this.checkBox92.Text = "可靠性";
            this.checkBox92.UseVisualStyleBackColor = true;
            // 
            // checkBox93
            // 
            this.checkBox93.AutoSize = true;
            this.checkBox93.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox93.Location = new System.Drawing.Point(325, 30);
            this.checkBox93.Name = "checkBox93";
            this.checkBox93.Size = new System.Drawing.Size(110, 18);
            this.checkBox93.TabIndex = 22;
            this.checkBox93.Text = "交付质量业绩";
            this.checkBox93.UseVisualStyleBackColor = true;
            // 
            // checkBox94
            // 
            this.checkBox94.AutoSize = true;
            this.checkBox94.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox94.Location = new System.Drawing.Point(139, 30);
            this.checkBox94.Name = "checkBox94";
            this.checkBox94.Size = new System.Drawing.Size(180, 18);
            this.checkBox94.TabIndex = 21;
            this.checkBox94.Text = "质量策划--部品质量保证";
            this.checkBox94.UseVisualStyleBackColor = true;
            // 
            // checkBox95
            // 
            this.checkBox95.AutoSize = true;
            this.checkBox95.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox95.Location = new System.Drawing.Point(23, 31);
            this.checkBox95.Name = "checkBox95";
            this.checkBox95.Size = new System.Drawing.Size(110, 18);
            this.checkBox95.TabIndex = 20;
            this.checkBox95.Text = "质量管理体系";
            this.checkBox95.UseVisualStyleBackColor = true;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label27.Location = new System.Drawing.Point(5, 6);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(42, 16);
            this.label27.TabIndex = 19;
            this.label27.Text = "质量";
            // 
            // pnlEnvironmentBottleneck
            // 
            this.pnlEnvironmentBottleneck.Controls.Add(this.checkBox96);
            this.pnlEnvironmentBottleneck.Controls.Add(this.checkBox97);
            this.pnlEnvironmentBottleneck.Controls.Add(this.checkBox98);
            this.pnlEnvironmentBottleneck.Controls.Add(this.label28);
            this.pnlEnvironmentBottleneck.Location = new System.Drawing.Point(19, 106);
            this.pnlEnvironmentBottleneck.Name = "pnlEnvironmentBottleneck";
            this.pnlEnvironmentBottleneck.Size = new System.Drawing.Size(520, 47);
            this.pnlEnvironmentBottleneck.TabIndex = 52;
            // 
            // checkBox96
            // 
            this.checkBox96.AutoSize = true;
            this.checkBox96.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox96.Location = new System.Drawing.Point(294, 23);
            this.checkBox96.Name = "checkBox96";
            this.checkBox96.Size = new System.Drawing.Size(166, 18);
            this.checkBox96.TabIndex = 15;
            this.checkBox96.Text = "环境审核--产品和服务";
            this.checkBox96.UseVisualStyleBackColor = true;
            // 
            // checkBox97
            // 
            this.checkBox97.AutoSize = true;
            this.checkBox97.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox97.Location = new System.Drawing.Point(125, 23);
            this.checkBox97.Name = "checkBox97";
            this.checkBox97.Size = new System.Drawing.Size(152, 18);
            this.checkBox97.TabIndex = 14;
            this.checkBox97.Text = "环境审核--公司水平";
            this.checkBox97.UseVisualStyleBackColor = true;
            // 
            // checkBox98
            // 
            this.checkBox98.AutoSize = true;
            this.checkBox98.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox98.Location = new System.Drawing.Point(7, 23);
            this.checkBox98.Name = "checkBox98";
            this.checkBox98.Size = new System.Drawing.Size(110, 18);
            this.checkBox98.TabIndex = 13;
            this.checkBox98.Text = "环境管理体系";
            this.checkBox98.UseVisualStyleBackColor = true;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label28.Location = new System.Drawing.Point(3, 4);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(42, 16);
            this.label28.TabIndex = 9;
            this.label28.Text = "环境";
            // 
            // pnlManageBottleneck
            // 
            this.pnlManageBottleneck.Controls.Add(this.checkBox99);
            this.pnlManageBottleneck.Controls.Add(this.checkBox100);
            this.pnlManageBottleneck.Controls.Add(this.checkBox101);
            this.pnlManageBottleneck.Controls.Add(this.checkBox102);
            this.pnlManageBottleneck.Controls.Add(this.label29);
            this.pnlManageBottleneck.Location = new System.Drawing.Point(19, 58);
            this.pnlManageBottleneck.Name = "pnlManageBottleneck";
            this.pnlManageBottleneck.Size = new System.Drawing.Size(400, 47);
            this.pnlManageBottleneck.TabIndex = 51;
            // 
            // checkBox99
            // 
            this.checkBox99.AutoSize = true;
            this.checkBox99.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox99.Location = new System.Drawing.Point(294, 24);
            this.checkBox99.Name = "checkBox99";
            this.checkBox99.Size = new System.Drawing.Size(82, 18);
            this.checkBox99.TabIndex = 13;
            this.checkBox99.Text = "风险管理";
            this.checkBox99.UseVisualStyleBackColor = true;
            // 
            // checkBox100
            // 
            this.checkBox100.AutoSize = true;
            this.checkBox100.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox100.Location = new System.Drawing.Point(185, 24);
            this.checkBox100.Name = "checkBox100";
            this.checkBox100.Size = new System.Drawing.Size(103, 18);
            this.checkBox100.TabIndex = 12;
            this.checkBox100.Text = "TQM工作程序";
            this.checkBox100.UseVisualStyleBackColor = true;
            // 
            // checkBox101
            // 
            this.checkBox101.AutoSize = true;
            this.checkBox101.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox101.Location = new System.Drawing.Point(97, 24);
            this.checkBox101.Name = "checkBox101";
            this.checkBox101.Size = new System.Drawing.Size(82, 18);
            this.checkBox101.TabIndex = 11;
            this.checkBox101.Text = "客户满意";
            this.checkBox101.UseVisualStyleBackColor = true;
            // 
            // checkBox102
            // 
            this.checkBox102.AutoSize = true;
            this.checkBox102.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox102.Location = new System.Drawing.Point(8, 24);
            this.checkBox102.Name = "checkBox102";
            this.checkBox102.Size = new System.Drawing.Size(54, 18);
            this.checkBox102.TabIndex = 10;
            this.checkBox102.Text = "管理";
            this.checkBox102.UseVisualStyleBackColor = true;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label29.Location = new System.Drawing.Point(4, 3);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(42, 16);
            this.label29.TabIndex = 9;
            this.label29.Text = "管理";
            // 
            // pnlCompanyProfileBottleneck
            // 
            this.pnlCompanyProfileBottleneck.Controls.Add(this.checkBox103);
            this.pnlCompanyProfileBottleneck.Controls.Add(this.checkBox104);
            this.pnlCompanyProfileBottleneck.Controls.Add(this.checkBox105);
            this.pnlCompanyProfileBottleneck.Controls.Add(this.label30);
            this.pnlCompanyProfileBottleneck.Location = new System.Drawing.Point(19, 4);
            this.pnlCompanyProfileBottleneck.Name = "pnlCompanyProfileBottleneck";
            this.pnlCompanyProfileBottleneck.Size = new System.Drawing.Size(309, 52);
            this.pnlCompanyProfileBottleneck.TabIndex = 50;
            // 
            // checkBox103
            // 
            this.checkBox103.AutoSize = true;
            this.checkBox103.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox103.Location = new System.Drawing.Point(185, 30);
            this.checkBox103.Name = "checkBox103";
            this.checkBox103.Size = new System.Drawing.Size(68, 18);
            this.checkBox103.TabIndex = 7;
            this.checkBox103.Text = "依附性";
            this.checkBox103.UseVisualStyleBackColor = true;
            // 
            // checkBox104
            // 
            this.checkBox104.AutoSize = true;
            this.checkBox104.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox104.Location = new System.Drawing.Point(97, 30);
            this.checkBox104.Name = "checkBox104";
            this.checkBox104.Size = new System.Drawing.Size(82, 18);
            this.checkBox104.TabIndex = 6;
            this.checkBox104.Text = "全球能力";
            this.checkBox104.UseVisualStyleBackColor = true;
            // 
            // checkBox105
            // 
            this.checkBox105.AutoSize = true;
            this.checkBox105.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox105.Location = new System.Drawing.Point(8, 30);
            this.checkBox105.Name = "checkBox105";
            this.checkBox105.Size = new System.Drawing.Size(82, 18);
            this.checkBox105.TabIndex = 5;
            this.checkBox105.Text = "股权关系";
            this.checkBox105.UseVisualStyleBackColor = true;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label30.Location = new System.Drawing.Point(4, 6);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(76, 16);
            this.label30.TabIndex = 4;
            this.label30.Text = "公司概况";
            // 
            // btnPreviewBottleneck
            // 
            this.btnPreviewBottleneck.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnPreviewBottleneck.Location = new System.Drawing.Point(482, 573);
            this.btnPreviewBottleneck.Name = "btnPreviewBottleneck";
            this.btnPreviewBottleneck.Size = new System.Drawing.Size(75, 32);
            this.btnPreviewBottleneck.TabIndex = 49;
            this.btnPreviewBottleneck.Text = "预览";
            this.btnPreviewBottleneck.UseVisualStyleBackColor = true;
            // 
            // btnSaveBottleneck
            // 
            this.btnSaveBottleneck.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnSaveBottleneck.Location = new System.Drawing.Point(288, 573);
            this.btnSaveBottleneck.Name = "btnSaveBottleneck";
            this.btnSaveBottleneck.Size = new System.Drawing.Size(75, 32);
            this.btnSaveBottleneck.TabIndex = 48;
            this.btnSaveBottleneck.Text = "保存";
            this.btnSaveBottleneck.UseVisualStyleBackColor = true;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.pnlKey);
            this.tabPage4.Location = new System.Drawing.Point(4, 25);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(923, 627);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "关键型";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // pnlKey
            // 
            this.pnlKey.Controls.Add(this.pnlPurchaseKey);
            this.pnlKey.Controls.Add(this.pnlProductivityKey);
            this.pnlKey.Controls.Add(this.pnlFinanceKey);
            this.pnlKey.Controls.Add(this.pnlTechnicalAbilityKey);
            this.pnlKey.Controls.Add(this.pnlAftersalesKey);
            this.pnlKey.Controls.Add(this.pnlLogisticsKey);
            this.pnlKey.Controls.Add(this.pnlQualityKey);
            this.pnlKey.Controls.Add(this.pnlEnvironmentKey);
            this.pnlKey.Controls.Add(this.pnlManageKey);
            this.pnlKey.Controls.Add(this.pnlCompanyProfileKey);
            this.pnlKey.Controls.Add(this.btnPreviewKey);
            this.pnlKey.Controls.Add(this.btnSaveKey);
            this.pnlKey.Location = new System.Drawing.Point(5, 8);
            this.pnlKey.Name = "pnlKey";
            this.pnlKey.Size = new System.Drawing.Size(913, 625);
            this.pnlKey.TabIndex = 3;
            // 
            // pnlPurchaseKey
            // 
            this.pnlPurchaseKey.Controls.Add(this.checkBox106);
            this.pnlPurchaseKey.Controls.Add(this.checkBox107);
            this.pnlPurchaseKey.Controls.Add(this.label31);
            this.pnlPurchaseKey.Location = new System.Drawing.Point(19, 517);
            this.pnlPurchaseKey.Name = "pnlPurchaseKey";
            this.pnlPurchaseKey.Size = new System.Drawing.Size(355, 50);
            this.pnlPurchaseKey.TabIndex = 59;
            // 
            // checkBox106
            // 
            this.checkBox106.AutoSize = true;
            this.checkBox106.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox106.Location = new System.Drawing.Point(151, 28);
            this.checkBox106.Name = "checkBox106";
            this.checkBox106.Size = new System.Drawing.Size(110, 18);
            this.checkBox106.TabIndex = 50;
            this.checkBox106.Text = "分包方的业绩";
            this.checkBox106.UseVisualStyleBackColor = true;
            // 
            // checkBox107
            // 
            this.checkBox107.AutoSize = true;
            this.checkBox107.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox107.Location = new System.Drawing.Point(16, 29);
            this.checkBox107.Name = "checkBox107";
            this.checkBox107.Size = new System.Drawing.Size(82, 18);
            this.checkBox107.TabIndex = 49;
            this.checkBox107.Text = "采购程序";
            this.checkBox107.UseVisualStyleBackColor = true;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label31.Location = new System.Drawing.Point(4, 6);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(42, 16);
            this.label31.TabIndex = 48;
            this.label31.Text = "采购";
            // 
            // pnlProductivityKey
            // 
            this.pnlProductivityKey.Controls.Add(this.checkBox108);
            this.pnlProductivityKey.Controls.Add(this.checkBox109);
            this.pnlProductivityKey.Controls.Add(this.label32);
            this.pnlProductivityKey.Location = new System.Drawing.Point(19, 465);
            this.pnlProductivityKey.Name = "pnlProductivityKey";
            this.pnlProductivityKey.Size = new System.Drawing.Size(355, 50);
            this.pnlProductivityKey.TabIndex = 58;
            // 
            // checkBox108
            // 
            this.checkBox108.AutoSize = true;
            this.checkBox108.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox108.Location = new System.Drawing.Point(154, 28);
            this.checkBox108.Name = "checkBox108";
            this.checkBox108.Size = new System.Drawing.Size(82, 18);
            this.checkBox108.TabIndex = 46;
            this.checkBox108.Text = "成本目标";
            this.checkBox108.UseVisualStyleBackColor = true;
            // 
            // checkBox109
            // 
            this.checkBox109.AutoSize = true;
            this.checkBox109.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox109.Location = new System.Drawing.Point(14, 28);
            this.checkBox109.Name = "checkBox109";
            this.checkBox109.Size = new System.Drawing.Size(138, 18);
            this.checkBox109.TabIndex = 45;
            this.checkBox109.Text = "内部成本降低程序";
            this.checkBox109.UseVisualStyleBackColor = true;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label32.Location = new System.Drawing.Point(3, 4);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(76, 16);
            this.label32.TabIndex = 44;
            this.label32.Text = "生产效率";
            // 
            // pnlFinanceKey
            // 
            this.pnlFinanceKey.Controls.Add(this.checkBox110);
            this.pnlFinanceKey.Controls.Add(this.checkBox111);
            this.pnlFinanceKey.Controls.Add(this.label33);
            this.pnlFinanceKey.Location = new System.Drawing.Point(19, 409);
            this.pnlFinanceKey.Name = "pnlFinanceKey";
            this.pnlFinanceKey.Size = new System.Drawing.Size(355, 51);
            this.pnlFinanceKey.TabIndex = 57;
            // 
            // checkBox110
            // 
            this.checkBox110.AutoSize = true;
            this.checkBox110.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox110.Location = new System.Drawing.Point(146, 27);
            this.checkBox110.Name = "checkBox110";
            this.checkBox110.Size = new System.Drawing.Size(82, 18);
            this.checkBox110.TabIndex = 37;
            this.checkBox110.Text = "财款期限";
            this.checkBox110.UseVisualStyleBackColor = true;
            // 
            // checkBox111
            // 
            this.checkBox111.AutoSize = true;
            this.checkBox111.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox111.Location = new System.Drawing.Point(11, 28);
            this.checkBox111.Name = "checkBox111";
            this.checkBox111.Size = new System.Drawing.Size(82, 18);
            this.checkBox111.TabIndex = 36;
            this.checkBox111.Text = "财务评价";
            this.checkBox111.UseVisualStyleBackColor = true;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label33.Location = new System.Drawing.Point(4, 5);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(42, 16);
            this.label33.TabIndex = 35;
            this.label33.Text = "财务";
            // 
            // pnlTechnicalAbilityKey
            // 
            this.pnlTechnicalAbilityKey.Controls.Add(this.checkBox112);
            this.pnlTechnicalAbilityKey.Controls.Add(this.checkBox113);
            this.pnlTechnicalAbilityKey.Controls.Add(this.checkBox114);
            this.pnlTechnicalAbilityKey.Controls.Add(this.checkBox115);
            this.pnlTechnicalAbilityKey.Controls.Add(this.checkBox116);
            this.pnlTechnicalAbilityKey.Controls.Add(this.checkBox117);
            this.pnlTechnicalAbilityKey.Controls.Add(this.checkBox118);
            this.pnlTechnicalAbilityKey.Controls.Add(this.label34);
            this.pnlTechnicalAbilityKey.Location = new System.Drawing.Point(19, 326);
            this.pnlTechnicalAbilityKey.Name = "pnlTechnicalAbilityKey";
            this.pnlTechnicalAbilityKey.Size = new System.Drawing.Size(572, 80);
            this.pnlTechnicalAbilityKey.TabIndex = 56;
            // 
            // checkBox112
            // 
            this.checkBox112.AutoSize = true;
            this.checkBox112.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox112.Location = new System.Drawing.Point(245, 55);
            this.checkBox112.Name = "checkBox112";
            this.checkBox112.Size = new System.Drawing.Size(82, 18);
            this.checkBox112.TabIndex = 48;
            this.checkBox112.Text = "设计更改";
            this.checkBox112.UseVisualStyleBackColor = true;
            // 
            // checkBox113
            // 
            this.checkBox113.AutoSize = true;
            this.checkBox113.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox113.Location = new System.Drawing.Point(147, 55);
            this.checkBox113.Name = "checkBox113";
            this.checkBox113.Size = new System.Drawing.Size(54, 18);
            this.checkBox113.TabIndex = 47;
            this.checkBox113.Text = "设计";
            this.checkBox113.UseVisualStyleBackColor = true;
            // 
            // checkBox114
            // 
            this.checkBox114.AutoSize = true;
            this.checkBox114.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox114.Location = new System.Drawing.Point(8, 55);
            this.checkBox114.Name = "checkBox114";
            this.checkBox114.Size = new System.Drawing.Size(54, 18);
            this.checkBox114.TabIndex = 46;
            this.checkBox114.Text = "研发";
            this.checkBox114.UseVisualStyleBackColor = true;
            // 
            // checkBox115
            // 
            this.checkBox115.AutoSize = true;
            this.checkBox115.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox115.Location = new System.Drawing.Point(438, 31);
            this.checkBox115.Name = "checkBox115";
            this.checkBox115.Size = new System.Drawing.Size(82, 18);
            this.checkBox115.TabIndex = 45;
            this.checkBox115.Text = "试制样件";
            this.checkBox115.UseVisualStyleBackColor = true;
            // 
            // checkBox116
            // 
            this.checkBox116.AutoSize = true;
            this.checkBox116.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox116.Location = new System.Drawing.Point(245, 31);
            this.checkBox116.Name = "checkBox116";
            this.checkBox116.Size = new System.Drawing.Size(110, 18);
            this.checkBox116.TabIndex = 44;
            this.checkBox116.Text = "产品工程技术";
            this.checkBox116.UseVisualStyleBackColor = true;
            // 
            // checkBox117
            // 
            this.checkBox117.AutoSize = true;
            this.checkBox117.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox117.Location = new System.Drawing.Point(147, 31);
            this.checkBox117.Name = "checkBox117";
            this.checkBox117.Size = new System.Drawing.Size(82, 18);
            this.checkBox117.TabIndex = 43;
            this.checkBox117.Text = "工程经验";
            this.checkBox117.UseVisualStyleBackColor = true;
            // 
            // checkBox118
            // 
            this.checkBox118.AutoSize = true;
            this.checkBox118.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox118.Location = new System.Drawing.Point(8, 31);
            this.checkBox118.Name = "checkBox118";
            this.checkBox118.Size = new System.Drawing.Size(124, 18);
            this.checkBox118.TabIndex = 42;
            this.checkBox118.Text = "产品和工程技术";
            this.checkBox118.UseVisualStyleBackColor = true;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label34.Location = new System.Drawing.Point(5, 7);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(76, 16);
            this.label34.TabIndex = 41;
            this.label34.Text = "技术能力";
            // 
            // pnlAftersalesKey
            // 
            this.pnlAftersalesKey.Controls.Add(this.checkBox119);
            this.pnlAftersalesKey.Controls.Add(this.checkBox120);
            this.pnlAftersalesKey.Controls.Add(this.checkBox121);
            this.pnlAftersalesKey.Controls.Add(this.checkBox122);
            this.pnlAftersalesKey.Controls.Add(this.label35);
            this.pnlAftersalesKey.Location = new System.Drawing.Point(19, 269);
            this.pnlAftersalesKey.Name = "pnlAftersalesKey";
            this.pnlAftersalesKey.Size = new System.Drawing.Size(572, 54);
            this.pnlAftersalesKey.TabIndex = 55;
            // 
            // checkBox119
            // 
            this.checkBox119.AutoSize = true;
            this.checkBox119.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox119.Location = new System.Drawing.Point(441, 29);
            this.checkBox119.Name = "checkBox119";
            this.checkBox119.Size = new System.Drawing.Size(54, 18);
            this.checkBox119.TabIndex = 32;
            this.checkBox119.Text = "赔偿";
            this.checkBox119.UseVisualStyleBackColor = true;
            // 
            // checkBox120
            // 
            this.checkBox120.AutoSize = true;
            this.checkBox120.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox120.Location = new System.Drawing.Point(248, 28);
            this.checkBox120.Name = "checkBox120";
            this.checkBox120.Size = new System.Drawing.Size(96, 18);
            this.checkBox120.TabIndex = 31;
            this.checkBox120.Text = "合作和支持";
            this.checkBox120.UseVisualStyleBackColor = true;
            // 
            // checkBox121
            // 
            this.checkBox121.AutoSize = true;
            this.checkBox121.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox121.Location = new System.Drawing.Point(150, 28);
            this.checkBox121.Name = "checkBox121";
            this.checkBox121.Size = new System.Drawing.Size(82, 18);
            this.checkBox121.TabIndex = 30;
            this.checkBox121.Text = "服务读物";
            this.checkBox121.UseVisualStyleBackColor = true;
            // 
            // checkBox122
            // 
            this.checkBox122.AutoSize = true;
            this.checkBox122.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox122.Location = new System.Drawing.Point(11, 29);
            this.checkBox122.Name = "checkBox122";
            this.checkBox122.Size = new System.Drawing.Size(138, 18);
            this.checkBox122.TabIndex = 29;
            this.checkBox122.Text = "技术文件（售后）";
            this.checkBox122.UseVisualStyleBackColor = true;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label35.Location = new System.Drawing.Point(4, 6);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(42, 16);
            this.label35.TabIndex = 28;
            this.label35.Text = "售后";
            // 
            // pnlLogisticsKey
            // 
            this.pnlLogisticsKey.Controls.Add(this.checkBox123);
            this.pnlLogisticsKey.Controls.Add(this.checkBox124);
            this.pnlLogisticsKey.Controls.Add(this.checkBox125);
            this.pnlLogisticsKey.Controls.Add(this.label36);
            this.pnlLogisticsKey.Location = new System.Drawing.Point(19, 213);
            this.pnlLogisticsKey.Name = "pnlLogisticsKey";
            this.pnlLogisticsKey.Size = new System.Drawing.Size(572, 52);
            this.pnlLogisticsKey.TabIndex = 54;
            // 
            // checkBox123
            // 
            this.checkBox123.AutoSize = true;
            this.checkBox123.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox123.Location = new System.Drawing.Point(314, 29);
            this.checkBox123.Name = "checkBox123";
            this.checkBox123.Size = new System.Drawing.Size(166, 18);
            this.checkBox123.TabIndex = 26;
            this.checkBox123.Text = "环境审核--产品和服务";
            this.checkBox123.UseVisualStyleBackColor = true;
            // 
            // checkBox124
            // 
            this.checkBox124.AutoSize = true;
            this.checkBox124.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox124.Location = new System.Drawing.Point(129, 29);
            this.checkBox124.Name = "checkBox124";
            this.checkBox124.Size = new System.Drawing.Size(159, 18);
            this.checkBox124.TabIndex = 25;
            this.checkBox124.Text = "交付准确性/服务水平";
            this.checkBox124.UseVisualStyleBackColor = true;
            // 
            // checkBox125
            // 
            this.checkBox125.AutoSize = true;
            this.checkBox125.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox125.Location = new System.Drawing.Point(13, 29);
            this.checkBox125.Name = "checkBox125";
            this.checkBox125.Size = new System.Drawing.Size(82, 18);
            this.checkBox125.TabIndex = 24;
            this.checkBox125.Text = "物流体系";
            this.checkBox125.UseVisualStyleBackColor = true;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label36.Location = new System.Drawing.Point(5, 5);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(42, 16);
            this.label36.TabIndex = 23;
            this.label36.Text = "物流";
            // 
            // pnlQualityKey
            // 
            this.pnlQualityKey.Controls.Add(this.checkBox126);
            this.pnlQualityKey.Controls.Add(this.checkBox127);
            this.pnlQualityKey.Controls.Add(this.checkBox128);
            this.pnlQualityKey.Controls.Add(this.checkBox129);
            this.pnlQualityKey.Controls.Add(this.checkBox130);
            this.pnlQualityKey.Controls.Add(this.label37);
            this.pnlQualityKey.Location = new System.Drawing.Point(19, 155);
            this.pnlQualityKey.Name = "pnlQualityKey";
            this.pnlQualityKey.Size = new System.Drawing.Size(634, 56);
            this.pnlQualityKey.TabIndex = 53;
            // 
            // checkBox126
            // 
            this.checkBox126.AutoSize = true;
            this.checkBox126.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox126.Location = new System.Drawing.Point(537, 31);
            this.checkBox126.Name = "checkBox126";
            this.checkBox126.Size = new System.Drawing.Size(82, 18);
            this.checkBox126.TabIndex = 24;
            this.checkBox126.Text = "问题处理";
            this.checkBox126.UseVisualStyleBackColor = true;
            // 
            // checkBox127
            // 
            this.checkBox127.AutoSize = true;
            this.checkBox127.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox127.Location = new System.Drawing.Point(453, 31);
            this.checkBox127.Name = "checkBox127";
            this.checkBox127.Size = new System.Drawing.Size(68, 18);
            this.checkBox127.TabIndex = 23;
            this.checkBox127.Text = "可靠性";
            this.checkBox127.UseVisualStyleBackColor = true;
            // 
            // checkBox128
            // 
            this.checkBox128.AutoSize = true;
            this.checkBox128.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox128.Location = new System.Drawing.Point(325, 30);
            this.checkBox128.Name = "checkBox128";
            this.checkBox128.Size = new System.Drawing.Size(110, 18);
            this.checkBox128.TabIndex = 22;
            this.checkBox128.Text = "交付质量业绩";
            this.checkBox128.UseVisualStyleBackColor = true;
            // 
            // checkBox129
            // 
            this.checkBox129.AutoSize = true;
            this.checkBox129.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox129.Location = new System.Drawing.Point(139, 30);
            this.checkBox129.Name = "checkBox129";
            this.checkBox129.Size = new System.Drawing.Size(180, 18);
            this.checkBox129.TabIndex = 21;
            this.checkBox129.Text = "质量策划--部品质量保证";
            this.checkBox129.UseVisualStyleBackColor = true;
            // 
            // checkBox130
            // 
            this.checkBox130.AutoSize = true;
            this.checkBox130.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox130.Location = new System.Drawing.Point(23, 31);
            this.checkBox130.Name = "checkBox130";
            this.checkBox130.Size = new System.Drawing.Size(110, 18);
            this.checkBox130.TabIndex = 20;
            this.checkBox130.Text = "质量管理体系";
            this.checkBox130.UseVisualStyleBackColor = true;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label37.Location = new System.Drawing.Point(5, 6);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(42, 16);
            this.label37.TabIndex = 19;
            this.label37.Text = "质量";
            // 
            // pnlEnvironmentKey
            // 
            this.pnlEnvironmentKey.Controls.Add(this.checkBox131);
            this.pnlEnvironmentKey.Controls.Add(this.checkBox132);
            this.pnlEnvironmentKey.Controls.Add(this.checkBox133);
            this.pnlEnvironmentKey.Controls.Add(this.label38);
            this.pnlEnvironmentKey.Location = new System.Drawing.Point(19, 106);
            this.pnlEnvironmentKey.Name = "pnlEnvironmentKey";
            this.pnlEnvironmentKey.Size = new System.Drawing.Size(520, 47);
            this.pnlEnvironmentKey.TabIndex = 52;
            // 
            // checkBox131
            // 
            this.checkBox131.AutoSize = true;
            this.checkBox131.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox131.Location = new System.Drawing.Point(294, 23);
            this.checkBox131.Name = "checkBox131";
            this.checkBox131.Size = new System.Drawing.Size(166, 18);
            this.checkBox131.TabIndex = 15;
            this.checkBox131.Text = "环境审核--产品和服务";
            this.checkBox131.UseVisualStyleBackColor = true;
            // 
            // checkBox132
            // 
            this.checkBox132.AutoSize = true;
            this.checkBox132.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox132.Location = new System.Drawing.Point(125, 23);
            this.checkBox132.Name = "checkBox132";
            this.checkBox132.Size = new System.Drawing.Size(152, 18);
            this.checkBox132.TabIndex = 14;
            this.checkBox132.Text = "环境审核--公司水平";
            this.checkBox132.UseVisualStyleBackColor = true;
            // 
            // checkBox133
            // 
            this.checkBox133.AutoSize = true;
            this.checkBox133.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox133.Location = new System.Drawing.Point(7, 23);
            this.checkBox133.Name = "checkBox133";
            this.checkBox133.Size = new System.Drawing.Size(110, 18);
            this.checkBox133.TabIndex = 13;
            this.checkBox133.Text = "环境管理体系";
            this.checkBox133.UseVisualStyleBackColor = true;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label38.Location = new System.Drawing.Point(3, 4);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(42, 16);
            this.label38.TabIndex = 9;
            this.label38.Text = "环境";
            // 
            // pnlManageKey
            // 
            this.pnlManageKey.Controls.Add(this.checkBox134);
            this.pnlManageKey.Controls.Add(this.checkBox135);
            this.pnlManageKey.Controls.Add(this.checkBox136);
            this.pnlManageKey.Controls.Add(this.checkBox137);
            this.pnlManageKey.Controls.Add(this.label39);
            this.pnlManageKey.Location = new System.Drawing.Point(19, 58);
            this.pnlManageKey.Name = "pnlManageKey";
            this.pnlManageKey.Size = new System.Drawing.Size(400, 47);
            this.pnlManageKey.TabIndex = 51;
            // 
            // checkBox134
            // 
            this.checkBox134.AutoSize = true;
            this.checkBox134.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox134.Location = new System.Drawing.Point(294, 24);
            this.checkBox134.Name = "checkBox134";
            this.checkBox134.Size = new System.Drawing.Size(82, 18);
            this.checkBox134.TabIndex = 13;
            this.checkBox134.Text = "风险管理";
            this.checkBox134.UseVisualStyleBackColor = true;
            // 
            // checkBox135
            // 
            this.checkBox135.AutoSize = true;
            this.checkBox135.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox135.Location = new System.Drawing.Point(185, 24);
            this.checkBox135.Name = "checkBox135";
            this.checkBox135.Size = new System.Drawing.Size(103, 18);
            this.checkBox135.TabIndex = 12;
            this.checkBox135.Text = "TQM工作程序";
            this.checkBox135.UseVisualStyleBackColor = true;
            // 
            // checkBox136
            // 
            this.checkBox136.AutoSize = true;
            this.checkBox136.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox136.Location = new System.Drawing.Point(97, 24);
            this.checkBox136.Name = "checkBox136";
            this.checkBox136.Size = new System.Drawing.Size(82, 18);
            this.checkBox136.TabIndex = 11;
            this.checkBox136.Text = "客户满意";
            this.checkBox136.UseVisualStyleBackColor = true;
            // 
            // checkBox137
            // 
            this.checkBox137.AutoSize = true;
            this.checkBox137.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox137.Location = new System.Drawing.Point(8, 24);
            this.checkBox137.Name = "checkBox137";
            this.checkBox137.Size = new System.Drawing.Size(54, 18);
            this.checkBox137.TabIndex = 10;
            this.checkBox137.Text = "管理";
            this.checkBox137.UseVisualStyleBackColor = true;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label39.Location = new System.Drawing.Point(4, 3);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(42, 16);
            this.label39.TabIndex = 9;
            this.label39.Text = "管理";
            // 
            // pnlCompanyProfileKey
            // 
            this.pnlCompanyProfileKey.Controls.Add(this.checkBox138);
            this.pnlCompanyProfileKey.Controls.Add(this.checkBox139);
            this.pnlCompanyProfileKey.Controls.Add(this.checkBox140);
            this.pnlCompanyProfileKey.Controls.Add(this.label40);
            this.pnlCompanyProfileKey.Location = new System.Drawing.Point(19, 4);
            this.pnlCompanyProfileKey.Name = "pnlCompanyProfileKey";
            this.pnlCompanyProfileKey.Size = new System.Drawing.Size(309, 52);
            this.pnlCompanyProfileKey.TabIndex = 50;
            // 
            // checkBox138
            // 
            this.checkBox138.AutoSize = true;
            this.checkBox138.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox138.Location = new System.Drawing.Point(185, 30);
            this.checkBox138.Name = "checkBox138";
            this.checkBox138.Size = new System.Drawing.Size(68, 18);
            this.checkBox138.TabIndex = 7;
            this.checkBox138.Text = "依附性";
            this.checkBox138.UseVisualStyleBackColor = true;
            // 
            // checkBox139
            // 
            this.checkBox139.AutoSize = true;
            this.checkBox139.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox139.Location = new System.Drawing.Point(97, 30);
            this.checkBox139.Name = "checkBox139";
            this.checkBox139.Size = new System.Drawing.Size(82, 18);
            this.checkBox139.TabIndex = 6;
            this.checkBox139.Text = "全球能力";
            this.checkBox139.UseVisualStyleBackColor = true;
            // 
            // checkBox140
            // 
            this.checkBox140.AutoSize = true;
            this.checkBox140.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox140.Location = new System.Drawing.Point(8, 30);
            this.checkBox140.Name = "checkBox140";
            this.checkBox140.Size = new System.Drawing.Size(82, 18);
            this.checkBox140.TabIndex = 5;
            this.checkBox140.Text = "股权关系";
            this.checkBox140.UseVisualStyleBackColor = true;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label40.Location = new System.Drawing.Point(4, 6);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(76, 16);
            this.label40.TabIndex = 4;
            this.label40.Text = "公司概况";
            // 
            // btnPreviewKey
            // 
            this.btnPreviewKey.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnPreviewKey.Location = new System.Drawing.Point(482, 573);
            this.btnPreviewKey.Name = "btnPreviewKey";
            this.btnPreviewKey.Size = new System.Drawing.Size(75, 32);
            this.btnPreviewKey.TabIndex = 49;
            this.btnPreviewKey.Text = "预览";
            this.btnPreviewKey.UseVisualStyleBackColor = true;
            // 
            // btnSaveKey
            // 
            this.btnSaveKey.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnSaveKey.Location = new System.Drawing.Point(288, 573);
            this.btnSaveKey.Name = "btnSaveKey";
            this.btnSaveKey.Size = new System.Drawing.Size(75, 32);
            this.btnSaveKey.TabIndex = 48;
            this.btnSaveKey.Text = "保存";
            this.btnSaveKey.UseVisualStyleBackColor = true;
            // 
            // 认证评估模板设置_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(982, 704);
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "认证评估模板设置_Form";
            this.Text = "认证评估模板设置_Form";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.pnlCommon.ResumeLayout(false);
            this.pnlPurchaseCommon.ResumeLayout(false);
            this.pnlPurchaseCommon.PerformLayout();
            this.pnlProductivityCommon.ResumeLayout(false);
            this.pnlProductivityCommon.PerformLayout();
            this.pnlFinanceCommon.ResumeLayout(false);
            this.pnlFinanceCommon.PerformLayout();
            this.pnlTechnicalAbilityCommon.ResumeLayout(false);
            this.pnlTechnicalAbilityCommon.PerformLayout();
            this.pnlAftersalesCommon.ResumeLayout(false);
            this.pnlAftersalesCommon.PerformLayout();
            this.pnlLogisticsCommon.ResumeLayout(false);
            this.pnlLogisticsCommon.PerformLayout();
            this.pnlQualityCommon.ResumeLayout(false);
            this.pnlQualityCommon.PerformLayout();
            this.pnlEnvironmentCommon.ResumeLayout(false);
            this.pnlEnvironmentCommon.PerformLayout();
            this.pnlManageCommon.ResumeLayout(false);
            this.pnlManageCommon.PerformLayout();
            this.pnlCompanyProfileCommon.ResumeLayout(false);
            this.pnlCompanyProfileCommon.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.pnlLeveraged.ResumeLayout(false);
            this.pnlPurchaseLeveraged.ResumeLayout(false);
            this.pnlPurchaseLeveraged.PerformLayout();
            this.pnlProductivityLeveraged.ResumeLayout(false);
            this.pnlProductivityLeveraged.PerformLayout();
            this.pnlFinanceLeveraged.ResumeLayout(false);
            this.pnlFinanceLeveraged.PerformLayout();
            this.pnlTechnicalAbilityLeveraged.ResumeLayout(false);
            this.pnlTechnicalAbilityLeveraged.PerformLayout();
            this.pnlAftersalesLeveraged.ResumeLayout(false);
            this.pnlAftersalesLeveraged.PerformLayout();
            this.pnlLogisticsLeveraged.ResumeLayout(false);
            this.pnlLogisticsLeveraged.PerformLayout();
            this.pnlQualityLeveraged.ResumeLayout(false);
            this.pnlQualityLeveraged.PerformLayout();
            this.pnlEnvironmentLeveraged.ResumeLayout(false);
            this.pnlEnvironmentLeveraged.PerformLayout();
            this.pnlManageLeveraged.ResumeLayout(false);
            this.pnlManageLeveraged.PerformLayout();
            this.pnlCompanyProfileLeveraged.ResumeLayout(false);
            this.pnlCompanyProfileLeveraged.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.pnlBottleneck.ResumeLayout(false);
            this.pnlPurchaseBottleneck.ResumeLayout(false);
            this.pnlPurchaseBottleneck.PerformLayout();
            this.pnlProductivityBottleneck.ResumeLayout(false);
            this.pnlProductivityBottleneck.PerformLayout();
            this.pnlFinanceBottleneck.ResumeLayout(false);
            this.pnlFinanceBottleneck.PerformLayout();
            this.pnlTechnicalAbilityBottleneck.ResumeLayout(false);
            this.pnlTechnicalAbilityBottleneck.PerformLayout();
            this.pnlAftersalesBottleneck.ResumeLayout(false);
            this.pnlAftersalesBottleneck.PerformLayout();
            this.pnlLogisticsBottleneck.ResumeLayout(false);
            this.pnlLogisticsBottleneck.PerformLayout();
            this.pnlQualityBottleneck.ResumeLayout(false);
            this.pnlQualityBottleneck.PerformLayout();
            this.pnlEnvironmentBottleneck.ResumeLayout(false);
            this.pnlEnvironmentBottleneck.PerformLayout();
            this.pnlManageBottleneck.ResumeLayout(false);
            this.pnlManageBottleneck.PerformLayout();
            this.pnlCompanyProfileBottleneck.ResumeLayout(false);
            this.pnlCompanyProfileBottleneck.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.pnlKey.ResumeLayout(false);
            this.pnlPurchaseKey.ResumeLayout(false);
            this.pnlPurchaseKey.PerformLayout();
            this.pnlProductivityKey.ResumeLayout(false);
            this.pnlProductivityKey.PerformLayout();
            this.pnlFinanceKey.ResumeLayout(false);
            this.pnlFinanceKey.PerformLayout();
            this.pnlTechnicalAbilityKey.ResumeLayout(false);
            this.pnlTechnicalAbilityKey.PerformLayout();
            this.pnlAftersalesKey.ResumeLayout(false);
            this.pnlAftersalesKey.PerformLayout();
            this.pnlLogisticsKey.ResumeLayout(false);
            this.pnlLogisticsKey.PerformLayout();
            this.pnlQualityKey.ResumeLayout(false);
            this.pnlQualityKey.PerformLayout();
            this.pnlEnvironmentKey.ResumeLayout(false);
            this.pnlEnvironmentKey.PerformLayout();
            this.pnlManageKey.ResumeLayout(false);
            this.pnlManageKey.PerformLayout();
            this.pnlCompanyProfileKey.ResumeLayout(false);
            this.pnlCompanyProfileKey.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.Panel pnlCommon;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnPreviewCommon;
        private System.Windows.Forms.Button btnSaveCommon;
        private System.Windows.Forms.Panel pnlCompanyProfileCommon;
        private System.Windows.Forms.Panel pnlManageCommon;
        private System.Windows.Forms.CheckBox checkBox3;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox checkBox7;
        private System.Windows.Forms.CheckBox checkBox4;
        private System.Windows.Forms.CheckBox checkBox5;
        private System.Windows.Forms.CheckBox checkBox6;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel pnlEnvironmentCommon;
        private System.Windows.Forms.Panel pnlQualityCommon;
        private System.Windows.Forms.CheckBox checkBox12;
        private System.Windows.Forms.CheckBox checkBox13;
        private System.Windows.Forms.CheckBox checkBox14;
        private System.Windows.Forms.Panel pnlLogisticsCommon;
        private System.Windows.Forms.CheckBox checkBox15;
        private System.Windows.Forms.CheckBox checkBox8;
        private System.Windows.Forms.CheckBox checkBox9;
        private System.Windows.Forms.CheckBox checkBox10;
        private System.Windows.Forms.CheckBox checkBox11;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox checkBox21;
        private System.Windows.Forms.CheckBox checkBox22;
        private System.Windows.Forms.CheckBox checkBox23;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel pnlAftersalesCommon;
        private System.Windows.Forms.Panel pnlTechnicalAbilityCommon;
        private System.Windows.Forms.CheckBox checkBox17;
        private System.Windows.Forms.CheckBox checkBox18;
        private System.Windows.Forms.CheckBox checkBox19;
        private System.Windows.Forms.CheckBox checkBox20;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel pnlFinanceCommon;
        private System.Windows.Forms.CheckBox checkBox33;
        private System.Windows.Forms.CheckBox checkBox31;
        private System.Windows.Forms.CheckBox checkBox32;
        private System.Windows.Forms.CheckBox checkBox30;
        private System.Windows.Forms.CheckBox checkBox27;
        private System.Windows.Forms.CheckBox checkBox28;
        private System.Windows.Forms.CheckBox checkBox29;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Panel pnlProductivityCommon;
        private System.Windows.Forms.CheckBox checkBox25;
        private System.Windows.Forms.CheckBox checkBox26;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel pnlPurchaseCommon;
        private System.Windows.Forms.CheckBox checkBox39;
        private System.Windows.Forms.CheckBox checkBox40;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.CheckBox checkBox36;
        private System.Windows.Forms.CheckBox checkBox37;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Panel pnlLeveraged;
        private System.Windows.Forms.Panel pnlPurchaseLeveraged;
        private System.Windows.Forms.CheckBox checkBox16;
        private System.Windows.Forms.CheckBox checkBox24;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Panel pnlProductivityLeveraged;
        private System.Windows.Forms.CheckBox checkBox34;
        private System.Windows.Forms.CheckBox checkBox35;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Panel pnlFinanceLeveraged;
        private System.Windows.Forms.CheckBox checkBox38;
        private System.Windows.Forms.CheckBox checkBox41;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Panel pnlTechnicalAbilityLeveraged;
        private System.Windows.Forms.CheckBox checkBox42;
        private System.Windows.Forms.CheckBox checkBox43;
        private System.Windows.Forms.CheckBox checkBox44;
        private System.Windows.Forms.CheckBox checkBox45;
        private System.Windows.Forms.CheckBox checkBox46;
        private System.Windows.Forms.CheckBox checkBox47;
        private System.Windows.Forms.CheckBox checkBox48;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Panel pnlAftersalesLeveraged;
        private System.Windows.Forms.CheckBox checkBox49;
        private System.Windows.Forms.CheckBox checkBox50;
        private System.Windows.Forms.CheckBox checkBox51;
        private System.Windows.Forms.CheckBox checkBox52;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Panel pnlLogisticsLeveraged;
        private System.Windows.Forms.CheckBox checkBox53;
        private System.Windows.Forms.CheckBox checkBox54;
        private System.Windows.Forms.CheckBox checkBox55;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Panel pnlQualityLeveraged;
        private System.Windows.Forms.CheckBox checkBox56;
        private System.Windows.Forms.CheckBox checkBox57;
        private System.Windows.Forms.CheckBox checkBox58;
        private System.Windows.Forms.CheckBox checkBox59;
        private System.Windows.Forms.CheckBox checkBox60;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Panel pnlEnvironmentLeveraged;
        private System.Windows.Forms.CheckBox checkBox61;
        private System.Windows.Forms.CheckBox checkBox62;
        private System.Windows.Forms.CheckBox checkBox63;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Panel pnlManageLeveraged;
        private System.Windows.Forms.CheckBox checkBox64;
        private System.Windows.Forms.CheckBox checkBox65;
        private System.Windows.Forms.CheckBox checkBox66;
        private System.Windows.Forms.CheckBox checkBox67;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Panel pnlCompanyProfileLeveraged;
        private System.Windows.Forms.CheckBox checkBox68;
        private System.Windows.Forms.CheckBox checkBox69;
        private System.Windows.Forms.CheckBox checkBox70;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Button btnPreviewLeveraged;
        private System.Windows.Forms.Button btnSaveLeveraged;
        private System.Windows.Forms.Panel pnlBottleneck;
        private System.Windows.Forms.Panel pnlPurchaseBottleneck;
        private System.Windows.Forms.CheckBox checkBox71;
        private System.Windows.Forms.CheckBox checkBox72;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Panel pnlProductivityBottleneck;
        private System.Windows.Forms.CheckBox checkBox73;
        private System.Windows.Forms.CheckBox checkBox74;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Panel pnlFinanceBottleneck;
        private System.Windows.Forms.CheckBox checkBox75;
        private System.Windows.Forms.CheckBox checkBox76;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Panel pnlTechnicalAbilityBottleneck;
        private System.Windows.Forms.CheckBox checkBox77;
        private System.Windows.Forms.CheckBox checkBox78;
        private System.Windows.Forms.CheckBox checkBox79;
        private System.Windows.Forms.CheckBox checkBox80;
        private System.Windows.Forms.CheckBox checkBox81;
        private System.Windows.Forms.CheckBox checkBox82;
        private System.Windows.Forms.CheckBox checkBox83;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Panel pnlAftersalesBottleneck;
        private System.Windows.Forms.CheckBox checkBox84;
        private System.Windows.Forms.CheckBox checkBox85;
        private System.Windows.Forms.CheckBox checkBox86;
        private System.Windows.Forms.CheckBox checkBox87;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Panel pnlLogisticsBottleneck;
        private System.Windows.Forms.CheckBox checkBox88;
        private System.Windows.Forms.CheckBox checkBox89;
        private System.Windows.Forms.CheckBox checkBox90;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Panel pnlQualityBottleneck;
        private System.Windows.Forms.CheckBox checkBox91;
        private System.Windows.Forms.CheckBox checkBox92;
        private System.Windows.Forms.CheckBox checkBox93;
        private System.Windows.Forms.CheckBox checkBox94;
        private System.Windows.Forms.CheckBox checkBox95;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Panel pnlEnvironmentBottleneck;
        private System.Windows.Forms.CheckBox checkBox96;
        private System.Windows.Forms.CheckBox checkBox97;
        private System.Windows.Forms.CheckBox checkBox98;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Panel pnlManageBottleneck;
        private System.Windows.Forms.CheckBox checkBox99;
        private System.Windows.Forms.CheckBox checkBox100;
        private System.Windows.Forms.CheckBox checkBox101;
        private System.Windows.Forms.CheckBox checkBox102;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Panel pnlCompanyProfileBottleneck;
        private System.Windows.Forms.CheckBox checkBox103;
        private System.Windows.Forms.CheckBox checkBox104;
        private System.Windows.Forms.CheckBox checkBox105;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Button btnPreviewBottleneck;
        private System.Windows.Forms.Button btnSaveBottleneck;
        private System.Windows.Forms.Panel pnlKey;
        private System.Windows.Forms.Panel pnlPurchaseKey;
        private System.Windows.Forms.CheckBox checkBox106;
        private System.Windows.Forms.CheckBox checkBox107;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Panel pnlProductivityKey;
        private System.Windows.Forms.CheckBox checkBox108;
        private System.Windows.Forms.CheckBox checkBox109;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Panel pnlFinanceKey;
        private System.Windows.Forms.CheckBox checkBox110;
        private System.Windows.Forms.CheckBox checkBox111;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Panel pnlTechnicalAbilityKey;
        private System.Windows.Forms.CheckBox checkBox112;
        private System.Windows.Forms.CheckBox checkBox113;
        private System.Windows.Forms.CheckBox checkBox114;
        private System.Windows.Forms.CheckBox checkBox115;
        private System.Windows.Forms.CheckBox checkBox116;
        private System.Windows.Forms.CheckBox checkBox117;
        private System.Windows.Forms.CheckBox checkBox118;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Panel pnlAftersalesKey;
        private System.Windows.Forms.CheckBox checkBox119;
        private System.Windows.Forms.CheckBox checkBox120;
        private System.Windows.Forms.CheckBox checkBox121;
        private System.Windows.Forms.CheckBox checkBox122;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Panel pnlLogisticsKey;
        private System.Windows.Forms.CheckBox checkBox123;
        private System.Windows.Forms.CheckBox checkBox124;
        private System.Windows.Forms.CheckBox checkBox125;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Panel pnlQualityKey;
        private System.Windows.Forms.CheckBox checkBox126;
        private System.Windows.Forms.CheckBox checkBox127;
        private System.Windows.Forms.CheckBox checkBox128;
        private System.Windows.Forms.CheckBox checkBox129;
        private System.Windows.Forms.CheckBox checkBox130;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Panel pnlEnvironmentKey;
        private System.Windows.Forms.CheckBox checkBox131;
        private System.Windows.Forms.CheckBox checkBox132;
        private System.Windows.Forms.CheckBox checkBox133;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Panel pnlManageKey;
        private System.Windows.Forms.CheckBox checkBox134;
        private System.Windows.Forms.CheckBox checkBox135;
        private System.Windows.Forms.CheckBox checkBox136;
        private System.Windows.Forms.CheckBox checkBox137;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Panel pnlCompanyProfileKey;
        private System.Windows.Forms.CheckBox checkBox138;
        private System.Windows.Forms.CheckBox checkBox139;
        private System.Windows.Forms.CheckBox checkBox140;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Button btnPreviewKey;
        private System.Windows.Forms.Button btnSaveKey;
    }
}