﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
namespace MMClient
{
    public partial class 认证评估模板设置_Form : Form
    {
        List<string> selectedOption = null;

        public 认证评估模板设置_Form()
        {
            InitializeComponent();
        }
        /// <summary>
        /// 一般型 保存选中的选项
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSaveCommon_Click(object sender, EventArgs e)
        {
            ////1.获取所有选中的选项
            //
            string[] panelName = {"pnlCompanyProfileCommon","pnlManageCommon","pnlEnvironmentCommon","pnlQualityCommon","pnlLogisticsCommon","pnlAftersalesCommon","pnlTechnicalAbilityCommon","pnlFinanceCommon","pnlProductivityCommon","pnlPurchaseCommon"};
            string[] mainStandard = { "公司概况","管理1","环境","质量","物流","售后","技术能力","财务","生产效率","采购"};
            Panel panel = null;
            selectedOption = new List<string>();
            
            bool isMainStandard = false;
            for (int i = 0; i < pnlCommon.Controls.Count; ++i)
            {
                Control c = this.pnlCommon.Controls[i];
                if (c.GetType() == typeof(Panel))
                {
                    panel = this.pnlCommon.Controls[i] as Panel;
                    foreach (Control control in panel.Controls)
                    {
                        if (control.GetType() == typeof(CheckBox))
                        {
                            CheckBox cb = control as CheckBox;
                            //若选中将选项添加到list中
                            if (cb.Checked)
                            {
                                isMainStandard = true;
                                selectedOption.Add(cb.Text);
                            }
                        }
                    }
                    if (isMainStandard)
                    {
                        selectedOption.Add(mainStandard[Convert.ToInt32(panel.Tag)]);
                        isMainStandard = false;
                    }
                }
                
            }
        }

        private void btnPreviewCommon_Click(object sender, EventArgs e)
        {
            if (this.selectedOption == null || this.selectedOption.Count == 0)
            {
                MessageBox.Show("请先保存后在预览");
                return;
            }
            Preview_Form pf = new Preview_Form("基本能力评估模板预览", selectedOption);
            pf.Show();
                
            
        }

        //public static DataSet ListToDataSet<T>(IList<T> list)
        //{
        //    if (list == null || list.Count <= 0)
        //    {
        //        return null;
        //    }
        //    DataSet ds = new DataSet();
        //    DataTable dt = new DataTable(typeof(T).Name);
        //    DataColumn column;
        //    DataRow curRow;
        //    System.Reflection.PropertyInfo[] myPropertyInfo = typeof(T).GetProperties(System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance);
        //    foreach (T t in list)
        //    {
        //        if (t == null)
        //        {
        //            continue;
        //        }
        //        curRow = dt.NewRow();
        //        for (int i = 0, j = myPropertyInfo.Length; i < j; i++)
        //        {
        //            System.Reflection.PropertyInfo pi = myPropertyInfo[i];
        //            string name = pi.Name;
        //            if (dt.Columns[name] == null)
        //            {
        //                column = new DataColumn(name, pi.PropertyType);
        //                dt.Columns.Add(column);
        //            }
        //            curRow[name] = pi.GetValue(t, null);
        //        }
        //        dt.Rows.Add(curRow);
        //    }
        //    ds.Tables.Add(dt);
        //    return ds; 
        //}

    }
}
