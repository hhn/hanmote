﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Bll;

namespace MMClient
{
    public partial class ListSupplierExpenseTypeInfoForm : Form
    {
        private string title = "";
        private SupplierManagementBLL smbll = new SupplierManagementBLL();
        
        public ListSupplierExpenseTypeInfoForm()
        {
            InitializeComponent();

        }
        public ListSupplierExpenseTypeInfoForm(string title,string supplierID,DataTable dt)
        {
            InitializeComponent();
            this.titlelbl.Text = title;
            LoadListView();
            this.BindDataToColumns(dt);
        }

        /// <summary>  
        /// 初始化上传列表  
        /// </summary>  
        private void LoadListView()
        {
            listView1.View = View.Details;
            listView1.CheckBoxes = false;
            listView1.GridLines = true;
            listView1.Columns.Add("物料类型", 50, HorizontalAlignment.Center);
            listView1.Columns.Add("采购支出", 150, HorizontalAlignment.Center);
            listView1.Columns.Add("综合PIP等级", 150, HorizontalAlignment.Center);
            listView1.Columns.Add("质量", 50, HorizontalAlignment.Center);
            listView1.Columns.Add("成本", 50, HorizontalAlignment.Center);
            listView1.Columns.Add("交付", 50, HorizontalAlignment.Center);
            listView1.Columns.Add("服务", 50, HorizontalAlignment.Center);
            listView1.Columns.Add("供应类型", 150, HorizontalAlignment.Center);
        }

        /// <summary>
        /// 将数据绑定到每一列上
        /// </summary>
        /// <param name="dt"></param>
        private void BindDataToColumns(DataTable dt)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                ListViewItem lvi = new ListViewItem(dt.Rows[i].ItemArray[0].ToString());
                lvi.SubItems.Add(dt.Rows[i].ItemArray[1].ToString());
                lvi.SubItems.Add(dt.Rows[i].ItemArray[7].ToString());
                lvi.SubItems.Add(dt.Rows[i].ItemArray[2].ToString());
                lvi.SubItems.Add(dt.Rows[i].ItemArray[3].ToString());
                lvi.SubItems.Add(dt.Rows[i].ItemArray[4].ToString());
                lvi.SubItems.Add(dt.Rows[i].ItemArray[5].ToString());
                lvi.SubItems.Add(dt.Rows[i].ItemArray[8].ToString());
                listView1.Items.Add(lvi);
            }

        }
    }
}
