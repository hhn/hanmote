﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using WeifenLuo.WinFormsUI.Docking;


using Lib.Common.CommonUtils;
using MMClient.ContractManage.OutlineAgreement;
using MMClient.ContractManage.QuotaArrangement;
using MMClient.ContractManage.ContractTemplateManage;
using MMClient.ContractManage.ContractTextManage;
using MMClient.OrderExecution;
using MMClient.OrderExecution.InvoiceAndPayment;
using MMClient.CatalogManage;
using MMClient.RA.PurchaseMonitor;
using MMClient.RA.PurchaseExpense;
using MMClient.RA.SourseTracing;
using MMClient.RA;
using MMClient.RA.SupplierPerformance;
using MMClient.RA.StockAnalysis;
using MMClient.RA.SupplierAnalysis;
using MMClient.RA.SupplierDependentFactors;
using MMClient.RA.Customized;
using MMClient.InventoryManagement;
using MMClient.FileManage;
using MMClient.MD;
using MMClient.MD.MT;
using MMClient.MD.SP;
using MMClient.MD.FI;
using MMClient.MD.GN;
using MMClient.SupplierPerformance;
using MMClient.SupplierPerformance.SupplierClassify;
using MMClient.SupplierPerformance.LPS;
using MMClient.SupplierPerformance.CostReduction;
using MMClient.SupplierPerformance.DataRecord;
using MMClient.SupplierPerformance.PerformanceAssess;
using MMClient.SupplierPerformance.SupplierValue;
using MMClient.MD.MT.MaterialType;
using MMClient.SourcingManage.ProcurementPlan;
using MMClient.SourcingManage.SourceingRecord;
using MMClient.SourcingManage.SourcingManagement;
using MMClient.SourcingManage.SourcingManagement.Inquiry;
using MMClient.SystemConfig.UserManage;
using MMClient.SystemConfig.DataBackupRestoreManage;
using MMClient.SystemConfig.PermissionManage;
using MMClient.SupplierPerformance.SPReport;

using Lib.Bll.SourcingManage.ProcurementPlan;
using Lib.Model.SourcingManage.SourcingManagement;
using Lib.Bll.SourcingManage.SourcingManagement;

using MMClient.CertificationManagement.CertificationProcessing;
using MMClient.CertificationManagement.CertificationProcessing.LocationEvaluation;
using MMClient.CertificationManagement.CertificationProcessing.ImprovementReport;
using Lib.Bll.CertificationProcess.LocationalEvaluation;
using MMClient.SystemConfig;
using System.Media;
using System.Threading;
using System.Diagnostics;
using System.IO;
using Lib.SqlServerDAL.SystemConfig;
using Lib.Bll.SystemConfig.UserManage;
using MMClient.CertificationManagement.文件导入;
using MMClient.CertificationManagement.Material_group_positioning;
using MMClient.Material_group_positioning;
using MMClient.SupplierPerformance.SupplierService;
using MMClient.MD.NewMMarketPrice;
using MMClient.MD.NewMMarketPrice.createRecordInfo;
using MMClient.SupplierPerformance.Supplierdelivery;
using MMClient.SupplierPerformance.SupplierList;
using MMClient.MD.MT.StrategInfo;
using MMClient.supplyManage;
using MMClient.SupplierPerformance.SupplierList.SupplierSupply;
using MMClient.Material_group_positioning.GoalAndAim;
using Lib.Model.SystemConfig;
using MMClient.SupplierPerformance.qulity;
using MMClient.SupplierPerformance.DeliveryGoods;
using MMClient.SupplierPerformance.SecletedEvaluatorForm;
using MMClient.SourcingManage.Volvo;

namespace MMClient
{
    public partial class UserUI : Form
    {

        private SourceSupplierBLL sourceSupplierBll = new SourceSupplierBLL();
        #region 采购计划管理窗体声明
        //计划管理
        private ManagerPlansForm managerPlansForm = null;

        //采购计划管理窗体窗体
        private ShowDemand_Form showDemand_Form = null;

        //新建需求计划（标准）窗体
        private NewStandard_Form newStandard_Form = null;

        //新建需求计划（框架协议）窗体
        private NewFramework_Form newFramework_Form = null;

        //新建需求计划（需求预测）窗体
        private NewPrePlan_Form newPrePlan_Form = null;

        //需求预测窗体
        private DemandForecast_Form demandForecast_Form = null;

        //审核提交（标准）窗体
        private AuditSubmitStandard_Form auditSubmitStandard_Form = null;

        //审核提交（框架协议）窗体
        private AuditSubmitFramework_Form auditSubmitFramework_Form = null; 
        //质量收获窗体
        private qulityForm qulity_Form = null;
        //
        private SecletedEvaluatorForm secletedEvaluatorForm = null;
        //
        private DeliveryEvalForm deliveryGoods = null;
        //生成采购计划（标准）窗体
        private CreatePurchasing_Form createPurchasing_Form = null;
        //nwe生成采购计划
        private CreatePurcharsePlansForm createPurcharsePlansForm = null;
        //审核采购计划
        private ReviewSummaryDemand reviewSummaryDemand_Form = null;
        //生成采购计划（框架协议）窗体
        private CreateFramework_Form createFramework_Form = null;
        //查看改善计划
        private SubmitImprovementReportForm submitImprovementReport = null;
        //测试
        private saveAsForm savsAs = null;
        //需求汇总
        private AggregateMaterial aggregateMaterial_Form = null;
        #endregion

        #region 采购寻源管理窗体声明

        //创建寻源窗体
        private Summary_Demand_Search summary_Demand_Search = null;
        //投标信息窗体
        private BiddingInformation_Form biddingInformation_Form = null;
        //参看询价询价单列表
        private AllInquiry allInquiry = null;
        //询价窗体
        private Inquiry_Form inquiry_Form = null;

        private EditInquery editInquery = null;
        //执行询价窗体
        private Offer_Form offer_Form = null;

        //维护报价窗体
        private MaintenanceOfferPriceForm maintenanceOfferPriceForm = null;

        //比价窗体
        private Compare_Form compare_Form = null;

        //查看招标
        private ShowBid_Form showBid_Form = null;

        //处理招标窗体
        private Bidding_Form bidding_Form = null;

        //发布招标
        private ReleaseBid_Form release_Form = null;

        //更改招标窗体
        private ChangeTender_Form changeTender_Form = null;

        //审批
        private CheckBid_Form checkBid_Form = null;
        //投标窗体
        //private Offer_Form offer_Form = null;
        //评标窗体
        //private Compare_Form compare_Form = null;
        //竞价窗体
        //private Inquiry_Form inquiry_Form = null;

        //谈判列表窗体
        public ShowTransact_Form showTransact_Form = null;

        //创建谈判窗体
        private Transact_Form transact_Form = null;

        //处理谈判窗体
        private DealTransact_Form dealTransact_Form = null;

        //拍卖窗体
        private Auction_Form auction_Form = null;
        //货源清单列表窗体
        private SourceList_Form sourceList_Form = null;
        //创建源清单窗体
        private NewSourceList_Form newSourceList_Form = null;
        //维护源清单窗体
        private EditSourceList_Form editSourceList_Form = null;
        //寻源管理仓
        private InqueryBin_From inqueryBin_From = null;
        private ApproveInvivationFrom approveInvivationFrom = null;
        //寻源信息维护窗体
        private ShowRecordInfo_Form recordInfo_Form = null;
        //采购信息维护窗体
        private SourcingRecord_Form sourcingRecord_Form = null;

        private MRPCountForm newBomForm = null;
        private ReviewBomForm reviewBomForm = null;
        private MRPCount mrpCount = null;
        #endregion

        #region 框架协议
        //协议合同
        private AgreementContractManageForm agreementContractManageForm = null;
        //计划协议
      //  private SchedulingAgreementForm schedulingAgreementForm = null;
      //  private SchedulingAgreementManageForm schedulingAgreementManageForm = null;
        //配额协议
        private QuotaArrangementInitialForm quotaArrangementForm = null;
        #endregion

        #region 合同管理

        //合同模板管理
        private ContractTemplateManageForm contractTemplateManageForm;
        //合同文本管理
        private ContractTextManageForm contractTextManageForm;

        //合同文本

        #endregion

        #region 订单管理

        //订单执行管理界面
        QuryOrder purchaseOrderManageForm = null;
        //发票与支付主界面
        private InvoiceAndPaymentForm invoiceAndPaymentForm = null;
        //生成订单
        Order OrderForm = null;

        #endregion

        #region 自助采购

        /// <summary>
        /// 自助采购主界面
        /// </summary>
        private CatalogManageForm catalogManageForm = null;

        #endregion

        #region 文件管理
        //文件管理
        FileManageForm fileManageForm = null;
        #endregion

        #region 采购监控分析

        //物料ABC类分析
        private Monitor_MaterialABCAnalysis mmABC = null;
        private Monitor_SupplierABCAnalysis msABC = null;

        #endregion

        #region 采购花费分析
        Cost_PurchaseCostDistribution cpcd = null; //采购花费分布
        CostStructureAnalysis costSA = null;   //成本结构分析
        CostComparativeAnalysis costCA = null;   //成本比较分析
        CostTrendAnalysis costTA = null;          //成本趋势分析
        #endregion

        #region 寻源分析
        MaterialPriceOverview pao = null; //价格分析
        SupplierBiddingAnalysis sba = null; //供应商给竞价分析
        #endregion

        #region 合同执行分析

        ConstractionManagement cm = null; //合同管理分析

        #endregion

        #region 供应商绩效分析
        SupplierComparativeAnalysis ssr = null;//供应商对比分析
        SupplierTrendAnalysis sta = null;//供应商趋势报表
        SupplierScoreRanking sScoreRanking = null;
        #endregion

        #region 库存分析
        StockAgeAnalysis saa = null;//库龄分析
        StockABCAnalysis sABCa = null;//库存物料ABC类分析
        #endregion

        #region 供应商分析
        SupplierAnalysis sas = null;    //7.供应商分析
        #endregion

        #region 供应商依赖因素分析
        SupplierDependentFactors sdf = null; //供应商依赖因素分析
        #endregion

        #region 定制分析
        Customized ct = null;   //8.定制
        #endregion

        #region 库存管理模块窗体声明

        #region 基础配置
        //库存类型设置
        Frm_StockType frm_StockType = null;

        //库存状态设置
        Frm_StockState frm_StockState = null;

        #endregion

        //库存移动
        Frm_StockMove frm_StockMove = null;

        //物料凭证
        Frm_MaterialDocument frm_MaterialDocument = null;
        //物料凭证->更改
        Frm_ChangeDocument frm_ChangeDocument = null;
        //物料凭证->显示
        Frm_DisplayDocument frm_DisplayDocument = null;
        //物料凭证->取消/冲销
        Frm_CancelDocument frm_CancelDocument = null;
        //物料凭证->查询
        Frm_QueryDocument frm_QueryDocument = null;

        //移动类型
        //移动类型查看
        Frm_FindMoveType frm_FindMoveType = null;
        //移动类型->TransactionEvent
        Frm_TransOrEvent frm_TransOrEvent = null;

        //收发存报表
        Frm_ReceivingReport frm_ReceivingReport = null;
        // 收发存报表->库存总览
        Frm_StockOverview frm_StockOverview = null;
        // 收发存报表->工厂库存
        Frm_FactoryStock frm_FactoryStock = null;
        //收发存报表->仓库库存
        Frm_StockStock frm_StockStock = null;
        //收发存报表->寄售库存
        Frm_ConsignmentStock frm_ConnsignmentStock = null;
        //收发存报表->在途库存
        Frm_TransitStock frm_TransitStock = null;

        

        //收货评分
        Frm_ReceivingScore frm_ReceivingScore = null;
        //收货评分->质检评分
        Frm_ReceiveScore frm_ReceiveScore = null;
        //收货评分->装运评分
        Frm_ShipmentScore frm_shipmentScore = null;
        //收货评分->非原材料拒收数量
        Frm_NonRawMaterialScore frm_NonRawMaterialScore = null;


        //库存盘点
        Frm_StockCheck frm_StockCheck = null;
        //库存盘点->集中创建盘点凭证
        Frm_CreateStocktaking frm_CreateStockTaking = null;

        //库存盘点->单个创建凭证
        Frm_CreateStocktaking1 frm_CreateStockTaking1 = null;
        //库存盘点->冻结与解冻
        Frm_FreezingStock frm_FreezingStock = null;
        //库存盘点->输入盘点结果
        Frm_InputResult frm_InputResult = null;
        //库存盘点->查看差异
        Frm_ShowDifference frm_ShowDifference = null;
        //库存盘点->盘点过账
        Frm_CheckPost frm_CheckPost = null;
        //库存盘点->输出盘点清单
        Frm_StockList frm_StockList = null; 
        //库存盘点->差异调整
        Frm_AdjustDifference frm_AdjustDifference = null;
        //库存盘点->查看差异调整
        Frm_ShowDiffLoop frm_ShowDiffLoop = null;
        //库存盘点->取消差异调整
        Frm_CancelAdjust frm_CancelAdjust = null;


        //安全库存
        Frm_SaveStock frm_SaveStock = null;
        //安全库存->手动设置
        Frm_SaftyStockSet frm_SaftyStockSet = null;
        //安全库存->系统计算
        Frm_SaftyStockComp frm_SaftyStockComp = null;
        //安全库存->输入需求量
        Frm_InputDemand frm_InputDemand = null;
        //安全库存->查看安全库存
        Frm_ShowSaftyStock frm_showSaftyStock = null;
        //安全库存->更新安全库存
        Frm_UpdateSaftyStock frm_updateSaftyStock = null;
        //安全库存->计算安全库存
       // Frm_SaftyInventory frm_SaftyInventory = null; 


        //到期提醒
        Frm_ExpirationReminder frm_ExpirationReminder = null;
        //到期提醒->设置提前天数
        Frm_Remind frm_Remind = null;
        //到期提醒->处理到期物料
        Frm_RemindHandle frm_RemindHandle = null;

        #endregion
        
        #region 物料主数据
        //新建物料数据
        MTEstablish1 NewMaterialDataForm = null;
        //物料主数据基本视图
        MTMaintain MaterialBasicViewForm = null;
        //物料主数据采购视图
        MTBUYER MaterialBuyerViewForm = null;
        //物料主数据会计视图
        MTACT MaterialAccountViewForm = null;
        //物料主数据存储视图
        存储视图 MaterialStockViewForm = null;
        //建立批次
        BatchFeature NewBatchForm= null;
        //查询批次
        BatchQury QuryBatchForm = null;
        //分类视图
        MaterialClass MaterialClassViewForm = null;
        #endregion

        #region 供应商主数据
        //新建供应商
        NewSupplier NewSupplierForm = null;
        //供应商主数据基本视图
        SPMaintain SupplierBasicViewForm = null;
        //供应商主数据公司代码视图
        SPCPY1 SupplierCompanyCodeViewForm = null;
        //供应商主数据采购组织视图
        SPORG SupplierBuyerOrgViewForm = null;
        //记录查看
        private PurchasesRecordInfoViewForm purchasesRecordInfoViewForm = null;
        //添加记录
        private AddPurRecordInfoForm addPurRecordInfoForm = null;
        //市场价格记录
        private StanMarketPriceForm mMartketPriceForm = null;
        //创建供应商代码
        private CreateCode CreateCodeForm = null;

        #endregion

        #region 财务主数据
        //生成会计凭证
        ManualPosting NewVocherForm = null;
        //查询会计凭证
        ProofInformation QuryVocherForm = null;
        //查询科目信息
        actsummy QuryAccountForm = null;
        #endregion

        #region 一般设置
        //维护策略信息
        StrateInfo strateInfo = new StrateInfo();
        //维护目标值和最低值
        GreenValueAndLowValueForm greenValueAndLowValueForm = null;
        //维护物料组
        Material_Group MaintainMaterialGroupForm = null;
        //维护物料类型
        //维护物料大分类
        MTType MaterialTypeAForm = null;
        MTtypeB MaterialTypeBForm = null;
        MTTypeC MaterialTypeCForm = null;
        //维护货币信息
        Currency MaintainCurrencyForm = null;
        //维护国家信息
        Country MaintainCountryForm = null;
        //维护评估类
        //维护付款条件
        Payment_Clause MaintainPaymentClauseForm = null;
        //维护工厂信息
        Factory MaintainFactoryForm = null;
        //维护计量单位
        Measurement MaintainMeasurementForm = null;
        //维护仓库信息
        Stock MaintainStockForm = null;
        //维护公司
        Company MaintainCompanyForm = null;
        //维护采购组
        BuyerGroup MaintainBuyerGroupForm = null;
        //维护采购组织
        BuyerOrganization MaintainBuyerOrganizationForm = null;
        //维护交付条件
        TradeClause MaintainTradeClauseForm = null;
        //维护评估类
        EvaluationClass MaintainEvaluationClassForm = null;
        //维护产品组
        Division MaintainDivisionForm = null;
        //维护税码
        TaxCode MaintainTaxCodeForm = null;
        //维护成本中心
        CostCenter MaintainCostCenterForm = null;
        //维护工业标识
        IndustryCategory MaintainIndustryCategoryForm = null;
        //给采购组织分配物料组
        MaintainPurOrg maintainPurOrgForm = null;
        //维护评估要素
        MaintainMaterEvalFactor maintainMaterEvalFactor = null;
        //维护证书信息
        CertiTypeForm certiTypeForm = null;
        //维护税率信息
        TaxCode taxesForm = null;
        //维护记录类型
        RecodeTypeForm recodeTypeForm = null;
        //给工厂分配采购组织
        DiviedPurOrgnitionsForm diviedPurOrgnitionsForm = null;
        //维护号码段
        MaintenNum MaintenNumForm = null;
        //维护账户组
        MaintenAg MaintenAgForm = null;
        //
        MTPerformanceModel MTPerformanceModel = null;
        #endregion

        #region 供应商绩效

        //供应商绩效评估
        SupplierPerformanceAutoForm supplierPerformanceAutoForm = null;
        SupplierPerformanceAddForm supplierPerformanceAddForm = null;
        SupplierPerformanceResultDisplay supplierPerformanceResultDisplayForm = null;
        SupplierPerformanceBatchAddForm supplierPerformanceBatchAddForm = null;
        //数据维护
        ComplaintRejectAddForm complaintRejectAddForm = null;
        ServiceScoreAddForm serviceScoreAddForm = null;
        ProductAuditForm productAuditForm = null;
        SystemProcessAuditForm systemProcessAuditForm = null;
        QuestionaireForm questionareForm = null;
        SupplierTurnoverForm supplierTurnoverForm = null;
        //无评估供应商清单显示
        NoSupplierPerformanceList noSPListForm = null;
        //供应商分级
        SupplierClassificationForm supplierClassificationForm = null;
        ClassificationResultForm classificationResultForm = null;
        ResultGroupForm resultGroupForm = null;
        //低效能供应商管理
        LPSWarningForm lpsWarningForm = null;
        LPSProcessForm lpsProcessForm = null;
        LPSOutCheckForm lpsOutCheckForm = null;
        //降成本
        CostReductionForm costReductionForm = null;
        CostReductionResultForm costReductionResultForm = null;
        //供应商价值
        BusinessValueAssessForm businessValueAssessForm = null;
        //供应商区分结果
        supplierClassifyList supplierClassifyList = null;

        //供应商管理策略
        SupplierManageStrategy supplierManageStrategy = null;
        SupplierStateTrackingForm supplierStateTrackingForm = null;
        //外部服务评价
        ExServiceForm exServiceForm = null;
        //一般服务 
        InterForm interForm = null;
        //服务自定义比例
        IntialServiceRate intialServiceRate = null;
        //交货评估
        deliveryForm DeliveryForm = null;
        #endregion

        #region 供应商报表
        /// <summary>
        /// 供应商绩效比较
        /// </summary>
        Frm_SPCompare frm_SPCompare = null;

        /// <summary>
        /// 基于物料组的供应商评估
        /// </summary>
        Frm_SPCompareBOMG frm_SPCompareBOMG = null;

        /// <summary>
        /// 基于行业的供应商评估
        /// </summary>
        Frm_SPCompareBOI frm_SPCompareBOI = null;

        /// <summary>
        /// 供应商对比雷达图
        /// </summary>
        Frm_SPCompareRadarChart frm_SPCompareRadarChart = null;

        /// <summary>
        /// 供应商分析对比图
        /// </summary>
        Frm_SPCompareBubbleChart frm_SPCompareBubbleChart = null;

        #endregion

        #region
        /// <summary>
        /// 供应商清单模块
        /// </summary>
        SupplierListForm supplierListForm = null;
        ExistedSuppAnaForm existedSuppAnaForm = null;
        RepalcedSuppAnaForm repalcedSuppAnaForm = null;
        SupplistPrd SupplistPrd = null;
        //手动维护优先级
        private ManualMaintLevel ManualMaintLevel = null;
        //货源清单
        supplySource supplySource = null;



        #endregion

        #region 供应商供货信息
        /// <summary>
        /// 供应商供货信息
        /// </summary>
        SupplyInfoForm supplyInfoForm = null;
        #endregion

        #region 供应商证书信息
        /// <summary>
        /// 供应商证书信息
        /// </summary>
        CertiFileForm certiFileForm = null;
        #endregion

        #region 供应商认证
        //认证前期准备Form
        private SupplierCertificationForm supplierCertificationForm;
        //初步筛选Form
        private InitialFilterForm initialFilterFrm;
        private SupplierPreselectionForm supplierPreselectionFrm;
        //协议与承诺Form
        private AgreementAndCommitmentsForm agreementAndCommitmentsFrm;
        //供应商能力评估Form
        private SupplierEvaluationForm supplierEvalutationFrm;
        //swot分析Form
        private SwotForm swotFrm;
        //差异识别Form
        private DifferentialIdentificationForm differentialIdentificationFrm;
        //改进/验收Form
        private ImprovementAndCheckForm imporvementAndCheckFrm;
        //供应商准入筛选
        SupplierAccessFilter SupplierAccessFilter = null;
        //供应商预评
        SupplierPreCertificate SupplierPreCertificate = null;
        //评估员预评
        Evaluate Evaluate = null;
        //供应商现场评估
        LocationalEvaluationForm locationalEvaluationForm = null;
        //供应山用户管理
        UserListForm userListForm = null;
        //供应商现场评估——single
        LocalEvaluationSingleForm localEvaluationSingleForm = null;
        //供应商准入
        PassedCertificationForm finshCertificationForm = null;
        //企业用户管理
        SystemUserManagerForm systemUserManagerForm = null;
        //领导决策
        LeaderDecide leaderDecide = null;
        //准入供应商
        Supplier_Permission Supplier_Permission = null;
        //文件上传
        Fupload Fupload = null;
        #endregion

        #region 物料定位
        //竞争力
        Material_group_positioning.AddItemGoals addItemGoals = null;
        //项目目标和指标，废弃
        AddMTGgoals addMTGgoals = null;
        
        //资产性物料组
        AssertMtGroupGoal assertMtGroupGoal = null;
        //维护性物料组
        MaintainMtGroupGoal maintainMtGroupGoal = null;
        //生产性物料组
        PrdMtGroupGoals PrdMtGroupGoals = null;


        //公司运转
        AddGeneralItemGoals AddGeneralItemGoals = null;
        //风险等级
        RiskAssessment riskAssessment = null;
        //等级定位
        N_MT_GroupPosition n_MT_GroupPosition = null;
        //物料群组
        importDataBtn importData = null;
        #endregion

        //_20180408form _20180408form = null;
        //收发邮件
        VolvoMessage1_Form VolvoMessage1_Form = null;

        //创建成本分析询价
        VolvoInquiry0_Form VolvoInquiry0_Form = null;

        //查看询价单
        VolvoInquiry4_Form VolvoInquiry4_Form = null;

        //分配供应商
        VolvoInquiry2_Form VolvoInquiry2_Form = null;

        //报表展示
        VolvoInquiry3_Form VolvoInquiry3_Form = null;

        //横向比较
        VolvoAnalysisGraph_Form VolvoAnalysisGraph_Form = null;

        //纵向比较
        volvoAnalysisGraph_z_form volvoAnalysisGraph_z_form = null;

        //标杆
        VolvoBenchmark volvoBenchmark = null;




        public UserUI()
        {
            InitializeComponent();
            SPReportGlobalVariable.userUI = this;
            InitializeBottomInfo();
            //播放任务查看音段
            string count = sourceSupplierBll.countTaskNumberByUserId(SingleUserInstance.getCurrentUserInstance().User_ID);

            if (!String.IsNullOrEmpty(count) && int.Parse(count) > 0)
            {
                TasksForm newsForm = new TasksForm();

                newsForm.Show();

            }

        }
         //播放任务查看音段
        private void play()
        {
            try
            {
                SoundPlayer sp = new SoundPlayer();
                sp.SoundLocation = @"C:\taskInformRadio.wav";
                sp.Play();
            }
            catch (Exception)
            {
                //MessageBox.Show("播放失败");
            }
        }
        /// <summary>
        /// 加载节点
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void loadSelectedNodes(object sender, EventArgs e)
        {
            LKB_UserName.Text = "当前用户：" + SingleUserInstance.getCurrentUserInstance().Login_Name;
            UserListBLL baseUserBLL = new UserListBLL();
            DataTable dtNodes = baseUserBLL.getAuthorNodes(SingleUserInstance.getCurrentUserInstance().User_ID);
            //找到供应商认证节点 
            TreeNode tempRoot = null;
            foreach (TreeNode node in this.tvFunction.Nodes)
            {
                //将每个根节点代入方法进行查找
                tempRoot = FindNode(node, "供应商认证");
                if(tempRoot !=null)
                {
                    break;
                }
            }
            if (dtNodes.Rows.Count>0)
            {
                for(int i=0;i<dtNodes.Rows.Count;i++)
                {
                    foreach (TreeNode node2 in this.tvFunction.Nodes)
                    {
                        //将每个根节点代入方法进行查找
                        TreeNode newNode = new TreeNode(dtNodes.Rows[i][1].ToString().Trim());
                        newNode.Name = dtNodes.Rows[i][0].ToString();
                        newNode.Text = dtNodes.Rows[i][1].ToString();
                        //添加子节点
                        if (tempRoot != null)
                        {
                            tempRoot.Nodes.Add(newNode);
                            break;
                        }
                    }
                }
            }
        }
        /// <summary>
        /// 递归查询,找到返回该节点
        /// </summary>
        /// <param name="node"></param>
        /// <param name="name"></param>
        /// <returns></returns>
         //递归查询,找到返回该节点
        private TreeNode FindNode(TreeNode node, string name)
        {
            //接受返回的节点
            TreeNode ret = null;
            //循环查找
            foreach (TreeNode temp in node.Nodes)
            {
                //是否有子节点
                if (temp.Nodes.Count != 0)
                {
                    //如果找到
                    if ((ret = FindNode(temp, name)) != null)
                    {
                        return ret;
                    }
                }
                //如果找到
                if (string.Equals(temp.Name, name))
                {
                    return temp;
                }
            }
            return ret;
        }
        /// <summary>
        /// 初始化底部信息
        /// </summary>
        private int InitializeBottomInfo() {
            return 1;
        }
      
        /// <summary>
        /// 鼠标单击
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tvFunction_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            //检查进入窗口的权限
            //if (!validatePermission(e))
            //{
            //    return;
            //}

            string nodeName = e.Node.Name;
            switch (nodeName)
            {              

                #region 采购计划管理窗体选择

                //采购计划管理窗体
                case "showDemand": showDemand(); break;
                //采购计划管理窗体
                case "managerPlansForm": managerPlans(); break;
                //生成采购计划
                case "GenPurPlans": createPurcharsePlans(); break;

                //审核提交（标准）窗体
                case "auditSubmitStandard": auditSubmitStandard(); break;

                //汇总
                case "aggregate_Info": aggregateInfo(); break;

                //寻源
                case "source": source(); break;

                //审核提交（框架协议）窗体
                case "auditSubmitFramework": auditSubmitFramework(); break;

                //生成采购计划（标准）窗体
                case "createPurchasing": createPurchasing(); break;

                //生成采购计划（框架协议）窗体
                case "createFramework": createFramework(); break; 
                //生成质量收货窗体
                case "qulityForm": createQulityForm(); break;
                    //生成评审评估窗体
                case "SecletedEvaluator": createSecletedEvaluatorForm(); break;
                //生成收货窗体
                case "deliveryEval": createDeliveryGoods(); break;
                // 给工厂分配采购组织
                case "DiviedFPurID": createDiviedFPurID(); break;
                #endregion

                #region 采购寻源管理窗体选择

                //源清单
                case "sourceLists":allSourceList(); break;
                //创建源清单窗体
                case "newSourceList": createSourceList(); break;
                //维护源清单窗体
                case "maintenanceSourceList": editSourceList(); break;
                //寻源管理仓
                case "InqueryBin": inqueryBin();break;
                //审批
                case "ApproveInvitation":approveInvitation();break;
                //创建执行货源窗体
                case "createSource": selectSourceMethod(); break;
                //查看询价
                case "viewInquirys": allInquirylist(); break;
                //创建询价
                case "newInquiry": newInquiry(); break;
                //更改询价
                case "editInquiry": editInquiry(); break;
                //查看报价
                case "offerInfo": offerInfo(); break;
                //执行询价
                case "executeInqueryPrice": executeInqueryPrice(); break;

                //查看比价窗体
                case "compareInfo": compareInfo(); break;
                
                //维护报价
                case "maintenanceOfferPrice": maintenanceOfferPrice(); break;

                //执行比价窗体
                case "executeCompare": executeCompare(); break;

                //查看历史招标
                case "showBids": showBids(); break;

                //创建招标窗体
                case "newBidding": newBidding(); break;

                case "releaseBid": releaseBid(); break;

                //更改招标窗体
                case "changeTender": editTender(); break;

                //处理投标窗体
                case "offerBidding": offerBidding(); break;
                //进行评标窗体
                case "compareBidding": compareBidding(); break;

                //审批
                case "checkBid": checkBid(); break;

                //谈判列表
                case "transactList": transactList(); break;

                //创建谈判
                case "createTransact": createTransact(); break;

                //处理谈判
                case "dealTrans": dealTrans(); break;

                //竞价窗体
                case "newAuction": newAuction(); break;
                //竞拍窗体
                case "offerAuction": offerAuction(); break;

                //普通招标法窗体
                case "generalMethod": createSourceList(); break;

                //寻源信息维护窗体
                case "dfsdadsdfsg": sourcingRecord(); break;
                //寻源信息维护窗体
                case "allRecords": allRecords(); break;


                case "newBOM": newBOM(); break;

                case "reviewBOM": reviewBOM(); break;
                // case "": reviewBOM(); break;
                #endregion

                #region 报表分析模块选择窗口
                #region 采购监控分析
                case "MaterialABCAnalysis": MaterialABCAnalysis(); break;
                case "SupplierABCAnalysis": SupplierABCAnalysis(); break;
                #endregion

                #region 采购花费分析
                case "PurchaseConstDistribution": PurchaseConstDistribution(); break;
                case "CostStructureAnalysis": CostStructureAnalysis(); break;
                case "CostComparativeAnalysis": CostComparativeAnalysis(); break;
                case "CostTrenAnalysis": CostTrenAnalysis(); break;   
                #endregion

                #region 寻源分析
                case "MaterialPriceAnalysis": MaterialPriceAnalysis(); break;
                case "SupplierBiddingAnalysis": SupplierBiddingAnalysis(); break;
                #endregion

                #region 合同执行分析
                case "ConstractionManagement": ConstractionManagement(); break;
                #endregion

                #region 供应商绩效分析
                case "SupplierComparativeAnalysis": SupplierComparativeAnalysis(); break;
                case "SupplierTrendAnalysis": SupplierTrendAnalysis(); break;
                case "SupplierScoreRanking": SupplierScoreRanking(); break;
                #endregion

                #region 库存分析
                case "StockAgeAnalysis": StockAgeAnalysis(); break;
                case "StockABCAnalysis": StockABCAnalysis(); break;
                #endregion

                #region 供应商分析
                case "SupplierAnalysis": SupplierAnalysis(); break;
                #endregion

                #region 供应商依赖因素分析
                case "SuppplierDependentFactors": SuppplierDependentFactors(); break;
                #endregion

                #region 定制分析
                case "Customized": Customized(); break;
                #endregion

                #endregion

                #region 库存管理模块选择窗口
                case "node_StockType": mNode_StockType();  //库存类型设置
                    break;
                case "node_StockState": mNode_StockState(); //库存状态设置
                    break;
                
                //货物移动
                //case "MaterialMove": MaterialMove(); break;
                case "Receiving": Receiving(); break;

                //物料凭证
                case "DisplayDocument": DisplayDocument(); break;
                case "ChangeDocument": ChangeDocument(); break;
                case "CancelDocument": CancelDocument(); break;
                case "QueryDocument": QueryDocument(); break;

                //移动类型
                case "FindMoveType": FindMoveType(); break;
                case "TransactionEvent": FindTransEvent(); break;

                //收发存报表
               // case "Report": Report(); break;
                case "StockOverview": StockOverview(); break;
                case "FactoryStock": FactoryStock(); break;
                case "StockStock": StockStock(); break;
                case "ConsignmentStock": ConsignmentStock(); break;
                case "TransitStock": TransitStock(); break;

                
                //安全库存
                //case "SaveStock": SaveStock(); break;
                case "SaftyStockSet": SaftyStockSet(); break;
                case "SaftyStockComp": SaftyStockComp(); break;
                case "InputDemand": InputDemand(); break;
                case "ShowSaftyInventory": ShowSaftyStock(); break;
                case "UpdateSaftyStock": UpdateSaftyStock(); break;
                case "compute": SaftyStockComp(); break;

                //库存盘点
                //case "StockCheck": StockCheck(); break;
                case "createDocument": createDocument(); break;
                case "CreateDocument": CreateDocument(); break;
                case "Freezing": Freezing(); break;
                case "InputResult": InputResult(); break;
                case "ShowDifference": ShowDifference(); break;
                case "CheckPost": CheckPost(); break;
                case "StockList": StockList(); break;
                case "AdjustDifference": AdjustDifference(); break;
                case "ShowDiffLoop": ShowDiffLoop(); break;
                case "CancelAdjust": CancelAdjust(); break;

                
                //收货评分
                //case "ReceivingScore": ReceivingScore(); break;
                case "ReceiveScore": ReceiveScore(); break;
                case "ShipmentScore": ShipmentScore(); break;
                case "NonRawMaterialScore": NonRawMaterialScore(); break;

                //到期提醒
                //case "ExpirationReminder": ExpirationReminder(); break;
                case "Remind": Remind(); break;
                case "RemindHandle": RemindHandle(); break;

                #endregion

                #region 合同库管理模块选择窗口

                case "manageContractTemplateNode": newContractTemplateManageForm(); break;
                case "manageContractTextNode": newContractTextManageForm(); break;

                #endregion

                #region 框架协议模块选择窗口

                //case "newAgreementContractNode": newAgreementContract(); break;
               // case "editAgreementContractNode": editAgreementContract(); break;
                case "manageAgreementContractNode": manageAgreementContract(); break;
               // case "newSchedulingAgreementNode": newSchedulingAgreement(); break;
               // case "manageSchedulingAgreementNode": manageSchedulingAgreement(); break;
                case "quotaArrangementManageNode": manageQuotaArrangement(); break;

                #endregion

                #region 文件管理模块选择窗口

                case "fileManageNode": newFileManageForm(); break;

                #endregion

                #region 订单管理模块选择窗口

                case "POManageNode": newPOManageForm(); break;
                case "InvoiceAndPaymentNode": newInvoiceAndPaymentForm(); break;
                case "OrderCreate": newOrderform();break;

                #endregion 

                #region 自助采购模块选择窗口

                case "SelfServicePurchasingNode": newCatalogManageForm(); break;

                #endregion

                #region 主数据模块选择窗口
                case "NewMaterialData": NewMaterialData(); break;
                case "MaterialBasicView": MaterialBasicView(); break;
                case "MaterialAccountView": MaterialAccountView(); break;
                case "MaterialBuyerView": MaterialBuyerView(); break;
                case "MaterialStockView": MaterialStockView(); break;
                case "NewBatch": NewBatch(); break;
                case "QuryBatch": QuryBatch(); break;
                case "NewSupplier": NewSupplier(); break;
                case "SupplierBasicView": SupplierBasicView(); break;
                case "SupplierBuyerOrgView": SupplierBuyerOrgView(); break;
                case "SupplierCompanyCodeView": SupplierCompanyCodeview(); break;
                case "NewVocher": NewVocher(); break;
                case "CreateCode":newCreateCode();break;
                case "QuryVocher": QuryVocher(); break;
                case "QuryAccount": QuryAccount(); break;
                case "MaterialTypeA": MaterialTypeA(); break;
                case "MaintainPaymentClause": MaintainPaymentClause(); break;
                case "MaterialTypeB": MaterialTypeB(); break;
                case "MaterialTypeC": MaterialTypeC(); break;
                case "MaintainCurrency": MaintainCurrency(); break;
                case "MaintainCountry": MaintainCountry(); break;
                case "MaintainMaterialGroup": MaintainMaterialGroup(); break;
                case "MaintainFactory": MaintainFactory(); break;
                case "MaintainMeasurement": MaintainMeasurement(); break;
                case "MaintainStock": MaintainStock(); break;
                case "MaintainCompanyCode": MaintainCompany(); break;
                case "MaintainBuyerGroup": MaintainBuyerGroup(); break;
                    //采购组织分配物料组
                case "purOrgAllotmtGroup": purOrgAllotmtGroup(); break;
                    //维护采购组织
                case "MaintainBuyerOrganization": MaintainBuyerOrganization(); break;
                //维护评估要素
                case "MaintainMaterEvalFactor": MaintainMaterEvalFactor(); break;

                case "MaintainTradeClause": MaintainTradeClause(); break;
                case "MaintainEvaluationClass": MaintainEvaluationClass(); break;
                case "MaintainDivision": MaintainDivision(); break;
                case "MaintainTaxCode": MaintainTaxCode(); break;
                case "MaterialClassView": MaintainMaterialClass();break;
                case "MaintainCostCenter":MaintainCostCenter();break;
                case "MaintainIndustryCategory": MaintainIndustryCategory();break;
                    //维护证书类型
                case "MainCertiType":newMainCertiType();break;
                case "performanceModel": NewPerformanceModel(); break;

                case "AddPurRecordInfoForm": addPurRecordInfo_1(); break;
                case "PurchasesRecordInfoViewForm": purchasesRecordInfoView(); break;
                case "MMartketPriceForm": MMartketPriceForm(); break;
                case "GoalValueAndLowValueForm": newGoalVlaueAndLowValueForm(); break;
                case "strategyInfoForm": newStrateInfo(); break;
                case "newSupplierCertiForm": newSupplierCertiForm(); break;
                case "newTaxeClassForm": newTaxeClassForm();break;
                case "recordTypeForm": newrecordTypeForm(); break;
                case "MaintenNum": newMaintenNum();break;
                case "MaintenAg":newMaintenAg();break;


                #endregion

                #region 供应商绩效

                //供应商绩效自动评估
                case "SPAutoNode": newSuppliperPerformanceAutoForm(); break;
                //供应商绩效手动维护
                case "SPAddNode": newSuppliperPerformanceAddForm(); break;
                //供应商绩效批量评估
                case "SPBatchAddNode": newSupplierPerformanceBatchAddForm(); break;
                //供应商绩效结果查看
                case "SPResultNode": newSupplierPerformanceResultDisplayForm(); break;
                //抱怨/拒绝数据录入
                case "ComplaintRejectAddNode": newComplaintRejectAddForm(); break;
                //服务相关数据录入
                case "ServiceAddNode": newServiceAddForm(); break;
                //产品审核数据录入
                case "ProductAuditNode": newProductAddForm(); break;
                //体系/过程审核数据录入
                case "SystemProcessAuditNode": newSystemProcessAuditForm(); break;
                //供应商营业额录入
                case "SupplierTurnoverNode": newSupplierTurnoverForm(); break;
                //收货问卷调查
                case "QuestionaireNode": newQuestionaireForm(); break;
                //无评估清单的供应商
                case "SPListNode": newNoSPListForm(); break;
                //供应商分级
                case "SupplierClassifyNode": newSupplierClassificationForm(); break;
                //供应商分级结果查看
                case "ClassificationResultNode": newClassificationResultForm(); break;
                //分组查看分级结果
                case "ResultGroupNode": newResultGroupForm();break;
                //低效能供应商警告
                case "LPSWarningNode": newLPSWarningForm(); break;
                //低效能供应商管理三级流程
                case "LPSProcessNode": newLPSProcessNode(); break;
                //低效能供应商淘汰
                case "LPSOutNode": newLPSOutNode(); break;
                //降成本计算
                case "CostReductionCalculateNode": newCostReductionCalculateNode(); break;
                //降成本结果查询
                case "CostReductionResultNode": newCostReductionResultNode(); break;
                //业务价值评估(供应商细分)
                case "BusinessValueAssessNode": newBusinessValueAssessNode(); break;
                //供应商跟踪
                case "SupplierTrackNode": newSupplierTrackNode(); break;
                //管理策略
                case "SupplierManageStrategy":newSupplierManageStrategy();break;
                //外部服务
                case "ExService": newExServiceForm();break;
                //一般服务
                case "InterForm": newInterForm(); break;
                //服务自定义比例
                case "IntialServiceRate":newIntialServiceRate();break;
                //交货评估页面
                case "deliveryForm":newDiveryForm();break;

                //区分结果清单
                case "supplierClassifyResult": newSupplierClassifyResult();break;

                #endregion

                #region 供应商绩效报表
                //供应商绩效自动评估
                case "SP_Compare": viewSPCompare(); break;
                //基于物料组的供应商评估
                case "SP_CompareBaseOnMaterialGroup": viewSPCompareBaseOnMaterialGroup(); break;
                //基于行业的供应商评估
                case "SP_CompareBaseOnIndustry": viewSPCompareBaseOnIndustry(); break;
                //供应商对比雷达图
                case "SP_CompareRadarChart": viewSPCompareRadarChart(); break;
                //供应商分析气泡图
                case "SP_CompareBubbleChart": viewSPCompareBubbleChart(); break;
                #endregion

                #region 系统管理

                case "sysUserManager": sysUserManageForm(); break;
                case "userGroupManageNode": userGroupManageForm(); break;
                case "userRoleManageNode": userRoleManageForm(); break;
                case "organizationManageNode": organizationManageForm(); break;
                case "backupDataNode": backupDatabaseForm(); break;
                case "restoreDataNode": restoreDatabaseForm(); break;

                #endregion

                #region 供应商认证
                //认证前期准备
                case "certificationPreparationNode": newSupplierCertificationForm(); break;
                //初步筛选
                case "initialFilterNode": newSupplierPreselectionFrm(); break;
                //协议与承诺
                case "agreementAndCommitmentsNode": newAgreementAndCommitmentsFrm(); break;
                //供应商能力评估
                case "supplierEvaluationNode": newSupplierEvaluationFrm(); break;
                //SWOT分析
                case "SWOTNode": newSwotFrm(); break;
                //差异识别与选择措施
                case "differentialIdentificationNode": newDifferentialIdentificationFrm(); break;
                //改进/验收
                case "imporvementAndCheckNode": newImporvementAndCheckFrm(); break;
                //供应商准入筛选
                case "selectSupplier": newSupplierAccessFilter(); break;
                //供应商预评
                case "PerMainEval": newSupplierPreCertificate(); break;
                //评估员预评
                case "PerEval": newEvaluate(); break;
                //供应商现场评估
                case "LocationEvaluateNode":newLocationalEvaluationForm(); break;
                //供应商现场评估_single
                case "localEvaluationSingleNode": newLocalEvaluationSingleForm(); break;
                //供应商用户权限管理
                case "userManageNode": newUserListForm();break;
                //供应商准入
                case "certificationFinished":newCertificationFinishedForm();break;
                case "Decison": newLeaderDecide(); break;
                //准入供应商查看
                case "passedSupplier": newSupplierPermission(); break;
                //改善计划查看
                case "ImprovementReport":newImprovementReportForm();break;
                //文件上传
               // case "Fupload": newFupload(); break;
                //文件上传
               // case "测试": newTest(); break;
                #endregion

                #region 物料定位
                //核心竞争力
                case "AddItemGoals": newAddItemGoalsForm(); break;
                //供应目标和指标
                case "AddMTGgoals": newAddMTGgoalsForm(); break;
                #region 供应目标设定
                //生产性物料组
                case "prdGoalAndAimForm":newPrdGoalAndAimForm();break;
                //维护性物料组
                case "maintainMtGpForm": newMaintainMtGpForm(); break;

                //资产性物料组
                case "assetMtGroupForm": newAssetMtGroupForm(); break;

                #endregion
                //公司运转
                case "AddGeneralItemGoals": newAddGeneralItemGoalsForm(); break;
                //风险等级
                case "RiskAssessment":  newRiskAssessmentForm(); break;
                //等级定位
                case "importData":newImportData();break;
                #endregion


                #region 供应商清单
                case "SupplierListForm":newSupplierListForm();break;
                case "newSupplistPrdForm": newSuppListPrd(); break;
                case "ManualMaintLevelForm": newManualMaintLevelForm(); break;
                //供货清单
                case "supplySourceForm":newsupplySourceForm();break;
                #endregion

                #region 供应商供货信息
                case "supplyInfoShowForm": newSupplyInfoListForm(); break;
                #endregion


                //case "_20180408form": creat_20180408form(); break;
                //收发邮件
                case "message": creat_VolvoMessage1_Form(); break;

                //创建成本分析询价
                case "create": creat_VolvoInquiry0_Form(); break;

                //查看询价单
                case "look": creat_VolvoInquiry4_Form(); break;

                //分配供应商
                case "distribution": creat_VolvoInquiry2_Form(); break;

                //报表展示
                case "compare": creat_VolvoInquiry3_Form(); break;

                //横向比较
                case "horizontal": creat_VolvoAnalysisGraph_Form(); break;

                //纵向比较
                case "vertical": creat_volvoAnalysisGraph_z_form(); break;

                //
                case "benchmarkData": creat_volvoBenchmark(); break;

                default: break;
            }
        }

        private void NewPerformanceModel()
        {
            if (this.MTPerformanceModel == null || this.MTPerformanceModel.IsDisposed)
            {
                this.MTPerformanceModel = new MTPerformanceModel();
            }
            this.MTPerformanceModel.TopLevel = false;
            this.MTPerformanceModel.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MTPerformanceModel.Show(this.dockPnlForm, DockState.Document);
        }

        private void creat_volvoBenchmark()
        {
            if (this.volvoBenchmark == null || this.volvoBenchmark.IsDisposed)
            {
                this.volvoBenchmark = new VolvoBenchmark();
            }
            this.volvoBenchmark.TopLevel = false;
            this.volvoBenchmark.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;

            this.volvoBenchmark.Show(this.dockPnlForm, DockState.Document);

        }

        private void creat_volvoAnalysisGraph_z_form()
        {
            //纵向比较
            if (this.volvoAnalysisGraph_z_form == null || this.volvoAnalysisGraph_z_form.IsDisposed)
            {
                this.volvoAnalysisGraph_z_form = new volvoAnalysisGraph_z_form();
            }
            this.volvoAnalysisGraph_z_form.TopLevel = false;
            this.volvoAnalysisGraph_z_form.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.volvoAnalysisGraph_z_form.Show(this.dockPnlForm, DockState.Document);
        }

        private void creat_VolvoAnalysisGraph_Form()
        {
            //横向比较
            if (this.VolvoAnalysisGraph_Form == null || this.VolvoAnalysisGraph_Form.IsDisposed)
            {
                this.VolvoAnalysisGraph_Form = new VolvoAnalysisGraph_Form();
            }
            this.VolvoAnalysisGraph_Form.TopLevel = false;
            this.VolvoAnalysisGraph_Form.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.VolvoAnalysisGraph_Form.Show(this.dockPnlForm, DockState.Document);
        }


        private void creat_VolvoInquiry3_Form()
        {
            //报表展示
            if (this.VolvoInquiry3_Form == null || this.VolvoInquiry3_Form.IsDisposed)
            {
                this.VolvoInquiry3_Form = new VolvoInquiry3_Form();
            }
            this.VolvoInquiry3_Form.TopLevel = false;
            this.VolvoInquiry3_Form.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.VolvoInquiry3_Form.Show(this.dockPnlForm, DockState.Document);
        }

        private void creat_VolvoMessage1_Form()
        {
            //收发邮件
            if (this.VolvoMessage1_Form == null || this.VolvoMessage1_Form.IsDisposed)
            {
                this.VolvoMessage1_Form = new VolvoMessage1_Form();
            }
            this.VolvoMessage1_Form.TopLevel = false;
            this.VolvoMessage1_Form.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.VolvoMessage1_Form.Show(this.dockPnlForm, DockState.Document);
        }


        private void creat_VolvoInquiry0_Form()
        {
            //创建成本分析询价
            if (this.VolvoInquiry0_Form == null || this.VolvoInquiry0_Form.IsDisposed)
            {
                this.VolvoInquiry0_Form = new VolvoInquiry0_Form(this);

            }
            this.VolvoInquiry0_Form.TopLevel = false;
            this.VolvoInquiry0_Form.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.VolvoInquiry0_Form.Show(this.dockPnlForm, DockState.Document);
        }

        private void creat_VolvoInquiry4_Form()
        {
            //查看询价单
            if (this.VolvoInquiry4_Form == null || this.VolvoInquiry4_Form.IsDisposed)
            {
                this.VolvoInquiry4_Form = new VolvoInquiry4_Form();
            }
            this.VolvoInquiry4_Form.TopLevel = false;
            this.VolvoInquiry4_Form.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.VolvoInquiry4_Form.Show(this.dockPnlForm, DockState.Document);
        }

        private void creat_VolvoInquiry2_Form()
        {
            //分配供应商
            if (this.VolvoInquiry2_Form == null || this.VolvoInquiry2_Form.IsDisposed)
            {
                this.VolvoInquiry2_Form = new VolvoInquiry2_Form();
            }
            this.VolvoInquiry2_Form.TopLevel = false;
            this.VolvoInquiry2_Form.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.VolvoInquiry2_Form.Show(this.dockPnlForm, DockState.Document);
        }
        private void createDiviedFPurID()
        {
            if (this.diviedPurOrgnitionsForm == null || this.diviedPurOrgnitionsForm.IsDisposed)
            {
                this.diviedPurOrgnitionsForm = new DiviedPurOrgnitionsForm();
            }
            this.diviedPurOrgnitionsForm.TopLevel = false;
            this.diviedPurOrgnitionsForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.diviedPurOrgnitionsForm.Show(this.dockPnlForm, DockState.Document);
        }


        private void createDeliveryGoods()
        {
            if (this.deliveryGoods == null || this.deliveryGoods.IsDisposed)
            {
                this.deliveryGoods = new DeliveryEvalForm();
            }
            this.deliveryGoods.TopLevel = false;
            this.deliveryGoods.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.deliveryGoods.Show(this.dockPnlForm, DockState.Document);
        }


        private void createSecletedEvaluatorForm()
        {
            if (this.secletedEvaluatorForm == null || this.secletedEvaluatorForm.IsDisposed)
            {
                this.secletedEvaluatorForm = new SecletedEvaluatorForm();
            }
            this.secletedEvaluatorForm.TopLevel = false;
            this.secletedEvaluatorForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.secletedEvaluatorForm.Show(this.dockPnlForm, DockState.Document);
        }


        private void createQulityForm()
        {
            if (this.qulity_Form == null || this.qulity_Form.IsDisposed)
            {
                this.qulity_Form = new qulityForm();
            }
            this.qulity_Form.TopLevel = false;
            this.qulity_Form.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.qulity_Form.Show(this.dockPnlForm, DockState.Document);
        }


        private void newManualMaintLevelForm()
	    {
	        if (this.ManualMaintLevel == null || this.ManualMaintLevel.IsDisposed)
	        {
                        this.ManualMaintLevel = new ManualMaintLevel();
            }
	        this.ManualMaintLevel.TopLevel = false;
	        this.ManualMaintLevel.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
	        this.ManualMaintLevel.Show(this.dockPnlForm, DockState.Document);
	    }

        private void createPurcharsePlans()
        {
            if (this.createPurcharsePlansForm == null || this.createPurcharsePlansForm.IsDisposed)
            {
                this.createPurcharsePlansForm = new CreatePurcharsePlansForm();
            }
            this.createPurcharsePlansForm.TopLevel = false;
            this.createPurcharsePlansForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.createPurcharsePlansForm.Show(this.dockPnlForm, DockState.Document);
        }

        /// <summary>
        /// 货源清单
        /// </summary>
        private void newsupplySourceForm()
        {
            if (this.supplySource == null || this.supplySource.IsDisposed)
            {
                this.supplySource = new supplySource();
            }
            this.supplySource.TopLevel = false;
            this.supplySource.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.supplySource.Show(this.dockPnlForm, DockState.Document);
        }

        private void newSupplierClassifyResult()
        {

            if (this.supplierClassifyList == null || this.supplierClassifyList.IsDisposed)
            {
                this.supplierClassifyList = new supplierClassifyList();
            }
            this.supplierClassifyList.TopLevel = false;
            this.supplierClassifyList.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.supplierClassifyList.Show(this.dockPnlForm, DockState.Document);
        }

        private void newSuppListPrd()
        {
            if (this.SupplistPrd == null || this.SupplistPrd.IsDisposed)
            {
                this.SupplistPrd = new SupplistPrd();
            }
            this.SupplistPrd.TopLevel = false;
            this.SupplistPrd.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.SupplistPrd.Show(this.dockPnlForm, DockState.Document);
        }

        private void newTaxeClassForm()
        {
            if (this.taxesForm == null || this.taxesForm.IsDisposed)
            {
                this.taxesForm = new TaxCode();
            }
            this.taxesForm.TopLevel = false;
            this.taxesForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.taxesForm.Show(this.dockPnlForm, DockState.Document);
        }
        private void newrecordTypeForm()
        {
            if (this.recodeTypeForm == null || this.recodeTypeForm.IsDisposed)
            {
                this.recodeTypeForm = new RecodeTypeForm();
            }
            this.recodeTypeForm.TopLevel = false;
            this.recodeTypeForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.recodeTypeForm.Show(this.dockPnlForm, DockState.Document);
        }
        //维护号码段
        private void newMaintenNum() {
            if (this.MaintenNumForm == null || this.MaintenNumForm.IsDisposed)
            {
                this.MaintenNumForm = new MaintenNum();
            }
            this.MaintenNumForm.TopLevel = false;
            this.MaintenNumForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaintenNumForm.Show(this.dockPnlForm, DockState.Document);

        }
        //维护账户组
        private void newMaintenAg() {
            if (this.MaintenAgForm == null || this.MaintenAgForm.IsDisposed)
            {
                this.MaintenAgForm = new MaintenAg();
            }
            this.MaintenAgForm.TopLevel = false;
            this.MaintenAgForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaintenAgForm.Show(this.dockPnlForm, DockState.Document);
        }
        //创建供应商编码
        private void newCreateCode() {
            if (this.CreateCodeForm == null || this.CreateCodeForm.IsDisposed)
            {
                this.CreateCodeForm = new CreateCode();
            }
            this.CreateCodeForm.TopLevel = false;
            this.CreateCodeForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.CreateCodeForm.Show(this.dockPnlForm, DockState.Document);
        }

        #region 供应目标设置
        //资本性物料

        private void newAssetMtGroupForm()
        {
            if (this.assertMtGroupGoal == null || this.assertMtGroupGoal.IsDisposed)
            {
                this.assertMtGroupGoal = new AssertMtGroupGoal();
            }
            this.assertMtGroupGoal.TopLevel = false;
            this.assertMtGroupGoal.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.assertMtGroupGoal.Show(this.dockPnlForm, DockState.Document);
        }
        //维护性物料
        private void newMaintainMtGpForm()
        {
            if (this.maintainMtGroupGoal == null || this.maintainMtGroupGoal.IsDisposed)
            {
                this.maintainMtGroupGoal = new MaintainMtGroupGoal();
            }
            this.maintainMtGroupGoal.TopLevel = false;
            this.maintainMtGroupGoal.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.maintainMtGroupGoal.Show(this.dockPnlForm, DockState.Document);
        }
        //生产性物料
        private void newPrdGoalAndAimForm()
        {
            if (this.PrdMtGroupGoals == null || this.PrdMtGroupGoals.IsDisposed)
            {
                this.PrdMtGroupGoals = new PrdMtGroupGoals();
            }
            this.PrdMtGroupGoals.TopLevel = false;
            this.PrdMtGroupGoals.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.PrdMtGroupGoals.Show(this.dockPnlForm, DockState.Document);
        }
        #endregion
        


        private void newMainCertiType()
        {
            if (this.certiTypeForm == null || this.certiTypeForm.IsDisposed)
            {
                this.certiTypeForm = new CertiTypeForm();
            }
            this.certiTypeForm.TopLevel = false;
            this.certiTypeForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.certiTypeForm.Show(this.dockPnlForm, DockState.Document);
        }

        //供应商证书信息
        private void newSupplierCertiForm()
        {
            if (this.certiFileForm == null || this.certiFileForm.IsDisposed)
            {
                this.certiFileForm = new CertiFileForm();
            }
            this.certiFileForm.TopLevel = false;
            this.certiFileForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.certiFileForm.Show(this.dockPnlForm, DockState.Document);
        }

        private void newSupplyInfoListForm()
        {
            if (this.supplyInfoForm == null || this.supplyInfoForm.IsDisposed)
            {
                this.supplyInfoForm = new SupplyInfoForm();
            }
            this.supplyInfoForm.TopLevel = false;
            this.supplyInfoForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.supplyInfoForm.Show(this.dockPnlForm, DockState.Document);
        }

        /// <summary>
        /// 供应商清单
        /// </summary>
        private void newSupplierListForm()
        {

            if (this.supplierListForm == null || this.supplierListForm.IsDisposed)
            {
                this.supplierListForm = new SupplierListForm();
            }
            this.supplierListForm.TopLevel = false;
            this.supplierListForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.supplierListForm.Show(this.dockPnlForm, DockState.Document);
        }

        private void newSupplierManageStrategy()
        {
            if (this.supplierManageStrategy == null || this.supplierManageStrategy.IsDisposed)
            {
                this.supplierManageStrategy = new SupplierManageStrategy();
            }
            this.supplierManageStrategy.TopLevel = false;
            this.supplierManageStrategy.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.supplierManageStrategy.Show(this.dockPnlForm, DockState.Document);
        }

        private void newDiveryForm()
        {
            if (this.DeliveryForm == null || this.DeliveryForm.IsDisposed)
            {
                this.DeliveryForm = new deliveryForm();
            }
            this.DeliveryForm.TopLevel = false;
            this.DeliveryForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.DeliveryForm.Show(this.dockPnlForm, DockState.Document);
        }

        /// <summary>
        /// 自定义服务占比
        /// </summary>
        private void newIntialServiceRate()
        {
            if (this.intialServiceRate == null || this.intialServiceRate.IsDisposed)
            {
                this.intialServiceRate = new IntialServiceRate();
            }
            this.intialServiceRate.TopLevel = false;
            this.intialServiceRate.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.intialServiceRate.Show(this.dockPnlForm, DockState.Document);
        }

        private void MMartketPriceForm()
        {
            if (this.mMartketPriceForm == null || this.mMartketPriceForm.IsDisposed)
            {
                this.mMartketPriceForm = new StanMarketPriceForm();
            }
            this.mMartketPriceForm.TopLevel = false;
            this.mMartketPriceForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.mMartketPriceForm.Show(this.dockPnlForm, DockState.Document);
        }

        //一般服务
        private void newInterForm()
        {
            if (this.interForm == null || this.interForm.IsDisposed)
            {
                this.interForm = new InterForm();
            }
            this.interForm.TopLevel = false;
            this.interForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.interForm.Show(this.dockPnlForm, DockState.Document);
        }

        private void newExServiceForm()
        {
            if (this.exServiceForm == null || this.exServiceForm.IsDisposed)
            {
                this.exServiceForm = new ExServiceForm();
            }
            this.exServiceForm.TopLevel = false;
            this.exServiceForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.exServiceForm.Show(this.dockPnlForm, DockState.Document);
        }

        private void newAddGeneralItemGoalsForm()
        {
            if (this.AddGeneralItemGoals == null || this.AddGeneralItemGoals.IsDisposed)
            {
                this.AddGeneralItemGoals = new AddGeneralItemGoals();
            }
            this.AddGeneralItemGoals.TopLevel = false;
            this.AddGeneralItemGoals.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.AddGeneralItemGoals.Show(this.dockPnlForm, DockState.Document);
        }


        private void newTest()
        {
            if (this.savsAs == null || this.savsAs.IsDisposed)
            {
                this.savsAs = new saveAsForm();
            }
            this.savsAs.TopLevel = false;
            this.savsAs.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.savsAs.Show(this.dockPnlForm, DockState.Document);
        }

        private void newImprovementReportForm()
        {
            if (this.submitImprovementReport == null || this.submitImprovementReport.IsDisposed)
            {
                this.submitImprovementReport = new SubmitImprovementReportForm();
            }
            this.submitImprovementReport.TopLevel = false;
            this.submitImprovementReport.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.submitImprovementReport.Show(this.dockPnlForm, DockState.Document);
        }

        private void sysUserManageForm()
        {
            if (this.systemUserManagerForm == null || this.systemUserManagerForm.IsDisposed)
            {
                this.systemUserManagerForm = new SystemUserManagerForm();
            }
            this.systemUserManagerForm.TopLevel = false;
            this.systemUserManagerForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.systemUserManagerForm.Show(this.dockPnlForm, DockState.Document);
        }

        private void transactList()
        {
            if (this.showTransact_Form == null || this.showTransact_Form.IsDisposed)
            {
                this.showTransact_Form = new ShowTransact_Form();
            }
            this.showTransact_Form.TopLevel = false;
            this.showTransact_Form.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.showTransact_Form.Show(this.dockPnlForm, DockState.Document);
        }

        private void dealTrans()
        {
            if (showTransact_Form == null || showTransact_Form.bid == null)
            {
                MessageBox.Show("未选中招标计划，请重新选择");
            }
            else
            {
                SourceSupplier ss = sourceSupplierBll.getSupplierByBidState(showTransact_Form.bid.BidId);
                if (ss == null)
                {
                    MessageBox.Show(showTransact_Form.bid.BidId + "还未创建谈判，请创建谈判");
                }
                else
                {
                    dealTransact_Form = new DealTransact_Form(this, showTransact_Form.bid);
                    SingletonUserUI.addToUserUI(dealTransact_Form);
                }
            }
        }

        private void createTransact()
        {
            if (showTransact_Form == null || showTransact_Form.bid == null)
            {
                MessageBox.Show("未选中待谈判的招标计划，请重新选择");
            }
            else
            {
                if (showTransact_Form.bid.BidState != 4)
                {
                    MessageBox.Show(showTransact_Form.bid.BidId + "无法创建新谈判");
                }
                else
                {
                    transact_Form = new Transact_Form(this, showTransact_Form.bid);
                    SingletonUserUI.addToUserUI(transact_Form);
                }
            }
        }


        #region  采购寻源模块窗体生成

        /// <summary>
        ///加载采购计划管理窗体
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void showDemand()
        {
            if (this.showDemand_Form == null || this.showDemand_Form.IsDisposed)
            {
                this.showDemand_Form = new ShowDemand_Form(this);

            }
            this.showDemand_Form.TopLevel = false;
            this.showDemand_Form.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.showDemand_Form.Show(this.dockPnlForm, DockState.Document);
        }
        

        /// <summary>
        /// 删除
        /// </summary>
        private void delete_Info()
        {
            if (showDemand_Form.summaryDemand == null)
            {
                MessageBox.Show("未选中需求计划，请重新选择！");
                return;
            }
            else
            {
                if (MessageBox.Show("确定删除需求单号" + showDemand_Form.summaryDemand.Demand_ID + "?", "提示", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    Summary_DemandBLL summaryDemandBll = new Summary_DemandBLL();
                    Demand_MaterialBLL demandMaterialBll = new Demand_MaterialBLL();
                    int result = demandMaterialBll.deleteDemandMaterial(showDemand_Form.summaryDemand.Demand_ID, null);
                    int deresult = summaryDemandBll.deleteSummaryDemandById(showDemand_Form.summaryDemand.Demand_ID);
                    if (deresult > 0)
                    {
                        MessageBox.Show("删除成功");
                        showDemand_Form.flushMaterialGridView();
                    }
                    else
                    {
                        MessageBox.Show("删除失败");
                    }
                }
            }

        }

        /// <summary>
        ///新建需求计划（框架协议）窗体
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void newFramework()
        {
            if (this.newFramework_Form == null || this.newFramework_Form.IsDisposed)
            {
                this.newFramework_Form = new NewFramework_Form();
            }
            this.newFramework_Form.TopLevel = false;
            this.newFramework_Form.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.newFramework_Form.Show(this.dockPnlForm, DockState.Document);
        }

        /// <summary>
        ///新建需求计划（需求预测）窗体
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void newPrePlan()
        {
            if (this.newPrePlan_Form == null || this.newPrePlan_Form.IsDisposed)
            {
                this.newPrePlan_Form = new NewPrePlan_Form();
            }
            this.newPrePlan_Form.TopLevel = false;
            this.newPrePlan_Form.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.newPrePlan_Form.Show(this.dockPnlForm, DockState.Document);
        }

        /// <summary>
        ///  //未加载  明细显示窗体
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void menuDetail()
        {
            if (this.newFramework_Form == null || this.newFramework_Form.IsDisposed)
            {
                this.newFramework_Form = new NewFramework_Form();
            }
            this.newFramework_Form.TopLevel = false;
            this.newFramework_Form.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.newFramework_Form.Show(this.dockPnlForm, DockState.Document);
        }

        /// <summary>
        ///需求预测窗体
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void demandForecast()
        {
            if (this.demandForecast_Form == null || this.demandForecast_Form.IsDisposed)
            {
                this.demandForecast_Form = new DemandForecast_Form(this);
            }
            this.demandForecast_Form.TopLevel = false;
            this.demandForecast_Form.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.demandForecast_Form.Show(this.dockPnlForm, DockState.Document);
        }

        /// <summary>
        ///审核提交（标准）窗体
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void auditSubmitStandard()
        {
            if (this.reviewSummaryDemand_Form == null || this.reviewSummaryDemand_Form.IsDisposed)
            {
                this.reviewSummaryDemand_Form = new ReviewSummaryDemand();
            }
            this.reviewSummaryDemand_Form.TopLevel = false;
            this.reviewSummaryDemand_Form.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.reviewSummaryDemand_Form.Show(this.dockPnlForm, DockState.Document);
           
        }

        /// <summary>
        /// 汇总
        /// </summary>
        private void aggregateInfo()
        {
            if(aggregateMaterial_Form==null || aggregateMaterial_Form.IsDisposed)
            {
                aggregateMaterial_Form  = new AggregateMaterial();
            }
            this.aggregateMaterial_Form.TopLevel = false;
            this.aggregateMaterial_Form.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.aggregateMaterial_Form.Show(this.dockPnlForm, DockState.Document);
        }

        /// <summary>
        /// 指定需求计划创建寻源（分散采购）
        /// </summary>
        private void source()
        {
            if (showDemand_Form.summaryDemand == null)
            {
                MessageBox.Show("未选中需求计划，请重新选择！");
                return;
            }
            if (showDemand_Form.summaryDemand.State.Equals("审核通过"))
            {
                if (this.summary_Demand_Search == null || this.summary_Demand_Search.IsDisposed)
                {
                    this.summary_Demand_Search = new Summary_Demand_Search(showDemand_Form.summaryDemand);
                }
                this.summary_Demand_Search.TopLevel = false;
                this.summary_Demand_Search.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                this.summary_Demand_Search.Show(this.dockPnlForm, DockState.Document);
            }
            else
            {
                MessageBox.Show("无法创建寻源");
            }
        }

        /// <summary>
        ///审核提交（框架协议）窗体
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void auditSubmitFramework()
        {
            if (this.auditSubmitFramework_Form == null || this.auditSubmitFramework_Form.IsDisposed)
            {
                this.auditSubmitFramework_Form = new AuditSubmitFramework_Form();
            }
            this.auditSubmitFramework_Form.TopLevel = false;
            this.auditSubmitFramework_Form.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.auditSubmitFramework_Form.Show(this.dockPnlForm, DockState.Document);
        }

        /// <summary>
        ///生成采购计划（标准）窗体
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void createPurchasing()
        {
            CreatePurchasing_Form createPurchasing_Form = new CreatePurchasing_Form(this);

            //传递参数至计划订单模块
            String item1 = null;
            String item2 = null;
            String item3 = null;
            if (showDemand_Form.checkDt == null || showDemand_Form.checkDt.Rows.Count == 0 || showDemand_Form.j == 0)
            {
                createPurchasing_Form.addDynamicDrawTable();
                SingletonUserUI.addToUserUI(createPurchasing_Form);
            }
            else
            {
                if (showDemand_Form.checkDt.Rows[0][13].ToString().Equals("审核未通过"))
                {
                    MessageBox.Show("采购计划审核未通过!", "无法生成");
                    return;
                }
                if (showDemand_Form.checkDt.Rows[0][13].ToString().Equals("待审核"))
                {
                    MessageBox.Show("采购计划未审核，请先审核!", "无法生成");
                    return;
                }
                if (showDemand_Form.checkDt.Rows[0][13].ToString().Equals("确定采购价格"))
                {
                    if (MessageBox.Show("该采购计划价格已经生成，是否重新生成？", "提示", MessageBoxButtons.YesNo) == DialogResult.No)
                    {
                        return;
                    }
                }
                //传递参数至计划订单模块
                item1 = showDemand_Form.checkDt.Rows[0][3].ToString().Trim();
                item2 = showDemand_Form.checkDt.Rows[0][0].ToString().Trim();
                item3 = showDemand_Form.checkDt.Rows[0][1].ToString().Trim();
                item1 = " Purchase_Type = '" + item1 + "'";
                item2 = " Demand_ID = '" + item2 + "'";
                item3 = " Material_ID = '" + item3 + "'";
                createPurchasing_Form.DynamicDrawTable(item1, item2, item3, true);
                SingletonUserUI.addToUserUI(createPurchasing_Form);
            }
        }

        /// <summary>
        ///生成采购计划（框架协议）窗体
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void createFramework()
        {
            if (this.createFramework_Form == null || this.createFramework_Form.IsDisposed)
            {
                this.createFramework_Form = new CreateFramework_Form();
            }
            this.createFramework_Form.TopLevel = false;
            this.createFramework_Form.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.createFramework_Form.Show(this.dockPnlForm, DockState.Document);
        }
        #endregion

        #region 寻源管理模块窗口生成

        /// <summary>
        /// 货源清单列表
        /// </summary>
        private void allSourceList()
        {
            if (this.sourceList_Form == null || this.sourceList_Form.IsDisposed)
            {
                this.sourceList_Form = new SourceList_Form();
            }
            this.sourceList_Form.TopLevel = false;
            this.sourceList_Form.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.sourceList_Form.Show(this.dockPnlForm, DockState.Document);
        }
        /// <summary>
        ///新建源清单窗体
        /// </summary>
        private void createSourceList()
        {
            if (this.newSourceList_Form == null || this.newSourceList_Form.IsDisposed)
            {
                this.newSourceList_Form = new NewSourceList_Form();
            }
            this.newSourceList_Form.TopLevel = false;
            this.newSourceList_Form.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.newSourceList_Form.Show(this.dockPnlForm, DockState.Document);
        }

        /// <summary>
        ///维护源清单窗体
        /// </summary>
        private void editSourceList()
        {
            if (this.editSourceList_Form == null || this.editSourceList_Form.IsDisposed)
            {
                this.editSourceList_Form = new EditSourceList_Form(sourceList_Form.oneSourceList);
            }
            this.editSourceList_Form.TopLevel = false;
            this.editSourceList_Form.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.editSourceList_Form.Show(this.dockPnlForm, DockState.Document);
        }

        /// <summary>
        /// 寻源管理仓
        /// </summary>
        private void inqueryBin()
        {
            
            if (this.inqueryBin_From == null || this.inqueryBin_From.IsDisposed)
            {
                this.inqueryBin_From = new InqueryBin_From();
            }
            this.inqueryBin_From.TopLevel = false;
            this.inqueryBin_From.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.inqueryBin_From.Show(this.dockPnlForm, DockState.Document);

        }

        /// <summary>
        /// 审批邀请函
        /// </summary>
        private void approveInvitation()
        {
            if (this.approveInvivationFrom == null || this.approveInvivationFrom.IsDisposed)
            {
                this.approveInvivationFrom = new ApproveInvivationFrom();
            }
            this.approveInvivationFrom.TopLevel = false;
            this.approveInvivationFrom.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.approveInvivationFrom.Show(this.dockPnlForm, DockState.Document);
        }

        /// <summary>
        ///加载新建寻源窗体
        /// </summary>
        private void selectSourceMethod()
        {
            if (this.summary_Demand_Search == null || this.summary_Demand_Search.IsDisposed)
            {
                this.summary_Demand_Search = new Summary_Demand_Search();
            }
            this.summary_Demand_Search.TopLevel = false;
            this.summary_Demand_Search.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.summary_Demand_Search.Show(this.dockPnlForm, DockState.Document);
        }



        /// <summary>
        /// 查看询价（询价单列表）
        /// </summary>
        private void allInquirylist()
        {
            if (this.allInquiry == null || this.allInquiry.IsDisposed)
            {
                this.allInquiry = new AllInquiry();
            }
            this.allInquiry.TopLevel = false;
            this.allInquiry.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.allInquiry.Show(this.dockPnlForm, DockState.Document);
        }

        /// <summary>
        ///创建询价窗体
        /// </summary>
        private void newInquiry()
        {
            if (this.inquiry_Form == null || this.inquiry_Form.IsDisposed)
            {
                this.inquiry_Form = new Inquiry_Form();
            }
            this.inquiry_Form.TopLevel = false;
            this.inquiry_Form.FormBorderStyle = FormBorderStyle.None;
            this.inquiry_Form.Show(this.dockPnlForm, DockState.Document);
        }

        /// <summary>
        /// 更改询价
        /// </summary>
        public void editInquiry()
        {
            if (this.editInquery == null || this.editInquery.IsDisposed)
            {
                this.editInquery = new EditInquery();
            }
            this.editInquery.TopLevel = false;
            this.editInquery.FormBorderStyle = FormBorderStyle.None;
            this.editInquery.Show(this.dockPnlForm, DockState.Document);
        }


        /// <summary>
        /// 查看报价
        /// </summary>
        private void offerInfo()
        {

        }

        /// <summary>
        ///执行询价窗体
        /// </summary>
        private void executeInqueryPrice()
        {
            if(this.offer_Form == null || this.offer_Form.IsDisposed)
            {
                this.offer_Form = new Offer_Form();
            }
            this.offer_Form.TopLevel = false;
            this.offer_Form.FormBorderStyle = FormBorderStyle.None;
            this.offer_Form.Show(this.dockPnlForm, DockState.Document);
        }
        

        /// <summary>
        /// 查看比价信息
        /// </summary>
        private void compareInfo()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 维护报价
        /// </summary>
        private void maintenanceOfferPrice()
        {
            if (this.maintenanceOfferPriceForm == null || this.maintenanceOfferPriceForm.IsDisposed)
            {
                this.maintenanceOfferPriceForm = new MaintenanceOfferPriceForm();
            }
            this.maintenanceOfferPriceForm.TopLevel = false;
            this.maintenanceOfferPriceForm.FormBorderStyle = FormBorderStyle.None;
            this.maintenanceOfferPriceForm.Show(this.dockPnlForm, DockState.Document);
        }

        /// <summary>
        /// 执行比价
        /// </summary>
        private void executeCompare()
        {
            this.compare_Form = new Compare_Form();
            this.compare_Form.TopLevel = false;
            this.compare_Form.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.compare_Form.Show(this.dockPnlForm, DockState.Document);
        }

        //查看招标
        private void showBids()
        {
            if (this.showBid_Form == null || this.showBid_Form.IsDisposed)
            {
                this.showBid_Form = new ShowBid_Form();
            }
            this.showBid_Form.TopLevel = false;
            this.showBid_Form.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.showBid_Form.Show(this.dockPnlForm, DockState.Document);
        }

        /// <summary>
        ///创建招标窗体
        /// </summary>
        private void newBidding()
        {
            if (this.bidding_Form == null || this.bidding_Form.IsDisposed)
            {
                this.bidding_Form = new Bidding_Form();
            }
            this.bidding_Form.TopLevel = false;
            this.bidding_Form.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.bidding_Form.Show(this.dockPnlForm, DockState.Document);

        }

        private void releaseBid()
        {
            if (showBid_Form == null || showBid_Form.bid == null)
            {
                MessageBox.Show("未选中招标计划，请重新选择");
            }
            else
            {
                release_Form = new ReleaseBid_Form(this, showBid_Form.bid);
                SingletonUserUI.addToUserUI(release_Form);
            }
        }

        /// <summary>
        /// 更改招标窗体
        /// </summary>
        private void editTender()
        {
            if (changeTender_Form == null || changeTender_Form.IsDisposed)
            {
                changeTender_Form = new ChangeTender_Form();
            }
            changeTender_Form.TopLevel = false;
            changeTender_Form.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            changeTender_Form.Show(dockPnlForm, DockState.Document);
        }
        /// <summary>
        ///处理投标窗体
        /// </summary>
        private void offerBidding()
        {
            if (showBid_Form == null || showBid_Form.bid == null)
            {
                MessageBox.Show("未选中招标计划，请重新选择");
            }
            else
            {
                OfferBid_Form ob_Form = new OfferBid_Form(this, showBid_Form.bid);
                SingletonUserUI.addToUserUI(ob_Form);
            }
        }
        /// <summary>
        ///进行评标窗体
        /// </summary>
        private void compareBidding()
        {
            if (showBid_Form == null || showBid_Form.bid == null)
            {
                MessageBox.Show("未选中招标计划，请重新选择");
            }
            else
            {
                CompareBid_Form ob_Form = new CompareBid_Form(this, showBid_Form.bid);
                SingletonUserUI.addToUserUI(ob_Form);
            }
        }

        //审批招标
        private void checkBid()
        {
            if (this.checkBid_Form == null || this.checkBid_Form.IsDisposed)
            {
                this.checkBid_Form = new CheckBid_Form();
            }
            this.checkBid_Form.TopLevel = false;
            this.checkBid_Form.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.checkBid_Form.Show(this.dockPnlForm, DockState.Document);
        }

        /// <summary>
        ///竞价窗体
        /// </summary>
        private void newAuction()
        {
            if (this.inquiry_Form == null || this.inquiry_Form.IsDisposed)
            {
                this.inquiry_Form = new Inquiry_Form();
            }
            this.inquiry_Form.TopLevel = false;
            this.inquiry_Form.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.inquiry_Form.Show(this.dockPnlForm, DockState.Document);
        }
        /// <summary>
        ///竞拍窗体
        /// </summary>
        private void offerAuction()
        {
            if (this.auction_Form == null || this.auction_Form.IsDisposed)
            {
                this.auction_Form = new Auction_Form();
            }
            this.auction_Form.TopLevel = false;
            this.auction_Form.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.auction_Form.Show(this.dockPnlForm, DockState.Document);
        }



        /// <summary>
        ///加载寻源记录窗体
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void sourcingRecord()
        {
            if (this.sourcingRecord_Form == null || this.sourcingRecord_Form.IsDisposed)
            {
                this.sourcingRecord_Form = new SourcingRecord_Form();
            }
            this.sourcingRecord_Form.TopLevel = false;
            this.sourcingRecord_Form.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.sourcingRecord_Form.Show(this.dockPnlForm, DockState.Document);
        }

        /// <summary>
        ///寻源信息维护窗体
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void allRecords()
        {
            if (this.recordInfo_Form == null || this.recordInfo_Form.IsDisposed)
            {
                this.recordInfo_Form = new ShowRecordInfo_Form();
            }
            this.recordInfo_Form.TopLevel = false;
            this.recordInfo_Form.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.recordInfo_Form.Show(this.dockPnlForm, DockState.Document);
        }

        /// <summary>
        ///新建BOM窗体
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void newBOM()
        {
            if (this.mrpCount == null || this.mrpCount.IsDisposed)
            {
                this.mrpCount = new MRPCount();
            }
            this.mrpCount.TopLevel = false;
            this.mrpCount.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.mrpCount.Show(this.dockPnlForm, DockState.Document);
        }
        /// <summary>
        ///查询BOM窗体
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void reviewBOM()
        {
            if (this.reviewBomForm == null || this.reviewBomForm.IsDisposed)
            {
                this.reviewBomForm = new ReviewBomForm();
            }
            this.reviewBomForm.TopLevel = false;
            this.reviewBomForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.reviewBomForm.Show(this.dockPnlForm, DockState.Document);
        }

        #endregion

        #region 库存管理模块窗口生成

        /// <summary>
        /// 基础设置
        /// 库存类型基本设置
        /// </summary>
        private void mNode_StockType()
        {
            if (frm_StockType == null || frm_StockType.IsDisposed)
            {
                frm_StockType = new Frm_StockType();
            }
            frm_StockType.TopLevel = false;
            frm_StockType.Dock = DockStyle.Fill;
            frm_StockType.Show(this.dockPnlForm, DockState.Document);
        }

        /// <summary>
        /// 基础设置
        /// 库存类型基本设置
        /// </summary>
        private void mNode_StockState()
        {
            if (frm_StockState == null || frm_StockState.IsDisposed)
            {
                frm_StockState = new Frm_StockState();
            }
            frm_StockState.TopLevel = false;
            frm_StockState.Dock = DockStyle.Fill;
            frm_StockState.Show(this.dockPnlForm, DockState.Document);
        }

        /// <summary>
        /// 货物移动
        /// </summary>
        private void MaterialMove()
        {
            if (frm_StockMove == null || frm_StockMove.IsDisposed)
            {
                frm_StockMove = new Frm_StockMove();
            }
            frm_StockMove.TopLevel = false;
            frm_StockMove.Dock = DockStyle.Fill;
            frm_StockMove.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            frm_StockMove.Show(this.dockPnlForm, DockState.Document);
            this.frm_StockMove.Controls.Clear();
            Frm_Receiving frm_Receiving = new Frm_Receiving();
            frm_Receiving.TopLevel = false;
            frm_StockMove.Controls.Add(frm_Receiving);
            //frm_Test.Dock = DockStyle.Fill;
            frm_Receiving.Location = new Point(0, 24);
            frm_Receiving.Show();
        }

        /// <summary>
        /// 物料凭证->更改
        /// </summary>
        private void ChangeDocument()
        {
            if (frm_ChangeDocument == null || frm_ChangeDocument.IsDisposed)
            {
                frm_ChangeDocument = new Frm_ChangeDocument();
            }
            frm_ChangeDocument.TopLevel = false;
            frm_ChangeDocument.Dock = DockStyle.Fill;
            frm_ChangeDocument.Show(this.dockPnlForm, DockState.Document);
        }

        /// <summary>
        /// 物料凭证->显示
        /// </summary>
        private void DisplayDocument()
        {
            if (frm_DisplayDocument == null || frm_DisplayDocument.IsDisposed)
            {
                frm_DisplayDocument = new Frm_DisplayDocument();
            }
            frm_DisplayDocument.TopLevel = false;
            frm_DisplayDocument.Dock = DockStyle.Fill;
            frm_DisplayDocument.Show(this.dockPnlForm, DockState.Document);
        }

        /// <summary>
        /// 物料凭证->取消/冲销
        /// </summary>
        private void CancelDocument()
        {
            if (frm_CancelDocument == null || frm_CancelDocument.IsDisposed)
            {
                frm_CancelDocument = new Frm_CancelDocument(this);
            }
            frm_CancelDocument.TopLevel = false;
            frm_CancelDocument.Dock = DockStyle.Fill;
            frm_CancelDocument.Show(this.dockPnlForm, DockState.Document);
        }

        /// <summary>
        /// 物料凭证->查询
        /// </summary>
        private void QueryDocument()
        {
            if (frm_QueryDocument == null || frm_QueryDocument.IsDisposed)
            {
                frm_QueryDocument = new Frm_QueryDocument();
            }
            frm_QueryDocument.TopLevel = false;
            frm_QueryDocument.Dock = DockStyle.Fill;
            frm_QueryDocument.Show(this.dockPnlForm, DockState.Document);
        }

        /// <summary>
        /// 移动类型->查看
        /// </summary>
        private void FindMoveType()
        {
            if (frm_FindMoveType == null || frm_FindMoveType.IsDisposed)
            {
                frm_FindMoveType = new Frm_FindMoveType();
            }
            frm_FindMoveType.TopLevel = false;
            frm_FindMoveType.Dock = DockStyle.Fill;
            frm_FindMoveType.Show(this.dockPnlForm, DockState.Document);
        }

        /// <summary>
        /// 移动类型->事务/事件
        /// </summary>
        private void FindTransEvent()
        {
            if (frm_TransOrEvent == null || frm_TransOrEvent.IsDisposed)
            {
                frm_TransOrEvent = new Frm_TransOrEvent();
            }
            frm_TransOrEvent.TopLevel = false;
            frm_TransOrEvent.Dock = DockStyle.Fill;
            frm_TransOrEvent.Show(this.dockPnlForm, DockState.Document);
        }

        /// <summary>
        /// 货物移动->收货
        /// </summary>
        private void Receiving()
        {
            if (frm_StockMove == null || frm_StockMove.IsDisposed)
            {
                frm_StockMove = new Frm_StockMove();
            }
            frm_StockMove.TopLevel = false;
            frm_StockMove.Dock = DockStyle.Fill;
            frm_StockMove.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            frm_StockMove.Show(this.dockPnlForm, DockState.Document);
            this.frm_StockMove.Controls.Clear();
            Frm_Receiving frm_Receiving = new Frm_Receiving();
            frm_Receiving.TopLevel = false;
            frm_StockMove.Controls.Add(frm_Receiving);
            frm_Receiving.Location = new Point(0, 24);
            frm_Receiving.Show();
        }

        /// <summary>
        /// 收发存报表
        /// </summary>
        private void Report()
        {
            if (frm_ReceivingReport == null || frm_ReceivingReport.IsDisposed)
            {
                frm_ReceivingReport = new Frm_ReceivingReport();
            }
            frm_ReceivingReport.TopLevel = false;
            frm_ReceivingReport.Dock = DockStyle.Fill;
            frm_ReceivingReport.Show(this.dockPnlForm, DockState.Document);
        }

        /// <summary>
        /// 收发存报表->库存总览
        /// </summary>
        private void StockOverview()
        {
            if (frm_StockOverview == null || frm_StockOverview.IsDisposed)
            {
                frm_StockOverview = new Frm_StockOverview( );
            }
            frm_StockOverview.TopLevel = false;
            frm_StockOverview.Dock = DockStyle.Fill;
            frm_StockOverview.Show(this.dockPnlForm, DockState.Document); 
        }
        /// <summary>
        /// 收发存报表->工厂库存
        /// </summary>
        private void FactoryStock()
        {
            if (frm_FactoryStock == null || frm_FactoryStock.IsDisposed)
            {
                frm_FactoryStock = new Frm_FactoryStock();
            }
            frm_FactoryStock.TopLevel = false;
            frm_FactoryStock.Dock = DockStyle.Fill;
            frm_FactoryStock.Show(this.dockPnlForm, DockState.Document); 
        }
        /// <summary>
        /// 收发存报表->仓库库存
        /// </summary>
        private void StockStock()
        {
            if (frm_StockStock == null || frm_StockStock.IsDisposed)
            {
                frm_StockStock = new Frm_StockStock();
            }
            frm_StockStock.TopLevel = false;
            frm_StockStock.Dock = DockStyle.Fill;
            frm_StockStock.Show(this.dockPnlForm, DockState.Document);
        }
        /// <summary>
        /// 收发存报表->寄售库存
        /// </summary>
        private void ConsignmentStock()
        {
            if (frm_ConnsignmentStock == null || frm_ConnsignmentStock.IsDisposed)
            {
                frm_ConnsignmentStock = new Frm_ConsignmentStock();
            }
            frm_ConnsignmentStock.TopLevel = false;
            frm_ConnsignmentStock.Dock = DockStyle.Fill;
            frm_ConnsignmentStock.Show(this.dockPnlForm, DockState.Document);
        }

        /// <summary>
        /// 收发存报表->在途库存
        /// </summary>
        private void TransitStock()
        {
            if (frm_TransitStock == null || frm_TransitStock.IsDisposed)
            {
                frm_TransitStock = new Frm_TransitStock();
            }
            frm_TransitStock.TopLevel = false;
            frm_TransitStock.Dock = DockStyle.Fill;
            frm_TransitStock.Show(this.dockPnlForm, DockState.Document);
        }


        /// <summary>
        /// 安全库存
        /// </summary>
        private void SaveStock()
        {
            if (frm_SaveStock == null || frm_SaveStock.IsDisposed)
            {
                frm_SaveStock = new Frm_SaveStock();
            }
            frm_SaveStock.TopLevel = false;
            frm_SaveStock.Dock = DockStyle.Fill;
            frm_SaveStock.Show(this.dockPnlForm, DockState.Document);
        }

        /// <summary>
        /// 安全库存->手动设置
        /// </summary>
        private void SaftyStockSet()
        {
           /* //加载下一个窗体前，先清空所有控件
            this.frm_SaveStock.Controls.Clear();
            Frm_SaftyStockSet frm_SaftyStockSet = new Frm_SaftyStockSet();
            //取消其“顶级控件”的特性
            frm_SaftyStockSet.TopLevel = false;
            //将frm_Receiving窗体加载到frm_StockMove窗体中
            frm_SaveStock.Controls.Add(frm_SaftyStockSet);
            frm_SaftyStockSet.Location = new Point(0, 24);
            //在frm_StockMove中显示frm_Receiving窗体
            frm_SaftyStockSet.Show();*/


            if (frm_SaftyStockSet == null || frm_SaftyStockSet.IsDisposed)
            {
                frm_SaftyStockSet = new Frm_SaftyStockSet();
            }
            frm_SaftyStockSet.TopLevel = false;
            frm_SaftyStockSet.Dock = DockStyle.Fill;
            frm_SaftyStockSet.Show(this.dockPnlForm, DockState.Document);

        }


        /// <summary>
        /// 安全库存->系统计算
        /// </summary>
        private void SaftyStockComp()
        {
            if (frm_SaftyStockComp == null || frm_SaftyStockComp.IsDisposed)
            {
                frm_SaftyStockComp = new Frm_SaftyStockComp();
            }
            frm_SaftyStockComp.TopLevel = false;
            frm_SaftyStockComp.Dock = DockStyle.Fill;
            frm_SaftyStockComp.Show(this.dockPnlForm, DockState.Document);
        }

        /// <summary>
        /// 安全库存->录入需求
        /// </summary>
        private void InputDemand()
        {
            if (frm_InputDemand == null || frm_InputDemand.IsDisposed)
            {
                frm_InputDemand = new Frm_InputDemand();
            }
            frm_InputDemand.TopLevel = false;
            frm_InputDemand.Dock = DockStyle.Fill;
            frm_InputDemand.Show(this.dockPnlForm, DockState.Document);
        }

        /// <summary>
        /// 安全库存->查看安全库存
        /// </summary>
        private void ShowSaftyStock()
        {
            if (frm_showSaftyStock == null || frm_showSaftyStock.IsDisposed)
            {
                frm_showSaftyStock = new Frm_ShowSaftyStock();
            }
            frm_showSaftyStock.TopLevel = false;
            frm_showSaftyStock.Dock = DockStyle.Fill;
            frm_showSaftyStock.Show(this.dockPnlForm, DockState.Document);
        }

        /// <summary>
        /// 安全库存->更新安全库存
        /// </summary>
        private void UpdateSaftyStock()
        {
            if (frm_updateSaftyStock == null || frm_updateSaftyStock.IsDisposed)
            {
                frm_updateSaftyStock = new Frm_UpdateSaftyStock();
            }
            frm_updateSaftyStock.TopLevel = false;
            frm_updateSaftyStock.Dock = DockStyle.Fill;
            frm_updateSaftyStock.Show(this.dockPnlForm, DockState.Document);

        }

        /// <summary>
        /// 安全库存->计算
        /// </summary>
//       private void ComputeSaftyStock()
     //   {
     //       if (frm_SaftyInventory == null || frm_SaftyInventory.IsDisposed)
    //        {
     //           frm_SaftyInventory = new Frm_SaftyInventory();
     //       }
     //       frm_SaftyInventory.TopLevel = false;
    //        frm_SaftyInventory.Dock = DockStyle.Fill;
    //        frm_SaftyInventory.Show(this.dockPnlForm, DockState.Document);

   //     }

         /// <summary>
        /// 库存盘点
        /// </summary>
        private void StockCheck()
        {
            if (frm_StockCheck == null || frm_StockCheck.IsDisposed)
            {
                frm_StockCheck = new Frm_StockCheck();
            }
            frm_StockCheck.TopLevel = false;
            frm_StockCheck.Dock = DockStyle.Fill;
            frm_StockCheck.Show(this.dockPnlForm, DockState.Document);
        }


        /// <summary>
        /// 库存盘点->集中创建凭证
        /// </summary>
        private void createDocument()
        {
            if (frm_CreateStockTaking == null || frm_CreateStockTaking.IsDisposed)
            {
                frm_CreateStockTaking = new Frm_CreateStocktaking();
            }
            frm_CreateStockTaking.TopLevel = false;
            frm_CreateStockTaking.Dock = DockStyle.Fill;
            frm_CreateStockTaking.Show(this.dockPnlForm, DockState.Document);

        }

        /// <summary>
        /// 库存盘点->单个创建凭证
        /// </summary>
        private void CreateDocument()
        {
            if (frm_CreateStockTaking1 == null || frm_CreateStockTaking1.IsDisposed)
            {
                frm_CreateStockTaking1 = new Frm_CreateStocktaking1();
            }
            frm_CreateStockTaking1.TopLevel = false;
            frm_CreateStockTaking1.Dock = DockStyle.Fill;
            frm_CreateStockTaking1.Show(this.dockPnlForm, DockState.Document);

        }
        /// <summary>
        /// 库存盘点->冻结与解冻
        /// </summary>
        private void Freezing()
        {
            if (frm_FreezingStock == null || frm_FreezingStock.IsDisposed)
            {
                frm_FreezingStock = new Frm_FreezingStock();
            }
            frm_FreezingStock.TopLevel = false;
            frm_FreezingStock.Dock = DockStyle.Fill;
            frm_FreezingStock.Show(this.dockPnlForm, DockState.Document);

           
        }

        /// <summary>
        /// 库存盘点->输入盘点结果
        /// </summary>
        private void InputResult()
        {
            if (frm_InputResult == null || frm_InputResult.IsDisposed)
            {
                frm_InputResult = new Frm_InputResult();
            }
            frm_InputResult.TopLevel = false;
            frm_InputResult.Dock = DockStyle.Fill;
            frm_InputResult.Show(this.dockPnlForm, DockState.Document);

        }

        /// <summary>
        /// 库存盘点->查看差异
        /// </summary>
        private void ShowDifference()
        {
            if (frm_ShowDifference == null || frm_ShowDifference.IsDisposed)
            {
                frm_ShowDifference = new Frm_ShowDifference();
            }
            frm_ShowDifference.TopLevel = false;
            frm_ShowDifference.Dock = DockStyle.Fill;
            frm_ShowDifference.Show(this.dockPnlForm, DockState.Document);
           
        }


        /// <summary>
        /// 库存盘点->盘点过账
        /// </summary>
        private void CheckPost()
        {
            if (frm_CheckPost == null || frm_CheckPost.IsDisposed)
            {
                frm_CheckPost = new Frm_CheckPost();
            }
            frm_CheckPost.TopLevel = false;
            frm_CheckPost.Dock = DockStyle.Fill;
            frm_CheckPost.Show(this.dockPnlForm, DockState.Document);
        }

           /// <summary>
        /// 库存盘点->输出盘点清单
        /// </summary>
        private void StockList()
        {
            if (frm_StockList == null || frm_StockList.IsDisposed)
            {
                frm_StockList = new Frm_StockList();
            }
            frm_StockList.TopLevel = false;
            frm_StockList.Dock = DockStyle.Fill;
            frm_StockList.Show(this.dockPnlForm, DockState.Document);

            }
        /// <summary>
        /// 库存盘点->差异调整
        /// </summary>
        private void AdjustDifference()
        {
            if (frm_AdjustDifference == null || frm_AdjustDifference.IsDisposed)
            {
                frm_AdjustDifference = new Frm_AdjustDifference();
            }
            frm_AdjustDifference.TopLevel = false;
            frm_AdjustDifference.Dock = DockStyle.Fill;
            frm_AdjustDifference.Show(this.dockPnlForm, DockState.Document);

        }

        /// <summary>
        /// 库存盘点->查看差异调整
        /// </summary>
        private void ShowDiffLoop()
        {

            if (frm_ShowDiffLoop == null || frm_ShowDiffLoop.IsDisposed)
            {
                frm_ShowDiffLoop = new Frm_ShowDiffLoop();
            }
            frm_ShowDiffLoop.TopLevel = false;
            frm_ShowDiffLoop.Dock = DockStyle.Fill;
            frm_ShowDiffLoop.Show(this.dockPnlForm, DockState.Document);
            
        }
        /// <summary>
        /// 库存盘点->取消差异调整
        /// </summary>
        private void CancelAdjust()
        {

            if (frm_CancelAdjust == null || frm_CancelAdjust.IsDisposed)
            {
                frm_CancelAdjust = new Frm_CancelAdjust();
            }
            frm_CancelAdjust.TopLevel = false;
            frm_CancelAdjust.Dock = DockStyle.Fill;
            frm_CancelAdjust.Show(this.dockPnlForm, DockState.Document);
            
        }



        /// <summary>
        /// 收货评分
        /// </summary>
        private void ReceivingScore()
        {
            if (frm_ReceivingScore == null || frm_ReceivingScore.IsDisposed)
            {
                frm_ReceivingScore = new Frm_ReceivingScore();
            }
            frm_ReceivingScore.TopLevel = false;
            frm_ReceivingScore.Dock = DockStyle.Fill;
            frm_ReceivingScore.Show(this.dockPnlForm, DockState.Document);
        }


        /// <summary>
        /// 收货评分->质检评分
        /// </summary>
        private void ReceiveScore()
        {
            if (frm_ReceiveScore == null || frm_ReceiveScore.IsDisposed)
            {
                frm_ReceiveScore = new Frm_ReceiveScore();
            }
            frm_ReceiveScore.TopLevel = false;
            frm_ReceiveScore.Dock = DockStyle.Fill;
            frm_ReceiveScore.Show(this.dockPnlForm, DockState.Document);
           
        }

        /// <summary>
        /// 收货评分->装运评分
        /// </summary>
        private void ShipmentScore()
        {
            if (frm_shipmentScore == null || frm_shipmentScore.IsDisposed)
            {
                frm_shipmentScore = new Frm_ShipmentScore();
            }
            frm_shipmentScore.TopLevel = false;
            frm_shipmentScore.Dock = DockStyle.Fill;
            frm_shipmentScore.Show(this.dockPnlForm, DockState.Document);

        }
            
        /// <summary>
        /// 收货评分->非原材料拒收数量
        /// </summary>
        private void NonRawMaterialScore()
        {
            if (frm_NonRawMaterialScore == null || frm_NonRawMaterialScore.IsDisposed)
            {
                frm_NonRawMaterialScore = new Frm_NonRawMaterialScore();
            }
            frm_NonRawMaterialScore.TopLevel = false;
            frm_NonRawMaterialScore.Dock = DockStyle.Fill;
            frm_NonRawMaterialScore.Show(this.dockPnlForm, DockState.Document);

        }

        /// <summary>
        /// 到期提醒
        /// </summary>
        private void ExpirationReminder()
        {
            if (frm_ExpirationReminder == null || frm_ExpirationReminder.IsDisposed)
            {
                frm_ExpirationReminder = new Frm_ExpirationReminder();
            }
            frm_ExpirationReminder.TopLevel = false;
            frm_ExpirationReminder.Dock = DockStyle.Fill;
            frm_ExpirationReminder.Show(this.dockPnlForm, DockState.Document);
        }

        /// <summary>
        /// 到期提醒->设置提前天数
        /// </summary>
        private void Remind()
        {
            if (frm_Remind == null || frm_Remind.IsDisposed)
            {
                frm_Remind = new Frm_Remind();
            }
            frm_Remind.TopLevel = false;
            frm_Remind.Dock = DockStyle.Fill;
            frm_Remind.Show(this.dockPnlForm, DockState.Document);

        }

        /// <summary>
        /// 到期提->处理到期物料
        /// </summary>
        private void RemindHandle()
        {
            if (frm_RemindHandle == null || frm_RemindHandle.IsDisposed)
            {
                frm_RemindHandle = new Frm_RemindHandle();
            }
            frm_RemindHandle.TopLevel = false;
            frm_RemindHandle.Dock = DockStyle.Fill;
            frm_RemindHandle.Show(this.dockPnlForm, DockState.Document);
          
        }
        #endregion 

        #region 合同库模块窗口生成

        /// <summary>
        /// 生成合同模板
        /// </summary>
        private void newContractTemplateManageForm()
        {
            if (this.contractTemplateManageForm == null || this.contractTemplateManageForm.IsDisposed)
            {
                this.contractTemplateManageForm = new ContractTemplateManageForm();
            }
            this.contractTemplateManageForm.TopLevel = false;
            this.contractTemplateManageForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.contractTemplateManageForm.Show(this.dockPnlForm, DockState.Document);
        }

        /// <summary>
        /// 生成合同文本
        /// </summary>
        private void newContractTextManageForm()
        {
            if (this.contractTextManageForm == null || this.contractTextManageForm.IsDisposed)
            {
                this.contractTextManageForm = new ContractTextManageForm();
            }
            this.contractTextManageForm.TopLevel = false;
            this.contractTextManageForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.contractTextManageForm.Show(this.dockPnlForm, DockState.Document);
        }

        #endregion

        #region 报表分析模块窗口生成

        #region 采购监控分析
        /// <summary>
        /// 物料ABC类分析
        /// </summary>
        private void MaterialABCAnalysis()
        {
            if (mmABC == null || mmABC.IsDisposed)
            {
                mmABC = new Monitor_MaterialABCAnalysis();
            }
            mmABC.TopLevel = false;
            mmABC.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            mmABC.Show(this.dockPnlForm, DockState.Document);
        }

        /// <summary>
        /// 供应商ABC类分析
        /// </summary>
        private void SupplierABCAnalysis()
        {
            if (msABC == null || msABC.IsDisposed)
            {
                msABC = new Monitor_SupplierABCAnalysis();
            }
            msABC.TopLevel = false;
            msABC.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            msABC.Show(this.dockPnlForm, DockState.Document);
        }
        #endregion

        #region 采购花费分析
        /// <summary>
        /// 采购花费分布
        /// </summary>
        private void PurchaseConstDistribution()
        {
            if (cpcd == null || cpcd.IsDisposed)
            {
                cpcd = new Cost_PurchaseCostDistribution();
            }
            cpcd.TopLevel = false;
            cpcd.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            cpcd.Show(this.dockPnlForm, DockState.Document);
        }

        /// <summary>
        /// 成本结构分析
        /// </summary>
        private void CostStructureAnalysis()
        {
            if (costSA == null || costSA.IsDisposed)
            {
                costSA = new CostStructureAnalysis();
            }
            costSA.TopLevel = false;
            costSA.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            costSA.Show(this.dockPnlForm, DockState.Document);
        }

        /// <summary>
        /// 成本比较分析
        /// </summary>
        private void CostComparativeAnalysis()
        {
            if (costCA == null || costCA.IsDisposed)
            {
                costCA = new CostComparativeAnalysis();
            }
            costCA.TopLevel = false;
            costCA.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            costCA.Show(this.dockPnlForm, DockState.Document);
        }

        /// <summary>
        /// 成本趋势分析
        /// </summary>
        private void CostTrenAnalysis()
        {
            if (costTA == null || costTA.IsDisposed)
            {
                costTA = new CostTrendAnalysis();
            }
            costTA.TopLevel = false;
            costTA.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            costTA.Show(this.dockPnlForm, DockState.Document);
        }

        #endregion

        #region 寻源分析
        /// <summary>
        /// 价格分析
        /// </summary>
        private void MaterialPriceAnalysis()
        {
            if (pao == null || pao.IsDisposed)
            {
                pao = new MaterialPriceOverview();
            }
            pao.TopLevel = false;
            pao.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            pao.Show(this.dockPnlForm, DockState.Document);
        }

        /// <summary>
        /// 供应商竞价分析
        /// </summary>
        private void SupplierBiddingAnalysis()
        {
            if (sba == null || sba.IsDisposed)
            {
                sba = new SupplierBiddingAnalysis();
            }
            sba.TopLevel = false;
            sba.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            sba.Show(this.dockPnlForm, DockState.Document);
        }
        #endregion

        #region 合同执行分析

        private void ConstractionManagement()
        {
            if (cm == null || cm.IsDisposed)
            {
                cm = new ConstractionManagement();
            }
            cm.TopLevel = false;
            cm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            cm.Show(this.dockPnlForm, DockState.Document);
        }
        #endregion

        #region 供应商绩效分析
        /// <summary>
        /// 供应商对比雷达图
        /// </summary>
        private void SupplierComparativeAnalysis()
        {
            if (ssr == null || ssr.IsDisposed)
            {
                ssr = new SupplierComparativeAnalysis();
            }
            ssr.TopLevel = false;
            ssr.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            ssr.Show(this.dockPnlForm, DockState.Document);
        }

        /// <summary>
        /// 供应商趋势报表
        /// </summary>
        private void SupplierTrendAnalysis()
        {
            if (sta == null || sta.IsDisposed)
            {
                sta = new SupplierTrendAnalysis();
            }
            sta.TopLevel = false;
            sta.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            sta.Show(this.dockPnlForm, DockState.Document);
        }

        /// <summary>
        /// 供应商得分排名
        /// </summary>
        private void SupplierScoreRanking()
        {
            if (sScoreRanking == null || sScoreRanking.IsDisposed)
            {
                sScoreRanking = new SupplierScoreRanking();
            }
            sScoreRanking.TopLevel = false;
            sScoreRanking.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            sScoreRanking.Show(this.dockPnlForm, DockState.Document);
        }
        #endregion

        #region 库存分析
        /// <summary>
        /// 库龄分析
        /// </summary>
        private void StockAgeAnalysis()
        {
            if (saa == null || saa.IsDisposed)
            {
                saa = new StockAgeAnalysis();
            }
            saa.TopLevel = false;
            saa.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            saa.Show(this.dockPnlForm, DockState.Document);
        }

        /// <summary>
        /// 库存物料ABC类分析
        /// </summary>
        private void StockABCAnalysis()
        {
            if (sABCa == null || sABCa.IsDisposed)
            {
                sABCa = new StockABCAnalysis();
            }
            sABCa.TopLevel = false;
            sABCa.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            sABCa.Show(this.dockPnlForm, DockState.Document);
        }

        #endregion

        #region 供应商分析
        /// <summary>
        /// 供应商分析
        /// </summary>
        private void SupplierAnalysis()
        {
            if (sas == null || sas.IsDisposed)
            {
                sas = new SupplierAnalysis();
            }
            sas.TopLevel = false;
            sas.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            sas.Show(this.dockPnlForm, DockState.Document);
        }
        #endregion

        #region 供应商依赖因素分析
        private void SuppplierDependentFactors()
        {
            if (sdf == null || sdf.IsDisposed)
            {
                sdf = new SupplierDependentFactors();
            }
            sdf.TopLevel = false;
            sdf.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            sdf.Show(this.dockPnlForm, DockState.Document);
        }
        #endregion

        #region 定制分析
        /// <summary>
        /// 定制分析
        /// </summary>
        private void Customized()
        {
            if (ct == null || ct.IsDisposed)
            {
                ct = new Customized();
            }
            ct.TopLevel = false;
            ct.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            ct.Show(this.dockPnlForm, DockState.Document);
        }
        #endregion
   
        #endregion

        #region 框架协议模块窗口生成
        /// <summary>
        /// 新建协议合同
        /// </summary>
        
        private void newAgreementContract() {
            
        }
        

        private void editAgreementContract() {

        }

        /// <summary>
        /// 管理协议合同
        /// </summary>
        private void manageAgreementContract() {
            if (this.agreementContractManageForm == null || this.agreementContractManageForm.IsDisposed)
            {
                this.agreementContractManageForm = new AgreementContractManageForm();
            }
            this.agreementContractManageForm.TopLevel = false;
            this.agreementContractManageForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.agreementContractManageForm.Show(this.dockPnlForm, DockState.Document);
        }

        /// <summary>
        /// 新建计划协议
        /// </summary>
        private void newSchedulingAgreement() {
            
        }

        /// <summary>
        /// 显示计划协议
        /// </summary>
        private void displaySchedulingAgreement() { 
            
        }

        /// <summary>
        /// 管理计划协议
        /// </summary>
        private void manageSchedulingAgreement() {
            
        }

        /// <summary>
        /// 管理配额协议
        /// </summary>
        private void manageQuotaArrangement() {
            if (this.quotaArrangementForm == null || this.quotaArrangementForm.IsDisposed)
            {
                this.quotaArrangementForm = new QuotaArrangementInitialForm();
            }

            this.quotaArrangementForm.TopLevel = false;
            this.quotaArrangementForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.quotaArrangementForm.Show(this.dockPnlForm, DockState.Document);
        }

        #endregion

        #region 文件管理模块窗口生成

        /// <summary>
        /// 文件管理
        /// </summary>
        private void newFileManageForm() {
            if (this.fileManageForm == null || this.fileManageForm.IsDisposed)
            {
                this.fileManageForm = new FileManageForm();
            }
            this.fileManageForm.TopLevel = false;
            this.fileManageForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.fileManageForm.Show(this.dockPnlForm, DockState.Document);
            
        }

        #endregion

        #region 订单管理模块窗口生成

        /// <summary>
        /// 生成采购订单管理界面
        /// </summary>
        private void newPOManageForm() {
            if (this.purchaseOrderManageForm == null || this.purchaseOrderManageForm.IsDisposed)
            {
                this.purchaseOrderManageForm = new QuryOrder();
            }
            this.purchaseOrderManageForm.TopLevel = false;
            this.purchaseOrderManageForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.purchaseOrderManageForm.Show(this.dockPnlForm, DockState.Document);
        }

        /// <summary>
        /// 生成发票与支付界面
        /// </summary>
        private void newInvoiceAndPaymentForm()
        {
            if (this.invoiceAndPaymentForm == null || this.invoiceAndPaymentForm.IsDisposed)
            {
                this.invoiceAndPaymentForm = new InvoiceAndPaymentForm();
            }
            this.invoiceAndPaymentForm.TopLevel = false;
            this.invoiceAndPaymentForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.invoiceAndPaymentForm.Show(this.dockPnlForm, DockState.Document);
        }
        /// <summary>
        /// 生成订单
        /// </summary>
        private void newOrderform()
        {
            if (this.OrderForm == null || this.OrderForm.IsDisposed)
            {
                this.OrderForm = new Order();
            }
            this.OrderForm.TopLevel = false;
            this.OrderForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.OrderForm.Show(this.dockPnlForm, DockState.Document);
        }

        #endregion

        #region 自助采购模块窗口生成

        /// <summary>
        /// 生成自助采购主界面
        /// </summary>
        private void newCatalogManageForm() {
            if (this.catalogManageForm == null || this.catalogManageForm.IsDisposed)
            {
                this.catalogManageForm = new CatalogManageForm();
            }
            this.catalogManageForm.TopLevel = false;
            this.catalogManageForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.catalogManageForm.Show(this.dockPnlForm, DockState.Document);
        }

        #endregion

        #region 主数据模块窗口生成
        //新建物料数据
        private void NewMaterialData()
        {
            if (this.NewMaterialDataForm == null || this.NewMaterialDataForm.IsDisposed)
            {
                this.NewMaterialDataForm = new MTEstablish1();
            }
            this.NewMaterialDataForm.TopLevel = false;
            this.NewMaterialDataForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.NewMaterialDataForm.Show(this.dockPnlForm, DockState.Document);
        }
        //物料主数据基本视图
        private void MaterialBasicView()
        {
            if (this.MaterialBasicViewForm == null || this.MaterialBasicViewForm.IsDisposed)
            {
                this.MaterialBasicViewForm = new MTMaintain();
            }
            this.MaterialBasicViewForm.TopLevel = false;
            this.MaterialBasicViewForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaterialBasicViewForm.Show(this.dockPnlForm, DockState.Document);
        }


        //物料主数据会计视图
        private void MaterialAccountView()
        {
            if (this.MaterialAccountViewForm == null || this.MaterialAccountViewForm.IsDisposed)
            {
                this.MaterialAccountViewForm = new MTACT();
            }
            this.MaterialAccountViewForm.TopLevel = false;
            this.MaterialAccountViewForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaterialAccountViewForm.Show(this.dockPnlForm, DockState.Document);
        }
        //物料主数据采购视图
        private void MaterialBuyerView()
        {
            if (this.MaterialBuyerViewForm == null || this.MaterialBuyerViewForm.IsDisposed)
            {
                this.MaterialBuyerViewForm = new MTBUYER();
            }
            this.MaterialBuyerViewForm.TopLevel = false;
            this.MaterialBuyerViewForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaterialBuyerViewForm.Show(this.dockPnlForm, DockState.Document);
        }
        //物料主数据存储视图
        private void MaterialStockView()
        {
            if (this.MaterialStockViewForm == null || this.MaterialStockViewForm.IsDisposed)
            {
                this.MaterialStockViewForm = new 存储视图();
            }
            this.MaterialStockViewForm.TopLevel = false;
            this.MaterialStockViewForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaterialStockViewForm.Show(this.dockPnlForm, DockState.Document);
        }
        //物料主数据分类视图
        private void MaintainMaterialClass()
        {
            if (this.MaterialClassViewForm == null || this.MaterialClassViewForm.IsDisposed)
            {
                this.MaterialClassViewForm = new MaterialClass();
            }
            this.MaterialClassViewForm.TopLevel = false;
            this.MaterialClassViewForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaterialClassViewForm.Show(this.dockPnlForm, DockState.Document);
        }
        //建立批次
        private void NewBatch()
        {
            if (this.NewBatchForm == null || this.NewBatchForm.IsDisposed)
            {
                this.NewBatchForm = new BatchFeature();
            }
            this.NewBatchForm.TopLevel = false;
            this.NewBatchForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.NewBatchForm.Show(this.dockPnlForm, DockState.Document);
        }
        //查询批次
        private void QuryBatch()
        {
            if (this.QuryBatchForm == null || this.QuryBatchForm.IsDisposed)
            {
                this.QuryBatchForm = new BatchQury(); 
            }
            this.QuryBatchForm.TopLevel = false;
            this.QuryBatchForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.QuryBatchForm.Show(this.dockPnlForm, DockState.Document);
        }
        //库存管理MPN
        //非库存管理MPN
        //新建供应商数据
        private void NewSupplier()
        {
            if (this.NewSupplierForm == null || this.NewSupplierForm.IsDisposed)
            {
                this.NewSupplierForm = new NewSupplier();
            }
            this.NewSupplierForm.TopLevel = false;
            this.NewSupplierForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.NewSupplierForm.Show(this.dockPnlForm, DockState.Document);
        }
        private void SupplierBasicView()
        {
            if (this.SupplierBasicViewForm == null || this.SupplierBasicViewForm.IsDisposed)
            {
                this.SupplierBasicViewForm = new SPMaintain(); 
            }
            this.SupplierBasicViewForm.TopLevel = false;
            this.SupplierBasicViewForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.SupplierBasicViewForm.Show(this.dockPnlForm, DockState.Document);
        }
       

        private void SupplierBuyerOrgView()
        {
            if (this.SupplierBuyerOrgViewForm == null || this.SupplierBuyerOrgViewForm.IsDisposed)
            {
                this.SupplierBuyerOrgViewForm = new SPORG();
            }
            this.SupplierBuyerOrgViewForm.TopLevel = false;
            this.SupplierBuyerOrgViewForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.SupplierBuyerOrgViewForm.Show(this.dockPnlForm, DockState.Document);
        }
        private void SupplierCompanyCodeview()
        {
            if (this.SupplierCompanyCodeViewForm == null || this.SupplierCompanyCodeViewForm.IsDisposed)
            {
                this.SupplierCompanyCodeViewForm = new SPCPY1(); 
            }
            this.SupplierCompanyCodeViewForm.TopLevel = false;
            this.SupplierCompanyCodeViewForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.SupplierCompanyCodeViewForm.Show(this.dockPnlForm, DockState.Document);
        }
        //财务主数据
        private void NewVocher()
        {
            if (this.NewVocherForm == null || this.NewVocherForm.IsDisposed)
            {
                this.NewVocherForm = new ManualPosting();   
            }
            this.NewVocherForm.TopLevel = false;
            this.NewVocherForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.NewVocherForm.Show(this.dockPnlForm, DockState.Document);
        }
        private void QuryVocher()
        {
            if (this.QuryVocherForm == null || this.QuryVocherForm.IsDisposed)
            {
                this.QuryVocherForm = new ProofInformation();
            }
            this.QuryVocherForm.TopLevel = false;
            this.QuryVocherForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.QuryVocherForm.Show(this.dockPnlForm, DockState.Document);
        }
        private void QuryAccount()
        {
            if (this.QuryAccountForm == null || this.QuryAccountForm.IsDisposed)
            {
                this.QuryAccountForm = new actsummy();
            }
            this.QuryAccountForm.TopLevel = false;
            this.QuryAccountForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.QuryAccountForm.Show(this.dockPnlForm, DockState.Document);
        }
        //一般设置
        //维护物料类型
        //维护物料大分类
        private void MaterialTypeA()
        {
            if (this.MaterialTypeAForm == null || this.MaterialTypeAForm.IsDisposed)
            {
                this.MaterialTypeAForm = new MTType();
            }
            this.MaterialTypeAForm.TopLevel = false;
            this.MaterialTypeAForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaterialTypeAForm.Show(this.dockPnlForm, DockState.Document);
        }
        //维护物料小分类
        private void MaterialTypeB()
        {
            if (this.MaterialTypeBForm == null || this.MaterialTypeBForm.IsDisposed)
            {
                this.MaterialTypeBForm = new MTtypeB();
            }
            this.MaterialTypeBForm.TopLevel = false;
            this.MaterialTypeBForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaterialTypeBForm.Show(this.dockPnlForm, DockState.Document);
        }
        //维护物料类型
        private void MaterialTypeC()
        {
            if (this.MaterialTypeCForm == null || this.MaterialTypeCForm.IsDisposed)
            {
                this.MaterialTypeCForm = new MTTypeC(); 
            }
            this.MaterialTypeCForm.TopLevel = false;
            this.MaterialTypeCForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaterialTypeCForm.Show(this.dockPnlForm, DockState.Document);
        }
        //维护付款条件
        private void MaintainPaymentClause()
        {
            if (this.MaintainPaymentClauseForm == null || this.MaintainPaymentClauseForm.IsDisposed)
            {
                this.MaintainPaymentClauseForm = new Payment_Clause();
            }
            this.MaintainPaymentClauseForm.TopLevel = false;
            this.MaintainPaymentClauseForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaintainPaymentClauseForm.Show(this.dockPnlForm, DockState.Document);
        }
        //维护货币
        private void MaintainCurrency()
        {
            if (this.MaintainCurrencyForm == null || this.MaintainCurrencyForm.IsDisposed)
            {
                this.MaintainCurrencyForm = new Currency();
            }
            this.MaintainCurrencyForm.TopLevel = false;
            this.MaintainCurrencyForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaintainCurrencyForm.Show(this.dockPnlForm, DockState.Document);
        }
        //维护国家
        private void MaintainCountry()
        {
            if (this.MaintainCountryForm == null || this.MaintainCountryForm.IsDisposed)
            {
                this.MaintainCountryForm = new Country();
            }
            this.MaintainCountryForm.TopLevel = false;
            this.MaintainCountryForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaintainCountryForm.Show(this.dockPnlForm, DockState.Document);
        }

        //维护策略信息库
        private void newStrateInfo()
        {
            if (this.strateInfo == null || this.strateInfo.IsDisposed)
            {
                this.strateInfo = new StrateInfo();
            }
            this.strateInfo.TopLevel = false;
            this.strateInfo.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.strateInfo.Show(this.dockPnlForm, DockState.Document);
        }


        //维护目标值和最低值
        private void newGoalVlaueAndLowValueForm()
        {
            if (this.greenValueAndLowValueForm == null || this.greenValueAndLowValueForm.IsDisposed)
            {
                this.greenValueAndLowValueForm = new GreenValueAndLowValueForm();
            }
            this.greenValueAndLowValueForm.TopLevel = false;
            this.greenValueAndLowValueForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.greenValueAndLowValueForm.Show(this.dockPnlForm, DockState.Document);
        }
        //维护物料组
        private void MaintainMaterialGroup()
        {
            if (this.MaintainMaterialGroupForm == null || this.MaintainMaterialGroupForm.IsDisposed)
            {
                this.MaintainMaterialGroupForm = new Material_Group();
            }
            this.MaintainMaterialGroupForm.TopLevel = false;
            this.MaintainMaterialGroupForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaintainMaterialGroupForm.Show(this.dockPnlForm, DockState.Document);
        }

        //维护工厂
        private void MaintainFactory()
        {
            if (this.MaintainFactoryForm == null || this.MaintainFactoryForm.IsDisposed)
            {
                this.MaintainFactoryForm = new Factory();
            }
            this.MaintainFactoryForm.TopLevel = false;
            this.MaintainFactoryForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaintainFactoryForm.Show(this.dockPnlForm, DockState.Document);
        }
        //维护付款条件
        private void MaintainMeasurement()
        {
            if (this.MaintainMeasurementForm == null || this.MaintainMeasurementForm.IsDisposed)
            {
                this.MaintainMeasurementForm = new Measurement();
            }
            this.MaintainMeasurementForm.TopLevel = false;
            this.MaintainMeasurementForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaintainMeasurementForm.Show(this.dockPnlForm, DockState.Document);
        }
        //维护交付条件
        private void MaintainTradeClause()
        {
            if (this.MaintainTradeClauseForm == null || this.MaintainTradeClauseForm.IsDisposed)
            {
                this.MaintainTradeClauseForm = new TradeClause();
            }
            this.MaintainTradeClauseForm.TopLevel = false;
            this.MaintainTradeClauseForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaintainTradeClauseForm.Show(this.dockPnlForm, DockState.Document);
        }
        //维护评估类
        private void MaintainEvaluationClass()
        {
            if (this.MaintainEvaluationClassForm == null || this.MaintainEvaluationClassForm.IsDisposed)
            {
                this.MaintainEvaluationClassForm = new EvaluationClass();
            }
            this.MaintainEvaluationClassForm.TopLevel = false;
            this.MaintainEvaluationClassForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaintainEvaluationClassForm.Show(this.dockPnlForm, DockState.Document);
        }
        //维护仓库
        private void MaintainStock()
        {
            if (this.MaintainStockForm == null || this.MaintainStockForm.IsDisposed)
            {
                this.MaintainStockForm = new Stock();
            }
            this.MaintainStockForm.TopLevel = false;
            this.MaintainStockForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaintainStockForm.Show(this.dockPnlForm, DockState.Document);
        }
        //维护公司
        private void MaintainCompany()
        {
            if (this.MaintainCompanyForm == null || this.MaintainCompanyForm.IsDisposed)
            {
                this.MaintainCompanyForm = new Company();
            }
            this.MaintainCompanyForm.TopLevel = false;
            this.MaintainCompanyForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaintainCompanyForm.Show(this.dockPnlForm, DockState.Document);
        }
        //维护采购组
        private void MaintainBuyerGroup()
        {
            if (this.MaintainBuyerGroupForm == null || this.MaintainBuyerGroupForm.IsDisposed)
            {
                this.MaintainBuyerGroupForm = new BuyerGroup();
            }
            this.MaintainBuyerGroupForm.TopLevel = false;
            this.MaintainBuyerGroupForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaintainBuyerGroupForm.Show(this.dockPnlForm, DockState.Document);
        }
        //给采购组织分配物料组
        private void purOrgAllotmtGroup()
        {
            if (this.MaintainBuyerOrganizationForm == null || this.MaintainBuyerOrganizationForm.IsDisposed)
            {
                this.MaintainBuyerOrganizationForm = new BuyerOrganization();
            }
            this.MaintainBuyerOrganizationForm.TopLevel = false;
            this.MaintainBuyerOrganizationForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaintainBuyerOrganizationForm.Show(this.dockPnlForm, DockState.Document);
        }


        //维护采购组织
        private void MaintainBuyerOrganization()
        {
            if (this.maintainPurOrgForm == null || this.maintainPurOrgForm.IsDisposed)
            {
                this.maintainPurOrgForm = new MaintainPurOrg();
            }
            this.maintainPurOrgForm.TopLevel = false;
            this.maintainPurOrgForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.maintainPurOrgForm.Show(this.dockPnlForm, DockState.Document);
        }
        //维护评估要素
             private void MaintainMaterEvalFactor()
        {
            if (this.maintainMaterEvalFactor == null || this.maintainMaterEvalFactor.IsDisposed)
            {
                this.maintainMaterEvalFactor = new MaintainMaterEvalFactor();
            }
            this.maintainMaterEvalFactor.TopLevel = false;
            this.maintainMaterEvalFactor.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.maintainMaterEvalFactor.Show(this.dockPnlForm, DockState.Document);
        }
        //维护产品组
        private void MaintainDivision()
        {
            if (this.MaintainDivisionForm == null || this.MaintainDivisionForm.IsDisposed)
            {
                this.MaintainDivisionForm = new Division();
            }
            this.MaintainDivisionForm.TopLevel = false;
            this.MaintainDivisionForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaintainDivisionForm.Show(this.dockPnlForm, DockState.Document);
        }
        //维护税码
        private void MaintainTaxCode()
        {
            if (this.MaintainTaxCodeForm == null || this.MaintainTaxCodeForm.IsDisposed)
            {
                this.MaintainTaxCodeForm = new TaxCode();
            }
            this.MaintainTaxCodeForm.TopLevel = false;
            this.MaintainTaxCodeForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaintainTaxCodeForm.Show(this.dockPnlForm, DockState.Document);
        }
        //维护成本中心
        private void MaintainCostCenter()
        {
            if (this.MaintainCostCenterForm == null || this.MaintainCostCenterForm.IsDisposed)
            {
                this.MaintainCostCenterForm = new CostCenter();
            }
            this.MaintainCostCenterForm.TopLevel = false;
            this.MaintainCostCenterForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaintainCostCenterForm.Show(this.dockPnlForm, DockState.Document);
        }
        //维护工业标识
        private void MaintainIndustryCategory()
        {
            if (this.MaintainIndustryCategoryForm == null || this.MaintainIndustryCategoryForm.IsDisposed)
            {
                this.MaintainIndustryCategoryForm = new IndustryCategory();
            }
            this.MaintainIndustryCategoryForm.TopLevel = false;
            this.MaintainIndustryCategoryForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaintainIndustryCategoryForm.Show(this.dockPnlForm, DockState.Document);
        }


        
        private void purchasesRecordInfoView()
        {
            if (this.purchasesRecordInfoViewForm == null || this.purchasesRecordInfoViewForm.IsDisposed)
            {
                this.purchasesRecordInfoViewForm = new PurchasesRecordInfoViewForm();
            }
            this.purchasesRecordInfoViewForm.TopLevel = false;
            this.purchasesRecordInfoViewForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.purchasesRecordInfoViewForm.Show(this.dockPnlForm, DockState.Document);
        }

        private void addPurRecordInfo_1()
        {
            if (this.addPurRecordInfoForm == null || this.addPurRecordInfoForm.IsDisposed)
            {
                this.addPurRecordInfoForm = new AddPurRecordInfoForm();
            }
            this.addPurRecordInfoForm.TopLevel = false;
            this.addPurRecordInfoForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.addPurRecordInfoForm.Show(this.dockPnlForm, DockState.Document);
        }
        ///
        //计划管理
        private void managerPlans()
        {
            if (this.managerPlansForm == null || this.managerPlansForm.IsDisposed)
            {
                this.managerPlansForm = new ManagerPlansForm();
            }
            this.managerPlansForm.TopLevel = false;
            this.managerPlansForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.managerPlansForm.Show(this.dockPnlForm, DockState.Document);
        }




        #endregion

        #region 供应商绩效
        /// <summary>
        /// 供应商绩效自动评估
        /// </summary>
        private void newSuppliperPerformanceAutoForm()
        {
            if (this.supplierPerformanceAutoForm == null || this.supplierPerformanceAutoForm.IsDisposed)
            {
                this.supplierPerformanceAutoForm = new SupplierPerformanceAutoForm();
            }
            this.supplierPerformanceAutoForm.TopLevel = false;
            this.supplierPerformanceAutoForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.supplierPerformanceAutoForm.Show(this.dockPnlForm, DockState.Document);
        }

        /// <summary>
        /// 手工维护供应商绩效
        /// </summary>
        private void newSuppliperPerformanceAddForm()
        {
            if (this.supplierPerformanceAddForm == null || this.supplierPerformanceAddForm.IsDisposed)
            {
                this.supplierPerformanceAddForm = new SupplierPerformanceAddForm();
            }
            this.supplierPerformanceAddForm.TopLevel = false;
            this.supplierPerformanceAddForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.supplierPerformanceAddForm.Show(this.dockPnlForm, DockState.Document);
        }

        /// <summary>
        /// 供应商绩效批量评估
        /// </summary>
        private void newSupplierPerformanceBatchAddForm()
        {
            if (this.supplierPerformanceBatchAddForm == null || this.supplierPerformanceBatchAddForm.IsDisposed)
            {
                this.supplierPerformanceBatchAddForm = new SupplierPerformanceBatchAddForm();
            }
            this.supplierPerformanceBatchAddForm.TopLevel = false;
            this.supplierPerformanceBatchAddForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.supplierPerformanceBatchAddForm.Show(this.dockPnlForm, DockState.Document);
        }


        /// <summary>
        /// 供应商绩效评估结果展示
        /// </summary>
        private void newSupplierPerformanceResultDisplayForm()
        {
            if (this.supplierPerformanceResultDisplayForm == null || this.supplierPerformanceResultDisplayForm.IsDisposed)
            {
                this.supplierPerformanceResultDisplayForm = new SupplierPerformanceResultDisplay();
            }
            this.supplierPerformanceResultDisplayForm.TopLevel = false;
            this.supplierPerformanceResultDisplayForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.supplierPerformanceResultDisplayForm.Show(this.dockPnlForm, DockState.Document);
        }

        /// <summary>
        /// 抱怨/拒绝情况录入
        /// </summary>
        private void newComplaintRejectAddForm()
        {
            if (this.complaintRejectAddForm == null || this.complaintRejectAddForm.IsDisposed)
            {
                this.complaintRejectAddForm = new ComplaintRejectAddForm();
            }
            this.complaintRejectAddForm.TopLevel = false;
            this.complaintRejectAddForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.complaintRejectAddForm.Show(this.dockPnlForm, DockState.Document);
        }

        /// <summary>
        /// 服务相关数据录入
        /// </summary>
        private void newServiceAddForm()
        {
            if (this.serviceScoreAddForm == null || this.serviceScoreAddForm.IsDisposed)
            {
                this.serviceScoreAddForm = new ServiceScoreAddForm();
            }
            this.serviceScoreAddForm.TopLevel = false;
            this.serviceScoreAddForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.serviceScoreAddForm.Show(this.dockPnlForm, DockState.Document);
        }

        /// <summary>
        /// 体系/过程审核数据录入
        /// </summary>
        private void newSystemProcessAuditForm()
        {
            if (this.systemProcessAuditForm == null || this.systemProcessAuditForm.IsDisposed)
            {
                this.systemProcessAuditForm = new SystemProcessAuditForm();
            }
            this.systemProcessAuditForm.TopLevel = false;
            this.systemProcessAuditForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.systemProcessAuditForm.Show(this.dockPnlForm, DockState.Document);
        }

        /// <summary>
        /// 供应商营业额录入
        /// </summary>
        private void newSupplierTurnoverForm()
        {
            if (this.supplierTurnoverForm == null || this.supplierTurnoverForm.IsDisposed)
            {
                this.supplierTurnoverForm = new SupplierTurnoverForm();
            }
            this.supplierTurnoverForm.TopLevel = false;
            this.supplierTurnoverForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.supplierTurnoverForm.Show(this.dockPnlForm, DockState.Document);
        }

        /// <summary>
        /// 收货问卷调查
        /// </summary>
        private void newQuestionaireForm()
        {
            if (this.questionareForm == null || this.questionareForm.IsDisposed)
            {
                this.questionareForm = new QuestionaireForm();
            }
            this.questionareForm.TopLevel = false;
            this.questionareForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.questionareForm.Show(this.dockPnlForm, DockState.Document);
        }

        /// <summary>
        /// 产品审核数据录入
        /// </summary>
        private void newProductAddForm()
        {
            if (this.productAuditForm == null || this.productAuditForm.IsDisposed)
            {
                this.productAuditForm = new ProductAuditForm();
            }
            this.productAuditForm.TopLevel = false;
            this.productAuditForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.productAuditForm.Show(this.dockPnlForm, DockState.Document);
        }

        /// <summary>
        /// 无评估供应商清单
        /// </summary>
        private void newNoSPListForm()
        {
            if (this.noSPListForm == null || this.noSPListForm.IsDisposed)
            {
                this.noSPListForm = new NoSupplierPerformanceList();

            }
            this.noSPListForm.TopLevel = false;
            this.noSPListForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.noSPListForm.Show(this.dockPnlForm, DockState.Document);
        }

        //供应商分级
        private void newSupplierClassificationForm()
        {
            if (this.supplierClassificationForm == null || this.supplierClassificationForm.IsDisposed)
            {
                this.supplierClassificationForm = new SupplierClassificationForm();

            }
            this.supplierClassificationForm.TopLevel = false;
            this.supplierClassificationForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.supplierClassificationForm.Show(this.dockPnlForm, DockState.Document);
        }

        /// <summary>
        /// 供应商分级结果查看
        /// </summary>
        private void newClassificationResultForm()
        {
            if (this.classificationResultForm == null || this.classificationResultForm.IsDisposed)
            {
                this.classificationResultForm = new ClassificationResultForm();

            }
            this.classificationResultForm.TopLevel = false;
            this.classificationResultForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.classificationResultForm.Show(this.dockPnlForm, DockState.Document);
        }

        /// <summary>
        /// 分组查看供应商分级结果
        /// </summary>
        private void newResultGroupForm()
        {
            if (this.resultGroupForm == null || this.resultGroupForm.IsDisposed)
            {
                this.resultGroupForm = new ResultGroupForm();
            }
            this.resultGroupForm.TopLevel = false;
            this.resultGroupForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.resultGroupForm.Show(this.dockPnlForm, DockState.Document);
        }

        /// <summary>
        /// 低效能供应商警告
        /// </summary>
        private void newLPSWarningForm()
        {
            if (this.lpsWarningForm == null || this.lpsWarningForm.IsDisposed)
            {
                this.lpsWarningForm = new LPSWarningForm();

            }
            this.lpsWarningForm.TopLevel = false;
            this.lpsWarningForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.lpsWarningForm.Show(this.dockPnlForm, DockState.Document);
        }

        /// <summary>
        /// 低效能供应商管理三级流程
        /// </summary>
        private void newLPSProcessNode()
        {
            if (this.lpsProcessForm == null || this.lpsProcessForm.IsDisposed)
            {
                this.lpsProcessForm = new LPSProcessForm();

            }
            this.lpsProcessForm.TopLevel = false;
            this.lpsProcessForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.lpsProcessForm.Show(this.dockPnlForm, DockState.Document);
        }

        /// <summary>
        /// 低效能供应商淘汰
        /// </summary>
        private void newLPSOutNode()
        {
            if (this.lpsOutCheckForm == null || this.lpsOutCheckForm.IsDisposed)
            {
                this.lpsOutCheckForm = new LPSOutCheckForm();

            }
            this.lpsOutCheckForm.TopLevel = false;
            this.lpsOutCheckForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.lpsOutCheckForm.Show(this.dockPnlForm, DockState.Document);
        }


        /// <summary>
        /// 降成本计算
        /// </summary>
        private void newCostReductionCalculateNode()
        {
            if (this.costReductionForm == null || this.costReductionForm.IsDisposed)
            {
                this.costReductionForm = new CostReductionForm();

            }
            this.costReductionForm.TopLevel = false;
            this.costReductionForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.costReductionForm.Show(this.dockPnlForm, DockState.Document);
        }

        /// <summary>
        /// 降成本结果展示
        /// </summary>
        private void newCostReductionResultNode()
        {
            if (this.costReductionResultForm == null || this.costReductionResultForm.IsDisposed)
            {
                this.costReductionResultForm = new CostReductionResultForm();

            }
            this.costReductionResultForm.TopLevel = false;
            this.costReductionResultForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.costReductionResultForm.Show(this.dockPnlForm, DockState.Document);
        }

        /// <summary>
        /// 业务价值评估
        /// </summary>
        private void newBusinessValueAssessNode()
        {
            if (this.businessValueAssessForm == null || this.businessValueAssessForm.IsDisposed)
            {
                this.businessValueAssessForm = new BusinessValueAssessForm();

            }
            this.businessValueAssessForm.TopLevel = false;
            this.businessValueAssessForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.businessValueAssessForm.Show(this.dockPnlForm, DockState.Document);
        }

        /// <summary>
        /// 供应商跟踪
        /// </summary>
        private void newSupplierTrackNode()
        {
            if (this.supplierStateTrackingForm == null || this.supplierStateTrackingForm.IsDisposed)
            {
                this.supplierStateTrackingForm = new SupplierStateTrackingForm();

            }
            this.supplierStateTrackingForm.TopLevel = false;
            this.supplierStateTrackingForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.supplierStateTrackingForm.Show(this.dockPnlForm, DockState.Document);
        }

     

        #endregion

        #region 供应商绩效报表
        /// <summary>
        /// 供应商绩效比较
        /// </summary>
        private void viewSPCompare()
        {
            if (frm_SPCompare == null || frm_SPCompare.IsDisposed)
            {
                frm_SPCompare = new Frm_SPCompare();
            }
            frm_SPCompare.TopLevel = false;
            frm_SPCompare.Dock = DockStyle.Fill;
            frm_SPCompare.Show(this.dockPnlForm, DockState.Document);
        }

        /// <summary>
        /// 基于物料组的供应商评估
        /// </summary>
        private void viewSPCompareBaseOnMaterialGroup()
        {
            if (frm_SPCompareBOMG == null || frm_SPCompareBOMG.IsDisposed)
            {
                frm_SPCompareBOMG = new Frm_SPCompareBOMG();
            }
            frm_SPCompareBOMG.TopLevel = false;
            frm_SPCompareBOMG.Dock = DockStyle.Fill;
            frm_SPCompareBOMG.Show(this.dockPnlForm, DockState.Document);
        }

        /// <summary>
        /// 基于行业的供应商评估
        /// </summary>
        private void viewSPCompareBaseOnIndustry()
        {
            if (frm_SPCompareBOI == null || frm_SPCompareBOI.IsDisposed)
            {
                frm_SPCompareBOI = new Frm_SPCompareBOI();
            }
            frm_SPCompareBOI.TopLevel = false;
            frm_SPCompareBOI.Dock = DockStyle.Fill;
            frm_SPCompareBOI.Show(this.dockPnlForm, DockState.Document);
        }

        /// <summary>
        /// 供应商对比雷达图
        /// </summary>
        private void viewSPCompareRadarChart()
        {
            if (frm_SPCompareRadarChart == null || frm_SPCompareRadarChart.IsDisposed)
            {
                frm_SPCompareRadarChart = new Frm_SPCompareRadarChart();
            }
            frm_SPCompareRadarChart.TopLevel = false;
            frm_SPCompareRadarChart.Dock = DockStyle.Fill;
            frm_SPCompareRadarChart.Show(this.dockPnlForm, DockState.Document);
        }

        /// <summary>
        /// 供应商分析雷达图
        /// </summary>
        private void viewSPCompareBubbleChart()
        {
            if (frm_SPCompareBubbleChart == null || frm_SPCompareBubbleChart.IsDisposed)
            {
                frm_SPCompareBubbleChart = new Frm_SPCompareBubbleChart();
            }
            frm_SPCompareBubbleChart.TopLevel = false;
            frm_SPCompareBubbleChart.Dock = DockStyle.Fill;
            frm_SPCompareBubbleChart.Show(this.dockPnlForm, DockState.Document);
        }

        #endregion

        #region 供应商认证窗口生成

        /// <summary>
        /// 供应商认证前期准备
        /// </summary>
        private void newSupplierCertificationForm()
        {
            if (this.supplierCertificationForm == null || this.supplierCertificationForm.IsDisposed)
            {
                this.supplierCertificationForm = new SupplierCertificationForm();
            }
            this.supplierCertificationForm.TopLevel = false;
            this.supplierCertificationForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.supplierCertificationForm.Show(this.dockPnlForm, DockState.Document);
        }

        /// <summary>
        /// 初步筛选Form
        /// </summary>
        private void newSupplierPreselectionFrm()
        {
            if (this.supplierPreselectionFrm == null || this.supplierPreselectionFrm.IsDisposed)
            {
                this.supplierPreselectionFrm = new SupplierPreselectionForm();
            }
            this.supplierPreselectionFrm.TopLevel = false;
            this.supplierPreselectionFrm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.supplierPreselectionFrm.Show(this.dockPnlForm, DockState.Document);
        }
        /// <summary>
        /// 协议与承诺Form
        /// </summary>
        private void newAgreementAndCommitmentsFrm()
        {
            if (this.agreementAndCommitmentsFrm == null || this.agreementAndCommitmentsFrm.IsDisposed)
            {
                this.agreementAndCommitmentsFrm = new AgreementAndCommitmentsForm();
            }
            this.agreementAndCommitmentsFrm.TopLevel = false;
            this.agreementAndCommitmentsFrm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.agreementAndCommitmentsFrm.Show(this.dockPnlForm, DockState.Document);

        }
        /// <summary>
        /// 供应商能力评估Form
        /// </summary>
        private void newSupplierEvaluationFrm()
        {
            if (this.supplierEvalutationFrm == null || this.supplierEvalutationFrm.IsDisposed)
            {
                this.supplierEvalutationFrm = new SupplierEvaluationForm();
            }
            this.supplierEvalutationFrm.TopLevel = false;
            this.supplierEvalutationFrm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.supplierEvalutationFrm.Show(this.dockPnlForm, DockState.Document);
        }
        /// <summary>
        /// swot分析Form
        /// </summary>
        private void newSwotFrm()
        {
            if (this.swotFrm == null || this.swotFrm.IsDisposed)
            {
                this.swotFrm = new SwotForm();
            }
            this.swotFrm.TopLevel = false;
            this.swotFrm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.swotFrm.Show(this.dockPnlForm, DockState.Document);

        }
        /// <summary>
        /// 差异识别Form
        /// </summary>
        private void newDifferentialIdentificationFrm()
        {
            if (this.differentialIdentificationFrm == null || this.differentialIdentificationFrm.IsDisposed)
            {
                this.differentialIdentificationFrm = new DifferentialIdentificationForm();
            }
            this.differentialIdentificationFrm.TopLevel = false;
            this.differentialIdentificationFrm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.differentialIdentificationFrm.Show(this.dockPnlForm, DockState.Document);
        }
        /// <summary>
        /// 改进/验收Form
        /// </summary>
        private void newImporvementAndCheckFrm()
        {
            if (this.imporvementAndCheckFrm == null || this.imporvementAndCheckFrm.IsDisposed)
            {
                this.imporvementAndCheckFrm = new ImprovementAndCheckForm();
            }
            this.imporvementAndCheckFrm.TopLevel = false;
            this.imporvementAndCheckFrm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.imporvementAndCheckFrm.Show(this.dockPnlForm, DockState.Document);
        }

        private void newSupplierAccessFilter()
        {
            if (this.SupplierAccessFilter == null || this.SupplierAccessFilter.IsDisposed)
            {
                this.SupplierAccessFilter = new SupplierAccessFilter();
            }
            this.SupplierAccessFilter.TopLevel = false;
            this.SupplierAccessFilter.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.SupplierAccessFilter.Show(this.dockPnlForm, DockState.Document);
        }
        private void newSupplierPreCertificate()
        {
            if (this.SupplierPreCertificate == null || this.SupplierPreCertificate.IsDisposed)
            {
                this.SupplierPreCertificate = new SupplierPreCertificate();
            }
            this.SupplierPreCertificate.TopLevel = false;
            this.SupplierPreCertificate.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.SupplierPreCertificate.Show(this.dockPnlForm, DockState.Document);
        }

        private void newEvaluate()
        {
            if (this.Evaluate == null || this.Evaluate.IsDisposed)
            {
                this.Evaluate = new Evaluate();
            }
            this.Evaluate.TopLevel = false;
            this.Evaluate.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Evaluate.Show(this.dockPnlForm, DockState.Document);
        }
       /* private void newFupload()
        {
            if (this.Fupload == null || this.Fupload.IsDisposed)
            {
                this.Fupload = new Fupload();
            }
            this.Fupload.TopLevel = false;
            this.Fupload.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Fupload.Show(this.dockPnlForm, DockState.Document);
        }*/
        /// <summary>
        /// 供应商现场评估
        /// </summary>
        private void newLocationalEvaluationForm()
        {
            if (this.locationalEvaluationForm == null || this.locationalEvaluationForm.IsDisposed)
            {
                this.locationalEvaluationForm = new LocationalEvaluationForm();
            }
            this.locationalEvaluationForm.TopLevel = false;
            this.locationalEvaluationForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.locationalEvaluationForm.Show(this.dockPnlForm, DockState.Document);
        }

        /// <summary>
        /// 供应用户权限管理
        /// </summary>
        private void newUserListForm()
        {
            if (this.userListForm == null || this.userListForm.IsDisposed)
            {
                this.userListForm = new UserListForm();
            }
            this.userListForm.TopLevel = false;
            this.userListForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.userListForm.Show(this.dockPnlForm, DockState.Document);
        }
        /// <summary>
        /// 供应商准入
        /// </summary>
        private void newCertificationFinishedForm()
        {
            if(this.finshCertificationForm == null || this.finshCertificationForm.IsDisposed)
            {
                this.finshCertificationForm = new PassedCertificationForm();
            }
            this.finshCertificationForm.TopLevel = false;
            this.finshCertificationForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.finshCertificationForm.Show(this.dockPnlForm, DockState.Document);
        }
        /// <summary>
        /// 
        /// </summary>
        private void newLocalEvaluationSingleForm()
        {
            if (this.localEvaluationSingleForm == null || this.localEvaluationSingleForm.IsDisposed)
            {
                this.localEvaluationSingleForm = new LocalEvaluationSingleForm();
            }
            this.localEvaluationSingleForm.TopLevel = false;
            this.localEvaluationSingleForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.localEvaluationSingleForm.Show(this.dockPnlForm, DockState.Document);
        }

        /// <summary>
        /// 领导决策菜单窗口
        /// </summary>
        private void newLeaderDecide()
        {
            if (this.leaderDecide == null || this.leaderDecide.IsDisposed)
            {
                this.leaderDecide = new LeaderDecide();
            }
            this.leaderDecide.TopLevel = false;
            this.leaderDecide.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.leaderDecide.Show(this.dockPnlForm, DockState.Document);
        }

        /// <summary>
        /// 已准入页面
        /// </summary>
        private void newSupplierPermission()
        {
            if (this.Supplier_Permission == null || this.Supplier_Permission.IsDisposed)
            {
                this.Supplier_Permission = new Supplier_Permission();
            }
            this.Supplier_Permission.TopLevel = false;
            this.Supplier_Permission.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Supplier_Permission.Show(this.dockPnlForm, DockState.Document);
        }
        #endregion

        #region 物料定位窗体生成

        /// <summary>
        /// 竞争力
        /// </summary>
        private void newAddItemGoalsForm()
        {
            if (this.addItemGoals == null || this.addItemGoals.IsDisposed)
            {
                this.addItemGoals = new Material_group_positioning.AddItemGoals();
            }
            this.addItemGoals.TopLevel = false;
            this.addItemGoals.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.addItemGoals.Show(this.dockPnlForm, DockState.Document);
        }

        /// <summary>
        /// 项目目标和指标窗体
        /// </summary>
        private void newAddMTGgoalsForm()
        {
            if (this.addMTGgoals == null || this.addMTGgoals.IsDisposed)
            {
                this.addMTGgoals = new AddMTGgoals();
            }
            this.addMTGgoals.TopLevel = false;
            this.addMTGgoals.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.addMTGgoals.Show(this.dockPnlForm, DockState.Document);
        }

        //公司运转

        //风险等级
        private void newRiskAssessmentForm()
        {
            if (this.riskAssessment == null || this.riskAssessment.IsDisposed)
            {
                this.riskAssessment = new RiskAssessment();
            }
            this.riskAssessment.TopLevel = false;
            this.riskAssessment.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.riskAssessment.Show(this.dockPnlForm, DockState.Document);
        }
        //物料群组定位
        private void newImportData()
        {
            if (this.importData == null || this.importData.IsDisposed)
            {
                this.importData = new importDataBtn();
            }
            this.importData.TopLevel = false;
            this.importData.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.importData.Show(this.dockPnlForm, DockState.Document);
        }



        #endregion


        #region 系统管理
        /// <summary>
        /// 用户管理
        /// </summary>
        private void userManageForm()
        {
            int managerFlag = SingleUserInstance.getCurrentUserInstance().Manager_Flag;
            if (managerFlag == 1)   //管理员
            {
                UserListForm userListForm = new UserListForm();
                userListForm.ShowDialog(this);
            }
            else     //普通用户
            {
                UserDetailInforForm userDetailInfor = new UserDetailInforForm("null","null",0, 0);
                userDetailInfor.initEditingUserinfor(SingleUserInstance.getCurrentUserInstance().User_ID, SingleUserInstance.getCurrentUserInstance().Username);
                userDetailInfor.ShowDialog(this);
            }
        }

        /// <summary>
        /// 用户组管理
        /// </summary>
        private void userGroupManageForm()
        {
            int managerFlag = SingleUserInstance.getCurrentUserInstance().Manager_Flag;
            if (managerFlag == 1)   //管理员
            {
                UserGroupManageForm userGroupManageForm = new UserGroupManageForm();
                userGroupManageForm.ShowDialog(this);
            }
            else     //普通用户
            {
                GroupListForm groupListForm = new GroupListForm();
                groupListForm.setGroupByUserId(SingleUserInstance.getCurrentUserInstance().User_ID);
                groupListForm.ShowDialog(this);
            }
        }

        /// <summary>
        /// 角色管理
        /// </summary>
        private void userRoleManageForm()
        {
            int managerFlag = SingleUserInstance.getCurrentUserInstance().Manager_Flag;
            if (managerFlag == 1)   //管理员
            {
                UserRoleManageForm userRoleManageForm = new UserRoleManageForm();
                userRoleManageForm.ShowDialog(this);
            }
            else     //普通用户
            {
                RoleListForm roleListForm = new RoleListForm();
                roleListForm.setRoleByUserId(SingleUserInstance.getCurrentUserInstance().User_ID);
                roleListForm.ShowDialog(this);
            }
        }

        /// <summary>
        /// 组织机构管理
        /// </summary>
        private void organizationManageForm()
        {
            int managerFlag = SingleUserInstance.getCurrentUserInstance().Manager_Flag;
            if (managerFlag == 1)
            {
                OrganizationManageForm organizationManageForm = new OrganizationManageForm();
                organizationManageForm.ShowDialog(this);
            }
            else
            {
                
            }
        }

        /// <summary>
        /// 备份数据
        /// </summary>
        private void backupDatabaseForm()
        {
            int managerFlag = SingleUserInstance.getCurrentUserInstance().Manager_Flag;
            if (managerFlag == 1)
            {
                BackupDatabaseForm backupDatabaseForm = new BackupDatabaseForm();
                backupDatabaseForm.ShowDialog(this);
            }
            else
            {
                FunctionLevelPermissionValidate.displayAccessDenied();
            }
        }

        /// <summary>
        /// 恢复数据
        /// </summary>
        private void restoreDatabaseForm()
        {
            int managerFlag = SingleUserInstance.getCurrentUserInstance().Manager_Flag;
            if (managerFlag == 1)
            {
                RestoreDatabaseForm restoreDatabaseForm = new RestoreDatabaseForm();
                restoreDatabaseForm.ShowDialog(this);
            }
            else
            {
                FunctionLevelPermissionValidate.displayAccessDenied();
            }
        }

        #endregion

        /// <summary>
        /// 检查进入窗口的权限
        /// </summary>
        /// <returns></returns>
        private bool validatePermission(TreeNodeMouseClickEventArgs e)
        {
            //树形菜单只检查叶子节点的权限
            if (e.Node.Nodes.Count > 0)
            {
                return true;
            }

            //系统管理 在具体函数中进行权限判断
            if (e.Node.Parent != null && e.Node.Parent.Name.Equals("systemConfigNode"))
            {
                return true;
            }

            return FunctionLevelPermissionValidate.checkEnterWindowPermission(e.Node.Name);
        }

        /// <summary>
        /// 显示在UserUI上
        /// </summary>
        /// <param name="form"></param>
        public void displayOnDockPanel(DockContent form) {
            form.TopLevel = false;
            form.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            form.Show(this.dockPnlForm, DockState.Document);
        }

        #region 状态栏操作

        /// <summary>
        /// 设置最左侧状态栏内容
        /// </summary>
        /// <param name="message">待显示内容</param>
        public void setLeftStatusLabelText(String message)
        {
            this.leftStatusLabel.Text = message;
        }

        /// <summary>
        /// 设置中间状态栏内容
        /// </summary>
        /// <param name="message">待显示内容</param>
        public void setMiddleStatusLabelText(String message)
        {
            this.middleStatusLabel.Text = message;
        }

        /// <summary>
        /// 设置最右侧状态栏内容
        /// </summary>
        /// <param name="message">待显示内容</param>
        public void setRightStatusLabelText(String message)
        {
            this.rightStatusLabel.Text = message;
        }

        /// <summary>
        /// 激活的窗口改变，清空所有状态栏信息
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dockPnlForm_ActiveContentChanged(object sender, EventArgs e)
        {
            setLeftStatusLabelText("");
            setMiddleStatusLabelText("");
            setRightStatusLabelText("");
        }

        #endregion
        /// <summary>
        /// 任务查看
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void 任务查看ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string count = sourceSupplierBll.countTaskNumberByUserId(SingleUserInstance.getCurrentUserInstance().User_ID);
            if (!String.IsNullOrEmpty(count) && int.Parse(count)>0)
            {
                TasksForm newsForm = new TasksForm();
                newsForm.ShowDialog();
            }
        }

        private void tvFunction_AfterSelect(object sender, TreeViewEventArgs e)
        {

        }
    }
}