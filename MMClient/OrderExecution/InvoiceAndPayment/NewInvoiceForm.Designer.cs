﻿namespace MMClient.OrderExecution.InvoiceAndPayment
{
    partial class NewInvoiceForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label1 = new System.Windows.Forms.Label();
            this.tbInvoiceCode = new System.Windows.Forms.TextBox();
            this.tbInvoiceNumber = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cbxSupplierName = new System.Windows.Forms.ComboBox();
            this.lblSupplierName = new System.Windows.Forms.Label();
            this.cbxSupplierID = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            this.cbxCertificateID = new System.Windows.Forms.ComboBox();
            this.tbPaymentType = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.tbMaker = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.cbxCurrencyType = new System.Windows.Forms.ComboBox();
            this.tbSum = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.tbRecipientIdentifyNumber = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.tbRecipientName = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.tbPayerIdentifyNumber = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.tbPayerName = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.dtpCreate = new System.Windows.Forms.DateTimePicker();
            this.dtpMakeOut = new System.Windows.Forms.DateTimePicker();
            this.cbxCertificateType = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.dgvItems = new System.Windows.Forms.DataGridView();
            this.MaterialID = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.MaterialName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UnitPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Quantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Sum = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Currency = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnCalculate = new System.Windows.Forms.Button();
            this.dataGridViewComboBoxColumn1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvItems)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(49, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "发票代码：";
            // 
            // tbInvoiceCode
            // 
            this.tbInvoiceCode.Location = new System.Drawing.Point(120, 26);
            this.tbInvoiceCode.Name = "tbInvoiceCode";
            this.tbInvoiceCode.Size = new System.Drawing.Size(213, 21);
            this.tbInvoiceCode.TabIndex = 1;
            // 
            // tbInvoiceNumber
            // 
            this.tbInvoiceNumber.Location = new System.Drawing.Point(455, 26);
            this.tbInvoiceNumber.Name = "tbInvoiceNumber";
            this.tbInvoiceNumber.Size = new System.Drawing.Size(213, 21);
            this.tbInvoiceNumber.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(384, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 12);
            this.label2.TabIndex = 2;
            this.label2.Text = "发票号码：";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cbxSupplierName);
            this.groupBox1.Controls.Add(this.lblSupplierName);
            this.groupBox1.Controls.Add(this.cbxSupplierID);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.cbxCertificateID);
            this.groupBox1.Controls.Add(this.tbPaymentType);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.tbMaker);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.cbxCurrencyType);
            this.groupBox1.Controls.Add(this.tbSum);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.tbRecipientIdentifyNumber);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.tbRecipientName);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.tbPayerIdentifyNumber);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.tbPayerName);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.dtpCreate);
            this.groupBox1.Controls.Add(this.dtpMakeOut);
            this.groupBox1.Controls.Add(this.cbxCertificateType);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.tbInvoiceNumber);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.tbInvoiceCode);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(789, 437);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "发票明细";
            // 
            // cbxSupplierName
            // 
            this.cbxSupplierName.FormattingEnabled = true;
            this.cbxSupplierName.Location = new System.Drawing.Point(455, 148);
            this.cbxSupplierName.Name = "cbxSupplierName";
            this.cbxSupplierName.Size = new System.Drawing.Size(213, 20);
            this.cbxSupplierName.TabIndex = 34;
            // 
            // lblSupplierName
            // 
            this.lblSupplierName.AutoSize = true;
            this.lblSupplierName.Location = new System.Drawing.Point(372, 151);
            this.lblSupplierName.Name = "lblSupplierName";
            this.lblSupplierName.Size = new System.Drawing.Size(77, 12);
            this.lblSupplierName.TabIndex = 33;
            this.lblSupplierName.Text = "供应商名称：";
            // 
            // cbxSupplierID
            // 
            this.cbxSupplierID.FormattingEnabled = true;
            this.cbxSupplierID.Location = new System.Drawing.Point(142, 148);
            this.cbxSupplierID.Name = "cbxSupplierID";
            this.cbxSupplierID.Size = new System.Drawing.Size(191, 20);
            this.cbxSupplierID.TabIndex = 32;
            this.cbxSupplierID.SelectedIndexChanged += new System.EventHandler(this.cbxSupplierID_SelectedIndexChanged);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(49, 151);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(77, 12);
            this.label15.TabIndex = 31;
            this.label15.Text = "供应商编号：";
            // 
            // cbxCertificateID
            // 
            this.cbxCertificateID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxCertificateID.FormattingEnabled = true;
            this.cbxCertificateID.Items.AddRange(new object[] {
            "CGDD2016031715430201"});
            this.cbxCertificateID.Location = new System.Drawing.Point(455, 65);
            this.cbxCertificateID.Name = "cbxCertificateID";
            this.cbxCertificateID.Size = new System.Drawing.Size(213, 20);
            this.cbxCertificateID.TabIndex = 30;
            this.cbxCertificateID.SelectedIndexChanged += new System.EventHandler(this.cbxCertificateID_SelectedIndexChanged);
            // 
            // tbPaymentType
            // 
            this.tbPaymentType.Location = new System.Drawing.Point(142, 345);
            this.tbPaymentType.Name = "tbPaymentType";
            this.tbPaymentType.Size = new System.Drawing.Size(526, 21);
            this.tbPaymentType.TabIndex = 29;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(49, 348);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(65, 12);
            this.label13.TabIndex = 28;
            this.label13.Text = "付款条件：";
            // 
            // tbMaker
            // 
            this.tbMaker.Location = new System.Drawing.Point(455, 382);
            this.tbMaker.Name = "tbMaker";
            this.tbMaker.Size = new System.Drawing.Size(213, 21);
            this.tbMaker.TabIndex = 27;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(384, 385);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(65, 12);
            this.label12.TabIndex = 26;
            this.label12.Text = "开票人员：";
            // 
            // cbxCurrencyType
            // 
            this.cbxCurrencyType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxCurrencyType.FormattingEnabled = true;
            this.cbxCurrencyType.Location = new System.Drawing.Point(286, 382);
            this.cbxCurrencyType.Name = "cbxCurrencyType";
            this.cbxCurrencyType.Size = new System.Drawing.Size(69, 20);
            this.cbxCurrencyType.TabIndex = 25;
            this.cbxCurrencyType.SelectedIndexChanged += new System.EventHandler(this.cbxCurrencyType_SelectedIndexChanged);
            // 
            // tbSum
            // 
            this.tbSum.Enabled = false;
            this.tbSum.Location = new System.Drawing.Point(142, 382);
            this.tbSum.Name = "tbSum";
            this.tbSum.Size = new System.Drawing.Size(136, 21);
            this.tbSum.TabIndex = 24;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(49, 385);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(65, 12);
            this.label11.TabIndex = 23;
            this.label11.Text = "合计金额：";
            // 
            // tbRecipientIdentifyNumber
            // 
            this.tbRecipientIdentifyNumber.Location = new System.Drawing.Point(142, 307);
            this.tbRecipientIdentifyNumber.Name = "tbRecipientIdentifyNumber";
            this.tbRecipientIdentifyNumber.Size = new System.Drawing.Size(526, 21);
            this.tbRecipientIdentifyNumber.TabIndex = 22;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(49, 310);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(89, 12);
            this.label10.TabIndex = 21;
            this.label10.Text = "收款方识别号：";
            // 
            // tbRecipientName
            // 
            this.tbRecipientName.Location = new System.Drawing.Point(142, 268);
            this.tbRecipientName.Name = "tbRecipientName";
            this.tbRecipientName.Size = new System.Drawing.Size(526, 21);
            this.tbRecipientName.TabIndex = 20;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(49, 271);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(77, 12);
            this.label9.TabIndex = 19;
            this.label9.Text = "收款方名称：";
            // 
            // tbPayerIdentifyNumber
            // 
            this.tbPayerIdentifyNumber.Location = new System.Drawing.Point(142, 228);
            this.tbPayerIdentifyNumber.Name = "tbPayerIdentifyNumber";
            this.tbPayerIdentifyNumber.Size = new System.Drawing.Size(526, 21);
            this.tbPayerIdentifyNumber.TabIndex = 18;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(49, 231);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(89, 12);
            this.label8.TabIndex = 17;
            this.label8.Text = "付款方识别号：";
            // 
            // tbPayerName
            // 
            this.tbPayerName.Location = new System.Drawing.Point(142, 189);
            this.tbPayerName.Name = "tbPayerName";
            this.tbPayerName.Size = new System.Drawing.Size(526, 21);
            this.tbPayerName.TabIndex = 16;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(49, 192);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(77, 12);
            this.label7.TabIndex = 15;
            this.label7.Text = "付款方名称：";
            // 
            // dtpCreate
            // 
            this.dtpCreate.Enabled = false;
            this.dtpCreate.Location = new System.Drawing.Point(455, 105);
            this.dtpCreate.Name = "dtpCreate";
            this.dtpCreate.Size = new System.Drawing.Size(213, 21);
            this.dtpCreate.TabIndex = 14;
            // 
            // dtpMakeOut
            // 
            this.dtpMakeOut.Location = new System.Drawing.Point(120, 105);
            this.dtpMakeOut.Name = "dtpMakeOut";
            this.dtpMakeOut.Size = new System.Drawing.Size(213, 21);
            this.dtpMakeOut.TabIndex = 13;
            // 
            // cbxCertificateType
            // 
            this.cbxCertificateType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxCertificateType.FormattingEnabled = true;
            this.cbxCertificateType.Items.AddRange(new object[] {
            "标准采购订单"});
            this.cbxCertificateType.Location = new System.Drawing.Point(120, 65);
            this.cbxCertificateType.Name = "cbxCertificateType";
            this.cbxCertificateType.Size = new System.Drawing.Size(213, 20);
            this.cbxCertificateType.TabIndex = 12;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(384, 111);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(65, 12);
            this.label6.TabIndex = 10;
            this.label6.Text = "创建日期：";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(49, 111);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(65, 12);
            this.label5.TabIndex = 8;
            this.label5.Text = "开票日期：";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(384, 68);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 12);
            this.label4.TabIndex = 6;
            this.label4.Text = "凭证编号：";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(49, 68);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 12);
            this.label3.TabIndex = 4;
            this.label3.Text = "凭证类型：";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(29, 458);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(41, 12);
            this.label14.TabIndex = 5;
            this.label14.Text = "项目：";
            // 
            // dgvItems
            // 
            this.dgvItems.AllowUserToAddRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvItems.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvItems.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvItems.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.MaterialID,
            this.MaterialName,
            this.UnitPrice,
            this.Quantity,
            this.Sum,
            this.Currency});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvItems.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvItems.Location = new System.Drawing.Point(12, 491);
            this.dgvItems.Name = "dgvItems";
            this.dgvItems.RowHeadersVisible = false;
            this.dgvItems.RowTemplate.Height = 23;
            this.dgvItems.Size = new System.Drawing.Size(699, 174);
            this.dgvItems.TabIndex = 6;
            this.dgvItems.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgvItems_EditingControlShowing);
            // 
            // MaterialID
            // 
            this.MaterialID.HeaderText = "物料编号";
            this.MaterialID.Name = "MaterialID";
            this.MaterialID.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.MaterialID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.MaterialID.Width = 150;
            // 
            // MaterialName
            // 
            this.MaterialName.HeaderText = "物料名称";
            this.MaterialName.Name = "MaterialName";
            this.MaterialName.Width = 140;
            // 
            // UnitPrice
            // 
            this.UnitPrice.HeaderText = "单价";
            this.UnitPrice.Name = "UnitPrice";
            // 
            // Quantity
            // 
            this.Quantity.HeaderText = "数量";
            this.Quantity.Name = "Quantity";
            // 
            // Sum
            // 
            this.Sum.HeaderText = "金额";
            this.Sum.Name = "Sum";
            // 
            // Currency
            // 
            this.Currency.HeaderText = "货币";
            this.Currency.Name = "Currency";
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(350, 453);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(75, 23);
            this.btnAdd.TabIndex = 7;
            this.btnAdd.Text = "增加";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(438, 453);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(75, 23);
            this.btnDelete.TabIndex = 8;
            this.btnDelete.Text = "删除";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(613, 453);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 9;
            this.btnSave.Text = "保存";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnCalculate
            // 
            this.btnCalculate.Location = new System.Drawing.Point(527, 453);
            this.btnCalculate.Name = "btnCalculate";
            this.btnCalculate.Size = new System.Drawing.Size(75, 23);
            this.btnCalculate.TabIndex = 10;
            this.btnCalculate.Text = "计算";
            this.btnCalculate.UseVisualStyleBackColor = true;
            this.btnCalculate.Click += new System.EventHandler(this.btnCalculate_Click);
            // 
            // dataGridViewComboBoxColumn1
            // 
            this.dataGridViewComboBoxColumn1.HeaderText = "物料编号";
            this.dataGridViewComboBoxColumn1.Name = "dataGridViewComboBoxColumn1";
            this.dataGridViewComboBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewComboBoxColumn1.Width = 150;
            // 
            // NewInvoiceForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(789, 674);
            this.Controls.Add(this.btnCalculate);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.dgvItems);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "NewInvoiceForm";
            this.Text = "新建发票";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvItems)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbInvoiceCode;
        private System.Windows.Forms.TextBox tbInvoiceNumber;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbRecipientIdentifyNumber;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox tbRecipientName;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox tbPayerIdentifyNumber;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox tbPayerName;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DateTimePicker dtpCreate;
        private System.Windows.Forms.DateTimePicker dtpMakeOut;
        private System.Windows.Forms.ComboBox cbxCertificateType;
        private System.Windows.Forms.TextBox tbPaymentType;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox tbMaker;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox cbxCurrencyType;
        private System.Windows.Forms.TextBox tbSum;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.DataGridView dgvItems;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnCalculate;
        private System.Windows.Forms.ComboBox cbxCertificateID;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label lblSupplierName;
        private System.Windows.Forms.ComboBox cbxSupplierID;
        private System.Windows.Forms.ComboBox cbxSupplierName;
        private System.Windows.Forms.DataGridViewComboBoxColumn MaterialID;
        private System.Windows.Forms.DataGridViewTextBoxColumn MaterialName;
        private System.Windows.Forms.DataGridViewTextBoxColumn UnitPrice;
        private System.Windows.Forms.DataGridViewTextBoxColumn Quantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn Sum;
        private System.Windows.Forms.DataGridViewTextBoxColumn Currency;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn1;
    }
}