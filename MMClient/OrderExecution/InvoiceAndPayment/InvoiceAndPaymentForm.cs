﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MMClient.ContractManage;
using WeifenLuo.WinFormsUI.Docking;
using Lib.Common.CommonUtils;
using Lib.Model.PurchaseOrderExecution;
using Lib.Bll.PurchaseOrderExecutionBLL;

namespace MMClient.OrderExecution.InvoiceAndPayment
{
    public partial class InvoiceAndPaymentForm : DockContent
    {
        //记录查询结果
        private List<Invoice> invoiceList = new List<Invoice>();
        //业务逻辑处理
        private InvoiceBLL invoiceTool = new InvoiceBLL();
        NewInvoiceForm newInvoiceForm = null;
        NewInvoiceForm editinvoiceForm = null;
        NewInvoiceForm previewinvoiceForm = null;
        public InvoiceAndPaymentForm()
        {
            InitializeComponent();

            this.cbxCertificateID.SelectedIndex = 0;
            this.cbxStatus.SelectedIndex = 0;
        }

        /// <summary>
        /// 新建发票界面
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tspAdd_Click(object sender, EventArgs e)
        {
            if(newInvoiceForm == null || newInvoiceForm.IsDisposed)
            {
                newInvoiceForm = new NewInvoiceForm();
            }
            newInvoiceForm.Show();
        }

        /// <summary>
        /// 搜索
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSearch_Click(object sender, EventArgs e)
        {
            //先看开始时间和截止时间
            DateTime beginTime = this.dtpBeginTime.Value;
            DateTime endTime = this.dtpEndTime.Value;

            if (beginTime > endTime)
            {
                MessageUtil.ShowError("请正确选择起止日期");
                return;
            }

            string invoiceCode = this.tbInvoiceCode.Text.Trim();
            string invoiceNumber = this.tbInvoiceNumber.Text.Trim();
            string certificateID = this.cbxCertificateID.Text;
            if (certificateID.Equals("全部订单"))
                certificateID = "";
            string status = this.cbxStatus.Text;
            if (status.Equals("全部状态"))
                status = "";
            
            Dictionary<string, object> conditions = new Dictionary<string, object>();
            conditions.Add("Invoice_Code", invoiceCode);
            conditions.Add("Invoice_Number", invoiceNumber);
            conditions.Add("Certificate_Code", certificateID);
            conditions.Add("Status", status);

            invoiceList = invoiceTool.getInvoiceListByConditions(conditions, beginTime, endTime);
            displayList(invoiceList);
        }

        /// <summary>
        /// 展示
        /// </summary>
        /// <param name="items"></param>
        private void displayList(List<Invoice> items) {
            this.dgvItems.Rows.Clear();
            int i = 0;
            foreach (Invoice invoice in items) {
                this.dgvItems.Rows.Add();
                DataGridViewRow row = this.dgvItems.Rows[i];
                row.Cells[1].Value = invoice.Invoice_Code;
                row.Cells[2].Value = invoice.Invoice_Number;
                row.Cells[3].Value = invoice.Certificate_Type;
                row.Cells[4].Value = invoice.Certificate_ID;
                row.Cells[5].Value = invoice.Create_Time;

                i++;
            }
        }

        /// <summary>
        /// 容差设置
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tsbToleranceSetting_Click(object sender, EventArgs e)
        {
            InvoiceToleranceSettingForm toleranceForm = new InvoiceToleranceSettingForm();
            toleranceForm.ShowDialog();
        }

        /// <summary>
        /// 查看
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tsbPreview_Click(object sender, EventArgs e)
        {
            int num = -1;
            for (int i = 0; i < this.dgvItems.Rows.Count; i++)
            {
                DataGridViewRow row = this.dgvItems.Rows[i];
                string isSelected = row.Cells[0].EditedFormattedValue.ToString();
                if (isSelected.Equals("True"))
                {
                    if (num == -1)
                        num = i;
                    else {
                        MessageUtil.ShowError("不要选择多项！");
                        return;
                    }
                }
            }
            if (num < 0 || num >= this.dgvItems.Rows.Count) {
                MessageUtil.ShowError("没有选择预览项！");
                return;
            }

            Invoice invoice = invoiceList[num];

            if(previewinvoiceForm == null || previewinvoiceForm.IsDisposed)
            {
                previewinvoiceForm = new NewInvoiceForm(invoice, "preview");
                
            }
            previewinvoiceForm.Show();
        }

        /// <summary>
        /// 点击编辑
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tsbEdit_Click(object sender, EventArgs e)
        {
            int num = -1;
            for (int i = 0; i < this.dgvItems.Rows.Count; i++)
            {
                DataGridViewRow row = this.dgvItems.Rows[i];
                string isSelected = row.Cells[0].EditedFormattedValue.ToString();
                if (isSelected.Equals("True"))
                {
                    if (num == -1)
                        num = i;
                    else
                    {
                        MessageUtil.ShowError("不要选择多项！");
                        return;
                    }
                }
            }
            if (num < 0 || num >= this.dgvItems.Rows.Count)
            {
                MessageUtil.ShowError("没有选择编辑项！");
                return;
            }

            Invoice invoice = invoiceList[num];
            if(editinvoiceForm ==null || editinvoiceForm.IsDisposed)
            {
                editinvoiceForm = new NewInvoiceForm(invoice, "edit");
            }
            editinvoiceForm.Show();
        }

        /// <summary>
        /// 校验计算
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tspCheck_Click(object sender, EventArgs e)
        {
            // 计算前提是所选的所有发票均为同一订单
            List<Invoice> checkList = new List<Invoice>();
            for (int i = 0; i < this.dgvItems.Rows.Count; i++) {
                DataGridViewRow row = this.dgvItems.Rows[i];
                string isSelected = row.Cells[0].EditedFormattedValue.ToString();
                if (isSelected.Equals("True"))
                {
                    checkList.Add(invoiceList[i]);
                }
            }
            if (checkList.Count == 0) {
                MessageUtil.ShowError("请选择校验发票!");
                return;
            }

            string POID = this.cbxCertificateID.Text;
            if (POID.Equals("全部订单")) {
                MessageUtil.ShowError("请选择校验订单!");
                return;
            }
            foreach (Invoice invoice in checkList) {
                if (!invoice.Certificate_ID.Equals(POID)) {
                    MessageUtil.ShowError("请选择同一订单发票!");
                }
            }
            //获取对应订单的相关信息

            InvoiceCheckResultForm checkResultForm = new InvoiceCheckResultForm(checkList, POID);
            checkResultForm.ShowDialog();
        }

        private void tsbDelete_Click(object sender, EventArgs e)
        {
            List<Invoice> checkList = new List<Invoice>();
            for (int i = 0; i < this.dgvItems.Rows.Count; i++)
            {
                DataGridViewRow row = this.dgvItems.Rows[i];
                string isSelected = row.Cells[i].EditedFormattedValue.ToString();
                if (isSelected.Equals("True"))
                {
                    checkList.Add(invoiceList[i]);
                }
            }
            if (checkList.Count == 0)
            {
                MessageUtil.ShowError("请选择要删除的发票!");
                return;
            }

            var confirmResult = MessageUtil.ShowOKCancelAndQuestion("确定删除选定的发票?");
            if (confirmResult == DialogResult.Cancel)
            {
                return;
            }
            else
            {
                int num = 0;
                foreach (Invoice invoice in checkList)
                {
                    num += this.invoiceTool.deleteInvoice(invoice.Invoice_ID);
                }
                if (num > 0)
                {
                    MessageBox.Show("删除成功");
                }
                else
                {
                    MessageBox.Show("删除失败");
                }
                this.btnSearch.PerformClick();
            }
        }

        private void cbxCertificateID_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
