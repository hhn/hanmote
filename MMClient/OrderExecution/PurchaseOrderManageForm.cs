﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Common.CommonUtils;
using WeifenLuo.WinFormsUI.Docking;
using Lib.SqlServerDAL;

namespace MMClient.OrderExecution
{
    public partial class PurchaseOrderManageForm : DockContent
    {
        public PurchaseOrderManageForm()
        {
            InitializeComponent();
            //订单状态ComboBox初始化
            this.cbxStatus.SelectedIndex = 0;
            //订单类型ComboBox初始化
            this.cbxPOType.SelectedIndex = 0;
        }

        /// <summary>
        /// 点击搜索
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSearch_Click(object sender, EventArgs e)
        {
            //先看开始时间和截止时间
            DateTime beginTime = this.dtpBeginTime.Value;
            DateTime endTime = this.dtpEndTime.Value;

            if (beginTime > endTime)
            {
                MessageUtil.ShowError("请正确选择起止日期");
                return;
            }

            StringBuilder sqlStr = new StringBuilder("select * from Summary_Demand ")
                .Append("where Apply_Date >= '").Append(beginTime.ToString())
                .Append("' and Apply_Date <= '").Append(endTime.ToString())
                .Append("'");
            if (!this.tbPOID.Text.Trim().Equals("")) {
                sqlStr.Append(" and Demand_ID = '")
                    .Append(this.tbPOID.Text.Trim())
                    .Append("'");
            }
            if (!this.tbSupplierID.Text.Trim().Equals("")) {
                sqlStr.Append(" and Supplier_ID = '")
                    .Append(this.tbSupplierID.Text.Trim())
                    .Append("'");
            }
            if (!this.cbxPOType.Text.Equals("全部")) {
                sqlStr.Append(" and Purchase_Type = '")
                    .Append(this.cbxPOType.Text)
                    .Append("'");
            }
            if (!this.cbxStatus.Text.Equals("全部")) {
                sqlStr.Append(" and State = '")
                    .Append(this.cbxStatus.Text)
                    .Append("'");
            }

            DataTable resultDt = DBHelper.ExecuteQueryDT(sqlStr.ToString());
            //显示查询结果
            this.dgvPOInfo.Rows.Clear();
            foreach (DataRow row in resultDt.Rows) {
                this.dgvPOInfo.Rows.Add();
                DataGridViewRow dgvRow = this.dgvPOInfo.Rows[this.dgvPOInfo.Rows.Count - 1];
                dgvRow.Cells[1].Value = row["Demand_ID"].ToString();
                dgvRow.Cells[2].Value = row["Purchase_Type"].ToString();
                dgvRow.Cells[3].Value = row["State"].ToString();
                dgvRow.Cells[4].Value = row["Supplier_ID"].ToString();
                dgvRow.Cells[5].Value = row["Apply_Date"].ToString();
            }
        }

        /// <summary>
        /// 点击删除按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tsbDelete_Click(object sender, EventArgs e)
        {
            List<string> POIDList = new List<string>();
            foreach (DataGridViewRow row in this.dgvPOInfo.Rows)
            {
                string isSelected = row.Cells[0].EditedFormattedValue.ToString();
                if (isSelected.Equals("True"))
                {
                    POIDList.Add(row.Cells[1].Value.ToString());
                }
            }

            if (POIDList.Count == 0)
            {
                MessageUtil.ShowError("没有选中的订单");
                return;
            }

            var confirmResult = MessageUtil.ShowWarning("确定删除选定的订单?");
            if (confirmResult == DialogResult.Cancel)
            {
                return;
            }

            StringBuilder strBui = new StringBuilder("delete from Summary_Demand where Demand_ID = '")
                .Append(POIDList[0]).Append("'");
            int deleteResult = DBHelper.ExecuteNonQuery(strBui.ToString());

            if (deleteResult > 0)
            {
                MessageBox.Show("删除成功");
            }
            else
            {
                MessageBox.Show("删除失败");
            }

            this.btnSearch.PerformClick();
        }

        /// <summary>
        /// 点击审核
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tsbCheck_Click(object sender, EventArgs e)
        {
            List<string> POIDList = new List<string>();

            //agreementContractList中保存的记录顺序和显示的记录顺序应该是一致的
            int i = 0;
            foreach (DataGridViewRow row in this.dgvPOInfo.Rows)
            {
                string isSelected = row.Cells[0].EditedFormattedValue.ToString();
                if (isSelected.Equals("True"))
                {
                    string status = row.Cells[3].Value.ToString();
                    if (!status.Equals("待审核"))
                    {
                        MessageUtil.ShowError("请正确选择订单");
                        return;
                    }

                    POIDList.Add(row.Cells[3].Value.ToString());
                }

                i++;
            }

            if (POIDList.Count == 0)
            {
                MessageUtil.ShowError("请选择要审核的订单");
                return;
            }

            //确认
            var result = MessageUtil.ShowOKCancelAndQuestion("确认审核订单？");
            if (result == DialogResult.Cancel)
            {
                return;
            }

            //审核所有的合同
            foreach (string POID in POIDList)
            {
                string newStatus = "待供应商确认";
                string sqlText = "update Summary_Demand set State = '" + newStatus
                    + "' where Demand_ID = '"
                    + POID
                    + "'";
                DBHelper.ExecuteNonQuery(sqlText);
            }

            MessageBox.Show("审核成功");
            
            this.btnSearch.PerformClick();
        }

        /// <summary>
        /// 点击查看
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tsbPreview_Click(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// 点击修改
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tsbContractEdit_Click(object sender, EventArgs e)
        {

        }
    }
}
