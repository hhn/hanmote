namespace MMClient
{
    partial class UserUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserUI));
            WeifenLuo.WinFormsUI.Docking.DockPanelSkin dockPanelSkin2 = new WeifenLuo.WinFormsUI.Docking.DockPanelSkin();
            WeifenLuo.WinFormsUI.Docking.AutoHideStripSkin autoHideStripSkin2 = new WeifenLuo.WinFormsUI.Docking.AutoHideStripSkin();
            WeifenLuo.WinFormsUI.Docking.DockPanelGradient dockPanelGradient4 = new WeifenLuo.WinFormsUI.Docking.DockPanelGradient();
            WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient8 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
            WeifenLuo.WinFormsUI.Docking.DockPaneStripSkin dockPaneStripSkin2 = new WeifenLuo.WinFormsUI.Docking.DockPaneStripSkin();
            WeifenLuo.WinFormsUI.Docking.DockPaneStripGradient dockPaneStripGradient2 = new WeifenLuo.WinFormsUI.Docking.DockPaneStripGradient();
            WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient9 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
            WeifenLuo.WinFormsUI.Docking.DockPanelGradient dockPanelGradient5 = new WeifenLuo.WinFormsUI.Docking.DockPanelGradient();
            WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient10 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
            WeifenLuo.WinFormsUI.Docking.DockPaneStripToolWindowGradient dockPaneStripToolWindowGradient2 = new WeifenLuo.WinFormsUI.Docking.DockPaneStripToolWindowGradient();
            WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient11 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
            WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient12 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
            WeifenLuo.WinFormsUI.Docking.DockPanelGradient dockPanelGradient6 = new WeifenLuo.WinFormsUI.Docking.DockPanelGradient();
            WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient13 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
            WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient14 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
            System.Windows.Forms.TreeNode treeNode254 = new System.Windows.Forms.TreeNode("新建物料数据");
            System.Windows.Forms.TreeNode treeNode255 = new System.Windows.Forms.TreeNode("基本视图");
            System.Windows.Forms.TreeNode treeNode256 = new System.Windows.Forms.TreeNode("会计视图");
            System.Windows.Forms.TreeNode treeNode257 = new System.Windows.Forms.TreeNode("采购视图");
            System.Windows.Forms.TreeNode treeNode258 = new System.Windows.Forms.TreeNode("存储视图");
            System.Windows.Forms.TreeNode treeNode259 = new System.Windows.Forms.TreeNode("建立批次");
            System.Windows.Forms.TreeNode treeNode260 = new System.Windows.Forms.TreeNode("查询批次");
            System.Windows.Forms.TreeNode treeNode261 = new System.Windows.Forms.TreeNode("批次管理", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode259,
            treeNode260});
            System.Windows.Forms.TreeNode treeNode262 = new System.Windows.Forms.TreeNode("分类视图");
            System.Windows.Forms.TreeNode treeNode263 = new System.Windows.Forms.TreeNode("物料主数据", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode254,
            treeNode255,
            treeNode256,
            treeNode257,
            treeNode258,
            treeNode261,
            treeNode262});
            System.Windows.Forms.TreeNode treeNode264 = new System.Windows.Forms.TreeNode("基本视图");
            System.Windows.Forms.TreeNode treeNode265 = new System.Windows.Forms.TreeNode("采购组织视图");
            System.Windows.Forms.TreeNode treeNode266 = new System.Windows.Forms.TreeNode("公司代码视图");
            System.Windows.Forms.TreeNode treeNode267 = new System.Windows.Forms.TreeNode("创建供应商编码");
            System.Windows.Forms.TreeNode treeNode268 = new System.Windows.Forms.TreeNode("供应商主数据", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode264,
            treeNode265,
            treeNode266,
            treeNode267});
            System.Windows.Forms.TreeNode treeNode269 = new System.Windows.Forms.TreeNode("生成会计凭证");
            System.Windows.Forms.TreeNode treeNode270 = new System.Windows.Forms.TreeNode("财务主数据", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode269});
            System.Windows.Forms.TreeNode treeNode271 = new System.Windows.Forms.TreeNode("维护物料组");
            System.Windows.Forms.TreeNode treeNode272 = new System.Windows.Forms.TreeNode("维护物料类型");
            System.Windows.Forms.TreeNode treeNode273 = new System.Windows.Forms.TreeNode("维护货币");
            System.Windows.Forms.TreeNode treeNode274 = new System.Windows.Forms.TreeNode("维护国家信息");
            System.Windows.Forms.TreeNode treeNode275 = new System.Windows.Forms.TreeNode("维护付款条件");
            System.Windows.Forms.TreeNode treeNode276 = new System.Windows.Forms.TreeNode("维护计量单位");
            System.Windows.Forms.TreeNode treeNode277 = new System.Windows.Forms.TreeNode("维护评估类");
            System.Windows.Forms.TreeNode treeNode278 = new System.Windows.Forms.TreeNode("维护公司代码");
            System.Windows.Forms.TreeNode treeNode279 = new System.Windows.Forms.TreeNode("维护工厂");
            System.Windows.Forms.TreeNode treeNode280 = new System.Windows.Forms.TreeNode("维护库存地");
            System.Windows.Forms.TreeNode treeNode281 = new System.Windows.Forms.TreeNode("维护采购组");
            System.Windows.Forms.TreeNode treeNode282 = new System.Windows.Forms.TreeNode("维护采购组织");
            System.Windows.Forms.TreeNode treeNode283 = new System.Windows.Forms.TreeNode("维护产品组");
            System.Windows.Forms.TreeNode treeNode284 = new System.Windows.Forms.TreeNode("维护交付条件");
            System.Windows.Forms.TreeNode treeNode285 = new System.Windows.Forms.TreeNode("维护成本中心");
            System.Windows.Forms.TreeNode treeNode286 = new System.Windows.Forms.TreeNode("维护工业标识");
            System.Windows.Forms.TreeNode treeNode287 = new System.Windows.Forms.TreeNode("采购组织分配物料组");
            System.Windows.Forms.TreeNode treeNode288 = new System.Windows.Forms.TreeNode("维护评估要素");
            System.Windows.Forms.TreeNode treeNode289 = new System.Windows.Forms.TreeNode("维护目标值和最低值");
            System.Windows.Forms.TreeNode treeNode290 = new System.Windows.Forms.TreeNode("维护策略信息库");
            System.Windows.Forms.TreeNode treeNode291 = new System.Windows.Forms.TreeNode("维护证书类型");
            System.Windows.Forms.TreeNode treeNode292 = new System.Windows.Forms.TreeNode("维护税率");
            System.Windows.Forms.TreeNode treeNode293 = new System.Windows.Forms.TreeNode("维护记录类型");
            System.Windows.Forms.TreeNode treeNode294 = new System.Windows.Forms.TreeNode("维护号码段");
            System.Windows.Forms.TreeNode treeNode295 = new System.Windows.Forms.TreeNode("维护账户组");
            System.Windows.Forms.TreeNode treeNode296 = new System.Windows.Forms.TreeNode("给采购组织指定工厂");
            System.Windows.Forms.TreeNode treeNode297 = new System.Windows.Forms.TreeNode("维护绩效评估模板");
            System.Windows.Forms.TreeNode treeNode298 = new System.Windows.Forms.TreeNode("一般设置", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode271,
            treeNode272,
            treeNode273,
            treeNode274,
            treeNode275,
            treeNode276,
            treeNode277,
            treeNode278,
            treeNode279,
            treeNode280,
            treeNode281,
            treeNode282,
            treeNode283,
            treeNode284,
            treeNode285,
            treeNode286,
            treeNode287,
            treeNode288,
            treeNode289,
            treeNode290,
            treeNode291,
            treeNode292,
            treeNode293,
            treeNode294,
            treeNode295,
            treeNode296,
            treeNode297});
            System.Windows.Forms.TreeNode treeNode299 = new System.Windows.Forms.TreeNode("数据管理", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode263,
            treeNode268,
            treeNode270,
            treeNode298});
            System.Windows.Forms.TreeNode treeNode300 = new System.Windows.Forms.TreeNode("采购管理");
            System.Windows.Forms.TreeNode treeNode301 = new System.Windows.Forms.TreeNode("采购审核");
            System.Windows.Forms.TreeNode treeNode302 = new System.Windows.Forms.TreeNode("采购汇总");
            System.Windows.Forms.TreeNode treeNode303 = new System.Windows.Forms.TreeNode("查看采购计划");
            System.Windows.Forms.TreeNode treeNode304 = new System.Windows.Forms.TreeNode("计划管理", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode300,
            treeNode301,
            treeNode302,
            treeNode303});
            System.Windows.Forms.TreeNode treeNode305 = new System.Windows.Forms.TreeNode("查看询价");
            System.Windows.Forms.TreeNode treeNode306 = new System.Windows.Forms.TreeNode("创建询价");
            System.Windows.Forms.TreeNode treeNode307 = new System.Windows.Forms.TreeNode("更改询价");
            System.Windows.Forms.TreeNode treeNode308 = new System.Windows.Forms.TreeNode("执行询价");
            System.Windows.Forms.TreeNode treeNode309 = new System.Windows.Forms.TreeNode("消息");
            System.Windows.Forms.TreeNode treeNode310 = new System.Windows.Forms.TreeNode("创建询价单");
            System.Windows.Forms.TreeNode treeNode311 = new System.Windows.Forms.TreeNode("查看询价单");
            System.Windows.Forms.TreeNode treeNode312 = new System.Windows.Forms.TreeNode("分配供应商");
            System.Windows.Forms.TreeNode treeNode313 = new System.Windows.Forms.TreeNode("查看供应商");
            System.Windows.Forms.TreeNode treeNode314 = new System.Windows.Forms.TreeNode("横向比较");
            System.Windows.Forms.TreeNode treeNode315 = new System.Windows.Forms.TreeNode("纵向比较");
            System.Windows.Forms.TreeNode treeNode316 = new System.Windows.Forms.TreeNode("标杆比较");
            System.Windows.Forms.TreeNode treeNode317 = new System.Windows.Forms.TreeNode("报价比较", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode314,
            treeNode315,
            treeNode316});
            System.Windows.Forms.TreeNode treeNode318 = new System.Windows.Forms.TreeNode("成本分析询价", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode309,
            treeNode310,
            treeNode311,
            treeNode312,
            treeNode313,
            treeNode317});
            System.Windows.Forms.TreeNode treeNode319 = new System.Windows.Forms.TreeNode("询价", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode305,
            treeNode306,
            treeNode307,
            treeNode308,
            treeNode318});
            System.Windows.Forms.TreeNode treeNode320 = new System.Windows.Forms.TreeNode("维护报价");
            System.Windows.Forms.TreeNode treeNode321 = new System.Windows.Forms.TreeNode("执行比价");
            System.Windows.Forms.TreeNode treeNode322 = new System.Windows.Forms.TreeNode("报价", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode320,
            treeNode321});
            System.Windows.Forms.TreeNode treeNode323 = new System.Windows.Forms.TreeNode("询/报价", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode319,
            treeNode322});
            System.Windows.Forms.TreeNode treeNode324 = new System.Windows.Forms.TreeNode("查看招标");
            System.Windows.Forms.TreeNode treeNode325 = new System.Windows.Forms.TreeNode("创建招标");
            System.Windows.Forms.TreeNode treeNode326 = new System.Windows.Forms.TreeNode("发布招标");
            System.Windows.Forms.TreeNode treeNode327 = new System.Windows.Forms.TreeNode("修改招标");
            System.Windows.Forms.TreeNode treeNode328 = new System.Windows.Forms.TreeNode("审批招标");
            System.Windows.Forms.TreeNode treeNode329 = new System.Windows.Forms.TreeNode("处理投标");
            System.Windows.Forms.TreeNode treeNode330 = new System.Windows.Forms.TreeNode("进行评标");
            System.Windows.Forms.TreeNode treeNode331 = new System.Windows.Forms.TreeNode("查看谈判");
            System.Windows.Forms.TreeNode treeNode332 = new System.Windows.Forms.TreeNode("创建谈判");
            System.Windows.Forms.TreeNode treeNode333 = new System.Windows.Forms.TreeNode("处理谈判");
            System.Windows.Forms.TreeNode treeNode334 = new System.Windows.Forms.TreeNode("谈判", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode331,
            treeNode332,
            treeNode333});
            System.Windows.Forms.TreeNode treeNode335 = new System.Windows.Forms.TreeNode("招标", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode324,
            treeNode325,
            treeNode326,
            treeNode327,
            treeNode328,
            treeNode329,
            treeNode330,
            treeNode334});
            System.Windows.Forms.TreeNode treeNode336 = new System.Windows.Forms.TreeNode("竞价");
            System.Windows.Forms.TreeNode treeNode337 = new System.Windows.Forms.TreeNode("竞拍");
            System.Windows.Forms.TreeNode treeNode338 = new System.Windows.Forms.TreeNode("拍卖", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode336,
            treeNode337});
            System.Windows.Forms.TreeNode treeNode339 = new System.Windows.Forms.TreeNode("寻源方式", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode323,
            treeNode335,
            treeNode338});
            System.Windows.Forms.TreeNode treeNode340 = new System.Windows.Forms.TreeNode("清单列表");
            System.Windows.Forms.TreeNode treeNode341 = new System.Windows.Forms.TreeNode("新建源清单");
            System.Windows.Forms.TreeNode treeNode342 = new System.Windows.Forms.TreeNode("更改源清单");
            System.Windows.Forms.TreeNode treeNode343 = new System.Windows.Forms.TreeNode("货源清单", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode340,
            treeNode341,
            treeNode342});
            System.Windows.Forms.TreeNode treeNode344 = new System.Windows.Forms.TreeNode("条件类型");
            System.Windows.Forms.TreeNode treeNode345 = new System.Windows.Forms.TreeNode("查看条件类型");
            System.Windows.Forms.TreeNode treeNode346 = new System.Windows.Forms.TreeNode("创建条件类型");
            System.Windows.Forms.TreeNode treeNode347 = new System.Windows.Forms.TreeNode("更改条件类型");
            System.Windows.Forms.TreeNode treeNode348 = new System.Windows.Forms.TreeNode("条件定价", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode344,
            treeNode345,
            treeNode346,
            treeNode347});
            System.Windows.Forms.TreeNode treeNode349 = new System.Windows.Forms.TreeNode("管理");
            System.Windows.Forms.TreeNode treeNode350 = new System.Windows.Forms.TreeNode("合同模板", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode349});
            System.Windows.Forms.TreeNode treeNode351 = new System.Windows.Forms.TreeNode("管理");
            System.Windows.Forms.TreeNode treeNode352 = new System.Windows.Forms.TreeNode("合同文本", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode351});
            System.Windows.Forms.TreeNode treeNode353 = new System.Windows.Forms.TreeNode("合同管理", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode350,
            treeNode352});
            System.Windows.Forms.TreeNode treeNode354 = new System.Windows.Forms.TreeNode("管理");
            System.Windows.Forms.TreeNode treeNode355 = new System.Windows.Forms.TreeNode("计划协议");
            System.Windows.Forms.TreeNode treeNode356 = new System.Windows.Forms.TreeNode("框架协议", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode353,
            treeNode354,
            treeNode355});
            System.Windows.Forms.TreeNode treeNode357 = new System.Windows.Forms.TreeNode("管理");
            System.Windows.Forms.TreeNode treeNode358 = new System.Windows.Forms.TreeNode("配额协议", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode357});
            System.Windows.Forms.TreeNode treeNode359 = new System.Windows.Forms.TreeNode("信息记录");
            System.Windows.Forms.TreeNode treeNode360 = new System.Windows.Forms.TreeNode("采购信息记录", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode359});
            System.Windows.Forms.TreeNode treeNode361 = new System.Windows.Forms.TreeNode("货源确定", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode339,
            treeNode343,
            treeNode348,
            treeNode356,
            treeNode358,
            treeNode360});
            System.Windows.Forms.TreeNode treeNode362 = new System.Windows.Forms.TreeNode("订单管理");
            System.Windows.Forms.TreeNode treeNode363 = new System.Windows.Forms.TreeNode("信息记录");
            System.Windows.Forms.TreeNode treeNode364 = new System.Windows.Forms.TreeNode("发票管理");
            System.Windows.Forms.TreeNode treeNode365 = new System.Windows.Forms.TreeNode("订单管理", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode362,
            treeNode363,
            treeNode364});
            System.Windows.Forms.TreeNode treeNode366 = new System.Windows.Forms.TreeNode("寻源管理仓");
            System.Windows.Forms.TreeNode treeNode367 = new System.Windows.Forms.TreeNode("寻源管理", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode366});
            System.Windows.Forms.TreeNode treeNode368 = new System.Windows.Forms.TreeNode("审批");
            System.Windows.Forms.TreeNode treeNode369 = new System.Windows.Forms.TreeNode("系统", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode368});
            System.Windows.Forms.TreeNode treeNode370 = new System.Windows.Forms.TreeNode("采购管理", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode304,
            treeNode361,
            treeNode365,
            treeNode367,
            treeNode369});
            System.Windows.Forms.TreeNode treeNode371 = new System.Windows.Forms.TreeNode("生产性物料");
            System.Windows.Forms.TreeNode treeNode372 = new System.Windows.Forms.TreeNode("非生产性物料");
            System.Windows.Forms.TreeNode treeNode373 = new System.Windows.Forms.TreeNode("供应目标设置", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode371,
            treeNode372});
            System.Windows.Forms.TreeNode treeNode374 = new System.Windows.Forms.TreeNode("风险等级");
            System.Windows.Forms.TreeNode treeNode375 = new System.Windows.Forms.TreeNode("等级定位");
            System.Windows.Forms.TreeNode treeNode376 = new System.Windows.Forms.TreeNode("物料定位", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode373,
            treeNode374,
            treeNode375});
            System.Windows.Forms.TreeNode treeNode377 = new System.Windows.Forms.TreeNode("供应商前期准备");
            System.Windows.Forms.TreeNode treeNode378 = new System.Windows.Forms.TreeNode("供应商认证", 1, 1);
            System.Windows.Forms.TreeNode treeNode379 = new System.Windows.Forms.TreeNode("供应商认证", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode377,
            treeNode378});
            System.Windows.Forms.TreeNode treeNode380 = new System.Windows.Forms.TreeNode("评估发起");
            System.Windows.Forms.TreeNode treeNode381 = new System.Windows.Forms.TreeNode("手工维护");
            System.Windows.Forms.TreeNode treeNode382 = new System.Windows.Forms.TreeNode("自动评估");
            System.Windows.Forms.TreeNode treeNode383 = new System.Windows.Forms.TreeNode("批量评估");
            System.Windows.Forms.TreeNode treeNode384 = new System.Windows.Forms.TreeNode("结果显示");
            System.Windows.Forms.TreeNode treeNode385 = new System.Windows.Forms.TreeNode("清单显示");
            System.Windows.Forms.TreeNode treeNode386 = new System.Windows.Forms.TreeNode("外部服务");
            System.Windows.Forms.TreeNode treeNode387 = new System.Windows.Forms.TreeNode("一般服务");
            System.Windows.Forms.TreeNode treeNode388 = new System.Windows.Forms.TreeNode("维护服务占比");
            System.Windows.Forms.TreeNode treeNode389 = new System.Windows.Forms.TreeNode("服务评估", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode386,
            treeNode387,
            treeNode388});
            System.Windows.Forms.TreeNode treeNode390 = new System.Windows.Forms.TreeNode("绩效评估", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode380,
            treeNode381,
            treeNode382,
            treeNode383,
            treeNode384,
            treeNode385,
            treeNode389});
            System.Windows.Forms.TreeNode treeNode391 = new System.Windows.Forms.TreeNode("市场价格记录");
            System.Windows.Forms.TreeNode treeNode392 = new System.Windows.Forms.TreeNode("采购信息查看");
            System.Windows.Forms.TreeNode treeNode393 = new System.Windows.Forms.TreeNode("添加采购记录");
            System.Windows.Forms.TreeNode treeNode394 = new System.Windows.Forms.TreeNode("信息记录(手动)", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode391,
            treeNode392,
            treeNode393});
            System.Windows.Forms.TreeNode treeNode395 = new System.Windows.Forms.TreeNode("体系/过程审核");
            System.Windows.Forms.TreeNode treeNode396 = new System.Windows.Forms.TreeNode("产品审核");
            System.Windows.Forms.TreeNode treeNode397 = new System.Windows.Forms.TreeNode("质量审核", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode395,
            treeNode396});
            System.Windows.Forms.TreeNode treeNode398 = new System.Windows.Forms.TreeNode("抱怨/拒绝情况录入");
            System.Windows.Forms.TreeNode treeNode399 = new System.Windows.Forms.TreeNode("服务情况录入");
            System.Windows.Forms.TreeNode treeNode400 = new System.Windows.Forms.TreeNode("收货问卷调查");
            System.Windows.Forms.TreeNode treeNode401 = new System.Windows.Forms.TreeNode("供应商营业额录入");
            System.Windows.Forms.TreeNode treeNode402 = new System.Windows.Forms.TreeNode("质量收货");
            System.Windows.Forms.TreeNode treeNode403 = new System.Windows.Forms.TreeNode("交货审查");
            System.Windows.Forms.TreeNode treeNode404 = new System.Windows.Forms.TreeNode("绩效评估数据维护", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode394,
            treeNode397,
            treeNode398,
            treeNode399,
            treeNode400,
            treeNode401,
            treeNode402,
            treeNode403});
            System.Windows.Forms.TreeNode treeNode405 = new System.Windows.Forms.TreeNode("执行分级");
            System.Windows.Forms.TreeNode treeNode406 = new System.Windows.Forms.TreeNode("分级查看");
            System.Windows.Forms.TreeNode treeNode407 = new System.Windows.Forms.TreeNode("分组查看分级结果");
            System.Windows.Forms.TreeNode treeNode408 = new System.Windows.Forms.TreeNode("供应商分级", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode405,
            treeNode406,
            treeNode407});
            System.Windows.Forms.TreeNode treeNode409 = new System.Windows.Forms.TreeNode("低效能供应商警告");
            System.Windows.Forms.TreeNode treeNode410 = new System.Windows.Forms.TreeNode("低效能供应商管理三级流程");
            System.Windows.Forms.TreeNode treeNode411 = new System.Windows.Forms.TreeNode("低效能供应商淘汰");
            System.Windows.Forms.TreeNode treeNode412 = new System.Windows.Forms.TreeNode("低效能供应商管理", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode409,
            treeNode410,
            treeNode411});
            System.Windows.Forms.TreeNode treeNode413 = new System.Windows.Forms.TreeNode("降成本计算");
            System.Windows.Forms.TreeNode treeNode414 = new System.Windows.Forms.TreeNode("降成本结果查询");
            System.Windows.Forms.TreeNode treeNode415 = new System.Windows.Forms.TreeNode("采购降成本", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode413,
            treeNode414});
            System.Windows.Forms.TreeNode treeNode416 = new System.Windows.Forms.TreeNode("供应商区分");
            System.Windows.Forms.TreeNode treeNode417 = new System.Windows.Forms.TreeNode("区分结果清单");
            System.Windows.Forms.TreeNode treeNode418 = new System.Windows.Forms.TreeNode("供应商价值", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode416,
            treeNode417});
            System.Windows.Forms.TreeNode treeNode419 = new System.Windows.Forms.TreeNode("供应商绩效比较");
            System.Windows.Forms.TreeNode treeNode420 = new System.Windows.Forms.TreeNode("基于物料组的供应商评估");
            System.Windows.Forms.TreeNode treeNode421 = new System.Windows.Forms.TreeNode("基于行业的供应商评估");
            System.Windows.Forms.TreeNode treeNode422 = new System.Windows.Forms.TreeNode("供应商对比雷达图");
            System.Windows.Forms.TreeNode treeNode423 = new System.Windows.Forms.TreeNode("供应商分析气泡图");
            System.Windows.Forms.TreeNode treeNode424 = new System.Windows.Forms.TreeNode("绩效评估报表", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode419,
            treeNode420,
            treeNode421,
            treeNode422,
            treeNode423});
            System.Windows.Forms.TreeNode treeNode425 = new System.Windows.Forms.TreeNode("清单概况");
            System.Windows.Forms.TreeNode treeNode426 = new System.Windows.Forms.TreeNode("货源信息");
            System.Windows.Forms.TreeNode treeNode427 = new System.Windows.Forms.TreeNode("优先级维护");
            System.Windows.Forms.TreeNode treeNode428 = new System.Windows.Forms.TreeNode("供应商清单", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode425,
            treeNode426,
            treeNode427});
            System.Windows.Forms.TreeNode treeNode429 = new System.Windows.Forms.TreeNode("供货信息查看");
            System.Windows.Forms.TreeNode treeNode430 = new System.Windows.Forms.TreeNode("供货信息", new System.Windows.Forms.TreeNode[] {
            treeNode429});
            System.Windows.Forms.TreeNode treeNode431 = new System.Windows.Forms.TreeNode("供应商证书");
            System.Windows.Forms.TreeNode treeNode432 = new System.Windows.Forms.TreeNode("供应商表现", new System.Windows.Forms.TreeNode[] {
            treeNode431});
            System.Windows.Forms.TreeNode treeNode433 = new System.Windows.Forms.TreeNode("供应商管理", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode376,
            treeNode379,
            treeNode390,
            treeNode404,
            treeNode408,
            treeNode412,
            treeNode415,
            treeNode418,
            treeNode424,
            treeNode428,
            treeNode430,
            treeNode432});
            System.Windows.Forms.TreeNode treeNode434 = new System.Windows.Forms.TreeNode("物料的ABC类分析");
            System.Windows.Forms.TreeNode treeNode435 = new System.Windows.Forms.TreeNode("供应商的ABC类分析");
            System.Windows.Forms.TreeNode treeNode436 = new System.Windows.Forms.TreeNode("采购监控分析", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode434,
            treeNode435});
            System.Windows.Forms.TreeNode treeNode437 = new System.Windows.Forms.TreeNode("采购花费分布");
            System.Windows.Forms.TreeNode treeNode438 = new System.Windows.Forms.TreeNode("成本结构分析");
            System.Windows.Forms.TreeNode treeNode439 = new System.Windows.Forms.TreeNode("成本比较分析");
            System.Windows.Forms.TreeNode treeNode440 = new System.Windows.Forms.TreeNode("成本趋势分析");
            System.Windows.Forms.TreeNode treeNode441 = new System.Windows.Forms.TreeNode("采购花费分析", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode437,
            treeNode438,
            treeNode439,
            treeNode440});
            System.Windows.Forms.TreeNode treeNode442 = new System.Windows.Forms.TreeNode("价格分析");
            System.Windows.Forms.TreeNode treeNode443 = new System.Windows.Forms.TreeNode("供应商竞价分析");
            System.Windows.Forms.TreeNode treeNode444 = new System.Windows.Forms.TreeNode("寻源分析", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode442,
            treeNode443});
            System.Windows.Forms.TreeNode treeNode445 = new System.Windows.Forms.TreeNode("合同管理分析");
            System.Windows.Forms.TreeNode treeNode446 = new System.Windows.Forms.TreeNode("合同执行分析", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode445});
            System.Windows.Forms.TreeNode treeNode447 = new System.Windows.Forms.TreeNode("供应商对比雷达图");
            System.Windows.Forms.TreeNode treeNode448 = new System.Windows.Forms.TreeNode("供应商趋势报表");
            System.Windows.Forms.TreeNode treeNode449 = new System.Windows.Forms.TreeNode("供应商得分排名");
            System.Windows.Forms.TreeNode treeNode450 = new System.Windows.Forms.TreeNode("供应商绩效分析", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode447,
            treeNode448,
            treeNode449});
            System.Windows.Forms.TreeNode treeNode451 = new System.Windows.Forms.TreeNode("库龄分析明细表");
            System.Windows.Forms.TreeNode treeNode452 = new System.Windows.Forms.TreeNode("库存物料ABC类分析");
            System.Windows.Forms.TreeNode treeNode453 = new System.Windows.Forms.TreeNode("库存分析", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode451,
            treeNode452});
            System.Windows.Forms.TreeNode treeNode454 = new System.Windows.Forms.TreeNode("供应商分析");
            System.Windows.Forms.TreeNode treeNode455 = new System.Windows.Forms.TreeNode("供应商依赖因素分析");
            System.Windows.Forms.TreeNode treeNode456 = new System.Windows.Forms.TreeNode("定制");
            System.Windows.Forms.TreeNode treeNode457 = new System.Windows.Forms.TreeNode("报表分析", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode436,
            treeNode441,
            treeNode444,
            treeNode446,
            treeNode450,
            treeNode453,
            treeNode454,
            treeNode455,
            treeNode456});
            System.Windows.Forms.TreeNode treeNode458 = new System.Windows.Forms.TreeNode("库存类型");
            System.Windows.Forms.TreeNode treeNode459 = new System.Windows.Forms.TreeNode("库存状态");
            System.Windows.Forms.TreeNode treeNode460 = new System.Windows.Forms.TreeNode("查看");
            System.Windows.Forms.TreeNode treeNode461 = new System.Windows.Forms.TreeNode("事务/事件");
            System.Windows.Forms.TreeNode treeNode462 = new System.Windows.Forms.TreeNode("移动类型", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode460,
            treeNode461});
            System.Windows.Forms.TreeNode treeNode463 = new System.Windows.Forms.TreeNode("基础配置", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode458,
            treeNode459,
            treeNode462});
            System.Windows.Forms.TreeNode treeNode464 = new System.Windows.Forms.TreeNode("收货", 0, 0);
            System.Windows.Forms.TreeNode treeNode465 = new System.Windows.Forms.TreeNode("货物移动", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode464});
            System.Windows.Forms.TreeNode treeNode466 = new System.Windows.Forms.TreeNode("库存总览");
            System.Windows.Forms.TreeNode treeNode467 = new System.Windows.Forms.TreeNode("工厂库存");
            System.Windows.Forms.TreeNode treeNode468 = new System.Windows.Forms.TreeNode("仓库库存");
            System.Windows.Forms.TreeNode treeNode469 = new System.Windows.Forms.TreeNode("寄售库存");
            System.Windows.Forms.TreeNode treeNode470 = new System.Windows.Forms.TreeNode("在途库存");
            System.Windows.Forms.TreeNode treeNode471 = new System.Windows.Forms.TreeNode("收发存报表", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode466,
            treeNode467,
            treeNode468,
            treeNode469,
            treeNode470});
            System.Windows.Forms.TreeNode treeNode472 = new System.Windows.Forms.TreeNode("手动设置安全库存");
            System.Windows.Forms.TreeNode treeNode473 = new System.Windows.Forms.TreeNode("查看安全库存");
            System.Windows.Forms.TreeNode treeNode474 = new System.Windows.Forms.TreeNode("更新安全库存");
            System.Windows.Forms.TreeNode treeNode475 = new System.Windows.Forms.TreeNode("输入需求量");
            System.Windows.Forms.TreeNode treeNode476 = new System.Windows.Forms.TreeNode("系统计算安全库存");
            System.Windows.Forms.TreeNode treeNode477 = new System.Windows.Forms.TreeNode("安全库存", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode472,
            treeNode473,
            treeNode474,
            treeNode475,
            treeNode476});
            System.Windows.Forms.TreeNode treeNode478 = new System.Windows.Forms.TreeNode("集中创建盘点凭证");
            System.Windows.Forms.TreeNode treeNode479 = new System.Windows.Forms.TreeNode("单个创建盘点凭证");
            System.Windows.Forms.TreeNode treeNode480 = new System.Windows.Forms.TreeNode("盘点冻结与解冻");
            System.Windows.Forms.TreeNode treeNode481 = new System.Windows.Forms.TreeNode("输入盘点结果");
            System.Windows.Forms.TreeNode treeNode482 = new System.Windows.Forms.TreeNode("差异清单一览");
            System.Windows.Forms.TreeNode treeNode483 = new System.Windows.Forms.TreeNode("盘点数据更新");
            System.Windows.Forms.TreeNode treeNode484 = new System.Windows.Forms.TreeNode("年度盘点", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode478,
            treeNode479,
            treeNode480,
            treeNode481,
            treeNode482,
            treeNode483});
            System.Windows.Forms.TreeNode treeNode485 = new System.Windows.Forms.TreeNode("显示物料清单");
            System.Windows.Forms.TreeNode treeNode486 = new System.Windows.Forms.TreeNode("差异调整");
            System.Windows.Forms.TreeNode treeNode487 = new System.Windows.Forms.TreeNode("显示差异调整");
            System.Windows.Forms.TreeNode treeNode488 = new System.Windows.Forms.TreeNode("取消差异调整");
            System.Windows.Forms.TreeNode treeNode489 = new System.Windows.Forms.TreeNode("循环盘点", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode485,
            treeNode486,
            treeNode487,
            treeNode488});
            System.Windows.Forms.TreeNode treeNode490 = new System.Windows.Forms.TreeNode("库存盘点", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode484,
            treeNode489});
            System.Windows.Forms.TreeNode treeNode491 = new System.Windows.Forms.TreeNode("质检评分");
            System.Windows.Forms.TreeNode treeNode492 = new System.Windows.Forms.TreeNode("装运评分");
            System.Windows.Forms.TreeNode treeNode493 = new System.Windows.Forms.TreeNode("非原材料拒收数量");
            System.Windows.Forms.TreeNode treeNode494 = new System.Windows.Forms.TreeNode("收货评分", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode491,
            treeNode492,
            treeNode493});
            System.Windows.Forms.TreeNode treeNode495 = new System.Windows.Forms.TreeNode("设置提前提醒天数");
            System.Windows.Forms.TreeNode treeNode496 = new System.Windows.Forms.TreeNode("到期物料提醒");
            System.Windows.Forms.TreeNode treeNode497 = new System.Windows.Forms.TreeNode("到期提醒", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode495,
            treeNode496});
            System.Windows.Forms.TreeNode treeNode498 = new System.Windows.Forms.TreeNode("库存管理", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode463,
            treeNode465,
            treeNode471,
            treeNode477,
            treeNode490,
            treeNode494,
            treeNode497});
            System.Windows.Forms.TreeNode treeNode499 = new System.Windows.Forms.TreeNode("文档管理", 1, 1);
            System.Windows.Forms.TreeNode treeNode500 = new System.Windows.Forms.TreeNode("用户管理");
            System.Windows.Forms.TreeNode treeNode501 = new System.Windows.Forms.TreeNode("用户组管理");
            System.Windows.Forms.TreeNode treeNode502 = new System.Windows.Forms.TreeNode("角色管理");
            System.Windows.Forms.TreeNode treeNode503 = new System.Windows.Forms.TreeNode("组织机构管理");
            System.Windows.Forms.TreeNode treeNode504 = new System.Windows.Forms.TreeNode("数据备份");
            System.Windows.Forms.TreeNode treeNode505 = new System.Windows.Forms.TreeNode("数据恢复");
            System.Windows.Forms.TreeNode treeNode506 = new System.Windows.Forms.TreeNode("系统管理", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode500,
            treeNode501,
            treeNode502,
            treeNode503,
            treeNode504,
            treeNode505});
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.任务查看ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.systemMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pnlTop = new System.Windows.Forms.Panel();
            this.LKB_UserName = new System.Windows.Forms.LinkLabel();
            this.pnlBottom = new System.Windows.Forms.Panel();
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.leftStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.leftBlankHolder = new System.Windows.Forms.ToolStripStatusLabel();
            this.middleStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.rightBlankHolder = new System.Windows.Forms.ToolStripStatusLabel();
            this.rightStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.pnlMiddle = new System.Windows.Forms.Panel();
            this.dockPnlForm = new WeifenLuo.WinFormsUI.Docking.DockPanel();
            this.pnlMiddleLeft = new System.Windows.Forms.Panel();
            this.tvFunction = new System.Windows.Forms.TreeView();
            this.tvImgList = new System.Windows.Forms.ImageList(this.components);
            this.pnlMiddleLeftTop = new System.Windows.Forms.Panel();
            this.menuStrip.SuspendLayout();
            this.pnlBottom.SuspendLayout();
            this.statusStrip.SuspendLayout();
            this.pnlMiddle.SuspendLayout();
            this.pnlMiddleLeft.SuspendLayout();
            this.pnlMiddleLeftTop.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip
            // 
            this.menuStrip.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.menuStrip.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.任务查看ToolStripMenuItem,
            this.systemMenuItem,
            this.toolMenuItem,
            this.helpMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(963, 25);
            this.menuStrip.TabIndex = 10;
            this.menuStrip.Text = "menuStrip1";
            // 
            // 任务查看ToolStripMenuItem
            // 
            this.任务查看ToolStripMenuItem.Name = "任务查看ToolStripMenuItem";
            this.任务查看ToolStripMenuItem.Size = new System.Drawing.Size(68, 21);
            this.任务查看ToolStripMenuItem.Text = "待办任务";
            this.任务查看ToolStripMenuItem.Click += new System.EventHandler(this.任务查看ToolStripMenuItem_Click);
            // 
            // systemMenuItem
            // 
            this.systemMenuItem.Name = "systemMenuItem";
            this.systemMenuItem.Size = new System.Drawing.Size(59, 21);
            this.systemMenuItem.Text = "系统(S)";
            // 
            // toolMenuItem
            // 
            this.toolMenuItem.Name = "toolMenuItem";
            this.toolMenuItem.Size = new System.Drawing.Size(59, 21);
            this.toolMenuItem.Text = "工具(T)";
            // 
            // helpMenuItem
            // 
            this.helpMenuItem.Name = "helpMenuItem";
            this.helpMenuItem.Size = new System.Drawing.Size(61, 21);
            this.helpMenuItem.Text = "帮助(H)";
            // 
            // pnlTop
            // 
            this.pnlTop.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.pnlTop.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pnlTop.BackgroundImage")));
            this.pnlTop.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlTop.Location = new System.Drawing.Point(0, 25);
            this.pnlTop.Name = "pnlTop";
            this.pnlTop.Size = new System.Drawing.Size(963, 55);
            this.pnlTop.TabIndex = 11;
            // 
            // LKB_UserName
            // 
            this.LKB_UserName.AutoSize = true;
            this.LKB_UserName.BackColor = System.Drawing.Color.Transparent;
            this.LKB_UserName.Font = new System.Drawing.Font("宋体", 15F);
            this.LKB_UserName.Location = new System.Drawing.Point(11, 8);
            this.LKB_UserName.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.LKB_UserName.Name = "LKB_UserName";
            this.LKB_UserName.Size = new System.Drawing.Size(109, 20);
            this.LKB_UserName.TabIndex = 2;
            this.LKB_UserName.TabStop = true;
            this.LKB_UserName.Text = "linkLabel1";
            this.LKB_UserName.Visible = false;
            // 
            // pnlBottom
            // 
            this.pnlBottom.Controls.Add(this.statusStrip);
            this.pnlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlBottom.Location = new System.Drawing.Point(0, 536);
            this.pnlBottom.Name = "pnlBottom";
            this.pnlBottom.Size = new System.Drawing.Size(963, 16);
            this.pnlBottom.TabIndex = 15;
            // 
            // statusStrip
            // 
            this.statusStrip.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.leftStatusLabel,
            this.leftBlankHolder,
            this.middleStatusLabel,
            this.rightBlankHolder,
            this.rightStatusLabel});
            this.statusStrip.Location = new System.Drawing.Point(0, -6);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(963, 22);
            this.statusStrip.TabIndex = 0;
            // 
            // leftStatusLabel
            // 
            this.leftStatusLabel.Name = "leftStatusLabel";
            this.leftStatusLabel.Size = new System.Drawing.Size(101, 17);
            this.leftStatusLabel.Text = "左边状态栏内容...";
            // 
            // leftBlankHolder
            // 
            this.leftBlankHolder.Name = "leftBlankHolder";
            this.leftBlankHolder.Size = new System.Drawing.Size(322, 17);
            this.leftBlankHolder.Spring = true;
            // 
            // middleStatusLabel
            // 
            this.middleStatusLabel.Name = "middleStatusLabel";
            this.middleStatusLabel.Size = new System.Drawing.Size(101, 17);
            this.middleStatusLabel.Text = "中间状态栏内容...";
            // 
            // rightBlankHolder
            // 
            this.rightBlankHolder.Name = "rightBlankHolder";
            this.rightBlankHolder.Size = new System.Drawing.Size(322, 17);
            this.rightBlankHolder.Spring = true;
            // 
            // rightStatusLabel
            // 
            this.rightStatusLabel.Name = "rightStatusLabel";
            this.rightStatusLabel.Size = new System.Drawing.Size(101, 17);
            this.rightStatusLabel.Text = "右边状态栏内容...";
            // 
            // pnlMiddle
            // 
            this.pnlMiddle.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.pnlMiddle.Controls.Add(this.dockPnlForm);
            this.pnlMiddle.Controls.Add(this.pnlMiddleLeft);
            this.pnlMiddle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlMiddle.Location = new System.Drawing.Point(0, 80);
            this.pnlMiddle.Name = "pnlMiddle";
            this.pnlMiddle.Size = new System.Drawing.Size(963, 456);
            this.pnlMiddle.TabIndex = 16;
            // 
            // dockPnlForm
            // 
            this.dockPnlForm.ActiveAutoHideContent = null;
            this.dockPnlForm.AutoScroll = true;
            this.dockPnlForm.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.dockPnlForm.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dockPnlForm.DockBackColor = System.Drawing.SystemColors.Control;
            this.dockPnlForm.DocumentStyle = WeifenLuo.WinFormsUI.Docking.DocumentStyle.DockingWindow;
            this.dockPnlForm.Location = new System.Drawing.Point(176, 0);
            this.dockPnlForm.Name = "dockPnlForm";
            this.dockPnlForm.Size = new System.Drawing.Size(787, 456);
            dockPanelGradient4.EndColor = System.Drawing.SystemColors.ControlLight;
            dockPanelGradient4.StartColor = System.Drawing.SystemColors.ControlLight;
            autoHideStripSkin2.DockStripGradient = dockPanelGradient4;
            tabGradient8.EndColor = System.Drawing.SystemColors.Control;
            tabGradient8.StartColor = System.Drawing.SystemColors.Control;
            tabGradient8.TextColor = System.Drawing.SystemColors.ControlDarkDark;
            autoHideStripSkin2.TabGradient = tabGradient8;
            dockPanelSkin2.AutoHideStripSkin = autoHideStripSkin2;
            tabGradient9.EndColor = System.Drawing.SystemColors.ControlLightLight;
            tabGradient9.StartColor = System.Drawing.SystemColors.ControlLightLight;
            tabGradient9.TextColor = System.Drawing.SystemColors.ControlText;
            dockPaneStripGradient2.ActiveTabGradient = tabGradient9;
            dockPanelGradient5.EndColor = System.Drawing.SystemColors.Control;
            dockPanelGradient5.StartColor = System.Drawing.SystemColors.Control;
            dockPaneStripGradient2.DockStripGradient = dockPanelGradient5;
            tabGradient10.EndColor = System.Drawing.SystemColors.ControlLight;
            tabGradient10.StartColor = System.Drawing.SystemColors.ControlLight;
            tabGradient10.TextColor = System.Drawing.SystemColors.ControlText;
            dockPaneStripGradient2.InactiveTabGradient = tabGradient10;
            dockPaneStripSkin2.DocumentGradient = dockPaneStripGradient2;
            tabGradient11.EndColor = System.Drawing.SystemColors.ActiveCaption;
            tabGradient11.LinearGradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            tabGradient11.StartColor = System.Drawing.SystemColors.GradientActiveCaption;
            tabGradient11.TextColor = System.Drawing.SystemColors.ActiveCaptionText;
            dockPaneStripToolWindowGradient2.ActiveCaptionGradient = tabGradient11;
            tabGradient12.EndColor = System.Drawing.SystemColors.Control;
            tabGradient12.StartColor = System.Drawing.SystemColors.Control;
            tabGradient12.TextColor = System.Drawing.SystemColors.ControlText;
            dockPaneStripToolWindowGradient2.ActiveTabGradient = tabGradient12;
            dockPanelGradient6.EndColor = System.Drawing.SystemColors.ControlLight;
            dockPanelGradient6.StartColor = System.Drawing.SystemColors.ControlLight;
            dockPaneStripToolWindowGradient2.DockStripGradient = dockPanelGradient6;
            tabGradient13.EndColor = System.Drawing.SystemColors.GradientInactiveCaption;
            tabGradient13.LinearGradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            tabGradient13.StartColor = System.Drawing.SystemColors.GradientInactiveCaption;
            tabGradient13.TextColor = System.Drawing.SystemColors.ControlText;
            dockPaneStripToolWindowGradient2.InactiveCaptionGradient = tabGradient13;
            tabGradient14.EndColor = System.Drawing.Color.Transparent;
            tabGradient14.StartColor = System.Drawing.Color.Transparent;
            tabGradient14.TextColor = System.Drawing.SystemColors.ControlDarkDark;
            dockPaneStripToolWindowGradient2.InactiveTabGradient = tabGradient14;
            dockPaneStripSkin2.ToolWindowGradient = dockPaneStripToolWindowGradient2;
            dockPanelSkin2.DockPaneStripSkin = dockPaneStripSkin2;
            this.dockPnlForm.Skin = dockPanelSkin2;
            this.dockPnlForm.TabIndex = 15;
            this.dockPnlForm.ActiveContentChanged += new System.EventHandler(this.dockPnlForm_ActiveContentChanged);
            // 
            // pnlMiddleLeft
            // 
            this.pnlMiddleLeft.Controls.Add(this.tvFunction);
            this.pnlMiddleLeft.Controls.Add(this.pnlMiddleLeftTop);
            this.pnlMiddleLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlMiddleLeft.Location = new System.Drawing.Point(0, 0);
            this.pnlMiddleLeft.Name = "pnlMiddleLeft";
            this.pnlMiddleLeft.Size = new System.Drawing.Size(176, 456);
            this.pnlMiddleLeft.TabIndex = 14;
            // 
            // tvFunction
            // 
            this.tvFunction.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tvFunction.ImageIndex = 0;
            this.tvFunction.ImageList = this.tvImgList;
            this.tvFunction.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.tvFunction.LabelEdit = true;
            this.tvFunction.Location = new System.Drawing.Point(0, 31);
            this.tvFunction.Name = "tvFunction";
            treeNode254.Name = "NewMaterialData";
            treeNode254.Text = "新建物料数据";
            treeNode255.Name = "MaterialBasicView";
            treeNode255.Text = "基本视图";
            treeNode256.Name = "MaterialAccountView";
            treeNode256.Text = "会计视图";
            treeNode257.Name = "MaterialBuyerView";
            treeNode257.Text = "采购视图";
            treeNode258.Name = "MaterialStockView";
            treeNode258.Text = "存储视图";
            treeNode259.Name = "NewBatch";
            treeNode259.Text = "建立批次";
            treeNode260.Name = "QuryBatch";
            treeNode260.Text = "查询批次";
            treeNode261.ImageIndex = 1;
            treeNode261.Name = "Batch";
            treeNode261.SelectedImageIndex = 1;
            treeNode261.Text = "批次管理";
            treeNode262.Name = "MaterialClassView";
            treeNode262.Text = "分类视图";
            treeNode263.ImageIndex = 1;
            treeNode263.Name = "MaterialData";
            treeNode263.SelectedImageIndex = 1;
            treeNode263.Text = "物料主数据";
            treeNode264.Name = "SupplierBasicView";
            treeNode264.Text = "基本视图";
            treeNode265.Name = "SupplierBuyerOrgView";
            treeNode265.Text = "采购组织视图";
            treeNode266.Name = "SupplierCompanyCodeView";
            treeNode266.Text = "公司代码视图";
            treeNode267.Name = "CreateCode";
            treeNode267.Text = "创建供应商编码";
            treeNode268.ImageIndex = 1;
            treeNode268.Name = "SupplierData";
            treeNode268.SelectedImageIndex = 1;
            treeNode268.Text = "供应商主数据";
            treeNode269.Name = "NewVocher";
            treeNode269.Text = "生成会计凭证";
            treeNode270.ImageIndex = 1;
            treeNode270.Name = "AccountData";
            treeNode270.SelectedImageIndex = 1;
            treeNode270.Text = "财务主数据";
            treeNode271.Name = "MaintainMaterialGroup";
            treeNode271.Text = "维护物料组";
            treeNode272.Name = "MaterialTypeA";
            treeNode272.Text = "维护物料类型";
            treeNode273.Name = "MaintainCurrency";
            treeNode273.Text = "维护货币";
            treeNode274.Name = "MaintainCountry";
            treeNode274.Text = "维护国家信息";
            treeNode275.Name = "MaintainPaymentClause";
            treeNode275.Text = "维护付款条件";
            treeNode276.Name = "MaintainMeasurement";
            treeNode276.Text = "维护计量单位";
            treeNode277.Name = "MaintainEvaluationClass";
            treeNode277.Text = "维护评估类";
            treeNode278.Name = "MaintainCompanyCode";
            treeNode278.Text = "维护公司代码";
            treeNode279.Name = "MaintainFactory";
            treeNode279.Text = "维护工厂";
            treeNode280.Name = "MaintainStock";
            treeNode280.Text = "维护库存地";
            treeNode281.Name = "MaintainBuyerGroup";
            treeNode281.Text = "维护采购组";
            treeNode282.Name = "MaintainBuyerOrganization";
            treeNode282.Text = "维护采购组织";
            treeNode283.Name = "MaintainDivision";
            treeNode283.Text = "维护产品组";
            treeNode284.Name = "MaintainTradeClause";
            treeNode284.Text = "维护交付条件";
            treeNode285.Name = "MaintainCostCenter";
            treeNode285.Text = "维护成本中心";
            treeNode286.Name = "MaintainIndustryCategory";
            treeNode286.Text = "维护工业标识";
            treeNode287.Name = "purOrgAllotmtGroup";
            treeNode287.Text = "采购组织分配物料组";
            treeNode288.Name = "MaintainMaterEvalFactor";
            treeNode288.Text = "维护评估要素";
            treeNode288.ToolTipText = "维护评估要素";
            treeNode289.Name = "GoalValueAndLowValueForm";
            treeNode289.Text = "维护目标值和最低值";
            treeNode290.Name = "strategyInfoForm";
            treeNode290.Text = "维护策略信息库";
            treeNode291.Name = "MainCertiType";
            treeNode291.Text = "维护证书类型";
            treeNode292.Name = "newTaxeClassForm";
            treeNode292.Text = "维护税率";
            treeNode293.Name = "recordTypeForm";
            treeNode293.Text = "维护记录类型";
            treeNode294.Name = "MaintenNum";
            treeNode294.Text = "维护号码段";
            treeNode295.Name = "MaintenAg";
            treeNode295.Text = "维护账户组";
            treeNode296.Name = "DiviedFPurID";
            treeNode296.Text = "给采购组织指定工厂";
            treeNode297.Name = "performanceModel";
            treeNode297.Text = "维护绩效评估模板";
            treeNode298.ImageIndex = 1;
            treeNode298.Name = "GeneralSettings";
            treeNode298.SelectedImageIndex = 1;
            treeNode298.Text = "一般设置";
            treeNode299.ImageIndex = 1;
            treeNode299.Name = "MainData";
            treeNode299.SelectedImageIndex = 1;
            treeNode299.Text = "数据管理";
            treeNode300.Name = "managerPlansForm";
            treeNode300.Text = "采购管理";
            treeNode301.Name = "auditSubmitStandard";
            treeNode301.Text = "采购审核";
            treeNode302.Name = "aggregate_Info";
            treeNode302.Text = "采购汇总";
            treeNode303.Name = "GenPurPlans";
            treeNode303.Text = "查看采购计划";
            treeNode304.ImageIndex = 1;
            treeNode304.Name = "节点1";
            treeNode304.SelectedImageIndex = 1;
            treeNode304.Text = "计划管理";
            treeNode305.Name = "viewInquirys";
            treeNode305.Text = "查看询价";
            treeNode306.Name = "newInquiry";
            treeNode306.Text = "创建询价";
            treeNode307.Name = "editInquiry";
            treeNode307.Text = "更改询价";
            treeNode308.Name = "executeInqueryPrice";
            treeNode308.Text = "执行询价";
            treeNode309.Name = "message";
            treeNode309.Text = "消息";
            treeNode310.Name = "create";
            treeNode310.Text = "创建询价单";
            treeNode311.Name = "look";
            treeNode311.Text = "查看询价单";
            treeNode312.Name = "distribution";
            treeNode312.Text = "分配供应商";
            treeNode313.Name = "compare";
            treeNode313.Text = "查看供应商";
            treeNode314.Name = "horizontal";
            treeNode314.Text = "横向比较";
            treeNode315.Name = "vertical";
            treeNode315.Text = "纵向比较";
            treeNode316.Name = "benchmarkData";
            treeNode316.Text = "标杆比较";
            treeNode317.ImageIndex = 1;
            treeNode317.Name = "节点6";
            treeNode317.SelectedImageIndex = 1;
            treeNode317.Text = "报价比较";
            treeNode318.ImageIndex = 1;
            treeNode318.Name = "成本分析询价";
            treeNode318.SelectedImageIndex = 1;
            treeNode318.Text = "成本分析询价";
            treeNode319.ImageIndex = 1;
            treeNode319.Name = "节点5";
            treeNode319.SelectedImageIndex = 1;
            treeNode319.Text = "询价";
            treeNode320.Name = "maintenanceOfferPrice";
            treeNode320.Text = "维护报价";
            treeNode321.Name = "executeCompare";
            treeNode321.Text = "执行比价";
            treeNode322.ImageIndex = 1;
            treeNode322.Name = "节点6";
            treeNode322.SelectedImageIndex = 1;
            treeNode322.Text = "报价";
            treeNode323.ImageIndex = 1;
            treeNode323.Name = "inquiry";
            treeNode323.SelectedImageIndex = 1;
            treeNode323.Text = "询/报价";
            treeNode324.Name = "showBids";
            treeNode324.Text = "查看招标";
            treeNode325.Name = "newBidding";
            treeNode325.Text = "创建招标";
            treeNode326.Name = "releaseBid";
            treeNode326.Text = "发布招标";
            treeNode327.Name = "changeTender";
            treeNode327.Text = "修改招标";
            treeNode328.Name = "checkBid";
            treeNode328.Text = "审批招标";
            treeNode329.Name = "offerBidding";
            treeNode329.Text = "处理投标";
            treeNode330.Name = "compareBidding";
            treeNode330.Text = "进行评标";
            treeNode331.Name = "transactList";
            treeNode331.Text = "查看谈判";
            treeNode332.Name = "createTransact";
            treeNode332.Text = "创建谈判";
            treeNode333.Name = "dealTrans";
            treeNode333.Text = "处理谈判";
            treeNode334.ImageIndex = 1;
            treeNode334.Name = "transact";
            treeNode334.SelectedImageIndex = 1;
            treeNode334.Text = "谈判";
            treeNode335.ImageIndex = 1;
            treeNode335.Name = "bidding";
            treeNode335.SelectedImageIndex = 1;
            treeNode335.Text = "招标";
            treeNode336.Name = "newAuction";
            treeNode336.Text = "竞价";
            treeNode337.Name = "offerAuction";
            treeNode337.Text = "竞拍";
            treeNode338.ImageIndex = 1;
            treeNode338.Name = "auction";
            treeNode338.SelectedImageIndex = 1;
            treeNode338.Text = "拍卖";
            treeNode339.ImageIndex = 1;
            treeNode339.Name = "节点4";
            treeNode339.SelectedImageIndex = 1;
            treeNode339.Text = "寻源方式";
            treeNode340.Name = "sourceLists";
            treeNode340.Text = "清单列表";
            treeNode341.Name = "newSourceList";
            treeNode341.Text = "新建源清单";
            treeNode342.Name = "maintenanceSourceList";
            treeNode342.Text = "更改源清单";
            treeNode343.ImageIndex = 1;
            treeNode343.Name = "sourceListInfo";
            treeNode343.SelectedImageIndex = 1;
            treeNode343.Text = "货源清单";
            treeNode344.Name = "condition";
            treeNode344.Text = "条件类型";
            treeNode345.Name = "conditionList";
            treeNode345.Text = "查看条件类型";
            treeNode346.Name = "addCondition";
            treeNode346.Text = "创建条件类型";
            treeNode347.Name = "editCondition";
            treeNode347.Text = "更改条件类型";
            treeNode348.ImageIndex = 1;
            treeNode348.Name = "conditionType";
            treeNode348.SelectedImageIndex = 1;
            treeNode348.Text = "条件定价";
            treeNode349.Name = "manageContractTemplateNode";
            treeNode349.Text = "管理";
            treeNode350.ImageIndex = 1;
            treeNode350.Name = "contractTemplateNode";
            treeNode350.SelectedImageIndex = 1;
            treeNode350.Text = "合同模板";
            treeNode351.Name = "manageContractTextNode";
            treeNode351.Text = "管理";
            treeNode352.ImageIndex = 1;
            treeNode352.Name = "contractTextNode";
            treeNode352.SelectedImageIndex = 1;
            treeNode352.Text = "合同文本";
            treeNode353.ImageIndex = 1;
            treeNode353.Name = "contractManageNode";
            treeNode353.SelectedImageIndex = 1;
            treeNode353.Text = "合同管理";
            treeNode354.ImageIndex = 0;
            treeNode354.Name = "manageAgreementContractNode";
            treeNode354.Text = "管理";
            treeNode355.Name = "节点3";
            treeNode355.Text = "计划协议";
            treeNode356.ImageIndex = 1;
            treeNode356.Name = "outlineAgreementNode";
            treeNode356.SelectedImageIndex = 1;
            treeNode356.Text = "框架协议";
            treeNode357.ImageKey = "Table4.png";
            treeNode357.Name = "quotaArrangementManageNode";
            treeNode357.Text = "管理";
            treeNode358.ImageIndex = 1;
            treeNode358.Name = "quotaArrangementNode";
            treeNode358.SelectedImageIndex = 1;
            treeNode358.Text = "配额协议";
            treeNode359.Name = "allRecords";
            treeNode359.Text = "信息记录";
            treeNode360.ImageIndex = 1;
            treeNode360.Name = "sourceRecord";
            treeNode360.SelectedImageIndex = 1;
            treeNode360.Text = "采购信息记录";
            treeNode361.ImageIndex = 1;
            treeNode361.Name = "sourceMM";
            treeNode361.SelectedImageIndex = 1;
            treeNode361.Text = "货源确定";
            treeNode362.Name = "OrderCreate";
            treeNode362.Text = "订单管理";
            treeNode363.ImageKey = "Table4.png";
            treeNode363.Name = "POManageNode";
            treeNode363.Text = "信息记录";
            treeNode364.Name = "InvoiceAndPaymentNode";
            treeNode364.Text = "发票管理";
            treeNode365.ImageIndex = 1;
            treeNode365.Name = "节点6";
            treeNode365.SelectedImageIndex = 1;
            treeNode365.Text = "订单管理";
            treeNode366.Name = "InqueryBin";
            treeNode366.Text = "寻源管理仓";
            treeNode367.ImageIndex = 1;
            treeNode367.Name = "InqueryManage";
            treeNode367.SelectedImageIndex = 1;
            treeNode367.Text = "寻源管理";
            treeNode368.Name = "ApproveInvitation";
            treeNode368.Text = "审批";
            treeNode369.ImageIndex = 1;
            treeNode369.Name = "system";
            treeNode369.SelectedImageIndex = 1;
            treeNode369.Text = "系统";
            treeNode370.ImageIndex = 1;
            treeNode370.Name = "procurementMM";
            treeNode370.SelectedImageIndex = 1;
            treeNode370.Text = "采购管理";
            treeNode371.Name = "prdGoalAndAimForm";
            treeNode371.Text = "生产性物料";
            treeNode372.Name = "assetMtGroupForm";
            treeNode372.Text = "非生产性物料";
            treeNode373.ImageIndex = 1;
            treeNode373.Name = "节点0";
            treeNode373.SelectedImageIndex = 1;
            treeNode373.Text = "供应目标设置";
            treeNode374.Name = "RiskAssessment";
            treeNode374.Text = "风险等级";
            treeNode375.Name = "importData";
            treeNode375.Text = "等级定位";
            treeNode376.ImageIndex = 1;
            treeNode376.Name = "物料定位";
            treeNode376.SelectedImageIndex = 1;
            treeNode376.Text = "物料定位";
            treeNode377.Name = "certificationPreparationNode";
            treeNode377.Text = "供应商前期准备";
            treeNode378.ImageIndex = 1;
            treeNode378.Name = "供应商认证";
            treeNode378.SelectedImageIndex = 1;
            treeNode378.Text = "供应商认证";
            treeNode379.ImageIndex = 1;
            treeNode379.Name = "供应商认证";
            treeNode379.SelectedImageIndex = 1;
            treeNode379.Text = "供应商认证";
            treeNode380.Name = "SecletedEvaluator";
            treeNode380.Text = "评估发起";
            treeNode381.Name = "SPAddNode";
            treeNode381.Text = "手工维护";
            treeNode382.Name = "SPAutoNode";
            treeNode382.Text = "自动评估";
            treeNode383.Name = "SPBatchAddNode";
            treeNode383.Text = "批量评估";
            treeNode384.Name = "SPResultNode";
            treeNode384.Text = "结果显示";
            treeNode385.Name = "SPListNode";
            treeNode385.Text = "清单显示";
            treeNode386.Name = "ExService";
            treeNode386.Tag = "外部服务";
            treeNode386.Text = "外部服务";
            treeNode387.Name = "InterForm";
            treeNode387.Text = "一般服务";
            treeNode388.Name = "IntialServiceRate";
            treeNode388.Text = "维护服务占比";
            treeNode389.ImageIndex = 1;
            treeNode389.Name = "服务评估";
            treeNode389.SelectedImageIndex = 1;
            treeNode389.Tag = "";
            treeNode389.Text = "服务评估";
            treeNode390.ImageIndex = 1;
            treeNode390.Name = "SupplierPerformanceNode";
            treeNode390.SelectedImageIndex = 1;
            treeNode390.Text = "绩效评估";
            treeNode391.Name = "MMartketPriceForm";
            treeNode391.Text = "市场价格记录";
            treeNode392.Name = "PurchasesRecordInfoViewForm";
            treeNode392.Text = "采购信息查看";
            treeNode393.Name = "AddPurRecordInfoForm";
            treeNode393.Text = "添加采购记录";
            treeNode394.ImageIndex = 1;
            treeNode394.Name = "信息记录(手动)";
            treeNode394.SelectedImageIndex = 1;
            treeNode394.Text = "信息记录(手动)";
            treeNode395.Name = "SystemProcessAuditNode";
            treeNode395.Text = "体系/过程审核";
            treeNode396.Name = "ProductAuditNode";
            treeNode396.Text = "产品审核";
            treeNode397.ImageIndex = 1;
            treeNode397.Name = "QualityAuditNode";
            treeNode397.SelectedImageIndex = 1;
            treeNode397.Text = "质量审核";
            treeNode398.Name = "ComplaintRejectAddNode";
            treeNode398.Text = "抱怨/拒绝情况录入";
            treeNode399.Name = "ServiceAddNode";
            treeNode399.Text = "服务情况录入";
            treeNode400.Name = "QuestionaireNode";
            treeNode400.Text = "收货问卷调查";
            treeNode401.Name = "SupplierTurnoverNode";
            treeNode401.Text = "供应商营业额录入";
            treeNode402.Name = "qulityForm";
            treeNode402.Text = "质量收货";
            treeNode403.Name = "deliveryEval";
            treeNode403.Text = "交货审查";
            treeNode404.ImageIndex = 1;
            treeNode404.Name = "SPDataAddNode";
            treeNode404.SelectedImageIndex = 1;
            treeNode404.Text = "绩效评估数据维护";
            treeNode405.Name = "SupplierClassifyNode";
            treeNode405.Text = "执行分级";
            treeNode406.Name = "ClassificationResultNode";
            treeNode406.Text = "分级查看";
            treeNode407.Name = "ResultGroupNode";
            treeNode407.Text = "分组查看分级结果";
            treeNode408.ImageIndex = 1;
            treeNode408.Name = "SupplierClassifyNode";
            treeNode408.SelectedImageIndex = 1;
            treeNode408.Text = "供应商分级";
            treeNode409.Name = "LPSWarningNode";
            treeNode409.Text = "低效能供应商警告";
            treeNode410.Name = "LPSProcessNode";
            treeNode410.Text = "低效能供应商管理三级流程";
            treeNode411.Name = "LPSOutNode";
            treeNode411.Text = "低效能供应商淘汰";
            treeNode412.ImageIndex = 1;
            treeNode412.Name = "LPSNode";
            treeNode412.SelectedImageIndex = 1;
            treeNode412.Text = "低效能供应商管理";
            treeNode413.Name = "CostReductionCalculateNode";
            treeNode413.Text = "降成本计算";
            treeNode414.Name = "CostReductionResultNode";
            treeNode414.Text = "降成本结果查询";
            treeNode415.ImageIndex = 1;
            treeNode415.Name = "CostReductionNode";
            treeNode415.SelectedImageIndex = 1;
            treeNode415.Text = "采购降成本";
            treeNode416.Name = "BusinessValueAssessNode";
            treeNode416.Text = "供应商区分";
            treeNode417.Name = "supplierClassifyResult";
            treeNode417.Text = "区分结果清单";
            treeNode418.ImageIndex = 1;
            treeNode418.Name = "SupplierValueNode";
            treeNode418.SelectedImageIndex = 1;
            treeNode418.Text = "供应商价值";
            treeNode419.Name = "SP_Compare";
            treeNode419.Text = "供应商绩效比较";
            treeNode420.Name = "SP_CompareBaseOnMaterialGroup";
            treeNode420.Text = "基于物料组的供应商评估";
            treeNode421.Name = "SP_CompareBaseOnIndustry";
            treeNode421.Text = "基于行业的供应商评估";
            treeNode422.Name = "SP_CompareRadarChart";
            treeNode422.Text = "供应商对比雷达图";
            treeNode423.Name = "SP_CompareBubbleChart";
            treeNode423.Text = "供应商分析气泡图";
            treeNode424.ImageIndex = 1;
            treeNode424.Name = "SP_Report";
            treeNode424.SelectedImageIndex = 1;
            treeNode424.Text = "绩效评估报表";
            treeNode425.Name = "newSupplistPrdForm";
            treeNode425.Text = "清单概况";
            treeNode426.Name = "supplySourceForm";
            treeNode426.Text = "货源信息";
            treeNode427.Name = "ManualMaintLevelForm";
            treeNode427.Text = "优先级维护";
            treeNode428.ImageIndex = 1;
            treeNode428.Name = "SupplierList";
            treeNode428.SelectedImageIndex = 1;
            treeNode428.Text = "供应商清单";
            treeNode429.Name = "supplyInfoShowForm";
            treeNode429.Text = "供货信息查看";
            treeNode430.ImageIndex = 1;
            treeNode430.Name = "节点0";
            treeNode430.Text = "供货信息";
            treeNode431.Name = "newSupplierCertiForm";
            treeNode431.Text = "供应商证书";
            treeNode432.ImageIndex = 1;
            treeNode432.Name = "节点0";
            treeNode432.Text = "供应商表现";
            treeNode433.ImageIndex = 1;
            treeNode433.Name = "供应商管理";
            treeNode433.SelectedImageIndex = 1;
            treeNode433.Text = "供应商管理";
            treeNode434.Name = "MaterialABCAnalysis";
            treeNode434.Tag = "100";
            treeNode434.Text = "物料的ABC类分析";
            treeNode434.ToolTipText = "物料的ABC类分析";
            treeNode435.Name = "SupplierABCAnalysis";
            treeNode435.Tag = "101";
            treeNode435.Text = "供应商的ABC类分析";
            treeNode436.ImageIndex = 1;
            treeNode436.Name = "PurchaseMonitor";
            treeNode436.SelectedImageIndex = 1;
            treeNode436.Tag = "100";
            treeNode436.Text = "采购监控分析";
            treeNode436.ToolTipText = "采购监控分析";
            treeNode437.Name = "PurchaseConstDistribution";
            treeNode437.Tag = "201";
            treeNode437.Text = "采购花费分布";
            treeNode437.ToolTipText = "采购花费分布";
            treeNode438.Name = "CostStructureAnalysis";
            treeNode438.Tag = "202";
            treeNode438.Text = "成本结构分析";
            treeNode438.ToolTipText = "成本结构分析";
            treeNode439.Name = "CostComparativeAnalysis";
            treeNode439.Tag = "203";
            treeNode439.Text = "成本比较分析";
            treeNode439.ToolTipText = "成本比较分析";
            treeNode440.Name = "CostTrenAnalysis";
            treeNode440.Tag = "204";
            treeNode440.Text = "成本趋势分析";
            treeNode440.ToolTipText = "成本趋势分析";
            treeNode441.ImageIndex = 1;
            treeNode441.Name = "PurchaseExpense";
            treeNode441.SelectedImageIndex = 1;
            treeNode441.Tag = "200";
            treeNode441.Text = "采购花费分析";
            treeNode441.ToolTipText = "采购花费分析";
            treeNode442.Name = "MaterialPriceAnalysis";
            treeNode442.Tag = "300";
            treeNode442.Text = "价格分析";
            treeNode442.ToolTipText = "价格分析";
            treeNode443.Name = "SupplierBiddingAnalysis";
            treeNode443.Tag = "301";
            treeNode443.Text = "供应商竞价分析";
            treeNode443.ToolTipText = "供应商竞价分析";
            treeNode444.ImageIndex = 1;
            treeNode444.Name = "SourseTracing";
            treeNode444.SelectedImageIndex = 1;
            treeNode444.Tag = "300";
            treeNode444.Text = "寻源分析";
            treeNode444.ToolTipText = "寻源分析";
            treeNode445.Name = "ConstractionManagement";
            treeNode445.Tag = "400";
            treeNode445.Text = "合同管理分析";
            treeNode445.ToolTipText = "合同管理分析";
            treeNode446.ImageIndex = 1;
            treeNode446.Name = "ConstractionExecutuon";
            treeNode446.SelectedImageIndex = 1;
            treeNode446.Tag = "400";
            treeNode446.Text = "合同执行分析";
            treeNode446.ToolTipText = "合同执行分析";
            treeNode447.Name = "SupplierComparativeAnalysis";
            treeNode447.Tag = "500";
            treeNode447.Text = "供应商对比雷达图";
            treeNode447.ToolTipText = "供应商对比雷达图";
            treeNode448.Name = "SupplierTrendAnalysis";
            treeNode448.Tag = "501";
            treeNode448.Text = "供应商趋势报表";
            treeNode448.ToolTipText = "供应商趋势报表";
            treeNode449.Name = "SupplierScoreRanking";
            treeNode449.Tag = "502";
            treeNode449.Text = "供应商得分排名";
            treeNode449.ToolTipText = "供应商得分排名";
            treeNode450.ImageIndex = 1;
            treeNode450.Name = "SupplierPerformance";
            treeNode450.SelectedImageIndex = 1;
            treeNode450.Tag = "500";
            treeNode450.Text = "供应商绩效分析";
            treeNode450.ToolTipText = "供应商绩效分析";
            treeNode451.Name = "StockAgeAnalysis";
            treeNode451.Tag = "600";
            treeNode451.Text = "库龄分析明细表";
            treeNode451.ToolTipText = "库龄分析明细表";
            treeNode452.Name = "StockABCAnalysis";
            treeNode452.Tag = "601";
            treeNode452.Text = "库存物料ABC类分析";
            treeNode452.ToolTipText = "库存物料ABC类分析";
            treeNode453.ImageIndex = 1;
            treeNode453.Name = "StockAnalysis";
            treeNode453.SelectedImageIndex = 1;
            treeNode453.Tag = "600";
            treeNode453.Text = "库存分析";
            treeNode453.ToolTipText = "库存分析";
            treeNode454.Name = "SupplierAnalysis";
            treeNode454.Tag = "700";
            treeNode454.Text = "供应商分析";
            treeNode454.ToolTipText = "供应商分析";
            treeNode455.Name = "SuppplierDependentFactors";
            treeNode455.Tag = "800";
            treeNode455.Text = "供应商依赖因素分析";
            treeNode455.ToolTipText = "供应商依赖因素分析";
            treeNode456.Name = "Customized";
            treeNode456.Tag = "900";
            treeNode456.Text = "定制";
            treeNode456.ToolTipText = "定制";
            treeNode457.ImageIndex = 1;
            treeNode457.Name = "节点9";
            treeNode457.SelectedImageIndex = 1;
            treeNode457.Text = "报表分析";
            treeNode458.Name = "node_StockType";
            treeNode458.Tag = "10101";
            treeNode458.Text = "库存类型";
            treeNode459.Name = "node_StockState";
            treeNode459.Tag = "10102";
            treeNode459.Text = "库存状态";
            treeNode460.Name = "FindMoveType";
            treeNode460.Text = "查看";
            treeNode460.ToolTipText = "查看";
            treeNode461.Name = "TransactionEvent";
            treeNode461.Text = "事务/事件";
            treeNode461.ToolTipText = "事务/事件";
            treeNode462.ImageIndex = 1;
            treeNode462.Name = "MoveType";
            treeNode462.SelectedImageIndex = 1;
            treeNode462.Text = "移动类型";
            treeNode462.ToolTipText = "移动类型";
            treeNode463.ImageIndex = 1;
            treeNode463.Name = "基础配置";
            treeNode463.SelectedImageIndex = 1;
            treeNode463.Text = "基础配置";
            treeNode464.ImageIndex = 0;
            treeNode464.Name = "Receiving";
            treeNode464.SelectedImageIndex = 0;
            treeNode464.Text = "收货";
            treeNode464.ToolTipText = "收货";
            treeNode465.ImageIndex = 1;
            treeNode465.Name = "MaterialMove";
            treeNode465.SelectedImageIndex = 1;
            treeNode465.Tag = "10";
            treeNode465.Text = "货物移动";
            treeNode465.ToolTipText = "货物移动";
            treeNode466.Name = "StockOverview";
            treeNode466.Text = "库存总览";
            treeNode467.Name = "FactoryStock";
            treeNode467.Text = "工厂库存";
            treeNode468.Name = "StockStock";
            treeNode468.Text = "仓库库存";
            treeNode469.Name = "ConsignmentStock";
            treeNode469.Text = "寄售库存";
            treeNode470.Name = "TransitStock";
            treeNode470.Text = "在途库存";
            treeNode471.ImageIndex = 1;
            treeNode471.Name = "Report";
            treeNode471.SelectedImageIndex = 1;
            treeNode471.Text = "收发存报表";
            treeNode471.ToolTipText = "收发存报表";
            treeNode472.Name = "SaftyStockSet";
            treeNode472.Text = "手动设置安全库存";
            treeNode473.Name = "ShowSaftyInventory";
            treeNode473.Text = "查看安全库存";
            treeNode474.Name = "UpdateSaftyStock";
            treeNode474.Text = "更新安全库存";
            treeNode475.Name = "InputDemand";
            treeNode475.Text = "输入需求量";
            treeNode476.Name = "SaftyStockComp";
            treeNode476.Text = "系统计算安全库存";
            treeNode477.ImageIndex = 1;
            treeNode477.Name = "SaveStock";
            treeNode477.SelectedImageIndex = 1;
            treeNode477.Text = "安全库存";
            treeNode477.ToolTipText = "安全库存";
            treeNode478.Name = "createDocument";
            treeNode478.Text = "集中创建盘点凭证";
            treeNode479.Name = "CreateDocument";
            treeNode479.Text = "单个创建盘点凭证";
            treeNode480.Name = "Freezing";
            treeNode480.Text = "盘点冻结与解冻";
            treeNode481.Name = "InputResult";
            treeNode481.Text = "输入盘点结果";
            treeNode482.Name = "ShowDifference";
            treeNode482.Text = "差异清单一览";
            treeNode483.Name = "CheckPost";
            treeNode483.Text = "盘点数据更新";
            treeNode484.ImageIndex = 1;
            treeNode484.Name = "节点0";
            treeNode484.SelectedImageIndex = 1;
            treeNode484.Text = "年度盘点";
            treeNode485.Name = "StockList";
            treeNode485.Text = "显示物料清单";
            treeNode486.Name = "AdjustDifference";
            treeNode486.Text = "差异调整";
            treeNode487.Name = "ShowDiffLoop";
            treeNode487.Text = "显示差异调整";
            treeNode488.Name = "CancelAdjust";
            treeNode488.Text = "取消差异调整";
            treeNode489.ImageIndex = 1;
            treeNode489.Name = "节点1";
            treeNode489.SelectedImageIndex = 1;
            treeNode489.Text = "循环盘点";
            treeNode490.ImageIndex = 1;
            treeNode490.Name = "StockCheck";
            treeNode490.SelectedImageIndex = 1;
            treeNode490.Text = "库存盘点";
            treeNode490.ToolTipText = "库存盘点";
            treeNode491.Name = "ReceiveScore";
            treeNode491.Text = "质检评分";
            treeNode492.Name = "ShipmentScore";
            treeNode492.Text = "装运评分";
            treeNode493.Name = "NonRawMaterialScore";
            treeNode493.Text = "非原材料拒收数量";
            treeNode494.ImageIndex = 1;
            treeNode494.Name = "ReceivingScore";
            treeNode494.SelectedImageIndex = 1;
            treeNode494.Text = "收货评分";
            treeNode494.ToolTipText = "收货评分";
            treeNode495.Name = "Remind";
            treeNode495.Text = "设置提前提醒天数";
            treeNode496.Name = "RemindHandle";
            treeNode496.Text = "到期物料提醒";
            treeNode497.ImageIndex = 1;
            treeNode497.Name = "ExpirationReminder";
            treeNode497.SelectedImageIndex = 1;
            treeNode497.Text = "到期提醒";
            treeNode497.ToolTipText = "到期提醒";
            treeNode498.ImageIndex = 1;
            treeNode498.Name = "节点12";
            treeNode498.SelectedImageIndex = 1;
            treeNode498.Text = "库存管理";
            treeNode499.ImageIndex = 1;
            treeNode499.Name = "fileManageNode";
            treeNode499.SelectedImageIndex = 1;
            treeNode499.Text = "文档管理";
            treeNode500.Name = "sysUserManager";
            treeNode500.Text = "用户管理";
            treeNode501.Name = "userGroupManageNode";
            treeNode501.Text = "用户组管理";
            treeNode502.Name = "userRoleManageNode";
            treeNode502.Text = "角色管理";
            treeNode503.Name = "organizationManageNode";
            treeNode503.Text = "组织机构管理";
            treeNode504.Name = "backupDataNode";
            treeNode504.Text = "数据备份";
            treeNode505.Name = "restoreDataNode";
            treeNode505.Text = "数据恢复";
            treeNode506.ImageIndex = 1;
            treeNode506.Name = "systemConfigNode";
            treeNode506.SelectedImageIndex = 1;
            treeNode506.Text = "系统管理";
            this.tvFunction.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode299,
            treeNode370,
            treeNode433,
            treeNode457,
            treeNode498,
            treeNode499,
            treeNode506});
            this.tvFunction.SelectedImageIndex = 0;
            this.tvFunction.Size = new System.Drawing.Size(176, 425);
            this.tvFunction.TabIndex = 1;
            this.tvFunction.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.tvFunction_AfterSelect);
            this.tvFunction.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.tvFunction_NodeMouseClick);
            // 
            // tvImgList
            // 
            this.tvImgList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("tvImgList.ImageStream")));
            this.tvImgList.TransparentColor = System.Drawing.Color.Transparent;
            this.tvImgList.Images.SetKeyName(0, "Table4.png");
            this.tvImgList.Images.SetKeyName(1, "Folder2.png");
            // 
            // pnlMiddleLeftTop
            // 
            this.pnlMiddleLeftTop.AutoScroll = true;
            this.pnlMiddleLeftTop.Controls.Add(this.LKB_UserName);
            this.pnlMiddleLeftTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlMiddleLeftTop.Location = new System.Drawing.Point(0, 0);
            this.pnlMiddleLeftTop.Name = "pnlMiddleLeftTop";
            this.pnlMiddleLeftTop.Size = new System.Drawing.Size(176, 31);
            this.pnlMiddleLeftTop.TabIndex = 0;
            // 
            // UserUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(963, 552);
            this.Controls.Add(this.pnlMiddle);
            this.Controls.Add(this.pnlBottom);
            this.Controls.Add(this.pnlTop);
            this.Controls.Add(this.menuStrip);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.Name = "UserUI";
            this.Text = " ";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.loadSelectedNodes);
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.pnlMiddle.ResumeLayout(false);
            this.pnlMiddleLeft.ResumeLayout(false);
            this.pnlMiddleLeftTop.ResumeLayout(false);
            this.pnlMiddleLeftTop.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.Panel pnlTop;
        private System.Windows.Forms.Panel pnlBottom;
        private System.Windows.Forms.Panel pnlMiddle;
        private System.Windows.Forms.Panel pnlMiddleLeft;
        private System.Windows.Forms.Panel pnlMiddleLeftTop;
        private System.Windows.Forms.ImageList tvImgList;
        private System.Windows.Forms.ToolStripMenuItem systemMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpMenuItem;
        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.ToolStripStatusLabel leftStatusLabel;
        private System.Windows.Forms.ToolStripStatusLabel leftBlankHolder;
        private System.Windows.Forms.ToolStripStatusLabel middleStatusLabel;
        private System.Windows.Forms.ToolStripStatusLabel rightBlankHolder;
        private System.Windows.Forms.ToolStripStatusLabel rightStatusLabel;
        public WeifenLuo.WinFormsUI.Docking.DockPanel dockPnlForm;
        private System.Windows.Forms.TreeView tvFunction;
        private System.Windows.Forms.ToolStripMenuItem 任务查看ToolStripMenuItem;
        private System.Windows.Forms.LinkLabel LKB_UserName;
    }
}