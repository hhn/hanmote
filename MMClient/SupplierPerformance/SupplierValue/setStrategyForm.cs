﻿using Lib.Bll.SupplierPerformaceBLL;
using Lib.Common.CommonUtils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MMClient.SupplierPerformance.SupplierValue
{
    public partial class setStrategyForm : Form
    {
        string supplierId;
        string levelType;
        SupplierValueBLL supplierValueBLL = new SupplierValueBLL();
        public setStrategyForm(string supplierId, string levelType, string supplier_Name)
        {
            InitializeComponent();
            this.TB_SupplierName.Text = supplier_Name;
            this.supplierId = supplierId;
            this.levelType = levelType;
        }

        private void setStrategyForm_Load(object sender, EventArgs e)
        {
            this.TB_classifyType.Text = levelType;
            //加载策略类型
            CB_strategy.DisplayMember = "typeName";
            CB_strategy.ValueMember = "typeId";
            CB_strategy.DataSource =supplierValueBLL.getStrategyType();

            loadListBoxData();
            this.CB_strategy.SelectedIndexChanged += new EventHandler(this.CB_strategy_SelectedIndexChanged);
            
        }

        private void loadListBoxData()
        {
            CLB_strategy.DataSource = supplierValueBLL.getStrategyInfo(this.CB_strategy.SelectedValue.ToString());
            CLB_strategy.DisplayMember = "strategyInfo";
            CLB_strategy.ValueMember = "strategyId";
            DataTable dt = supplierValueBLL.getExsitStrategyBySupplierId(this.supplierId);
            String strategyId;
            for (int i = 0; i < CLB_strategy.Items.Count; i++)
            {
                this.CLB_strategy.SetSelected(i, true);
                strategyId = CLB_strategy.SelectedValue.ToString();
                for (int j = 0; j < dt.Rows.Count; j++)
                {
                   

                    if (strategyId.Equals(dt.Rows[j]["strategyId"].ToString()))
                    {
                        this.CLB_strategy.SetItemChecked(i, true);
                    }

                }
               
            }
            




           
        }

        private void CB_strategy_SelectedIndexChanged(object sender, EventArgs e)
        {
            //加载数据到checkListBox
            loadListBoxData();
        }
        //保存到数据库
        private void button1_Click(object sender, EventArgs e)
        {
            string strategyId = "";
            string strategyContent = "";
            bool flag = true;
            string tip;
            //删除原有策略信息
            supplierValueBLL.delExsitedStrategyInfo(this.supplierId);
                for (int i = 0; i < CLB_strategy.Items.Count; i++)
                {
                    if (CLB_strategy.GetItemChecked(i))
                    {
                        this.CLB_strategy.SetSelected(i, true);
                        strategyContent = CLB_strategy.GetItemText(CLB_strategy.Items[i]);
                        strategyId = CLB_strategy.SelectedValue.ToString();
                        try
                        {
                            flag &= supplierValueBLL.insertStategyInfo(strategyId, this.supplierId);
                        }
                        catch (Exception ex)
                        {
                            MessageUtil.ShowError(ex.Message);
                        }

                    }

                }

                if (flag)
                {
                    tip = "保存成功!";
                }
                else
                {
                    tip = "出错了!";
                }
            MessageUtil.ShowTips(tip);



        }
    }
}
