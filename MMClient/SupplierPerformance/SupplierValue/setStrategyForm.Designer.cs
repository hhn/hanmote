﻿namespace MMClient.SupplierPerformance.SupplierValue
{
    partial class setStrategyForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.TB_classifyType = new System.Windows.Forms.TextBox();
            this.TB_SupplierName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.CB_strategy = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.CLB_strategy = new System.Windows.Forms.CheckedListBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.TB_classifyType);
            this.groupBox1.Controls.Add(this.TB_SupplierName);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(3, 7);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(713, 71);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "抬头信息";
            // 
            // TB_classifyType
            // 
            this.TB_classifyType.Enabled = false;
            this.TB_classifyType.Location = new System.Drawing.Point(377, 27);
            this.TB_classifyType.Name = "TB_classifyType";
            this.TB_classifyType.Size = new System.Drawing.Size(165, 21);
            this.TB_classifyType.TabIndex = 3;
            // 
            // TB_SupplierName
            // 
            this.TB_SupplierName.Enabled = false;
            this.TB_SupplierName.Location = new System.Drawing.Point(103, 27);
            this.TB_SupplierName.Name = "TB_SupplierName";
            this.TB_SupplierName.Size = new System.Drawing.Size(165, 21);
            this.TB_SupplierName.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(306, 30);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 12);
            this.label2.TabIndex = 1;
            this.label2.Text = "区分类型：";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(21, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "供应商名称：";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.button1);
            this.groupBox2.Controls.Add(this.CB_strategy);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.CLB_strategy);
            this.groupBox2.Location = new System.Drawing.Point(3, 84);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(713, 454);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "制定策略";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(330, 22);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 5;
            this.button1.Text = "保存";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // CB_strategy
            // 
            this.CB_strategy.FormattingEnabled = true;
            this.CB_strategy.Location = new System.Drawing.Point(80, 26);
            this.CB_strategy.Name = "CB_strategy";
            this.CB_strategy.Size = new System.Drawing.Size(179, 20);
            this.CB_strategy.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 29);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 12);
            this.label3.TabIndex = 2;
            this.label3.Text = "策略类型：";
            // 
            // CLB_strategy
            // 
            this.CLB_strategy.ColumnWidth = 100;
            this.CLB_strategy.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.CLB_strategy.FormattingEnabled = true;
            this.CLB_strategy.Location = new System.Drawing.Point(3, 63);
            this.CLB_strategy.Name = "CLB_strategy";
            this.CLB_strategy.Size = new System.Drawing.Size(707, 388);
            this.CLB_strategy.TabIndex = 3;
            // 
            // setStrategyForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(720, 540);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "setStrategyForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "设置策略";
            this.Load += new System.EventHandler(this.setStrategyForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckedListBox CLB_strategy;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox CB_strategy;
        private System.Windows.Forms.TextBox TB_classifyType;
        private System.Windows.Forms.TextBox TB_SupplierName;
        private System.Windows.Forms.Button button1;
    }
}