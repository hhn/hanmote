﻿namespace MMClient.SupplierPerformance.SupplierValue
{
    partial class supplierClassifyList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgv_classifyResult = new System.Windows.Forms.DataGridView();
            this.RowNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Supplier_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Supplier_Name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Record_Time = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Supplier_Position = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.stratege = new System.Windows.Forms.DataGridViewButtonColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.CB_purOrg = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.DTP_sTime = new System.Windows.Forms.DateTimePicker();
            this.TB_SUpplierName = new System.Windows.Forms.TextBox();
            this.pageNext1 = new pager.pagetool.pageNext();
            this.DTP_eTime = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.CB_level = new System.Windows.Forms.ComboBox();
            this.btn_query = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_classifyResult)).BeginInit();
            this.SuspendLayout();
            // 
            // dgv_classifyResult
            // 
            this.dgv_classifyResult.AllowUserToAddRows = false;
            this.dgv_classifyResult.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.dgv_classifyResult.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_classifyResult.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.dgv_classifyResult.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgv_classifyResult.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_classifyResult.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.RowNumber,
            this.Supplier_ID,
            this.Supplier_Name,
            this.Record_Time,
            this.Supplier_Position,
            this.stratege});
            this.dgv_classifyResult.Location = new System.Drawing.Point(13, 98);
            this.dgv_classifyResult.Name = "dgv_classifyResult";
            this.dgv_classifyResult.RowTemplate.Height = 23;
            this.dgv_classifyResult.Size = new System.Drawing.Size(765, 547);
            this.dgv_classifyResult.TabIndex = 0;
            this.dgv_classifyResult.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_classifyResult_CellContentClick);
            // 
            // RowNumber
            // 
            this.RowNumber.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.RowNumber.DataPropertyName = "RowNumber";
            this.RowNumber.HeaderText = "序号";
            this.RowNumber.Name = "RowNumber";
            this.RowNumber.Width = 54;
            // 
            // Supplier_ID
            // 
            this.Supplier_ID.DataPropertyName = "Supplier_ID";
            this.Supplier_ID.HeaderText = "供应商编号";
            this.Supplier_ID.Name = "Supplier_ID";
            // 
            // Supplier_Name
            // 
            this.Supplier_Name.DataPropertyName = "Supplier_Name";
            this.Supplier_Name.HeaderText = "供应商名称";
            this.Supplier_Name.Name = "Supplier_Name";
            // 
            // Record_Time
            // 
            this.Record_Time.DataPropertyName = "Record_Time";
            this.Record_Time.HeaderText = "区分时间";
            this.Record_Time.Name = "Record_Time";
            // 
            // Supplier_Position
            // 
            this.Supplier_Position.DataPropertyName = "Supplier_Position";
            this.Supplier_Position.HeaderText = "区分结果";
            this.Supplier_Position.Name = "Supplier_Position";
            // 
            // stratege
            // 
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            dataGridViewCellStyle5.NullValue = "制定策略";
            this.stratege.DefaultCellStyle = dataGridViewCellStyle5;
            this.stratege.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.stratege.HeaderText = "制定策略";
            this.stratege.Name = "stratege";
            this.stratege.ReadOnly = true;
            this.stratege.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.stratege.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.stratege.Text = "制定策略";
            this.stratege.ToolTipText = "为供应商制定策略";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 12);
            this.label1.TabIndex = 1;
            this.label1.Text = "采购组织名称:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 12);
            this.label2.TabIndex = 3;
            this.label2.Text = "供应商名称:";
            // 
            // CB_purOrg
            // 
            this.CB_purOrg.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CB_purOrg.FormattingEnabled = true;
            this.CB_purOrg.Location = new System.Drawing.Point(95, 22);
            this.CB_purOrg.Name = "CB_purOrg";
            this.CB_purOrg.Size = new System.Drawing.Size(129, 20);
            this.CB_purOrg.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(333, 25);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 12);
            this.label3.TabIndex = 7;
            this.label3.Text = "时间:";
            // 
            // DTP_sTime
            // 
            this.DTP_sTime.Location = new System.Drawing.Point(378, 19);
            this.DTP_sTime.Name = "DTP_sTime";
            this.DTP_sTime.Size = new System.Drawing.Size(109, 21);
            this.DTP_sTime.TabIndex = 8;
            // 
            // TB_SUpplierName
            // 
            this.TB_SUpplierName.Location = new System.Drawing.Point(95, 57);
            this.TB_SUpplierName.Name = "TB_SUpplierName";
            this.TB_SUpplierName.Size = new System.Drawing.Size(129, 21);
            this.TB_SUpplierName.TabIndex = 9;
            // 
            // pageNext1
            // 
            this.pageNext1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pageNext1.Location = new System.Drawing.Point(0, 651);
            this.pageNext1.Name = "pageNext1";
            this.pageNext1.PageIndex = 1;
            this.pageNext1.PageSize = 100;
            this.pageNext1.RecordCount = 0;
            this.pageNext1.Size = new System.Drawing.Size(790, 37);
            this.pageNext1.TabIndex = 10;
            this.pageNext1.OnPageChanged += new System.EventHandler(this.pageNext1_OnPageChanged);
            // 
            // DTP_eTime
            // 
            this.DTP_eTime.Location = new System.Drawing.Point(516, 19);
            this.DTP_eTime.Name = "DTP_eTime";
            this.DTP_eTime.Size = new System.Drawing.Size(109, 21);
            this.DTP_eTime.TabIndex = 11;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(493, 25);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(17, 12);
            this.label4.TabIndex = 12;
            this.label4.Text = "--";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(330, 63);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 12);
            this.label5.TabIndex = 13;
            this.label5.Text = "优先级";
            // 
            // CB_level
            // 
            this.CB_level.AutoCompleteCustomSource.AddRange(new string[] {
            "--请选择--",
            "A",
            "B",
            "C",
            "D"});
            this.CB_level.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CB_level.FormattingEnabled = true;
            this.CB_level.Items.AddRange(new object[] {
            "",
            "A",
            "B",
            "C",
            "D"});
            this.CB_level.Location = new System.Drawing.Point(408, 57);
            this.CB_level.Name = "CB_level";
            this.CB_level.Size = new System.Drawing.Size(102, 20);
            this.CB_level.TabIndex = 14;
            // 
            // btn_query
            // 
            this.btn_query.Location = new System.Drawing.Point(550, 55);
            this.btn_query.Name = "btn_query";
            this.btn_query.Size = new System.Drawing.Size(75, 23);
            this.btn_query.TabIndex = 15;
            this.btn_query.Text = "查询";
            this.btn_query.UseVisualStyleBackColor = true;
            this.btn_query.Click += new System.EventHandler(this.btn_query_Click);
            // 
            // supplierClassifyList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(790, 688);
            this.Controls.Add(this.btn_query);
            this.Controls.Add(this.CB_level);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.DTP_eTime);
            this.Controls.Add(this.pageNext1);
            this.Controls.Add(this.TB_SUpplierName);
            this.Controls.Add(this.DTP_sTime);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.CB_purOrg);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dgv_classifyResult);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "supplierClassifyList";
            this.Text = "供应商区分结果清单";
            this.Load += new System.EventHandler(this.supplierClassifyList_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_classifyResult)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgv_classifyResult;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox CB_purOrg;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker DTP_sTime;
        private System.Windows.Forms.TextBox TB_SUpplierName;
        private pager.pagetool.pageNext pageNext1;
        private System.Windows.Forms.DateTimePicker DTP_eTime;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox CB_level;
        private System.Windows.Forms.Button btn_query;
        private System.Windows.Forms.DataGridViewTextBoxColumn RowNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn Supplier_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Supplier_Name;
        private System.Windows.Forms.DataGridViewTextBoxColumn Record_Time;
        private System.Windows.Forms.DataGridViewTextBoxColumn Supplier_Position;
        private System.Windows.Forms.DataGridViewButtonColumn stratege;
    }
}