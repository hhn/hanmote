﻿using Lib.Bll.SupplierPerformaceBLL;
using Lib.Common.CommonUtils;
using Lib.Model.SupplierManagementModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MMClient.SupplierPerformance.SupplierValue
{
    public partial class supplierClassifyList : WeifenLuo.WinFormsUI.Docking.DockContent
    {

        SupplierValueBLL supplierValueBLL = new SupplierValueBLL();
        conditionInfo conditionInfo = new conditionInfo();
        private int total;
        setStrategyForm setStrategyForm = null;

        public supplierClassifyList()
        {
            InitializeComponent();
        }
        
        private void supplierClassifyList_Load(object sender, EventArgs e)
        {
            //
            DTP_sTime.Value= DateTime.Now.AddYears(-1);
            //读取采购组织
            CB_purOrg.DataSource = supplierValueBLL.getPurOrg();

            //优先级默认选中第一个
            CB_level.SelectedIndex = 0;

            //加载数据
            pageNext1_Load();


            //激活下拉框change事件
            this.CB_purOrg.SelectedIndexChanged+= new System.EventHandler(this.CB_purOrg_SelectedIndexChanged);
        }
        /// <summary>
        /// 下拉选项
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CB_purOrg_SelectedIndexChanged(object sender, EventArgs e)
        {
            pageNext1_Load();
        }
        /// <summary>
        /// 分页控件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pageNext1_Load()
        {
            conditionInfo.PurId = CB_purOrg.Text;
            conditionInfo.SupplierName = TB_SUpplierName.Text;
            conditionInfo.STime = DTP_sTime.Value;
            conditionInfo.ETime = DTP_eTime.Value;
            conditionInfo.Level = CB_level.Text;
           
            total =supplierValueBLL.getRecordCount(conditionInfo);
            pageNext1.DrawControl(total);
            LoadData();
        }
        /// <summary>
        /// 加载分页数据
        /// </summary>
        private void LoadData()
        {
            //查询条件对象
            conditionInfo.PurId = CB_purOrg.Text;
            conditionInfo.SupplierName = TB_SUpplierName.Text;
            conditionInfo.STime = DTP_sTime.Value;
            conditionInfo.ETime = DTP_eTime.Value;
            conditionInfo.Level = CB_level.Text;
            conditionInfo.PageIndex = this.pageNext1.PageIndex;
            conditionInfo.PageSize = this.pageNext1.PageSize;
            //
            dgv_classifyResult.AutoGenerateColumns = false;
            //读取区分结果
            dgv_classifyResult.DataSource = supplierValueBLL.getClassifyResult(conditionInfo);
           /* if (dgv_classifyResult.Rows.Count == 0) {
                MessageUtil.ShowTips("未查询到数据!");

            }*/
        }
        /// <summary>
        /// 翻页事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pageNext1_OnPageChanged(object sender, EventArgs e)
        {
            pageNext1_Load();
        }
        /// <summary>
        /// 查询按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_query_Click(object sender, EventArgs e)
        {
            pageNext1_Load();

        }

        private void dgv_classifyResult_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

            //按钮点击事件
            if (dgv_classifyResult.Columns[e.ColumnIndex].Name.Equals("stratege")) {
               string Supplier_ID = dgv_classifyResult.CurrentRow.Cells["Supplier_ID"].Value.ToString();
                string Supplier_Position=  dgv_classifyResult.CurrentRow.Cells["Supplier_Position"].Value.ToString();
               string Supplier_Name= dgv_classifyResult.CurrentRow.Cells["Supplier_Name"].Value.ToString();
                //显示制定策略窗体
                  setStrategyForm = new setStrategyForm(Supplier_ID, Supplier_Position, Supplier_Name);
                  setStrategyForm.ShowDialog();
              

            }
        }
    }

}
