﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Bll.SupplierPerformaceBLL;
using WeifenLuo.WinFormsUI.Docking;
using MMClient.CertificationManagement.文件导入;
using Lib.SqlServerDAL;
using Lib.Bll.ServiceBll;
using Lib.Common.CommonUtils;

namespace MMClient.SupplierPerformance.SupplierValue
{
    public partial class BusinessValueAssessForm : DockContent
    {
        SupplierPerformanceBLL supplierPerformanceBLL = new SupplierPerformanceBLL();
        SupplierValueBLL businessValueBLL = new SupplierValueBLL();
        ServiceBill serviceBll = new ServiceBill();
        DataTable supplierInfoTable;
        String supplierID;
        String year;
        String assessName;
        String assessDate = DateTime.Now.ToString();
        Dictionary<string, string> supplierMap = null;
        Dictionary<string, string> mtGroupMap = null;
        private ImportFileForm importFileForm;

        public BusinessValueAssessForm()
        {
            InitializeComponent();

            //选择供应商id
           // supplierInfoTable = supplierPerformanceBLL.querySupplier();
           // comboBox_supplierID.DataSource = supplierInfoTable;
            //comboBox_supplierID.DisplayMember = supplierInfoTable.Columns["Supplier_ID"].ToString();
            //lable_supplierName.Text = (supplierInfoTable.Rows[comboBox_supplierID.SelectedIndex]["Supplier_Name"]).ToString();

            //在年份选项中给combobox添加2015年至现在的年份
            for (int i = DateTime.Now.Year; i >= 2015; i--)
            {
                string year = string.Format("{0}", i);
                comboBox_year.Items.Add(year);
            }
            comboBox_year.SelectedIndex = 0;
        }

        /// <summary>
        /// 供应商ID
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void comboBox_supplierID_SelectedIndexChanged(object sender, EventArgs e)
        {
            /*if (this.comboBox_supplierID.Text.Equals(""))
            {
                return;
            }
            //物料组
            DataTable dt = serviceBll.getMtGroupidAndName(this.comboBox_supplierID.SelectedValue.ToString());
            this.mtGroupId.DisplayMember = "mtGroupName";
            this.mtGroupId.ValueMember = "mtGroupId";
            this.mtGroupId.DataSource = dt;*/
        }

        /// <summary>
        /// 评估人
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBox15_TextChanged(object sender, EventArgs e)
        {
            assessName = textBox_name.Text.ToString();
        }

        /// <summary>
        /// 评估日期
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            assessDate = dateTimePicker1.Value.ToString();
        }

        /// <summary>
        /// 判断基本信息输入是否完整
        /// </summary>
        /// <returns></returns>
        private Boolean isInfoComplete()
        {
            
            if (String.IsNullOrEmpty(comboBox_supplierID.Text.ToString()))
            {
                MessageBox.Show("请选择供应商ID！");
                return false;
            }
            else if(String.IsNullOrEmpty(comboBox_year.Text.ToString()))
            {
                this.supplierID = this.comboBox_supplierID.SelectedValue.ToString();
                MessageBox.Show("请选择年份！");
                return false;
            }
            else if(String.IsNullOrEmpty(textBox_name.Text.ToString()))
            {
                MessageBox.Show("请输入评估人！");
                return false;
            }
            else if(String.IsNullOrEmpty(dateTimePicker1.Text.ToString()))
            {
                MessageBox.Show("请选择维护时间！");
                return false;
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        /// 计算公司份额 Button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button_portion_Click(object sender, EventArgs e)
        {
            //判断基本信息是否输入完整
            Boolean isInfoComplete = this.isInfoComplete();
            if (isInfoComplete == false)
            {
                return;
            }
            supplierID = this.comboBox_supplierID.SelectedValue.ToString();
            //全年采购业务额
            Decimal purchasingTurnover = businessValueBLL.queryPurchasingTurnover(supplierID, year);
           // Decimal purchasingTurnover = 1000;
            if (purchasingTurnover > 0)
            {
                textBox_purchasingTurnover.Text = Math.Round(purchasingTurnover,1).ToString();
            }
            else
            {
                textBox_purchasingTurnover.Text = "暂无数据";
            }

            //供应商营业额
            Decimal supplierTurnover = businessValueBLL.querySupplierTurnover(supplierID,year);
            //Decimal supplierTurnover = 2000;
            if (supplierTurnover > 0)
            {
                textBox_supplierTurnover.Text = Math.Round(supplierTurnover, 1).ToString();
            }
            else
            {
                textBox_supplierTurnover.Text = "暂无数据";
            }

            //计算公司份额
            Decimal companyPortion = 0M;
            if ((purchasingTurnover < 0) || (supplierTurnover <= 0))
            {
                textBox_companyPortion.Text = "缺少计算数据或供应商营业额为0";
                textBox_turnoverRst.Clear();
                return;
            }
            try
            {
                companyPortion = purchasingTurnover / supplierTurnover;
                //公司份额保留小数点后三位
                textBox_companyPortion.Text = Math.Round(companyPortion, 3).ToString();
            }
            catch
            {
                textBox_companyPortion.Text = "缺少计算数据或供应商营业额为0";
                textBox_turnoverRst.Clear();
                return;
            }
            
            //公司业务份额定位
            String turnoverRst = businessValueBLL.positionCompanyPortion(companyPortion);
            textBox_turnoverRst.Text = turnoverRst;
        }

        /// <summary>
        /// 年份
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void comboBox_year_SelectedIndexChanged(object sender, EventArgs e)
        {
            year = comboBox_year.Text.ToString();
        }

        /// <summary>
        /// 评估吸引力水平 Button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button_appeal_Click(object sender, EventArgs e)
        {
            //检查是否六个comboBox都有分数
            if (String.IsNullOrEmpty(comboBox1.Text.ToString()) || String.IsNullOrEmpty(comboBox2.Text.ToString()) || String.IsNullOrEmpty(comboBox3.Text.ToString()) || String.IsNullOrEmpty(comboBox4.Text.ToString()) || String.IsNullOrEmpty(comboBox5.Text.ToString()) || String.IsNullOrEmpty(comboBox6.Text.ToString()))
            {
                MessageBox.Show("请检查是否六个要素都已评分！");
                return;
            }

            Decimal score = 0M;
            //计算6个吸引力评估要素的总分
            try
            {
                score = score + Convert.ToDecimal(comboBox1.Text);
                score = score + Convert.ToDecimal(comboBox2.Text);
                score = score + Convert.ToDecimal(comboBox3.Text);
                score = score + Convert.ToDecimal(comboBox4.Text);
                score = score + Convert.ToDecimal(comboBox5.Text);
                score = score + Convert.ToDecimal(comboBox6.Text);
            }
            catch
            {
                MessageBox.Show("分数计算失败，请重试！");
                return;
            }

            //吸引力水平定位
            String appealRst = businessValueBLL.positionAppealLevel(score);
            //显示吸引力水平定位
            textBox_appealScore.Text = score.ToString();
            textBox_appealRst.Text = appealRst;
           
        }

        /// <summary>
        /// 查看供应商关系细分 Button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button_check_Click(object sender, EventArgs e)
        {
            //因为判断了如果计算方法返回为101，则为无数据，所以不需要单独判断是否有评估数据
            //计算方法中用else if而不是else（防止暂无数据的情况）
            String turnoverRst = textBox_turnoverRst.Text.ToString();
             String appealRst = textBox_appealRst.Text.ToString();
           // String turnoverRst = "H";
          // String appealRst = "M";
            if ((turnoverRst != "H") && (turnoverRst != "M") && (turnoverRst != "L") && (turnoverRst != "N"))
            {
                MessageBox.Show("缺少评估数据！");
                return;
            }
            else if ((appealRst != "H") && (appealRst != "M") && (appealRst != "L") && (appealRst != "N"))
            {
                MessageBox.Show("缺少评估数据！");
                return;
            }

            #region 采购品项定位结果
            //冯曦写的
             String supplyType = businessValueBLL.querySupplyType(supplierID);


            Random rd = new Random();
            if (rd.Next(1, 10) < 2.5 && rd.Next(1, 10) >= 1)
            {

                supplyType = "瓶颈";
            }
            else if (rd.Next(1, 10) < 5 && rd.Next(1, 10) >= 2.5)
            {
                supplyType = "关键";
            }
            else if (rd.Next(1, 10) < 7.5 && rd.Next(1, 10) >= 5)
            {
                supplyType = "一般";
            }
            else
            {
                supplyType = "杠杆";
            }



            if (supplyType == "101")
            {
                textBox_supplyType.Text = "暂无数据";
                return;
            }
            textBox_supplyType.Text = supplyType;

            #endregion

            String supplierValue = businessValueBLL.positionSupplier(turnoverRst,appealRst);
            TB_SupplierValue.Text = supplierValue;
            String supplierRst = businessValueBLL.segmentSupplier(supplierValue,supplyType);
            if (supplierRst == "101")
            {
                textBox_RelationshipSeg.Text = "暂无数据";
                return;
            }
            textBox_RelationshipSeg.Text = supplierRst;
            
        }
        
        /// <summary>
        /// 保存 Button 保存供应商定位的结果
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button_save_Click(object sender, EventArgs e)
        {
            try
            {
                this.isInfoComplete();
                //保存到数据表Supplier_Portion中
                List<Object> savePara = new List<Object>();
                savePara.Add(supplierID);
                //Company_Portion字段
                String companyPortion = textBox_turnoverRst.Text;
                if ((companyPortion != "H") && (companyPortion != "M") && (companyPortion != "L") && (companyPortion != "N"))
                {
                    savePara.Add(null);
                }
                else
                {
                    savePara.Add(companyPortion);
                }
                //Appeal_Level字段
                String appealLevel = textBox_appealRst.Text;
                if ((appealLevel != "H") && (appealLevel != "M") && (appealLevel != "L") && (appealLevel != "N"))
                {
                    savePara.Add(null);
                }
                else
                {
                    savePara.Add(textBox_appealRst.Text);
                }
                //Business_Value字段 计算供应商价值，因为前面没有在界面上展示出来
                String businessValue = businessValueBLL.positionSupplier((textBox_turnoverRst.Text), (textBox_appealRst.Text));
                if (businessValue == "101")
                {
                    savePara.Add(null);
                }
                else
                {
                    savePara.Add(businessValue);
                }
                //Relationship_Segment字段
                String relationshipSegment = textBox_RelationshipSeg.Text;
                if ((relationshipSegment != "S") && (relationshipSegment != "T") && (relationshipSegment != "C"))
                {
                    savePara.Add(null);
                }
                else
                {
                    savePara.Add(relationshipSegment);
                }
                //Supplier_Position字段
                String supplierPosition = textBox_result.Text;
                if ((supplierPosition != "A") && (supplierPosition != "B") && (supplierPosition != "C") && (supplierPosition != "D"))
                {
                    savePara.Add(null);
                }
                else
                {
                    savePara.Add(supplierPosition);
                }

                savePara.Add(comboBox_year.Text);
                savePara.Add(dateTimePicker1.Value.ToString());
                savePara.Add(textBox_name.Text);

                int lines = businessValueBLL.saveBusinessValue(savePara);
                if (lines > 0)
                {
                    MessageBox.Show("保存成功！");
                    return;
                }
                else
                {
                    MessageBox.Show("保存失败！");
                    return;
                }
            }
            catch
            {
                MessageBox.Show("信息未填写完整");
            }
        }

        /// <summary>
        /// Button 查看供应商分区结果（最终结果）
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button_checkResult_Click(object sender, EventArgs e)
        {
            //因为判断了如果计算方法返回为101，则为无数据，所以不需要单独判断是否有评估数据
            //计算方法中用else if而不是else（防止暂无数据的情况）

            #region 展示供应商评估结果
            //供应商评估结果（供应商分级）
             String classifyRst = supplierPerformanceBLL.queryNewClassificationResult(supplierID, comboBox_year.Text.ToString());
            //String classifyRst = "C";

            if (classifyRst == "A")
            {
                textBox_assessResult.Text = "卓越";
            }
            else if (classifyRst == "B")
            {
                textBox_assessResult.Text = "好";
            }
            else if ((classifyRst == "C") || (classifyRst == "D"))
            {
                textBox_assessResult.Text = "差";
            }
            else if (classifyRst == "101")
            {
                textBox_assessResult.Text = "暂无数据";
            }
            else
            {
                textBox_assessResult.Text = "暂无数据";
            }
            #endregion

            #region 供应商区分结果
            String supplierRelationship = textBox_RelationshipSeg.Text.ToString();
            String supplierRst = businessValueBLL.supplierPositionRst(classifyRst, supplierRelationship);
            if (supplierRst == "101")
            {
                textBox_result.Text = "暂无数据";
                return;
            }
            textBox_result.Text = supplierRst;
            #endregion 
        }

        private void button1_Click(object sender, EventArgs e)//导入采购业务额
        {
            /* Form1 f = new Form1();
             f.Show();*/
         
            if (this.importFileForm == null || this.importFileForm.IsDisposed)
            {
                this.importFileForm = new ImportFileForm();
            }
            importFileForm.Show();

            /*SupplierManageStrategy f = new SupplierManageStrategy();
            f.Show();*/
        }

        private void BusinessValueAssessForm_Load(object sender, EventArgs e)
        {

            textBox_name.Text = SingleUserInstance.getCurrentUserInstance().Username;
           
            String sql1 = " select Buyer_Org, Buyer_Org_Name from  Buyer_Org ";

            DataTable dt1 = DBHelper.ExecuteQueryDT(sql1);
            PurOrg.DisplayMember = "Buyer_Org_Name";
            PurOrg.ValueMember = "Buyer_Org";
            PurOrg.DataSource = dt1;
        }

     
        /// <summary>
        /// 查询采购组织名称
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PurOrg_SelectedIndexChanged(object sender, EventArgs e)
        {
              
            //TODO:根据采购组织查询供应商
            DataTable dt = serviceBll.getSupplierIdByPurId(this.PurOrg.SelectedValue.ToString());
            this.comboBox_supplierID.DisplayMember = "Supplier_Name";
            this.comboBox_supplierID.ValueMember = "Supplier_ID";
            this.comboBox_supplierID.DataSource = dt;


        }

        ///// <summary>
        ///// 清除控件中所有comboBox和textBox的内容
        ///// </summary>
        ///// <param name="ctrl"></param>
        //private void clearControls(Control ctrl)
        //{
        //    foreach (Control c in ctrl.Controls)
        //    {
        //        if (c is TextBox)
        //        {
        //            ((TextBox)(c)).Text =String.Empty;
        //        }
        //        else if(c is ComboBox)
        //        {
        //            ((ComboBox)(c)).Text = "";
        //        }
        //    }
        //}
    }
}
