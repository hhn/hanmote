﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;
using Lib.Bll.SupplierPerformaceBLL;

namespace MMClient.SupplierPerformance
{
    public partial class SupplierPerformanceBatchAddForm : DockContent
    {
        String materialID;
        List<String> supplierIDList = new List<String>();
        DataTable materialInfoTable;
        DateTime startTime = new DateTime();
        DateTime endTime = new DateTime();

        //用户自定义参数
        List<double> mainStandardWeight = new List<double>();//存放主标准权重(放5个数)

        List<double> priceSSWeight = new List<double>();//存放价格次标准权重（放2个数）
        List<double> qualitySSWeight = new List<double>();//存放质量次标准权重（放3个数）
        List<double> deliverySSWeight = new List<double>();//存放收获次标准权重（放4个数）

        String assessName;
        String assessDate = DateTime.Now.ToString();//修改

        List<double> priceLevelPara = new List<double>();
        List<double> priceHistoryPara = new List<double>();
        List<double> PPMPara = new List<double>();
        //用list传递值，之后将其中的值赋值给double型变量paraConfirmDate
        List<double> confirmDatePara = new List<double>();

        SupplierPerformanceBLL supplierPerformanceBLL = new SupplierPerformanceBLL();

        Decimal priceLevelScore, priceHistoryScore, priceScore, receiveGoodsScore, qualityAuditScore, complaintRejectScore, qualityScore;
        Decimal deliverOnTimeScore, confirmDateScore, quantityReliabilityScore, goodsShipmentScore, deliverScore, serviceSupportScore, externalServiceScore, totalScore;

        /// <summary>
        /// 初始化窗体
        /// </summary>
        public SupplierPerformanceBatchAddForm()
        {
            InitializeComponent();

            //选择物料id
            materialInfoTable = supplierPerformanceBLL.queryMaterial();
            comboBox_materialID.DataSource = materialInfoTable;
            comboBox_materialID.DisplayMember = materialInfoTable.Columns["Material_ID"].ToString();
            label_materialName.Text = (materialInfoTable.Rows[comboBox_materialID.SelectedIndex]["Material_Name"]).ToString();

            comboBox5.Items.Add("定期评估");
            comboBox5.Items.Add("累计评估");

            comboBox_weightMS.Items.Add("01");
            comboBox_weightMS.Items.Add("02");
            comboBox_weightMS.Items.Add("03");

            comboBox_weightSS.Items.Add("01");
            comboBox_weightSS.Items.Add("02");

            //在年份选项中给combobox添加2015年至现在的年份
            for (int i = DateTime.Now.Year; i >= 2015; i--)
            {
                string year = string.Format("{0}", i);
                comboBox_year.Items.Add(year);
            }
            comboBox_year.SelectedIndex = 0;
        }

        /// <summary>
        /// 评估 Button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            materialID = comboBox_materialID.Text.ToString();
            //判断用户输入的评估信息是否完整
            if (materialID == null || startTime == null || endTime == null || mainStandardWeight == null || priceSSWeight == null || qualitySSWeight == null || deliverySSWeight == null || assessName == null || assessDate == null)
            {
                MessageBox.Show("请输入完整的评估信息！");
                return;
            }
            else if (mainStandardWeight.Count != 5 || priceSSWeight.Count != 2 || qualitySSWeight.Count != 3 || deliverySSWeight.Count != 4)
            {
                MessageBox.Show("请检查主标准和次标准的权重！");
                return;
            }

            //显示用户自定义参数的界面
            UserDefineForm userDefineForm = new UserDefineForm(priceLevelPara, priceHistoryPara, PPMPara, confirmDatePara);
            userDefineForm.Owner = this;
            userDefineForm.ShowDialog();

            //检查参数是否录入成功
            if (priceLevelPara.Count != 2 || priceHistoryPara.Count != 2 || PPMPara.Count != 2 || confirmDatePara.Count != 1)
            {
                MessageBox.Show("评估自定义参数录入不成功！");
                return;
            }

            //若用户已输入完整的信息，则启动评估
            //选择符合条件的供应商列表
            supplierIDList.Clear();
            supplierIDList = supplierPerformanceBLL.querySupplier(materialID);
            
            //若查询出的待评估供应商列表为空，则返回
            if (supplierIDList.Count < 1)
            {
                MessageBox.Show("当前尚无待评估的供应商！");
                return;
            }

            dataGridView.Rows.Clear();
            //遍历每一个供应商ID，进行绩效评估和结果保存
            int count = dataGridView.Rows.Count;
      
            for (int i = 0; i < supplierIDList.Count; i++)
            {
                String supplierID = supplierIDList[i];

                if (!String.IsNullOrEmpty(supplierID))
                {
                    int index = this.dataGridView.Rows.Add();
                    this.dataGridView.Rows[index].Cells[0].Value = supplierID;
                    try
                    {
                        assess(supplierID);
                        this.dataGridView.Rows[index].Cells[1].Value = "是";
                    }
                    catch
                    {
                        this.dataGridView.Rows[index].Cells[1].Value = "否";//未完成评估
                        continue;
                    }

                    //评估结果保存
                    int lines1 = save(supplierID);
                    if (lines1 > 0)
                    {
                        this.dataGridView.Rows[index].Cells[2].Value = "是";
                    }
                    else
                    {
                        this.dataGridView.Rows[index].Cells[2].Value = "否";
                    }
                }
                
            }

                
        }

        /// <summary>
        /// 供应商绩效评估
        /// </summary>
        /// <param name="supplierID"></param>
        private void assess(String supplierID)
        {
            //分数为值101为异常的标志

            //主标准：价格 OK
            //调用计算 价格-价格水平 分数的方法，并显示在界面上
            priceLevelScore = supplierPerformanceBLL.queryPriceLevel(materialID, supplierID, startTime, endTime, priceLevelPara);

            //调用计算 价格-价格历史 分数的方法，并显示在界面上
            priceHistoryScore = supplierPerformanceBLL.queryPriceHistory(materialID, supplierID, startTime, endTime, priceHistoryPara);

            //调用计算 价格 分数的方法，并显示在界面上
            if ((priceLevelScore > 100) || (priceHistoryScore > 100))
            {
                priceScore = 101;
            }
            else
            {
                priceScore = supplierPerformanceBLL.calculatePriceScore(priceLevelScore, priceHistoryScore, priceSSWeight);
            }

            //主标准：质量
            //调用计算 质量-收货 分数的方法，并显示在界面上
            receiveGoodsScore = supplierPerformanceBLL.queryReceiveGoods(materialID, supplierID, startTime, endTime, PPMPara);

            //调用计算 质量-质量审计 分数的方法，并显示在界面上
            qualityAuditScore = supplierPerformanceBLL.queryQualityAudit(materialID, supplierID, startTime, endTime);

            //调用计算 质量-抱怨/拒绝水平 分数的方法，并显示在界面上
            complaintRejectScore = supplierPerformanceBLL.queryComplaintReject(materialID, supplierID, startTime, endTime);

            //调用计算 质量 分数的方法，并显示在界面上
            if ((receiveGoodsScore > 100) || (qualityAuditScore > 100) || (complaintRejectScore > 100))
            {
                qualityScore = 101;
            }
            else
            {
                qualityScore = supplierPerformanceBLL.calculateQualityScore(receiveGoodsScore, qualityAuditScore, complaintRejectScore, qualitySSWeight);
            }

            //主标准：交货

            //调用计算 交货-按时交货的表现 分数的方法，并显示在界面上
            deliverOnTimeScore = supplierPerformanceBLL.queryDeliverOnTime(materialID, supplierID, startTime, endTime);

            //调用计算 交货-确认日期 分数的方法，并显示在界面上
            double paraConfirmDate = confirmDatePara[0];
            confirmDateScore = supplierPerformanceBLL.queryConfirmDate(materialID, supplierID, startTime, endTime, paraConfirmDate);

            //调用计算 交货-数量可靠性 的分数的方法，并显示在界面上
            quantityReliabilityScore = supplierPerformanceBLL.queryQuantityReliability(materialID, supplierID, startTime, endTime);

            //调用计算 交货-对装运须知的遵守 的分数的方法，并显示在界面上
            goodsShipmentScore = supplierPerformanceBLL.queryGoodsShipment(materialID, supplierID, startTime, endTime);


            //调用计算 交货 分数的方法，并显示在界面上
            if ((deliverOnTimeScore > 100) || (confirmDateScore > 100) || (quantityReliabilityScore > 100) || (goodsShipmentScore > 100))
            {
                deliverScore = 101;
            }
            else
            {
                deliverScore = supplierPerformanceBLL.calculateDeliverScore(deliverOnTimeScore, confirmDateScore, quantityReliabilityScore, goodsShipmentScore, deliverySSWeight);
            }

            //主标准：一般服务/支持

            //调用计算 一般服务/支持 分数的方法，并显示在界面上    
            serviceSupportScore = supplierPerformanceBLL.queryServiceSupport(materialID, supplierID, startTime, endTime);

            //主标准：外部服务

            //调用计算 外部服务 分数的方法，并显示在界面上
            externalServiceScore = supplierPerformanceBLL.queryExternalService(materialID, supplierID, startTime, endTime);

            //调用计算 总分 分数的方法，并显示在界面上
            if ((priceScore > 100) || (qualityScore > 100) || (deliverScore > 100) || (serviceSupportScore > 100) || (externalServiceScore > 100))
            {
                totalScore = 101;
            }
            else
            {
                totalScore = supplierPerformanceBLL.calculateTotalScore(priceScore, qualityScore, deliverScore, serviceSupportScore, externalServiceScore, mainStandardWeight);
            }
        }

        /// <summary>
        /// 供应商绩效评估结果保存
        /// </summary>
        /// <param name="supplierID"></param>
        private int save(String supplierID)
        {
            List<Object> saveRstPara = new List<object>();
            if (saveRstPara.Count != 0)
            {
                saveRstPara.Clear();
            }
            saveRstPara.Add(supplierID);

            //采购组织添加null
            saveRstPara.Add("null");
            saveRstPara.Add(materialID);

            #region 将分数添加到saveRstPara中（判断添加分数还是字符串“null”）
            
            //总分
            if(totalScore <= 100)
            {
                saveRstPara.Add(totalScore);
            }
            else
            {
                saveRstPara.Add("null");
            }

            //价格
            if (priceScore <= 100)
            {
                saveRstPara.Add(priceScore);
            }
            else
            {
                saveRstPara.Add("null");
            }
            //价格水平
            if (priceLevelScore <= 100)
            {
                saveRstPara.Add(priceLevelScore);
            }
            else
            {
                saveRstPara.Add("null");
            }
            //价格历史
            if (priceHistoryScore <= 100)
            {
                saveRstPara.Add(priceHistoryScore);
            }
            else
            {
                saveRstPara.Add("null");
            }
            //质量
            if (qualityScore <= 100)
            {
                saveRstPara.Add(qualityScore);
            }
            else
            {
                saveRstPara.Add("null");
            }
            //收货
            if (receiveGoodsScore <= 100)
            {
                saveRstPara.Add(receiveGoodsScore);
            }
            else
            {
                saveRstPara.Add("null");
            }

            //质量审计
            if (qualityAuditScore <= 100)
            {
                saveRstPara.Add(qualityAuditScore);
            }
            else
            {
                saveRstPara.Add("null");
            }

            //抱怨/拒绝水平
            if (complaintRejectScore <= 100)
            {
                saveRstPara.Add(complaintRejectScore);
            }
            else
            {
                saveRstPara.Add("null");
            }

            //交货
            if (deliverScore <= 100)
            {
                saveRstPara.Add(deliverScore);
            }
            else
            {
                saveRstPara.Add("null");
            }

            //按时交货的表现
            if (deliverOnTimeScore <= 100)
            {
                saveRstPara.Add(deliverOnTimeScore);
            }
            else
            {
                saveRstPara.Add("null");
            }

            //确认日期
            if (confirmDateScore <= 100)
            {
                saveRstPara.Add(confirmDateScore);
            }
            else
            {
                saveRstPara.Add("null");
            }

            //数量可靠性
            if (quantityReliabilityScore <= 100)
            {
                saveRstPara.Add(quantityReliabilityScore);
            }
            else
            {
                saveRstPara.Add("null");
            }

            //对装运须知的遵守
            if (goodsShipmentScore <= 100)
            {
                saveRstPara.Add(goodsShipmentScore);
            }
            else
            {
                saveRstPara.Add("null");
            }

            //一般服务/支持
            if (serviceSupportScore <= 100)
            {
                saveRstPara.Add(serviceSupportScore);
            }
            else
            {
                saveRstPara.Add("null");
            }

            //外部服务
            if (externalServiceScore <= 100)
            {
                saveRstPara.Add(externalServiceScore);
            }
            else
            {
                saveRstPara.Add("null");
            }
            #endregion

            //评估类型
            if (comboBox5.Text == "累计评估")
            {
                saveRstPara.Add(1);
            }
            else
            {
                saveRstPara.Add(0);
            }

            StringBuilder timePeriod = new StringBuilder();
            timePeriod.Append(comboBox_year.Text).Append("年").Append(comboBox3.Text);
            saveRstPara.Add(timePeriod.ToString());//时间范围

            saveRstPara.Add(assessName);
            saveRstPara.Add(assessDate);

            int lines = supplierPerformanceBLL.saveResult(saveRstPara);
            return lines;
           
        }

        #region 基本信息
        /// <summary>
        /// 给评估时间赋值，startTime,endTime
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void comboBox3_SelectedIndexChanged(object sender, EventArgs e)
        {
            String year;
            //判断用户是否输入了年份
            if (comboBox_year.Text == null)
            {
                year = DateTime.Now.Year.ToString();
            }
            else
            {
                year = comboBox_year.Text;
            }


            switch (comboBox3.Text.ToString())
            {
                case "1月":
                    startTime = Convert.ToDateTime(year + "-" + "1-1");
                    endTime = Convert.ToDateTime(year + "-" + "2-1");
                    break;
                case "2月":
                    startTime = Convert.ToDateTime(year + "-" + "2-1");
                    endTime = Convert.ToDateTime(year + "-" + "3-1");
                    break;
                case "3月":
                    startTime = Convert.ToDateTime(year + "-" + "3-1");
                    endTime = Convert.ToDateTime(year + "-" + "4-1");
                    break;
                case "4月":
                    startTime = Convert.ToDateTime(year + "-" + "4-1");
                    endTime = Convert.ToDateTime(year + "-" + "5-1");
                    break;
                case "5月":
                    startTime = Convert.ToDateTime(year + "-" + "5-1");
                    endTime = Convert.ToDateTime(year + "-" + "6-1");
                    break;
                case "6月":
                    startTime = Convert.ToDateTime(year + "-" + "6-1");
                    endTime = Convert.ToDateTime(year + "-" + "7-1");
                    break;
                case "7月":
                    startTime = Convert.ToDateTime(year + "-" + "7-1");
                    endTime = Convert.ToDateTime(year + "-" + "8-1");
                    break;
                case "8月":
                    startTime = Convert.ToDateTime(year + "-" + "8-1");
                    endTime = Convert.ToDateTime(year + "-" + "9-1");
                    break;
                case "9月":
                    startTime = Convert.ToDateTime(year + "-" + "9-1");
                    endTime = Convert.ToDateTime(year + "-" + "10-1");
                    break;
                case "10月":
                    startTime = Convert.ToDateTime(year + "-" + "10-1");
                    endTime = Convert.ToDateTime(year + "-" + "11-1");
                    break;
                case "11月":
                    startTime = Convert.ToDateTime(year + "-" + "11-1");
                    endTime = Convert.ToDateTime(year + "-" + "12-1");
                    break;
                case "12月":
                    startTime = Convert.ToDateTime(year + "-" + "12-1");
                    endTime = Convert.ToDateTime(year + "-" + "12-31" + " 23:59:59");
                    break;
                case "1月-2月":
                    startTime = Convert.ToDateTime(year + "-" + "1-1");
                    endTime = Convert.ToDateTime(year + "-" + "3-1");
                    break;
                case "1月-3月":
                    startTime = Convert.ToDateTime(year + "-" + "1-1");
                    endTime = Convert.ToDateTime(year + "-" + "4-1");
                    break;
                case "1月-4月":
                    startTime = Convert.ToDateTime(year + "-" + "1-1");
                    endTime = Convert.ToDateTime(year + "-" + "5-1");
                    break;
                case "1月-5月":
                    startTime = Convert.ToDateTime(year + "-" + "1-1");
                    endTime = Convert.ToDateTime(year + "-" + "6-1");
                    break;
                case "1月-6月":
                    startTime = Convert.ToDateTime(year + "-" + "1-1");
                    endTime = Convert.ToDateTime(year + "-" + "7-1");
                    break;
                case "1月-7月":
                    startTime = Convert.ToDateTime(year + "-" + "1-1");
                    endTime = Convert.ToDateTime(year + "-" + "8-1");
                    break;
                case "1月-8月":
                    startTime = Convert.ToDateTime(year + "-" + "1-1");
                    endTime = Convert.ToDateTime(year + "-" + "9-1");
                    break;
                case "1月-9月":
                    startTime = Convert.ToDateTime(year + "-" + "1-1");
                    endTime = Convert.ToDateTime(year + "-" + "100-1");
                    break;
                case "1月-10月":
                    startTime = Convert.ToDateTime(year + "-" + "1-1");
                    endTime = Convert.ToDateTime(year + "-" + "11-1");
                    break;
                case "1月-11月":
                    startTime = Convert.ToDateTime(year + "-" + "1-1");
                    endTime = Convert.ToDateTime(year + "-" + "11-1");
                    break;
                case "1月-12月":
                    startTime = Convert.ToDateTime(year + "-" + "1-1");
                    endTime = Convert.ToDateTime(year + "-" + "12-31" + " 23:59:59");
                    break;
                default:
                    MessageBox.Show("请选择正确的评估时间范围！");
                    break;
            }
        }

        /// <summary>
        /// 评估时间，时间范围
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void comboBox5_SelectedIndexChanged(object sender, EventArgs e)
        {
            comboBox3.Items.Clear();
            //根据选定的评估类型，更改时间范围下拉框的选项
            if (comboBox5.Text.ToString() == "定期评估")
            {
                comboBox3.Enabled = true;
                comboBox3.Items.Add("1月");
                comboBox3.Items.Add("2月");
                comboBox3.Items.Add("3月");
                comboBox3.Items.Add("4月");
                comboBox3.Items.Add("5月");
                comboBox3.Items.Add("6月");
                comboBox3.Items.Add("7月");
                comboBox3.Items.Add("8月");
                comboBox3.Items.Add("9月");
                comboBox3.Items.Add("10月");
                comboBox3.Items.Add("11月");
                comboBox3.Items.Add("12月");
            }
            else if (comboBox5.Text.ToString() == "累计评估")
            {
                comboBox3.Enabled = true;
                comboBox3.Items.Add("1月-2月");
                comboBox3.Items.Add("1月-3月");
                comboBox3.Items.Add("1月-4月");
                comboBox3.Items.Add("1月-5月");
                comboBox3.Items.Add("1月-6月");
                comboBox3.Items.Add("1月-7月");
                comboBox3.Items.Add("1月-8月");
                comboBox3.Items.Add("1月-9月");
                comboBox3.Items.Add("1月-10月");
                comboBox3.Items.Add("1月-11月");
                comboBox3.Items.Add("1月-12月");
            }
        }

        /// <summary>
        /// 主标准权重输入
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void comboBox_weightMS_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox_weightMS.SelectedItem.ToString() == "03")
            {
                MainStandardWeightForm msWeightForm = new MainStandardWeightForm(mainStandardWeight);
                msWeightForm.Owner = this;
                msWeightForm.ShowDialog();
            }
            else if (comboBox_weightMS.SelectedItem.ToString() == "01")
            {
                int i = 0;
                //检查list是否为空，如果不为空，则置空
                if (mainStandardWeight.Count != 0)
                {
                    mainStandardWeight.Clear();
                }
                //将每个主标准权重设置为1
                while (i < 5)
                {
                    mainStandardWeight.Add(1);
                    i++;
                }
            }
            else if (comboBox_weightMS.SelectedItem.ToString() == "02")
            {
                //检查list是否为空，如果不为空，则置空
                if (mainStandardWeight.Count != 0)
                {
                    mainStandardWeight.Clear();
                }
                mainStandardWeight.Add(5);
                mainStandardWeight.Add(3);
                mainStandardWeight.Add(2);
                mainStandardWeight.Add(1);
                mainStandardWeight.Add(2);
            }
            else
            {
                MessageBox.Show("请选择主标准权重代码！");
            }

        }

        /// <summary>
        /// 次标准权重输入
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void comboBox_weightSS_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox_weightSS.SelectedItem.ToString() == "01")
            {
                //如果已经有值，清空
                if (priceSSWeight.Count != 0)
                {
                    priceSSWeight.Clear();
                }
                if (qualitySSWeight.Count != 0)
                {
                    qualitySSWeight.Clear();
                }
                if (deliverySSWeight.Count != 0)
                {
                    deliverySSWeight.Clear();
                }

                int i = 0;
                //将每个价格次标准权重设置为1，共2个
                while (i < 2)
                {
                    priceSSWeight.Add(1);
                    i++;
                }

                //将每个质量次标准权重设置为1，共3个
                int j = 0;
                while (j < 3)
                {
                    qualitySSWeight.Add(1);
                    j++;
                }

                //将每个交货次标准权重设置为1，共4个
                int k = 0;
                while (k < 4)
                {
                    deliverySSWeight.Add(1);
                    k++;
                }

            }
            else if (comboBox_weightSS.SelectedItem.ToString() == "02")
            {
                SecondaryStandardWeightForm ssWeightForm = new SecondaryStandardWeightForm(priceSSWeight, qualitySSWeight, deliverySSWeight);
                ssWeightForm.Owner = this;
                ssWeightForm.ShowDialog();
            }
            else
            {
                MessageBox.Show("请选择次标准权重代码！");
            }
        }

        /// <summary>
        /// 评估人
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBox15_TextChanged(object sender, EventArgs e)
        {
            assessName = textBox15.Text.ToString();
        }

        /// <summary>
        /// 评估日期
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            assessDate = dateTimePicker1.Value.ToString();
        }

        /// <summary>
        /// 物料ID
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void comboBox_materialID_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (materialInfoTable.Rows.Count > 0)
            {
                label_materialName.Text = (materialInfoTable.Rows[comboBox_materialID.SelectedIndex]["Material_Name"]).ToString();
            }
        }
        #endregion
    }
}
