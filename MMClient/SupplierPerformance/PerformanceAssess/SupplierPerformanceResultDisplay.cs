﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MMClient.ContractManage;
using Lib.Bll.SupplierPerformaceBLL;
using WeifenLuo.WinFormsUI.Docking;
using Lib.SqlServerDAL;
using Lib.Common.CommonUtils;
using Lib.Common.MMCException.Bll;
using Lib.Common.MMCException.IDAL;

namespace MMClient.SupplierPerformance
{
    public partial class SupplierPerformanceResultDisplay : DockContent
    {
        private string materialID;
        private string supplierID;
        private string purchasingORGID;
        private string condition;
        private DataTable supplierInfoTable, materialInfoTable, purchasingORGInfoTable;
        //保存数据库中的ID
        List<int> numList = new List<int>();

        SupplierPerformanceBLL supplierPerformanceBLL = new SupplierPerformanceBLL();
        public SupplierPerformanceResultDisplay()
        {
            InitializeComponent();

            //选择供应商id
            supplierInfoTable = supplierPerformanceBLL.querySupplier();
            comboBox_supplierID.DataSource = supplierInfoTable;
            comboBox_supplierID.DisplayMember = supplierInfoTable.Columns["Supplier_ID"].ToString();
            label_supplierName.Text = (supplierInfoTable.Rows[comboBox_supplierID.SelectedIndex]["Supplier_Name"]).ToString();
        }

        //button 查看
        private void button1_Click(object sender, EventArgs e)
        {
            condition = "";
            if(!string.IsNullOrEmpty(materialID))
            {
                condition += " AND  Material_ID = '" + comboBox_materialID.Text.ToString().Trim()+"'"; 
            }
            if (!string.IsNullOrEmpty(supplierID))
            {
                condition += " AND  Supplier_ID = '" + comboBox_supplierID.Text.ToString().Trim() + "'";
            }
            if (!string.IsNullOrEmpty(purchasingORGID))
            {
                condition += " AND  PurchasingORG_ID = '" + comboBox_purchasingORGID.Text.ToString().Trim() + "'";
            }
            DataTable resultTable = supplierPerformanceBLL.queryResult(materialID, supplierID, purchasingORGID);

            //添加之前先清空DataGridView
            dataGridView.Rows.Clear();
            numList.Clear();//清除保存ID的list

            for(int i = 0; i <resultTable.Rows.Count;i++)
            {
                //添加一行(!!!)
                this.dataGridView.Rows.Add();
                DataGridViewRow row = this.dataGridView.Rows[i];
                //显示编号
                row.Cells[0].Value = Convert.ToString(i + 1);
               //类型 
                if (Convert.ToString(resultTable.Rows[i][0]) == "True")
               {
                   row.Cells[1].Value = "累计评估";
               }
               else
               {
                   row.Cells[1].Value = "定期评估";
               }

                //时间范围
                row.Cells[2].Value = resultTable.Rows[i][1];
                //评估人
                row.Cells[3].Value = resultTable.Rows[i][2];
                //评估时间
                row.Cells[4].Value = resultTable.Rows[i][3];
                //价格
                row.Cells[5].Value = resultTable.Rows[i][4];
                //质量
                row.Cells[6].Value = resultTable.Rows[i][5];
                //收获
                row.Cells[7].Value = resultTable.Rows[i][6];
                //一般服务/支持
                row.Cells[8].Value = resultTable.Rows[i][7];
                //外部服务
                row.Cells[9].Value = resultTable.Rows[i][8];
                //总分
                row.Cells[10].Value = resultTable.Rows[i][9];
                //添加Button
                row.Cells[11].Value = "查看详情";
                //添加数据库中的编号id，在查询详细信息时使用，不显示在界面上
                numList.Add(Convert.ToInt16(resultTable.Rows[i][13]));
            }
            pageTool_Load(sender, e);

        }
        
        /// <summary>
        /// 当鼠标点击DataGridView的内容，如果是Button，则弹出窗口
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridView_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {
            //如果是“查看详情”button
            try
            {
                if (dataGridView.Columns[e.ColumnIndex] is DataGridViewButtonColumn)
                {
                    int rowNum = e.RowIndex;
                    int idNum = numList[rowNum];

                    DetailedResultForm drForm = new DetailedResultForm(materialID, supplierID, purchasingORGID, idNum);
                    drForm.Owner = this;
                    drForm.ShowDialog();
                }
            }
            catch
            {
                MessageBox.Show("当前尚无记录可查看！");
                return;
            }
        }

        //供应商代码
        private void comboBox_supplierID_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            supplierID = comboBox_supplierID.Text.ToString();
            if (supplierInfoTable.Rows.Count > 0)
            {
                label_supplierName.Text = supplierInfoTable.Rows[comboBox_supplierID.SelectedIndex]["Supplier_Name"].ToString();
            }
            
            //选择的供应商变化时，则重新查询对应的采购组织信息
            purchasingORGInfoTable = supplierPerformanceBLL.queryPurchasingORG(supplierID);
            comboBox_purchasingORGID.DataSource = purchasingORGInfoTable;
            if (purchasingORGInfoTable.Rows.Count > 0)
            {
                comboBox_purchasingORGID.DisplayMember = purchasingORGInfoTable.Columns["PurchasingORG_ID"].ToString();
                label_purchasingORGName.Text = (purchasingORGInfoTable.Rows[comboBox_purchasingORGID.SelectedIndex]["PurchasingORG_Name"]).ToString();
            }

            //选择的供应商变化时，则重新查询对应的物料信息
            materialInfoTable = supplierPerformanceBLL.queryMaterialID(supplierID);
            comboBox_materialID.DataSource = materialInfoTable;
            if (materialInfoTable.Rows.Count > 0)
            {
                comboBox_materialID.DisplayMember = materialInfoTable.Columns["Material_ID"].ToString();
                label_materialName.Text = (materialInfoTable.Rows[comboBox_materialID.SelectedIndex]["Material_Name"]).ToString();
            }
        }

        private void pageTool_Load(object sender, EventArgs e)
        {
            string sql = "SELECT count(*) from [Supplier_Performance] where 1=1 " + condition;
            try
            {
                int i = int.Parse(DBHelper.ExecuteQueryDT(sql).Rows[0][0].ToString());
                int totalNum = i;
                pageTool.DrawControl(totalNum);
                //事件改变调用函数
                pageTool.OnPageChanged += pageTool_OnPageChanged;
            }
            catch (Lib.Common.MMCException.Bll.BllException ex)
            {
                MessageUtil.ShowTips("数据库异常，请稍后重试");
                return;
            }
        }

        private void pageTool_OnPageChanged(object sender, EventArgs e)
        {
            LoadData();
        }
        private void LoadData()
        {
            try
            {
                dataGridView.DataSource = findConditionalInforByPage(this.pageTool.PageSize, this.pageTool.PageIndex);
            }
            catch (BllException ex)
            {
                MessageUtil.ShowError(ex.Message);
            }
        }
        private object findConditionalInforByPage(int pageSize, int pageIndex)
        {
            try
            {
                string sql = @"SELECT top " + pageSize + "IsCulmunativeAssess,Evaluation_Period,Creator_Name,Evaluation_Time, Price_Score,Quality_Score,Delivery_Score,GeneralServiceAndSupport_Score,ExternalService_Score, Total_Score,Supplier_ID,Material_ID,PurchasingORG_ID,ID   FROM [Supplier_Performance] where 1=1  "+condition+" AND  ID  not in(select top " + pageSize * (pageIndex - 1) + " ID from Supplier_Performance where 1=1  " + condition + "   ORDER BY ID ASC)ORDER BY ID";
                return DBHelper.ExecuteQueryDT(sql);
            }
            catch (DBException ex)
            {
                throw new BllException("当前无数据", ex);
            };
        }

        //物料ID
        private void comboBox_materialID_SelectedIndexChanged(object sender, EventArgs e)
        {
            materialID = comboBox_materialID.Text.ToString();
            if (materialInfoTable.Rows.Count > 0)
            {
                label_materialName.Text = (materialInfoTable.Rows[comboBox_materialID.SelectedIndex]["Material_Name"]).ToString();
            }
        }

        //采购组织ID
        private void comboBox_purchasingORGID_SelectedIndexChanged(object sender, EventArgs e)
        {
            purchasingORGID = comboBox_purchasingORGID.Text.ToString();
            if (purchasingORGInfoTable.Rows.Count > 0)
            {
                label_purchasingORGName.Text = (purchasingORGInfoTable.Rows[comboBox_purchasingORGID.SelectedIndex]["PurchasingORG_Name"]).ToString();
            }
        }
    }
}
