﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Bll.SupplierPerformaceBLL;
using WeifenLuo.WinFormsUI.Docking;

namespace MMClient.SupplierPerformance
{
    public partial class NoSupplierPerformanceList : DockContent
    {
        SupplierPerformanceBLL supplierPerformanceBLL = new SupplierPerformanceBLL();
        DataTable noSPList;
        DateTime startTime = new DateTime();
        DateTime endTime = new DateTime();
        public NoSupplierPerformanceList()
        {
            InitializeComponent();
        }

        //查询按钮
        private void button1_Click(object sender, EventArgs e)
        {
            //根据选定的评估类型，更改时间范围下拉框的选项
            if (comboBox5.SelectedItem.ToString() == "定期评估")
            {
                startTime = this.dateTimePicker2.Value;
                int month = this.dateTimePicker2.Value.Month + 1;
                endTime = Convert.ToDateTime(this.dateTimePicker2.Value.Year + "年" + month + "月");
            }
            else
            {
                startTime = this.dateTimePicker2.Value;
                endTime = this.dateTimePicker3.Value;
            }
            noSPList = supplierPerformanceBLL.queryNoSupplierPerformanceList(startTime, endTime);
            //添加之前先清空DataGridView
            DataTable dt = (DataTable)this.dataGridView1.DataSource ;
            dt = null;
            this.dataGridView1.DataSource = noSPList;
        }

        private void comboBox5_SelectedIndexChanged(object sender, EventArgs e)
        {
            //根据选定的评估类型，更改时间范围下拉框的选项
            if (comboBox5.SelectedItem.ToString() == "定期评估")
            {
                this.dateTimePicker3.Visible = false;
                this.label1.Visible = false;
            }
            else if (comboBox5.SelectedItem.ToString() == "累计评估")
            {
                this.dateTimePicker3.Visible = true;
                this.label1.Visible = true; 
            }
        }
    }
}
