﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;
using Lib.Bll.SupplierPerformaceBLL;

namespace MMClient.SupplierPerformance.LPS
{
    public partial class LPSWarningForm : DockContent
    {
        Decimal priceScore, qualityScore, deliveryScore, serviceSupportScore, externalServiceScore;
        List<Decimal> scoreThreshodList = new List<Decimal>();//依此存放5个主标准的分数阈值
        LPSBLL lpsBLL = new LPSBLL();
        DataTable supplierInfoTable;
        String supplierID2, supplierName2;//用于保存第2张选项卡“关键指数表现”中的

        public LPSWarningForm()
        {
            InitializeComponent();

            //在年份选项中给combobox添加2015年至现在的年份
            for (int i = DateTime.Now.Year; i >= 2015; i--)
            {
                string year = string.Format("{0}", i);
                comboBox_year.Items.Add(year);
            }
            comboBox_year.SelectedIndex = 0;

            //给时间范围comboBox添加选项
            comboBox_timePeriod.Enabled = true;
            comboBox_timePeriod.Items.Add("1月");
            comboBox_timePeriod.Items.Add("2月");
            comboBox_timePeriod.Items.Add("3月");
            comboBox_timePeriod.Items.Add("4月");
            comboBox_timePeriod.Items.Add("5月");
            comboBox_timePeriod.Items.Add("6月");
            comboBox_timePeriod.Items.Add("7月");
            comboBox_timePeriod.Items.Add("8月");
            comboBox_timePeriod.Items.Add("9月");
            comboBox_timePeriod.Items.Add("10月");
            comboBox_timePeriod.Items.Add("11月");
            comboBox_timePeriod.Items.Add("12月");
            comboBox_timePeriod.Items.Add("1月-2月");
            comboBox_timePeriod.Items.Add("1月-3月");
            comboBox_timePeriod.Items.Add("1月-4月");
            comboBox_timePeriod.Items.Add("1月-5月");
            comboBox_timePeriod.Items.Add("1月-6月");
            comboBox_timePeriod.Items.Add("1月-7月");
            comboBox_timePeriod.Items.Add("1月-8月");
            comboBox_timePeriod.Items.Add("1月-9月");
            comboBox_timePeriod.Items.Add("1月-10月");
            comboBox_timePeriod.Items.Add("1月-11月");
            comboBox_timePeriod.Items.Add("1月-12月");

            //在选项卡"关键指数表现"中，添加供应商选项
            SupplierPerformanceBLL supplierPerformanceBLL = new SupplierPerformanceBLL();
            supplierInfoTable = supplierPerformanceBLL.querySupplier();
            comboBox_supplierID.DataSource = supplierInfoTable;
            comboBox_supplierID.DisplayMember = supplierInfoTable.Columns["Supplier_ID"].ToString();
            lable_supplierName.Text = (supplierInfoTable.Rows[comboBox_supplierID.SelectedIndex]["Supplier_Name"]).ToString();

            supplierID2 = comboBox_supplierID.Text.ToString();
            supplierName2 = lable_supplierName.Text.ToString();
        }

        #region 绩效评估分数指标不达标

        //“查看”button
        private void button_Click(object sender, EventArgs e)
        {
            //点击“查看”之后，首先清空datagridview中显示的内容
            dataGridView.Rows.Clear();
            //检查是否输入分数阈值
            if (String.IsNullOrEmpty(textBox_price.Text) || String.IsNullOrEmpty(textBox_quality.Text) || String.IsNullOrEmpty(textBox_delivery.Text) || String.IsNullOrEmpty(textBox_serviceSupport.Text) || String.IsNullOrEmpty(textBox_externalService.Text))
            {
                MessageBox.Show("请输入各标准阈值，用于界定供应商是否进入低效能供应商管理流程！");
                return;
            }

            try
            {
                priceScore = Convert.ToDecimal(textBox_price.Text);
                qualityScore = Convert.ToDecimal(textBox_quality.Text);
                deliveryScore = Convert.ToDecimal(textBox_delivery.Text);
                serviceSupportScore = Convert.ToDecimal(textBox_serviceSupport.Text);
                externalServiceScore = Convert.ToDecimal(textBox_externalService.Text);

                //检查输入的分数是否在0-100之间
                if (priceScore < 0 || priceScore > 100 || qualityScore < 0 || qualityScore > 100 || deliveryScore < 0 || deliveryScore > 100 || serviceSupportScore < 0 || serviceSupportScore > 100 || externalServiceScore < 0 || externalServiceScore > 100)
                {
                    MessageBox.Show("请检查输入的分数阈值是否在0-100区间内！");
                    return;
                }
            }
            catch
            {
                MessageBox.Show("请检查输入的分数阈值是否为数字！");
                return;
            }

            //检查是否选择年份和时间范围
            if (String.IsNullOrEmpty(comboBox_year.Text) || String.IsNullOrEmpty(comboBox_timePeriod.Text))
            {
                MessageBox.Show("请选择年份及时间范围！");
                return;
            }

            //将分数阈值依次添加到scoreThresholdList中
            scoreThreshodList.Clear();
            scoreThreshodList.Add(priceScore);
            scoreThreshodList.Add(qualityScore);
            scoreThreshodList.Add(deliveryScore);
            scoreThreshodList.Add(serviceSupportScore);
            scoreThreshodList.Add(externalServiceScore);

            String timePeriod;
            StringBuilder timePeriodString = new StringBuilder();
            timePeriodString.Append(comboBox_year.Text).Append("年").Append(comboBox_timePeriod.Text);
            timePeriod = timePeriodString.ToString();
            String year = comboBox_year.Text;

            DataTable lpsList = lpsBLL.queryLowPerformanceSupplier(scoreThreshodList, timePeriod, year);
            //添加之前先清空DataGridView
            dataGridView.Rows.Clear();

            for (int i = 0; i < lpsList.Rows.Count; i++)
            {
                //添加一行(!!!)
                this.dataGridView.Rows.Add();
                DataGridViewRow row = this.dataGridView.Rows[i];

                //供应商ID
                row.Cells[0].Value = lpsList.Rows[i][0];
                //供应商名称
                row.Cells[1].Value = lpsList.Rows[i][1];
                //发送警告BUTTON
                row.Cells[2].Value = "发送警告";
            }
        }

        /// <summary>
        /// 当鼠标点击DataGridView的内容，如果是Button（发送警告），则弹出窗口
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            String supplierID, supplierName;
            int rowNum = dataGridView.CurrentCell.RowIndex;
            supplierID = dataGridView.Rows[rowNum].Cells[0].Value.ToString();
            supplierName = dataGridView.Rows[rowNum].Cells[1].Value.ToString();

            //如果是“发送警告”button
            try
            {
                WarningSendForm wsForm = new WarningSendForm(supplierID, supplierName);
                wsForm.Owner = this;
                wsForm.ShowDialog();
            }
            catch
            {
                return;
            }
        }
        #endregion

        #region 关键指数表现不达标

        /// <summary>
        /// 选项卡“关键指数表现”中选择的供应商改变
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void comboBox_supplierID_SelectedIndexChanged(object sender, EventArgs e)
        {
            supplierID2 = comboBox_supplierID.Text.ToString();
            lable_supplierName.Text = supplierInfoTable.Rows[comboBox_supplierID.SelectedIndex]["Supplier_Name"].ToString();
            supplierName2 = lable_supplierName.Text;
        }

        /// <summary>
        /// button 确定进入LPS流程
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            //获取记录时间和记录人信息
            DateTime time;
            String name;
            time = Convert.ToDateTime(dateTimePicker1.Text);

            //检查是否选择供应商
            if (String.IsNullOrEmpty(comboBox_supplierID.Text))
            {
                MessageBox.Show("请选择供应商！");
                return;
            }

            //检查是否输入警告发送人信息
            if (String.IsNullOrEmpty(textBox_name.Text))
            {
                MessageBox.Show("请输入警告发送人姓名！");
                return;
            }
            name = textBox_name.Text;

            //保存LPS记录和关键指标陈述记录
            LPSBLL lpsBLL = new LPSBLL();
            //保存LPS记录之前先检查是否已经存在记录
            bool rst = lpsBLL.queryRecordOrNot(supplierID2);
            if (rst == true)
            {
                MessageBox.Show("该供应商已进入低效能供应商管理流程，请在低效能供应商管理三级流程中查看或修改状态！");
                return;
            }
            //检查是否选择了一个或多个关键指标
            if (checkBox1.Checked == false && checkBox2.Checked == false && checkBox3.Checked == false && checkBox4.Checked == false && checkBox5.Checked == false && checkBox6.Checked == false && checkBox7.Checked == false && checkBox8.Checked == false && checkBox9.Checked == false && checkBox10.Checked == false && checkBox11.Checked == false && checkBox12.Checked == false && checkBox13.Checked == false && checkBox14.Checked == false)
            {
                MessageBox.Show("请选择未达标的关键指标！");
                return;
            }

            //将用户选择的checkbox的选项添加到一个List中，作为保存到数据库时的参数
            List<String> kpiList = new List<string>();
            foreach (Control c in groupBox_kpi.Controls)
            {
                if (c is CheckBox && ((CheckBox)c).Checked == true)
                {
                    kpiList.Add(((CheckBox)c).Text.ToString());
                }
            }

            //保存陈述内容（先保存陈述内容，再更新低效能供应商数据表，以防陈述内容保存失败，但是却更新了低效能供应商数据表的情况）
            String statement = textBox_statement.Text.ToString();
            supplierID2 = comboBox_supplierID.Text.ToString();
            supplierName2 = lable_supplierName.Text.ToString();
            int linesStatement = lpsBLL.saveKPIStatement(supplierID2, kpiList, statement, time, name);
            if (linesStatement != 1)
            {
                MessageBox.Show("关键指标及陈述保存失败！");
                return;
            }

            //因为发送警告阶段在LPS流程中的LPS_Degree记录为0，因此直接将'0'作为参数传入
            int lines = lpsBLL.saveLPSState(supplierID2, "0", time, name);

            //判断是否保存成功
            if (lines == 1)
            {
                MessageBox.Show("ID为" + supplierID2 + "的供应商" + supplierName2 + "已进入低效能供应商管理流程！当前状态为0，即警告状态!");
                return;
            }
            else
            {
                MessageBox.Show("保存出错，请重试！");
            }
        }

        #endregion

    }
}