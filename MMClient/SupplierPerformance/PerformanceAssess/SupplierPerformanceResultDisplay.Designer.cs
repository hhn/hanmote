﻿namespace MMClient.SupplierPerformance
{
    partial class SupplierPerformanceResultDisplay
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.comboBox_supplierID = new System.Windows.Forms.ComboBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label_purchasingORGName = new System.Windows.Forms.Label();
            this.comboBox_purchasingORGID = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label_supplierName = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.comboBox_materialID = new System.Windows.Forms.ComboBox();
            this.label_materialName = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.dataGridView = new System.Windows.Forms.DataGridView();
            this.ColumnNum = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnPeriod = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnQuality = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnDeliver = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnServiceSupport = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnExternalService = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnTotalScore = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnCheck = new System.Windows.Forms.DataGridViewButtonColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pageTool = new pager.pagetool.pageNext();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(84, -83);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 17);
            this.label1.TabIndex = 8;
            this.label1.Text = "采购组织";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(359, -21);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(68, 17);
            this.label4.TabIndex = 13;
            this.label4.Text = "供应商名称";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(359, -83);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 17);
            this.label3.TabIndex = 12;
            this.label3.Text = "采购组织名称";
            // 
            // comboBox_supplierID
            // 
            this.comboBox_supplierID.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.comboBox_supplierID.FormattingEnabled = true;
            this.comboBox_supplierID.Location = new System.Drawing.Point(84, 12);
            this.comboBox_supplierID.Margin = new System.Windows.Forms.Padding(3, 7, 3, 7);
            this.comboBox_supplierID.Name = "comboBox_supplierID";
            this.comboBox_supplierID.Size = new System.Drawing.Size(139, 25);
            this.comboBox_supplierID.TabIndex = 11;
            this.comboBox_supplierID.SelectedIndexChanged += new System.EventHandler(this.comboBox_supplierID_SelectedIndexChanged_1);
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(165, -87);
            this.comboBox1.Margin = new System.Windows.Forms.Padding(3, 7, 3, 7);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(133, 25);
            this.comboBox1.TabIndex = 10;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.Location = new System.Drawing.Point(22, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 17);
            this.label2.TabIndex = 9;
            this.label2.Text = "供应商：";
            // 
            // label_purchasingORGName
            // 
            this.label_purchasingORGName.AutoSize = true;
            this.label_purchasingORGName.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label_purchasingORGName.Location = new System.Drawing.Point(606, 44);
            this.label_purchasingORGName.Name = "label_purchasingORGName";
            this.label_purchasingORGName.Size = new System.Drawing.Size(80, 17);
            this.label_purchasingORGName.TabIndex = 18;
            this.label_purchasingORGName.Text = "采购组织名称";
            // 
            // comboBox_purchasingORGID
            // 
            this.comboBox_purchasingORGID.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.comboBox_purchasingORGID.FormattingEnabled = true;
            this.comboBox_purchasingORGID.Location = new System.Drawing.Point(609, 12);
            this.comboBox_purchasingORGID.Margin = new System.Windows.Forms.Padding(3, 7, 3, 7);
            this.comboBox_purchasingORGID.Name = "comboBox_purchasingORGID";
            this.comboBox_purchasingORGID.Size = new System.Drawing.Size(125, 25);
            this.comboBox_purchasingORGID.TabIndex = 17;
            this.comboBox_purchasingORGID.SelectedIndexChanged += new System.EventHandler(this.comboBox_purchasingORGID_SelectedIndexChanged);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label16.Location = new System.Drawing.Point(526, 15);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(68, 17);
            this.label16.TabIndex = 16;
            this.label16.Text = "采购组织：";
            // 
            // label_supplierName
            // 
            this.label_supplierName.AutoSize = true;
            this.label_supplierName.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label_supplierName.Location = new System.Drawing.Point(81, 44);
            this.label_supplierName.Name = "label_supplierName";
            this.label_supplierName.Size = new System.Drawing.Size(68, 17);
            this.label_supplierName.TabIndex = 19;
            this.label_supplierName.Text = "供应商名称";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label5.Location = new System.Drawing.Point(285, 15);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(44, 17);
            this.label5.TabIndex = 38;
            this.label5.Text = "物料：";
            // 
            // comboBox_materialID
            // 
            this.comboBox_materialID.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.comboBox_materialID.FormattingEnabled = true;
            this.comboBox_materialID.Location = new System.Drawing.Point(346, 12);
            this.comboBox_materialID.Margin = new System.Windows.Forms.Padding(3, 7, 3, 7);
            this.comboBox_materialID.Name = "comboBox_materialID";
            this.comboBox_materialID.Size = new System.Drawing.Size(148, 25);
            this.comboBox_materialID.TabIndex = 39;
            this.comboBox_materialID.SelectedIndexChanged += new System.EventHandler(this.comboBox_materialID_SelectedIndexChanged);
            // 
            // label_materialName
            // 
            this.label_materialName.AutoSize = true;
            this.label_materialName.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label_materialName.Location = new System.Drawing.Point(357, 44);
            this.label_materialName.Name = "label_materialName";
            this.label_materialName.Size = new System.Drawing.Size(56, 17);
            this.label_materialName.TabIndex = 40;
            this.label_materialName.Text = "物料名称";
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.button1.Location = new System.Drawing.Point(834, 12);
            this.button1.Margin = new System.Windows.Forms.Padding(3, 7, 3, 7);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(106, 38);
            this.button1.TabIndex = 43;
            this.button1.Text = "查  询";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // dataGridView
            // 
            this.dataGridView.AllowUserToOrderColumns = true;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.dataGridView.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColumnNum,
            this.ColumnType,
            this.ColumnPeriod,
            this.ColumnName,
            this.ColumnDate,
            this.ColumnPrice,
            this.ColumnQuality,
            this.ColumnDeliver,
            this.ColumnServiceSupport,
            this.ColumnExternalService,
            this.ColumnTotalScore,
            this.ColumnCheck});
            this.dataGridView.Location = new System.Drawing.Point(25, 86);
            this.dataGridView.Margin = new System.Windows.Forms.Padding(3, 7, 3, 7);
            this.dataGridView.Name = "dataGridView";
            this.dataGridView.RowTemplate.Height = 27;
            this.dataGridView.Size = new System.Drawing.Size(1115, 572);
            this.dataGridView.TabIndex = 45;
            this.dataGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView_CellContentClick_1);
            // 
            // ColumnNum
            // 
            this.ColumnNum.HeaderText = "NO.";
            this.ColumnNum.Name = "ColumnNum";
            // 
            // ColumnType
            // 
            this.ColumnType.HeaderText = "评估类型";
            this.ColumnType.Name = "ColumnType";
            // 
            // ColumnPeriod
            // 
            this.ColumnPeriod.HeaderText = "时间范围";
            this.ColumnPeriod.Name = "ColumnPeriod";
            // 
            // ColumnName
            // 
            this.ColumnName.HeaderText = "评估人";
            this.ColumnName.Name = "ColumnName";
            // 
            // ColumnDate
            // 
            this.ColumnDate.HeaderText = "评估时间";
            this.ColumnDate.Name = "ColumnDate";
            // 
            // ColumnPrice
            // 
            this.ColumnPrice.HeaderText = "价格";
            this.ColumnPrice.Name = "ColumnPrice";
            // 
            // ColumnQuality
            // 
            this.ColumnQuality.HeaderText = "质量";
            this.ColumnQuality.Name = "ColumnQuality";
            // 
            // ColumnDeliver
            // 
            this.ColumnDeliver.HeaderText = "收货";
            this.ColumnDeliver.Name = "ColumnDeliver";
            // 
            // ColumnServiceSupport
            // 
            this.ColumnServiceSupport.HeaderText = "一般服务/支持";
            this.ColumnServiceSupport.Name = "ColumnServiceSupport";
            // 
            // ColumnExternalService
            // 
            this.ColumnExternalService.HeaderText = "外部服务";
            this.ColumnExternalService.Name = "ColumnExternalService";
            // 
            // ColumnTotalScore
            // 
            this.ColumnTotalScore.HeaderText = "总分";
            this.ColumnTotalScore.Name = "ColumnTotalScore";
            // 
            // ColumnCheck
            // 
            this.ColumnCheck.HeaderText = "详细情况";
            this.ColumnCheck.Name = "ColumnCheck";
            this.ColumnCheck.Text = "查看详情";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.pageTool);
            this.panel1.Controls.Add(this.comboBox_supplierID);
            this.panel1.Controls.Add(this.dataGridView);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.label16);
            this.panel1.Controls.Add(this.label_materialName);
            this.panel1.Controls.Add(this.comboBox_purchasingORGID);
            this.panel1.Controls.Add(this.comboBox_materialID);
            this.panel1.Controls.Add(this.label_purchasingORGName);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label_supplierName);
            this.panel1.Location = new System.Drawing.Point(3, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1209, 734);
            this.panel1.TabIndex = 46;
            // 
            // pageTool
            // 
            this.pageTool.Location = new System.Drawing.Point(139, 669);
            this.pageTool.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pageTool.Name = "pageTool";
            this.pageTool.PageIndex = 1;
            this.pageTool.PageSize = 100;
            this.pageTool.RecordCount = 0;
            this.pageTool.Size = new System.Drawing.Size(780, 46);
            this.pageTool.TabIndex = 46;
            this.pageTool.OnPageChanged += new System.EventHandler(this.pageTool_OnPageChanged);
            this.pageTool.Load += new System.EventHandler(this.pageTool_Load);
            // 
            // SupplierPerformanceResultDisplay
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1224, 750);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.comboBox1);
            this.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Margin = new System.Windows.Forms.Padding(3, 7, 3, 7);
            this.Name = "SupplierPerformanceResultDisplay";
            this.Text = "供应商评估结果显示";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox comboBox_supplierID;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label_purchasingORGName;
        private System.Windows.Forms.ComboBox comboBox_purchasingORGID;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label_supplierName;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox comboBox_materialID;
        private System.Windows.Forms.Label label_materialName;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridView dataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnNum;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnType;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnPeriod;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnPrice;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnQuality;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnDeliver;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnServiceSupport;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnExternalService;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnTotalScore;
        private System.Windows.Forms.DataGridViewButtonColumn ColumnCheck;
        private System.Windows.Forms.Panel panel1;
        private pager.pagetool.pageNext pageTool;
    }
}