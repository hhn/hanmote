﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Bll.SupplierPerformaceBLL;

namespace MMClient.SupplierPerformance
{
    public partial class DetailedResultForm : Form
    {
        String materialID;
        String supplierID;
        String purchasingORGID;
        public DetailedResultForm(String materialID,String supplierID,String purchasingORGID,int idNum)
        {
            InitializeComponent();

            //将物料ID，供应商ID，采购组织ID作为参数从SupplierPerformanceResultDisplay中传递过来，用于显示
            this.materialID = materialID;
            this.supplierID = supplierID;
            this.purchasingORGID = purchasingORGID;

            //供应商ID
            textBox1.Text = supplierID.ToString();
            //物料ID
            textBox2.Text = materialID.ToString();
            //采购组织ID
            textBox3.Text = purchasingORGID.ToString();

            SupplierPerformanceBLL supplierPerformanceAutoBLL = new SupplierPerformanceBLL();
            DataTable resultTable = supplierPerformanceAutoBLL.queryDetailResult(materialID, supplierID, purchasingORGID,idNum);
            foreach (DataRow row in resultTable.Rows)
            {
                //总分
                textBoxTotalScore.Text = row[4].ToString();
                //价格
                TextboxPrice.Text = row[5].ToString();
                //价格水平
                TextboxAuto_priceLevel.Text = row[6].ToString();
                //价格历史
                TextboxAuto_priceHistory.Text = row[7].ToString();
                //质量
                TextboxAuto_quality.Text = row[8].ToString();
                //收获
                TextboxAuto_receiveGoods.Text = row[9].ToString();
                //质量审计
                TextboxAuto_qualityAudit.Text = row[10].ToString();
                //抱怨/拒绝水平
                TextboxAuto_complaintReject.Text = row[11].ToString();
                //交货
                TextboxAuto_deliver.Text = row[12].ToString();
                //按时交货的表现
                TextboxAuto_deliverOnTime.Text = row[13].ToString();
                //确认日期
                TextboxAuto_confirmDate.Text = row[14].ToString();
                //数量可靠性
                TextboxAuto_quantityReliability.Text = row[15].ToString();
                //装运须知的遵守
                TextboxAuto_goodsShipment.Text = row[16].ToString();
                //一般服务/支持
                TextboxAuto_serviceSupport.Text = row[17].ToString();
                //外部服务
                TextboxAuto_externalService.Text = row[18].ToString();

                //评估类型
                if (row[19].ToString() == "True")
                {
                    textBox4.Text = "累计评估";
                }
                else
                {
                    textBox4.Text = "定期评估";
                }

                //时间范围
                textBox5.Text = row[20].ToString();
                //评估人
                textBox6.Text = row[21].ToString();
                //评估日期
                textBox7.Text = row[22].ToString();
            }
        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void textBox5_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox7_TextChanged(object sender, EventArgs e)
        {

        }

        private void label10_Click(object sender, EventArgs e)
        {

        }
    }
}
