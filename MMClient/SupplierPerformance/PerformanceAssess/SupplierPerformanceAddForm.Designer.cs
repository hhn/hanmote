﻿namespace MMClient.SupplierPerformance
{
    partial class SupplierPerformanceAddForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.TextBoxTotalScore = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.TextboxExternalService = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.TextboxServiceSupport = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.TextboxGoodsShipment = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.TextboxDeliver = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.TextboxDeliverOnTime = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.TextboxConfirmDate = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.TextboxQuantityReliability = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.TextboxQuality = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.TextboxReceiveGoods = new System.Windows.Forms.TextBox();
            this.TextboxQualityAudit = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.TextboxComplaintReject = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label28 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.TextboxPrice = new System.Windows.Forms.TextBox();
            this.TextboxPriceLevel = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.TextboxPriceHistory = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.button4 = new System.Windows.Forms.Button();
            this.CB_modelType = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.comboBox_purchasingORGID = new System.Windows.Forms.ComboBox();
            this.comboBox_materialID = new System.Windows.Forms.ComboBox();
            this.comboBox_weightSS = new System.Windows.Forms.ComboBox();
            this.label30 = new System.Windows.Forms.Label();
            this.comboBox_weightMS = new System.Windows.Forms.ComboBox();
            this.label31 = new System.Windows.Forms.Label();
            this.comboBox5 = new System.Windows.Forms.ComboBox();
            this.label26 = new System.Windows.Forms.Label();
            this.lable_materialName = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.label20 = new System.Windows.Forms.Label();
            this.textBox15 = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.lable_supplierName = new System.Windows.Forms.Label();
            this.lable_purchasingORGName = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBox_supplierID = new System.Windows.Forms.ComboBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.groupBoxResult = new System.Windows.Forms.GroupBox();
            this.button3 = new System.Windows.Forms.Button();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBoxResult.SuspendLayout();
            this.SuspendLayout();
            // 
            // TextBoxTotalScore
            // 
            this.TextBoxTotalScore.Enabled = false;
            this.TextBoxTotalScore.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.TextBoxTotalScore.Location = new System.Drawing.Point(615, 67);
            this.TextBoxTotalScore.Margin = new System.Windows.Forms.Padding(2);
            this.TextBoxTotalScore.Name = "TextBoxTotalScore";
            this.TextBoxTotalScore.ReadOnly = true;
            this.TextBoxTotalScore.Size = new System.Drawing.Size(148, 23);
            this.TextBoxTotalScore.TabIndex = 74;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label27.Location = new System.Drawing.Point(552, 69);
            this.label27.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(32, 17);
            this.label27.TabIndex = 73;
            this.label27.Text = "总分";
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.button1.Location = new System.Drawing.Point(47, 31);
            this.button1.Margin = new System.Windows.Forms.Padding(2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(131, 26);
            this.button1.TabIndex = 72;
            this.button1.Text = "计算主标准及总分分数";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.TextboxExternalService);
            this.groupBox4.Controls.Add(this.label18);
            this.groupBox4.Controls.Add(this.label17);
            this.groupBox4.Controls.Add(this.TextboxServiceSupport);
            this.groupBox4.Font = new System.Drawing.Font("微软雅黑", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.groupBox4.Location = new System.Drawing.Point(87, 502);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox4.Size = new System.Drawing.Size(725, 74);
            this.groupBox4.TabIndex = 69;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "服务";
            // 
            // TextboxExternalService
            // 
            this.TextboxExternalService.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.TextboxExternalService.Location = new System.Drawing.Point(425, 23);
            this.TextboxExternalService.Margin = new System.Windows.Forms.Padding(2);
            this.TextboxExternalService.Name = "TextboxExternalService";
            this.TextboxExternalService.Size = new System.Drawing.Size(122, 23);
            this.TextboxExternalService.TabIndex = 62;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label18.Location = new System.Drawing.Point(327, 29);
            this.label18.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(92, 17);
            this.label18.TabIndex = 61;
            this.label18.Text = "外部服务总分：";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label17.Location = new System.Drawing.Point(64, 29);
            this.label17.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(121, 17);
            this.label17.TabIndex = 59;
            this.label17.Text = "一般服务/支持总分：";
            // 
            // TextboxServiceSupport
            // 
            this.TextboxServiceSupport.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.TextboxServiceSupport.Location = new System.Drawing.Point(186, 26);
            this.TextboxServiceSupport.Margin = new System.Windows.Forms.Padding(2);
            this.TextboxServiceSupport.Name = "TextboxServiceSupport";
            this.TextboxServiceSupport.Size = new System.Drawing.Size(97, 23);
            this.TextboxServiceSupport.TabIndex = 60;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.TextboxGoodsShipment);
            this.groupBox3.Controls.Add(this.label16);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Controls.Add(this.TextboxDeliver);
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this.TextboxDeliverOnTime);
            this.groupBox3.Controls.Add(this.label14);
            this.groupBox3.Controls.Add(this.TextboxConfirmDate);
            this.groupBox3.Controls.Add(this.label15);
            this.groupBox3.Controls.Add(this.TextboxQuantityReliability);
            this.groupBox3.Font = new System.Drawing.Font("微软雅黑", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.groupBox3.Location = new System.Drawing.Point(87, 300);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox3.Size = new System.Drawing.Size(725, 174);
            this.groupBox3.TabIndex = 68;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "交货";
            // 
            // TextboxGoodsShipment
            // 
            this.TextboxGoodsShipment.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.TextboxGoodsShipment.Location = new System.Drawing.Point(464, 127);
            this.TextboxGoodsShipment.Margin = new System.Windows.Forms.Padding(2);
            this.TextboxGoodsShipment.Name = "TextboxGoodsShipment";
            this.TextboxGoodsShipment.Size = new System.Drawing.Size(115, 23);
            this.TextboxGoodsShipment.TabIndex = 58;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label16.Location = new System.Drawing.Point(326, 130);
            this.label16.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(145, 17);
            this.label16.TabIndex = 57;
            this.label16.Text = "交货-对装运须知的遵守：";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label12.Location = new System.Drawing.Point(64, 63);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(68, 17);
            this.label12.TabIndex = 49;
            this.label12.Text = "交货总分：";
            // 
            // TextboxDeliver
            // 
            this.TextboxDeliver.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.TextboxDeliver.Location = new System.Drawing.Point(156, 61);
            this.TextboxDeliver.Margin = new System.Windows.Forms.Padding(2);
            this.TextboxDeliver.Name = "TextboxDeliver";
            this.TextboxDeliver.ReadOnly = true;
            this.TextboxDeliver.Size = new System.Drawing.Size(127, 23);
            this.TextboxDeliver.TabIndex = 50;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label13.Location = new System.Drawing.Point(326, 32);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(133, 17);
            this.label13.TabIndex = 51;
            this.label13.Text = "交货-按时交货的表现：";
            // 
            // TextboxDeliverOnTime
            // 
            this.TextboxDeliverOnTime.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.TextboxDeliverOnTime.Location = new System.Drawing.Point(464, 30);
            this.TextboxDeliverOnTime.Margin = new System.Windows.Forms.Padding(2);
            this.TextboxDeliverOnTime.Name = "TextboxDeliverOnTime";
            this.TextboxDeliverOnTime.Size = new System.Drawing.Size(115, 23);
            this.TextboxDeliverOnTime.TabIndex = 52;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label14.Location = new System.Drawing.Point(327, 63);
            this.label14.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(121, 17);
            this.label14.TabIndex = 53;
            this.label14.Text = "交货-遵守确认日期：";
            // 
            // TextboxConfirmDate
            // 
            this.TextboxConfirmDate.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.TextboxConfirmDate.Location = new System.Drawing.Point(464, 61);
            this.TextboxConfirmDate.Margin = new System.Windows.Forms.Padding(2);
            this.TextboxConfirmDate.Name = "TextboxConfirmDate";
            this.TextboxConfirmDate.Size = new System.Drawing.Size(115, 23);
            this.TextboxConfirmDate.TabIndex = 54;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label15.Location = new System.Drawing.Point(327, 96);
            this.label15.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(109, 17);
            this.label15.TabIndex = 55;
            this.label15.Text = "交货-数量可靠性：";
            // 
            // TextboxQuantityReliability
            // 
            this.TextboxQuantityReliability.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.TextboxQuantityReliability.Location = new System.Drawing.Point(464, 94);
            this.TextboxQuantityReliability.Margin = new System.Windows.Forms.Padding(2);
            this.TextboxQuantityReliability.Name = "TextboxQuantityReliability";
            this.TextboxQuantityReliability.Size = new System.Drawing.Size(115, 23);
            this.TextboxQuantityReliability.TabIndex = 56;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.TextboxQuality);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.TextboxReceiveGoods);
            this.groupBox2.Controls.Add(this.TextboxQualityAudit);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.TextboxComplaintReject);
            this.groupBox2.Font = new System.Drawing.Font("微软雅黑", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.groupBox2.Location = new System.Drawing.Point(86, 150);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox2.Size = new System.Drawing.Size(726, 126);
            this.groupBox2.TabIndex = 67;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "质量";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label10.Location = new System.Drawing.Point(326, 58);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(97, 17);
            this.label10.TabIndex = 45;
            this.label10.Text = "质量-质量审计：";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label8.Location = new System.Drawing.Point(62, 58);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(68, 17);
            this.label8.TabIndex = 41;
            this.label8.Text = "质量总分：";
            // 
            // TextboxQuality
            // 
            this.TextboxQuality.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.TextboxQuality.Location = new System.Drawing.Point(157, 56);
            this.TextboxQuality.Margin = new System.Windows.Forms.Padding(2);
            this.TextboxQuality.Name = "TextboxQuality";
            this.TextboxQuality.ReadOnly = true;
            this.TextboxQuality.Size = new System.Drawing.Size(127, 23);
            this.TextboxQuality.TabIndex = 42;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label9.Location = new System.Drawing.Point(326, 28);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(73, 17);
            this.label9.TabIndex = 43;
            this.label9.Text = "质量-收货：";
            // 
            // TextboxReceiveGoods
            // 
            this.TextboxReceiveGoods.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.TextboxReceiveGoods.Location = new System.Drawing.Point(463, 26);
            this.TextboxReceiveGoods.Margin = new System.Windows.Forms.Padding(2);
            this.TextboxReceiveGoods.Name = "TextboxReceiveGoods";
            this.TextboxReceiveGoods.Size = new System.Drawing.Size(115, 23);
            this.TextboxReceiveGoods.TabIndex = 44;
            // 
            // TextboxQualityAudit
            // 
            this.TextboxQualityAudit.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.TextboxQualityAudit.Location = new System.Drawing.Point(463, 56);
            this.TextboxQualityAudit.Margin = new System.Windows.Forms.Padding(2);
            this.TextboxQualityAudit.Name = "TextboxQualityAudit";
            this.TextboxQualityAudit.Size = new System.Drawing.Size(115, 23);
            this.TextboxQualityAudit.TabIndex = 46;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label11.Location = new System.Drawing.Point(326, 89);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(126, 17);
            this.label11.TabIndex = 47;
            this.label11.Text = "质量-抱怨/拒绝水平：";
            // 
            // TextboxComplaintReject
            // 
            this.TextboxComplaintReject.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.TextboxComplaintReject.Location = new System.Drawing.Point(464, 86);
            this.TextboxComplaintReject.Margin = new System.Windows.Forms.Padding(2);
            this.TextboxComplaintReject.Name = "TextboxComplaintReject";
            this.TextboxComplaintReject.Size = new System.Drawing.Size(115, 23);
            this.TextboxComplaintReject.TabIndex = 48;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label28);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.TextboxPrice);
            this.groupBox1.Controls.Add(this.TextboxPriceLevel);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.TextboxPriceHistory);
            this.groupBox1.Font = new System.Drawing.Font("微软雅黑", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.groupBox1.Location = new System.Drawing.Point(86, 37);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox1.Size = new System.Drawing.Size(726, 90);
            this.groupBox1.TabIndex = 66;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "价格";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label28.Location = new System.Drawing.Point(62, 28);
            this.label28.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(68, 17);
            this.label28.TabIndex = 41;
            this.label28.Text = "价格总分：";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label6.Location = new System.Drawing.Point(328, 28);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(97, 17);
            this.label6.TabIndex = 37;
            this.label6.Text = "价格-价格水平：";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(-97, 22);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(37, 20);
            this.label5.TabIndex = 35;
            this.label5.Text = "价格";
            // 
            // TextboxPrice
            // 
            this.TextboxPrice.Enabled = false;
            this.TextboxPrice.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.TextboxPrice.Location = new System.Drawing.Point(157, 26);
            this.TextboxPrice.Margin = new System.Windows.Forms.Padding(2);
            this.TextboxPrice.Name = "TextboxPrice";
            this.TextboxPrice.ReadOnly = true;
            this.TextboxPrice.Size = new System.Drawing.Size(127, 23);
            this.TextboxPrice.TabIndex = 36;
            // 
            // TextboxPriceLevel
            // 
            this.TextboxPriceLevel.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.TextboxPriceLevel.Location = new System.Drawing.Point(464, 26);
            this.TextboxPriceLevel.Margin = new System.Windows.Forms.Padding(2);
            this.TextboxPriceLevel.Name = "TextboxPriceLevel";
            this.TextboxPriceLevel.Size = new System.Drawing.Size(115, 23);
            this.TextboxPriceLevel.TabIndex = 38;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label7.Location = new System.Drawing.Point(327, 54);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(97, 17);
            this.label7.TabIndex = 39;
            this.label7.Text = "价格-价格历史：";
            // 
            // TextboxPriceHistory
            // 
            this.TextboxPriceHistory.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.TextboxPriceHistory.Location = new System.Drawing.Point(464, 54);
            this.TextboxPriceHistory.Margin = new System.Windows.Forms.Padding(2);
            this.TextboxPriceHistory.Name = "TextboxPriceHistory";
            this.TextboxPriceHistory.Size = new System.Drawing.Size(115, 23);
            this.TextboxPriceHistory.TabIndex = 40;
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.button2.Location = new System.Drawing.Point(205, 31);
            this.button2.Margin = new System.Windows.Forms.Padding(2);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(56, 26);
            this.button2.TabIndex = 63;
            this.button2.Text = "保存";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.button4);
            this.groupBox6.Controls.Add(this.CB_modelType);
            this.groupBox6.Controls.Add(this.label3);
            this.groupBox6.Controls.Add(this.TextBoxTotalScore);
            this.groupBox6.Controls.Add(this.label27);
            this.groupBox6.Controls.Add(this.comboBox_purchasingORGID);
            this.groupBox6.Controls.Add(this.comboBox_materialID);
            this.groupBox6.Controls.Add(this.comboBox_weightSS);
            this.groupBox6.Controls.Add(this.label30);
            this.groupBox6.Controls.Add(this.comboBox_weightMS);
            this.groupBox6.Controls.Add(this.label31);
            this.groupBox6.Controls.Add(this.comboBox5);
            this.groupBox6.Controls.Add(this.label26);
            this.groupBox6.Controls.Add(this.lable_materialName);
            this.groupBox6.Controls.Add(this.label22);
            this.groupBox6.Controls.Add(this.dateTimePicker1);
            this.groupBox6.Controls.Add(this.label20);
            this.groupBox6.Controls.Add(this.textBox15);
            this.groupBox6.Controls.Add(this.label19);
            this.groupBox6.Controls.Add(this.lable_supplierName);
            this.groupBox6.Controls.Add(this.lable_purchasingORGName);
            this.groupBox6.Controls.Add(this.label2);
            this.groupBox6.Controls.Add(this.label1);
            this.groupBox6.Controls.Add(this.comboBox_supplierID);
            this.groupBox6.Font = new System.Drawing.Font("微软雅黑", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.groupBox6.Location = new System.Drawing.Point(47, 83);
            this.groupBox6.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox6.Size = new System.Drawing.Size(1002, 197);
            this.groupBox6.TabIndex = 38;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "基本信息";
            // 
            // button4
            // 
            this.button4.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.button4.Location = new System.Drawing.Point(643, 105);
            this.button4.Margin = new System.Windows.Forms.Padding(2);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(66, 26);
            this.button4.TabIndex = 75;
            this.button4.Text = "计算总分";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click_1);
            // 
            // CB_modelType
            // 
            this.CB_modelType.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.CB_modelType.FormattingEnabled = true;
            this.CB_modelType.Items.AddRange(new object[] {
            "01",
            "02"});
            this.CB_modelType.Location = new System.Drawing.Point(368, 63);
            this.CB_modelType.Margin = new System.Windows.Forms.Padding(2);
            this.CB_modelType.Name = "CB_modelType";
            this.CB_modelType.Size = new System.Drawing.Size(148, 25);
            this.CB_modelType.TabIndex = 76;
            this.toolTip1.SetToolTip(this.CB_modelType, "01 默认模板，02自定义模板");
            this.CB_modelType.SelectedIndexChanged += new System.EventHandler(this.CB_modelType_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label3.Location = new System.Drawing.Point(278, 66);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(92, 17);
            this.label3.TabIndex = 75;
            this.label3.Text = "评估模板类型：";
            // 
            // comboBox_purchasingORGID
            // 
            this.comboBox_purchasingORGID.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.comboBox_purchasingORGID.FormattingEnabled = true;
            this.comboBox_purchasingORGID.Location = new System.Drawing.Point(615, 18);
            this.comboBox_purchasingORGID.Margin = new System.Windows.Forms.Padding(2);
            this.comboBox_purchasingORGID.Name = "comboBox_purchasingORGID";
            this.comboBox_purchasingORGID.Size = new System.Drawing.Size(148, 25);
            this.comboBox_purchasingORGID.TabIndex = 33;
            this.comboBox_purchasingORGID.SelectedIndexChanged += new System.EventHandler(this.comboBox_purchasingORGID_SelectedIndexChanged_1);
            // 
            // comboBox_materialID
            // 
            this.comboBox_materialID.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.comboBox_materialID.FormattingEnabled = true;
            this.comboBox_materialID.Location = new System.Drawing.Point(368, 17);
            this.comboBox_materialID.Margin = new System.Windows.Forms.Padding(2);
            this.comboBox_materialID.Name = "comboBox_materialID";
            this.comboBox_materialID.Size = new System.Drawing.Size(148, 25);
            this.comboBox_materialID.TabIndex = 46;
            this.comboBox_materialID.SelectedIndexChanged += new System.EventHandler(this.comboBox_materialID_SelectedIndexChanged_1);
            // 
            // comboBox_weightSS
            // 
            this.comboBox_weightSS.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.comboBox_weightSS.FormattingEnabled = true;
            this.comboBox_weightSS.Items.AddRange(new object[] {
            "01",
            "02"});
            this.comboBox_weightSS.Location = new System.Drawing.Point(368, 147);
            this.comboBox_weightSS.Margin = new System.Windows.Forms.Padding(2);
            this.comboBox_weightSS.Name = "comboBox_weightSS";
            this.comboBox_weightSS.Size = new System.Drawing.Size(148, 25);
            this.comboBox_weightSS.TabIndex = 53;
            this.toolTip1.SetToolTip(this.comboBox_weightSS, "01 各次标准权重相等\r\n02 各次标准权重由用户自定义");
            this.comboBox_weightSS.Visible = false;
            this.comboBox_weightSS.SelectedIndexChanged += new System.EventHandler(this.comboBox_weightSS_SelectedIndexChanged_1);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label30.Location = new System.Drawing.Point(278, 153);
            this.label30.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(92, 17);
            this.label30.TabIndex = 52;
            this.label30.Text = "次标准权重码：";
            this.label30.Visible = false;
            // 
            // comboBox_weightMS
            // 
            this.comboBox_weightMS.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.comboBox_weightMS.FormattingEnabled = true;
            this.comboBox_weightMS.Items.AddRange(new object[] {
            "01",
            "02",
            "03"});
            this.comboBox_weightMS.Location = new System.Drawing.Point(100, 150);
            this.comboBox_weightMS.Margin = new System.Windows.Forms.Padding(2);
            this.comboBox_weightMS.Name = "comboBox_weightMS";
            this.comboBox_weightMS.Size = new System.Drawing.Size(148, 25);
            this.comboBox_weightMS.TabIndex = 51;
            this.toolTip1.SetToolTip(this.comboBox_weightMS, "01 各主标准权重相等\r\n02 各主标准权重为固定值\r\n （价格5，质量3，收获2，一般服务/支持1，外部服务2）\r\n03 各主标准权重由用户自定义");
            this.comboBox_weightMS.Visible = false;
            this.comboBox_weightMS.SelectedIndexChanged += new System.EventHandler(this.comboBox_weightMS_SelectedIndexChanged_1);
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label31.Location = new System.Drawing.Point(14, 154);
            this.label31.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(92, 17);
            this.label31.TabIndex = 50;
            this.label31.Text = "主标准权重码：";
            this.label31.Visible = false;
            // 
            // comboBox5
            // 
            this.comboBox5.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.comboBox5.FormattingEnabled = true;
            this.comboBox5.Items.AddRange(new object[] {
            "定期评估",
            "累计评估"});
            this.comboBox5.Location = new System.Drawing.Point(100, 66);
            this.comboBox5.Margin = new System.Windows.Forms.Padding(2);
            this.comboBox5.Name = "comboBox5";
            this.comboBox5.Size = new System.Drawing.Size(148, 25);
            this.comboBox5.TabIndex = 49;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label26.Location = new System.Drawing.Point(14, 66);
            this.label26.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(68, 17);
            this.label26.TabIndex = 48;
            this.label26.Text = "评估类型：";
            // 
            // lable_materialName
            // 
            this.lable_materialName.AutoSize = true;
            this.lable_materialName.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lable_materialName.Location = new System.Drawing.Point(415, 41);
            this.lable_materialName.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lable_materialName.Name = "lable_materialName";
            this.lable_materialName.Size = new System.Drawing.Size(56, 17);
            this.lable_materialName.TabIndex = 47;
            this.lable_materialName.Text = "物料名称";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label22.Location = new System.Drawing.Point(278, 21);
            this.label22.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(44, 17);
            this.label22.TabIndex = 45;
            this.label22.Text = "物料：";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.dateTimePicker1.Location = new System.Drawing.Point(369, 107);
            this.dateTimePicker1.Margin = new System.Windows.Forms.Padding(2);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(148, 23);
            this.dateTimePicker1.TabIndex = 42;
            this.dateTimePicker1.ValueChanged += new System.EventHandler(this.dateTimePicker1_ValueChanged_1);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label20.Location = new System.Drawing.Point(278, 110);
            this.label20.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(68, 17);
            this.label20.TabIndex = 41;
            this.label20.Text = "评估日期：";
            // 
            // textBox15
            // 
            this.textBox15.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox15.Location = new System.Drawing.Point(100, 108);
            this.textBox15.Margin = new System.Windows.Forms.Padding(2);
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new System.Drawing.Size(150, 23);
            this.textBox15.TabIndex = 40;
            this.textBox15.TextChanged += new System.EventHandler(this.textBox15_TextChanged_1);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label19.Location = new System.Drawing.Point(15, 108);
            this.label19.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(56, 17);
            this.label19.TabIndex = 39;
            this.label19.Text = "评估人：";
            // 
            // lable_supplierName
            // 
            this.lable_supplierName.AutoSize = true;
            this.lable_supplierName.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lable_supplierName.Location = new System.Drawing.Point(136, 43);
            this.lable_supplierName.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lable_supplierName.Name = "lable_supplierName";
            this.lable_supplierName.Size = new System.Drawing.Size(68, 17);
            this.lable_supplierName.TabIndex = 38;
            this.lable_supplierName.Text = "供应商名称";
            // 
            // lable_purchasingORGName
            // 
            this.lable_purchasingORGName.AutoSize = true;
            this.lable_purchasingORGName.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lable_purchasingORGName.Location = new System.Drawing.Point(640, 42);
            this.lable_purchasingORGName.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lable_purchasingORGName.Name = "lable_purchasingORGName";
            this.lable_purchasingORGName.Size = new System.Drawing.Size(80, 17);
            this.lable_purchasingORGName.TabIndex = 37;
            this.lable_purchasingORGName.Text = "采购组织名称";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.Location = new System.Drawing.Point(14, 21);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 17);
            this.label2.TabIndex = 36;
            this.label2.Text = "供应商：";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(549, 22);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 17);
            this.label1.TabIndex = 35;
            this.label1.Text = "采购组织：";
            // 
            // comboBox_supplierID
            // 
            this.comboBox_supplierID.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.comboBox_supplierID.FormattingEnabled = true;
            this.comboBox_supplierID.Location = new System.Drawing.Point(100, 18);
            this.comboBox_supplierID.Margin = new System.Windows.Forms.Padding(2);
            this.comboBox_supplierID.Name = "comboBox_supplierID";
            this.comboBox_supplierID.Size = new System.Drawing.Size(148, 25);
            this.comboBox_supplierID.TabIndex = 34;
            this.comboBox_supplierID.SelectedIndexChanged += new System.EventHandler(this.comboBox_supplierID_SelectedIndexChanged_1);
            // 
            // groupBoxResult
            // 
            this.groupBoxResult.AutoSize = true;
            this.groupBoxResult.Controls.Add(this.groupBox1);
            this.groupBoxResult.Controls.Add(this.groupBox2);
            this.groupBoxResult.Controls.Add(this.groupBox4);
            this.groupBoxResult.Controls.Add(this.groupBox3);
            this.groupBoxResult.Font = new System.Drawing.Font("微软雅黑", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.groupBoxResult.Location = new System.Drawing.Point(47, 284);
            this.groupBoxResult.Margin = new System.Windows.Forms.Padding(2);
            this.groupBoxResult.Name = "groupBoxResult";
            this.groupBoxResult.Padding = new System.Windows.Forms.Padding(2);
            this.groupBoxResult.Size = new System.Drawing.Size(1002, 646);
            this.groupBoxResult.TabIndex = 73;
            this.groupBoxResult.TabStop = false;
            this.groupBoxResult.Text = "评估结果";
            // 
            // button3
            // 
            this.button3.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.button3.Location = new System.Drawing.Point(290, 33);
            this.button3.Margin = new System.Windows.Forms.Padding(2);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(139, 26);
            this.button3.TabIndex = 74;
            this.button3.Text = "判断是否为低效能供应商";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click_1);
            // 
            // SupplierPerformanceAddForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1142, 945);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.groupBoxResult);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "SupplierPerformanceAddForm";
            this.Text = "手动添加供应商绩效评估";
            this.Load += new System.EventHandler(this.SupplierPerformanceAddForm_Load);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBoxResult.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox TextBoxTotalScore;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox TextboxExternalService;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox TextboxServiceSupport;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox TextboxDeliver;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox TextboxDeliverOnTime;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox TextboxConfirmDate;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox TextboxQuantityReliability;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox TextboxQuality;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox TextboxReceiveGoods;
        private System.Windows.Forms.TextBox TextboxQualityAudit;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox TextboxComplaintReject;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox TextboxPrice;
        private System.Windows.Forms.TextBox TextboxPriceLevel;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox TextboxPriceHistory;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox TextboxGoodsShipment;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.ComboBox comboBox_weightSS;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.ComboBox comboBox_weightMS;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.ComboBox comboBox5;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label lable_materialName;
        private System.Windows.Forms.ComboBox comboBox_materialID;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox textBox15;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label lable_supplierName;
        private System.Windows.Forms.Label lable_purchasingORGName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBox_supplierID;
        private System.Windows.Forms.ComboBox comboBox_purchasingORGID;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.GroupBox groupBoxResult;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.ComboBox CB_modelType;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button4;
    }
}