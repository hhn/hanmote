﻿namespace MMClient.SupplierPerformance
{
    partial class SupplierPerformanceAutoForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.button1 = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.dateTimePicker3 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.comboBox_weightSS = new System.Windows.Forms.ComboBox();
            this.label30 = new System.Windows.Forms.Label();
            this.comboBox_weightMS = new System.Windows.Forms.ComboBox();
            this.label31 = new System.Windows.Forms.Label();
            this.TextBoxAuto_totalScore = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.comboBox5 = new System.Windows.Forms.ComboBox();
            this.label26 = new System.Windows.Forms.Label();
            this.lable_materialName = new System.Windows.Forms.Label();
            this.cbMaterialID = new System.Windows.Forms.ComboBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.label20 = new System.Windows.Forms.Label();
            this.textBox15 = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.lable_supplierName = new System.Windows.Forms.Label();
            this.lable_purchasingORGName = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBox_supplierID = new System.Windows.Forms.ComboBox();
            this.comboBox_purchasingORGID = new System.Windows.Forms.ComboBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.TextboxAuto_externalService = new System.Windows.Forms.TextBox();
            this.TextboxAuto_serviceSupport = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.TextboxAuto_deliver = new System.Windows.Forms.TextBox();
            this.TextboxAuto_confirmDate = new System.Windows.Forms.TextBox();
            this.TextboxAuto_goodsShipment = new System.Windows.Forms.TextBox();
            this.TextboxAuto_deliverOnTime = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.TextboxAuto_quantityReliability = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.TextboxAuto_receiveGoods = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.TextboxAuto_qualityAudit = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.TextboxAuto_complaintReject = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.TextboxAuto_quality = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.TextboxAuto_price = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.TextboxAuto_priceLevel = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.TextboxAuto_priceHistory = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.button3 = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.button1.Location = new System.Drawing.Point(16, 11);
            this.button1.Margin = new System.Windows.Forms.Padding(2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(58, 30);
            this.button1.TabIndex = 6;
            this.button1.Text = "评估";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.dateTimePicker3);
            this.panel1.Controls.Add(this.dateTimePicker2);
            this.panel1.Controls.Add(this.comboBox_weightSS);
            this.panel1.Controls.Add(this.label30);
            this.panel1.Controls.Add(this.comboBox_weightMS);
            this.panel1.Controls.Add(this.label31);
            this.panel1.Controls.Add(this.TextBoxAuto_totalScore);
            this.panel1.Controls.Add(this.label27);
            this.panel1.Controls.Add(this.comboBox5);
            this.panel1.Controls.Add(this.label26);
            this.panel1.Controls.Add(this.lable_materialName);
            this.panel1.Controls.Add(this.cbMaterialID);
            this.panel1.Controls.Add(this.label22);
            this.panel1.Controls.Add(this.label21);
            this.panel1.Controls.Add(this.dateTimePicker1);
            this.panel1.Controls.Add(this.label20);
            this.panel1.Controls.Add(this.textBox15);
            this.panel1.Controls.Add(this.label19);
            this.panel1.Controls.Add(this.lable_supplierName);
            this.panel1.Controls.Add(this.lable_purchasingORGName);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.comboBox_supplierID);
            this.panel1.Controls.Add(this.comboBox_purchasingORGID);
            this.panel1.Location = new System.Drawing.Point(7, 20);
            this.panel1.Margin = new System.Windows.Forms.Padding(2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1088, 188);
            this.panel1.TabIndex = 35;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(585, 70);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(23, 20);
            this.label3.TabIndex = 35;
            this.label3.Text = "到";
            // 
            // dateTimePicker3
            // 
            this.dateTimePicker3.Location = new System.Drawing.Point(674, 66);
            this.dateTimePicker3.Name = "dateTimePicker3";
            this.dateTimePicker3.Size = new System.Drawing.Size(144, 25);
            this.dateTimePicker3.TabIndex = 34;
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.Location = new System.Drawing.Point(395, 67);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(144, 25);
            this.dateTimePicker2.TabIndex = 33;
            // 
            // comboBox_weightSS
            // 
            this.comboBox_weightSS.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.comboBox_weightSS.FormattingEnabled = true;
            this.comboBox_weightSS.Location = new System.Drawing.Point(396, 111);
            this.comboBox_weightSS.Margin = new System.Windows.Forms.Padding(2);
            this.comboBox_weightSS.Name = "comboBox_weightSS";
            this.comboBox_weightSS.Size = new System.Drawing.Size(140, 25);
            this.comboBox_weightSS.TabIndex = 28;
            this.toolTip1.SetToolTip(this.comboBox_weightSS, "01 各次标准权重相等\r\n02 各次标准权重由用户自定义");
            this.comboBox_weightSS.SelectedIndexChanged += new System.EventHandler(this.comboBox_weightSS_SelectedIndexChanged);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label30.Location = new System.Drawing.Point(288, 116);
            this.label30.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(92, 17);
            this.label30.TabIndex = 27;
            this.label30.Text = "次标准权重码：";
            // 
            // comboBox_weightMS
            // 
            this.comboBox_weightMS.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.comboBox_weightMS.FormattingEnabled = true;
            this.comboBox_weightMS.Location = new System.Drawing.Point(94, 115);
            this.comboBox_weightMS.Margin = new System.Windows.Forms.Padding(2);
            this.comboBox_weightMS.Name = "comboBox_weightMS";
            this.comboBox_weightMS.Size = new System.Drawing.Size(144, 25);
            this.comboBox_weightMS.TabIndex = 26;
            this.toolTip1.SetToolTip(this.comboBox_weightMS, "01 各主标准权重相等\r\n02 各主标准权重为固定值\r\n （价格5，质量3，收获2，一般服务/支持1，外部服务2）\r\n03 各主标准权重由用户自定义");
            this.comboBox_weightMS.SelectedIndexChanged += new System.EventHandler(this.comboBox_weightMS_SelectedIndexChanged);
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label31.Location = new System.Drawing.Point(4, 117);
            this.label31.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(92, 17);
            this.label31.TabIndex = 25;
            this.label31.Text = "主标准权重码：";
            // 
            // TextBoxAuto_totalScore
            // 
            this.TextBoxAuto_totalScore.Enabled = false;
            this.TextBoxAuto_totalScore.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.TextBoxAuto_totalScore.Location = new System.Drawing.Point(674, 111);
            this.TextBoxAuto_totalScore.Margin = new System.Windows.Forms.Padding(2);
            this.TextBoxAuto_totalScore.Name = "TextBoxAuto_totalScore";
            this.TextBoxAuto_totalScore.Size = new System.Drawing.Size(144, 23);
            this.TextBoxAuto_totalScore.TabIndex = 24;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label27.Location = new System.Drawing.Point(585, 116);
            this.label27.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(44, 17);
            this.label27.TabIndex = 23;
            this.label27.Text = "总分：";
            // 
            // comboBox5
            // 
            this.comboBox5.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.comboBox5.FormattingEnabled = true;
            this.comboBox5.Location = new System.Drawing.Point(94, 72);
            this.comboBox5.Margin = new System.Windows.Forms.Padding(2);
            this.comboBox5.Name = "comboBox5";
            this.comboBox5.Size = new System.Drawing.Size(144, 25);
            this.comboBox5.TabIndex = 22;
           // this.comboBox5.SelectedIndexChanged += new System.EventHandler(this.comboBox5_SelectedIndexChanged);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label26.Location = new System.Drawing.Point(5, 75);
            this.label26.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(68, 17);
            this.label26.TabIndex = 21;
            this.label26.Text = "评估类型：";
            // 
            // lable_materialName
            // 
            this.lable_materialName.AutoSize = true;
            this.lable_materialName.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lable_materialName.Location = new System.Drawing.Point(434, 48);
            this.lable_materialName.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lable_materialName.Name = "lable_materialName";
            this.lable_materialName.Size = new System.Drawing.Size(56, 17);
            this.lable_materialName.TabIndex = 20;
            this.lable_materialName.Text = "物料名称";
            // 
            // cbMaterialID
            // 
            this.cbMaterialID.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.cbMaterialID.FormattingEnabled = true;
            this.cbMaterialID.Location = new System.Drawing.Point(394, 20);
            this.cbMaterialID.Margin = new System.Windows.Forms.Padding(2);
            this.cbMaterialID.Name = "cbMaterialID";
            this.cbMaterialID.Size = new System.Drawing.Size(144, 25);
            this.cbMaterialID.TabIndex = 19;
            this.cbMaterialID.SelectedIndexChanged += new System.EventHandler(this.comboBox_materialID_SelectedIndexChanged);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label22.Location = new System.Drawing.Point(290, 26);
            this.label22.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(44, 17);
            this.label22.TabIndex = 18;
            this.label22.Text = "物料：";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label21.Location = new System.Drawing.Point(289, 75);
            this.label21.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(68, 17);
            this.label21.TabIndex = 16;
            this.label21.Text = "时间范围：";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.dateTimePicker1.Location = new System.Drawing.Point(396, 154);
            this.dateTimePicker1.Margin = new System.Windows.Forms.Padding(2);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(143, 23);
            this.dateTimePicker1.TabIndex = 15;
            this.dateTimePicker1.ValueChanged += new System.EventHandler(this.dateTimePicker1_ValueChanged);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label20.Location = new System.Drawing.Point(288, 157);
            this.label20.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(68, 17);
            this.label20.TabIndex = 14;
            this.label20.Text = "评估日期：";
            // 
            // textBox15
            // 
            this.textBox15.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox15.Location = new System.Drawing.Point(94, 155);
            this.textBox15.Margin = new System.Windows.Forms.Padding(2);
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new System.Drawing.Size(144, 23);
            this.textBox15.TabIndex = 13;
            this.textBox15.TextChanged += new System.EventHandler(this.textBox15_TextChanged);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label19.Location = new System.Drawing.Point(4, 158);
            this.label19.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(56, 17);
            this.label19.TabIndex = 12;
            this.label19.Text = "评估人：";
            // 
            // lable_supplierName
            // 
            this.lable_supplierName.AutoSize = true;
            this.lable_supplierName.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lable_supplierName.Location = new System.Drawing.Point(124, 51);
            this.lable_supplierName.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lable_supplierName.Name = "lable_supplierName";
            this.lable_supplierName.Size = new System.Drawing.Size(68, 17);
            this.lable_supplierName.TabIndex = 11;
            this.lable_supplierName.Text = "供应商名称";
            // 
            // lable_purchasingORGName
            // 
            this.lable_purchasingORGName.AutoSize = true;
            this.lable_purchasingORGName.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lable_purchasingORGName.Location = new System.Drawing.Point(714, 46);
            this.lable_purchasingORGName.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lable_purchasingORGName.Name = "lable_purchasingORGName";
            this.lable_purchasingORGName.Size = new System.Drawing.Size(80, 17);
            this.lable_purchasingORGName.TabIndex = 10;
            this.lable_purchasingORGName.Text = "采购组织名称";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.Location = new System.Drawing.Point(5, 25);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 17);
            this.label2.TabIndex = 9;
            this.label2.Text = "供应商：";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(586, 23);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 17);
            this.label1.TabIndex = 8;
            this.label1.Text = "采购组织：";
            // 
            // comboBox_supplierID
            // 
            this.comboBox_supplierID.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.comboBox_supplierID.FormattingEnabled = true;
            this.comboBox_supplierID.Location = new System.Drawing.Point(94, 21);
            this.comboBox_supplierID.Margin = new System.Windows.Forms.Padding(2);
            this.comboBox_supplierID.Name = "comboBox_supplierID";
            this.comboBox_supplierID.Size = new System.Drawing.Size(144, 25);
            this.comboBox_supplierID.TabIndex = 7;
            this.comboBox_supplierID.SelectedIndexChanged += new System.EventHandler(this.comboBox_supplierID_SelectedIndexChanged);
            // 
            // comboBox_purchasingORGID
            // 
            this.comboBox_purchasingORGID.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.comboBox_purchasingORGID.FormattingEnabled = true;
            this.comboBox_purchasingORGID.Location = new System.Drawing.Point(674, 15);
            this.comboBox_purchasingORGID.Margin = new System.Windows.Forms.Padding(2);
            this.comboBox_purchasingORGID.Name = "comboBox_purchasingORGID";
            this.comboBox_purchasingORGID.Size = new System.Drawing.Size(144, 25);
            this.comboBox_purchasingORGID.TabIndex = 6;
            this.comboBox_purchasingORGID.SelectedIndexChanged += new System.EventHandler(this.comboBox_purchasingORGID_SelectedIndexChanged);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.groupBox6);
            this.panel2.Location = new System.Drawing.Point(9, 261);
            this.panel2.Margin = new System.Windows.Forms.Padding(2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1110, 700);
            this.panel2.TabIndex = 36;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.groupBox4);
            this.groupBox6.Controls.Add(this.groupBox3);
            this.groupBox6.Controls.Add(this.groupBox2);
            this.groupBox6.Controls.Add(this.groupBox1);
            this.groupBox6.Font = new System.Drawing.Font("微软雅黑", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.groupBox6.Location = new System.Drawing.Point(5, 12);
            this.groupBox6.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox6.Size = new System.Drawing.Size(1087, 620);
            this.groupBox6.TabIndex = 72;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "评估结果";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.TextboxAuto_externalService);
            this.groupBox4.Controls.Add(this.TextboxAuto_serviceSupport);
            this.groupBox4.Controls.Add(this.label18);
            this.groupBox4.Controls.Add(this.label17);
            this.groupBox4.Location = new System.Drawing.Point(124, 463);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox4.Size = new System.Drawing.Size(696, 81);
            this.groupBox4.TabIndex = 66;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "服务";
            // 
            // TextboxAuto_externalService
            // 
            this.TextboxAuto_externalService.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.TextboxAuto_externalService.Location = new System.Drawing.Point(402, 31);
            this.TextboxAuto_externalService.Margin = new System.Windows.Forms.Padding(2);
            this.TextboxAuto_externalService.Name = "TextboxAuto_externalService";
            this.TextboxAuto_externalService.ReadOnly = true;
            this.TextboxAuto_externalService.Size = new System.Drawing.Size(124, 23);
            this.TextboxAuto_externalService.TabIndex = 62;
            // 
            // TextboxAuto_serviceSupport
            // 
            this.TextboxAuto_serviceSupport.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.TextboxAuto_serviceSupport.Location = new System.Drawing.Point(156, 34);
            this.TextboxAuto_serviceSupport.Margin = new System.Windows.Forms.Padding(2);
            this.TextboxAuto_serviceSupport.Name = "TextboxAuto_serviceSupport";
            this.TextboxAuto_serviceSupport.ReadOnly = true;
            this.TextboxAuto_serviceSupport.Size = new System.Drawing.Size(100, 23);
            this.TextboxAuto_serviceSupport.TabIndex = 60;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label18.Location = new System.Drawing.Point(297, 34);
            this.label18.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(92, 17);
            this.label18.TabIndex = 61;
            this.label18.Text = "外部服务总分：";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label17.Location = new System.Drawing.Point(32, 37);
            this.label17.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(121, 17);
            this.label17.TabIndex = 59;
            this.label17.Text = "一般服务/支持总分：";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Controls.Add(this.TextboxAuto_deliver);
            this.groupBox3.Controls.Add(this.TextboxAuto_confirmDate);
            this.groupBox3.Controls.Add(this.TextboxAuto_goodsShipment);
            this.groupBox3.Controls.Add(this.TextboxAuto_deliverOnTime);
            this.groupBox3.Controls.Add(this.label15);
            this.groupBox3.Controls.Add(this.TextboxAuto_quantityReliability);
            this.groupBox3.Controls.Add(this.label14);
            this.groupBox3.Controls.Add(this.label16);
            this.groupBox3.Location = new System.Drawing.Point(124, 290);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox3.Size = new System.Drawing.Size(696, 153);
            this.groupBox3.TabIndex = 65;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "交货";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label13.Location = new System.Drawing.Point(256, 25);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(133, 17);
            this.label13.TabIndex = 51;
            this.label13.Text = "交货-按时交货的表现：";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label12.Location = new System.Drawing.Point(32, 64);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(68, 17);
            this.label12.TabIndex = 49;
            this.label12.Text = "交货总分：";
            // 
            // TextboxAuto_deliver
            // 
            this.TextboxAuto_deliver.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.TextboxAuto_deliver.Location = new System.Drawing.Point(108, 58);
            this.TextboxAuto_deliver.Margin = new System.Windows.Forms.Padding(2);
            this.TextboxAuto_deliver.Name = "TextboxAuto_deliver";
            this.TextboxAuto_deliver.ReadOnly = true;
            this.TextboxAuto_deliver.Size = new System.Drawing.Size(105, 23);
            this.TextboxAuto_deliver.TabIndex = 50;
            // 
            // TextboxAuto_confirmDate
            // 
            this.TextboxAuto_confirmDate.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.TextboxAuto_confirmDate.Location = new System.Drawing.Point(404, 49);
            this.TextboxAuto_confirmDate.Margin = new System.Windows.Forms.Padding(2);
            this.TextboxAuto_confirmDate.Name = "TextboxAuto_confirmDate";
            this.TextboxAuto_confirmDate.ReadOnly = true;
            this.TextboxAuto_confirmDate.Size = new System.Drawing.Size(105, 23);
            this.TextboxAuto_confirmDate.TabIndex = 54;
            // 
            // TextboxAuto_goodsShipment
            // 
            this.TextboxAuto_goodsShipment.Enabled = false;
            this.TextboxAuto_goodsShipment.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.TextboxAuto_goodsShipment.Location = new System.Drawing.Point(404, 101);
            this.TextboxAuto_goodsShipment.Margin = new System.Windows.Forms.Padding(2);
            this.TextboxAuto_goodsShipment.Name = "TextboxAuto_goodsShipment";
            this.TextboxAuto_goodsShipment.ReadOnly = true;
            this.TextboxAuto_goodsShipment.Size = new System.Drawing.Size(105, 23);
            this.TextboxAuto_goodsShipment.TabIndex = 58;
            // 
            // TextboxAuto_deliverOnTime
            // 
            this.TextboxAuto_deliverOnTime.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.TextboxAuto_deliverOnTime.Location = new System.Drawing.Point(404, 22);
            this.TextboxAuto_deliverOnTime.Margin = new System.Windows.Forms.Padding(2);
            this.TextboxAuto_deliverOnTime.Name = "TextboxAuto_deliverOnTime";
            this.TextboxAuto_deliverOnTime.ReadOnly = true;
            this.TextboxAuto_deliverOnTime.Size = new System.Drawing.Size(105, 23);
            this.TextboxAuto_deliverOnTime.TabIndex = 52;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label15.Location = new System.Drawing.Point(256, 77);
            this.label15.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(109, 17);
            this.label15.TabIndex = 55;
            this.label15.Text = "交货-数量可靠性：";
            // 
            // TextboxAuto_quantityReliability
            // 
            this.TextboxAuto_quantityReliability.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.TextboxAuto_quantityReliability.Location = new System.Drawing.Point(404, 75);
            this.TextboxAuto_quantityReliability.Margin = new System.Windows.Forms.Padding(2);
            this.TextboxAuto_quantityReliability.Name = "TextboxAuto_quantityReliability";
            this.TextboxAuto_quantityReliability.ReadOnly = true;
            this.TextboxAuto_quantityReliability.Size = new System.Drawing.Size(105, 23);
            this.TextboxAuto_quantityReliability.TabIndex = 56;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label14.Location = new System.Drawing.Point(256, 51);
            this.label14.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(121, 17);
            this.label14.TabIndex = 53;
            this.label14.Text = "交货-遵守确认日期：";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label16.Location = new System.Drawing.Point(256, 104);
            this.label16.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(145, 17);
            this.label16.TabIndex = 57;
            this.label16.Text = "交货-对装运须知的遵守：";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.TextboxAuto_receiveGoods);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.TextboxAuto_qualityAudit);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.TextboxAuto_complaintReject);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.TextboxAuto_quality);
            this.groupBox2.Location = new System.Drawing.Point(124, 146);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox2.Size = new System.Drawing.Size(696, 124);
            this.groupBox2.TabIndex = 64;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "质量";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label8.Location = new System.Drawing.Point(31, 52);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(68, 17);
            this.label8.TabIndex = 41;
            this.label8.Text = "质量总分：";
            // 
            // TextboxAuto_receiveGoods
            // 
            this.TextboxAuto_receiveGoods.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.TextboxAuto_receiveGoods.Location = new System.Drawing.Point(379, 16);
            this.TextboxAuto_receiveGoods.Margin = new System.Windows.Forms.Padding(2);
            this.TextboxAuto_receiveGoods.Name = "TextboxAuto_receiveGoods";
            this.TextboxAuto_receiveGoods.ReadOnly = true;
            this.TextboxAuto_receiveGoods.Size = new System.Drawing.Size(115, 23);
            this.TextboxAuto_receiveGoods.TabIndex = 44;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label9.Location = new System.Drawing.Point(255, 22);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(73, 17);
            this.label9.TabIndex = 43;
            this.label9.Text = "质量-收货：";
            // 
            // TextboxAuto_qualityAudit
            // 
            this.TextboxAuto_qualityAudit.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.TextboxAuto_qualityAudit.Location = new System.Drawing.Point(379, 43);
            this.TextboxAuto_qualityAudit.Margin = new System.Windows.Forms.Padding(2);
            this.TextboxAuto_qualityAudit.Name = "TextboxAuto_qualityAudit";
            this.TextboxAuto_qualityAudit.ReadOnly = true;
            this.TextboxAuto_qualityAudit.Size = new System.Drawing.Size(115, 23);
            this.TextboxAuto_qualityAudit.TabIndex = 46;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label10.Location = new System.Drawing.Point(255, 48);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(97, 17);
            this.label10.TabIndex = 45;
            this.label10.Text = "质量-质量审计：";
            // 
            // TextboxAuto_complaintReject
            // 
            this.TextboxAuto_complaintReject.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.TextboxAuto_complaintReject.Location = new System.Drawing.Point(379, 69);
            this.TextboxAuto_complaintReject.Margin = new System.Windows.Forms.Padding(2);
            this.TextboxAuto_complaintReject.Name = "TextboxAuto_complaintReject";
            this.TextboxAuto_complaintReject.ReadOnly = true;
            this.TextboxAuto_complaintReject.Size = new System.Drawing.Size(115, 23);
            this.TextboxAuto_complaintReject.TabIndex = 48;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label11.Location = new System.Drawing.Point(255, 74);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(126, 17);
            this.label11.TabIndex = 47;
            this.label11.Text = "质量-抱怨/拒绝水平：";
            // 
            // TextboxAuto_quality
            // 
            this.TextboxAuto_quality.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.TextboxAuto_quality.Location = new System.Drawing.Point(108, 49);
            this.TextboxAuto_quality.Margin = new System.Windows.Forms.Padding(2);
            this.TextboxAuto_quality.Name = "TextboxAuto_quality";
            this.TextboxAuto_quality.ReadOnly = true;
            this.TextboxAuto_quality.Size = new System.Drawing.Size(105, 23);
            this.TextboxAuto_quality.TabIndex = 42;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.TextboxAuto_price);
            this.groupBox1.Controls.Add(this.label28);
            this.groupBox1.Controls.Add(this.TextboxAuto_priceLevel);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.TextboxAuto_priceHistory);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Location = new System.Drawing.Point(124, 38);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox1.Size = new System.Drawing.Size(696, 104);
            this.groupBox1.TabIndex = 63;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "价格";
            // 
            // TextboxAuto_price
            // 
            this.TextboxAuto_price.Enabled = false;
            this.TextboxAuto_price.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.TextboxAuto_price.Location = new System.Drawing.Point(108, 30);
            this.TextboxAuto_price.Margin = new System.Windows.Forms.Padding(2);
            this.TextboxAuto_price.Name = "TextboxAuto_price";
            this.TextboxAuto_price.ReadOnly = true;
            this.TextboxAuto_price.Size = new System.Drawing.Size(105, 23);
            this.TextboxAuto_price.TabIndex = 36;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label28.Location = new System.Drawing.Point(26, 33);
            this.label28.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(68, 17);
            this.label28.TabIndex = 41;
            this.label28.Text = "价格总分：";
            // 
            // TextboxAuto_priceLevel
            // 
            this.TextboxAuto_priceLevel.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.TextboxAuto_priceLevel.Location = new System.Drawing.Point(354, 16);
            this.TextboxAuto_priceLevel.Margin = new System.Windows.Forms.Padding(2);
            this.TextboxAuto_priceLevel.Name = "TextboxAuto_priceLevel";
            this.TextboxAuto_priceLevel.ReadOnly = true;
            this.TextboxAuto_priceLevel.Size = new System.Drawing.Size(132, 23);
            this.TextboxAuto_priceLevel.TabIndex = 38;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label6.Location = new System.Drawing.Point(253, 20);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(97, 17);
            this.label6.TabIndex = 37;
            this.label6.Text = "价格-价格水平：";
            // 
            // TextboxAuto_priceHistory
            // 
            this.TextboxAuto_priceHistory.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.TextboxAuto_priceHistory.Location = new System.Drawing.Point(354, 43);
            this.TextboxAuto_priceHistory.Margin = new System.Windows.Forms.Padding(2);
            this.TextboxAuto_priceHistory.Name = "TextboxAuto_priceHistory";
            this.TextboxAuto_priceHistory.ReadOnly = true;
            this.TextboxAuto_priceHistory.Size = new System.Drawing.Size(132, 23);
            this.TextboxAuto_priceHistory.TabIndex = 40;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label7.Location = new System.Drawing.Point(253, 46);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(97, 17);
            this.label7.TabIndex = 39;
            this.label7.Text = "价格-价格历史：";
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.button2.Location = new System.Drawing.Point(101, 11);
            this.button2.Margin = new System.Windows.Forms.Padding(2);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(57, 30);
            this.button2.TabIndex = 63;
            this.button2.Text = "保存";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.panel1);
            this.groupBox5.Font = new System.Drawing.Font("微软雅黑", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.groupBox5.Location = new System.Drawing.Point(9, 45);
            this.groupBox5.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox5.Size = new System.Drawing.Size(1110, 212);
            this.groupBox5.TabIndex = 37;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "基本信息";
            // 
            // button3
            // 
            this.button3.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.button3.Location = new System.Drawing.Point(198, 11);
            this.button3.Margin = new System.Windows.Forms.Padding(2);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(133, 30);
            this.button3.TabIndex = 75;
            this.button3.Text = "判断是否为低效能供应商";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // SupplierPerformanceAutoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1253, 903);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.button2);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "SupplierPerformanceAutoForm";
            this.Text = " 自动评估";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label lable_supplierName;
        private System.Windows.Forms.Label lable_purchasingORGName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBox_supplierID;
        private System.Windows.Forms.ComboBox comboBox_purchasingORGID;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox TextboxAuto_externalService;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox TextboxAuto_serviceSupport;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox TextboxAuto_goodsShipment;
        private System.Windows.Forms.TextBox TextboxAuto_quantityReliability;
        private System.Windows.Forms.TextBox TextboxAuto_confirmDate;
        private System.Windows.Forms.TextBox TextboxAuto_deliverOnTime;
        private System.Windows.Forms.TextBox TextboxAuto_deliver;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox textBox15;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label lable_materialName;
        private System.Windows.Forms.ComboBox cbMaterialID;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.ComboBox comboBox5;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox TextBoxAuto_totalScore;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox TextboxAuto_price;
        private System.Windows.Forms.TextBox TextboxAuto_priceLevel;
        private System.Windows.Forms.TextBox TextboxAuto_priceHistory;
        private System.Windows.Forms.ComboBox comboBox_weightMS;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.ComboBox comboBox_weightSS;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox TextboxAuto_quality;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox TextboxAuto_receiveGoods;
        private System.Windows.Forms.TextBox TextboxAuto_qualityAudit;
        private System.Windows.Forms.TextBox TextboxAuto_complaintReject;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker dateTimePicker3;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
    }
}