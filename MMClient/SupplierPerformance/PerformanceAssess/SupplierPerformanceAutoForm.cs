﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MMClient.ContractManage;
using Lib.Bll.SupplierPerformaceBLL;
using WeifenLuo.WinFormsUI.Docking;
using MMClient.SupplierPerformance.PerformanceAssess;

namespace MMClient.SupplierPerformance
{
    public partial class SupplierPerformanceAutoForm : DockContent
    {
        String materialID;
        String supplierID;
        String purchasingORGID;
        DataTable supplierInfoTable, materialInfoTable, purchasingORGInfoTable;
        DateTime startTime = new DateTime();
        DateTime endTime = new DateTime();

        //用户自定义参数
        List<double> mainStandardWeight = new List<double>();//存放主标准权重(放5个数)

        List<double> priceSSWeight = new List<double>();//存放价格次标准权重（放2个数）
        List<double> qualitySSWeight = new List<double>();//存放质量次标准权重（放3个数）
        List<double> deliverySSWeight = new List<double>();//存放收获次标准权重（放4个数）

        String assessName;
        String assessDate = DateTime.Now.ToString();//修改

        List<double> priceLevelPara = new List<double>();
        List<double> priceHistoryPara = new List<double>();
        List<double> PPMPara = new List<double>();
        //用list传递值，之后将其中的值赋值给double型变量paraConfirmDate
        List<double> confirmDatePara = new List<double>();

        SupplierPerformanceBLL supplierPerformanceBLL = new SupplierPerformanceBLL();

        Decimal priceLevelScore, priceHistoryScore, priceScore, receiveGoodsScore, qualityAuditScore, complaintRejectScore, qualityScore;
        Decimal deliverOnTimeScore, confirmDateScore, quantityReliabilityScore, goodsShipmentScore, deliverScore, serviceSupportScore, externalServiceScore, totalScore;

        public SupplierPerformanceAutoForm()
        {
            InitializeComponent();

            //选择供应商id
            supplierInfoTable = supplierPerformanceBLL.querySupplier();
            comboBox_supplierID.DataSource = supplierInfoTable;
            comboBox_supplierID.DisplayMember = supplierInfoTable.Columns["Supplier_ID"].ToString();
            lable_supplierName.Text = (supplierInfoTable.Rows[comboBox_supplierID.SelectedIndex]["Supplier_Name"]).ToString();


            comboBox5.Items.Add("定期评估");
            comboBox5.Items.Add("累计评估");

            comboBox_weightMS.Items.Add("01");
            comboBox_weightMS.Items.Add("02");
            comboBox_weightMS.Items.Add("03");

            comboBox_weightSS.Items.Add("01");
            comboBox_weightSS.Items.Add("02");
        }

        //启动评估
        private void button1_Click(object sender, EventArgs e)
        {
           
            ////根据选定的评估类型，更改时间范围下拉框的选项
          //  if (comboBox5.SelectedItem.ToString() == "定期评估")
          //  {
          //      startTime = this.dateTimePicker2.Value;
           //     int month = this.dateTimePicker2.Value.Month + 1;
           //     endTime = Convert.ToDateTime(this.dateTimePicker2.Value.Year + "年" + month + "月");
            //}
            //else 
            //{
                startTime = this.dateTimePicker2.Value;
                endTime = this.dateTimePicker3.Value;
           // }
            if (supplierID == null || materialID == null || startTime == null || endTime == null || mainStandardWeight == null || priceSSWeight == null || qualitySSWeight == null || deliverySSWeight == null || assessName == null || assessDate == null)
            {
                MessageBox.Show("请输入完整的评估信息！");
                return;
            }
            else if (mainStandardWeight.Count != 5 || priceSSWeight.Count != 2 || qualitySSWeight.Count != 3 || deliverySSWeight.Count != 4)
            {
                MessageBox.Show("请检查主标准和次标准的权重！");
                return;
            }
            //若用户已输入完整的信息，则启动评估
            else
            {
                //显示用户自定义参数的界面，传入对象参数记录数据
                UserDefineForm userDefineForm = new UserDefineForm(priceLevelPara, priceHistoryPara, PPMPara, confirmDatePara);
                userDefineForm.Owner = this;
                userDefineForm.ShowDialog();

                //检查参数是否录入成功
                if (priceLevelPara.Count != 2 || priceHistoryPara.Count != 2 || PPMPara.Count != 2 || confirmDatePara.Count != 1)
                {
                    MessageBox.Show("评估自定义参数录入不成功！");
                    return;
                }

                //分数为值101为异常的标志
                //主标准：价格
                //调用计算 价格-价格水平 分数的方法，并显示在界面上
                //已测试 right
                priceLevelScore = supplierPerformanceBLL.calculatePriceLevel(materialID, supplierID, startTime, endTime, priceLevelPara);
                if (priceLevelScore <= 100)
                {
                    TextboxAuto_priceLevel.Text = Math.Round(priceLevelScore, 1).ToString();
                }
                else
                {
                    TextboxAuto_priceLevel.Text = "暂无评估数据";
                }

                //调用计算 价格-价格历史 分数的方法，并显示在界面上
                priceHistoryScore = supplierPerformanceBLL.calculatePriceHistory(materialID, supplierID, startTime, endTime, priceHistoryPara);
                if (priceHistoryScore <= 100)
                {
                    TextboxAuto_priceHistory.Text = Math.Round(priceHistoryScore, 1).ToString();
                }
                else
                {
                    TextboxAuto_priceHistory.Text = "暂无评估数据";
                }


                //调用计算 价格 分数的方法，并显示在界面上
                if ((priceLevelScore > 100) || (priceHistoryScore > 100))
                {
                    priceScore = 101;
                    TextboxAuto_price.Text = "暂无评估数据";
                }
                else
                {
                    priceScore = supplierPerformanceBLL.calculatePriceScore(priceLevelScore, priceHistoryScore, priceSSWeight);
                    TextboxAuto_price.Text = Math.Round(priceScore, 1).ToString();
                }

                //主标准：质量
                //调用计算 质量-收货 分数的方法，并显示在界面上
                receiveGoodsScore = supplierPerformanceBLL.queryReceiveGoods(materialID, supplierID, startTime, endTime, PPMPara);
                if (receiveGoodsScore < 100)
                {
                    TextboxAuto_receiveGoods.Text = Math.Round(receiveGoodsScore, 1).ToString();
                }
                else
                {
                    TextboxAuto_receiveGoods.Text = "暂无评估数据";
                }
                //调用计算 质量-质量审计 分数的方法，并显示在界面上
                qualityAuditScore = supplierPerformanceBLL.queryQualityAudit(materialID, supplierID, startTime, endTime);
                if (qualityAuditScore < 100)
                {
                    TextboxAuto_qualityAudit.Text = Math.Round(qualityAuditScore, 1).ToString();
                }
                else
                {
                    TextboxAuto_qualityAudit.Text = "暂无评估数据";
                }

                //调用计算 质量-抱怨/拒绝水平 分数的方法，并显示在界面上
                //complaintRejectScore = supplierPerformanceBLL.queryComplaintReject(materialID, supplierID, purchasingORGID, startTime, endTime);
                complaintRejectScore = supplierPerformanceBLL.queryComplaintReject(materialID, supplierID, startTime, endTime);

                if (complaintRejectScore < 100)
                {
                    TextboxAuto_complaintReject.Text = Math.Round(complaintRejectScore, 1).ToString();
                }
                else
                {
                    TextboxAuto_complaintReject.Text = "暂无评估数据";
                }

                //调用计算 质量 分数的方法，并显示在界面上
                if ((receiveGoodsScore > 100) || (qualityAuditScore > 100) || (complaintRejectScore > 100))
                {
                    qualityScore = 101;
                    TextboxAuto_quality.Text = "暂无评估数据";
                 
                }
                else
                {
                    qualityScore = supplierPerformanceBLL.calculateQualityScore(receiveGoodsScore, qualityAuditScore, complaintRejectScore, qualitySSWeight);
                    TextboxAuto_quality.Text = Math.Round(qualityScore, 1).ToString();
                }

                //主标准：交货

                //调用计算 交货-按时交货的表现 分数的方法，并显示在界面上
                //deliverOnTimeScore = supplierPerformanceBLL.queryDeliverOnTime(materialID, supplierID, purchasingORGID, startTime, endTime);
                deliverOnTimeScore = supplierPerformanceBLL.queryDeliverOnTime(materialID, supplierID, startTime, endTime);
                if (deliverOnTimeScore < 100)
                {
                    TextboxAuto_deliverOnTime.Text = Math.Round(deliverOnTimeScore, 1).ToString();

                }
                else
                {
                    TextboxAuto_deliverOnTime.Text = "暂无评估数据";
               
                }


                //调用计算 交货-确认日期 分数的方法，并显示在界面上
                double paraConfirmDate = confirmDatePara[0];
                //confirmDateScore = supplierPerformanceBLL.queryConfirmDate(materialID, supplierID, purchasingORGID, startTime, endTime, paraConfirmDate);
                confirmDateScore = supplierPerformanceBLL.queryConfirmDate(materialID, supplierID, startTime, endTime, paraConfirmDate);
                if (confirmDateScore < 100)
                {
                    TextboxAuto_confirmDate.Text = Math.Round(confirmDateScore, 1).ToString();
                }
                else
                {
                    TextboxAuto_confirmDate.Text = "暂无评估数据";
                }

                //调用计算 交货-数量可靠性 的分数的方法，并显示在界面上
                quantityReliabilityScore = supplierPerformanceBLL.queryQuantityReliability(materialID, supplierID, startTime, endTime);
                if (quantityReliabilityScore < 100)
                {
                    TextboxAuto_quantityReliability.Text = Math.Round(quantityReliabilityScore, 1).ToString();
                }
                else
                {
                    TextboxAuto_quantityReliability.Text = "暂无评估数据";
                }


                //调用计算 交货-对装运须知的遵守 的分数的方法，并显示在界面上
                goodsShipmentScore = supplierPerformanceBLL.queryGoodsShipment(materialID, supplierID, startTime, endTime);
                if (goodsShipmentScore < 100)
                {
                    TextboxAuto_goodsShipment.Text = Math.Round(goodsShipmentScore, 1).ToString();
                }
                else
                {
                    TextboxAuto_goodsShipment.Text = "暂无评估数据";
                }


                //调用计算 交货 分数的方法，并显示在界面上
                if ((deliverOnTimeScore > 100) || (confirmDateScore > 100) || (quantityReliabilityScore > 100) || (goodsShipmentScore > 100))
                {
                    deliverScore = 101;
                    TextboxAuto_deliver.Text = "暂无评估数据";
                }
                else
                {
                    deliverScore = supplierPerformanceBLL.calculateDeliverScore(deliverOnTimeScore, confirmDateScore, quantityReliabilityScore, goodsShipmentScore, deliverySSWeight);
                    TextboxAuto_deliver.Text = Math.Round(deliverScore, 1).ToString();
                }


                //主标准：一般服务/支持

                //调用计算 一般服务/支持 分数的方法，并显示在界面上    
                serviceSupportScore = supplierPerformanceBLL.queryServiceSupport(materialID, supplierID, startTime, endTime);
                if (serviceSupportScore < 100)
                {
                    TextboxAuto_serviceSupport.Text = Math.Round(serviceSupportScore, 1).ToString();
                }
                else
                {
                    TextboxAuto_serviceSupport.Text = "暂无评估数据";
                }


                //主标准：外部服务

                //调用计算 外部服务 分数的方法，并显示在界面上
                externalServiceScore = supplierPerformanceBLL.queryExternalService(materialID, supplierID, startTime, endTime);
                if (externalServiceScore < 100)
                {
                    TextboxAuto_externalService.Text = Math.Round(externalServiceScore, 1).ToString();
                }
                else
                {
                    TextboxAuto_externalService.Text = "暂无评估数据";
                }

                //调用计算 总分 分数的方法，并显示在界面上
                if ((priceScore > 100) || (qualityScore > 100) || (deliverScore > 100) || (serviceSupportScore > 100) || (externalServiceScore > 100))
                {
                    totalScore = 101;
                    TextBoxAuto_totalScore.Text = "暂无评估数据";
                }
                else
                {
                    totalScore = supplierPerformanceBLL.calculateTotalScore(priceScore, qualityScore, deliverScore, serviceSupportScore, externalServiceScore, mainStandardWeight);
                    TextBoxAuto_totalScore.Text = Math.Round(totalScore, 1).ToString();
                }
            }
        }

        //保存,button
        private void button2_Click(object sender, EventArgs e)
        {
            List<Object> saveRstPara = new List<object>();
            if (saveRstPara.Count != 0)
            {
                saveRstPara.Clear();
            }
            saveRstPara.Add(supplierID);
            saveRstPara.Add(purchasingORGID);
            saveRstPara.Add(materialID);
            saveRstPara.Add(totalScore);

            #region 将分数添加到saveRstPara中（判断添加分数还是字符串“null”）
            //价格
            if (priceScore <= 100)
            {
                saveRstPara.Add(priceScore);
            }
            else
            {
                saveRstPara.Add("null");
            }
            //价格水平
            if (priceLevelScore <= 100)
            {
                saveRstPara.Add(priceLevelScore);
            }
            else
            {
                saveRstPara.Add("null");
            }
            //价格历史
            if (priceHistoryScore <= 100)
            {
                saveRstPara.Add(priceHistoryScore);
            }
            else
            {
                saveRstPara.Add("null");
            }
            //质量
            if (qualityScore <= 100)
            {
                saveRstPara.Add(qualityScore);
            }
            else
            {
                saveRstPara.Add("null");
            }
            //收货
            if (receiveGoodsScore <= 100)
            {
                saveRstPara.Add(receiveGoodsScore);
            }
            else
            {
                saveRstPara.Add("null");
            }

            //质量审计
            if (qualityAuditScore <= 100)
            {
                saveRstPara.Add(qualityAuditScore);
            }
            else
            {
                saveRstPara.Add("null");
            }

            //抱怨/拒绝水平
            if (complaintRejectScore <= 100)
            {
                saveRstPara.Add(complaintRejectScore);
            }
            else
            {
                saveRstPara.Add("null");
            }

            //交货
            if (deliverScore <= 100)
            {
                saveRstPara.Add(deliverScore);
            }
            else
            {
                saveRstPara.Add("null");
            }

            //按时交货的表现
            if (deliverOnTimeScore <= 100)
            {
                saveRstPara.Add(deliverOnTimeScore);
            }
            else
            {
                saveRstPara.Add("null");
            }

            //确认日期
            if (confirmDateScore <= 100)
            {
                saveRstPara.Add(confirmDateScore);
            }
            else
            {
                saveRstPara.Add("null");
            }

            //数量可靠性
            if (quantityReliabilityScore <= 100)
            {
                saveRstPara.Add(quantityReliabilityScore);
            }
            else
            {
                saveRstPara.Add("null");
            }

            //对装运须知的遵守
            if (goodsShipmentScore <= 100)
            {
                saveRstPara.Add(goodsShipmentScore);
            }
            else
            {
                saveRstPara.Add("null");
            }

            //一般服务/支持
            if (serviceSupportScore <= 100)
            {
                saveRstPara.Add(serviceSupportScore);
            }
            else
            {
                saveRstPara.Add("null");
            }

            //外部服务
            if (externalServiceScore <= 100)
            {
                saveRstPara.Add(externalServiceScore);
            }
            else
            {
                saveRstPara.Add("null");
            }
            #endregion

            //评估类型
            if (comboBox5.Text == "累计评估")
            {
                saveRstPara.Add(1);
            }
            else
            {
                saveRstPara.Add(0);
            }
            saveRstPara.Add(this.dateTimePicker1.Value.ToString());//时间范围 index =19 

            saveRstPara.Add(assessName);
            saveRstPara.Add(assessDate);

            int lines = supplierPerformanceBLL.saveResult(saveRstPara);
            if (lines > 0)
            {
                MessageBox.Show("绩效评估结果保存成功！");
            }

        }

        /// <summary>
        /// 评估时间，时间范围
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //private void comboBox5_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    //根据选定的评估类型，更改时间范围下拉框的选项
        //    if (comboBox5.SelectedItem.ToString() == "定期评估")
        //    {
        //        this.dateTimePicker3.Visible = false;
        //        this.label3.Visible = false;
        //    }
        //    else if (comboBox5.SelectedItem.ToString() == "累计评估")
        //    {
        //        this.dateTimePicker3.Visible = true;
        //        this.label3.Visible = true;
        //    }
        //}

        //主标准权重输入
        private void comboBox_weightMS_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox_weightMS.SelectedItem.ToString() == "03")
            {
                MainStandardWeightForm msWeightForm = new MainStandardWeightForm(mainStandardWeight);
                msWeightForm.Owner = this;
                msWeightForm.ShowDialog();
            }
            else if (comboBox_weightMS.SelectedItem.ToString() == "01")
            {
                int i = 0;
                //检查list是否为空，如果不为空，则置空
                if (mainStandardWeight.Count != 0)
                {
                    mainStandardWeight.Clear();
                }
                //将每个主标准权重设置为1
                while (i < 5)
                {
                    mainStandardWeight.Add(1);
                    i++;
                }
            }
            else if (comboBox_weightMS.SelectedItem.ToString() == "02")
            {
                //检查list是否为空，如果不为空，则置空
                if (mainStandardWeight.Count != 0)
                {
                    mainStandardWeight.Clear();
                }
                mainStandardWeight.Add(5);
                mainStandardWeight.Add(3);
                mainStandardWeight.Add(2);
                mainStandardWeight.Add(1);
                mainStandardWeight.Add(2);
            }
            else
            {
                MessageBox.Show("请选择主标准权重代码！");
            }

        }

        //次标准权重
        private void comboBox_weightSS_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox_weightSS.SelectedItem.ToString() == "01")
            {
                //如果已经有值，清空
                if (priceSSWeight.Count != 0)
                {
                    priceSSWeight.Clear();
                }
                if (qualitySSWeight.Count != 0)
                {
                    qualitySSWeight.Clear();
                }
                if (deliverySSWeight.Count != 0)
                {
                    deliverySSWeight.Clear();
                }

                int i = 0;
                //将每个价格次标准权重设置为1，共2个
                while (i < 2)
                {
                    priceSSWeight.Add(1);
                    i++;
                }

                //将每个质量次标准权重设置为1，共3个
                int j = 0;
                while (j < 3)
                {
                    qualitySSWeight.Add(1);
                    j++;
                }

                //将每个交货次标准权重设置为1，共4个
                int k = 0;
                while (k < 4)
                {
                    deliverySSWeight.Add(1);
                    k++;
                }

            }
            else if (comboBox_weightSS.SelectedItem.ToString() == "02")
            {
                SecondaryStandardWeightForm ssWeightForm = new SecondaryStandardWeightForm(priceSSWeight, qualitySSWeight, deliverySSWeight);
                ssWeightForm.Owner = this;
                ssWeightForm.ShowDialog();
            }
            else
            {
                MessageBox.Show("请选择次标准权重代码！");
            }
        }

        //评估人
        private void textBox15_TextChanged(object sender, EventArgs e)
        {
            assessName = textBox15.Text.ToString();
        }

        //评估日期
        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            assessDate = dateTimePicker1.Value.ToString();
        }

        //供应商ID
        private void comboBox_supplierID_SelectedIndexChanged(object sender, EventArgs e)
        {
            supplierID = comboBox_supplierID.Text.ToString();
            lable_supplierName.Text = supplierInfoTable.Rows[comboBox_supplierID.SelectedIndex]["Supplier_Name"].ToString();

            //选择的供应商变化时，则重新查询对应的采购组织信息
            purchasingORGInfoTable = supplierPerformanceBLL.queryPurchasingORG(supplierID);
            comboBox_purchasingORGID.DataSource = purchasingORGInfoTable;
            if (purchasingORGInfoTable.Rows.Count > 0)
            {
                comboBox_purchasingORGID.DisplayMember = purchasingORGInfoTable.Columns["PurchasingORG_ID"].ToString();
                lable_purchasingORGName.Text = (purchasingORGInfoTable.Rows[comboBox_purchasingORGID.SelectedIndex]["PurchasingORG_Name"]).ToString();
            }

            //选择的供应商变化时，则重新查询对应的物料信息
            materialInfoTable = supplierPerformanceBLL.queryMaterialID(supplierID);
            cbMaterialID.DataSource = materialInfoTable;
            if (materialInfoTable.Rows.Count > 0)
            {
                cbMaterialID.DisplayMember = materialInfoTable.Columns["Material_ID"].ToString();
                lable_materialName.Text = (materialInfoTable.Rows[cbMaterialID.SelectedIndex]["Material_Name"]).ToString();
            }
        }

        //物料ID
        private void comboBox_materialID_SelectedIndexChanged(object sender, EventArgs e)
        {
            materialID = cbMaterialID.Text.ToString();
            if (materialInfoTable.Rows.Count > 0)
            {
                lable_materialName.Text = (materialInfoTable.Rows[cbMaterialID.SelectedIndex]["Material_Name"]).ToString();
            }
        }

        //采购组织ID
        private void comboBox_purchasingORGID_SelectedIndexChanged(object sender, EventArgs e)
        {
            purchasingORGID = comboBox_purchasingORGID.Text.ToString();
            if (purchasingORGInfoTable.Rows.Count > 0)
            {
                lable_purchasingORGName.Text = (purchasingORGInfoTable.Rows[comboBox_purchasingORGID.SelectedIndex]["PurchasingORG_Name"]).ToString();
            }
        }

        /// <summary>
        /// button 判断是否为低效能供应商
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button3_Click(object sender, EventArgs e)
        {
            List<Decimal> score = new List<Decimal>();
            //score1到score6分别为：价格分数、质量分数、交货分数、一般服务支持分数、外部服务分数、总分
            Decimal score1, score2, score3, score4, score5, score6;
            score.Clear();
            try
            {
                //依次将主标准分数加入List中
                score1 = Convert.ToDecimal(TextboxAuto_price.Text.ToString());
                score2 = Convert.ToDecimal(TextboxAuto_quality.Text.ToString());
                score3 = Convert.ToDecimal(TextboxAuto_deliver.Text.ToString());
                score4 = Convert.ToDecimal(TextboxAuto_serviceSupport.Text.ToString());
                score5 = Convert.ToDecimal(TextboxAuto_externalService.Text.ToString());
                score6 = Convert.ToDecimal(TextBoxAuto_totalScore.Text.ToString());

            }
            catch
            {
                MessageBox.Show("主标准无有效分数！");
                return;
            }
            score.Add(score1);
            score.Add(score2);
            score.Add(score3);
            score.Add(score4);
            score.Add(score5);
            score.Add(score6);
            JudgeLowPerformanceForm judgeLowPerformanceForm = new JudgeLowPerformanceForm(supplierID, score);
            judgeLowPerformanceForm.Owner = this;
            judgeLowPerformanceForm.ShowDialog();
        }
    }
}
