﻿using Lib.Bll.SupplierPerformaceBLL;
using Lib.Common.MMCException.IDAL;
using Lib.SqlServerDAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;

namespace MMClient.SupplierPerformance.DeliveryGoods
{
    public partial class DeliveryEvalForm : DockContent
    {
        private string supplierID;
        SupplierPerformanceBLL supplierPerformanceBLL = new SupplierPerformanceBLL();
        
        public DeliveryEvalForm()
        {
            InitializeComponent();
        }

        private void DeliveryEvalForm_Load(object sender, EventArgs e)
        {
            //选择供应商id
            DataTable dt = supplierPerformanceBLL.querySupplier();
            this.cbSupplierId.DisplayMember = "Supplier_ID";
            this.cbSupplierId.ValueMember = "Supplier_Name";
            this.cbSupplierId.DataSource = dt;
        }
        

        private void cbSupplierId_SelectedIndexChanged(object sender, EventArgs e)
        {
            supplierID = this.cbSupplierId.Text.ToString();
            this.lbSupplierName.Text = this.cbSupplierId.SelectedValue.ToString();
            //选择的供应商变化时，则重新查询对应的物料信息
            DataTable dtMaterial = supplierPerformanceBLL.queryMaterialID(supplierID);
            this.cbMaterialId.DataSource = dtMaterial;
            if (dtMaterial.Rows.Count > 0)
            {
                this.cbMaterialId.DisplayMember = "Material_ID";
                this.cbMaterialId.ValueMember = "Material_Name";
                this.lbMaterialName.Text = this.cbMaterialId.SelectedValue.ToString();
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            int reciveNum;
            int reciveSumNum;
            string Order_ID="NULL";//待选择采购订单
            try
            {
                reciveNum = Convert.ToInt32(this.textBox1.Text.ToString().Trim());
               
            }catch(Exception ex)
            {
                MessageBox.Show("接货数量不合法");
                this.textBox1.Text = "";
                return;
            }
            try
            {
                reciveSumNum = Convert.ToInt32(this.textBox2.Text.ToString().Trim());

            }
            catch (Exception ex)
            {
                MessageBox.Show("订单数量不合法");
                this.textBox2.Text = "";
                return;
            }
            double score;
            try
            {
                score = Convert.ToDouble(this.textBox3.Text.ToString().Trim());

            }
            catch (Exception ex)
            {
                MessageBox.Show("装货得分输入不合法");
                this.textBox3.Text = "";
                return;
            }
            string orderIdStrsql = "select Order_ID  from[Order_Info] WHERE Supplier_ID = '" + this.cbSupplierId.Text.ToString() + "'";
            try
            {
               // Order_ID = DBHelper.ExecuteQueryDT(orderIdStrsql).Rows[0][0].ToString();
            }catch(Exception ex)
            {
                MessageBox.Show("当前供应商还没有生成订单");
                return;
            }
            string insertSQl = @"INSERT INTO GoodsReceive_Note (
	                                        [GoodsReceiveNote_ID],
	                                        [Order_ID],
	                                        [Supplier_ID],
	                                        [Material_ID],
	                                        [Receive_Count],
	                                        [CountInOrder],
	                                        [GoodsShipment_Score],
	                                        [StockIn_Date]
                                        )
                                        VALUES
	                                        (
		                                        'NULL',
		                                        '"+ Order_ID + @"',
		                                        '"+this.cbSupplierId.Text.ToString()+@"',
		                                        '"+this.cbMaterialId.Text.ToString()+@"',
		                                        '"+reciveNum+@"',
		                                        '"+reciveSumNum+@"',
		                                        '"+score+@"',
		                                        '"+this.dateTimePicker1.Value+@"'
	                                        )";
            try
            {
                DBHelper.ExecuteNonQuery(insertSQl);
                MessageBox.Show("插入成功");
                DialogResult dr;
                dr = MessageBox.Show("是否接着录入?", "检查", MessageBoxButtons.YesNoCancel,
                MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2);
                if (dr == DialogResult.Yes)
                {
                    setNull();
                    return;
                }
                else
                {
                    this.Close();
                }
            }
            catch(DBException exc)
            {
                MessageBox.Show("插入失败");
            }
        }

        private void setNull()
        {
            this.textBox1.Text = "";
            this.textBox2.Text = "";
            this.textBox3.Text = "";
        }
    }
}
