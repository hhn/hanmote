﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Lib.ContionSettings.SupplierPerformaceCS.SPReportCS;
using Lib.Bll.SupplierPerformaceBLL;
using ChartDirector;

namespace MMClient.SupplierPerformance.SPReport
{
    public partial class Frm_SPCompareBubbleChartResult : WeifenLuo.WinFormsUI.Docking.DockContent
    {

        #region 公共变量
        /// <summary>
        /// 供应商比较分析气泡图
        /// </summary>
        private SPR_SPCompareBubbleCharBLL sPR_SPCompareBubbleCharBLL = null;

        /// <summary>
        /// 查询气泡图所用条件
        /// </summary>
        private SPCompareBubbleChartConditionValue sPCompareBubbleChartConditionValue = null;

        #endregion

        #region 窗体构造函数
        public Frm_SPCompareBubbleChartResult()
        {
            InitializeComponent();
        }

        public Frm_SPCompareBubbleChartResult(SPCompareBubbleChartConditionValue sPCompareBubbleChartConditionValue)
            :this()
        {
            //显示气泡图条件
            this.sPCompareBubbleChartConditionValue = sPCompareBubbleChartConditionValue;
        }

        #endregion

        #region 窗体事件函数

        /// <summary>
        /// 窗体加载时触发
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Frm_SPCompareBubbleChartResult_Load(object sender, EventArgs e)
        {
            this.initChartData(sPCompareBubbleChartConditionValue);
        }

        #endregion


        #region 公共操作函数

        // <summary>
        /// 查询数据
        /// </summary>
        /// <param name="conditionvalue"></param>
        private void initChartData(SPCompareBubbleChartConditionValue conditionvalue)
        {
            if (this.sPR_SPCompareBubbleCharBLL == null)
            {
                this.sPR_SPCompareBubbleCharBLL = new SPR_SPCompareBubbleCharBLL();
            }
            //查询数据库，得到采购组织信息的DataTable
            //DataTable dtResult = sPR_SPCompareRadarCharBLL.getSupplierEvaluationScore(conditionvalue);
            WinChartViewer viewer = this.viewChart;
            this.drawChart(viewer);
        }

        /// <summary>
        /// 作图
        /// </summary>
        /// <param name="viewer"></param>
        public void drawChart(WinChartViewer viewer)
        {
            // The XYZ points for the bubble chart
            double[] dataX0 = { 150, 300, 1000, 1700 };
            double[] dataY0 = { 12, 60, 25, 65 };
            double[] dataZ0 = { 20, 50, 50, 85 };

            double[] dataX1 = { 500, 1000, 1300 };
            double[] dataY1 = { 35, 50, 75 };
            double[] dataZ1 = { 30, 55, 95 };

            // Create a XYChart object of size 450 x 420 pixels
            XYChart c = new XYChart(1270, 650);

            // Set the plotarea at (55, 65) and of size 350 x 300 pixels, with a light grey border
            // (0xc0c0c0). Turn on both horizontal and vertical grid lines with light grey color
            // (0xc0c0c0)
            c.setPlotArea(55, 65, 900, 500, -1, -1, 0xc0c0c0, 0xc0c0c0, -1);

            // Add a legend box at (50, 30) (top of the chart) with horizontal layout. Use 12pt
            // Times Bold Italic font. Set the background and border color to Transparent.
            //c.addLegend(50, 30, false, "微软雅黑 Bold ", 12).setBackground(Chart.Transparent);

            LegendBox b = c.addLegend(1210, 75, true, "微软雅黑", 10);
            b.setAlignment(Chart.TopRight);
            b.setBackground(Chart.silverColor(), Chart.Transparent, 1);

            // Add a title to the chart using 18pt Times Bold Itatic font.
            //c.addTitle("Product Comparison Chart", "微软雅黑 Bold Italic", 18);

            // Add a title to the y axis using 12pt Arial Bold Italic font
            c.yAxis().setTitle("供应商评估成绩", "微软雅黑 Bold", 12);

            // Add a title to the x axis using 12pt Arial Bold Italic font
            c.xAxis().setTitle("采购订单总金额", "微软雅黑 Bold", 12);

            // Set the axes line width to 3 pixels
            c.xAxis().setWidth(3);
            c.yAxis().setWidth(3);

            // Add (dataX0, dataY0) as a scatter layer with semi-transparent red (0x80ff3333) circle
            // symbols, where the circle size is modulated by dataZ0. This creates a bubble effect.
            c.addScatterLayer(dataX0, dataY0, "湖北武汉东风日产汽车股份有限公司", Chart.CircleSymbol, 9,
                unchecked((int)0x80ff3333), unchecked((int)0x80ff3333)).setSymbolScale(dataZ0);

            // Add (dataX1, dataY1) as a scatter layer with semi-transparent green (0x803333ff)
            // circle symbols, where the circle size is modulated by dataZ1. This creates a bubble
            // effect.
            c.addScatterLayer(dataX1, dataY1, "东风本田", Chart.CircleSymbol, 9,
                unchecked((int)0x803333ff), unchecked((int)0x803333ff)).setSymbolScale(dataZ1);

            // Output the chart
            viewer.Chart = c;
        }


        #endregion 

        
    }
}
