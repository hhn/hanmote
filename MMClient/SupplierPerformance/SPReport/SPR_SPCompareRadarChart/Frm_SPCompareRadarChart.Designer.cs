﻿namespace MMClient.SupplierPerformance.SPReport
{
    partial class Frm_SPCompareRadarChart
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.gb_ConditionSettings = new System.Windows.Forms.GroupBox();
            this.lb_seperator = new System.Windows.Forms.Label();
            this.gb_Supplier = new System.Windows.Forms.GroupBox();
            this.dgv_SupplierList = new System.Windows.Forms.DataGridView();
            this.sSelection = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.sId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sIndustry = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sAddress = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gb_Standard = new System.Windows.Forms.GroupBox();
            this.Price_Score = new System.Windows.Forms.CheckBox();
            this.PriceLevel_Score = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.PriceHistory_Score = new System.Windows.Forms.CheckBox();
            this.Quality_Score = new System.Windows.Forms.CheckBox();
            this.GoodReceipt_Score = new System.Windows.Forms.CheckBox();
            this.QualityAudit_Score = new System.Windows.Forms.CheckBox();
            this.ComplaintAndReject_Score = new System.Windows.Forms.CheckBox();
            this.Delivery_Score = new System.Windows.Forms.CheckBox();
            this.OnTimeDelivery_Score = new System.Windows.Forms.CheckBox();
            this.ConfirmDate_Score = new System.Windows.Forms.CheckBox();
            this.QuantityReliability_Score = new System.Windows.Forms.CheckBox();
            this.Shipment_Score = new System.Windows.Forms.CheckBox();
            this.GeneralServiceAndSupport_Score = new System.Windows.Forms.CheckBox();
            this.ExternalService_Score = new System.Windows.Forms.CheckBox();
            this.lb_SecondStandard = new System.Windows.Forms.Label();
            this.lb_MainStandard = new System.Windows.Forms.Label();
            this.btn_SelectPurchaseGroup = new System.Windows.Forms.Button();
            this.cbb_TimeInterval = new System.Windows.Forms.ComboBox();
            this.cbb_Year = new System.Windows.Forms.ComboBox();
            this.lb_TimeInterval = new System.Windows.Forms.Label();
            this.lb_Year = new System.Windows.Forms.Label();
            this.lb_EvaluationPeriod = new System.Windows.Forms.Label();
            this.txt_PurchaseGroupId = new System.Windows.Forms.TextBox();
            this.txt_PurchaseGroupName = new System.Windows.Forms.TextBox();
            this.lb_PurchaseGroup = new System.Windows.Forms.Label();
            this.btn_Clear = new System.Windows.Forms.Button();
            this.btn_Close = new System.Windows.Forms.Button();
            this.btn_OK = new System.Windows.Forms.Button();
            this.gb_ConditionSettings.SuspendLayout();
            this.gb_Supplier.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_SupplierList)).BeginInit();
            this.gb_Standard.SuspendLayout();
            this.SuspendLayout();
            // 
            // gb_ConditionSettings
            // 
            this.gb_ConditionSettings.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gb_ConditionSettings.Controls.Add(this.lb_seperator);
            this.gb_ConditionSettings.Controls.Add(this.gb_Supplier);
            this.gb_ConditionSettings.Controls.Add(this.gb_Standard);
            this.gb_ConditionSettings.Controls.Add(this.btn_SelectPurchaseGroup);
            this.gb_ConditionSettings.Controls.Add(this.cbb_TimeInterval);
            this.gb_ConditionSettings.Controls.Add(this.cbb_Year);
            this.gb_ConditionSettings.Controls.Add(this.lb_TimeInterval);
            this.gb_ConditionSettings.Controls.Add(this.lb_Year);
            this.gb_ConditionSettings.Controls.Add(this.lb_EvaluationPeriod);
            this.gb_ConditionSettings.Controls.Add(this.txt_PurchaseGroupId);
            this.gb_ConditionSettings.Controls.Add(this.txt_PurchaseGroupName);
            this.gb_ConditionSettings.Controls.Add(this.lb_PurchaseGroup);
            this.gb_ConditionSettings.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.gb_ConditionSettings.Location = new System.Drawing.Point(28, 91);
            this.gb_ConditionSettings.Name = "gb_ConditionSettings";
            this.gb_ConditionSettings.Size = new System.Drawing.Size(1069, 567);
            this.gb_ConditionSettings.TabIndex = 0;
            this.gb_ConditionSettings.TabStop = false;
            this.gb_ConditionSettings.Text = "条件设置";
            // 
            // lb_seperator
            // 
            this.lb_seperator.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lb_seperator.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.lb_seperator.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lb_seperator.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lb_seperator.Location = new System.Drawing.Point(6, 125);
            this.lb_seperator.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lb_seperator.Name = "lb_seperator";
            this.lb_seperator.Size = new System.Drawing.Size(1057, 1);
            this.lb_seperator.TabIndex = 10;
            // 
            // gb_Supplier
            // 
            this.gb_Supplier.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gb_Supplier.Controls.Add(this.dgv_SupplierList);
            this.gb_Supplier.Location = new System.Drawing.Point(353, 137);
            this.gb_Supplier.Margin = new System.Windows.Forms.Padding(2);
            this.gb_Supplier.Name = "gb_Supplier";
            this.gb_Supplier.Padding = new System.Windows.Forms.Padding(2);
            this.gb_Supplier.Size = new System.Drawing.Size(700, 425);
            this.gb_Supplier.TabIndex = 34;
            this.gb_Supplier.TabStop = false;
            this.gb_Supplier.Text = "选择供应商";
            // 
            // dgv_SupplierList
            // 
            this.dgv_SupplierList.AllowUserToAddRows = false;
            this.dgv_SupplierList.AllowUserToDeleteRows = false;
            this.dgv_SupplierList.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv_SupplierList.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv_SupplierList.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            this.dgv_SupplierList.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgv_SupplierList.ColumnHeadersHeight = 25;
            this.dgv_SupplierList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgv_SupplierList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.sSelection,
            this.sId,
            this.sName,
            this.sIndustry,
            this.sAddress});
            this.dgv_SupplierList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_SupplierList.EnableHeadersVisualStyles = false;
            this.dgv_SupplierList.Location = new System.Drawing.Point(2, 18);
            this.dgv_SupplierList.Margin = new System.Windows.Forms.Padding(2);
            this.dgv_SupplierList.MultiSelect = false;
            this.dgv_SupplierList.Name = "dgv_SupplierList";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.GradientActiveCaption;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_SupplierList.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgv_SupplierList.RowHeadersWidth = 47;
            this.dgv_SupplierList.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Black;
            this.dgv_SupplierList.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this.dgv_SupplierList.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.Black;
            this.dgv_SupplierList.RowTemplate.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.Black;
            this.dgv_SupplierList.RowTemplate.Height = 27;
            this.dgv_SupplierList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv_SupplierList.Size = new System.Drawing.Size(696, 405);
            this.dgv_SupplierList.TabIndex = 35;
            this.dgv_SupplierList.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.dgv_SupplierList_RowPostPaint);
            // 
            // sSelection
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.NullValue = false;
            dataGridViewCellStyle2.Padding = new System.Windows.Forms.Padding(7, 0, 0, 0);
            this.sSelection.DefaultCellStyle = dataGridViewCellStyle2;
            this.sSelection.HeaderText = "OK";
            this.sSelection.Name = "sSelection";
            this.sSelection.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.sSelection.Width = 30;
            // 
            // sId
            // 
            this.sId.DataPropertyName = "Supplier_ID";
            this.sId.HeaderText = "供应商编号";
            this.sId.Name = "sId";
            this.sId.ReadOnly = true;
            this.sId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // sName
            // 
            this.sName.DataPropertyName = "Supplier_Name";
            this.sName.HeaderText = "供应商名称";
            this.sName.Name = "sName";
            this.sName.ReadOnly = true;
            this.sName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.sName.Width = 250;
            // 
            // sIndustry
            // 
            this.sIndustry.DataPropertyName = "SupplierIndustry_Id";
            this.sIndustry.HeaderText = "行业";
            this.sIndustry.Name = "sIndustry";
            this.sIndustry.ReadOnly = true;
            this.sIndustry.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // sAddress
            // 
            this.sAddress.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.sAddress.DataPropertyName = "Address";
            this.sAddress.HeaderText = "供应商地址";
            this.sAddress.Name = "sAddress";
            this.sAddress.ReadOnly = true;
            this.sAddress.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // gb_Standard
            // 
            this.gb_Standard.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.gb_Standard.BackColor = System.Drawing.SystemColors.Control;
            this.gb_Standard.Controls.Add(this.Price_Score);
            this.gb_Standard.Controls.Add(this.PriceLevel_Score);
            this.gb_Standard.Controls.Add(this.label6);
            this.gb_Standard.Controls.Add(this.label7);
            this.gb_Standard.Controls.Add(this.label9);
            this.gb_Standard.Controls.Add(this.label8);
            this.gb_Standard.Controls.Add(this.label5);
            this.gb_Standard.Controls.Add(this.PriceHistory_Score);
            this.gb_Standard.Controls.Add(this.Quality_Score);
            this.gb_Standard.Controls.Add(this.GoodReceipt_Score);
            this.gb_Standard.Controls.Add(this.QualityAudit_Score);
            this.gb_Standard.Controls.Add(this.ComplaintAndReject_Score);
            this.gb_Standard.Controls.Add(this.Delivery_Score);
            this.gb_Standard.Controls.Add(this.OnTimeDelivery_Score);
            this.gb_Standard.Controls.Add(this.ConfirmDate_Score);
            this.gb_Standard.Controls.Add(this.QuantityReliability_Score);
            this.gb_Standard.Controls.Add(this.Shipment_Score);
            this.gb_Standard.Controls.Add(this.GeneralServiceAndSupport_Score);
            this.gb_Standard.Controls.Add(this.ExternalService_Score);
            this.gb_Standard.Controls.Add(this.lb_SecondStandard);
            this.gb_Standard.Controls.Add(this.lb_MainStandard);
            this.gb_Standard.Location = new System.Drawing.Point(6, 136);
            this.gb_Standard.Name = "gb_Standard";
            this.gb_Standard.Size = new System.Drawing.Size(258, 425);
            this.gb_Standard.TabIndex = 11;
            this.gb_Standard.TabStop = false;
            this.gb_Standard.Text = "标准选择";
            // 
            // Price_Score
            // 
            this.Price_Score.AutoSize = true;
            this.Price_Score.Checked = true;
            this.Price_Score.CheckState = System.Windows.Forms.CheckState.Checked;
            this.Price_Score.Location = new System.Drawing.Point(19, 69);
            this.Price_Score.Name = "Price_Score";
            this.Price_Score.Size = new System.Drawing.Size(51, 21);
            this.Price_Score.TabIndex = 33;
            this.Price_Score.Text = "价格";
            this.Price_Score.UseVisualStyleBackColor = true;
            // 
            // PriceLevel_Score
            // 
            this.PriceLevel_Score.AutoSize = true;
            this.PriceLevel_Score.Location = new System.Drawing.Point(120, 58);
            this.PriceLevel_Score.Name = "PriceLevel_Score";
            this.PriceLevel_Score.Size = new System.Drawing.Size(75, 21);
            this.PriceLevel_Score.TabIndex = 31;
            this.PriceLevel_Score.Text = "价格水平";
            this.PriceLevel_Score.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label6.Location = new System.Drawing.Point(8, 44);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(244, 1);
            this.label6.TabIndex = 14;
            // 
            // label7
            // 
            this.label7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label7.Location = new System.Drawing.Point(8, 108);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(244, 1);
            this.label7.TabIndex = 18;
            // 
            // label9
            // 
            this.label9.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label9.Location = new System.Drawing.Point(8, 369);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(244, 1);
            this.label9.TabIndex = 32;
            // 
            // label8
            // 
            this.label8.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label8.Location = new System.Drawing.Point(8, 314);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(244, 1);
            this.label8.TabIndex = 30;
            // 
            // label5
            // 
            this.label5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label5.Location = new System.Drawing.Point(8, 206);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(244, 1);
            this.label5.TabIndex = 24;
            // 
            // PriceHistory_Score
            // 
            this.PriceHistory_Score.AutoSize = true;
            this.PriceHistory_Score.Location = new System.Drawing.Point(120, 82);
            this.PriceHistory_Score.Name = "PriceHistory_Score";
            this.PriceHistory_Score.Size = new System.Drawing.Size(75, 21);
            this.PriceHistory_Score.TabIndex = 16;
            this.PriceHistory_Score.Text = "价格历史";
            this.PriceHistory_Score.UseVisualStyleBackColor = true;
            // 
            // Quality_Score
            // 
            this.Quality_Score.AutoSize = true;
            this.Quality_Score.Checked = true;
            this.Quality_Score.CheckState = System.Windows.Forms.CheckState.Checked;
            this.Quality_Score.Location = new System.Drawing.Point(19, 149);
            this.Quality_Score.Name = "Quality_Score";
            this.Quality_Score.Size = new System.Drawing.Size(51, 21);
            this.Quality_Score.TabIndex = 15;
            this.Quality_Score.Text = "质量";
            this.Quality_Score.UseVisualStyleBackColor = true;
            // 
            // GoodReceipt_Score
            // 
            this.GoodReceipt_Score.AutoSize = true;
            this.GoodReceipt_Score.Location = new System.Drawing.Point(120, 120);
            this.GoodReceipt_Score.Name = "GoodReceipt_Score";
            this.GoodReceipt_Score.Size = new System.Drawing.Size(51, 21);
            this.GoodReceipt_Score.TabIndex = 17;
            this.GoodReceipt_Score.Text = "收货";
            this.GoodReceipt_Score.UseVisualStyleBackColor = true;
            // 
            // QualityAudit_Score
            // 
            this.QualityAudit_Score.AutoSize = true;
            this.QualityAudit_Score.Location = new System.Drawing.Point(120, 147);
            this.QualityAudit_Score.Name = "QualityAudit_Score";
            this.QualityAudit_Score.Size = new System.Drawing.Size(75, 21);
            this.QualityAudit_Score.TabIndex = 20;
            this.QualityAudit_Score.Text = "质量审计";
            this.QualityAudit_Score.UseVisualStyleBackColor = true;
            // 
            // ComplaintAndReject_Score
            // 
            this.ComplaintAndReject_Score.AutoSize = true;
            this.ComplaintAndReject_Score.Location = new System.Drawing.Point(120, 174);
            this.ComplaintAndReject_Score.Name = "ComplaintAndReject_Score";
            this.ComplaintAndReject_Score.Size = new System.Drawing.Size(104, 21);
            this.ComplaintAndReject_Score.TabIndex = 19;
            this.ComplaintAndReject_Score.Text = "抱怨/拒绝水平";
            this.ComplaintAndReject_Score.UseVisualStyleBackColor = true;
            // 
            // Delivery_Score
            // 
            this.Delivery_Score.AutoSize = true;
            this.Delivery_Score.Checked = true;
            this.Delivery_Score.CheckState = System.Windows.Forms.CheckState.Checked;
            this.Delivery_Score.Location = new System.Drawing.Point(19, 253);
            this.Delivery_Score.Name = "Delivery_Score";
            this.Delivery_Score.Size = new System.Drawing.Size(51, 21);
            this.Delivery_Score.TabIndex = 21;
            this.Delivery_Score.Text = "交货";
            this.Delivery_Score.UseVisualStyleBackColor = true;
            // 
            // OnTimeDelivery_Score
            // 
            this.OnTimeDelivery_Score.AutoSize = true;
            this.OnTimeDelivery_Score.Location = new System.Drawing.Point(120, 213);
            this.OnTimeDelivery_Score.Name = "OnTimeDelivery_Score";
            this.OnTimeDelivery_Score.Size = new System.Drawing.Size(111, 21);
            this.OnTimeDelivery_Score.TabIndex = 22;
            this.OnTimeDelivery_Score.Text = "按时交货的表现";
            this.OnTimeDelivery_Score.UseVisualStyleBackColor = true;
            // 
            // ConfirmDate_Score
            // 
            this.ConfirmDate_Score.AutoSize = true;
            this.ConfirmDate_Score.Location = new System.Drawing.Point(120, 239);
            this.ConfirmDate_Score.Name = "ConfirmDate_Score";
            this.ConfirmDate_Score.Size = new System.Drawing.Size(75, 21);
            this.ConfirmDate_Score.TabIndex = 28;
            this.ConfirmDate_Score.Text = "确认日期";
            this.ConfirmDate_Score.UseVisualStyleBackColor = true;
            // 
            // QuantityReliability_Score
            // 
            this.QuantityReliability_Score.AutoSize = true;
            this.QuantityReliability_Score.Location = new System.Drawing.Point(120, 264);
            this.QuantityReliability_Score.Name = "QuantityReliability_Score";
            this.QuantityReliability_Score.Size = new System.Drawing.Size(87, 21);
            this.QuantityReliability_Score.TabIndex = 27;
            this.QuantityReliability_Score.Text = "数量可靠性";
            this.QuantityReliability_Score.UseVisualStyleBackColor = true;
            // 
            // Shipment_Score
            // 
            this.Shipment_Score.AutoSize = true;
            this.Shipment_Score.Location = new System.Drawing.Point(120, 288);
            this.Shipment_Score.Name = "Shipment_Score";
            this.Shipment_Score.Size = new System.Drawing.Size(75, 21);
            this.Shipment_Score.TabIndex = 29;
            this.Shipment_Score.Text = "装运须知";
            this.Shipment_Score.UseVisualStyleBackColor = true;
            // 
            // GeneralServiceAndSupport_Score
            // 
            this.GeneralServiceAndSupport_Score.AutoSize = true;
            this.GeneralServiceAndSupport_Score.Checked = true;
            this.GeneralServiceAndSupport_Score.CheckState = System.Windows.Forms.CheckState.Checked;
            this.GeneralServiceAndSupport_Score.Location = new System.Drawing.Point(19, 331);
            this.GeneralServiceAndSupport_Score.Name = "GeneralServiceAndSupport_Score";
            this.GeneralServiceAndSupport_Score.Size = new System.Drawing.Size(104, 21);
            this.GeneralServiceAndSupport_Score.TabIndex = 26;
            this.GeneralServiceAndSupport_Score.Text = "一般服务/支持";
            this.GeneralServiceAndSupport_Score.UseVisualStyleBackColor = true;
            // 
            // ExternalService_Score
            // 
            this.ExternalService_Score.AutoSize = true;
            this.ExternalService_Score.Checked = true;
            this.ExternalService_Score.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ExternalService_Score.Location = new System.Drawing.Point(19, 388);
            this.ExternalService_Score.Name = "ExternalService_Score";
            this.ExternalService_Score.Size = new System.Drawing.Size(75, 21);
            this.ExternalService_Score.TabIndex = 25;
            this.ExternalService_Score.Text = "外部服务";
            this.ExternalService_Score.UseVisualStyleBackColor = true;
            // 
            // lb_SecondStandard
            // 
            this.lb_SecondStandard.AutoSize = true;
            this.lb_SecondStandard.Location = new System.Drawing.Point(127, 22);
            this.lb_SecondStandard.Name = "lb_SecondStandard";
            this.lb_SecondStandard.Size = new System.Drawing.Size(44, 17);
            this.lb_SecondStandard.TabIndex = 13;
            this.lb_SecondStandard.Text = "次标准";
            // 
            // lb_MainStandard
            // 
            this.lb_MainStandard.AutoSize = true;
            this.lb_MainStandard.Location = new System.Drawing.Point(22, 22);
            this.lb_MainStandard.Name = "lb_MainStandard";
            this.lb_MainStandard.Size = new System.Drawing.Size(44, 17);
            this.lb_MainStandard.TabIndex = 12;
            this.lb_MainStandard.Text = "主标准";
            // 
            // btn_SelectPurchaseGroup
            // 
            this.btn_SelectPurchaseGroup.BackColor = System.Drawing.Color.Transparent;
            this.btn_SelectPurchaseGroup.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_SelectPurchaseGroup.FlatAppearance.BorderColor = System.Drawing.SystemColors.Control;
            this.btn_SelectPurchaseGroup.FlatAppearance.BorderSize = 0;
            this.btn_SelectPurchaseGroup.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btn_SelectPurchaseGroup.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btn_SelectPurchaseGroup.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_SelectPurchaseGroup.Image = global::MMClient.Properties.Resources.search;
            this.btn_SelectPurchaseGroup.Location = new System.Drawing.Point(496, 25);
            this.btn_SelectPurchaseGroup.Name = "btn_SelectPurchaseGroup";
            this.btn_SelectPurchaseGroup.Size = new System.Drawing.Size(19, 22);
            this.btn_SelectPurchaseGroup.TabIndex = 4;
            this.btn_SelectPurchaseGroup.UseVisualStyleBackColor = false;
            this.btn_SelectPurchaseGroup.Click += new System.EventHandler(this.btn_SelectPurchaseGroup_Click);
            // 
            // cbb_TimeInterval
            // 
            this.cbb_TimeInterval.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbb_TimeInterval.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cbb_TimeInterval.FormattingEnabled = true;
            this.cbb_TimeInterval.Location = new System.Drawing.Point(192, 86);
            this.cbb_TimeInterval.Name = "cbb_TimeInterval";
            this.cbb_TimeInterval.Size = new System.Drawing.Size(159, 25);
            this.cbb_TimeInterval.TabIndex = 9;
            this.cbb_TimeInterval.SelectedIndexChanged += new System.EventHandler(this.cbb_TimeInterval_SelectedIndexChanged);
            // 
            // cbb_Year
            // 
            this.cbb_Year.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbb_Year.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cbb_Year.FormattingEnabled = true;
            this.cbb_Year.Items.AddRange(new object[] {
            "",
            "2016",
            "2017",
            "2018",
            "2019",
            "2020",
            "2021",
            "2022",
            "2023",
            "2024"});
            this.cbb_Year.Location = new System.Drawing.Point(103, 86);
            this.cbb_Year.Name = "cbb_Year";
            this.cbb_Year.Size = new System.Drawing.Size(83, 25);
            this.cbb_Year.TabIndex = 8;
            this.cbb_Year.SelectedIndexChanged += new System.EventHandler(this.cbb_Year_SelectedIndexChanged);
            // 
            // lb_TimeInterval
            // 
            this.lb_TimeInterval.AutoSize = true;
            this.lb_TimeInterval.Location = new System.Drawing.Point(190, 64);
            this.lb_TimeInterval.Name = "lb_TimeInterval";
            this.lb_TimeInterval.Size = new System.Drawing.Size(32, 17);
            this.lb_TimeInterval.TabIndex = 7;
            this.lb_TimeInterval.Text = "时段";
            // 
            // lb_Year
            // 
            this.lb_Year.AutoSize = true;
            this.lb_Year.Location = new System.Drawing.Point(103, 64);
            this.lb_Year.Name = "lb_Year";
            this.lb_Year.Size = new System.Drawing.Size(32, 17);
            this.lb_Year.TabIndex = 6;
            this.lb_Year.Text = "年度";
            // 
            // lb_EvaluationPeriod
            // 
            this.lb_EvaluationPeriod.AutoSize = true;
            this.lb_EvaluationPeriod.Location = new System.Drawing.Point(9, 64);
            this.lb_EvaluationPeriod.Name = "lb_EvaluationPeriod";
            this.lb_EvaluationPeriod.Size = new System.Drawing.Size(56, 17);
            this.lb_EvaluationPeriod.TabIndex = 5;
            this.lb_EvaluationPeriod.Text = "评估周期";
            // 
            // txt_PurchaseGroupId
            // 
            this.txt_PurchaseGroupId.Location = new System.Drawing.Point(353, 26);
            this.txt_PurchaseGroupId.MaxLength = 20;
            this.txt_PurchaseGroupId.Name = "txt_PurchaseGroupId";
            this.txt_PurchaseGroupId.Size = new System.Drawing.Size(144, 23);
            this.txt_PurchaseGroupId.TabIndex = 3;
            // 
            // txt_PurchaseGroupName
            // 
            this.txt_PurchaseGroupName.Location = new System.Drawing.Point(103, 26);
            this.txt_PurchaseGroupName.MaxLength = 20;
            this.txt_PurchaseGroupName.Name = "txt_PurchaseGroupName";
            this.txt_PurchaseGroupName.Size = new System.Drawing.Size(248, 23);
            this.txt_PurchaseGroupName.TabIndex = 2;
            // 
            // lb_PurchaseGroup
            // 
            this.lb_PurchaseGroup.AutoSize = true;
            this.lb_PurchaseGroup.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lb_PurchaseGroup.Location = new System.Drawing.Point(9, 31);
            this.lb_PurchaseGroup.Name = "lb_PurchaseGroup";
            this.lb_PurchaseGroup.Size = new System.Drawing.Size(56, 17);
            this.lb_PurchaseGroup.TabIndex = 1;
            this.lb_PurchaseGroup.Text = "采购组织";
            // 
            // btn_Clear
            // 
            this.btn_Clear.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn_Clear.Location = new System.Drawing.Point(116, 45);
            this.btn_Clear.Name = "btn_Clear";
            this.btn_Clear.Size = new System.Drawing.Size(70, 27);
            this.btn_Clear.TabIndex = 37;
            this.btn_Clear.Text = "清除";
            this.btn_Clear.UseVisualStyleBackColor = true;
            this.btn_Clear.Click += new System.EventHandler(this.btn_Clear_Click);
            // 
            // btn_Close
            // 
            this.btn_Close.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn_Close.Location = new System.Drawing.Point(203, 45);
            this.btn_Close.Name = "btn_Close";
            this.btn_Close.Size = new System.Drawing.Size(70, 27);
            this.btn_Close.TabIndex = 38;
            this.btn_Close.Text = "关闭";
            this.btn_Close.UseVisualStyleBackColor = true;
            this.btn_Close.Click += new System.EventHandler(this.btn_Close_Click);
            // 
            // btn_OK
            // 
            this.btn_OK.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn_OK.Location = new System.Drawing.Point(28, 45);
            this.btn_OK.Name = "btn_OK";
            this.btn_OK.Size = new System.Drawing.Size(70, 27);
            this.btn_OK.TabIndex = 36;
            this.btn_OK.Text = "确定";
            this.btn_OK.UseVisualStyleBackColor = true;
            this.btn_OK.Click += new System.EventHandler(this.btn_OK_Click);
            // 
            // Frm_SPCompareRadarChart
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1107, 669);
            this.Controls.Add(this.gb_ConditionSettings);
            this.Controls.Add(this.btn_Clear);
            this.Controls.Add(this.btn_Close);
            this.Controls.Add(this.btn_OK);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Frm_SPCompareRadarChart";
            this.ShowInTaskbar = false;
            this.Text = "供应商对比雷达图";
            this.gb_ConditionSettings.ResumeLayout(false);
            this.gb_ConditionSettings.PerformLayout();
            this.gb_Supplier.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_SupplierList)).EndInit();
            this.gb_Standard.ResumeLayout(false);
            this.gb_Standard.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gb_ConditionSettings;
        private System.Windows.Forms.Button btn_SelectPurchaseGroup;
        private System.Windows.Forms.ComboBox cbb_TimeInterval;
        private System.Windows.Forms.ComboBox cbb_Year;
        private System.Windows.Forms.Label lb_TimeInterval;
        private System.Windows.Forms.Label lb_Year;
        private System.Windows.Forms.Label lb_EvaluationPeriod;
        private System.Windows.Forms.TextBox txt_PurchaseGroupId;
        private System.Windows.Forms.TextBox txt_PurchaseGroupName;
        private System.Windows.Forms.Label lb_PurchaseGroup;
        private System.Windows.Forms.Button btn_Clear;
        private System.Windows.Forms.Button btn_Close;
        private System.Windows.Forms.Button btn_OK;
        private System.Windows.Forms.GroupBox gb_Standard;
        private System.Windows.Forms.CheckBox Price_Score;
        private System.Windows.Forms.CheckBox PriceLevel_Score;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox PriceHistory_Score;
        private System.Windows.Forms.CheckBox Quality_Score;
        private System.Windows.Forms.CheckBox GoodReceipt_Score;
        private System.Windows.Forms.CheckBox QualityAudit_Score;
        private System.Windows.Forms.CheckBox ComplaintAndReject_Score;
        private System.Windows.Forms.CheckBox Delivery_Score;
        private System.Windows.Forms.CheckBox OnTimeDelivery_Score;
        private System.Windows.Forms.CheckBox ConfirmDate_Score;
        private System.Windows.Forms.CheckBox QuantityReliability_Score;
        private System.Windows.Forms.CheckBox Shipment_Score;
        private System.Windows.Forms.CheckBox GeneralServiceAndSupport_Score;
        private System.Windows.Forms.CheckBox ExternalService_Score;
        private System.Windows.Forms.Label lb_SecondStandard;
        private System.Windows.Forms.Label lb_MainStandard;
        private System.Windows.Forms.GroupBox gb_Supplier;
        private System.Windows.Forms.DataGridView dgv_SupplierList;
        private System.Windows.Forms.Label lb_seperator;
        private System.Windows.Forms.DataGridViewCheckBoxColumn sSelection;
        private System.Windows.Forms.DataGridViewTextBoxColumn sId;
        private System.Windows.Forms.DataGridViewTextBoxColumn sName;
        private System.Windows.Forms.DataGridViewTextBoxColumn sIndustry;
        private System.Windows.Forms.DataGridViewTextBoxColumn sAddress;
    }
}