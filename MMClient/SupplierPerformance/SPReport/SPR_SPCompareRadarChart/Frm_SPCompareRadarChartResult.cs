﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Lib.ContionSettings.SupplierPerformaceCS.SPReportCS;
using Lib.Bll.SupplierPerformaceBLL;
using ChartDirector;

namespace MMClient.SupplierPerformance.SPReport
{
    public partial class Frm_SPCompareRadarChartResult : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        #region 公共变量
        /// <summary>
        /// 供应商对比雷达图
        /// </summary>
        private SPR_SPCompareRadarCharBLL sPR_SPCompareRadarCharBLL = null;

        /// <summary>
        /// 查询雷达图所用条件
        /// </summary>
        private SPCompareRadarChartConditionValue sPCompareRadarChartConditionValue = null;

        #endregion

        #region 窗体构造函数

        public Frm_SPCompareRadarChartResult()
        {
            InitializeComponent();
        }

        public Frm_SPCompareRadarChartResult(SPCompareRadarChartConditionValue sPCompareRadarChartConditionValue)
            :this()
        {
            //显示雷达图条件
            this.sPCompareRadarChartConditionValue = sPCompareRadarChartConditionValue;
        }

        #endregion

        #region 窗体事件函数

        /// <summary>
        /// 窗体初始化时触发
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Frm_SPCompareRadarChartResult_Load(object sender, EventArgs e)
        {
            this.initChartData(sPCompareRadarChartConditionValue);
        }

        /// <summary>
        /// panel绘制时触发
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pl_Chart_Paint(object sender, PaintEventArgs e)
        {
            //初始化画笔，颜色：Slver，宽度：1px；
            Pen pen = new Pen(Color.Black, 1);
            //e.Graphics.DrawRectangle(pen, this.pl_Chart.Location.X, this.pl_Chart.Location.Y, this.pl_Chart.Width - 1, this.pl_Chart.Height - 1);
        }

        #endregion

        #region 公共操作函数

        /// <summary>
        /// 查询数据
        /// </summary>
        /// <param name="conditionvalue"></param>
        private void initChartData(SPCompareRadarChartConditionValue conditionvalue)
        {
            if (this.sPR_SPCompareRadarCharBLL == null)
            {
                this.sPR_SPCompareRadarCharBLL = new SPR_SPCompareRadarCharBLL();
            }
            //查询数据库，得到采购组织信息的DataTable
            DataTable dtResult = sPR_SPCompareRadarCharBLL.getSupplierEvaluationScore(conditionvalue);
            WinChartViewer viewer = this.viewChart;
            this.drawChart(viewer,dtResult,conditionvalue);
        }

        /// <summary>
        /// 作图
        /// </summary>
        /// <param name="view">制定显示的位置</param>
        /// <param name="data">给图标传递数据</param>
        private void drawChart(WinChartViewer viewer, DataTable data, SPCompareRadarChartConditionValue conditionvalue)
        {
            if (data != null && data.Rows.Count > 0)
            {
                Dictionary<string, string> temp;

                //提取标签名称标签
                string standardName = "";
                temp = conditionvalue.SaveSelectedStandardName;
                foreach (string sSName in temp.Values)
                {
                    standardName += sSName + ",";
                }
                standardName = standardName.Substring(0, standardName.Length - 1);
                //设置标签名称
                string[] label = standardName.Split(',');  //用分号隔开,转换成字符串数组

                //取出选择的标准Key
                List<string> standardAttributeList = new List<string>();
                foreach (string sAttribute in temp.Keys)
                {
                    standardAttributeList.Add(sAttribute);
                }


                //画一个大小为480*380的极图，背景色为金色，边界为1px的3D效果
                PolarChart c = new PolarChart(1500, 650, 0xffffff, 0xF0F0F0, 0);

                c.setPlotArea(400, 300, 250, 0xffffff);

                //Set the Grid style to circular grid
                c.setGridStyle(false);

                LegendBox b = c.addLegend(950, 75, true, "微软雅黑", 10);
                b.setAlignment(Chart.TopRight);
                b.setBackground(Chart.silverColor(), Chart.Transparent, 1);


                //读取数据，转换成double类型
                //取出保存的供应商信息
                Dictionary<string, string> tempSupplierMap = conditionvalue.SavaSelectedSupplierMap;
                List<string> supplierIdList = new List<string>();
                foreach (string sName in tempSupplierMap.Values)
                {
                    supplierIdList.Add(sName);
                }

                //读取出每个供应商的评分数据
                Dictionary<string, List<double>> supplierDataMap = new Dictionary<string, List<double>>();
                for (int i = 0; i < data.Rows.Count; i++)
                {
                    List<double> dataList = new List<double>();
                    foreach (string strStandardAtr in standardAttributeList)
                    {
                        dataList.Add(data.Rows[i][strStandardAtr] == null ? 0 : Convert.ToDouble(data.Rows[i][strStandardAtr]));
                    }
                    supplierDataMap.Add(supplierIdList[i], dataList);
                }

                string chartSupplierName;
                double[] chartSupplierData;

                //向图表中插入数据
                foreach (string key in supplierDataMap.Keys)  //根据键取值
                {
                    int j = 0;
                    chartSupplierName = key;
                    chartSupplierData = supplierDataMap[key].ToArray();   //将list中的数据复制到double数组中
                    c.addAreaLayer(chartSupplierData, unchecked((int)0x806666cc + 400 * j), chartSupplierName);
                    j++;
                }
                c.angularAxis().setLabels(label);
                viewer.Chart = c;
            }
            else
            {
                MessageBox.Show(this,
                                "亲，没有可查询的数据哟！",
                                "数据缺失警告",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Warning);
            }
        }

        #endregion

        
    }
}
