﻿namespace MMClient.SupplierPerformance.SPReport
{
    partial class Frm_SPCompareResult
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btn_OK = new System.Windows.Forms.Button();
            this.gb_Result = new System.Windows.Forms.GroupBox();
            this.dgv_Result = new System.Windows.Forms.DataGridView();
            this.txt_MName = new System.Windows.Forms.TextBox();
            this.txt_MId = new System.Windows.Forms.TextBox();
            this.lb_M = new System.Windows.Forms.Label();
            this.txt_ET = new System.Windows.Forms.TextBox();
            this.lb_ET = new System.Windows.Forms.Label();
            this.txt_EN = new System.Windows.Forms.TextBox();
            this.lb_EN = new System.Windows.Forms.Label();
            this.txt_SName = new System.Windows.Forms.TextBox();
            this.txt_SId = new System.Windows.Forms.TextBox();
            this.lb_S = new System.Windows.Forms.Label();
            this.txt_POName = new System.Windows.Forms.TextBox();
            this.txt_POId = new System.Windows.Forms.TextBox();
            this.lb_PO = new System.Windows.Forms.Label();
            this.dtProject = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dtSupplier = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dtMaterial = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gb_Result.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_Result)).BeginInit();
            this.SuspendLayout();
            // 
            // btn_OK
            // 
            this.btn_OK.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn_OK.Location = new System.Drawing.Point(28, 45);
            this.btn_OK.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn_OK.Name = "btn_OK";
            this.btn_OK.Size = new System.Drawing.Size(70, 27);
            this.btn_OK.TabIndex = 29;
            this.btn_OK.Text = "确定";
            this.btn_OK.UseVisualStyleBackColor = true;
            this.btn_OK.Click += new System.EventHandler(this.btn_OK_Click);
            // 
            // gb_Result
            // 
            this.gb_Result.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gb_Result.Controls.Add(this.dgv_Result);
            this.gb_Result.Controls.Add(this.txt_MName);
            this.gb_Result.Controls.Add(this.txt_MId);
            this.gb_Result.Controls.Add(this.lb_M);
            this.gb_Result.Controls.Add(this.txt_ET);
            this.gb_Result.Controls.Add(this.lb_ET);
            this.gb_Result.Controls.Add(this.txt_EN);
            this.gb_Result.Controls.Add(this.lb_EN);
            this.gb_Result.Controls.Add(this.txt_SName);
            this.gb_Result.Controls.Add(this.txt_SId);
            this.gb_Result.Controls.Add(this.lb_S);
            this.gb_Result.Controls.Add(this.txt_POName);
            this.gb_Result.Controls.Add(this.txt_POId);
            this.gb_Result.Controls.Add(this.lb_PO);
            this.gb_Result.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.gb_Result.Location = new System.Drawing.Point(28, 100);
            this.gb_Result.Margin = new System.Windows.Forms.Padding(2);
            this.gb_Result.Name = "gb_Result";
            this.gb_Result.Padding = new System.Windows.Forms.Padding(2);
            this.gb_Result.Size = new System.Drawing.Size(956, 565);
            this.gb_Result.TabIndex = 30;
            this.gb_Result.TabStop = false;
            this.gb_Result.Text = "供应商绩效比较结果";
            // 
            // dgv_Result
            // 
            this.dgv_Result.AllowUserToAddRows = false;
            this.dgv_Result.AllowUserToDeleteRows = false;
            this.dgv_Result.AllowUserToResizeColumns = false;
            this.dgv_Result.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv_Result.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv_Result.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.dgv_Result.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            this.dgv_Result.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgv_Result.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.ControlLight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_Result.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgv_Result.ColumnHeadersHeight = 25;
            this.dgv_Result.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgv_Result.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dtProject,
            this.dtSupplier,
            this.dtMaterial});
            this.dgv_Result.EnableHeadersVisualStyles = false;
            this.dgv_Result.Location = new System.Drawing.Point(458, 25);
            this.dgv_Result.Margin = new System.Windows.Forms.Padding(2);
            this.dgv_Result.Name = "dgv_Result";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.GradientActiveCaption;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_Result.RowHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.dgv_Result.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.Black;
            this.dgv_Result.RowsDefaultCellStyle = dataGridViewCellStyle6;
            this.dgv_Result.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.Black;
            this.dgv_Result.RowTemplate.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.Black;
            this.dgv_Result.RowTemplate.Height = 27;
            this.dgv_Result.RowTemplate.ReadOnly = true;
            this.dgv_Result.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv_Result.Size = new System.Drawing.Size(483, 532);
            this.dgv_Result.TabIndex = 13;
            this.dgv_Result.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.dgv_Result_RowPostPaint);
            // 
            // txt_MName
            // 
            this.txt_MName.Location = new System.Drawing.Point(80, 256);
            this.txt_MName.Margin = new System.Windows.Forms.Padding(2);
            this.txt_MName.MaxLength = 20;
            this.txt_MName.Name = "txt_MName";
            this.txt_MName.ReadOnly = true;
            this.txt_MName.Size = new System.Drawing.Size(265, 23);
            this.txt_MName.TabIndex = 12;
            // 
            // txt_MId
            // 
            this.txt_MId.Location = new System.Drawing.Point(80, 221);
            this.txt_MId.Margin = new System.Windows.Forms.Padding(2);
            this.txt_MId.MaxLength = 20;
            this.txt_MId.Name = "txt_MId";
            this.txt_MId.ReadOnly = true;
            this.txt_MId.Size = new System.Drawing.Size(144, 23);
            this.txt_MId.TabIndex = 11;
            // 
            // lb_M
            // 
            this.lb_M.AutoSize = true;
            this.lb_M.Location = new System.Drawing.Point(12, 224);
            this.lb_M.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lb_M.Name = "lb_M";
            this.lb_M.Size = new System.Drawing.Size(44, 17);
            this.lb_M.TabIndex = 10;
            this.lb_M.Text = "物料：";
            // 
            // txt_ET
            // 
            this.txt_ET.Location = new System.Drawing.Point(78, 374);
            this.txt_ET.Margin = new System.Windows.Forms.Padding(2);
            this.txt_ET.MaxLength = 20;
            this.txt_ET.Name = "txt_ET";
            this.txt_ET.ReadOnly = true;
            this.txt_ET.Size = new System.Drawing.Size(267, 23);
            this.txt_ET.TabIndex = 9;
            // 
            // lb_ET
            // 
            this.lb_ET.AutoSize = true;
            this.lb_ET.Location = new System.Drawing.Point(12, 376);
            this.lb_ET.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lb_ET.Name = "lb_ET";
            this.lb_ET.Size = new System.Drawing.Size(68, 17);
            this.lb_ET.TabIndex = 8;
            this.lb_ET.Text = "评估时间：";
            // 
            // txt_EN
            // 
            this.txt_EN.Location = new System.Drawing.Point(80, 314);
            this.txt_EN.Margin = new System.Windows.Forms.Padding(2);
            this.txt_EN.MaxLength = 8;
            this.txt_EN.Name = "txt_EN";
            this.txt_EN.ReadOnly = true;
            this.txt_EN.Size = new System.Drawing.Size(92, 23);
            this.txt_EN.TabIndex = 7;
            // 
            // lb_EN
            // 
            this.lb_EN.AutoSize = true;
            this.lb_EN.Location = new System.Drawing.Point(12, 316);
            this.lb_EN.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lb_EN.Name = "lb_EN";
            this.lb_EN.Size = new System.Drawing.Size(56, 17);
            this.lb_EN.TabIndex = 6;
            this.lb_EN.Text = "评估者：";
            // 
            // txt_SName
            // 
            this.txt_SName.Location = new System.Drawing.Point(80, 166);
            this.txt_SName.Margin = new System.Windows.Forms.Padding(2);
            this.txt_SName.MaxLength = 20;
            this.txt_SName.Name = "txt_SName";
            this.txt_SName.ReadOnly = true;
            this.txt_SName.Size = new System.Drawing.Size(265, 23);
            this.txt_SName.TabIndex = 5;
            // 
            // txt_SId
            // 
            this.txt_SId.Location = new System.Drawing.Point(80, 131);
            this.txt_SId.Margin = new System.Windows.Forms.Padding(2);
            this.txt_SId.MaxLength = 20;
            this.txt_SId.Name = "txt_SId";
            this.txt_SId.ReadOnly = true;
            this.txt_SId.Size = new System.Drawing.Size(144, 23);
            this.txt_SId.TabIndex = 4;
            // 
            // lb_S
            // 
            this.lb_S.AutoSize = true;
            this.lb_S.Location = new System.Drawing.Point(12, 133);
            this.lb_S.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lb_S.Name = "lb_S";
            this.lb_S.Size = new System.Drawing.Size(56, 17);
            this.lb_S.TabIndex = 3;
            this.lb_S.Text = "供应商：";
            // 
            // txt_POName
            // 
            this.txt_POName.Location = new System.Drawing.Point(80, 77);
            this.txt_POName.Margin = new System.Windows.Forms.Padding(2);
            this.txt_POName.MaxLength = 20;
            this.txt_POName.Name = "txt_POName";
            this.txt_POName.ReadOnly = true;
            this.txt_POName.Size = new System.Drawing.Size(265, 23);
            this.txt_POName.TabIndex = 2;
            // 
            // txt_POId
            // 
            this.txt_POId.Location = new System.Drawing.Point(80, 43);
            this.txt_POId.Margin = new System.Windows.Forms.Padding(2);
            this.txt_POId.MaxLength = 20;
            this.txt_POId.Name = "txt_POId";
            this.txt_POId.ReadOnly = true;
            this.txt_POId.Size = new System.Drawing.Size(144, 23);
            this.txt_POId.TabIndex = 1;
            // 
            // lb_PO
            // 
            this.lb_PO.AutoSize = true;
            this.lb_PO.Location = new System.Drawing.Point(12, 45);
            this.lb_PO.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lb_PO.Name = "lb_PO";
            this.lb_PO.Size = new System.Drawing.Size(68, 17);
            this.lb_PO.TabIndex = 0;
            this.lb_PO.Text = "采购组织：";
            // 
            // dtProject
            // 
            this.dtProject.DataPropertyName = "Project";
            this.dtProject.HeaderText = "项目";
            this.dtProject.Name = "dtProject";
            this.dtProject.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dtProject.Width = 120;
            // 
            // dtSupplier
            // 
            this.dtSupplier.DataPropertyName = "SupplierScore";
            dataGridViewCellStyle3.Format = "0.00";
            dataGridViewCellStyle3.NullValue = null;
            this.dtSupplier.DefaultCellStyle = dataGridViewCellStyle3;
            this.dtSupplier.HeaderText = "供应商总体绩效";
            this.dtSupplier.Name = "dtSupplier";
            this.dtSupplier.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dtSupplier.Width = 155;
            // 
            // dtMaterial
            // 
            this.dtMaterial.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dtMaterial.DataPropertyName = "MaterialScore";
            dataGridViewCellStyle4.Format = "0.00";
            dataGridViewCellStyle4.NullValue = null;
            this.dtMaterial.DefaultCellStyle = dataGridViewCellStyle4;
            this.dtMaterial.HeaderText = "选定物料得分";
            this.dtMaterial.Name = "dtMaterial";
            this.dtMaterial.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Frm_SPCompareResult
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1006, 676);
            this.Controls.Add(this.gb_Result);
            this.Controls.Add(this.btn_OK);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Frm_SPCompareResult";
            this.Text = "供应商绩效比较结果";
            this.Load += new System.EventHandler(this.Frm_SPCompareResult_Load);
            this.gb_Result.ResumeLayout(false);
            this.gb_Result.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_Result)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn_OK;
        private System.Windows.Forms.GroupBox gb_Result;
        private System.Windows.Forms.TextBox txt_ET;
        private System.Windows.Forms.Label lb_ET;
        private System.Windows.Forms.TextBox txt_EN;
        private System.Windows.Forms.Label lb_EN;
        private System.Windows.Forms.TextBox txt_SName;
        private System.Windows.Forms.TextBox txt_SId;
        private System.Windows.Forms.Label lb_S;
        private System.Windows.Forms.TextBox txt_POName;
        private System.Windows.Forms.TextBox txt_POId;
        private System.Windows.Forms.Label lb_PO;
        private System.Windows.Forms.TextBox txt_MName;
        private System.Windows.Forms.TextBox txt_MId;
        private System.Windows.Forms.Label lb_M;
        private System.Windows.Forms.DataGridView dgv_Result;
        private System.Windows.Forms.DataGridViewTextBoxColumn dtProject;
        private System.Windows.Forms.DataGridViewTextBoxColumn dtSupplier;
        private System.Windows.Forms.DataGridViewTextBoxColumn dtMaterial;
    }
}