﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Lib.ContionSettings.SupplierPerformaceCS.SPReportCS;
using Lib.Bll.SupplierPerformaceBLL;

namespace MMClient.SupplierPerformance.SPReport
{
    public partial class Frm_SPCompareResult : WeifenLuo.WinFormsUI.Docking.DockContent
    {

        #region 公共变量
        /// <summary>
        /// 供应商比较业务层
        /// </summary>
        private SPR_SPCompareBLL sPR_SPCompareBLL = null;

        /// <summary>
        /// 查询评分所用条件
        /// </summary>
        private SPCompareConditionValue sPCompareConditionValue = null;

        #endregion

        #region 窗体构造函数

        public Frm_SPCompareResult()
        {
            InitializeComponent();
        }

        public Frm_SPCompareResult(SPCompareConditionValue sPCompareConditionValue)
            :this()
        {
            this.sPCompareConditionValue = sPCompareConditionValue;
            this.dgv_Result.TopLeftHeaderCell.Value = "序号";
        }

        #endregion

        #region 窗体事件函数
        /// <summary>
        /// 按钮->确定
        /// 此处是关闭窗口作用
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_OK_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// 窗体载入实触发
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Frm_SPCompareResult_Load(object sender, EventArgs e)
        {
            this.initFormInfo(this.sPCompareConditionValue);
        }

        /// <summary>
        /// 添加序号
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgv_Result_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            var dgv = sender as DataGridView;
            if (dgv != null)
            {
                Rectangle rect = new Rectangle(e.RowBounds.Location.X, e.RowBounds.Location.Y, (dgv.RowHeadersWidth + 16) / 2, e.RowBounds.Height);
                TextRenderer.DrawText(e.Graphics, (e.RowIndex + 1).ToString(), dgv.RowHeadersDefaultCellStyle.Font, rect, dgv.RowHeadersDefaultCellStyle.ForeColor, TextFormatFlags.VerticalCenter | TextFormatFlags.Right);
            }
        }

        #endregion

        #region 公共操作函数

        /// <summary>
        /// 初始化窗体信息
        /// </summary>
        /// <param name="sPCompareValue">上一个界面的条件值</param>
        private void initFormInfo(SPCompareConditionValue sPCompareConditionValue)
        {
            this.txt_POId.Text = sPCompareConditionValue.PurchaseId;
            this.txt_POName.Text = sPCompareConditionValue.PurchaseName;
            this.txt_SId.Text = sPCompareConditionValue.SupplierId;
            this.txt_SName.Text = sPCompareConditionValue.SupplierName;
            this.txt_MId.Text = sPCompareConditionValue.MaterialId;
            this.txt_MName.Text = sPCompareConditionValue.MaterialName;

            //加载评分数据
            this.bindCompareResult(sPCompareConditionValue);
        }

        /// <summary>
        /// 绑定采购组织信息
        /// </summary>
        private void bindCompareResult(SPCompareConditionValue sPCompareConditionValue)
        {
            if (sPR_SPCompareBLL == null)
            {
                sPR_SPCompareBLL = new SPR_SPCompareBLL();
            }

            //获取评估人和评估时间
            string[] evaluationCreatorAndTime = sPR_SPCompareBLL.getEvaluationCreatorAndTime(sPCompareConditionValue);
            this.txt_EN.Text = evaluationCreatorAndTime[0];
            this.txt_ET.Text = evaluationCreatorAndTime[1];

            //查询数据库，得到采购组织信息的DataTable
            DataTable dtResult = sPR_SPCompareBLL.getSupplierMaterialScoreCompareResult(sPCompareConditionValue);
            this.dgv_Result.DataSource = dtResult;
        }

        #endregion 

    }
}
