﻿namespace MMClient.SupplierPerformance.SPReport
{
    partial class Frm_SPCompare
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_OK = new System.Windows.Forms.Button();
            this.btn_Close = new System.Windows.Forms.Button();
            this.btn_Clear = new System.Windows.Forms.Button();
            this.gb_ConditionSettings = new System.Windows.Forms.GroupBox();
            this.btn_SelectMaterial = new System.Windows.Forms.Button();
            this.btn_SelectSupplier = new System.Windows.Forms.Button();
            this.btn_SelectPurchaseGroup = new System.Windows.Forms.Button();
            this.txt_MaterialId = new System.Windows.Forms.TextBox();
            this.txt_MaterialName = new System.Windows.Forms.TextBox();
            this.lb_Material = new System.Windows.Forms.Label();
            this.cbb_TimeInterval = new System.Windows.Forms.ComboBox();
            this.cbb_Year = new System.Windows.Forms.ComboBox();
            this.lb_TimeInterval = new System.Windows.Forms.Label();
            this.lb_Year = new System.Windows.Forms.Label();
            this.lb_EvaluationPeriod = new System.Windows.Forms.Label();
            this.txt_SupplierId = new System.Windows.Forms.TextBox();
            this.txt_SupplierName = new System.Windows.Forms.TextBox();
            this.txt_PurchaseGroupId = new System.Windows.Forms.TextBox();
            this.txt_PurchaseGroupName = new System.Windows.Forms.TextBox();
            this.lb_PurchaseGroup = new System.Windows.Forms.Label();
            this.lb_Supplier = new System.Windows.Forms.Label();
            this.gb_ConditionSettings.SuspendLayout();
            this.SuspendLayout();
            // 
            // btn_OK
            // 
            this.btn_OK.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn_OK.Location = new System.Drawing.Point(28, 45);
            this.btn_OK.Name = "btn_OK";
            this.btn_OK.Size = new System.Drawing.Size(70, 27);
            this.btn_OK.TabIndex = 28;
            this.btn_OK.Text = "确定";
            this.btn_OK.UseVisualStyleBackColor = true;
            this.btn_OK.Click += new System.EventHandler(this.btn_OK_Click);
            // 
            // btn_Close
            // 
            this.btn_Close.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn_Close.Location = new System.Drawing.Point(203, 45);
            this.btn_Close.Name = "btn_Close";
            this.btn_Close.Size = new System.Drawing.Size(70, 27);
            this.btn_Close.TabIndex = 29;
            this.btn_Close.Text = "关闭";
            this.btn_Close.UseVisualStyleBackColor = true;
            this.btn_Close.Click += new System.EventHandler(this.btn_Close_Click);
            // 
            // btn_Clear
            // 
            this.btn_Clear.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn_Clear.Location = new System.Drawing.Point(116, 45);
            this.btn_Clear.Name = "btn_Clear";
            this.btn_Clear.Size = new System.Drawing.Size(70, 27);
            this.btn_Clear.TabIndex = 30;
            this.btn_Clear.Text = "清除";
            this.btn_Clear.UseVisualStyleBackColor = true;
            this.btn_Clear.Click += new System.EventHandler(this.btn_Clear_Click);
            // 
            // gb_ConditionSettings
            // 
            this.gb_ConditionSettings.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gb_ConditionSettings.Controls.Add(this.btn_SelectMaterial);
            this.gb_ConditionSettings.Controls.Add(this.btn_SelectSupplier);
            this.gb_ConditionSettings.Controls.Add(this.btn_SelectPurchaseGroup);
            this.gb_ConditionSettings.Controls.Add(this.txt_MaterialId);
            this.gb_ConditionSettings.Controls.Add(this.txt_MaterialName);
            this.gb_ConditionSettings.Controls.Add(this.lb_Material);
            this.gb_ConditionSettings.Controls.Add(this.cbb_TimeInterval);
            this.gb_ConditionSettings.Controls.Add(this.cbb_Year);
            this.gb_ConditionSettings.Controls.Add(this.lb_TimeInterval);
            this.gb_ConditionSettings.Controls.Add(this.lb_Year);
            this.gb_ConditionSettings.Controls.Add(this.lb_EvaluationPeriod);
            this.gb_ConditionSettings.Controls.Add(this.txt_SupplierId);
            this.gb_ConditionSettings.Controls.Add(this.txt_SupplierName);
            this.gb_ConditionSettings.Controls.Add(this.txt_PurchaseGroupId);
            this.gb_ConditionSettings.Controls.Add(this.txt_PurchaseGroupName);
            this.gb_ConditionSettings.Controls.Add(this.lb_PurchaseGroup);
            this.gb_ConditionSettings.Controls.Add(this.lb_Supplier);
            this.gb_ConditionSettings.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.gb_ConditionSettings.Location = new System.Drawing.Point(28, 105);
            this.gb_ConditionSettings.Name = "gb_ConditionSettings";
            this.gb_ConditionSettings.Size = new System.Drawing.Size(597, 295);
            this.gb_ConditionSettings.TabIndex = 31;
            this.gb_ConditionSettings.TabStop = false;
            this.gb_ConditionSettings.Text = "条件设置";
            // 
            // btn_SelectMaterial
            // 
            this.btn_SelectMaterial.BackColor = System.Drawing.Color.Transparent;
            this.btn_SelectMaterial.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_SelectMaterial.FlatAppearance.BorderColor = System.Drawing.SystemColors.Control;
            this.btn_SelectMaterial.FlatAppearance.BorderSize = 0;
            this.btn_SelectMaterial.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btn_SelectMaterial.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btn_SelectMaterial.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_SelectMaterial.Image = global::MMClient.Properties.Resources.search;
            this.btn_SelectMaterial.Location = new System.Drawing.Point(501, 228);
            this.btn_SelectMaterial.Name = "btn_SelectMaterial";
            this.btn_SelectMaterial.Size = new System.Drawing.Size(19, 22);
            this.btn_SelectMaterial.TabIndex = 16;
            this.btn_SelectMaterial.UseVisualStyleBackColor = false;
            this.btn_SelectMaterial.Click += new System.EventHandler(this.btn_SelectMaterial_Click);
            // 
            // btn_SelectSupplier
            // 
            this.btn_SelectSupplier.BackColor = System.Drawing.Color.Transparent;
            this.btn_SelectSupplier.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_SelectSupplier.FlatAppearance.BorderColor = System.Drawing.SystemColors.Control;
            this.btn_SelectSupplier.FlatAppearance.BorderSize = 0;
            this.btn_SelectSupplier.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btn_SelectSupplier.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btn_SelectSupplier.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_SelectSupplier.Image = global::MMClient.Properties.Resources.search;
            this.btn_SelectSupplier.Location = new System.Drawing.Point(501, 171);
            this.btn_SelectSupplier.Name = "btn_SelectSupplier";
            this.btn_SelectSupplier.Size = new System.Drawing.Size(19, 22);
            this.btn_SelectSupplier.TabIndex = 15;
            this.btn_SelectSupplier.UseVisualStyleBackColor = false;
            this.btn_SelectSupplier.Click += new System.EventHandler(this.btn_SelectSupplier_Click);
            // 
            // btn_SelectPurchaseGroup
            // 
            this.btn_SelectPurchaseGroup.BackColor = System.Drawing.Color.Transparent;
            this.btn_SelectPurchaseGroup.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_SelectPurchaseGroup.FlatAppearance.BorderColor = System.Drawing.SystemColors.Control;
            this.btn_SelectPurchaseGroup.FlatAppearance.BorderSize = 0;
            this.btn_SelectPurchaseGroup.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btn_SelectPurchaseGroup.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btn_SelectPurchaseGroup.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_SelectPurchaseGroup.Image = global::MMClient.Properties.Resources.search;
            this.btn_SelectPurchaseGroup.Location = new System.Drawing.Point(501, 35);
            this.btn_SelectPurchaseGroup.Name = "btn_SelectPurchaseGroup";
            this.btn_SelectPurchaseGroup.Size = new System.Drawing.Size(19, 22);
            this.btn_SelectPurchaseGroup.TabIndex = 14;
            this.btn_SelectPurchaseGroup.UseVisualStyleBackColor = false;
            this.btn_SelectPurchaseGroup.Click += new System.EventHandler(this.btn_SelectPurchaseGroup_Click);
            // 
            // txt_MaterialId
            // 
            this.txt_MaterialId.Location = new System.Drawing.Point(357, 230);
            this.txt_MaterialId.MaxLength = 20;
            this.txt_MaterialId.Name = "txt_MaterialId";
            this.txt_MaterialId.ReadOnly = true;
            this.txt_MaterialId.Size = new System.Drawing.Size(144, 23);
            this.txt_MaterialId.TabIndex = 13;
            // 
            // txt_MaterialName
            // 
            this.txt_MaterialName.Location = new System.Drawing.Point(103, 230);
            this.txt_MaterialName.MaxLength = 20;
            this.txt_MaterialName.Name = "txt_MaterialName";
            this.txt_MaterialName.ReadOnly = true;
            this.txt_MaterialName.Size = new System.Drawing.Size(248, 23);
            this.txt_MaterialName.TabIndex = 12;
            // 
            // lb_Material
            // 
            this.lb_Material.AutoSize = true;
            this.lb_Material.Location = new System.Drawing.Point(9, 235);
            this.lb_Material.Name = "lb_Material";
            this.lb_Material.Size = new System.Drawing.Size(32, 17);
            this.lb_Material.TabIndex = 11;
            this.lb_Material.Text = "物料";
            // 
            // cbb_TimeInterval
            // 
            this.cbb_TimeInterval.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbb_TimeInterval.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cbb_TimeInterval.FormattingEnabled = true;
            this.cbb_TimeInterval.Location = new System.Drawing.Point(192, 117);
            this.cbb_TimeInterval.Name = "cbb_TimeInterval";
            this.cbb_TimeInterval.Size = new System.Drawing.Size(159, 25);
            this.cbb_TimeInterval.TabIndex = 10;
            this.cbb_TimeInterval.SelectedIndexChanged += new System.EventHandler(this.cbb_TimeInterval_SelectedIndexChanged);
            // 
            // cbb_Year
            // 
            this.cbb_Year.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbb_Year.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cbb_Year.FormattingEnabled = true;
            this.cbb_Year.Items.AddRange(new object[] {
            "",
            "2016",
            "2017",
            "2018",
            "2019",
            "2020",
            "2021",
            "2022",
            "2023",
            "2024"});
            this.cbb_Year.Location = new System.Drawing.Point(103, 117);
            this.cbb_Year.Name = "cbb_Year";
            this.cbb_Year.Size = new System.Drawing.Size(83, 25);
            this.cbb_Year.TabIndex = 9;
            this.cbb_Year.SelectedIndexChanged += new System.EventHandler(this.cbb_Year_SelectedIndexChanged);
            // 
            // lb_TimeInterval
            // 
            this.lb_TimeInterval.AutoSize = true;
            this.lb_TimeInterval.Location = new System.Drawing.Point(190, 95);
            this.lb_TimeInterval.Name = "lb_TimeInterval";
            this.lb_TimeInterval.Size = new System.Drawing.Size(32, 17);
            this.lb_TimeInterval.TabIndex = 8;
            this.lb_TimeInterval.Text = "时段";
            // 
            // lb_Year
            // 
            this.lb_Year.AutoSize = true;
            this.lb_Year.Location = new System.Drawing.Point(103, 95);
            this.lb_Year.Name = "lb_Year";
            this.lb_Year.Size = new System.Drawing.Size(32, 17);
            this.lb_Year.TabIndex = 7;
            this.lb_Year.Text = "年度";
            // 
            // lb_EvaluationPeriod
            // 
            this.lb_EvaluationPeriod.AutoSize = true;
            this.lb_EvaluationPeriod.Location = new System.Drawing.Point(9, 95);
            this.lb_EvaluationPeriod.Name = "lb_EvaluationPeriod";
            this.lb_EvaluationPeriod.Size = new System.Drawing.Size(56, 17);
            this.lb_EvaluationPeriod.TabIndex = 6;
            this.lb_EvaluationPeriod.Text = "评估周期";
            // 
            // txt_SupplierId
            // 
            this.txt_SupplierId.Location = new System.Drawing.Point(357, 173);
            this.txt_SupplierId.MaxLength = 20;
            this.txt_SupplierId.Name = "txt_SupplierId";
            this.txt_SupplierId.ReadOnly = true;
            this.txt_SupplierId.Size = new System.Drawing.Size(144, 23);
            this.txt_SupplierId.TabIndex = 5;
            // 
            // txt_SupplierName
            // 
            this.txt_SupplierName.Location = new System.Drawing.Point(103, 173);
            this.txt_SupplierName.MaxLength = 20;
            this.txt_SupplierName.Name = "txt_SupplierName";
            this.txt_SupplierName.ReadOnly = true;
            this.txt_SupplierName.Size = new System.Drawing.Size(248, 23);
            this.txt_SupplierName.TabIndex = 4;
            // 
            // txt_PurchaseGroupId
            // 
            this.txt_PurchaseGroupId.Location = new System.Drawing.Point(357, 37);
            this.txt_PurchaseGroupId.MaxLength = 20;
            this.txt_PurchaseGroupId.Name = "txt_PurchaseGroupId";
            this.txt_PurchaseGroupId.Size = new System.Drawing.Size(144, 23);
            this.txt_PurchaseGroupId.TabIndex = 3;
            this.txt_PurchaseGroupId.TextChanged += new System.EventHandler(this.txt_PurchaseGroupId_TextChanged);
            // 
            // txt_PurchaseGroupName
            // 
            this.txt_PurchaseGroupName.Location = new System.Drawing.Point(103, 37);
            this.txt_PurchaseGroupName.MaxLength = 20;
            this.txt_PurchaseGroupName.Name = "txt_PurchaseGroupName";
            this.txt_PurchaseGroupName.Size = new System.Drawing.Size(248, 23);
            this.txt_PurchaseGroupName.TabIndex = 2;
            // 
            // lb_PurchaseGroup
            // 
            this.lb_PurchaseGroup.AutoSize = true;
            this.lb_PurchaseGroup.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lb_PurchaseGroup.Location = new System.Drawing.Point(9, 42);
            this.lb_PurchaseGroup.Name = "lb_PurchaseGroup";
            this.lb_PurchaseGroup.Size = new System.Drawing.Size(56, 17);
            this.lb_PurchaseGroup.TabIndex = 1;
            this.lb_PurchaseGroup.Text = "采购组织";
            // 
            // lb_Supplier
            // 
            this.lb_Supplier.AutoSize = true;
            this.lb_Supplier.Location = new System.Drawing.Point(9, 178);
            this.lb_Supplier.Name = "lb_Supplier";
            this.lb_Supplier.Size = new System.Drawing.Size(44, 17);
            this.lb_Supplier.TabIndex = 0;
            this.lb_Supplier.Text = "供应商";
            // 
            // Frm_SPCompare
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(656, 583);
            this.Controls.Add(this.gb_ConditionSettings);
            this.Controls.Add(this.btn_Clear);
            this.Controls.Add(this.btn_Close);
            this.Controls.Add(this.btn_OK);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Frm_SPCompare";
            this.Text = "供应商绩效比较";
            this.gb_ConditionSettings.ResumeLayout(false);
            this.gb_ConditionSettings.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn_OK;
        private System.Windows.Forms.Button btn_Close;
        private System.Windows.Forms.Button btn_Clear;
        private System.Windows.Forms.GroupBox gb_ConditionSettings;
        private System.Windows.Forms.Label lb_Supplier;
        private System.Windows.Forms.Label lb_PurchaseGroup;
        private System.Windows.Forms.TextBox txt_PurchaseGroupId;
        private System.Windows.Forms.TextBox txt_PurchaseGroupName;
        private System.Windows.Forms.TextBox txt_SupplierId;
        private System.Windows.Forms.TextBox txt_SupplierName;
        private System.Windows.Forms.Label lb_EvaluationPeriod;
        private System.Windows.Forms.Label lb_TimeInterval;
        private System.Windows.Forms.Label lb_Year;
        private System.Windows.Forms.ComboBox cbb_TimeInterval;
        private System.Windows.Forms.ComboBox cbb_Year;
        private System.Windows.Forms.TextBox txt_MaterialId;
        private System.Windows.Forms.TextBox txt_MaterialName;
        private System.Windows.Forms.Label lb_Material;
        private System.Windows.Forms.Button btn_SelectPurchaseGroup;
        private System.Windows.Forms.Button btn_SelectMaterial;
        private System.Windows.Forms.Button btn_SelectSupplier;
    }
}