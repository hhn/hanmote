﻿namespace MMClient.SupplierPerformance.SPReport
{
    partial class Frm_SPCompareBOIResult
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            this.gb_Result = new System.Windows.Forms.GroupBox();
            this.dgv_Result = new System.Windows.Forms.DataGridView();
            this.dtSupplierId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dtSupplierName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dtCountry = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dtIndustry = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dtPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dtDelivery = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dtQuality = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dtService = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dtOffset = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txt_POName = new System.Windows.Forms.TextBox();
            this.txt_POId = new System.Windows.Forms.TextBox();
            this.lb_PO = new System.Windows.Forms.Label();
            this.btn_OK = new System.Windows.Forms.Button();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.gb_Result.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_Result)).BeginInit();
            this.SuspendLayout();
            // 
            // gb_Result
            // 
            this.gb_Result.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gb_Result.Controls.Add(this.dgv_Result);
            this.gb_Result.Controls.Add(this.txt_POName);
            this.gb_Result.Controls.Add(this.txt_POId);
            this.gb_Result.Controls.Add(this.lb_PO);
            this.gb_Result.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.gb_Result.Location = new System.Drawing.Point(25, 84);
            this.gb_Result.Margin = new System.Windows.Forms.Padding(2);
            this.gb_Result.Name = "gb_Result";
            this.gb_Result.Padding = new System.Windows.Forms.Padding(2);
            this.gb_Result.Size = new System.Drawing.Size(972, 508);
            this.gb_Result.TabIndex = 34;
            this.gb_Result.TabStop = false;
            this.gb_Result.Text = "基于行业的供应商评估排列清单";
            // 
            // dgv_Result
            // 
            this.dgv_Result.AllowUserToAddRows = false;
            this.dgv_Result.AllowUserToDeleteRows = false;
            this.dgv_Result.AllowUserToResizeColumns = false;
            this.dgv_Result.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv_Result.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv_Result.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.dgv_Result.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            this.dgv_Result.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgv_Result.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.ControlLight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_Result.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgv_Result.ColumnHeadersHeight = 25;
            this.dgv_Result.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgv_Result.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dtSupplierId,
            this.dtSupplierName,
            this.dtCountry,
            this.dtIndustry,
            this.dtPrice,
            this.dtDelivery,
            this.dtQuality,
            this.dtService,
            this.dtOffset});
            this.dgv_Result.EnableHeadersVisualStyles = false;
            this.dgv_Result.Location = new System.Drawing.Point(12, 63);
            this.dgv_Result.Margin = new System.Windows.Forms.Padding(2);
            this.dgv_Result.Name = "dgv_Result";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.GradientActiveCaption;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_Result.RowHeadersDefaultCellStyle = dataGridViewCellStyle8;
            this.dgv_Result.RowHeadersWidth = 46;
            this.dgv_Result.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle9.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.Color.Black;
            this.dgv_Result.RowsDefaultCellStyle = dataGridViewCellStyle9;
            this.dgv_Result.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.Black;
            this.dgv_Result.RowTemplate.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.Black;
            this.dgv_Result.RowTemplate.Height = 27;
            this.dgv_Result.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv_Result.Size = new System.Drawing.Size(941, 440);
            this.dgv_Result.TabIndex = 3;
            this.dgv_Result.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.dgv_Result_RowPostPaint);
            // 
            // dtSupplierId
            // 
            this.dtSupplierId.DataPropertyName = "Supplier_ID";
            this.dtSupplierId.HeaderText = "供应商编号";
            this.dtSupplierId.Name = "dtSupplierId";
            this.dtSupplierId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dtSupplierId.Width = 120;
            // 
            // dtSupplierName
            // 
            this.dtSupplierName.DataPropertyName = "Supplier_Name";
            this.dtSupplierName.HeaderText = "供应商名称";
            this.dtSupplierName.Name = "dtSupplierName";
            this.dtSupplierName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dtSupplierName.Width = 270;
            // 
            // dtCountry
            // 
            this.dtCountry.DataPropertyName = "Nation";
            this.dtCountry.HeaderText = "国家";
            this.dtCountry.Name = "dtCountry";
            this.dtCountry.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dtCountry.Width = 70;
            // 
            // dtIndustry
            // 
            this.dtIndustry.DataPropertyName = "SupplierIndustry_Name";
            this.dtIndustry.FillWeight = 80F;
            this.dtIndustry.HeaderText = "行业";
            this.dtIndustry.Name = "dtIndustry";
            this.dtIndustry.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dtPrice
            // 
            this.dtPrice.DataPropertyName = "Price_Score";
            dataGridViewCellStyle3.Format = "0.00";
            dataGridViewCellStyle3.NullValue = null;
            this.dtPrice.DefaultCellStyle = dataGridViewCellStyle3;
            this.dtPrice.HeaderText = "价格";
            this.dtPrice.Name = "dtPrice";
            this.dtPrice.Width = 60;
            // 
            // dtDelivery
            // 
            this.dtDelivery.DataPropertyName = "Delivery_Score";
            dataGridViewCellStyle4.Format = "0.00";
            dataGridViewCellStyle4.NullValue = null;
            this.dtDelivery.DefaultCellStyle = dataGridViewCellStyle4;
            this.dtDelivery.HeaderText = "交货";
            this.dtDelivery.Name = "dtDelivery";
            this.dtDelivery.Width = 60;
            // 
            // dtQuality
            // 
            this.dtQuality.DataPropertyName = "Quality_Score";
            dataGridViewCellStyle5.Format = "0.00";
            dataGridViewCellStyle5.NullValue = null;
            this.dtQuality.DefaultCellStyle = dataGridViewCellStyle5;
            this.dtQuality.HeaderText = "质量";
            this.dtQuality.Name = "dtQuality";
            this.dtQuality.Width = 60;
            // 
            // dtService
            // 
            this.dtService.DataPropertyName = "GeneralServiceAndSupport_Score";
            dataGridViewCellStyle6.Format = "0.00";
            dataGridViewCellStyle6.NullValue = null;
            this.dtService.DefaultCellStyle = dataGridViewCellStyle6;
            this.dtService.HeaderText = "服务";
            this.dtService.Name = "dtService";
            this.dtService.Width = 60;
            // 
            // dtOffset
            // 
            this.dtOffset.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle7.Format = "0.00";
            dataGridViewCellStyle7.NullValue = null;
            this.dtOffset.DefaultCellStyle = dataGridViewCellStyle7;
            this.dtOffset.HeaderText = "平均偏差";
            this.dtOffset.Name = "dtOffset";
            // 
            // txt_POName
            // 
            this.txt_POName.Location = new System.Drawing.Point(212, 25);
            this.txt_POName.Margin = new System.Windows.Forms.Padding(2);
            this.txt_POName.MaxLength = 20;
            this.txt_POName.Name = "txt_POName";
            this.txt_POName.ReadOnly = true;
            this.txt_POName.Size = new System.Drawing.Size(222, 23);
            this.txt_POName.TabIndex = 2;
            // 
            // txt_POId
            // 
            this.txt_POId.Location = new System.Drawing.Point(77, 25);
            this.txt_POId.Margin = new System.Windows.Forms.Padding(2);
            this.txt_POId.MaxLength = 20;
            this.txt_POId.Name = "txt_POId";
            this.txt_POId.ReadOnly = true;
            this.txt_POId.Size = new System.Drawing.Size(132, 23);
            this.txt_POId.TabIndex = 1;
            // 
            // lb_PO
            // 
            this.lb_PO.AutoSize = true;
            this.lb_PO.Location = new System.Drawing.Point(9, 27);
            this.lb_PO.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lb_PO.Name = "lb_PO";
            this.lb_PO.Size = new System.Drawing.Size(68, 17);
            this.lb_PO.TabIndex = 0;
            this.lb_PO.Text = "采购组织：";
            // 
            // btn_OK
            // 
            this.btn_OK.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn_OK.Location = new System.Drawing.Point(28, 45);
            this.btn_OK.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn_OK.Name = "btn_OK";
            this.btn_OK.Size = new System.Drawing.Size(70, 27);
            this.btn_OK.TabIndex = 33;
            this.btn_OK.Text = "确定";
            this.btn_OK.UseVisualStyleBackColor = true;
            this.btn_OK.Click += new System.EventHandler(this.btn_OK_Click);
            // 
            // Frm_SPCompareBOIResult
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1006, 598);
            this.Controls.Add(this.gb_Result);
            this.Controls.Add(this.btn_OK);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Frm_SPCompareBOIResult";
            this.Text = "基于物料组的供应商评估结果";
            this.Load += new System.EventHandler(this.Frm_SPCompareBOIResult_Load);
            this.gb_Result.ResumeLayout(false);
            this.gb_Result.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_Result)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gb_Result;
        private System.Windows.Forms.DataGridView dgv_Result;
        private System.Windows.Forms.TextBox txt_POName;
        private System.Windows.Forms.TextBox txt_POId;
        private System.Windows.Forms.Label lb_PO;
        private System.Windows.Forms.Button btn_OK;
        private System.Windows.Forms.DataGridViewTextBoxColumn dtSupplierId;
        private System.Windows.Forms.DataGridViewTextBoxColumn dtSupplierName;
        private System.Windows.Forms.DataGridViewTextBoxColumn dtCountry;
        private System.Windows.Forms.DataGridViewTextBoxColumn dtIndustry;
        private System.Windows.Forms.DataGridViewTextBoxColumn dtPrice;
        private System.Windows.Forms.DataGridViewTextBoxColumn dtDelivery;
        private System.Windows.Forms.DataGridViewTextBoxColumn dtQuality;
        private System.Windows.Forms.DataGridViewTextBoxColumn dtService;
        private System.Windows.Forms.DataGridViewTextBoxColumn dtOffset;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
    }
}