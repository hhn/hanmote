﻿namespace MMClient.SupplierPerformance.SecletedEvaluatorForm
{
    partial class SecletedEvaluatorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.button3 = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.comboBox_purchasingORGID = new System.Windows.Forms.ComboBox();
            this.comboBox_supplierID = new System.Windows.Forms.ComboBox();
            this.lable_materialName = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBox_materialID = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.lable_purchasingORGName = new System.Windows.Forms.Label();
            this.lable_supplierName = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.勾选 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Supplier_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Supplier_Name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MaterialId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Supplier_AccountGroup = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Supplier_Class = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.button3);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Controls.Add(this.button2);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.comboBox_purchasingORGID);
            this.panel1.Controls.Add(this.comboBox_supplierID);
            this.panel1.Controls.Add(this.lable_materialName);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.comboBox_materialID);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label22);
            this.panel1.Controls.Add(this.lable_purchasingORGName);
            this.panel1.Controls.Add(this.lable_supplierName);
            this.panel1.Controls.Add(this.groupBox2);
            this.panel1.Location = new System.Drawing.Point(13, 13);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(830, 762);
            this.panel1.TabIndex = 0;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(34, 685);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 63;
            this.button3.Text = "选  择";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(19, 649);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 12);
            this.label3.TabIndex = 62;
            this.label3.Text = "选择评审员：";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.textBox1);
            this.groupBox1.Location = new System.Drawing.Point(19, 510);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(804, 106);
            this.groupBox1.TabIndex = 61;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "通知消息：";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(15, 21);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(783, 79);
            this.textBox1.TabIndex = 0;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(754, 74);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(72, 23);
            this.button2.TabIndex = 60;
            this.button2.Text = "重  置";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(637, 74);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(79, 23);
            this.button1.TabIndex = 59;
            this.button1.Text = "查  询";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // comboBox_purchasingORGID
            // 
            this.comboBox_purchasingORGID.FormattingEnabled = true;
            this.comboBox_purchasingORGID.Location = new System.Drawing.Point(572, 16);
            this.comboBox_purchasingORGID.Margin = new System.Windows.Forms.Padding(2);
            this.comboBox_purchasingORGID.Name = "comboBox_purchasingORGID";
            this.comboBox_purchasingORGID.Size = new System.Drawing.Size(144, 20);
            this.comboBox_purchasingORGID.TabIndex = 50;
            // 
            // comboBox_supplierID
            // 
            this.comboBox_supplierID.FormattingEnabled = true;
            this.comboBox_supplierID.Location = new System.Drawing.Point(68, 15);
            this.comboBox_supplierID.Margin = new System.Windows.Forms.Padding(2);
            this.comboBox_supplierID.Name = "comboBox_supplierID";
            this.comboBox_supplierID.Size = new System.Drawing.Size(144, 20);
            this.comboBox_supplierID.TabIndex = 51;
            this.comboBox_supplierID.SelectedIndexChanged += new System.EventHandler(this.comboBox_supplierID_SelectedIndexChanged);
            // 
            // lable_materialName
            // 
            this.lable_materialName.AutoSize = true;
            this.lable_materialName.Location = new System.Drawing.Point(336, 36);
            this.lable_materialName.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lable_materialName.Name = "lable_materialName";
            this.lable_materialName.Size = new System.Drawing.Size(53, 12);
            this.lable_materialName.TabIndex = 58;
            this.lable_materialName.Text = "物料名称";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(505, 20);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 12);
            this.label1.TabIndex = 52;
            this.label1.Text = "采购组织:";
            // 
            // comboBox_materialID
            // 
            this.comboBox_materialID.FormattingEnabled = true;
            this.comboBox_materialID.Location = new System.Drawing.Point(298, 15);
            this.comboBox_materialID.Margin = new System.Windows.Forms.Padding(2);
            this.comboBox_materialID.Name = "comboBox_materialID";
            this.comboBox_materialID.Size = new System.Drawing.Size(144, 20);
            this.comboBox_materialID.TabIndex = 57;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.Location = new System.Drawing.Point(17, 18);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 12);
            this.label2.TabIndex = 53;
            this.label2.Text = "供应商:";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(259, 19);
            this.label22.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(35, 12);
            this.label22.TabIndex = 56;
            this.label22.Text = "物料:";
            // 
            // lable_purchasingORGName
            // 
            this.lable_purchasingORGName.AutoSize = true;
            this.lable_purchasingORGName.Location = new System.Drawing.Point(608, 40);
            this.lable_purchasingORGName.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lable_purchasingORGName.Name = "lable_purchasingORGName";
            this.lable_purchasingORGName.Size = new System.Drawing.Size(77, 12);
            this.lable_purchasingORGName.TabIndex = 54;
            this.lable_purchasingORGName.Text = "采购组织名称";
            // 
            // lable_supplierName
            // 
            this.lable_supplierName.AutoSize = true;
            this.lable_supplierName.Location = new System.Drawing.Point(99, 36);
            this.lable_supplierName.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lable_supplierName.Name = "lable_supplierName";
            this.lable_supplierName.Size = new System.Drawing.Size(65, 12);
            this.lable_supplierName.TabIndex = 55;
            this.lable_supplierName.Text = "供应商名称";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dataGridView1);
            this.groupBox2.Location = new System.Drawing.Point(19, 102);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox2.Size = new System.Drawing.Size(809, 402);
            this.groupBox2.TabIndex = 49;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "待评估供应商信息列表";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.勾选,
            this.Supplier_ID,
            this.Supplier_Name,
            this.MaterialId,
            this.Supplier_AccountGroup,
            this.Supplier_Class});
            this.dataGridView1.Location = new System.Drawing.Point(15, 19);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.Size = new System.Drawing.Size(789, 368);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridView1_CellMouseClick);
            // 
            // 勾选
            // 
            this.勾选.HeaderText = "勾选";
            this.勾选.Name = "勾选";
            this.勾选.Width = 50;
            // 
            // Supplier_ID
            // 
            this.Supplier_ID.DataPropertyName = "供应商编号";
            this.Supplier_ID.HeaderText = "供应商编号";
            this.Supplier_ID.Name = "Supplier_ID";
            // 
            // Supplier_Name
            // 
            this.Supplier_Name.DataPropertyName = "供应商名称";
            this.Supplier_Name.HeaderText = "供应商名称";
            this.Supplier_Name.Name = "Supplier_Name";
            // 
            // MaterialId
            // 
            this.MaterialId.DataPropertyName = "物料编号";
            this.MaterialId.HeaderText = "物料编号";
            this.MaterialId.Name = "MaterialId";
            // 
            // Supplier_AccountGroup
            // 
            this.Supplier_AccountGroup.DataPropertyName = "账户组";
            this.Supplier_AccountGroup.HeaderText = "账户组";
            this.Supplier_AccountGroup.Name = "Supplier_AccountGroup";
            // 
            // Supplier_Class
            // 
            this.Supplier_Class.DataPropertyName = "供应商类型";
            this.Supplier_Class.HeaderText = "供应商类型";
            this.Supplier_Class.Name = "Supplier_Class";
            // 
            // SecletedEvaluatorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(953, 803);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "SecletedEvaluatorForm";
            this.Text = "评估通知发起";
            this.Load += new System.EventHandler(this.SecletedEvaluatorForm_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ComboBox comboBox_purchasingORGID;
        private System.Windows.Forms.ComboBox comboBox_supplierID;
        private System.Windows.Forms.Label lable_materialName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBox_materialID;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label lable_purchasingORGName;
        private System.Windows.Forms.Label lable_supplierName;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.DataGridViewCheckBoxColumn 勾选;
        private System.Windows.Forms.DataGridViewTextBoxColumn Supplier_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Supplier_Name;
        private System.Windows.Forms.DataGridViewTextBoxColumn MaterialId;
        private System.Windows.Forms.DataGridViewTextBoxColumn Supplier_AccountGroup;
        private System.Windows.Forms.DataGridViewTextBoxColumn Supplier_Class;
    }
}