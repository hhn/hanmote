﻿using Lib.Bll.SystemConfig.UserManage;
using Lib.Common.CommonUtils;
using Lib.Model.SystemConfig;
using Lib.SqlServerDAL.SystemConfig;
using MMClient.SystemConfig.PermissionManage;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;

namespace MMClient.SupplierPerformance.SecletedEvaluatorForm
{
    public partial class userForm : Form
    {
        private const int DEFAULT_PAGE_SIZE = 20;

        //总记录条数
        private int sumSize;
        int[] flag ;
        //存储查询结果
        private DataTable userInforTable;
        SystemUserOrganicRelationshipBLL systemUserOrganicRelationshipBLL = new SystemUserOrganicRelationshipBLL();
        List<string> supplierInfo = new List<string>();
        string content = "";
        string Message = "";
        public userForm(List<string> supplierInfo, string content)
        {
            InitializeComponent();
            this.supplierInfo = supplierInfo;
            this.content = content;
            for(int i =0;i<supplierInfo.Count; i++)
            {
                Message += supplierInfo[i];
            }
        }

        private void ReseachByCondition_Click(object sender, EventArgs e)
        {
            SystemUserOrganicRelationshipModel researchModel = new SystemUserOrganicRelationshipModel();
            researchModel.sysUserName = this.UserNAmeQe.Text;
            userInforTable = systemUserOrganicRelationshipBLL.findAllSystemUserOrganicRelationshipInfoByCondition(researchModel, 10, 0);
            this.SystemUserManager.AutoGenerateColumns = false;
            this.SystemUserManager.DataSource = userInforTable;
            setZero();
        }

        private void ResetInfoList_Click(object sender, EventArgs e)
        {
            this.UserNAmeQe.Text = "";
            fillSystemUserManagerTable();
            setZero();
        }
        /// <summary>
        /// 初始化已选择编号数组
        /// </summary>
        private void setZero()
        {
            if (flag != null)
            {
                for (int i = 0; i < flag.Length; i++)
                {
                    flag[i] = 0;
                }
            }
        }

        private void fillSystemUserManagerTable()
        {
            //查询当前页大小的记录
            userInforTable = systemUserOrganicRelationshipBLL.findAllSystemUserOrganicRelationshipInfo(10, 0, 0);
            this.SystemUserManager.AutoGenerateColumns = false;
            this.SystemUserManager.DataSource = userInforTable;
            //记录总数
            this.sumSize = systemUserOrganicRelationshipBLL.calculateUserNumber();
        }

        private void SystemUserManager_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if ( e.RowIndex != -1 && e.RowIndex >= 0 &&!this.SystemUserManager.Rows[e.RowIndex].IsNewRow)
            {
                if (e.ColumnIndex == 0)
                {
                    if (flag[e.RowIndex] == 0)
                    {
                        this.SystemUserManager.Rows[e.RowIndex].Cells[0].Value = true;
                        flag[e.RowIndex] = 1;
                    }
                    else
                    {
                        this.SystemUserManager.Rows[e.RowIndex].Cells[0].Value = false;
                        flag[e.RowIndex] = 0;
                    }
                }
            }
            else
            {
                MessageBox.Show("选择失败");
            }
        }
        /// <summary>
        /// 发送邮件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            int IS = 0;
            List<MailAddress> list = new List<MailAddress>();
            for (int i = 0; i < SystemUserManager.RowCount; i++)
            {
                DataGridViewRow curRow =  null;
                try
                {
                    curRow = this.SystemUserManager.Rows[i];
                }
                catch (Exception ex)
                {
                    MessageBox.Show("数据加载失败");
                    return;
                }
                if (curRow == null || curRow.Cells["Email"] == null) break;

                if (flag[i] == 1)
                {
                    string email = curRow.Cells["Email"].Value.ToString();
                    MailAddress mailAddress = new MailAddress(email);
                    list.Add(mailAddress);
                    IS = 1;
                }
            }
            if (IS == 0)
            {
                MessageBox.Show("当前没有选择评估员");
                return;
            }
            try
            {
                string dateStr ="请及时处理，最迟："+ DateTime.Now.Year + "年" + DateTime.Now.Month + "月" + (DateTime.Now.Day + 3) + "日";
                EmailUtil.SendEmail("汉默特评估通知", Message+"<p>" +content+"<p>"+ dateStr, list);
                MessageBox.Show("邮件发送成功");
            }
            catch(Exception ex)
            {
                MessageBox.Show("邮件发送失败");
            }
        }

        private void userForm_Load(object sender, EventArgs e)
        {
            ResetInfoList_Click(sender,e);
            flag = new int[this.SystemUserManager.Rows.Count];
            for (int i = 0; i < this.SystemUserManager.Rows.Count; i++)
            {
                flag[i] = 0;
            }
        }
    }
}
