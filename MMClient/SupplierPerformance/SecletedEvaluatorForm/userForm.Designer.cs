﻿namespace MMClient.SupplierPerformance.SecletedEvaluatorForm
{
    partial class userForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.SystemUserManager = new System.Windows.Forms.DataGridView();
            this.勾选 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.companyClass = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sysUserName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sysUserID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.JobPosition = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DepartName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PhoneNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Email = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.headerPanel = new System.Windows.Forms.Panel();
            this.ResetInfoList = new System.Windows.Forms.Button();
            this.UserNAmeQe = new System.Windows.Forms.TextBox();
            this.ReseachByCondition = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SystemUserManager)).BeginInit();
            this.headerPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.SystemUserManager);
            this.panel1.Controls.Add(this.headerPanel);
            this.panel1.Location = new System.Drawing.Point(13, 13);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1090, 680);
            this.panel1.TabIndex = 0;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(435, 625);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(137, 34);
            this.button1.TabIndex = 4;
            this.button1.Text = "确认发送";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(1, 130);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 12);
            this.label1.TabIndex = 3;
            this.label1.Text = "勾选待评估人";
            // 
            // SystemUserManager
            // 
            this.SystemUserManager.AllowUserToAddRows = false;
            this.SystemUserManager.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.SystemUserManager.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.SystemUserManager.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.勾选,
            this.companyClass,
            this.sysUserName,
            this.sysUserID,
            this.JobPosition,
            this.DepartName,
            this.PhoneNumber,
            this.Email});
            this.SystemUserManager.Location = new System.Drawing.Point(4, 155);
            this.SystemUserManager.Name = "SystemUserManager";
            this.SystemUserManager.RowTemplate.Height = 23;
            this.SystemUserManager.Size = new System.Drawing.Size(1070, 453);
            this.SystemUserManager.TabIndex = 2;
            this.SystemUserManager.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.SystemUserManager_CellMouseClick);
            // 
            // 勾选
            // 
            this.勾选.HeaderText = "勾选";
            this.勾选.Name = "勾选";
            this.勾选.Width = 50;
            // 
            // companyClass
            // 
            this.companyClass.DataPropertyName = "所属公司";
            this.companyClass.HeaderText = "所属公司";
            this.companyClass.Name = "companyClass";
            this.companyClass.Width = 200;
            // 
            // sysUserName
            // 
            this.sysUserName.DataPropertyName = "姓名";
            this.sysUserName.HeaderText = "姓名";
            this.sysUserName.Name = "sysUserName";
            // 
            // sysUserID
            // 
            this.sysUserID.DataPropertyName = "员工编号";
            this.sysUserID.HeaderText = "员工编号";
            this.sysUserID.Name = "sysUserID";
            // 
            // JobPosition
            // 
            this.JobPosition.DataPropertyName = "职务";
            this.JobPosition.HeaderText = "职务";
            this.JobPosition.Name = "JobPosition";
            // 
            // DepartName
            // 
            this.DepartName.DataPropertyName = "所属部门";
            this.DepartName.HeaderText = "所属部门";
            this.DepartName.Name = "DepartName";
            this.DepartName.Width = 150;
            // 
            // PhoneNumber
            // 
            this.PhoneNumber.DataPropertyName = "联系方式";
            this.PhoneNumber.HeaderText = "联系方式";
            this.PhoneNumber.Name = "PhoneNumber";
            this.PhoneNumber.Width = 125;
            // 
            // Email
            // 
            this.Email.DataPropertyName = "邮箱";
            this.Email.HeaderText = "邮箱";
            this.Email.Name = "Email";
            this.Email.Width = 200;
            // 
            // headerPanel
            // 
            this.headerPanel.Controls.Add(this.ResetInfoList);
            this.headerPanel.Controls.Add(this.UserNAmeQe);
            this.headerPanel.Controls.Add(this.ReseachByCondition);
            this.headerPanel.Controls.Add(this.label4);
            this.headerPanel.Location = new System.Drawing.Point(3, 13);
            this.headerPanel.Name = "headerPanel";
            this.headerPanel.Size = new System.Drawing.Size(1071, 95);
            this.headerPanel.TabIndex = 1;
            // 
            // ResetInfoList
            // 
            this.ResetInfoList.Location = new System.Drawing.Point(361, 32);
            this.ResetInfoList.Name = "ResetInfoList";
            this.ResetInfoList.Size = new System.Drawing.Size(91, 23);
            this.ResetInfoList.TabIndex = 61;
            this.ResetInfoList.Text = "重  置";
            this.ResetInfoList.UseVisualStyleBackColor = true;
            this.ResetInfoList.Click += new System.EventHandler(this.ResetInfoList_Click);
            // 
            // UserNAmeQe
            // 
            this.UserNAmeQe.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.UserNAmeQe.Location = new System.Drawing.Point(60, 15);
            this.UserNAmeQe.Name = "UserNAmeQe";
            this.UserNAmeQe.Size = new System.Drawing.Size(104, 23);
            this.UserNAmeQe.TabIndex = 59;
            // 
            // ReseachByCondition
            // 
            this.ReseachByCondition.Location = new System.Drawing.Point(233, 32);
            this.ReseachByCondition.Name = "ReseachByCondition";
            this.ReseachByCondition.Size = new System.Drawing.Size(91, 23);
            this.ReseachByCondition.TabIndex = 57;
            this.ReseachByCondition.Text = "查  询";
            this.ReseachByCondition.UseVisualStyleBackColor = true;
            this.ReseachByCondition.Click += new System.EventHandler(this.ReseachByCondition_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.label4.Location = new System.Drawing.Point(7, 20);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 17);
            this.label4.TabIndex = 55;
            this.label4.Text = "姓  名：";
            // 
            // userForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1122, 705);
            this.Controls.Add(this.panel1);
            this.Name = "userForm";
            this.Text = "评审员信息";
            this.Load += new System.EventHandler(this.userForm_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SystemUserManager)).EndInit();
            this.headerPanel.ResumeLayout(false);
            this.headerPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel headerPanel;
        private System.Windows.Forms.Button ResetInfoList;
        private System.Windows.Forms.TextBox UserNAmeQe;
        private System.Windows.Forms.Button ReseachByCondition;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridView SystemUserManager;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn 勾选;
        private System.Windows.Forms.DataGridViewTextBoxColumn companyClass;
        private System.Windows.Forms.DataGridViewTextBoxColumn sysUserName;
        private System.Windows.Forms.DataGridViewTextBoxColumn sysUserID;
        private System.Windows.Forms.DataGridViewTextBoxColumn JobPosition;
        private System.Windows.Forms.DataGridViewTextBoxColumn DepartName;
        private System.Windows.Forms.DataGridViewTextBoxColumn PhoneNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn Email;
    }
}