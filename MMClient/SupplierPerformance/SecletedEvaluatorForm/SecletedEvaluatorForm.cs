﻿using Lib.Bll.SupplierPerformaceBLL;
using Lib.SqlServerDAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;

namespace MMClient.SupplierPerformance.SecletedEvaluatorForm
{
    public partial class SecletedEvaluatorForm : DockContent
    {
        String  supplierID ; 
        List<Object> saveCRPara = new List<object>();
        DataTable supplierInfoTable, materialInfoTable, purchasingORGInfoTable; 
        SupplierPerformanceBLL supplierPerformanceBLL = new SupplierPerformanceBLL();
        string conditions = "";
        private userForm userForm = null;
        int[] flag;
        private void button1_Click(object sender, EventArgs e)
        {
            conditions = "";
            string supplierId = this.comboBox_supplierID.Text.ToString().Trim();
            string materialId = this.comboBox_materialID.Text.ToString().Trim();
            string org = this.comboBox_purchasingORGID.Text.ToString().Trim();
            if(!string.IsNullOrEmpty(supplierId))
            {
                conditions += " AND Supplier_Base.Supplier_ID = '" + supplierId + "'";
            }
            if(!string.IsNullOrEmpty(materialId))
            {
                conditions += " AND RecordInfo.MaterialId = '" + materialId + "'";
            }
           
            string sql = @"SELECT
	                            Supplier_Base.Supplier_ID AS 供应商编号,
	                            Supplier_Base.Supplier_Name AS 供应商名称,
	                            RecordInfo.MaterialId AS 物料编号,
	                            Supplier_Base.Supplier_AccountGroup AS 账户组,
	                            Supplier_Base.Supplier_Class AS 供应商类型
                            FROM
	                            Supplier_Base,
	                            RecordInfo

                            WHERE
	                            Supplier_Base.Supplier_ID = RecordInfo.SupplierId " + conditions;
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            DataTable tab = (DataTable)this.dataGridView1.DataSource;
            tab = null;
            this.dataGridView1.DataSource = dt;
            setZero();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            conditions = "";
            string sql = @"SELECT
	                            Supplier_Base.Supplier_ID AS 供应商编号,
	                            Supplier_Base.Supplier_Name AS 供应商名称,
	                            RecordInfo.MaterialId AS 物料编号,
	                            Supplier_Base.Supplier_AccountGroup AS 账户组,
	                            Supplier_Base.Supplier_Class AS 供应商类型
                            FROM
	                            Supplier_Base,
	                            RecordInfo

                            WHERE
	                            Supplier_Base.Supplier_ID = RecordInfo.SupplierId " + conditions;
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            DataTable tab = (DataTable)this.dataGridView1.DataSource;
            tab = null;
            this.dataGridView1.DataSource = dt;
            setZero();
        }
        /// <summary>
        /// 初始化已选择编号数组
        /// </summary>
        private void setZero()
        {
            if(flag!=null)
            {
                for(int i=0;i<flag.Length;i++)
                {
                    flag[i] = 0;
                }
            }
        }

        /// <summary>
        /// 选中
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridView1_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex >= 0 && e.RowIndex != -1 && !this.dataGridView1.Rows[e.RowIndex].IsNewRow)
            {
                if (e.ColumnIndex == 0)
                {
                    if (flag[e.RowIndex] == 0)
                    {
                        this.dataGridView1.Rows[e.RowIndex].Cells[0].Value = true;
                        flag[e.RowIndex] = 1;
                    }
                    else
                    {
                        this.dataGridView1.Rows[e.RowIndex].Cells[0].Value = false;
                        flag[e.RowIndex] = 0;
                    }

                }
            }
            else
            {
                MessageBox.Show("选择失败");
            }
        }

        private void SecletedEvaluatorForm_Load(object sender, EventArgs e)
        {
            button2_Click(sender, e);
            flag = new int[this.dataGridView1.Rows.Count];
            for(int i=0;i< this.dataGridView1.Rows.Count;i++)
            {
                flag[i] = 0;
            }
        }
        /// <summary>
        /// 发送
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button3_Click(object sender, EventArgs e)
        {
            int IS = 0;
            List<string> supplierInfo = new List<string>();
            for(int i = 0;i<dataGridView1.RowCount;i++)
            {
                DataGridViewRow curRow = null;
                try
                {
                    curRow = this.dataGridView1.Rows[i];
                }
                catch (Exception ex)
                {
                    return;
                }
                if (curRow == null || curRow.Cells["Supplier_ID"].Value == null) break;

                if (flag[i] == 1)
                {
                    string info = "供应商编号：" + curRow.Cells["Supplier_ID"].Value.ToString() + "<br> 供应商名称：" + curRow.Cells["Supplier_Name"].Value.ToString() + "<br> 物料编号：" + curRow.Cells["MaterialId"].Value.ToString();
                    supplierInfo.Add(info);
                    supplierInfo.Add("<br><br>");
                    IS = 1;
                }
            }
            if(IS==0)
            {
                MessageBox.Show("当前没有选择供应商");
                return;
            }
            if (userForm == null || userForm.IsDisposed)
            {
                this.userForm = new userForm(supplierInfo,this.textBox1.Text.ToString());
            }
            userForm.Show();
        }

        public SecletedEvaluatorForm()
        {
            InitializeComponent();
            //选择供应商id
            supplierInfoTable = supplierPerformanceBLL.querySupplier();
            comboBox_supplierID.DataSource = supplierInfoTable;
            comboBox_supplierID.DisplayMember = supplierInfoTable.Columns["Supplier_ID"].ToString();
            lable_supplierName.Text = (supplierInfoTable.Rows[comboBox_supplierID.SelectedIndex]["Supplier_Name"]).ToString();
          
        }

        private void comboBox_supplierID_SelectedIndexChanged(object sender, EventArgs e)
        {
            supplierID = comboBox_supplierID.Text.ToString();
            lable_supplierName.Text = supplierInfoTable.Rows[comboBox_supplierID.SelectedIndex]["Supplier_Name"].ToString();

            //选择的供应商变化时，则重新查询对应的采购组织信息
            purchasingORGInfoTable = supplierPerformanceBLL.queryPurchasingORG(supplierID);
            comboBox_purchasingORGID.DataSource = purchasingORGInfoTable;
            if (purchasingORGInfoTable.Rows.Count > 0)
            {
                comboBox_purchasingORGID.DisplayMember = purchasingORGInfoTable.Columns["PurchasingORG_ID"].ToString();
                lable_purchasingORGName.Text = (purchasingORGInfoTable.Rows[comboBox_purchasingORGID.SelectedIndex]["PurchasingORG_Name"]).ToString();
            }

            //选择的供应商变化时，则重新查询对应的物料信息
            materialInfoTable = supplierPerformanceBLL.queryMaterialID(supplierID);
            comboBox_materialID.DataSource = materialInfoTable;
            if (materialInfoTable.Rows.Count > 0)
            {
                comboBox_materialID.DisplayMember = materialInfoTable.Columns["Material_ID"].ToString();
                lable_materialName.Text = (materialInfoTable.Rows[comboBox_materialID.SelectedIndex]["Material_Name"]).ToString();
            }
        }
    }
}
