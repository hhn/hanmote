﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Bll.SupplierPerformaceBLL;
using WeifenLuo.WinFormsUI.Docking;

namespace MMClient.SupplierPerformance
{
    public partial class CostReductionForm : DockContent
    {
        DataTable supplierInfoTable, materialInfoTable;
        SupplierPerformanceBLL supplierPerformanceBLL = new SupplierPerformanceBLL();
        String supplierID, materialID;
        String year, month;
        Decimal costReduction, costReductionRate;
        CostReductionBLL costReductionBLL = new CostReductionBLL();
        bool isControled = false;
        List<Object> savePara = new List<object>();

        public CostReductionForm()
        {
            InitializeComponent();

            //选择供应商id
            supplierInfoTable = supplierPerformanceBLL.querySupplier();
            comboBox_supplierID.DisplayMember = supplierInfoTable.Columns["Supplier_ID"].ToString();
            comboBox_supplierID.ValueMember = supplierInfoTable.Columns["Supplier_ID"].ToString();
            comboBox_supplierID.DataSource = supplierInfoTable;
            label_supplierName.Text = (supplierInfoTable.Rows[comboBox_supplierID.SelectedIndex]["Supplier_Name"]).ToString();
            

            //在年份选项中给combobox添加2015年至现在的年份
            for (int i = DateTime.Now.Year; i >= 2015; i--)
            {
                string year = string.Format("{0}", i);
                comboBox_year.Items.Add(year);
            }
            comboBox_year.SelectedIndex = 0;

            //添加月份
            comboBox_month.Items.Add("1");
            comboBox_month.Items.Add("2");
            comboBox_month.Items.Add("3");
            comboBox_month.Items.Add("4");
            comboBox_month.Items.Add("5");
            comboBox_month.Items.Add("6");
            comboBox_month.Items.Add("7");
            comboBox_month.Items.Add("8");
            comboBox_month.Items.Add("9");
            comboBox_month.Items.Add("10");
            comboBox_month.Items.Add("11");
            comboBox_month.Items.Add("12");
             
        }

        //供应商ID comboBox
        private void comboBox_supplierID_SelectedIndexChanged(object sender, EventArgs e)
        {
            supplierID = comboBox_supplierID.Text.ToString();
            if (supplierInfoTable.Rows.Count > 0)
            {
                label_supplierName.Text = supplierInfoTable.Rows[comboBox_supplierID.SelectedIndex]["Supplier_Name"].ToString();
            }

            //选择的供应商变化时，则重新查询对应的物料信息
            materialInfoTable = supplierPerformanceBLL.queryMaterialID(supplierID);
            comboBox_materialID.DataSource = materialInfoTable;
            if (materialInfoTable.Rows.Count > 0)
            {
                comboBox_materialID.DisplayMember = materialInfoTable.Columns["Material_ID"].ToString();
                label_materialName.Text = (materialInfoTable.Rows[comboBox_materialID.SelectedIndex]["Material_Name"]).ToString();
            }
        }

        //Button 计算
        private void button_calculate_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(comboBox_supplierID.Text) || String.IsNullOrEmpty(comboBox_materialID.Text) || String.IsNullOrEmpty(comboBox_year.Text) || String.IsNullOrEmpty(comboBox_month.Text) || String.IsNullOrEmpty(textBox_Name.ToString()))
            {
                MessageBox.Show("请完善基本信息！");
                return;
            }

            supplierID = comboBox_supplierID.Text;
            materialID = comboBox_materialID.Text;
            year = comboBox_year.Text;
            month = comboBox_month.Text;

            //判断是否为市场管制品种
            isControled = costReductionBLL.queryControledOrNot(materialID);

            //该物料为市场管制品种
            if (isControled == true)
            {
                //计算采购成本降低额
                costReduction = costReductionBLL.calculateControledCostReduction(supplierID, materialID, year, month);
                if (costReduction == 0)
                {
                    textBox_CostReduction.Text = "暂无数据";
                }
                else
                {
                    textBox_CostReduction.Text = Math.Round(costReduction, 2).ToString();
                }

                //计算采购成本降低率
                costReductionRate = costReductionBLL.calculateControledCostReductionRate(supplierID, materialID, year, month);
                if (costReductionRate == 0)
                {
                    textBox_CostReductionRate.Text = "暂无数据";
                }
                else
                {
                    textBox_CostReductionRate.Text = Math.Round(costReductionRate, 2).ToString();
                }
            }
            //该物料为非市场管制品种
            else
            {
                //计算采购成本降低额
                costReduction = costReductionBLL.calculateUncontroledCostReduction(supplierID, materialID, year, month);
                if (costReduction == 0)
                {
                    textBox_CostReduction.Text = "暂无数据";
                }
                else
                {
                    textBox_CostReduction.Text = Math.Round(costReduction, 2).ToString();
                }

                //计算采购成本降低率
                costReductionRate = costReductionBLL.calculateUncontroledCostReductionRate(supplierID, materialID, year, month);
                if (costReductionRate == 0)
                {
                    textBox_CostReductionRate.Text = "暂无数据";
                }
                else
                {
                    textBox_CostReductionRate.Text = Math.Round(costReductionRate, 2).ToString();
                }
            }

            //显示物料是否为市场管制品种
            if (isControled == true)
            {
                textBox_materialType.Text = "市场管制品种";
            }
            if (isControled == false)
            {
                textBox_materialType.Text = "非市场管制品种";
            }
        }

        //保存Button
        private void button_save_Click(object sender, EventArgs e)
        {
            //如果降成本额和降成本率没有数据，则不保存
            if (textBox_CostReduction.Text == "暂无数据" || textBox_CostReductionRate.Text == "暂无数据")
            {
                MessageBox.Show("采购降成本无计算结果！");
                return;
            }

            //将需要保存到数据表中的字段，按顺序添加到List<Object>类型的savePara中
            savePara.Clear();
            savePara.Add(supplierID);
            savePara.Add(materialID);
            if (isControled == true)
            {
                savePara.Add("管制");
            }
            else
            {
                savePara.Add("非管制");
            }
            savePara.Add(Math.Round(costReduction,3));
            savePara.Add(Math.Round(costReductionRate,3));
            savePara.Add(year);
            savePara.Add(month);
            savePara.Add(Convert.ToDateTime(dateTimePicker.Text));
            savePara.Add(textBox_Name.Text.ToString());

            int lines = costReductionBLL.saveCostReduction(savePara);
            if (lines == 1)
            {
                MessageBox.Show("保存成功！");
                return;
            }
            else
            {
                MessageBox.Show("保存失败！");
                return;
            }
        }
    }
}
