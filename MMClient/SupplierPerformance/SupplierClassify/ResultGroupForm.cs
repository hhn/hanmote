﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;
using Lib.Bll.SupplierPerformaceBLL;

namespace MMClient.SupplierPerformance.SupplierClassify
{
    public partial class ResultGroupForm : DockContent
    {
        String purchasingORGID,category,year;
        DataTable purchasingORGInfoTable = new DataTable();
        DataTable categoryTable = new DataTable();
        SupplierPerformanceBLL supplierPerformanceBLL = new SupplierPerformanceBLL();

        public ResultGroupForm()
        {
            InitializeComponent();

            //采购组织id
            purchasingORGInfoTable = supplierPerformanceBLL.queryPurchasingORG();
            comboBox_purchasingORGID.DataSource = purchasingORGInfoTable;
            comboBox_purchasingORGID.DisplayMember = purchasingORGInfoTable.Columns["PurchasingORG_ID"].ToString();
            label_purchasingORGName.Text = (purchasingORGInfoTable.Rows[comboBox_purchasingORGID.SelectedIndex]["PurchasingORG_Name"]).ToString();

            //供应商类别
            categoryTable.Clear();
            categoryTable = supplierPerformanceBLL.queryCategory();
            //comboBox_category.DataSource = categoryTable;
            /*
            for (int i = categoryTable.Columns.Count - 1; i >= 0; i--)
            {

                foreach (DataRow r in categoryTable.Rows)
                {

                    if (!r.IsNull(categoryTable.Columns[i]))

                        continue;

                }
            
                categoryTable.Columns.RemoveAt(i);
            }
            */
            List<String> tmpList = new List<String>();
            foreach (DataRow r in categoryTable.Rows)
            {
                String tmpString = r[0].ToString();
                tmpList.Add(tmpString);
            }
            for (int i = 0; i < tmpList.Count; i++)
            {
                comboBox_category.Items.Add(tmpList[i]);
            }

                //在年份选项中给combobox添加2015年至现在的年份
                for (int i = DateTime.Now.Year; i >= 2015; i--)
                {
                    string year = string.Format("{0}", i);
                    comboBox_year.Items.Add(year);
                }
            comboBox_year.SelectedIndex = 0;
        }

        private void comboBox_purchasingORGID_SelectedIndexChanged(object sender, EventArgs e)
        {
            purchasingORGID = comboBox_purchasingORGID.Text.ToString();
            if (purchasingORGInfoTable.Rows.Count > 0)
            {
                label_purchasingORGName.Text = (purchasingORGInfoTable.Rows[comboBox_purchasingORGID.SelectedIndex]["PurchasingORG_Name"]).ToString();
            }
        }

        /// <summary>
        /// 查询供应商分级结果
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button_check_Click(object sender, EventArgs e)
        {
            //每一次点击查询前，清空datagridview中展示的内容
            dataGridView.AllowUserToAddRows = false;

            for (int i = 0; i < dataGridView.Rows.Count; i++)
            {
                DataGridViewRow row = dataGridView.Rows[i];
                dataGridView.Rows.Remove(row);
                i--;
            }

            purchasingORGID = comboBox_purchasingORGID.Text.ToString();
            year = comboBox_year.Text.ToString();
            category = comboBox_category.Text.ToString();

            if (String.IsNullOrEmpty(purchasingORGID))
            {
                MessageBox.Show("请选择采购组织ID！");
                return;
            }
            else if (String.IsNullOrEmpty(category))
            {
                MessageBox.Show("请选择供应商分类！");
                return;
            }
            else if (String.IsNullOrEmpty(year))
            {
                MessageBox.Show("请选择年份！");
            }

            DataTable rstTable;
            rstTable = supplierPerformanceBLL.queryClassificationGroupResult(purchasingORGID,category,year);
            if (rstTable.Rows.Count == 0)
            {
                MessageBox.Show("当前尚无分级信息！");
                return;
            }

            //添加之前先清空DataGridView
            dataGridView.Rows.Clear();

            for (int i = 0; i < rstTable.Rows.Count; i++)
            {
                //添加一行(!!!)
                this.dataGridView.Rows.Add();
                DataGridViewRow row = this.dataGridView.Rows[i];

                //curRow.Cells[0].Value = rstTable.Rows[i][0];
                //添加供应商名称
                String supplierID = rstTable.Rows[i][0].ToString();
                String supplierName = supplierPerformanceBLL.querySupplierName(supplierID);
                row.Cells[0].Value = supplierName;
                row.Cells[1].Value = rstTable.Rows[i][1].ToString();
                row.Cells[2].Value = rstTable.Rows[i][2].ToString();
            }
        }

    }
}
