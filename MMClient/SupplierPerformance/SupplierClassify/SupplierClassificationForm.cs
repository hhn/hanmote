﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;
using Lib.Bll.SupplierPerformaceBLL;

namespace MMClient.SupplierPerformance.SupplierClassify
{
    public partial class SupplierClassificationForm : DockContent
    {
        String supplierID;
        DataTable supplierInfoTable;
        String classifyRst;
        List<Object> saveRstPara = new List<object>();//将数据表中的字段依次添加，用于保存结果
        SupplierPerformanceBLL supplierPerformanceBLL = new SupplierPerformanceBLL();

        public SupplierClassificationForm()
        {
            InitializeComponent();

            //选择供应商id
            supplierInfoTable = supplierPerformanceBLL.querySupplier();
            comboBox_supplierID.DataSource = supplierInfoTable;
            comboBox_supplierID.DisplayMember = supplierInfoTable.Columns["Supplier_ID"].ToString();
            lable_supplierName.Text = (supplierInfoTable.Rows[comboBox_supplierID.SelectedIndex]["Supplier_Name"]).ToString();
            try
            {
                this.comboBox_supplierID.SelectedIndex = 0;
            }
            catch
            {
                MessageBox.Show("当前无供应商");
                return;
            }
        }

        //comboBox 选择供应商ID
        private void comboBox_supplierID_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (supplierInfoTable.Rows.Count <= 0)
            {
                lable_supplierName.Text = supplierInfoTable.Rows[comboBox_supplierID.SelectedIndex]["Supplier_Name"].ToString();
            }
            
        }

        /// <summary>
        /// button 执行分级
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button_classify_Click(object sender, EventArgs e)
        {
            supplierID = comboBox_supplierID.Text.ToString();
            DateTime start = this.dateTimePicker1.Value;
            DateTime end = this.dateTimePicker2.Value;
            if (String.IsNullOrEmpty(supplierID))
            {
                MessageBox.Show("请选择供应商ID及有效年份！");
                return;
            }
            classifyRst = supplierPerformanceBLL.executeSupplierClassify(supplierID, start,end);

            //无分级结果
            if (classifyRst == "N")
            {
                MessageBox.Show("当前供应商无有效分级结果！");
                textBox_result.Clear();
                return;
            }
            else
            {
                textBox_result.Text = classifyRst;
            }
        }

        /// <summary>
        /// button 保存
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button_save_Click(object sender, EventArgs e)
        {
            supplierID = comboBox_supplierID.Text.ToString();
            if (String.IsNullOrEmpty(supplierID))
            {
                MessageBox.Show("请选择供应商ID及有效年份！");
                return;
            }
            else if (String.IsNullOrEmpty(textBox_result.Text) || String.IsNullOrEmpty(classifyRst) || classifyRst == "N")
            {
                MessageBox.Show("保存结果前请执行分级操作！");
                return;
            }
            else if (String.IsNullOrEmpty(textBox_name.Text))
            {
                MessageBox.Show("请输入分级维护人信息！");
                return;
            }

            saveRstPara.Clear();
            saveRstPara.Add(supplierID);
            saveRstPara.Add(classifyRst);
            saveRstPara.Add(this.dateTimePicker.Value.Year.ToString());
            saveRstPara.Add(Convert.ToDateTime(dateTimePicker.Text));
            saveRstPara.Add(textBox_name.Text.ToString());
            saveRstPara.Add(this.dateTimePicker1.Value.Year.ToString()+"/"+this.dateTimePicker1.Value.Month.ToString()+"-"+ this.dateTimePicker2.Value.Year.ToString()+"/"+this.dateTimePicker2.Value.Month.ToString());

            int lines = supplierPerformanceBLL.saveClassificationResult(saveRstPara);

            if (lines > 0)
            {
                MessageBox.Show("分级结果保存成功！");
                return;
            }
            else
            {
                MessageBox.Show("分级结果保存失败！");
                return;
            }

            //执行完保存操作之后给字符串赋空值
            classifyRst = "";
        }

    }
}
