﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;
using Lib.Bll.SupplierPerformaceBLL;

namespace MMClient.SupplierPerformance.SupplierClassify
{
    public partial class ClassificationResultForm : DockContent
    {
        SupplierPerformanceBLL supplierPerformanceBLL = new SupplierPerformanceBLL();
        DataTable supplierInfoTable;
        String supplierID, year;

        public ClassificationResultForm()
        {
            InitializeComponent();

            //选择供应商id
            supplierInfoTable = supplierPerformanceBLL.querySupplier();
            comboBox_supplierID.DataSource = supplierInfoTable;
            comboBox_supplierID.DisplayMember = supplierInfoTable.Columns["Supplier_ID"].ToString();
            lable_supplierName.Text = (supplierInfoTable.Rows[comboBox_supplierID.SelectedIndex]["Supplier_Name"]).ToString();
            try
            {
                this.comboBox_supplierID.SelectedIndex = 0;
            }
            catch
            {
                MessageBox.Show("当前无供应商");
                return;
            }
            int k = DateTime.Now.Year;

            for (int i = k-10; i <= k; i++)
            {
                string year = string.Format("{0}", i);
                comboBox_year.Items.Add(year);
            }
        }

        //选择供应商ID
        private void comboBox_supplierID_SelectedIndexChanged(object sender, EventArgs e)
        {
            supplierID = comboBox_supplierID.Text.ToString();
            if (supplierInfoTable.Rows.Count > 0)
            {
                lable_supplierName.Text = supplierInfoTable.Rows[comboBox_supplierID.SelectedIndex]["Supplier_Name"].ToString();
            }
        }

        //选择年份
        private void comboBox_year_SelectedIndexChanged(object sender, EventArgs e)
        {
            year = comboBox_year.Text.ToString();
        }

        /// <summary>
        /// button 查询
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button_check_Click(object sender, EventArgs e)
        {
            //每一次点击查询前，清空datagridview中展示的内容
            dataGridView.AllowUserToAddRows = false;

            for(int i=0;i<dataGridView.Rows.Count;i++)
            {
                DataGridViewRow row = dataGridView.Rows[i];
                dataGridView.Rows.Remove(row);
                i--;
            }

            if (String.IsNullOrEmpty(comboBox_supplierID.Text) || String.IsNullOrEmpty(comboBox_year.Text))
            {
                MessageBox.Show("请选择供应商代码和年份！");
                return;
            }
            DataTable rstTable;
            rstTable = supplierPerformanceBLL.queryClassificationResult(supplierID, year);

            if (rstTable.Rows.Count == 0)
            {
                MessageBox.Show("当前尚无分级信息！");
                return;
            }

            //添加之前先清空DataGridView
            dataGridView.Rows.Clear();

            for (int i = 0; i < rstTable.Rows.Count; i++)
            {
                //添加一行(!!!)
                this.dataGridView.Rows.Add();
                DataGridViewRow row = this.dataGridView.Rows[i];

                row.Cells[0].Value = rstTable.Rows[i][0];
                //添加供应商名称
                row.Cells[1].Value = supplierInfoTable.Rows[comboBox_supplierID.SelectedIndex]["Supplier_Name"].ToString();
                row.Cells[2].Value = rstTable.Rows[i][1];
                row.Cells[3].Value = rstTable.Rows[i][2];
                row.Cells[4].Value = rstTable.Rows[i][3];
                row.Cells[5].Value = rstTable.Rows[i][4];
            }
        }
    }
}
