﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;
using Lib.Model.ServiceEvaluation;
using Lib.Bll.ServiceBll;
using Lib.Common.CommonUtils;

namespace MMClient.SupplierPerformance.SupplierService
{
    public partial class InterForm : DockContent
    {
        Dictionary<string, string> supplierMap = null;
        Dictionary<string, string> mtGroupMap = null;
        Dictionary<string, string> mtIdMap = null;
        ServiceBill serviceBll = new ServiceBill();
       
        public InterForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            GeneralServiceModle serviceModel = new GeneralServiceModle();
          
            DataTable dt =  serviceBll.getGeneServiceRate(this.MtGroupId.Text);

            serviceModel.SupplierId = this.supplierId.SelectedValue.ToString();
            serviceModel.SupplierName = this.supplierId.Text.ToString();
            serviceModel.MtName = this.mtId.Text.ToString();
            serviceModel.MtID = this.mtId.SelectedValue.ToString();
            serviceModel.MtGroupName = this.MtGroupId.SelectedValue.ToString();
            serviceModel.GenServiceId = this.genServiceId.Text;
            serviceModel.FactoryId = this.factoryId.Text;
            serviceModel.Creator = this.creator.Text;
            serviceModel.ServiceTime = DateTime.Now;
            serviceModel.InnovationSco = float.Parse(showmm(this.panel3))*20;
            serviceModel.InReason1 = this.InnoReason.Text.ToString();
            serviceModel.RealizableSco = float.Parse(showmm(this.panel4))*20;
            serviceModel.ReReson = this.ReReason.Text.ToString();
            serviceModel.UserServiceSco = float.Parse(showmm(this.panel5))*20;
            serviceModel.UserSerReson = this.UserService.Text.ToString();
            if (serviceModel.InnovationSco == 0 || serviceModel.RealizableSco == 0 || serviceModel.UserServiceSco == 0)
            {
                MessageUtil.ShowTips("请输入分数！");
                return;
            }
            if (dt.Rows.Count > 0)
            {
                serviceModel.InnoRate1 = dt.Rows[0]["创新性"].ToString();
                serviceModel.ReRate1 = dt.Rows[0]["可靠性"].ToString();
                serviceModel.UserRate1 = dt.Rows[0]["用户服务"].ToString();
                serviceModel.ServiceTotalScore = (serviceModel.InnovationSco * float.Parse(dt.Rows[0]["创新性"].ToString().TrimEnd('%'))
                    + serviceModel.RealizableSco* float.Parse(dt.Rows[0]["可靠性"].ToString().TrimEnd('%'))
                    + serviceModel.UserServiceSco*float.Parse(dt.Rows[0]["用户服务"].ToString().TrimEnd('%')))/100;
                
            }
            else {
                serviceModel.ServiceTotalScore = (serviceModel.InnovationSco + serviceModel.RealizableSco + serviceModel.UserServiceSco) / 3;
            }
            serviceModel.ServiceTotalScore = Math.Round(serviceModel.ServiceTotalScore, 2);
            if (serviceBll.insertGenServiceData(serviceModel))
            {
                MessageUtil.ShowTips("保存成功！");

            }
        }



        private string showmm(Panel panel)
        {

            foreach (RadioButton rad in panel.Controls)
            {

                if (rad.Checked)
                {
                    return rad.Tag.ToString();
                }

            }
            MessageUtil.ShowError("未选择分数！");
            return "0";
        }

        private void InterForm_Load(object sender, EventArgs e)
        {

            this.serviceModelName.DataSource = serviceBll.getGenServiceModelName();

            DataTable dt = serviceBll.getSupplierId(SingleUserInstance.getCurrentUserInstance().User_ID);
            //supplierMap = serviceBill.getSupplierId("20180416181829");
            if (dt.Rows.Count == 0)
            {
                MessageUtil.ShowTips("没有需要评估的供应商！");
                return;
            }
            this.supplierId.DisplayMember = "供应商名称";
            this.supplierId.ValueMember = "供应商编号";
            this.supplierId.DataSource = dt;

            this.creator.Text = SingleUserInstance.getCurrentUserInstance().Username;
            this.ServiceTime.Text = DateTime.Now.ToString("yyyy-MM-dd");
            this.genServiceId.Text = DateTime.Now.ToString("yyyymmddhhss");
        }


        private void factoryId_TextChanged(object sender, EventArgs e)
        {
            if (this.factoryId.Text.Equals("W0001"))
            {
                this.faName.Text = "武汉工厂";

            }
            if (this.factoryId.Text.Equals("B0001"))
            {
                this.faName.Text = "北京工厂";

            }
            if (this.factoryId.Text.Equals("G0001"))
            {
                this.faName.Text = "广州工厂";

            }
           
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void serviceSummry_Click(object sender, EventArgs e)
        {
            一般服务模板 geneServiceSum = new 一般服务模板();
            geneServiceSum.Show();
        }

        private void factoryId_Validating(object sender, CancelEventArgs e)
        {
            if (factoryId.Text.Equals(""))
            {
                errorUser.SetError(factoryId, "不能为空");
            }
            else
            {
                //如果设置为空的错误信息将不显示错误标记
                errorUser.SetError(factoryId, "");
            }
        }

        private void InnoReason_Validating(object sender, CancelEventArgs e)
        {
            if (InnoReason.Text.Equals(""))
            {
                errorUser.SetError(InnoReason, "不能为空");
            }
            else
            {
                //如果设置为空的错误信息将不显示错误标记
                errorUser.SetError(InnoReason, "");
            }
        }

        private void ReReason_Validating(object sender, CancelEventArgs e)
        {
            if (ReReason.Text.Equals(""))
            {
                errorUser.SetError(ReReason, "不能为空");
            }
            else
            {
                //如果设置为空的错误信息将不显示错误标记
                errorUser.SetError(ReReason, "");
            }
        }

        private void UserService_Validating(object sender, CancelEventArgs e)
        {
            if (UserService.Text.Equals(""))
            {
                errorUser.SetError(UserService, "不能为空");
            }
            else
            {
                //如果设置为空的错误信息将不显示错误标记
                errorUser.SetError(UserService, "");
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            NewGenServiceModel newGenServiceModel = new NewGenServiceModel();
            newGenServiceModel.Show();
        }

        private void serviceModelName_TextChanged(object sender, EventArgs e)
        {
            DataTable dt = serviceBll.getGenServiceModel(this.serviceModelName.Text);
            //质量
            string[] Innos = dt.Rows[0]["Innos"].ToString().Split(',');
            //及时性
            string[] Res = dt.Rows[0]["Res"].ToString().Split(',');

            string[] USs = dt.Rows[0]["USs"].ToString().Split(',');
            if (Innos.Length < 5)
            {
                return;
            }
            this.RE1.Text = Res[0];
            this.RE2.Text = Res[1];
            this.RE3.Text = Res[2];
            this.RE4.Text = Res[3];
            this.RE5.Text = Res[4];

            this.Inno1.Text = Innos[0];
            this.Inno2.Text = Innos[1];
            this.Inno3.Text = Innos[2];
            this.Inno4.Text = Innos[3];
            this.Inno5.Text = Innos[4];

            this.US1.Text = USs[0];
            this.US2.Text = USs[1];
            this.US3.Text = USs[2];
            this.US4.Text = USs[3];
            this.US5.Text = USs[4];
        }

       

        private void reSetBtn_Click(object sender, EventArgs e)
        {
            this.factoryId.Text = "";
            this.InnoReason.Text = "";
            this.ReReason.Text = "";
            this.UserService.Text = "";
            clear(this.panel3);
            clear(this.panel4);
            InterForm_Load(sender, e);
        }

        private void clear(Panel panel)
        {
            foreach (RadioButton rad in panel.Controls)
            {

                if (rad.Checked)
                {
                    rad.Checked = false;
                }

            }
        }
        //加载物料组信息
        private void supplierId_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dt = serviceBll.getMtGroupidAndName(this.supplierId.SelectedValue.ToString());
            this.MtGroupId.DisplayMember = "mtGroupName";
            this.MtGroupId.ValueMember = "mtGroupId";
            this.MtGroupId.DataSource = dt;

        }
        //加载物料信息
        private void MtGroupId_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dt = serviceBll.getMtidAndName(this.MtGroupId.SelectedValue.ToString(), this.supplierId.SelectedValue.ToString());
            this.mtId.DisplayMember = "物料名称";
            this.mtId.ValueMember = "物料编号";
            this.mtId.DataSource = dt;
        }
    }
}
