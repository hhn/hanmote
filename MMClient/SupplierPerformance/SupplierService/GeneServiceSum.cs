﻿using Lib.Bll.ServiceBll;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MMClient.SupplierPerformance.SupplierService
{
    public partial class 一般服务模板 : Form
    {
        ServiceBill serviceBll = new ServiceBill();
        public 一般服务模板()
        {
            InitializeComponent();
        }

        private void GeneServiceSum_Load(object sender, EventArgs e)
        {
            dataGridView1.AutoGenerateColumns = false;
            DataTable dt = serviceBll.getGenServiceData();
            this.endTime.Value = this.startTime.Value.AddDays(1);
            this.supplierName.DataSource = serviceBll.getGenSupplierName();
            dataGridView1.DataSource = serviceBll.getGenServiceData();
        }

        private void supplierName_TextChanged(object sender, EventArgs e)
        {
            this.MateGroupName.DataSource = serviceBll.getGenMateGroupName(this.supplierName.Text);
        }

        private void MateGroupName_TextChanged(object sender, EventArgs e)
        {
            this.MaterialName.DataSource = serviceBll.getGenMateName(this.supplierName.Text, this.MateGroupName.Text);
        }

        private void searchBtn_Click(object sender, EventArgs e)
        {
            double sum = 0;
            DataTable dt = serviceBll.getGenServiceData(this.supplierName.Text, this.MateGroupName.Text, this.MaterialName.Text, this.startTime.Value.ToString("yyyy-MM-dd"), this.endTime.Value.ToString("yyyy-MM-dd"));
            foreach (DataRow dataRow in dt.Rows)
            {
                sum += float.Parse(dataRow["serviceTotalScore"].ToString());

            }
            DataRow row = dt.NewRow();
            row["serviceTotalScore"] =Math.Round( sum / dt.Rows.Count,2);
            row["GenServiceId"] = "汇总结果";
            row["supplierName"] = this.supplierName.Text;
            row["MtGroupName"] = this.MateGroupName.Text;
            row["danWei"] = "记录条数：" + dt.Rows.Count;
            row["mtName"] = this.MaterialName.Text;
            row["supplierId"] = this.startTime.Text + "--" + this.endTime.Text;
            dt.Rows.Add(row.ItemArray);
            dataGridView1.DataSource = dt;
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void 返回_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
