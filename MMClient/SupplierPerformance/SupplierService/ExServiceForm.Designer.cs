﻿namespace MMClient.SupplierPerformance.SupplierService
{
    partial class ExServiceForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panel1 = new System.Windows.Forms.Panel();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label23 = new System.Windows.Forms.Label();
            this.SQReason = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.SQ1 = new System.Windows.Forms.Label();
            this.SQ2 = new System.Windows.Forms.Label();
            this.SQ3 = new System.Windows.Forms.Label();
            this.SQ4 = new System.Windows.Forms.Label();
            this.SQ5 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.ServiceQScore5 = new System.Windows.Forms.RadioButton();
            this.ServiceQScore2 = new System.Windows.Forms.RadioButton();
            this.ServiceQScore4 = new System.Windows.Forms.RadioButton();
            this.ServiceQScore3 = new System.Windows.Forms.RadioButton();
            this.ServiceQScore1 = new System.Windows.Forms.RadioButton();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.ST1 = new System.Windows.Forms.Label();
            this.ST2 = new System.Windows.Forms.Label();
            this.ST3 = new System.Windows.Forms.Label();
            this.ST4 = new System.Windows.Forms.Label();
            this.ST5 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.ServiceTimeScore5 = new System.Windows.Forms.RadioButton();
            this.ServiceTimeScore2 = new System.Windows.Forms.RadioButton();
            this.ServiceTimeScore4 = new System.Windows.Forms.RadioButton();
            this.ServiceTimeScore3 = new System.Windows.Forms.RadioButton();
            this.ServiceTimeScore1 = new System.Windows.Forms.RadioButton();
            this.STReason = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.MtGroupId = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.mtId = new System.Windows.Forms.ComboBox();
            this.serviceModelName = new System.Windows.Forms.ComboBox();
            this.label32 = new System.Windows.Forms.Label();
            this.button4 = new System.Windows.Forms.Button();
            this.servicePlace = new System.Windows.Forms.TextBox();
            this.reset = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.createTime = new System.Windows.Forms.TextBox();
            this.creator = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.supplierId = new System.Windows.Forms.ComboBox();
            this.ExtServiceId = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.errorUser = new System.Windows.Forms.ErrorProvider(this.components);
            this.panel1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorUser)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.panel1.Controls.Add(this.button3);
            this.panel1.Controls.Add(this.button2);
            this.panel1.Controls.Add(this.tableLayoutPanel1);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(925, 819);
            this.panel1.TabIndex = 0;
            // 
            // button3
            // 
            this.button3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button3.Location = new System.Drawing.Point(704, 779);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 5;
            this.button3.Text = "保存";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button2.Location = new System.Drawing.Point(815, 779);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 4;
            this.button2.Text = "返回";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel1.ColumnCount = 6;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66319F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66736F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66736F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66736F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66736F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66736F));
            this.tableLayoutPanel1.Controls.Add(this.label23, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.SQReason, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.label22, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.SQ1, 5, 5);
            this.tableLayoutPanel1.Controls.Add(this.SQ2, 4, 5);
            this.tableLayoutPanel1.Controls.Add(this.SQ3, 3, 5);
            this.tableLayoutPanel1.Controls.Add(this.SQ4, 2, 5);
            this.tableLayoutPanel1.Controls.Add(this.SQ5, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.label16, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.panel4, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.label15, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.label14, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.ST1, 5, 2);
            this.tableLayoutPanel1.Controls.Add(this.ST2, 4, 2);
            this.tableLayoutPanel1.Controls.Add(this.ST3, 3, 2);
            this.tableLayoutPanel1.Controls.Add(this.ST4, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.ST5, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.label8, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.label6, 5, 0);
            this.tableLayoutPanel1.Controls.Add(this.label5, 4, 0);
            this.tableLayoutPanel1.Controls.Add(this.label4, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.label3, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.label2, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel3, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.STReason, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.label7, 0, 1);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 179);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 8;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(925, 594);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // label23
            // 
            this.label23.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label23.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.label23, 6);
            this.label23.Location = new System.Drawing.Point(4, 496);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(917, 97);
            this.label23.TabIndex = 26;
            this.label23.Text = "系统将得分5、4、3、2、1折算成100%，即100%、80%、60%、30%、20%、10%创建人员对每次评价结果发送（保存）到外部服务数据库中，系统按照指定的" +
    "周期，进行汇总评价结果";
            // 
            // SQReason
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.SQReason, 5);
            this.SQReason.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SQReason.Location = new System.Drawing.Point(154, 398);
            this.SQReason.Margin = new System.Windows.Forms.Padding(0, 0, 15, 0);
            this.SQReason.Multiline = true;
            this.SQReason.Name = "SQReason";
            this.SQReason.Size = new System.Drawing.Size(755, 97);
            this.SQReason.TabIndex = 25;
            this.SQReason.Validating += new System.ComponentModel.CancelEventHandler(this.SQReason_Validating);
            // 
            // label22
            // 
            this.label22.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(4, 440);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(146, 12);
            this.label22.TabIndex = 24;
            this.label22.Text = "打分依据";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // SQ1
            // 
            this.SQ1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.SQ1.AutoSize = true;
            this.SQ1.Location = new System.Drawing.Point(773, 342);
            this.SQ1.Name = "SQ1";
            this.SQ1.Size = new System.Drawing.Size(148, 12);
            this.SQ1.TabIndex = 23;
            this.SQ1.Text = "标准";
            this.SQ1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // SQ2
            // 
            this.SQ2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.SQ2.AutoEllipsis = true;
            this.SQ2.AutoSize = true;
            this.SQ2.Location = new System.Drawing.Point(619, 342);
            this.SQ2.Name = "SQ2";
            this.SQ2.Size = new System.Drawing.Size(147, 12);
            this.SQ2.TabIndex = 22;
            this.SQ2.Text = "标准";
            this.SQ2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // SQ3
            // 
            this.SQ3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.SQ3.AutoSize = true;
            this.SQ3.Location = new System.Drawing.Point(465, 342);
            this.SQ3.Name = "SQ3";
            this.SQ3.Size = new System.Drawing.Size(147, 12);
            this.SQ3.TabIndex = 21;
            this.SQ3.Text = "标准";
            this.SQ3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // SQ4
            // 
            this.SQ4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.SQ4.AutoSize = true;
            this.SQ4.Location = new System.Drawing.Point(311, 342);
            this.SQ4.Name = "SQ4";
            this.SQ4.Size = new System.Drawing.Size(147, 12);
            this.SQ4.TabIndex = 20;
            this.SQ4.Text = "标准";
            this.SQ4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // SQ5
            // 
            this.SQ5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.SQ5.AutoSize = true;
            this.SQ5.Location = new System.Drawing.Point(157, 342);
            this.SQ5.Name = "SQ5";
            this.SQ5.Size = new System.Drawing.Size(147, 12);
            this.SQ5.TabIndex = 19;
            this.SQ5.Text = "标准";
            this.SQ5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label16
            // 
            this.label16.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(4, 342);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(146, 12);
            this.label16.TabIndex = 18;
            this.label16.Text = "标准";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel4
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.panel4, 5);
            this.panel4.Controls.Add(this.ServiceQScore5);
            this.panel4.Controls.Add(this.ServiceQScore2);
            this.panel4.Controls.Add(this.ServiceQScore4);
            this.panel4.Controls.Add(this.ServiceQScore3);
            this.panel4.Controls.Add(this.ServiceQScore1);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(154, 259);
            this.panel4.Margin = new System.Windows.Forms.Padding(0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(770, 40);
            this.panel4.TabIndex = 17;
            // 
            // ServiceQScore5
            // 
            this.ServiceQScore5.AutoSize = true;
            this.ServiceQScore5.Location = new System.Drawing.Point(681, 18);
            this.ServiceQScore5.Name = "ServiceQScore5";
            this.ServiceQScore5.Size = new System.Drawing.Size(14, 13);
            this.ServiceQScore5.TabIndex = 4;
            this.ServiceQScore5.TabStop = true;
            this.ServiceQScore5.Tag = "1";
            this.ServiceQScore5.UseVisualStyleBackColor = true;
            // 
            // ServiceQScore2
            // 
            this.ServiceQScore2.AutoSize = true;
            this.ServiceQScore2.Location = new System.Drawing.Point(220, 18);
            this.ServiceQScore2.Name = "ServiceQScore2";
            this.ServiceQScore2.Size = new System.Drawing.Size(14, 13);
            this.ServiceQScore2.TabIndex = 3;
            this.ServiceQScore2.TabStop = true;
            this.ServiceQScore2.Tag = "4";
            this.ServiceQScore2.UseVisualStyleBackColor = true;
            // 
            // ServiceQScore4
            // 
            this.ServiceQScore4.AutoSize = true;
            this.ServiceQScore4.Location = new System.Drawing.Point(531, 18);
            this.ServiceQScore4.Name = "ServiceQScore4";
            this.ServiceQScore4.Size = new System.Drawing.Size(14, 13);
            this.ServiceQScore4.TabIndex = 2;
            this.ServiceQScore4.TabStop = true;
            this.ServiceQScore4.Tag = "2";
            this.ServiceQScore4.UseVisualStyleBackColor = true;
            // 
            // ServiceQScore3
            // 
            this.ServiceQScore3.AutoSize = true;
            this.ServiceQScore3.Location = new System.Drawing.Point(375, 18);
            this.ServiceQScore3.Name = "ServiceQScore3";
            this.ServiceQScore3.Size = new System.Drawing.Size(14, 13);
            this.ServiceQScore3.TabIndex = 1;
            this.ServiceQScore3.TabStop = true;
            this.ServiceQScore3.Tag = "3";
            this.ServiceQScore3.UseVisualStyleBackColor = true;
            // 
            // ServiceQScore1
            // 
            this.ServiceQScore1.AutoSize = true;
            this.ServiceQScore1.Location = new System.Drawing.Point(70, 18);
            this.ServiceQScore1.Name = "ServiceQScore1";
            this.ServiceQScore1.Size = new System.Drawing.Size(14, 13);
            this.ServiceQScore1.TabIndex = 0;
            this.ServiceQScore1.TabStop = true;
            this.ServiceQScore1.Tag = "5";
            this.ServiceQScore1.UseVisualStyleBackColor = true;
            // 
            // label15
            // 
            this.label15.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(4, 273);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(146, 12);
            this.label15.TabIndex = 16;
            this.label15.Text = "服务质量";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label14
            // 
            this.label14.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(4, 203);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(146, 12);
            this.label14.TabIndex = 14;
            this.label14.Text = "打分依据";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ST1
            // 
            this.ST1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.ST1.AutoSize = true;
            this.ST1.Location = new System.Drawing.Point(773, 105);
            this.ST1.Name = "ST1";
            this.ST1.Size = new System.Drawing.Size(148, 12);
            this.ST1.TabIndex = 13;
            this.ST1.Text = "标准";
            this.ST1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ST2
            // 
            this.ST2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.ST2.AutoSize = true;
            this.ST2.Location = new System.Drawing.Point(619, 105);
            this.ST2.Name = "ST2";
            this.ST2.Size = new System.Drawing.Size(147, 12);
            this.ST2.TabIndex = 12;
            this.ST2.Text = "标准";
            this.ST2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ST3
            // 
            this.ST3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.ST3.AutoSize = true;
            this.ST3.Location = new System.Drawing.Point(465, 105);
            this.ST3.Name = "ST3";
            this.ST3.Size = new System.Drawing.Size(147, 12);
            this.ST3.TabIndex = 11;
            this.ST3.Text = "标准";
            this.ST3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ST4
            // 
            this.ST4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.ST4.AutoSize = true;
            this.ST4.Location = new System.Drawing.Point(311, 105);
            this.ST4.Name = "ST4";
            this.ST4.Size = new System.Drawing.Size(147, 12);
            this.ST4.TabIndex = 10;
            this.ST4.Text = "标准";
            this.ST4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ST5
            // 
            this.ST5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.ST5.AutoSize = true;
            this.ST5.Location = new System.Drawing.Point(157, 105);
            this.ST5.Name = "ST5";
            this.ST5.Size = new System.Drawing.Size(147, 12);
            this.ST5.TabIndex = 9;
            this.ST5.Text = "标准";
            this.ST5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(4, 105);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(146, 12);
            this.label8.TabIndex = 8;
            this.label8.Text = "标准";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(773, 5);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(148, 12);
            this.label6.TabIndex = 5;
            this.label6.Text = "1";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(619, 5);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(147, 12);
            this.label5.TabIndex = 4;
            this.label5.Text = "2";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(465, 5);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(147, 12);
            this.label4.TabIndex = 3;
            this.label4.Text = "3";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.label3.Location = new System.Drawing.Point(311, 5);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(147, 12);
            this.label3.TabIndex = 2;
            this.label3.Text = "4";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(157, 5);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(147, 12);
            this.label2.TabIndex = 1;
            this.label2.Text = "5";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(146, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "打分项";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel3
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.panel3, 5);
            this.panel3.Controls.Add(this.ServiceTimeScore5);
            this.panel3.Controls.Add(this.ServiceTimeScore2);
            this.panel3.Controls.Add(this.ServiceTimeScore4);
            this.panel3.Controls.Add(this.ServiceTimeScore3);
            this.panel3.Controls.Add(this.ServiceTimeScore1);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(154, 22);
            this.panel3.Margin = new System.Windows.Forms.Padding(0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(770, 40);
            this.panel3.TabIndex = 7;
            // 
            // ServiceTimeScore5
            // 
            this.ServiceTimeScore5.AutoSize = true;
            this.ServiceTimeScore5.Location = new System.Drawing.Point(681, 13);
            this.ServiceTimeScore5.Name = "ServiceTimeScore5";
            this.ServiceTimeScore5.Size = new System.Drawing.Size(14, 13);
            this.ServiceTimeScore5.TabIndex = 4;
            this.ServiceTimeScore5.TabStop = true;
            this.ServiceTimeScore5.Tag = "1";
            this.ServiceTimeScore5.UseVisualStyleBackColor = true;
            // 
            // ServiceTimeScore2
            // 
            this.ServiceTimeScore2.AutoSize = true;
            this.ServiceTimeScore2.Location = new System.Drawing.Point(220, 14);
            this.ServiceTimeScore2.Name = "ServiceTimeScore2";
            this.ServiceTimeScore2.Size = new System.Drawing.Size(14, 13);
            this.ServiceTimeScore2.TabIndex = 3;
            this.ServiceTimeScore2.TabStop = true;
            this.ServiceTimeScore2.Tag = "4";
            this.ServiceTimeScore2.UseVisualStyleBackColor = true;
            // 
            // ServiceTimeScore4
            // 
            this.ServiceTimeScore4.AutoSize = true;
            this.ServiceTimeScore4.Location = new System.Drawing.Point(531, 13);
            this.ServiceTimeScore4.Name = "ServiceTimeScore4";
            this.ServiceTimeScore4.Size = new System.Drawing.Size(14, 13);
            this.ServiceTimeScore4.TabIndex = 2;
            this.ServiceTimeScore4.TabStop = true;
            this.ServiceTimeScore4.Tag = "2";
            this.ServiceTimeScore4.UseVisualStyleBackColor = true;
            // 
            // ServiceTimeScore3
            // 
            this.ServiceTimeScore3.AutoSize = true;
            this.ServiceTimeScore3.Location = new System.Drawing.Point(375, 13);
            this.ServiceTimeScore3.Name = "ServiceTimeScore3";
            this.ServiceTimeScore3.Size = new System.Drawing.Size(14, 13);
            this.ServiceTimeScore3.TabIndex = 1;
            this.ServiceTimeScore3.TabStop = true;
            this.ServiceTimeScore3.Tag = "3";
            this.ServiceTimeScore3.UseVisualStyleBackColor = true;
            // 
            // ServiceTimeScore1
            // 
            this.ServiceTimeScore1.AutoSize = true;
            this.ServiceTimeScore1.Location = new System.Drawing.Point(70, 13);
            this.ServiceTimeScore1.Name = "ServiceTimeScore1";
            this.ServiceTimeScore1.Size = new System.Drawing.Size(14, 13);
            this.ServiceTimeScore1.TabIndex = 0;
            this.ServiceTimeScore1.TabStop = true;
            this.ServiceTimeScore1.Tag = "5";
            this.ServiceTimeScore1.UseVisualStyleBackColor = true;
            // 
            // STReason
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.STReason, 5);
            this.STReason.Dock = System.Windows.Forms.DockStyle.Fill;
            this.STReason.Location = new System.Drawing.Point(154, 161);
            this.STReason.Margin = new System.Windows.Forms.Padding(0, 0, 15, 0);
            this.STReason.Multiline = true;
            this.STReason.Name = "STReason";
            this.STReason.Size = new System.Drawing.Size(755, 97);
            this.STReason.TabIndex = 15;
            this.STReason.Validating += new System.ComponentModel.CancelEventHandler(this.STReason_Validating);
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(4, 36);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(146, 12);
            this.label7.TabIndex = 6;
            this.label7.Text = "服务及时性";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.AutoScroll = true;
            this.panel2.Controls.Add(this.MtGroupId);
            this.panel2.Controls.Add(this.label12);
            this.panel2.Controls.Add(this.mtId);
            this.panel2.Controls.Add(this.serviceModelName);
            this.panel2.Controls.Add(this.label32);
            this.panel2.Controls.Add(this.button4);
            this.panel2.Controls.Add(this.servicePlace);
            this.panel2.Controls.Add(this.reset);
            this.panel2.Controls.Add(this.button1);
            this.panel2.Controls.Add(this.createTime);
            this.panel2.Controls.Add(this.creator);
            this.panel2.Controls.Add(this.label27);
            this.panel2.Controls.Add(this.label28);
            this.panel2.Controls.Add(this.label29);
            this.panel2.Controls.Add(this.supplierId);
            this.panel2.Controls.Add(this.ExtServiceId);
            this.panel2.Controls.Add(this.label26);
            this.panel2.Controls.Add(this.label25);
            this.panel2.Controls.Add(this.label24);
            this.panel2.Location = new System.Drawing.Point(3, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(919, 170);
            this.panel2.TabIndex = 0;
            // 
            // MtGroupId
            // 
            this.MtGroupId.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.MtGroupId.FormattingEnabled = true;
            this.MtGroupId.Items.AddRange(new object[] {
            "T01",
            "T02",
            "T03",
            "T04"});
            this.MtGroupId.Location = new System.Drawing.Point(99, 59);
            this.MtGroupId.Name = "MtGroupId";
            this.MtGroupId.Size = new System.Drawing.Size(146, 20);
            this.MtGroupId.TabIndex = 26;
            this.MtGroupId.SelectedIndexChanged += new System.EventHandler(this.MtGroupId_SelectedIndexChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(15, 62);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(65, 12);
            this.label12.TabIndex = 24;
            this.label12.Text = "物料组编号";
            // 
            // mtId
            // 
            this.mtId.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.mtId.FormattingEnabled = true;
            this.mtId.Items.AddRange(new object[] {
            "T01",
            "T02",
            "T03",
            "T04"});
            this.mtId.Location = new System.Drawing.Point(99, 93);
            this.mtId.Name = "mtId";
            this.mtId.Size = new System.Drawing.Size(146, 20);
            this.mtId.TabIndex = 23;
            // 
            // serviceModelName
            // 
            this.serviceModelName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.serviceModelName.FormattingEnabled = true;
            this.serviceModelName.Location = new System.Drawing.Point(99, 131);
            this.serviceModelName.Name = "serviceModelName";
            this.serviceModelName.Size = new System.Drawing.Size(146, 20);
            this.serviceModelName.TabIndex = 22;
            this.serviceModelName.TextChanged += new System.EventHandler(this.serviceModel_TextChanged);
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(15, 134);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(53, 12);
            this.label32.TabIndex = 21;
            this.label32.Text = "评价模板";
            this.label32.Click += new System.EventHandler(this.label32_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(567, 135);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 18;
            this.button4.Text = "服务汇总";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // servicePlace
            // 
            this.servicePlace.Location = new System.Drawing.Point(420, 20);
            this.servicePlace.Name = "servicePlace";
            this.servicePlace.Size = new System.Drawing.Size(146, 21);
            this.servicePlace.TabIndex = 17;
            this.servicePlace.Validating += new System.ComponentModel.CancelEventHandler(this.servicePlace_Validating);
            // 
            // reset
            // 
            this.reset.Location = new System.Drawing.Point(451, 135);
            this.reset.Name = "reset";
            this.reset.Size = new System.Drawing.Size(75, 23);
            this.reset.TabIndex = 16;
            this.reset.Text = "重置";
            this.reset.UseVisualStyleBackColor = true;
            this.reset.Click += new System.EventHandler(this.reset_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(338, 135);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 15;
            this.button1.Text = "新建模板";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // createTime
            // 
            this.createTime.Location = new System.Drawing.Point(713, 27);
            this.createTime.Name = "createTime";
            this.createTime.Size = new System.Drawing.Size(146, 21);
            this.createTime.TabIndex = 10;
            // 
            // creator
            // 
            this.creator.Location = new System.Drawing.Point(713, 67);
            this.creator.Name = "creator";
            this.creator.Size = new System.Drawing.Size(146, 21);
            this.creator.TabIndex = 9;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(654, 30);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(53, 12);
            this.label27.TabIndex = 8;
            this.label27.Text = "创建时间";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(666, 70);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(41, 12);
            this.label28.TabIndex = 7;
            this.label28.Text = "创建者";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(347, 29);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(53, 12);
            this.label29.TabIndex = 6;
            this.label29.Text = "服务地点";
            // 
            // supplierId
            // 
            this.supplierId.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.supplierId.FormattingEnabled = true;
            this.supplierId.Location = new System.Drawing.Point(99, 21);
            this.supplierId.Name = "supplierId";
            this.supplierId.Size = new System.Drawing.Size(146, 20);
            this.supplierId.TabIndex = 5;
            this.supplierId.SelectedIndexChanged += new System.EventHandler(this.supplierId_SelectedIndexChanged);
            this.supplierId.Validating += new System.ComponentModel.CancelEventHandler(this.supplierId_Validating);
            // 
            // ExtServiceId
            // 
            this.ExtServiceId.Location = new System.Drawing.Point(420, 64);
            this.ExtServiceId.Name = "ExtServiceId";
            this.ExtServiceId.Size = new System.Drawing.Size(146, 21);
            this.ExtServiceId.TabIndex = 4;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(337, 67);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(77, 12);
            this.label26.TabIndex = 2;
            this.label26.Text = "外部服务编码";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(15, 96);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(53, 12);
            this.label25.TabIndex = 1;
            this.label25.Text = "物料编号";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(15, 30);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(65, 12);
            this.label24.TabIndex = 0;
            this.label24.Text = "供应商编号";
            // 
            // errorUser
            // 
            this.errorUser.ContainerControl = this;
            // 
            // ExServiceForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1192, 843);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "ExServiceForm";
            this.Text = "外部服务";
            this.Load += new System.EventHandler(this.ExServiceForm_Load);
            this.panel1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorUser)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox SQReason;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label SQ1;
        private System.Windows.Forms.Label SQ2;
        private System.Windows.Forms.Label SQ3;
        private System.Windows.Forms.Label SQ4;
        private System.Windows.Forms.Label SQ5;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.RadioButton ServiceQScore5;
        private System.Windows.Forms.RadioButton ServiceQScore2;
        private System.Windows.Forms.RadioButton ServiceQScore4;
        private System.Windows.Forms.RadioButton ServiceQScore3;
        private System.Windows.Forms.RadioButton ServiceQScore1;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label ST1;
        private System.Windows.Forms.Label ST2;
        private System.Windows.Forms.Label ST3;
        private System.Windows.Forms.Label ST4;
        private System.Windows.Forms.Label ST5;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox STReason;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox createTime;
        private System.Windows.Forms.TextBox creator;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.ComboBox supplierId;
        private System.Windows.Forms.TextBox ExtServiceId;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.RadioButton ServiceTimeScore5;
        private System.Windows.Forms.RadioButton ServiceTimeScore2;
        private System.Windows.Forms.RadioButton ServiceTimeScore4;
        private System.Windows.Forms.RadioButton ServiceTimeScore3;
        private System.Windows.Forms.RadioButton ServiceTimeScore1;
        private System.Windows.Forms.ErrorProvider errorUser;
        private System.Windows.Forms.Button reset;
        private System.Windows.Forms.TextBox servicePlace;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.ComboBox serviceModelName;
        private System.Windows.Forms.ComboBox mtId;
        private System.Windows.Forms.ComboBox MtGroupId;
        private System.Windows.Forms.Label label12;
    }
}