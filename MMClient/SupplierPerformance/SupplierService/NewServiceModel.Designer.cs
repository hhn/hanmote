﻿namespace MMClient.SupplierPerformance.SupplierService
{
    partial class NewServiceModel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.ST5 = new System.Windows.Forms.TextBox();
            this.SQ5 = new System.Windows.Forms.TextBox();
            this.ST4 = new System.Windows.Forms.TextBox();
            this.SQ4 = new System.Windows.Forms.TextBox();
            this.ST3 = new System.Windows.Forms.TextBox();
            this.SQ3 = new System.Windows.Forms.TextBox();
            this.ST2 = new System.Windows.Forms.TextBox();
            this.SQ2 = new System.Windows.Forms.TextBox();
            this.ST1 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.SQ1 = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.serviceModelName = new System.Windows.Forms.ComboBox();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.ST5, 2, 5);
            this.tableLayoutPanel1.Controls.Add(this.SQ5, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.ST4, 2, 4);
            this.tableLayoutPanel1.Controls.Add(this.SQ4, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.ST3, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.SQ3, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.ST2, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.SQ2, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.ST1, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.label3, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label2, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.label4, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label5, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.label6, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.label7, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.label8, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.SQ1, 1, 1);
            this.tableLayoutPanel1.GrowStyle = System.Windows.Forms.TableLayoutPanelGrowStyle.AddColumns;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(13, 51);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 6;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(899, 318);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // ST5
            // 
            this.ST5.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.ST5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ST5.Location = new System.Drawing.Point(480, 260);
            this.ST5.Margin = new System.Windows.Forms.Padding(0);
            this.ST5.Multiline = true;
            this.ST5.Name = "ST5";
            this.ST5.Size = new System.Drawing.Size(418, 57);
            this.ST5.TabIndex = 17;
            // 
            // SQ5
            // 
            this.SQ5.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.SQ5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SQ5.Location = new System.Drawing.Point(62, 260);
            this.SQ5.Margin = new System.Windows.Forms.Padding(0);
            this.SQ5.Multiline = true;
            this.SQ5.Name = "SQ5";
            this.SQ5.Size = new System.Drawing.Size(417, 57);
            this.SQ5.TabIndex = 16;
            // 
            // ST4
            // 
            this.ST4.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.ST4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ST4.Location = new System.Drawing.Point(480, 203);
            this.ST4.Margin = new System.Windows.Forms.Padding(0);
            this.ST4.Multiline = true;
            this.ST4.Name = "ST4";
            this.ST4.Size = new System.Drawing.Size(418, 56);
            this.ST4.TabIndex = 15;
            // 
            // SQ4
            // 
            this.SQ4.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.SQ4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SQ4.Location = new System.Drawing.Point(62, 203);
            this.SQ4.Margin = new System.Windows.Forms.Padding(0);
            this.SQ4.Multiline = true;
            this.SQ4.Name = "SQ4";
            this.SQ4.Size = new System.Drawing.Size(417, 56);
            this.SQ4.TabIndex = 14;
            // 
            // ST3
            // 
            this.ST3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.ST3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ST3.Location = new System.Drawing.Point(480, 146);
            this.ST3.Margin = new System.Windows.Forms.Padding(0);
            this.ST3.Multiline = true;
            this.ST3.Name = "ST3";
            this.ST3.Size = new System.Drawing.Size(418, 56);
            this.ST3.TabIndex = 13;
            // 
            // SQ3
            // 
            this.SQ3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.SQ3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SQ3.Location = new System.Drawing.Point(62, 146);
            this.SQ3.Margin = new System.Windows.Forms.Padding(0);
            this.SQ3.Multiline = true;
            this.SQ3.Name = "SQ3";
            this.SQ3.Size = new System.Drawing.Size(417, 56);
            this.SQ3.TabIndex = 12;
            // 
            // ST2
            // 
            this.ST2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.ST2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ST2.Location = new System.Drawing.Point(480, 89);
            this.ST2.Margin = new System.Windows.Forms.Padding(0);
            this.ST2.Multiline = true;
            this.ST2.Name = "ST2";
            this.ST2.Size = new System.Drawing.Size(418, 56);
            this.ST2.TabIndex = 11;
            // 
            // SQ2
            // 
            this.SQ2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.SQ2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SQ2.Location = new System.Drawing.Point(62, 89);
            this.SQ2.Margin = new System.Windows.Forms.Padding(0);
            this.SQ2.Multiline = true;
            this.SQ2.Name = "SQ2";
            this.SQ2.Size = new System.Drawing.Size(417, 56);
            this.SQ2.TabIndex = 10;
            // 
            // ST1
            // 
            this.ST1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.ST1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ST1.Location = new System.Drawing.Point(480, 32);
            this.ST1.Margin = new System.Windows.Forms.Padding(0);
            this.ST1.Multiline = true;
            this.ST1.Name = "ST1";
            this.ST1.Size = new System.Drawing.Size(418, 56);
            this.ST1.TabIndex = 9;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.Location = new System.Drawing.Point(483, 10);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(412, 12);
            this.label3.TabIndex = 2;
            this.label3.Text = "服务及时性";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // supplierLabel
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 10);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(54, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "分数";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(65, 10);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(411, 12);
            this.label2.TabIndex = 1;
            this.label2.Text = "服务质量";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(4, 54);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(54, 12);
            this.label4.TabIndex = 3;
            this.label4.Text = "1";
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(4, 111);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(54, 12);
            this.label5.TabIndex = 4;
            this.label5.Text = "2";
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(4, 168);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(54, 12);
            this.label6.TabIndex = 5;
            this.label6.Text = "3";
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(4, 225);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(54, 12);
            this.label7.TabIndex = 6;
            this.label7.Text = "4";
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(4, 282);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(54, 12);
            this.label8.TabIndex = 7;
            this.label8.Text = "5";
            // 
            // SQ1
            // 
            this.SQ1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.SQ1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SQ1.Location = new System.Drawing.Point(62, 32);
            this.SQ1.Margin = new System.Windows.Forms.Padding(0);
            this.SQ1.Multiline = true;
            this.SQ1.Name = "SQ1";
            this.SQ1.Size = new System.Drawing.Size(417, 56);
            this.SQ1.TabIndex = 8;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(732, 397);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "保存";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(837, 397);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 3;
            this.button2.Text = "关闭";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(18, 22);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(59, 12);
            this.label9.TabIndex = 4;
            this.label9.Text = "模板名称:";
            // 
            // serviceModelName
            // 
            this.serviceModelName.FormattingEnabled = true;
            this.serviceModelName.Location = new System.Drawing.Point(75, 18);
            this.serviceModelName.Name = "serviceModelName";
            this.serviceModelName.Size = new System.Drawing.Size(188, 20);
            this.serviceModelName.TabIndex = 7;
            // 
            // NewServiceModel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(962, 451);
            this.Controls.Add(this.serviceModelName);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "NewServiceModel";
            this.Text = "新建标准";
            this.Load += new System.EventHandler(this.NewServiceModel_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox ST5;
        private System.Windows.Forms.TextBox SQ5;
        private System.Windows.Forms.TextBox ST4;
        private System.Windows.Forms.TextBox SQ4;
        private System.Windows.Forms.TextBox ST3;
        private System.Windows.Forms.TextBox SQ3;
        private System.Windows.Forms.TextBox ST2;
        private System.Windows.Forms.TextBox SQ2;
        private System.Windows.Forms.TextBox ST1;
        private System.Windows.Forms.TextBox SQ1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox serviceModelName;
    }
}