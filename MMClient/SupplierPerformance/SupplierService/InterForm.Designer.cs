﻿namespace MMClient.SupplierPerformance.SupplierService
{
    partial class InterForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ReReason = new System.Windows.Forms.TextBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.radioButton6 = new System.Windows.Forms.RadioButton();
            this.radioButton7 = new System.Windows.Forms.RadioButton();
            this.radioButton8 = new System.Windows.Forms.RadioButton();
            this.radioButton9 = new System.Windows.Forms.RadioButton();
            this.radioButton10 = new System.Windows.Forms.RadioButton();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.Inno1 = new System.Windows.Forms.Label();
            this.Inno2 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.RE1 = new System.Windows.Forms.Label();
            this.RE2 = new System.Windows.Forms.Label();
            this.RE3 = new System.Windows.Forms.Label();
            this.RE4 = new System.Windows.Forms.Label();
            this.RE5 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.Inno3 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.UserService = new System.Windows.Forms.TextBox();
            this.label41 = new System.Windows.Forms.Label();
            this.US1 = new System.Windows.Forms.Label();
            this.US2 = new System.Windows.Forms.Label();
            this.US3 = new System.Windows.Forms.Label();
            this.US4 = new System.Windows.Forms.Label();
            this.US5 = new System.Windows.Forms.Label();
            this.Inno4 = new System.Windows.Forms.Label();
            this.Inno5 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.radioButton5 = new System.Windows.Forms.RadioButton();
            this.radioButton4 = new System.Windows.Forms.RadioButton();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.InnoReason = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.radioButton11 = new System.Windows.Forms.RadioButton();
            this.radioButton12 = new System.Windows.Forms.RadioButton();
            this.radioButton13 = new System.Windows.Forms.RadioButton();
            this.radioButton14 = new System.Windows.Forms.RadioButton();
            this.radioButton15 = new System.Windows.Forms.RadioButton();
            this.label35 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.supplierName = new System.Windows.Forms.Panel();
            this.reSetBtn = new System.Windows.Forms.Button();
            this.MtGroupId = new System.Windows.Forms.ComboBox();
            this.label19 = new System.Windows.Forms.Label();
            this.serviceModelName = new System.Windows.Forms.ComboBox();
            this.label18 = new System.Windows.Forms.Label();
            this.button4 = new System.Windows.Forms.Button();
            this.serviceSummry = new System.Windows.Forms.Button();
            this.mtId = new System.Windows.Forms.ComboBox();
            this.faName = new System.Windows.Forms.Label();
            this.factoryId = new System.Windows.Forms.ComboBox();
            this.ServiceTime = new System.Windows.Forms.TextBox();
            this.creator = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.supplierId = new System.Windows.Forms.ComboBox();
            this.genServiceId = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.errorUser = new System.Windows.Forms.ErrorProvider(this.components);
            this.panel4.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel1.SuspendLayout();
            this.supplierName.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorUser)).BeginInit();
            this.SuspendLayout();
            // 
            // ReReason
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.ReReason, 5);
            this.ReReason.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ReReason.Location = new System.Drawing.Point(82, 273);
            this.ReReason.Margin = new System.Windows.Forms.Padding(0, 0, 15, 0);
            this.ReReason.Multiline = true;
            this.ReReason.Name = "ReReason";
            this.ReReason.Size = new System.Drawing.Size(827, 62);
            this.ReReason.TabIndex = 25;
            this.ReReason.Validating += new System.ComponentModel.CancelEventHandler(this.ReReason_Validating);
            // 
            // panel4
            // 
            this.panel4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.SetColumnSpan(this.panel4, 5);
            this.panel4.Controls.Add(this.radioButton6);
            this.panel4.Controls.Add(this.radioButton7);
            this.panel4.Controls.Add(this.radioButton8);
            this.panel4.Controls.Add(this.radioButton9);
            this.panel4.Controls.Add(this.radioButton10);
            this.panel4.Location = new System.Drawing.Point(82, 179);
            this.panel4.Margin = new System.Windows.Forms.Padding(0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(842, 30);
            this.panel4.TabIndex = 17;
            // 
            // radioButton6
            // 
            this.radioButton6.AutoSize = true;
            this.radioButton6.Location = new System.Drawing.Point(752, 10);
            this.radioButton6.Name = "radioButton6";
            this.radioButton6.Size = new System.Drawing.Size(14, 13);
            this.radioButton6.TabIndex = 4;
            this.radioButton6.TabStop = true;
            this.radioButton6.Tag = "1";
            this.radioButton6.UseVisualStyleBackColor = true;
            // 
            // radioButton7
            // 
            this.radioButton7.AutoSize = true;
            this.radioButton7.Location = new System.Drawing.Point(243, 8);
            this.radioButton7.Name = "radioButton7";
            this.radioButton7.Size = new System.Drawing.Size(14, 13);
            this.radioButton7.TabIndex = 3;
            this.radioButton7.TabStop = true;
            this.radioButton7.Tag = "4";
            this.radioButton7.UseVisualStyleBackColor = true;
            // 
            // radioButton8
            // 
            this.radioButton8.AutoSize = true;
            this.radioButton8.Location = new System.Drawing.Point(575, 8);
            this.radioButton8.Name = "radioButton8";
            this.radioButton8.Size = new System.Drawing.Size(14, 13);
            this.radioButton8.TabIndex = 2;
            this.radioButton8.TabStop = true;
            this.radioButton8.Tag = "2";
            this.radioButton8.UseVisualStyleBackColor = true;
            // 
            // radioButton9
            // 
            this.radioButton9.AutoSize = true;
            this.radioButton9.Location = new System.Drawing.Point(409, 8);
            this.radioButton9.Name = "radioButton9";
            this.radioButton9.Size = new System.Drawing.Size(14, 13);
            this.radioButton9.TabIndex = 1;
            this.radioButton9.TabStop = true;
            this.radioButton9.Tag = "3";
            this.radioButton9.UseVisualStyleBackColor = true;
            // 
            // radioButton10
            // 
            this.radioButton10.AutoSize = true;
            this.radioButton10.Location = new System.Drawing.Point(76, 8);
            this.radioButton10.Name = "radioButton10";
            this.radioButton10.Size = new System.Drawing.Size(14, 13);
            this.radioButton10.TabIndex = 0;
            this.radioButton10.TabStop = true;
            this.radioButton10.Tag = "5";
            this.radioButton10.UseVisualStyleBackColor = true;
            // 
            // label15
            // 
            this.label15.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(4, 188);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(41, 12);
            this.label15.TabIndex = 16;
            this.label15.Text = "可靠性";
            // 
            // label14
            // 
            this.label14.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(4, 141);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(53, 12);
            this.label14.TabIndex = 14;
            this.label14.Text = "事实证据";
            // 
            // Inno1
            // 
            this.Inno1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.Inno1.AutoSize = true;
            this.Inno1.Location = new System.Drawing.Point(757, 78);
            this.Inno1.Name = "Inno1";
            this.Inno1.Size = new System.Drawing.Size(59, 12);
            this.Inno1.TabIndex = 13;
            this.Inno1.Text = "测试标准1";
            // 
            // Inno2
            // 
            this.Inno2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.Inno2.AutoSize = true;
            this.Inno2.Location = new System.Drawing.Point(589, 78);
            this.Inno2.Name = "Inno2";
            this.Inno2.Size = new System.Drawing.Size(59, 12);
            this.Inno2.TabIndex = 12;
            this.Inno2.Text = "测试标准1";
            // 
            // label22
            // 
            this.label22.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(4, 298);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(53, 12);
            this.label22.TabIndex = 24;
            this.label22.Text = "事实证据";
            // 
            // RE1
            // 
            this.RE1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.RE1.AutoSize = true;
            this.RE1.Location = new System.Drawing.Point(757, 235);
            this.RE1.Name = "RE1";
            this.RE1.Size = new System.Drawing.Size(59, 12);
            this.RE1.TabIndex = 23;
            this.RE1.Text = "测试标准2";
            // 
            // RE2
            // 
            this.RE2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.RE2.AutoSize = true;
            this.RE2.Location = new System.Drawing.Point(589, 235);
            this.RE2.Name = "RE2";
            this.RE2.Size = new System.Drawing.Size(59, 12);
            this.RE2.TabIndex = 22;
            this.RE2.Text = "测试标准2";
            // 
            // RE3
            // 
            this.RE3.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.RE3.AutoSize = true;
            this.RE3.Location = new System.Drawing.Point(421, 235);
            this.RE3.Name = "RE3";
            this.RE3.Size = new System.Drawing.Size(59, 12);
            this.RE3.TabIndex = 21;
            this.RE3.Text = "测试标准2";
            // 
            // RE4
            // 
            this.RE4.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.RE4.AutoSize = true;
            this.RE4.Location = new System.Drawing.Point(253, 235);
            this.RE4.Name = "RE4";
            this.RE4.Size = new System.Drawing.Size(59, 12);
            this.RE4.TabIndex = 20;
            this.RE4.Text = "测试标准2";
            // 
            // RE5
            // 
            this.RE5.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.RE5.AutoSize = true;
            this.RE5.Location = new System.Drawing.Point(85, 235);
            this.RE5.Name = "RE5";
            this.RE5.Size = new System.Drawing.Size(59, 12);
            this.RE5.TabIndex = 19;
            this.RE5.Text = "测试标准2";
            // 
            // label16
            // 
            this.label16.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(4, 235);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(29, 12);
            this.label16.TabIndex = 18;
            this.label16.Text = "标准";
            // 
            // label23
            // 
            this.label23.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label23.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.label23, 6);
            this.label23.Location = new System.Drawing.Point(4, 493);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(917, 66);
            this.label23.TabIndex = 26;
            this.label23.Text = "系统将得分5、4、3、2、1折算成100%，即100%、80%、60%、30%、20%、10%创建人员对每次评价结果发送（保存）到外部服务数据库中，系统按照指定的" +
    "周期，进行汇总评价结果";
            // 
            // Inno3
            // 
            this.Inno3.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.Inno3.AutoSize = true;
            this.Inno3.Location = new System.Drawing.Point(421, 78);
            this.Inno3.Name = "Inno3";
            this.Inno3.Size = new System.Drawing.Size(59, 12);
            this.Inno3.TabIndex = 11;
            this.Inno3.Text = "测试标准1";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel1.ColumnCount = 6;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.Controls.Add(this.UserService, 1, 9);
            this.tableLayoutPanel1.Controls.Add(this.label41, 0, 9);
            this.tableLayoutPanel1.Controls.Add(this.US1, 5, 8);
            this.tableLayoutPanel1.Controls.Add(this.US2, 4, 8);
            this.tableLayoutPanel1.Controls.Add(this.US3, 3, 8);
            this.tableLayoutPanel1.Controls.Add(this.US4, 2, 8);
            this.tableLayoutPanel1.Controls.Add(this.US5, 1, 8);
            this.tableLayoutPanel1.Controls.Add(this.ReReason, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.label22, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.RE1, 5, 5);
            this.tableLayoutPanel1.Controls.Add(this.RE2, 4, 5);
            this.tableLayoutPanel1.Controls.Add(this.RE3, 3, 5);
            this.tableLayoutPanel1.Controls.Add(this.RE4, 2, 5);
            this.tableLayoutPanel1.Controls.Add(this.RE5, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.label16, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.panel4, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.label15, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.label14, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.Inno1, 5, 2);
            this.tableLayoutPanel1.Controls.Add(this.Inno2, 4, 2);
            this.tableLayoutPanel1.Controls.Add(this.Inno3, 3, 2);
            this.tableLayoutPanel1.Controls.Add(this.Inno4, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.Inno5, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.label8, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.label7, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label6, 5, 0);
            this.tableLayoutPanel1.Controls.Add(this.label5, 4, 0);
            this.tableLayoutPanel1.Controls.Add(this.label4, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.label3, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.label2, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel3, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.InnoReason, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.label23, 0, 10);
            this.tableLayoutPanel1.Controls.Add(this.label34, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.panel5, 1, 7);
            this.tableLayoutPanel1.Controls.Add(this.label35, 0, 8);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 158);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 11;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(925, 560);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // UserService
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.UserService, 5);
            this.UserService.Dock = System.Windows.Forms.DockStyle.Fill;
            this.UserService.Location = new System.Drawing.Point(82, 430);
            this.UserService.Margin = new System.Windows.Forms.Padding(0, 0, 15, 0);
            this.UserService.Multiline = true;
            this.UserService.Name = "UserService";
            this.UserService.Size = new System.Drawing.Size(827, 62);
            this.UserService.TabIndex = 36;
            this.UserService.Validating += new System.ComponentModel.CancelEventHandler(this.UserService_Validating);
            // 
            // label41
            // 
            this.label41.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(4, 455);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(53, 12);
            this.label41.TabIndex = 35;
            this.label41.Text = "事实证据";
            // 
            // US1
            // 
            this.US1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.US1.AutoSize = true;
            this.US1.Location = new System.Drawing.Point(757, 392);
            this.US1.Name = "US1";
            this.US1.Size = new System.Drawing.Size(59, 12);
            this.US1.TabIndex = 34;
            this.US1.Text = "测试标准3";
            // 
            // US2
            // 
            this.US2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.US2.AutoSize = true;
            this.US2.Location = new System.Drawing.Point(589, 392);
            this.US2.Name = "US2";
            this.US2.Size = new System.Drawing.Size(59, 12);
            this.US2.TabIndex = 33;
            this.US2.Text = "测试标准3";
            // 
            // US3
            // 
            this.US3.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.US3.AutoSize = true;
            this.US3.Location = new System.Drawing.Point(421, 392);
            this.US3.Name = "US3";
            this.US3.Size = new System.Drawing.Size(59, 12);
            this.US3.TabIndex = 32;
            this.US3.Text = "测试标准3";
            // 
            // US4
            // 
            this.US4.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.US4.AutoSize = true;
            this.US4.Location = new System.Drawing.Point(253, 392);
            this.US4.Name = "US4";
            this.US4.Size = new System.Drawing.Size(59, 12);
            this.US4.TabIndex = 31;
            this.US4.Text = "测试标准3";
            // 
            // US5
            // 
            this.US5.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.US5.AutoSize = true;
            this.US5.Location = new System.Drawing.Point(85, 392);
            this.US5.Name = "US5";
            this.US5.Size = new System.Drawing.Size(59, 12);
            this.US5.TabIndex = 30;
            this.US5.Text = "测试标准3";
            // 
            // Inno4
            // 
            this.Inno4.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.Inno4.AutoSize = true;
            this.Inno4.Location = new System.Drawing.Point(253, 78);
            this.Inno4.Name = "Inno4";
            this.Inno4.Size = new System.Drawing.Size(59, 12);
            this.Inno4.TabIndex = 10;
            this.Inno4.Text = "测试标准1";
            // 
            // Inno5
            // 
            this.Inno5.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.Inno5.AutoSize = true;
            this.Inno5.Location = new System.Drawing.Point(85, 78);
            this.Inno5.Name = "Inno5";
            this.Inno5.Size = new System.Drawing.Size(59, 12);
            this.Inno5.TabIndex = 9;
            this.Inno5.Text = "测试标准1";
            // 
            // label8
            // 
            this.label8.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(4, 78);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(29, 12);
            this.label8.TabIndex = 8;
            this.label8.Text = "标准";
            // 
            // label7
            // 
            this.label7.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(4, 31);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(41, 12);
            this.label7.TabIndex = 6;
            this.label7.Text = "创新性";
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(757, 5);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(164, 12);
            this.label6.TabIndex = 5;
            this.label6.Text = "1";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(589, 5);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(161, 12);
            this.label5.TabIndex = 4;
            this.label5.Text = "2";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(421, 5);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(161, 12);
            this.label4.TabIndex = 3;
            this.label4.Text = "3";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(253, 5);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(161, 12);
            this.label3.TabIndex = 2;
            this.label3.Text = "4";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(85, 5);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(161, 12);
            this.label2.TabIndex = 1;
            this.label2.Text = "5";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "打分项";
            // 
            // panel3
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.panel3, 5);
            this.panel3.Controls.Add(this.radioButton5);
            this.panel3.Controls.Add(this.radioButton4);
            this.panel3.Controls.Add(this.radioButton3);
            this.panel3.Controls.Add(this.radioButton2);
            this.panel3.Controls.Add(this.radioButton1);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(82, 22);
            this.panel3.Margin = new System.Windows.Forms.Padding(0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(842, 30);
            this.panel3.TabIndex = 7;
            // 
            // radioButton5
            // 
            this.radioButton5.AutoSize = true;
            this.radioButton5.Location = new System.Drawing.Point(752, 9);
            this.radioButton5.Name = "radioButton5";
            this.radioButton5.Size = new System.Drawing.Size(14, 13);
            this.radioButton5.TabIndex = 4;
            this.radioButton5.TabStop = true;
            this.radioButton5.Tag = "1";
            this.radioButton5.UseVisualStyleBackColor = true;
            // 
            // radioButton4
            // 
            this.radioButton4.AutoSize = true;
            this.radioButton4.Location = new System.Drawing.Point(243, 8);
            this.radioButton4.Name = "radioButton4";
            this.radioButton4.Size = new System.Drawing.Size(14, 13);
            this.radioButton4.TabIndex = 3;
            this.radioButton4.TabStop = true;
            this.radioButton4.Tag = "4";
            this.radioButton4.UseVisualStyleBackColor = true;
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.Location = new System.Drawing.Point(575, 9);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(14, 13);
            this.radioButton3.TabIndex = 2;
            this.radioButton3.TabStop = true;
            this.radioButton3.Tag = "2";
            this.radioButton3.UseVisualStyleBackColor = true;
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(409, 8);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(14, 13);
            this.radioButton2.TabIndex = 1;
            this.radioButton2.TabStop = true;
            this.radioButton2.Tag = "3";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Location = new System.Drawing.Point(76, 9);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(14, 13);
            this.radioButton1.TabIndex = 0;
            this.radioButton1.TabStop = true;
            this.radioButton1.Tag = "5";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // InnoReason
            // 
            this.InnoReason.AllowDrop = true;
            this.tableLayoutPanel1.SetColumnSpan(this.InnoReason, 5);
            this.InnoReason.Dock = System.Windows.Forms.DockStyle.Fill;
            this.InnoReason.Location = new System.Drawing.Point(82, 116);
            this.InnoReason.Margin = new System.Windows.Forms.Padding(0, 0, 15, 0);
            this.InnoReason.Multiline = true;
            this.InnoReason.Name = "InnoReason";
            this.InnoReason.Size = new System.Drawing.Size(827, 62);
            this.InnoReason.TabIndex = 15;
            this.InnoReason.Validating += new System.ComponentModel.CancelEventHandler(this.InnoReason_Validating);
            // 
            // label34
            // 
            this.label34.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(4, 345);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(53, 12);
            this.label34.TabIndex = 27;
            this.label34.Text = "用户服务";
            // 
            // panel5
            // 
            this.panel5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.SetColumnSpan(this.panel5, 5);
            this.panel5.Controls.Add(this.radioButton11);
            this.panel5.Controls.Add(this.radioButton12);
            this.panel5.Controls.Add(this.radioButton13);
            this.panel5.Controls.Add(this.radioButton14);
            this.panel5.Controls.Add(this.radioButton15);
            this.panel5.Location = new System.Drawing.Point(82, 336);
            this.panel5.Margin = new System.Windows.Forms.Padding(0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(842, 30);
            this.panel5.TabIndex = 28;
            // 
            // radioButton11
            // 
            this.radioButton11.AutoSize = true;
            this.radioButton11.Location = new System.Drawing.Point(752, 8);
            this.radioButton11.Name = "radioButton11";
            this.radioButton11.Size = new System.Drawing.Size(14, 13);
            this.radioButton11.TabIndex = 4;
            this.radioButton11.TabStop = true;
            this.radioButton11.Tag = "1";
            this.radioButton11.UseVisualStyleBackColor = true;
            // 
            // radioButton12
            // 
            this.radioButton12.AutoSize = true;
            this.radioButton12.Location = new System.Drawing.Point(243, 9);
            this.radioButton12.Name = "radioButton12";
            this.radioButton12.Size = new System.Drawing.Size(14, 13);
            this.radioButton12.TabIndex = 3;
            this.radioButton12.TabStop = true;
            this.radioButton12.Tag = "4";
            this.radioButton12.UseVisualStyleBackColor = true;
            // 
            // radioButton13
            // 
            this.radioButton13.AutoSize = true;
            this.radioButton13.Location = new System.Drawing.Point(575, 8);
            this.radioButton13.Name = "radioButton13";
            this.radioButton13.Size = new System.Drawing.Size(14, 13);
            this.radioButton13.TabIndex = 2;
            this.radioButton13.TabStop = true;
            this.radioButton13.Tag = "2";
            this.radioButton13.UseVisualStyleBackColor = true;
            // 
            // radioButton14
            // 
            this.radioButton14.AutoSize = true;
            this.radioButton14.Location = new System.Drawing.Point(409, 9);
            this.radioButton14.Name = "radioButton14";
            this.radioButton14.Size = new System.Drawing.Size(14, 13);
            this.radioButton14.TabIndex = 1;
            this.radioButton14.TabStop = true;
            this.radioButton14.Tag = "3";
            this.radioButton14.UseVisualStyleBackColor = true;
            // 
            // radioButton15
            // 
            this.radioButton15.AutoSize = true;
            this.radioButton15.Location = new System.Drawing.Point(76, 8);
            this.radioButton15.Name = "radioButton15";
            this.radioButton15.Size = new System.Drawing.Size(14, 13);
            this.radioButton15.TabIndex = 0;
            this.radioButton15.TabStop = true;
            this.radioButton15.Tag = "5";
            this.radioButton15.UseVisualStyleBackColor = true;
            // 
            // label35
            // 
            this.label35.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(4, 392);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(29, 12);
            this.label35.TabIndex = 29;
            this.label35.Text = "标准";
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.button2);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.supplierName);
            this.panel1.Controls.Add(this.tableLayoutPanel1);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(961, 759);
            this.panel1.TabIndex = 1;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(828, 724);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 4;
            this.button2.Text = "返回";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(727, 724);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 3;
            this.button1.Text = "保存";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // supplierName
            // 
            this.supplierName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.supplierName.Controls.Add(this.reSetBtn);
            this.supplierName.Controls.Add(this.MtGroupId);
            this.supplierName.Controls.Add(this.label19);
            this.supplierName.Controls.Add(this.serviceModelName);
            this.supplierName.Controls.Add(this.label18);
            this.supplierName.Controls.Add(this.button4);
            this.supplierName.Controls.Add(this.serviceSummry);
            this.supplierName.Controls.Add(this.mtId);
            this.supplierName.Controls.Add(this.faName);
            this.supplierName.Controls.Add(this.factoryId);
            this.supplierName.Controls.Add(this.ServiceTime);
            this.supplierName.Controls.Add(this.creator);
            this.supplierName.Controls.Add(this.label27);
            this.supplierName.Controls.Add(this.label28);
            this.supplierName.Controls.Add(this.label29);
            this.supplierName.Controls.Add(this.supplierId);
            this.supplierName.Controls.Add(this.genServiceId);
            this.supplierName.Controls.Add(this.label26);
            this.supplierName.Controls.Add(this.label25);
            this.supplierName.Controls.Add(this.label24);
            this.supplierName.Location = new System.Drawing.Point(3, 3);
            this.supplierName.Name = "supplierName";
            this.supplierName.Size = new System.Drawing.Size(955, 149);
            this.supplierName.TabIndex = 2;
            // 
            // reSetBtn
            // 
            this.reSetBtn.Location = new System.Drawing.Point(573, 105);
            this.reSetBtn.Name = "reSetBtn";
            this.reSetBtn.Size = new System.Drawing.Size(75, 23);
            this.reSetBtn.TabIndex = 25;
            this.reSetBtn.Text = "重置";
            this.reSetBtn.UseVisualStyleBackColor = true;
            this.reSetBtn.Click += new System.EventHandler(this.reSetBtn_Click);
            // 
            // MtGroupId
            // 
            this.MtGroupId.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.MtGroupId.FormattingEnabled = true;
            this.MtGroupId.Items.AddRange(new object[] {
            "T01",
            "T02",
            "T03",
            "T04"});
            this.MtGroupId.Location = new System.Drawing.Point(99, 55);
            this.MtGroupId.Name = "MtGroupId";
            this.MtGroupId.Size = new System.Drawing.Size(146, 20);
            this.MtGroupId.TabIndex = 23;
            this.MtGroupId.SelectedIndexChanged += new System.EventHandler(this.MtGroupId_SelectedIndexChanged);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(15, 58);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(65, 12);
            this.label19.TabIndex = 22;
            this.label19.Text = "物料组编号";
            // 
            // serviceModelName
            // 
            this.serviceModelName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.serviceModelName.FormattingEnabled = true;
            this.serviceModelName.Location = new System.Drawing.Point(100, 122);
            this.serviceModelName.Name = "serviceModelName";
            this.serviceModelName.Size = new System.Drawing.Size(146, 20);
            this.serviceModelName.TabIndex = 21;
            this.serviceModelName.TextChanged += new System.EventHandler(this.serviceModelName_TextChanged);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(15, 130);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(53, 12);
            this.label18.TabIndex = 20;
            this.label18.Text = "选择模板";
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(352, 105);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 19;
            this.button4.Text = "新建模板";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // serviceSummry
            // 
            this.serviceSummry.Location = new System.Drawing.Point(465, 105);
            this.serviceSummry.Name = "serviceSummry";
            this.serviceSummry.Size = new System.Drawing.Size(75, 23);
            this.serviceSummry.TabIndex = 18;
            this.serviceSummry.Text = "服务汇总";
            this.serviceSummry.UseVisualStyleBackColor = true;
            this.serviceSummry.Click += new System.EventHandler(this.serviceSummry_Click);
            // 
            // mtId
            // 
            this.mtId.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.mtId.FormattingEnabled = true;
            this.mtId.Items.AddRange(new object[] {
            "T01",
            "T02",
            "T03",
            "T04"});
            this.mtId.Location = new System.Drawing.Point(99, 85);
            this.mtId.Name = "mtId";
            this.mtId.Size = new System.Drawing.Size(146, 20);
            this.mtId.TabIndex = 17;
            // 
            // faName
            // 
            this.faName.AutoSize = true;
            this.faName.Location = new System.Drawing.Point(463, 44);
            this.faName.Name = "faName";
            this.faName.Size = new System.Drawing.Size(53, 12);
            this.faName.TabIndex = 16;
            this.faName.Text = "工厂描述";
            // 
            // factoryId
            // 
            this.factoryId.FormattingEnabled = true;
            this.factoryId.Items.AddRange(new object[] {
            "W0001",
            "B0001",
            "G0001"});
            this.factoryId.Location = new System.Drawing.Point(431, 23);
            this.factoryId.Name = "factoryId";
            this.factoryId.Size = new System.Drawing.Size(146, 20);
            this.factoryId.TabIndex = 11;
            this.factoryId.TextChanged += new System.EventHandler(this.factoryId_TextChanged);
            this.factoryId.Validating += new System.ComponentModel.CancelEventHandler(this.factoryId_Validating);
            // 
            // ServiceTime
            // 
            this.ServiceTime.Location = new System.Drawing.Point(702, 58);
            this.ServiceTime.Name = "ServiceTime";
            this.ServiceTime.ReadOnly = true;
            this.ServiceTime.Size = new System.Drawing.Size(146, 21);
            this.ServiceTime.TabIndex = 10;
            // 
            // creator
            // 
            this.creator.Location = new System.Drawing.Point(702, 20);
            this.creator.Name = "creator";
            this.creator.ReadOnly = true;
            this.creator.Size = new System.Drawing.Size(146, 21);
            this.creator.TabIndex = 9;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(618, 67);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(53, 12);
            this.label27.TabIndex = 8;
            this.label27.Text = "创建时间";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(618, 31);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(41, 12);
            this.label28.TabIndex = 7;
            this.label28.Text = "创建者";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(347, 31);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(53, 12);
            this.label29.TabIndex = 6;
            this.label29.Text = "工厂编码";
            // 
            // supplierId
            // 
            this.supplierId.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.supplierId.FormattingEnabled = true;
            this.supplierId.Location = new System.Drawing.Point(99, 21);
            this.supplierId.Name = "supplierId";
            this.supplierId.Size = new System.Drawing.Size(146, 20);
            this.supplierId.TabIndex = 5;
            this.supplierId.SelectedIndexChanged += new System.EventHandler(this.supplierId_SelectedIndexChanged);

            // 
            // genServiceId
            // 
            this.genServiceId.Location = new System.Drawing.Point(431, 67);
            this.genServiceId.Name = "genServiceId";
            this.genServiceId.ReadOnly = true;
            this.genServiceId.Size = new System.Drawing.Size(146, 21);
            this.genServiceId.TabIndex = 4;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(347, 70);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(77, 12);
            this.label26.TabIndex = 2;
            this.label26.Text = "一般服务编码";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(15, 96);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(53, 12);
            this.label25.TabIndex = 1;
            this.label25.Text = "物料编号";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(15, 30);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(65, 12);
            this.label24.TabIndex = 0;
            this.label24.Text = "供应商编号";
            // 
            // errorUser
            // 
            this.errorUser.ContainerControl = this;
            // 
            // InterForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1203, 780);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "InterForm";
            this.Text = "一般服务";
            this.Load += new System.EventHandler(this.InterForm_Load);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.supplierName.ResumeLayout(false);
            this.supplierName.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorUser)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox ReReason;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label RE1;
        private System.Windows.Forms.Label RE2;
        private System.Windows.Forms.Label RE3;
        private System.Windows.Forms.Label RE4;
        private System.Windows.Forms.Label RE5;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.RadioButton radioButton6;
        private System.Windows.Forms.RadioButton radioButton7;
        private System.Windows.Forms.RadioButton radioButton8;
        private System.Windows.Forms.RadioButton radioButton9;
        private System.Windows.Forms.RadioButton radioButton10;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label Inno1;
        private System.Windows.Forms.Label Inno2;
        private System.Windows.Forms.Label Inno3;
        private System.Windows.Forms.Label Inno4;
        private System.Windows.Forms.Label Inno5;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.RadioButton radioButton5;
        private System.Windows.Forms.RadioButton radioButton4;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.TextBox InnoReason;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel supplierName;
        private System.Windows.Forms.ComboBox factoryId;
        private System.Windows.Forms.TextBox ServiceTime;
        private System.Windows.Forms.TextBox creator;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.ComboBox supplierId;
        private System.Windows.Forms.TextBox genServiceId;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label faName;
        private System.Windows.Forms.TextBox UserService;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label US1;
        private System.Windows.Forms.Label US2;
        private System.Windows.Forms.Label US3;
        private System.Windows.Forms.Label US4;
        private System.Windows.Forms.Label US5;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.RadioButton radioButton11;
        private System.Windows.Forms.RadioButton radioButton12;
        private System.Windows.Forms.RadioButton radioButton13;
        private System.Windows.Forms.RadioButton radioButton14;
        private System.Windows.Forms.RadioButton radioButton15;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.ComboBox mtId;
        private System.Windows.Forms.Button serviceSummry;
        private System.Windows.Forms.ErrorProvider errorUser;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.ComboBox serviceModelName;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.ComboBox MtGroupId;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Button reSetBtn;
    }
}