﻿namespace MMClient.SupplierPerformance.SupplierService
{
    partial class 一般服务模板
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.返回 = new System.Windows.Forms.Button();
            this.endTime = new System.Windows.Forms.DateTimePicker();
            this.searchBtn = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.startTime = new System.Windows.Forms.DateTimePicker();
            this.MaterialName = new System.Windows.Forms.ComboBox();
            this.MateGroupName = new System.Windows.Forms.ComboBox();
            this.supplierName = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.mtGroupName = new System.Windows.Forms.Label();
            this.供应商 = new System.Windows.Forms.Label();
            this.服务工单 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.供应商代码 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.供应商名称 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.物料组名称 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.物料名称 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.单位 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.时间 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.综合 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.创新评分 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.创新权重 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.可靠性评分 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.可靠性权重 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.用户服务评分 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.用户服务权重 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.服务工单,
            this.供应商代码,
            this.供应商名称,
            this.物料组名称,
            this.物料名称,
            this.单位,
            this.时间,
            this.综合,
            this.创新评分,
            this.创新权重,
            this.可靠性评分,
            this.可靠性权重,
            this.用户服务评分,
            this.用户服务权重});
            this.dataGridView1.Location = new System.Drawing.Point(152, 142);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.Size = new System.Drawing.Size(1163, 339);
            this.dataGridView1.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.dataGridView1);
            this.panel1.Location = new System.Drawing.Point(-143, -52);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1318, 541);
            this.panel1.TabIndex = 1;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.返回);
            this.panel2.Controls.Add(this.endTime);
            this.panel2.Controls.Add(this.searchBtn);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.startTime);
            this.panel2.Controls.Add(this.MaterialName);
            this.panel2.Controls.Add(this.MateGroupName);
            this.panel2.Controls.Add(this.supplierName);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.mtGroupName);
            this.panel2.Controls.Add(this.供应商);
            this.panel2.Location = new System.Drawing.Point(152, 64);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1163, 72);
            this.panel2.TabIndex = 1;
            // 
            // 返回
            // 
            this.返回.Location = new System.Drawing.Point(632, 40);
            this.返回.Name = "返回";
            this.返回.Size = new System.Drawing.Size(75, 23);
            this.返回.TabIndex = 27;
            this.返回.Text = "返回";
            this.返回.UseVisualStyleBackColor = true;
            this.返回.Click += new System.EventHandler(this.返回_Click);
            // 
            // endTime
            // 
            this.endTime.Location = new System.Drawing.Point(471, 11);
            this.endTime.Name = "endTime";
            this.endTime.Size = new System.Drawing.Size(126, 21);
            this.endTime.TabIndex = 26;
            // 
            // searchBtn
            // 
            this.searchBtn.Location = new System.Drawing.Point(522, 41);
            this.searchBtn.Name = "searchBtn";
            this.searchBtn.Size = new System.Drawing.Size(75, 23);
            this.searchBtn.TabIndex = 25;
            this.searchBtn.Text = "查询";
            this.searchBtn.UseVisualStyleBackColor = true;
            this.searchBtn.Click += new System.EventHandler(this.searchBtn_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(448, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(17, 12);
            this.label2.TabIndex = 24;
            this.label2.Text = "--";
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(255, 17);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 23;
            this.label1.Text = "日期选择";
            // 
            // startTime
            // 
            this.startTime.Location = new System.Drawing.Point(316, 11);
            this.startTime.Name = "startTime";
            this.startTime.Size = new System.Drawing.Size(126, 21);
            this.startTime.TabIndex = 22;
            // 
            // MaterialName
            // 
            this.MaterialName.FormattingEnabled = true;
            this.MaterialName.Location = new System.Drawing.Point(316, 43);
            this.MaterialName.Name = "MaterialName";
            this.MaterialName.Size = new System.Drawing.Size(126, 20);
            this.MaterialName.TabIndex = 21;
            // 
            // MateGroupName
            // 
            this.MateGroupName.FormattingEnabled = true;
            this.MateGroupName.Location = new System.Drawing.Point(84, 43);
            this.MateGroupName.Name = "MateGroupName";
            this.MateGroupName.Size = new System.Drawing.Size(127, 20);
            this.MateGroupName.TabIndex = 20;
            this.MateGroupName.TextChanged += new System.EventHandler(this.MateGroupName_TextChanged);
            // 
            // supplierName
            // 
            this.supplierName.FormattingEnabled = true;
            this.supplierName.Location = new System.Drawing.Point(84, 14);
            this.supplierName.Name = "supplierName";
            this.supplierName.Size = new System.Drawing.Size(127, 20);
            this.supplierName.TabIndex = 19;
            this.supplierName.TextChanged += new System.EventHandler(this.supplierName_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(255, 46);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 12);
            this.label3.TabIndex = 18;
            this.label3.Text = "物料";
            // 
            // mtGroupName
            // 
            this.mtGroupName.AutoSize = true;
            this.mtGroupName.Location = new System.Drawing.Point(13, 46);
            this.mtGroupName.Name = "mtGroupName";
            this.mtGroupName.Size = new System.Drawing.Size(41, 12);
            this.mtGroupName.TabIndex = 17;
            this.mtGroupName.Text = "物料组";
            // 
            // 供应商
            // 
            this.供应商.AutoSize = true;
            this.供应商.Location = new System.Drawing.Point(13, 18);
            this.供应商.Name = "供应商";
            this.供应商.Size = new System.Drawing.Size(65, 12);
            this.供应商.TabIndex = 16;
            this.供应商.Text = "供应商名称";
            // 
            // 服务工单
            // 
            this.服务工单.DataPropertyName = "GenServiceId";
            this.服务工单.HeaderText = "服务工单";
            this.服务工单.Name = "服务工单";
            // 
            // 供应商代码
            // 
            this.供应商代码.DataPropertyName = "supplierId";
            this.供应商代码.HeaderText = "供应商代码";
            this.供应商代码.Name = "供应商代码";
            // 
            // 供应商名称
            // 
            this.供应商名称.DataPropertyName = "supplierName";
            this.供应商名称.HeaderText = "供应商名称";
            this.供应商名称.Name = "供应商名称";
            // 
            // 物料组名称
            // 
            this.物料组名称.DataPropertyName = "MtGroupName";
            this.物料组名称.HeaderText = "物料组名称";
            this.物料组名称.Name = "物料组名称";
            // 
            // 物料名称
            // 
            this.物料名称.DataPropertyName = "mtName";
            this.物料名称.HeaderText = "物料名称";
            this.物料名称.Name = "物料名称";
            // 
            // 单位
            // 
            this.单位.DataPropertyName = "danWei";
            this.单位.HeaderText = "单位";
            this.单位.Name = "单位";
            // 
            // 时间
            // 
            this.时间.DataPropertyName = "ServiceTime";
            this.时间.HeaderText = "时间";
            this.时间.Name = "时间";
            // 
            // 综合
            // 
            this.综合.DataPropertyName = "serviceTotalScore";
            this.综合.HeaderText = "综合";
            this.综合.Name = "综合";
            // 
            // 创新评分
            // 
            this.创新评分.DataPropertyName = "InnovationSco";
            this.创新评分.HeaderText = "创新评分";
            this.创新评分.Name = "创新评分";
            // 
            // 创新权重
            // 
            this.创新权重.DataPropertyName = "InRate";
            this.创新权重.HeaderText = "创新权重";
            this.创新权重.Name = "创新权重";
            // 
            // 可靠性评分
            // 
            this.可靠性评分.DataPropertyName = "RealizableSco";
            this.可靠性评分.HeaderText = "可靠性评分";
            this.可靠性评分.Name = "可靠性评分";
            // 
            // 可靠性权重
            // 
            this.可靠性权重.DataPropertyName = "ReRate";
            this.可靠性权重.HeaderText = "可靠性权重";
            this.可靠性权重.Name = "可靠性权重";
            // 
            // 用户服务评分
            // 
            this.用户服务评分.DataPropertyName = "UserServiceSco";
            this.用户服务评分.HeaderText = "用户服务评分";
            this.用户服务评分.Name = "用户服务评分";
            // 
            // 用户服务权重
            // 
            this.用户服务权重.DataPropertyName = "UserRate";
            this.用户服务权重.HeaderText = "用户服务权重";
            this.用户服务权重.Name = "用户服务权重";
            // 
            // 一般服务模板
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1187, 501);
            this.Controls.Add(this.panel1);
            this.Name = "一般服务模板";
            this.Text = "一般服务模板";
            this.Load += new System.EventHandler(this.GeneServiceSum_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button searchBtn;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker startTime;
        private System.Windows.Forms.ComboBox MaterialName;
        private System.Windows.Forms.ComboBox MateGroupName;
        private System.Windows.Forms.ComboBox supplierName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label mtGroupName;
        private System.Windows.Forms.Label 供应商;
        private System.Windows.Forms.DateTimePicker endTime;
        private System.Windows.Forms.Button 返回;
        private System.Windows.Forms.DataGridViewTextBoxColumn 服务工单;
        private System.Windows.Forms.DataGridViewTextBoxColumn 供应商代码;
        private System.Windows.Forms.DataGridViewTextBoxColumn 供应商名称;
        private System.Windows.Forms.DataGridViewTextBoxColumn 物料组名称;
        private System.Windows.Forms.DataGridViewTextBoxColumn 物料名称;
        private System.Windows.Forms.DataGridViewTextBoxColumn 单位;
        private System.Windows.Forms.DataGridViewTextBoxColumn 时间;
        private System.Windows.Forms.DataGridViewTextBoxColumn 综合;
        private System.Windows.Forms.DataGridViewTextBoxColumn 创新评分;
        private System.Windows.Forms.DataGridViewTextBoxColumn 创新权重;
        private System.Windows.Forms.DataGridViewTextBoxColumn 可靠性评分;
        private System.Windows.Forms.DataGridViewTextBoxColumn 可靠性权重;
        private System.Windows.Forms.DataGridViewTextBoxColumn 用户服务评分;
        private System.Windows.Forms.DataGridViewTextBoxColumn 用户服务权重;
    }
}