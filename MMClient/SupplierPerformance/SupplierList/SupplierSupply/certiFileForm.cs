﻿using Lib.SqlServerDAL;
using MMClient.SupplierPerformance.SPReport;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MMClient.SupplierPerformance.SupplierList.SupplierSupply
{
    public partial class CertiFileForm : WeifenLuo.WinFormsUI.Docking.DockContent 
    {  
        public CertiFileForm()
        {
            InitializeComponent();    
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void certiFileForm_Load(object sender, EventArgs e)
        {
            String sqlText = @"select DISTINCT supplierId from TabCertiFile";
            List<String> list = new List<string>();
            DataTable dt = DBHelper.ExecuteQueryDT(sqlText);
            for (int i = 0; i < dt.Rows.Count; i++) {
                list.Add(dt.Rows[i][0].ToString());

            }
            CB_SupplierId.DataSource = list;
        }

        private void CB_SupplierId_SelectedIndexChanged(object sender, EventArgs e)
        {
            LB_supplierName.Text = "供应商" + CB_SupplierId.Text;
            System.DateTime currentTime = new System.DateTime();
            currentTime = System.DateTime.Now;
            DateTime expTime= new System.DateTime();
            int days = 0;

            String sqlText = @"select tcft.CertiFileType as fileType,fileName,regTime,expTime,explainText,orgName from TabCertiFile tcf,TabCertiFileType tcft where supplierId='"+CB_SupplierId.Text.ToString()+ "' and tcft.CertiFileID=tcf.fileType";
            dgv_certiFile.DataSource = DBHelper.ExecuteQueryDT(sqlText);
            
            foreach (DataGridViewRow r in dgv_certiFile.Rows) {
                expTime = Convert.ToDateTime(r.Cells["expTime"].Value.ToString());
                days = (expTime - currentTime).Days;
                if (days <= 30 && days > 0)
                {
                    r.Cells["expTime"].Style.BackColor = Color.Orange;
                    r.Cells["expTime"].ToolTipText = days + "天后过期";
                }
                else if(days<0){

                    r.Cells["expTime"].Style.BackColor = Color.Red;
                    r.Cells["expTime"].ToolTipText = "已过期";
                }
            }
        }

        private void btn_SelectPurchaseGroup_Click(object sender, EventArgs e)
        {
            Point btnLocation = this.btn_SelectPurchaseGroup.PointToScreen(new Point(0, 0));
            int btnWidth = this.btn_SelectPurchaseGroup.Width;
            Point formShowLocation = new Point(btnLocation.X + btnWidth, btnLocation.Y);

            Frm_SelectBuyerOrg frm_SelectBuyerOrg = new Frm_SelectBuyerOrg(formShowLocation);
            DialogResult result = frm_SelectBuyerOrg.ShowDialog();
            if (result == DialogResult.OK)
            {
                //为采购组织编号赋值
                this.CB_SupplierId.Text = frm_SelectBuyerOrg.purchaseId;
                //为采购组织名称赋值
                this.CB_SupplierName.Text = frm_SelectBuyerOrg.purchaseName;
            }
        }
    }
}
