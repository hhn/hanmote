﻿namespace MMClient.SupplierPerformance.SupplierList.SupplierSupply
{
    partial class CertiFileForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dgv_certiFile = new System.Windows.Forms.DataGridView();
            this.label7 = new System.Windows.Forms.Label();
            this.CB_SupplierId = new System.Windows.Forms.ComboBox();
            this.LB_supplierName = new System.Windows.Forms.Label();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fileType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fileName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.orgName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.regTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.expTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.explainText = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btn_SelectPurchaseGroup = new System.Windows.Forms.Button();
            this.B_SupplierId = new System.Windows.Forms.TextBox();
            this.CB_SupplierName = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_certiFile)).BeginInit();
            this.SuspendLayout();
            // 
            // supplierInforGroupBox
            // 
            this.groupBox1.Controls.Add(this.dgv_certiFile);
            this.groupBox1.Location = new System.Drawing.Point(12, 66);
            this.groupBox1.Name = "supplierInforGroupBox";
            this.groupBox1.Size = new System.Drawing.Size(746, 572);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "供应商证书";
            // 
            // dgv_certiFile
            // 
            this.dgv_certiFile.AllowUserToAddRows = false;
            this.dgv_certiFile.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_certiFile.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.dgv_certiFile.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgv_certiFile.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_certiFile.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.fileType,
            this.fileName,
            this.orgName,
            this.regTime,
            this.expTime,
            this.explainText});
            this.dgv_certiFile.Location = new System.Drawing.Point(10, 20);
            this.dgv_certiFile.Name = "dgv_certiFile";
            this.dgv_certiFile.RowTemplate.Height = 23;
            this.dgv_certiFile.Size = new System.Drawing.Size(730, 546);
            this.dgv_certiFile.TabIndex = 6;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(20, 15);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 12);
            this.label7.TabIndex = 8;
            this.label7.Text = "供应商：";
            // 
            // CB_SupplierId
            // 
            this.CB_SupplierId.FormattingEnabled = true;
            this.CB_SupplierId.Location = new System.Drawing.Point(80, 15);
            this.CB_SupplierId.Name = "CB_SupplierId";
            this.CB_SupplierId.Size = new System.Drawing.Size(142, 20);
            this.CB_SupplierId.TabIndex = 9;
            this.CB_SupplierId.SelectedIndexChanged += new System.EventHandler(this.CB_SupplierId_SelectedIndexChanged);
            // 
            // LB_supplierName
            // 
            this.LB_supplierName.AutoSize = true;
            this.LB_supplierName.Location = new System.Drawing.Point(237, 18);
            this.LB_supplierName.Name = "LB_supplierName";
            this.LB_supplierName.Size = new System.Drawing.Size(0, 12);
            this.LB_supplierName.TabIndex = 11;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "fileType";
            this.dataGridViewTextBoxColumn1.HeaderText = "文件类型";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Width = 115;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "fileName";
            this.dataGridViewTextBoxColumn2.HeaderText = "文件名称";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.Width = 114;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "orgName";
            this.dataGridViewTextBoxColumn3.HeaderText = "颁发组织";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.Width = 115;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "regTime";
            this.dataGridViewTextBoxColumn4.HeaderText = "获得时间";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.Width = 114;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "expTime";
            this.dataGridViewTextBoxColumn5.HeaderText = "过期时间";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.Width = 115;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "explainText";
            this.dataGridViewTextBoxColumn6.HeaderText = "说明";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.Width = 114;
            // 
            // fileType
            // 
            this.fileType.DataPropertyName = "fileType";
            this.fileType.HeaderText = "文件类型";
            this.fileType.Name = "fileType";
            // 
            // fileName
            // 
            this.fileName.DataPropertyName = "fileName";
            this.fileName.HeaderText = "文件名称";
            this.fileName.Name = "fileName";
            // 
            // orgName
            // 
            this.orgName.DataPropertyName = "orgName";
            this.orgName.HeaderText = "颁发组织";
            this.orgName.Name = "orgName";
            // 
            // regTime
            // 
            this.regTime.DataPropertyName = "regTime";
            this.regTime.HeaderText = "获得时间";
            this.regTime.Name = "regTime";
            // 
            // expTime
            // 
            this.expTime.DataPropertyName = "expTime";
            this.expTime.HeaderText = "过期时间";
            this.expTime.Name = "expTime";
            // 
            // explainText
            // 
            this.explainText.DataPropertyName = "explainText";
            this.explainText.HeaderText = "说明";
            this.explainText.Name = "explainText";
            // 
            // btn_SelectPurchaseGroup
            // 
            this.btn_SelectPurchaseGroup.BackColor = System.Drawing.Color.Transparent;
            this.btn_SelectPurchaseGroup.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_SelectPurchaseGroup.FlatAppearance.BorderColor = System.Drawing.SystemColors.Control;
            this.btn_SelectPurchaseGroup.FlatAppearance.BorderSize = 0;
            this.btn_SelectPurchaseGroup.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btn_SelectPurchaseGroup.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btn_SelectPurchaseGroup.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_SelectPurchaseGroup.Image = global::MMClient.Properties.Resources.search;
            this.btn_SelectPurchaseGroup.Location = new System.Drawing.Point(386, 40);
            this.btn_SelectPurchaseGroup.Name = "btn_SelectPurchaseGroup";
            this.btn_SelectPurchaseGroup.Size = new System.Drawing.Size(19, 22);
            this.btn_SelectPurchaseGroup.TabIndex = 15;
            this.btn_SelectPurchaseGroup.UseVisualStyleBackColor = false;
            this.btn_SelectPurchaseGroup.Visible = false;
            this.btn_SelectPurchaseGroup.Click += new System.EventHandler(this.btn_SelectPurchaseGroup_Click);
            // 
            // B_SupplierId
            // 
            this.B_SupplierId.Location = new System.Drawing.Point(80, 39);
            this.B_SupplierId.Name = "B_SupplierId";
            this.B_SupplierId.Size = new System.Drawing.Size(142, 21);
            this.B_SupplierId.TabIndex = 16;
            this.B_SupplierId.Visible = false;
            // 
            // CB_SupplierName
            // 
            this.CB_SupplierName.Location = new System.Drawing.Point(228, 40);
            this.CB_SupplierName.Name = "CB_SupplierName";
            this.CB_SupplierName.Size = new System.Drawing.Size(142, 21);
            this.CB_SupplierName.TabIndex = 17;
            this.CB_SupplierName.Visible = false;
            // 
            // CertiFileForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(763, 650);
            this.Controls.Add(this.CB_SupplierName);
            this.Controls.Add(this.B_SupplierId);
            this.Controls.Add(this.btn_SelectPurchaseGroup);
            this.Controls.Add(this.LB_supplierName);
            this.Controls.Add(this.CB_SupplierId);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "CertiFileForm";
            this.Text = "证书详情";
            this.Load += new System.EventHandler(this.certiFileForm_Load);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_certiFile)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox CB_SupplierId;
        private System.Windows.Forms.DataGridView dgv_certiFile;
        private System.Windows.Forms.Label LB_supplierName;
        private System.Windows.Forms.DataGridViewTextBoxColumn fileType;
        private System.Windows.Forms.DataGridViewTextBoxColumn fileName;
        private System.Windows.Forms.DataGridViewTextBoxColumn orgName;
        private System.Windows.Forms.DataGridViewTextBoxColumn regTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn expTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn explainText;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.Button btn_SelectPurchaseGroup;
        private System.Windows.Forms.TextBox B_SupplierId;
        private System.Windows.Forms.TextBox CB_SupplierName;
    }
}