﻿namespace MMClient.SupplierPerformance.SupplierList
{
    partial class ManualMaintLevel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnQueryInfo = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.eTime = new System.Windows.Forms.DateTimePicker();
            this.sTime = new System.Windows.Forms.DateTimePicker();
            this.pageNext1 = new pager.pagetool.pageNext();
            this.dgv_levelInfo = new System.Windows.Forms.DataGridView();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RowNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SupplierId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SupplierName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.supplierType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.level = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.levelDescription = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnDownLevel = new System.Windows.Forms.DataGridViewButtonColumn();
            this.BtnUpLevel = new System.Windows.Forms.DataGridViewButtonColumn();
            this.CB_SupplierName = new System.Windows.Forms.ComboBox();
            this.label = new System.Windows.Forms.Label();
            this.CB_SupplierId = new System.Windows.Forms.ComboBox();
            this.CB_purOrg = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_levelInfo)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.btnQueryInfo);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.eTime);
            this.groupBox1.Controls.Add(this.sTime);
            this.groupBox1.Controls.Add(this.pageNext1);
            this.groupBox1.Controls.Add(this.dgv_levelInfo);
            this.groupBox1.Controls.Add(this.CB_SupplierName);
            this.groupBox1.Controls.Add(this.label);
            this.groupBox1.Controls.Add(this.CB_SupplierId);
            this.groupBox1.Controls.Add(this.CB_purOrg);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(13, 5);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1034, 729);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "信息";
            // 
            // btnQueryInfo
            // 
            this.btnQueryInfo.Location = new System.Drawing.Point(550, 76);
            this.btnQueryInfo.Name = "btnQueryInfo";
            this.btnQueryInfo.Size = new System.Drawing.Size(75, 23);
            this.btnQueryInfo.TabIndex = 27;
            this.btnQueryInfo.Text = "查询";
            this.btnQueryInfo.UseVisualStyleBackColor = true;
            this.btnQueryInfo.Click += new System.EventHandler(this.btnQueryInfo_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(241, 76);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(17, 12);
            this.label5.TabIndex = 26;
            this.label5.Text = "--";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(33, 76);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 12);
            this.label4.TabIndex = 25;
            this.label4.Text = "起止时间：";
            // 
            // eTime
            // 
            this.eTime.Location = new System.Drawing.Point(273, 70);
            this.eTime.Name = "eTime";
            this.eTime.Size = new System.Drawing.Size(121, 21);
            this.eTime.TabIndex = 24;
            // 
            // sTime
            // 
            this.sTime.Location = new System.Drawing.Point(104, 70);
            this.sTime.Name = "sTime";
            this.sTime.Size = new System.Drawing.Size(121, 21);
            this.sTime.TabIndex = 23;
            // 
            // pageNext1
            // 
            this.pageNext1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.pageNext1.Location = new System.Drawing.Point(6, 686);
            this.pageNext1.Name = "pageNext1";
            this.pageNext1.PageIndex = 1;
            this.pageNext1.PageSize = 100;
            this.pageNext1.RecordCount = 0;
            this.pageNext1.Size = new System.Drawing.Size(660, 37);
            this.pageNext1.TabIndex = 22;
            this.pageNext1.OnPageChanged += new System.EventHandler(this.pageNext1_OnPageChanged);
            // 
            // dgv_levelInfo
            // 
            this.dgv_levelInfo.AllowUserToAddRows = false;
            this.dgv_levelInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgv_levelInfo.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_levelInfo.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.dgv_levelInfo.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgv_levelInfo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_levelInfo.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ID,
            this.RowNumber,
            this.SupplierId,
            this.SupplierName,
            this.supplierType,
            this.level,
            this.levelDescription,
            this.btnDownLevel,
            this.BtnUpLevel});
            this.dgv_levelInfo.Location = new System.Drawing.Point(6, 108);
            this.dgv_levelInfo.MaximumSize = new System.Drawing.Size(1500, 1000);
            this.dgv_levelInfo.Name = "dgv_levelInfo";
            this.dgv_levelInfo.RowTemplate.Height = 23;
            this.dgv_levelInfo.Size = new System.Drawing.Size(1022, 572);
            this.dgv_levelInfo.TabIndex = 15;
            this.dgv_levelInfo.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // ID
            // 
            this.ID.DataPropertyName = "ID";
            this.ID.HeaderText = "ID";
            this.ID.Name = "ID";
            this.ID.Visible = false;
            // 
            // RowNumber
            // 
            this.RowNumber.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.RowNumber.DataPropertyName = "RowNumber";
            this.RowNumber.HeaderText = "序号";
            this.RowNumber.Name = "RowNumber";
            this.RowNumber.Width = 54;
            // 
            // SupplierId
            // 
            this.SupplierId.DataPropertyName = "Supplier_ID";
            this.SupplierId.HeaderText = "供应商编号";
            this.SupplierId.Name = "SupplierId";
            // 
            // SupplierName
            // 
            this.SupplierName.DataPropertyName = "companyName";
            this.SupplierName.HeaderText = "供应商名称";
            this.SupplierName.Name = "SupplierName";
            // 
            // supplierType
            // 
            this.supplierType.DataPropertyName = "supplierType";
            this.supplierType.HeaderText = "供应商类型";
            this.supplierType.Name = "supplierType";
            this.supplierType.ReadOnly = true;
            // 
            // level
            // 
            this.level.DataPropertyName = "Supplier_Position";
            this.level.HeaderText = "优先级";
            this.level.Name = "level";
            // 
            // levelDescription
            // 
            this.levelDescription.DataPropertyName = "levelDescription";
            this.levelDescription.HeaderText = "优先级描述";
            this.levelDescription.Name = "levelDescription";
            this.levelDescription.ReadOnly = true;
            // 
            // btnDownLevel
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.NullValue = "降级";
            this.btnDownLevel.DefaultCellStyle = dataGridViewCellStyle1;
            this.btnDownLevel.HeaderText = "降级";
            this.btnDownLevel.Name = "btnDownLevel";
            this.btnDownLevel.Text = "降级";
            // 
            // BtnUpLevel
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.NullValue = "升级";
            this.BtnUpLevel.DefaultCellStyle = dataGridViewCellStyle2;
            this.BtnUpLevel.HeaderText = "升级";
            this.BtnUpLevel.Name = "BtnUpLevel";
            this.BtnUpLevel.Text = "升级";
            // 
            // CB_SupplierName
            // 
            this.CB_SupplierName.FormattingEnabled = true;
            this.CB_SupplierName.Location = new System.Drawing.Point(597, 31);
            this.CB_SupplierName.Name = "CB_SupplierName";
            this.CB_SupplierName.Size = new System.Drawing.Size(131, 20);
            this.CB_SupplierName.TabIndex = 21;
            // 
            // label
            // 
            this.label.AutoSize = true;
            this.label.Location = new System.Drawing.Point(520, 34);
            this.label.Name = "label";
            this.label.Size = new System.Drawing.Size(71, 12);
            this.label.TabIndex = 20;
            this.label.Text = "供应商名称:";
            // 
            // CB_SupplierId
            // 
            this.CB_SupplierId.FormattingEnabled = true;
            this.CB_SupplierId.Location = new System.Drawing.Point(354, 32);
            this.CB_SupplierId.Name = "CB_SupplierId";
            this.CB_SupplierId.Size = new System.Drawing.Size(121, 20);
            this.CB_SupplierId.TabIndex = 19;
            this.CB_SupplierId.SelectedIndexChanged += new System.EventHandler(this.CB_SupplierId_SelectedIndexChanged);
            // 
            // CB_purOrg
            // 
            this.CB_purOrg.FormattingEnabled = true;
            this.CB_purOrg.Location = new System.Drawing.Point(104, 32);
            this.CB_purOrg.Name = "CB_purOrg";
            this.CB_purOrg.Size = new System.Drawing.Size(121, 20);
            this.CB_purOrg.TabIndex = 18;
            this.CB_purOrg.SelectedIndexChanged += new System.EventHandler(this.CB_purOrg_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(271, 37);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 12);
            this.label2.TabIndex = 17;
            this.label2.Text = "供应商编号：";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(33, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 12);
            this.label1.TabIndex = 16;
            this.label1.Text = "采购组织：";
            // 
            // ManualMaintLevel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1059, 746);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "ManualMaintLevel";
            this.Text = "优先级维护";
            this.Load += new System.EventHandler(this.ManualMaintLevel_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_levelInfo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private pager.pagetool.pageNext pageNext1;
        private System.Windows.Forms.DataGridView dgv_levelInfo;
        private System.Windows.Forms.ComboBox CB_SupplierName;
        private System.Windows.Forms.Label label;
        private System.Windows.Forms.ComboBox CB_SupplierId;
        private System.Windows.Forms.ComboBox CB_purOrg;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker eTime;
        private System.Windows.Forms.DateTimePicker sTime;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnQueryInfo;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn RowNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn SupplierId;
        private System.Windows.Forms.DataGridViewTextBoxColumn SupplierName;
        private System.Windows.Forms.DataGridViewTextBoxColumn supplierType;
        private System.Windows.Forms.DataGridViewTextBoxColumn level;
        private System.Windows.Forms.DataGridViewTextBoxColumn levelDescription;
        private System.Windows.Forms.DataGridViewButtonColumn btnDownLevel;
        private System.Windows.Forms.DataGridViewButtonColumn BtnUpLevel;
    }
}