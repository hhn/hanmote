﻿namespace MMClient.SupplierPerformance.SupplierList
{
    partial class supplySource
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.CB_purOrg = new System.Windows.Forms.ComboBox();
            this.label = new System.Windows.Forms.Label();
            this.CB_mtGroupName = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dgv_MaterialInfo = new System.Windows.Forms.DataGridView();
            this.Material_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pageNext_Material = new pager.pagetool.pageNext();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.dgv_supplyInfo = new System.Windows.Forms.DataGridView();
            this.RowNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.supplyStatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PN_supplierInfo = new pager.pagetool.pageNext();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.groupBox1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_MaterialInfo)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_supplyInfo)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.CB_purOrg);
            this.groupBox1.Controls.Add(this.label);
            this.groupBox1.Controls.Add(this.CB_mtGroupName);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(824, 69);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "条件设置";
            // 
            // CB_purOrg
            // 
            this.CB_purOrg.FormattingEnabled = true;
            this.CB_purOrg.Location = new System.Drawing.Point(71, 23);
            this.CB_purOrg.Name = "CB_purOrg";
            this.CB_purOrg.Size = new System.Drawing.Size(121, 20);
            this.CB_purOrg.TabIndex = 6;
            this.CB_purOrg.SelectedIndexChanged += new System.EventHandler(this.CB_purOrg_SelectedIndexChanged);
            // 
            // label
            // 
            this.label.AutoSize = true;
            this.label.Location = new System.Drawing.Point(12, 26);
            this.label.Name = "label";
            this.label.Size = new System.Drawing.Size(53, 12);
            this.label.TabIndex = 5;
            this.label.Text = "采购组织";
            // 
            // CB_mtGroupName
            // 
            this.CB_mtGroupName.FormattingEnabled = true;
            this.CB_mtGroupName.Location = new System.Drawing.Point(325, 23);
            this.CB_mtGroupName.Name = "CB_mtGroupName";
            this.CB_mtGroupName.Size = new System.Drawing.Size(121, 20);
            this.CB_mtGroupName.TabIndex = 4;
            this.CB_mtGroupName.SelectedIndexChanged += new System.EventHandler(this.CB_mtGroupName_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(266, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "物料组：";
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Controls.Add(this.groupBox3);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1493, 590);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "供应源";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox2.Controls.Add(this.dgv_MaterialInfo);
            this.groupBox2.Controls.Add(this.pageNext_Material);
            this.groupBox2.Location = new System.Drawing.Point(6, 6);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(693, 578);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "物料信息";
            // 
            // dgv_MaterialInfo
            // 
            this.dgv_MaterialInfo.AllowUserToAddRows = false;
            this.dgv_MaterialInfo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.dgv_MaterialInfo.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_MaterialInfo.BackgroundColor = System.Drawing.SystemColors.HighlightText;
            this.dgv_MaterialInfo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgv_MaterialInfo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_MaterialInfo.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Material_ID,
            this.dataGridViewTextBoxColumn2});
            this.dgv_MaterialInfo.Location = new System.Drawing.Point(4, 20);
            this.dgv_MaterialInfo.Name = "dgv_MaterialInfo";
            this.dgv_MaterialInfo.RowTemplate.Height = 23;
            this.dgv_MaterialInfo.Size = new System.Drawing.Size(681, 512);
            this.dgv_MaterialInfo.TabIndex = 2;
            this.dgv_MaterialInfo.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_MaterialInfo_CellClick);
            // 
            // Material_ID
            // 
            this.Material_ID.DataPropertyName = "Material_ID";
            this.Material_ID.HeaderText = "物料编码";
            this.Material_ID.Name = "Material_ID";
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "Material_Name";
            this.dataGridViewTextBoxColumn2.HeaderText = "物料描述";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // pageNext_Material
            // 
            this.pageNext_Material.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.pageNext_Material.Location = new System.Drawing.Point(6, 538);
            this.pageNext_Material.Name = "pageNext_Material";
            this.pageNext_Material.PageIndex = 1;
            this.pageNext_Material.PageSize = 100;
            this.pageNext_Material.RecordCount = 0;
            this.pageNext_Material.Size = new System.Drawing.Size(645, 34);
            this.pageNext_Material.TabIndex = 1;
            this.pageNext_Material.OnPageChanged += new System.EventHandler(this.pageNext_Material_OnPageChanged);
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox3.Controls.Add(this.dgv_supplyInfo);
            this.groupBox3.Controls.Add(this.PN_supplierInfo);
            this.groupBox3.Location = new System.Drawing.Point(706, 6);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(781, 578);
            this.groupBox3.TabIndex = 5;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "供货信息";
            // 
            // dgv_supplyInfo
            // 
            this.dgv_supplyInfo.AllowUserToAddRows = false;
            this.dgv_supplyInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgv_supplyInfo.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_supplyInfo.BackgroundColor = System.Drawing.SystemColors.HighlightText;
            this.dgv_supplyInfo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgv_supplyInfo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_supplyInfo.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.RowNumber,
            this.Column3,
            this.Column1,
            this.Column2,
            this.supplyStatus});
            this.dgv_supplyInfo.Location = new System.Drawing.Point(6, 20);
            this.dgv_supplyInfo.Name = "dgv_supplyInfo";
            this.dgv_supplyInfo.RowTemplate.Height = 23;
            this.dgv_supplyInfo.Size = new System.Drawing.Size(769, 512);
            this.dgv_supplyInfo.TabIndex = 0;
            // 
            // RowNumber
            // 
            this.RowNumber.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.RowNumber.DataPropertyName = "RowNumber";
            this.RowNumber.HeaderText = "序号";
            this.RowNumber.Name = "RowNumber";
            this.RowNumber.Width = 54;
            // 
            // Column3
            // 
            this.Column3.DataPropertyName = "Supplier_Position";
            this.Column3.HeaderText = "优先级";
            this.Column3.Name = "Column3";
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "Supplier_Name";
            this.Column1.HeaderText = "供应商名称";
            this.Column1.Name = "Column1";
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "SupplierId";
            this.Column2.HeaderText = "供应商编号";
            this.Column2.Name = "Column2";
            // 
            // supplyStatus
            // 
            dataGridViewCellStyle1.NullValue = "有供货";
            this.supplyStatus.DefaultCellStyle = dataGridViewCellStyle1;
            this.supplyStatus.HeaderText = "供货状态";
            this.supplyStatus.Name = "supplyStatus";
            this.supplyStatus.ReadOnly = true;
            // 
            // PN_supplierInfo
            // 
            this.PN_supplierInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.PN_supplierInfo.Location = new System.Drawing.Point(6, 538);
            this.PN_supplierInfo.Name = "PN_supplierInfo";
            this.PN_supplierInfo.PageIndex = 1;
            this.PN_supplierInfo.PageSize = 100;
            this.PN_supplierInfo.RecordCount = 0;
            this.PN_supplierInfo.Size = new System.Drawing.Size(645, 34);
            this.PN_supplierInfo.TabIndex = 3;
            this.PN_supplierInfo.OnPageChanged += new System.EventHandler(this.PN_supplierInfo_OnPageChanged);
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Location = new System.Drawing.Point(12, 87);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1501, 616);
            this.tabControl1.TabIndex = 1;
            // 
            // supplySource
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1525, 715);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.tabControl1);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "supplySource";
            this.Text = "供应商管理策略";
            this.Load += new System.EventHandler(this.supplySource_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_MaterialInfo)).EndInit();
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_supplyInfo)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox CB_mtGroupName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox CB_purOrg;
        private System.Windows.Forms.Label label;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView dgv_MaterialInfo;
        private System.Windows.Forms.DataGridViewTextBoxColumn Material_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private pager.pagetool.pageNext pageNext_Material;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.DataGridView dgv_supplyInfo;
        private System.Windows.Forms.DataGridViewTextBoxColumn RowNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn supplyStatus;
        private pager.pagetool.pageNext PN_supplierInfo;
        private System.Windows.Forms.TabControl tabControl1;
    }
}