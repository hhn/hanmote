﻿using Lib.Bll.supplierListBll;
using Lib.Common.CommonUtils;
using Lib.Model.supplierListMode;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;

namespace MMClient.SupplierPerformance.SupplierList
{
    public partial class supplySource : DockContent
    {
        SupplierClassifyList SupplierListBll = new SupplierClassifyList();
        queryCondition queryCondition = new queryCondition();
        public supplySource()
        {
            InitializeComponent();
        }

        private void supplySource_Load(object sender, EventArgs e)
        {
            //加载基础信息：采购组织、物料组
            loadBaseInfo();
            
        }
        /// <summary>
        /// 加载所有物料的供应源
        /// </summary>
        private void loadSupplyInfo()
        {
            try
            {
                if (CB_mtGroupName.SelectedValue == null) {
                    return;
                }
                
                queryCondition.MtGroupName = CB_mtGroupName.SelectedValue.ToString();
                queryCondition.PurOrg = CB_purOrg.SelectedValue.ToString();
                queryCondition.PageSize = this.PN_supplierInfo.PageSize;
                queryCondition.PageIndex = this.PN_supplierInfo.PageIndex;
                if (dgv_MaterialInfo.CurrentRow != null)
                {
                    queryCondition.MtId = dgv_MaterialInfo.CurrentRow.Cells["Material_ID"].Value.ToString();
                }
                else {
                    queryCondition.MtId = "";
                }
                
                dgv_MaterialInfo.AutoGenerateColumns = false;
                dgv_supplyInfo.DataSource = SupplierListBll.getSupplySupplierInfo(queryCondition);

            }
            catch (Exception ex)
            {

                MessageUtil.ShowError("加载供应源失败！");
            }
        }
        /// <summary>
        /// 加载基础信息
        /// </summary>
        private void loadBaseInfo()
        {
            //加载采购组织
            CB_purOrg.DataSource = SupplierListBll.getPurOrg();
            CB_purOrg.DisplayMember = "Buyer_Org_Name";
            CB_purOrg.ValueMember = "Buyer_Org";
        }

        private void CB_purOrg_SelectedIndexChanged(object sender, EventArgs e)
        {
            String purOrg = "";
            //加载物料组
            if (CB_purOrg.SelectedValue != null) {
                purOrg = CB_purOrg.SelectedValue.ToString();
            }
            DataTable dt = SupplierListBll.getMtGroup(purOrg);
            CB_mtGroupName.DataSource = dt;
            CB_mtGroupName.DisplayMember = "MtGroup_Name";
            CB_mtGroupName.ValueMember = "MtGroup_ID";

        }

        private void CB_mtGroupName_SelectedIndexChanged(object sender, EventArgs e)
        {
            //加载物料数据
            pageNext_Material_Load();
        
        }

        private void pageNext_Material_OnPageChanged(object sender, EventArgs e)
        {
            //加载物料数据
            loadMtInfo();
        }

        private void PN_supplierInfo_OnPageChanged(object sender, EventArgs e)
        {
            //加载供应商信息
            loadSupplyInfo();

        }
        /// <summary>
        /// 选择单元格事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgv_MaterialInfo_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            //加载供应商信息
            loadSupplyInfo();

        }
        /// <summary>
        /// 加载物料信息
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pageNext_Material_Load()
        {
            string mtGroupId = "";
            if (CB_mtGroupName.SelectedValue != null) {

                mtGroupId = CB_mtGroupName.SelectedValue.ToString();
            }
            int count = SupplierListBll.getMtInfoCount(mtGroupId); ;
            pageNext_Material.DrawControl(count);
            this.pageNext_Material.OnPageChanged += new System.EventHandler(this.pageNext_Material_OnPageChanged);

            loadMtInfo();

        }
        /// <summary>
        /// 获取物料信息
        /// </summary>
        private void loadMtInfo()
        {
            try
            {
                //加载物料信息
                dgv_MaterialInfo.DataSource = SupplierListBll.getMtName(CB_purOrg.SelectedValue.ToString(), CB_mtGroupName.SelectedValue.ToString());
                if (dgv_MaterialInfo.Rows.Count != 0) {
                    //默认选中第一行
                    dgv_MaterialInfo.Rows[0].Selected = true;
                }
            }
            catch (Exception ex)
            {

                MessageUtil.ShowError("物料信息加载失败！");
            } 


            
        }
        /// <summary>
        /// 加载供应商供货信息
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PN_supplierInfo_Load()
        {
            if (dgv_MaterialInfo.CurrentRow != null)
            {
                queryCondition.MtId = dgv_MaterialInfo.CurrentRow.Cells["Material_ID"].Value.ToString();

                int count = SupplierListBll.getSupplierInfoCount(queryCondition);
                PN_supplierInfo.DrawControl(count);
                this.PN_supplierInfo.OnPageChanged += new System.EventHandler(this.PN_supplierInfo_OnPageChanged);
                loadSupplyInfo();
            }
           
        }

    }
}
