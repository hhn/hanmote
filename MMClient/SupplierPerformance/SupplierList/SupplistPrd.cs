﻿
using Lib.Bll.supplierListBll;
using System;
using Lib.Model.supplierListMode;
using Lib.Common.CommonUtils;
using System.Data;

namespace MMClient.SupplierPerformance.SupplierList
{
    public partial class SupplistPrd : WeifenLuo.WinFormsUI.Docking.DockContent
    {

        SupplierClassifyList SupplierListBll = new SupplierClassifyList();
        queryCondition queryCondition = new queryCondition();
        SupplierListModel supplierListModel = new SupplierListModel();
        private int total;

        public SupplistPrd()
        {
            InitializeComponent();
            pageNext1.PageSize = 10;
            dgv_SupplierInfo.AutoGenerateColumns = false;
        }

        /// <summary>
        /// 分页控件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pageNext1_Load()
        {
            queryCondition.PurOrg = this.CB_purOrg.Text.Trim();
            queryCondition.MtGroupName = this.CB_mtGroup.Text.Trim();
            queryCondition.MtId = this.CB_mtId.SelectedValue.ToString();
            total = SupplierListBll.getListCounts(queryCondition);
            pageNext1.DrawControl(total);
            loadData();
        }
        /// <summary>
        /// 加载数据
        /// </summary>
        private void loadData()
        {
            queryCondition.PageSize = this.pageNext1.PageSize;
            queryCondition.PageIndex = this.pageNext1.PageIndex;

            queryCondition.PurOrg = this.CB_purOrg.Text.Trim();
            queryCondition.MtGroupName = this.CB_mtGroup.Text.Trim();
            queryCondition.MtId = this.CB_mtId.SelectedValue.ToString();
            dgv_SupplierInfo.DataSource = SupplierListBll.getSupplierInfo(queryCondition);
        }

        private void SupplistPrd_Load(object sender, EventArgs e)
        {
            //加载采购组织
            CB_purOrg.DisplayMember = "Buyer_Org_Name";
            CB_purOrg.ValueMember = "Buyer_Org";
            CB_purOrg.DataSource = SupplierListBll.getPurOrg();
            DataTable dt= SupplierListBll.getExistedList(this.CB_purOrg.SelectedValue.ToString());
            //加载已经存在的清单
            foreach (DataRow item in dt.Rows)
            {
                ListBox_exsitedList.Items.Add(item[0].ToString());
            }

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

            //加载物料组
            CB_mtGroup.DisplayMember = "MtGroup_Name";
            CB_mtGroup.ValueMember = "MtGroup_ID";
            CB_mtGroup.DataSource = SupplierListBll.getMtGroup(CB_purOrg.SelectedValue.ToString());
        }

        private void CB_mtGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            //加载物料数据
            CB_mtId.DisplayMember = "Material_Name";
            CB_mtId.ValueMember = "Material_ID"; 
            CB_mtId.DataSource = SupplierListBll.getMtName(CB_purOrg.SelectedValue.ToString(), CB_mtGroup.SelectedValue.ToString());
        }
        
        private void CB_mtName_SelectedIndexChanged(object sender, EventArgs e)
        {
            //生成清单号
            TimeSpan cha = (DateTime.Now - TimeZone.CurrentTimeZone.ToLocalTime(new System.DateTime(1970, 1, 1)));
            long t = (long)cha.TotalSeconds;
            if (CB_mtId.SelectedValue != null) {
                int length = CB_mtId.SelectedValue.ToString().Length;
                TB_SupplierListId.Text = t.ToString() + CB_mtId.SelectedValue.ToString().Substring(length - 4, 4);
            }
            //清单描述
            TB_Des.Text = CB_purOrg.Text + "-->" + CB_mtGroup.Text + "-->" + CB_mtId.Text + "供应商清单";
            //加载供应商的数据
            pageNext1_Load();
        }
        //生成清单
        private void 保存ToolStripMenuItem_Click(object sender, EventArgs e)
        {

            //输入检查
            if (dataValidate()) {
                //抬头信息
                supplierListModel.PurOrgId = CB_purOrg.SelectedValue.ToString();
                supplierListModel.MtId = CB_mtId.SelectedValue.ToString();
                supplierListModel.SupplierListid1 = TB_SupplierListId.Text;
                supplierListModel.Description = TB_Des.Text;
                supplierListModel.CreateTime = System.DateTime.Now;
                Boolean flag = SupplierListBll.saveSupplierListInfo(dgv_SupplierInfo, supplierListModel);
                if (flag)
                {

                    MessageUtil.ShowTips("保存成功");
                    //更新已存在订单
                    ListBox_exsitedList.Items.Add(TB_SupplierListId.Text);
                }
                else
                {
                    MessageUtil.ShowError("保存失败！请重试");

                }
            }
        }
        /// <summary>
        /// 检查输入
        /// </summary>
        private bool dataValidate()
        {
            bool flag = true;
            if (TB_SupplierListId.Text.Equals("") || TB_SupplierListId.Text == "") {
                MessageUtil.ShowError("清单号为空！");
                flag = false;
            }
           
            if (CB_purOrg.Text.Equals("") || CB_purOrg.Text == "")
            {
                MessageUtil.ShowError("未选择采购组织");
                flag = false;

            }
            else {
                if (CB_mtGroup.Text.Equals("") || CB_mtGroup.Text == "")
                {
                    MessageUtil.ShowError("未选择物料组");
                    flag = false;

                }
                if (CB_mtId.Text.Equals("") || CB_mtId.Text == "")
                {
                    MessageUtil.ShowError("未选择物料");
                    flag = false;

                }

            }

            return flag;

        }

        private void ListBox_exsitedList_SelectedIndexChanged(object sender, EventArgs e)
        {
            //读取供应商的清单信息
           dgv_SupplierInfo.DataSource = SupplierListBll.getExistedListInfo(ListBox_exsitedList.SelectedItem.ToString());
        }
        /// <summary>
        /// 翻页操作
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pageNext1_OnPageChanged(object sender, EventArgs e)
        {
            loadData();
        }
    }
}
