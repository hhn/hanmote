﻿using Lib.Bll.supplierListBll;
using Lib.Common.CommonUtils;
using Lib.Model.supplierListMode;
using Lib.SqlServerDAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;

namespace MMClient.SupplierPerformance.SupplierList
{
    public partial class ManualMaintLevel : DockContent
    {
        SupplierClassifyList SupplierListBll = new SupplierClassifyList();
        queryCondition queryCondition = new queryCondition();
        ShowResonForm ShowResonForm = null;
        public ManualMaintLevel()
        {
            InitializeComponent();
        }
        /// <summary>
        /// 加载数据
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ManualMaintLevel_Load(object sender, EventArgs e)
        {   
            //初始化数据
            initData();
            
        }
        /// <summary>
        /// 初始化数据
        /// </summary>
        private void initData()
        {
            //加载采购组织
            this.CB_purOrg.DisplayMember= "Buyer_Org_Name";
            this.CB_purOrg.ValueMember= "Buyer_Org";
            this.CB_purOrg.DataSource = SupplierListBll.getPurOrg();
            //当前时间往前一年
            this.sTime.Value = DateTime.Now.AddYears(-1);
            //加载表格数据
            pageNext1_Load();
        }

        /// <summary>
        /// 加载列表数据
        /// </summary>
        private void loadData()
        {
            queryCondition.PurOrg = this.CB_purOrg.Text.ToString();
            queryCondition.SupplierId = this.CB_SupplierId.Text.ToString();
            queryCondition.SupplierName = this.CB_SupplierName.Text.ToString();
            queryCondition.STime = this.sTime.Value;
            queryCondition.ETime = this.eTime.Value;
            queryCondition.PageIndex = pageNext1.PageIndex;
            queryCondition.PageSize = pageNext1.PageSize;
            dgv_levelInfo.AutoGenerateColumns = false;
            dgv_levelInfo.DataSource = SupplierListBll.getSupplierLevelInfo(queryCondition);
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
           string supplier = dgv_levelInfo.CurrentRow.Cells["SupplierId"].Value.ToString();
            string level = dgv_levelInfo.CurrentRow.Cells["level"].Value.ToString();
            setLevel(supplier, level, e);
            //刷新数据
            loadData();

        }
        /// <summary>
        /// 
        /// </summary>
        private void setLevel(string supplier, string level, DataGridViewCellEventArgs e)
        {
            bool flag = false;
            string beforLevel = dgv_levelInfo.CurrentRow.Cells["level"].Value.ToString();
            string ID = dgv_levelInfo.CurrentRow.Cells["ID"].Value.ToString();
            string  afterLevel= "";
            try
            {

                if (dgv_levelInfo.Columns[e.ColumnIndex].Name.Equals("btnDownLevel"))
                {
                    switch (beforLevel)
                    {
                        case "A": afterLevel = "B"; break;
                        case "B": afterLevel = "C"; break;
                        case "C": afterLevel = "D"; break;
                        case "D": showErr(beforLevel); return;
                        default:
                            break;
                    }
                    //执行降级或升级操作
                    doThis(supplier, beforLevel, afterLevel, ID);

                  
                }
                if (dgv_levelInfo.Columns[e.ColumnIndex].Name.Equals("BtnUpLevel"))
                {
                    switch (beforLevel)
                    {
                        case "A": showErr(beforLevel); return;
                        case "B": afterLevel = "A"; break;
                        case "C": afterLevel = "B"; break;
                        case "D": afterLevel = "C"; break;
                        default:
                            break;
                    }
                    doThis(supplier, beforLevel, afterLevel, ID);

                }

            }
            catch (Exception ex)
            {

                MessageUtil.ShowError("操作失败!"+ex.Message);
            }

        }

        private void doThis(string supplier, string beforLevel, string afterLevel, string ID)
        {
            ShowResonForm = new ShowResonForm(supplier, beforLevel, afterLevel, ID);
            ShowResonForm.ShowDialog();
        }

        private void showErr(string le)
        {
            if (le.Equals("A")) {
                MessageUtil.ShowTips("已经为最高优先级!");

            }
            if (le.Equals("D")) {
                MessageUtil.ShowTips("已经为最低优先级!");
            }
        }

      

        private void btnQueryInfo_Click(object sender, EventArgs e)
        {
            pageNext1_Load();
        }
        /// <summary>
        /// 翻页控件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pageNext1_Load()
        {
            queryCondition.PurOrg = this.CB_purOrg.Text.ToString();
            queryCondition.SupplierId = this.CB_SupplierId.Text.ToString();
            queryCondition.SupplierName = this.CB_SupplierName.ToString();
            queryCondition.STime = this.sTime.Value;
            queryCondition.ETime = this.eTime.Value;

            try
            {
                pageNext1.DrawControl(SupplierListBll.getSupLevelCount(queryCondition));
                loadData();
            }
            catch (Exception ex)
            {
                MessageUtil.ShowTips("加载出错！"+ex.Message);
                
            }
            
        }
        /// <summary>
        /// 翻页事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pageNext1_OnPageChanged(object sender, EventArgs e)
        {
            loadData();
        }

        private void CB_purOrg_SelectedIndexChanged(object sender, EventArgs e)
        {
            
            string sql_1 = "select  DISTINCT Supplier_ID as id from Supplier_Purchasing_Org where PurchasingORG_Name='"+this.CB_purOrg.SelectedValue.ToString()+"'";
            DataTable dt_1 = DBHelper.ExecuteQueryDT(sql_1);
            this.CB_SupplierId.DataSource = dt_1;
            this.CB_SupplierId.DisplayMember = "id";
            this.CB_SupplierId.ValueMember = "id";
            dt_1 = null;
            pageNext1_Load();

        }

        private void CB_SupplierId_SelectedIndexChanged(object sender, EventArgs e)
        {
            string sql_2 = "select  DISTINCT Supplier_Name as name from Supplier_Purchasing_Org where Supplier_ID='" + this.CB_SupplierId.SelectedValue.ToString() + "'";
            DataTable dt_2 = DBHelper.ExecuteQueryDT(sql_2);
            this.CB_SupplierName.DataSource = dt_2;
            this.CB_SupplierName.DisplayMember = "name";
            this.CB_SupplierName.ValueMember = "name";
            dt_2 = null;
            pageNext1_Load();
        }
    }
}
