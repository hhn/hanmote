﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;

namespace MMClient.SupplierPerformance.SupplierList
{
    public partial class SupplierListForm :DockContent
    {
        public SupplierListForm()
        {
            InitializeComponent();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            SupplierdetailedInfo supplierdetailedInfo = new SupplierdetailedInfo();
            supplierdetailedInfo.Show();
        }
    }
}
