﻿namespace MMClient.SupplierPerformance.SupplierList
{
    partial class SupplistPrd
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.pageNext1 = new pager.pagetool.pageNext();
            this.dgv_SupplierInfo = new System.Windows.Forms.DataGridView();
            this.RowNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.supplierLevel = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.levelDes = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.supplierType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.supplierId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.supplierName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.导出ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.打印ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.保存ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.CB_mtGroup = new System.Windows.Forms.ComboBox();
            this.CB_purOrg = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.TB_SupplierListId = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.TB_Des = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.ListBox_exsitedList = new System.Windows.Forms.ListBox();
            this.CB_mtId = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_SupplierInfo)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.pageNext1);
            this.groupBox2.Controls.Add(this.dgv_SupplierInfo);
            this.groupBox2.Location = new System.Drawing.Point(0, 238);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1110, 484);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "供应源";
            // 
            // pageNext1
            // 
            this.pageNext1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.pageNext1.Location = new System.Drawing.Point(12, 441);
            this.pageNext1.Name = "pageNext1";
            this.pageNext1.PageIndex = 1;
            this.pageNext1.PageSize = 100;
            this.pageNext1.RecordCount = 0;
            this.pageNext1.Size = new System.Drawing.Size(887, 37);
            this.pageNext1.TabIndex = 1;
            this.pageNext1.OnPageChanged += new System.EventHandler(this.pageNext1_OnPageChanged);
            // 
            // dgv_SupplierInfo
            // 
            this.dgv_SupplierInfo.AllowDrop = true;
            this.dgv_SupplierInfo.AllowUserToAddRows = false;
            this.dgv_SupplierInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgv_SupplierInfo.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_SupplierInfo.BackgroundColor = System.Drawing.SystemColors.HighlightText;
            this.dgv_SupplierInfo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgv_SupplierInfo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_SupplierInfo.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.RowNumber,
            this.supplierLevel,
            this.levelDes,
            this.supplierType,
            this.supplierId,
            this.supplierName});
            this.dgv_SupplierInfo.Location = new System.Drawing.Point(11, 20);
            this.dgv_SupplierInfo.Name = "dgv_SupplierInfo";
            this.dgv_SupplierInfo.RowTemplate.Height = 23;
            this.dgv_SupplierInfo.Size = new System.Drawing.Size(1093, 415);
            this.dgv_SupplierInfo.TabIndex = 0;
            // 
            // RowNumber
            // 
            this.RowNumber.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.RowNumber.DataPropertyName = "RowNumber";
            this.RowNumber.HeaderText = "序号";
            this.RowNumber.Name = "RowNumber";
            this.RowNumber.ReadOnly = true;
            this.RowNumber.Width = 51;
            // 
            // supplierLevel
            // 
            this.supplierLevel.DataPropertyName = "Supplier_Position";
            this.supplierLevel.HeaderText = "供应商优先级";
            this.supplierLevel.Name = "supplierLevel";
            this.supplierLevel.ReadOnly = true;
            // 
            // levelDes
            // 
            this.levelDes.DataPropertyName = "levelDescription";
            this.levelDes.HeaderText = "优先级描述";
            this.levelDes.Name = "levelDes";
            this.levelDes.ReadOnly = true;
            // 
            // supplierType
            // 
            this.supplierType.DataPropertyName = "supplierType";
            this.supplierType.HeaderText = "供应商类型";
            this.supplierType.Name = "supplierType";
            this.supplierType.ReadOnly = true;
            // 
            // supplierId
            // 
            this.supplierId.DataPropertyName = "Supplier_ID";
            this.supplierId.HeaderText = "供应商编号";
            this.supplierId.Name = "supplierId";
            this.supplierId.ReadOnly = true;
            // 
            // supplierName
            // 
            this.supplierName.DataPropertyName = "Supplier_Name";
            this.supplierName.HeaderText = "供应商名称";
            this.supplierName.Name = "supplierName";
            this.supplierName.ReadOnly = true;
            this.supplierName.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // 导出ToolStripMenuItem
            // 
            this.导出ToolStripMenuItem.Font = new System.Drawing.Font("Microsoft YaHei UI", 9F, System.Drawing.FontStyle.Underline);
            this.导出ToolStripMenuItem.Name = "导出ToolStripMenuItem";
            this.导出ToolStripMenuItem.Size = new System.Drawing.Size(44, 21);
            this.导出ToolStripMenuItem.Text = "导出";
            // 
            // 打印ToolStripMenuItem
            // 
            this.打印ToolStripMenuItem.Font = new System.Drawing.Font("Microsoft YaHei UI", 9F, System.Drawing.FontStyle.Underline);
            this.打印ToolStripMenuItem.Name = "打印ToolStripMenuItem";
            this.打印ToolStripMenuItem.Size = new System.Drawing.Size(44, 21);
            this.打印ToolStripMenuItem.Text = "打印";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.打印ToolStripMenuItem,
            this.导出ToolStripMenuItem,
            this.保存ToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1140, 25);
            this.menuStrip1.TabIndex = 5;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // 保存ToolStripMenuItem
            // 
            this.保存ToolStripMenuItem.Font = new System.Drawing.Font("Microsoft YaHei UI", 9F, System.Drawing.FontStyle.Underline);
            this.保存ToolStripMenuItem.Name = "保存ToolStripMenuItem";
            this.保存ToolStripMenuItem.Size = new System.Drawing.Size(44, 21);
            this.保存ToolStripMenuItem.Text = "保存";
            this.保存ToolStripMenuItem.Click += new System.EventHandler(this.保存ToolStripMenuItem_Click);
            // 
            // CB_mtGroup
            // 
            this.CB_mtGroup.FormattingEnabled = true;
            this.CB_mtGroup.Location = new System.Drawing.Point(74, 110);
            this.CB_mtGroup.Name = "CB_mtGroup";
            this.CB_mtGroup.Size = new System.Drawing.Size(289, 20);
            this.CB_mtGroup.TabIndex = 28;
            this.CB_mtGroup.SelectedIndexChanged += new System.EventHandler(this.CB_mtGroup_SelectedIndexChanged);
            // 
            // CB_purOrg
            // 
            this.CB_purOrg.FormattingEnabled = true;
            this.CB_purOrg.Location = new System.Drawing.Point(74, 65);
            this.CB_purOrg.Name = "CB_purOrg";
            this.CB_purOrg.Size = new System.Drawing.Size(289, 20);
            this.CB_purOrg.TabIndex = 27;
            this.CB_purOrg.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 24);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 12);
            this.label5.TabIndex = 25;
            this.label5.Text = "描述：";
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(412, 85);
            this.textBox4.Multiline = true;
            this.textBox4.Name = "textBox4";
            this.textBox4.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBox4.Size = new System.Drawing.Size(286, 95);
            this.textBox4.TabIndex = 24;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(406, 59);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 12);
            this.label4.TabIndex = 23;
            this.label4.Text = "内部注释：";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 118);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 12);
            this.label3.TabIndex = 21;
            this.label3.Text = "物料组：";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 68);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 12);
            this.label2.TabIndex = 13;
            this.label2.Text = "采购组织：";
            // 
            // TB_SupplierListId
            // 
            this.TB_SupplierListId.Enabled = false;
            this.TB_SupplierListId.Location = new System.Drawing.Point(517, 21);
            this.TB_SupplierListId.Name = "TB_SupplierListId";
            this.TB_SupplierListId.Size = new System.Drawing.Size(181, 21);
            this.TB_SupplierListId.TabIndex = 12;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(410, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(101, 12);
            this.label1.TabIndex = 11;
            this.label1.Text = "供应商清单编码：";
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.TB_Des);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.ListBox_exsitedList);
            this.groupBox1.Controls.Add(this.CB_mtId);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.CB_mtGroup);
            this.groupBox1.Controls.Add(this.CB_purOrg);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.textBox4);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.TB_SupplierListId);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(11, 28);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1098, 204);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "通用抬头数据";
            // 
            // TB_Des
            // 
            this.TB_Des.Location = new System.Drawing.Point(74, 17);
            this.TB_Des.Multiline = true;
            this.TB_Des.Name = "TB_Des";
            this.TB_Des.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.TB_Des.Size = new System.Drawing.Size(289, 39);
            this.TB_Des.TabIndex = 33;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(733, 20);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(89, 12);
            this.label7.TabIndex = 32;
            this.label7.Text = "已存在供应清单";
            // 
            // ListBox_exsitedList
            // 
            this.ListBox_exsitedList.FormattingEnabled = true;
            this.ListBox_exsitedList.ItemHeight = 12;
            this.ListBox_exsitedList.Location = new System.Drawing.Point(735, 49);
            this.ListBox_exsitedList.Name = "ListBox_exsitedList";
            this.ListBox_exsitedList.Size = new System.Drawing.Size(259, 136);
            this.ListBox_exsitedList.TabIndex = 31;
            this.ListBox_exsitedList.SelectedIndexChanged += new System.EventHandler(this.ListBox_exsitedList_SelectedIndexChanged);
            // 
            // CB_mtId
            // 
            this.CB_mtId.FormattingEnabled = true;
            this.CB_mtId.Location = new System.Drawing.Point(74, 160);
            this.CB_mtId.Name = "CB_mtId";
            this.CB_mtId.Size = new System.Drawing.Size(289, 20);
            this.CB_mtId.TabIndex = 30;
            this.CB_mtId.SelectedIndexChanged += new System.EventHandler(this.CB_mtName_SelectedIndexChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 163);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 12);
            this.label6.TabIndex = 29;
            this.label6.Text = "物料：";
            // 
            // SupplistPrd
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1140, 734);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "SupplistPrd";
            this.Text = "概况信息";
            this.Load += new System.EventHandler(this.SupplistPrd_Load);
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_SupplierInfo)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView dgv_SupplierInfo;
        private System.Windows.Forms.ToolStripMenuItem 导出ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 打印ToolStripMenuItem;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 保存ToolStripMenuItem;
        private System.Windows.Forms.ComboBox CB_mtGroup;
        private System.Windows.Forms.ComboBox CB_purOrg;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox TB_SupplierListId;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private pager.pagetool.pageNext pageNext1;
        private System.Windows.Forms.ComboBox CB_mtId;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ListBox ListBox_exsitedList;
        private System.Windows.Forms.TextBox TB_Des;
        private System.Windows.Forms.DataGridViewTextBoxColumn RowNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn supplierLevel;
        private System.Windows.Forms.DataGridViewTextBoxColumn levelDes;
        private System.Windows.Forms.DataGridViewTextBoxColumn supplierType;
        private System.Windows.Forms.DataGridViewTextBoxColumn supplierId;
        private System.Windows.Forms.DataGridViewTextBoxColumn supplierName;
    }
}