﻿using Lib.Bll.supplierListBll;
using Lib.Common.CommonUtils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MMClient.SupplierPerformance.SupplierList
{
    public partial class ShowResonForm : Form
    {
        private string mess;
        private string supplier;
        private string beforeLevel;
        private string afterLevel;
        private string ID;
        SupplierClassifyList SupplierListBll = new SupplierClassifyList();

        public ShowResonForm(string supplier, string beforeLevel, string afterLevel, string ID)
        {
            InitializeComponent();
            this.supplier = supplier;
            this.beforeLevel = beforeLevel;
            this.afterLevel = afterLevel;
            this.ID = ID;
        }

        private void ShowResonForm_Load(object sender, EventArgs e)
        {
            this.supplierName.Text = this.supplier;
            this.levelResult.Text = beforeLevel + "->" + afterLevel;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (MessageUtil.ShowOKCancelAndQuestion("确定执行?") == DialogResult.OK) {
               bool flag = SupplierListBll.setLevel(supplier, afterLevel, ID);
                if (flag)
                {
                    string modifyMess = beforeLevel + "->" + afterLevel;
                    string reason = this.reasonInfo.Text;
                    bool f = SupplierListBll.saveModifyInfo(supplier, modifyMess, reason);
                    if (f)
                        MessageUtil.ShowTips("操作成功!");
                    else
                        MessageUtil.ShowTips("操作失败!");
                }
                this.Close();
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
