﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MMClient.ContractManage;
using Lib.Bll.SupplierPerformaceBLL;
using WeifenLuo.WinFormsUI.Docking;
using Lib.Common.MMCException.IDAL;

namespace MMClient.SupplierPerformance
{
    public partial class ComplaintRejectAddForm : DockContent
    {
        String materialID, supplierID, purchasingORGID;
        Double ratio,cost;
        Decimal transactionAmount;
        int count;
        DateTime time;
        String name;
        String year;
        List<Object> saveCRPara = new List<object>();

        DataTable supplierInfoTable, materialInfoTable, purchasingORGInfoTable;

        SupplierPerformanceBLL supplierPerformanceBLL = new SupplierPerformanceBLL();

        public ComplaintRejectAddForm()
        {
            InitializeComponent();

            //选择供应商id
            supplierInfoTable = supplierPerformanceBLL.querySupplier();
            comboBox_supplierID.DataSource = supplierInfoTable;
            comboBox_supplierID.DisplayMember = supplierInfoTable.Columns["Supplier_ID"].ToString();
            lable_supplierName.Text = (supplierInfoTable.Rows[comboBox_supplierID.SelectedIndex]["Supplier_Name"]).ToString();
            //在年份选项中给combobox添加过去10年数据
            for (int i = DateTime.Now.Year; i >= DateTime.Now.Year-10; i--)
            {
                string year = string.Format("{0}", i);
                comboBox_year.Items.Add(year);
            }
            comboBox_year.SelectedIndex = 0;

        }
        //保存
        private void button_Save_Click(object sender, EventArgs e)
        {
            if (comboBox_supplierID == null || comboBox_materialID == null || comboBox_purchasingORGID == null )
            {
                MessageBox.Show("请选择供应商、物料、采购组织代码，若无选项请先在主数据视图中维护相关信息！");
                return;
            }
            else if(textBox_Ratio == null || textBox_Cost == null || textBox_Count == null || dateTimePicker1 == null || textBox_Name == null)
            {
                MessageBox.Show("请输入完整的抱怨/拒绝情况信息！");
                return;
            }
            else if (textBox_TransactionAmount == null)
            {
                MessageBox.Show("请计算年度交易总额！");
                return;
            }
            else if (textBox_Name.Text == null || dateTimePicker1.Text == null)
            {
                MessageBox.Show("请输入创建时间及创建人！");
                return;
            }
            else if (transactionAmount == 0)
            {
                MessageBox.Show("请先计算交易量，若交易量计算无数值，则当前尚无交易，不可录入抱怨/拒绝情况记录！");
                return;
            }
            else
            {
                time = Convert.ToDateTime(dateTimePicker1.Text);
                name = textBox_Name.Text;

                try
                {
                    ratio = Convert.ToDouble(textBox_Ratio.Text);
                    cost = Convert.ToDouble(textBox_Cost.Text);
                    count = Convert.ToInt16(textBox_Count.Text);
                }
                catch
                {
                    MessageBox.Show("请检查业务量比例参数、交易总量和质量通知成本输入的值是否为数字，质量通知的次数是否为整数！");
                    return;
                }
                if (saveCRPara.Count != 0)
                {
                    saveCRPara.Clear();
                }
                saveCRPara.Add(supplierID);
                saveCRPara.Add(materialID);
                saveCRPara.Add(purchasingORGID);
                saveCRPara.Add(ratio);
                saveCRPara.Add(transactionAmount);
                saveCRPara.Add(cost);
                saveCRPara.Add(count);
                saveCRPara.Add(time);
                saveCRPara.Add(name);
                try
                {
                    supplierPerformanceBLL.saveComplaintRejectRecord(saveCRPara);
                    MessageBox.Show("抱怨/拒绝情况录入成功！");
                    DialogResult dr;
                    dr = MessageBox.Show("是否接着录入?", "检查", MessageBoxButtons.YesNoCancel,
                    MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2);
                    if (dr == DialogResult.Yes)
                    {
                        setNull();
                        return;
                    }
                    else
                    {
                        this.Close();
                    }
                }
                catch(DBException ex)
                {
                    MessageBox.Show("抱怨/拒绝情况录入失败！");
                }
               
            }
        }

        private void setNull()
        {
            saveCRPara.Clear();
            this.textBox_Cost.Text = "";
            this.textBox_Count.Text = "";
            this.textBox_Ratio.Text = "";
            this.textBox_TransactionAmount.Text = "";
        }

        private void comboBox_year_SelectedIndexChanged(object sender, EventArgs e)
        {
            String year = comboBox_year.Text;
        }

        //计算年度交易总额
        private void button1_Click(object sender, EventArgs e)
        {
            if (materialID == null || supplierID == null || comboBox_year.Text == null)
            {
                MessageBox.Show("请选择供应商、物料及年份！");
                return;
            }
            else
            {
                //计算交易总量
                year = comboBox_year.Text;
                transactionAmount = supplierPerformanceBLL.queryTransactionAmount(materialID, supplierID, year);
                textBox_TransactionAmount.Text = Math.Round(transactionAmount, 1).ToString();
                if (transactionAmount == 0)
                {
                    MessageBox.Show("当前尚无交易，不可录入抱怨/拒绝情况记录！");
                    return;
                }
            }
            
        }

        //选择供应商id
        private void comboBox_supplierID_SelectedIndexChanged(object sender, EventArgs e)
        {
            supplierID = comboBox_supplierID.Text.ToString();
            lable_supplierName.Text = supplierInfoTable.Rows[comboBox_supplierID.SelectedIndex]["Supplier_Name"].ToString();

            //选择的供应商变化时，则重新查询对应的采购组织信息
            purchasingORGInfoTable = supplierPerformanceBLL.queryPurchasingORG(supplierID);
            comboBox_purchasingORGID.DataSource = purchasingORGInfoTable;
            if (purchasingORGInfoTable.Rows.Count > 0)
            {
                comboBox_purchasingORGID.DisplayMember = purchasingORGInfoTable.Columns["PurchasingORG_ID"].ToString();
                lable_purchasingORGName.Text = (purchasingORGInfoTable.Rows[comboBox_purchasingORGID.SelectedIndex]["PurchasingORG_Name"]).ToString();
            }

            //选择的供应商变化时，则重新查询对应的物料信息
            materialInfoTable = supplierPerformanceBLL.queryMaterialID(supplierID);
            comboBox_materialID.DataSource = materialInfoTable;
            if (materialInfoTable.Rows.Count > 0)
            {
                comboBox_materialID.DisplayMember = materialInfoTable.Columns["Material_ID"].ToString();
                lable_materialName.Text = (materialInfoTable.Rows[comboBox_materialID.SelectedIndex]["Material_Name"]).ToString();
            }
        }

        //物料id
        private void comboBox_materialID_SelectedIndexChanged(object sender, EventArgs e)
        {
            materialID = comboBox_materialID.Text.ToString();
            if (materialInfoTable.Rows.Count > 0)
            {
                lable_materialName.Text = (materialInfoTable.Rows[comboBox_materialID.SelectedIndex]["Material_Name"]).ToString();
            }
        }

        //采购组织id
        private void comboBox_purchasingORGID_SelectedIndexChanged(object sender, EventArgs e)
        {
            purchasingORGID = comboBox_purchasingORGID.Text.ToString();
            if (purchasingORGInfoTable.Rows.Count > 0)
            {
                lable_purchasingORGName.Text = (purchasingORGInfoTable.Rows[comboBox_purchasingORGID.SelectedIndex]["PurchasingORG_Name"]).ToString();
            }
        }
    }
}
