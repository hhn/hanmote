﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Aspose.Cells;
using Lib.Bll.SupplierPerformaceBLL;
using WeifenLuo.WinFormsUI.Docking;

namespace MMClient.SupplierPerformance.DataRecord
{
    public partial class QuestionaireForm : DockContent
    {
        String supplierID, materialID;
        DataTable supplierInfoTable, materialInfoTable;
        SupplierPerformanceBLL supplierPerformanceBLL = new SupplierPerformanceBLL();

        public QuestionaireForm()
        {
            InitializeComponent();

            //选择供应商id
            supplierInfoTable = supplierPerformanceBLL.querySupplier();
            comboBox_supplierID.DataSource = supplierInfoTable;
            comboBox_supplierID.DisplayMember = supplierInfoTable.Columns["Supplier_ID"].ToString();
            lable_supplierName.Text = (supplierInfoTable.Rows[comboBox_supplierID.SelectedIndex]["Supplier_Name"]).ToString();
        }

        /// <summary>
        /// 打开excel选择框
        /// </summary>
        /// <param name="workbook"></param>
        /// <param name="cells"></param>
        /// <returns></returns>
        private Decimal Import()
        {
            //打开excel选择框
            OpenFileDialog frm = new OpenFileDialog();
            frm.Filter = "Excel文件(*.xls,xlsx)|*.xls;*.xlsx";
            Decimal score = -1;//返回-1表示当前无问卷结果
            if (frm.ShowDialog() == DialogResult.OK)
            {
                string excelName = frm.FileName;
                Workbook excel = new Workbook(excelName);
                score = GetImportExcelRoute(excel);
            }
            return score;
        }

        /// <summary>
        /// 读取excel
        /// </summary>
        /// <param name="workbook"></param>
        /// <param name="cells"></param>
        /// <returns></returns>
        public Decimal GetImportExcelRoute(Workbook excel)
        {
            String cellContent;
            Decimal score = 0;
            Decimal weightedCount = 0;

            //读取第0张worksheet,问卷只有一张worksheet
            //int sheetNum = 0;

            //Worksheet sheet = excel.Worksheets[sheetNum];
            //Cells cells = sheet.Cells;
            //chapterInfoName = sheet.Name;

            //int rowcount = cells.MaxRow;
            //int columncount = cells.MaxColumn;

            #region 改掉循环的新代码（运行有问题）
            ////每次循环之前将score和weightedCount置为0
            //score = 0;
            //weightedCount = 0;

            //for (int j = 2; j <= rowcount; j++)
            //{
            //    for (int i = 2; i <= 6; i++)
            //    {
            //        //cells[j, i].StringValue.Trim()操作excel
            //        cellContent = cells[j, i].StringValue.Trim();

            //        //如果用户对该项无评分，则不计入分数
            //        if ((i == 2) && (cellContent == "X"))
            //        {
            //            score = score + 0;
            //            continue;
            //        }
            //        //若用户对该项的评分为3分，则分数加3，分母（weightedCount）加3
            //        else if ((i == 3) && (cellContent == "X"))
            //        {
            //            score = score + 3;
            //            weightedCount = weightedCount + 3;
            //            continue;
            //        }
            //        //若用户对该项的评分为2分，则分数加2，分母（weightedCount）加3
            //        else if ((i == 4) && (cellContent == "X"))
            //        {
            //            score = score + 2;
            //            weightedCount = weightedCount + 3;
            //            continue;
            //        }
            //        //若用户对该项的评分为1分，则分数加1，分母（weightedCount）加3
            //        else if ((i == 5) && (cellContent == "X"))
            //        {
            //            score = score + 1;
            //            weightedCount = weightedCount + 3;
            //            continue;
            //        }
            //        //若用户对该项的评分为0分，则分数加0，分母（weightedCount）加3
            //        else if ((i == 6) && (cellContent == "X"))
            //        {
            //            score = score + 0;
            //            weightedCount = weightedCount + 3;
            //            continue;
            //        }
            //    }

            //    if (weightedCount == 0)
            //    {
            //        //返回-1，表示当前问卷无调查结果
            //       score = -1;
            //    }
            //    else
            //    {
            //        score = (score / weightedCount) * 100;
            //    }

            //    //if (chapterInfo.ContainsKey(sheetNum))
            //    //{
            //    //    chapterInfo[sheetNum] = chapterInfoName;
            //    //}
            //    //else
            //    //{
            //    //    chapterInfo.Add(sheetNum, chapterInfoName);
            //    //}
            //    //chapterInfo.Add(chapterInfoName);
            //}
            #endregion

            #region 未改掉循环的代码
              //循环读取每一张worksheet
            for (int sheetNum = 0; sheetNum <= 0; sheetNum++)
            {
                Worksheet sheet = excel.Worksheets[sheetNum];
                Cells cells = sheet.Cells;

                int rowcount = cells.MaxRow;
                //int columncount = cells.MaxColumn;

                //每次循环之前将score和weightedCount置为0
                score = 0;
                weightedCount = 0;

                for (int j = 2; j <= rowcount; j++)
                {
                    for (int i = 2; i <= 6; i++)
                    {
                        //cells[j, i].StringValue.Trim()操作excel
                        cellContent = cells[j, i].StringValue.Trim();

                        //如果用户对该项无评分，则不计入分数
                        if ((i == 2) && (cellContent == "X"))
                        {
                            score = score + 0;
                            continue;
                        }
                        //若用户对该项的评分为3分，则分数加3，分母（weightedCount）加3
                        else if ((i == 3) && (cellContent == "X"))
                        {
                            score = score + 3;
                            weightedCount = weightedCount + 3;
                            continue;
                        }
                        //若用户对该项的评分为2分，则分数加2，分母（weightedCount）加3
                        else if ((i == 4) && (cellContent == "X"))
                        {
                            score = score + 2;
                            weightedCount = weightedCount + 3;
                            continue;
                        }
                        //若用户对该项的评分为1分，则分数加1，分母（weightedCount）加3
                        else if ((i == 5) && (cellContent == "X"))
                        {
                            score = score + 1;
                            weightedCount = weightedCount + 3;
                            continue;
                        }
                        //若用户对该项的评分为0分，则分数加0，分母（weightedCount）加3
                        else if ((i == 6) && (cellContent == "X"))
                        {
                            score = score + 0;
                            weightedCount = weightedCount + 3;
                            continue;
                        }
                    }
                }
                if (weightedCount == 0)
                {
                    score = -1;
                }
                else
                {
                    score = (score / weightedCount) * 100;
                }
            }
            #endregion

            return score;
        }

        /// <summary>
        /// “计算收货问卷得分”Button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button_calculate_Click(object sender, EventArgs e)
        {
            textBox_score.Clear();
            Decimal score = this.Import();

            //显示分数结果
            if (score < 0)
            {
                //默认数据
                textBox_score.Text = "40";
                //textBox_score.Text = "暂无数据";
                return;
            }
            else
            {
                textBox_score.Text = Math.Round(score, 1).ToString();
            }  
        }

        /// <summary>
        /// 供应商ID
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void comboBox_supplierID_SelectedIndexChanged(object sender, EventArgs e)
        {
            supplierID = comboBox_supplierID.Text.ToString();
            lable_supplierName.Text = supplierInfoTable.Rows[comboBox_supplierID.SelectedIndex]["Supplier_Name"].ToString();


            //选择的供应商变化时，则重新查询对应的物料信息
            materialInfoTable = supplierPerformanceBLL.queryMaterialID(supplierID);
            comboBox_materialID.DataSource = materialInfoTable;
            if (materialInfoTable.Rows.Count > 0)
            {
                comboBox_materialID.DisplayMember = materialInfoTable.Columns["Material_ID"].ToString();
                lable_materialName.Text = (materialInfoTable.Rows[comboBox_materialID.SelectedIndex]["Material_Name"]).ToString();
            }
        }

        /// <summary>
        /// 物料ID
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void comboBox_materialID_SelectedIndexChanged(object sender, EventArgs e)
        {
            materialID = comboBox_materialID.Text.ToString();
            if (materialInfoTable.Rows.Count > 0)
            {
                lable_materialName.Text = (materialInfoTable.Rows[comboBox_materialID.SelectedIndex]["Material_Name"]).ToString();
            }
        }

        /// <summary>
        /// “保存”Button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button_button_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(comboBox_materialID.Text.ToString()) || String.IsNullOrEmpty(comboBox_supplierID.Text.ToString()))
            {
                MessageBox.Show("请选择供应商代码和物料代码，若无对应的物料代码，请在主数据视图中维护！");
                return;
            }
            if (String.IsNullOrEmpty(textBox_Name.Text.ToString()))
            {
                MessageBox.Show("请输入维护人信息！");
                return;
            }
            if (String.IsNullOrEmpty(textBox_score.Text.ToString()))
            {
                MessageBox.Show("请先计算问卷调查分数！");
                return;
            }

            List<Object> saveQuestionaire = new List<Object>();
            saveQuestionaire.Clear();
            saveQuestionaire.Add(comboBox_supplierID.Text.ToString());
            saveQuestionaire.Add(comboBox_materialID.Text.ToString());
            saveQuestionaire.Add(textBox_score.Text.ToString());
            saveQuestionaire.Add(Convert.ToDateTime(dateTimePicker1.Text));
            saveQuestionaire.Add(textBox_Name.Text.ToString());

            //int lines = supplierPerformanceBLL.savePSAuditRecord(savePSAuditPara, chapterInfo, scoreList);
            int lines = -1;
            try
            {
                lines = supplierPerformanceBLL.saveQuestionaire(saveQuestionaire);
            }
            catch
            {
                MessageBox.Show("保存失败！");
                return;
            }

            if (lines > 0)
            {
                MessageBox.Show("问卷调查结果保存成功！");
                return;
            }
        }



    }
}
