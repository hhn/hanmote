﻿namespace MMClient.SupplierPerformance
{
    partial class ServiceScoreAddForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBox_Name = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.label8 = new System.Windows.Forms.Label();
            this.lable_materialName = new System.Windows.Forms.Label();
            this.comboBox_materialID = new System.Windows.Forms.ComboBox();
            this.label22 = new System.Windows.Forms.Label();
            this.lable_supplierName = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.comboBox_supplierID = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button_saveServiceSupport = new System.Windows.Forms.Button();
            this.textBox_SSuserService = new System.Windows.Forms.TextBox();
            this.textBox_SSreliability = new System.Windows.Forms.TextBox();
            this.textBox_SScreativity = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.button_saveExternalService = new System.Windows.Forms.Button();
            this.textBox_ESservicePromptness = new System.Windows.Forms.TextBox();
            this.textBox_ESserviceQuality = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // textBox_Name
            // 
            this.textBox_Name.Location = new System.Drawing.Point(553, 83);
            this.textBox_Name.Name = "textBox_Name";
            this.textBox_Name.Size = new System.Drawing.Size(174, 25);
            this.textBox_Name.TabIndex = 39;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(473, 90);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(67, 15);
            this.label9.TabIndex = 38;
            this.label9.Text = "创建人：";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(179, 86);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(173, 25);
            this.dateTimePicker1.TabIndex = 37;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(83, 93);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(82, 15);
            this.label8.TabIndex = 36;
            this.label8.Text = "创建时间：";
            // 
            // lable_materialName
            // 
            this.lable_materialName.AutoSize = true;
            this.lable_materialName.Location = new System.Drawing.Point(572, 56);
            this.lable_materialName.Name = "lable_materialName";
            this.lable_materialName.Size = new System.Drawing.Size(67, 15);
            this.lable_materialName.TabIndex = 29;
            this.lable_materialName.Text = "物料名称";
            // 
            // comboBox_materialID
            // 
            this.comboBox_materialID.FormattingEnabled = true;
            this.comboBox_materialID.Location = new System.Drawing.Point(553, 27);
            this.comboBox_materialID.Name = "comboBox_materialID";
            this.comboBox_materialID.Size = new System.Drawing.Size(174, 23);
            this.comboBox_materialID.TabIndex = 28;
            this.comboBox_materialID.SelectedIndexChanged += new System.EventHandler(this.comboBox_materialID_SelectedIndexChanged);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(488, 33);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(52, 15);
            this.label22.TabIndex = 27;
            this.label22.Text = "物料：";
            // 
            // lable_supplierName
            // 
            this.lable_supplierName.AutoSize = true;
            this.lable_supplierName.Location = new System.Drawing.Point(194, 59);
            this.lable_supplierName.Name = "lable_supplierName";
            this.lable_supplierName.Size = new System.Drawing.Size(82, 15);
            this.lable_supplierName.TabIndex = 26;
            this.lable_supplierName.Text = "供应商名称";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(98, 36);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 15);
            this.label2.TabIndex = 24;
            this.label2.Text = "供应商：";
            // 
            // comboBox_supplierID
            // 
            this.comboBox_supplierID.FormattingEnabled = true;
            this.comboBox_supplierID.Location = new System.Drawing.Point(179, 33);
            this.comboBox_supplierID.Name = "comboBox_supplierID";
            this.comboBox_supplierID.Size = new System.Drawing.Size(173, 23);
            this.comboBox_supplierID.TabIndex = 22;
            this.comboBox_supplierID.SelectedIndexChanged += new System.EventHandler(this.comboBox_supplierID_SelectedIndexChanged);
            // 
            // supplierInforGroupBox
            // 
            this.groupBox1.Controls.Add(this.button_saveServiceSupport);
            this.groupBox1.Controls.Add(this.textBox_SSuserService);
            this.groupBox1.Controls.Add(this.textBox_SSreliability);
            this.groupBox1.Controls.Add(this.textBox_SScreativity);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Location = new System.Drawing.Point(52, 186);
            this.groupBox1.Name = "supplierInforGroupBox";
            this.groupBox1.Size = new System.Drawing.Size(365, 171);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "一般服务/支持";
            // 
            // button_saveServiceSupport
            // 
            this.button_saveServiceSupport.Location = new System.Drawing.Point(277, 138);
            this.button_saveServiceSupport.Name = "button_saveServiceSupport";
            this.button_saveServiceSupport.Size = new System.Drawing.Size(75, 23);
            this.button_saveServiceSupport.TabIndex = 6;
            this.button_saveServiceSupport.Text = "保存";
            this.button_saveServiceSupport.UseVisualStyleBackColor = true;
            this.button_saveServiceSupport.Click += new System.EventHandler(this.button_saveServiceSupport_Click);
            // 
            // textBox_SSuserService
            // 
            this.textBox_SSuserService.Location = new System.Drawing.Point(179, 61);
            this.textBox_SSuserService.Name = "textBox_SSuserService";
            this.textBox_SSuserService.Size = new System.Drawing.Size(173, 25);
            this.textBox_SSuserService.TabIndex = 5;
            // 
            // textBox_SSreliability
            // 
            this.textBox_SSreliability.Location = new System.Drawing.Point(179, 97);
            this.textBox_SSreliability.Name = "textBox_SSreliability";
            this.textBox_SSreliability.Size = new System.Drawing.Size(173, 25);
            this.textBox_SSreliability.TabIndex = 4;
            // 
            // textBox_SScreativity
            // 
            this.textBox_SScreativity.Location = new System.Drawing.Point(179, 24);
            this.textBox_SScreativity.Name = "textBox_SScreativity";
            this.textBox_SScreativity.Size = new System.Drawing.Size(173, 25);
            this.textBox_SScreativity.TabIndex = 3;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(83, 64);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(67, 15);
            this.label7.TabIndex = 2;
            this.label7.Text = "用户服务";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(98, 100);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(52, 15);
            this.label6.TabIndex = 1;
            this.label6.Text = "可靠性";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(98, 27);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(52, 15);
            this.label5.TabIndex = 0;
            this.label5.Text = "创新性";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.button_saveExternalService);
            this.groupBox2.Controls.Add(this.textBox_ESservicePromptness);
            this.groupBox2.Controls.Add(this.textBox_ESserviceQuality);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Location = new System.Drawing.Point(463, 186);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(376, 171);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "外部服务";
            // 
            // button_saveExternalService
            // 
            this.button_saveExternalService.Location = new System.Drawing.Point(232, 97);
            this.button_saveExternalService.Name = "button_saveExternalService";
            this.button_saveExternalService.Size = new System.Drawing.Size(75, 23);
            this.button_saveExternalService.TabIndex = 4;
            this.button_saveExternalService.Text = "保存";
            this.button_saveExternalService.UseVisualStyleBackColor = true;
            this.button_saveExternalService.Click += new System.EventHandler(this.button_saveExternalService_Click);
            // 
            // textBox_ESservicePromptness
            // 
            this.textBox_ESservicePromptness.Location = new System.Drawing.Point(142, 61);
            this.textBox_ESservicePromptness.Name = "textBox_ESservicePromptness";
            this.textBox_ESservicePromptness.Size = new System.Drawing.Size(174, 25);
            this.textBox_ESservicePromptness.TabIndex = 3;
            // 
            // textBox_ESserviceQuality
            // 
            this.textBox_ESserviceQuality.Location = new System.Drawing.Point(142, 24);
            this.textBox_ESserviceQuality.Name = "textBox_ESserviceQuality";
            this.textBox_ESserviceQuality.Size = new System.Drawing.Size(174, 25);
            this.textBox_ESserviceQuality.TabIndex = 2;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(45, 64);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(82, 15);
            this.label11.TabIndex = 1;
            this.label11.Text = "服务及时性";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(60, 27);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(67, 15);
            this.label10.TabIndex = 0;
            this.label10.Text = "服务质量";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.textBox_Name);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.comboBox_supplierID);
            this.groupBox3.Controls.Add(this.dateTimePicker1);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.lable_supplierName);
            this.groupBox3.Controls.Add(this.lable_materialName);
            this.groupBox3.Controls.Add(this.label22);
            this.groupBox3.Controls.Add(this.comboBox_materialID);
            this.groupBox3.Location = new System.Drawing.Point(52, 43);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(787, 127);
            this.groupBox3.TabIndex = 40;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "基本信息";
            // 
            // ServiceScoreAddForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(903, 748);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "ServiceScoreAddForm";
            this.Text = "服务情况录入";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox textBox_Name;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lable_materialName;
        private System.Windows.Forms.ComboBox comboBox_materialID;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label lable_supplierName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox comboBox_supplierID;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button button_saveServiceSupport;
        private System.Windows.Forms.TextBox textBox_SSuserService;
        private System.Windows.Forms.TextBox textBox_SSreliability;
        private System.Windows.Forms.TextBox textBox_SScreativity;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox textBox_ESservicePromptness;
        private System.Windows.Forms.TextBox textBox_ESserviceQuality;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button button_saveExternalService;
    }
}