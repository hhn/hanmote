﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Aspose.Cells;
using Lib.Bll.SupplierPerformaceBLL;
using WeifenLuo.WinFormsUI.Docking;
using Lib.Common.CommonUtils;
using Lib.Common.MMCException.IDAL;
using Lib.SqlServerDAL;
using System.IO;

namespace MMClient.SupplierPerformance
{
    public partial class SystemProcessAuditForm : DockContent
    {
        SupplierPerformanceBLL supplierPerformanceBLL = new SupplierPerformanceBLL();
        String supplierID, materialID;
        DataTable excelTbl = null;
        DataTable supplierInfoTable, materialInfoTable;
        int isOut = 0;
        int isChecked = 0;
        List<String> insertListSQL = new List<string>();
        public SystemProcessAuditForm()
        {
            InitializeComponent();

            //选择供应商id
            supplierInfoTable = supplierPerformanceBLL.querySupplier();
            comboBox_supplierID.DataSource = supplierInfoTable;
            comboBox_supplierID.DisplayMember = supplierInfoTable.Columns["Supplier_ID"].ToString();
            lable_supplierName.Text = (supplierInfoTable.Rows[comboBox_supplierID.SelectedIndex]["Supplier_Name"]).ToString();
        }

        /// <summary>
        /// “保存”Button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button_save_Click(object sender, EventArgs e)
        {
            if(isOut==0)
            {
                if (String.IsNullOrEmpty(comboBox_materialID.Text.ToString()) || String.IsNullOrEmpty(comboBox_supplierID.Text.ToString()))
                {
                    MessageBox.Show("请选择供应商代码和物料代码，若无对应的物料代码，请在主数据视图中维护！");
                    return;
                }
                if (String.IsNullOrEmpty(textBox_Name.Text.ToString()))
                {
                    MessageBox.Show("请输入维护人信息！");
                    return;
                }
                string ProcessScore =  this.txtProcessScore.Text.ToString();
                string SystemScore =  this.txtSystemScore.Text.ToString();
                try
                {
                    Convert.ToDouble(ProcessScore);
                }catch(Exception ex)
                {
                    MessageBox.Show("过程审核分数录入不合法");
                    return;
                }
                try
                {
                    Convert.ToDouble(SystemScore);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("体系审核分数录入不合法");
                    return;
                }
                string insertSQLProcess = @"INSERT INTO System_Process_Audit(  
                                                     [Supplier_ID],
                                                     [Material_ID],
                                                     [Audit_Type],
                                                     [Chapter_Name],
                                                     [Score],
                                                     [Create_Time],
                                                     [Creator_Name]
                                                )
                                                VALUES
	                                                ( 
		                                                '" + this.comboBox_supplierID.Text.ToString() + @"',
                                                        '" + comboBox_materialID.Text.ToString() + @"',
		                                                '过程审核',
		                                                '字段弃用',
		                                                '" + ProcessScore + @"',
		                                                '" + System.DateTime.Now + @"',
		                                                '" + this.textBox_Name.Text.ToString() + "')";
                string insertSQLSystem = @"INSERT INTO System_Process_Audit(  
                                                     [Supplier_ID],
                                                     [Material_ID],
                                                     [Audit_Type],
                                                     [Chapter_Name],
                                                     [Score],
                                                     [Create_Time],
                                                     [Creator_Name]
                                                )
                                                VALUES
	                                                ( 
		                                                '" + this.comboBox_supplierID.Text.ToString() + @"',
                                                        '" + comboBox_materialID.Text.ToString() + @"',
		                                                '体系审核',
		                                                '字段弃用',
		                                                '" + SystemScore + @"',
		                                                '" + System.DateTime.Now+ @"',
		                                                '" + this.textBox_Name.Text.ToString() + "')";
                try
                {
                    DBHelper.ExecuteNonQuery(insertSQLProcess);
                    DBHelper.ExecuteNonQuery(insertSQLSystem);
                    MessageBox.Show("记录保存成功！");
                    return;
                }
                catch (DBException ex)
                {
                    MessageBox.Show("过程/体系审核记录保存失败！");
                    return;
                }
            }
            else
            {
                //导入外部数据
                if(isChecked==0)
                {
                    MessageBox.Show("请先检查导入的数据");
                    return;
                }
                else
                {
                   for(int i= 0;i<insertListSQL.Count;i++)
                    {
                        try
                        {
                            DBHelper.ExecuteNonQuery(insertListSQL[i]);
                        }catch(DBException ex)
                        {
                            MessageBox.Show("第"+(i+1)+"行记录保存失败！");
                            return;
                        }
                    }
                }
            }
        }
        /// <summary>
        /// 生成模板
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            string path = "C:\\审核模板.xlsx";
            DataTable table = new DataTable();
            table.Columns.Add("供应商编号");
            table.Columns.Add("物料编号");
            table.Columns.Add("审核类型");
            table.Columns.Add("审核分数");
            table.Columns.Add("创建人");
            table.Columns.Add("创建时间");
            DialogResult dr;
            dr = MessageBox.Show("是否已有模板?", "检查", MessageBoxButtons.YesNoCancel,
                     MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2);
            if (dr == DialogResult.Yes)
            {
                return;
            }
            else if (dr == DialogResult.No)
            {
                try
                {
                    FileImportOrExport.TableToExcel(table, path);
                    this.LbpathShow.Text = "模板已存在路径： " + path;
                    this.LbpathShow.Visible = true;
                }
                catch (Exception ex)
                {
                    MessageBox.Show("模板生成失败,请确保同名的Excel是关闭的");
                }
            }
        }
        /// <summary>
        /// daoru
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            isChecked = 0;
            DialogResult dr1 = this.openFileDialog1.ShowDialog();
            if (DialogResult.OK == dr1) //判断是否选择文件  
            {
                string path = this.openFileDialog1.FileName.Trim();
                if (string.IsNullOrEmpty(path))
                {
                    MessageBox.Show("请选择要导入的EXCEL文件！！！", "信息");
                    return;
                }
                if (!File.Exists(path))  //判断文件是否存在  
                {
                    MessageBox.Show("信息", "找不到对应的Excel文件，请重新选择。");
                    return;
                }
                try
                {
                    excelTbl = FileImportOrExport.ExcelToTable(path);
                }catch(Exception ex)
                {
                    MessageBox.Show("请确保excel没有被打开");
                    return;
                }

                if (excelTbl == null)
                {
                    return;
                }
                if(this.dataTable.Rows.Count!=0)
                {
                    this.dataTable.DataSource = null;
                }
                this.dataTable.DataSource = excelTbl;
            }
               
        }
        /// <summary>
        /// 使用导入
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            this.groupBox2.Visible = true;
            isOut = 1;
        }
        /// <summary>
        /// 数据检查
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnDataCheck_Click(object sender, EventArgs e)
        {
            isChecked = 0;
            //检查
            if (dataTable.Rows.Count < 1 )
            {
                MessageBox.Show("请先确保该表格内存在数据在检查", "表格为空");
                return;
            }
            if(this.dataTable.Columns.Count!=6)
            {
                MessageBox.Show("格式错位", "表格格式");
                return;
            }
            insertListSQL.Clear();
           
            if(excelTbl==null)
            {
                MessageBox.Show("无数据", "表格格式");
                return;
            }
            DataTable dt = excelTbl;
            string dateTime = "";
            for (int i = 0;i<dt.Rows.Count;i++)
            {
                string supplierId = dt.Rows[i][0].ToString().Trim();
                string matrialId = dt.Rows[i][1].ToString().Trim();
                string type = dt.Rows[i][2].ToString().Trim();
                string score = dt.Rows[i][3].ToString().Trim();
                string creator = dt.Rows[i][4].ToString().Trim();
                string time = dt.Rows[i][5].ToString().Trim();
                if (string.IsNullOrEmpty(supplierId))
                {
                    MessageBox.Show("第" + (i + 1) + "行，供应商编号不得为空", "数据检查");
                    return;
                }
                if (string.IsNullOrEmpty(matrialId))
                {
                    MessageBox.Show("第" + (i + 1) + "行，物料编号不得为空", "数据检查");
                    return;
                }
                else
                {
                    DataTable able = supplierPerformanceBLL.queryMaterialID(supplierId);
                    if (able == null || able.Rows.Count < 1)
                    {
                        MessageBox.Show("第" + (i + 1) + "行，该供应商没有对应的物料关系", "数据检查");
                        return;
                    }
                }
                if (string.IsNullOrEmpty(type))
                {
                    MessageBox.Show("第" + (i + 1) + "行，审核类型不得为空", "数据检查");
                    return;
                }
                else
                {
                    if (type == "过程审核" || type == "体系审核")
                    {
                        //
                    }
                    else
                    {
                        MessageBox.Show("第" + (i + 1) + "行，审核类型只能是：过程审核/体系审核", "数据检查");
                        return;
                    }
                }
                if (string.IsNullOrEmpty(score))
                {
                    MessageBox.Show("第" + (i + 1) + "行，审核分数不得为空", "数据检查");
                    return;
                }
                else
                {
                    try
                    {
                        Convert.ToDouble(score);
                    }
                    catch (Exception ed)
                    {
                        MessageBox.Show("第" + (i + 1) + "行，审核分数输入有误", "数据检查");
                        return;
                    }
                }
                if (string.IsNullOrEmpty(creator))
                {
                    MessageBox.Show("第" + (i + 1) + "行，创建人不得为空", "数据检查");
                    return;
                }
                if (string.IsNullOrEmpty(time))
                {
                    MessageBox.Show("第" + (i + 1) + "行，创建时间不得为空", "数据检查");
                    return;
                }
                try
                {
                    
                    DateTime dtime = Convert.ToDateTime(time);
                    dateTime =  string.Format("{0:yyyy-MM-dd HH:mm:ss.fff}", dtime);
                }
                catch(Exception ex)
                {
                    MessageBox.Show("第" + (i + 1) + "行，创建时间格式不正确", "数据检查");
                    return;
                }
                
                string insertSQL = @"INSERT INTO System_Process_Audit(  
                                                     [Supplier_ID],
                                                     [Material_ID],
                                                     [Audit_Type],
                                                     [Score],
                                                     [Create_Time],
                                                     [Creator_Name]
                                                )
                                                VALUES
	                                                ( 
		                                                '" + supplierId + @"',
                                                        '" + matrialId + @"',
		                                                '" + type + @"',
		                                                '" + score + @"',
		                                                '" + dateTime + @"',
		                                                '" + creator + "')";
                insertListSQL.Add(insertSQL);
            }
            isChecked = 1;
            MessageBox.Show("检查无误");
        }

        //供应商id
        private void comboBox_supplierID_SelectedIndexChanged(object sender, EventArgs e)
        {
            supplierID = comboBox_supplierID.Text.ToString();
            lable_supplierName.Text = supplierInfoTable.Rows[comboBox_supplierID.SelectedIndex]["Supplier_Name"].ToString();


            //选择的供应商变化时，则重新查询对应的物料信息
            materialInfoTable = supplierPerformanceBLL.queryMaterialID(supplierID);
            comboBox_materialID.DataSource = materialInfoTable;
            if (materialInfoTable.Rows.Count > 0)
            {
                comboBox_materialID.DisplayMember = materialInfoTable.Columns["Material_ID"].ToString();
                lable_materialName.Text = (materialInfoTable.Rows[comboBox_materialID.SelectedIndex]["Material_Name"]).ToString();
            }
        }

        //物料id
        private void comboBox_materialID_SelectedIndexChanged(object sender, EventArgs e)
        {
            materialID = comboBox_materialID.Text.ToString();
            if (materialInfoTable.Rows.Count > 0)
            {
                lable_materialName.Text = (materialInfoTable.Rows[comboBox_materialID.SelectedIndex]["Material_Name"]).ToString();
            }
        }
    }
}
