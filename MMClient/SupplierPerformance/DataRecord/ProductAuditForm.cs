﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Bll.SupplierPerformaceBLL;
using WeifenLuo.WinFormsUI.Docking;

namespace MMClient.SupplierPerformance
{
    public partial class ProductAuditForm : DockContent
    {
        String supplierID, materialID;
        String name;
        DateTime time;
        Decimal ATotalNum, ADefectNum, BTotalNum, BDefectNum, CTotalNum, CDefectNum;
        Decimal score;

        //按照“Product_Audit表”中字段的顺序依次添加，在调用写入数据库的方法时作为参数传入
        List<Object> savePAPara = new List<object>();

        DataTable supplierInfoTable, materialInfoTable;

        SupplierPerformanceBLL supplierPerformanceBLL = new SupplierPerformanceBLL();

        public ProductAuditForm()
        {
            InitializeComponent();
            //comboBox_materialID.Items.Add("222");
            //comboBox_materialID.Items.Add("333");
            //comboBox_supplierID.Items.Add("123");
            //comboBox_supplierID.Items.Add("456");

            //选择供应商id
            supplierInfoTable = supplierPerformanceBLL.querySupplier();
            comboBox_supplierID.DataSource = supplierInfoTable;
            comboBox_supplierID.DisplayMember = supplierInfoTable.Columns["Supplier_ID"].ToString();
            lable_supplierName.Text = (supplierInfoTable.Rows[comboBox_supplierID.SelectedIndex]["Supplier_Name"]).ToString();
        }

        /// <summary>
        /// 创建人
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBox_Name_TextChanged(object sender, EventArgs e)
        {
            name = textBox_Name.Text.ToString();
        }

        /// <summary>
        /// 供应商id
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void comboBox_supplierID_SelectedIndexChanged(object sender, EventArgs e)
        {
            supplierID = comboBox_supplierID.Text.ToString();
            lable_supplierName.Text = supplierInfoTable.Rows[comboBox_supplierID.SelectedIndex]["Supplier_Name"].ToString();

            //选择的供应商变化时，则重新查询对应的物料信息
            materialInfoTable = supplierPerformanceBLL.queryMaterialID(supplierID);
            comboBox_materialID.DataSource = materialInfoTable;
            if (materialInfoTable.Rows.Count > 0)
            {
                comboBox_materialID.DisplayMember = materialInfoTable.Columns["Material_ID"].ToString();
                lable_materialName.Text = (materialInfoTable.Rows[comboBox_materialID.SelectedIndex]["Material_Name"]).ToString();
            }
        }

        /// <summary>
        /// 物料id
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void comboBox_materialID_SelectedIndexChanged(object sender, EventArgs e)
        {
            materialID = comboBox_materialID.Text.ToString();
            if (materialInfoTable.Rows.Count > 0)
            {
                lable_materialName.Text = (materialInfoTable.Rows[comboBox_materialID.SelectedIndex]["Material_Name"]).ToString();
            }
        }

        /// <summary>
        /// 创建时间
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            time = Convert.ToDateTime(dateTimePicker1.Text);
        }

        /// <summary>
        /// "保存"Button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button_save_Click(object sender, EventArgs e)
        {
            //用户未选择时间则为默认日期，为防止没有默认日期，此处再进行一次赋值
            time = Convert.ToDateTime(dateTimePicker1.Text);

            //检查基本信息是否输入完整
            if (String.IsNullOrEmpty(supplierID) || String.IsNullOrEmpty(materialID) || String.IsNullOrEmpty(name))
            {
                MessageBox.Show("请输入完整的基本信息！");
                return;
            }

            //检查样本数量和缺陷数量是否为大于0的数字
            try
            {
                ATotalNum = Convert.ToDecimal(textBox_Atotal.Text);
                ADefectNum = Convert.ToDecimal(textBox_Adefect.Text);
                BTotalNum = Convert.ToDecimal(textBox_Btotal.Text);
                BDefectNum = Convert.ToDecimal(textBox_Bdefect.Text);
                CTotalNum = Convert.ToDecimal(textBox_Ctotal.Text);
                CDefectNum = Convert.ToDecimal(textBox_Cdefect.Text);

                if (ATotalNum < 0 || ADefectNum < 0 )
                {
                    MessageBox.Show("请检查输入的样本数和缺陷数是否大于0的数字！");
                    return;
                }

                if (BTotalNum < 0 || BDefectNum < 0 )
                {
                    MessageBox.Show("请检查输入的样本数和缺陷数是否为大于0的数字！");
                    return;
                }

                if (CTotalNum < 0  || CDefectNum < 0 )
                {
                    MessageBox.Show("请检查输入的样本数和缺陷数是否为大于0的数字！");
                    return;
                }

            }
            catch
            {
                MessageBox.Show("请检查输入的样本数和缺陷数是否为大于0的数字！");
                return;
            }

            //检查样本数量是否大于缺陷数量
            if (ATotalNum < ADefectNum || BTotalNum < BDefectNum || CTotalNum < CDefectNum)
            {
                MessageBox.Show("输入的样本数量需大于缺陷数量！");
                return;
            }

            //计算产品审核分数
            score = 100 * (1 - (ADefectNum * 10 + BDefectNum * 5 + CDefectNum * 1) / (ATotalNum * 10 + BTotalNum * 5 + CTotalNum * 1));

            //向List<Object> savePAPara中按照数据表字段顺序添加项
            savePAPara.Clear();
            savePAPara.Add(supplierID);
            savePAPara.Add(materialID);
            savePAPara.Add(ATotalNum);
            savePAPara.Add(BTotalNum);
            savePAPara.Add(CTotalNum);
            savePAPara.Add(ADefectNum);
            savePAPara.Add(BDefectNum);
            savePAPara.Add(CDefectNum);
            savePAPara.Add(score);
            savePAPara.Add(time);
            savePAPara.Add(name);

            int lines;
            lines = supplierPerformanceBLL.saveProductAuditRecord(savePAPara);

            if (lines > 0)
            {
                MessageBox.Show("产品审核结果保存成功！");
                return;
            }
        }
    }
}
