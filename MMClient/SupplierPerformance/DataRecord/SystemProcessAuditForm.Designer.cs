﻿namespace MMClient.SupplierPerformance
{
    partial class SystemProcessAuditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.textBox_Name = new System.Windows.Forms.TextBox();
            this.lable_materialName = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.comboBox_supplierID = new System.Windows.Forms.ComboBox();
            this.comboBox_materialID = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.lable_supplierName = new System.Windows.Forms.Label();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.button_save = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtProcessScore = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtSystemScore = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.LbpathShow = new System.Windows.Forms.Label();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.dataTable = new System.Windows.Forms.DataGridView();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.BtnDataCheck = new System.Windows.Forms.Button();
            this.groupBox4.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.textBox_Name);
            this.groupBox4.Controls.Add(this.lable_materialName);
            this.groupBox4.Controls.Add(this.label9);
            this.groupBox4.Controls.Add(this.comboBox_supplierID);
            this.groupBox4.Controls.Add(this.comboBox_materialID);
            this.groupBox4.Controls.Add(this.label2);
            this.groupBox4.Controls.Add(this.label22);
            this.groupBox4.Controls.Add(this.lable_supplierName);
            this.groupBox4.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.groupBox4.Location = new System.Drawing.Point(11, 11);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox4.Size = new System.Drawing.Size(752, 164);
            this.groupBox4.TabIndex = 3;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "基本信息";
            // 
            // textBox_Name
            // 
            this.textBox_Name.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox_Name.Location = new System.Drawing.Point(114, 93);
            this.textBox_Name.Margin = new System.Windows.Forms.Padding(2);
            this.textBox_Name.Name = "textBox_Name";
            this.textBox_Name.Size = new System.Drawing.Size(183, 23);
            this.textBox_Name.TabIndex = 43;
            // 
            // lable_materialName
            // 
            this.lable_materialName.AutoSize = true;
            this.lable_materialName.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lable_materialName.Location = new System.Drawing.Point(475, 58);
            this.lable_materialName.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lable_materialName.Name = "lable_materialName";
            this.lable_materialName.Size = new System.Drawing.Size(56, 17);
            this.lable_materialName.TabIndex = 35;
            this.lable_materialName.Text = "物料名称";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label9.Location = new System.Drawing.Point(46, 96);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(56, 17);
            this.label9.TabIndex = 42;
            this.label9.Text = "创建人：";
            // 
            // comboBox_supplierID
            // 
            this.comboBox_supplierID.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.comboBox_supplierID.FormattingEnabled = true;
            this.comboBox_supplierID.Location = new System.Drawing.Point(116, 31);
            this.comboBox_supplierID.Margin = new System.Windows.Forms.Padding(2);
            this.comboBox_supplierID.Name = "comboBox_supplierID";
            this.comboBox_supplierID.Size = new System.Drawing.Size(181, 25);
            this.comboBox_supplierID.TabIndex = 30;
            this.comboBox_supplierID.SelectedIndexChanged += new System.EventHandler(this.comboBox_supplierID_SelectedIndexChanged);
            // 
            // comboBox_materialID
            // 
            this.comboBox_materialID.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.comboBox_materialID.FormattingEnabled = true;
            this.comboBox_materialID.Location = new System.Drawing.Point(411, 31);
            this.comboBox_materialID.Margin = new System.Windows.Forms.Padding(2);
            this.comboBox_materialID.Name = "comboBox_materialID";
            this.comboBox_materialID.Size = new System.Drawing.Size(183, 25);
            this.comboBox_materialID.TabIndex = 34;
            this.comboBox_materialID.SelectedIndexChanged += new System.EventHandler(this.comboBox_materialID_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.Location = new System.Drawing.Point(46, 34);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 17);
            this.label2.TabIndex = 31;
            this.label2.Text = "供应商：";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label22.Location = new System.Drawing.Point(351, 34);
            this.label22.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(44, 17);
            this.label22.TabIndex = 33;
            this.label22.Text = "物料：";
            // 
            // lable_supplierName
            // 
            this.lable_supplierName.AutoSize = true;
            this.lable_supplierName.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lable_supplierName.Location = new System.Drawing.Point(163, 58);
            this.lable_supplierName.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lable_supplierName.Name = "lable_supplierName";
            this.lable_supplierName.Size = new System.Drawing.Size(68, 17);
            this.lable_supplierName.TabIndex = 32;
            this.lable_supplierName.Text = "供应商名称";
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "章节名";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "审核类型";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "得分";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // button_save
            // 
            this.button_save.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.button_save.Location = new System.Drawing.Point(331, 677);
            this.button_save.Margin = new System.Windows.Forms.Padding(2);
            this.button_save.Name = "button_save";
            this.button_save.Size = new System.Drawing.Size(95, 26);
            this.button_save.TabIndex = 2;
            this.button_save.Text = "保存结果";
            this.button_save.UseVisualStyleBackColor = true;
            this.button_save.Click += new System.EventHandler(this.button_save_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtProcessScore);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtSystemScore);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(11, 181);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(751, 98);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "录入体系/过程总分";
            // 
            // txtProcessScore
            // 
            this.txtProcessScore.Location = new System.Drawing.Point(446, 43);
            this.txtProcessScore.Name = "txtProcessScore";
            this.txtProcessScore.Size = new System.Drawing.Size(181, 21);
            this.txtProcessScore.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(359, 49);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(89, 12);
            this.label3.TabIndex = 2;
            this.label3.Text = "过程审核分数：";
            // 
            // txtSystemScore
            // 
            this.txtSystemScore.Location = new System.Drawing.Point(136, 43);
            this.txtSystemScore.Name = "txtSystemScore";
            this.txtSystemScore.Size = new System.Drawing.Size(181, 21);
            this.txtSystemScore.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(47, 48);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "体系审核分数：";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.BtnDataCheck);
            this.groupBox2.Controls.Add(this.dataTable);
            this.groupBox2.Controls.Add(this.LbpathShow);
            this.groupBox2.Controls.Add(this.button2);
            this.groupBox2.Controls.Add(this.button1);
            this.groupBox2.Location = new System.Drawing.Point(11, 330);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(751, 342);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "导入体系/过程总分";
            this.groupBox2.Visible = false;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(283, 16);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(112, 20);
            this.button2.TabIndex = 1;
            this.button2.Text = "导入记录";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(21, 16);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(112, 20);
            this.button1.TabIndex = 0;
            this.button1.Text = "点击生成模板";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // LbpathShow
            // 
            this.LbpathShow.AutoSize = true;
            this.LbpathShow.Location = new System.Drawing.Point(19, 42);
            this.LbpathShow.Name = "LbpathShow";
            this.LbpathShow.Size = new System.Drawing.Size(41, 12);
            this.LbpathShow.TabIndex = 2;
            this.LbpathShow.Text = "label5";
            this.LbpathShow.Visible = false;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // dataTable
            // 
            this.dataTable.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dataTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataTable.Location = new System.Drawing.Point(6, 60);
            this.dataTable.Name = "dataTable";
            this.dataTable.ReadOnly = true;
            this.dataTable.RowTemplate.Height = 23;
            this.dataTable.Size = new System.Drawing.Size(739, 276);
            this.dataTable.TabIndex = 3;
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Location = new System.Drawing.Point(9, 302);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(77, 12);
            this.linkLabel1.TabIndex = 4;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "使用外部导入";
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // BtnDataCheck
            // 
            this.BtnDataCheck.Location = new System.Drawing.Point(564, 16);
            this.BtnDataCheck.Name = "BtnDataCheck";
            this.BtnDataCheck.Size = new System.Drawing.Size(112, 20);
            this.BtnDataCheck.TabIndex = 4;
            this.BtnDataCheck.Text = "数据检查";
            this.BtnDataCheck.UseVisualStyleBackColor = true;
            this.BtnDataCheck.Click += new System.EventHandler(this.BtnDataCheck_Click);
            // 
            // SystemProcessAuditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(774, 714);
            this.Controls.Add(this.linkLabel1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.button_save);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "SystemProcessAuditForm";
            this.Text = "体系/过程审核";
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label lable_materialName;
        private System.Windows.Forms.ComboBox comboBox_supplierID;
        private System.Windows.Forms.ComboBox comboBox_materialID;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label lable_supplierName;
        private System.Windows.Forms.TextBox textBox_Name;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.Button button_save;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtProcessScore;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtSystemScore;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label LbpathShow;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.DataGridView dataTable;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.Button BtnDataCheck;
    }
}