﻿using Lib.Bll.SupplierPerformaceBLL;
using Lib.Common.MMCException.IDAL;
using Lib.SqlServerDAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;

namespace MMClient.SupplierPerformance.qulity
{
    public partial class qulityForm :  DockContent
    {
        private string supplierID;
        bool isRow = false;//是原材料？
        SupplierPerformanceBLL supplierPerformanceBLL = new SupplierPerformanceBLL();
        public qulityForm()
        {
            InitializeComponent();
        }

        private void cbSupplierId_SelectedIndexChanged(object sender, EventArgs e)
        {
            supplierID = this.cbSupplierId.Text.ToString();
            this.lbSupplierName.Text = this.cbSupplierId.SelectedValue.ToString();
            //选择的供应商变化时，则重新查询对应的物料信息
            DataTable dtMaterial = supplierPerformanceBLL.queryMaterialID(supplierID);
            this.cbMaterialId.DataSource = dtMaterial;
            if (dtMaterial.Rows.Count > 0)
            {
                this.cbMaterialId.DisplayMember = "Material_ID";
                this.cbMaterialId.ValueMember = "Material_Name";
                this.lbMaterialName.Text = this.cbMaterialId.SelectedValue.ToString();
            }
            //根据物料编号查出物料是否属于原材料
            string sql = "select isRowMaterial from Bigclassfy where ClassfyID  = (select Material_Type from Material where Material_ID='" + this.cbMaterialId.Text.ToString() + "')";
            try
            {
                DataTable dt = DBHelper.ExecuteQueryDT(sql);
                if (dt.Rows.Count > 0)
                {
                    if ("0" == dt.Rows[0][0].ToString())
                    {
                        isRow = false;
                    }
                    else
                    {
                        isRow = true;
                    }

                }
            }
            catch
            {
                MessageBox.Show("查询失败！！");
            }
            if (isRow)
            {
                this.groupBox2.Visible = true;
                this.groupBox1.Visible = false;
            }
            else
            {
                this.groupBox1.Visible = true;
                this.groupBox2.Visible = false;
            }
        }

        private void qulityForm_Load(object sender, EventArgs e)
        {
            //选择供应商id
            DataTable dt  = supplierPerformanceBLL.querySupplier();
            this.cbSupplierId.DisplayMember = "Supplier_ID";
            this.cbSupplierId.ValueMember = "Supplier_Name";
            this.cbSupplierId.DataSource = dt;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox1.Text == "合格")
            {
                this.txtScore.Text = "100";
            }
            else if (comboBox1.Text == "内部附件改判合格")
            {
                this.txtScore.Text = "90";
            }
            else if (comboBox1.Text == "仲裁复检改判合格")
            {
                this.txtScore.Text = "80";
            }
            else if (comboBox1.Text == "让步接收")
            {
                this.txtScore.Text = "70";
            }
            else if (comboBox1.Text == "重检让步接收")
            {
                this.txtScore.Text = "60";
            }
            else if (comboBox1.Text == "仲裁让步接收")
            {
                this.txtScore.Text = "50";
            }
            else
            {
                this.txtScore.Text = "1";
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {

            if(isRow)
            {
                string sql = @"INSERT INTO  Raw_Material_Receive_Check  (  
	                            [Supplier_ID],
	                            [Material_ID],
	                            [RawMaterialReceiveCheck_Score],
	                            [Create_Time],
	                            [Creator_Name],
	                            [Check_Result]
                            )
                            VALUES (  
		                            '"+this.cbSupplierId.Text.ToString()+@"',
                                    '"+this.cbMaterialId.Text.ToString()+@"',
		                            '"+this.txtScore.Text.ToString()+@"',
		                            '"+System.DateTime.Now+@"', 
                                    '"+this.txtCreator.Text.ToString()+@"',
                                    '"+this.comboBox1.Text.ToString()+"')";
                try
                {
                    DBHelper.ExecuteNonQuery(sql);
                    MessageBox.Show("插入成功");
                    DialogResult dr;
                    dr = MessageBox.Show("是否接着录入?", "检查", MessageBoxButtons.YesNoCancel,
                    MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2);
                    if (dr == DialogResult.Yes)
                    {
                        setNull();
                        return;
                    }
                    else
                    {
                        this.Close();
                    }
                }
                catch(DBException exc)
                {
                    MessageBox.Show("插入失败");
                    return;
                }
            }
            else
            {
                //计算分数
                double SumNum;
                double rejectedNum;
                

              
                double PPMHigh;
                double PPMLow;
                double PPMScore;
                try
                {
                    PPMHigh = Convert.ToDouble(this.textBox3.Text);
                }
                catch (Exception EX)
                {
                    MessageBox.Show("PPM high 输入错误");
                    this.textBox3.Text = "";
                    return;
                }
                try
                {
                    PPMLow = Convert.ToDouble(this.textBox4.Text);
                }
                catch (Exception EX)
                {
                    MessageBox.Show("PPM low 输入错误");
                    this.textBox4.Text = "";
                    return;
                }
                if(PPMHigh<=PPMLow)
                {
                    MessageBox.Show("PPM high 大于 PPM low");
                    return;
                }
               
                try
                {
                    SumNum = Convert.ToDouble(this.textBox1.Text);
                    //自定义时间区间内的material 总数量 来源信息记录

                }
                catch (Exception EX)
                {
                    MessageBox.Show("本次供货数量输入错误");
                    this.textBox1.Text = "";
                    return;
                }
                try
                {
                    rejectedNum = Convert.ToDouble(this.textBox2.Text);

                }
                catch (Exception EX)
                {
                    MessageBox.Show("检验未达标/拒绝数量输入错误");
                    this.textBox2.Text = "";
                    return;
                }
                if(SumNum<=rejectedNum)
                {
                    MessageBox.Show("供货总数 大于 拒绝数");
                    return;
                }
                double PPM = 1000000 * (rejectedNum / SumNum);
                if (PPM <= PPMLow)
                {
                    PPMScore = 100;
                }
                else if (PPM >= PPMHigh)
                {
                    PPMScore = 0;
                }
                else
                {
                    PPMScore = (100 / (PPMHigh - PPMLow)) * (PPMHigh - PPM);
                }
                string sql = @"INSERT INTO  NonRaw_Material_Receive_Check  (
	                                [Supplier_ID],
	                                [Material_ID],
	                                [Reject_Num],
	                                [PPM],
	                                [PPM_Score],
	                                [Create_Time],
	                                [Creator_Name]
                                )
                                VALUES
	                                (
		                            '" + this.cbSupplierId.Text.ToString() + @"',
                                    '" + this.cbMaterialId.Text.ToString() + @"',
		                            '" + rejectedNum + @"',
		                            '" + PPM + @"', 
                                    '" + PPMScore + @"',
                                    '" + DateTime.Now + "','"+this.txtCreator.Text.ToString()+"')";
                try
                {
                    DBHelper.ExecuteNonQuery(sql);
                    MessageBox.Show("插入成功");
                    DialogResult dr;
                    dr = MessageBox.Show("是否接着录入?", "检查", MessageBoxButtons.YesNoCancel,
                    MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2);
                    if (dr == DialogResult.Yes)
                    {
                        setNull();
                        return;
                    }
                    else
                    {
                        this.Close();
                    }
                }
                catch (DBException exc)
                {
                    MessageBox.Show("插入失败");
                    return;
                }
            }
        }
        private void setNull()
        {
            this.textBox1.Text = "";
            this.textBox2.Text = "";
            this.textBox3.Text = "";
            this.textBox4.Text = "";
        }
    }
}
