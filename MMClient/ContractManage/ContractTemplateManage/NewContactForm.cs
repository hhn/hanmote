﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Collections;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MMClient.ContractManage;
using Word = Microsoft.Office.Interop.Word;
using System.Text.RegularExpressions;
using Lib.SqlServerDAL;
using Lib.SqlServerDAL.ContractManageDAL;
using Lib.Model.ContractManage.ContractDocs;
using WeifenLuo.WinFormsUI.Docking;

namespace MMClient.ContractManage.ContractTemplateManage
{
    public partial class NewContactForm : DockContent
    {
        //用于显示的DataTable
        DataTable displayDataTable = new DataTable();
        //为了维护章节和条款的有序，需要实时记录二者的位置
        //只要记录每个章节下有多少个条款就OK
        Hashtable TermsOfSection = new Hashtable();
        //3个数据库表的数据库工具
        ContractTemplateDAL contractTemplateDALTool = new ContractTemplateDAL();
        ContractTemplateSectionDAL contractTemplateSectionDALTool = new ContractTemplateSectionDAL();
        ContractTemplateTermDAL contractTemplateTermDALTool = new ContractTemplateTermDAL();

        public NewContactForm()
        {
            InitializeComponent();

            //初始化
            initialDataTable(displayDataTable);
            //绑定数据源
            this.dgvContractTemplateDisplay.DataSource = displayDataTable;
        }

        /// <summary>
        /// 初始化DataTable
        /// </summary>
        /// <param name="dt"></param>
        private void initialDataTable(DataTable dt) {
            //是否选中那个checkbox column调整大小
            this.IsSelected.Width = 60;

            DataColumn dc = new DataColumn("名称");
            dc.ReadOnly = true;
            dt.Columns.Add(dc);

            dc = new DataColumn("类型");
            dc.ReadOnly = true;
            dt.Columns.Add(dc);

            dc = new DataColumn("说明");
            dc.ReadOnly = true;
            dt.Columns.Add(dc);

            dc = new DataColumn("自定义注释");
            dc.ReadOnly = true;
            dt.Columns.Add(dc);
        }

        /// <summary>
        /// 合同模板文件添加章节
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAddSection_Click(object sender, EventArgs e)
        {
            Contract_Template_Item_Section newItemSection = new Contract_Template_Item_Section();
            newItemSection.type = "章节";
            AddNewContractTemplateSectionForm newForm = new AddNewContractTemplateSectionForm(newItemSection);
            newForm.ShowDialog();
            if (newForm.isSaved) {
                //记录到Hashtable中去
                if (TermsOfSection.ContainsKey(newItemSection.name))
                {
                    MessageBox.Show("错误", "同名章节已存在", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                else {
                    TermsOfSection.Add(newItemSection.name, 0);
                }
                //确实是保存的，添加到DataTable中去
                newItemSection = newForm.ItemSection;
                DataRow dr = displayDataTable.NewRow();
                dr["名称"] = newItemSection.name;
                dr["类型"] = newItemSection.type;
                dr["说明"] = newItemSection.detail;
                dr["自定义注释"] = newItemSection.annotation;

                displayDataTable.Rows.Add(dr);
            }
        }

        /// <summary>
        /// 添加合同模板条款
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAddTerm_Click(object sender, EventArgs e)
        {
            //查看是否选定一个章节
            if (this.dgvContractTemplateDisplay.SelectedRows.Count == 0) {
                MessageBox.Show("请先添加章节", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            DataGridViewRow dgvr = this.dgvContractTemplateDisplay.SelectedRows[0];
            //记录当前行号
            int currentIndex = this.dgvContractTemplateDisplay.CurrentRow.Index;
            //类型，确定选中的是不是章节
            string type = dgvr.Cells[2].Value.ToString().Trim();
            //章节的名称
            string sectionName = dgvr.Cells[1].Value.ToString().Trim();
            if (!type.Equals("章节")) {
                MessageBox.Show("请选择章节", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            //增加条款
            Contract_Template_Item_Section newItemSection = new Contract_Template_Item_Section();
            newItemSection.type = "条款";
            AddNewContractTemplateTermForm newForm = new AddNewContractTemplateTermForm(newItemSection);
            newForm.ShowDialog();

            if (newForm.isSaved)
            {
                //确实是保存的，添加到DataTable中去
                newItemSection = newForm.ItemSection;
                DataRow dr = displayDataTable.NewRow();
                dr["名称"] = newItemSection.name;
                dr["类型"] = newItemSection.type;
                dr["说明"] = newItemSection.detail;
                dr["自定义注释"] = newItemSection.annotation;

                displayDataTable.Rows.InsertAt(dr, currentIndex + Convert.ToInt32(TermsOfSection[sectionName]) + 1);
                
                TermsOfSection[sectionName] = Convert.ToInt32(TermsOfSection[sectionName]) + 1;
            }
        }

        /// <summary>
        /// 删除章节和条款
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDelete_Click(object sender, EventArgs e)
        {
            //查看是否选定一个章节或者条款
            if (this.dgvContractTemplateDisplay.SelectedRows.Count == 0)
            {
                MessageBox.Show("请先选择删除对象", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            DataGridViewRow dgvr = this.dgvContractTemplateDisplay.SelectedRows[0];
            //记录当前行号
            int currentIndex = this.dgvContractTemplateDisplay.CurrentRow.Index;
            //类型，确定选中的是不是章节
            string type = dgvr.Cells[2].Value.ToString().Trim();
            //章节的名称
            string sectionName = dgvr.Cells[1].Value.ToString().Trim();
            if (type.Equals("章节"))
            {
                DialogResult result = MessageBox.Show("确定要删除该章节及下属所有条款？", "警告", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
                if (result == DialogResult.Cancel)
                {
                    return;
                }
                else
                {
                    //删除该章节
                    displayDataTable.Rows.RemoveAt(currentIndex);
                    //删除下属条款
                    for (int i = 0; i < Convert.ToInt32(TermsOfSection[sectionName]); i++)
                    {
                        displayDataTable.Rows.RemoveAt(currentIndex);
                    }
                    //从TermsOfSection中移除
                    TermsOfSection.Remove(sectionName);
                }
            }
            else {
                DialogResult result = MessageBox.Show("确定要删除该条款？", "警告", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
                if (result == DialogResult.Cancel)
                {
                    return;
                }
                else
                {
                    //删除该条款
                    displayDataTable.Rows.RemoveAt(currentIndex);

                    TermsOfSection[sectionName] = Convert.ToInt32(TermsOfSection[sectionName]) - 1;
                }
            }
        }

        /// <summary>
        /// 点击生成，生成合同模板
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnGenerate_Click(object sender, EventArgs e)
        {
            //先检测是否可以生成
            if (this.tbContractTemplateID.Text.Trim().Equals("")
                || this.tbContractTemplateName.Text.Trim().Equals("")
                || this.dgvContractTemplateDisplay.Rows.Count == 0) {
                    MessageBox.Show("错误", "请填写完整", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
            }

            //生成合同模板
            //生成的过程就是把合同模板ID和Name，以及添加的章节、条款的信息都记录下来
            bool isSucceed = true;
            //记录合同模板
            Contract_Template newContractTemplate = new Contract_Template();
            newContractTemplate.ID = this.tbContractTemplateID.Text.Trim();
            newContractTemplate.Name = this.tbContractTemplateName.Text.Trim();
            //判断记录是否已存在
            if (contractTemplateDALTool.getContractTemplateByID(newContractTemplate.ID) != null) {
                MessageBox.Show("该编号合同模板已存在", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            newContractTemplate.Status = "未审核";
            newContractTemplate.Create_Time = System.DateTime.Now;
            isSucceed &= (contractTemplateDALTool.addNewContractTemplate(newContractTemplate) > 0);

            //记录章节、条款
            int orderOfSection = 0;
            int orderOfTerm = 0;
            string currentSectionID = "";
            for (int i = 0; i < displayDataTable.Rows.Count; i++) {
                DataRow dr = displayDataTable.Rows[i];
                string type = dr["类型"].ToString().Trim();
                if (type.Equals("章节")) {
                    Contract_Template_Section newSection = new Contract_Template_Section();
                    newSection.Name = dr["名称"].ToString();
                    newSection.Order_Number = orderOfSection;
                    newSection.Contract_Template_ID = newContractTemplate.ID;
                    newSection.Detail = dr["说明"].ToString();
                    newSection.Annotation = dr["自定义注释"].ToString();
                    //章节的ID的确定方法
                    newSection.ID = newSection.Name + "_" + newSection.Contract_Template_ID;
                    //insert
                    isSucceed &= (contractTemplateSectionDALTool.addNewContractTemplateSection(newSection) > 0);
                    
                    currentSectionID = newSection.ID;
                    orderOfTerm = 0;
                    orderOfSection++;
                }
                else if (type.Equals("条款")) {
                    Contract_Template_Term newTerm = new Contract_Template_Term();
                    newTerm.Order_Number = orderOfTerm;
                    newTerm.Term_Sample_ID = dr["名称"].ToString();
                    newTerm.Section_ID = currentSectionID;
                    newTerm.Detail = dr["说明"].ToString();
                    newTerm.Annotation = dr["自定义注释"].ToString();
                    //insert
                    isSucceed &= (contractTemplateTermDALTool.addNewContractTemplateTerm(newTerm) > 0);

                    orderOfTerm++;
                }
            }

            if (isSucceed)
            {
                MessageBox.Show("合同模板新建成功!");
            }
            else {
                MessageBox.Show("合同模板新建失败!");
            }
        }
    }
}
