﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Model.ContractManage.ContractDocs;
using Lib.SqlServerDAL.ContractManageDAL;
using WeifenLuo.WinFormsUI.Docking;

namespace MMClient.ContractManage.ContractTemplateManage
{
    public partial class AddNewContractTemplateTermForm : DockContent
    {
        public Contract_Template_Item_Section ItemSection;
        public bool isSaved = false;
        //Contract_Template_Term_Sample数据库表查询
        ContractTemplateTermDAL sql_tool = new ContractTemplateTermDAL();

        public AddNewContractTemplateTermForm(Contract_Template_Item_Section _ItemSection)
        {
            InitializeComponent();

            this.ItemSection = _ItemSection;
            //初始化
            initialForm();
        }

        /// <summary>
        /// 初始化窗体
        /// </summary>
        private void initialForm()
        {
            //从数据库中查询得到所有的条款Sample的Name，加入到ComboBox中
            LinkedList<Contract_Template_Term_Sample> result = sql_tool.getAllTermSample();
            foreach (Contract_Template_Term_Sample term_sample in result) {
                this.cbxTermID.Items.Add(term_sample.Name);
            }

            if (ItemSection.name != null)
            {
                this.cbxTermID.SelectedIndex = this.cbxTermID.Items.IndexOf(ItemSection.name);
            }
            if (ItemSection.detail != null)
            {
                this.tbTermDetail.Text = ItemSection.detail;
            }
            if (ItemSection.annotation != null)
            {
                this.rtbAnnotation.Text = ItemSection.annotation;
            }
        }

        /// <summary>
        /// 点击保存按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSave_Click(object sender, EventArgs e)
        {
            ItemSection.name = this.cbxTermID.SelectedItem.ToString();
            ItemSection.detail = this.tbTermDetail.Text.Trim();
            ItemSection.annotation = this.rtbAnnotation.Text.Trim();

            isSaved = true;
            this.Close();
        }
    }
}
