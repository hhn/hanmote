﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Collections;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MMClient.ContractManage;
using Word = Microsoft.Office.Interop.Word;
using System.Text.RegularExpressions;
using Lib.SqlServerDAL;
using Lib.SqlServerDAL.ContractManageDAL;
using Lib.Model.ContractManage.ContractDocs;
using WeifenLuo.WinFormsUI.Docking;
using Lib.Common.CommonUtils;
using Microsoft.Office.Interop.Word;

namespace MMClient.ContractManage.ContractTemplateManage
{
    public partial class NewContractTemplateForm : DockContent
    {
        //用来记录合同条款的信息
        private class TermInfo
        {
            public string TermFileName;
            //默认为0
            public int order = 0;
            public string title;
        }

        //用于显示的DataTable
        System.Data.DataTable displayDataTable = new System.Data.DataTable();
        //为了维护章节和条款的有序，需要实时记录二者的位置
        //只要记录每个章节下有多少个条款就OK
        Hashtable TermsOfSection = new Hashtable();
        //3个数据库表的数据库工具
        ContractTemplateDAL contractTemplateDALTool = new ContractTemplateDAL();
        ContractTemplateSectionDAL contractTemplateSectionDALTool = new ContractTemplateSectionDAL();
        ContractTemplateTermDAL contractTemplateTermDALTool = new ContractTemplateTermDAL();
        //
        FTPTool ftp = FTPTool.getInstance();

        public NewContractTemplateForm()
        {
            InitializeComponent();

            //初始化
            initialDataTable(displayDataTable);
            //绑定数据源
            this.dgvContractTemplateDisplay.DataSource = displayDataTable;
        }

        /// <summary>
        /// 初始化DataTable
        /// </summary>
        /// <param name="dt"></param>
        private void initialDataTable(System.Data.DataTable dt) {
            //是否选中那个checkbox column调整大小
            this.IsSelected.Width = 60;

            DataColumn dc = new DataColumn("名称");
            dc.ReadOnly = true;
            dt.Columns.Add(dc);

            dc = new DataColumn("类型");
            dc.ReadOnly = true;
            dt.Columns.Add(dc);

            dc = new DataColumn("说明");
            dc.ReadOnly = true;
            dt.Columns.Add(dc);

            dc = new DataColumn("自定义注释");
            dc.ReadOnly = true;
            dt.Columns.Add(dc);
        }

        /// <summary>
        /// 合同模板文件添加章节
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAddSection_Click(object sender, EventArgs e)
        {
            Contract_Template_Item_Section newItemSection = new Contract_Template_Item_Section();
            newItemSection.type = "章节";
            AddNewContractTemplateSectionForm newForm = new AddNewContractTemplateSectionForm(newItemSection);
            newForm.ShowDialog();
            if (newForm.isSaved) {
                //记录到Hashtable中去
                if (TermsOfSection.ContainsKey(newItemSection.name))
                {
                    MessageBox.Show("错误", "同名章节已存在", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                else {
                    TermsOfSection.Add(newItemSection.name, 0);
                }
                //确实是保存的，添加到DataTable中去
                newItemSection = newForm.ItemSection;
                DataRow dr = displayDataTable.NewRow();
                dr["名称"] = newItemSection.name;
                dr["类型"] = newItemSection.type;
                dr["说明"] = newItemSection.detail;
                dr["自定义注释"] = newItemSection.annotation;

                displayDataTable.Rows.Add(dr);
            }
        }

        /// <summary>
        /// 添加合同模板条款
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAddTerm_Click(object sender, EventArgs e)
        {
            //查看是否选定一个章节
            if (this.dgvContractTemplateDisplay.SelectedRows.Count == 0) {
                MessageBox.Show("请先添加章节", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            DataGridViewRow dgvr = this.dgvContractTemplateDisplay.SelectedRows[0];
            //记录当前行号
            int currentIndex = this.dgvContractTemplateDisplay.CurrentRow.Index;
            //类型，确定选中的是不是章节
            string type = dgvr.Cells[2].Value.ToString().Trim();
            //章节的名称
            string sectionName = dgvr.Cells[1].Value.ToString().Trim();
            if (!type.Equals("章节")) {
                MessageBox.Show("请选择章节", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            //增加条款
            Contract_Template_Item_Section newItemSection = new Contract_Template_Item_Section();
            newItemSection.type = "条款";
            AddNewContractTemplateTermForm newForm = new AddNewContractTemplateTermForm(newItemSection);
            newForm.ShowDialog();

            if (newForm.isSaved)
            {
                //确实是保存的，添加到DataTable中去
                newItemSection = newForm.ItemSection;
                DataRow dr = displayDataTable.NewRow();
                dr["名称"] = newItemSection.name;
                dr["类型"] = newItemSection.type;
                dr["说明"] = newItemSection.detail;
                dr["自定义注释"] = newItemSection.annotation;

                displayDataTable.Rows.InsertAt(dr, currentIndex + Convert.ToInt32(TermsOfSection[sectionName]) + 1);
                
                TermsOfSection[sectionName] = Convert.ToInt32(TermsOfSection[sectionName]) + 1;
            }
        }

        /// <summary>
        /// 删除章节和条款
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDelete_Click(object sender, EventArgs e)
        {
            //查看是否选定一个章节或者条款
            if (this.dgvContractTemplateDisplay.SelectedRows.Count == 0)
            {
                MessageBox.Show("请先选择删除对象", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            DataGridViewRow dgvr = this.dgvContractTemplateDisplay.SelectedRows[0];
            //记录当前行号
            int currentIndex = this.dgvContractTemplateDisplay.CurrentRow.Index;
            //类型，确定选中的是不是章节
            string type = dgvr.Cells[2].Value.ToString().Trim();
            //章节的名称
            string sectionName = dgvr.Cells[1].Value.ToString().Trim();
            if (type.Equals("章节"))
            {
                DialogResult result = MessageBox.Show("确定要删除该章节及下属所有条款？", "警告", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
                if (result == DialogResult.Cancel)
                {
                    return;
                }
                else
                {
                    //删除该章节
                    displayDataTable.Rows.RemoveAt(currentIndex);
                    //删除下属条款
                    for (int i = 0; i < Convert.ToInt32(TermsOfSection[sectionName]); i++)
                    {
                        displayDataTable.Rows.RemoveAt(currentIndex);
                    }
                    //从TermsOfSection中移除
                    TermsOfSection.Remove(sectionName);
                }
            }
            else {
                DialogResult result = MessageBox.Show("确定要删除该条款？", "警告", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
                if (result == DialogResult.Cancel)
                {
                    return;
                }
                else
                {
                    //删除该条款
                    displayDataTable.Rows.RemoveAt(currentIndex);

                    TermsOfSection[sectionName] = Convert.ToInt32(TermsOfSection[sectionName]) - 1;
                }
            }
        }

        /// <summary>
        /// 点击生成，生成合同模板
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnGenerate_Click(object sender, EventArgs e)
        {
            //先检测是否可以生成
            if (this.tbContractTemplateID.Text.Trim().Equals("")
                || this.tbContractTemplateName.Text.Trim().Equals("")
                || this.dgvContractTemplateDisplay.Rows.Count == 0) {
                    MessageBox.Show("错误", "请填写完整", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
            }

            //生成合同模板
            //*** 初始设计版本：生成的过程就是把合同模板ID和Name，以及添加的章节、条款的信息都记录下来  ***
            //### 更新后的设计版本：保留记录章节，条款的过程不变；同时下载所有对应的word文件，在本地合并，将合并后生成的文档上传到服务器
            //并删除本地的遗留文件 ###
            bool isSucceed = true;
            //记录合同模板
            Contract_Template newContractTemplate = new Contract_Template();
            newContractTemplate.ID = this.tbContractTemplateID.Text.Trim();
            newContractTemplate.Name = this.tbContractTemplateName.Text.Trim();
            //判断记录是否已存在
            if (contractTemplateDALTool.getContractTemplateByID(newContractTemplate.ID) != null) {
                MessageBox.Show("该编号合同模板已存在", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            newContractTemplate.Status = "未审核";
            newContractTemplate.Create_Time = System.DateTime.Now;
            isSucceed &= (contractTemplateDALTool.addNewContractTemplate(newContractTemplate) > 0);

            //默认的本地存放位置
            string defaultDic = @"D:\Contracts\";
            if (!File.Exists(defaultDic))
            {
                System.IO.Directory.CreateDirectory(defaultDic);
            }
            //pp存放term_sample在服务器的位置和本地存放的位置,termFileList存放word在本地的位置,termInfoList存放条款信息
            LinkedList<string> termFileList = new LinkedList<string>();
            LinkedList<TermInfo> termInfoList = new LinkedList<TermInfo>();
            Dictionary<string, string> pp = new Dictionary<string, string>();

            //记录章节、条款
            #region 记录章节、条款
            int orderOfSection = 0;
            int orderOfTerm = 0;
            string currentSectionID = "";
            int order = 0;
            for (int i = 0; i < displayDataTable.Rows.Count; i++) {
                DataRow dr = displayDataTable.Rows[i];
                string type = dr["类型"].ToString().Trim();
                if (type.Equals("章节")) {
                    Contract_Template_Section newSection = new Contract_Template_Section();
                    newSection.Name = dr["名称"].ToString();
                    newSection.Order_Number = orderOfSection;
                    newSection.Contract_Template_ID = newContractTemplate.ID;
                    newSection.Detail = dr["说明"].ToString();
                    newSection.Annotation = dr["自定义注释"].ToString();
                    //章节的ID的确定方法
                    newSection.ID = newSection.Name + "_" + newSection.Contract_Template_ID;
                    //insert
                    isSucceed &= (contractTemplateSectionDALTool.addNewContractTemplateSection(newSection) > 0);
                    
                    currentSectionID = newSection.ID;
                    orderOfTerm = 0;
                    orderOfSection++;
                }
                else if (type.Equals("条款")) {
                    Contract_Template_Term newTerm = new Contract_Template_Term();
                    newTerm.Order_Number = orderOfTerm;
                    newTerm.Term_Sample_ID = dr["名称"].ToString();
                    newTerm.Section_ID = currentSectionID;
                    newTerm.Detail = dr["说明"].ToString();
                    newTerm.Annotation = dr["自定义注释"].ToString();

                    Contract_Template_Term_Sample term_sample = contractTemplateTermDALTool.getTermSampleByName(newTerm.Term_Sample_ID);
                    string la = term_sample.FileName;
                    int index = la.IndexOf(@"/");
                    string remoteFile = la.Substring(index + 1);
                    string localFile = defaultDic + term_sample.Name + ".docx";
                    if (ftp.checkFileExist(remoteFile))
                    {
                        pp.Add(remoteFile, localFile);
                        termFileList.AddLast(localFile);
                        TermInfo termInfo = new TermInfo();
                        termInfo.TermFileName = localFile;
                        termInfo.order = order;
                        order++;
                        termInfoList.AddLast(termInfo);
                    }
                    else
                    {
                        MessageBox.Show("条款文件" + remoteFile + "不存在！");
                        return;
                    }
                    //insert
                    isSucceed &= (contractTemplateTermDALTool.addNewContractTemplateTerm(newTerm) > 0);

                    orderOfTerm++;
                }
            }
            LinkedListNode<TermInfo> lastNode = termInfoList.Last;
            if (lastNode != null)
            {
                lastNode.Value.order = -1;
            }
            #endregion

            #region 下载条款文件，合并生成对应的模板文件
            //下载条款文件，合并生成对应的模板文件
            LinkedList<string> failedFiles = ftp.download(pp);
            string contractFile = newContractTemplate.Name;
            string localContractFile = "";
            if (failedFiles.Count > 0)
            {
                isSucceed = false;
            }
            else
            {
                localContractFile = defaultDic + contractFile + ".docx";
                MergeFilesByInsert(localContractFile, termInfoList);
            }
            #endregion 
   
            #region 上传生成的模板文件，并删除遗留的本地文件
            string remoteContractFile = "合同模板/模板文件/" + contractFile + ".docx";
            bool isUploaded = ftp.upload(localContractFile, remoteContractFile);
            if (!isUploaded)
            {
                isSucceed = false;
            }
            else
            {
                if(File.Exists(localContractFile))
                {
                    File.Delete(localContractFile);
                }
                foreach(string src in termFileList)
                {
                    if(File.Exists(src))
                    {
                        File.Delete(src);
                    }
                }
            }
            #endregion

            if (isSucceed)
            {
                MessageBox.Show("已成功创建合同模板!");
            }
            else {
                MessageBox.Show("合同模板创建失败!");
            }
        }

        #region 合并word
        /// <summary>
        /// 采取插入合并两个word
        /// </summary>
        /// <param name="desFileName">目标word文件</param>
        /// <param name="srcFileNameList">需要插入目标word的src文件</param>
        private void MergeFilesByInsert(string desFileName, LinkedList<TermInfo> termInfoList)
        {
            Microsoft.Office.Interop.Word.Application wordApp = new Microsoft.Office.Interop.Word.Application();
            Microsoft.Office.Interop.Word.Document objDocLast = null;

            object objMissing = System.Reflection.Missing.Value;
            object objFalse = false;
            object confirmConversion = false;
            object link = false;
            object attachment = false;
            object desObject = desFileName;

            //创建file
            createWordFile(desFileName);
            //MessageBox.Show("tt");

            try
            {
                //打开模板文件
                //MessageBox.Show("try");
                objDocLast = wordApp.Documents.Open(
                 ref desObject,    //FileName  
                 ref objMissing,   //ConfirmVersions  
                 ref objMissing,   //ReadOnly  
                 ref objMissing,   //AddToRecentFiles  
                 ref objMissing,   //PasswordDocument  
                 ref objMissing,   //PasswordTemplate  
                 ref objMissing,   //Revert  
                 ref objMissing,   //WritePasswordDocument  
                 ref objMissing,   //WritePasswordTemplate  
                 ref objMissing,   //Format  
                 ref objMissing,   //Enconding  
                 ref objMissing,   //Visible  
                 ref objMissing,   //OpenAndRepair  
                 ref objMissing,   //DocumentDirection  
                 ref objMissing,   //NoEncodingDialog  
                 ref objMissing    //XMLTransform  
                 );
                MessageBox.Show("00000");
                objDocLast.Activate();

                //*******
                MessageBox.Show("11111");

                //遍历所有需要插入的word
                foreach (TermInfo termInfo in termInfoList)
                {
                    //判断下是否需要插入标题
                    if (termInfo.order > 0)
                    {
                        //插入标题
                        StringBuilder titleStr = new StringBuilder("第");
                        titleStr.Append(getChineseNumber(termInfo.order))
                            .Append("条  ")
                            .Append(termInfo.title);
                        //字体加粗
                        wordApp.Selection.Font.Bold = 1;
                        //设置段间距
                        wordApp.Selection.ParagraphFormat.LineUnitBefore = 0.5f;
                        wordApp.Selection.ParagraphFormat.LineUnitAfter = 0.5f;
                        //设置首行缩进
                        wordApp.Selection.ParagraphFormat.CharacterUnitFirstLineIndent = 2;
                        //插入文本
                        wordApp.Selection.TypeText(titleStr.ToString());
                        //回车符
                        wordApp.Selection.TypeParagraph();
                    }
                    else if (termInfo.order == -1)
                    {
                        //插入换页符
                        object nextPageBreak = (int)WdBreakType.wdSectionBreakNextPage;
                        //字体不加粗
                        wordApp.Selection.Font.Bold = 0;
                        //先插入一句 "（以下无正文，为合同签署页）"
                        wordApp.Selection.TypeText("（以下无正文，为合同签署页）");
                        wordApp.Selection.InsertBreak(ref nextPageBreak);
                    }

                    wordApp.Selection.InsertFile(
                        termInfo.TermFileName,
                        ref objMissing,
                        ref confirmConversion,
                        ref link,
                        ref attachment
                        );
                }
                //保存
                objDocLast.SaveAs(
                  ref desObject,      //FileName  
                  ref objMissing,     //FileFormat  
                  ref objMissing,     //LockComments  
                  ref objMissing,     //PassWord       
                  ref objMissing,     //AddToRecentFiles  
                  ref objMissing,     //WritePassword  
                  ref objMissing,     //ReadOnlyRecommended  
                  ref objMissing,     //EmbedTrueTypeFonts  
                  ref objMissing,     //SaveNativePictureFormat  
                  ref objMissing,     //SaveFormsData  
                  ref objMissing,     //SaveAsAOCELetter,  
                  ref objMissing,     //Encoding  
                  ref objMissing,     //InsertLineBreaks  
                  ref objMissing,     //AllowSubstitutions  
                  ref objMissing,     //LineEnding  
                  ref objMissing      //AddBiDiMarks  
                );
                objDocLast.Close(ref objMissing, ref objMissing, ref objMissing);
            }
            catch (Exception e)
            {
                //MessageBox.Show("生成失败");
                throw e;
            }
            finally
            {
                //关闭wordApp
                wordApp.Quit(
                  ref objMissing,     //SaveChanges  
                  ref objMissing,     //OriginalFormat  
                  ref objMissing      //RoutDocument  
                  );
                wordApp = null;

                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
                GC.WaitForPendingFinalizers();
            }
        }

        /// <summary>
        /// 创建word文件
        /// </summary>
        /// <param name="fileName">文件名称</param>
        private void createWordFile(string fileName)
        {
            if (File.Exists(fileName))
            {
                MessageBox.Show("文件已存在");
                return;
            }
            object fileObject = fileName;
            Microsoft.Office.Interop.Word.Application wordApp = new Microsoft.Office.Interop.Word.Application();
            Microsoft.Office.Interop.Word.Document wordDocument = new Microsoft.Office.Interop.Word.Document();
            object missing = System.Reflection.Missing.Value;
            try
            {
                wordDocument = wordApp.Documents.Add(
                    ref missing,
                    ref missing,
                    ref missing,
                    ref missing);
            }
            finally
            {
                wordDocument.SaveAs(ref fileObject, ref missing, ref missing, ref missing, ref missing,
                            ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing,
                            ref missing, ref missing, ref missing, ref missing);
                wordDocument.Close(ref missing, ref missing, ref missing);

                wordApp.Quit(ref missing, ref missing, ref missing);
                //结束WINWORD.exe程序
                wordApp = null;
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
                GC.WaitForPendingFinalizers();
            }
        }

        /// <summary>
        /// 获得数字的中文字符(默认数字不会大于100)
        /// </summary>
        /// <param name="num"></param>
        /// <returns></returns>
        private string getChineseNumber(int num)
        {
            StringBuilder strBui = new StringBuilder();
            int tens = num / 10;
            int ones = num % 10;

            if (tens == 0)
            {
                strBui.Append(getChineseSingleDigit(ones));
            }
            else
            {
                if (tens > 1)
                {
                    strBui.Append(getChineseSingleDigit(tens));
                }
                strBui.Append("十")
                    .Append(getChineseSingleDigit(ones));
            }

            return strBui.ToString();
        }

        /// <summary>
        /// 返回单个字符对应的中文
        /// </summary>
        /// <param name="num"></param>
        /// <returns></returns>
        private string getChineseSingleDigit(int num)
        {
            string result = "";
            switch (num)
            {
                case 1: result = "一"; break;
                case 2: result = "二"; break;
                case 3: result = "三"; break;
                case 4: result = "四"; break;
                case 5: result = "五"; break;
                case 6: result = "六"; break;
                case 7: result = "七"; break;
                case 8: result = "八"; break;
                case 9: result = "九"; break;

                default: result = ""; break;
            }

            return result;
        }
        #endregion
    }
}
