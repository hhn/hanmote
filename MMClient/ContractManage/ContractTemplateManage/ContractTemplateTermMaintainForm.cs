﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.SqlServerDAL.ContractManageDAL;
using WeifenLuo.WinFormsUI.Docking;
using Lib.Common.CommonUtils;

namespace MMClient.ContractManage.ContractTemplateManage
{
    public partial class ContractTemplateTermMaintainForm : DockContent
    {
        //数据库工具
        ContractTemplateTermSampleDAL contractTemplateTermSampleDALTool = new ContractTemplateTermSampleDAL();
        //显示的数据
        DataTable displatDataTable = new DataTable();
        //封装的FTP客户端
        FTPTool ftpClient = FTPTool.getInstance();

        public ContractTemplateTermMaintainForm()
        {
            InitializeComponent();
            //初始化DataTable结构
            initDataTable(displatDataTable);

            this.dgvDisplay.DataSource = displatDataTable;
        }

        /// <summary>
        /// 窗体加载
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ContractTemplateTermMaintainForm_Load(object sender, EventArgs e)
        {
            initComboBox();
        }

        /// <summary>
        /// 进入上传合同模板条款界面
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAdd_Click(object sender, EventArgs e)
        {
            ContractTemplateTermEditForm termEditForm = new ContractTemplateTermEditForm();
            termEditForm.ShowDialog();
        }

        /// <summary>
        /// 初始化ComboBox
        /// </summary>
        private void initComboBox() {
            string sql = "SELECT DISTINCT Class FROM Contract_Template_Term_Sample";
            DataTable dt;
            dt = contractTemplateTermSampleDALTool.getDataTableByExecSql(sql);
            if (dt != null && dt.Rows.Count > 0) {
                foreach (DataRow dr in dt.Rows) { 
                    string termClass = dr["Class"].ToString();
                    this.cbxTermType.Items.Add(termClass);
                }
            }

            this.cbxTermType.SelectedIndex = 0;
        }

        private void initDataTable(DataTable dt) {
            DataGridViewCheckBoxColumn checkBoxColumn = new DataGridViewCheckBoxColumn();
            {
                checkBoxColumn.HeaderText = "是否选中";
                checkBoxColumn.Name = "是否选中";
                checkBoxColumn.CellTemplate = new DataGridViewCheckBoxCell();

            }
            checkBoxColumn.FillWeight = 90;
            this.dgvDisplay.Columns.Insert(0, checkBoxColumn);

            DataColumn dc;
            dc = new DataColumn("条款名称");
            dc.ReadOnly = true;
            dt.Columns.Add(dc);

            dc = new DataColumn("条款类型");
            dc.ReadOnly = true;
            dt.Columns.Add(dc);

            dc = new DataColumn("注释");
            dc.ReadOnly = true;
            dt.Columns.Add(dc);
        }

        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSearch_Click(object sender, EventArgs e)
        {
            string type = this.cbxTermType.SelectedItem.ToString();
            DataTable resultDT = contractTemplateTermSampleDALTool.getContractTemplateTermSampleByType(type);
            if (resultDT == null)
                return;

            displatDataTable.Rows.Clear();
            foreach (DataRow dr in resultDT.Rows) {
                DataRow displatDr = displatDataTable.NewRow();
                displatDr["条款名称"] = dr["Name"];
                displatDr["条款类型"] = dr["Class"];
                displatDr["注释"] = dr["Detail"];

                displatDataTable.Rows.Add(displatDr);
            }
        }

        /// <summary>
        /// 删除选定的合同条款，支持批量删除
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDelete_Click(object sender, EventArgs e)
        {
            string root_directory = "合同模板/合同条款/";
            LinkedList<string> fileDelete = new LinkedList<string>();
            LinkedList<string> failDeleteFileList = new LinkedList<string>();
            foreach (DataGridViewRow row in this.dgvDisplay.Rows)
            {
                string isSelected = row.Cells[0].EditedFormattedValue.ToString();
                if (isSelected.Equals("True"))
                {
                    string termName = DataGridViewCellTool.getDataGridViewCellValueString(row.Cells[1]);
                    string termClass = DataGridViewCellTool.getDataGridViewCellValueString(row.Cells[2]);
                    string remoteFile = root_directory + termClass + "/" + termName + ".docx";
                    if (!ftpClient.checkFileExist(remoteFile))
                    {
                        MessageUtil.ShowError("服务器上不存在文件： " + remoteFile);
                        continue;
                    }
                    fileDelete.AddLast(remoteFile);
                    //删除数据库中的数据
                    contractTemplateTermSampleDALTool.deleteContractTemplateTermSampleByPK(termName,termClass);
                }
            }
            if (fileDelete.Count == 0)
            {
                MessageUtil.ShowError("请至少选择一个要删除的文件！");
                return;
            }
            if (!ftpClient.serverIsConnect())
            {
                MessageUtil.ShowError("无法连接服务器！");
                return;
            }
            failDeleteFileList = ftpClient.deleteFileList(fileDelete);
        

            if (failDeleteFileList.Count == 0)
            {
               
                MessageBox.Show("删除成功！");
            }
            else
            {
                MessageUtil.ShowError(failDeleteFileList.Count + "个文件未删除！");
            }
            this.btnSearch.PerformClick();
        }

        /// <summary>
        /// 下载合同条款，支持批量下载
        /// 合同条款在服务器上的存放位置为：ftpserver:root_directory/合同模板/合同条款/%合同条款类别%/%合同条款名称%.docx
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnEdit_Click(object sender, EventArgs e)
        {
            string root_directory = "合同模板/合同条款/";
            Dictionary<string, string> fileDownload = new Dictionary<string, string>();
            Dictionary<string, string> remoteFileSafeName = new Dictionary<string, string>();
            foreach(DataGridViewRow row in this.dgvDisplay.Rows)
            {
                string isSelected = row.Cells[0].EditedFormattedValue.ToString();
                if(isSelected.Equals("True"))
                {
                    string termName = DataGridViewCellTool.getDataGridViewCellValueString(row.Cells[1]);
                    string termClass = DataGridViewCellTool.getDataGridViewCellValueString(row.Cells[2]);
                    if (!ftpClient.checkFileExist(root_directory + termClass + "/" + termName + ".docx"))
                    {
                        MessageUtil.ShowError("服务器上不存在文件： " + root_directory + termClass + "/" + termName + ".docx");
                        continue;
                    }
                    remoteFileSafeName.Add(root_directory + termClass + "/" + termName + ".docx",termName + ".docx");
                }
            }
            if (remoteFileSafeName.Count == 0)
            {
                MessageUtil.ShowError("请至少选择一个下载文件！");
                return;
            }
            if (!ftpClient.serverIsConnect())
            {
                MessageUtil.ShowError("无法连接服务器！");
                return;
            }

            //选择下载文件存放的文件夹
            FolderBrowserDialog downloadFolder = new FolderBrowserDialog();
            downloadFolder.Description = "请选择存放位置";
            downloadFolder.ShowNewFolderButton = false;

            DialogResult result = downloadFolder.ShowDialog();
            if (result == DialogResult.OK)
            {
                string selectedPath = downloadFolder.SelectedPath + "/";
                foreach (KeyValuePair<string,string> kvPair in remoteFileSafeName)
                {
                    string remoteFile = kvPair.Key;
                    string fileSafeName = kvPair.Value;
                    string localFile = selectedPath + fileSafeName;
                    fileDownload.Add(remoteFile,localFile);
                }
                LinkedList<string> failDownloadList = ftpClient.download(fileDownload);
                if (failDownloadList.Count == 0)
                {
                    MessageBox.Show("下载成功！");
                }
                else
                {
                    MessageUtil.ShowError(failDownloadList.Count + "个文件未下载成功！");
                }
            }

            this.btnSearch.PerformClick();
        }
    }
}
