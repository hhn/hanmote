﻿namespace MMClient.ContractManage.ContractTemplateManage
{
    partial class AddNewContractTemplateSectionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSave = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.rtbSectionAnnotation = new System.Windows.Forms.RichTextBox();
            this.tbSectionDetail = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tbSectionName = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(104, 258);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 13;
            this.btnSave.Text = "保存";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(28, 157);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 12);
            this.label3.TabIndex = 12;
            this.label3.Text = "自定义注释：";
            // 
            // rtbSectionAnnotation
            // 
            this.rtbSectionAnnotation.Location = new System.Drawing.Point(138, 121);
            this.rtbSectionAnnotation.Name = "rtbSectionAnnotation";
            this.rtbSectionAnnotation.Size = new System.Drawing.Size(174, 96);
            this.rtbSectionAnnotation.TabIndex = 11;
            this.rtbSectionAnnotation.Text = "";
            // 
            // tbSectionDetail
            // 
            this.tbSectionDetail.Location = new System.Drawing.Point(138, 68);
            this.tbSectionDetail.Name = "tbSectionDetail";
            this.tbSectionDetail.Size = new System.Drawing.Size(174, 21);
            this.tbSectionDetail.TabIndex = 10;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(28, 71);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(113, 12);
            this.label2.TabIndex = 9;
            this.label2.Text = "合同模板章节说明：";
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(28, 37);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(113, 12);
            this.label1.TabIndex = 7;
            this.label1.Text = "合同模板章节名称：";
            // 
            // tbSectionName
            // 
            this.tbSectionName.Location = new System.Drawing.Point(138, 34);
            this.tbSectionName.Name = "tbSectionName";
            this.tbSectionName.Size = new System.Drawing.Size(174, 21);
            this.tbSectionName.TabIndex = 14;
            // 
            // AddNewContractTemplateSectionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(347, 315);
            this.Controls.Add(this.tbSectionName);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.rtbSectionAnnotation);
            this.Controls.Add(this.tbSectionDetail);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "AddNewContractTemplateSectionForm";
            this.Text = "合同章节编辑";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.RichTextBox rtbSectionAnnotation;
        private System.Windows.Forms.TextBox tbSectionDetail;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbSectionName;

    }
}