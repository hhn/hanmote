﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;
using Lib.Model.ContractManage.OutlineAgreement;
using Lib.Bll.ContractManageBLL.OutlineAgreementBLL;
using Lib.Common.CommonControls;
using Lib.Common.CommonUtils;

namespace MMClient.ContractManage.OutlineAgreement
{
    //维护交货计划的DateTimePicker控件使用仍然有问题
    public partial class DeliverySchedulingItemsForm : DockContent
    {
        //计划协议实例
        Scheduling_Agreement schedulingAgreement;
        //具体的框架协议项
        Scheduling_Agreement_Item schedulingAgreementItem;
        //交货计划
        List<Delivery_Item> deliveryItemList;
        //全局变量
        DataGridViewCell currentCell;
        //业务逻辑处理工具
        SchedulingAgreementBLL schedulingAgreementTool = new SchedulingAgreementBLL();
        //操作类型
        string opType;

        public DeliverySchedulingItemsForm(Scheduling_Agreement _schedulingAgreement,
            Scheduling_Agreement_Item _schedulingAgreementItem, List<Delivery_Item> _deliveryItemList,
            string _opType)
        {
            InitializeComponent();
            this.schedulingAgreement = _schedulingAgreement;
            this.schedulingAgreementItem = _schedulingAgreementItem;
            this.deliveryItemList = _deliveryItemList;
            this.opType = _opType;

            initialForm();
        }

        /// <summary>
        /// 根据传入的Scheduling_Agreement初始化界面
        /// </summary>
        /// <param name="cur_Scheduling_Agreement"></param>
        private void initialForm()
        {
            //显示抬头数据
            this.tbSchedulingAgreementID.Text = schedulingAgreement.Scheduling_Agreement_ID;
            this.tbTargetAmount.Text = Convert.ToString(schedulingAgreementItem.Target_Quantity);
            this.lblOUn.Text = schedulingAgreementItem.OUn;
            this.tbMaterialID.Text = schedulingAgreementItem.Material_ID;
            //this.lblMaterialName.Text = schedulingAgreementItem.

            //插入"交货日"和"统计交货日"两个DateTimePickerColumn
            CalendarColumn calColumn1 = new CalendarColumn();
            calColumn1.Name = "Delivery_Time";
            calColumn1.HeaderText = "交货日";
            calColumn1.Width = 135;
            this.dgvDeliveryScheduling.Columns.Insert(1, calColumn1);
            CalendarColumn calColumn2 = new CalendarColumn();
            calColumn2.Name = "Record_Delivery_Time";
            calColumn2.HeaderText = "统计交货日";
            calColumn2.Width = 135;
            this.dgvDeliveryScheduling.Columns.Insert(3, calColumn2);

            //显示交货计划
            deliveryItemList = schedulingAgreementTool.getDeliveryItemList(
                schedulingAgreement.Scheduling_Agreement_ID, schedulingAgreementItem.Item_Num);
            fillDeliveryItem(deliveryItemList);

            //判断操作类型
            if (opType.Equals("display")) {
                this.dgvDeliveryScheduling.ReadOnly = true;
                this.btnAdd.Enabled = false;
                this.btnDelete.Enabled = false;
                this.btnSave.Enabled = false;
            }
        }

        /// <summary>
        /// 显示交货计划
        /// </summary>
        /// <param name="deliveryItemList"></param>
        private void fillDeliveryItem(List<Delivery_Item> items)
        {
            foreach (Delivery_Item item in items) {
                this.dgvDeliveryScheduling.Rows.Add();
                int lastRowIndex = this.dgvDeliveryScheduling.Rows.Count-1;
                DataGridViewRow currentRow = this.dgvDeliveryScheduling.Rows[lastRowIndex];
                currentRow.Cells[0].Value = item.Item_Num;
                currentRow.Cells[1].Value = item.Delivery_Time;
                currentRow.Cells[2].Value = item.Delivery_Amount;
                currentRow.Cells[3].Value = item.Record_Delivery_Time;
                currentRow.Cells[4].Value = item.Accumulate_Delivery_Amount;
                currentRow.Cells[5].Value = item.LastTime_Accumulate_Amount;
                currentRow.Cells[6].Value = item.Not_Delivery_Amount;
                currentRow.Cells[7].Value = item.Status;

                //设置已收货部分不可编辑
                if (item.Status.Equals("已收货")) {
                    currentRow.ReadOnly = true;
                }
            }
        }

        /// <summary>
        /// 点击保存
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSave_Click(object sender, EventArgs e)
        {
            //检测是否有交货计划
            if (this.dgvDeliveryScheduling.Rows.Count <= 0) {
                MessageUtil.ShowError("请添加交货计划");
                return;
            }
            //读取交货计划行
            double sum = 0.0;
            List<Delivery_Item> newDeliveryItemList = new List<Delivery_Item>();
            try
            {
                foreach (DataGridViewRow row in this.dgvDeliveryScheduling.Rows)
                {
                    Delivery_Item item = new Delivery_Item();
                    item.Scheduling_Agreement_ID = schedulingAgreement.Scheduling_Agreement_ID;
                    item.Material_Item_Num = schedulingAgreementItem.Item_Num;
                    item.Item_Num = Convert.ToInt32(row.Cells[0].Value.ToString());
                    item.Delivery_Time = Convert.ToDateTime(row.Cells[1].Value.ToString());
                    item.Delivery_Amount = Convert.ToDouble(row.Cells[2].Value.ToString());
                    item.Record_Delivery_Time = Convert.ToDateTime(row.Cells[3].Value.ToString());
                    item.Accumulate_Delivery_Amount = Convert.ToDouble(row.Cells[4].Value.ToString());
                    item.LastTime_Accumulate_Amount = Convert.ToDouble(row.Cells[5].Value.ToString());
                    item.Not_Delivery_Amount = Convert.ToDouble(row.Cells[6].Value.ToString());
                    item.Status = row.Cells[7].Value.ToString();

                    newDeliveryItemList.Add(item);
                    sum += item.Delivery_Amount;
                }
            }
            catch (Exception) {
                MessageUtil.ShowError("请正确填写");
                return;
            }
            //判断是否合理
            if (!MathUtil.equalsZero(sum - schedulingAgreementItem.Target_Quantity)) {
                MessageUtil.ShowError("计划收货数量与计划协议总数量不符");
                return;
            }
            //保存
            int num = schedulingAgreementTool.updateDeliveryItemList(schedulingAgreement.Scheduling_Agreement_ID,
                schedulingAgreementItem.Item_Num, newDeliveryItemList);
            if (num > 0)
                MessageBox.Show("保存成功");
            else
                MessageBox.Show("保存失败");
        }

        /// <summary>
        /// 增加交货计划
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAdd_Click(object sender, EventArgs e)
        {
            this.dgvDeliveryScheduling.Rows.Add();

            //项目号
            int rowCount = this.dgvDeliveryScheduling.Rows.Count;
            if (rowCount == 1)
            {
                this.dgvDeliveryScheduling.Rows[0].Cells[0].Value = 1;
            }
            else
            {
                this.dgvDeliveryScheduling.Rows[rowCount - 1].Cells[0].Value =
                    Convert.ToInt32(this.dgvDeliveryScheduling.Rows[rowCount - 2].Cells[0].Value) + 1;
            }
            //状态
            this.dgvDeliveryScheduling.Rows[rowCount - 1].Cells[7].Value = "待收货";
        }

        /// <summary>
        /// 点击删除
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (this.dgvDeliveryScheduling.Rows.Count == 0)
            {
                MessageBox.Show("没有可删除内容", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            int rowIndex = this.dgvDeliveryScheduling.CurrentCell.RowIndex;
            if (rowIndex >= 0 && rowIndex < this.dgvDeliveryScheduling.Rows.Count)
            {
                string status = this.dgvDeliveryScheduling.Rows[rowIndex].Cells[7].Value.ToString();
                if (status.Equals("已收货")) {
                    MessageUtil.ShowError("不能删除已收货行");
                    return;
                }
                this.dgvDeliveryScheduling.Rows.RemoveAt(rowIndex);
                updateRowNum();
            }
        }

        /// <summary>
        /// 刷新行的序号
        /// </summary>
        private void updateRowNum()
        {
            int i = 1;
            foreach (DataGridViewRow row in this.dgvDeliveryScheduling.Rows)
            {
                row.Cells[0].Value = i;
                i++;
            }
        }

        private void DeliverySchedulingItemsForm_Load(object sender, EventArgs e)
        {

        }

        private void dgvDeliveryScheduling_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
