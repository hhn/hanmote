﻿namespace MMClient.ContractManage.OutlineAgreement
{
    partial class NewContractManageForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbBasicData = new System.Windows.Forms.GroupBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cbxCurrencyType = new System.Windows.Forms.ComboBox();
            this.lbCurrencyType = new System.Windows.Forms.Label();
            this.cbxStorageLocation = new System.Windows.Forms.ComboBox();
            this.lbStorageLocation = new System.Windows.Forms.Label();
            this.cbxPurchaseOrg = new System.Windows.Forms.ComboBox();
            this.lbPurchaseOrg = new System.Windows.Forms.Label();
            this.cbxPurchaseGroup = new System.Windows.Forms.ComboBox();
            this.lbPurchaseGroup = new System.Windows.Forms.Label();
            this.cbxTargetNum = new System.Windows.Forms.ComboBox();
            this.lbTargetNum = new System.Windows.Forms.Label();
            this.cbxTime = new System.Windows.Forms.ComboBox();
            this.lbTime = new System.Windows.Forms.Label();
            this.cbxEndTime = new System.Windows.Forms.ComboBox();
            this.lbEndTime = new System.Windows.Forms.Label();
            this.cbxBeginTime = new System.Windows.Forms.ComboBox();
            this.lbBeginTime = new System.Windows.Forms.Label();
            this.cbxContractType = new System.Windows.Forms.ComboBox();
            this.lbContractType = new System.Windows.Forms.Label();
            this.cbxFactoryId = new System.Windows.Forms.ComboBox();
            this.lbFactoryId = new System.Windows.Forms.Label();
            this.lbSupplierId = new System.Windows.Forms.Label();
            this.cbxSupplierId = new System.Windows.Forms.ComboBox();
            this.cbxContractId = new System.Windows.Forms.ComboBox();
            this.lbContractId = new System.Windows.Forms.Label();
            this.gbProjectData = new System.Windows.Forms.GroupBox();
            this.tab = new System.Windows.Forms.TabControl();
            this.tbMaterialNum = new System.Windows.Forms.TabPage();
            this.tbMaterialSection = new System.Windows.Forms.TabPage();
            this.tbConditionCode = new System.Windows.Forms.TabPage();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.lbMaterialData = new System.Windows.Forms.Label();
            this.dgvNumCondition = new System.Windows.Forms.DataGridView();
            this.dgvTimeCondition = new System.Windows.Forms.DataGridView();
            this.dgvCondition = new System.Windows.Forms.DataGridView();
            this.gbBasicData.SuspendLayout();
            this.gbProjectData.SuspendLayout();
            this.tab.SuspendLayout();
            this.tbMaterialNum.SuspendLayout();
            this.tbMaterialSection.SuspendLayout();
            this.tbConditionCode.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvNumCondition)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTimeCondition)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCondition)).BeginInit();
            this.SuspendLayout();
            // 
            // gbBasicData
            // 
            this.gbBasicData.Controls.Add(this.groupBox1);
            this.gbBasicData.Controls.Add(this.cbxCurrencyType);
            this.gbBasicData.Controls.Add(this.lbCurrencyType);
            this.gbBasicData.Controls.Add(this.cbxStorageLocation);
            this.gbBasicData.Controls.Add(this.lbStorageLocation);
            this.gbBasicData.Controls.Add(this.cbxPurchaseOrg);
            this.gbBasicData.Controls.Add(this.lbPurchaseOrg);
            this.gbBasicData.Controls.Add(this.cbxPurchaseGroup);
            this.gbBasicData.Controls.Add(this.lbPurchaseGroup);
            this.gbBasicData.Controls.Add(this.cbxTargetNum);
            this.gbBasicData.Controls.Add(this.lbTargetNum);
            this.gbBasicData.Controls.Add(this.cbxTime);
            this.gbBasicData.Controls.Add(this.lbTime);
            this.gbBasicData.Controls.Add(this.cbxEndTime);
            this.gbBasicData.Controls.Add(this.lbEndTime);
            this.gbBasicData.Controls.Add(this.cbxBeginTime);
            this.gbBasicData.Controls.Add(this.lbBeginTime);
            this.gbBasicData.Controls.Add(this.cbxContractType);
            this.gbBasicData.Controls.Add(this.lbContractType);
            this.gbBasicData.Controls.Add(this.cbxFactoryId);
            this.gbBasicData.Controls.Add(this.lbFactoryId);
            this.gbBasicData.Controls.Add(this.lbSupplierId);
            this.gbBasicData.Controls.Add(this.cbxSupplierId);
            this.gbBasicData.Controls.Add(this.cbxContractId);
            this.gbBasicData.Controls.Add(this.lbContractId);
            this.gbBasicData.Location = new System.Drawing.Point(0, 0);
            this.gbBasicData.Name = "gbBasicData";
            this.gbBasicData.Size = new System.Drawing.Size(1134, 216);
            this.gbBasicData.TabIndex = 0;
            this.gbBasicData.TabStop = false;
            this.gbBasicData.Text = "基本数据项";
            // 
            // groupBox1
            // 
            this.groupBox1.Location = new System.Drawing.Point(0, 213);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(200, 100);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "groupBox1";
            // 
            // cbxCurrencyType
            // 
            this.cbxCurrencyType.FormattingEnabled = true;
            this.cbxCurrencyType.Location = new System.Drawing.Point(966, 150);
            this.cbxCurrencyType.Name = "cbxCurrencyType";
            this.cbxCurrencyType.Size = new System.Drawing.Size(121, 20);
            this.cbxCurrencyType.TabIndex = 24;
            // 
            // lbCurrencyType
            // 
            this.lbCurrencyType.AutoSize = true;
            this.lbCurrencyType.Location = new System.Drawing.Point(855, 158);
            this.lbCurrencyType.Name = "lbCurrencyType";
            this.lbCurrencyType.Size = new System.Drawing.Size(29, 12);
            this.lbCurrencyType.TabIndex = 23;
            this.lbCurrencyType.Text = "货币";
            // 
            // cbxStorageLocation
            // 
            this.cbxStorageLocation.FormattingEnabled = true;
            this.cbxStorageLocation.Location = new System.Drawing.Point(680, 150);
            this.cbxStorageLocation.Name = "cbxStorageLocation";
            this.cbxStorageLocation.Size = new System.Drawing.Size(121, 20);
            this.cbxStorageLocation.TabIndex = 22;
            // 
            // lbStorageLocation
            // 
            this.lbStorageLocation.AutoSize = true;
            this.lbStorageLocation.Location = new System.Drawing.Point(589, 158);
            this.lbStorageLocation.Name = "lbStorageLocation";
            this.lbStorageLocation.Size = new System.Drawing.Size(41, 12);
            this.lbStorageLocation.TabIndex = 21;
            this.lbStorageLocation.Text = "库存地";
            // 
            // cbxPurchaseOrg
            // 
            this.cbxPurchaseOrg.FormattingEnabled = true;
            this.cbxPurchaseOrg.Location = new System.Drawing.Point(389, 150);
            this.cbxPurchaseOrg.Name = "cbxPurchaseOrg";
            this.cbxPurchaseOrg.Size = new System.Drawing.Size(121, 20);
            this.cbxPurchaseOrg.TabIndex = 20;
            // 
            // lbPurchaseOrg
            // 
            this.lbPurchaseOrg.AutoSize = true;
            this.lbPurchaseOrg.Location = new System.Drawing.Point(295, 158);
            this.lbPurchaseOrg.Name = "lbPurchaseOrg";
            this.lbPurchaseOrg.Size = new System.Drawing.Size(65, 12);
            this.lbPurchaseOrg.TabIndex = 19;
            this.lbPurchaseOrg.Text = "采购组织：";
            // 
            // cbxPurchaseGroup
            // 
            this.cbxPurchaseGroup.FormattingEnabled = true;
            this.cbxPurchaseGroup.Location = new System.Drawing.Point(98, 150);
            this.cbxPurchaseGroup.Name = "cbxPurchaseGroup";
            this.cbxPurchaseGroup.Size = new System.Drawing.Size(121, 20);
            this.cbxPurchaseGroup.TabIndex = 18;
            // 
            // lbPurchaseGroup
            // 
            this.lbPurchaseGroup.AutoSize = true;
            this.lbPurchaseGroup.Location = new System.Drawing.Point(12, 158);
            this.lbPurchaseGroup.Name = "lbPurchaseGroup";
            this.lbPurchaseGroup.Size = new System.Drawing.Size(53, 12);
            this.lbPurchaseGroup.TabIndex = 17;
            this.lbPurchaseGroup.Text = "采购组：";
            // 
            // cbxTargetNum
            // 
            this.cbxTargetNum.FormattingEnabled = true;
            this.cbxTargetNum.Location = new System.Drawing.Point(966, 91);
            this.cbxTargetNum.Name = "cbxTargetNum";
            this.cbxTargetNum.Size = new System.Drawing.Size(121, 20);
            this.cbxTargetNum.TabIndex = 16;
            // 
            // lbTargetNum
            // 
            this.lbTargetNum.AutoSize = true;
            this.lbTargetNum.Location = new System.Drawing.Point(855, 94);
            this.lbTargetNum.Name = "lbTargetNum";
            this.lbTargetNum.Size = new System.Drawing.Size(53, 12);
            this.lbTargetNum.TabIndex = 15;
            this.lbTargetNum.Text = "目标值：";
            // 
            // cbxTime
            // 
            this.cbxTime.FormattingEnabled = true;
            this.cbxTime.Location = new System.Drawing.Point(680, 86);
            this.cbxTime.Name = "cbxTime";
            this.cbxTime.Size = new System.Drawing.Size(121, 20);
            this.cbxTime.TabIndex = 14;
            // 
            // lbTime
            // 
            this.lbTime.AutoSize = true;
            this.lbTime.Location = new System.Drawing.Point(589, 94);
            this.lbTime.Name = "lbTime";
            this.lbTime.Size = new System.Drawing.Size(89, 12);
            this.lbTime.TabIndex = 13;
            this.lbTime.Text = "协议签署时间：";
            // 
            // cbxEndTime
            // 
            this.cbxEndTime.FormattingEnabled = true;
            this.cbxEndTime.Location = new System.Drawing.Point(389, 86);
            this.cbxEndTime.Name = "cbxEndTime";
            this.cbxEndTime.Size = new System.Drawing.Size(121, 20);
            this.cbxEndTime.TabIndex = 12;
            // 
            // lbEndTime
            // 
            this.lbEndTime.AutoSize = true;
            this.lbEndTime.Location = new System.Drawing.Point(295, 94);
            this.lbEndTime.Name = "lbEndTime";
            this.lbEndTime.Size = new System.Drawing.Size(89, 12);
            this.lbEndTime.TabIndex = 11;
            this.lbEndTime.Text = "协议截止时间：";
            // 
            // cbxBeginTime
            // 
            this.cbxBeginTime.FormattingEnabled = true;
            this.cbxBeginTime.Location = new System.Drawing.Point(98, 86);
            this.cbxBeginTime.Name = "cbxBeginTime";
            this.cbxBeginTime.Size = new System.Drawing.Size(121, 20);
            this.cbxBeginTime.TabIndex = 10;
            // 
            // lbBeginTime
            // 
            this.lbBeginTime.AutoSize = true;
            this.lbBeginTime.Location = new System.Drawing.Point(12, 94);
            this.lbBeginTime.Name = "lbBeginTime";
            this.lbBeginTime.Size = new System.Drawing.Size(89, 12);
            this.lbBeginTime.TabIndex = 9;
            this.lbBeginTime.Text = "协议开始时间：";
            // 
            // cbxContractType
            // 
            this.cbxContractType.FormattingEnabled = true;
            this.cbxContractType.Location = new System.Drawing.Point(966, 33);
            this.cbxContractType.Name = "cbxContractType";
            this.cbxContractType.Size = new System.Drawing.Size(121, 20);
            this.cbxContractType.TabIndex = 8;
            // 
            // lbContractType
            // 
            this.lbContractType.AutoSize = true;
            this.lbContractType.Location = new System.Drawing.Point(855, 41);
            this.lbContractType.Name = "lbContractType";
            this.lbContractType.Size = new System.Drawing.Size(65, 12);
            this.lbContractType.TabIndex = 7;
            this.lbContractType.Text = "协议类型：";
            // 
            // cbxFactoryId
            // 
            this.cbxFactoryId.FormattingEnabled = true;
            this.cbxFactoryId.Location = new System.Drawing.Point(680, 33);
            this.cbxFactoryId.Name = "cbxFactoryId";
            this.cbxFactoryId.Size = new System.Drawing.Size(121, 20);
            this.cbxFactoryId.TabIndex = 6;
            // 
            // lbFactoryId
            // 
            this.lbFactoryId.AutoSize = true;
            this.lbFactoryId.Location = new System.Drawing.Point(589, 41);
            this.lbFactoryId.Name = "lbFactoryId";
            this.lbFactoryId.Size = new System.Drawing.Size(65, 12);
            this.lbFactoryId.TabIndex = 4;
            this.lbFactoryId.Text = "工厂编号：";
            // 
            // lbSupplierId
            // 
            this.lbSupplierId.AutoSize = true;
            this.lbSupplierId.Location = new System.Drawing.Point(295, 36);
            this.lbSupplierId.Name = "lbSupplierId";
            this.lbSupplierId.Size = new System.Drawing.Size(77, 12);
            this.lbSupplierId.TabIndex = 3;
            this.lbSupplierId.Text = "供应商编号：";
            // 
            // cbxSupplierId
            // 
            this.cbxSupplierId.FormattingEnabled = true;
            this.cbxSupplierId.Location = new System.Drawing.Point(389, 33);
            this.cbxSupplierId.Name = "cbxSupplierId";
            this.cbxSupplierId.Size = new System.Drawing.Size(121, 20);
            this.cbxSupplierId.TabIndex = 2;
            // 
            // cbxContractId
            // 
            this.cbxContractId.FormattingEnabled = true;
            this.cbxContractId.Location = new System.Drawing.Point(98, 33);
            this.cbxContractId.Name = "cbxContractId";
            this.cbxContractId.Size = new System.Drawing.Size(121, 20);
            this.cbxContractId.TabIndex = 1;
            // 
            // lbContractId
            // 
            this.lbContractId.AutoSize = true;
            this.lbContractId.Location = new System.Drawing.Point(12, 33);
            this.lbContractId.Name = "lbContractId";
            this.lbContractId.Size = new System.Drawing.Size(65, 12);
            this.lbContractId.TabIndex = 0;
            this.lbContractId.Text = "协议编号：";
            this.lbContractId.Click += new System.EventHandler(this.lbContractId_Click);
            // 
            // gbProjectData
            // 
            this.gbProjectData.Controls.Add(this.tab);
            this.gbProjectData.Controls.Add(this.dataGridView1);
            this.gbProjectData.Controls.Add(this.lbMaterialData);
            this.gbProjectData.Location = new System.Drawing.Point(6, 213);
            this.gbProjectData.Name = "gbProjectData";
            this.gbProjectData.Size = new System.Drawing.Size(1134, 564);
            this.gbProjectData.TabIndex = 1;
            this.gbProjectData.TabStop = false;
            this.gbProjectData.Text = "项目数据";
            // 
            // tab
            // 
            this.tab.Controls.Add(this.tbMaterialNum);
            this.tab.Controls.Add(this.tbMaterialSection);
            this.tab.Controls.Add(this.tbConditionCode);
            this.tab.Location = new System.Drawing.Point(510, 59);
            this.tab.Name = "tab";
            this.tab.SelectedIndex = 0;
            this.tab.Size = new System.Drawing.Size(571, 457);
            this.tab.TabIndex = 2;
            this.tab.Tag = "";
            // 
            // tbMaterialNum
            // 
            this.tbMaterialNum.Controls.Add(this.dgvNumCondition);
            this.tbMaterialNum.Location = new System.Drawing.Point(4, 22);
            this.tbMaterialNum.Name = "tbMaterialNum";
            this.tbMaterialNum.Padding = new System.Windows.Forms.Padding(3);
            this.tbMaterialNum.Size = new System.Drawing.Size(563, 431);
            this.tbMaterialNum.TabIndex = 0;
            this.tbMaterialNum.Text = "数量区间定价";
            this.tbMaterialNum.UseVisualStyleBackColor = true;
            // 
            // tbMaterialSection
            // 
            this.tbMaterialSection.Controls.Add(this.dgvTimeCondition);
            this.tbMaterialSection.Location = new System.Drawing.Point(4, 22);
            this.tbMaterialSection.Name = "tbMaterialSection";
            this.tbMaterialSection.Padding = new System.Windows.Forms.Padding(3);
            this.tbMaterialSection.Size = new System.Drawing.Size(563, 431);
            this.tbMaterialSection.TabIndex = 1;
            this.tbMaterialSection.Text = "时间区间定价";
            this.tbMaterialSection.UseVisualStyleBackColor = true;
            // 
            // tbConditionCode
            // 
            this.tbConditionCode.Controls.Add(this.dgvCondition);
            this.tbConditionCode.Location = new System.Drawing.Point(4, 22);
            this.tbConditionCode.Name = "tbConditionCode";
            this.tbConditionCode.Padding = new System.Windows.Forms.Padding(3);
            this.tbConditionCode.Size = new System.Drawing.Size(563, 431);
            this.tbConditionCode.TabIndex = 2;
            this.tbConditionCode.Text = "条件代码";
            this.tbConditionCode.UseVisualStyleBackColor = true;
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(8, 81);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.Size = new System.Drawing.Size(496, 435);
            this.dataGridView1.TabIndex = 1;
            // 
            // lbMaterialData
            // 
            this.lbMaterialData.AutoSize = true;
            this.lbMaterialData.Location = new System.Drawing.Point(72, 59);
            this.lbMaterialData.Name = "lbMaterialData";
            this.lbMaterialData.Size = new System.Drawing.Size(53, 12);
            this.lbMaterialData.TabIndex = 0;
            this.lbMaterialData.Text = "物料数据";
            // 
            // dgvNumCondition
            // 
            this.dgvNumCondition.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvNumCondition.Location = new System.Drawing.Point(0, 0);
            this.dgvNumCondition.Name = "dgvNumCondition";
            this.dgvNumCondition.RowTemplate.Height = 23;
            this.dgvNumCondition.Size = new System.Drawing.Size(563, 431);
            this.dgvNumCondition.TabIndex = 0;
            // 
            // dgvTimeCondition
            // 
            this.dgvTimeCondition.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTimeCondition.Location = new System.Drawing.Point(0, 0);
            this.dgvTimeCondition.Name = "dgvTimeCondition";
            this.dgvTimeCondition.RowTemplate.Height = 23;
            this.dgvTimeCondition.Size = new System.Drawing.Size(563, 431);
            this.dgvTimeCondition.TabIndex = 0;
            // 
            // dgvCondition
            // 
            this.dgvCondition.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCondition.Location = new System.Drawing.Point(0, 0);
            this.dgvCondition.Name = "dgvCondition";
            this.dgvCondition.RowTemplate.Height = 23;
            this.dgvCondition.Size = new System.Drawing.Size(563, 431);
            this.dgvCondition.TabIndex = 0;
            // 
            // NewContractManageForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1118, 741);
            this.Controls.Add(this.gbProjectData);
            this.Controls.Add(this.gbBasicData);
            this.Name = "NewContractManageForm";
            this.Text = "NewContractManageForm";
            this.gbBasicData.ResumeLayout(false);
            this.gbBasicData.PerformLayout();
            this.gbProjectData.ResumeLayout(false);
            this.gbProjectData.PerformLayout();
            this.tab.ResumeLayout(false);
            this.tbMaterialNum.ResumeLayout(false);
            this.tbMaterialSection.ResumeLayout(false);
            this.tbConditionCode.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvNumCondition)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTimeCondition)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCondition)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbBasicData;
        private System.Windows.Forms.Label lbContractId;
        private System.Windows.Forms.ComboBox cbxSupplierId;
        private System.Windows.Forms.ComboBox cbxContractId;
        private System.Windows.Forms.Label lbSupplierId;
        private System.Windows.Forms.ComboBox cbxFactoryId;
        private System.Windows.Forms.Label lbFactoryId;
        private System.Windows.Forms.ComboBox cbxContractType;
        private System.Windows.Forms.Label lbContractType;
        private System.Windows.Forms.ComboBox cbxEndTime;
        private System.Windows.Forms.Label lbEndTime;
        private System.Windows.Forms.ComboBox cbxBeginTime;
        private System.Windows.Forms.Label lbBeginTime;
        private System.Windows.Forms.ComboBox cbxTargetNum;
        private System.Windows.Forms.Label lbTargetNum;
        private System.Windows.Forms.ComboBox cbxTime;
        private System.Windows.Forms.Label lbTime;
        private System.Windows.Forms.Label lbStorageLocation;
        private System.Windows.Forms.ComboBox cbxPurchaseOrg;
        private System.Windows.Forms.Label lbPurchaseOrg;
        private System.Windows.Forms.ComboBox cbxPurchaseGroup;
        private System.Windows.Forms.Label lbPurchaseGroup;
        private System.Windows.Forms.Label lbCurrencyType;
        private System.Windows.Forms.ComboBox cbxStorageLocation;
        private System.Windows.Forms.ComboBox cbxCurrencyType;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox gbProjectData;
        private System.Windows.Forms.TabControl tab;
        private System.Windows.Forms.TabPage tbMaterialNum;
        private System.Windows.Forms.TabPage tbMaterialSection;
        private System.Windows.Forms.TabPage tbConditionCode;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label lbMaterialData;
        private System.Windows.Forms.DataGridView dgvNumCondition;
        private System.Windows.Forms.DataGridView dgvTimeCondition;
        private System.Windows.Forms.DataGridView dgvCondition;
    }
}