﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Bll.MDBll.General;
using Lib.Bll.ContractManageBLL.OutlineAgreementBLL;
using Lib.Model.MD;
using Lib.Model.ContractManage.OutlineAgreement;
using Lib.Common.CommonUtils;
using WeifenLuo.WinFormsUI.Docking;
using Lib.Model.SourcingManage.SourceingRecord;

namespace MMClient.ContractManage.OutlineAgreement
{
    public partial class SForm : DockContent
    {
        #region 属性定义
        //框架协议工具
        private OutlineAgreementTool tool = new OutlineAgreementTool();
        //是否初始化完成
        private bool isInitialized = false;
        //主数据读取工具
        private GeneralBLL generalTool = new GeneralBLL();
        private AgreementContractBLL agreementContractTool = new AgreementContractBLL();
        //操作类型：生成/查看
        private string opType = "";
        //创建者编号，先写死
        private string createrId = "TT2017XX";
        private string agreementId;
        private Agreement_Contract agreementContract = new Agreement_Contract();
        private List<Agreement_Contract_Item> itemList = new List<Agreement_Contract_Item>();
        private List<RecordInfo> infoList = new List<RecordInfo>();
        
        #endregion

        #region 构造方法
        public SForm()
        {
            InitializeComponent();
            this.opType = "new";
            this.agreementId = "KJXY" + createrId + DateTime.Now.ToString("yyyyMMddHHmmssfff");
            this.cbxAgreementType.SelectedIndex = 0;
            bindDataForCreate();
            writeDataForCreate();
        }

        public SForm(Agreement_Contract contract,string _opType)
        {
            InitializeComponent();
            this.opType = _opType;
            this.agreementContract = contract;
            if (!opType.Equals("new"))
            {
                initialDataForOthers(contract);
            }
            else
            {
                this.cbxAgreementType.SelectedIndex = 0;
                this.cbxCurrencyType.SelectedIndex = this.cbxCurrencyType.Items.IndexOf("CNY");
            }
        }
        #endregion

        #region 为新建合同界面绑定数据
        //为所有的下拉列表绑定数据源
        private void bindDataForCreate()
        {
            //绑定寻源编号数据源
            List<string> sourceIdList = tool.getSourceIdFromRecordInfo();
            if (sourceIdList != null)
            {
                this.cbxSourceId.DataSource = sourceIdList;
            }
            else
            {
                MessageUtil.ShowTips("当前没有需要签订的合同！");
                return;
            }

            //绑定供应商编号和工厂编号数据源
            string sourceId = this.cbxSourceId.SelectedValue.ToString();
            List<string> supplierList = tool.getSupplierBySourceId(sourceId);
            if (supplierList != null)
            {
                this.cbxSupplierID.DataSource = supplierList;
            }
            List<string> factoryList = tool.getFactoryBySourceId(sourceId);
            if (factoryList != null)
            {
                this.cbxFactoryId.DataSource = factoryList;
            }

            //绑定付款条件数据源
            List<string> paymentClauseList = generalTool.GetAllPaymentClauseName();
            if (paymentClauseList != null)
            {
                this.cbxPaymentClause.DataSource = paymentClauseList;
            }

            //绑定交付条件数据源
            List<string> tradeClauseList = generalTool.GetAllTradeClauseID();
            if (tradeClauseList != null)
            {
                this.cbxTradeClause.DataSource = tradeClauseList;
            }
            isInitialized = true;
        }

        //填写对应的文本框
        private void writeDataForCreate()
        {
            string sourceId = this.cbxSourceId.SelectedValue.ToString();
            string supplierId = this.cbxSupplierID.SelectedValue.ToString();
            string factoryId = this.cbxFactoryId.SelectedValue.ToString();
            DataTable dt = tool.getAllBasicInformationFromRecordId(sourceId, supplierId, factoryId);
            if (dt != null)
            {
                this.tbPurchaseGroup.Text = getStringFromDB(dt,0,0);
                this.tbPurchaseOrg.Text = getStringFromDB(dt,0,1);
                this.tbStockId.Text = getStringFromDB(dt,0,2);
                int rowNum = dt.Rows.Count;
                if (this.dgvMaterialItem.Rows.Count != 0)
                {
                    this.dgvMaterialItem.Rows.Clear();
                }
                this.dgvMaterialItem.Rows.Add(rowNum);
                for (int i = 0; i < rowNum; i++)
                {
                    DataGridViewRow currentRow = this.dgvMaterialItem.Rows[i];
                    currentRow.Cells[0].Value = i + 1;
                    currentRow.Cells[1].Value = "";
                    currentRow.Cells[2].Value = "";
                    currentRow.Cells[3].Value = getStringFromDB(dt,i,3);
                    currentRow.Cells[4].Value = getStringFromDB(dt,i,4);
                    currentRow.Cells[5].Value = getDoubleFromDB(dt,i,6);
                    currentRow.Cells[6].Value = 1;
                    currentRow.Cells[7].Value = getDoubleFromDB(dt,i,5);
                    currentRow.Cells[8].Value = "PC";
                    currentRow.Cells[9].Value = getStringFromDB(dt,i,7);
                    currentRow.Cells[10].Value = factoryId;
                }
            }
        }
        #endregion

        #region 数据类型转换
        private string getStringFromDB(DataTable dt, int i, int j)
        {
            if (dt == null)
                return "";
            return Convert.IsDBNull(dt.Rows[i][j]) ? "" : dt.Rows[i][j].ToString();
        }

        private double getDoubleFromDB(DataTable dt, int i, int j)
        {
            if (dt == null)
                return 0.00;
            return Convert.IsDBNull(dt.Rows[i][j]) ? 0.00 : Convert.ToDouble(dt.Rows[i][j].ToString());
        }

        private double convertStringToDouble(string s)
        {
            if (s == null || s.Trim().Equals(""))
                return 0.00;
            else
                return Convert.ToDouble(s);
        }

        private int convertStringToInt(string s)
        {
            if (s == null || s.Equals(""))
                return 0;
            else
                return Convert.ToInt32(s);
        }

        private string getDataGridViewCellString(DataGridViewCell cell)
        {
            if (cell.Value == null)
                return "";
            return cell.Value.ToString();
        }

        private int getDataGridViewCellInt32(DataGridViewCell cell)
        {
            if (cell.Value == null)
                return 0;
            return Convert.ToInt32(cell.Value.ToString());
        }

        private double getDataGridViewCellDouble(DataGridViewCell cell)
        {
            if (cell.Value == null)
                return 0.00;
            return Convert.ToDouble(cell.Value.ToString());
        }
        #endregion

        #region 下拉框&按钮响应事件
        private void cbxSourceId_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (isInitialized)
            {
                string sourceId = this.cbxSourceId.SelectedValue.ToString();
                DataTable dt = tool.getBasicInformationBySourceId(sourceId);
                if (dt != null)
                {
                    isInitialized = false;
                    this.cbxSupplierID.DataSource = dt;
                    this.cbxSupplierID.DisplayMember = "SupplierId";
                    this.cbxSupplierID.ValueMember = "SupplierId";
                    this.cbxFactoryId.DataSource = dt;
                    this.cbxFactoryId.DisplayMember = "FactoryId";
                    this.cbxFactoryId.ValueMember = "FactoryId";
                    isInitialized = true;
                }
                writeDataForCreate();
            }
        }

        private void cbxSupplierID_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (isInitialized)
            {
                writeDataForCreate();
            }
        }

        private void cbxFactoryId_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (isInitialized)
            {
                writeDataForCreate();
            }
        }

        private void cbxPaymentClause_SelectedIndexChanged(object sender, EventArgs e)
        {
            string paymentTypeID = this.cbxPaymentClause.Text.Trim();
            if (!paymentTypeID.Equals(""))
            {
                //从主数据表中读取付款条件
                generalTool = new GeneralBLL();
                PaymentClause info = generalTool.GetClause(paymentTypeID);
                if (info != null)
                {
                    this.tbDays1.Text = info.First_Date.ToString();
                    this.tbDiscountRate1.Text = info.First_Discount.ToString();
                    this.tbDays2.Text = info.Second_Date.ToString();
                    this.tbDiscountRate2.Text = info.Second_Discount.ToString();
                    this.tbDays3.Text = info.Final_Date.ToString();
                }
                else
                {
                    MessageUtil.ShowError("付款条件不存在！");
                }
            }
            else
            {
                this.tbDays1.Text = "";
                this.tbDiscountRate1.Text = "";
                this.tbDays2.Text = "";
                this.tbDiscountRate2.Text = "";
                this.tbDays3.Text = "";
            }
        }

        private void cbxTradeClause_SelectedIndexChanged(object sender, EventArgs e)
        {
            string tradClauseID = this.cbxTradeClause.Text.Trim();
            if (!tradClauseID.Equals(""))
            {
                DataTable dt = generalTool.GetTradeClauseByID(tradClauseID);
                if (dt != null)
                {
                    string tradeClauseName = Convert.IsDBNull(dt.Rows[0]["TradeClause_Name"]) ? "" : dt.Rows[0]["TradeClause_Name"].ToString();
                    this.tblTermText.Text = tradeClauseName;
                }
                else
                {
                    MessageUtil.ShowError("交付条件不存在！");
                }
            }
            else
            {
                this.tblTermText.Text = "";
            }
        }


        private void btnSave_Click(object sender, EventArgs e)
        {
            Agreement_Contract oldOne = agreementContract;
            Agreement_Contract newOne = new Agreement_Contract();
            #region 为contract对象赋值
            newOne.Creater_ID = this.createrId;
            if (opType.Equals("new"))
            {
                newOne.Agreement_Contract_ID = this.agreementId;
                newOne.Agreement_Contract_Version = "1";
                newOne.Status = "待审核";
                newOne.IsValid = 1;
                newOne.Finished = 0.00;
               
                Agreement_Contract searchResult = agreementContractTool.getAgreementContractByID(newOne.Agreement_Contract_ID,
newOne.Agreement_Contract_Version);
                if (searchResult != null)
                {
                    MessageBox.Show("该ID合同已存在", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
               
            }
            else
            {
                newOne.Agreement_Contract_ID = oldOne.Agreement_Contract_ID;
                newOne.Agreement_Contract_Version = oldOne.Agreement_Contract_Version;
                newOne.Status = "待审核";
                newOne.IsValid = oldOne.IsValid;
                newOne.Finished = oldOne.Finished;
            }
            newOne.Agreement_Contract_Type = this.cbxAgreementType.Text.Trim();
            newOne.Supplier_ID = this.cbxSupplierID.Text.Trim();
            newOne.Purchase_Group = this.tbPurchaseGroup.Text.Trim();
            newOne.Purchase_Organization = this.tbPurchaseOrg.Text.Trim();
            newOne.Create_Time = this.dtpCreateTime.Value;
            newOne.Creater_ID = this.createrId;
            newOne.Factory = this.cbxFactoryId.Text.Trim();
            newOne.Storage_Location = this.tbStockId.Text.Trim();
            newOne.Begin_Time = this.dtpBeginTime.Value;
            newOne.End_Time = this.dtpEndTime.Value;
            newOne.IT_Term_Code = this.cbxTradeClause.Text.Trim();
            newOne.IT_Term_Text = this.tblTermText.Text.Trim();
            newOne.Payment_Type = this.cbxPaymentClause.Text.Trim();
            newOne.Days1 = convertStringToInt(this.tbDays1.Text.Trim());
            newOne.Days2 = convertStringToInt(this.tbDays2.Text.Trim());
            newOne.Days3 = convertStringToInt(this.tbDays3.Text.Trim());
            newOne.Discount_Rate1 = convertStringToDouble(this.tbDiscountRate1.Text.Trim());
            newOne.Discount_Rate2 = convertStringToDouble(this.tbDiscountRate2.Text.Trim());
            newOne.Target_Value = convertStringToDouble(this.tbTargetValue.Text.Trim());
            newOne.Currency_Type = this.cbxCurrencyType.Text.Trim();
            #endregion
            //保存到框架协议表中
            #region 输入检测(必填项目&格式)
            if(newOne.Agreement_Contract_ID.Equals("")
               ||newOne.Supplier_ID.Equals("")
               ||newOne.Factory.Equals("")
               ||newOne.Purchase_Group.Equals("")
               ||newOne.Purchase_Organization.Equals("")
               ||newOne.IT_Term_Code.Equals("")
               ||newOne.Payment_Type.Equals(""))
            {
                MessageBox.Show("请填写完整", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (newOne.Agreement_Contract_Type.Equals("价值合同")
                && newOne.Target_Value == 0.0
                && newOne.Currency_Type.Equals(""))
            {
                MessageBox.Show("请填写目标值", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (this.dgvMaterialItem.Rows.Count <= 0)
            {
                MessageUtil.ShowError("请添加数据!");
                return;
            }
            #endregion
            #region 为itemList赋值
            int i = 1;
            foreach (DataGridViewRow row in this.dgvMaterialItem.Rows)
            {
                Agreement_Contract_Item item = new Agreement_Contract_Item();
                item.A = getDataGridViewCellString(row.Cells[2]);
                item.Agreement_Contract_ID = newOne.Agreement_Contract_ID;
                item.Agreement_Contract_Version = newOne.Agreement_Contract_Version;
                item.Every = getDataGridViewCellDouble(row.Cells[6]);
                item.Factory = getDataGridViewCellString(row.Cells[10]);
                item.FinishedQuantity = 0.00;
                item.I = getDataGridViewCellString(row.Cells[1]);
                item.Item_Num = i;
                item.Material_Group = "";
                item.Material_ID = getDataGridViewCellString(row.Cells[3]);
                item.Net_Price = getDataGridViewCellDouble(row.Cells[5]);
                item.OPU = "PC";
                item.OUn = getDataGridViewCellString(row.Cells[8]);
                item.Price_Determin_ID = getDataGridViewCellString(row.Cells[9]);
                item.Short_Text = getDataGridViewCellString(row.Cells[4]);
                item.Stock_ID = "";
                item.Target_Quantity = getDataGridViewCellDouble(row.Cells[7]);
                itemList.Add(item);
                RecordInfo info = new RecordInfo();
                info.ContactState = newOne.Status;
                info.CreatTime = newOne.Create_Time;
                info.DemandCount = Convert.ToInt32(item.Target_Quantity);
                info.EndTime = newOne.End_Time;
                info.FactoryId = newOne.Factory;
                info.FromId = newOne.Agreement_Contract_ID;
                info.FromType = 2;
                info.MaterialId = item.Material_ID;
                info.MaterialName = item.Short_Text;
                info.NetPrice = (float)item.Net_Price;
                info.PaymentClause = newOne.Payment_Type;
                info.PriceDetermineId = item.Price_Determin_ID;
                info.PurchaseGroup = newOne.Purchase_Group;
                info.PurchaseOrg = newOne.Purchase_Organization;
                info.RecordInfoId = "RI" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + i;
                info.StartTime = newOne.Begin_Time;
                info.StockId = item.Stock_ID;
                info.SupplierId = newOne.Supplier_ID;
                info.SupplierName = "";
                info.TradeClause = newOne.IT_Term_Code;
                infoList.Add(info);
                i++;
            }
            if (newOne.Agreement_Contract_Type.Equals("数量合同"))
            {
                double sum = 0.00;
                foreach (Agreement_Contract_Item item in itemList)
                {
                    if (item.Target_Quantity == 0.00)
                    {
                        MessageUtil.ShowError("请填写目标值！");
                        return;
                    }
                    sum += item.Target_Quantity;
                }
                newOne.Target_Value = sum;
            }
            #endregion
            #region  写入并更新信息记录表
            int rowCount = agreementContractTool.addNewAgreementContract(newOne,itemList);
            bool flag = saveRecordInfo(infoList);
            string sourceId = this.cbxSourceId.SelectedValue.ToString();
            string supplierId = this.cbxSupplierID.SelectedValue.ToString();
            string factoryId = this.cbxFactoryId.SelectedValue.ToString();
            int count = tool.updateRecordInfo(sourceId,supplierId,factoryId);
            #endregion
            //MessageBox.Show("rowCount: " + rowCount + " flag: " + flag + " count: " + count);
            if (rowCount > 0 && flag && count > 0)
            {
                MessageUtil.ShowTips("合同保存成功！");
            }
            else
            {
                MessageBox.Show("网络错误，请稍后重试！");
                return;
            }
        }
        #endregion

        #region 写入数据库
        /// <summary>
        /// 将数据写入到采购信息记录表中，只写入Record_Info表
        /// </summary>
        private bool saveRecordInfo(List<RecordInfo> infoList)
        {
            int count = tool.addRecordInfo(infoList);
            if (count > 0)
                return true;
            else
                return false;
        }
        #endregion

        #region 为编辑/查看/变更合同界面写入数据
        /// <summary>
        /// 为编辑/查看/变更合同绑定下拉框数据源
        /// </summary>
        private void bindDataForOthers()
        {
            //货币类型ComboBox初始化
            List<string> currencyList = generalTool.GetCurrencyType();
            if (currencyList != null)
            {
                this.cbxCurrencyType.DataSource = currencyList;
                int cnyIndex = this.cbxCurrencyType.Items.IndexOf("CNY");
                if (cnyIndex != -1)
                {
                    this.cbxCurrencyType.SelectedIndex = cnyIndex;
                }
            }

            //寻源编号下拉框初始化

            //供应商编号下拉框初始化
            List<string> supplierList = generalTool.GetAllSupplier();
            if (supplierList != null)
            {
                this.cbxSupplierID.DataSource = supplierList;
            }

            //工厂下拉框初始化
            List<string> factoryList = generalTool.GetAllFactory();
            if (factoryList != null)
            {
                this.cbxFactoryId.DataSource = factoryList;
            }

            //付款条件下拉框初始化
            List<string> paymentList = generalTool.GetAllPaymentClauseName();
            if (paymentList != null)
            {
                this.cbxPaymentClause.DataSource = paymentList;
            }

            //交付条件下拉框初始化
            List<string> tradeList = generalTool.GetAllTradeClauseID();
            {
                if (tradeList != null)
                {
                    this.cbxTradeClause.DataSource = tradeList;
                }
            }
        }

        private void initialDataForOthers(Agreement_Contract contract)
        {
            bindDataForOthers();
            switch (opType)
            {
                case "view": this.Text = "查看框架协议"; break;
                case "edit": this.Text = "编辑框架协议"; break;
                default: break;
            }
            this.cbxSupplierID.Text = contract.Supplier_ID;
            this.cbxFactoryId.Text = contract.Factory;
            this.cbxAgreementType.SelectedIndex = this.cbxAgreementType.Items.IndexOf(contract.Agreement_Contract_Type);
            this.dtpBeginTime.Value = contract.Begin_Time;
            this.dtpEndTime.Value = contract.End_Time;
            this.tbPurchaseGroup.Text = contract.Purchase_Group;
            this.tbPurchaseOrg.Text = contract.Purchase_Organization;
            this.dtpCreateTime.Value = contract.Create_Time;
            this.tbStockId.Text = contract.Storage_Location;
            this.cbxPaymentClause.SelectedIndex = this.cbxPaymentClause.Items.IndexOf(contract.Payment_Type);
            this.tbDays1.Text = contract.Days1 + "";
            this.tbDiscountRate1.Text = contract.Discount_Rate1 + "";
            this.tbDays2.Text = contract.Days2 + "";
            this.tbDiscountRate2.Text = contract.Discount_Rate2 + "";
            this.tbDays3.Text = contract.Days3 + "";
            this.tbTargetValue.Text = contract.Target_Value + "";
            this.cbxCurrencyType.SelectedIndex = this.cbxCurrencyType.Items.IndexOf(
                contract.Currency_Type);
            this.cbxTradeClause.Text = contract.IT_Term_Code;
            this.tblTermText.Text = contract.IT_Term_Text;

            //填写项目数据
            List<Agreement_Contract_Item> itemList = agreementContractTool.getItemsByAgreementContractID(contract.Agreement_Contract_ID, contract.Agreement_Contract_Version);
            int rowNum = itemList.Count;
            this.dgvMaterialItem.Rows.Add(rowNum);
            int i = 0;
            foreach (Agreement_Contract_Item item in itemList)
            {
                DataGridViewRow currentRow = this.dgvMaterialItem.Rows[i];
                currentRow.Cells[0].Value = item.Item_Num;
                currentRow.Cells[1].Value = item.I;
                currentRow.Cells[2].Value = item.A;
                currentRow.Cells[3].Value = item.Material_ID;
                currentRow.Cells[4].Value = item.Short_Text;
                currentRow.Cells[5].Value = item.Net_Price;
                currentRow.Cells[6].Value = item.Every;
                currentRow.Cells[7].Value = item.Target_Quantity;
                currentRow.Cells[8].Value = item.OUn;
                currentRow.Cells[9].Value = item.Price_Determin_ID;
                currentRow.Cells[10].Value = item.Factory;
                i++;
            }
            if (opType.Equals("view"))
            {
                this.dgvMaterialItem.ReadOnly = true;
                this.btnSave.Enabled = false;
            }
        }
        #endregion

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
