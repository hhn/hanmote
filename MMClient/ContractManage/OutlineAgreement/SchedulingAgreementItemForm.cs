﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Model.ContractManage.OutlineAgreement;
using Lib.Bll.ContractManageBLL.OutlineAgreementBLL;
using WeifenLuo.WinFormsUI.Docking;
using Lib.Bll.MDBll.General;
using Lib.Bll.MDBll.MT;
using Lib.Model.MD.MT;

namespace MMClient.ContractManage.OutlineAgreement
{
    public partial class SchedulingAgreementItemForm : DockContent
    {
        //业务逻辑
        private SchedulingAgreementBLL schedulingAgreementTool = new SchedulingAgreementBLL();
        private GeneralBLL generalTool = new GeneralBLL();
        private MaterialBLL materialTool = new MaterialBLL();
        private MaterialAccountBLL materialAccountTool = new MaterialAccountBLL();
        //当前form操作类型(new、edit、display)
        private string opType;
        //保存协议的信息
        Scheduling_Agreement schedulingAgreement;
        //框架协议项
        List<Scheduling_Agreement_Item> currentItemList;
        //parentForm
        private DockContent parentForm;
        // 所有的物料ID
        private List<string> materialIDList = null;
        // 所有的物料组
        private List<string> materialGroupIDList = null;

        /// <summary>
        /// 框架协议项
        /// </summary>
        /// <param name="_schedulingAgreement"></param>
        /// <param name="_opType"></param>
        public SchedulingAgreementItemForm(Scheduling_Agreement _schedulingAgreement, string _opType,
            DockContent _parentForm)
        {
            InitializeComponent();

            this.schedulingAgreement = _schedulingAgreement;
            this.opType = _opType;
            this.parentForm = _parentForm;

            initialForm();
        }

        /// <summary>
        /// 初始化界面
        /// </summary>
        private void initialForm()
        {
            //获取所有的物料ID
            materialIDList = materialTool.GetAllMaterialID();
            //获取所有的物料组ID
            materialGroupIDList = generalTool.GetAllGroupName();

            //显示协议抬头信息
            this.tbAgreementID.Text = schedulingAgreement.Scheduling_Agreement_ID;
            this.tbAgreementType.Text = schedulingAgreement.Scheduling_Agreement_Type;
            this.tbSupplierID.Text = schedulingAgreement.Supplier_ID;
            this.tbCurrencyType.Text = schedulingAgreement.Currency_Type;
            this.dtpCreateTime.Value = schedulingAgreement.Create_Time;

            if (opType.Equals("display"))
            {
                this.btnAdd.Enabled = false;
                this.btnDelete.Enabled = false;
                this.btnSave.Enabled = false;
            }

            if (!opType.Equals("new")) {
                List<Scheduling_Agreement_Item> itemsList = schedulingAgreementTool.getItemsBySchedulingAgreementID(
                    schedulingAgreement.Scheduling_Agreement_ID);
                fillMaterialItem(itemsList);
            }
        }

        /// <summary>
        /// 填写已存在的数据
        /// </summary>
        /// <param name="itemList"></param>
        private void fillMaterialItem(List<Scheduling_Agreement_Item> itemList)
        {
            int rowNum = itemList.Count;
            this.dgvAgreementItems.Rows.Add(rowNum);
            int i = 0;
            foreach (Scheduling_Agreement_Item item in itemList)
            {
                DataGridViewRow currentRow = this.dgvAgreementItems.Rows[i];
                currentRow.Cells[0].Value = item.Item_Num;
                currentRow.Cells[1].Value = item.I;
                currentRow.Cells[2].Value = item.A;
                //绑定数据源
                DataGridViewComboBoxCell cell =
                    (DataGridViewComboBoxCell)currentRow.Cells[3];
                cell.DataSource = materialIDList;
                cell.Value = item.Material_ID;
                //currentRow.Cells[3].Value = item.Material_ID;
                currentRow.Cells[4].Value = item.Short_Text;
                currentRow.Cells[5].Value = convertDoubleToString(item.Target_Quantity);
                currentRow.Cells[6].Value = item.OUn;
                currentRow.Cells[7].Value = convertDoubleToString(item.Net_Price);
                currentRow.Cells[8].Value = convertDoubleToString(item.Every);
                currentRow.Cells[9].Value = item.OPU;
                currentRow.Cells[10].Value = item.Material_Group;
                currentRow.Cells[11].Value = item.Factory;

                i++;
            }

            if (opType.Equals("display"))
            {
                this.dgvAgreementItems.ReadOnly = true;
            }
        }

        /// <summary>
        /// 保存
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSave_Click(object sender, EventArgs e)
        {
            //先检测是否存在至少一条有效记录
            //最后一个是系统默认条件，不算
            if (this.dgvAgreementItems.Rows.Count <= 0)
            {
                MessageBox.Show("请添加数据", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            List<Scheduling_Agreement_Item> itemList = new List<Scheduling_Agreement_Item>();
            try
            {
                foreach (DataGridViewRow row in this.dgvAgreementItems.Rows)
                {
                    Scheduling_Agreement_Item item = new Scheduling_Agreement_Item();
                    item.Scheduling_Agreement_ID = schedulingAgreement.Scheduling_Agreement_ID;
                    item.Item_Num = Convert.ToInt32(row.Cells[0].Value.ToString());
                    item.I = getDataGridViewCellValueString(row.Cells[1]);
                    item.A = getDataGridViewCellValueString(row.Cells[2]);
                    item.Material_ID = getDataGridViewCellValueString(row.Cells[3]);
                    item.Short_Text = getDataGridViewCellValueString(row.Cells[4]);
                    item.Target_Quantity = getDataGridViewCellValueDouble(row.Cells[5]);
                    item.OUn = getDataGridViewCellValueString(row.Cells[6]);
                    item.Net_Price = getDataGridViewCellValueDouble(row.Cells[7]);
                    item.Every = getDataGridViewCellValueDouble(row.Cells[8]);
                    item.OPU = getDataGridViewCellValueString(row.Cells[9]);
                    item.Material_Group = getDataGridViewCellValueString(row.Cells[10]);
                    item.Factory = getDataGridViewCellValueString(row.Cells[11]);

                    itemList.Add(item);
                };
            }
            catch (Exception ex) {
                MessageBox.Show("请正确填写", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            # region 判断操作类型

            if (opType.Equals("new"))
            {
                //新建计划协议
                int result = schedulingAgreementTool.addNewSchedulingAgreement(
                    schedulingAgreement, itemList);
                if (result > 0)
                {
                    MessageBox.Show("保存成功");
                    opType = "edit";
                }
                else
                {
                    MessageBox.Show("保存失败");
                }
            }
            else if(opType.Equals("edit")){
                var result = MessageBox.Show("确认更改计划协议内容？", "确认", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                if (result == DialogResult.Cancel)
                {
                    return;
                }
                //计划协议更新
                schedulingAgreement.Status = "待审核";
                int num = schedulingAgreementTool.updateSchedulingAgreement(
                    schedulingAgreement, itemList);
                if (num > 0)
                    MessageBox.Show("更新成功");
                else
                    MessageBox.Show("更新失败");
            }

            # endregion
        }

        /// <summary>
        /// 获得DataGridViewCell的string值
        /// </summary>
        /// <param name="cell"></param>
        /// <returns></returns>
        private string getDataGridViewCellValueString(DataGridViewCell cell)
        {
            if (cell.Value == null)
                return "";
            else
                return cell.Value.ToString();
        }

        /// <summary>
        /// 获得DataGridView的double值
        /// </summary>
        /// <param name="cell"></param>
        /// <returns></returns>
        private double getDataGridViewCellValueDouble(DataGridViewCell cell)
        {
            if (cell.Value == null || String.IsNullOrEmpty(cell.Value.ToString()))
                return 0.0;
            else
                return Convert.ToDouble(cell.Value.ToString());
        }

        /// <summary>
        /// 点击添加
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAdd_Click(object sender, EventArgs e)
        {
            //支持多种添加形式
            this.dgvAgreementItems.Rows.Add();

            //项目号
            int rowCount = this.dgvAgreementItems.Rows.Count;
            if (rowCount == 1)
            {
                this.dgvAgreementItems.Rows[0].Cells[0].Value = 1;
            }
            else
            {
                this.dgvAgreementItems.Rows[rowCount - 1].Cells[0].Value =
                    Convert.ToInt32(this.dgvAgreementItems.Rows[rowCount - 2].Cells[0].Value) + 1;
            }
            setDataGridViewEditable(this.dgvAgreementItems.Rows[rowCount - 1].Cells[0], false);
            //设置物料ID源
            DataGridViewComboBoxCell cell = (DataGridViewComboBoxCell)this.dgvAgreementItems.Rows[rowCount - 1].Cells[3];
            cell.DataSource = materialIDList;
            //设置工厂
            this.dgvAgreementItems.Rows[rowCount - 1].Cells[this.dgvAgreementItems.Columns.Count - 1].Value = schedulingAgreement.Factory;
            setDataGridViewEditable(this.dgvAgreementItems.Rows[rowCount - 1].Cells[this.dgvAgreementItems.Columns.Count - 1], false);
        }

        /// <summary>
        /// 设置单元格是否可编辑
        /// </summary>
        /// <param name="cell">设置的单元格</param>
        /// <param name="status">是否可编辑</param>
        private void setDataGridViewEditable(DataGridViewCell cell, bool status)
        {
            if (status)
            {
                cell.ReadOnly = false;
                cell.Style.BackColor = Color.White;
            }
            else
            {
                cell.ReadOnly = true;
                cell.Style.BackColor = Color.LightSkyBlue;
            }
        }

        /// <summary>
        /// 如果I列内容发生变化，其余列相应变化
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvAgreementItems_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            //初始化的时候也会触发这个时间，此时row为空，这里进行过滤一下
            if (e.RowIndex < 0 || e.RowIndex >= this.dgvAgreementItems.Rows.Count)
            {
                return;
            }
            if (this.dgvAgreementItems.Columns[e.ColumnIndex].HeaderText.Equals("I"))
            {
                //datagridview内容为空时，value为null，不是""
                object value = this.dgvAgreementItems.Rows[e.RowIndex].Cells[e.ColumnIndex].Value;
                string str = value == null ? "" : value.ToString().Trim();
                DataGridViewRow row = this.dgvAgreementItems.Rows[e.RowIndex];
                //为空
                if (str.Equals(""))
                {
                    setDataGridViewEditable(row.Cells[3], true);           //物料编号需填写
                    // 绑定数据源
                    DataGridViewComboBoxCell cell = (DataGridViewComboBoxCell)row.Cells[3];
                    setDataGridViewEditable(row.Cells[6], true);           //OUn需要填写
                    setDataGridViewEditable(row.Cells[7], true);           //净价需要填写
                    setDataGridViewEditable(row.Cells[8], true);           //每 需要填写
                    setDataGridViewEditable(row.Cells[9], true);           //OPU需要填写
                    setDataGridViewEditable(row.Cells[10], false);
                }
                else if (str.Equals("W"))
                {
                    //物料组
                    setDataGridViewEditable(row.Cells[3], false);           //物料编号不需填写
                    // 清空数据源
                    DataGridViewComboBoxCell cell = (DataGridViewComboBoxCell)row.Cells[3];
                    cell.Value = "";
                    setDataGridViewEditable(row.Cells[6], false);           //OUn不需要填写
                    setDataGridViewEditable(row.Cells[7], false);           //净价不需要填写
                    setDataGridViewEditable(row.Cells[8], false);           //每 不需要填写
                    setDataGridViewEditable(row.Cells[9], false);           //OPU不需要填写
                }
                else if (str.Equals("M"))
                {

                }
                else if (str.Equals("D"))
                {

                }
                else if (str.Equals("K"))
                {

                }
            }
        }

        /// <summary>
        /// 把double转化为string
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        private string convertDoubleToString(double value)
        {
            if (Math.Abs(value) <= 0.00000001)
                return "";
            else
                return Convert.ToString(value);
        }

        /// <summary>
        /// 点击删除行
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (this.dgvAgreementItems.Rows.Count == 0)
            {
                MessageBox.Show("没有可删除内容", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            int rowIndex = this.dgvAgreementItems.CurrentCell.RowIndex;
            if (rowIndex >= 0 && rowIndex < this.dgvAgreementItems.Rows.Count)
            {
                this.dgvAgreementItems.Rows.RemoveAt(rowIndex);
                updateRowNum();
            }
        }

        /// <summary>
        /// 刷新行的序号
        /// </summary>
        private void updateRowNum()
        {
            int i = 1;
            foreach (DataGridViewRow row in this.dgvAgreementItems.Rows)
            {
                row.Cells[0].Value = i;
                i++;
            }
        }

        /// <summary>
        /// 维护交货计划
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDeliveryManage_Click(object sender, EventArgs e)
        {
            if (this.dgvAgreementItems.Rows.Count == 0)
            {
                MessageBox.Show("请选择项目", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            int rowIndex = this.dgvAgreementItems.CurrentCell.RowIndex;
            if (rowIndex >= 0 && rowIndex < this.dgvAgreementItems.Rows.Count)
            {
                //获取Scheduling_Agreement_Item
                Scheduling_Agreement_Item schedulingAgreementItem = new Scheduling_Agreement_Item();
                DataGridViewRow selectedRow = this.dgvAgreementItems.Rows[rowIndex];
                schedulingAgreementItem.Scheduling_Agreement_ID = schedulingAgreement.Scheduling_Agreement_ID;
                schedulingAgreementItem.Item_Num = Convert.ToInt32(selectedRow.Cells[0].Value.ToString());
                schedulingAgreementItem.I = getDataGridViewCellValueString(selectedRow.Cells[1]);
                schedulingAgreementItem.A = getDataGridViewCellValueString(selectedRow.Cells[2]);
                schedulingAgreementItem.Material_ID = getDataGridViewCellValueString(selectedRow.Cells[3]);
                schedulingAgreementItem.Short_Text = getDataGridViewCellValueString(selectedRow.Cells[4]);
                schedulingAgreementItem.Target_Quantity = getDataGridViewCellValueDouble(selectedRow.Cells[5]);
                schedulingAgreementItem.OUn = getDataGridViewCellValueString(selectedRow.Cells[6]);
                schedulingAgreementItem.Net_Price = getDataGridViewCellValueDouble(selectedRow.Cells[7]);
                schedulingAgreementItem.Every = getDataGridViewCellValueDouble(selectedRow.Cells[8]);
                schedulingAgreementItem.OPU = getDataGridViewCellValueString(selectedRow.Cells[9]);
                schedulingAgreementItem.Material_Group = getDataGridViewCellValueString(selectedRow.Cells[10]);
                schedulingAgreementItem.Factory = getDataGridViewCellValueString(selectedRow.Cells[11]);

                //获取对应的交货计划
                string agreementID = schedulingAgreement.Scheduling_Agreement_ID;
                int materialNum = Convert.ToInt32(this.dgvAgreementItems.Rows[rowIndex].Cells[0].Value.ToString());
                List<Delivery_Item> itemList = schedulingAgreementTool.getDeliveryItemList(
                    agreementID, materialNum);

                DeliverySchedulingItemsForm form = new DeliverySchedulingItemsForm(schedulingAgreement, schedulingAgreementItem, itemList, opType);
                SingletonUserUI.addToUserUI(form);
            }
        }

        private void dgvAgreementItems_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            int columnIndex = this.dgvAgreementItems.CurrentCell.ColumnIndex;
            if (columnIndex == 3 && e.Control is ComboBox)
            {
                ComboBox combo = e.Control as ComboBox;
                combo.SelectedIndexChanged += materialID_SelectedIndexChanged;
            }
        }

        private void materialID_SelectedIndexChanged(object sender, EventArgs e)
        {
            int rowIndex = this.dgvAgreementItems.CurrentCell.RowIndex;
            int columnIndex = this.dgvAgreementItems.CurrentCell.ColumnIndex;
            //可以转换
            if (rowIndex >= 0)
            {
                var sendingCB = sender as DataGridViewComboBoxEditingControl;
                string materialID = sendingCB.EditingControlFormattedValue.ToString();
                if (!materialID.Equals(""))
                {
                    //DataTable dt = generalTool.getMaterialInfo(materialID);
                    MaterialBase materialInfo = materialAccountTool
                        .GetBasicInformation(materialID);

                    //DataRow curRow = dt.Rows[0];
                    DataGridViewRow dgvRow = this.dgvAgreementItems.Rows[rowIndex];
                    //短文本
                    dgvRow.Cells[4].Value = materialInfo.Material_Name;
                    //单位
                    dgvRow.Cells[6].Value = materialInfo.Measurement;
                    //物料组
                    dgvRow.Cells[10].Value = materialInfo.Material_Group;
                }
            }
        }

        private void SchedulingAgreementItemForm_Load(object sender, EventArgs e)
        {

        }
    }
}
