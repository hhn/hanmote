﻿namespace MMClient.ContractManage.OutlineAgreement
{
    partial class ContractMonitor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lbOwner = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lbSupplierName = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lbStatus = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lbContractType = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lbContractId = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.btnSave = new System.Windows.Forms.Button();
            this.tbTimeAlert = new System.Windows.Forms.TextBox();
            this.tbNumberAlert = new System.Windows.Forms.TextBox();
            this.tbTargetNumber = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.tbValueAlert = new System.Windows.Forms.TextBox();
            this.tbValueRatioAlert = new System.Windows.Forms.TextBox();
            this.tbTargetValue = new System.Windows.Forms.TextBox();
            this.tbPurchaseGroup = new System.Windows.Forms.TextBox();
            this.tbPurchaseOrg = new System.Windows.Forms.TextBox();
            this.tbContractType = new System.Windows.Forms.TextBox();
            this.tbContractId = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.cbxMesurement = new System.Windows.Forms.ComboBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.cbxCurrencyType = new System.Windows.Forms.ComboBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.dtpEndTime = new System.Windows.Forms.DateTimePicker();
            this.label15 = new System.Windows.Forms.Label();
            this.dtpStartTime = new System.Windows.Forms.DateTimePicker();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.cbxIsSecret = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.dgvDetail = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDetail)).BeginInit();
            this.SuspendLayout();
            // 
            // supplierInforGroupBox
            // 
            this.groupBox1.Controls.Add(this.lbOwner);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.lbSupplierName);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.lbStatus);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.lbContractType);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.lbContractId);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(3, 2);
            this.groupBox1.Name = "supplierInforGroupBox";
            this.groupBox1.Size = new System.Drawing.Size(767, 100);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // lbOwner
            // 
            this.lbOwner.AutoSize = true;
            this.lbOwner.Location = new System.Drawing.Point(108, 57);
            this.lbOwner.Name = "lbOwner";
            this.lbOwner.Size = new System.Drawing.Size(65, 12);
            this.lbOwner.TabIndex = 9;
            this.lbOwner.Text = "0010李静宇";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(25, 57);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(77, 12);
            this.label5.TabIndex = 8;
            this.label5.Text = "合同所有者：";
            // 
            // lbSupplierName
            // 
            this.lbSupplierName.AutoSize = true;
            this.lbSupplierName.Location = new System.Drawing.Point(574, 27);
            this.lbSupplierName.Name = "lbSupplierName";
            this.lbSupplierName.Size = new System.Drawing.Size(53, 12);
            this.lbSupplierName.TabIndex = 7;
            this.lbSupplierName.Text = "SP001003";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(527, 27);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 12);
            this.label4.TabIndex = 6;
            this.label4.Text = "供应商：";
            // 
            // lbStatus
            // 
            this.lbStatus.AutoSize = true;
            this.lbStatus.Location = new System.Drawing.Point(411, 27);
            this.lbStatus.Name = "lbStatus";
            this.lbStatus.Size = new System.Drawing.Size(41, 12);
            this.lbStatus.TabIndex = 5;
            this.lbStatus.Text = "处理中";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(373, 27);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 12);
            this.label3.TabIndex = 4;
            this.label3.Text = "状态：";
            // 
            // lbContractType
            // 
            this.lbContractType.AutoSize = true;
            this.lbContractType.Location = new System.Drawing.Point(279, 27);
            this.lbContractType.Name = "lbContractType";
            this.lbContractType.Size = new System.Drawing.Size(53, 12);
            this.lbContractType.TabIndex = 3;
            this.lbContractType.Text = "数量合同";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(223, 27);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 12);
            this.label2.TabIndex = 2;
            this.label2.Text = "合同类型：";
            // 
            // lbContractId
            // 
            this.lbContractId.AutoSize = true;
            this.lbContractId.Location = new System.Drawing.Point(79, 27);
            this.lbContractId.Name = "lbContractId";
            this.lbContractId.Size = new System.Drawing.Size(65, 12);
            this.lbContractId.TabIndex = 1;
            this.lbContractId.Text = "5001000192";
            this.lbContractId.Click += new System.EventHandler(this.lbContractId_Click);
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(25, 27);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(65, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "合同编号：";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(3, 108);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(757, 400);
            this.tabControl1.TabIndex = 1;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.btnSave);
            this.tabPage1.Controls.Add(this.tbTimeAlert);
            this.tabPage1.Controls.Add(this.tbNumberAlert);
            this.tabPage1.Controls.Add(this.tbTargetNumber);
            this.tabPage1.Controls.Add(this.label26);
            this.tabPage1.Controls.Add(this.tbValueAlert);
            this.tabPage1.Controls.Add(this.tbValueRatioAlert);
            this.tabPage1.Controls.Add(this.tbTargetValue);
            this.tabPage1.Controls.Add(this.tbPurchaseGroup);
            this.tabPage1.Controls.Add(this.tbPurchaseOrg);
            this.tabPage1.Controls.Add(this.tbContractType);
            this.tabPage1.Controls.Add(this.tbContractId);
            this.tabPage1.Controls.Add(this.label25);
            this.tabPage1.Controls.Add(this.cbxMesurement);
            this.tabPage1.Controls.Add(this.label24);
            this.tabPage1.Controls.Add(this.label23);
            this.tabPage1.Controls.Add(this.label22);
            this.tabPage1.Controls.Add(this.label21);
            this.tabPage1.Controls.Add(this.label20);
            this.tabPage1.Controls.Add(this.cbxCurrencyType);
            this.tabPage1.Controls.Add(this.label19);
            this.tabPage1.Controls.Add(this.label18);
            this.tabPage1.Controls.Add(this.label17);
            this.tabPage1.Controls.Add(this.label16);
            this.tabPage1.Controls.Add(this.dtpEndTime);
            this.tabPage1.Controls.Add(this.label15);
            this.tabPage1.Controls.Add(this.dtpStartTime);
            this.tabPage1.Controls.Add(this.label14);
            this.tabPage1.Controls.Add(this.label13);
            this.tabPage1.Controls.Add(this.label12);
            this.tabPage1.Controls.Add(this.label11);
            this.tabPage1.Controls.Add(this.label10);
            this.tabPage1.Controls.Add(this.cbxIsSecret);
            this.tabPage1.Controls.Add(this.label9);
            this.tabPage1.Controls.Add(this.label8);
            this.tabPage1.Controls.Add(this.label7);
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(749, 374);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "预警";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(475, 319);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 46;
            this.btnSave.Text = "保存";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.button1_Click);
            // 
            // tbTimeAlert
            // 
            this.tbTimeAlert.Location = new System.Drawing.Point(94, 319);
            this.tbTimeAlert.Name = "tbTimeAlert";
            this.tbTimeAlert.Size = new System.Drawing.Size(104, 21);
            this.tbTimeAlert.TabIndex = 45;
            // 
            // tbNumberAlert
            // 
            this.tbNumberAlert.Location = new System.Drawing.Point(502, 244);
            this.tbNumberAlert.Name = "tbNumberAlert";
            this.tbNumberAlert.Size = new System.Drawing.Size(121, 21);
            this.tbNumberAlert.TabIndex = 44;
            // 
            // tbTargetNumber
            // 
            this.tbTargetNumber.Location = new System.Drawing.Point(502, 213);
            this.tbTargetNumber.Name = "tbTargetNumber";
            this.tbTargetNumber.Size = new System.Drawing.Size(121, 21);
            this.tbTargetNumber.TabIndex = 43;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(608, 109);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(11, 12);
            this.label26.TabIndex = 42;
            this.label26.Text = "%";
            // 
            // tbValueAlert
            // 
            this.tbValueAlert.Location = new System.Drawing.Point(502, 137);
            this.tbValueAlert.Name = "tbValueAlert";
            this.tbValueAlert.Size = new System.Drawing.Size(121, 21);
            this.tbValueAlert.TabIndex = 41;
            // 
            // tbValueRatioAlert
            // 
            this.tbValueRatioAlert.Location = new System.Drawing.Point(502, 106);
            this.tbValueRatioAlert.Name = "tbValueRatioAlert";
            this.tbValueRatioAlert.Size = new System.Drawing.Size(100, 21);
            this.tbValueRatioAlert.TabIndex = 40;
            // 
            // tbTargetValue
            // 
            this.tbTargetValue.Location = new System.Drawing.Point(502, 73);
            this.tbTargetValue.Name = "tbTargetValue";
            this.tbTargetValue.Size = new System.Drawing.Size(121, 21);
            this.tbTargetValue.TabIndex = 39;
            // 
            // tbPurchaseGroup
            // 
            this.tbPurchaseGroup.Location = new System.Drawing.Point(77, 213);
            this.tbPurchaseGroup.Name = "tbPurchaseGroup";
            this.tbPurchaseGroup.ReadOnly = true;
            this.tbPurchaseGroup.Size = new System.Drawing.Size(121, 21);
            this.tbPurchaseGroup.TabIndex = 38;
            // 
            // tbPurchaseOrg
            // 
            this.tbPurchaseOrg.Location = new System.Drawing.Point(77, 181);
            this.tbPurchaseOrg.Name = "tbPurchaseOrg";
            this.tbPurchaseOrg.ReadOnly = true;
            this.tbPurchaseOrg.Size = new System.Drawing.Size(121, 21);
            this.tbPurchaseOrg.TabIndex = 37;
            // 
            // tbContractType
            // 
            this.tbContractType.Location = new System.Drawing.Point(77, 72);
            this.tbContractType.Name = "tbContractType";
            this.tbContractType.ReadOnly = true;
            this.tbContractType.Size = new System.Drawing.Size(121, 21);
            this.tbContractType.TabIndex = 36;
            // 
            // tbContractId
            // 
            this.tbContractId.Location = new System.Drawing.Point(77, 36);
            this.tbContractId.Name = "tbContractId";
            this.tbContractId.ReadOnly = true;
            this.tbContractId.Size = new System.Drawing.Size(121, 21);
            this.tbContractId.TabIndex = 35;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(401, 247);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(89, 12);
            this.label25.TabIndex = 33;
            this.label25.Text = "警报释放数量：";
            // 
            // cbxMesurement
            // 
            this.cbxMesurement.FormattingEnabled = true;
            this.cbxMesurement.Location = new System.Drawing.Point(628, 213);
            this.cbxMesurement.Name = "cbxMesurement";
            this.cbxMesurement.Size = new System.Drawing.Size(63, 20);
            this.cbxMesurement.TabIndex = 32;
            this.cbxMesurement.Text = "EA";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(401, 221);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(95, 12);
            this.label24.TabIndex = 30;
            this.label24.Text = "目标数量/单位：";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label23.Location = new System.Drawing.Point(401, 190);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(31, 12);
            this.label23.TabIndex = 29;
            this.label23.Text = "数量";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(401, 140);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(89, 12);
            this.label22.TabIndex = 27;
            this.label22.Text = "警报释放价值：";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(401, 109);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(89, 12);
            this.label21.TabIndex = 25;
            this.label21.Text = "警报释放比例：";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(401, 76);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(53, 12);
            this.label20.TabIndex = 23;
            this.label20.Text = "目标值：";
            // 
            // cbxCurrencyType
            // 
            this.cbxCurrencyType.FormattingEnabled = true;
            this.cbxCurrencyType.Location = new System.Drawing.Point(502, 37);
            this.cbxCurrencyType.Name = "cbxCurrencyType";
            this.cbxCurrencyType.Size = new System.Drawing.Size(121, 20);
            this.cbxCurrencyType.TabIndex = 22;
            this.cbxCurrencyType.Text = "CNY";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(401, 40);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(41, 12);
            this.label19.TabIndex = 21;
            this.label19.Text = "货币：";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label18.Location = new System.Drawing.Point(401, 14);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(31, 12);
            this.label18.TabIndex = 20;
            this.label18.Text = "价值";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(204, 322);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(17, 12);
            this.label17.TabIndex = 19;
            this.label17.Text = "天";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(6, 322);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(65, 12);
            this.label16.TabIndex = 17;
            this.label16.Text = "警报失效：";
            // 
            // dtpEndTime
            // 
            this.dtpEndTime.Location = new System.Drawing.Point(221, 287);
            this.dtpEndTime.Name = "dtpEndTime";
            this.dtpEndTime.Size = new System.Drawing.Size(103, 21);
            this.dtpEndTime.TabIndex = 16;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(204, 293);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(11, 12);
            this.label15.TabIndex = 15;
            this.label15.Text = "-";
            // 
            // dtpStartTime
            // 
            this.dtpStartTime.Location = new System.Drawing.Point(94, 287);
            this.dtpStartTime.Name = "dtpStartTime";
            this.dtpStartTime.Size = new System.Drawing.Size(104, 21);
            this.dtpStartTime.TabIndex = 14;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(6, 293);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(83, 12);
            this.label14.TabIndex = 13;
            this.label14.Text = "有效期从/到：";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label13.Location = new System.Drawing.Point(6, 264);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(31, 12);
            this.label13.TabIndex = 12;
            this.label13.Text = "日期";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(6, 216);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(53, 12);
            this.label12.TabIndex = 10;
            this.label12.Text = "采购组：";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(6, 185);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(65, 12);
            this.label11.TabIndex = 8;
            this.label11.Text = "采购组织：";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label10.Location = new System.Drawing.Point(6, 160);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(57, 12);
            this.label10.TabIndex = 7;
            this.label10.Text = "组织机构";
            // 
            // cbxIsSecret
            // 
            this.cbxIsSecret.FormattingEnabled = true;
            this.cbxIsSecret.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.cbxIsSecret.Items.AddRange(new object[] {
            "机密",
            "非机密"});
            this.cbxIsSecret.Location = new System.Drawing.Point(77, 106);
            this.cbxIsSecret.Name = "cbxIsSecret";
            this.cbxIsSecret.Size = new System.Drawing.Size(121, 20);
            this.cbxIsSecret.TabIndex = 6;
            this.cbxIsSecret.Text = "非机密";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 109);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(41, 12);
            this.label9.TabIndex = 5;
            this.label9.Text = "机密：";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 76);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(65, 12);
            this.label8.TabIndex = 3;
            this.label8.Text = "合同类型：";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 40);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(65, 12);
            this.label7.TabIndex = 1;
            this.label7.Text = "合同编号：";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label6.Location = new System.Drawing.Point(6, 14);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(31, 12);
            this.label6.TabIndex = 0;
            this.label6.Text = "标识";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.dgvDetail);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(749, 374);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "凭证";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // dgvDetail
            // 
            this.dgvDetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDetail.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column4,
            this.Column5,
            this.Column6,
            this.Column7,
            this.Column8,
            this.Column9,
            this.Column10,
            this.Column11});
            this.dgvDetail.Location = new System.Drawing.Point(3, 0);
            this.dgvDetail.Name = "dgvDetail";
            this.dgvDetail.RowTemplate.Height = 23;
            this.dgvDetail.Size = new System.Drawing.Size(746, 371);
            this.dgvDetail.TabIndex = 0;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "凭证类型";
            this.Column1.Name = "Column1";
            this.Column1.Width = 77;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "凭证编号";
            this.Column2.Name = "Column2";
            this.Column2.Width = 77;
            // 
            // Column4
            // 
            this.Column4.HeaderText = "项目编号";
            this.Column4.Name = "Column4";
            this.Column4.Width = 77;
            // 
            // Column5
            // 
            this.Column5.HeaderText = "日期";
            this.Column5.Name = "Column5";
            this.Column5.Width = 66;
            // 
            // Column6
            // 
            this.Column6.HeaderText = "数量";
            this.Column6.Name = "Column6";
            this.Column6.Width = 66;
            // 
            // Column7
            // 
            this.Column7.HeaderText = "单位";
            this.Column7.Name = "Column7";
            this.Column7.Width = 66;
            // 
            // Column8
            // 
            this.Column8.HeaderText = "价值";
            this.Column8.Name = "Column8";
            this.Column8.Width = 66;
            // 
            // Column9
            // 
            this.Column9.HeaderText = "货币";
            this.Column9.Name = "Column9";
            this.Column9.Width = 66;
            // 
            // Column10
            // 
            this.Column10.HeaderText = "工厂";
            this.Column10.Name = "Column10";
            this.Column10.Width = 66;
            // 
            // Column11
            // 
            this.Column11.HeaderText = "状态";
            this.Column11.Name = "Column11";
            this.Column11.Width = 66;
            // 
            // ContractMonitor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(772, 511);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "ContractMonitor";
            this.Text = "框架协议执行监控";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvDetail)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lbOwner;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lbSupplierName;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lbStatus;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lbContractType;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lbContractId;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.ComboBox cbxMesurement;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.ComboBox cbxCurrencyType;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.DateTimePicker dtpEndTime;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.DateTimePicker dtpStartTime;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox cbxIsSecret;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.DataGridView dgvDetail;
        private System.Windows.Forms.TextBox tbTimeAlert;
        private System.Windows.Forms.TextBox tbNumberAlert;
        private System.Windows.Forms.TextBox tbTargetNumber;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox tbValueAlert;
        private System.Windows.Forms.TextBox tbValueRatioAlert;
        private System.Windows.Forms.TextBox tbTargetValue;
        private System.Windows.Forms.TextBox tbPurchaseGroup;
        private System.Windows.Forms.TextBox tbPurchaseOrg;
        private System.Windows.Forms.TextBox tbContractType;
        private System.Windows.Forms.TextBox tbContractId;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column11;
    }
}