﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Model.ContractManage.OutlineAgreement;
using Lib.Bll.ContractManageBLL.OutlineAgreementBLL;
using Lib.Model.MD;
using Lib.Bll.MDBll.General;
using Lib.Bll.MDBll.MT;
using Lib.Bll.SourcingManage.SourcingManagement;
using WeifenLuo.WinFormsUI.Docking;
using Lib.Common.CommonUtils;
using Lib.SqlServerDAL;
using Lib.Model.SourcingManage.SourcingManagement;

namespace MMClient.ContractManage.OutlineAgreement
{
    public partial class AgreementContractForm : DockContent
    {
        #region 字段初始化
        //协议合同操作工具
        private AgreementContractBLL agreementContractTool = new AgreementContractBLL();
        //主数据读取工具
        private GeneralBLL generalTool = new GeneralBLL();
        //寻源子项结果读取工具
        private SourceItemBLL sourceItemTool = new SourceItemBLL();
     //   private MaterialBLL materialTool = new MaterialBLL();
        //协议合同对象
        private Agreement_Contract agreementContract = new Agreement_Contract();
        //协议物料项
        private List<Agreement_Contract_Item> itemList = new List<Agreement_Contract_Item>();
        //操作类型
        private string opType = "";
        //框架协议工具
        private OutlineAgreementTool tool = new OutlineAgreementTool();
        //是否初始化完成
        bool isInitialized = false;
        //框架协议id
        string agreementId = "";
        //创建者编号，先写死
        string createrId = "TT2017XX";
        #endregion

        #region 构造方法(有参/无参)
        public AgreementContractForm() {
            InitializeComponent();
            opType = "new";
            #region 数据初始化
            #region 合同基本数据
            //协议类型ComboBox初始化
            this.cbxAgreementType.SelectedIndex = 0;
            //默认使用寻源子项编号作为合同编号
            bindContractID();
            bindSupplier();
            bindFactory();
            #endregion
            #region 合同参考数据
            bindBuyerGroup();
            //bindBuyerOrganization();
            #endregion
            #region 合同附加信息
            bindCurency();
            bindPaymentClause();
            bindTradeClause();
            #endregion
            #endregion
            disableMutualChange();
        }

        /// <summary>
        /// 有参数构造函数
        /// </summary>
        /// <param name="contract"></param>
        public AgreementContractForm(Agreement_Contract contract, string opType) {
            InitializeComponent();

            this.agreementContract = contract;
            //填写数据
            this.opType = opType;
            if (!opType.Equals("new"))
            {
                intialForm(contract);
                disabledControls(opType);
                disableMutualChange();
            }
            else {
                //协议类型ComboBox初始化
                this.cbxAgreementType.SelectedIndex = 0;
                //货币类型ComboBox初始化
                this.cbxCurrencyType.SelectedIndex = this.cbxCurrencyType.Items.IndexOf("CNY");
            }
        }
        #endregion

        #region 初始化ComboBox
        private void bindBuyerGroup()
        {
            //采购组ComboBox初始化
            List<string> buyerGroupList = generalTool.GetAllBuyerGroup();
            if (buyerGroupList != null)
            {
                this.cbxBuyerGroup.DataSource = buyerGroupList;
            }
        }

        private void bindBuyerOrganization()
        {
            //采购组织ComboBox初始化
            List<string> buyerOrganizationList = generalTool.GetAllBuyerOrganization();
            if (buyerOrganizationList != null)
            {
                this.cbxBuyerOrgnization.DataSource = buyerOrganizationList;
            }
        }

        private void bindPaymentClause()
        {
            //付款条件ComboBox初始化
            List<string> paymentClauseList = generalTool.GetAllPaymentClauseName();
            if (paymentClauseList != null)
            {
                this.cbPaymentClause.DataSource = paymentClauseList;
            }
        }

        //根据供应商和采购组织取得对应的付款条件
        private string getPaymentClause(string supplier,string purchaseGroup)
        {
            string result;
            string sql = "SELECT Payment_Clause FROM Supplier_Purchasing_Org WHERE Supplier_ID = '" + supplier + "'" + " AND PurchasingORG_ID = '" + purchaseGroup + "'";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            if (dt == null || dt.Rows.Count == 0)
                result = null;
            else
            {
                Object value = dt.Rows[0]["Payment_Clause"];
                if (Convert.IsDBNull(value))
                    result = null;
                else
                    result = value.ToString().Trim();
            }
            return result;
        }

        //
        private string getTradeClause(string supplier, string purchaseGroup)
        {
            string result;
            string sql = "SELECT Trade_Clause FROM Supplier_Purchasing_Org WHERE Supplier_ID = '" + supplier + "'" + " AND PurchasingORG_ID = '" + purchaseGroup + "'";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            if (dt == null || dt.Rows.Count == 0)
                result = null;
            else
            {
                Object value = dt.Rows[0]["Trade_Clause"];
                if (Convert.IsDBNull(value))
                    result = null;
                else
                    result = value.ToString().Trim();
            }
            return result;
        }

        private void bindTradeClause()
        {
            //交付条件初始化
            List<string> tradeClauseList = generalTool.GetAllTradeClauseID();
            if (tradeClauseList != null)
            {
                this.cbTradeClause.DataSource = tradeClauseList;
            }
        }

        //绑定合同编号，适用场景：新建合同
        //取出的是state为0的寻源子项的编号
        private void bindContractID()
        {
            cbxContractID.SelectedIndexChanged -= new EventHandler(cbxContractID_SelectedIndexChanged);
            List<string> sourceItemIDList = sourceItemTool.getAllValidItemID();
            if(sourceItemIDList == null)
            {
                MessageUtil.ShowError("当前没有需要签订的合同！");
                return;
            }
            else
            {
                this.cbxContractID.DataSource = sourceItemIDList;
            }
            cbxContractID.SelectedIndexChanged += new EventHandler(cbxContractID_SelectedIndexChanged);
        }

        //绑定合同编号，适用场景：修改，查看，变更合同
        //取出的是state为1的寻源子项的编号
        private void bindContractIDForEdit()
        {
            cbxContractID.SelectedIndexChanged -= new EventHandler(cbxContractID_SelectedIndexChanged);
            List<string> sourceItemIDList = sourceItemTool.getAllInvalidItemID();
            if (sourceItemIDList != null)
            {
                this.cbxContractID.DataSource = sourceItemIDList;
            }
            cbxContractID.SelectedIndexChanged += new EventHandler(cbxContractID_SelectedIndexChanged);
        }

        private void bindCurency()
        {
            //货币类型ComboBox初始化
            List<string> currencyList = generalTool.GetCurrencyType();
            if (currencyList != null)
            {
                this.cbxCurrencyType.DataSource = currencyList;
                int cnyIndex = this.cbxCurrencyType.Items.IndexOf("CNY");
                if (cnyIndex != -1)
                {
                    this.cbxCurrencyType.SelectedIndex = cnyIndex;
                }
            }
        }

        private void bindSupplier()
        {
            //供应商编号ComboBox初始化
            cbxSupplierID.SelectedIndexChanged -= new EventHandler(cbxSupplierID_SelectedIndexChanged);
            List<string> supplierIDList = generalTool.GetAllSupplier();
            if (supplierIDList != null)
            {
                this.cbxSupplierID.DataSource = supplierIDList;
            }
            cbxSupplierID.SelectedIndexChanged += new EventHandler(cbxSupplierID_SelectedIndexChanged);
        }

        private void bindFactory()
        {
            //工厂ComboBox初始化
            //cbxFactory.SelectedIndexChanged -= new EventHandler(cbxFactory_SelectedIndexChanged);
            List<string> factoryIDList = generalTool.GetAllFactory();
            if (factoryIDList != null)
            {
                this.cbxFactory.DataSource = factoryIDList;
            }
            //cbxFactory.SelectedIndexChanged += new EventHandler(cbxFactory_SelectedIndexChanged);
        }

        #region 提供所有可用的付款&交付条件供用户选择
        private void bindAllPaymentClause()
        {
            //付款条件ComboBox初始化
            List<string> paymentClauseList = generalTool.GetAllPaymentClauseName();
            if (paymentClauseList != null)
            {
                this.cbPaymentClause.DataSource = paymentClauseList;
            }
        }

        private void bindAllTradeClause()
        {
            //交付条件初始化
            List<string> tradeClauseList = generalTool.GetAllTradeClauseID();
            if (tradeClauseList != null)
            {
                this.cbTradeClause.DataSource = tradeClauseList;
            }
        }
        #endregion
        #endregion

        #region 修改/查看合同时填充已有的数据
        /// <summary>
        /// 初始化form
        /// </summary>
        /// <param name="contract"></param>
        private void intialForm(Agreement_Contract contract) {
            bindCurency();
            //界面抬头
            switch (opType) {
                case "edit": this.Text = "协议合同编辑"; break;
                case "change": this.Text = "协议合同变更"; break;
                case "display": this.Text = "协议合同显示"; break;
                default: break;
            }
            //读取已存在的数据
            //合同元数据不可更改，因此直接设置Text
            //读取合同编号
           // bindContractIDForEdit();
            bindContractID();
            this.cbxContractID.Text = contract.Agreement_Contract_ID;
            //读取合同类型
            this.cbxAgreementType.SelectedIndex = this.cbxAgreementType.Items.IndexOf(contract.Agreement_Contract_Type);
            //读取供应商编号
            bindSupplier();
            this.cbxSupplierID.Text = contract.Supplier_ID;
            //读取工厂编号
            bindFactory();
            this.cbxFactory.Text = contract.Factory;
            //读取合同起止时间
            this.dtpBeginTime.Value = contract.Begin_Time;
            this.dtpEndTime.Value = contract.End_Time;
            
            //参考数据
            //读取采购组
            bindBuyerGroup();
            this.cbxBuyerGroup.Text = contract.Purchase_Group;
            //读取采购组织
            bindBuyerOrganization();
            this.cbxBuyerOrgnization.Text = contract.Purchase_Organization;
            //读取合同创建时间
            this.dtpCreateTime.Value = contract.Create_Time;
            //读取库存地
            this.cbxStorageLocation.SelectedIndex = this.cbxStorageLocation.Items.IndexOf(contract.Storage_Location);
            
            //交货和支付条件
            bindPaymentClause();
            this.cbPaymentClause.SelectedIndex = this.cbPaymentClause.Items.IndexOf(contract.Payment_Type);
           // this.tbPaymentType.Text = contract.Payment_Type;
            this.tbDays1.Text = convertNumToString(contract.Days1);
            this.tbDiscountRate1.Text = convertNumToString(contract.Discount_Rate1);
            this.tbDays2.Text = convertNumToString(contract.Days2);
            this.tbDiscountRate2.Text = convertNumToString(contract.Discount_Rate2);
            this.tbDays3.Text = convertNumToString(contract.Days3);

            this.tbTargetValue.Text = convertNumToString(contract.Target_Value);
            this.cbxCurrencyType.SelectedIndex = this.cbxCurrencyType.Items.IndexOf(
                contract.Currency_Type);
            //this.tbExchangeRate.Text = convertNumToString(contract.Exchange_Rate);
            //交付条件初始化
            bindTradeClause();
            this.tblTermText.Text = contract.IT_Term_Text;
        }
        #endregion

        #region 自定义数据转换方法
        /// <summary>
        /// 将Num转换为string
        /// </summary>
        /// <param name="num"></param>
        /// <returns></returns>
        private string convertNumToString(int num) {
            if (num == 0)
                return "";
            else
                return Convert.ToString(num);
        }
        private string convertNumToString(double num) {
            if (num == 0.0)
                return "";
            else
                return Convert.ToString(num);
        }

        /// <summary>
        /// 将string转换为double
        /// </summary>
        /// <param name="num"></param>
        /// <returns></returns>
        private double convertStringToDoubleNum(string num) {
            if (num.Equals(""))
                return 0.0;
            else
                return Convert.ToDouble(num);
        }

        private int convertStringToIntNum(string num) {
            if (num.Equals(""))
                return 0;
            else
                return Convert.ToInt32(num);
        }
        #endregion

        #region 点击下一页响应事件
        /// <summary>
        /// 点击下一步，进入项目总览界面
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnNextPage_Click(object sender, EventArgs e)
        {
            //记录改变前的合同信息
            Agreement_Contract oldAgreementContract = agreementContract;

            //读取当前信息
            Agreement_Contract newAgreementContract = new Agreement_Contract();

            newAgreementContract.Agreement_Contract_ID = this.cbxContractID.Text.Trim();
            if (opType.Equals("new"))
            {
                //初始Version为1
                newAgreementContract.Agreement_Contract_Version = "1";
                //合同初始 status 为 "待审核"
                newAgreementContract.Status = "待审核";
                //记录创建用户, 先默认"2015001"
                newAgreementContract.Creater_ID = "2015001";
            }
            else {
                newAgreementContract.Creater_ID = oldAgreementContract.Creater_ID;
                newAgreementContract.Status = "待审核";
                newAgreementContract.Agreement_Contract_Version = oldAgreementContract.Agreement_Contract_Version;
            }
            newAgreementContract.Agreement_Contract_Type = this.cbxAgreementType.Text;
            newAgreementContract.Create_Time = this.dtpCreateTime.Value;
            newAgreementContract.Supplier_ID = this.cbxSupplierID.Text.Trim();
            newAgreementContract.Begin_Time = this.dtpBeginTime.Value;
            newAgreementContract.End_Time = this.dtpEndTime.Value;

            newAgreementContract.Purchase_Group = this.cbxBuyerGroup.Text.Trim();
            newAgreementContract.Purchase_Organization = this.cbxBuyerOrgnization.Text.Trim();
            newAgreementContract.Factory = this.cbxFactory.Text.Trim();
            newAgreementContract.Storage_Location = this.cbxStorageLocation.Text.Trim();

            //还要考虑异常处理
            newAgreementContract.Payment_Type = this.cbPaymentClause.Text.Trim();
            newAgreementContract.Days1 = convertStringToIntNum(this.tbDays1.Text.Trim());
            newAgreementContract.Discount_Rate1 = convertStringToDoubleNum(this.tbDiscountRate1.Text.Trim());
            newAgreementContract.Days2 = convertStringToIntNum(this.tbDays2.Text.Trim());
            newAgreementContract.Discount_Rate2 = convertStringToDoubleNum(this.tbDiscountRate2.Text.Trim());
            newAgreementContract.Days3 = convertStringToIntNum(this.tbDays3.Text.Trim());
            newAgreementContract.Target_Value = convertStringToDoubleNum(this.tbTargetValue.Text.Trim());
            newAgreementContract.Currency_Type = this.cbxCurrencyType.Text.Trim();
            //newAgreementContract.Exchange_Rate = convertStringToDoubleNum(this.tbExchangeRate.Text.Trim());
            newAgreementContract.IT_Term_Code = this.cbTradeClause.Text.Trim();
            newAgreementContract.IT_Term_Text = this.tblTermText.Text.Trim();

            //有效时间区间是否合法
            if (newAgreementContract.Begin_Time > newAgreementContract.End_Time)
            {
                MessageBox.Show("请正确填写时间", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            //必填项不能为空
            if (newAgreementContract.Agreement_Contract_ID.Equals("")
                || newAgreementContract.Supplier_ID.Equals("")
                || newAgreementContract.Purchase_Group.Equals("")
                || newAgreementContract.Purchase_Organization.Equals("")
                || newAgreementContract.Factory.Equals("")
                || newAgreementContract.IT_Term_Code.Equals("")
                || newAgreementContract.Payment_Type.Equals(""))
            {

                    MessageBox.Show("请填写完整", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
            }
            //价值合同必须填写 目标值
            if (newAgreementContract.Agreement_Contract_Type.Equals("价值合同")
                && newAgreementContract.Target_Value == 0.0
                && newAgreementContract.Currency_Type.Equals(""))
            {
                    MessageBox.Show("请填写目标值", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
            }

            //如果是"new"，查看当前合同ID是否已存在
            if (opType.Equals("new")) {
                Agreement_Contract searchResult = agreementContractTool.getAgreementContractByID(newAgreementContract.Agreement_Contract_ID,
                newAgreementContract.Agreement_Contract_Version);
                if (searchResult != null)
                {
                    MessageBox.Show("该ID合同已存在", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }

            //隐藏前一个界面
            this.Hide();
            
            //进入项目总览界面
            AgreementMaterialOverviewForm agreementMaterialForm;
            if (opType.Equals("change"))
            {
                string oldV = newAgreementContract.Agreement_Contract_Version;
                newAgreementContract.Agreement_Contract_Version = (Convert.ToInt32(oldV) + 1).ToString();
                agreementMaterialForm = new AgreementMaterialOverviewForm(oldAgreementContract, newAgreementContract,
                    opType, this);
            }
            else {
                agreementMaterialForm = new AgreementMaterialOverviewForm(newAgreementContract, opType, this);
            }

            UserUI userUI = SingletonUserUI.getUserUI();
            userUI.displayOnDockPanel(agreementMaterialForm);
        }
        #endregion

        #region 部分禁用
        /// <summary>
        /// 禁止手动改变付款条件
        /// 付款条件只能通过选取付款码来改变
        /// </summary>
        private void disableMutualChange()
        {
            this.tbDays1.Enabled = false;
            this.tbDays2.Enabled = false;
            this.tbDays3.Enabled = false;
            this.tbDiscountRate1.Enabled = false;
            this.tbDiscountRate2.Enabled = false;
            this.tblTermText.Enabled = false;
        }

        /// <summary>
        /// 根据操作类型，将部分控件设为禁止修改
        /// </summary>
        /// <param name="opType"></param>
        private void disabledControls(string opType)
        {
            //根据操作：修改(edit)、变更(change)、展示(display)，分别 disabled 不同的控件
            this.cbxContractID.Enabled = false;
            this.cbxAgreementType.Enabled = false;
            this.dtpCreateTime.Enabled = false;
            this.cbxSupplierID.Enabled = false;

            # region 部分选择禁用

            if (opType.Equals("change"))
            {
                this.cbxBuyerGroup.Enabled = false;
                this.cbxBuyerOrgnization.Enabled = false;
                this.cbxFactory.Enabled = false;
                this.cbPaymentClause.Enabled = false;
                this.tbDays1.Enabled = false;
                this.tbDiscountRate1.Enabled = false;
                this.tbDays2.Enabled = false;
                this.tbDiscountRate2.Enabled = false;
                this.tbDays3.Enabled = false;

                this.cbxCurrencyType.Enabled = false;
                //this.tbExchangeRate.Enabled = false;
                this.cbTradeClause.Enabled = false;
                this.tblTermText.Enabled = false;

                this.dtpBeginTime.Enabled = false;
                this.dtpEndTime.Enabled = false;
                //this.tbTargetValue.Enabled = false;
                this.cbxStorageLocation.Enabled = false;
            }
            else if (opType.Equals("display"))
            {
                this.cbxBuyerGroup.Enabled = false;
                this.cbxBuyerOrgnization.Enabled = false;
                this.cbxFactory.Enabled = false;
                this.cbPaymentClause.Enabled = false;
                this.tbDays1.Enabled = false;
                this.tbDiscountRate1.Enabled = false;
                this.tbDays2.Enabled = false;
                this.tbDiscountRate2.Enabled = false;
                this.tbDays3.Enabled = false;

                this.cbxCurrencyType.Enabled = false;
                //this.tbExchangeRate.Enabled = false;
                this.cbTradeClause.Enabled = false;
                this.tblTermText.Enabled = false;

                this.dtpBeginTime.Enabled = false;
                this.dtpEndTime.Enabled = false;
                this.tbTargetValue.Enabled = false;
                this.cbxStorageLocation.Enabled = false;
            }

            # endregion
        }
        #endregion

        #region 保留响应事件
        private void AgreementContractForm_Load(object sender, EventArgs e)
        {

        }

        private void groupBox3_Enter(object sender, EventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }
        #endregion

        #region 合同元数据响应事件

        private void cbxContractID_SelectedIndexChanged(object sender, EventArgs e)
        {
            string itemID = this.cbxContractID.Text.Trim();
            SourceItem sourceItem = sourceItemTool.getSourceItemByID(itemID);
            //bindSupplier();
            //bindFactory();
            this.cbxSupplierID.SelectedIndex = this.cbxSupplierID.Items.IndexOf(sourceItem.SupplierID);
            this.cbxFactory.SelectedIndex = this.cbxFactory.Items.IndexOf(sourceItem.FactoryID);
        }

        private void cbxAgreementType_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// 根据供应商编号绑定采购组织数据源
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cbxSupplierID_SelectedIndexChanged(object sender, EventArgs e)
        {
                string supplierID = cbxSupplierID.Text.Trim();
                //
                //绑定采购组织数据源
                string sql = "SELECT DISTINCT(PurchasingORG_ID) FROM [MMBackup].[dbo].[Supplier_Purchasing_Org] WHERE Supplier_ID = '" + supplierID + "'";
                DataTable dt = DBHelper.ExecuteQueryDT(sql);
                List<string> buyerOrgList = new List<string>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (Convert.IsDBNull(dt.Rows[i][0]))
                        continue;
                    string buyerOrg = dt.Rows[i][0].ToString();
                    if (!buyerOrg.Trim().Equals(""))
                        buyerOrgList.Add(buyerOrg);
                }
                if (buyerOrgList.Count != 0)
                    this.cbxBuyerOrgnization.DataSource = buyerOrgList;
                else
                {
                    MessageUtil.ShowError("请在主数据中维护供应商采购组织视图！");
                    this.cbxBuyerOrgnization.DataSource = null;
                }
        }

        /// <summary>
        /// 根据工厂编号绑定库存地数据源
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cbxFactory_SelectedIndexChanged(object sender, EventArgs e)
        {
            string factory = this.cbxFactory.Text;
            if (!factory.Equals(""))
            {
                List<string> storageLocationList = generalTool.GetAllStock(factory);
                this.cbxStorageLocation.DataSource = storageLocationList;
            }
            else
            {
                this.cbxStorageLocation.DataSource = null;
            }
        }
        #endregion

        #region 合同参考数据响应事件
        private void cbxBuyerGroup_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void cbxBuyerOrgnization_SelectedIndexChanged(object sender, EventArgs e)
        {
            string supplierID = cbxSupplierID.Text.Trim();
            string purchaseOrgnization = cbxBuyerOrgnization.Text.Trim();
            if (purchaseOrgnization != null && !purchaseOrgnization.Equals(""))
            {
                string paymentClause = getPaymentClause(supplierID, purchaseOrgnization);
                string tradeClause = getTradeClause(supplierID, purchaseOrgnization);
                if (paymentClause != null && !paymentClause.Equals(""))
                    cbPaymentClause.Text = paymentClause;
                if (tradeClause != null && !tradeClause.Equals(""))
                    cbTradeClause.Text = tradeClause;
            }
        }
        #endregion

        #region 付款和交付条件响应事件
        /// <summary>
        /// 付款条件改变
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cbPaymentClause_SelectedIndexChanged(object sender, EventArgs e)
        {
            string paymentTypeID = this.cbPaymentClause.Text.Trim();
            if (!paymentTypeID.Equals(""))
            {
                //从主数据表中读取付款条件
                generalTool = new GeneralBLL();
                PaymentClause info = generalTool.GetClause(paymentTypeID);
                if (info != null)
                {
                    this.tbDays1.Text = info.First_Date.ToString();
                    this.tbDiscountRate1.Text = info.First_Discount.ToString();
                    this.tbDays2.Text = info.Second_Date.ToString();
                    this.tbDiscountRate2.Text = info.Second_Discount.ToString();
                    this.tbDays3.Text = info.Final_Date.ToString();
                }
                else
                {
                    MessageUtil.ShowError("付款条件不存在！");
                }
            }
            else
            {
                this.tbDays1.Text = "";
                this.tbDiscountRate1.Text = "";
                this.tbDays2.Text = "";
                this.tbDiscountRate2.Text = "";
                this.tbDays3.Text = "";
            }
        }

        /// <summary>
        /// 交付条件改变
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cbTradeClause_SelectedIndexChanged(object sender, EventArgs e)
        {
            string tradClauseID = this.cbTradeClause.Text.Trim();
            if (!tradClauseID.Equals(""))
            {
                DataTable dt = generalTool.GetTradeClauseByID(tradClauseID);
                if (dt != null)
                {
                    string tradeClauseName = Convert.IsDBNull(dt.Rows[0]["TradeClause_Name"]) ? "" : dt.Rows[0]["TradeClause_Name"].ToString();
                    this.tblTermText.Text = tradeClauseName;
                }
                else
                {
                    MessageUtil.ShowError("交付条件不存在！");
                }
            }
            else
            {
                this.tblTermText.Text = "";
            }
        }
        #endregion



    }
}
