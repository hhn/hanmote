﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Model.ContractManage.OutlineAgreement;
using Lib.SqlServerDAL.ContractManageDAL;
using WeifenLuo.WinFormsUI.Docking;
using Lib.Bll.ContractManageBLL.OutlineAgreementBLL;
using Lib.Common.CommonUtils;

namespace MMClient.ContractManage.OutlineAgreement
{
    public partial class AgreementContractManageForm : DockContent
    {
        //记录查询结果
        //协议合同
        List<Agreement_Contract> agreementContractList = new List<Agreement_Contract>();
        //查询工具
        AgreementContractBLL agreementContractTool = new AgreementContractBLL();

        public AgreementContractManageForm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// form窗体加载
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AgreementContractManageForm_Load(object sender, EventArgs e)
        {
            //协议类型
            this.cbxAgreementContractType.SelectedIndex = 0;
            //合同状态
            this.cbxStatus.SelectedIndex = 0;
        }

        /// <summary>
        /// 点击搜索按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSearch_Click(object sender, EventArgs e)
        {
            //从所有的合同信息中搜索满足当前条件的合同信息
            //先看开始时间和截止时间
            DateTime _beginTime = this.dtpBeginTime.Value;
            DateTime _endTime = this.dtpEndTime.Value;

            //真正的起始时间设置为当天的0点0分0秒
            //真正的截止时间设置为当天的23点59分59秒
            DateTime beginTime = new DateTime(_beginTime.Year,_beginTime.Month,_beginTime.Day,0,0,0);
            DateTime endTime = new DateTime(_endTime.Year,_endTime.Month,_endTime.Day,23,59,59);

            if (beginTime > endTime)
            {
                MessageUtil.ShowError("请选择正确的起止日期");
                return;
            }

            string contractID = this.tbAgreementContractID.Text.Trim();
            string supplierID = this.tbSupplierID.Text.Trim();
            string contractType = this.cbxAgreementContractType.SelectedItem.ToString();
            if (contractType.Equals("全部"))
                contractType = "";
            string contractStatus = this.cbxStatus.SelectedItem.ToString();
            if (contractStatus.Equals("全部"))
                contractStatus = "";

            Dictionary<string, object> conditions = new Dictionary<string, object>();
            conditions.Add("Agreement_Contract_ID", contractID);
            conditions.Add("Supplier_ID", supplierID);
            conditions.Add("Agreement_Contract_Type", contractType);
            conditions.Add("Status", contractStatus);

            agreementContractList = agreementContractTool.getAgreementContractByCondition(
                conditions, beginTime, endTime);

            displayContractList(agreementContractList);
        }

        /// <summary>
        /// 展示查询结果
        /// </summary>
        /// <param name="contractList"></param>
        private void displayContractList(List<Agreement_Contract> contractList) { 
            //clear
            this.dgvAgreementContract.Rows.Clear();
            int i = 0;
            foreach (Agreement_Contract contract in contractList) {
                this.dgvAgreementContract.Rows.Add();
                DataGridViewRow currentRow = this.dgvAgreementContract.Rows[i];
                currentRow.Cells[1].Value = contract.Agreement_Contract_ID;
                currentRow.Cells[2].Value = contract.Agreement_Contract_Version;
                currentRow.Cells[3].Value = contract.Agreement_Contract_Type;
                currentRow.Cells[4].Value = contract.Create_Time;
                currentRow.Cells[5].Value = contract.Creater_ID;
                currentRow.Cells[6].Value = contract.Supplier_ID;
                currentRow.Cells[7].Value = contract.Status;

                i++;
            }
        }

        /// <summary>
        /// 删除选中的协议合同
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tsbDelete_Click(object sender, EventArgs e)
        {
            List<string> contractIDList = new List<string>();
            List<string> contractVersionList = new List<string>();
            foreach (DataGridViewRow row in this.dgvAgreementContract.Rows) {
                string isSelected = row.Cells[0].EditedFormattedValue.ToString();
                if (isSelected.Equals("True")) {
                    contractIDList.Add(row.Cells[1].Value.ToString());
                    contractVersionList.Add(row.Cells[2].Value.ToString());
                }
            }

            if (contractIDList.Count == 0) {
                MessageUtil.ShowError("请选择要删除的协议合同");
                return;                
            }

            var confirmResult = MessageUtil.ShowYesNoAndWarning("确定删除选定的协议合同?");
            if (confirmResult == DialogResult.No) {
                return;
            }

            int deleteResult = agreementContractTool.deleteAgreementContractList(contractIDList, contractVersionList);

            if (deleteResult > 0)
            {
                MessageBox.Show("删除成功");
            }
            else {
                MessageBox.Show("删除失败");
            }

            this.btnSearch.PerformClick();
        }

        /// <summary>
        /// 显示选中的合同
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tsbPreview_Click(object sender, EventArgs e)
        {
            
            List<string> contractIDList = new List<string>();
            List<string> contractVersionList = new List<string>();
            foreach (DataGridViewRow row in this.dgvAgreementContract.Rows)
            {
                string isSelected = row.Cells[0].EditedFormattedValue.ToString();
                if (isSelected.Equals("True"))
                {
                    contractIDList.Add(row.Cells[1].Value.ToString());
                    contractVersionList.Add(row.Cells[2].Value.ToString());
                }
            }

            if (contractIDList.Count == 0) {
                MessageUtil.ShowError("请选择要查看的协议合同");
                return;
            }
            if (contractIDList.Count > 1)
            {
                MessageUtil.ShowError("不要同时选个多个协议合同");
                return;
            }

            string contractID = contractIDList[0];
            string contractVersion = contractVersionList[0];
            Agreement_Contract contract = null;
            foreach(Agreement_Contract con in agreementContractList){
                if(con.Agreement_Contract_ID.Equals(contractID) && con.Agreement_Contract_Version.Equals(contractVersion)){
                    contract = con;
                    break;
                }
            }
            if(contract != null){
                //显示预览的合同
                SForm displayForm = new SForm(contract, "view");
                UserUI userUI = SingletonUserUI.getUserUI();
                userUI.displayOnDockPanel(displayForm);
            }
            
        }

        /// <summary>
        /// 执行编辑操作
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tsbContractEdit_Click(object sender, EventArgs e)
        {
            /*
            List<string> contractIDList = new List<string>();
            List<int> contractVersionList = new List<int>();
            List<string> statusList = new List<string>();
            foreach (DataGridViewRow curRow in this.dgvAgreementContract.Rows)
            {
                string isSelected = curRow.Cells[0].EditedFormattedValue.ToString();
                if (isSelected.Equals("True"))
                {
                    contractIDList.Add(curRow.Cells[1].Value.ToString());
                    contractVersionList.Add(Convert.ToInt32(curRow.Cells[2].Value.ToString()));
                    statusList.Add(curRow.Cells[7].Value.ToString());
                }
            }

            if (contractIDList.Count == 0)
            {
                MessageUtil.ShowError("请选择要编辑的合同");
                return;
            }
            if (contractIDList.Count > 1)
            {
                MessageUtil.ShowError("不要同时选择多个合同");
                return;
            }

            string contractID = contractIDList[0];
            int contractVersion = contractVersionList[0];
            string status = statusList[0];

            if (!(status.Equals("待审核") || status.Equals("待供应商确认")))
            {
                MessageUtil.ShowError("只有待审核和待确认的协议合同才能被修改");
                return;
            }

            Agreement_Contract contract = null;
            foreach (Agreement_Contract con in agreementContractList)
            {
                if (con.Agreement_Contract_ID.Equals(contractID) && con.Agreement_Contract_Version == contractVersion)
                {
                    contract = con;
                    break;
                }
            }
            if (contract != null)
            {
                //显示预览的合同
                AgreementContractForm displayForm = new AgreementContractForm(contract, "edit");
                UserUI userUI = SingletonUserUI.getUserUI();
                userUI.displayOnDockPanel(displayForm);
            }
             * */
            MessageUtil.ShowTips("不允许修改框架协议！请与管理人员联系！");
        }

        /// <summary>
        /// 执行变更操作
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tsbContractChange_Click(object sender, EventArgs e)
        {
            /*
            List<string> contractIDList = new List<string>();
            List<string> contractVersionList = new List<string>();
            List<string> statusList = new List<string>();
            foreach (DataGridViewRow curRow in this.dgvAgreementContract.Rows)
            {
                string isSelected = curRow.Cells[0].EditedFormattedValue.ToString();
                if (isSelected.Equals("True"))
                {
                    contractIDList.Add(curRow.Cells[1].Value.ToString());
                    contractVersionList.Add(curRow.Cells[2].Value.ToString());
                    statusList.Add(curRow.Cells[7].Value.ToString());
                }
            }

            if (contractIDList.Count == 0)
            {
                MessageUtil.ShowError("请选择要变更的合同");
                return;
            }
            if (contractIDList.Count > 1)
            {
                MessageUtil.ShowError("不要同时选择多个合同");
                return;
            }

            //获得被选中信息
            string agreementContractID = contractIDList[0];
            string agreementContractVersion = contractVersionList[0];
            string status = statusList[0];

            if (!status.Equals("执行中")) {
                MessageUtil.ShowError("只有执行中的协议合同才能进行变更操作");
                return;
            }

            var result = MessageUtil.ShowOKCancelAndQuestion("确认变更协议合同？");
            if (result == DialogResult.Cancel) {
                return;
            }

            //先冻结合同
            Agreement_Contract targetContract = null;
            foreach (Agreement_Contract contract in agreementContractList) {
                if (contract.Agreement_Contract_ID.Equals(agreementContractID)
                    && contract.Agreement_Contract_Version.Equals(agreementContractVersion)) {

                        targetContract = contract;
                        break;
                }
            }
            if (targetContract != null) {
                UserUI userUI = SingletonUserUI.getUserUI();
                AgreementContractForm form = new AgreementContractForm(targetContract, "change");
                userUI.displayOnDockPanel(form);
            }
             * */
            MessageUtil.ShowTips("不允许变更合同，请与管理人员联系！");
        }

        /// <summary>
        /// 审核
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tsbCheck_Click(object sender, EventArgs e)
        {
            List<Agreement_Contract> contractList = new List<Agreement_Contract>();

            //agreementContractList中保存的记录顺序和显示的记录顺序应该是一致的
            int i = 0;
            foreach (DataGridViewRow row in this.dgvAgreementContract.Rows)
            {
                string isSelected = row.Cells[0].EditedFormattedValue.ToString();
                if (isSelected.Equals("True"))
                {
                    string status = row.Cells[7].Value.ToString();
                    if (!status.Equals("待审核")) {
                        MessageUtil.ShowError("只有待审核的协议合同才能进行审核操作");
                        return;
                    }
                    contractList.Add(agreementContractList[i]);
                }

                i++;
            }

            if (contractList.Count == 0)
            {
                MessageUtil.ShowError("请选择要审核的协议合同");
                return;
            }

            //确认
            var result = MessageUtil.ShowOKCancelAndQuestion("确认审核协议合同？");
            if (result == DialogResult.Cancel)
            {
                return;
            }

            //审核所有的合同
            int num = 0;
            foreach (Agreement_Contract contract in contractList) {
                contract.Status = "待供应商确认";
                num += agreementContractTool.updateAgreementContract(contract.Agreement_Contract_ID,
                    contract.Agreement_Contract_Version, contract);
            }
            if (num > 0)
            {
                MessageBox.Show("审核成功");
            }
            else {
                MessageBox.Show("审核失败");
            }

            this.btnSearch.PerformClick();
        }

        /// <summary>
        /// 点击监控
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tsbMonitorSetting_Click(object sender, EventArgs e)
        {
            List<string> contractIDList = new List<string>();
            List<string> contractVersionList = new List<string>();
            List<string> statusList = new List<string>();
            foreach (DataGridViewRow row in this.dgvAgreementContract.Rows)
            {
                string isSelected = row.Cells[0].EditedFormattedValue.ToString();
                if (isSelected.Equals("True"))
                {
                    contractIDList.Add(row.Cells[1].Value.ToString());
                    contractVersionList.Add(row.Cells[2].Value.ToString());
                    statusList.Add(row.Cells[7].Value.ToString());
                }
            }

            if (contractIDList.Count == 0)
            {
                MessageUtil.ShowError("请选择要进行监控的协议合同");
                return;
            }
            if (contractIDList.Count > 1)
            {
                MessageUtil.ShowError("不要同时选择多个合同");
                return;
            }

            string agreementContractID = contractIDList[0];
            string agreementContractVersion = contractVersionList[0];
            Agreement_Contract contract = null;
            foreach (Agreement_Contract tmp in agreementContractList) {
                if (tmp.Agreement_Contract_ID.Equals(agreementContractID)
                    && tmp.Agreement_Contract_Version.Equals(agreementContractVersion)) {
                        
                    contract = tmp;
                    break;
                }
            }

           // AgreementContractMonitorSettingForm form =
           //      new AgreementContractMonitorSettingForm(contract);
            ContractMonitor form = new ContractMonitor(contract);
            SingletonUserUI.addToUserUI(form);
        }

        /// <summary>
        /// 点击新建
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tsbNew_Click(object sender, EventArgs e)
        {
            SForm agreementForm = new SForm();
            UserUI userUI = SingletonUserUI.getUserUI();
            userUI.displayOnDockPanel(agreementForm);
        }

        /// <summary>
        /// 重载方法，防止在子控件获得焦点时父控件的滚动条自动滚动
        /// 本方法不需要手动调用
        /// </summary>
        /// <param name="activeControl">子控件</param>
        /// <returns>位置</returns>
        protected override Point ScrollToControl(Control activeControl)
        {
            return this.AutoScrollPosition;
        }

        private void tsbRefresh_Click(object sender, EventArgs e)
        {
            this.btnSearch.PerformClick();
        }

        private void dgvAgreementContract_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
