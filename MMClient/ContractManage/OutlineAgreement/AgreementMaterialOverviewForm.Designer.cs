﻿namespace MMClient.ContractManage.OutlineAgreement
{
    partial class AgreementMaterialOverviewForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tbCurrencyType = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.tbSupplierID = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.dtpCreateTime = new System.Windows.Forms.DateTimePicker();
            this.tbAgreementType = new System.Windows.Forms.TextBox();
            this.tbAgreementID = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.dgvAgreementItems = new System.Windows.Forms.DataGridView();
            this.Item_Num = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.I = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.A = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Material_ID = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Short_Text = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Target_Value = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OUn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Net_Price = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Every = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OPU = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Material_Group = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Factory = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.dataGridViewComboBoxColumn1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn2 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAgreementItems)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tbCurrencyType);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.tbSupplierID);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.dtpCreateTime);
            this.groupBox1.Controls.Add(this.tbAgreementType);
            this.groupBox1.Controls.Add(this.tbAgreementID);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(9, 5);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(811, 107);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "基本数据";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // tbCurrencyType
            // 
            this.tbCurrencyType.Location = new System.Drawing.Point(621, 70);
            this.tbCurrencyType.Name = "tbCurrencyType";
            this.tbCurrencyType.ReadOnly = true;
            this.tbCurrencyType.Size = new System.Drawing.Size(109, 21);
            this.tbCurrencyType.TabIndex = 10;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(545, 73);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(65, 12);
            this.label6.TabIndex = 9;
            this.label6.Text = "货币类型：";
            // 
            // tbSupplierID
            // 
            this.tbSupplierID.Location = new System.Drawing.Point(79, 70);
            this.tbSupplierID.Name = "tbSupplierID";
            this.tbSupplierID.ReadOnly = true;
            this.tbSupplierID.Size = new System.Drawing.Size(100, 21);
            this.tbSupplierID.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(20, 73);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 12);
            this.label4.TabIndex = 6;
            this.label4.Text = "供应商：";
            // 
            // dtpCreateTime
            // 
            this.dtpCreateTime.Enabled = false;
            this.dtpCreateTime.Location = new System.Drawing.Point(621, 25);
            this.dtpCreateTime.Name = "dtpCreateTime";
            this.dtpCreateTime.Size = new System.Drawing.Size(109, 21);
            this.dtpCreateTime.TabIndex = 5;
            // 
            // tbAgreementType
            // 
            this.tbAgreementType.Location = new System.Drawing.Point(347, 28);
            this.tbAgreementType.Name = "tbAgreementType";
            this.tbAgreementType.ReadOnly = true;
            this.tbAgreementType.Size = new System.Drawing.Size(100, 21);
            this.tbAgreementType.TabIndex = 4;
            // 
            // tbAgreementID
            // 
            this.tbAgreementID.Location = new System.Drawing.Point(79, 28);
            this.tbAgreementID.Name = "tbAgreementID";
            this.tbAgreementID.ReadOnly = true;
            this.tbAgreementID.Size = new System.Drawing.Size(100, 21);
            this.tbAgreementID.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(545, 31);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 12);
            this.label3.TabIndex = 2;
            this.label3.Text = "协议日期：";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(276, 31);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 12);
            this.label2.TabIndex = 1;
            this.label2.Text = "协议类型：";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "协议ID：";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 133);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(65, 12);
            this.label7.TabIndex = 1;
            this.label7.Text = "框架协议项";
            // 
            // dgvAgreementItems
            // 
            this.dgvAgreementItems.AllowUserToAddRows = false;
            this.dgvAgreementItems.AllowUserToResizeRows = false;
            this.dgvAgreementItems.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvAgreementItems.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvAgreementItems.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Item_Num,
            this.I,
            this.A,
            this.Material_ID,
            this.Short_Text,
            this.Target_Value,
            this.OUn,
            this.Net_Price,
            this.Every,
            this.OPU,
            this.Material_Group,
            this.Factory});
            this.dgvAgreementItems.Location = new System.Drawing.Point(9, 164);
            this.dgvAgreementItems.MultiSelect = false;
            this.dgvAgreementItems.Name = "dgvAgreementItems";
            this.dgvAgreementItems.RowHeadersVisible = false;
            this.dgvAgreementItems.RowTemplate.Height = 23;
            this.dgvAgreementItems.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgvAgreementItems.Size = new System.Drawing.Size(811, 711);
            this.dgvAgreementItems.TabIndex = 2;
            this.dgvAgreementItems.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvAgreementItems_CellValueChanged);
            this.dgvAgreementItems.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgvAgreementItems_EditingControlShowing);
            // 
            // Item_Num
            // 
            this.Item_Num.HeaderText = "项目";
            this.Item_Num.Name = "Item_Num";
            // 
            // I
            // 
            this.I.HeaderText = "I";
            this.I.Name = "I";
            // 
            // A
            // 
            this.A.HeaderText = "A";
            this.A.Name = "A";
            // 
            // Material_ID
            // 
            this.Material_ID.HeaderText = "物料编号";
            this.Material_ID.Name = "Material_ID";
            this.Material_ID.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Material_ID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // Short_Text
            // 
            this.Short_Text.HeaderText = "短文本";
            this.Short_Text.Name = "Short_Text";
            // 
            // Target_Value
            // 
            this.Target_Value.HeaderText = "目标数量";
            this.Target_Value.Name = "Target_Value";
            // 
            // OUn
            // 
            this.OUn.HeaderText = "OUn";
            this.OUn.Name = "OUn";
            // 
            // Net_Price
            // 
            this.Net_Price.HeaderText = "净价";
            this.Net_Price.Name = "Net_Price";
            // 
            // Every
            // 
            this.Every.HeaderText = "每";
            this.Every.Name = "Every";
            // 
            // OPU
            // 
            this.OPU.HeaderText = "OPU";
            this.OPU.Name = "OPU";
            // 
            // Material_Group
            // 
            this.Material_Group.HeaderText = "物料组";
            this.Material_Group.Name = "Material_Group";
            this.Material_Group.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // Factory
            // 
            this.Factory.HeaderText = "工厂";
            this.Factory.Name = "Factory";
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(742, 128);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 3;
            this.btnSave.Text = "保存";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(133, 128);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(75, 23);
            this.btnAdd.TabIndex = 4;
            this.btnAdd.Text = "增加";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(248, 128);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(75, 23);
            this.btnDelete.TabIndex = 5;
            this.btnDelete.Text = "删除";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // dataGridViewComboBoxColumn1
            // 
            this.dataGridViewComboBoxColumn1.HeaderText = "物料编号";
            this.dataGridViewComboBoxColumn1.Name = "dataGridViewComboBoxColumn1";
            this.dataGridViewComboBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewComboBoxColumn1.Width = 67;
            // 
            // dataGridViewComboBoxColumn2
            // 
            this.dataGridViewComboBoxColumn2.HeaderText = "物料组";
            this.dataGridViewComboBoxColumn2.Name = "dataGridViewComboBoxColumn2";
            this.dataGridViewComboBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewComboBoxColumn2.Width = 68;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(544, 128);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 6;
            this.button1.Text = "导出";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // AgreementMaterialOverviewForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(839, 743);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.dgvAgreementItems);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "AgreementMaterialOverviewForm";
            this.Text = "协议合同项目总览";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.AgreementMaterialOverviewForm_FormClosing);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAgreementItems)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbCurrencyType;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tbSupplierID;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker dtpCreateTime;
        private System.Windows.Forms.TextBox tbAgreementType;
        private System.Windows.Forms.TextBox tbAgreementID;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DataGridView dgvAgreementItems;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.DataGridViewTextBoxColumn Item_Num;
        private System.Windows.Forms.DataGridViewTextBoxColumn I;
        private System.Windows.Forms.DataGridViewTextBoxColumn A;
        private System.Windows.Forms.DataGridViewComboBoxColumn Material_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Short_Text;
        private System.Windows.Forms.DataGridViewTextBoxColumn Target_Value;
        private System.Windows.Forms.DataGridViewTextBoxColumn OUn;
        private System.Windows.Forms.DataGridViewTextBoxColumn Net_Price;
        private System.Windows.Forms.DataGridViewTextBoxColumn Every;
        private System.Windows.Forms.DataGridViewTextBoxColumn OPU;
        private System.Windows.Forms.DataGridViewTextBoxColumn Material_Group;
        private System.Windows.Forms.DataGridViewTextBoxColumn Factory;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn1;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn2;
        private System.Windows.Forms.Button button1;
    }
}