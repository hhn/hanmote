﻿namespace MMClient.ContractManage.OutlineAgreement
{
    partial class SchedulingAgreementForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cbxBuyerOrganization = new System.Windows.Forms.ComboBox();
            this.cbxBuyerGroup = new System.Windows.Forms.ComboBox();
            this.cbxStorageLocation = new System.Windows.Forms.ComboBox();
            this.cbxFactory = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.cbxPaymentClause = new System.Windows.Forms.ComboBox();
            this.cbxCurrencyType = new System.Windows.Forms.ComboBox();
            this.label27 = new System.Windows.Forms.Label();
            this.tbITTermText = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.tbTargetValue = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.tbDays3 = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.tbDiscountRate2 = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.tbDays2 = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.tbDiscountRate1 = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.tbDays1 = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.cbxSupplierID = new System.Windows.Forms.ComboBox();
            this.tbAgreementID = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.dtpEndTime = new System.Windows.Forms.DateTimePicker();
            this.dtpBeginTime = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.dtpCreateTime = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.cbxAgreementType = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.btnNext = new System.Windows.Forms.Button();
            this.cbTradeClause = new System.Windows.Forms.ComboBox();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // supplierInforGroupBox
            // 
            this.groupBox1.Controls.Add(this.cbxBuyerOrganization);
            this.groupBox1.Controls.Add(this.cbxBuyerGroup);
            this.groupBox1.Controls.Add(this.cbxStorageLocation);
            this.groupBox1.Controls.Add(this.cbxFactory);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Location = new System.Drawing.Point(4, 125);
            this.groupBox1.Name = "supplierInforGroupBox";
            this.groupBox1.Size = new System.Drawing.Size(769, 132);
            this.groupBox1.TabIndex = 12;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "参考数据";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // cbxBuyerOrganization
            // 
            this.cbxBuyerOrganization.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxBuyerOrganization.FormattingEnabled = true;
            this.cbxBuyerOrganization.Location = new System.Drawing.Point(391, 31);
            this.cbxBuyerOrganization.Name = "cbxBuyerOrganization";
            this.cbxBuyerOrganization.Size = new System.Drawing.Size(110, 20);
            this.cbxBuyerOrganization.TabIndex = 27;
            // 
            // cbxBuyerGroup
            // 
            this.cbxBuyerGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxBuyerGroup.FormattingEnabled = true;
            this.cbxBuyerGroup.Location = new System.Drawing.Point(129, 31);
            this.cbxBuyerGroup.Name = "cbxBuyerGroup";
            this.cbxBuyerGroup.Size = new System.Drawing.Size(113, 20);
            this.cbxBuyerGroup.TabIndex = 26;
            this.cbxBuyerGroup.SelectedIndexChanged += new System.EventHandler(this.cbxBuyerGroup_SelectedIndexChanged);
            // 
            // cbxStorageLocation
            // 
            this.cbxStorageLocation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxStorageLocation.FormattingEnabled = true;
            this.cbxStorageLocation.Location = new System.Drawing.Point(130, 69);
            this.cbxStorageLocation.Name = "cbxStorageLocation";
            this.cbxStorageLocation.Size = new System.Drawing.Size(114, 20);
            this.cbxStorageLocation.TabIndex = 25;
            // 
            // cbxFactory
            // 
            this.cbxFactory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxFactory.FormattingEnabled = true;
            this.cbxFactory.Location = new System.Drawing.Point(614, 28);
            this.cbxFactory.Name = "cbxFactory";
            this.cbxFactory.Size = new System.Drawing.Size(114, 20);
            this.cbxFactory.TabIndex = 24;
            this.cbxFactory.SelectedIndexChanged += new System.EventHandler(this.cbxFactory_SelectedIndexChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(304, 31);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(65, 12);
            this.label7.TabIndex = 21;
            this.label7.Text = "采购组织：";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(63, 31);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(53, 12);
            this.label8.TabIndex = 20;
            this.label8.Text = "采购组：";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(61, 72);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(53, 12);
            this.label14.TabIndex = 17;
            this.label14.Text = "库存地：";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(547, 31);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(41, 12);
            this.label13.TabIndex = 16;
            this.label13.Text = "工厂：";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.cbTradeClause);
            this.groupBox3.Controls.Add(this.cbxPaymentClause);
            this.groupBox3.Controls.Add(this.cbxCurrencyType);
            this.groupBox3.Controls.Add(this.label27);
            this.groupBox3.Controls.Add(this.tbITTermText);
            this.groupBox3.Controls.Add(this.label26);
            this.groupBox3.Controls.Add(this.tbTargetValue);
            this.groupBox3.Controls.Add(this.label24);
            this.groupBox3.Controls.Add(this.label23);
            this.groupBox3.Controls.Add(this.label21);
            this.groupBox3.Controls.Add(this.tbDays3);
            this.groupBox3.Controls.Add(this.label22);
            this.groupBox3.Controls.Add(this.label18);
            this.groupBox3.Controls.Add(this.tbDiscountRate2);
            this.groupBox3.Controls.Add(this.label19);
            this.groupBox3.Controls.Add(this.tbDays2);
            this.groupBox3.Controls.Add(this.label20);
            this.groupBox3.Controls.Add(this.label17);
            this.groupBox3.Controls.Add(this.tbDiscountRate1);
            this.groupBox3.Controls.Add(this.label16);
            this.groupBox3.Controls.Add(this.tbDays1);
            this.groupBox3.Controls.Add(this.label15);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Location = new System.Drawing.Point(4, 263);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(769, 200);
            this.groupBox3.TabIndex = 14;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "参考数据";
            // 
            // cbxPaymentClause
            // 
            this.cbxPaymentClause.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxPaymentClause.FormattingEnabled = true;
            this.cbxPaymentClause.Location = new System.Drawing.Point(130, 32);
            this.cbxPaymentClause.Name = "cbxPaymentClause";
            this.cbxPaymentClause.Size = new System.Drawing.Size(114, 20);
            this.cbxPaymentClause.TabIndex = 49;
            this.cbxPaymentClause.SelectedIndexChanged += new System.EventHandler(this.cbxPaymentClause_SelectedIndexChanged);
            // 
            // cbxCurrencyType
            // 
            this.cbxCurrencyType.FormattingEnabled = true;
            this.cbxCurrencyType.Location = new System.Drawing.Point(651, 32);
            this.cbxCurrencyType.Name = "cbxCurrencyType";
            this.cbxCurrencyType.Size = new System.Drawing.Size(45, 20);
            this.cbxCurrencyType.TabIndex = 48;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(610, 32);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(41, 12);
            this.label27.TabIndex = 47;
            this.label27.Text = "货币：";
            // 
            // tbITTermText
            // 
            this.tbITTermText.Location = new System.Drawing.Point(534, 68);
            this.tbITTermText.Name = "tbITTermText";
            this.tbITTermText.Size = new System.Drawing.Size(162, 21);
            this.tbITTermText.TabIndex = 46;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(390, 71);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(65, 12);
            this.label26.TabIndex = 44;
            this.label26.Text = "交付条件：";
            // 
            // tbTargetValue
            // 
            this.tbTargetValue.Location = new System.Drawing.Point(468, 29);
            this.tbTargetValue.Name = "tbTargetValue";
            this.tbTargetValue.Size = new System.Drawing.Size(120, 21);
            this.tbTargetValue.TabIndex = 42;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(389, 32);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(53, 12);
            this.label24.TabIndex = 41;
            this.label24.Text = "目标值：";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(232, 146);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(29, 12);
            this.label23.TabIndex = 40;
            this.label23.Text = "净额";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(200, 146);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(17, 12);
            this.label21.TabIndex = 39;
            this.label21.Text = "天";
            // 
            // tbDays3
            // 
            this.tbDays3.Location = new System.Drawing.Point(129, 143);
            this.tbDays3.Name = "tbDays3";
            this.tbDays3.Size = new System.Drawing.Size(60, 21);
            this.tbDays3.TabIndex = 38;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(49, 146);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(41, 12);
            this.label22.TabIndex = 37;
            this.label22.Text = "付款：";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(287, 109);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(11, 12);
            this.label18.TabIndex = 36;
            this.label18.Text = "%";
            // 
            // tbDiscountRate2
            // 
            this.tbDiscountRate2.Location = new System.Drawing.Point(234, 106);
            this.tbDiscountRate2.Name = "tbDiscountRate2";
            this.tbDiscountRate2.Size = new System.Drawing.Size(45, 21);
            this.tbDiscountRate2.TabIndex = 35;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(200, 109);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(17, 12);
            this.label19.TabIndex = 34;
            this.label19.Text = "天";
            // 
            // tbDays2
            // 
            this.tbDays2.Location = new System.Drawing.Point(129, 106);
            this.tbDays2.Name = "tbDays2";
            this.tbDays2.Size = new System.Drawing.Size(60, 21);
            this.tbDays2.TabIndex = 33;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(49, 109);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(41, 12);
            this.label20.TabIndex = 32;
            this.label20.Text = "付款：";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(287, 69);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(11, 12);
            this.label17.TabIndex = 31;
            this.label17.Text = "%";
            // 
            // tbDiscountRate1
            // 
            this.tbDiscountRate1.Location = new System.Drawing.Point(234, 66);
            this.tbDiscountRate1.Name = "tbDiscountRate1";
            this.tbDiscountRate1.Size = new System.Drawing.Size(45, 21);
            this.tbDiscountRate1.TabIndex = 30;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(200, 69);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(17, 12);
            this.label16.TabIndex = 29;
            this.label16.Text = "天";
            // 
            // tbDays1
            // 
            this.tbDays1.Location = new System.Drawing.Point(129, 66);
            this.tbDays1.Name = "tbDays1";
            this.tbDays1.Size = new System.Drawing.Size(60, 21);
            this.tbDays1.TabIndex = 28;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(49, 69);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(41, 12);
            this.label15.TabIndex = 27;
            this.label15.Text = "付款：";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(44, 32);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(65, 12);
            this.label9.TabIndex = 25;
            this.label9.Text = "付款条件：";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.cbxSupplierID);
            this.groupBox2.Controls.Add(this.tbAgreementID);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.dtpEndTime);
            this.groupBox2.Controls.Add(this.dtpBeginTime);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.dtpCreateTime);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.cbxAgreementType);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Location = new System.Drawing.Point(4, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(769, 116);
            this.groupBox2.TabIndex = 15;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "抬头数据";
            // 
            // cbxSupplierID
            // 
            this.cbxSupplierID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxSupplierID.FormattingEnabled = true;
            this.cbxSupplierID.Location = new System.Drawing.Point(130, 71);
            this.cbxSupplierID.Name = "cbxSupplierID";
            this.cbxSupplierID.Size = new System.Drawing.Size(112, 20);
            this.cbxSupplierID.TabIndex = 30;
            // 
            // tbAgreementID
            // 
            this.tbAgreementID.Location = new System.Drawing.Point(130, 29);
            this.tbAgreementID.Name = "tbAgreementID";
            this.tbAgreementID.Size = new System.Drawing.Size(112, 21);
            this.tbAgreementID.TabIndex = 29;
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(51, 32);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(65, 12);
            this.label1.TabIndex = 28;
            this.label1.Text = "协议编号：";
            // 
            // dtpEndTime
            // 
            this.dtpEndTime.Location = new System.Drawing.Point(618, 68);
            this.dtpEndTime.Name = "dtpEndTime";
            this.dtpEndTime.Size = new System.Drawing.Size(110, 21);
            this.dtpEndTime.TabIndex = 27;
            // 
            // dtpBeginTime
            // 
            this.dtpBeginTime.Location = new System.Drawing.Point(391, 68);
            this.dtpBeginTime.Name = "dtpBeginTime";
            this.dtpBeginTime.Size = new System.Drawing.Size(110, 21);
            this.dtpBeginTime.TabIndex = 26;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(523, 74);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 12);
            this.label2.TabIndex = 25;
            this.label2.Text = "协议截止时间：";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(282, 74);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(89, 12);
            this.label6.TabIndex = 24;
            this.label6.Text = "协议开始时间：";
            // 
            // dtpCreateTime
            // 
            this.dtpCreateTime.Enabled = false;
            this.dtpCreateTime.Location = new System.Drawing.Point(618, 26);
            this.dtpCreateTime.Name = "dtpCreateTime";
            this.dtpCreateTime.Size = new System.Drawing.Size(110, 21);
            this.dtpCreateTime.TabIndex = 23;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(538, 32);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 12);
            this.label3.TabIndex = 22;
            this.label3.Text = "协议日期：";
            // 
            // cbxAgreementType
            // 
            this.cbxAgreementType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxAgreementType.FormattingEnabled = true;
            this.cbxAgreementType.Items.AddRange(new object[] {
            "LP",
            "LPA"});
            this.cbxAgreementType.Location = new System.Drawing.Point(389, 29);
            this.cbxAgreementType.Name = "cbxAgreementType";
            this.cbxAgreementType.Size = new System.Drawing.Size(110, 20);
            this.cbxAgreementType.TabIndex = 21;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(301, 32);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 12);
            this.label4.TabIndex = 20;
            this.label4.Text = "协议类型：";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(34, 74);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(77, 12);
            this.label5.TabIndex = 18;
            this.label5.Text = "供应商编号：";
            // 
            // btnNext
            // 
            this.btnNext.Location = new System.Drawing.Point(657, 479);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(75, 23);
            this.btnNext.TabIndex = 17;
            this.btnNext.Text = "下一步";
            this.btnNext.UseVisualStyleBackColor = true;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // cbTradeClause
            // 
            this.cbTradeClause.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbTradeClause.FormattingEnabled = true;
            this.cbTradeClause.Location = new System.Drawing.Point(468, 69);
            this.cbTradeClause.Name = "cbTradeClause";
            this.cbTradeClause.Size = new System.Drawing.Size(49, 20);
            this.cbTradeClause.TabIndex = 50;
            this.cbTradeClause.SelectedIndexChanged += new System.EventHandler(this.cbTradeClause_SelectedIndexChanged);
            // 
            // SchedulingAgreementForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(778, 514);
            this.Controls.Add(this.btnNext);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "SchedulingAgreementForm";
            this.Text = "创建计划协议 初始数据";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.TextBox tbAgreementID;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dtpEndTime;
        private System.Windows.Forms.DateTimePicker dtpBeginTime;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DateTimePicker dtpCreateTime;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cbxAgreementType;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox tbITTermText;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox tbTargetValue;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox tbDays3;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox tbDiscountRate2;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox tbDays2;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox tbDiscountRate1;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox tbDays1;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox cbxCurrencyType;
        private System.Windows.Forms.ComboBox cbxSupplierID;
        private System.Windows.Forms.ComboBox cbxFactory;
        private System.Windows.Forms.ComboBox cbxStorageLocation;
        private System.Windows.Forms.ComboBox cbxBuyerOrganization;
        private System.Windows.Forms.ComboBox cbxBuyerGroup;
        private System.Windows.Forms.ComboBox cbxPaymentClause;
        private System.Windows.Forms.ComboBox cbTradeClause;
    }
}