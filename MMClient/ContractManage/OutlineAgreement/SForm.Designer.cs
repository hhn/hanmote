﻿namespace MMClient.ContractManage.OutlineAgreement
{
    partial class SForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.tblTermText = new System.Windows.Forms.TextBox();
            this.cbxTradeClause = new System.Windows.Forms.ComboBox();
            this.cbxPaymentClause = new System.Windows.Forms.ComboBox();
            this.cbxCurrencyType = new System.Windows.Forms.ComboBox();
            this.label27 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.tbTargetValue = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.tbDays3 = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.tbDiscountRate2 = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.tbDays2 = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.tbDiscountRate1 = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.tbDays1 = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tbStockId = new System.Windows.Forms.TextBox();
            this.tbPurchaseOrg = new System.Windows.Forms.TextBox();
            this.tbPurchaseGroup = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.dtpCreateTime = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cbxSourceId = new System.Windows.Forms.ComboBox();
            this.cbxSupplierID = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.cbxFactoryId = new System.Windows.Forms.ComboBox();
            this.dtpEndTime = new System.Windows.Forms.DateTimePicker();
            this.dtpBeginTime = new System.Windows.Forms.DateTimePicker();
            this.label7 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.cbxAgreementType = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.btnSave = new System.Windows.Forms.Button();
            this.dataGridViewComboBoxColumn1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn2 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn3 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn4 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn5 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dgvMaterialItem = new System.Windows.Forms.DataGridView();
            this.Item_Num = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.I = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.A = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Material_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Material_Name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Net_Price = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Every = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Demand_Count = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OUn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Price_Determin_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Factory = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMaterialItem)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(-1, 1);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(775, 498);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupBox3);
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(767, 472);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "基本数据";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.tblTermText);
            this.groupBox3.Controls.Add(this.cbxTradeClause);
            this.groupBox3.Controls.Add(this.cbxPaymentClause);
            this.groupBox3.Controls.Add(this.cbxCurrencyType);
            this.groupBox3.Controls.Add(this.label27);
            this.groupBox3.Controls.Add(this.label26);
            this.groupBox3.Controls.Add(this.tbTargetValue);
            this.groupBox3.Controls.Add(this.label24);
            this.groupBox3.Controls.Add(this.label23);
            this.groupBox3.Controls.Add(this.label21);
            this.groupBox3.Controls.Add(this.tbDays3);
            this.groupBox3.Controls.Add(this.label22);
            this.groupBox3.Controls.Add(this.label18);
            this.groupBox3.Controls.Add(this.tbDiscountRate2);
            this.groupBox3.Controls.Add(this.label19);
            this.groupBox3.Controls.Add(this.tbDays2);
            this.groupBox3.Controls.Add(this.label20);
            this.groupBox3.Controls.Add(this.label17);
            this.groupBox3.Controls.Add(this.tbDiscountRate1);
            this.groupBox3.Controls.Add(this.label16);
            this.groupBox3.Controls.Add(this.tbDays1);
            this.groupBox3.Controls.Add(this.label15);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Location = new System.Drawing.Point(3, 247);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(753, 195);
            this.groupBox3.TabIndex = 4;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "交货和支付条件";
            // 
            // tblTermText
            // 
            this.tblTermText.Location = new System.Drawing.Point(541, 72);
            this.tblTermText.Name = "tblTermText";
            this.tblTermText.Size = new System.Drawing.Size(147, 21);
            this.tblTermText.TabIndex = 28;
            // 
            // cbxTradeClause
            // 
            this.cbxTradeClause.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxTradeClause.FormattingEnabled = true;
            this.cbxTradeClause.Location = new System.Drawing.Point(459, 72);
            this.cbxTradeClause.Name = "cbxTradeClause";
            this.cbxTradeClause.Size = new System.Drawing.Size(46, 20);
            this.cbxTradeClause.TabIndex = 27;
            this.cbxTradeClause.SelectedIndexChanged += new System.EventHandler(this.cbxTradeClause_SelectedIndexChanged);
            // 
            // cbxPaymentClause
            // 
            this.cbxPaymentClause.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxPaymentClause.FormattingEnabled = true;
            this.cbxPaymentClause.Location = new System.Drawing.Point(120, 36);
            this.cbxPaymentClause.Name = "cbxPaymentClause";
            this.cbxPaymentClause.Size = new System.Drawing.Size(121, 20);
            this.cbxPaymentClause.TabIndex = 26;
            this.cbxPaymentClause.SelectedIndexChanged += new System.EventHandler(this.cbxPaymentClause_SelectedIndexChanged);
            // 
            // cbxCurrencyType
            // 
            this.cbxCurrencyType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxCurrencyType.FormattingEnabled = true;
            this.cbxCurrencyType.Location = new System.Drawing.Point(643, 35);
            this.cbxCurrencyType.Name = "cbxCurrencyType";
            this.cbxCurrencyType.Size = new System.Drawing.Size(45, 20);
            this.cbxCurrencyType.TabIndex = 25;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(602, 38);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(41, 12);
            this.label27.TabIndex = 24;
            this.label27.Text = "货币：";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(382, 77);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(65, 12);
            this.label26.TabIndex = 21;
            this.label26.Text = "交付条件：";
            // 
            // tbTargetValue
            // 
            this.tbTargetValue.Location = new System.Drawing.Point(460, 35);
            this.tbTargetValue.Name = "tbTargetValue";
            this.tbTargetValue.Size = new System.Drawing.Size(120, 21);
            this.tbTargetValue.TabIndex = 17;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(381, 38);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(53, 12);
            this.label24.TabIndex = 16;
            this.label24.Text = "目标值：";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(224, 152);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(29, 12);
            this.label23.TabIndex = 15;
            this.label23.Text = "净额";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(192, 152);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(17, 12);
            this.label21.TabIndex = 14;
            this.label21.Text = "天";
            // 
            // tbDays3
            // 
            this.tbDays3.Location = new System.Drawing.Point(121, 149);
            this.tbDays3.Name = "tbDays3";
            this.tbDays3.Size = new System.Drawing.Size(60, 21);
            this.tbDays3.TabIndex = 13;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(41, 152);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(41, 12);
            this.label22.TabIndex = 12;
            this.label22.Text = "付款：";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(279, 115);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(11, 12);
            this.label18.TabIndex = 11;
            this.label18.Text = "%";
            // 
            // tbDiscountRate2
            // 
            this.tbDiscountRate2.Location = new System.Drawing.Point(226, 112);
            this.tbDiscountRate2.Name = "tbDiscountRate2";
            this.tbDiscountRate2.Size = new System.Drawing.Size(45, 21);
            this.tbDiscountRate2.TabIndex = 10;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(192, 115);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(17, 12);
            this.label19.TabIndex = 9;
            this.label19.Text = "天";
            // 
            // tbDays2
            // 
            this.tbDays2.Location = new System.Drawing.Point(121, 112);
            this.tbDays2.Name = "tbDays2";
            this.tbDays2.Size = new System.Drawing.Size(60, 21);
            this.tbDays2.TabIndex = 8;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(41, 115);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(41, 12);
            this.label20.TabIndex = 7;
            this.label20.Text = "付款：";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(279, 75);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(11, 12);
            this.label17.TabIndex = 6;
            this.label17.Text = "%";
            // 
            // tbDiscountRate1
            // 
            this.tbDiscountRate1.Location = new System.Drawing.Point(226, 72);
            this.tbDiscountRate1.Name = "tbDiscountRate1";
            this.tbDiscountRate1.Size = new System.Drawing.Size(45, 21);
            this.tbDiscountRate1.TabIndex = 5;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(192, 75);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(17, 12);
            this.label16.TabIndex = 4;
            this.label16.Text = "天";
            // 
            // tbDays1
            // 
            this.tbDays1.Location = new System.Drawing.Point(121, 72);
            this.tbDays1.Name = "tbDays1";
            this.tbDays1.Size = new System.Drawing.Size(60, 21);
            this.tbDays1.TabIndex = 3;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(41, 75);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(41, 12);
            this.label15.TabIndex = 2;
            this.label15.Text = "付款：";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(36, 38);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(65, 12);
            this.label8.TabIndex = 0;
            this.label8.Text = "付款条件：";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.tbStockId);
            this.groupBox2.Controls.Add(this.tbPurchaseOrg);
            this.groupBox2.Controls.Add(this.tbPurchaseGroup);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.dtpCreateTime);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Location = new System.Drawing.Point(4, 129);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(753, 112);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "参考数据";
            // 
            // tbStockId
            // 
            this.tbStockId.Location = new System.Drawing.Point(121, 71);
            this.tbStockId.Name = "tbStockId";
            this.tbStockId.Size = new System.Drawing.Size(113, 21);
            this.tbStockId.TabIndex = 31;
            // 
            // tbPurchaseOrg
            // 
            this.tbPurchaseOrg.Location = new System.Drawing.Point(379, 30);
            this.tbPurchaseOrg.Name = "tbPurchaseOrg";
            this.tbPurchaseOrg.Size = new System.Drawing.Size(112, 21);
            this.tbPurchaseOrg.TabIndex = 30;
            // 
            // tbPurchaseGroup
            // 
            this.tbPurchaseGroup.Location = new System.Drawing.Point(121, 30);
            this.tbPurchaseGroup.Name = "tbPurchaseGroup";
            this.tbPurchaseGroup.Size = new System.Drawing.Size(113, 21);
            this.tbPurchaseGroup.TabIndex = 29;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(296, 33);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(65, 12);
            this.label5.TabIndex = 13;
            this.label5.Text = "采购组织：";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(55, 33);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 12);
            this.label4.TabIndex = 12;
            this.label4.Text = "采购组：";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(53, 74);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(53, 12);
            this.label14.TabIndex = 9;
            this.label14.Text = "库存地：";
            // 
            // dtpCreateTime
            // 
            this.dtpCreateTime.Enabled = false;
            this.dtpCreateTime.Location = new System.Drawing.Point(612, 34);
            this.dtpCreateTime.Name = "dtpCreateTime";
            this.dtpCreateTime.Size = new System.Drawing.Size(110, 21);
            this.dtpCreateTime.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(537, 34);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 12);
            this.label3.TabIndex = 4;
            this.label3.Text = "协议日期：";
            // 
            // supplierInforGroupBox
            // 
            this.groupBox1.Controls.Add(this.cbxSourceId);
            this.groupBox1.Controls.Add(this.cbxSupplierID);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.cbxFactoryId);
            this.groupBox1.Controls.Add(this.dtpEndTime);
            this.groupBox1.Controls.Add(this.dtpBeginTime);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.cbxAgreementType);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(4, 6);
            this.groupBox1.Name = "supplierInforGroupBox";
            this.groupBox1.Size = new System.Drawing.Size(753, 117);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "抬头数据";
            // 
            // cbxSourceId
            // 
            this.cbxSourceId.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxSourceId.FormattingEnabled = true;
            this.cbxSourceId.Location = new System.Drawing.Point(121, 37);
            this.cbxSourceId.Name = "cbxSourceId";
            this.cbxSourceId.Size = new System.Drawing.Size(109, 20);
            this.cbxSourceId.TabIndex = 19;
            this.cbxSourceId.SelectedIndexChanged += new System.EventHandler(this.cbxSourceId_SelectedIndexChanged);
            // 
            // cbxSupplierID
            // 
            this.cbxSupplierID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxSupplierID.FormattingEnabled = true;
            this.cbxSupplierID.Location = new System.Drawing.Point(379, 32);
            this.cbxSupplierID.Name = "cbxSupplierID";
            this.cbxSupplierID.Size = new System.Drawing.Size(112, 20);
            this.cbxSupplierID.TabIndex = 18;
            this.cbxSupplierID.SelectedIndexChanged += new System.EventHandler(this.cbxSupplierID_SelectedIndexChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(41, 35);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(65, 12);
            this.label12.TabIndex = 16;
            this.label12.Text = "寻源编号：";
            // 
            // cbxFactoryId
            // 
            this.cbxFactoryId.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxFactoryId.FormattingEnabled = true;
            this.cbxFactoryId.Location = new System.Drawing.Point(608, 32);
            this.cbxFactoryId.Name = "cbxFactoryId";
            this.cbxFactoryId.Size = new System.Drawing.Size(114, 20);
            this.cbxFactoryId.TabIndex = 16;
            this.cbxFactoryId.SelectedIndexChanged += new System.EventHandler(this.cbxFactoryId_SelectedIndexChanged);
            // 
            // dtpEndTime
            // 
            this.dtpEndTime.Location = new System.Drawing.Point(608, 71);
            this.dtpEndTime.Name = "dtpEndTime";
            this.dtpEndTime.Size = new System.Drawing.Size(110, 21);
            this.dtpEndTime.TabIndex = 13;
            // 
            // dtpBeginTime
            // 
            this.dtpBeginTime.Location = new System.Drawing.Point(381, 71);
            this.dtpBeginTime.Name = "dtpBeginTime";
            this.dtpBeginTime.Size = new System.Drawing.Size(110, 21);
            this.dtpBeginTime.TabIndex = 12;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(513, 77);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(89, 12);
            this.label7.TabIndex = 11;
            this.label7.Text = "协议截止时间：";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(539, 40);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(41, 12);
            this.label13.TabIndex = 8;
            this.label13.Text = "工厂：";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(272, 77);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(89, 12);
            this.label6.TabIndex = 10;
            this.label6.Text = "协议开始时间：";
            // 
            // cbxAgreementType
            // 
            this.cbxAgreementType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxAgreementType.FormattingEnabled = true;
            this.cbxAgreementType.Items.AddRange(new object[] {
            "数量合同",
            "价值合同",
            "自助采购"});
            this.cbxAgreementType.Location = new System.Drawing.Point(120, 72);
            this.cbxAgreementType.Name = "cbxAgreementType";
            this.cbxAgreementType.Size = new System.Drawing.Size(110, 20);
            this.cbxAgreementType.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(41, 80);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 12);
            this.label2.TabIndex = 2;
            this.label2.Text = "协议类型：";
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(272, 32);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(77, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "供应商编号：";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.dgvMaterialItem);
            this.tabPage2.Controls.Add(this.btnSave);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(767, 472);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "项目数据";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(605, 439);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 205;
            this.btnSave.Text = "保存";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // dataGridViewComboBoxColumn1
            // 
            this.dataGridViewComboBoxColumn1.HeaderText = "条件类型";
            this.dataGridViewComboBoxColumn1.Items.AddRange(new object[] {
            "价格",
            "折扣",
            "折上折",
            "附加费用",
            "运费"});
            this.dataGridViewComboBoxColumn1.Name = "dataGridViewComboBoxColumn1";
            this.dataGridViewComboBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // dataGridViewComboBoxColumn2
            // 
            this.dataGridViewComboBoxColumn2.HeaderText = "定价类型";
            this.dataGridViewComboBoxColumn2.Items.AddRange(new object[] {
            "价格",
            "折扣",
            "附加费",
            "税收"});
            this.dataGridViewComboBoxColumn2.Name = "dataGridViewComboBoxColumn2";
            this.dataGridViewComboBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewComboBoxColumn2.Width = 80;
            // 
            // dataGridViewComboBoxColumn3
            // 
            this.dataGridViewComboBoxColumn3.HeaderText = "计算类型";
            this.dataGridViewComboBoxColumn3.Items.AddRange(new object[] {
            "数量",
            "百分比"});
            this.dataGridViewComboBoxColumn3.Name = "dataGridViewComboBoxColumn3";
            this.dataGridViewComboBoxColumn3.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewComboBoxColumn3.Width = 80;
            // 
            // dataGridViewComboBoxColumn4
            // 
            this.dataGridViewComboBoxColumn4.HeaderText = "舍入规则";
            this.dataGridViewComboBoxColumn4.Items.AddRange(new object[] {
            "四舍五入",
            "较高值",
            "较低值"});
            this.dataGridViewComboBoxColumn4.Name = "dataGridViewComboBoxColumn4";
            this.dataGridViewComboBoxColumn4.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewComboBoxColumn4.Width = 80;
            // 
            // dataGridViewComboBoxColumn5
            // 
            this.dataGridViewComboBoxColumn5.HeaderText = "正/负";
            this.dataGridViewComboBoxColumn5.Items.AddRange(new object[] {
            "+",
            "-"});
            this.dataGridViewComboBoxColumn5.Name = "dataGridViewComboBoxColumn5";
            this.dataGridViewComboBoxColumn5.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewComboBoxColumn5.Width = 60;
            // 
            // dgvMaterialItem
            // 
            this.dgvMaterialItem.AllowUserToAddRows = false;
            this.dgvMaterialItem.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvMaterialItem.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Item_Num,
            this.I,
            this.A,
            this.Material_ID,
            this.Material_Name,
            this.Net_Price,
            this.Every,
            this.Demand_Count,
            this.OUn,
            this.Price_Determin_ID,
            this.Factory});
            this.dgvMaterialItem.Location = new System.Drawing.Point(0, 0);
            this.dgvMaterialItem.Name = "dgvMaterialItem";
            this.dgvMaterialItem.RowHeadersVisible = false;
            this.dgvMaterialItem.RowTemplate.Height = 23;
            this.dgvMaterialItem.Size = new System.Drawing.Size(767, 433);
            this.dgvMaterialItem.TabIndex = 206;
            this.dgvMaterialItem.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // Item_Num
            // 
            this.Item_Num.HeaderText = "项目";
            this.Item_Num.Name = "Item_Num";
            this.Item_Num.Width = 67;
            // 
            // I
            // 
            this.I.HeaderText = "I";
            this.I.Name = "I";
            this.I.Width = 60;
            // 
            // A
            // 
            this.A.HeaderText = "A";
            this.A.Name = "A";
            this.A.Width = 60;
            // 
            // Material_ID
            // 
            this.Material_ID.HeaderText = "物料编号";
            this.Material_ID.Name = "Material_ID";
            this.Material_ID.Width = 80;
            // 
            // Material_Name
            // 
            this.Material_Name.HeaderText = "物料名称";
            this.Material_Name.Name = "Material_Name";
            this.Material_Name.Width = 80;
            // 
            // Net_Price
            // 
            this.Net_Price.HeaderText = "净价";
            this.Net_Price.Name = "Net_Price";
            this.Net_Price.Width = 67;
            // 
            // Every
            // 
            this.Every.HeaderText = "每";
            this.Every.Name = "Every";
            this.Every.Width = 67;
            // 
            // Demand_Count
            // 
            this.Demand_Count.HeaderText = "目标数量";
            this.Demand_Count.Name = "Demand_Count";
            this.Demand_Count.Width = 80;
            // 
            // OUn
            // 
            this.OUn.HeaderText = "OUn";
            this.OUn.Name = "OUn";
            this.OUn.Width = 67;
            // 
            // Price_Determin_ID
            // 
            this.Price_Determin_ID.HeaderText = "货源确定号";
            this.Price_Determin_ID.Name = "Price_Determin_ID";
            this.Price_Determin_ID.Width = 90;
            // 
            // Factory
            // 
            this.Factory.HeaderText = "工厂";
            this.Factory.Name = "Factory";
            this.Factory.Width = 67;
            // 
            // SForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(772, 511);
            this.Controls.Add(this.tabControl1);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "SForm";
            this.Text = "协议合同";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvMaterialItem)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox cbxSourceId;
        private System.Windows.Forms.ComboBox cbxSupplierID;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox cbxFactoryId;
        private System.Windows.Forms.DateTimePicker dtpEndTime;
        private System.Windows.Forms.DateTimePicker dtpBeginTime;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cbxAgreementType;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.DateTimePicker dtpCreateTime;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox tblTermText;
        private System.Windows.Forms.ComboBox cbxTradeClause;
        private System.Windows.Forms.ComboBox cbxPaymentClause;
        private System.Windows.Forms.ComboBox cbxCurrencyType;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox tbTargetValue;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox tbDays3;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox tbDiscountRate2;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox tbDays2;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox tbDiscountRate1;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox tbDays1;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox tbStockId;
        private System.Windows.Forms.TextBox tbPurchaseOrg;
        private System.Windows.Forms.TextBox tbPurchaseGroup;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn1;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn2;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn3;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn4;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn5;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.DataGridView dgvMaterialItem;
        private System.Windows.Forms.DataGridViewTextBoxColumn Item_Num;
        private System.Windows.Forms.DataGridViewTextBoxColumn I;
        private System.Windows.Forms.DataGridViewTextBoxColumn A;
        private System.Windows.Forms.DataGridViewTextBoxColumn Material_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Material_Name;
        private System.Windows.Forms.DataGridViewTextBoxColumn Net_Price;
        private System.Windows.Forms.DataGridViewTextBoxColumn Every;
        private System.Windows.Forms.DataGridViewTextBoxColumn Demand_Count;
        private System.Windows.Forms.DataGridViewTextBoxColumn OUn;
        private System.Windows.Forms.DataGridViewTextBoxColumn Price_Determin_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Factory;

    }
}