﻿namespace MMClient.ContractManage.OutlineAgreement
{
    partial class DeliverySchedulingItemsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tbNextDeliveryNum = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.lblOUn = new System.Windows.Forms.Label();
            this.tbAccumulateAmount = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.dtpLastDeliveryTime = new System.Windows.Forms.DateTimePicker();
            this.label10 = new System.Windows.Forms.Label();
            this.tbTargetAmount = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.lblMaterialName = new System.Windows.Forms.Label();
            this.tbMaterialID = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.tbSchedulingAgreementID = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.dgvDeliveryScheduling = new System.Windows.Forms.DataGridView();
            this.Item_Num = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Delivery_Amount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Accumulate_Delivery_Amount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LastTime_Accumulate_Amount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Not_Delivery_Amount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Status = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDeliveryScheduling)).BeginInit();
            this.SuspendLayout();
            // 
            // supplierInforGroupBox
            // 
            this.groupBox1.Controls.Add(this.tbNextDeliveryNum);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.lblOUn);
            this.groupBox1.Controls.Add(this.tbAccumulateAmount);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.dtpLastDeliveryTime);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.tbTargetAmount);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.lblMaterialName);
            this.groupBox1.Controls.Add(this.tbMaterialID);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.tbSchedulingAgreementID);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(7, 5);
            this.groupBox1.Name = "supplierInforGroupBox";
            this.groupBox1.Size = new System.Drawing.Size(815, 178);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "抬头数据";
            // 
            // tbNextDeliveryNum
            // 
            this.tbNextDeliveryNum.Location = new System.Drawing.Point(544, 100);
            this.tbNextDeliveryNum.Name = "tbNextDeliveryNum";
            this.tbNextDeliveryNum.ReadOnly = true;
            this.tbNextDeliveryNum.Size = new System.Drawing.Size(144, 21);
            this.tbNextDeliveryNum.TabIndex = 20;
            this.tbNextDeliveryNum.Text = "3";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(450, 106);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 12);
            this.label2.TabIndex = 19;
            this.label2.Text = "下一传送编号：";
            // 
            // lblOUn
            // 
            this.lblOUn.AutoSize = true;
            this.lblOUn.Location = new System.Drawing.Point(694, 28);
            this.lblOUn.Name = "lblOUn";
            this.lblOUn.Size = new System.Drawing.Size(29, 12);
            this.lblOUn.TabIndex = 18;
            this.lblOUn.Text = "单位";
            // 
            // tbAccumulateAmount
            // 
            this.tbAccumulateAmount.Location = new System.Drawing.Point(145, 140);
            this.tbAccumulateAmount.Name = "tbAccumulateAmount";
            this.tbAccumulateAmount.ReadOnly = true;
            this.tbAccumulateAmount.Size = new System.Drawing.Size(209, 21);
            this.tbAccumulateAmount.TabIndex = 17;
            this.tbAccumulateAmount.Text = "3000";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(60, 143);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(77, 12);
            this.label11.TabIndex = 16;
            this.label11.Text = "累计收货量：";
            // 
            // dtpLastDeliveryTime
            // 
            this.dtpLastDeliveryTime.Enabled = false;
            this.dtpLastDeliveryTime.Location = new System.Drawing.Point(145, 100);
            this.dtpLastDeliveryTime.Name = "dtpLastDeliveryTime";
            this.dtpLastDeliveryTime.Size = new System.Drawing.Size(209, 21);
            this.dtpLastDeliveryTime.TabIndex = 15;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(74, 106);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(65, 12);
            this.label10.TabIndex = 14;
            this.label10.Text = "最后传送：";
            // 
            // tbTargetAmount
            // 
            this.tbTargetAmount.Location = new System.Drawing.Point(544, 25);
            this.tbTargetAmount.Name = "tbTargetAmount";
            this.tbTargetAmount.ReadOnly = true;
            this.tbTargetAmount.Size = new System.Drawing.Size(144, 21);
            this.tbTargetAmount.TabIndex = 13;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(459, 28);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(65, 12);
            this.label9.TabIndex = 12;
            this.label9.Text = "计划数量：";
            // 
            // lblMaterialName
            // 
            this.lblMaterialName.AutoSize = true;
            this.lblMaterialName.Location = new System.Drawing.Point(459, 63);
            this.lblMaterialName.Name = "lblMaterialName";
            this.lblMaterialName.Size = new System.Drawing.Size(53, 12);
            this.lblMaterialName.TabIndex = 11;
            this.lblMaterialName.Text = "物料名称";
            this.lblMaterialName.Visible = false;
            // 
            // tbMaterialID
            // 
            this.tbMaterialID.Location = new System.Drawing.Point(145, 60);
            this.tbMaterialID.Name = "tbMaterialID";
            this.tbMaterialID.ReadOnly = true;
            this.tbMaterialID.Size = new System.Drawing.Size(209, 21);
            this.tbMaterialID.TabIndex = 10;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(72, 63);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(65, 12);
            this.label7.TabIndex = 9;
            this.label7.Text = "物料编号：";
            // 
            // tbSchedulingAgreementID
            // 
            this.tbSchedulingAgreementID.Location = new System.Drawing.Point(145, 25);
            this.tbSchedulingAgreementID.Name = "tbSchedulingAgreementID";
            this.tbSchedulingAgreementID.ReadOnly = true;
            this.tbSchedulingAgreementID.Size = new System.Drawing.Size(209, 21);
            this.tbSchedulingAgreementID.TabIndex = 1;
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(74, 28);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(65, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "协议编号：";
            // 
            // dgvDeliveryScheduling
            // 
            this.dgvDeliveryScheduling.AllowUserToAddRows = false;
            this.dgvDeliveryScheduling.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDeliveryScheduling.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Item_Num,
            this.Delivery_Amount,
            this.Accumulate_Delivery_Amount,
            this.LastTime_Accumulate_Amount,
            this.Not_Delivery_Amount,
            this.Status});
            this.dgvDeliveryScheduling.Location = new System.Drawing.Point(7, 226);
            this.dgvDeliveryScheduling.Name = "dgvDeliveryScheduling";
            this.dgvDeliveryScheduling.RowHeadersVisible = false;
            this.dgvDeliveryScheduling.RowTemplate.Height = 23;
            this.dgvDeliveryScheduling.Size = new System.Drawing.Size(815, 288);
            this.dgvDeliveryScheduling.TabIndex = 1;
            this.dgvDeliveryScheduling.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvDeliveryScheduling_CellContentClick);
            // 
            // Item_Num
            // 
            this.Item_Num.HeaderText = "编号";
            this.Item_Num.Name = "Item_Num";
            this.Item_Num.ReadOnly = true;
            this.Item_Num.Width = 70;
            // 
            // Delivery_Amount
            // 
            this.Delivery_Amount.HeaderText = "计划数量";
            this.Delivery_Amount.Name = "Delivery_Amount";
            // 
            // Accumulate_Delivery_Amount
            // 
            this.Accumulate_Delivery_Amount.HeaderText = "累积交货量";
            this.Accumulate_Delivery_Amount.Name = "Accumulate_Delivery_Amount";
            // 
            // LastTime_Accumulate_Amount
            // 
            this.LastTime_Accumulate_Amount.HeaderText = "上期累积量";
            this.LastTime_Accumulate_Amount.Name = "LastTime_Accumulate_Amount";
            // 
            // Not_Delivery_Amount
            // 
            this.Not_Delivery_Amount.HeaderText = "未清数量";
            this.Not_Delivery_Amount.Name = "Not_Delivery_Amount";
            // 
            // Status
            // 
            this.Status.HeaderText = "状态";
            this.Status.Name = "Status";
            this.Status.ReadOnly = true;
            this.Status.Width = 70;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(667, 197);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 3;
            this.btnSave.Text = "保存";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(465, 197);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(75, 23);
            this.btnAdd.TabIndex = 4;
            this.btnAdd.Text = "增加";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(569, 197);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(75, 23);
            this.btnDelete.TabIndex = 5;
            this.btnDelete.Text = "删除";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // DeliverySchedulingItemsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(834, 522);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.dgvDeliveryScheduling);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "DeliverySchedulingItemsForm";
            this.Text = "维护交货计划行";
            this.Load += new System.EventHandler(this.DeliverySchedulingItemsForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDeliveryScheduling)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox tbSchedulingAgreementID;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dgvDeliveryScheduling;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.TextBox tbAccumulateAmount;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.DateTimePicker dtpLastDeliveryTime;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox tbTargetAmount;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label lblOUn;
        private System.Windows.Forms.Label lblMaterialName;
        private System.Windows.Forms.TextBox tbMaterialID;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tbNextDeliveryNum;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.DataGridViewTextBoxColumn Item_Num;
        private System.Windows.Forms.DataGridViewTextBoxColumn Delivery_Amount;
        private System.Windows.Forms.DataGridViewTextBoxColumn Accumulate_Delivery_Amount;
        private System.Windows.Forms.DataGridViewTextBoxColumn LastTime_Accumulate_Amount;
        private System.Windows.Forms.DataGridViewTextBoxColumn Not_Delivery_Amount;
        private System.Windows.Forms.DataGridViewTextBoxColumn Status;
    }
}