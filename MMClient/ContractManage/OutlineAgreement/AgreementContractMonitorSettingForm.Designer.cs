﻿namespace MMClient.ContractManage.OutlineAgreement
{
    partial class AgreementContractMonitorSettingForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.cbEnableValueMonitor = new System.Windows.Forms.CheckBox();
            this.tbAlertValue = new System.Windows.Forms.TextBox();
            this.lblCurrencyType2 = new System.Windows.Forms.Label();
            this.cbEnableQuantityMonitor = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tbAlertQuantity = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.cbEnableDateMonitor = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.tbContractID = new System.Windows.Forms.TextBox();
            this.tbContractType = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.tbContractQuantity = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.tbContractValue = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.dtpBeginTime = new System.Windows.Forms.DateTimePicker();
            this.dtpEndTime = new System.Windows.Forms.DateTimePicker();
            this.label12 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lblCurrencyType = new System.Windows.Forms.Label();
            this.dtpAlertDate = new System.Windows.Forms.DateTimePicker();
            this.btnSave = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.tbContractVersion = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.dtpCreateTime = new System.Windows.Forms.DateTimePicker();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(146, 269);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(77, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "合同总金额：";
            // 
            // cbEnableValueMonitor
            // 
            this.cbEnableValueMonitor.AutoSize = true;
            this.cbEnableValueMonitor.Location = new System.Drawing.Point(45, 268);
            this.cbEnableValueMonitor.Name = "cbEnableValueMonitor";
            this.cbEnableValueMonitor.Size = new System.Drawing.Size(72, 16);
            this.cbEnableValueMonitor.TabIndex = 1;
            this.cbEnableValueMonitor.Text = "是否启用";
            this.cbEnableValueMonitor.UseVisualStyleBackColor = true;
            // 
            // tbAlertValue
            // 
            this.tbAlertValue.Location = new System.Drawing.Point(240, 266);
            this.tbAlertValue.Name = "tbAlertValue";
            this.tbAlertValue.Size = new System.Drawing.Size(175, 21);
            this.tbAlertValue.TabIndex = 2;
            // 
            // lblCurrencyType2
            // 
            this.lblCurrencyType2.AutoSize = true;
            this.lblCurrencyType2.Location = new System.Drawing.Point(435, 269);
            this.lblCurrencyType2.Name = "lblCurrencyType2";
            this.lblCurrencyType2.Size = new System.Drawing.Size(23, 12);
            this.lblCurrencyType2.TabIndex = 3;
            this.lblCurrencyType2.Text = "CNY";
            // 
            // cbEnableQuantityMonitor
            // 
            this.cbEnableQuantityMonitor.AutoSize = true;
            this.cbEnableQuantityMonitor.Location = new System.Drawing.Point(43, 316);
            this.cbEnableQuantityMonitor.Name = "cbEnableQuantityMonitor";
            this.cbEnableQuantityMonitor.Size = new System.Drawing.Size(72, 16);
            this.cbEnableQuantityMonitor.TabIndex = 4;
            this.cbEnableQuantityMonitor.Text = "是否启用";
            this.cbEnableQuantityMonitor.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(147, 317);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 12);
            this.label3.TabIndex = 5;
            this.label3.Text = "合同总数量：";
            // 
            // tbAlertQuantity
            // 
            this.tbAlertQuantity.Location = new System.Drawing.Point(240, 314);
            this.tbAlertQuantity.Name = "tbAlertQuantity";
            this.tbAlertQuantity.Size = new System.Drawing.Size(175, 21);
            this.tbAlertQuantity.TabIndex = 6;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(22, 29);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(0, 12);
            this.label5.TabIndex = 8;
            // 
            // cbEnableDateMonitor
            // 
            this.cbEnableDateMonitor.AutoSize = true;
            this.cbEnableDateMonitor.Location = new System.Drawing.Point(45, 362);
            this.cbEnableDateMonitor.Name = "cbEnableDateMonitor";
            this.cbEnableDateMonitor.Size = new System.Drawing.Size(72, 16);
            this.cbEnableDateMonitor.TabIndex = 9;
            this.cbEnableDateMonitor.Text = "是否启用";
            this.cbEnableDateMonitor.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(140, 363);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(89, 12);
            this.label6.TabIndex = 10;
            this.label6.Text = "合同警告日期：";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(47, 36);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(65, 12);
            this.label7.TabIndex = 11;
            this.label7.Text = "合同编号：";
            // 
            // tbContractID
            // 
            this.tbContractID.Location = new System.Drawing.Point(120, 33);
            this.tbContractID.Name = "tbContractID";
            this.tbContractID.ReadOnly = true;
            this.tbContractID.Size = new System.Drawing.Size(175, 21);
            this.tbContractID.TabIndex = 12;
            // 
            // tbContractType
            // 
            this.tbContractType.Location = new System.Drawing.Point(465, 33);
            this.tbContractType.Name = "tbContractType";
            this.tbContractType.ReadOnly = true;
            this.tbContractType.Size = new System.Drawing.Size(175, 21);
            this.tbContractType.TabIndex = 14;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(390, 36);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(65, 12);
            this.label8.TabIndex = 13;
            this.label8.Text = "合同类型：";
            // 
            // tbContractQuantity
            // 
            this.tbContractQuantity.Location = new System.Drawing.Point(120, 118);
            this.tbContractQuantity.Name = "tbContractQuantity";
            this.tbContractQuantity.ReadOnly = true;
            this.tbContractQuantity.Size = new System.Drawing.Size(175, 21);
            this.tbContractQuantity.TabIndex = 16;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(35, 121);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(77, 12);
            this.label9.TabIndex = 15;
            this.label9.Text = "合同总数量：";
            // 
            // tbContractValue
            // 
            this.tbContractValue.Location = new System.Drawing.Point(465, 74);
            this.tbContractValue.Name = "tbContractValue";
            this.tbContractValue.ReadOnly = true;
            this.tbContractValue.Size = new System.Drawing.Size(175, 21);
            this.tbContractValue.TabIndex = 18;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(377, 77);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(77, 12);
            this.label10.TabIndex = 17;
            this.label10.Text = "合同总金额：";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(32, 173);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(89, 12);
            this.label11.TabIndex = 19;
            this.label11.Text = "合同开始时间：";
            // 
            // dtpBeginTime
            // 
            this.dtpBeginTime.Enabled = false;
            this.dtpBeginTime.Location = new System.Drawing.Point(123, 167);
            this.dtpBeginTime.Name = "dtpBeginTime";
            this.dtpBeginTime.Size = new System.Drawing.Size(175, 21);
            this.dtpBeginTime.TabIndex = 20;
            // 
            // dtpEndTime
            // 
            this.dtpEndTime.Enabled = false;
            this.dtpEndTime.Location = new System.Drawing.Point(468, 167);
            this.dtpEndTime.Name = "dtpEndTime";
            this.dtpEndTime.Size = new System.Drawing.Size(172, 21);
            this.dtpEndTime.TabIndex = 22;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(377, 173);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(89, 12);
            this.label12.TabIndex = 21;
            this.label12.Text = "合同截止时间：";
            // 
            // supplierInforGroupBox
            // 
            this.groupBox1.Controls.Add(this.dtpCreateTime);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.tbContractVersion);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.lblCurrencyType);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.dtpEndTime);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.tbContractID);
            this.groupBox1.Controls.Add(this.dtpBeginTime);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.tbContractType);
            this.groupBox1.Controls.Add(this.tbContractValue);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.tbContractQuantity);
            this.groupBox1.Location = new System.Drawing.Point(13, 12);
            this.groupBox1.Name = "supplierInforGroupBox";
            this.groupBox1.Size = new System.Drawing.Size(749, 219);
            this.groupBox1.TabIndex = 23;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "合同基本数据";
            // 
            // lblCurrencyType
            // 
            this.lblCurrencyType.AutoSize = true;
            this.lblCurrencyType.Location = new System.Drawing.Point(655, 77);
            this.lblCurrencyType.Name = "lblCurrencyType";
            this.lblCurrencyType.Size = new System.Drawing.Size(23, 12);
            this.lblCurrencyType.TabIndex = 23;
            this.lblCurrencyType.Text = "CNY";
            // 
            // dtpAlertDate
            // 
            this.dtpAlertDate.Location = new System.Drawing.Point(240, 357);
            this.dtpAlertDate.Name = "dtpAlertDate";
            this.dtpAlertDate.Size = new System.Drawing.Size(175, 21);
            this.dtpAlertDate.TabIndex = 24;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(671, 358);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 25;
            this.btnSave.Text = "保存";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(35, 74);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 12);
            this.label2.TabIndex = 24;
            this.label2.Text = "合同版本号：";
            // 
            // tbContractVersion
            // 
            this.tbContractVersion.Location = new System.Drawing.Point(120, 71);
            this.tbContractVersion.Name = "tbContractVersion";
            this.tbContractVersion.ReadOnly = true;
            this.tbContractVersion.Size = new System.Drawing.Size(175, 21);
            this.tbContractVersion.TabIndex = 25;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(389, 121);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 12);
            this.label4.TabIndex = 26;
            this.label4.Text = "创建时间：";
            // 
            // dtpCreateTime
            // 
            this.dtpCreateTime.Enabled = false;
            this.dtpCreateTime.Location = new System.Drawing.Point(465, 115);
            this.dtpCreateTime.Name = "dtpCreateTime";
            this.dtpCreateTime.Size = new System.Drawing.Size(175, 21);
            this.dtpCreateTime.TabIndex = 27;
            // 
            // AgreementContractMonitorSettingForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(785, 471);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.dtpAlertDate);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.cbEnableDateMonitor);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.tbAlertQuantity);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.cbEnableQuantityMonitor);
            this.Controls.Add(this.lblCurrencyType2);
            this.Controls.Add(this.tbAlertValue);
            this.Controls.Add(this.cbEnableValueMonitor);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "AgreementContractMonitorSettingForm";
            this.Text = "协议合同监控设置";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox cbEnableValueMonitor;
        private System.Windows.Forms.TextBox tbAlertValue;
        private System.Windows.Forms.Label lblCurrencyType2;
        private System.Windows.Forms.CheckBox cbEnableQuantityMonitor;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbAlertQuantity;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox cbEnableDateMonitor;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tbContractID;
        private System.Windows.Forms.TextBox tbContractType;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox tbContractQuantity;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox tbContractValue;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.DateTimePicker dtpBeginTime;
        private System.Windows.Forms.DateTimePicker dtpEndTime;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lblCurrencyType;
        private System.Windows.Forms.DateTimePicker dtpAlertDate;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.TextBox tbContractVersion;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dtpCreateTime;
        private System.Windows.Forms.Label label4;
    }
}