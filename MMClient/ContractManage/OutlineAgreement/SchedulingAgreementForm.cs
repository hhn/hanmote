﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Model.ContractManage.OutlineAgreement;
using Lib.Bll.ContractManageBLL.OutlineAgreementBLL;
using WeifenLuo.WinFormsUI.Docking;
using Lib.Model.MD;
using Lib.Bll.MDBll.General;
using Lib.Common.CommonUtils;

namespace MMClient.ContractManage.OutlineAgreement
{
    public partial class SchedulingAgreementForm : DockContent
    {
        // 计划协议操作工具
        SchedulingAgreementBLL schedulingAgreementTool = new SchedulingAgreementBLL();
        // 主数据操作工具
        GeneralBLL generalTool = new GeneralBLL();
        // 计划协议对象
        Scheduling_Agreement schedulingAgreement = new Scheduling_Agreement();
        // 操作类型
        private string opType = "";

        public SchedulingAgreementForm()
        {
            InitializeComponent();
            opType = "new";
            //默认计划协议类型
            this.cbxAgreementType.SelectedIndex = 0;

            #region 数据初始化

            //货币类型ComboBox初始化
            List<String> currencyList = generalTool.GetCurrencyType();
            if (currencyList != null)
            {
                this.cbxCurrencyType.DataSource = currencyList;
                // 默认显示 CNY 项
                int cnyIndex = this.cbxCurrencyType.Items.IndexOf("CNY");
                if (cnyIndex != -1)
                {
                    this.cbxCurrencyType.SelectedIndex = cnyIndex;
                }
            }
            //供应商编号ComboBox初始化
            List<string> supplierIDList = generalTool.GetAllSupplier();
            if (supplierIDList != null)
            {
                this.cbxSupplierID.DataSource = supplierIDList;
            }
            //工厂ComboBox初始化
            List<string> factoryIDList = generalTool.GetAllFactory();
            if (factoryIDList != null)
            {
                this.cbxFactory.DataSource = factoryIDList;
            }
            //采购组ComboBox初始化
            List<string> buyerGroupList = generalTool.GetAllBuyerGroup();
            if (buyerGroupList != null)
            {
                this.cbxBuyerGroup.DataSource = buyerGroupList;
            }
            //采购组织ComboBox初始化
            List<string> buyerOrganizationList = generalTool.GetAllBuyerOrganization();
            if (buyerOrganizationList != null)
            {
                this.cbxBuyerOrganization.DataSource = buyerOrganizationList;
            }
            //付款条件ComboBox初始化
            List<string> paymentClauseList = generalTool.GetAllPaymentClauseName();
            if (paymentClauseList != null)
            {
                this.cbxPaymentClause.DataSource = paymentClauseList;
            }
            //
            List<string> tradeClauseList = generalTool.GetAllTradeClauseID();
            if(tradeClauseList != null)
            {
                this.cbTradeClause.DataSource = tradeClauseList;
            }
            #endregion

            //默认初始化一个编号
            string defaultContractID = "JHXY" + System.DateTime.Now.ToString("yyyyMMddHHmmss") + "2016001";
            this.tbAgreementID.Text = defaultContractID;
            disableMutualChange();
        }

        /// <summary>
        /// 有参构造函数
        /// </summary>
        /// <param name="schedulingAgreement"></param>
        /// <param name="opType"></param>
        public SchedulingAgreementForm(Scheduling_Agreement schedulingAgreement,
            string opType) {
            InitializeComponent();

            this.schedulingAgreement = schedulingAgreement;
            this.opType = opType;
            
            if (!opType.Equals("new")) {
                initialForm(schedulingAgreement);
                disabledControls(opType);
                disableMutualChange();
            }
        }

        /// <summary>
        /// 初始化Form
        /// </summary>
        /// <param name="schedulingAgreement"></param>
        private void initialForm(Scheduling_Agreement schedulingAgreement) {
            //货币类型ComboBox初始化
            List<String> currencyList = generalTool.GetCurrencyType();
            if (currencyList != null)
            {
                this.cbxCurrencyType.DataSource = currencyList;
                //显示原来选定的值
                int cnyIndex = this.cbxCurrencyType.Items.IndexOf(schedulingAgreement.Currency_Type);
                if (cnyIndex != -1)
                {
                    this.cbxCurrencyType.SelectedIndex = cnyIndex;
                }
            }
            //界面抬头
            switch (opType)
            {
                case "edit": this.Text = "更改计划协议"; break;
                case "display": this.Text = "显示计划协议"; break;
                default: break;
            }

            //读取已存在的数据
            //抬头数据
            this.tbAgreementID.Text = schedulingAgreement.Scheduling_Agreement_ID;
            this.cbxAgreementType.SelectedIndex = this.cbxAgreementType.Items.IndexOf(schedulingAgreement.Scheduling_Agreement_Type);
            //供应商编号ComboBox初始化
            List<string> supplierIDList = generalTool.GetAllSupplier();
            if (supplierIDList != null)
            {
                this.cbxSupplierID.DataSource = supplierIDList;
            }
            this.cbxSupplierID.SelectedIndex = this.cbxSupplierID.Items.IndexOf(schedulingAgreement.Supplier_ID);
            this.dtpCreateTime.Value = schedulingAgreement.Create_Time;
            this.dtpBeginTime.Value = schedulingAgreement.Begin_Time;
            this.dtpEndTime.Value = schedulingAgreement.End_Time;

            //参考数据
            //采购组ComboBox初始化
            List<string> buyerGroupList = generalTool.GetAllBuyerGroup();
            if (buyerGroupList != null)
            {
                this.cbxBuyerGroup.DataSource = buyerGroupList;
            }
            this.cbxBuyerGroup.SelectedIndex = this.cbxBuyerGroup.Items.IndexOf(schedulingAgreement.Purchase_Group);
            //采购组织ComboBox初始化
            List<string> buyerOrganizationList = generalTool.GetAllBuyerOrganization();
            if (buyerOrganizationList != null)
            {
                this.cbxBuyerOrganization.DataSource = buyerOrganizationList;
            }
            this.cbxBuyerOrganization.SelectedIndex = this.cbxBuyerOrganization.Items.IndexOf(schedulingAgreement.Purchase_Organization);
            //工厂ComboBox初始化
            List<string> factoryIDList = generalTool.GetAllFactory();
            if (factoryIDList != null)
            {
                this.cbxFactory.DataSource = factoryIDList;
            }
            this.cbxFactory.SelectedIndex = this.cbxFactory.Items.IndexOf(schedulingAgreement.Factory);
            this.cbxStorageLocation.SelectedIndex = this.cbxStorageLocation.Items.IndexOf(schedulingAgreement.Storage_Location);

            //交货和支付条件
            //付款码ComboBox初始化
            List<string> paymentClauseList = generalTool.GetAllPaymentClauseName();
            if (paymentClauseList != null)
            {
                this.cbxPaymentClause.DataSource = paymentClauseList;
            }
            this.cbxPaymentClause.SelectedIndex = this.cbxPaymentClause.Items.IndexOf(schedulingAgreement.Payment_Type);
            this.tbDays1.Text = convertNumToString(schedulingAgreement.Days1);
            this.tbDiscountRate1.Text = convertNumToString(schedulingAgreement.Discount_Rate1);
            this.tbDays2.Text = convertNumToString(schedulingAgreement.Days2);
            this.tbDiscountRate2.Text = convertNumToString(schedulingAgreement.Discount_Rate2);
            this.tbDays3.Text = convertNumToString(schedulingAgreement.Days3);

            this.tbTargetValue.Text = convertNumToString(schedulingAgreement.Target_Value);
            this.cbxCurrencyType.SelectedIndex = this.cbxCurrencyType.Items.IndexOf(
                schedulingAgreement.Currency_Type);
            //this.tbExchangeRate.Text = convertNumToString(schedulingAgreement.Exchange_Rate);
            List<string> tradeClauseList = generalTool.GetAllTradeClauseID();
            if (tradeClauseList != null)
            {
                this.cbTradeClause.DataSource = tradeClauseList;
            }
            this.cbTradeClause.SelectedIndex = this.cbTradeClause.Items.IndexOf(schedulingAgreement.IT_Term_Code);
            this.tbITTermText.Text = schedulingAgreement.IT_Term_Text;
        }

        /// <summary>
        /// 根据操作类型，将部分控件设为禁止修改
        /// </summary>
        /// <param name="opType"></param>
        private void disabledControls(string opType)
        {
            //根据操作：修改(edit)、变更(change)、展示(display)，分别 disabled 不同的控件
            this.tbAgreementID.Enabled = false;
            this.cbxAgreementType.Enabled = false;
            this.dtpCreateTime.Enabled = false;
            this.cbxSupplierID.Enabled = false;   

            this.cbxCurrencyType.Enabled = false;
            //this.tbExchangeRate.Enabled = false;
            this.cbTradeClause.Enabled = false;
            this.tbITTermText.Enabled = false;

            if (opType.Equals("display"))
            {
                this.dtpBeginTime.Enabled = false;
                this.dtpEndTime.Enabled = false;
                this.tbTargetValue.Enabled = false;
                this.cbxStorageLocation.Enabled = false;
                this.cbxBuyerGroup.Enabled = false;
                this.cbxBuyerOrganization.Enabled = false;
                this.cbxFactory.Enabled = false;
                this.cbxPaymentClause.Enabled = false;
                this.tbDays1.Enabled = false;
                this.tbDiscountRate1.Enabled = false;
                this.tbDays2.Enabled = false;
                this.tbDiscountRate2.Enabled = false;
                this.tbDays3.Enabled = false;
            }
        }

        /// <summary>
        /// 将Num转换为string
        /// </summary>
        /// <param name="num"></param>
        /// <returns></returns>
        private string convertNumToString(int num)
        {
            if (num == 0)
                return "";
            else
                return Convert.ToString(num);
        }
        private string convertNumToString(double num)
        {
            if (num == 0.0)
                return "";
            else
                return Convert.ToString(num);
        }

        /// <summary>
        /// 将string转换为double
        /// </summary>
        /// <param name="num"></param>
        /// <returns></returns>
        private double convertStringToDoubleNum(string num)
        {
            if (num.Equals(""))
                return 0.0;
            else
                return Convert.ToDouble(num);
        }

        private int convertStringToIntNum(string num)
        {
            if (num.Equals(""))
                return 0;
            else
                return Convert.ToInt32(num);
        }

        /// <summary>
        /// 点击下一步,进入项目总览界面
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnNext_Click(object sender, EventArgs e)
        {
            if (!opType.Equals("display")) {
                # region 读取信息
                schedulingAgreement.Scheduling_Agreement_ID = this.tbAgreementID.Text.Trim();
                schedulingAgreement.Scheduling_Agreement_Type = this.cbxAgreementType.Text;
                schedulingAgreement.Create_Time = this.dtpCreateTime.Value;
                schedulingAgreement.Supplier_ID = this.cbxSupplierID.Text.Trim();
                schedulingAgreement.Supplier_Name = "供应商名称";
                schedulingAgreement.Begin_Time = this.dtpBeginTime.Value;
                schedulingAgreement.End_Time = this.dtpEndTime.Value;

                schedulingAgreement.Purchase_Group = this.cbxBuyerGroup.Text.Trim();
                schedulingAgreement.Purchase_Organization = this.cbxBuyerOrganization.Text.Trim();
                schedulingAgreement.Factory = this.cbxFactory.Text.Trim();
                schedulingAgreement.Storage_Location = this.cbxStorageLocation.Text.Trim();

                //还要考虑异常处理
                schedulingAgreement.Payment_Type = this.cbxPaymentClause.Text.Trim();
                schedulingAgreement.Days1 = convertStringToIntNum(this.tbDays1.Text.Trim());
                schedulingAgreement.Discount_Rate1 = convertStringToDoubleNum(this.tbDiscountRate1.Text.Trim());
                schedulingAgreement.Days2 = convertStringToIntNum(this.tbDays2.Text.Trim());
                schedulingAgreement.Discount_Rate2 = convertStringToDoubleNum(this.tbDiscountRate2.Text.Trim());
                schedulingAgreement.Days3 = convertStringToIntNum(this.tbDays3.Text.Trim());
                schedulingAgreement.Target_Value = convertStringToDoubleNum(this.tbTargetValue.Text.Trim());
                schedulingAgreement.Currency_Type = this.cbxCurrencyType.Text.Trim();
                //schedulingAgreement.Exchange_Rate = convertStringToDoubleNum(this.tbExchangeRate.Text.Trim());
                schedulingAgreement.IT_Term_Code = this.cbTradeClause.Text.Trim();
                schedulingAgreement.IT_Term_Text = this.tbITTermText.Text.Trim();

                if (opType.Equals("new"))
                {
                    //计划协议初始 status 为 "待审核"
                    schedulingAgreement.Status = "待审核";
                    //记录创建用户, 先默认"2015001"
                    schedulingAgreement.Creater_ID = "2015001";
                }

                //有效时间区间是否合法
                if (schedulingAgreement.Begin_Time > schedulingAgreement.End_Time)
                {
                    MessageBox.Show("请正确填写时间", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                //必填项不能为空
                if (schedulingAgreement.Scheduling_Agreement_ID.Equals("")
                    || schedulingAgreement.Supplier_ID.Equals("")
                    || schedulingAgreement.Purchase_Group.Equals("")
                    || schedulingAgreement.Purchase_Organization.Equals("")
                    || schedulingAgreement.Factory.Equals("")
                    || schedulingAgreement.IT_Term_Code.Equals(""))
                {

                    MessageBox.Show("请填写完整", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                # endregion

                //查看当前协议ID是否已存在
                if (opType.Equals("new"))
                {
                    Scheduling_Agreement checkResult = schedulingAgreementTool.getSchedulingAgreementByID(
                        schedulingAgreement.Scheduling_Agreement_ID);
                    if (checkResult != null)
                    {
                        MessageBox.Show("该ID计划协议已存在", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }
            }
            
            //隐藏前一个界面
            this.Hide();
            //进入项目总览界面
            SchedulingAgreementItemForm itemForm = new SchedulingAgreementItemForm(schedulingAgreement, opType, this);
            UserUI userUI = SingletonUserUI.getUserUI();
            userUI.displayOnDockPanel(itemForm);
        }

        /// <summary>
        /// 工厂变化后改变仓库
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cbxFactory_SelectedIndexChanged(object sender, EventArgs e)
        {
            string factory = this.cbxFactory.Text;
            if (!factory.Equals(""))
            {
                List<string> storageLocationList = generalTool.GetAllStock(factory);
                this.cbxStorageLocation.DataSource = storageLocationList;
            }
            else
            {
                this.cbxStorageLocation.Items.Clear();
            }
        }

        private void cbxBuyerGroup_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// 防止手动修改付款条件
        /// 付款条件只能通过付款码选取
        /// </summary>
        private void disableMutualChange()
        {
            this.tbDays1.Enabled = false;
            this.tbDays2.Enabled = false;
            this.tbDays3.Enabled = false;
            this.tbDiscountRate1.Enabled = false;
            this.tbDiscountRate2.Enabled = false;
        }

        /// <summary>
        /// 付款码改变时付款条件也随之改变
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cbxPaymentClause_SelectedIndexChanged(object sender, EventArgs e)
        {
            string paymentTypeID = this.cbxPaymentClause.Text.Trim();
            if (!paymentTypeID.Equals(""))
            {
                //从主数据表中读取付款条件
                generalTool = new GeneralBLL();
                PaymentClause info = generalTool.GetClause(paymentTypeID);
                if (info != null)
                {
                    this.tbDays1.Text = info.First_Date.ToString();
                    this.tbDiscountRate1.Text = info.First_Discount.ToString();
                    this.tbDays2.Text = info.Second_Date.ToString();
                    this.tbDiscountRate2.Text = info.Second_Discount.ToString();
                    this.tbDays3.Text = info.Final_Date.ToString();
                }
                else
                {
                    MessageUtil.ShowError("付款条件不存在！");
                }
            }
            else
            {
                this.tbDays1.Text = "";
                this.tbDiscountRate1.Text = "";
                this.tbDays2.Text = "";
                this.tbDiscountRate2.Text = "";
                this.tbDays3.Text = "";
            }
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void cbTradeClause_SelectedIndexChanged(object sender, EventArgs e)
        {
            string tradClauseID = this.cbTradeClause.Text.Trim();
            if (!tradClauseID.Equals(""))
            {
                DataTable dt = generalTool.GetTradeClauseByID(tradClauseID);
                if (dt != null)
                {
                    string tradeClauseName = Convert.IsDBNull(dt.Rows[0]["TradeClause_Name"]) ? "" : dt.Rows[0]["TradeClause_Name"].ToString();
                    this.tbITTermText.Text = tradeClauseName;
                }
                else
                {
                    MessageUtil.ShowError("支付条件不存在！");
                }
            }
            else
            {
                this.tbITTermText.Text = "";
            }
        }
    }
}
