﻿namespace MMClient.ContractManage.QuotaArrangement
{
    partial class QuotaArrangementInitialForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSearch = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cbxMaterialID = new System.Windows.Forms.ComboBox();
            this.cbxFactoryID = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(256, 81);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(27, 23);
            this.btnSearch.TabIndex = 19;
            this.btnSearch.Text = "√";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(39, 129);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 12);
            this.label2.TabIndex = 17;
            this.label2.Text = "工厂：";
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(39, 86);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(41, 12);
            this.label1.TabIndex = 15;
            this.label1.Text = "物料：";
            // 
            // cbxMaterialID
            // 
            this.cbxMaterialID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxMaterialID.FormattingEnabled = true;
            this.cbxMaterialID.Location = new System.Drawing.Point(86, 83);
            this.cbxMaterialID.Name = "cbxMaterialID";
            this.cbxMaterialID.Size = new System.Drawing.Size(164, 20);
            this.cbxMaterialID.TabIndex = 17;
            this.cbxMaterialID.SelectedIndexChanged += new System.EventHandler(this.cbxMaterialID_SelectedIndexChanged);
            // 
            // cbxFactoryID
            // 
            this.cbxFactoryID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxFactoryID.FormattingEnabled = true;
            this.cbxFactoryID.Location = new System.Drawing.Point(86, 126);
            this.cbxFactoryID.Name = "cbxFactoryID";
            this.cbxFactoryID.Size = new System.Drawing.Size(164, 20);
            this.cbxFactoryID.TabIndex = 18;
            // 
            // QuotaArrangementInitialForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(369, 362);
            this.Controls.Add(this.cbxFactoryID);
            this.Controls.Add(this.cbxMaterialID);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "QuotaArrangementInitialForm";
            this.Text = "配额协议 初始屏幕";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbxMaterialID;
        private System.Windows.Forms.ComboBox cbxFactoryID;
    }
}