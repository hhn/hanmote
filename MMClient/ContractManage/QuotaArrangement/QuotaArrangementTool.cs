﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.Model.ContractManage.QuotaArrangement;
using Lib.Bll.ContractManageBLL.QuotaArrangementBLL;
using Lib.Common.CommonUtils;
using System.Data;
using Lib.SqlServerDAL;

namespace MMClient.ContractManage.QuotaArrangement
{
    /// <summary>
    /// 提供用户来获取配额协议的计算结果
    /// </summary>
    public class QuotaArrangementTool
    {
        private QuotaArrangementBLL quotaArrangementTool =
            new QuotaArrangementBLL();                      //配额协议查询工具

        /// <summary>
        /// 调用指定的配额协议，获得供应商分配结果
        /// </summary>
        /// <param name="materialID">物料编号</param>
        /// <param name="factoryID">工厂编号</param>
        /// <param name="quotaArrangementID">配额协议编号</param>
        /// <param name="requestAmount">申请数量</param>
        /// <returns>分配结果</returns>
        public List<KeyValuePair<string, double>> getAllocateResult(
            Quota_Arrangement quotaArrangement, double requestAmount) {
            //暂时不考虑某一区间的最大送货量

            List<KeyValuePair<string, double>> resultList =
                new List<KeyValuePair<string, double>>();
            
            //确定计算类型
            string calType = quotaArrangement.Cal_Type;
            List<Quota_Arrangement_Item> itemList =
                quotaArrangementTool.getQuotaArrangementItemList(
                quotaArrangement.Quota_Arrangement_ID);
            
            List<Quota_Arrangement_Item> sortedItemList = null;
            if (calType.Equals("Priority"))
            {
                //根据优先级排序
                sortedItemList = itemList.OrderBy(s => s.Pr).ToList();
            }
            else if (calType.Equals("Quota")) {
                //计算配额值
                foreach (Quota_Arrangement_Item item in itemList) {
                    item.Quota_Cal_Value = (item.Allocate_Amount + item.Quota_Base_Amount)
                        / item.Quota_Value;
                }
                //根据配额值排序
                sortedItemList = itemList.OrderBy(s => s.Quota_Cal_Value).ToList();
            }

            #region 分配
            
            foreach (Quota_Arrangement_Item item in sortedItemList)
            {
                //是否已经全部分配
                if (MathUtil.equalsZero(requestAmount) || requestAmount < 0)
                    break;
                //已分配的数量 > 最大分配数量
                if (item.Allocate_Amount > item.Max_Amount)
                    continue;
                double allocate = requestAmount;
                //设置了 最小批量尺寸限制
                if (!MathUtil.equalsZero(item.Min_Batch_Size)
                    && item.Min_Batch_Size > allocate)
                {
                    continue;
                }
                //设置了最大交付数量限制

                //设置了 最大批量尺寸限制
                if (!MathUtil.equalsZero(item.Max_Batch_Size))
                {
                    //超过最大批量尺寸限制
                    if (allocate > item.Max_Batch_Size)
                    {
                        //设置了 单次订单限制
                        if (item.One_Time.Equals("True"))
                        {
                            allocate = item.Max_Batch_Size;
                            KeyValuePair<string, double> allocateInfo =
                                    new KeyValuePair<string, double>(item.Supplier_ID, allocate);
                            requestAmount -= allocate;
                            resultList.Add(allocateInfo);
                        }
                        else
                        {
                            //可能会产生多个PO
                            int PONum = Convert.ToInt32(allocate / item.Max_Batch_Size);
                            for (int i = 0; i < PONum; i++)
                            {
                                KeyValuePair<string, double> allocateInfo =
                                    new KeyValuePair<string, double>(item.Supplier_ID, item.Max_Batch_Size);
                                requestAmount -= item.Max_Batch_Size;
                                resultList.Add(allocateInfo);
                            }
                            //有余数
                            if (!MathUtil.equalsZero(allocate % item.Max_Batch_Size))
                            {
                                double value = allocate % item.Max_Batch_Size;
                                //设置了 最小包装数
                                if (!MathUtil.equalsZero(item.RPro))
                                {
                                    value = item.RPro * Math.Ceiling(value / item.RPro);
                                }
                                KeyValuePair<string, double> allocateInfo =
                                    new KeyValuePair<string, double>(item.Supplier_ID, value);
                                resultList.Add(allocateInfo);
                                requestAmount -= value;
                            }
                        }
                    }
                    else
                    {
                        //未超过
                        KeyValuePair<string, double> allocateInfo =
                                    new KeyValuePair<string, double>(item.Supplier_ID, allocate);
                        requestAmount -= allocate;
                        resultList.Add(allocateInfo);
                    }
                }
                else
                {
                    //没有 最大批量尺寸限制
                    KeyValuePair<string, double> allocateInfo =
                                    new KeyValuePair<string, double>(item.Supplier_ID, allocate);
                    requestAmount -= allocate;
                    resultList.Add(allocateInfo);
                }

                //更新数据库 已分配的数量
                item.Allocate_Amount += allocate;
            }

            //更新数据库
            quotaArrangementTool.addQuotaArrangementItemList(quotaArrangement.Quota_Arrangement_ID,
                sortedItemList);

            #endregion

            return resultList;
        }

        //从源清单SourceList中获取物料id
        public List<string> getQuotaBasicInformation()
        {
            string sql = "select distinct(materialId) from SourceList";
            List<string> result = new List<string>();
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            if (dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    result.Add(dt.Rows[i][0].ToString());
                }
            }
            return result;
        }

        //根据物料id从源清单中获取对应的工厂id
        public List<string> getFactoryIdByMaterial(string materialId)
        {
            string sql = "select distinct(factoryId) from SourceList where materialId = '" + materialId + "'";
            List<string> result = new List<string>();
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            if (dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    result.Add(dt.Rows[i][0].ToString());
                }
            }
            return result;
        }

        //根据工厂编号获得工厂名称
        public List<string> getFactoryNameByFactoryId(string factoryId)
        {
            string sql = "select Factory_Name from Factory where Factory_ID = '" + factoryId + "'";
            List<string> result = new List<string>();
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            if (dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    result.Add(dt.Rows[i][0].ToString());
                }
            }
            return result;
        }

        //根据物料编号获得物料名称和物料单位
        public DataTable getMaterailInformationByMaterialId(string materialId)
        {
            string sql = "select Material_Name,Measurement from Material where Material_ID = '" + materialId + "'";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            return dt;
        }

        //根据物料编号和工厂编号获得对应的供应商ID列表
        public List<string> getSupplierByQuota(string materialID, string factoryID)
        {
            string sql = "select supplierId from SourceList where materialId = '" + materialID + "' and factoryId = '" + factoryID + "'";
            List<string> result = new List<string>();
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            if (dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    result.Add(dt.Rows[i][0].ToString());
                }
            }
            return result;
        }

        //根据供应商编号读取最大订货批量和最小订货批量
        public DataTable getBatchSizeByID(string supplierId)
        {
            string sql = "select Max_Batch_Size,Min_Batch_Size from Supplier_Base where Supplier_ID = '" + supplierId + "'";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            return dt;
        }
    }
}
