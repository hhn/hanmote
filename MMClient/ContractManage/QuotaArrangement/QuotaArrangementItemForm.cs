﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;
using Lib.Model.ContractManage.QuotaArrangement;
using Lib.Bll.ContractManageBLL.QuotaArrangementBLL;
using Lib.Common.CommonUtils;

namespace MMClient.ContractManage.QuotaArrangement
{
    public partial class QuotaArrangementItemForm : DockContent
    {
        private Quota_Arrangement quotaArrangement;                     //当前配额协议
        private List<Quota_Arrangement_Item> quotaArrangementItemList;  //配额协议项
        private QuotaArrangementBLL quotaArrangementTool 
            = new QuotaArrangementBLL();                                //配额协议操作工具
        private QuotaArrangementForm parentForm;                        //父窗体
        private List<Quota_Arrangement_Item> itemList = new List<Quota_Arrangement_Item>();

        public QuotaArrangementItemForm(Quota_Arrangement _quotaArrangement,
            QuotaArrangementForm _parentForm,List<Quota_Arrangement_Item> _itemList)
        {
            InitializeComponent();

            this.quotaArrangement = _quotaArrangement;
            this.parentForm = _parentForm;
            this.itemList = _itemList;
            initialForm();
        }

        /// <summary>
        /// 初始化Form
        /// 待修改*****************************
        /// </summary>
        private void initialForm() { 
            //抬头数据
            this.tbMaterialID.Text = quotaArrangement.Material_ID;
            this.lblMaterialName.Text = quotaArrangement.Material_Name;
            //this.lblMaterialName.Text = "铝锭";
            this.tbFactoryID.Text = quotaArrangement.Factory_ID;
            this.lblFactoryName.Text = quotaArrangement.Factory_Name;
            //this.lblFactoryName.Text = "武汉区工厂";
            this.tbQuotaArrangementID.Text = quotaArrangement.Quota_Arrangement_ID;
            this.tbOUn.Text = quotaArrangement.OUn;
            this.dtpBeginTime.Value = quotaArrangement.Begin_Time;
            this.dtpEndTime.Value = quotaArrangement.End_Time;
            this.dtpCreateTime.Value = quotaArrangement.Create_Time;
            this.tbMinQuantityResolve.Text = Convert.ToString(quotaArrangement.Min_Quantity_Resolve);
            this.tbCreatorID.Text = quotaArrangement.Creator_ID;

            //配额协议项数据
            if (itemList != null && itemList.Count != 0)
                quotaArrangementItemList = itemList;
            else
                quotaArrangementItemList = quotaArrangementTool.getQuotaArrangementItemList(
                quotaArrangement.Quota_Arrangement_ID);
            displayItemOnDataGridView(quotaArrangementItemList);
        }

        /// <summary>
        /// 展示在DataGridview上
        /// </summary>
        /// <param name="itemList"></param>
        private void displayItemOnDataGridView(List<Quota_Arrangement_Item> itemList) {
            this.dgvDisplay.Rows.Clear();
            int i = 0;
            foreach (Quota_Arrangement_Item item in itemList) {
                this.dgvDisplay.Rows.Add();
                DataGridViewRow row = this.dgvDisplay.Rows[i];
                row.Cells[0].Value = item.Item_Num;
                row.Cells[1].Value = item.P;
                row.Cells[2].Value = item.S;
                row.Cells[3].Value = item.Supplier_ID;
                row.Cells[4].Value = item.PP1;
                row.Cells[5].Value = item.Quota_Value;
                row.Cells[6].Value = item.Quota_Percentage + "%";
                row.Cells[7].Value = item.Allocate_Amount;
                row.Cells[8].Value = item.Max_Amount;
                row.Cells[9].Value = item.Quota_Base_Amount;
                row.Cells[10].Value = item.Max_Batch_Size;
                row.Cells[11].Value = item.Min_Batch_Size;
                row.Cells[12].Value = item.RPro;
                row.Cells[13].Value = item.One_Time;
                row.Cells[14].Value = item.Max_Delivery_Amount;
                row.Cells[15].Value = item.Period;
                row.Cells[16].Value = item.Pr;

                i++;
            }
        }

        /// <summary>
        /// 增加
        /// 需修改******************************
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAdd_Click(object sender, EventArgs e)
        {
            this.dgvDisplay.Rows.Add();
            //项目号
            int rowCount = this.dgvDisplay.Rows.Count;
            DataGridViewRow row = 
                this.dgvDisplay.Rows[rowCount - 1];
            if (rowCount == 1)
            {
                row.Cells[0].Value = 1;
            }
            else
            {
                row.Cells[0].Value =
                    Convert.ToInt32(this.dgvDisplay.Rows[rowCount - 2].Cells[0].Value) + 1;
            }
            //配额为0.0
            row.Cells[5].Value = "0.0";
        }

        /// <summary>
        /// 删除一行
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (this.dgvDisplay.Rows.Count == 0)
            {
                MessageBox.Show("没有可删除内容", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            int rowIndex = this.dgvDisplay.CurrentCell.RowIndex;
            if (rowIndex >= 0 && rowIndex < this.dgvDisplay.Rows.Count)
            {
                this.dgvDisplay.Rows.RemoveAt(rowIndex);
                updateRowNum();
            }
        }

        /// <summary>
        /// 刷新行的序号
        /// </summary>
        private void updateRowNum()
        {
            int i = 1;
            foreach (DataGridViewRow row in this.dgvDisplay.Rows)
            {
                row.Cells[0].Value = i;
                i++;
            }
        }

        /// <summary>
        /// 点击保存
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSave_Click(object sender, EventArgs e)
        {
            # region 读取合理的数据
            List<Quota_Arrangement_Item> itemList = 
                new List<Quota_Arrangement_Item>();
            //确定计算方式
            string opType = "";
            foreach (DataGridViewRow row in this.dgvDisplay.Rows) {
                Quota_Arrangement_Item item = new Quota_Arrangement_Item();
                item.Quota_Arrangement_ID = quotaArrangement.Quota_Arrangement_ID;
                item.Item_Num = Convert.ToInt32(row.Cells[0].Value);
                item.Material_ID = quotaArrangement.Material_ID;
                item.Factory_ID = quotaArrangement.Factory_ID;
                item.P = DataGridViewCellTool.
                    getDataGridViewCellValueString(row.Cells[1]);
                item.S = DataGridViewCellTool.
                    getDataGridViewCellValueString(row.Cells[2]);
                item.Supplier_ID = DataGridViewCellTool.
                    getDataGridViewCellValueString(row.Cells[3]);
                item.PP1 = DataGridViewCellTool.
                    getDataGridViewCellValueString(row.Cells[4]);
                item.Quota_Value = DataGridViewCellTool.
                    getDataGridViewCellValueDouble(row.Cells[5]);
                //百分比要去掉 %
                string percentageStr = DataGridViewCellTool.
                    getDataGridViewCellValueString(row.Cells[6]).
                    Replace("%", "");
                item.Quota_Percentage = percentageStr.Equals("") ?
                    0.0 : Convert.ToDouble(percentageStr);
                item.Allocate_Amount = DataGridViewCellTool.
                    getDataGridViewCellValueDouble(row.Cells[7]);
                item.Max_Amount = DataGridViewCellTool.
                    getDataGridViewCellValueDouble(row.Cells[8]);
                item.Quota_Base_Amount = DataGridViewCellTool.
                    getDataGridViewCellValueDouble(row.Cells[9]);
                item.Max_Batch_Size = DataGridViewCellTool.
                    getDataGridViewCellValueDouble(row.Cells[10]);
                item.Min_Batch_Size = DataGridViewCellTool.
                    getDataGridViewCellValueDouble(row.Cells[11]);
                item.RPro = DataGridViewCellTool.
                    getDataGridViewCellValueDouble(row.Cells[12]);
                //CheckBoxColumn的Value为True 和 False
                item.One_Time = row.Cells[13].EditedFormattedValue.ToString();
                item.Max_Delivery_Amount = DataGridViewCellTool.
                    getDataGridViewCellValueDouble(row.Cells[14]);
                item.Period = DataGridViewCellTool.
                    getDataGridViewCellValueString(row.Cells[15]);
                item.Pr = DataGridViewCellTool.
                    getDataGridViewCellValueInt(row.Cells[16]);
                //明确是采用Priority还是Quota
                //如果有变化，更新配额协议的Cal_Type
                if(opType.Equals("")){
                    if(item.Pr == 0)
                        opType = "Quota";
                    else
                        opType = "Priority";
                }
                else if(opType.Equals("Quota") && item.Pr > 0
                    || opType.Equals("Priority") && item.Pr == 0){
                    MessageUtil.ShowError("请正确填写");
                    return;
                }

                itemList.Add(item);
            }

            //检查合理性
            foreach (Quota_Arrangement_Item item in itemList) { 
                //Supplier_ID必须有
                if (item.Supplier_ID.Equals("")) {
                    MessageUtil.ShowError("填写错误");
                    return;
                }
                if (opType.Equals("Quota")) { 
                    //配额必须填写
                    if (MathUtil.equalsZero(item.Quota_Value)) {
                        MessageUtil.ShowError("请填写配额");
                        return;
                    }
                }
                //如果设置了 RPro，那么RPro和Max_Batch_Size应该一致
                if (!MathUtil.equalsZero(item.RPro)
                    && !MathUtil.equalsZero(item.Max_Batch_Size)) {
                    
                    double tmp = item.Max_Batch_Size % item.RPro;
                    if (!MathUtil.equalsZero(tmp)) {
                        MessageUtil.ShowError("RPro和最大批量尺寸请保持一致");
                        return;
                    }
                }
            }

            #endregion 

            # region 保存
            
            //更新Quota_Arrangement
            quotaArrangementTool.updateQuotaArrangement(quotaArrangement);

            //更新Quota_Arrangement_Item
            int num = quotaArrangementTool.addQuotaArrangementItemList(
                quotaArrangement.Quota_Arrangement_ID,
                itemList);
            if (num > 0)
            {
                MessageUtil.ShowTips("保存成功");
            }
            else {
                MessageUtil.ShowTips("保存失败");
            }

            #endregion
        }

        /// <summary>
        /// 单元格Value变化触发的事件
        /// 待修改，计算时要考虑配额基数***************************
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvDisplay_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (this.dgvDisplay.Rows.Count <= 0)
                return;

            //根据填写的配额自动计算配额百分比
            string columnName = this.dgvDisplay.Columns[e.ColumnIndex].Name;
            if (columnName.Equals("Quota_Value")) {
                List<double> quotaValueList = new List<double>();
                double sumValue = 0.0;
                //读取每个配额
                try
                {
                    foreach (DataGridViewRow row in this.dgvDisplay.Rows)
                    {
                        double value = DataGridViewCellTool.
                            getDataGridViewCellValueDouble(row.Cells[5]);
                        sumValue += value;
                        quotaValueList.Add(value);
                    }
                }
                catch (Exception) { 
                    //数据填写错误
                    return;
                }
                //填写百分比
                if (MathUtil.equalsZero(sumValue)) {
                    return;
                }
                int i = 0;
                foreach (DataGridViewRow row in this.dgvDisplay.Rows) {
                    string percentage =
                        String.Format("{0:0.00}", (quotaValueList[i] / sumValue) * 100)
                        + "%";
                    row.Cells[6].Value = percentage;
                    i++;
                }
            }
        }

        /// <summary>
        /// 关闭窗体
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void QuotaArrangementItemForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (this.parentForm != null && !this.parentForm.IsDisposed)
                this.parentForm.Close();
        }

        private void QuotaArrangementItemForm_Load(object sender, EventArgs e)
        {

        }

        private void dgvDisplay_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
