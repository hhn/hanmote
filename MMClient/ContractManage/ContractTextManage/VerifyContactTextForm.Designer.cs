﻿namespace MMClient.ContractManage.ContractTextManage
{
    partial class VerifyContactTextForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panelDatagridView = new System.Windows.Forms.Panel();
            this.dgvVerifyContractText = new System.Windows.Forms.DataGridView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnSearch = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.cbxBuyer = new System.Windows.Forms.ComboBox();
            this.btnSubmitVerify = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.panelDatagridView.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVerifyContractText)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelDatagridView
            // 
            this.panelDatagridView.Controls.Add(this.dgvVerifyContractText);
            this.panelDatagridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelDatagridView.Location = new System.Drawing.Point(0, 83);
            this.panelDatagridView.Name = "panelDatagridView";
            this.panelDatagridView.Size = new System.Drawing.Size(796, 413);
            this.panelDatagridView.TabIndex = 13;
            // 
            // dgvVerifyContractText
            // 
            this.dgvVerifyContractText.AllowUserToAddRows = false;
            this.dgvVerifyContractText.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvVerifyContractText.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvVerifyContractText.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvVerifyContractText.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvVerifyContractText.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvVerifyContractText.Location = new System.Drawing.Point(0, 0);
            this.dgvVerifyContractText.Name = "dgvVerifyContractText";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvVerifyContractText.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvVerifyContractText.RowHeadersVisible = false;
            this.dgvVerifyContractText.RowTemplate.Height = 23;
            this.dgvVerifyContractText.Size = new System.Drawing.Size(796, 413);
            this.dgvVerifyContractText.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnSearch);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.cbxBuyer);
            this.panel1.Controls.Add(this.btnSubmitVerify);
            this.panel1.Controls.Add(this.btnSave);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(796, 83);
            this.panel1.TabIndex = 12;
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(250, 33);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(75, 23);
            this.btnSearch.TabIndex = 10;
            this.btnSearch.Text = "查询";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 39);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 6;
            this.label1.Text = "采购员：";
            // 
            // cbxBuyer
            // 
            this.cbxBuyer.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxBuyer.FormattingEnabled = true;
            this.cbxBuyer.Items.AddRange(new object[] {
            "全部人员"});
            this.cbxBuyer.Location = new System.Drawing.Point(72, 36);
            this.cbxBuyer.Name = "cbxBuyer";
            this.cbxBuyer.Size = new System.Drawing.Size(121, 20);
            this.cbxBuyer.TabIndex = 7;
            // 
            // btnSubmitVerify
            // 
            this.btnSubmitVerify.Location = new System.Drawing.Point(586, 33);
            this.btnSubmitVerify.Name = "btnSubmitVerify";
            this.btnSubmitVerify.Size = new System.Drawing.Size(75, 23);
            this.btnSubmitVerify.TabIndex = 9;
            this.btnSubmitVerify.Text = "提交审核";
            this.btnSubmitVerify.UseVisualStyleBackColor = true;
            this.btnSubmitVerify.Click += new System.EventHandler(this.btnSubmitVerify_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(415, 33);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 8;
            this.btnSave.Text = "保存";
            this.btnSave.UseVisualStyleBackColor = true;
            // 
            // VerifyContactTextForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(796, 496);
            this.Controls.Add(this.panelDatagridView);
            this.Controls.Add(this.panel1);
            this.Name = "VerifyContactTextForm";
            this.Text = "审核合同";
            this.Load += new System.EventHandler(this.VerifyContactTextForm_Load);
            this.panelDatagridView.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvVerifyContractText)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Button btnSubmitVerify;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.ComboBox cbxBuyer;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panelDatagridView;
        private System.Windows.Forms.DataGridView dgvVerifyContractText;
    }
}