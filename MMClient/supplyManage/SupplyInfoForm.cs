﻿using Lib.SqlServerDAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
namespace MMClient.supplyManage
{
    public partial class SupplyInfoForm : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        public SupplyInfoForm()
        {
            InitializeComponent();
        }

         DataTable getSupplierId() {
            List<String> supplierIdList = new List<string>();
            DataTable dt =DBHelper.ExecuteQueryDT("select distinct SI.supplierId,sb.Supplier_Name from TabSupplyInfo SI,Supplier_Base sb WHERE   SI.SupplierId=sb.Supplier_ID ");
            return dt;
        }

        private void SupplyInfoForm_Load(object sender, EventArgs e)
        {
            this.CB_supplierId.DisplayMember = "Supplier_Name";
            this.CB_supplierId.ValueMember = "supplierId";
            this.CB_supplierId.DataSource = getSupplierId();
            
        }

   

        DataTable getSupplyInfo() {
            String sql = "select distinct mtGroupName,mtGroupId,mtId,mtName,ModifyTime from TabSupplyInfo where supplierId='" + this.CB_supplierId.SelectedValue.ToString()+"' ";
            return DBHelper.ExecuteQueryDT(sql);

        }

        private void CB_supplierId_SelectedIndexChanged(object sender, EventArgs e)
        {
           
            this.dgv_SupplyInfo.DataSource = getSupplyInfo();


        }
    }
}
