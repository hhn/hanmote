﻿namespace MMClient.supplyManage
{
    partial class SupplyInfoForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgv_SupplyInfo = new System.Windows.Forms.DataGridView();
            this.mtGroupId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mtGroupName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mtId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mtName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.time = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            this.CB_supplierId = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_SupplyInfo)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgv_SupplyInfo
            // 
            this.dgv_SupplyInfo.AllowUserToAddRows = false;
            this.dgv_SupplyInfo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.dgv_SupplyInfo.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_SupplyInfo.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.dgv_SupplyInfo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgv_SupplyInfo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_SupplyInfo.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.mtGroupId,
            this.mtGroupName,
            this.mtId,
            this.mtName,
            this.time});
            this.dgv_SupplyInfo.Location = new System.Drawing.Point(10, 73);
            this.dgv_SupplyInfo.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dgv_SupplyInfo.Name = "dgv_SupplyInfo";
            this.dgv_SupplyInfo.RowTemplate.Height = 27;
            this.dgv_SupplyInfo.Size = new System.Drawing.Size(760, 410);
            this.dgv_SupplyInfo.TabIndex = 0;
            // 
            // mtGroupId
            // 
            this.mtGroupId.DataPropertyName = "mtGroupId";
            this.mtGroupId.HeaderText = "物料组编号";
            this.mtGroupId.Name = "mtGroupId";
            // 
            // mtGroupName
            // 
            this.mtGroupName.DataPropertyName = "mtGroupName";
            this.mtGroupName.HeaderText = "物料组名称";
            this.mtGroupName.Name = "mtGroupName";
            // 
            // mtId
            // 
            this.mtId.DataPropertyName = "mtId";
            this.mtId.HeaderText = "物料编号";
            this.mtId.Name = "mtId";
            // 
            // mtName
            // 
            this.mtName.DataPropertyName = "mtName";
            this.mtName.HeaderText = "物料名称";
            this.mtName.Name = "mtName";
            // 
            // time
            // 
            this.time.DataPropertyName = "ModifyTime";
            this.time.HeaderText = "修改时间";
            this.time.Name = "time";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.CB_supplierId);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(10, 10);
            this.panel1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(760, 58);
            this.panel1.TabIndex = 1;
            // 
            // CB_supplierId
            // 
            this.CB_supplierId.FormattingEnabled = true;
            this.CB_supplierId.Location = new System.Drawing.Point(83, 24);
            this.CB_supplierId.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.CB_supplierId.Name = "CB_supplierId";
            this.CB_supplierId.Size = new System.Drawing.Size(130, 20);
            this.CB_supplierId.TabIndex = 1;
            this.CB_supplierId.SelectedIndexChanged += new System.EventHandler(this.CB_supplierId_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 26);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "供应商名称:";
            // 
            // SupplyInfoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(778, 485);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.dgv_SupplyInfo);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "SupplyInfoForm";
            this.Text = "供货信息查看";
            this.Load += new System.EventHandler(this.SupplyInfoForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_SupplyInfo)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgv_SupplyInfo;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox CB_supplierId;
        private System.Windows.Forms.DataGridViewTextBoxColumn mtGroupId;
        private System.Windows.Forms.DataGridViewTextBoxColumn mtGroupName;
        private System.Windows.Forms.DataGridViewTextBoxColumn mtId;
        private System.Windows.Forms.DataGridViewTextBoxColumn mtName;
        private System.Windows.Forms.DataGridViewTextBoxColumn time;
    }
}