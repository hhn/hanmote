﻿namespace MMClient.SourcingManage.ProcurementPlan
{
    partial class NewSummaryDemand
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.demandIdLabel = new System.Windows.Forms.Label();
            this.demandIdText = new System.Windows.Forms.TextBox();
            this.materialGridView = new System.Windows.Forms.DataGridView();
            this.materialId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.materialName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.materialGroup = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.applyNum = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.measurement = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.stockId = new MDBll.Batch.DataGridViewComboEditBoxColumn();
            this.DeliveryStartTime = new Lib.Common.CommonControls.CalendarColumn();
            this.DeliveryEndTime = new Lib.Common.CommonControls.CalendarColumn();
            this.phoneLabel = new System.Windows.Forms.Label();
            this.phoneText = new System.Windows.Forms.TextBox();
            this.departmentLabel = new System.Windows.Forms.Label();
            this.proposer = new System.Windows.Forms.Label();
            this.txt_Proposer = new System.Windows.Forms.TextBox();
            this.applyTime = new System.Windows.Forms.Label();
            this.purchaseTypeLabel = new System.Windows.Forms.Label();
            this.purchaseTypeCB = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.logisticsCB = new System.Windows.Forms.ComboBox();
            this.submitBtn = new System.Windows.Forms.Button();
            this.checkBtn = new System.Windows.Forms.Button();
            this.dtp_apply = new System.Windows.Forms.DateTimePicker();
            this.addBtn = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cmb_factory = new System.Windows.Forms.ComboBox();
            this.txtdepartment = new System.Windows.Forms.TextBox();
            this.chk_DeliveryTime = new System.Windows.Forms.CheckBox();
            this.BtnImportData = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.materialGridView)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // demandIdLabel
            // 
            this.demandIdLabel.AutoSize = true;
            this.demandIdLabel.Location = new System.Drawing.Point(24, 106);
            this.demandIdLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.demandIdLabel.Name = "demandIdLabel";
            this.demandIdLabel.Size = new System.Drawing.Size(53, 12);
            this.demandIdLabel.TabIndex = 0;
            this.demandIdLabel.Text = "需求单号";
            // 
            // demandIdText
            // 
            this.demandIdText.Location = new System.Drawing.Point(79, 104);
            this.demandIdText.Margin = new System.Windows.Forms.Padding(2);
            this.demandIdText.Name = "demandIdText";
            this.demandIdText.ReadOnly = true;
            this.demandIdText.Size = new System.Drawing.Size(203, 21);
            this.demandIdText.TabIndex = 1;
            // 
            // materialGridView
            // 
            this.materialGridView.AllowUserToAddRows = false;
            this.materialGridView.BackgroundColor = System.Drawing.SystemColors.Control;
            this.materialGridView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.materialGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.materialGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.materialId,
            this.materialName,
            this.materialGroup,
            this.applyNum,
            this.measurement,
            this.stockId,
            this.DeliveryStartTime,
            this.DeliveryEndTime});
            this.materialGridView.EnableHeadersVisualStyles = false;
            this.materialGridView.Location = new System.Drawing.Point(11, 17);
            this.materialGridView.Margin = new System.Windows.Forms.Padding(2);
            this.materialGridView.Name = "materialGridView";
            this.materialGridView.RowTemplate.Height = 27;
            this.materialGridView.Size = new System.Drawing.Size(953, 421);
            this.materialGridView.TabIndex = 2;
            this.materialGridView.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.materialGridView_RowPostPaint);
            // 
            // materialId
            // 
            this.materialId.HeaderText = "物料编号";
            this.materialId.Name = "materialId";
            this.materialId.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.materialId.Width = 150;
            // 
            // materialName
            // 
            this.materialName.HeaderText = "物料名称";
            this.materialName.Name = "materialName";
            // 
            // materialGroup
            // 
            this.materialGroup.HeaderText = "物料组";
            this.materialGroup.Name = "materialGroup";
            // 
            // applyNum
            // 
            this.applyNum.HeaderText = "申请数量";
            this.applyNum.Name = "applyNum";
            // 
            // measurement
            // 
            this.measurement.HeaderText = "单位";
            this.measurement.Name = "measurement";
            this.measurement.Width = 70;
            // 
            // stockId
            // 
            this.stockId.HeaderText = "仓库编号";
            this.stockId.Name = "stockId";
            this.stockId.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.stockId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // DeliveryStartTime
            // 
            this.DeliveryStartTime.HeaderText = "交货开始时间";
            this.DeliveryStartTime.Name = "DeliveryStartTime";
            this.DeliveryStartTime.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.DeliveryStartTime.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.DeliveryStartTime.Width = 140;
            // 
            // DeliveryEndTime
            // 
            this.DeliveryEndTime.HeaderText = "交货结束时间";
            this.DeliveryEndTime.Name = "DeliveryEndTime";
            this.DeliveryEndTime.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.DeliveryEndTime.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.DeliveryEndTime.Width = 140;
            // 
            // phoneLabel
            // 
            this.phoneLabel.AutoSize = true;
            this.phoneLabel.Location = new System.Drawing.Point(399, 151);
            this.phoneLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.phoneLabel.Name = "phoneLabel";
            this.phoneLabel.Size = new System.Drawing.Size(53, 12);
            this.phoneLabel.TabIndex = 3;
            this.phoneLabel.Text = "联系电话";
            // 
            // phoneText
            // 
            this.phoneText.Location = new System.Drawing.Point(454, 147);
            this.phoneText.Margin = new System.Windows.Forms.Padding(2);
            this.phoneText.Name = "phoneText";
            this.phoneText.Size = new System.Drawing.Size(115, 21);
            this.phoneText.TabIndex = 4;
            // 
            // departmentLabel
            // 
            this.departmentLabel.AutoSize = true;
            this.departmentLabel.Location = new System.Drawing.Point(23, 148);
            this.departmentLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.departmentLabel.Name = "departmentLabel";
            this.departmentLabel.Size = new System.Drawing.Size(53, 12);
            this.departmentLabel.TabIndex = 5;
            this.departmentLabel.Text = "申请部门";
            // 
            // proposer
            // 
            this.proposer.AutoSize = true;
            this.proposer.Location = new System.Drawing.Point(268, 151);
            this.proposer.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.proposer.Name = "proposer";
            this.proposer.Size = new System.Drawing.Size(41, 12);
            this.proposer.TabIndex = 7;
            this.proposer.Text = "申请人";
            // 
            // txt_Proposer
            // 
            this.txt_Proposer.Location = new System.Drawing.Point(312, 147);
            this.txt_Proposer.Margin = new System.Windows.Forms.Padding(2);
            this.txt_Proposer.Name = "txt_Proposer";
            this.txt_Proposer.Size = new System.Drawing.Size(64, 21);
            this.txt_Proposer.TabIndex = 8;
            // 
            // applyTime
            // 
            this.applyTime.AutoSize = true;
            this.applyTime.Location = new System.Drawing.Point(601, 151);
            this.applyTime.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.applyTime.Name = "applyTime";
            this.applyTime.Size = new System.Drawing.Size(53, 12);
            this.applyTime.TabIndex = 10;
            this.applyTime.Text = "申请日期";
            // 
            // purchaseTypeLabel
            // 
            this.purchaseTypeLabel.AutoSize = true;
            this.purchaseTypeLabel.Location = new System.Drawing.Point(23, 65);
            this.purchaseTypeLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.purchaseTypeLabel.Name = "purchaseTypeLabel";
            this.purchaseTypeLabel.Size = new System.Drawing.Size(53, 12);
            this.purchaseTypeLabel.TabIndex = 12;
            this.purchaseTypeLabel.Text = "采购类型";
            // 
            // purchaseTypeCB
            // 
            this.purchaseTypeCB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.purchaseTypeCB.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.purchaseTypeCB.FormattingEnabled = true;
            this.purchaseTypeCB.Items.AddRange(new object[] {
            "标准采购类型",
            "框架协议采购类型",
            "委外加工采购类型",
            "寄售采购类型"});
            this.purchaseTypeCB.Location = new System.Drawing.Point(79, 61);
            this.purchaseTypeCB.Margin = new System.Windows.Forms.Padding(2);
            this.purchaseTypeCB.Name = "purchaseTypeCB";
            this.purchaseTypeCB.Size = new System.Drawing.Size(151, 22);
            this.purchaseTypeCB.TabIndex = 31;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(272, 65);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 32;
            this.label1.Text = "物流方式";
            // 
            // logisticsCB
            // 
            this.logisticsCB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.logisticsCB.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.logisticsCB.FormattingEnabled = true;
            this.logisticsCB.Items.AddRange(new object[] {
            "物流中心式",
            "统谈统签直送式",
            "统谈分签式"});
            this.logisticsCB.Location = new System.Drawing.Point(326, 61);
            this.logisticsCB.Margin = new System.Windows.Forms.Padding(2);
            this.logisticsCB.Name = "logisticsCB";
            this.logisticsCB.Size = new System.Drawing.Size(151, 22);
            this.logisticsCB.TabIndex = 33;
            // 
            // submitBtn
            // 
            this.submitBtn.Location = new System.Drawing.Point(113, 10);
            this.submitBtn.Margin = new System.Windows.Forms.Padding(2);
            this.submitBtn.Name = "submitBtn";
            this.submitBtn.Size = new System.Drawing.Size(56, 27);
            this.submitBtn.TabIndex = 34;
            this.submitBtn.Text = "保存";
            this.submitBtn.UseVisualStyleBackColor = true;
            this.submitBtn.Visible = false;
            this.submitBtn.Click += new System.EventHandler(this.submitBtn_Click);
            // 
            // checkBtn
            // 
            this.checkBtn.Location = new System.Drawing.Point(26, 10);
            this.checkBtn.Margin = new System.Windows.Forms.Padding(2);
            this.checkBtn.Name = "checkBtn";
            this.checkBtn.Size = new System.Drawing.Size(64, 27);
            this.checkBtn.TabIndex = 35;
            this.checkBtn.Text = "检查";
            this.checkBtn.UseVisualStyleBackColor = true;
            this.checkBtn.Click += new System.EventHandler(this.checkBtn_Click);
            // 
            // dtp_apply
            // 
            this.dtp_apply.CustomFormat = "yyyy-MM-dd HH:mm:ss";
            this.dtp_apply.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtp_apply.Location = new System.Drawing.Point(658, 147);
            this.dtp_apply.Margin = new System.Windows.Forms.Padding(2);
            this.dtp_apply.Name = "dtp_apply";
            this.dtp_apply.Size = new System.Drawing.Size(177, 21);
            this.dtp_apply.TabIndex = 36;
            // 
            // addBtn
            // 
            this.addBtn.Location = new System.Drawing.Point(25, 624);
            this.addBtn.Margin = new System.Windows.Forms.Padding(2);
            this.addBtn.Name = "addBtn";
            this.addBtn.Size = new System.Drawing.Size(64, 22);
            this.addBtn.TabIndex = 37;
            this.addBtn.Text = "批量添加";
            this.addBtn.UseVisualStyleBackColor = true;
            this.addBtn.Click += new System.EventHandler(this.addBtn_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.materialGridView);
            this.groupBox1.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.groupBox1.Location = new System.Drawing.Point(26, 178);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox1.Size = new System.Drawing.Size(971, 442);
            this.groupBox1.TabIndex = 38;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "物料信息";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(503, 65);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 39;
            this.label2.Text = "工厂编号";
            // 
            // cmb_factory
            // 
            this.cmb_factory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_factory.FormattingEnabled = true;
            this.cmb_factory.Location = new System.Drawing.Point(562, 63);
            this.cmb_factory.Name = "cmb_factory";
            this.cmb_factory.Size = new System.Drawing.Size(121, 20);
            this.cmb_factory.TabIndex = 40;
            // 
            // txtdepartment
            // 
            this.txtdepartment.Location = new System.Drawing.Point(81, 146);
            this.txtdepartment.Name = "txtdepartment";
            this.txtdepartment.Size = new System.Drawing.Size(149, 21);
            this.txtdepartment.TabIndex = 42;
            // 
            // chk_DeliveryTime
            // 
            this.chk_DeliveryTime.AutoSize = true;
            this.chk_DeliveryTime.Location = new System.Drawing.Point(885, 149);
            this.chk_DeliveryTime.Name = "chk_DeliveryTime";
            this.chk_DeliveryTime.Size = new System.Drawing.Size(96, 16);
            this.chk_DeliveryTime.TabIndex = 41;
            this.chk_DeliveryTime.Text = "复用交货时间";
            this.chk_DeliveryTime.UseVisualStyleBackColor = true;
            this.chk_DeliveryTime.Visible = false;
            this.chk_DeliveryTime.CheckedChanged += new System.EventHandler(this.chk_DeliveryTime_CheckedChanged);
            // 
            // BtnImportData
            // 
            this.BtnImportData.Location = new System.Drawing.Point(873, 12);
            this.BtnImportData.Name = "BtnImportData";
            this.BtnImportData.Size = new System.Drawing.Size(124, 23);
            this.BtnImportData.TabIndex = 237;
            this.BtnImportData.Text = "外部导入采购计划";
            this.BtnImportData.UseVisualStyleBackColor = true;
            this.BtnImportData.Click += new System.EventHandler(this.BtnImportData_Click);
            // 
            // NewSummaryDemand
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1059, 662);
            this.Controls.Add(this.BtnImportData);
            this.Controls.Add(this.txtdepartment);
            this.Controls.Add(this.chk_DeliveryTime);
            this.Controls.Add(this.cmb_factory);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.addBtn);
            this.Controls.Add(this.dtp_apply);
            this.Controls.Add(this.checkBtn);
            this.Controls.Add(this.submitBtn);
            this.Controls.Add(this.logisticsCB);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.purchaseTypeCB);
            this.Controls.Add(this.purchaseTypeLabel);
            this.Controls.Add(this.applyTime);
            this.Controls.Add(this.txt_Proposer);
            this.Controls.Add(this.proposer);
            this.Controls.Add(this.departmentLabel);
            this.Controls.Add(this.phoneText);
            this.Controls.Add(this.phoneLabel);
            this.Controls.Add(this.demandIdText);
            this.Controls.Add(this.demandIdLabel);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "NewSummaryDemand";
            this.Text = "新建采购申请";
            ((System.ComponentModel.ISupportInitialize)(this.materialGridView)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label demandIdLabel;
        private System.Windows.Forms.TextBox demandIdText;
        private System.Windows.Forms.DataGridView materialGridView;
        private System.Windows.Forms.Label phoneLabel;
        private System.Windows.Forms.TextBox phoneText;
        private System.Windows.Forms.Label departmentLabel;
        private System.Windows.Forms.Label proposer;
        private System.Windows.Forms.TextBox txt_Proposer;
        private System.Windows.Forms.Label applyTime;
        private System.Windows.Forms.Label purchaseTypeLabel;
        private System.Windows.Forms.ComboBox purchaseTypeCB;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox logisticsCB;
        private System.Windows.Forms.Button submitBtn;
        private System.Windows.Forms.Button checkBtn;
        private System.Windows.Forms.DateTimePicker dtp_apply;
        private System.Windows.Forms.Button addBtn;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cmb_factory;
        private System.Windows.Forms.TextBox txtdepartment;
        private System.Windows.Forms.CheckBox chk_DeliveryTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn materialId;
        private System.Windows.Forms.DataGridViewTextBoxColumn materialName;
        private System.Windows.Forms.DataGridViewTextBoxColumn materialGroup;
        private System.Windows.Forms.DataGridViewTextBoxColumn applyNum;
        private System.Windows.Forms.DataGridViewTextBoxColumn measurement;
        private MDBll.Batch.DataGridViewComboEditBoxColumn stockId;
        private Lib.Common.CommonControls.CalendarColumn DeliveryStartTime;
        private Lib.Common.CommonControls.CalendarColumn DeliveryEndTime;
        private System.Windows.Forms.Button BtnImportData;
    }
}