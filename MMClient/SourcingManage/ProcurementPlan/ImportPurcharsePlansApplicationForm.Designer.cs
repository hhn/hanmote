﻿namespace MMClient.SourcingManage.ProcurementPlan
{
    partial class ImportPurcharsePlansApplicationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel1 = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.checkBtn = new System.Windows.Forms.Button();
            this.txtdepartment = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.materialGridView = new System.Windows.Forms.DataGridView();
            this.dtp_apply = new System.Windows.Forms.DateTimePicker();
            this.logisticsCB = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.purchaseTypeCB = new System.Windows.Forms.ComboBox();
            this.purchaseTypeLabel = new System.Windows.Forms.Label();
            this.applyTime = new System.Windows.Forms.Label();
            this.txt_Proposer = new System.Windows.Forms.TextBox();
            this.proposer = new System.Windows.Forms.Label();
            this.departmentLabel = new System.Windows.Forms.Label();
            this.phoneText = new System.Windows.Forms.TextBox();
            this.phoneLabel = new System.Windows.Forms.Label();
            this.demandIdText = new System.Windows.Forms.TextBox();
            this.demandIdLabel = new System.Windows.Forms.Label();
            this.submitBtn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.BtnoutImport = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.openFileDialog2 = new System.Windows.Forms.OpenFileDialog();
            this.LbpathShow = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.materialGridView)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.Controls.Add(this.groupBox2);
            this.panel1.Controls.Add(this.checkBtn);
            this.panel1.Controls.Add(this.txtdepartment);
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Controls.Add(this.dtp_apply);
            this.panel1.Controls.Add(this.logisticsCB);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.purchaseTypeCB);
            this.panel1.Controls.Add(this.purchaseTypeLabel);
            this.panel1.Controls.Add(this.applyTime);
            this.panel1.Controls.Add(this.txt_Proposer);
            this.panel1.Controls.Add(this.proposer);
            this.panel1.Controls.Add(this.departmentLabel);
            this.panel1.Controls.Add(this.phoneText);
            this.panel1.Controls.Add(this.phoneLabel);
            this.panel1.Controls.Add(this.demandIdText);
            this.panel1.Controls.Add(this.demandIdLabel);
            this.panel1.Controls.Add(this.submitBtn);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1003, 775);
            this.panel1.TabIndex = 2;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(19, 20);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(126, 33);
            this.button1.TabIndex = 61;
            this.button1.Text = "生成模板";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // checkBtn
            // 
            this.checkBtn.Location = new System.Drawing.Point(291, 724);
            this.checkBtn.Margin = new System.Windows.Forms.Padding(2);
            this.checkBtn.Name = "checkBtn";
            this.checkBtn.Size = new System.Drawing.Size(110, 25);
            this.checkBtn.TabIndex = 60;
            this.checkBtn.Text = "数据检查";
            this.checkBtn.UseVisualStyleBackColor = true;
            this.checkBtn.Click += new System.EventHandler(this.checkBtn_Click);
            // 
            // txtdepartment
            // 
            this.txtdepartment.Location = new System.Drawing.Point(77, 212);
            this.txtdepartment.Name = "txtdepartment";
            this.txtdepartment.Size = new System.Drawing.Size(149, 21);
            this.txtdepartment.TabIndex = 59;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.materialGridView);
            this.groupBox1.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.groupBox1.Location = new System.Drawing.Point(20, 248);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox1.Size = new System.Drawing.Size(971, 442);
            this.groupBox1.TabIndex = 56;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "物料信息";
            // 
            // materialGridView
            // 
            this.materialGridView.AllowUserToAddRows = false;
            this.materialGridView.BackgroundColor = System.Drawing.SystemColors.Control;
            this.materialGridView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.materialGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.materialGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.materialGridView.DefaultCellStyle = dataGridViewCellStyle2;
            this.materialGridView.EnableHeadersVisualStyles = false;
            this.materialGridView.Location = new System.Drawing.Point(11, 17);
            this.materialGridView.Margin = new System.Windows.Forms.Padding(2);
            this.materialGridView.Name = "materialGridView";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.materialGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.materialGridView.RowTemplate.Height = 27;
            this.materialGridView.Size = new System.Drawing.Size(953, 410);
            this.materialGridView.TabIndex = 2;
            // 
            // dtp_apply
            // 
            this.dtp_apply.CustomFormat = "yyyy-MM-dd HH:mm:ss";
            this.dtp_apply.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtp_apply.Location = new System.Drawing.Point(654, 213);
            this.dtp_apply.Margin = new System.Windows.Forms.Padding(2);
            this.dtp_apply.Name = "dtp_apply";
            this.dtp_apply.Size = new System.Drawing.Size(177, 21);
            this.dtp_apply.TabIndex = 55;
            // 
            // logisticsCB
            // 
            this.logisticsCB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.logisticsCB.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.logisticsCB.FormattingEnabled = true;
            this.logisticsCB.Items.AddRange(new object[] {
            "物流中心式",
            "统谈统签直送式",
            "统谈分签式"});
            this.logisticsCB.Location = new System.Drawing.Point(324, 123);
            this.logisticsCB.Margin = new System.Windows.Forms.Padding(2);
            this.logisticsCB.Name = "logisticsCB";
            this.logisticsCB.Size = new System.Drawing.Size(151, 22);
            this.logisticsCB.TabIndex = 54;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(270, 127);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 12);
            this.label3.TabIndex = 53;
            this.label3.Text = "物流方式";
            // 
            // purchaseTypeCB
            // 
            this.purchaseTypeCB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.purchaseTypeCB.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.purchaseTypeCB.FormattingEnabled = true;
            this.purchaseTypeCB.Items.AddRange(new object[] {
            "标准采购类型",
            "框架协议采购类型",
            "委外加工采购类型",
            "寄售采购类型"});
            this.purchaseTypeCB.Location = new System.Drawing.Point(77, 123);
            this.purchaseTypeCB.Margin = new System.Windows.Forms.Padding(2);
            this.purchaseTypeCB.Name = "purchaseTypeCB";
            this.purchaseTypeCB.Size = new System.Drawing.Size(151, 22);
            this.purchaseTypeCB.TabIndex = 52;
            // 
            // purchaseTypeLabel
            // 
            this.purchaseTypeLabel.AutoSize = true;
            this.purchaseTypeLabel.Location = new System.Drawing.Point(21, 127);
            this.purchaseTypeLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.purchaseTypeLabel.Name = "purchaseTypeLabel";
            this.purchaseTypeLabel.Size = new System.Drawing.Size(53, 12);
            this.purchaseTypeLabel.TabIndex = 51;
            this.purchaseTypeLabel.Text = "采购类型";
            // 
            // applyTime
            // 
            this.applyTime.AutoSize = true;
            this.applyTime.Location = new System.Drawing.Point(597, 217);
            this.applyTime.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.applyTime.Name = "applyTime";
            this.applyTime.Size = new System.Drawing.Size(53, 12);
            this.applyTime.TabIndex = 50;
            this.applyTime.Text = "申请日期";
            // 
            // txt_Proposer
            // 
            this.txt_Proposer.Location = new System.Drawing.Point(293, 212);
            this.txt_Proposer.Margin = new System.Windows.Forms.Padding(2);
            this.txt_Proposer.Name = "txt_Proposer";
            this.txt_Proposer.Size = new System.Drawing.Size(98, 21);
            this.txt_Proposer.TabIndex = 49;
            // 
            // proposer
            // 
            this.proposer.AutoSize = true;
            this.proposer.Location = new System.Drawing.Point(249, 216);
            this.proposer.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.proposer.Name = "proposer";
            this.proposer.Size = new System.Drawing.Size(41, 12);
            this.proposer.TabIndex = 48;
            this.proposer.Text = "申请人";
            // 
            // departmentLabel
            // 
            this.departmentLabel.AutoSize = true;
            this.departmentLabel.Location = new System.Drawing.Point(19, 214);
            this.departmentLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.departmentLabel.Name = "departmentLabel";
            this.departmentLabel.Size = new System.Drawing.Size(53, 12);
            this.departmentLabel.TabIndex = 47;
            this.departmentLabel.Text = "申请部门";
            // 
            // phoneText
            // 
            this.phoneText.Location = new System.Drawing.Point(450, 213);
            this.phoneText.Margin = new System.Windows.Forms.Padding(2);
            this.phoneText.Name = "phoneText";
            this.phoneText.Size = new System.Drawing.Size(115, 21);
            this.phoneText.TabIndex = 46;
            // 
            // phoneLabel
            // 
            this.phoneLabel.AutoSize = true;
            this.phoneLabel.Location = new System.Drawing.Point(395, 217);
            this.phoneLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.phoneLabel.Name = "phoneLabel";
            this.phoneLabel.Size = new System.Drawing.Size(53, 12);
            this.phoneLabel.TabIndex = 45;
            this.phoneLabel.Text = "联系电话";
            // 
            // demandIdText
            // 
            this.demandIdText.Location = new System.Drawing.Point(77, 166);
            this.demandIdText.Margin = new System.Windows.Forms.Padding(2);
            this.demandIdText.Name = "demandIdText";
            this.demandIdText.ReadOnly = true;
            this.demandIdText.Size = new System.Drawing.Size(203, 21);
            this.demandIdText.TabIndex = 44;
            // 
            // demandIdLabel
            // 
            this.demandIdLabel.AutoSize = true;
            this.demandIdLabel.Location = new System.Drawing.Point(22, 168);
            this.demandIdLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.demandIdLabel.Name = "demandIdLabel";
            this.demandIdLabel.Size = new System.Drawing.Size(53, 12);
            this.demandIdLabel.TabIndex = 43;
            this.demandIdLabel.Text = "需求单号";
            // 
            // submitBtn
            // 
            this.submitBtn.Location = new System.Drawing.Point(499, 724);
            this.submitBtn.Name = "submitBtn";
            this.submitBtn.Size = new System.Drawing.Size(110, 25);
            this.submitBtn.TabIndex = 3;
            this.submitBtn.Text = "保存";
            this.submitBtn.UseVisualStyleBackColor = true;
            this.submitBtn.Visible = false;
            this.submitBtn.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(18, 724);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(197, 12);
            this.label1.TabIndex = 10;
            this.label1.Text = "导入过程中因数据较多，请耐心等待";
            // 
            // BtnoutImport
            // 
            this.BtnoutImport.Location = new System.Drawing.Point(205, 20);
            this.BtnoutImport.Name = "BtnoutImport";
            this.BtnoutImport.Size = new System.Drawing.Size(126, 33);
            this.BtnoutImport.TabIndex = 9;
            this.BtnoutImport.Text = "选择数据源";
            this.BtnoutImport.UseVisualStyleBackColor = true;
            this.BtnoutImport.Visible = false;
            this.BtnoutImport.Click += new System.EventHandler(this.BtnoutImport_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // openFileDialog2
            // 
            this.openFileDialog2.FileName = "openFileDialog2";
            // 
            // LbpathShow
            // 
            this.LbpathShow.AutoSize = true;
            this.LbpathShow.ForeColor = System.Drawing.Color.Red;
            this.LbpathShow.Location = new System.Drawing.Point(366, 41);
            this.LbpathShow.Name = "LbpathShow";
            this.LbpathShow.Size = new System.Drawing.Size(41, 12);
            this.LbpathShow.TabIndex = 62;
            this.LbpathShow.Text = "label2";
            this.LbpathShow.Visible = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.BtnoutImport);
            this.groupBox2.Controls.Add(this.LbpathShow);
            this.groupBox2.Controls.Add(this.button1);
            this.groupBox2.Location = new System.Drawing.Point(20, 13);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(971, 87);
            this.groupBox2.TabIndex = 63;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "外部导入";
            // 
            // ImportPurcharsePlansApplicationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1040, 799);
            this.Controls.Add(this.panel1);
            this.Name = "ImportPurcharsePlansApplicationForm";
            this.Text = "导入采购计划";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.materialGridView)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button BtnoutImport;
        private System.Windows.Forms.Button submitBtn;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.TextBox txtdepartment;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView materialGridView;
        private System.Windows.Forms.DateTimePicker dtp_apply;
        private System.Windows.Forms.ComboBox logisticsCB;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox purchaseTypeCB;
        private System.Windows.Forms.Label purchaseTypeLabel;
        private System.Windows.Forms.Label applyTime;
        private System.Windows.Forms.TextBox txt_Proposer;
        private System.Windows.Forms.Label proposer;
        private System.Windows.Forms.Label departmentLabel;
        private System.Windows.Forms.TextBox phoneText;
        private System.Windows.Forms.Label phoneLabel;
        private System.Windows.Forms.TextBox demandIdText;
        private System.Windows.Forms.Label demandIdLabel;
        private System.Windows.Forms.Button checkBtn;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.OpenFileDialog openFileDialog2;
        private System.Windows.Forms.Label LbpathShow;
        private System.Windows.Forms.GroupBox groupBox2;
    }
}