﻿namespace MMClient.SourcingManage.ProcurementPlan
{
    partial class AggregateMaterial
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_StartTime = new System.Windows.Forms.Label();
            this.lbl_EndTime = new System.Windows.Forms.Label();
            this.dtp_startTime = new System.Windows.Forms.DateTimePicker();
            this.dtp_endTime = new System.Windows.Forms.DateTimePicker();
            this.btnQuery = new System.Windows.Forms.Button();
            this.demandGridView = new System.Windows.Forms.DataGridView();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.materialGridView = new System.Windows.Forms.DataGridView();
            this.label3 = new System.Windows.Forms.Label();
            this.result = new System.Windows.Forms.Label();
            this.btnSumPlans = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btnReset = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.cbFactory = new System.Windows.Forms.ComboBox();
            this.cbStock = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.pageTool = new pager.pagetool.pageNext();
            this.panel1 = new System.Windows.Forms.Panel();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            ((System.ComponentModel.ISupportInitialize)(this.demandGridView)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.materialGridView)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lbl_StartTime
            // 
            this.lbl_StartTime.AutoSize = true;
            this.lbl_StartTime.Location = new System.Drawing.Point(15, 100);
            this.lbl_StartTime.Name = "lbl_StartTime";
            this.lbl_StartTime.Size = new System.Drawing.Size(53, 12);
            this.lbl_StartTime.TabIndex = 7;
            this.lbl_StartTime.Text = "开始时间";
            // 
            // lbl_EndTime
            // 
            this.lbl_EndTime.AutoSize = true;
            this.lbl_EndTime.Location = new System.Drawing.Point(227, 98);
            this.lbl_EndTime.Name = "lbl_EndTime";
            this.lbl_EndTime.Size = new System.Drawing.Size(53, 12);
            this.lbl_EndTime.TabIndex = 7;
            this.lbl_EndTime.Text = "结束时间";
            // 
            // dtp_startTime
            // 
            this.dtp_startTime.Location = new System.Drawing.Point(74, 94);
            this.dtp_startTime.Name = "dtp_startTime";
            this.dtp_startTime.Size = new System.Drawing.Size(133, 21);
            this.dtp_startTime.TabIndex = 8;
            // 
            // dtp_endTime
            // 
            this.dtp_endTime.Location = new System.Drawing.Point(287, 91);
            this.dtp_endTime.Name = "dtp_endTime";
            this.dtp_endTime.Size = new System.Drawing.Size(139, 21);
            this.dtp_endTime.TabIndex = 9;
            // 
            // btnQuery
            // 
            this.btnQuery.Location = new System.Drawing.Point(491, 91);
            this.btnQuery.Name = "btnQuery";
            this.btnQuery.Size = new System.Drawing.Size(64, 21);
            this.btnQuery.TabIndex = 10;
            this.btnQuery.Text = "查询";
            this.btnQuery.UseVisualStyleBackColor = true;
            this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
            // 
            // demandGridView
            // 
            this.demandGridView.AllowUserToAddRows = false;
            this.demandGridView.BackgroundColor = System.Drawing.SystemColors.Control;
            this.demandGridView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.demandGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.demandGridView.EnableHeadersVisualStyles = false;
            this.demandGridView.Location = new System.Drawing.Point(8, 131);
            this.demandGridView.Name = "demandGridView";
            this.demandGridView.RowTemplate.Height = 23;
            this.demandGridView.Size = new System.Drawing.Size(1097, 340);
            this.demandGridView.TabIndex = 11;
            this.demandGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.demandGridView_CellContentClick);
            this.demandGridView.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.demandGridView_CellMouseClick);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.materialGridView);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.result);
            this.groupBox2.Location = new System.Drawing.Point(16, 540);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1135, 243);
            this.groupBox2.TabIndex = 12;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "物料信息";
            // 
            // materialGridView
            // 
            this.materialGridView.AllowUserToAddRows = false;
            this.materialGridView.BackgroundColor = System.Drawing.SystemColors.Control;
            this.materialGridView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.materialGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.materialGridView.EnableHeadersVisualStyles = false;
            this.materialGridView.Location = new System.Drawing.Point(6, 20);
            this.materialGridView.Name = "materialGridView";
            this.materialGridView.RowTemplate.Height = 23;
            this.materialGridView.Size = new System.Drawing.Size(451, 180);
            this.materialGridView.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 214);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 12);
            this.label3.TabIndex = 14;
            this.label3.Text = "合计:";
            // 
            // result
            // 
            this.result.AutoSize = true;
            this.result.Location = new System.Drawing.Point(72, 214);
            this.result.Name = "result";
            this.result.Size = new System.Drawing.Size(11, 12);
            this.result.TabIndex = 15;
            this.result.Text = "0";
            // 
            // btnSumPlans
            // 
            this.btnSumPlans.Location = new System.Drawing.Point(507, 791);
            this.btnSumPlans.Name = "btnSumPlans";
            this.btnSumPlans.Size = new System.Drawing.Size(116, 29);
            this.btnSumPlans.TabIndex = 16;
            this.btnSumPlans.Text = "生成汇总";
            this.btnSumPlans.UseVisualStyleBackColor = true;
            this.btnSumPlans.Click += new System.EventHandler(this.btnSumPlans_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btnReset);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Controls.Add(this.cbFactory);
            this.groupBox3.Controls.Add(this.cbStock);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.pageTool);
            this.groupBox3.Controls.Add(this.demandGridView);
            this.groupBox3.Controls.Add(this.lbl_StartTime);
            this.groupBox3.Controls.Add(this.dtp_startTime);
            this.groupBox3.Controls.Add(this.lbl_EndTime);
            this.groupBox3.Controls.Add(this.dtp_endTime);
            this.groupBox3.Controls.Add(this.btnQuery);
            this.groupBox3.Location = new System.Drawing.Point(16, 20);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(1135, 513);
            this.groupBox3.TabIndex = 13;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "需求信息";
            // 
            // btnReset
            // 
            this.btnReset.Location = new System.Drawing.Point(604, 91);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(64, 21);
            this.btnReset.TabIndex = 31;
            this.btnReset.Text = "重置";
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 39);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 27;
            this.label1.Text = "工厂编号";
            // 
            // cbFactory
            // 
            this.cbFactory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbFactory.FormattingEnabled = true;
            this.cbFactory.Location = new System.Drawing.Point(74, 36);
            this.cbFactory.Name = "cbFactory";
            this.cbFactory.Size = new System.Drawing.Size(133, 20);
            this.cbFactory.TabIndex = 28;
            this.cbFactory.SelectedIndexChanged += new System.EventHandler(this.cbFactory_SelectedIndexChanged);
            // 
            // cbStock
            // 
            this.cbStock.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbStock.FormattingEnabled = true;
            this.cbStock.Location = new System.Drawing.Point(287, 36);
            this.cbStock.Name = "cbStock";
            this.cbStock.Size = new System.Drawing.Size(139, 20);
            this.cbStock.TabIndex = 30;
            this.cbStock.Visible = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(229, 39);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 12);
            this.label2.TabIndex = 29;
            this.label2.Text = "库存地";
            this.label2.Visible = false;
            // 
            // pageTool
            // 
            this.pageTool.Location = new System.Drawing.Point(220, 476);
            this.pageTool.Name = "pageTool";
            this.pageTool.PageIndex = 1;
            this.pageTool.PageSize = 100;
            this.pageTool.RecordCount = 0;
            this.pageTool.Size = new System.Drawing.Size(688, 37);
            this.pageTool.TabIndex = 12;
            this.pageTool.OnPageChanged += new System.EventHandler(this.pageTool_OnPageChanged);
            this.pageTool.Load += new System.EventHandler(this.pageTool_Load);
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.Controls.Add(this.linkLabel1);
            this.panel1.Controls.Add(this.btnSumPlans);
            this.panel1.Controls.Add(this.groupBox2);
            this.panel1.Controls.Add(this.groupBox3);
            this.panel1.Location = new System.Drawing.Point(9, 13);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1154, 830);
            this.panel1.TabIndex = 21;
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Location = new System.Drawing.Point(22, 798);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(77, 12);
            this.linkLabel1.TabIndex = 14;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "查看汇总信息";
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // AggregateMaterial
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1177, 845);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "AggregateMaterial";
            this.Text = "需求计划汇总";
            this.Load += new System.EventHandler(this.AggregateMaterial_Load);
            ((System.ComponentModel.ISupportInitialize)(this.demandGridView)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.materialGridView)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label lbl_StartTime;
        private System.Windows.Forms.Label lbl_EndTime;
        private System.Windows.Forms.DateTimePicker dtp_startTime;
        private System.Windows.Forms.DateTimePicker dtp_endTime;
        private System.Windows.Forms.Button btnQuery;
        private System.Windows.Forms.DataGridView demandGridView;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView materialGridView;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label result;
        private System.Windows.Forms.Button btnSumPlans;
        private System.Windows.Forms.Panel panel1;
        private pager.pagetool.pageNext pageTool;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbFactory;
        private System.Windows.Forms.ComboBox cbStock;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnReset;
    }
}