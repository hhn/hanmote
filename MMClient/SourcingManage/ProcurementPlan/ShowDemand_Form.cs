﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.SqlServerDAL;
using Lib.SqlServerDAL.SourcingManage.ProcurementPlan;
using Lib.Model.SourcingManage.ProcurementPlan;
using System.Text.RegularExpressions;
using WeifenLuo.WinFormsUI.Docking;
using Lib.Bll.SourcingManage.ProcurementPlan;
using Lib.Common.CommonUtils;
using Lib.Common.MMCException.Bll;
using Lib.Common.MMCException.IDAL;

namespace MMClient.SourcingManage.ProcurementPlan
{
    public partial class ShowDemand_Form : DockContent
    {
        public UserUI userUI;
        //需求表格生成行数
        int demandNum = 0;
        //被选中的行值
        public int j = 0;

        Summary_DemandBLL summaryDemandBll = new Summary_DemandBLL();
        Demand_MaterialBLL demandMaterialBll = new Demand_MaterialBLL();

        public DataTable checkDt = null;
        public Summary_Demand summaryDemand;

        public ShowDemand_Form(UserUI userUi)
        {
            InitializeComponent(); 
            addItemsToCMB();
            this.userUI = userUi;
            DynamicDrawTable("", "", "state = '待审核'", false);
        }

        /// <summary>
        /// combobox自动加载items
        /// </summary>
        public void addItemsToCMB()
        {
            //需求单号自动加载至item
            DataTable findItem = summaryDemandBll.FindStandardDemand_ID("Demand_ID");
            if (findItem != null)
            {
                for (int n = 0; n < findItem.Rows.Count; n++)
                {
                    this.xqdh_cmb.Items.Add(findItem.Rows[n][0].ToString());
                }
            }
            this.xqdh_cmb.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            this.xqdh_cmb.AutoCompleteSource = AutoCompleteSource.ListItems;


            //状态自动加载至item
            findItem = summaryDemandBll.FindStandardDemand_ID("state");
            if (findItem != null)
            {
                for (int n = 0; n < findItem.Rows.Count; n++)
                {
                    this.type_cmb.Items.Add(findItem.Rows[n][0].ToString());
                }
            }
            this.type_cmb.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            this.type_cmb.AutoCompleteSource = AutoCompleteSource.ListItems;
        }

        /// <summary>
        /// 填入demandGridView
        /// </summary>
        public void DynamicDrawTable(String item1, String item2, String item3, Boolean isFromSH)
        { 
            //查询需求计划table
            DataTable dt = new DataTable();
            dt = summaryDemandBll.StandardByWLBH(item1, item2, item3);
            if (dt == null || dt.Rows.Count == 0)
            {
                MessageBox.Show("未找到相关记录，请重新编辑查询词条！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            demandNum = dt.Rows.Count;
            fillDemandGridView(dt);
        }

        /// <summary>
        /// 填充DemandGridView
        /// </summary>
        /// <param name="dt"></param>
        private void fillDemandGridView(DataTable dt)
        {
            demandGridView.Rows.Clear();
            if (demandNum > demandGridView.Rows.Count)
            {
                demandGridView.Rows.Add(demandNum - demandGridView.Rows.Count);
            }
            for (int i = 1; i <= demandNum; i++)
            {
                demandGridView.Rows[i - 1].Cells["demandId"].Value = dt.Rows[i - 1][0].ToString();
                demandGridView.Rows[i - 1].Cells["purchaseType"].Value = dt.Rows[i - 1][1].ToString();
                demandGridView.Rows[i - 1].Cells["proposerId"].Value = dt.Rows[i - 1][2].ToString();
                demandGridView.Rows[i - 1].Cells["state"].Value = dt.Rows[i - 1][4].ToString();
                demandGridView.Rows[i - 1].Cells["factoryId"].Value = dt.Rows[i-1][15].ToString();
                demandGridView.Rows[i - 1].Cells["applyTime"].Value = DateTime.Parse(dt.Rows[i - 1][7].ToString()).ToString("yyyy-MM-dd hh:mm:ss");
                if (dt.Rows[i - 1][10].ToString().Equals(""))
                {
                    demandGridView.Rows[i - 1].Cells["reviewTime"].Value = null;
                }
                else
                {
                    demandGridView.Rows[i - 1].Cells["reviewTime"].Value = DateTime.Parse(dt.Rows[i - 1][10].ToString()).ToString("yyyy-MM-dd hh:mm:ss");
                }
                
            }
        }

        /// <summary>
        /// 打开需求预测
        /// </summary>
        private void forecast_pb_Click(object sender, EventArgs e)
        {
            if (checkDt == null || checkDt.Rows.Count == 0 || j == 0)
            {
                MessageBox.Show("未选中计划订单，请重新选择！");
                return;
            }
            else
            {
                DemandForecast_Form demandForecastF_Form = new DemandForecast_Form(userUI);
                SingletonUserUI.addToUserUI(demandForecastF_Form);
            }

        }

        /// <summary>
        /// 刷新按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void flushButton_Click(object sender, EventArgs e)
        {
            flushMaterialGridView();
            fillMaterialGridView();
        }

        /// <summary>
        /// 刷新MaterialGridView
        /// </summary>
        public void flushMaterialGridView()
        {
            String item1 = null;
            String item2 = null;
            String item3 = null;
            if (!string.IsNullOrWhiteSpace(this.xqdh_cmb.Text))
            {
                item1 = " Demand_ID = '" + this.xqdh_cmb.Text + "'";
            }
            if (!string.IsNullOrWhiteSpace(this.wlbh_cmb.Text))
            {
                item2 = " Material_ID = '" + this.wlbh_cmb.Text + "'";
            }
            if (!string.IsNullOrWhiteSpace(this.type_cmb.Text))
            {
                item3 = " state = '" + this.type_cmb.Text + "'";
            }
            DynamicDrawTable(item1, item2, item3, false);
        }

        /// <summary>
        /// 关闭按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void closeButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// 查询按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void query_bt_Click(object sender, EventArgs e)
        {
            flushMaterialGridView();
            fillMaterialGridView();
        }

        /// <summary>
        /// 改变选中项需求计划
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void demandGridView_SelectionChanged(object sender, EventArgs e)
        {
            fillMaterialGridView();
        }

        /// <summary>
        /// 根据选中的需求计划展示该需求计划对应的所有物料信息
        /// </summary>
        private void fillMaterialGridView()
        {
            materialGridView.Rows.Clear();
            int index = demandGridView.CurrentRow.Index;
            Object obj = demandGridView.Rows[index].Cells[0].Value;
            if (obj == null)
            {
                return;
            } 
            String demandid= demandGridView.Rows[index].Cells[0].Value.ToString();
            //根据选中行id查找对应的需求计划
            summaryDemand = summaryDemandBll.findSummaryDemandByDemandID(demandid);     
            List<Demand_Material> demands = demandMaterialBll.findDemandMaterials(demandid);
            if (demands.Count > materialGridView.Rows.Count)
            {
                materialGridView.Rows.Add(demands.Count - materialGridView.Rows.Count);
            }
            for (int i = 0; i < demands.Count; i++)
            {
                materialGridView.Rows[i].Cells["materialId"].Value = demands.ElementAt(i).Material_ID;
                materialGridView.Rows[i].Cells["materialName"].Value = demands.ElementAt(i).Material_Name;
                materialGridView.Rows[i].Cells["materialGroup"].Value = demands.ElementAt(i).Material_Group;
                materialGridView.Rows[i].Cells["demandCount"].Value = demands.ElementAt(i).Demand_Count;        
                materialGridView.Rows[i].Cells["measurement"].Value = demands.ElementAt(i).Measurement;
                materialGridView.Rows[i].Cells["stockId"].Value = demands.ElementAt(i).Stock_ID;

            }
        }

        /// <summary>
        /// 画需求单号行号
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void demandGridView_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            var dgv = sender as DataGridView;
            if (dgv != null)
            {
                Rectangle rect = new Rectangle(e.RowBounds.Location.X, e.RowBounds.Location.Y, (dgv.RowHeadersWidth + 12) / 2, e.RowBounds.Height);
                TextRenderer.DrawText(e.Graphics, (e.RowIndex + 1).ToString(), dgv.RowHeadersDefaultCellStyle.Font, rect, dgv.RowHeadersDefaultCellStyle.ForeColor, TextFormatFlags.VerticalCenter | TextFormatFlags.Right);
            }
        }

        /// <summary>
        /// 画物料信息行号
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void materialGridView_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            var dgv = sender as DataGridView;
            if (dgv != null)
            {
                Rectangle rect = new Rectangle(e.RowBounds.Location.X, e.RowBounds.Location.Y, (dgv.RowHeadersWidth + 12) / 2, e.RowBounds.Height);
                TextRenderer.DrawText(e.Graphics, (e.RowIndex + 1).ToString(), dgv.RowHeadersDefaultCellStyle.Font, rect, dgv.RowHeadersDefaultCellStyle.ForeColor, TextFormatFlags.VerticalCenter | TextFormatFlags.Right);
            }
        }

        /// <summary>
        /// 鼠标进入事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void demandGridView_CellMouseEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
            {
                demandGridView.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.LightBlue;
                DataGridViewCellStyle style = new DataGridViewCellStyle();
                style.BackColor = Color.CornflowerBlue;
                demandGridView.Rows[e.RowIndex].HeaderCell.Style = style;
            }
        }

        /// <summary>
        /// 鼠标离开单元格事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void demandGridView_CellMouseLeave(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
            {
                demandGridView.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.White;
                DataGridViewCellStyle style = new DataGridViewCellStyle();
                style.BackColor = Color.WhiteSmoke;
                demandGridView.Rows[e.RowIndex].HeaderCell.Style = style;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pageTool_Load(object sender, EventArgs e)
        {
            string sql = "SELECT count(*) from Country";
            try
            {
                int i = int.Parse(DBHelper.ExecuteQueryDT(sql).Rows[0][0].ToString());
                int totalNum = i;
                pageTool.DrawControl(totalNum);
                //事件改变调用函数
                pageTool.OnPageChanged += pageTool_OnPageChanged;
            }
            catch (Lib.Common.MMCException.Bll.BllException ex)
            {
                MessageUtil.ShowTips("数据库异常，请稍后重试");
                return;
            }
        }

        private void pageTool_OnPageChanged(object sender, EventArgs e)
        {
            LoaddemandGridViewData();
        }
        private void LoaddemandGridViewData()
        {
            try
            {
                demandGridView.DataSource = FinddemandInforByPage(this.pageTool.PageSize, this.pageTool.PageIndex);
            }
            catch (BllException ex)
            {
                MessageUtil.ShowError(ex.Message);
            }
        }

        private object FinddemandInforByPage(int pageSize, int pageIndex)
        {
            try
            {
                string sql = @"SELECT top " + pageSize + "  Country_ID,Country_Name,Country_Eng  FROM Country   where Country_ID not in(select top " + pageSize * (pageIndex - 1) + " Country_ID from Country ORDER BY Country_ID ASC)ORDER BY Country_ID";
                return DBHelper.ExecuteQueryDT(sql);
            }
            catch (DBException ex)
            {
                throw new BllException("当前无数据", ex);
            }
        }

        private void pageNext1_Load(object sender, EventArgs e)
        {
            string sql = "SELECT count(*) from Country";
            try
            {
                int i = int.Parse(DBHelper.ExecuteQueryDT(sql).Rows[0][0].ToString());
                int totalNum = i;
                pageTool.DrawControl(totalNum);
                //事件改变调用函数
                pageTool.OnPageChanged += pageTool_OnPageChanged;
            }
            catch (Lib.Common.MMCException.Bll.BllException ex)
            {
                MessageUtil.ShowTips("数据库异常，请稍后重试");
                return;
            }
        }

        private void pageNext1_OnPageChanged(object sender, EventArgs e)
        {
            LoadmaterialGridViewViewData();
        }
        private void LoadmaterialGridViewViewData()
        {
            try
            {
                materialGridView.DataSource = FindmaterialGridViewInforByPage(this.pageTool.PageSize, this.pageTool.PageIndex);
            }
            catch (BllException ex)
            {
                MessageUtil.ShowError(ex.Message);
            }
        }
        private object FindmaterialGridViewInforByPage(int pageSize, int pageIndex)
        {
            try
            {
                string sql = @"SELECT top " + pageSize + "  Country_ID,Country_Name,Country_Eng  FROM Country   where Country_ID not in(select top " + pageSize * (pageIndex - 1) + " Country_ID from Country ORDER BY Country_ID ASC)ORDER BY Country_ID";
                return DBHelper.ExecuteQueryDT(sql);
            }
            catch (DBException ex)
            {
                throw new BllException("当前无数据", ex);
            }
        }
    }
}
