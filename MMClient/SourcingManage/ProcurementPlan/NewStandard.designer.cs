﻿namespace MMClient.SourcingManage.ProcurementPlan
{
    partial class NewStandard_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel5 = new System.Windows.Forms.Panel();
            this.kj_panel4 = new System.Windows.Forms.Panel();
            this.kj_cancel_bt = new System.Windows.Forms.Button();
            this.kj_return_bt = new System.Windows.Forms.Button();
            this.kj_submit_bt = new System.Windows.Forms.Button();
            this.kj_panel2 = new System.Windows.Forms.Panel();
            this.kj_wlmc2_cmb = new System.Windows.Forms.ComboBox();
            this.kj_wlbh2_cmb = new System.Windows.Forms.ComboBox();
            this.label29 = new System.Windows.Forms.Label();
            this.kj_qysj_dt = new System.Windows.Forms.DateTimePicker();
            this.label24 = new System.Windows.Forms.Label();
            this.kj_xqdh_tb = new System.Windows.Forms.TextBox();
            this.kj_xymc_tb = new System.Windows.Forms.TextBox();
            this.kj_gysh_tb = new System.Windows.Forms.TextBox();
            this.kj_xybh_tb = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.kj_gysmc_tb = new System.Windows.Forms.TextBox();
            this.kj_gysbh_tb = new System.Windows.Forms.TextBox();
            this.kj_wlgg_tb = new System.Windows.Forms.TextBox();
            this.kj_qqjg_tb = new System.Windows.Forms.TextBox();
            this.kj_wlbz_tb = new System.Windows.Forms.TextBox();
            this.kj_wlph_tb = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.kj_xysl_tb = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.kj_xylx_cmb = new System.Windows.Forms.ComboBox();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.cancel_bt = new System.Windows.Forms.Button();
            this.return_bt = new System.Windows.Forms.Button();
            this.submit_bt = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.sqrq = new System.Windows.Forms.Label();
            this.sqrq_dt = new System.Windows.Forms.DateTimePicker();
            this.lxdh_tb = new System.Windows.Forms.TextBox();
            this.xqsl_tb = new System.Windows.Forms.TextBox();
            this.mindhpl_tb = new System.Windows.Forms.TextBox();
            this.qtyq_rtb = new System.Windows.Forms.RichTextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.sqr_cmb = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.sqbm_cmb = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.jldw_tb = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.ckbh_cmb = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.xqdh_tb = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.wlfs_cmb = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cglx_cmb = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.wlmc_cmb = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.wlbh_cmb = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.wlgg = new System.Windows.Forms.Label();
            this.panel5.SuspendLayout();
            this.kj_panel4.SuspendLayout();
            this.kj_panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.kj_panel4);
            this.panel5.Controls.Add(this.kj_panel2);
            this.panel5.Location = new System.Drawing.Point(3, 123);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(675, 483);
            this.panel5.TabIndex = 162;
            this.panel5.Visible = false;
            // 
            // kj_panel4
            // 
            this.kj_panel4.Controls.Add(this.kj_cancel_bt);
            this.kj_panel4.Controls.Add(this.kj_return_bt);
            this.kj_panel4.Controls.Add(this.kj_submit_bt);
            this.kj_panel4.Location = new System.Drawing.Point(9, 398);
            this.kj_panel4.Name = "kj_panel4";
            this.kj_panel4.Size = new System.Drawing.Size(655, 60);
            this.kj_panel4.TabIndex = 159;
            // 
            // kj_cancel_bt
            // 
            this.kj_cancel_bt.Location = new System.Drawing.Point(287, 15);
            this.kj_cancel_bt.Name = "kj_cancel_bt";
            this.kj_cancel_bt.Size = new System.Drawing.Size(100, 30);
            this.kj_cancel_bt.TabIndex = 49;
            this.kj_cancel_bt.Text = "重置";
            this.kj_cancel_bt.UseVisualStyleBackColor = true;
            // 
            // kj_return_bt
            // 
            this.kj_return_bt.Location = new System.Drawing.Point(492, 15);
            this.kj_return_bt.Name = "kj_return_bt";
            this.kj_return_bt.Size = new System.Drawing.Size(100, 30);
            this.kj_return_bt.TabIndex = 48;
            this.kj_return_bt.Text = "关闭";
            this.kj_return_bt.UseVisualStyleBackColor = true;
            this.kj_return_bt.Click += new System.EventHandler(this.kj_return_bt_Click);
            // 
            // kj_submit_bt
            // 
            this.kj_submit_bt.Location = new System.Drawing.Point(82, 15);
            this.kj_submit_bt.Name = "kj_submit_bt";
            this.kj_submit_bt.Size = new System.Drawing.Size(100, 30);
            this.kj_submit_bt.TabIndex = 47;
            this.kj_submit_bt.Text = "提交";
            this.kj_submit_bt.UseVisualStyleBackColor = true;
            // 
            // kj_panel2
            // 
            this.kj_panel2.Controls.Add(this.kj_wlmc2_cmb);
            this.kj_panel2.Controls.Add(this.kj_wlbh2_cmb);
            this.kj_panel2.Controls.Add(this.label29);
            this.kj_panel2.Controls.Add(this.kj_qysj_dt);
            this.kj_panel2.Controls.Add(this.label24);
            this.kj_panel2.Controls.Add(this.kj_xqdh_tb);
            this.kj_panel2.Controls.Add(this.kj_xymc_tb);
            this.kj_panel2.Controls.Add(this.kj_gysh_tb);
            this.kj_panel2.Controls.Add(this.kj_xybh_tb);
            this.kj_panel2.Controls.Add(this.label16);
            this.kj_panel2.Controls.Add(this.label25);
            this.kj_panel2.Controls.Add(this.kj_gysmc_tb);
            this.kj_panel2.Controls.Add(this.kj_gysbh_tb);
            this.kj_panel2.Controls.Add(this.kj_wlgg_tb);
            this.kj_panel2.Controls.Add(this.kj_qqjg_tb);
            this.kj_panel2.Controls.Add(this.kj_wlbz_tb);
            this.kj_panel2.Controls.Add(this.kj_wlph_tb);
            this.kj_panel2.Controls.Add(this.label17);
            this.kj_panel2.Controls.Add(this.label18);
            this.kj_panel2.Controls.Add(this.label19);
            this.kj_panel2.Controls.Add(this.label20);
            this.kj_panel2.Controls.Add(this.label21);
            this.kj_panel2.Controls.Add(this.label22);
            this.kj_panel2.Controls.Add(this.kj_xysl_tb);
            this.kj_panel2.Controls.Add(this.label23);
            this.kj_panel2.Controls.Add(this.label26);
            this.kj_panel2.Controls.Add(this.label27);
            this.kj_panel2.Controls.Add(this.label28);
            this.kj_panel2.Controls.Add(this.label30);
            this.kj_panel2.Controls.Add(this.kj_xylx_cmb);
            this.kj_panel2.Controls.Add(this.label31);
            this.kj_panel2.Controls.Add(this.label32);
            this.kj_panel2.Controls.Add(this.label33);
            this.kj_panel2.Controls.Add(this.label34);
            this.kj_panel2.Controls.Add(this.label35);
            this.kj_panel2.Location = new System.Drawing.Point(9, 5);
            this.kj_panel2.Name = "kj_panel2";
            this.kj_panel2.Size = new System.Drawing.Size(655, 395);
            this.kj_panel2.TabIndex = 157;
            // 
            // kj_wlmc2_cmb
            // 
            this.kj_wlmc2_cmb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.kj_wlmc2_cmb.FormattingEnabled = true;
            this.kj_wlmc2_cmb.Location = new System.Drawing.Point(440, 218);
            this.kj_wlmc2_cmb.Name = "kj_wlmc2_cmb";
            this.kj_wlmc2_cmb.Size = new System.Drawing.Size(200, 22);
            this.kj_wlmc2_cmb.TabIndex = 158;
            // 
            // kj_wlbh2_cmb
            // 
            this.kj_wlbh2_cmb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.kj_wlbh2_cmb.FormattingEnabled = true;
            this.kj_wlbh2_cmb.ItemHeight = 14;
            this.kj_wlbh2_cmb.Location = new System.Drawing.Point(90, 218);
            this.kj_wlbh2_cmb.Name = "kj_wlbh2_cmb";
            this.kj_wlbh2_cmb.Size = new System.Drawing.Size(200, 22);
            this.kj_wlbh2_cmb.TabIndex = 156;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("宋体", 9F);
            this.label29.Location = new System.Drawing.Point(30, 222);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(53, 12);
            this.label29.TabIndex = 155;
            this.label29.Text = "物料编号";
            // 
            // kj_qysj_dt
            // 
            this.kj_qysj_dt.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.kj_qysj_dt.Location = new System.Drawing.Point(90, 113);
            this.kj_qysj_dt.Name = "kj_qysj_dt";
            this.kj_qysj_dt.Size = new System.Drawing.Size(200, 23);
            this.kj_qysj_dt.TabIndex = 154;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("宋体", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label24.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label24.Location = new System.Drawing.Point(-1, 337);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(87, 15);
            this.label24.TabIndex = 153;
            this.label24.Text = "供应商信息";
            // 
            // kj_xqdh_tb
            // 
            this.kj_xqdh_tb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.kj_xqdh_tb.Location = new System.Drawing.Point(90, 12);
            this.kj_xqdh_tb.Name = "kj_xqdh_tb";
            this.kj_xqdh_tb.Size = new System.Drawing.Size(550, 23);
            this.kj_xqdh_tb.TabIndex = 152;
            // 
            // kj_xymc_tb
            // 
            this.kj_xymc_tb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.kj_xymc_tb.Location = new System.Drawing.Point(440, 72);
            this.kj_xymc_tb.Name = "kj_xymc_tb";
            this.kj_xymc_tb.Size = new System.Drawing.Size(200, 23);
            this.kj_xymc_tb.TabIndex = 151;
            // 
            // kj_gysh_tb
            // 
            this.kj_gysh_tb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.kj_gysh_tb.Location = new System.Drawing.Point(90, 152);
            this.kj_gysh_tb.Name = "kj_gysh_tb";
            this.kj_gysh_tb.Size = new System.Drawing.Size(200, 23);
            this.kj_gysh_tb.TabIndex = 150;
            // 
            // kj_xybh_tb
            // 
            this.kj_xybh_tb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.kj_xybh_tb.Location = new System.Drawing.Point(90, 72);
            this.kj_xybh_tb.Name = "kj_xybh_tb";
            this.kj_xybh_tb.Size = new System.Drawing.Size(200, 23);
            this.kj_xybh_tb.TabIndex = 148;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("宋体", 9F);
            this.label16.Location = new System.Drawing.Point(380, 422);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(53, 12);
            this.label16.TabIndex = 77;
            this.label16.Text = "通过日期";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("宋体", 9F);
            this.label25.Location = new System.Drawing.Point(30, 422);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(53, 12);
            this.label25.TabIndex = 76;
            this.label25.Text = "审核状态";
            // 
            // kj_gysmc_tb
            // 
            this.kj_gysmc_tb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.kj_gysmc_tb.Location = new System.Drawing.Point(440, 363);
            this.kj_gysmc_tb.Name = "kj_gysmc_tb";
            this.kj_gysmc_tb.Size = new System.Drawing.Size(200, 23);
            this.kj_gysmc_tb.TabIndex = 71;
            // 
            // kj_gysbh_tb
            // 
            this.kj_gysbh_tb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.kj_gysbh_tb.Location = new System.Drawing.Point(90, 363);
            this.kj_gysbh_tb.Name = "kj_gysbh_tb";
            this.kj_gysbh_tb.Size = new System.Drawing.Size(200, 23);
            this.kj_gysbh_tb.TabIndex = 70;
            // 
            // kj_wlgg_tb
            // 
            this.kj_wlgg_tb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.kj_wlgg_tb.Location = new System.Drawing.Point(440, 258);
            this.kj_wlgg_tb.Name = "kj_wlgg_tb";
            this.kj_wlgg_tb.Size = new System.Drawing.Size(200, 23);
            this.kj_wlgg_tb.TabIndex = 68;
            // 
            // kj_qqjg_tb
            // 
            this.kj_qqjg_tb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.kj_qqjg_tb.Location = new System.Drawing.Point(440, 298);
            this.kj_qqjg_tb.Name = "kj_qqjg_tb";
            this.kj_qqjg_tb.Size = new System.Drawing.Size(200, 23);
            this.kj_qqjg_tb.TabIndex = 67;
            // 
            // kj_wlbz_tb
            // 
            this.kj_wlbz_tb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.kj_wlbz_tb.Location = new System.Drawing.Point(90, 298);
            this.kj_wlbz_tb.Name = "kj_wlbz_tb";
            this.kj_wlbz_tb.Size = new System.Drawing.Size(200, 23);
            this.kj_wlbz_tb.TabIndex = 66;
            // 
            // kj_wlph_tb
            // 
            this.kj_wlph_tb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.kj_wlph_tb.Location = new System.Drawing.Point(90, 258);
            this.kj_wlph_tb.Name = "kj_wlph_tb";
            this.kj_wlph_tb.Size = new System.Drawing.Size(200, 23);
            this.kj_wlph_tb.TabIndex = 65;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("宋体", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label17.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label17.Location = new System.Drawing.Point(15, 192);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(71, 15);
            this.label17.TabIndex = 62;
            this.label17.Text = "物料信息";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("宋体", 9F);
            this.label18.Location = new System.Drawing.Point(380, 302);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(53, 12);
            this.label18.TabIndex = 61;
            this.label18.Text = "前期价格";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("宋体", 9F);
            this.label19.Location = new System.Drawing.Point(30, 302);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(53, 12);
            this.label19.TabIndex = 60;
            this.label19.Text = "物料标准";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("宋体", 9F);
            this.label20.Location = new System.Drawing.Point(380, 262);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(53, 12);
            this.label20.TabIndex = 58;
            this.label20.Text = "物料规格";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("宋体", 9F);
            this.label21.Location = new System.Drawing.Point(30, 262);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(53, 12);
            this.label21.TabIndex = 57;
            this.label21.Text = "物料型号";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("宋体", 9F);
            this.label22.Location = new System.Drawing.Point(380, 222);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(53, 12);
            this.label22.TabIndex = 56;
            this.label22.Text = "物料名称";
            // 
            // kj_xysl_tb
            // 
            this.kj_xysl_tb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.kj_xysl_tb.Location = new System.Drawing.Point(440, 153);
            this.kj_xysl_tb.Name = "kj_xysl_tb";
            this.kj_xysl_tb.Size = new System.Drawing.Size(200, 23);
            this.kj_xysl_tb.TabIndex = 52;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("宋体", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label23.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label23.Location = new System.Drawing.Point(12, 47);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(71, 15);
            this.label23.TabIndex = 47;
            this.label23.Text = "协议信息";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("宋体", 9F);
            this.label26.Location = new System.Drawing.Point(368, 367);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(65, 12);
            this.label26.TabIndex = 39;
            this.label26.Text = "供应商名称";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("宋体", 9F);
            this.label27.Location = new System.Drawing.Point(18, 367);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(65, 12);
            this.label27.TabIndex = 37;
            this.label27.Text = "供应商编号";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("宋体", 9F);
            this.label28.Location = new System.Drawing.Point(380, 157);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(53, 12);
            this.label28.TabIndex = 35;
            this.label28.Text = "协议数量";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("宋体", 9F);
            this.label30.Location = new System.Drawing.Point(30, 157);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(53, 12);
            this.label30.TabIndex = 33;
            this.label30.Text = "供应商号";
            // 
            // kj_xylx_cmb
            // 
            this.kj_xylx_cmb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.kj_xylx_cmb.FormattingEnabled = true;
            this.kj_xylx_cmb.Location = new System.Drawing.Point(440, 113);
            this.kj_xylx_cmb.Name = "kj_xylx_cmb";
            this.kj_xylx_cmb.Size = new System.Drawing.Size(200, 22);
            this.kj_xylx_cmb.TabIndex = 32;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("宋体", 9F);
            this.label31.Location = new System.Drawing.Point(380, 117);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(53, 12);
            this.label31.TabIndex = 31;
            this.label31.Text = "协议类型";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("宋体", 9F);
            this.label32.Location = new System.Drawing.Point(30, 117);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(53, 12);
            this.label32.TabIndex = 29;
            this.label32.Text = "签约时间";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("宋体", 9F);
            this.label33.Location = new System.Drawing.Point(380, 77);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(53, 12);
            this.label33.TabIndex = 27;
            this.label33.Text = "协议名称";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("宋体", 9F);
            this.label34.Location = new System.Drawing.Point(30, 77);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(53, 12);
            this.label34.TabIndex = 25;
            this.label34.Text = "协议编号";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("宋体", 9F);
            this.label35.Location = new System.Drawing.Point(30, 17);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(53, 12);
            this.label35.TabIndex = 20;
            this.label35.Text = "需求单号";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.panel4);
            this.panel3.Controls.Add(this.panel2);
            this.panel3.Location = new System.Drawing.Point(1, 129);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(672, 362);
            this.panel3.TabIndex = 0;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.cancel_bt);
            this.panel4.Controls.Add(this.return_bt);
            this.panel4.Controls.Add(this.submit_bt);
            this.panel4.Location = new System.Drawing.Point(11, 279);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(655, 60);
            this.panel4.TabIndex = 157;
            // 
            // cancel_bt
            // 
            this.cancel_bt.Location = new System.Drawing.Point(287, 15);
            this.cancel_bt.Name = "cancel_bt";
            this.cancel_bt.Size = new System.Drawing.Size(100, 30);
            this.cancel_bt.TabIndex = 49;
            this.cancel_bt.Text = "重置";
            this.cancel_bt.UseVisualStyleBackColor = true;
            this.cancel_bt.Click += new System.EventHandler(this.cancel_bt_Click);
            // 
            // return_bt
            // 
            this.return_bt.Location = new System.Drawing.Point(492, 15);
            this.return_bt.Name = "return_bt";
            this.return_bt.Size = new System.Drawing.Size(100, 30);
            this.return_bt.TabIndex = 48;
            this.return_bt.Text = "关闭";
            this.return_bt.UseVisualStyleBackColor = true;
            this.return_bt.Click += new System.EventHandler(this.return_bt_Click);
            // 
            // submit_bt
            // 
            this.submit_bt.Location = new System.Drawing.Point(82, 15);
            this.submit_bt.Name = "submit_bt";
            this.submit_bt.Size = new System.Drawing.Size(100, 30);
            this.submit_bt.TabIndex = 47;
            this.submit_bt.Text = "提交";
            this.submit_bt.UseVisualStyleBackColor = true;
            this.submit_bt.Click += new System.EventHandler(this.submit_bt_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.sqrq);
            this.panel2.Controls.Add(this.sqrq_dt);
            this.panel2.Controls.Add(this.lxdh_tb);
            this.panel2.Controls.Add(this.xqsl_tb);
            this.panel2.Controls.Add(this.mindhpl_tb);
            this.panel2.Controls.Add(this.qtyq_rtb);
            this.panel2.Controls.Add(this.label15);
            this.panel2.Controls.Add(this.label11);
            this.panel2.Controls.Add(this.label12);
            this.panel2.Controls.Add(this.sqr_cmb);
            this.panel2.Controls.Add(this.label13);
            this.panel2.Controls.Add(this.sqbm_cmb);
            this.panel2.Controls.Add(this.label14);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.jldw_tb);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.ckbh_cmb);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Controls.Add(this.xqdh_tb);
            this.panel2.Location = new System.Drawing.Point(11, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(655, 280);
            this.panel2.TabIndex = 155;
            // 
            // sqrq
            // 
            this.sqrq.BackColor = System.Drawing.SystemColors.Window;
            this.sqrq.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.sqrq.Location = new System.Drawing.Point(442, 175);
            this.sqrq.Name = "sqrq";
            this.sqrq.Size = new System.Drawing.Size(163, 18);
            this.sqrq.TabIndex = 164;
            // 
            // sqrq_dt
            // 
            this.sqrq_dt.CustomFormat = "yyyy-MM-dd HH:mm:ss";
            this.sqrq_dt.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.sqrq_dt.Location = new System.Drawing.Point(440, 171);
            this.sqrq_dt.Name = "sqrq_dt";
            this.sqrq_dt.Size = new System.Drawing.Size(200, 23);
            this.sqrq_dt.TabIndex = 167;
            this.sqrq_dt.ValueChanged += new System.EventHandler(this.sqrq_dt_ValueChanged);
            // 
            // lxdh_tb
            // 
            this.lxdh_tb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lxdh_tb.Location = new System.Drawing.Point(90, 171);
            this.lxdh_tb.Name = "lxdh_tb";
            this.lxdh_tb.Size = new System.Drawing.Size(200, 23);
            this.lxdh_tb.TabIndex = 166;
            // 
            // xqsl_tb
            // 
            this.xqsl_tb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xqsl_tb.Location = new System.Drawing.Point(440, 91);
            this.xqsl_tb.Name = "xqsl_tb";
            this.xqsl_tb.Size = new System.Drawing.Size(200, 23);
            this.xqsl_tb.TabIndex = 165;
            // 
            // mindhpl_tb
            // 
            this.mindhpl_tb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.mindhpl_tb.Location = new System.Drawing.Point(90, 91);
            this.mindhpl_tb.Name = "mindhpl_tb";
            this.mindhpl_tb.Size = new System.Drawing.Size(200, 23);
            this.mindhpl_tb.TabIndex = 164;
            // 
            // qtyq_rtb
            // 
            this.qtyq_rtb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.qtyq_rtb.Location = new System.Drawing.Point(90, 212);
            this.qtyq_rtb.Name = "qtyq_rtb";
            this.qtyq_rtb.Size = new System.Drawing.Size(550, 60);
            this.qtyq_rtb.TabIndex = 163;
            this.qtyq_rtb.Text = "无";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("宋体", 9F);
            this.label15.Location = new System.Drawing.Point(30, 215);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(53, 12);
            this.label15.TabIndex = 162;
            this.label15.Text = "其他要求";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("宋体", 9F);
            this.label11.Location = new System.Drawing.Point(380, 175);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(53, 12);
            this.label11.TabIndex = 161;
            this.label11.Text = "申请日期";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("宋体", 9F);
            this.label12.Location = new System.Drawing.Point(30, 175);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(53, 12);
            this.label12.TabIndex = 160;
            this.label12.Text = "联系电话";
            // 
            // sqr_cmb
            // 
            this.sqr_cmb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.sqr_cmb.FormattingEnabled = true;
            this.sqr_cmb.Location = new System.Drawing.Point(440, 131);
            this.sqr_cmb.Name = "sqr_cmb";
            this.sqr_cmb.Size = new System.Drawing.Size(200, 22);
            this.sqr_cmb.TabIndex = 159;
            this.sqr_cmb.SelectedIndexChanged += new System.EventHandler(this.sqr_cmb_SelectedIndexChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("宋体", 9F);
            this.label13.Location = new System.Drawing.Point(392, 135);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(41, 12);
            this.label13.TabIndex = 158;
            this.label13.Text = "申请人";
            this.label13.Click += new System.EventHandler(this.label13_Click);
            // 
            // sqbm_cmb
            // 
            this.sqbm_cmb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.sqbm_cmb.FormattingEnabled = true;
            this.sqbm_cmb.Location = new System.Drawing.Point(90, 131);
            this.sqbm_cmb.Name = "sqbm_cmb";
            this.sqbm_cmb.Size = new System.Drawing.Size(200, 22);
            this.sqbm_cmb.TabIndex = 157;
            this.sqbm_cmb.SelectedIndexChanged += new System.EventHandler(this.sqbm_cmb_SelectedIndexChanged);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("宋体", 9F);
            this.label14.Location = new System.Drawing.Point(30, 135);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(53, 12);
            this.label14.TabIndex = 156;
            this.label14.Text = "申请部门";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("宋体", 9F);
            this.label4.Location = new System.Drawing.Point(380, 95);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 12);
            this.label4.TabIndex = 155;
            this.label4.Text = "需求数量";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("宋体", 9F);
            this.label5.Location = new System.Drawing.Point(6, 95);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(77, 12);
            this.label5.TabIndex = 154;
            this.label5.Text = "最小订货批量";
            // 
            // jldw_tb
            // 
            this.jldw_tb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.jldw_tb.FormattingEnabled = true;
            this.jldw_tb.Location = new System.Drawing.Point(440, 51);
            this.jldw_tb.Name = "jldw_tb";
            this.jldw_tb.Size = new System.Drawing.Size(200, 22);
            this.jldw_tb.TabIndex = 153;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("宋体", 9F);
            this.label8.Location = new System.Drawing.Point(380, 55);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(53, 12);
            this.label8.TabIndex = 152;
            this.label8.Text = "计量单位";
            // 
            // ckbh_cmb
            // 
            this.ckbh_cmb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.ckbh_cmb.FormattingEnabled = true;
            this.ckbh_cmb.Location = new System.Drawing.Point(90, 51);
            this.ckbh_cmb.Name = "ckbh_cmb";
            this.ckbh_cmb.Size = new System.Drawing.Size(200, 22);
            this.ckbh_cmb.TabIndex = 151;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("宋体", 9F);
            this.label9.Location = new System.Drawing.Point(30, 55);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(53, 12);
            this.label9.TabIndex = 150;
            this.label9.Text = "仓库编号";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("宋体", 9F);
            this.label10.Location = new System.Drawing.Point(30, 16);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(53, 12);
            this.label10.TabIndex = 149;
            this.label10.Text = "需求单号";
            // 
            // xqdh_tb
            // 
            this.xqdh_tb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xqdh_tb.Location = new System.Drawing.Point(90, 11);
            this.xqdh_tb.Name = "xqdh_tb";
            this.xqdh_tb.Size = new System.Drawing.Size(550, 23);
            this.xqdh_tb.TabIndex = 148;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.wlfs_cmb);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.cglx_cmb);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.wlmc_cmb);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.wlbh_cmb);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Location = new System.Drawing.Point(12, 50);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(655, 80);
            this.panel1.TabIndex = 154;
            // 
            // wlfs_cmb
            // 
            this.wlfs_cmb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.wlfs_cmb.FormattingEnabled = true;
            this.wlfs_cmb.Items.AddRange(new object[] {
            "物流中心式",
            "统谈统签直送式",
            "统谈分签式"});
            this.wlfs_cmb.Location = new System.Drawing.Point(440, 13);
            this.wlfs_cmb.Name = "wlfs_cmb";
            this.wlfs_cmb.Size = new System.Drawing.Size(200, 22);
            this.wlfs_cmb.TabIndex = 32;
            this.wlfs_cmb.Text = "物流中心式";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("宋体", 9F);
            this.label6.Location = new System.Drawing.Point(380, 17);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 12);
            this.label6.TabIndex = 31;
            this.label6.Text = "物流方式";
            // 
            // cglx_cmb
            // 
            this.cglx_cmb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.cglx_cmb.FormattingEnabled = true;
            this.cglx_cmb.Items.AddRange(new object[] {
            "标准采购类型",
            "框架协议采购类型",
            "委外加工采购类型",
            "寄售采购类型"});
            this.cglx_cmb.Location = new System.Drawing.Point(90, 13);
            this.cglx_cmb.Name = "cglx_cmb";
            this.cglx_cmb.Size = new System.Drawing.Size(200, 22);
            this.cglx_cmb.TabIndex = 30;
            this.cglx_cmb.Text = "标准采购类型";
            this.cglx_cmb.SelectedIndexChanged += new System.EventHandler(this.cglx_cmb_SelectedIndexChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("宋体", 9F);
            this.label7.Location = new System.Drawing.Point(30, 17);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 12);
            this.label7.TabIndex = 29;
            this.label7.Text = "采购类型";
            // 
            // wlmc_cmb
            // 
            this.wlmc_cmb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.wlmc_cmb.FormattingEnabled = true;
            this.wlmc_cmb.Location = new System.Drawing.Point(440, 50);
            this.wlmc_cmb.Name = "wlmc_cmb";
            this.wlmc_cmb.Size = new System.Drawing.Size(200, 22);
            this.wlmc_cmb.TabIndex = 28;
            this.wlmc_cmb.SelectedIndexChanged += new System.EventHandler(this.wlmc_cmb_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("宋体", 9F);
            this.label3.Location = new System.Drawing.Point(380, 54);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 12);
            this.label3.TabIndex = 27;
            this.label3.Text = "物料名称";
            // 
            // wlbh_cmb
            // 
            this.wlbh_cmb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.wlbh_cmb.FormattingEnabled = true;
            this.wlbh_cmb.ItemHeight = 14;
            this.wlbh_cmb.Location = new System.Drawing.Point(90, 50);
            this.wlbh_cmb.Name = "wlbh_cmb";
            this.wlbh_cmb.Size = new System.Drawing.Size(200, 22);
            this.wlbh_cmb.TabIndex = 26;
            this.wlbh_cmb.TextChanged += new System.EventHandler(this.wlbh_cmb_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("宋体", 9F);
            this.label2.Location = new System.Drawing.Point(30, 54);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 25;
            this.label2.Text = "物料编号";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // wlgg
            // 
            this.wlgg.AutoSize = true;
            this.wlgg.Font = new System.Drawing.Font("宋体", 9F);
            this.wlgg.Location = new System.Drawing.Point(673, 105);
            this.wlgg.Name = "wlgg";
            this.wlgg.Size = new System.Drawing.Size(53, 12);
            this.wlgg.TabIndex = 163;
            this.wlgg.Text = "物料规格";
            // 
            // NewStandard_Form
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1410, 664);
            this.Controls.Add(this.wlgg);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel5);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "NewStandard_Form";
            this.Text = "新建需求计划";
            this.panel5.ResumeLayout(false);
            this.kj_panel4.ResumeLayout(false);
            this.kj_panel2.ResumeLayout(false);
            this.kj_panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button cancel_bt;
        private System.Windows.Forms.Button return_bt;
        private System.Windows.Forms.Button submit_bt;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DateTimePicker sqrq_dt;
        private System.Windows.Forms.TextBox lxdh_tb;
        private System.Windows.Forms.TextBox xqsl_tb;
        private System.Windows.Forms.TextBox mindhpl_tb;
        private System.Windows.Forms.RichTextBox qtyq_rtb;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox sqr_cmb;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox sqbm_cmb;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox jldw_tb;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox ckbh_cmb;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox xqdh_tb;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cglx_cmb;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox wlmc_cmb;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox wlbh_cmb;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel kj_panel4;
        private System.Windows.Forms.Button kj_cancel_bt;
        private System.Windows.Forms.Button kj_return_bt;
        private System.Windows.Forms.Button kj_submit_bt;
        private System.Windows.Forms.Panel kj_panel2;
        private System.Windows.Forms.ComboBox kj_wlmc2_cmb;
        private System.Windows.Forms.ComboBox kj_wlbh2_cmb;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.DateTimePicker kj_qysj_dt;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox kj_xqdh_tb;
        private System.Windows.Forms.TextBox kj_xymc_tb;
        private System.Windows.Forms.TextBox kj_gysh_tb;
        private System.Windows.Forms.TextBox kj_xybh_tb;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox kj_gysmc_tb;
        private System.Windows.Forms.TextBox kj_gysbh_tb;
        private System.Windows.Forms.TextBox kj_wlgg_tb;
        private System.Windows.Forms.TextBox kj_qqjg_tb;
        private System.Windows.Forms.TextBox kj_wlbz_tb;
        private System.Windows.Forms.TextBox kj_wlph_tb;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox kj_xysl_tb;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.ComboBox kj_xylx_cmb;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label wlgg;
        private System.Windows.Forms.Label sqrq;
        private System.Windows.Forms.ComboBox wlfs_cmb;
    }
}