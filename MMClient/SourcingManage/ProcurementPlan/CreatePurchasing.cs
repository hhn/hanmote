﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Lib.IDAL.SourcingManage.ProcurementPlan;
using Lib.SqlServerDAL.SourcingManage.ProcurementPlan;
using Lib.Model.SourcingManage.ProcurementPlan;
using Lib.Bll.SourcingManage.ProcurementPlan;
using WeifenLuo.WinFormsUI.Docking;
using Lib.Common.MMCException.IDAL;
using Lib.SqlServerDAL;

namespace MMClient.SourcingManage.ProcurementPlan
{
    public partial class CreatePurchasing_Form : DockContent
    {
        public UserUI userUI;
        Summary_DemandDAL newStandardDAL = new Summary_DemandDAL();
        public DataTable checkDt = null;
        private ManagerPlansForm managerPlansForm = new ManagerPlansForm() ;
        private Summary_Demand summaryDemand = null;
        //需求表格生成行数
        int demandNum = 0;
        int conditingNum = 3;

        //被选中的行值
        int j = 0;
        int condition_j = 0;
        int[] sz = new int[50];

        public CreatePurchasing_Form( UserUI userUi)
        {
            InitializeComponent();
            this.userUI = userUi;
            InitialForm();
            addItemsToCMB();
            addDynamicDrawTable();
            conditionjTypeTable("", "", "", false);
        }

        private void InitialForm()
        {
            //传递参数至计划订单模块
            String item1 = null;
            String item2 = null;
            String item3 = null;

            if (managerPlansForm == null)
            {
                return;
            }
            if (managerPlansForm.checkDt == null || managerPlansForm.checkDt.Rows.Count == 0 || managerPlansForm.j == 0)
            {
                addDynamicDrawTable();
            }
            else
            {
                if (managerPlansForm.checkDt.Rows[0][13].ToString().Equals("审核未通过"))
                {
                    MessageBox.Show("采购计划审核未通过!", "无法生成");
                    return;
                }
                if (managerPlansForm.checkDt.Rows[0][13].ToString().Equals("待审核"))
                {
                    MessageBox.Show("采购计划未审核，请先审核!", "无法生成");
                    return;
                }
                if (managerPlansForm.checkDt.Rows[0][13].ToString().Equals("确定采购价格"))
                {
                    if (MessageBox.Show("该采购计划价格已经生成，是否重新生成？", "提示", MessageBoxButtons.YesNo) == DialogResult.No)
                    {
                        return;
                    }
                }
                //传递参数至计划订单模块
                item1 = managerPlansForm.checkDt.Rows[0][3].ToString().Trim();
                item2 = managerPlansForm.checkDt.Rows[0][0].ToString().Trim();
                item3 = managerPlansForm.checkDt.Rows[0][1].ToString().Trim();
                item1 = " Purchase_Type = '" + item1 + "'";
                item2 = " Demand_ID = '" + item2 + "'";
                item3 = " Material_ID = '" + item3 + "'";
                DynamicDrawTable(item1, item2, item3, true);
            }
            }

        //查询事件 
        private void query_bt_Click(object sender, EventArgs e)
        {
            addDynamicDrawTable();
        }

        /// <summary>
        /// 动态加载查询表格
        /// </summary>
        public void addDynamicDrawTable()
        {
            j = 0;
            String item1 = null;
            String item3 = null;
            if (!string.IsNullOrWhiteSpace(this.cglx_cmb.Text))
            {
                item1 = " Purchase_Type = '" + this.cglx_cmb.Text + "'";
            }
            if (!string.IsNullOrWhiteSpace(this.cgzt_cmb.Text))
            {
                item3 = " state = '" + this.cgzt_cmb.Text + "'";
            }
            try
            {
                DynamicDrawTable(item1, null, item3, false);
            }catch(DBException ex)
            {
                MessageBox.Show("数据加载失败");
            }
            
        }

        /// <summary>
        /// combobox自动加载items
        /// </summary>
        public void addItemsToCMB()
        {
            //需求单号自动加载至item
            DataTable findItem = null;
            try
            {
                findItem = newStandardDAL.FindStandardDemand_ID("state");
                 
            }catch(DBException ex)
            {
                MessageBox.Show("数据加载失败");
                return;
            }
            
            if (findItem != null && findItem.Rows.Count > 0 )
            {
                for (int n = 0; n < findItem.Rows.Count; n++)
                {
                    this.cgzt_cmb.Items.Add(findItem.Rows[n][0].ToString());
                }
            }
            this.cgzt_cmb.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            this.cgzt_cmb.AutoCompleteSource = AutoCompleteSource.ListItems;
            //仓库代码自动加载至item
            findItem = null;
            try
            {
                findItem = newStandardDAL.FindStandardDemand_ID("Stock_ID");
            }
            catch (DBException ex)
            {
                MessageBox.Show("数据加载失败");
                return;
            }
            if (findItem != null && findItem.Rows.Count > 0 )
            {
                for (int n = 0; n < findItem.Rows.Count; n++)
                {
                    this.bjckdm_tb.Items.Add(findItem.Rows[n][0].ToString());
                }
            }
            this.bjckdm_tb.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            this.bjckdm_tb.AutoCompleteSource = AutoCompleteSource.ListItems;
        }

        /// <summary>
        /// 动态删除表格
        /// </summary>
        public void RemoveDrawTable(int rowNum)
        {
            int high = rowNum * this.lbl_1.Height;
            if (rowNum != 0) 
            {
                for (int i = 1; i <= rowNum; i++)
                {
                    this.sjl_panel.Controls.Remove(this.sjl_panel.Controls["ch_"+i]);
                    this.sjl_panel.Controls.Remove(this.sjl_panel.Controls["cb_" + i]);
                    this.sjl_panel.Controls.Remove(this.sjl_panel.Controls["a_" + i]);
                    this.sjl_panel.Controls.Remove(this.sjl_panel.Controls["b_" + i]);
                    this.sjl_panel.Controls.Remove(this.sjl_panel.Controls["c_" + i]);
                    this.sjl_panel.Controls.Remove(this.sjl_panel.Controls["d_" + i]);
                    this.sjl_panel.Controls.Remove(this.sjl_panel.Controls["e_" + i]);
                    this.sjl_panel.Controls.Remove(this.sjl_panel.Controls["f_" + i]);
                    this.sjl_panel.Controls.Remove(this.sjl_panel.Controls["g_" + i]);
                    this.sjl_panel.Controls.Remove(this.sjl_panel.Controls["h_" + i]);
                    this.sjl_panel.Controls.Remove(this.sjl_panel.Controls["i_" + i]);
                    this.sjl_panel.Controls.Remove(this.sjl_panel.Controls["j_" + i]);
                    this.sjl_panel.Controls.Remove(this.sjl_panel.Controls["k_" + i]);
                }

            }
        }

        /// <summary>
        /// 动态画表格
        /// </summary>
        public void DynamicDrawTable(String item1, String item2, String item3, Boolean isFromSH)
        {
            int yuanDemandNum = demandNum;
            DataTable dt = new DataTable();
            if (isFromSH)
            {
                displayOnThis(item1, item2, item3);
                dt = checkDt;
                this.Text = dt.Rows[0][0].ToString() + "生成采购单";
            }
            else
            {
                //查询需求计划table
                dt = newStandardDAL.StandardByWLBH(item1, item2, item3);
            }
            if (dt==null||dt.Rows.Count == 0)
            {
                MessageBox.Show("未找到相关记录，请重新编辑查询词条！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            //取消选中行
            j = 0;
            demandNum = dt.Rows.Count;

            //第一个label的高
            int high = this.lbl_1.Height * demandNum;

            high = this.lbl_1.Height * (demandNum - yuanDemandNum);
            RemoveDrawTable(yuanDemandNum);
            MoveDownPanel(high);
            //动态改变panel的高度
            this.sjl_panel.Height = this.sjl_panel.Height + high;           
            
            //导航条label
            Label lbl_1_head = this.lbl_1;
            Label lbl_2_head = this.lbl_2;
            Label lbl_3_head = this.lbl_3;
           // Label lbl_4_head = this.lbl_4;
            Label lbl_5_head = this.lbl_5;
            Label lbl_6_head = this.lbl_6;
            Label lbl_7_head = this.lbl_7;
            Label lbl_8_head = this.lbl_8;
            Label lbl_9_head = this.lbl_9;
            Label lbl_10_head = this.lbl_10;
            Label lbl_11_head = this.lbl_11;
            Label lbl_12_head = this.lbl_12;

            //label和textbox的高度
            int h = this.lbl_1.Height;
            for (int i = 1; i <= demandNum; i++)
            {
                //lbl_1下的label
                TextBox a_lbl = new TextBox();
                a_lbl.AutoSize = false;
                a_lbl.BorderStyle = BorderStyle.FixedSingle;
                a_lbl.BackColor = System.Drawing.SystemColors.Window;
                a_lbl.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
                a_lbl.Name = "ch_" + i;
                a_lbl.TextAlign = HorizontalAlignment.Center;
                a_lbl.ReadOnly = true;
                a_lbl.Width = lbl_1_head.Width;
                a_lbl.Height = h;
                a_lbl.Location = new Point(lbl_1_head.Location.X, lbl_1_head.Location.Y + lbl_1_head.Height + (h - 1) * (i - 1) - 1);
                //添加选择框
                RadioButton a_cb = new RadioButton();
                a_cb.BringToFront();
                a_cb.AutoSize = true;
                a_cb.Name = "cb_" + i;
                a_cb.BackColor = System.Drawing.SystemColors.ButtonHighlight;
                a_cb.Size = new System.Drawing.Size(18, 18);
                a_cb.TabIndex = 121;
                a_cb.UseVisualStyleBackColor = false;
                a_cb.Location = new Point(lbl_1_head.Location.X + lbl_1_head.Width / 4 + 1, lbl_1_head.Location.Y + lbl_1_head.Height + (h - 1) * (i - 1) - 1 + h/4);
                
                //lbl_2下的textbox
                TextBox a_txt = new TextBox();
                a_txt.AutoSize = false;
                a_txt.BorderStyle = BorderStyle.FixedSingle;
                a_txt.BackColor = System.Drawing.SystemColors.Window;
                a_txt.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
                a_txt.Name = "a_" + i;
                a_txt.Text = i.ToString();
                a_txt.ReadOnly = true;
                a_txt.TextAlign = HorizontalAlignment.Center;
                a_txt.Width = lbl_2_head.Width;
                a_txt.Height = h;
                a_txt.Location = new Point(lbl_2_head.Location.X, lbl_2_head.Location.Y + lbl_2_head.Height + (h - 1) * (i - 1) - 1);

                //lbl_3下的textbox
                TextBox b_txt = new TextBox();
                b_txt.BorderStyle = BorderStyle.FixedSingle;
                b_txt.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
                b_txt.Name = "b_" + i;
                b_txt.TextAlign = HorizontalAlignment.Center;
                b_txt.Width = lbl_3_head.Width;
                b_txt.Height = h;
                b_txt.Location = new Point(lbl_3_head.Location.X, lbl_3_head.Location.Y + lbl_3_head.Height + (h - 1) * (i - 1) - 1);
                b_txt.Text = dt.Rows[i-1][0].ToString();

                //lbl_4下的textbox
                /*
                TextBox c_txt = new TextBox();
                c_txt.BorderStyle = BorderStyle.FixedSingle;
                c_txt.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
                c_txt.Name = "c_" + i;
                c_txt.TextAlign = HorizontalAlignment.Center;
                c_txt.Width = lbl_4_head.Width;
                c_txt.Height = h;
                c_txt.Location = new Point(lbl_4_head.Location.X, lbl_4_head.Location.Y + lbl_4_head.Height + (h - 1) * (i - 1) - 1);
                //根据采购状态判断采购单号
                if (dt.Rows[i - 1][14].ToString().Equals("已完成"))
                {
                    c_txt.Text = "PZ" + dt.Rows[i - 1][0].ToString();
                }
                */

                //lbl_5下的textbox
                TextBox d_txt = new TextBox();
                d_txt.BorderStyle = BorderStyle.FixedSingle;
                d_txt.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
                d_txt.Name = "d_" + i;
                d_txt.TextAlign = HorizontalAlignment.Center;
                d_txt.Width = lbl_5_head.Width;
                d_txt.Height = h;
                d_txt.Location = new Point(lbl_5_head.Location.X, lbl_5_head.Location.Y + lbl_5_head.Height + (h - 1) * (i - 1) - 1);
                /*根据采购状态判断采购计划状态
                if (dt.Rows[i - 1][14].ToString().Equals("已完成"))
                {
                    d_txt.ForeColor = System.Drawing.Color.Red;
                }
                d_txt.Text = dt.Rows[i - 1][14].ToString();
                 */
                d_txt.Text = dt.Rows[i-1][13].ToString();

                //lbl_6下的textbox
                TextBox e_txt = new TextBox();
                e_txt.BorderStyle = BorderStyle.FixedSingle;
                e_txt.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
                e_txt.Name = "e_" + i;
                e_txt.TextAlign = HorizontalAlignment.Center;
                e_txt.Width = lbl_6_head.Width;
                e_txt.Height = h;
                e_txt.Location = new Point(lbl_6_head.Location.X, lbl_6_head.Location.Y + lbl_6_head.Height + (h - 1) * (i - 1) - 1);
                e_txt.Text = dt.Rows[i - 1][1].ToString();

                //lbl_7下的textbox
                TextBox f_txt = new TextBox();
                f_txt.BorderStyle = BorderStyle.FixedSingle;
                f_txt.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
                f_txt.Name = "f_" + i;
                f_txt.TextAlign = HorizontalAlignment.Center;
                f_txt.Width = lbl_7_head.Width;
                f_txt.Height = h;
                f_txt.Location = new Point(lbl_7_head.Location.X, lbl_7_head.Location.Y + lbl_7_head.Height + (h - 1) * (i - 1) - 1);
                f_txt.Text = dt.Rows[i - 1][3].ToString();


                //lbl_8下的textbox
                TextBox g_txt = new TextBox();
                g_txt.BorderStyle = BorderStyle.FixedSingle;
                g_txt.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
                g_txt.Name = "g_" + i;
                g_txt.TextAlign = HorizontalAlignment.Center;
                g_txt.Width = lbl_8_head.Width;
                g_txt.Height = h;
                g_txt.Location = new Point(lbl_8_head.Location.X, lbl_8_head.Location.Y + lbl_8_head.Height + (h - 1) * (i - 1) - 1);
                g_txt.Text = dt.Rows[i - 1][6].ToString();

                //lbl_9下的textbox
                TextBox h_txt = new TextBox();
                h_txt.BorderStyle = BorderStyle.FixedSingle;
                h_txt.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
                h_txt.Name = "h_" + i;
                h_txt.TextAlign = HorizontalAlignment.Center;
                h_txt.Width = lbl_9_head.Width;
                h_txt.Height = h;
                h_txt.Location = new Point(lbl_9_head.Location.X, lbl_9_head.Location.Y + lbl_9_head.Height + (h - 1) * (i - 1) - 1);
                h_txt.Text = dt.Rows[i - 1][8].ToString();

                //lbl_10下的textbox
                TextBox i_txt = new TextBox();
                i_txt.BorderStyle = BorderStyle.FixedSingle;
                i_txt.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
                i_txt.Name = "i_" + i;
                i_txt.TextAlign = HorizontalAlignment.Center;
                i_txt.Width = lbl_10_head.Width;
                i_txt.Height = h;
                i_txt.Location = new Point(lbl_10_head.Location.X, lbl_10_head.Location.Y + lbl_10_head.Height + (h - 1) * (i - 1) - 1);
                /*根据采购状态判断采购价格
                if (dt.Rows[i - 1][14].ToString().Equals("已完成"))
                {
                    i_txt.ForeColor = System.Drawing.Color.Red;
                    i_txt.Text = "1500";
                }
                 */
                i_txt.ForeColor = System.Drawing.Color.Red;
                i_txt.Text = dt.Rows[i-1][14].ToString();

                //lbl_11下的textbox
                TextBox j_txt = new TextBox();
                j_txt.BorderStyle = BorderStyle.FixedSingle;
                j_txt.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
                j_txt.Name = "j_" + i;
                j_txt.TextAlign = HorizontalAlignment.Center;
                j_txt.Width = lbl_11_head.Width;
                j_txt.Height = h;
                j_txt.Location = new Point(lbl_11_head.Location.X, lbl_11_head.Location.Y + lbl_11_head.Height + (h - 1) * (i - 1) - 1);
                j_txt.Text = dt.Rows[i - 1][21].ToString();

                //lbl_12下的textbox
                TextBox k_txt = new TextBox();
                k_txt.BorderStyle = BorderStyle.FixedSingle;
                k_txt.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
                k_txt.Name = "k_" + i;
                k_txt.TextAlign = HorizontalAlignment.Center;
                k_txt.Width = lbl_12_head.Width;
                k_txt.Height = h;
                k_txt.Location = new Point(lbl_12_head.Location.X, lbl_12_head.Location.Y + lbl_12_head.Height + (h - 1) * (i - 1) - 1);
                //根据采购状态判断采购价格
                if (dt.Rows[i - 1][14].ToString().Equals("已完成"))
                {
                    k_txt.Text = "采购订单";
                }

                //绑定事件
                /*
                a_txt.KeyPress += new KeyPressEventHandler(TextBox_KeyPress);
                b_txt.KeyPress += new KeyPressEventHandler(TextBox_KeyPress);
                c_txt.KeyPress += new KeyPressEventHandler(TextBox_KeyPress);
                a_txt.TextChanged += new EventHandler(TextBox_TextChanged);
                b_txt.TextChanged += new EventHandler(TextBox_TextChanged);
                c_txt.TextChanged += new EventHandler(TextBox_TextChanged);
                a_txt.Validating += new CancelEventHandler(txt_Validating);
                b_txt.Validating += new CancelEventHandler(txt_Validating);
                c_txt.Validating += new CancelEventHandler(txt_Validating);
                */
                a_cb.CheckedChanged += new EventHandler(a_cb_CheckedChanged);
               
                //将控件加到窗体中
                this.sjl_panel.Controls.Add(a_cb);
                this.sjl_panel.Controls.Add(a_lbl);
                this.sjl_panel.Controls.Add(a_txt);
                this.sjl_panel.Controls.Add(b_txt);
                //this.sjl_panel.Controls.Add(c_txt);
                this.sjl_panel.Controls.Add(d_txt);
                this.sjl_panel.Controls.Add(e_txt);
                this.sjl_panel.Controls.Add(f_txt);
                this.sjl_panel.Controls.Add(g_txt);
                this.sjl_panel.Controls.Add(h_txt);
                this.sjl_panel.Controls.Add(i_txt);
                this.sjl_panel.Controls.Add(j_txt);
                this.sjl_panel.Controls.Add(k_txt);
            }
            
        }

        /// <summary>
        /// 将数据表格panel以下的容器控件往下平移hv_high高
        /// </summary>
        /// <param name="hv_high">移动的高度</param>
        public void MoveDownPanel(int add_high)
        {
            #region 下面的每个容器控件，向下平移
            //动态改变结论tabControl1的location
            this.tabControl1.Location = new Point(this.tabControl1.Location.X, this.tabControl1.Location.Y + add_high);
            #endregion
        }
        public void MoveDownButten(int add_high)
        {
            #region 下面的每个容器控件，向下平移
            this.qdsure_bt.Location = new Point(this.qdsure_bt.Location.X, this.qdsure_bt.Location.Y + add_high);
            this.qdcancel_bt.Location = new Point(this.qdcancel_bt.Location.X, this.qdcancel_bt.Location.Y + add_high);
            this.tabControl1.Height += add_high;
            #endregion
        }

        /// <summary>
        /// 从审核处接收采购信息数据
        /// </summary>
        public void displayOnThis(String item1, String item2, String item3)
        {
            DataTable dt = new DataTable();
            dt = newStandardDAL.StandardByWLBH(item1, item2, item3);
            if (dt == null || dt.Rows.Count == 0) 
            {
                return;
            }
            item1 = item1.Substring(item1.IndexOf("'") + 1, item1.LastIndexOf("'") - item1.IndexOf("'")-1);
            item2 = item2.Substring(item2.IndexOf("'") + 1, item2.LastIndexOf("'") - item2.IndexOf("'")-1);
            item3 = item3.Substring(item3.IndexOf("'") + 1, item3.LastIndexOf("'") - item3.IndexOf("'")-1);
            this.cglx_cmb.Text = item1;
            this.qdcglx_cmb.Text = item1;
            this.qdwlbh_cmb.Text = item3;
            this.qdwlmc_cmb.Text = dt.Rows[0][2].ToString();
            this.cgzt_cmb.Text = dt.Rows[0][13].ToString();
            checkDt = dt;

            //处理选中单号的采购价格、库存比较、预算管控及生成采购单
            purchasePrice(checkDt);
        }

        /// <summary>
        /// 选中项的采购信息数据
        /// </summary>
        private void a_cb_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton cb = sender as RadioButton;

            //前一选中整行颜色变回
            if (j != 0)
            {
                this.sjl_panel.Controls["ch_" + j].BackColor = System.Drawing.SystemColors.Window;
                this.sjl_panel.Controls["a_" + j].BackColor = System.Drawing.SystemColors.Window;
                this.sjl_panel.Controls["b_" + j].BackColor = System.Drawing.SystemColors.Window;
                //this.sjl_panel.Controls["c_" + j].BackColor = System.Drawing.SystemColors.Window;
                this.sjl_panel.Controls["d_" + j].BackColor = System.Drawing.SystemColors.Window;
                this.sjl_panel.Controls["e_" + j].BackColor = System.Drawing.SystemColors.Window;
                this.sjl_panel.Controls["f_" + j].BackColor = System.Drawing.SystemColors.Window;
                this.sjl_panel.Controls["g_" + j].BackColor = System.Drawing.SystemColors.Window;
                this.sjl_panel.Controls["h_" + j].BackColor = System.Drawing.SystemColors.Window;
                this.sjl_panel.Controls["i_" + j].BackColor = System.Drawing.SystemColors.Window;
                this.sjl_panel.Controls["j_" + j].BackColor = System.Drawing.SystemColors.Window;
                this.sjl_panel.Controls["k_" + j].BackColor = System.Drawing.SystemColors.Window;
            }

            //选中的行值
            j = Convert.ToInt32(cb.Name.ToString().Substring(3, cb.Name.ToString().Length - 3));

            //选中整行变色
            this.sjl_panel.Controls["ch_" + j].BackColor = System.Drawing.SystemColors.ButtonFace;
            this.sjl_panel.Controls["a_" + j].BackColor = System.Drawing.SystemColors.ButtonFace;
            this.sjl_panel.Controls["b_" + j].BackColor = System.Drawing.SystemColors.ButtonFace;
            //this.sjl_panel.Controls["c_" + j].BackColor = System.Drawing.SystemColors.ButtonFace;
            this.sjl_panel.Controls["d_" + j].BackColor = System.Drawing.SystemColors.ButtonFace;
            this.sjl_panel.Controls["e_" + j].BackColor = System.Drawing.SystemColors.ButtonFace;
            this.sjl_panel.Controls["f_" + j].BackColor = System.Drawing.SystemColors.ButtonFace;
            this.sjl_panel.Controls["g_" + j].BackColor = System.Drawing.SystemColors.ButtonFace;
            this.sjl_panel.Controls["h_" + j].BackColor = System.Drawing.SystemColors.ButtonFace;
            this.sjl_panel.Controls["i_" + j].BackColor = System.Drawing.SystemColors.ButtonFace;
            this.sjl_panel.Controls["j_" + j].BackColor = System.Drawing.SystemColors.ButtonFace;
            this.sjl_panel.Controls["k_" + j].BackColor = System.Drawing.SystemColors.ButtonFace;

            //获取选中行的需求单号ID并读取信息
            string ID = this.sjl_panel.Controls["b_" + j].Text;
            this.Text = ID + "生成采购单";
            String item1 = " Demand_ID = '" + ID + "'";
            String item2 = null;
            String item3 = null;
            checkDt = newStandardDAL.StandardByWLBH(item1, item2, item3);
            if (checkDt == null || checkDt.Rows.Count == 0) 
            {
                MessageBox.Show("未找到相关记录，请重新查询选择！");
                return;
            }

            //处理选中单号的采购价格、库存比较、预算管控及生成采购单
            purchasePrice(checkDt);
        }

        /// <summary>
        /// 选中项的确定采购价格page
        /// </summary>
        private void purchasePrice(DataTable dt)
        {
            //主要信息显示
            this.qdwlbh_cmb.Text = checkDt.Rows[0][1].ToString();
            this.qdcglx_cmb.Text = checkDt.Rows[0][3].ToString();
            this.qdwlmc_cmb.Text = checkDt.Rows[0][2].ToString();
            RemoveConditionTable(conditingNum);
            conditionjTypeTable("", "", "", false);
        }

        /// <summary>
        /// 条件类型添加事件
        /// </summary>
        private void qdadd_bt_Click(object sender, EventArgs e)
        {
            conditingNum++;
            conditionjTypeTable("", "", "", true);
        }

        /// <summary>
        /// 条件类型动态加载表格
        /// </summary>
        public void conditionjTypeTable(String item1, String item2, String item3, Boolean isFromSH)
        {
            //查询需求计划table
            DataTable dt = new DataTable();
            try
            {
                dt = newStandardDAL.StandardByWLBH(item1, item2, item3);
            }
            catch (DBException ex)
            {
                MessageBox.Show("数据加载失败");
            }
            
            if (dt == null || dt.Rows.Count == 0)
            {
                MessageBox.Show("未找到相关记录，请重新编辑查询词条！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            //conditingNum = dt.Rows.Count;

            //第一个label的高
            int high = this.tjlxlbl_1.Height * conditingNum;
            if (isFromSH)
            {
                MoveDownButten(this.tjlxlbl_1.Height);
                //动态改变panel的高度
                this.tjlx_panel.Height = this.tjlx_panel.Height + this.tjlxlbl_1.Height;
            }
            else
            {
                MoveDownButten(high);
                //动态改变panel的高度
                this.tjlx_panel.Height = this.tjlx_panel.Height + high;
            }

            //导航条label
            Label lbl_1_head = this.tjlxlbl_1;
            Label lbl_2_head = this.tjlxlbl_2;
            Label lbl_3_head = this.tjlxlbl_3;
            Label lbl_4_head = this.tjlxlbl_4;
            Label lbl_5_head = this.tjlxlbl_5;
            Label lbl_6_head = this.tjlxlbl_6;
            Label lbl_7_head = this.tjlxlbl_7;

            //label和textbox的高度
            int h = this.tjlxlbl_1.Height;
            sz[0] = 0;
            //条件类型自动加载至item
            DataTable findItem = null;
            try
            {
                findItem  = newStandardDAL.AddItemsToCombobox("Condition", "Price_Condition");  findItem = newStandardDAL.AddItemsToCombobox("Condition", "Price_Condition");
            }
            catch (DBException ex)
            {
                MessageBox.Show("数据加载失败");
            }
          

            for (int i = 1; i <= conditingNum; i++)
            {
                if (isFromSH) 
                {
                    i = conditingNum;
                }
                //初始选中状态为0
                sz[i] = 0;

                //lbl_1下的label
                TextBox a_lbl = new TextBox();
                a_lbl.AutoSize = false;
                a_lbl.BackColor = System.Drawing.SystemColors.Window;
                a_lbl.Name = "ch_" + i;
                a_lbl.TextAlign = HorizontalAlignment.Center;
                a_lbl.ReadOnly = true;
                a_lbl.Width = lbl_1_head.Width;
                a_lbl.Height = h;
                a_lbl.Location = new Point(lbl_1_head.Location.X, lbl_1_head.Location.Y + lbl_1_head.Height + (h - 1) * (i - 1) - 1);
                //添加选择框
                CheckBox a_cb = new CheckBox();
                a_cb.BringToFront();
                a_cb.AutoSize = true;
                a_cb.Name = "cb_" + i;
                a_cb.BackColor = System.Drawing.SystemColors.ButtonHighlight;
                a_cb.Size = new System.Drawing.Size(18, 18);
                a_cb.TabIndex = 121;
                a_cb.UseVisualStyleBackColor = false;
                a_cb.Location = new Point(lbl_1_head.Location.X + lbl_1_head.Width / 4 + 1, lbl_1_head.Location.Y + lbl_1_head.Height + (h - 1) * (i - 1) - 1 + h / 4);

                //lbl_2下的textbox
                TextBox a_txt = new TextBox();
                a_txt.AutoSize = false;
                a_txt.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
                a_txt.BackColor = System.Drawing.SystemColors.Window;
                a_txt.Name = "a_" + i;
                a_txt.Text = i.ToString();
                a_txt.ReadOnly = true;
                a_txt.TextAlign = HorizontalAlignment.Center;
                a_txt.Width = lbl_2_head.Width;
                a_txt.Height = h;
                a_txt.Location = new Point(lbl_2_head.Location.X, lbl_2_head.Location.Y + lbl_2_head.Height + (h - 1) * (i - 1) - 1);

                //lbl_3下的textbox
                ComboBox b_txt = new ComboBox();
                b_txt.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
                b_txt.Name = "b_" + i;
                b_txt.Width = lbl_3_head.Width;
                b_txt.Height = h;
                b_txt.Location = new Point(lbl_3_head.Location.X, lbl_3_head.Location.Y + lbl_3_head.Height + (h - 1) * (i - 1) - 1);
                //条件类型自动加载至item
                if (i == 1)
                {
                    b_txt.Text = "总价格";
                    b_txt.Items.Add("总价格");
                    b_txt.Items.Add("净价格");
                    b_txt.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList; 
                }
                else if (findItem != null && findItem.Rows.Count >0)
                {
                    for (int n = 0; n < findItem.Rows.Count; n++)
                    {
                        b_txt.Items.Add(findItem.Rows[n][0].ToString());
                    }
                }


                //lbl_4下的ComboBox
                ComboBox c_txt = new ComboBox();
                c_txt.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
                c_txt.Name = "c_" + i;
                c_txt.Text = "价格";
                c_txt.Items.Add("价格");
                c_txt.Items.Add("折扣");
                c_txt.Items.Add("附加费");
                c_txt.Items.Add("折上折");
                c_txt.Items.Add("税费");
                c_txt.Width = lbl_4_head.Width;
                c_txt.Height = h;
                c_txt.Location = new Point(lbl_4_head.Location.X, lbl_4_head.Location.Y + lbl_4_head.Height + (h - 1) * (i - 1) - 1);
               
                //lbl_5下的ComboBox
                ComboBox d_txt = new ComboBox();
                d_txt.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
                d_txt.Name = "d_" + i;
                d_txt.Items.Add("四舍五入");
                d_txt.Items.Add("舍入到较高值");
                d_txt.Items.Add("舍入到较低值");
                d_txt.Width = lbl_5_head.Width;
                d_txt.Height = h;
                d_txt.Location = new Point(lbl_5_head.Location.X, lbl_5_head.Location.Y + lbl_5_head.Height + (h - 1) * (i - 1) - 1);
                
                //lbl_6下的ComboBox
                ComboBox e_txt = new ComboBox();
                e_txt.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
                e_txt.Name = "e_" + i;
                e_txt.Text = "正";
                e_txt.Items.Add("正");
                e_txt.Items.Add("负");
                e_txt.Width = lbl_6_head.Width;
                e_txt.Height = h;
                e_txt.Location = new Point(lbl_6_head.Location.X, lbl_6_head.Location.Y + lbl_6_head.Height + (h - 1) * (i - 1) - 1);
                
                //lbl_7下的ComboBox
                TextBox f_txt = new TextBox();
                f_txt.AutoSize = false;
                f_txt.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
                f_txt.BackColor = System.Drawing.SystemColors.Window;
                f_txt.Name = "f_" + i;
                f_txt.TextAlign = HorizontalAlignment.Center;
                f_txt.Width = lbl_7_head.Width;
                f_txt.Height = h;
                f_txt.Location = new Point(lbl_7_head.Location.X, lbl_7_head.Location.Y + lbl_7_head.Height + (h - 1) * (i - 1) - 1);

                //绑定事件
                /*
                a_txt.KeyPress += new KeyPressEventHandler(TextBox_KeyPress);
                b_txt.KeyPress += new KeyPressEventHandler(TextBox_KeyPress);
                c_txt.KeyPress += new KeyPressEventHandler(TextBox_KeyPress);
                a_txt.TextChanged += new EventHandler(TextBox_TextChanged);
                b_txt.TextChanged += new EventHandler(TextBox_TextChanged);
                c_txt.TextChanged += new EventHandler(TextBox_TextChanged);
                a_txt.Validating += new CancelEventHandler(txt_Validating);
                b_txt.Validating += new CancelEventHandler(txt_Validating);
                c_txt.Validating += new CancelEventHandler(txt_Validating);
                */
                a_cb.CheckedChanged += new EventHandler(condition_a_cb_CheckedChanged);
                c_txt.TextChanged += new EventHandler(sign_CheckedChanged);

                //将控件加到窗体中
                this.tjlx_panel.Controls.Add(a_cb);
                this.tjlx_panel.Controls.Add(a_lbl);
                this.tjlx_panel.Controls.Add(a_txt);
                this.tjlx_panel.Controls.Add(b_txt);
                this.tjlx_panel.Controls.Add(c_txt);
                this.tjlx_panel.Controls.Add(d_txt);
                this.tjlx_panel.Controls.Add(e_txt);
                this.tjlx_panel.Controls.Add(f_txt);
            }
        }

        /// <summary>
        /// 选中的条件类型项
        /// </summary>
        private void condition_a_cb_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox cb = sender as CheckBox;
            //选中的行值
            condition_j = Convert.ToInt32(cb.Name.ToString().Substring(3, cb.Name.ToString().Length-3));
            if (cb.Checked)
            {
                sz[condition_j] = 1;
            }
            else 
            { 
                sz[condition_j] = 0;
            }
        }

        /// <summary>
        /// 正负值随定价等级变化
        /// </summary>
        private void sign_CheckedChanged(object sender, EventArgs e)
        {
            ComboBox cmb = sender as ComboBox;
            int i = Convert.ToInt32(cmb.Name.ToString().Substring(2, cmb.Name.ToString().Length - 2));
            if (cmb.Text.ToString().Trim().Equals("折扣"))
            {
                this.tjlx_panel.Controls["e_" + i].Text = "负";
            }
            else if (cmb.Text.ToString().Trim().Equals("折上折"))
            {
                this.tjlx_panel.Controls["e_" + i].Text = "负";
            }
        }

        /// <summary>
        /// 条件类型动态加载表格  删除按钮
        /// </summary>
        private void qddel_bt_Click(object sender, EventArgs e)
        {
            CheckBox cbox = null;
            int countNULL = conditingNum;
            for (int n = 1; n <= conditingNum; n++) 
            {
                if (sz[n] == 1)
                {
                    for (int i = n; i < conditingNum; i++)
                    {
                        cbox = this.tjlx_panel.Controls["cb_" + i] as CheckBox;
                        sz[i] = sz[i + 1];
                        if (sz[i] == 1) 
                        {
                            cbox.Checked = true;
                        }
                        else if (sz[i] == 0) 
                        { 
                            cbox.Checked = false; 
                        }
                        this.tjlx_panel.Controls["b_" + i].Text = this.tjlx_panel.Controls["b_" + (i + 1)].Text.ToString();
                        this.tjlx_panel.Controls["c_" + i].Text = this.tjlx_panel.Controls["c_" + (i + 1)].Text.ToString();
                        this.tjlx_panel.Controls["d_" + i].Text = this.tjlx_panel.Controls["d_" + (i + 1)].Text.ToString();
                        this.tjlx_panel.Controls["e_" + i].Text = this.tjlx_panel.Controls["e_" + (i + 1)].Text.ToString();
                        this.tjlx_panel.Controls["f_" + i].Text = this.tjlx_panel.Controls["f_" + (i + 1)].Text.ToString();
                    }
                    //删除最后一行
                    this.tjlx_panel.Controls.Remove(this.tjlx_panel.Controls["ch_" + conditingNum]);
                    this.tjlx_panel.Controls.Remove(this.tjlx_panel.Controls["cb_" + conditingNum]);
                    this.tjlx_panel.Controls.Remove(this.tjlx_panel.Controls["a_" + conditingNum]);
                    this.tjlx_panel.Controls.Remove(this.tjlx_panel.Controls["b_" + conditingNum]);
                    this.tjlx_panel.Controls.Remove(this.tjlx_panel.Controls["c_" + conditingNum]);
                    this.tjlx_panel.Controls.Remove(this.tjlx_panel.Controls["d_" + conditingNum]);
                    this.tjlx_panel.Controls.Remove(this.tjlx_panel.Controls["e_" + conditingNum]);
                    this.tjlx_panel.Controls.Remove(this.tjlx_panel.Controls["f_" + conditingNum]);
                    sz[conditingNum] = 0;
                    conditingNum--;
                    //动态改变panel的高度
                    this.tjlx_panel.Height = this.tjlx_panel.Height - this.tjlxlbl_1.Height;
                    MoveDownButten(-this.tjlxlbl_1.Height);
                    n--;
                }
                else if (sz[n] == 0)
                {
                    countNULL--;
                }
            }
            if (countNULL <= 0)
            {
                MessageBox.Show("未选中条件类型！");
                return;
            }
            else 
            {
                sz[0] = 0;
                this.fanxuan_cb.Checked = false;
            }
        }

        /// <summary>
        /// 动态删除条件类型表格
        /// </summary>
        public void RemoveConditionTable(int rowNum)
        {
            int high = rowNum * this.tjlxlbl_1.Height;
            if (rowNum != 0)
            {
                for (int i = 1; i <= rowNum; i++)
                {
                    this.tjlx_panel.Controls.Remove(this.tjlx_panel.Controls["ch_" + i]);
                    this.tjlx_panel.Controls.Remove(this.tjlx_panel.Controls["cb_" + i]);
                    this.tjlx_panel.Controls.Remove(this.tjlx_panel.Controls["a_" + i]);
                    this.tjlx_panel.Controls.Remove(this.tjlx_panel.Controls["b_" + i]);
                    this.tjlx_panel.Controls.Remove(this.tjlx_panel.Controls["c_" + i]);
                    this.tjlx_panel.Controls.Remove(this.tjlx_panel.Controls["d_" + i]);
                    this.tjlx_panel.Controls.Remove(this.tjlx_panel.Controls["e_" + i]);
                    this.tjlx_panel.Controls.Remove(this.tjlx_panel.Controls["f_" + i]);
                }
                //动态改变panel的高度
                this.tjlx_panel.Height = this.tjlx_panel.Height - high;
                //conditingNum = 0;
                //标记反选框
                sz[0] = 1;
                this.fanxuan_cb.Checked = false;
                MoveDownButten(-high);
            }
        }

        /// <summary>
        /// 条件列表反选
        /// </summary>
        private void fanxuan_cb_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox cbox = null;
            if (sz[0] == 1)
            {
                sz[0] = 0;
                return;
            }
            if (fanxuan_cb.Checked)
            {
                for (int i = 1; i <= conditingNum; i++)
                {
                    cbox = this.tjlx_panel.Controls["cb_" + i] as CheckBox;
                    if (cbox.Checked)
                    {
                        cbox.Checked = false;
                        sz[i] = 0;
                    }
                    else
                    {
                        cbox.Checked = true;
                        sz[i] = 1;
                    }
                }
            }
            else 
            {
                for (int i = 1; i <= conditingNum; i++)
                {
                    cbox = this.tjlx_panel.Controls["cb_" + i] as CheckBox;
                    cbox.Checked = false;
                    sz[i] = 0;
                }
            }
        }
         
        /// <summary>
        /// 通过条件类型确定采购价格
        /// </summary>
        private void qdsure_bt_Click(object sender, EventArgs e)
        {

            double PurchasePrice = 0;
            double price = 0;
            double P = 0;
            float test = 0;
            for (int i = 1; i <= conditingNum; i++)
            {
                newStandardDAL.addNewPrice_Condition("Price_Condition", checkDt.Rows[0][0].ToString().Trim(), this.tjlx_panel.Controls["b_" + i].Text.Trim(),
                    this.tjlx_panel.Controls["c_" + i].Text.Trim(),
                    this.tjlx_panel.Controls["d_" + i].Text.Trim(),
                    true,
                    test);
                if (sz[i] == 1)
                {
                    if (this.tjlx_panel.Controls["b_" + i].Text.Trim().Equals("总价格")) 
                    {
                        //取价格
                        price = Convert.ToDouble(this.tjlx_panel.Controls["f_" + i].Text.ToString());
                        P = price;
                    }

                    if (this.tjlx_panel.Controls["d_" + i].Text.Trim().Equals("四舍五入"))
                    {
                        price = Math.Round(price, 2);
                    }
                    else if (this.tjlx_panel.Controls["d_" + i].Text.Trim().Equals("舍入到较高值"))
                    {
                        price = Math.Round(price + 0.05, 2);
                    }
                    else if (this.tjlx_panel.Controls["d_" + i].Text.Trim().Equals("舍入到较高值"))
                    {
                        price = Math.Round(price - 0.05, 2);
                    }

                    if (this.tjlx_panel.Controls["c_" + i].Text.Trim().Equals("折扣"))
                    {
                        price = price - P * Convert.ToDouble(this.tjlx_panel.Controls["f_" + i].Text.ToString()) / 100;
                    }
                    else if (this.tjlx_panel.Controls["c_" + i].Text.Trim().Equals("附加费")) 
                    {
                        price = price - Convert.ToDouble(this.tjlx_panel.Controls["f_" + i].Text.ToString());
                    }

                }
            }
            for (int i = 1; i <= conditingNum; i++)
            {
                if (this.tjlx_panel.Controls["c_" + i].Text.Trim().Equals("税费"))
                    price = price * 1.0/(1+Convert.ToDouble(this.tjlx_panel.Controls["f_" + i].Text.ToString()) / 100);
            }
            price = Math.Round(price, 2);
            newStandardDAL.changePrice(price.ToString(),checkDt.Rows[0][0].ToString());
            newStandardDAL.changeState("确定采购价格", checkDt.Rows[0][0].ToString());
            this.sjl_panel.Controls["i_" + j].ForeColor = System.Drawing.Color.Red;
            this.sjl_panel.Controls["i_" + j].Text = price.ToString();
            this.sjl_panel.Controls["d_" + j].Text = "确定采购价格";
            MessageBox.Show("采购价格已确定");
        }

        /// <summary>
        /// 选中项的比较库存
        /// </summary>
        private void compareStock(DataTable dt)
        {
        }

        /// <summary>
        /// 选中项的预算管控
        /// </summary>
        private void budgetaryControl(DataTable dt)
        {
        }

        /// <summary>
        /// 选中项的生成采购单
        /// </summary>
        private void purchaseOrders(DataTable dt)
        {
        }

        /// <summary>
        /// 根据所选行的物料新建需求计划
        /// </summary>
        private void new_pb_Click(object sender, EventArgs e)
        {
            NewStandard_Form newStandard_Form = new NewStandard_Form(userUI);
            if (checkDt == null || checkDt.Rows.Count == 0 || j == 0)
            {
                newStandard_Form.newStandardByMaterial_ID(checkDt, false);
            }
            else
            {
                newStandard_Form.newStandardByMaterial_ID(checkDt, true);
            }
            SingletonUserUI.addToUserUI(newStandard_Form);
        }

        /// <summary>
        /// 根据所选行的需求单号删除相关信息
        /// </summary>
        private void delete_pb_Click(object sender, EventArgs e)
        {
            if (checkDt == null || checkDt.Rows.Count == 0 || j == 0)
            {
                MessageBox.Show("未选中计划订单，请重新选择！");
                return;
            }
            else
            {
                if (MessageBox.Show("确定删除需求单号" + checkDt.Rows[0][0].ToString() + "？", "", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    int ok = newStandardDAL.changeState("0", checkDt.Rows[0][0].ToString());
                    if (ok > 0)
                    {
                        MessageBox.Show("删除成功！");
                        this.sjl_panel.Controls["d_" + j].ForeColor = System.Drawing.Color.Red;
                        this.sjl_panel.Controls["d_" + j].Text = "已删除";
                    }
                    else
                    {
                        MessageBox.Show("删除失败！");
                    }
                }
                else
                {
                    MessageBox.Show("已取消删除！");
                }
            }
        }

        /// <summary>
        /// 重新编辑所选的需求单号
        /// </summary>
        private void edit_pb_Click(object sender, EventArgs e)
        {
            if (checkDt == null || checkDt.Rows.Count == 0 || j == 0)
            {
                MessageBox.Show("未选中计划订单，请重新选择！");
                return;
            }
            else if (checkDt.Rows[0][14].ToString().Equals("待审核"))
            {
                AuditSubmitStandard_Form auditSubmitStandard_Form = new AuditSubmitStandard_Form(userUI);
                auditSubmitStandard_Form.auditSubmitStandardByDemand_ID(checkDt);
                SingletonUserUI.addToUserUI(auditSubmitStandard_Form);
            }
            else
            {
                if (MessageBox.Show("选中计划订单已通过审核，编辑后需重新审核。\r\r是否确定重新编辑？", "", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    MessageBox.Show("订单：" + checkDt.Rows[0][0].ToString() + "将重新编辑！");
                    AuditSubmitStandard_Form auditSubmitStandard_Form = new AuditSubmitStandard_Form(userUI);
                    auditSubmitStandard_Form.auditSubmitStandardByDemand_ID(checkDt);
                    SingletonUserUI.addToUserUI(auditSubmitStandard_Form);
                }
                else
                {
                    MessageBox.Show("已取消编辑！");
                }
            }
        }

        /// <summary>
        /// 刷新查询
        /// </summary>
        private void refresh_pb_Click(object sender, EventArgs e)
        {
            j = 0;
            String item1 = null;
            String item2 = null;
            String item3 = null;
            if (!string.IsNullOrWhiteSpace(this.cglx_cmb.Text))
            {
                item1 = " Purchase_Type = '" + this.cglx_cmb.Text + "'";
            }
            if (!string.IsNullOrWhiteSpace(this.cgzt_cmb.Text))
            {
                item3 = " state = '" + this.cgzt_cmb.Text + "'";
            }
            DynamicDrawTable(item1, item2, item3, false);
        }

        /// <summary>
        /// 打开需求预测
        /// </summary>
        private void forecast_pb_Click(object sender, EventArgs e)
        {
            if (checkDt == null || checkDt.Rows.Count == 0 || j == 0)
            {
                MessageBox.Show("未选中计划订单，请重新选择！");
                return;
            }
            else
            {
                DemandForecast_Form demandForecastF_Form = new DemandForecast_Form(userUI);
                SingletonUserUI.addToUserUI(demandForecastF_Form);
            }

        }

        /// <summary>
        /// 打开需求审核
        /// </summary>
        private void examine_pb_Click(object sender, EventArgs e)
        {
            if (checkDt == null || checkDt.Rows.Count == 0 || j == 0)
            {
                MessageBox.Show("未选中计划订单，请重新选择！");
                return;
            }
            else if (checkDt.Rows[0][14].ToString().Equals("待审核"))
            {
                AuditSubmitStandard_Form auditSubmitStandard_Form = new AuditSubmitStandard_Form(userUI);
                auditSubmitStandard_Form.auditSubmitStandardByDemand_ID(checkDt);
                SingletonUserUI.addToUserUI(auditSubmitStandard_Form);
            }
            else
            {
                MessageBox.Show("选中计划订单已通过审核！");
            }
        }

        /// <summary>
        /// 打开生成采购单
        /// </summary>
        private void create_pb_Click(object sender, EventArgs e)
        {
            if (checkDt == null || checkDt.Rows.Count == 0 || j == 0)
            {
                MessageBox.Show("未选中计划订单，请重新选择！");
                return;
            }
            else
            {
                CreatePurchasing_Form createPurchasing_Form = new CreatePurchasing_Form(userUI);

                //传递参数至计划订单模块
                String item1 = checkDt.Rows[0][3].ToString().Trim();
                String item2 = checkDt.Rows[0][0].ToString().Trim();
                String item3 = checkDt.Rows[0][1].ToString().Trim();
                item1 = " Purchase_Type = '" + item1 + "'";
                item2 = " Demand_ID = '" + item2 + "'";
                item3 = " Material_ID = '" + item3 + "'";

                createPurchasing_Form.DynamicDrawTable(item1, item2, item3, true);
                SingletonUserUI.addToUserUI(createPurchasing_Form);
            }
        }

        private void close_pb_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        
    }
}
