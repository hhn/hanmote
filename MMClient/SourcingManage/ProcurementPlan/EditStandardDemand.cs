﻿using Lib.Bll.MDBll.General;
using Lib.Bll.SourcingManage.ProcurementPlan;
using Lib.Common.MMCException.IDAL;
using Lib.Model.SourcingManage.ProcurementPlan;
using Lib.SqlServerDAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;

namespace MMClient.SourcingManage.ProcurementPlan
{
    public partial class EditStandardDemand :Form
    {
        //通用信息逻辑层(获取所有工厂编号)
        private GeneralBLL generalBll;
        //需求计划逻辑层
        private Summary_DemandBLL summaryDemandBll;
        //需求物料逻辑层
        private Demand_MaterialBLL demandMaterialBll;
        //需求计划
        private Summary_Demand summaryDemand;

        private List<string> midsList = new List<string>();

        public EditStandardDemand(Summary_Demand summaryDemand)
        {
            InitializeComponent();
            generalBll = new GeneralBLL();
            summaryDemandBll = new Summary_DemandBLL();
            demandMaterialBll = new Demand_MaterialBLL();
            this.summaryDemand = summaryDemand;
            materialGridView.TopLeftHeaderCell.Value = "序号";
            
            initDemandData(summaryDemand);
            initMaterialData(summaryDemand.Demand_ID);
        }

        /// <summary>
        /// 初始化需求计划数据
        /// </summary>
        /// <param name="summaryDemand"></param>
        private void initDemandData(Summary_Demand summaryDemand)
        {
            purchaseTypeCB.Text = summaryDemand.Purchase_Type;
            logisticsCB.Text = summaryDemand.LogisticsMode;
            demandIdText.Text = summaryDemand.Demand_ID;
            txtdepartment.Text = summaryDemand.Department;
            txt_Proposer.Text = summaryDemand.Proposer_ID;
            phoneText.Text = summaryDemand.PhoneNum;
            txt_ApplyTime.Text = summaryDemand.Create_Time.ToString("yyyy-MM-dd hh:mm:ss");
        }

        /// <summary>
        /// 初始化物料信息
        /// </summary>
        private void initMaterialData(string demandId)
        {
            //根据需求编号查找对应的物料信息
            string sqlMaterial = @"SELECT
	                            Material_ID AS materialId,
	                            Material_Name AS materialName,
	                            Material_Group AS materialGroup,
	                            Demand_Count AS applyNum,
	                            Measurement AS measurement,
                                Factory_ID AS Factory_ID,
	                            Stock_ID AS stockId,
	                            DeliveryStartTime AS DeliveryStartTime,
	                            DeliveryEndTime AS DeliveryEndTime
                            FROM
	                            Demand_Material
                            WHERE
	                            Demand_ID = '" + demandId+"'";
            try
            {
                this.materialGridView.DataSource = DBHelper.ExecuteQueryDT(sqlMaterial);
            }
            catch(DBException ex)
            {
                MessageBox.Show("数据加载失败");
            }

        }

        
        /// <summary>
        /// 全部删除按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_allDelete_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("是否一并删除需求计划", "提示", MessageBoxButtons.YesNoCancel);
            if ( result== DialogResult.Yes)
            {
                for (int i = 0; i < materialGridView.Rows.Count; i++)
                {
                    string mid = materialGridView.Rows[i].Cells["materialId"].Value.ToString();
                    demandMaterialBll.deleteDemandMaterial(summaryDemand.Demand_ID, mid);
                }
                materialGridView.DataSource = null;
                summaryDemandBll.deleteSummaryDemandById(summaryDemand.Demand_ID);
               
            }
            else if(result == DialogResult.No)
            {
                for (int i = 0; i < materialGridView.Rows.Count; i++)
                {
                    string mid = materialGridView.Rows[i].Cells["materialId"].Value.ToString();
                    demandMaterialBll.deleteDemandMaterial(summaryDemand.Demand_ID, mid);
                }
                materialGridView.DataSource = null;
            }   
            
        }

        /// <summary>
        /// 批量添加物料
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void addBtn_Click(object sender, EventArgs e)
        {
            try
            {
                List<string> limitIds = new List<string>();
                foreach (DataGridViewRow dr in materialGridView.Rows)
                {
                    string mid = dr.Cells["materialId"].Value.ToString();
                    limitIds.Add(mid);
                }
                EditMaterialInfo editMaterialInfo = null;
                if (editMaterialInfo == null || editMaterialInfo.IsDisposed)
                {
                    editMaterialInfo = new EditMaterialInfo(this, limitIds);
                }
                editMaterialInfo.Show();
            }
            catch(Exception ex)
            {
                MessageBox.Show("添加异常，请退出后重新添加");
            }
        }

        /// <summary>
        /// 增添新的物料
        /// </summary>
        /// <param name="materialList"></param>
        public void fillMaterialBases(DataTable dt)
        {
            if (dt != null && dt.Rows.Count>0)
            {
               if(materialGridView.Rows.Count>0)
                {
                    DataTable table = (DataTable)materialGridView.DataSource;
                    table.Rows.Clear();
                }
                materialGridView.DataSource =  dt ;
            }
        }
        
        /// <summary>
        /// 检查按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            if (materialGridView.Rows.Count == 0)
            {
                MessageBox.Show("需求计划包含的物料数量为0,请至少添加一个物料","提示");
                return;
            }
            int i = 0;
            foreach (DataGridViewRow dgvr in materialGridView.Rows)
            {
                i++;
                DataTable dt = new DataTable();
                string MMID = "";
                string MMName = "";
                string MMGroup = "";
                string measuerment = "";
                string stockID = "";
                string factotyID = "";

                if (dgvr.Cells["materialId"].Value == null || dgvr.Cells["materialId"].Value.ToString().Trim().Equals(""))
                {
                    MessageBox.Show("物料编码不能为空", "提示");
                    return;
                }
                else
                {
                    MMID = dgvr.Cells["materialId"].Value.ToString().Trim();
                    string sqlcheck = "select Material_Group as 物料组 ,Material_Name as 物料名称,Measurement as 计量单位 from Material where Material_ID='" + MMID + "'";
                    try
                    {
                        dt = DBHelper.ExecuteQueryDT(sqlcheck);
                        if (dt.Rows.Count == 0)
                        {
                            MessageBox.Show("第 " + i + " 行物料编码缺失，请检查");
                            return;
                        }
                    }
                    catch (DBException ex)
                    {
                        MessageBox.Show("数据库检查异常");
                        return;
                    }
                }
                if (dgvr.Cells["materialName"].Value == null || dgvr.Cells["materialName"].Value.ToString().Trim().Equals(""))
                {
                    MessageBox.Show("物料名称不能为空", "提示");
                    return;
                }
                else
                {
                    MMName = dgvr.Cells["materialName"].Value.ToString().Trim();
                    if (!MMName.Equals(dt.Rows[0][1].ToString()))
                    {
                        MessageBox.Show("第 " + i + " 行物料名称错误，请检查\n 建议值：" + dt.Rows[0][1].ToString());
                        return;
                    }
                }
                if (dgvr.Cells["materialGroup"].Value == null || dgvr.Cells["materialGroup"].Value.ToString().Trim().Equals(""))
                {
                    MessageBox.Show("物料组不能为空", "提示");
                    return;
                }
                else
                {
                    MMGroup = dgvr.Cells["materialGroup"].Value.ToString().Trim();
                    if (!MMGroup.Equals(dt.Rows[0][0].ToString()))
                    {
                        MessageBox.Show("第" + i + "行物料组，请检查\n 建议值：" + dt.Rows[0][0].ToString());
                        return;
                    }
                }
                if (dgvr.Cells["applyNum"].Value == null || dgvr.Cells["applyNum"].Value.ToString().Trim().Equals(""))
                {
                    MessageBox.Show("申请数量不能为空", "提示");
                    return;
                }



                if (dgvr.Cells["measurement"].Value == null || dgvr.Cells["measurement"].Value.ToString().Trim().Equals(""))
                {
                    MessageBox.Show("单位名称不能为空", "提示");
                    return;
                }
                else
                {
                    measuerment = dgvr.Cells["measurement"].Value.ToString().Trim();
                    if (!measuerment.Equals(dt.Rows[0][2].ToString()))
                    {
                        MessageBox.Show("第 " + i + " 行计量单位错误，请检查\n 建议值：" + dt.Rows[0][2].ToString());
                        return;
                    }
                }
                if (dgvr.Cells["stockId"].Value == null || dgvr.Cells["stockId"].Value.ToString().Trim().Equals(""))
                {
                    MessageBox.Show("库存编号不能为空", "提示");
                    return;
                }
                else
                {
                    dt = null;
                    stockID = dgvr.Cells["stockId"].Value.ToString().Trim();
                    try
                    {
                        dt = DBHelper.ExecuteQueryDT("select Factory_ID from Stock  where Stock_ID = '" + stockID + "' ");
                        if (dt.Rows.Count == 0)
                        {
                            MessageBox.Show("第" + i + "行，库存与工厂关系对应有误");
                            return;
                        }
                    }
                    catch (DBException ex)
                    {
                        MessageBox.Show("数据库检查异常");
                        return;
                    }
                }
                if (dgvr.Cells["Factory_ID"].Value == null || dgvr.Cells["Factory_ID"].Value.ToString().Trim().Equals(""))
                {
                    MessageBox.Show("工厂编号不能为空", "提示");
                    return;
                }
                else
                {
                    factotyID = dgvr.Cells["Factory_ID"].Value.ToString().Trim();
                    if (!factotyID.Equals(dt.Rows[0][0]))
                    {
                        MessageBox.Show("第" + i + "行，库存与工厂关系对应有误");
                        return;
                    }
                }
                if (dgvr.Cells["DeliveryStartTime"].Value == null || dgvr.Cells["DeliveryStartTime"].Value.ToString().Trim().Equals(""))
                {
                    MessageBox.Show("交货开始时间不能为空", "提示");
                    return;
                }
                if (dgvr.Cells["DeliveryEndTime"].Value == null || dgvr.Cells["DeliveryEndTime"].Value.ToString().Trim().Equals(""))
                {
                    MessageBox.Show("交货结束时间不能为空", "提示");
                    return;
                }

            }
            if (phoneText.Text.ToString().Trim().Equals(""))
            {
                MessageBox.Show("联系电话不能为空", "提示");
                return;
            }
            MessageBox.Show("申请信息无误");
            submitBtn.Visible = true;
        }

        /// <summary>
        /// 保存按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void submitBtn_Click(object sender, EventArgs e)
        {
            //更改需求计划
            Summary_Demand demand = new Summary_Demand();
            demand.Demand_ID = demandIdText.Text;
            demand.Purchase_Type = purchaseTypeCB.Text;
            demand.LogisticsMode = logisticsCB.Text;
            demand.Department = txtdepartment.Text;
            demand.Proposer_ID = txt_Proposer.Text;
            demand.PhoneNum = phoneText.Text;
            demand.Create_Time = DateTime.Parse(txt_ApplyTime.Text);
            demand.State = "待审核";
            int result = summaryDemandBll.UpdateSummaryDemand(demand);
            //更改需求计划对应的物料
            foreach (DataGridViewRow dgvr in materialGridView.Rows)
            {
                Demand_Material demand_material = new Demand_Material();
                demand_material.Demand_ID = demandIdText.Text;
                demand_material.Material_ID = dgvr.Cells["materialId"].Value.ToString();
                demand_material.Material_Name = dgvr.Cells["materialName"].Value.ToString();
                demand_material.Material_Group = dgvr.Cells["materialGroup"].Value.ToString();
                demand_material.Demand_Count = Convert.ToInt32(dgvr.Cells["applyNum"].Value.ToString());
                demand_material.Measurement = dgvr.Cells["measurement"].Value.ToString();
                demand_material.Stock_ID = dgvr.Cells["stockId"].Value.ToString();
                demand_material.Factory_ID = dgvr.Cells["Factory_ID"].Value.ToString(); 
                demand_material.DeliveryStartTime = DateTime.Parse(dgvr.Cells["DeliveryStartTime"].Value.ToString());
                demand_material.DeliveryEndTime = DateTime.Parse(dgvr.Cells["DeliveryEndTime"].Value.ToString());
                //将需求计划添加到数据库
                if (midsList.Contains(demand_material.Material_ID))
                {
                    int ures = demandMaterialBll.updateSummaryMaterials(demand_material, demand_material.Material_ID);
                }
                else
                {
                    demandMaterialBll.addSummaryMaterials(demand_material);
                }
            }
            MessageBox.Show("需求计划修改成功","提示");
            this.Close();
        }

        private void materialGridView_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {
            if (this.materialGridView.Columns[e.ColumnIndex].Name == "删除")
            {
                int index = materialGridView.CurrentRow.Index;   
                string materialId = materialGridView.Rows[index].Cells["materialId"].Value.ToString();
                midsList.Remove(materialId);
                if (MessageBox.Show("是否确定删除？", "提示", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    int result = demandMaterialBll.deleteDemandMaterial(summaryDemand.Demand_ID, materialId);
                    materialGridView.Rows.RemoveAt(index);
                    if (result > 0)
                    {                        
                        MessageBox.Show("删除成功");
                    }
                }  
            }
        }
    }
}
