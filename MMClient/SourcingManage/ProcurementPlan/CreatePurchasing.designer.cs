﻿namespace MMClient.SourcingManage.ProcurementPlan
{
    partial class CreatePurchasing_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CreatePurchasing_Form));
            this.sjl_panel = new System.Windows.Forms.Panel();
            this.lbl_6 = new System.Windows.Forms.Label();
            this.lbl_5 = new System.Windows.Forms.Label();
            this.lbl_12 = new System.Windows.Forms.Label();
            this.lbl_11 = new System.Windows.Forms.Label();
            this.lbl_9 = new System.Windows.Forms.Label();
            this.lbl_10 = new System.Windows.Forms.Label();
            this.lbl_8 = new System.Windows.Forms.Label();
            this.lbl_7 = new System.Windows.Forms.Label();
            this.lbl_1 = new System.Windows.Forms.Label();
            this.lbl_2 = new System.Windows.Forms.Label();
            this.lbl_3 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.qdcgjg_tp = new System.Windows.Forms.TabPage();
            this.label51 = new System.Windows.Forms.Label();
            this.tjlx_panel = new System.Windows.Forms.Panel();
            this.fanxuan_cb = new System.Windows.Forms.CheckBox();
            this.tjlxlbl_7 = new System.Windows.Forms.Label();
            this.tjlxlbl_5 = new System.Windows.Forms.Label();
            this.tjlxlbl_3 = new System.Windows.Forms.Label();
            this.tjlxlbl_4 = new System.Windows.Forms.Label();
            this.tjlxlbl_6 = new System.Windows.Forms.Label();
            this.tjlxlbl_1 = new System.Windows.Forms.Label();
            this.tjlxlbl_2 = new System.Windows.Forms.Label();
            this.qddel_bt = new System.Windows.Forms.Button();
            this.qdadd_bt = new System.Windows.Forms.Button();
            this.qdwlbh_cmb = new System.Windows.Forms.ComboBox();
            this.qdwlmc_cmb = new System.Windows.Forms.ComboBox();
            this.qdcglx_cmb = new System.Windows.Forms.ComboBox();
            this.qdsure_bt = new System.Windows.Forms.Button();
            this.qdcancel_bt = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.bjkc_tp = new System.Windows.Forms.TabPage();
            this.panel7 = new System.Windows.Forms.Panel();
            this.label54 = new System.Windows.Forms.Label();
            this.textBox39 = new System.Windows.Forms.TextBox();
            this.textBox40 = new System.Windows.Forms.TextBox();
            this.textBox41 = new System.Windows.Forms.TextBox();
            this.label53 = new System.Windows.Forms.Label();
            this.textBox32 = new System.Windows.Forms.TextBox();
            this.textBox34 = new System.Windows.Forms.TextBox();
            this.textBox38 = new System.Windows.Forms.TextBox();
            this.label72 = new System.Windows.Forms.Label();
            this.textBox95 = new System.Windows.Forms.TextBox();
            this.textBox96 = new System.Windows.Forms.TextBox();
            this.textBox97 = new System.Windows.Forms.TextBox();
            this.label60 = new System.Windows.Forms.Label();
            this.textBox59 = new System.Windows.Forms.TextBox();
            this.textBox60 = new System.Windows.Forms.TextBox();
            this.textBox61 = new System.Windows.Forms.TextBox();
            this.label65 = new System.Windows.Forms.Label();
            this.textBox74 = new System.Windows.Forms.TextBox();
            this.textBox75 = new System.Windows.Forms.TextBox();
            this.textBox76 = new System.Windows.Forms.TextBox();
            this.label66 = new System.Windows.Forms.Label();
            this.textBox77 = new System.Windows.Forms.TextBox();
            this.textBox78 = new System.Windows.Forms.TextBox();
            this.textBox79 = new System.Windows.Forms.TextBox();
            this.label67 = new System.Windows.Forms.Label();
            this.textBox80 = new System.Windows.Forms.TextBox();
            this.textBox81 = new System.Windows.Forms.TextBox();
            this.textBox82 = new System.Windows.Forms.TextBox();
            this.label68 = new System.Windows.Forms.Label();
            this.textBox83 = new System.Windows.Forms.TextBox();
            this.textBox84 = new System.Windows.Forms.TextBox();
            this.textBox85 = new System.Windows.Forms.TextBox();
            this.checkBox5 = new System.Windows.Forms.CheckBox();
            this.checkBox8 = new System.Windows.Forms.CheckBox();
            this.checkBox10 = new System.Windows.Forms.CheckBox();
            this.label69 = new System.Windows.Forms.Label();
            this.textBox86 = new System.Windows.Forms.TextBox();
            this.textBox87 = new System.Windows.Forms.TextBox();
            this.textBox88 = new System.Windows.Forms.TextBox();
            this.textBox89 = new System.Windows.Forms.TextBox();
            this.label70 = new System.Windows.Forms.Label();
            this.label71 = new System.Windows.Forms.Label();
            this.textBox90 = new System.Windows.Forms.TextBox();
            this.textBox91 = new System.Windows.Forms.TextBox();
            this.textBox92 = new System.Windows.Forms.TextBox();
            this.textBox93 = new System.Windows.Forms.TextBox();
            this.textBox94 = new System.Windows.Forms.TextBox();
            this.button15 = new System.Windows.Forms.Button();
            this.button16 = new System.Windows.Forms.Button();
            this.button17 = new System.Windows.Forms.Button();
            this.bjdel_bt = new System.Windows.Forms.Button();
            this.bjquery_bt = new System.Windows.Forms.Button();
            this.bjcgsl_tb = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.bjckdm_tb = new System.Windows.Forms.ComboBox();
            this.bjwlbm_tb = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.ysgk_tp = new System.Windows.Forms.TabPage();
            this.panel9 = new System.Windows.Forms.Panel();
            this.label82 = new System.Windows.Forms.Label();
            this.textBox116 = new System.Windows.Forms.TextBox();
            this.textBox117 = new System.Windows.Forms.TextBox();
            this.textBox118 = new System.Windows.Forms.TextBox();
            this.label78 = new System.Windows.Forms.Label();
            this.textBox104 = new System.Windows.Forms.TextBox();
            this.textBox105 = new System.Windows.Forms.TextBox();
            this.textBox106 = new System.Windows.Forms.TextBox();
            this.label55 = new System.Windows.Forms.Label();
            this.textBox21 = new System.Windows.Forms.TextBox();
            this.textBox23 = new System.Windows.Forms.TextBox();
            this.textBox24 = new System.Windows.Forms.TextBox();
            this.label56 = new System.Windows.Forms.Label();
            this.textBox25 = new System.Windows.Forms.TextBox();
            this.textBox26 = new System.Windows.Forms.TextBox();
            this.textBox27 = new System.Windows.Forms.TextBox();
            this.label73 = new System.Windows.Forms.Label();
            this.textBox28 = new System.Windows.Forms.TextBox();
            this.textBox29 = new System.Windows.Forms.TextBox();
            this.textBox30 = new System.Windows.Forms.TextBox();
            this.label74 = new System.Windows.Forms.Label();
            this.textBox31 = new System.Windows.Forms.TextBox();
            this.textBox45 = new System.Windows.Forms.TextBox();
            this.textBox46 = new System.Windows.Forms.TextBox();
            this.label75 = new System.Windows.Forms.Label();
            this.textBox47 = new System.Windows.Forms.TextBox();
            this.textBox48 = new System.Windows.Forms.TextBox();
            this.textBox49 = new System.Windows.Forms.TextBox();
            this.label76 = new System.Windows.Forms.Label();
            this.textBox98 = new System.Windows.Forms.TextBox();
            this.textBox99 = new System.Windows.Forms.TextBox();
            this.textBox100 = new System.Windows.Forms.TextBox();
            this.label77 = new System.Windows.Forms.Label();
            this.textBox101 = new System.Windows.Forms.TextBox();
            this.textBox102 = new System.Windows.Forms.TextBox();
            this.textBox103 = new System.Windows.Forms.TextBox();
            this.checkBox11 = new System.Windows.Forms.CheckBox();
            this.checkBox12 = new System.Windows.Forms.CheckBox();
            this.checkBox13 = new System.Windows.Forms.CheckBox();
            this.label79 = new System.Windows.Forms.Label();
            this.textBox107 = new System.Windows.Forms.TextBox();
            this.textBox108 = new System.Windows.Forms.TextBox();
            this.textBox109 = new System.Windows.Forms.TextBox();
            this.textBox110 = new System.Windows.Forms.TextBox();
            this.label80 = new System.Windows.Forms.Label();
            this.label81 = new System.Windows.Forms.Label();
            this.textBox111 = new System.Windows.Forms.TextBox();
            this.textBox112 = new System.Windows.Forms.TextBox();
            this.textBox113 = new System.Windows.Forms.TextBox();
            this.textBox114 = new System.Windows.Forms.TextBox();
            this.textBox115 = new System.Windows.Forms.TextBox();
            this.button18 = new System.Windows.Forms.Button();
            this.button19 = new System.Windows.Forms.Button();
            this.button20 = new System.Windows.Forms.Button();
            this.ysdel_bt = new System.Windows.Forms.Button();
            this.ysquery_bt = new System.Windows.Forms.Button();
            this.ysysph_tb = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.yssqbm_tb = new System.Windows.Forms.ComboBox();
            this.yswlbm_tb = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.sccgd_tp = new System.Windows.Forms.TabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.xybh_tb = new System.Windows.Forms.TextBox();
            this.gysbh_tb = new System.Windows.Forms.TextBox();
            this.label49 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.cgksrq_dt = new System.Windows.Forms.DateTimePicker();
            this.cgjsrq_dt = new System.Windows.Forms.DateTimePicker();
            this.hxcgpz_cmb = new System.Windows.Forms.ComboBox();
            this.label40 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.fpyz_cmb = new System.Windows.Forms.ComboBox();
            this.label37 = new System.Windows.Forms.Label();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.comboBox16 = new System.Windows.Forms.ComboBox();
            this.comboBox17 = new System.Windows.Forms.ComboBox();
            this.label34 = new System.Windows.Forms.Label();
            this.textBox18 = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.cgy1_cmb = new System.Windows.Forms.ComboBox();
            this.cgzz1_cmb = new System.Windows.Forms.ComboBox();
            this.label31 = new System.Windows.Forms.Label();
            this.fpbl1_tb = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.scdel_bt = new System.Windows.Forms.Button();
            this.scadd_bt = new System.Windows.Forms.Button();
            this.label30 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.sccancel_bt = new System.Windows.Forms.Button();
            this.screturn_bt = new System.Windows.Forms.Button();
            this.scsubmit_bt = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.sclxdh_tb = new System.Windows.Forms.TextBox();
            this.scsqr_tb = new System.Windows.Forms.TextBox();
            this.scsqbm_tb = new System.Windows.Forms.TextBox();
            this.scxqsl_tb = new System.Windows.Forms.TextBox();
            this.sczxdhpl_tb = new System.Windows.Forms.TextBox();
            this.scjldw_tb = new System.Windows.Forms.TextBox();
            this.scckbh_tb = new System.Windows.Forms.TextBox();
            this.label83 = new System.Windows.Forms.Label();
            this.scxqdh_tb = new System.Windows.Forms.TextBox();
            this.scsqrq_dt = new System.Windows.Forms.DateTimePicker();
            this.scqtyq_rtb = new System.Windows.Forms.RichTextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.sccgdh_tb = new System.Windows.Forms.TextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.scwlfs_tb = new System.Windows.Forms.TextBox();
            this.sccglx_tb = new System.Windows.Forms.TextBox();
            this.scwlmc_tb = new System.Windows.Forms.TextBox();
            this.scwlbh_tb = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.cgddName = new System.Windows.Forms.Label();
            this.cgzt_cmb = new System.Windows.Forms.ComboBox();
            this.cglx_cmb = new System.Windows.Forms.ComboBox();
            this.query_bt = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.label29 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.close_pb = new System.Windows.Forms.PictureBox();
            this.create_pb = new System.Windows.Forms.PictureBox();
            this.examine_pb = new System.Windows.Forms.PictureBox();
            this.forecast_pb = new System.Windows.Forms.PictureBox();
            this.refresh_pb = new System.Windows.Forms.PictureBox();
            this.edit_pb = new System.Windows.Forms.PictureBox();
            this.delete_pb = new System.Windows.Forms.PictureBox();
            this.detail_pb = new System.Windows.Forms.PictureBox();
            this.new_pb = new System.Windows.Forms.PictureBox();
            this.sjl_panel.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.qdcgjg_tp.SuspendLayout();
            this.tjlx_panel.SuspendLayout();
            this.bjkc_tp.SuspendLayout();
            this.panel7.SuspendLayout();
            this.ysgk_tp.SuspendLayout();
            this.panel9.SuspendLayout();
            this.sccgd_tp.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.close_pb)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.create_pb)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.examine_pb)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.forecast_pb)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.refresh_pb)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.edit_pb)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.delete_pb)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.detail_pb)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.new_pb)).BeginInit();
            this.SuspendLayout();
            // 
            // sjl_panel
            // 
            this.sjl_panel.Controls.Add(this.lbl_6);
            this.sjl_panel.Controls.Add(this.lbl_5);
            this.sjl_panel.Controls.Add(this.lbl_12);
            this.sjl_panel.Controls.Add(this.lbl_11);
            this.sjl_panel.Controls.Add(this.lbl_9);
            this.sjl_panel.Controls.Add(this.lbl_10);
            this.sjl_panel.Controls.Add(this.lbl_8);
            this.sjl_panel.Controls.Add(this.lbl_7);
            this.sjl_panel.Controls.Add(this.lbl_1);
            this.sjl_panel.Controls.Add(this.lbl_2);
            this.sjl_panel.Controls.Add(this.lbl_3);
            this.sjl_panel.Location = new System.Drawing.Point(10, 111);
            this.sjl_panel.Name = "sjl_panel";
            this.sjl_panel.Size = new System.Drawing.Size(883, 54);
            this.sjl_panel.TabIndex = 221;
            // 
            // lbl_6
            // 
            this.lbl_6.BackColor = System.Drawing.SystemColors.Window;
            this.lbl_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl_6.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_6.Location = new System.Drawing.Point(299, 15);
            this.lbl_6.Name = "lbl_6";
            this.lbl_6.Size = new System.Drawing.Size(80, 25);
            this.lbl_6.TabIndex = 164;
            this.lbl_6.Text = "物料编号";
            this.lbl_6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_5
            // 
            this.lbl_5.BackColor = System.Drawing.SystemColors.Window;
            this.lbl_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl_5.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_5.Location = new System.Drawing.Point(200, 15);
            this.lbl_5.Name = "lbl_5";
            this.lbl_5.Size = new System.Drawing.Size(100, 25);
            this.lbl_5.TabIndex = 163;
            this.lbl_5.Text = "采购计划状态";
            this.lbl_5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_12
            // 
            this.lbl_12.BackColor = System.Drawing.SystemColors.Window;
            this.lbl_12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl_12.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_12.Location = new System.Drawing.Point(753, 15);
            this.lbl_12.Name = "lbl_12";
            this.lbl_12.Size = new System.Drawing.Size(100, 25);
            this.lbl_12.TabIndex = 155;
            this.lbl_12.Text = "后续采购凭证";
            this.lbl_12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_11
            // 
            this.lbl_11.BackColor = System.Drawing.SystemColors.Window;
            this.lbl_11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl_11.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_11.Location = new System.Drawing.Point(689, 15);
            this.lbl_11.Name = "lbl_11";
            this.lbl_11.Size = new System.Drawing.Size(65, 25);
            this.lbl_11.TabIndex = 136;
            this.lbl_11.Text = "申请部门";
            this.lbl_11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_9
            // 
            this.lbl_9.BackColor = System.Drawing.SystemColors.Window;
            this.lbl_9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl_9.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_9.Location = new System.Drawing.Point(561, 15);
            this.lbl_9.Name = "lbl_9";
            this.lbl_9.Size = new System.Drawing.Size(65, 25);
            this.lbl_9.TabIndex = 131;
            this.lbl_9.Text = "采购数量";
            this.lbl_9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_10
            // 
            this.lbl_10.BackColor = System.Drawing.SystemColors.Window;
            this.lbl_10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl_10.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_10.Location = new System.Drawing.Point(625, 15);
            this.lbl_10.Name = "lbl_10";
            this.lbl_10.Size = new System.Drawing.Size(65, 25);
            this.lbl_10.TabIndex = 126;
            this.lbl_10.Text = "采购价格";
            this.lbl_10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_8
            // 
            this.lbl_8.BackColor = System.Drawing.SystemColors.Window;
            this.lbl_8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl_8.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_8.Location = new System.Drawing.Point(497, 15);
            this.lbl_8.Name = "lbl_8";
            this.lbl_8.Size = new System.Drawing.Size(65, 25);
            this.lbl_8.TabIndex = 114;
            this.lbl_8.Text = "计量单位";
            this.lbl_8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_7
            // 
            this.lbl_7.BackColor = System.Drawing.SystemColors.Window;
            this.lbl_7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl_7.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_7.Location = new System.Drawing.Point(378, 15);
            this.lbl_7.Name = "lbl_7";
            this.lbl_7.Size = new System.Drawing.Size(120, 25);
            this.lbl_7.TabIndex = 107;
            this.lbl_7.Text = "采购类型";
            this.lbl_7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_1
            // 
            this.lbl_1.BackColor = System.Drawing.SystemColors.Window;
            this.lbl_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl_1.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_1.Location = new System.Drawing.Point(3, 15);
            this.lbl_1.Name = "lbl_1";
            this.lbl_1.Size = new System.Drawing.Size(30, 25);
            this.lbl_1.TabIndex = 86;
            this.lbl_1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_2
            // 
            this.lbl_2.BackColor = System.Drawing.SystemColors.Window;
            this.lbl_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl_2.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_2.Location = new System.Drawing.Point(32, 15);
            this.lbl_2.Name = "lbl_2";
            this.lbl_2.Size = new System.Drawing.Size(40, 25);
            this.lbl_2.TabIndex = 86;
            this.lbl_2.Text = "序号";
            this.lbl_2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_3
            // 
            this.lbl_3.BackColor = System.Drawing.SystemColors.Window;
            this.lbl_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl_3.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_3.Location = new System.Drawing.Point(71, 15);
            this.lbl_3.Name = "lbl_3";
            this.lbl_3.Size = new System.Drawing.Size(130, 25);
            this.lbl_3.TabIndex = 86;
            this.lbl_3.Text = "需求单号";
            this.lbl_3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tabControl1
            // 
            this.tabControl1.AccessibleName = "确定采购价格";
            this.tabControl1.Controls.Add(this.qdcgjg_tp);
            this.tabControl1.Controls.Add(this.bjkc_tp);
            this.tabControl1.Controls.Add(this.ysgk_tp);
            this.tabControl1.Controls.Add(this.sccgd_tp);
            this.tabControl1.Location = new System.Drawing.Point(10, 181);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(910, 805);
            this.tabControl1.TabIndex = 86;
            // 
            // qdcgjg_tp
            // 
            this.qdcgjg_tp.BackColor = System.Drawing.SystemColors.Control;
            this.qdcgjg_tp.Controls.Add(this.label51);
            this.qdcgjg_tp.Controls.Add(this.tjlx_panel);
            this.qdcgjg_tp.Controls.Add(this.qddel_bt);
            this.qdcgjg_tp.Controls.Add(this.qdadd_bt);
            this.qdcgjg_tp.Controls.Add(this.qdwlbh_cmb);
            this.qdcgjg_tp.Controls.Add(this.qdwlmc_cmb);
            this.qdcgjg_tp.Controls.Add(this.qdcglx_cmb);
            this.qdcgjg_tp.Controls.Add(this.qdsure_bt);
            this.qdcgjg_tp.Controls.Add(this.qdcancel_bt);
            this.qdcgjg_tp.Controls.Add(this.label6);
            this.qdcgjg_tp.Controls.Add(this.label5);
            this.qdcgjg_tp.Controls.Add(this.label4);
            this.qdcgjg_tp.Controls.Add(this.label3);
            this.qdcgjg_tp.Controls.Add(this.label2);
            this.qdcgjg_tp.Location = new System.Drawing.Point(4, 22);
            this.qdcgjg_tp.Name = "qdcgjg_tp";
            this.qdcgjg_tp.Padding = new System.Windows.Forms.Padding(3);
            this.qdcgjg_tp.Size = new System.Drawing.Size(902, 779);
            this.qdcgjg_tp.TabIndex = 0;
            this.qdcgjg_tp.Text = "确定采购价格";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.ForeColor = System.Drawing.Color.Crimson;
            this.label51.Location = new System.Drawing.Point(75, 123);
            this.label51.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(125, 12);
            this.label51.TabIndex = 224;
            this.label51.Text = "请先选择一个采购计划";
            // 
            // tjlx_panel
            // 
            this.tjlx_panel.Controls.Add(this.fanxuan_cb);
            this.tjlx_panel.Controls.Add(this.tjlxlbl_7);
            this.tjlx_panel.Controls.Add(this.tjlxlbl_5);
            this.tjlx_panel.Controls.Add(this.tjlxlbl_3);
            this.tjlx_panel.Controls.Add(this.tjlxlbl_4);
            this.tjlx_panel.Controls.Add(this.tjlxlbl_6);
            this.tjlx_panel.Controls.Add(this.tjlxlbl_1);
            this.tjlx_panel.Controls.Add(this.tjlxlbl_2);
            this.tjlx_panel.Location = new System.Drawing.Point(7, 200);
            this.tjlx_panel.Name = "tjlx_panel";
            this.tjlx_panel.Size = new System.Drawing.Size(700, 55);
            this.tjlx_panel.TabIndex = 223;
            // 
            // fanxuan_cb
            // 
            this.fanxuan_cb.AutoSize = true;
            this.fanxuan_cb.Location = new System.Drawing.Point(11, 20);
            this.fanxuan_cb.Name = "fanxuan_cb";
            this.fanxuan_cb.Size = new System.Drawing.Size(15, 14);
            this.fanxuan_cb.TabIndex = 224;
            this.fanxuan_cb.UseVisualStyleBackColor = true;
            this.fanxuan_cb.CheckedChanged += new System.EventHandler(this.fanxuan_cb_CheckedChanged);
            // 
            // tjlxlbl_7
            // 
            this.tjlxlbl_7.BackColor = System.Drawing.SystemColors.Window;
            this.tjlxlbl_7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tjlxlbl_7.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tjlxlbl_7.Location = new System.Drawing.Point(592, 15);
            this.tjlxlbl_7.Name = "tjlxlbl_7";
            this.tjlxlbl_7.Size = new System.Drawing.Size(80, 22);
            this.tjlxlbl_7.TabIndex = 167;
            this.tjlxlbl_7.Text = "现值比率%";
            this.tjlxlbl_7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tjlxlbl_5
            // 
            this.tjlxlbl_5.BackColor = System.Drawing.SystemColors.Window;
            this.tjlxlbl_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tjlxlbl_5.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tjlxlbl_5.Location = new System.Drawing.Point(389, 15);
            this.tjlxlbl_5.Name = "tjlxlbl_5";
            this.tjlxlbl_5.Size = new System.Drawing.Size(140, 22);
            this.tjlxlbl_5.TabIndex = 163;
            this.tjlxlbl_5.Text = "舍入规则";
            this.tjlxlbl_5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tjlxlbl_3
            // 
            this.tjlxlbl_3.BackColor = System.Drawing.SystemColors.Window;
            this.tjlxlbl_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tjlxlbl_3.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tjlxlbl_3.Location = new System.Drawing.Point(71, 15);
            this.tjlxlbl_3.Name = "tjlxlbl_3";
            this.tjlxlbl_3.Size = new System.Drawing.Size(180, 22);
            this.tjlxlbl_3.TabIndex = 159;
            this.tjlxlbl_3.Text = "条件类型";
            this.tjlxlbl_3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tjlxlbl_4
            // 
            this.tjlxlbl_4.BackColor = System.Drawing.SystemColors.Window;
            this.tjlxlbl_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tjlxlbl_4.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tjlxlbl_4.Location = new System.Drawing.Point(250, 15);
            this.tjlxlbl_4.Name = "tjlxlbl_4";
            this.tjlxlbl_4.Size = new System.Drawing.Size(140, 22);
            this.tjlxlbl_4.TabIndex = 146;
            this.tjlxlbl_4.Text = "定价等级";
            this.tjlxlbl_4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tjlxlbl_6
            // 
            this.tjlxlbl_6.BackColor = System.Drawing.SystemColors.Window;
            this.tjlxlbl_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tjlxlbl_6.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tjlxlbl_6.Location = new System.Drawing.Point(528, 15);
            this.tjlxlbl_6.Name = "tjlxlbl_6";
            this.tjlxlbl_6.Size = new System.Drawing.Size(65, 22);
            this.tjlxlbl_6.TabIndex = 114;
            this.tjlxlbl_6.Text = "正/负";
            this.tjlxlbl_6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tjlxlbl_1
            // 
            this.tjlxlbl_1.BackColor = System.Drawing.SystemColors.Window;
            this.tjlxlbl_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tjlxlbl_1.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tjlxlbl_1.Location = new System.Drawing.Point(3, 15);
            this.tjlxlbl_1.Name = "tjlxlbl_1";
            this.tjlxlbl_1.Size = new System.Drawing.Size(30, 22);
            this.tjlxlbl_1.TabIndex = 86;
            this.tjlxlbl_1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tjlxlbl_2
            // 
            this.tjlxlbl_2.BackColor = System.Drawing.SystemColors.Window;
            this.tjlxlbl_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tjlxlbl_2.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tjlxlbl_2.Location = new System.Drawing.Point(32, 15);
            this.tjlxlbl_2.Name = "tjlxlbl_2";
            this.tjlxlbl_2.Size = new System.Drawing.Size(40, 22);
            this.tjlxlbl_2.TabIndex = 86;
            this.tjlxlbl_2.Text = "序号";
            this.tjlxlbl_2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // qddel_bt
            // 
            this.qddel_bt.Location = new System.Drawing.Point(87, 150);
            this.qddel_bt.Name = "qddel_bt";
            this.qddel_bt.Size = new System.Drawing.Size(60, 30);
            this.qddel_bt.TabIndex = 87;
            this.qddel_bt.Text = "删除";
            this.qddel_bt.UseVisualStyleBackColor = true;
            this.qddel_bt.Click += new System.EventHandler(this.qddel_bt_Click);
            // 
            // qdadd_bt
            // 
            this.qdadd_bt.Location = new System.Drawing.Point(7, 150);
            this.qdadd_bt.Name = "qdadd_bt";
            this.qdadd_bt.Size = new System.Drawing.Size(60, 30);
            this.qdadd_bt.TabIndex = 86;
            this.qdadd_bt.Text = "添加";
            this.qdadd_bt.UseVisualStyleBackColor = true;
            this.qdadd_bt.Click += new System.EventHandler(this.qdadd_bt_Click);
            // 
            // qdwlbh_cmb
            // 
            this.qdwlbh_cmb.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.qdwlbh_cmb.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.qdwlbh_cmb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.qdwlbh_cmb.FormattingEnabled = true;
            this.qdwlbh_cmb.ItemHeight = 14;
            this.qdwlbh_cmb.Location = new System.Drawing.Point(65, 77);
            this.qdwlbh_cmb.Name = "qdwlbh_cmb";
            this.qdwlbh_cmb.Size = new System.Drawing.Size(160, 22);
            this.qdwlbh_cmb.TabIndex = 85;
            // 
            // qdwlmc_cmb
            // 
            this.qdwlmc_cmb.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.qdwlmc_cmb.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.qdwlmc_cmb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.qdwlmc_cmb.FormattingEnabled = true;
            this.qdwlmc_cmb.ItemHeight = 14;
            this.qdwlmc_cmb.Location = new System.Drawing.Point(315, 77);
            this.qdwlmc_cmb.Name = "qdwlmc_cmb";
            this.qdwlmc_cmb.Size = new System.Drawing.Size(160, 22);
            this.qdwlmc_cmb.TabIndex = 84;
            // 
            // qdcglx_cmb
            // 
            this.qdcglx_cmb.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.qdcglx_cmb.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.qdcglx_cmb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.qdcglx_cmb.FormattingEnabled = true;
            this.qdcglx_cmb.ItemHeight = 14;
            this.qdcglx_cmb.Location = new System.Drawing.Point(65, 39);
            this.qdcglx_cmb.Name = "qdcglx_cmb";
            this.qdcglx_cmb.Size = new System.Drawing.Size(160, 22);
            this.qdcglx_cmb.TabIndex = 83;
            // 
            // qdsure_bt
            // 
            this.qdsure_bt.Location = new System.Drawing.Point(51, 270);
            this.qdsure_bt.Name = "qdsure_bt";
            this.qdsure_bt.Size = new System.Drawing.Size(100, 30);
            this.qdsure_bt.TabIndex = 75;
            this.qdsure_bt.Text = "确定";
            this.qdsure_bt.UseVisualStyleBackColor = true;
            this.qdsure_bt.Click += new System.EventHandler(this.qdsure_bt_Click);
            // 
            // qdcancel_bt
            // 
            this.qdcancel_bt.Location = new System.Drawing.Point(204, 270);
            this.qdcancel_bt.Name = "qdcancel_bt";
            this.qdcancel_bt.Size = new System.Drawing.Size(100, 30);
            this.qdcancel_bt.TabIndex = 74;
            this.qdcancel_bt.Text = "取消";
            this.qdcancel_bt.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("宋体", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label6.Location = new System.Drawing.Point(5, 120);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(71, 15);
            this.label6.TabIndex = 62;
            this.label6.Text = "条件类型";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("宋体", 9F);
            this.label5.Location = new System.Drawing.Point(5, 80);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 12);
            this.label5.TabIndex = 60;
            this.label5.Text = "物料编号";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("宋体", 9F);
            this.label4.Location = new System.Drawing.Point(255, 80);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 12);
            this.label4.TabIndex = 58;
            this.label4.Text = "物料名称";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("宋体", 9F);
            this.label3.Location = new System.Drawing.Point(5, 42);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 12);
            this.label3.TabIndex = 56;
            this.label3.Text = "采购类型";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label2.Location = new System.Drawing.Point(5, 10);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 12);
            this.label2.TabIndex = 0;
            this.label2.Text = "计算方案";
            // 
            // bjkc_tp
            // 
            this.bjkc_tp.BackColor = System.Drawing.SystemColors.Control;
            this.bjkc_tp.Controls.Add(this.panel7);
            this.bjkc_tp.Controls.Add(this.button15);
            this.bjkc_tp.Controls.Add(this.button16);
            this.bjkc_tp.Controls.Add(this.button17);
            this.bjkc_tp.Controls.Add(this.bjdel_bt);
            this.bjkc_tp.Controls.Add(this.bjquery_bt);
            this.bjkc_tp.Controls.Add(this.bjcgsl_tb);
            this.bjkc_tp.Controls.Add(this.label7);
            this.bjkc_tp.Controls.Add(this.bjckdm_tb);
            this.bjkc_tp.Controls.Add(this.bjwlbm_tb);
            this.bjkc_tp.Controls.Add(this.label8);
            this.bjkc_tp.Controls.Add(this.label9);
            this.bjkc_tp.Location = new System.Drawing.Point(4, 22);
            this.bjkc_tp.Name = "bjkc_tp";
            this.bjkc_tp.Padding = new System.Windows.Forms.Padding(3);
            this.bjkc_tp.Size = new System.Drawing.Size(902, 779);
            this.bjkc_tp.TabIndex = 1;
            this.bjkc_tp.Text = "比较库存";
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.label54);
            this.panel7.Controls.Add(this.textBox39);
            this.panel7.Controls.Add(this.textBox40);
            this.panel7.Controls.Add(this.textBox41);
            this.panel7.Controls.Add(this.label53);
            this.panel7.Controls.Add(this.textBox32);
            this.panel7.Controls.Add(this.textBox34);
            this.panel7.Controls.Add(this.textBox38);
            this.panel7.Controls.Add(this.label72);
            this.panel7.Controls.Add(this.textBox95);
            this.panel7.Controls.Add(this.textBox96);
            this.panel7.Controls.Add(this.textBox97);
            this.panel7.Controls.Add(this.label60);
            this.panel7.Controls.Add(this.textBox59);
            this.panel7.Controls.Add(this.textBox60);
            this.panel7.Controls.Add(this.textBox61);
            this.panel7.Controls.Add(this.label65);
            this.panel7.Controls.Add(this.textBox74);
            this.panel7.Controls.Add(this.textBox75);
            this.panel7.Controls.Add(this.textBox76);
            this.panel7.Controls.Add(this.label66);
            this.panel7.Controls.Add(this.textBox77);
            this.panel7.Controls.Add(this.textBox78);
            this.panel7.Controls.Add(this.textBox79);
            this.panel7.Controls.Add(this.label67);
            this.panel7.Controls.Add(this.textBox80);
            this.panel7.Controls.Add(this.textBox81);
            this.panel7.Controls.Add(this.textBox82);
            this.panel7.Controls.Add(this.label68);
            this.panel7.Controls.Add(this.textBox83);
            this.panel7.Controls.Add(this.textBox84);
            this.panel7.Controls.Add(this.textBox85);
            this.panel7.Controls.Add(this.checkBox5);
            this.panel7.Controls.Add(this.checkBox8);
            this.panel7.Controls.Add(this.checkBox10);
            this.panel7.Controls.Add(this.label69);
            this.panel7.Controls.Add(this.textBox86);
            this.panel7.Controls.Add(this.textBox87);
            this.panel7.Controls.Add(this.textBox88);
            this.panel7.Controls.Add(this.textBox89);
            this.panel7.Controls.Add(this.label70);
            this.panel7.Controls.Add(this.label71);
            this.panel7.Controls.Add(this.textBox90);
            this.panel7.Controls.Add(this.textBox91);
            this.panel7.Controls.Add(this.textBox92);
            this.panel7.Controls.Add(this.textBox93);
            this.panel7.Controls.Add(this.textBox94);
            this.panel7.Location = new System.Drawing.Point(7, 47);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(871, 153);
            this.panel7.TabIndex = 223;
            // 
            // label54
            // 
            this.label54.BackColor = System.Drawing.SystemColors.Window;
            this.label54.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label54.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label54.Location = new System.Drawing.Point(565, 15);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(80, 25);
            this.label54.TabIndex = 195;
            this.label54.Text = "库存数据";
            this.label54.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox39
            // 
            this.textBox39.BackColor = System.Drawing.SystemColors.Window;
            this.textBox39.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox39.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox39.Location = new System.Drawing.Point(565, 39);
            this.textBox39.Name = "textBox39";
            this.textBox39.Size = new System.Drawing.Size(80, 25);
            this.textBox39.TabIndex = 192;
            this.textBox39.Text = "1000";
            this.textBox39.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox40
            // 
            this.textBox40.BackColor = System.Drawing.SystemColors.Window;
            this.textBox40.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox40.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox40.Location = new System.Drawing.Point(565, 87);
            this.textBox40.Name = "textBox40";
            this.textBox40.Size = new System.Drawing.Size(80, 25);
            this.textBox40.TabIndex = 194;
            this.textBox40.Text = "1000";
            this.textBox40.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox41
            // 
            this.textBox41.BackColor = System.Drawing.SystemColors.Window;
            this.textBox41.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox41.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox41.Location = new System.Drawing.Point(565, 63);
            this.textBox41.Name = "textBox41";
            this.textBox41.Size = new System.Drawing.Size(80, 25);
            this.textBox41.TabIndex = 193;
            this.textBox41.Text = "2000";
            this.textBox41.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label53
            // 
            this.label53.BackColor = System.Drawing.SystemColors.Window;
            this.label53.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label53.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label53.Location = new System.Drawing.Point(407, 15);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(80, 25);
            this.label53.TabIndex = 191;
            this.label53.Text = "仓库名称";
            this.label53.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox32
            // 
            this.textBox32.BackColor = System.Drawing.SystemColors.Window;
            this.textBox32.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox32.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox32.Location = new System.Drawing.Point(407, 39);
            this.textBox32.Name = "textBox32";
            this.textBox32.Size = new System.Drawing.Size(80, 25);
            this.textBox32.TabIndex = 188;
            this.textBox32.Text = "1000";
            this.textBox32.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox34
            // 
            this.textBox34.BackColor = System.Drawing.SystemColors.Window;
            this.textBox34.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox34.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox34.Location = new System.Drawing.Point(407, 87);
            this.textBox34.Name = "textBox34";
            this.textBox34.Size = new System.Drawing.Size(80, 25);
            this.textBox34.TabIndex = 190;
            this.textBox34.Text = "1000";
            this.textBox34.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox38
            // 
            this.textBox38.BackColor = System.Drawing.SystemColors.Window;
            this.textBox38.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox38.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox38.Location = new System.Drawing.Point(407, 63);
            this.textBox38.Name = "textBox38";
            this.textBox38.Size = new System.Drawing.Size(80, 25);
            this.textBox38.TabIndex = 189;
            this.textBox38.Text = "2000";
            this.textBox38.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label72
            // 
            this.label72.BackColor = System.Drawing.SystemColors.Window;
            this.label72.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label72.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label72.Location = new System.Drawing.Point(249, 15);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(80, 25);
            this.label72.TabIndex = 187;
            this.label72.Text = "物料组";
            this.label72.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox95
            // 
            this.textBox95.BackColor = System.Drawing.SystemColors.Window;
            this.textBox95.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox95.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox95.Location = new System.Drawing.Point(249, 39);
            this.textBox95.Name = "textBox95";
            this.textBox95.Size = new System.Drawing.Size(80, 25);
            this.textBox95.TabIndex = 184;
            this.textBox95.Text = "1000";
            this.textBox95.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox96
            // 
            this.textBox96.BackColor = System.Drawing.SystemColors.Window;
            this.textBox96.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox96.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox96.Location = new System.Drawing.Point(249, 87);
            this.textBox96.Name = "textBox96";
            this.textBox96.Size = new System.Drawing.Size(80, 25);
            this.textBox96.TabIndex = 186;
            this.textBox96.Text = "1000";
            this.textBox96.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox97
            // 
            this.textBox97.BackColor = System.Drawing.SystemColors.Window;
            this.textBox97.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox97.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox97.Location = new System.Drawing.Point(249, 63);
            this.textBox97.Name = "textBox97";
            this.textBox97.Size = new System.Drawing.Size(80, 25);
            this.textBox97.TabIndex = 185;
            this.textBox97.Text = "2000";
            this.textBox97.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label60
            // 
            this.label60.BackColor = System.Drawing.SystemColors.Window;
            this.label60.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label60.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label60.Location = new System.Drawing.Point(644, 15);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(80, 25);
            this.label60.TabIndex = 167;
            this.label60.Text = "需求数量";
            this.label60.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox59
            // 
            this.textBox59.BackColor = System.Drawing.SystemColors.Window;
            this.textBox59.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox59.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox59.Location = new System.Drawing.Point(644, 39);
            this.textBox59.Name = "textBox59";
            this.textBox59.Size = new System.Drawing.Size(80, 25);
            this.textBox59.TabIndex = 164;
            this.textBox59.Text = "1000";
            this.textBox59.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox60
            // 
            this.textBox60.BackColor = System.Drawing.SystemColors.Window;
            this.textBox60.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox60.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox60.Location = new System.Drawing.Point(644, 87);
            this.textBox60.Name = "textBox60";
            this.textBox60.Size = new System.Drawing.Size(80, 25);
            this.textBox60.TabIndex = 166;
            this.textBox60.Text = "1000";
            this.textBox60.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox61
            // 
            this.textBox61.BackColor = System.Drawing.SystemColors.Window;
            this.textBox61.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox61.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox61.Location = new System.Drawing.Point(644, 63);
            this.textBox61.Name = "textBox61";
            this.textBox61.Size = new System.Drawing.Size(80, 25);
            this.textBox61.TabIndex = 165;
            this.textBox61.Text = "2000";
            this.textBox61.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label65
            // 
            this.label65.BackColor = System.Drawing.SystemColors.Window;
            this.label65.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label65.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label65.Location = new System.Drawing.Point(328, 15);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(80, 25);
            this.label65.TabIndex = 163;
            this.label65.Text = "仓库编号";
            this.label65.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox74
            // 
            this.textBox74.BackColor = System.Drawing.SystemColors.Window;
            this.textBox74.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox74.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox74.Location = new System.Drawing.Point(328, 39);
            this.textBox74.Name = "textBox74";
            this.textBox74.Size = new System.Drawing.Size(80, 25);
            this.textBox74.TabIndex = 160;
            this.textBox74.Text = "1000";
            this.textBox74.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox75
            // 
            this.textBox75.BackColor = System.Drawing.SystemColors.Window;
            this.textBox75.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox75.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox75.Location = new System.Drawing.Point(328, 87);
            this.textBox75.Name = "textBox75";
            this.textBox75.Size = new System.Drawing.Size(80, 25);
            this.textBox75.TabIndex = 162;
            this.textBox75.Text = "1000";
            this.textBox75.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox76
            // 
            this.textBox76.BackColor = System.Drawing.SystemColors.Window;
            this.textBox76.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox76.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox76.Location = new System.Drawing.Point(328, 63);
            this.textBox76.Name = "textBox76";
            this.textBox76.Size = new System.Drawing.Size(80, 25);
            this.textBox76.TabIndex = 161;
            this.textBox76.Text = "2000";
            this.textBox76.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label66
            // 
            this.label66.BackColor = System.Drawing.SystemColors.Window;
            this.label66.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label66.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label66.Location = new System.Drawing.Point(71, 15);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(100, 25);
            this.label66.TabIndex = 159;
            this.label66.Text = "物料编号";
            this.label66.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox77
            // 
            this.textBox77.BackColor = System.Drawing.SystemColors.Window;
            this.textBox77.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox77.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox77.Location = new System.Drawing.Point(71, 39);
            this.textBox77.Name = "textBox77";
            this.textBox77.Size = new System.Drawing.Size(100, 25);
            this.textBox77.TabIndex = 156;
            this.textBox77.Text = "F00000011";
            this.textBox77.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox78
            // 
            this.textBox78.BackColor = System.Drawing.SystemColors.Window;
            this.textBox78.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox78.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox78.Location = new System.Drawing.Point(71, 87);
            this.textBox78.Name = "textBox78";
            this.textBox78.Size = new System.Drawing.Size(100, 25);
            this.textBox78.TabIndex = 158;
            this.textBox78.Text = "F00000026";
            this.textBox78.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox79
            // 
            this.textBox79.BackColor = System.Drawing.SystemColors.Window;
            this.textBox79.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox79.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox79.Location = new System.Drawing.Point(71, 63);
            this.textBox79.Name = "textBox79";
            this.textBox79.Size = new System.Drawing.Size(100, 25);
            this.textBox79.TabIndex = 157;
            this.textBox79.Text = "F00000026";
            this.textBox79.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label67
            // 
            this.label67.BackColor = System.Drawing.SystemColors.Window;
            this.label67.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label67.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label67.Location = new System.Drawing.Point(723, 15);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(80, 25);
            this.label67.TabIndex = 151;
            this.label67.Text = "采购数量";
            this.label67.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox80
            // 
            this.textBox80.BackColor = System.Drawing.SystemColors.Window;
            this.textBox80.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox80.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox80.Location = new System.Drawing.Point(723, 39);
            this.textBox80.Name = "textBox80";
            this.textBox80.Size = new System.Drawing.Size(80, 25);
            this.textBox80.TabIndex = 147;
            this.textBox80.Text = "1000";
            this.textBox80.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox81
            // 
            this.textBox81.BackColor = System.Drawing.SystemColors.Window;
            this.textBox81.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox81.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox81.Location = new System.Drawing.Point(723, 87);
            this.textBox81.Name = "textBox81";
            this.textBox81.Size = new System.Drawing.Size(80, 25);
            this.textBox81.TabIndex = 149;
            this.textBox81.Text = "1000";
            this.textBox81.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox82
            // 
            this.textBox82.BackColor = System.Drawing.SystemColors.Window;
            this.textBox82.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox82.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox82.Location = new System.Drawing.Point(723, 63);
            this.textBox82.Name = "textBox82";
            this.textBox82.Size = new System.Drawing.Size(80, 25);
            this.textBox82.TabIndex = 148;
            this.textBox82.Text = "2000";
            this.textBox82.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label68
            // 
            this.label68.BackColor = System.Drawing.SystemColors.Window;
            this.label68.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label68.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label68.Location = new System.Drawing.Point(170, 15);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(80, 25);
            this.label68.TabIndex = 146;
            this.label68.Text = "物料类型";
            this.label68.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox83
            // 
            this.textBox83.BackColor = System.Drawing.SystemColors.Window;
            this.textBox83.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox83.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox83.Location = new System.Drawing.Point(170, 39);
            this.textBox83.Name = "textBox83";
            this.textBox83.Size = new System.Drawing.Size(80, 25);
            this.textBox83.TabIndex = 142;
            this.textBox83.Text = "F00000011";
            this.textBox83.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox84
            // 
            this.textBox84.BackColor = System.Drawing.SystemColors.Window;
            this.textBox84.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox84.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox84.Location = new System.Drawing.Point(170, 87);
            this.textBox84.Name = "textBox84";
            this.textBox84.Size = new System.Drawing.Size(80, 25);
            this.textBox84.TabIndex = 144;
            this.textBox84.Text = "F00000026";
            this.textBox84.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox85
            // 
            this.textBox85.BackColor = System.Drawing.SystemColors.Window;
            this.textBox85.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox85.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox85.Location = new System.Drawing.Point(170, 63);
            this.textBox85.Name = "textBox85";
            this.textBox85.Size = new System.Drawing.Size(80, 25);
            this.textBox85.TabIndex = 143;
            this.textBox85.Text = "F00000026";
            this.textBox85.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // checkBox5
            // 
            this.checkBox5.AutoSize = true;
            this.checkBox5.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.checkBox5.Location = new System.Drawing.Point(11, 44);
            this.checkBox5.Name = "checkBox5";
            this.checkBox5.Size = new System.Drawing.Size(15, 14);
            this.checkBox5.TabIndex = 121;
            this.checkBox5.UseVisualStyleBackColor = false;
            // 
            // checkBox8
            // 
            this.checkBox8.AutoSize = true;
            this.checkBox8.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.checkBox8.Location = new System.Drawing.Point(11, 93);
            this.checkBox8.Name = "checkBox8";
            this.checkBox8.Size = new System.Drawing.Size(15, 14);
            this.checkBox8.TabIndex = 118;
            this.checkBox8.UseVisualStyleBackColor = false;
            // 
            // checkBox10
            // 
            this.checkBox10.AutoSize = true;
            this.checkBox10.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.checkBox10.Location = new System.Drawing.Point(11, 69);
            this.checkBox10.Name = "checkBox10";
            this.checkBox10.Size = new System.Drawing.Size(15, 14);
            this.checkBox10.TabIndex = 116;
            this.checkBox10.UseVisualStyleBackColor = false;
            // 
            // label69
            // 
            this.label69.BackColor = System.Drawing.SystemColors.Window;
            this.label69.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label69.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label69.Location = new System.Drawing.Point(486, 15);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(80, 25);
            this.label69.TabIndex = 114;
            this.label69.Text = "计量单位";
            this.label69.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox86
            // 
            this.textBox86.BackColor = System.Drawing.SystemColors.Window;
            this.textBox86.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox86.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox86.Location = new System.Drawing.Point(486, 39);
            this.textBox86.Name = "textBox86";
            this.textBox86.Size = new System.Drawing.Size(80, 25);
            this.textBox86.TabIndex = 108;
            this.textBox86.Text = "TON";
            this.textBox86.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox87
            // 
            this.textBox87.BackColor = System.Drawing.SystemColors.Window;
            this.textBox87.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox87.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox87.Location = new System.Drawing.Point(486, 87);
            this.textBox87.Name = "textBox87";
            this.textBox87.Size = new System.Drawing.Size(80, 25);
            this.textBox87.TabIndex = 110;
            this.textBox87.Text = "TON";
            this.textBox87.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox88
            // 
            this.textBox88.BackColor = System.Drawing.SystemColors.Window;
            this.textBox88.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox88.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox88.Location = new System.Drawing.Point(486, 63);
            this.textBox88.Name = "textBox88";
            this.textBox88.Size = new System.Drawing.Size(80, 25);
            this.textBox88.TabIndex = 109;
            this.textBox88.Text = "TON";
            this.textBox88.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox89
            // 
            this.textBox89.BackColor = System.Drawing.SystemColors.Window;
            this.textBox89.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox89.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox89.Location = new System.Drawing.Point(3, 39);
            this.textBox89.Name = "textBox89";
            this.textBox89.Size = new System.Drawing.Size(30, 25);
            this.textBox89.TabIndex = 0;
            this.textBox89.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label70
            // 
            this.label70.BackColor = System.Drawing.SystemColors.Window;
            this.label70.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label70.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label70.Location = new System.Drawing.Point(3, 15);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(30, 25);
            this.label70.TabIndex = 86;
            this.label70.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label71
            // 
            this.label71.BackColor = System.Drawing.SystemColors.Window;
            this.label71.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label71.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label71.Location = new System.Drawing.Point(32, 15);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(40, 25);
            this.label71.TabIndex = 86;
            this.label71.Text = "序号";
            this.label71.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox90
            // 
            this.textBox90.BackColor = System.Drawing.SystemColors.Window;
            this.textBox90.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox90.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox90.Location = new System.Drawing.Point(3, 87);
            this.textBox90.Name = "textBox90";
            this.textBox90.Size = new System.Drawing.Size(30, 25);
            this.textBox90.TabIndex = 8;
            this.textBox90.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox91
            // 
            this.textBox91.BackColor = System.Drawing.SystemColors.Window;
            this.textBox91.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox91.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox91.Location = new System.Drawing.Point(32, 39);
            this.textBox91.Name = "textBox91";
            this.textBox91.Size = new System.Drawing.Size(40, 25);
            this.textBox91.TabIndex = 1;
            this.textBox91.Text = "1";
            this.textBox91.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox92
            // 
            this.textBox92.BackColor = System.Drawing.SystemColors.Window;
            this.textBox92.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox92.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox92.Location = new System.Drawing.Point(32, 87);
            this.textBox92.Name = "textBox92";
            this.textBox92.Size = new System.Drawing.Size(40, 25);
            this.textBox92.TabIndex = 9;
            this.textBox92.Text = "3";
            this.textBox92.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox93
            // 
            this.textBox93.BackColor = System.Drawing.SystemColors.Window;
            this.textBox93.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox93.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox93.Location = new System.Drawing.Point(3, 63);
            this.textBox93.Name = "textBox93";
            this.textBox93.Size = new System.Drawing.Size(30, 25);
            this.textBox93.TabIndex = 4;
            this.textBox93.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox94
            // 
            this.textBox94.BackColor = System.Drawing.SystemColors.Window;
            this.textBox94.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox94.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox94.Location = new System.Drawing.Point(32, 63);
            this.textBox94.Name = "textBox94";
            this.textBox94.Size = new System.Drawing.Size(40, 25);
            this.textBox94.TabIndex = 5;
            this.textBox94.Text = "2";
            this.textBox94.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // button15
            // 
            this.button15.Location = new System.Drawing.Point(97, 206);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(100, 30);
            this.button15.TabIndex = 78;
            this.button15.Text = "确定";
            this.button15.UseVisualStyleBackColor = true;
            // 
            // button16
            // 
            this.button16.Location = new System.Drawing.Point(250, 206);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(100, 30);
            this.button16.TabIndex = 77;
            this.button16.Text = "取消";
            this.button16.UseVisualStyleBackColor = true;
            // 
            // button17
            // 
            this.button17.Location = new System.Drawing.Point(406, 206);
            this.button17.Name = "button17";
            this.button17.Size = new System.Drawing.Size(100, 30);
            this.button17.TabIndex = 76;
            this.button17.Text = "返回";
            this.button17.UseVisualStyleBackColor = true;
            // 
            // bjdel_bt
            // 
            this.bjdel_bt.Location = new System.Drawing.Point(747, 11);
            this.bjdel_bt.Name = "bjdel_bt";
            this.bjdel_bt.Size = new System.Drawing.Size(60, 30);
            this.bjdel_bt.TabIndex = 72;
            this.bjdel_bt.Text = "删除";
            this.bjdel_bt.UseVisualStyleBackColor = true;
            // 
            // bjquery_bt
            // 
            this.bjquery_bt.Location = new System.Drawing.Point(673, 11);
            this.bjquery_bt.Name = "bjquery_bt";
            this.bjquery_bt.Size = new System.Drawing.Size(60, 30);
            this.bjquery_bt.TabIndex = 71;
            this.bjquery_bt.Text = "查询";
            this.bjquery_bt.UseVisualStyleBackColor = true;
            // 
            // bjcgsl_tb
            // 
            this.bjcgsl_tb.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.bjcgsl_tb.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.HistoryList;
            this.bjcgsl_tb.Location = new System.Drawing.Point(503, 16);
            this.bjcgsl_tb.Name = "bjcgsl_tb";
            this.bjcgsl_tb.Size = new System.Drawing.Size(120, 21);
            this.bjcgsl_tb.TabIndex = 70;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("宋体", 9F);
            this.label7.Location = new System.Drawing.Point(443, 20);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 12);
            this.label7.TabIndex = 69;
            this.label7.Text = "采购数量";
            // 
            // bjckdm_tb
            // 
            this.bjckdm_tb.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.bjckdm_tb.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.bjckdm_tb.Location = new System.Drawing.Point(281, 16);
            this.bjckdm_tb.Name = "bjckdm_tb";
            this.bjckdm_tb.Size = new System.Drawing.Size(120, 20);
            this.bjckdm_tb.TabIndex = 68;
            // 
            // bjwlbm_tb
            // 
            this.bjwlbm_tb.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.bjwlbm_tb.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.bjwlbm_tb.Location = new System.Drawing.Point(65, 16);
            this.bjwlbm_tb.Name = "bjwlbm_tb";
            this.bjwlbm_tb.Size = new System.Drawing.Size(120, 20);
            this.bjwlbm_tb.TabIndex = 67;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("宋体", 9F);
            this.label8.Location = new System.Drawing.Point(221, 20);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(53, 12);
            this.label8.TabIndex = 66;
            this.label8.Text = "仓库代码";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("宋体", 9F);
            this.label9.Location = new System.Drawing.Point(5, 20);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(53, 12);
            this.label9.TabIndex = 65;
            this.label9.Text = "物料编码";
            // 
            // ysgk_tp
            // 
            this.ysgk_tp.BackColor = System.Drawing.SystemColors.Control;
            this.ysgk_tp.Controls.Add(this.panel9);
            this.ysgk_tp.Controls.Add(this.button18);
            this.ysgk_tp.Controls.Add(this.button19);
            this.ysgk_tp.Controls.Add(this.button20);
            this.ysgk_tp.Controls.Add(this.ysdel_bt);
            this.ysgk_tp.Controls.Add(this.ysquery_bt);
            this.ysgk_tp.Controls.Add(this.ysysph_tb);
            this.ysgk_tp.Controls.Add(this.label10);
            this.ysgk_tp.Controls.Add(this.yssqbm_tb);
            this.ysgk_tp.Controls.Add(this.yswlbm_tb);
            this.ysgk_tp.Controls.Add(this.label13);
            this.ysgk_tp.Controls.Add(this.label14);
            this.ysgk_tp.Location = new System.Drawing.Point(4, 22);
            this.ysgk_tp.Name = "ysgk_tp";
            this.ysgk_tp.Padding = new System.Windows.Forms.Padding(3);
            this.ysgk_tp.Size = new System.Drawing.Size(902, 779);
            this.ysgk_tp.TabIndex = 2;
            this.ysgk_tp.Text = "预算管控";
            // 
            // panel9
            // 
            this.panel9.Controls.Add(this.label82);
            this.panel9.Controls.Add(this.textBox116);
            this.panel9.Controls.Add(this.textBox117);
            this.panel9.Controls.Add(this.textBox118);
            this.panel9.Controls.Add(this.label78);
            this.panel9.Controls.Add(this.textBox104);
            this.panel9.Controls.Add(this.textBox105);
            this.panel9.Controls.Add(this.textBox106);
            this.panel9.Controls.Add(this.label55);
            this.panel9.Controls.Add(this.textBox21);
            this.panel9.Controls.Add(this.textBox23);
            this.panel9.Controls.Add(this.textBox24);
            this.panel9.Controls.Add(this.label56);
            this.panel9.Controls.Add(this.textBox25);
            this.panel9.Controls.Add(this.textBox26);
            this.panel9.Controls.Add(this.textBox27);
            this.panel9.Controls.Add(this.label73);
            this.panel9.Controls.Add(this.textBox28);
            this.panel9.Controls.Add(this.textBox29);
            this.panel9.Controls.Add(this.textBox30);
            this.panel9.Controls.Add(this.label74);
            this.panel9.Controls.Add(this.textBox31);
            this.panel9.Controls.Add(this.textBox45);
            this.panel9.Controls.Add(this.textBox46);
            this.panel9.Controls.Add(this.label75);
            this.panel9.Controls.Add(this.textBox47);
            this.panel9.Controls.Add(this.textBox48);
            this.panel9.Controls.Add(this.textBox49);
            this.panel9.Controls.Add(this.label76);
            this.panel9.Controls.Add(this.textBox98);
            this.panel9.Controls.Add(this.textBox99);
            this.panel9.Controls.Add(this.textBox100);
            this.panel9.Controls.Add(this.label77);
            this.panel9.Controls.Add(this.textBox101);
            this.panel9.Controls.Add(this.textBox102);
            this.panel9.Controls.Add(this.textBox103);
            this.panel9.Controls.Add(this.checkBox11);
            this.panel9.Controls.Add(this.checkBox12);
            this.panel9.Controls.Add(this.checkBox13);
            this.panel9.Controls.Add(this.label79);
            this.panel9.Controls.Add(this.textBox107);
            this.panel9.Controls.Add(this.textBox108);
            this.panel9.Controls.Add(this.textBox109);
            this.panel9.Controls.Add(this.textBox110);
            this.panel9.Controls.Add(this.label80);
            this.panel9.Controls.Add(this.label81);
            this.panel9.Controls.Add(this.textBox111);
            this.panel9.Controls.Add(this.textBox112);
            this.panel9.Controls.Add(this.textBox113);
            this.panel9.Controls.Add(this.textBox114);
            this.panel9.Controls.Add(this.textBox115);
            this.panel9.Location = new System.Drawing.Point(7, 47);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(893, 153);
            this.panel9.TabIndex = 224;
            // 
            // label82
            // 
            this.label82.BackColor = System.Drawing.SystemColors.Window;
            this.label82.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label82.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label82.Location = new System.Drawing.Point(807, 15);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(80, 25);
            this.label82.TabIndex = 203;
            this.label82.Text = "预算平衡";
            this.label82.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox116
            // 
            this.textBox116.BackColor = System.Drawing.SystemColors.Window;
            this.textBox116.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox116.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox116.Location = new System.Drawing.Point(807, 39);
            this.textBox116.Name = "textBox116";
            this.textBox116.Size = new System.Drawing.Size(80, 25);
            this.textBox116.TabIndex = 200;
            this.textBox116.Text = "1000";
            this.textBox116.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox117
            // 
            this.textBox117.BackColor = System.Drawing.SystemColors.Window;
            this.textBox117.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox117.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox117.Location = new System.Drawing.Point(807, 87);
            this.textBox117.Name = "textBox117";
            this.textBox117.Size = new System.Drawing.Size(80, 25);
            this.textBox117.TabIndex = 202;
            this.textBox117.Text = "1000";
            this.textBox117.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox118
            // 
            this.textBox118.BackColor = System.Drawing.SystemColors.Window;
            this.textBox118.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox118.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox118.Location = new System.Drawing.Point(807, 63);
            this.textBox118.Name = "textBox118";
            this.textBox118.Size = new System.Drawing.Size(80, 25);
            this.textBox118.TabIndex = 201;
            this.textBox118.Text = "2000";
            this.textBox118.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label78
            // 
            this.label78.BackColor = System.Drawing.SystemColors.Window;
            this.label78.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label78.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label78.Location = new System.Drawing.Point(170, 15);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(65, 25);
            this.label78.TabIndex = 199;
            this.label78.Text = "计量单位";
            this.label78.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox104
            // 
            this.textBox104.BackColor = System.Drawing.SystemColors.Window;
            this.textBox104.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox104.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox104.Location = new System.Drawing.Point(170, 39);
            this.textBox104.Name = "textBox104";
            this.textBox104.Size = new System.Drawing.Size(65, 25);
            this.textBox104.TabIndex = 196;
            this.textBox104.Text = "TON";
            this.textBox104.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox105
            // 
            this.textBox105.BackColor = System.Drawing.SystemColors.Window;
            this.textBox105.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox105.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox105.Location = new System.Drawing.Point(170, 87);
            this.textBox105.Name = "textBox105";
            this.textBox105.Size = new System.Drawing.Size(65, 25);
            this.textBox105.TabIndex = 198;
            this.textBox105.Text = "TON";
            this.textBox105.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox106
            // 
            this.textBox106.BackColor = System.Drawing.SystemColors.Window;
            this.textBox106.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox106.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox106.Location = new System.Drawing.Point(170, 63);
            this.textBox106.Name = "textBox106";
            this.textBox106.Size = new System.Drawing.Size(65, 25);
            this.textBox106.TabIndex = 197;
            this.textBox106.Text = "TON";
            this.textBox106.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label55
            // 
            this.label55.BackColor = System.Drawing.SystemColors.Window;
            this.label55.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label55.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label55.Location = new System.Drawing.Point(570, 15);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(80, 25);
            this.label55.TabIndex = 195;
            this.label55.Text = "已使用预算";
            this.label55.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox21
            // 
            this.textBox21.BackColor = System.Drawing.SystemColors.Window;
            this.textBox21.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox21.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox21.Location = new System.Drawing.Point(570, 39);
            this.textBox21.Name = "textBox21";
            this.textBox21.Size = new System.Drawing.Size(80, 25);
            this.textBox21.TabIndex = 192;
            this.textBox21.Text = "1000";
            this.textBox21.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox23
            // 
            this.textBox23.BackColor = System.Drawing.SystemColors.Window;
            this.textBox23.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox23.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox23.Location = new System.Drawing.Point(570, 87);
            this.textBox23.Name = "textBox23";
            this.textBox23.Size = new System.Drawing.Size(80, 25);
            this.textBox23.TabIndex = 194;
            this.textBox23.Text = "1000";
            this.textBox23.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox24
            // 
            this.textBox24.BackColor = System.Drawing.SystemColors.Window;
            this.textBox24.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox24.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox24.Location = new System.Drawing.Point(570, 63);
            this.textBox24.Name = "textBox24";
            this.textBox24.Size = new System.Drawing.Size(80, 25);
            this.textBox24.TabIndex = 193;
            this.textBox24.Text = "2000";
            this.textBox24.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label56
            // 
            this.label56.BackColor = System.Drawing.SystemColors.Window;
            this.label56.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label56.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label56.Location = new System.Drawing.Point(392, 15);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(100, 25);
            this.label56.TabIndex = 191;
            this.label56.Text = "申请部门";
            this.label56.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox25
            // 
            this.textBox25.BackColor = System.Drawing.SystemColors.Window;
            this.textBox25.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox25.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox25.Location = new System.Drawing.Point(392, 39);
            this.textBox25.Name = "textBox25";
            this.textBox25.Size = new System.Drawing.Size(100, 25);
            this.textBox25.TabIndex = 188;
            this.textBox25.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox26
            // 
            this.textBox26.BackColor = System.Drawing.SystemColors.Window;
            this.textBox26.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox26.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox26.Location = new System.Drawing.Point(392, 87);
            this.textBox26.Name = "textBox26";
            this.textBox26.Size = new System.Drawing.Size(100, 25);
            this.textBox26.TabIndex = 190;
            this.textBox26.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox27
            // 
            this.textBox27.BackColor = System.Drawing.SystemColors.Window;
            this.textBox27.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox27.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox27.Location = new System.Drawing.Point(392, 63);
            this.textBox27.Name = "textBox27";
            this.textBox27.Size = new System.Drawing.Size(100, 25);
            this.textBox27.TabIndex = 189;
            this.textBox27.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label73
            // 
            this.label73.BackColor = System.Drawing.SystemColors.Window;
            this.label73.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label73.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label73.Location = new System.Drawing.Point(234, 15);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(80, 25);
            this.label73.TabIndex = 187;
            this.label73.Text = "采购数量";
            this.label73.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox28
            // 
            this.textBox28.BackColor = System.Drawing.SystemColors.Window;
            this.textBox28.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox28.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox28.Location = new System.Drawing.Point(234, 39);
            this.textBox28.Name = "textBox28";
            this.textBox28.Size = new System.Drawing.Size(80, 25);
            this.textBox28.TabIndex = 184;
            this.textBox28.Text = "1000";
            this.textBox28.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox29
            // 
            this.textBox29.BackColor = System.Drawing.SystemColors.Window;
            this.textBox29.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox29.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox29.Location = new System.Drawing.Point(234, 87);
            this.textBox29.Name = "textBox29";
            this.textBox29.Size = new System.Drawing.Size(80, 25);
            this.textBox29.TabIndex = 186;
            this.textBox29.Text = "1000";
            this.textBox29.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox30
            // 
            this.textBox30.BackColor = System.Drawing.SystemColors.Window;
            this.textBox30.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox30.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox30.Location = new System.Drawing.Point(234, 63);
            this.textBox30.Name = "textBox30";
            this.textBox30.Size = new System.Drawing.Size(80, 25);
            this.textBox30.TabIndex = 185;
            this.textBox30.Text = "2000";
            this.textBox30.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label74
            // 
            this.label74.BackColor = System.Drawing.SystemColors.Window;
            this.label74.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label74.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label74.Location = new System.Drawing.Point(649, 15);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(80, 25);
            this.label74.TabIndex = 167;
            this.label74.Text = "剩余预算";
            this.label74.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox31
            // 
            this.textBox31.BackColor = System.Drawing.SystemColors.Window;
            this.textBox31.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox31.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox31.Location = new System.Drawing.Point(649, 39);
            this.textBox31.Name = "textBox31";
            this.textBox31.Size = new System.Drawing.Size(80, 25);
            this.textBox31.TabIndex = 164;
            this.textBox31.Text = "1000";
            this.textBox31.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox45
            // 
            this.textBox45.BackColor = System.Drawing.SystemColors.Window;
            this.textBox45.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox45.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox45.Location = new System.Drawing.Point(649, 87);
            this.textBox45.Name = "textBox45";
            this.textBox45.Size = new System.Drawing.Size(80, 25);
            this.textBox45.TabIndex = 166;
            this.textBox45.Text = "1000";
            this.textBox45.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox46
            // 
            this.textBox46.BackColor = System.Drawing.SystemColors.Window;
            this.textBox46.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox46.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox46.Location = new System.Drawing.Point(649, 63);
            this.textBox46.Name = "textBox46";
            this.textBox46.Size = new System.Drawing.Size(80, 25);
            this.textBox46.TabIndex = 165;
            this.textBox46.Text = "2000";
            this.textBox46.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label75
            // 
            this.label75.BackColor = System.Drawing.SystemColors.Window;
            this.label75.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label75.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label75.Location = new System.Drawing.Point(313, 15);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(80, 25);
            this.label75.TabIndex = 163;
            this.label75.Text = "采购价格";
            this.label75.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox47
            // 
            this.textBox47.BackColor = System.Drawing.SystemColors.Window;
            this.textBox47.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox47.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox47.Location = new System.Drawing.Point(313, 39);
            this.textBox47.Name = "textBox47";
            this.textBox47.Size = new System.Drawing.Size(80, 25);
            this.textBox47.TabIndex = 160;
            this.textBox47.Text = "1000";
            this.textBox47.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox48
            // 
            this.textBox48.BackColor = System.Drawing.SystemColors.Window;
            this.textBox48.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox48.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox48.Location = new System.Drawing.Point(313, 87);
            this.textBox48.Name = "textBox48";
            this.textBox48.Size = new System.Drawing.Size(80, 25);
            this.textBox48.TabIndex = 162;
            this.textBox48.Text = "1000";
            this.textBox48.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox49
            // 
            this.textBox49.BackColor = System.Drawing.SystemColors.Window;
            this.textBox49.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox49.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox49.Location = new System.Drawing.Point(313, 63);
            this.textBox49.Name = "textBox49";
            this.textBox49.Size = new System.Drawing.Size(80, 25);
            this.textBox49.TabIndex = 161;
            this.textBox49.Text = "2000";
            this.textBox49.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label76
            // 
            this.label76.BackColor = System.Drawing.SystemColors.Window;
            this.label76.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label76.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label76.Location = new System.Drawing.Point(71, 15);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(100, 25);
            this.label76.TabIndex = 159;
            this.label76.Text = "物料编号";
            this.label76.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox98
            // 
            this.textBox98.BackColor = System.Drawing.SystemColors.Window;
            this.textBox98.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox98.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox98.Location = new System.Drawing.Point(71, 39);
            this.textBox98.Name = "textBox98";
            this.textBox98.Size = new System.Drawing.Size(100, 25);
            this.textBox98.TabIndex = 156;
            this.textBox98.Text = "F00000011";
            this.textBox98.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox99
            // 
            this.textBox99.BackColor = System.Drawing.SystemColors.Window;
            this.textBox99.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox99.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox99.Location = new System.Drawing.Point(71, 87);
            this.textBox99.Name = "textBox99";
            this.textBox99.Size = new System.Drawing.Size(100, 25);
            this.textBox99.TabIndex = 158;
            this.textBox99.Text = "F00000026";
            this.textBox99.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox100
            // 
            this.textBox100.BackColor = System.Drawing.SystemColors.Window;
            this.textBox100.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox100.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox100.Location = new System.Drawing.Point(71, 63);
            this.textBox100.Name = "textBox100";
            this.textBox100.Size = new System.Drawing.Size(100, 25);
            this.textBox100.TabIndex = 157;
            this.textBox100.Text = "F00000026";
            this.textBox100.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label77
            // 
            this.label77.BackColor = System.Drawing.SystemColors.Window;
            this.label77.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label77.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label77.Location = new System.Drawing.Point(728, 15);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(80, 25);
            this.label77.TabIndex = 151;
            this.label77.Text = "采购预算";
            this.label77.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox101
            // 
            this.textBox101.BackColor = System.Drawing.SystemColors.Window;
            this.textBox101.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox101.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox101.Location = new System.Drawing.Point(728, 39);
            this.textBox101.Name = "textBox101";
            this.textBox101.Size = new System.Drawing.Size(80, 25);
            this.textBox101.TabIndex = 147;
            this.textBox101.Text = "1000";
            this.textBox101.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox102
            // 
            this.textBox102.BackColor = System.Drawing.SystemColors.Window;
            this.textBox102.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox102.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox102.Location = new System.Drawing.Point(728, 87);
            this.textBox102.Name = "textBox102";
            this.textBox102.Size = new System.Drawing.Size(80, 25);
            this.textBox102.TabIndex = 149;
            this.textBox102.Text = "1000";
            this.textBox102.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox103
            // 
            this.textBox103.BackColor = System.Drawing.SystemColors.Window;
            this.textBox103.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox103.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox103.Location = new System.Drawing.Point(728, 63);
            this.textBox103.Name = "textBox103";
            this.textBox103.Size = new System.Drawing.Size(80, 25);
            this.textBox103.TabIndex = 148;
            this.textBox103.Text = "2000";
            this.textBox103.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // checkBox11
            // 
            this.checkBox11.AutoSize = true;
            this.checkBox11.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.checkBox11.Location = new System.Drawing.Point(11, 44);
            this.checkBox11.Name = "checkBox11";
            this.checkBox11.Size = new System.Drawing.Size(15, 14);
            this.checkBox11.TabIndex = 121;
            this.checkBox11.UseVisualStyleBackColor = false;
            // 
            // checkBox12
            // 
            this.checkBox12.AutoSize = true;
            this.checkBox12.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.checkBox12.Location = new System.Drawing.Point(11, 93);
            this.checkBox12.Name = "checkBox12";
            this.checkBox12.Size = new System.Drawing.Size(15, 14);
            this.checkBox12.TabIndex = 118;
            this.checkBox12.UseVisualStyleBackColor = false;
            // 
            // checkBox13
            // 
            this.checkBox13.AutoSize = true;
            this.checkBox13.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.checkBox13.Location = new System.Drawing.Point(11, 69);
            this.checkBox13.Name = "checkBox13";
            this.checkBox13.Size = new System.Drawing.Size(15, 14);
            this.checkBox13.TabIndex = 116;
            this.checkBox13.UseVisualStyleBackColor = false;
            // 
            // label79
            // 
            this.label79.BackColor = System.Drawing.SystemColors.Window;
            this.label79.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label79.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label79.Location = new System.Drawing.Point(491, 15);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(80, 25);
            this.label79.TabIndex = 114;
            this.label79.Text = "部门预算";
            this.label79.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox107
            // 
            this.textBox107.BackColor = System.Drawing.SystemColors.Window;
            this.textBox107.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox107.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox107.Location = new System.Drawing.Point(491, 39);
            this.textBox107.Name = "textBox107";
            this.textBox107.Size = new System.Drawing.Size(80, 25);
            this.textBox107.TabIndex = 108;
            this.textBox107.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox108
            // 
            this.textBox108.BackColor = System.Drawing.SystemColors.Window;
            this.textBox108.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox108.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox108.Location = new System.Drawing.Point(491, 87);
            this.textBox108.Name = "textBox108";
            this.textBox108.Size = new System.Drawing.Size(80, 25);
            this.textBox108.TabIndex = 110;
            this.textBox108.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox109
            // 
            this.textBox109.BackColor = System.Drawing.SystemColors.Window;
            this.textBox109.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox109.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox109.Location = new System.Drawing.Point(491, 63);
            this.textBox109.Name = "textBox109";
            this.textBox109.Size = new System.Drawing.Size(80, 25);
            this.textBox109.TabIndex = 109;
            this.textBox109.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox110
            // 
            this.textBox110.BackColor = System.Drawing.SystemColors.Window;
            this.textBox110.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox110.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox110.Location = new System.Drawing.Point(3, 39);
            this.textBox110.Name = "textBox110";
            this.textBox110.Size = new System.Drawing.Size(30, 25);
            this.textBox110.TabIndex = 0;
            this.textBox110.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label80
            // 
            this.label80.BackColor = System.Drawing.SystemColors.Window;
            this.label80.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label80.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label80.Location = new System.Drawing.Point(3, 15);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(30, 25);
            this.label80.TabIndex = 86;
            this.label80.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label81
            // 
            this.label81.BackColor = System.Drawing.SystemColors.Window;
            this.label81.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label81.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label81.Location = new System.Drawing.Point(32, 15);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(40, 25);
            this.label81.TabIndex = 86;
            this.label81.Text = "序号";
            this.label81.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox111
            // 
            this.textBox111.BackColor = System.Drawing.SystemColors.Window;
            this.textBox111.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox111.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox111.Location = new System.Drawing.Point(3, 87);
            this.textBox111.Name = "textBox111";
            this.textBox111.Size = new System.Drawing.Size(30, 25);
            this.textBox111.TabIndex = 8;
            this.textBox111.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox112
            // 
            this.textBox112.BackColor = System.Drawing.SystemColors.Window;
            this.textBox112.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox112.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox112.Location = new System.Drawing.Point(32, 39);
            this.textBox112.Name = "textBox112";
            this.textBox112.Size = new System.Drawing.Size(40, 25);
            this.textBox112.TabIndex = 1;
            this.textBox112.Text = "1";
            this.textBox112.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox113
            // 
            this.textBox113.BackColor = System.Drawing.SystemColors.Window;
            this.textBox113.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox113.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox113.Location = new System.Drawing.Point(32, 87);
            this.textBox113.Name = "textBox113";
            this.textBox113.Size = new System.Drawing.Size(40, 25);
            this.textBox113.TabIndex = 9;
            this.textBox113.Text = "3";
            this.textBox113.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox114
            // 
            this.textBox114.BackColor = System.Drawing.SystemColors.Window;
            this.textBox114.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox114.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox114.Location = new System.Drawing.Point(3, 63);
            this.textBox114.Name = "textBox114";
            this.textBox114.Size = new System.Drawing.Size(30, 25);
            this.textBox114.TabIndex = 4;
            this.textBox114.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox115
            // 
            this.textBox115.BackColor = System.Drawing.SystemColors.Window;
            this.textBox115.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox115.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox115.Location = new System.Drawing.Point(32, 63);
            this.textBox115.Name = "textBox115";
            this.textBox115.Size = new System.Drawing.Size(40, 25);
            this.textBox115.TabIndex = 5;
            this.textBox115.Text = "2";
            this.textBox115.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // button18
            // 
            this.button18.Location = new System.Drawing.Point(87, 206);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(100, 30);
            this.button18.TabIndex = 90;
            this.button18.Text = "确定";
            this.button18.UseVisualStyleBackColor = true;
            // 
            // button19
            // 
            this.button19.Location = new System.Drawing.Point(240, 206);
            this.button19.Name = "button19";
            this.button19.Size = new System.Drawing.Size(100, 30);
            this.button19.TabIndex = 89;
            this.button19.Text = "取消";
            this.button19.UseVisualStyleBackColor = true;
            // 
            // button20
            // 
            this.button20.Location = new System.Drawing.Point(396, 206);
            this.button20.Name = "button20";
            this.button20.Size = new System.Drawing.Size(100, 30);
            this.button20.TabIndex = 88;
            this.button20.Text = "返回";
            this.button20.UseVisualStyleBackColor = true;
            // 
            // ysdel_bt
            // 
            this.ysdel_bt.Location = new System.Drawing.Point(804, 11);
            this.ysdel_bt.Name = "ysdel_bt";
            this.ysdel_bt.Size = new System.Drawing.Size(60, 30);
            this.ysdel_bt.TabIndex = 86;
            this.ysdel_bt.Text = "删除";
            this.ysdel_bt.UseVisualStyleBackColor = true;
            // 
            // ysquery_bt
            // 
            this.ysquery_bt.Location = new System.Drawing.Point(730, 11);
            this.ysquery_bt.Name = "ysquery_bt";
            this.ysquery_bt.Size = new System.Drawing.Size(60, 30);
            this.ysquery_bt.TabIndex = 85;
            this.ysquery_bt.Text = "查询";
            this.ysquery_bt.UseVisualStyleBackColor = true;
            // 
            // ysysph_tb
            // 
            this.ysysph_tb.Location = new System.Drawing.Point(554, 16);
            this.ysysph_tb.Name = "ysysph_tb";
            this.ysysph_tb.Size = new System.Drawing.Size(160, 21);
            this.ysysph_tb.TabIndex = 84;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("宋体", 9F);
            this.label10.Location = new System.Drawing.Point(494, 20);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(53, 12);
            this.label10.TabIndex = 83;
            this.label10.Text = "预算平衡";
            // 
            // yssqbm_tb
            // 
            this.yssqbm_tb.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.yssqbm_tb.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.yssqbm_tb.Location = new System.Drawing.Point(310, 16);
            this.yssqbm_tb.Name = "yssqbm_tb";
            this.yssqbm_tb.Size = new System.Drawing.Size(160, 20);
            this.yssqbm_tb.TabIndex = 82;
            // 
            // yswlbm_tb
            // 
            this.yswlbm_tb.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.yswlbm_tb.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.yswlbm_tb.Location = new System.Drawing.Point(65, 16);
            this.yswlbm_tb.Name = "yswlbm_tb";
            this.yswlbm_tb.Size = new System.Drawing.Size(160, 20);
            this.yswlbm_tb.TabIndex = 81;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("宋体", 9F);
            this.label13.Location = new System.Drawing.Point(250, 20);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(53, 12);
            this.label13.TabIndex = 80;
            this.label13.Text = "申请部门";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("宋体", 9F);
            this.label14.Location = new System.Drawing.Point(5, 20);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(53, 12);
            this.label14.TabIndex = 79;
            this.label14.Text = "物料编码";
            // 
            // sccgd_tp
            // 
            this.sccgd_tp.BackColor = System.Drawing.SystemColors.Control;
            this.sccgd_tp.Controls.Add(this.panel1);
            this.sccgd_tp.Controls.Add(this.panel4);
            this.sccgd_tp.Controls.Add(this.panel5);
            this.sccgd_tp.Controls.Add(this.panel2);
            this.sccgd_tp.Controls.Add(this.panel3);
            this.sccgd_tp.Controls.Add(this.cgddName);
            this.sccgd_tp.Location = new System.Drawing.Point(4, 22);
            this.sccgd_tp.Name = "sccgd_tp";
            this.sccgd_tp.Padding = new System.Windows.Forms.Padding(3);
            this.sccgd_tp.Size = new System.Drawing.Size(902, 779);
            this.sccgd_tp.TabIndex = 3;
            this.sccgd_tp.Text = "生成采购单";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.xybh_tb);
            this.panel1.Controls.Add(this.gysbh_tb);
            this.panel1.Controls.Add(this.label49);
            this.panel1.Controls.Add(this.label50);
            this.panel1.Location = new System.Drawing.Point(7, 404);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(655, 33);
            this.panel1.TabIndex = 165;
            // 
            // xybh_tb
            // 
            this.xybh_tb.BackColor = System.Drawing.SystemColors.Window;
            this.xybh_tb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xybh_tb.Location = new System.Drawing.Point(438, 5);
            this.xybh_tb.Name = "xybh_tb";
            this.xybh_tb.ReadOnly = true;
            this.xybh_tb.Size = new System.Drawing.Size(200, 23);
            this.xybh_tb.TabIndex = 178;
            // 
            // gysbh_tb
            // 
            this.gysbh_tb.BackColor = System.Drawing.SystemColors.Window;
            this.gysbh_tb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.gysbh_tb.Location = new System.Drawing.Point(88, 5);
            this.gysbh_tb.Name = "gysbh_tb";
            this.gysbh_tb.ReadOnly = true;
            this.gysbh_tb.Size = new System.Drawing.Size(200, 23);
            this.gysbh_tb.TabIndex = 177;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("宋体", 9F);
            this.label49.Location = new System.Drawing.Point(378, 10);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(53, 12);
            this.label49.TabIndex = 176;
            this.label49.Text = "协议编号";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("宋体", 9F);
            this.label50.Location = new System.Drawing.Point(16, 10);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(65, 12);
            this.label50.TabIndex = 175;
            this.label50.Text = "供应商编号";
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.cgksrq_dt);
            this.panel4.Controls.Add(this.cgjsrq_dt);
            this.panel4.Controls.Add(this.hxcgpz_cmb);
            this.panel4.Controls.Add(this.label40);
            this.panel4.Controls.Add(this.label38);
            this.panel4.Controls.Add(this.label39);
            this.panel4.Controls.Add(this.fpyz_cmb);
            this.panel4.Controls.Add(this.label37);
            this.panel4.Controls.Add(this.checkBox1);
            this.panel4.Controls.Add(this.comboBox16);
            this.panel4.Controls.Add(this.comboBox17);
            this.panel4.Controls.Add(this.label34);
            this.panel4.Controls.Add(this.textBox18);
            this.panel4.Controls.Add(this.label35);
            this.panel4.Controls.Add(this.label36);
            this.panel4.Controls.Add(this.cgy1_cmb);
            this.panel4.Controls.Add(this.cgzz1_cmb);
            this.panel4.Controls.Add(this.label31);
            this.panel4.Controls.Add(this.fpbl1_tb);
            this.panel4.Controls.Add(this.label32);
            this.panel4.Controls.Add(this.label33);
            this.panel4.Controls.Add(this.scdel_bt);
            this.panel4.Controls.Add(this.scadd_bt);
            this.panel4.Controls.Add(this.label30);
            this.panel4.Location = new System.Drawing.Point(1, 440);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(774, 242);
            this.panel4.TabIndex = 163;
            // 
            // cgksrq_dt
            // 
            this.cgksrq_dt.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.cgksrq_dt.Location = new System.Drawing.Point(110, 173);
            this.cgksrq_dt.Name = "cgksrq_dt";
            this.cgksrq_dt.Size = new System.Drawing.Size(160, 23);
            this.cgksrq_dt.TabIndex = 147;
            // 
            // cgjsrq_dt
            // 
            this.cgjsrq_dt.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.cgjsrq_dt.Location = new System.Drawing.Point(360, 173);
            this.cgjsrq_dt.Name = "cgjsrq_dt";
            this.cgjsrq_dt.Size = new System.Drawing.Size(160, 23);
            this.cgjsrq_dt.TabIndex = 146;
            // 
            // hxcgpz_cmb
            // 
            this.hxcgpz_cmb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.hxcgpz_cmb.FormattingEnabled = true;
            this.hxcgpz_cmb.Location = new System.Drawing.Point(109, 214);
            this.hxcgpz_cmb.Name = "hxcgpz_cmb";
            this.hxcgpz_cmb.Size = new System.Drawing.Size(160, 22);
            this.hxcgpz_cmb.TabIndex = 138;
            this.hxcgpz_cmb.Text = "采购订单";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("宋体", 9F);
            this.label40.Location = new System.Drawing.Point(26, 217);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(77, 12);
            this.label40.TabIndex = 137;
            this.label40.Text = "后续采购凭证";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("宋体", 9F);
            this.label38.Location = new System.Drawing.Point(277, 178);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(77, 12);
            this.label38.TabIndex = 134;
            this.label38.Text = "采购结束日期";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("宋体", 9F);
            this.label39.Location = new System.Drawing.Point(26, 179);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(77, 12);
            this.label39.TabIndex = 133;
            this.label39.Text = "采购开始日期";
            // 
            // fpyz_cmb
            // 
            this.fpyz_cmb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.fpyz_cmb.FormattingEnabled = true;
            this.fpyz_cmb.Location = new System.Drawing.Point(109, 137);
            this.fpyz_cmb.Name = "fpyz_cmb";
            this.fpyz_cmb.Size = new System.Drawing.Size(160, 22);
            this.fpyz_cmb.TabIndex = 132;
            this.fpyz_cmb.Text = "百分比";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("宋体", 9F);
            this.label37.Location = new System.Drawing.Point(50, 141);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(53, 12);
            this.label37.TabIndex = 131;
            this.label37.Text = "分配原则";
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(29, 103);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(15, 14);
            this.checkBox1.TabIndex = 130;
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // comboBox16
            // 
            this.comboBox16.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.comboBox16.FormattingEnabled = true;
            this.comboBox16.Location = new System.Drawing.Point(602, 99);
            this.comboBox16.Name = "comboBox16";
            this.comboBox16.Size = new System.Drawing.Size(160, 22);
            this.comboBox16.TabIndex = 129;
            // 
            // comboBox17
            // 
            this.comboBox17.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.comboBox17.FormattingEnabled = true;
            this.comboBox17.Location = new System.Drawing.Point(109, 99);
            this.comboBox17.Name = "comboBox17";
            this.comboBox17.Size = new System.Drawing.Size(160, 22);
            this.comboBox17.TabIndex = 128;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("宋体", 9F);
            this.label34.Location = new System.Drawing.Point(555, 103);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(41, 12);
            this.label34.TabIndex = 127;
            this.label34.Text = "采购员";
            // 
            // textBox18
            // 
            this.textBox18.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox18.Location = new System.Drawing.Point(360, 99);
            this.textBox18.Name = "textBox18";
            this.textBox18.Size = new System.Drawing.Size(160, 23);
            this.textBox18.TabIndex = 126;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("宋体", 9F);
            this.label35.Location = new System.Drawing.Point(300, 103);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(53, 12);
            this.label35.TabIndex = 125;
            this.label35.Text = "分配比例";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("宋体", 9F);
            this.label36.Location = new System.Drawing.Point(50, 103);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(53, 12);
            this.label36.TabIndex = 124;
            this.label36.Text = "采购组织";
            // 
            // cgy1_cmb
            // 
            this.cgy1_cmb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.cgy1_cmb.FormattingEnabled = true;
            this.cgy1_cmb.Location = new System.Drawing.Point(602, 60);
            this.cgy1_cmb.Name = "cgy1_cmb";
            this.cgy1_cmb.Size = new System.Drawing.Size(160, 22);
            this.cgy1_cmb.TabIndex = 123;
            this.cgy1_cmb.Text = "采购员A";
            // 
            // cgzz1_cmb
            // 
            this.cgzz1_cmb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.cgzz1_cmb.FormattingEnabled = true;
            this.cgzz1_cmb.Location = new System.Drawing.Point(109, 60);
            this.cgzz1_cmb.Name = "cgzz1_cmb";
            this.cgzz1_cmb.Size = new System.Drawing.Size(160, 22);
            this.cgzz1_cmb.TabIndex = 122;
            this.cgzz1_cmb.Text = "0008";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("宋体", 9F);
            this.label31.Location = new System.Drawing.Point(555, 64);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(41, 12);
            this.label31.TabIndex = 121;
            this.label31.Text = "采购员";
            // 
            // fpbl1_tb
            // 
            this.fpbl1_tb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.fpbl1_tb.Location = new System.Drawing.Point(360, 60);
            this.fpbl1_tb.Name = "fpbl1_tb";
            this.fpbl1_tb.Size = new System.Drawing.Size(160, 23);
            this.fpbl1_tb.TabIndex = 120;
            this.fpbl1_tb.Text = "100%";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("宋体", 9F);
            this.label32.Location = new System.Drawing.Point(300, 64);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(53, 12);
            this.label32.TabIndex = 119;
            this.label32.Text = "分配比例";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("宋体", 9F);
            this.label33.Location = new System.Drawing.Point(50, 64);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(53, 12);
            this.label33.TabIndex = 118;
            this.label33.Text = "采购组织";
            // 
            // scdel_bt
            // 
            this.scdel_bt.Location = new System.Drawing.Point(190, 7);
            this.scdel_bt.Name = "scdel_bt";
            this.scdel_bt.Size = new System.Drawing.Size(60, 30);
            this.scdel_bt.TabIndex = 117;
            this.scdel_bt.Text = "删除";
            this.scdel_bt.UseVisualStyleBackColor = true;
            // 
            // scadd_bt
            // 
            this.scadd_bt.Location = new System.Drawing.Point(110, 7);
            this.scadd_bt.Name = "scadd_bt";
            this.scadd_bt.Size = new System.Drawing.Size(60, 30);
            this.scadd_bt.TabIndex = 116;
            this.scadd_bt.Text = "添加";
            this.scadd_bt.UseVisualStyleBackColor = true;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label30.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label30.Location = new System.Drawing.Point(13, 16);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(83, 12);
            this.label30.TabIndex = 115;
            this.label30.Text = "账户分配组织";
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.sccancel_bt);
            this.panel5.Controls.Add(this.screturn_bt);
            this.panel5.Controls.Add(this.scsubmit_bt);
            this.panel5.Location = new System.Drawing.Point(25, 681);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(655, 60);
            this.panel5.TabIndex = 164;
            // 
            // sccancel_bt
            // 
            this.sccancel_bt.Location = new System.Drawing.Point(285, 10);
            this.sccancel_bt.Name = "sccancel_bt";
            this.sccancel_bt.Size = new System.Drawing.Size(100, 30);
            this.sccancel_bt.TabIndex = 49;
            this.sccancel_bt.Text = "取消";
            this.sccancel_bt.UseVisualStyleBackColor = true;
            // 
            // screturn_bt
            // 
            this.screturn_bt.Location = new System.Drawing.Point(490, 10);
            this.screturn_bt.Name = "screturn_bt";
            this.screturn_bt.Size = new System.Drawing.Size(100, 30);
            this.screturn_bt.TabIndex = 48;
            this.screturn_bt.Text = "返回";
            this.screturn_bt.UseVisualStyleBackColor = true;
            // 
            // scsubmit_bt
            // 
            this.scsubmit_bt.Location = new System.Drawing.Point(80, 10);
            this.scsubmit_bt.Name = "scsubmit_bt";
            this.scsubmit_bt.Size = new System.Drawing.Size(100, 30);
            this.scsubmit_bt.TabIndex = 47;
            this.scsubmit_bt.Text = "提交";
            this.scsubmit_bt.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.sclxdh_tb);
            this.panel2.Controls.Add(this.scsqr_tb);
            this.panel2.Controls.Add(this.scsqbm_tb);
            this.panel2.Controls.Add(this.scxqsl_tb);
            this.panel2.Controls.Add(this.sczxdhpl_tb);
            this.panel2.Controls.Add(this.scjldw_tb);
            this.panel2.Controls.Add(this.scckbh_tb);
            this.panel2.Controls.Add(this.label83);
            this.panel2.Controls.Add(this.scxqdh_tb);
            this.panel2.Controls.Add(this.scsqrq_dt);
            this.panel2.Controls.Add(this.scqtyq_rtb);
            this.panel2.Controls.Add(this.label15);
            this.panel2.Controls.Add(this.label16);
            this.panel2.Controls.Add(this.label17);
            this.panel2.Controls.Add(this.label18);
            this.panel2.Controls.Add(this.label19);
            this.panel2.Controls.Add(this.label20);
            this.panel2.Controls.Add(this.label21);
            this.panel2.Controls.Add(this.label22);
            this.panel2.Controls.Add(this.label23);
            this.panel2.Controls.Add(this.label24);
            this.panel2.Controls.Add(this.sccgdh_tb);
            this.panel2.Location = new System.Drawing.Point(7, 123);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(655, 280);
            this.panel2.TabIndex = 158;
            // 
            // sclxdh_tb
            // 
            this.sclxdh_tb.BackColor = System.Drawing.SystemColors.Window;
            this.sclxdh_tb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.sclxdh_tb.Location = new System.Drawing.Point(88, 166);
            this.sclxdh_tb.Name = "sclxdh_tb";
            this.sclxdh_tb.ReadOnly = true;
            this.sclxdh_tb.Size = new System.Drawing.Size(200, 23);
            this.sclxdh_tb.TabIndex = 179;
            // 
            // scsqr_tb
            // 
            this.scsqr_tb.BackColor = System.Drawing.SystemColors.Window;
            this.scsqr_tb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.scsqr_tb.Location = new System.Drawing.Point(438, 126);
            this.scsqr_tb.Name = "scsqr_tb";
            this.scsqr_tb.ReadOnly = true;
            this.scsqr_tb.Size = new System.Drawing.Size(200, 23);
            this.scsqr_tb.TabIndex = 178;
            // 
            // scsqbm_tb
            // 
            this.scsqbm_tb.BackColor = System.Drawing.SystemColors.Window;
            this.scsqbm_tb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.scsqbm_tb.Location = new System.Drawing.Point(88, 126);
            this.scsqbm_tb.Name = "scsqbm_tb";
            this.scsqbm_tb.ReadOnly = true;
            this.scsqbm_tb.Size = new System.Drawing.Size(200, 23);
            this.scsqbm_tb.TabIndex = 177;
            // 
            // scxqsl_tb
            // 
            this.scxqsl_tb.BackColor = System.Drawing.SystemColors.Window;
            this.scxqsl_tb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.scxqsl_tb.Location = new System.Drawing.Point(438, 86);
            this.scxqsl_tb.Name = "scxqsl_tb";
            this.scxqsl_tb.ReadOnly = true;
            this.scxqsl_tb.Size = new System.Drawing.Size(200, 23);
            this.scxqsl_tb.TabIndex = 176;
            // 
            // sczxdhpl_tb
            // 
            this.sczxdhpl_tb.BackColor = System.Drawing.SystemColors.Window;
            this.sczxdhpl_tb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.sczxdhpl_tb.Location = new System.Drawing.Point(88, 86);
            this.sczxdhpl_tb.Name = "sczxdhpl_tb";
            this.sczxdhpl_tb.ReadOnly = true;
            this.sczxdhpl_tb.Size = new System.Drawing.Size(200, 23);
            this.sczxdhpl_tb.TabIndex = 175;
            // 
            // scjldw_tb
            // 
            this.scjldw_tb.BackColor = System.Drawing.SystemColors.Window;
            this.scjldw_tb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.scjldw_tb.Location = new System.Drawing.Point(438, 46);
            this.scjldw_tb.Name = "scjldw_tb";
            this.scjldw_tb.ReadOnly = true;
            this.scjldw_tb.Size = new System.Drawing.Size(200, 23);
            this.scjldw_tb.TabIndex = 174;
            // 
            // scckbh_tb
            // 
            this.scckbh_tb.BackColor = System.Drawing.SystemColors.Window;
            this.scckbh_tb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.scckbh_tb.Location = new System.Drawing.Point(88, 46);
            this.scckbh_tb.Name = "scckbh_tb";
            this.scckbh_tb.ReadOnly = true;
            this.scckbh_tb.Size = new System.Drawing.Size(200, 23);
            this.scckbh_tb.TabIndex = 173;
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.Font = new System.Drawing.Font("宋体", 9F);
            this.label83.Location = new System.Drawing.Point(378, 11);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(53, 12);
            this.label83.TabIndex = 169;
            this.label83.Text = "需求单号";
            // 
            // scxqdh_tb
            // 
            this.scxqdh_tb.BackColor = System.Drawing.SystemColors.Window;
            this.scxqdh_tb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.scxqdh_tb.Location = new System.Drawing.Point(438, 7);
            this.scxqdh_tb.Name = "scxqdh_tb";
            this.scxqdh_tb.ReadOnly = true;
            this.scxqdh_tb.Size = new System.Drawing.Size(200, 23);
            this.scxqdh_tb.TabIndex = 168;
            // 
            // scsqrq_dt
            // 
            this.scsqrq_dt.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.scsqrq_dt.Location = new System.Drawing.Point(438, 167);
            this.scsqrq_dt.Name = "scsqrq_dt";
            this.scsqrq_dt.Size = new System.Drawing.Size(200, 23);
            this.scsqrq_dt.TabIndex = 167;
            // 
            // scqtyq_rtb
            // 
            this.scqtyq_rtb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.scqtyq_rtb.Location = new System.Drawing.Point(88, 208);
            this.scqtyq_rtb.Name = "scqtyq_rtb";
            this.scqtyq_rtb.ReadOnly = true;
            this.scqtyq_rtb.Size = new System.Drawing.Size(550, 60);
            this.scqtyq_rtb.TabIndex = 163;
            this.scqtyq_rtb.Text = "";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("宋体", 9F);
            this.label15.Location = new System.Drawing.Point(28, 211);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(53, 12);
            this.label15.TabIndex = 162;
            this.label15.Text = "其他要求";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("宋体", 9F);
            this.label16.Location = new System.Drawing.Point(378, 171);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(53, 12);
            this.label16.TabIndex = 161;
            this.label16.Text = "申请日期";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("宋体", 9F);
            this.label17.Location = new System.Drawing.Point(28, 171);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(53, 12);
            this.label17.TabIndex = 160;
            this.label17.Text = "联系电话";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("宋体", 9F);
            this.label18.Location = new System.Drawing.Point(390, 131);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(41, 12);
            this.label18.TabIndex = 158;
            this.label18.Text = "申请人";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("宋体", 9F);
            this.label19.Location = new System.Drawing.Point(28, 131);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(53, 12);
            this.label19.TabIndex = 156;
            this.label19.Text = "申请部门";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("宋体", 9F);
            this.label20.Location = new System.Drawing.Point(378, 91);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(53, 12);
            this.label20.TabIndex = 155;
            this.label20.Text = "需求数量";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("宋体", 9F);
            this.label21.Location = new System.Drawing.Point(4, 91);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(77, 12);
            this.label21.TabIndex = 154;
            this.label21.Text = "最小订货批量";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("宋体", 9F);
            this.label22.Location = new System.Drawing.Point(378, 51);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(53, 12);
            this.label22.TabIndex = 152;
            this.label22.Text = "计量单位";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("宋体", 9F);
            this.label23.Location = new System.Drawing.Point(28, 51);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(53, 12);
            this.label23.TabIndex = 150;
            this.label23.Text = "仓库编号";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("宋体", 9F);
            this.label24.Location = new System.Drawing.Point(28, 11);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(53, 12);
            this.label24.TabIndex = 149;
            this.label24.Text = "采购单号";
            // 
            // sccgdh_tb
            // 
            this.sccgdh_tb.BackColor = System.Drawing.SystemColors.Window;
            this.sccgdh_tb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.sccgdh_tb.Location = new System.Drawing.Point(88, 7);
            this.sccgdh_tb.Name = "sccgdh_tb";
            this.sccgdh_tb.ReadOnly = true;
            this.sccgdh_tb.Size = new System.Drawing.Size(200, 23);
            this.sccgdh_tb.TabIndex = 148;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.scwlfs_tb);
            this.panel3.Controls.Add(this.sccglx_tb);
            this.panel3.Controls.Add(this.scwlmc_tb);
            this.panel3.Controls.Add(this.scwlbh_tb);
            this.panel3.Controls.Add(this.label25);
            this.panel3.Controls.Add(this.label26);
            this.panel3.Controls.Add(this.label27);
            this.panel3.Controls.Add(this.label28);
            this.panel3.Location = new System.Drawing.Point(7, 44);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(655, 80);
            this.panel3.TabIndex = 157;
            // 
            // scwlfs_tb
            // 
            this.scwlfs_tb.BackColor = System.Drawing.SystemColors.Window;
            this.scwlfs_tb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.scwlfs_tb.Location = new System.Drawing.Point(438, 46);
            this.scwlfs_tb.Name = "scwlfs_tb";
            this.scwlfs_tb.ReadOnly = true;
            this.scwlfs_tb.Size = new System.Drawing.Size(200, 23);
            this.scwlfs_tb.TabIndex = 172;
            // 
            // sccglx_tb
            // 
            this.sccglx_tb.BackColor = System.Drawing.SystemColors.Window;
            this.sccglx_tb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.sccglx_tb.Location = new System.Drawing.Point(88, 46);
            this.sccglx_tb.Name = "sccglx_tb";
            this.sccglx_tb.ReadOnly = true;
            this.sccglx_tb.Size = new System.Drawing.Size(200, 23);
            this.sccglx_tb.TabIndex = 171;
            // 
            // scwlmc_tb
            // 
            this.scwlmc_tb.BackColor = System.Drawing.SystemColors.Window;
            this.scwlmc_tb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.scwlmc_tb.Location = new System.Drawing.Point(438, 6);
            this.scwlmc_tb.Name = "scwlmc_tb";
            this.scwlmc_tb.ReadOnly = true;
            this.scwlmc_tb.Size = new System.Drawing.Size(200, 23);
            this.scwlmc_tb.TabIndex = 170;
            // 
            // scwlbh_tb
            // 
            this.scwlbh_tb.BackColor = System.Drawing.SystemColors.Window;
            this.scwlbh_tb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.scwlbh_tb.Location = new System.Drawing.Point(88, 6);
            this.scwlbh_tb.Name = "scwlbh_tb";
            this.scwlbh_tb.ReadOnly = true;
            this.scwlbh_tb.Size = new System.Drawing.Size(200, 23);
            this.scwlbh_tb.TabIndex = 169;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("宋体", 9F);
            this.label25.Location = new System.Drawing.Point(378, 51);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(53, 12);
            this.label25.TabIndex = 31;
            this.label25.Text = "物流方式";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("宋体", 9F);
            this.label26.Location = new System.Drawing.Point(28, 51);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(53, 12);
            this.label26.TabIndex = 29;
            this.label26.Text = "采购类型";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("宋体", 9F);
            this.label27.Location = new System.Drawing.Point(378, 11);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(53, 12);
            this.label27.TabIndex = 27;
            this.label27.Text = "物料名称";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("宋体", 9F);
            this.label28.Location = new System.Drawing.Point(28, 11);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(53, 12);
            this.label28.TabIndex = 25;
            this.label28.Text = "物料编号";
            // 
            // cgddName
            // 
            this.cgddName.AutoSize = true;
            this.cgddName.Font = new System.Drawing.Font("宋体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.cgddName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.cgddName.Location = new System.Drawing.Point(35, 7);
            this.cgddName.Name = "cgddName";
            this.cgddName.Size = new System.Drawing.Size(133, 19);
            this.cgddName.TabIndex = 156;
            this.cgddName.Text = "****采购订单";
            // 
            // cgzt_cmb
            // 
            this.cgzt_cmb.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cgzt_cmb.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cgzt_cmb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.cgzt_cmb.FormattingEnabled = true;
            this.cgzt_cmb.ItemHeight = 14;
            this.cgzt_cmb.Location = new System.Drawing.Point(388, 80);
            this.cgzt_cmb.Name = "cgzt_cmb";
            this.cgzt_cmb.Size = new System.Drawing.Size(200, 22);
            this.cgzt_cmb.TabIndex = 83;
            this.cgzt_cmb.Text = "审核通过";
            // 
            // cglx_cmb
            // 
            this.cglx_cmb.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cglx_cmb.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cglx_cmb.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cglx_cmb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.cglx_cmb.FormattingEnabled = true;
            this.cglx_cmb.ItemHeight = 14;
            this.cglx_cmb.Items.AddRange(new object[] {
            "标准采购类型",
            "框架协议采购类型",
            "寄售采购类型",
            "委外加工类型",
            "STO（库存转移单）"});
            this.cglx_cmb.Location = new System.Drawing.Point(67, 81);
            this.cglx_cmb.Name = "cglx_cmb";
            this.cglx_cmb.Size = new System.Drawing.Size(200, 22);
            this.cglx_cmb.TabIndex = 82;
            // 
            // query_bt
            // 
            this.query_bt.Location = new System.Drawing.Point(651, 74);
            this.query_bt.Name = "query_bt";
            this.query_bt.Size = new System.Drawing.Size(85, 23);
            this.query_bt.TabIndex = 80;
            this.query_bt.Text = "查询";
            this.query_bt.UseVisualStyleBackColor = true;
            this.query_bt.Click += new System.EventHandler(this.query_bt_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("宋体", 9F);
            this.label11.Location = new System.Drawing.Point(307, 85);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(53, 12);
            this.label11.TabIndex = 78;
            this.label11.Text = "采购状态";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("宋体", 9F);
            this.label12.Location = new System.Drawing.Point(8, 85);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(53, 12);
            this.label12.TabIndex = 77;
            this.label12.Text = "采购类型";
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.SystemColors.Control;
            this.panel6.Controls.Add(this.label29);
            this.panel6.Controls.Add(this.label41);
            this.panel6.Controls.Add(this.label42);
            this.panel6.Controls.Add(this.label43);
            this.panel6.Controls.Add(this.label44);
            this.panel6.Controls.Add(this.label45);
            this.panel6.Controls.Add(this.label46);
            this.panel6.Controls.Add(this.label47);
            this.panel6.Controls.Add(this.label48);
            this.panel6.Controls.Add(this.close_pb);
            this.panel6.Controls.Add(this.create_pb);
            this.panel6.Controls.Add(this.examine_pb);
            this.panel6.Controls.Add(this.forecast_pb);
            this.panel6.Controls.Add(this.refresh_pb);
            this.panel6.Controls.Add(this.edit_pb);
            this.panel6.Controls.Add(this.delete_pb);
            this.panel6.Controls.Add(this.detail_pb);
            this.panel6.Controls.Add(this.new_pb);
            this.panel6.Location = new System.Drawing.Point(0, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(768, 66);
            this.panel6.TabIndex = 223;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("宋体", 9F);
            this.label29.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label29.Location = new System.Drawing.Point(295, 46);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(29, 12);
            this.label29.TabIndex = 102;
            this.label29.Text = "寻源";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("宋体", 9F);
            this.label41.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label41.Location = new System.Drawing.Point(256, 46);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(29, 12);
            this.label41.TabIndex = 101;
            this.label41.Text = "审核";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("宋体", 9F);
            this.label42.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label42.Location = new System.Drawing.Point(336, 46);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(29, 12);
            this.label42.TabIndex = 100;
            this.label42.Text = "关闭";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("宋体", 9F);
            this.label43.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label43.Location = new System.Drawing.Point(213, 46);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(29, 12);
            this.label43.TabIndex = 99;
            this.label43.Text = "预测";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("宋体", 9F);
            this.label44.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label44.Location = new System.Drawing.Point(172, 46);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(29, 12);
            this.label44.TabIndex = 98;
            this.label44.Text = "刷新";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("宋体", 9F);
            this.label45.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label45.Location = new System.Drawing.Point(133, 46);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(29, 12);
            this.label45.TabIndex = 97;
            this.label45.Text = "编辑";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("宋体", 9F);
            this.label46.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label46.Location = new System.Drawing.Point(90, 46);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(29, 12);
            this.label46.TabIndex = 96;
            this.label46.Text = "删除";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("宋体", 9F);
            this.label47.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label47.Location = new System.Drawing.Point(51, 46);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(29, 12);
            this.label47.TabIndex = 95;
            this.label47.Text = "明细";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("宋体", 9F);
            this.label48.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label48.Location = new System.Drawing.Point(8, 46);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(29, 12);
            this.label48.TabIndex = 94;
            this.label48.Text = "新建";
            // 
            // close_pb
            // 
            this.close_pb.Image = ((System.Drawing.Image)(resources.GetObject("close_pb.Image")));
            this.close_pb.Location = new System.Drawing.Point(330, 2);
            this.close_pb.Name = "close_pb";
            this.close_pb.Size = new System.Drawing.Size(40, 42);
            this.close_pb.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.close_pb.TabIndex = 93;
            this.close_pb.TabStop = false;
            this.close_pb.Click += new System.EventHandler(this.close_pb_Click);
            // 
            // create_pb
            // 
            this.create_pb.Image = ((System.Drawing.Image)(resources.GetObject("create_pb.Image")));
            this.create_pb.Location = new System.Drawing.Point(289, 2);
            this.create_pb.Name = "create_pb";
            this.create_pb.Size = new System.Drawing.Size(40, 42);
            this.create_pb.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.create_pb.TabIndex = 92;
            this.create_pb.TabStop = false;
            this.create_pb.Click += new System.EventHandler(this.create_pb_Click);
            // 
            // examine_pb
            // 
            this.examine_pb.Image = ((System.Drawing.Image)(resources.GetObject("examine_pb.Image")));
            this.examine_pb.Location = new System.Drawing.Point(248, 2);
            this.examine_pb.Name = "examine_pb";
            this.examine_pb.Size = new System.Drawing.Size(40, 42);
            this.examine_pb.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.examine_pb.TabIndex = 91;
            this.examine_pb.TabStop = false;
            this.examine_pb.Click += new System.EventHandler(this.examine_pb_Click);
            // 
            // forecast_pb
            // 
            this.forecast_pb.Image = ((System.Drawing.Image)(resources.GetObject("forecast_pb.Image")));
            this.forecast_pb.Location = new System.Drawing.Point(207, 2);
            this.forecast_pb.Name = "forecast_pb";
            this.forecast_pb.Size = new System.Drawing.Size(40, 42);
            this.forecast_pb.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.forecast_pb.TabIndex = 89;
            this.forecast_pb.TabStop = false;
            this.forecast_pb.Click += new System.EventHandler(this.forecast_pb_Click);
            // 
            // refresh_pb
            // 
            this.refresh_pb.Image = ((System.Drawing.Image)(resources.GetObject("refresh_pb.Image")));
            this.refresh_pb.Location = new System.Drawing.Point(166, 2);
            this.refresh_pb.Name = "refresh_pb";
            this.refresh_pb.Size = new System.Drawing.Size(40, 42);
            this.refresh_pb.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.refresh_pb.TabIndex = 88;
            this.refresh_pb.TabStop = false;
            this.refresh_pb.Click += new System.EventHandler(this.refresh_pb_Click);
            // 
            // edit_pb
            // 
            this.edit_pb.Image = ((System.Drawing.Image)(resources.GetObject("edit_pb.Image")));
            this.edit_pb.Location = new System.Drawing.Point(125, 2);
            this.edit_pb.Name = "edit_pb";
            this.edit_pb.Size = new System.Drawing.Size(40, 42);
            this.edit_pb.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.edit_pb.TabIndex = 87;
            this.edit_pb.TabStop = false;
            this.edit_pb.Click += new System.EventHandler(this.edit_pb_Click);
            // 
            // delete_pb
            // 
            this.delete_pb.Image = ((System.Drawing.Image)(resources.GetObject("delete_pb.Image")));
            this.delete_pb.Location = new System.Drawing.Point(84, 2);
            this.delete_pb.Name = "delete_pb";
            this.delete_pb.Size = new System.Drawing.Size(40, 42);
            this.delete_pb.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.delete_pb.TabIndex = 86;
            this.delete_pb.TabStop = false;
            this.delete_pb.Click += new System.EventHandler(this.delete_pb_Click);
            // 
            // detail_pb
            // 
            this.detail_pb.Image = ((System.Drawing.Image)(resources.GetObject("detail_pb.Image")));
            this.detail_pb.Location = new System.Drawing.Point(43, 2);
            this.detail_pb.Name = "detail_pb";
            this.detail_pb.Size = new System.Drawing.Size(40, 42);
            this.detail_pb.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.detail_pb.TabIndex = 85;
            this.detail_pb.TabStop = false;
            // 
            // new_pb
            // 
            this.new_pb.Image = ((System.Drawing.Image)(resources.GetObject("new_pb.Image")));
            this.new_pb.Location = new System.Drawing.Point(2, 2);
            this.new_pb.Name = "new_pb";
            this.new_pb.Size = new System.Drawing.Size(40, 42);
            this.new_pb.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.new_pb.TabIndex = 84;
            this.new_pb.TabStop = false;
            this.new_pb.Click += new System.EventHandler(this.new_pb_Click);
            // 
            // CreatePurchasing_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1100, 706);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.sjl_panel);
            this.Controls.Add(this.cgzt_cmb);
            this.Controls.Add(this.cglx_cmb);
            this.Controls.Add(this.query_bt);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label12);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "CreatePurchasing_Form";
            this.Text = "生成采购计划";
            this.sjl_panel.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.qdcgjg_tp.ResumeLayout(false);
            this.qdcgjg_tp.PerformLayout();
            this.tjlx_panel.ResumeLayout(false);
            this.tjlx_panel.PerformLayout();
            this.bjkc_tp.ResumeLayout(false);
            this.bjkc_tp.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.ysgk_tp.ResumeLayout(false);
            this.ysgk_tp.PerformLayout();
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.sccgd_tp.ResumeLayout(false);
            this.sccgd_tp.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.close_pb)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.create_pb)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.examine_pb)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.forecast_pb)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.refresh_pb)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.edit_pb)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.delete_pb)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.detail_pb)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.new_pb)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cgzt_cmb;
        private System.Windows.Forms.ComboBox cglx_cmb;
        private System.Windows.Forms.Button query_bt;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage qdcgjg_tp;
        private System.Windows.Forms.Button qdsure_bt;
        private System.Windows.Forms.Button qdcancel_bt;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TabPage bjkc_tp;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.Button button16;
        private System.Windows.Forms.Button button17;
        private System.Windows.Forms.Button bjdel_bt;
        private System.Windows.Forms.Button bjquery_bt;
        private System.Windows.Forms.TextBox bjcgsl_tb;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox bjckdm_tb;
        private System.Windows.Forms.ComboBox bjwlbm_tb;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TabPage ysgk_tp;
        private System.Windows.Forms.Button button18;
        private System.Windows.Forms.Button button19;
        private System.Windows.Forms.Button button20;
        private System.Windows.Forms.Button ysdel_bt;
        private System.Windows.Forms.Button ysquery_bt;
        private System.Windows.Forms.TextBox ysysph_tb;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox yssqbm_tb;
        private System.Windows.Forms.ComboBox yswlbm_tb;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TabPage sccgd_tp;
        private System.Windows.Forms.ComboBox qdwlbh_cmb;
        private System.Windows.Forms.ComboBox qdwlmc_cmb;
        private System.Windows.Forms.ComboBox qdcglx_cmb;
        private System.Windows.Forms.Button qddel_bt;
        private System.Windows.Forms.Button qdadd_bt;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DateTimePicker scsqrq_dt;
        private System.Windows.Forms.RichTextBox scqtyq_rtb;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox sccgdh_tb;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label cgddName;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Button sccancel_bt;
        private System.Windows.Forms.Button screturn_bt;
        private System.Windows.Forms.Button scsubmit_bt;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.DateTimePicker cgksrq_dt;
        private System.Windows.Forms.DateTimePicker cgjsrq_dt;
        private System.Windows.Forms.ComboBox hxcgpz_cmb;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.ComboBox fpyz_cmb;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.ComboBox comboBox16;
        private System.Windows.Forms.ComboBox comboBox17;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TextBox textBox18;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.ComboBox cgy1_cmb;
        private System.Windows.Forms.ComboBox cgzz1_cmb;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox fpbl1_tb;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Button scdel_bt;
        private System.Windows.Forms.Button scadd_bt;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Panel tjlx_panel;
        private System.Windows.Forms.Label tjlxlbl_7;
        private System.Windows.Forms.Label tjlxlbl_5;
        private System.Windows.Forms.Label tjlxlbl_3;
        private System.Windows.Forms.Label tjlxlbl_4;
        private System.Windows.Forms.Label tjlxlbl_6;
        private System.Windows.Forms.Label tjlxlbl_1;
        private System.Windows.Forms.Label tjlxlbl_2;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.TextBox textBox39;
        private System.Windows.Forms.TextBox textBox40;
        private System.Windows.Forms.TextBox textBox41;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.TextBox textBox32;
        private System.Windows.Forms.TextBox textBox34;
        private System.Windows.Forms.TextBox textBox38;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.TextBox textBox95;
        private System.Windows.Forms.TextBox textBox96;
        private System.Windows.Forms.TextBox textBox97;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.TextBox textBox59;
        private System.Windows.Forms.TextBox textBox60;
        private System.Windows.Forms.TextBox textBox61;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.TextBox textBox74;
        private System.Windows.Forms.TextBox textBox75;
        private System.Windows.Forms.TextBox textBox76;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.TextBox textBox77;
        private System.Windows.Forms.TextBox textBox78;
        private System.Windows.Forms.TextBox textBox79;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.TextBox textBox80;
        private System.Windows.Forms.TextBox textBox81;
        private System.Windows.Forms.TextBox textBox82;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.TextBox textBox83;
        private System.Windows.Forms.TextBox textBox84;
        private System.Windows.Forms.TextBox textBox85;
        private System.Windows.Forms.CheckBox checkBox5;
        private System.Windows.Forms.CheckBox checkBox8;
        private System.Windows.Forms.CheckBox checkBox10;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.TextBox textBox86;
        private System.Windows.Forms.TextBox textBox87;
        private System.Windows.Forms.TextBox textBox88;
        private System.Windows.Forms.TextBox textBox89;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.TextBox textBox90;
        private System.Windows.Forms.TextBox textBox91;
        private System.Windows.Forms.TextBox textBox92;
        private System.Windows.Forms.TextBox textBox93;
        private System.Windows.Forms.TextBox textBox94;
        private System.Windows.Forms.Panel sjl_panel;
        private System.Windows.Forms.Label lbl_5;
        private System.Windows.Forms.Label lbl_12;
        private System.Windows.Forms.Label lbl_11;
        private System.Windows.Forms.Label lbl_9;
        private System.Windows.Forms.Label lbl_10;
        private System.Windows.Forms.Label lbl_8;
        private System.Windows.Forms.Label lbl_7;
        private System.Windows.Forms.Label lbl_1;
        private System.Windows.Forms.Label lbl_2;
        private System.Windows.Forms.Label lbl_3;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Label label82;
        private System.Windows.Forms.TextBox textBox116;
        private System.Windows.Forms.TextBox textBox117;
        private System.Windows.Forms.TextBox textBox118;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.TextBox textBox104;
        private System.Windows.Forms.TextBox textBox105;
        private System.Windows.Forms.TextBox textBox106;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.TextBox textBox21;
        private System.Windows.Forms.TextBox textBox23;
        private System.Windows.Forms.TextBox textBox24;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.TextBox textBox25;
        private System.Windows.Forms.TextBox textBox26;
        private System.Windows.Forms.TextBox textBox27;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.TextBox textBox28;
        private System.Windows.Forms.TextBox textBox29;
        private System.Windows.Forms.TextBox textBox30;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.TextBox textBox31;
        private System.Windows.Forms.TextBox textBox45;
        private System.Windows.Forms.TextBox textBox46;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.TextBox textBox47;
        private System.Windows.Forms.TextBox textBox48;
        private System.Windows.Forms.TextBox textBox49;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.TextBox textBox98;
        private System.Windows.Forms.TextBox textBox99;
        private System.Windows.Forms.TextBox textBox100;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.TextBox textBox101;
        private System.Windows.Forms.TextBox textBox102;
        private System.Windows.Forms.TextBox textBox103;
        private System.Windows.Forms.CheckBox checkBox11;
        private System.Windows.Forms.CheckBox checkBox12;
        private System.Windows.Forms.CheckBox checkBox13;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.TextBox textBox107;
        private System.Windows.Forms.TextBox textBox108;
        private System.Windows.Forms.TextBox textBox109;
        private System.Windows.Forms.TextBox textBox110;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.Label label81;
        private System.Windows.Forms.TextBox textBox111;
        private System.Windows.Forms.TextBox textBox112;
        private System.Windows.Forms.TextBox textBox113;
        private System.Windows.Forms.TextBox textBox114;
        private System.Windows.Forms.TextBox textBox115;
        private System.Windows.Forms.Label label83;
        private System.Windows.Forms.TextBox scxqdh_tb;
        private System.Windows.Forms.Label lbl_6;
        private System.Windows.Forms.CheckBox fanxuan_cb;
        private System.Windows.Forms.TextBox scwlfs_tb;
        private System.Windows.Forms.TextBox sccglx_tb;
        private System.Windows.Forms.TextBox scwlmc_tb;
        private System.Windows.Forms.TextBox scwlbh_tb;
        private System.Windows.Forms.TextBox sclxdh_tb;
        private System.Windows.Forms.TextBox scsqr_tb;
        private System.Windows.Forms.TextBox scsqbm_tb;
        private System.Windows.Forms.TextBox scxqsl_tb;
        private System.Windows.Forms.TextBox sczxdhpl_tb;
        private System.Windows.Forms.TextBox scjldw_tb;
        private System.Windows.Forms.TextBox scckbh_tb;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.PictureBox close_pb;
        private System.Windows.Forms.PictureBox examine_pb;
        private System.Windows.Forms.PictureBox refresh_pb;
        private System.Windows.Forms.PictureBox edit_pb;
        private System.Windows.Forms.PictureBox delete_pb;
        private System.Windows.Forms.PictureBox detail_pb;
        private System.Windows.Forms.PictureBox new_pb;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.PictureBox create_pb;
        private System.Windows.Forms.PictureBox forecast_pb;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox xybh_tb;
        private System.Windows.Forms.TextBox gysbh_tb;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label51;
    }
}