﻿namespace MMClient.SourcingManage.ProcurementPlan
{
    partial class AuditSubmitFramework_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.wlfs_cmb = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cglx_cmb = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.wlmc_cmb = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.wlbh_cmb = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.wlmc2_cmb = new System.Windows.Forms.ComboBox();
            this.wlbh2_cmb = new System.Windows.Forms.ComboBox();
            this.label29 = new System.Windows.Forms.Label();
            this.qysj_dt = new System.Windows.Forms.DateTimePicker();
            this.label24 = new System.Windows.Forms.Label();
            this.xqdh_tb = new System.Windows.Forms.TextBox();
            this.xymc_tb = new System.Windows.Forms.TextBox();
            this.gysh_tb = new System.Windows.Forms.TextBox();
            this.xybh_tb = new System.Windows.Forms.TextBox();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.comboBox4 = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.gysmc_tb = new System.Windows.Forms.TextBox();
            this.gysbh_tb = new System.Windows.Forms.TextBox();
            this.wlgg_tb = new System.Windows.Forms.TextBox();
            this.qqjg_tb = new System.Windows.Forms.TextBox();
            this.wlbz_tb = new System.Windows.Forms.TextBox();
            this.wlph_tb = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.xysl_tb = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.xylx_cmb = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.cancel_bt = new System.Windows.Forms.Button();
            this.return_bt = new System.Windows.Forms.Button();
            this.submit_bt = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.tgrq_dt = new System.Windows.Forms.DateTimePicker();
            this.shzt_cmb = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.wlfs_cmb);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.cglx_cmb);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.wlmc_cmb);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.wlbh_cmb);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Location = new System.Drawing.Point(27, 75);
            this.panel1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(873, 100);
            this.panel1.TabIndex = 154;
            // 
            // wlfs_cmb
            // 
            this.wlfs_cmb.FormattingEnabled = true;
            this.wlfs_cmb.Location = new System.Drawing.Point(585, 14);
            this.wlfs_cmb.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.wlfs_cmb.Name = "wlfs_cmb";
            this.wlfs_cmb.Size = new System.Drawing.Size(265, 23);
            this.wlfs_cmb.TabIndex = 32;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("宋体", 9F);
            this.label6.Location = new System.Drawing.Point(507, 17);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(67, 15);
            this.label6.TabIndex = 31;
            this.label6.Text = "物流方式";
            // 
            // cglx_cmb
            // 
            this.cglx_cmb.FormattingEnabled = true;
            this.cglx_cmb.Location = new System.Drawing.Point(120, 17);
            this.cglx_cmb.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cglx_cmb.Name = "cglx_cmb";
            this.cglx_cmb.Size = new System.Drawing.Size(265, 23);
            this.cglx_cmb.TabIndex = 30;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("宋体", 9F);
            this.label7.Location = new System.Drawing.Point(39, 20);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(67, 15);
            this.label7.TabIndex = 29;
            this.label7.Text = "采购类型";
            // 
            // wlmc_cmb
            // 
            this.wlmc_cmb.FormattingEnabled = true;
            this.wlmc_cmb.Location = new System.Drawing.Point(587, 64);
            this.wlmc_cmb.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.wlmc_cmb.Name = "wlmc_cmb";
            this.wlmc_cmb.Size = new System.Drawing.Size(265, 23);
            this.wlmc_cmb.TabIndex = 28;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("宋体", 9F);
            this.label3.Location = new System.Drawing.Point(506, 67);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(67, 15);
            this.label3.TabIndex = 27;
            this.label3.Text = "物料名称";
            // 
            // wlbh_cmb
            // 
            this.wlbh_cmb.FormattingEnabled = true;
            this.wlbh_cmb.ItemHeight = 15;
            this.wlbh_cmb.Location = new System.Drawing.Point(119, 64);
            this.wlbh_cmb.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.wlbh_cmb.Name = "wlbh_cmb";
            this.wlbh_cmb.Size = new System.Drawing.Size(265, 23);
            this.wlbh_cmb.TabIndex = 26;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("宋体", 9F);
            this.label2.Location = new System.Drawing.Point(39, 67);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 15);
            this.label2.TabIndex = 25;
            this.label2.Text = "物料编号";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.wlmc2_cmb);
            this.panel2.Controls.Add(this.wlbh2_cmb);
            this.panel2.Controls.Add(this.label29);
            this.panel2.Controls.Add(this.qysj_dt);
            this.panel2.Controls.Add(this.label24);
            this.panel2.Controls.Add(this.xqdh_tb);
            this.panel2.Controls.Add(this.xymc_tb);
            this.panel2.Controls.Add(this.gysh_tb);
            this.panel2.Controls.Add(this.xybh_tb);
            this.panel2.Controls.Add(this.dateTimePicker1);
            this.panel2.Controls.Add(this.comboBox4);
            this.panel2.Controls.Add(this.label15);
            this.panel2.Controls.Add(this.label25);
            this.panel2.Controls.Add(this.gysmc_tb);
            this.panel2.Controls.Add(this.gysbh_tb);
            this.panel2.Controls.Add(this.wlgg_tb);
            this.panel2.Controls.Add(this.qqjg_tb);
            this.panel2.Controls.Add(this.wlbz_tb);
            this.panel2.Controls.Add(this.wlph_tb);
            this.panel2.Controls.Add(this.label17);
            this.panel2.Controls.Add(this.label18);
            this.panel2.Controls.Add(this.label19);
            this.panel2.Controls.Add(this.label20);
            this.panel2.Controls.Add(this.label21);
            this.panel2.Controls.Add(this.label22);
            this.panel2.Controls.Add(this.xysl_tb);
            this.panel2.Controls.Add(this.label16);
            this.panel2.Controls.Add(this.label11);
            this.panel2.Controls.Add(this.label12);
            this.panel2.Controls.Add(this.label13);
            this.panel2.Controls.Add(this.label14);
            this.panel2.Controls.Add(this.xylx_cmb);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Location = new System.Drawing.Point(27, 174);
            this.panel2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(873, 494);
            this.panel2.TabIndex = 155;
            // 
            // wlmc2_cmb
            // 
            this.wlmc2_cmb.FormattingEnabled = true;
            this.wlmc2_cmb.Location = new System.Drawing.Point(587, 272);
            this.wlmc2_cmb.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.wlmc2_cmb.Name = "wlmc2_cmb";
            this.wlmc2_cmb.Size = new System.Drawing.Size(265, 23);
            this.wlmc2_cmb.TabIndex = 158;
            // 
            // wlbh2_cmb
            // 
            this.wlbh2_cmb.FormattingEnabled = true;
            this.wlbh2_cmb.ItemHeight = 15;
            this.wlbh2_cmb.Location = new System.Drawing.Point(120, 272);
            this.wlbh2_cmb.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.wlbh2_cmb.Name = "wlbh2_cmb";
            this.wlbh2_cmb.Size = new System.Drawing.Size(265, 23);
            this.wlbh2_cmb.TabIndex = 156;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("宋体", 9F);
            this.label29.Location = new System.Drawing.Point(40, 278);
            this.label29.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(67, 15);
            this.label29.TabIndex = 155;
            this.label29.Text = "物料编号";
            // 
            // qysj_dt
            // 
            this.qysj_dt.Location = new System.Drawing.Point(120, 141);
            this.qysj_dt.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.qysj_dt.Name = "qysj_dt";
            this.qysj_dt.Size = new System.Drawing.Size(265, 25);
            this.qysj_dt.TabIndex = 154;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("宋体", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label24.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label24.Location = new System.Drawing.Point(-1, 421);
            this.label24.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(109, 19);
            this.label24.TabIndex = 153;
            this.label24.Text = "供应商信息";
            // 
            // xqdh_tb
            // 
            this.xqdh_tb.Location = new System.Drawing.Point(120, 40);
            this.xqdh_tb.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.xqdh_tb.Name = "xqdh_tb";
            this.xqdh_tb.Size = new System.Drawing.Size(732, 25);
            this.xqdh_tb.TabIndex = 152;
            // 
            // xymc_tb
            // 
            this.xymc_tb.Location = new System.Drawing.Point(587, 90);
            this.xymc_tb.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.xymc_tb.Name = "xymc_tb";
            this.xymc_tb.Size = new System.Drawing.Size(265, 25);
            this.xymc_tb.TabIndex = 151;
            // 
            // gysh_tb
            // 
            this.gysh_tb.Location = new System.Drawing.Point(120, 190);
            this.gysh_tb.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gysh_tb.Name = "gysh_tb";
            this.gysh_tb.Size = new System.Drawing.Size(265, 25);
            this.gysh_tb.TabIndex = 150;
            // 
            // xybh_tb
            // 
            this.xybh_tb.Location = new System.Drawing.Point(120, 90);
            this.xybh_tb.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.xybh_tb.Name = "xybh_tb";
            this.xybh_tb.Size = new System.Drawing.Size(265, 25);
            this.xybh_tb.TabIndex = 148;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(587, 521);
            this.dateTimePicker1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(265, 25);
            this.dateTimePicker1.TabIndex = 147;
            // 
            // comboBox4
            // 
            this.comboBox4.FormattingEnabled = true;
            this.comboBox4.Location = new System.Drawing.Point(120, 522);
            this.comboBox4.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.comboBox4.Name = "comboBox4";
            this.comboBox4.Size = new System.Drawing.Size(265, 23);
            this.comboBox4.TabIndex = 79;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("宋体", 9F);
            this.label15.Location = new System.Drawing.Point(507, 528);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(67, 15);
            this.label15.TabIndex = 77;
            this.label15.Text = "通过日期";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("宋体", 9F);
            this.label25.Location = new System.Drawing.Point(40, 528);
            this.label25.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(67, 15);
            this.label25.TabIndex = 76;
            this.label25.Text = "审核状态";
            // 
            // gysmc_tb
            // 
            this.gysmc_tb.Location = new System.Drawing.Point(587, 454);
            this.gysmc_tb.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gysmc_tb.Name = "gysmc_tb";
            this.gysmc_tb.Size = new System.Drawing.Size(265, 25);
            this.gysmc_tb.TabIndex = 71;
            // 
            // gysbh_tb
            // 
            this.gysbh_tb.Location = new System.Drawing.Point(120, 454);
            this.gysbh_tb.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gysbh_tb.Name = "gysbh_tb";
            this.gysbh_tb.Size = new System.Drawing.Size(265, 25);
            this.gysbh_tb.TabIndex = 70;
            // 
            // wlgg_tb
            // 
            this.wlgg_tb.Location = new System.Drawing.Point(587, 322);
            this.wlgg_tb.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.wlgg_tb.Name = "wlgg_tb";
            this.wlgg_tb.Size = new System.Drawing.Size(265, 25);
            this.wlgg_tb.TabIndex = 68;
            // 
            // qqjg_tb
            // 
            this.qqjg_tb.Location = new System.Drawing.Point(587, 372);
            this.qqjg_tb.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.qqjg_tb.Name = "qqjg_tb";
            this.qqjg_tb.Size = new System.Drawing.Size(265, 25);
            this.qqjg_tb.TabIndex = 67;
            // 
            // wlbz_tb
            // 
            this.wlbz_tb.Location = new System.Drawing.Point(120, 372);
            this.wlbz_tb.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.wlbz_tb.Name = "wlbz_tb";
            this.wlbz_tb.Size = new System.Drawing.Size(265, 25);
            this.wlbz_tb.TabIndex = 66;
            // 
            // wlph_tb
            // 
            this.wlph_tb.Location = new System.Drawing.Point(120, 322);
            this.wlph_tb.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.wlph_tb.Name = "wlph_tb";
            this.wlph_tb.Size = new System.Drawing.Size(265, 25);
            this.wlph_tb.TabIndex = 65;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("宋体", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label17.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label17.Location = new System.Drawing.Point(20, 240);
            this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(89, 19);
            this.label17.TabIndex = 62;
            this.label17.Text = "物料信息";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("宋体", 9F);
            this.label18.Location = new System.Drawing.Point(507, 378);
            this.label18.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(67, 15);
            this.label18.TabIndex = 61;
            this.label18.Text = "前期价格";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("宋体", 9F);
            this.label19.Location = new System.Drawing.Point(40, 378);
            this.label19.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(67, 15);
            this.label19.TabIndex = 60;
            this.label19.Text = "物料标准";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("宋体", 9F);
            this.label20.Location = new System.Drawing.Point(507, 328);
            this.label20.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(67, 15);
            this.label20.TabIndex = 58;
            this.label20.Text = "物料规格";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("宋体", 9F);
            this.label21.Location = new System.Drawing.Point(40, 328);
            this.label21.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(67, 15);
            this.label21.TabIndex = 57;
            this.label21.Text = "物料牌号";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("宋体", 9F);
            this.label22.Location = new System.Drawing.Point(507, 278);
            this.label22.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(67, 15);
            this.label22.TabIndex = 56;
            this.label22.Text = "物料名称";
            // 
            // xysl_tb
            // 
            this.xysl_tb.Location = new System.Drawing.Point(587, 191);
            this.xysl_tb.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.xysl_tb.Name = "xysl_tb";
            this.xysl_tb.Size = new System.Drawing.Size(265, 25);
            this.xysl_tb.TabIndex = 52;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("宋体", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label16.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label16.Location = new System.Drawing.Point(20, 9);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(89, 19);
            this.label16.TabIndex = 47;
            this.label16.Text = "协议信息";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("宋体", 9F);
            this.label11.Location = new System.Drawing.Point(491, 459);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(82, 15);
            this.label11.TabIndex = 39;
            this.label11.Text = "供应商名称";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("宋体", 9F);
            this.label12.Location = new System.Drawing.Point(24, 459);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(82, 15);
            this.label12.TabIndex = 37;
            this.label12.Text = "供应商编号";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("宋体", 9F);
            this.label13.Location = new System.Drawing.Point(507, 196);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(67, 15);
            this.label13.TabIndex = 35;
            this.label13.Text = "协议数量";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("宋体", 9F);
            this.label14.Location = new System.Drawing.Point(40, 196);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(67, 15);
            this.label14.TabIndex = 33;
            this.label14.Text = "供应商号";
            // 
            // xylx_cmb
            // 
            this.xylx_cmb.FormattingEnabled = true;
            this.xylx_cmb.Location = new System.Drawing.Point(587, 141);
            this.xylx_cmb.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.xylx_cmb.Name = "xylx_cmb";
            this.xylx_cmb.Size = new System.Drawing.Size(265, 23);
            this.xylx_cmb.TabIndex = 32;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("宋体", 9F);
            this.label4.Location = new System.Drawing.Point(507, 146);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(67, 15);
            this.label4.TabIndex = 31;
            this.label4.Text = "协议类型";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("宋体", 9F);
            this.label5.Location = new System.Drawing.Point(40, 146);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(67, 15);
            this.label5.TabIndex = 29;
            this.label5.Text = "签约时间";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("宋体", 9F);
            this.label8.Location = new System.Drawing.Point(507, 96);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(67, 15);
            this.label8.TabIndex = 27;
            this.label8.Text = "协议名称";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("宋体", 9F);
            this.label9.Location = new System.Drawing.Point(40, 96);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(67, 15);
            this.label9.TabIndex = 25;
            this.label9.Text = "协议编号";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("宋体", 9F);
            this.label10.Location = new System.Drawing.Point(40, 46);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(67, 15);
            this.label10.TabIndex = 20;
            this.label10.Text = "需求单号";
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.cancel_bt);
            this.panel4.Controls.Add(this.return_bt);
            this.panel4.Controls.Add(this.submit_bt);
            this.panel4.Location = new System.Drawing.Point(27, 728);
            this.panel4.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(873, 75);
            this.panel4.TabIndex = 158;
            // 
            // cancel_bt
            // 
            this.cancel_bt.Location = new System.Drawing.Point(383, 19);
            this.cancel_bt.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cancel_bt.Name = "cancel_bt";
            this.cancel_bt.Size = new System.Drawing.Size(133, 38);
            this.cancel_bt.TabIndex = 49;
            this.cancel_bt.Text = "取消";
            this.cancel_bt.UseVisualStyleBackColor = true;
            // 
            // return_bt
            // 
            this.return_bt.Location = new System.Drawing.Point(656, 19);
            this.return_bt.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.return_bt.Name = "return_bt";
            this.return_bt.Size = new System.Drawing.Size(133, 38);
            this.return_bt.TabIndex = 48;
            this.return_bt.Text = "返回";
            this.return_bt.UseVisualStyleBackColor = true;
            // 
            // submit_bt
            // 
            this.submit_bt.Location = new System.Drawing.Point(109, 19);
            this.submit_bt.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.submit_bt.Name = "submit_bt";
            this.submit_bt.Size = new System.Drawing.Size(133, 38);
            this.submit_bt.TabIndex = 47;
            this.submit_bt.Text = "提交";
            this.submit_bt.UseVisualStyleBackColor = true;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.tgrq_dt);
            this.panel3.Controls.Add(this.shzt_cmb);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Controls.Add(this.label26);
            this.panel3.Location = new System.Drawing.Point(27, 660);
            this.panel3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(873, 69);
            this.panel3.TabIndex = 157;
            // 
            // tgrq_dt
            // 
            this.tgrq_dt.Location = new System.Drawing.Point(585, 26);
            this.tgrq_dt.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tgrq_dt.Name = "tgrq_dt";
            this.tgrq_dt.Size = new System.Drawing.Size(265, 25);
            this.tgrq_dt.TabIndex = 148;
            // 
            // shzt_cmb
            // 
            this.shzt_cmb.FormattingEnabled = true;
            this.shzt_cmb.Location = new System.Drawing.Point(119, 26);
            this.shzt_cmb.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.shzt_cmb.Name = "shzt_cmb";
            this.shzt_cmb.Size = new System.Drawing.Size(265, 23);
            this.shzt_cmb.TabIndex = 61;
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("宋体", 9F);
            this.label1.Location = new System.Drawing.Point(505, 31);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(67, 15);
            this.label1.TabIndex = 58;
            this.label1.Text = "通过日期";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("宋体", 9F);
            this.label26.Location = new System.Drawing.Point(39, 31);
            this.label26.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(67, 15);
            this.label26.TabIndex = 57;
            this.label26.Text = "审核状态";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("宋体", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label27.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label27.Location = new System.Drawing.Point(351, 25);
            this.label27.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(240, 27);
            this.label27.TabIndex = 159;
            this.label27.Text = "****采购需求计划";
            // 
            // AuditSubmitFramework_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1103, 862);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "AuditSubmitFramework_Form";
            this.Text = "审核提交（框架协议）";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ComboBox wlfs_cmb;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cglx_cmb;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox wlmc_cmb;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox wlbh_cmb;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox xqdh_tb;
        private System.Windows.Forms.TextBox xymc_tb;
        private System.Windows.Forms.TextBox gysh_tb;
        private System.Windows.Forms.TextBox xybh_tb;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.ComboBox comboBox4;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox gysmc_tb;
        private System.Windows.Forms.TextBox gysbh_tb;
        private System.Windows.Forms.TextBox wlgg_tb;
        private System.Windows.Forms.TextBox qqjg_tb;
        private System.Windows.Forms.TextBox wlbz_tb;
        private System.Windows.Forms.TextBox wlph_tb;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox xysl_tb;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox xylx_cmb;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button cancel_bt;
        private System.Windows.Forms.Button return_bt;
        private System.Windows.Forms.Button submit_bt;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.DateTimePicker tgrq_dt;
        private System.Windows.Forms.ComboBox shzt_cmb;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.DateTimePicker qysj_dt;
        private System.Windows.Forms.ComboBox wlmc2_cmb;
        private System.Windows.Forms.ComboBox wlbh2_cmb;
        private System.Windows.Forms.Label label29;
    }
}