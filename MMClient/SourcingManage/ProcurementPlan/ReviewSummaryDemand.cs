﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Model.SourcingManage.ProcurementPlan;
using WeifenLuo.WinFormsUI.Docking;
using Lib.Bll.SourcingManage.ProcurementPlan;
using Lib.SqlServerDAL;
using Lib.Common.MMCException.IDAL;

namespace MMClient.SourcingManage.ProcurementPlan
{
    public partial class ReviewSummaryDemand :DockContent
    {
        private Summary_DemandBLL summaryDemandBll;
        private Demand_MaterialBLL demandMaterialBll;
      
        private Summary_Demand summaryDemand = new Summary_Demand();
        //Summary_Demand summaryDemand
        public ReviewSummaryDemand()
        {
            InitializeComponent(); 
            summaryDemandBll = new Summary_DemandBLL();
            demandMaterialBll = new Demand_MaterialBLL();
            filldgv_demand("","");
            filldetails();
        }

        /// <summary>
        /// 初始化数据
        /// </summary>
        private void initData(DataTable dt)
        {
            if(dt == null || dt.Rows.Count == 0)
            {
                return;
            }
            materialGridView.TopLeftHeaderCell.Value = "序号";
            txt_purchaseType.Text = summaryDemand.Purchase_Type = dt.Rows[0][0].ToString();
            txt_logistics.Text = summaryDemand.LogisticsMode = dt.Rows[0][1].ToString();
            txt_FactoryID.Text = summaryDemand.Factory_ID = dt.Rows[0][2].ToString();
            txt_demandId.Text = summaryDemand.Demand_ID = dt.Rows[0][3].ToString();
            txt_department.Text = summaryDemand.Department = dt.Rows[0][4].ToString();
            txt_proposer.Text = summaryDemand.Proposer_ID = dt.Rows[0][5].ToString();
            txt_phonenum.Text = summaryDemand.PhoneNum = dt.Rows[0][6].ToString();
            txt_applyTime.Text = summaryDemand.Create_Time.ToString(dt.Rows[0][7].ToString());
            fillMaterialGridView();
        }

        /// <summary>
        /// 根据选中的需求计划展示该需求计划对应的所有物料信息
        /// </summary>
        private void fillMaterialGridView()
        {
            materialGridView.Rows.Clear();
            List<Demand_Material> demands = demandMaterialBll.findDemandMaterials(summaryDemand.Demand_ID);
            if (demands.Count > materialGridView.Rows.Count)
            {
                materialGridView.Rows.Add(demands.Count - materialGridView.Rows.Count);
            }
            for (int i = 0; i < demands.Count; i++)
            {
                materialGridView.Rows[i].Cells["materialId"].Value = demands.ElementAt(i).Material_ID;
                materialGridView.Rows[i].Cells["materialName"].Value = demands.ElementAt(i).Material_Name;
                materialGridView.Rows[i].Cells["materialGroup"].Value = demands.ElementAt(i).Material_Group;
                materialGridView.Rows[i].Cells["applyNum"].Value = demands.ElementAt(i).Demand_Count;
                materialGridView.Rows[i].Cells["measurement"].Value = demands.ElementAt(i).Measurement;
                materialGridView.Rows[i].Cells["stockId"].Value = demands.ElementAt(i).Stock_ID;
                materialGridView.Rows[i].Cells["DeliveryStartTime"].Value = demands.ElementAt(i).DeliveryStartTime.ToString("yyyy-MM-dd");
                materialGridView.Rows[i].Cells["DeliveryEndTime"].Value = demands.ElementAt(i).DeliveryEndTime.ToString("yyyy-MM-dd");
            }
        }

        /// <summary>
        /// 关闭按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cancle_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// 审核按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void submit_Click(object sender, EventArgs e)
        {
            Summary_Demand demand = new Summary_Demand();
            demand.Demand_ID = summaryDemand.Demand_ID;
            demand.ReviewAdvice = rtb_reviewAdvice.Text.ToString();
            demand.Follow_Certificate = this.txtCertification.Text.ToString();
            if(this.txtPrice.Text == "")
            {
                MessageBox.Show("请确定采购价格");
                return;
            }
            try
            {
                double price = Convert.ToDouble(this.txtPrice.Text.ToString()); 
            }
            catch (Exception ex)
            {
                MessageBox.Show("输入不合法，采购价格只能是数字");
                return;
            }
            demand.Purchase_Price = this.txtPrice.Text.ToString();
            demand.flag = 0+"";
            if (cb_reviewResult.SelectedIndex == 0)
            {
                demand.State = "审核通过";
            }
            else
            {
                demand.State = "审核未通过";
            }
            int result = summaryDemandBll.reviewDemandState(demand);
            
            if (result > 0)
            {
                MessageBox.Show("审核成功");
            }
            else
            {
                MessageBox.Show("审核失败");
            }
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //
            string purchaseType = this.cbPurType.Text.ToString().Trim();
            string sendType = this.cbSendby.Text.ToString().Trim();
            filldgv_demand(purchaseType,sendType);
        }
        /// <summary>
        /// 填充TABLE
        /// </summary>
        private void filldgv_demand(string purchaseType,string sendType)
        {
            string sql = "";
            if (purchaseType == "" && sendType == "")
            {
                sql = "select  Demand_ID as  需求单号 , Purchase_Type as 采购类型 , LogisticsMode as 物流方式 from Summary_Demand  where State = '待审核'";
            }
            else
            {
                sql = "select  Demand_ID as  需求单号 , Purchase_Type as 采购类型 , LogisticsMode as 物流方式 from Summary_Demand  where State = '待审核' and Purchase_Type = '" + purchaseType + "' and LogisticsMode = '" + sendType + "'";
            }
            try
            {
                DataTable dt = DBHelper.ExecuteQueryDT(sql);
                this.dgv_demand.DataSource = dt;
                dt = null;
            }catch(DBException ex)
            {
                MessageBox.Show("数据加载失败");
                return;
            }
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            string sql = "select  Demand_ID as  需求单号 , Purchase_Type as 采购类型 , LogisticsMode as 物流方式 from Summary_Demand  where State = '待审核'";
            try
            {
                DataTable dt = DBHelper.ExecuteQueryDT(sql);
                this.dgv_demand.DataSource = dt;
                dt = null;
            }
            catch (DBException ex)
            {
                MessageBox.Show("数据加载失败");
                return;
            }
        }
        
        private void dgv_demand_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                string demandID = this.dgv_demand.CurrentRow.Cells["需求单号"].Value.ToString();
                string sql = "select  Purchase_Type , LogisticsMode,Factory_ID, Demand_ID ,Department, Proposer_ID , PhoneNum,Create_Time from Summary_Demand  where State = '待审核' AND Demand_ID='" + demandID + "' ";
                DataTable dt = DBHelper.ExecuteQueryDT(sql);
                initData(dt);
            }
            catch (DBException ex)
            {
                MessageBox.Show("数据加载失败");
            }
        }

        private void filldetails()
        {
            string demandID = this.dgv_demand.Rows[0].Cells["需求单号"].Value.ToString();
            string sql = "select  Purchase_Type , LogisticsMode,Factory_ID, Demand_ID ,Department, Proposer_ID , PhoneNum,Create_Time from Summary_Demand  where State = '待审核' AND Demand_ID='" + demandID + "' ";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            initData(dt);
        }

        private void dgv_demand_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
