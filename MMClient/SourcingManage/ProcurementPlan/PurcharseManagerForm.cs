﻿using Lib.Bll.SourcingManage.ProcurementPlan;
using Lib.Common.MMCException.IDAL;
using Lib.Model.SourcingManage.ProcurementPlan;
using Lib.SqlServerDAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;

namespace MMClient.SourcingManage.ProcurementPlan
{
    public partial class PurcharseManagerForm : DockContent
    {
        private Summary_DemandBLL summaryDemandBll;
        private Demand_MaterialBLL demandMaterialBll;

        private Summary_Demand summaryDemand;
        public PurcharseManagerForm()
        {
            InitializeComponent();
            summaryDemandBll = new Summary_DemandBLL();
            demandMaterialBll = new Demand_MaterialBLL();
            fillcbNumber();
        }

        private void fillcbNumber()
        {
            try
            {
                string sql = "SELECT DISTINCT Demand_ID  as id FROM Summary_Demand WHERE State = '待审核'";
                DataTable dt = DBHelper.ExecuteQueryDT(sql);
                this.cbNumber.DataSource = dt;
                this.cbNumber.DisplayMember = "id";
                this.cbNumber.ValueMember = "id";
              dt = null;
            }catch(DBException ex)
            {
                MessageBox.Show("数据加载失败，请稍后重试！！");
            }
           
        }

        /// <summary>
        /// 初始化数据
        /// </summary>
        private void initData()
        {
            if(summaryDemand ==null)
            {
                MessageBox.Show("未被初始化");
                return;
            }
            materialGridView.TopLeftHeaderCell.Value = "序号";
            txt_purchaseType.Text = summaryDemand.Purchase_Type;
            txt_logistics.Text = summaryDemand.LogisticsMode;
            txt_FactoryID.Text = summaryDemand.Factory_ID;
            txt_demandId.Text = summaryDemand.Demand_ID;
            txt_department.Text = summaryDemand.Department;
            txt_proposer.Text = summaryDemand.Proposer_ID;
            txt_phonenum.Text = summaryDemand.PhoneNum;
            txt_applyTime.Text = summaryDemand.Create_Time.ToString("yyyy-MM-dd hh:mm:ss");
            
        }
        /// <summary>
        /// 根据选中的需求计划展示该需求计划对应的所有物料信息
        /// </summary>
        private void fillMaterialGridView()
        {
            materialGridView.Rows.Clear();
            List < Demand_Material >   demands = null;
            try
            {
                 demands = demandMaterialBll.findDemandMaterials(summaryDemand.Demand_ID);
            }catch(DBException ex)
            {
                MessageBox.Show("数据查询失败");
                return;
            }
            if (demands.Count > materialGridView.Rows.Count)
            {
                materialGridView.Rows.Add(demands.Count - materialGridView.Rows.Count);
            }
            for (int i = 0; i < demands.Count; i++)
            {
                materialGridView.Rows[i].Cells["materialId"].Value = demands.ElementAt(i).Material_ID;
                materialGridView.Rows[i].Cells["materialName"].Value = demands.ElementAt(i).Material_Name;
                materialGridView.Rows[i].Cells["materialGroup"].Value = demands.ElementAt(i).Material_Group;
                materialGridView.Rows[i].Cells["applyNum"].Value = demands.ElementAt(i).Demand_Count;
                materialGridView.Rows[i].Cells["measurement"].Value = demands.ElementAt(i).Measurement;
                materialGridView.Rows[i].Cells["stockId"].Value = demands.ElementAt(i).Stock_ID;
                materialGridView.Rows[i].Cells["DeliveryStartTime"].Value = demands.ElementAt(i).DeliveryStartTime.ToString("yyyy-MM-dd");
                materialGridView.Rows[i].Cells["DeliveryEndTime"].Value = demands.ElementAt(i).DeliveryEndTime.ToString("yyyy-MM-dd");
            }
        }

        private void cancle_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void submit_Click(object sender, EventArgs e)
        {
            Summary_Demand demand = new Summary_Demand();
            demand.Demand_ID = summaryDemand.Demand_ID;
            demand.ReviewAdvice = rtb_reviewAdvice.Text.ToString();
            if (cb_reviewResult.SelectedIndex == 0)
            {
                demand.State = "审核通过";
            }
            else
            {
                demand.State = "审核未通过";
            }
            int result = summaryDemandBll.reviewDemandState(demand);

            if (result > 0)
            {
                MessageBox.Show("审核成功");
            }
            else
            {
                MessageBox.Show("审核失败");
            }
        }

        private void cbNumber_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.cbNumber.Text.ToString().Trim() == "") 
            {
                return;
            }
            try
            {
                summaryDemand = null;
                summaryDemand = summaryDemandBll.findSummaryDemandByDemandID(this.cbNumber.Text.ToString().Trim());
                initData();
                fillMaterialGridView();
            }
            catch(DBException ex)
            {
                MessageBox.Show("数据加载失败");
            }
        }

        private void PurcharseManagerForm_Load(object sender, EventArgs e)
        {
            cbNumber_SelectedIndexChanged(sender, e);
        }
    }
}
