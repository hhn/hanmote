﻿namespace MMClient.SourcingManage.ProcurementPlan
{
    partial class ReviewSummaryDemand
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.materialGridView = new System.Windows.Forms.DataGridView();
            this.materialId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.materialName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.materialGroup = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.applyNum = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.measurement = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.stockId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DeliveryStartTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DeliveryEndTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.purchaseTypeLabel = new System.Windows.Forms.Label();
            this.applyTime = new System.Windows.Forms.Label();
            this.txt_proposer = new System.Windows.Forms.TextBox();
            this.proposer = new System.Windows.Forms.Label();
            this.departmentLabel = new System.Windows.Forms.Label();
            this.txt_phonenum = new System.Windows.Forms.TextBox();
            this.phoneLabel = new System.Windows.Forms.Label();
            this.txt_demandId = new System.Windows.Forms.TextBox();
            this.demandIdLabel = new System.Windows.Forms.Label();
            this.submit = new System.Windows.Forms.Button();
            this.cancle = new System.Windows.Forms.Button();
            this.txt_logistics = new System.Windows.Forms.TextBox();
            this.txt_purchaseType = new System.Windows.Forms.TextBox();
            this.txt_department = new System.Windows.Forms.TextBox();
            this.txt_applyTime = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.rtb_reviewAdvice = new System.Windows.Forms.RichTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cb_reviewResult = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txt_FactoryID = new System.Windows.Forms.TextBox();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.txtCertification = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtPrice = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.详情 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btnReset = new System.Windows.Forms.Button();
            this.cbSendby = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.cbPurType = new System.Windows.Forms.ComboBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.dgv_demand = new System.Windows.Forms.DataGridView();
            this.需求单号 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.采购类型 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.物流方式 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.materialGridView)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.详情.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_demand)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.materialGridView);
            this.groupBox1.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.groupBox1.Location = new System.Drawing.Point(14, 124);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox1.Size = new System.Drawing.Size(932, 199);
            this.groupBox1.TabIndex = 53;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "物料信息";
            // 
            // materialGridView
            // 
            this.materialGridView.AllowUserToAddRows = false;
            this.materialGridView.BackgroundColor = System.Drawing.SystemColors.Control;
            this.materialGridView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.materialGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.materialGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.materialGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.materialId,
            this.materialName,
            this.materialGroup,
            this.applyNum,
            this.measurement,
            this.stockId,
            this.DeliveryStartTime,
            this.DeliveryEndTime});
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.materialGridView.DefaultCellStyle = dataGridViewCellStyle8;
            this.materialGridView.EnableHeadersVisualStyles = false;
            this.materialGridView.Location = new System.Drawing.Point(8, 18);
            this.materialGridView.Margin = new System.Windows.Forms.Padding(2);
            this.materialGridView.Name = "materialGridView";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.materialGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.materialGridView.RowTemplate.Height = 27;
            this.materialGridView.Size = new System.Drawing.Size(908, 162);
            this.materialGridView.TabIndex = 2;
            // 
            // materialId
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.materialId.DefaultCellStyle = dataGridViewCellStyle2;
            this.materialId.HeaderText = "物料编号";
            this.materialId.Name = "materialId";
            this.materialId.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.materialId.Width = 150;
            // 
            // materialName
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.materialName.DefaultCellStyle = dataGridViewCellStyle3;
            this.materialName.HeaderText = "物料名称";
            this.materialName.Name = "materialName";
            // 
            // materialGroup
            // 
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.materialGroup.DefaultCellStyle = dataGridViewCellStyle4;
            this.materialGroup.HeaderText = "物料组";
            this.materialGroup.Name = "materialGroup";
            // 
            // applyNum
            // 
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.applyNum.DefaultCellStyle = dataGridViewCellStyle5;
            this.applyNum.HeaderText = "申请数量";
            this.applyNum.Name = "applyNum";
            // 
            // measurement
            // 
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.measurement.DefaultCellStyle = dataGridViewCellStyle6;
            this.measurement.HeaderText = "单位";
            this.measurement.Name = "measurement";
            this.measurement.Width = 70;
            // 
            // stockId
            // 
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.stockId.DefaultCellStyle = dataGridViewCellStyle7;
            this.stockId.HeaderText = "仓库编号";
            this.stockId.Name = "stockId";
            // 
            // DeliveryStartTime
            // 
            this.DeliveryStartTime.HeaderText = "交货开始时间";
            this.DeliveryStartTime.Name = "DeliveryStartTime";
            this.DeliveryStartTime.Width = 120;
            // 
            // DeliveryEndTime
            // 
            this.DeliveryEndTime.HeaderText = "交货结束时间";
            this.DeliveryEndTime.Name = "DeliveryEndTime";
            this.DeliveryEndTime.Width = 120;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(244, 20);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 50;
            this.label1.Text = "物流方式";
            // 
            // purchaseTypeLabel
            // 
            this.purchaseTypeLabel.AutoSize = true;
            this.purchaseTypeLabel.Location = new System.Drawing.Point(21, 17);
            this.purchaseTypeLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.purchaseTypeLabel.Name = "purchaseTypeLabel";
            this.purchaseTypeLabel.Size = new System.Drawing.Size(53, 12);
            this.purchaseTypeLabel.TabIndex = 48;
            this.purchaseTypeLabel.Text = "采购类型";
            // 
            // applyTime
            // 
            this.applyTime.AutoSize = true;
            this.applyTime.Location = new System.Drawing.Point(505, 89);
            this.applyTime.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.applyTime.Name = "applyTime";
            this.applyTime.Size = new System.Drawing.Size(53, 12);
            this.applyTime.TabIndex = 47;
            this.applyTime.Text = "申请日期";
            // 
            // txt_proposer
            // 
            this.txt_proposer.Location = new System.Drawing.Point(227, 86);
            this.txt_proposer.Margin = new System.Windows.Forms.Padding(2);
            this.txt_proposer.Name = "txt_proposer";
            this.txt_proposer.ReadOnly = true;
            this.txt_proposer.Size = new System.Drawing.Size(64, 21);
            this.txt_proposer.TabIndex = 45;
            // 
            // proposer
            // 
            this.proposer.AutoSize = true;
            this.proposer.Location = new System.Drawing.Point(183, 90);
            this.proposer.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.proposer.Name = "proposer";
            this.proposer.Size = new System.Drawing.Size(41, 12);
            this.proposer.TabIndex = 44;
            this.proposer.Text = "申请人";
            // 
            // departmentLabel
            // 
            this.departmentLabel.AutoSize = true;
            this.departmentLabel.Location = new System.Drawing.Point(21, 90);
            this.departmentLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.departmentLabel.Name = "departmentLabel";
            this.departmentLabel.Size = new System.Drawing.Size(53, 12);
            this.departmentLabel.TabIndex = 43;
            this.departmentLabel.Text = "申请部门";
            // 
            // txt_phonenum
            // 
            this.txt_phonenum.Location = new System.Drawing.Point(367, 86);
            this.txt_phonenum.Margin = new System.Windows.Forms.Padding(2);
            this.txt_phonenum.Name = "txt_phonenum";
            this.txt_phonenum.ReadOnly = true;
            this.txt_phonenum.Size = new System.Drawing.Size(115, 21);
            this.txt_phonenum.TabIndex = 42;
            // 
            // phoneLabel
            // 
            this.phoneLabel.AutoSize = true;
            this.phoneLabel.Location = new System.Drawing.Point(312, 90);
            this.phoneLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.phoneLabel.Name = "phoneLabel";
            this.phoneLabel.Size = new System.Drawing.Size(53, 12);
            this.phoneLabel.TabIndex = 41;
            this.phoneLabel.Text = "联系电话";
            // 
            // txt_demandId
            // 
            this.txt_demandId.Location = new System.Drawing.Point(77, 50);
            this.txt_demandId.Margin = new System.Windows.Forms.Padding(2);
            this.txt_demandId.Name = "txt_demandId";
            this.txt_demandId.ReadOnly = true;
            this.txt_demandId.Size = new System.Drawing.Size(147, 21);
            this.txt_demandId.TabIndex = 40;
            // 
            // demandIdLabel
            // 
            this.demandIdLabel.AutoSize = true;
            this.demandIdLabel.Location = new System.Drawing.Point(22, 52);
            this.demandIdLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.demandIdLabel.Name = "demandIdLabel";
            this.demandIdLabel.Size = new System.Drawing.Size(53, 12);
            this.demandIdLabel.TabIndex = 39;
            this.demandIdLabel.Text = "申请单号";
            // 
            // submit
            // 
            this.submit.Location = new System.Drawing.Point(784, 32);
            this.submit.Name = "submit";
            this.submit.Size = new System.Drawing.Size(66, 29);
            this.submit.TabIndex = 54;
            this.submit.Text = "提交";
            this.submit.UseVisualStyleBackColor = true;
            this.submit.Click += new System.EventHandler(this.submit_Click);
            // 
            // cancle
            // 
            this.cancle.Location = new System.Drawing.Point(874, 32);
            this.cancle.Name = "cancle";
            this.cancle.Size = new System.Drawing.Size(66, 29);
            this.cancle.TabIndex = 55;
            this.cancle.Text = "关闭";
            this.cancle.UseVisualStyleBackColor = true;
            this.cancle.Click += new System.EventHandler(this.cancle_Click);
            // 
            // txt_logistics
            // 
            this.txt_logistics.Location = new System.Drawing.Point(302, 17);
            this.txt_logistics.Name = "txt_logistics";
            this.txt_logistics.ReadOnly = true;
            this.txt_logistics.Size = new System.Drawing.Size(125, 21);
            this.txt_logistics.TabIndex = 56;
            // 
            // txt_purchaseType
            // 
            this.txt_purchaseType.Location = new System.Drawing.Point(79, 18);
            this.txt_purchaseType.Name = "txt_purchaseType";
            this.txt_purchaseType.ReadOnly = true;
            this.txt_purchaseType.Size = new System.Drawing.Size(126, 21);
            this.txt_purchaseType.TabIndex = 57;
            // 
            // txt_department
            // 
            this.txt_department.Location = new System.Drawing.Point(80, 86);
            this.txt_department.Name = "txt_department";
            this.txt_department.ReadOnly = true;
            this.txt_department.Size = new System.Drawing.Size(87, 21);
            this.txt_department.TabIndex = 58;
            // 
            // txt_applyTime
            // 
            this.txt_applyTime.Location = new System.Drawing.Point(563, 85);
            this.txt_applyTime.Name = "txt_applyTime";
            this.txt_applyTime.ReadOnly = true;
            this.txt_applyTime.Size = new System.Drawing.Size(147, 21);
            this.txt_applyTime.TabIndex = 59;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.rtb_reviewAdvice);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.cb_reviewResult);
            this.groupBox2.Location = new System.Drawing.Point(16, 647);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(949, 115);
            this.groupBox2.TabIndex = 60;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "审核意见";
            // 
            // rtb_reviewAdvice
            // 
            this.rtb_reviewAdvice.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rtb_reviewAdvice.Location = new System.Drawing.Point(14, 20);
            this.rtb_reviewAdvice.Name = "rtb_reviewAdvice";
            this.rtb_reviewAdvice.Size = new System.Drawing.Size(929, 64);
            this.rtb_reviewAdvice.TabIndex = 0;
            this.rtb_reviewAdvice.Text = "";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 96);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 61;
            this.label2.Text = "审核结果";
            // 
            // cb_reviewResult
            // 
            this.cb_reviewResult.FormattingEnabled = true;
            this.cb_reviewResult.Items.AddRange(new object[] {
            "通过",
            "不通过"});
            this.cb_reviewResult.Location = new System.Drawing.Point(77, 93);
            this.cb_reviewResult.Name = "cb_reviewResult";
            this.cb_reviewResult.Size = new System.Drawing.Size(121, 20);
            this.cb_reviewResult.TabIndex = 62;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(456, 20);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 12);
            this.label3.TabIndex = 63;
            this.label3.Text = "工厂编号";
            // 
            // txt_FactoryID
            // 
            this.txt_FactoryID.Location = new System.Drawing.Point(516, 17);
            this.txt_FactoryID.Name = "txt_FactoryID";
            this.txt_FactoryID.ReadOnly = true;
            this.txt_FactoryID.Size = new System.Drawing.Size(100, 21);
            this.txt_FactoryID.TabIndex = 64;
            // 
            // dataGridViewTextBoxColumn1
            // 
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle10;
            this.dataGridViewTextBoxColumn1.HeaderText = "物料编号";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn1.Width = 150;
            // 
            // dataGridViewTextBoxColumn2
            // 
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewTextBoxColumn2.DefaultCellStyle = dataGridViewCellStyle11;
            this.dataGridViewTextBoxColumn2.HeaderText = "物料名称";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn3
            // 
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewTextBoxColumn3.DefaultCellStyle = dataGridViewCellStyle12;
            this.dataGridViewTextBoxColumn3.HeaderText = "物料组";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // dataGridViewTextBoxColumn4
            // 
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewTextBoxColumn4.DefaultCellStyle = dataGridViewCellStyle13;
            this.dataGridViewTextBoxColumn4.HeaderText = "申请数量";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            // 
            // dataGridViewTextBoxColumn5
            // 
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewTextBoxColumn5.DefaultCellStyle = dataGridViewCellStyle14;
            this.dataGridViewTextBoxColumn5.HeaderText = "单位";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.Width = 70;
            // 
            // dataGridViewTextBoxColumn6
            // 
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewTextBoxColumn6.DefaultCellStyle = dataGridViewCellStyle15;
            this.dataGridViewTextBoxColumn6.HeaderText = "仓库编号";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.HeaderText = "交货开始时间";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.Width = 140;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.HeaderText = "交货结束时间";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.Width = 140;
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.Controls.Add(this.groupBox4);
            this.panel1.Controls.Add(this.详情);
            this.panel1.Controls.Add(this.groupBox3);
            this.panel1.Controls.Add(this.groupBox2);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1032, 851);
            this.panel1.TabIndex = 65;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.txtCertification);
            this.groupBox4.Controls.Add(this.label7);
            this.groupBox4.Controls.Add(this.txtPrice);
            this.groupBox4.Controls.Add(this.label6);
            this.groupBox4.Controls.Add(this.submit);
            this.groupBox4.Controls.Add(this.cancle);
            this.groupBox4.Location = new System.Drawing.Point(16, 768);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(949, 65);
            this.groupBox4.TabIndex = 67;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "确定采购价格及后续凭证";
            // 
            // txtCertification
            // 
            this.txtCertification.Location = new System.Drawing.Point(306, 32);
            this.txtCertification.Name = "txtCertification";
            this.txtCertification.Size = new System.Drawing.Size(121, 21);
            this.txtCertification.TabIndex = 59;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(225, 32);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 12);
            this.label7.TabIndex = 58;
            this.label7.Text = "后续凭证";
            // 
            // txtPrice
            // 
            this.txtPrice.Location = new System.Drawing.Point(77, 29);
            this.txtPrice.Name = "txtPrice";
            this.txtPrice.Size = new System.Drawing.Size(121, 21);
            this.txtPrice.TabIndex = 57;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 29);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 12);
            this.label6.TabIndex = 56;
            this.label6.Text = "采购价格";
            // 
            // 详情
            // 
            this.详情.Controls.Add(this.purchaseTypeLabel);
            this.详情.Controls.Add(this.groupBox1);
            this.详情.Controls.Add(this.label1);
            this.详情.Controls.Add(this.txt_FactoryID);
            this.详情.Controls.Add(this.txt_logistics);
            this.详情.Controls.Add(this.demandIdLabel);
            this.详情.Controls.Add(this.applyTime);
            this.详情.Controls.Add(this.label3);
            this.详情.Controls.Add(this.txt_purchaseType);
            this.详情.Controls.Add(this.txt_demandId);
            this.详情.Controls.Add(this.txt_proposer);
            this.详情.Controls.Add(this.txt_department);
            this.详情.Controls.Add(this.phoneLabel);
            this.详情.Controls.Add(this.proposer);
            this.详情.Controls.Add(this.txt_applyTime);
            this.详情.Controls.Add(this.txt_phonenum);
            this.详情.Controls.Add(this.departmentLabel);
            this.详情.Location = new System.Drawing.Point(16, 310);
            this.详情.Name = "详情";
            this.详情.Size = new System.Drawing.Size(949, 331);
            this.详情.TabIndex = 66;
            this.详情.TabStop = false;
            this.详情.Text = "信息详情";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btnReset);
            this.groupBox3.Controls.Add(this.cbSendby);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.cbPurType);
            this.groupBox3.Controls.Add(this.button1);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.dgv_demand);
            this.groupBox3.Location = new System.Drawing.Point(13, 9);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(949, 295);
            this.groupBox3.TabIndex = 65;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "待审核列表";
            // 
            // btnReset
            // 
            this.btnReset.Location = new System.Drawing.Point(667, 18);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(75, 23);
            this.btnReset.TabIndex = 13;
            this.btnReset.Text = "重  置";
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // cbSendby
            // 
            this.cbSendby.FormattingEnabled = true;
            this.cbSendby.Items.AddRange(new object[] {
            "物流中心式",
            "统谈统签直送式",
            "统谈分签式"});
            this.cbSendby.Location = new System.Drawing.Point(326, 20);
            this.cbSendby.Name = "cbSendby";
            this.cbSendby.Size = new System.Drawing.Size(157, 20);
            this.cbSendby.TabIndex = 12;
            this.cbSendby.Text = "物流中心式";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(267, 23);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 12);
            this.label5.TabIndex = 11;
            this.label5.Text = "物流方式";
            // 
            // cbPurType
            // 
            this.cbPurType.FormattingEnabled = true;
            this.cbPurType.Items.AddRange(new object[] {
            "标准采购类型",
            "框架协议采购类型",
            "委外加工采购类型",
            "寄售采购类型"});
            this.cbPurType.Location = new System.Drawing.Point(94, 20);
            this.cbPurType.Name = "cbPurType";
            this.cbPurType.Size = new System.Drawing.Size(121, 20);
            this.cbPurType.TabIndex = 10;
            this.cbPurType.Text = "标准采购类型";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(552, 18);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 9;
            this.button1.Text = "查  询";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 23);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 12);
            this.label4.TabIndex = 8;
            this.label4.Text = "采购类型";
            // 
            // dgv_demand
            // 
            this.dgv_demand.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgv_demand.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.dgv_demand.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgv_demand.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_demand.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.需求单号,
            this.采购类型,
            this.物流方式});
            this.dgv_demand.Location = new System.Drawing.Point(14, 50);
            this.dgv_demand.Name = "dgv_demand";
            this.dgv_demand.RowTemplate.Height = 23;
            this.dgv_demand.Size = new System.Drawing.Size(919, 239);
            this.dgv_demand.TabIndex = 6;
            this.dgv_demand.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_demand_CellContentClick);
            this.dgv_demand.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgv_demand_CellMouseClick);
            // 
            // 需求单号
            // 
            this.需求单号.DataPropertyName = "需求单号";
            this.需求单号.HeaderText = "单号";
            this.需求单号.Name = "需求单号";
            this.需求单号.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.需求单号.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.需求单号.Width = 300;
            // 
            // 采购类型
            // 
            this.采购类型.DataPropertyName = "采购类型";
            this.采购类型.HeaderText = "采购类型";
            this.采购类型.Name = "采购类型";
            this.采购类型.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.采购类型.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.采购类型.Width = 200;
            // 
            // 物流方式
            // 
            this.物流方式.DataPropertyName = "物流方式";
            this.物流方式.HeaderText = "物流方式";
            this.物流方式.Name = "物流方式";
            this.物流方式.Width = 200;
            // 
            // ReviewSummaryDemand
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1057, 854);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "ReviewSummaryDemand";
            this.Text = "审核";
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.materialGridView)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.详情.ResumeLayout(false);
            this.详情.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_demand)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView materialGridView;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label purchaseTypeLabel;
        private System.Windows.Forms.Label applyTime;
        private System.Windows.Forms.TextBox txt_proposer;
        private System.Windows.Forms.Label proposer;
        private System.Windows.Forms.Label departmentLabel;
        private System.Windows.Forms.TextBox txt_phonenum;
        private System.Windows.Forms.Label phoneLabel;
        private System.Windows.Forms.TextBox txt_demandId;
        private System.Windows.Forms.Label demandIdLabel;
        private System.Windows.Forms.Button submit;
        private System.Windows.Forms.Button cancle;
        private System.Windows.Forms.TextBox txt_logistics;
        private System.Windows.Forms.TextBox txt_purchaseType;
        private System.Windows.Forms.TextBox txt_department;
        private System.Windows.Forms.TextBox txt_applyTime;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RichTextBox rtb_reviewAdvice;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cb_reviewResult;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txt_FactoryID;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn materialId;
        private System.Windows.Forms.DataGridViewTextBoxColumn materialName;
        private System.Windows.Forms.DataGridViewTextBoxColumn materialGroup;
        private System.Windows.Forms.DataGridViewTextBoxColumn applyNum;
        private System.Windows.Forms.DataGridViewTextBoxColumn measurement;
        private System.Windows.Forms.DataGridViewTextBoxColumn stockId;
        private System.Windows.Forms.DataGridViewTextBoxColumn DeliveryStartTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn DeliveryEndTime;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox 详情;
        private System.Windows.Forms.DataGridView dgv_demand;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cbSendby;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cbPurType;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.DataGridViewTextBoxColumn 需求单号;
        private System.Windows.Forms.DataGridViewTextBoxColumn 采购类型;
        private System.Windows.Forms.DataGridViewTextBoxColumn 物流方式;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox txtCertification;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtPrice;
        private System.Windows.Forms.Label label6;
    }
}