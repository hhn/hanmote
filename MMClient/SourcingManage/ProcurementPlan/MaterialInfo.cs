﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Model.MD.MT;
using Lib.Bll.MDBll.MT;

namespace MMClient.SourcingManage.ProcurementPlan
{
    public partial class MaterialInfo : Form
    {
        private NewSummaryDemand standardmand;
        MaterialBLL materialBll = new MaterialBLL();

        /// <summary>
        /// 空构造函数
        /// </summary>
        public MaterialInfo():this(null){

        }
        public MaterialInfo(NewSummaryDemand standardmand)
        {
            InitializeComponent();
            this.standardmand = standardmand;
            dgv_materials.TopLeftHeaderCell.Value = "序号";
        }

        /// <summary>
        /// 搜索
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_Search_Click(object sender, EventArgs e)
        {
            string condition = "";
            if (!string.IsNullOrEmpty(this.txtMName.Text.ToString().Trim()))
            {
                condition += "where   Material_Name like '%" + this.txtMName.Text.ToString().Trim() + "%'";
            }
            if (!string.IsNullOrEmpty(this.TxtMType.Text.ToString().Trim()))
            {
                condition += " and Material_Group like '%" + this.TxtMType.Text.ToString().Trim() + "%' ";
            }
            dgv_materials.Rows.Clear();
            List<MaterialBase> materials = materialBll.GetMaterialBases(condition);
            if (materials != null)
            {
                int i = 0;
                foreach (MaterialBase m in materials)
                {
                    dgv_materials.Rows.Add(1);
                    dgv_materials.Rows[i].Cells["materialId"].Value = m.Material_ID;
                    dgv_materials.Rows[i].Cells["materialName"].Value = m.Material_Name;
                    dgv_materials.Rows[i].Cells["materialGroup"].Value = m.Material_Group;
                    dgv_materials.Rows[i].Cells["measurement"].Value = m.Measurement;
                    i++;
                }
            }
        }

        /// <summary>
        /// 画行号
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgv_materials_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            var dgv = sender as DataGridView;
            if (dgv != null)
            {
                Rectangle rect = new Rectangle(e.RowBounds.Location.X, e.RowBounds.Location.Y, (dgv.RowHeadersWidth + 12) / 2, e.RowBounds.Height);
                TextRenderer.DrawText(e.Graphics, (e.RowIndex + 1).ToString(), dgv.RowHeadersDefaultCellStyle.Font, rect, dgv.RowHeadersDefaultCellStyle.ForeColor, TextFormatFlags.VerticalCenter | TextFormatFlags.Right);
            }
        }

        /// <summary>
        /// 关闭按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_Cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// 确认按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_Confirm_Click(object sender, EventArgs e)
        {
            List<MaterialBase> materialList = new List<MaterialBase>();
            foreach (DataGridViewRow dgvr in dgv_materials.Rows)
            {
                if(dgvr.Selected){
                    MaterialBase mb = new MaterialBase();
                    mb.Material_ID = dgvr.Cells["materialId"].Value.ToString();
                    mb.Material_Name = dgvr.Cells["materialName"].Value.ToString();
                    mb.Material_Group = dgvr.Cells["materialGroup"].Value.ToString();
                    mb.Measurement = dgvr.Cells["measurement"].Value.ToString();
                    materialList.Add(mb);
                }   
            }       
            standardmand.fillMaterialBases(materialList);
            this.Close();
        }
        
        /// <summary>
        /// 鼠标进入单元格事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgv_materials_CellMouseEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
            {
                dgv_materials.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.LightBlue;
                DataGridViewCellStyle style = new DataGridViewCellStyle();
                style.BackColor = Color.CornflowerBlue;
                dgv_materials.Rows[e.RowIndex].HeaderCell.Style = style;
            }
        }

        /// <summary>
        /// 鼠标离开单元格事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgv_materials_CellMouseLeave(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
            {
                dgv_materials.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.White;
                DataGridViewCellStyle style = new DataGridViewCellStyle();
                style.BackColor = Color.WhiteSmoke;
                dgv_materials.Rows[e.RowIndex].HeaderCell.Style = style;
            }
        }

        private void btn_Confirm_Click_1(object sender, EventArgs e)
        {
            btn_Confirm_Click(sender, e);
        }

        private void btn_Cancel_Click_1(object sender, EventArgs e)
        {
            btn_Cancel_Click(sender, e);
        }

        private void btn_Search_Click_1(object sender, EventArgs e)
        {
            btn_Search_Click(sender, e);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.txtMName.Text = "";
            this.TxtMType.Text = "";
            btn_Search_Click(sender, e);
        }

        private void MaterialInfo_Load(object sender, EventArgs e)
        {
            btn_Search_Click(sender, e);
        }
    }
}
