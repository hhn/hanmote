﻿namespace MMClient.SourcingManage.ProcurementPlan
{
    partial class DetailSummaryDemand
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            this.txt_purchaseType = new System.Windows.Forms.TextBox();
            this.txt_logistics = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.purchaseTypeLabel = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.rtb_reviewAdvice = new System.Windows.Forms.RichTextBox();
            this.txt_applyTime = new System.Windows.Forms.TextBox();
            this.txt_department = new System.Windows.Forms.TextBox();
            this.txt_purchase = new System.Windows.Forms.TextBox();
            this.txt_logistic = new System.Windows.Forms.TextBox();
            this.cancle = new System.Windows.Forms.Button();
            this.btn_edit = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.materialGridView = new System.Windows.Forms.DataGridView();
            this.materialId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.materialName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.materialGroup = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.applyNum = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.measurement = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.stockId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.applyTime = new System.Windows.Forms.Label();
            this.txt_proposer = new System.Windows.Forms.TextBox();
            this.proposer = new System.Windows.Forms.Label();
            this.departmentLabel = new System.Windows.Forms.Label();
            this.txt_phonenum = new System.Windows.Forms.TextBox();
            this.phoneLabel = new System.Windows.Forms.Label();
            this.txt_demandId = new System.Windows.Forms.TextBox();
            this.demandIdLabel = new System.Windows.Forms.Label();
            this.txt_reviewResult = new System.Windows.Forms.TextBox();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label5 = new System.Windows.Forms.Label();
            this.txt_factoryID = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtPurPrice = new System.Windows.Forms.TextBox();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.materialGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // txt_purchaseType
            // 
            this.txt_purchaseType.Location = new System.Drawing.Point(159, -23);
            this.txt_purchaseType.Name = "txt_purchaseType";
            this.txt_purchaseType.ReadOnly = true;
            this.txt_purchaseType.Size = new System.Drawing.Size(126, 21);
            this.txt_purchaseType.TabIndex = 77;
            // 
            // txt_logistics
            // 
            this.txt_logistics.Location = new System.Drawing.Point(408, -23);
            this.txt_logistics.Name = "txt_logistics";
            this.txt_logistics.ReadOnly = true;
            this.txt_logistics.Size = new System.Drawing.Size(125, 21);
            this.txt_logistics.TabIndex = 76;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(350, -21);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 72;
            this.label1.Text = "物流方式";
            // 
            // purchaseTypeLabel
            // 
            this.purchaseTypeLabel.AutoSize = true;
            this.purchaseTypeLabel.Location = new System.Drawing.Point(101, -21);
            this.purchaseTypeLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.purchaseTypeLabel.Name = "purchaseTypeLabel";
            this.purchaseTypeLabel.Size = new System.Drawing.Size(53, 12);
            this.purchaseTypeLabel.TabIndex = 71;
            this.purchaseTypeLabel.Text = "采购类型";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(484, 471);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 96;
            this.label2.Text = "审核结果";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.rtb_reviewAdvice);
            this.groupBox2.Location = new System.Drawing.Point(11, 332);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(794, 100);
            this.groupBox2.TabIndex = 95;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "审核意见";
            // 
            // rtb_reviewAdvice
            // 
            this.rtb_reviewAdvice.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rtb_reviewAdvice.Location = new System.Drawing.Point(14, 20);
            this.rtb_reviewAdvice.Name = "rtb_reviewAdvice";
            this.rtb_reviewAdvice.ReadOnly = true;
            this.rtb_reviewAdvice.Size = new System.Drawing.Size(762, 74);
            this.rtb_reviewAdvice.TabIndex = 0;
            this.rtb_reviewAdvice.Text = "";
            // 
            // txt_applyTime
            // 
            this.txt_applyTime.Location = new System.Drawing.Point(510, 95);
            this.txt_applyTime.Name = "txt_applyTime";
            this.txt_applyTime.ReadOnly = true;
            this.txt_applyTime.Size = new System.Drawing.Size(147, 21);
            this.txt_applyTime.TabIndex = 94;
            // 
            // txt_department
            // 
            this.txt_department.Location = new System.Drawing.Point(76, 96);
            this.txt_department.Name = "txt_department";
            this.txt_department.ReadOnly = true;
            this.txt_department.Size = new System.Drawing.Size(87, 21);
            this.txt_department.TabIndex = 93;
            // 
            // txt_purchase
            // 
            this.txt_purchase.Location = new System.Drawing.Point(75, 13);
            this.txt_purchase.Name = "txt_purchase";
            this.txt_purchase.ReadOnly = true;
            this.txt_purchase.Size = new System.Drawing.Size(126, 21);
            this.txt_purchase.TabIndex = 92;
            // 
            // txt_logistic
            // 
            this.txt_logistic.Location = new System.Drawing.Point(324, 13);
            this.txt_logistic.Name = "txt_logistic";
            this.txt_logistic.ReadOnly = true;
            this.txt_logistic.Size = new System.Drawing.Size(125, 21);
            this.txt_logistic.TabIndex = 91;
            // 
            // cancle
            // 
            this.cancle.Location = new System.Drawing.Point(739, 463);
            this.cancle.Name = "cancle";
            this.cancle.Size = new System.Drawing.Size(66, 29);
            this.cancle.TabIndex = 90;
            this.cancle.Text = "关闭";
            this.cancle.UseVisualStyleBackColor = true;
            this.cancle.Click += new System.EventHandler(this.cancle_Click);
            // 
            // btn_edit
            // 
            this.btn_edit.Location = new System.Drawing.Point(667, 463);
            this.btn_edit.Name = "btn_edit";
            this.btn_edit.Size = new System.Drawing.Size(66, 29);
            this.btn_edit.TabIndex = 89;
            this.btn_edit.Text = "修改";
            this.btn_edit.UseVisualStyleBackColor = true;
            this.btn_edit.Visible = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.materialGridView);
            this.groupBox1.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.groupBox1.Location = new System.Drawing.Point(11, 128);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox1.Size = new System.Drawing.Size(794, 199);
            this.groupBox1.TabIndex = 88;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "物料信息";
            // 
            // materialGridView
            // 
            this.materialGridView.AllowUserToAddRows = false;
            this.materialGridView.BackgroundColor = System.Drawing.SystemColors.Control;
            this.materialGridView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.materialGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.materialGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.materialGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.materialId,
            this.materialName,
            this.materialGroup,
            this.applyNum,
            this.measurement,
            this.stockId});
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.materialGridView.DefaultCellStyle = dataGridViewCellStyle8;
            this.materialGridView.EnableHeadersVisualStyles = false;
            this.materialGridView.Location = new System.Drawing.Point(14, 19);
            this.materialGridView.Margin = new System.Windows.Forms.Padding(2);
            this.materialGridView.Name = "materialGridView";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.materialGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.materialGridView.RowTemplate.Height = 27;
            this.materialGridView.Size = new System.Drawing.Size(762, 162);
            this.materialGridView.TabIndex = 2;
            // 
            // materialId
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.materialId.DefaultCellStyle = dataGridViewCellStyle2;
            this.materialId.HeaderText = "物料编号";
            this.materialId.Name = "materialId";
            this.materialId.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.materialId.Width = 150;
            // 
            // materialName
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.materialName.DefaultCellStyle = dataGridViewCellStyle3;
            this.materialName.HeaderText = "物料名称";
            this.materialName.Name = "materialName";
            this.materialName.Width = 150;
            // 
            // materialGroup
            // 
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.materialGroup.DefaultCellStyle = dataGridViewCellStyle4;
            this.materialGroup.HeaderText = "物料组";
            this.materialGroup.Name = "materialGroup";
            // 
            // applyNum
            // 
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.applyNum.DefaultCellStyle = dataGridViewCellStyle5;
            this.applyNum.HeaderText = "申请数量";
            this.applyNum.Name = "applyNum";
            // 
            // measurement
            // 
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.measurement.DefaultCellStyle = dataGridViewCellStyle6;
            this.measurement.HeaderText = "单位";
            this.measurement.Name = "measurement";
            this.measurement.Width = 70;
            // 
            // stockId
            // 
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.stockId.DefaultCellStyle = dataGridViewCellStyle7;
            this.stockId.HeaderText = "仓库编号";
            this.stockId.Name = "stockId";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(266, 15);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 12);
            this.label3.TabIndex = 87;
            this.label3.Text = "物流方式";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(17, 15);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 12);
            this.label4.TabIndex = 86;
            this.label4.Text = "采购类型";
            // 
            // applyTime
            // 
            this.applyTime.AutoSize = true;
            this.applyTime.Location = new System.Drawing.Point(452, 98);
            this.applyTime.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.applyTime.Name = "applyTime";
            this.applyTime.Size = new System.Drawing.Size(53, 12);
            this.applyTime.TabIndex = 85;
            this.applyTime.Text = "申请日期";
            // 
            // txt_proposer
            // 
            this.txt_proposer.Location = new System.Drawing.Point(212, 96);
            this.txt_proposer.Margin = new System.Windows.Forms.Padding(2);
            this.txt_proposer.Name = "txt_proposer";
            this.txt_proposer.ReadOnly = true;
            this.txt_proposer.Size = new System.Drawing.Size(64, 21);
            this.txt_proposer.TabIndex = 84;
            // 
            // proposer
            // 
            this.proposer.AutoSize = true;
            this.proposer.Location = new System.Drawing.Point(168, 98);
            this.proposer.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.proposer.Name = "proposer";
            this.proposer.Size = new System.Drawing.Size(41, 12);
            this.proposer.TabIndex = 83;
            this.proposer.Text = "申请人";
            // 
            // departmentLabel
            // 
            this.departmentLabel.AutoSize = true;
            this.departmentLabel.Location = new System.Drawing.Point(17, 98);
            this.departmentLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.departmentLabel.Name = "departmentLabel";
            this.departmentLabel.Size = new System.Drawing.Size(53, 12);
            this.departmentLabel.TabIndex = 82;
            this.departmentLabel.Text = "申请部门";
            // 
            // txt_phonenum
            // 
            this.txt_phonenum.Location = new System.Drawing.Point(334, 96);
            this.txt_phonenum.Margin = new System.Windows.Forms.Padding(2);
            this.txt_phonenum.Name = "txt_phonenum";
            this.txt_phonenum.ReadOnly = true;
            this.txt_phonenum.Size = new System.Drawing.Size(115, 21);
            this.txt_phonenum.TabIndex = 81;
            // 
            // phoneLabel
            // 
            this.phoneLabel.AutoSize = true;
            this.phoneLabel.Location = new System.Drawing.Point(279, 98);
            this.phoneLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.phoneLabel.Name = "phoneLabel";
            this.phoneLabel.Size = new System.Drawing.Size(53, 12);
            this.phoneLabel.TabIndex = 80;
            this.phoneLabel.Text = "联系电话";
            // 
            // txt_demandId
            // 
            this.txt_demandId.Location = new System.Drawing.Point(73, 54);
            this.txt_demandId.Margin = new System.Windows.Forms.Padding(2);
            this.txt_demandId.Name = "txt_demandId";
            this.txt_demandId.ReadOnly = true;
            this.txt_demandId.Size = new System.Drawing.Size(399, 21);
            this.txt_demandId.TabIndex = 79;
            // 
            // demandIdLabel
            // 
            this.demandIdLabel.AutoSize = true;
            this.demandIdLabel.Location = new System.Drawing.Point(18, 56);
            this.demandIdLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.demandIdLabel.Name = "demandIdLabel";
            this.demandIdLabel.Size = new System.Drawing.Size(53, 12);
            this.demandIdLabel.TabIndex = 78;
            this.demandIdLabel.Text = "需求单号";
            // 
            // txt_reviewResult
            // 
            this.txt_reviewResult.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txt_reviewResult.Location = new System.Drawing.Point(543, 468);
            this.txt_reviewResult.Name = "txt_reviewResult";
            this.txt_reviewResult.ReadOnly = true;
            this.txt_reviewResult.Size = new System.Drawing.Size(100, 21);
            this.txt_reviewResult.TabIndex = 97;
            this.txt_reviewResult.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // dataGridViewTextBoxColumn1
            // 
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle10;
            this.dataGridViewTextBoxColumn1.HeaderText = "物料编号";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn1.Width = 150;
            // 
            // dataGridViewTextBoxColumn2
            // 
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewTextBoxColumn2.DefaultCellStyle = dataGridViewCellStyle11;
            this.dataGridViewTextBoxColumn2.HeaderText = "物料名称";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.Width = 150;
            // 
            // dataGridViewTextBoxColumn3
            // 
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewTextBoxColumn3.DefaultCellStyle = dataGridViewCellStyle12;
            this.dataGridViewTextBoxColumn3.HeaderText = "物料组";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // dataGridViewTextBoxColumn4
            // 
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewTextBoxColumn4.DefaultCellStyle = dataGridViewCellStyle13;
            this.dataGridViewTextBoxColumn4.HeaderText = "申请数量";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            // 
            // dataGridViewTextBoxColumn5
            // 
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewTextBoxColumn5.DefaultCellStyle = dataGridViewCellStyle14;
            this.dataGridViewTextBoxColumn5.HeaderText = "单位";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.Width = 70;
            // 
            // dataGridViewTextBoxColumn6
            // 
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewTextBoxColumn6.DefaultCellStyle = dataGridViewCellStyle15;
            this.dataGridViewTextBoxColumn6.HeaderText = "仓库编号";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            // 
            // dataGridViewTextBoxColumn7
            // 
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewTextBoxColumn7.DefaultCellStyle = dataGridViewCellStyle16;
            this.dataGridViewTextBoxColumn7.HeaderText = "工厂编号";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(484, 16);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 12);
            this.label5.TabIndex = 98;
            this.label5.Text = "工厂编号";
            // 
            // txt_factoryID
            // 
            this.txt_factoryID.Location = new System.Drawing.Point(543, 13);
            this.txt_factoryID.Name = "txt_factoryID";
            this.txt_factoryID.ReadOnly = true;
            this.txt_factoryID.Size = new System.Drawing.Size(100, 21);
            this.txt_factoryID.TabIndex = 99;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(13, 462);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 12);
            this.label6.TabIndex = 100;
            this.label6.Text = "采购价格";
            // 
            // txtPurPrice
            // 
            this.txtPurPrice.Location = new System.Drawing.Point(84, 459);
            this.txtPurPrice.Name = "txtPurPrice";
            this.txtPurPrice.ReadOnly = true;
            this.txtPurPrice.Size = new System.Drawing.Size(131, 21);
            this.txtPurPrice.TabIndex = 101;
            // 
            // DetailSummaryDemand
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(923, 519);
            this.Controls.Add(this.txtPurPrice);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txt_factoryID);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txt_reviewResult);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.txt_applyTime);
            this.Controls.Add(this.txt_department);
            this.Controls.Add(this.txt_purchase);
            this.Controls.Add(this.txt_logistic);
            this.Controls.Add(this.cancle);
            this.Controls.Add(this.btn_edit);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.applyTime);
            this.Controls.Add(this.txt_proposer);
            this.Controls.Add(this.proposer);
            this.Controls.Add(this.departmentLabel);
            this.Controls.Add(this.txt_phonenum);
            this.Controls.Add(this.phoneLabel);
            this.Controls.Add(this.txt_demandId);
            this.Controls.Add(this.demandIdLabel);
            this.Controls.Add(this.txt_purchaseType);
            this.Controls.Add(this.txt_logistics);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.purchaseTypeLabel);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "DetailSummaryDemand";
            this.Text = "查看需求计划";
            this.groupBox2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.materialGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txt_purchaseType;
        private System.Windows.Forms.TextBox txt_logistics;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label purchaseTypeLabel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RichTextBox rtb_reviewAdvice;
        private System.Windows.Forms.TextBox txt_applyTime;
        private System.Windows.Forms.TextBox txt_department;
        private System.Windows.Forms.TextBox txt_purchase;
        private System.Windows.Forms.TextBox txt_logistic;
        private System.Windows.Forms.Button cancle;
        private System.Windows.Forms.Button btn_edit;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView materialGridView;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label applyTime;
        private System.Windows.Forms.TextBox txt_proposer;
        private System.Windows.Forms.Label proposer;
        private System.Windows.Forms.Label departmentLabel;
        private System.Windows.Forms.TextBox txt_phonenum;
        private System.Windows.Forms.Label phoneLabel;
        private System.Windows.Forms.TextBox txt_demandId;
        private System.Windows.Forms.Label demandIdLabel;
        private System.Windows.Forms.TextBox txt_reviewResult;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txt_factoryID;
        private System.Windows.Forms.DataGridViewTextBoxColumn materialId;
        private System.Windows.Forms.DataGridViewTextBoxColumn materialName;
        private System.Windows.Forms.DataGridViewTextBoxColumn materialGroup;
        private System.Windows.Forms.DataGridViewTextBoxColumn applyNum;
        private System.Windows.Forms.DataGridViewTextBoxColumn measurement;
        private System.Windows.Forms.DataGridViewTextBoxColumn stockId;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtPurPrice;
    }
}