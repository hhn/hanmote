﻿using Lib.Bll.SourcingManage.ProcurementPlan;
using Lib.Common.MMCException.IDAL;
using Lib.Model.SourcingManage.ProcurementPlan;
using Lib.SqlServerDAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;

namespace MMClient.SourcingManage.ProcurementPlan
{
    public partial class ManagerPlansForm : DockContent
    {
        //需求表格生成行数
        int demandNum = 0;
        //被选中的行值
        public int j = 0;

        Summary_DemandBLL summaryDemandBll = new Summary_DemandBLL();
        Demand_MaterialBLL demandMaterialBll = new Demand_MaterialBLL();

        public DataTable checkDt = null;
        public Summary_Demand summaryDemand = new Summary_Demand();
        public ManagerPlansForm()
        {
            InitializeComponent();
            addItemsToCMB();
        }
        public void addItemsToCMB()
        {
            //需求单号自动加载至item
            try
            {
                DataTable findItem = summaryDemandBll.FindStandardDemand_ID("Demand_ID");
                this.xqdh_cmb.DataSource = null;
                if (findItem != null)
                {
                    this.xqdh_cmb.DataSource = findItem;
                    this.xqdh_cmb.ValueMember = "id";
                    this.xqdh_cmb.DisplayMember = "id";
                }
            }
            catch (DBException ex)
            {
                MessageBox.Show("数据加载失败");
            }
        }
        /// <summary>
        /// shenming: 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void query_bt_Click(object sender, EventArgs e)
        {
            if (this.xqdh_cmb.Text.ToString().Trim() == "")
            {
                MessageBox.Show("请先选择一个需求单号");
                return;
            }
            if (materialGridView.Rows.Count > 0) materialGridView.Rows.Clear();
            string demandId = this.xqdh_cmb.Text.ToString();
            string state = this.state_cmb.Text.ToString();
            DataTable dt = new DataTable();
            string sql = @"SELECT
	                            Purchase_Type as 采购类型 ,
                                State as 状态,
	                            Demand_ID as 需求单号,
	                            Proposer_ID as 申请人,
	                            Create_Time as 申请日期,
                                ReviewTime as 审核日期 
                            FROM
	                            Summary_Demand where State = '"+ state + "' OR Demand_ID ='"+ demandId + "'";
            dt = DBHelper.ExecuteQueryDT(sql);
            this.demandGridView.DataSource = dt;
        }
        private void fillMaterialGridView()
        {
            materialGridView.Rows.Clear();
            if(demandGridView == null || demandGridView.Rows.Count<1)
            {
                return;
            }
            String demandid = demandGridView.CurrentRow.Cells["demandId"].Value.ToString();
            //根据选中行id查找对应的需求计划
            try
            {
                List<Demand_Material> demands = demandMaterialBll.findDemandMaterials(demandid);
                if (demands.Count > materialGridView.Rows.Count)
                {
                    materialGridView.Rows.Add(demands.Count - materialGridView.Rows.Count);
                }
                for (int i = 0; i < demands.Count; i++)
                {
                    materialGridView.Rows[i].Cells["materialId"].Value = demands.ElementAt(i).Material_ID;
                    materialGridView.Rows[i].Cells["materialName"].Value = demands.ElementAt(i).Material_Name;
                    materialGridView.Rows[i].Cells["materialGroup"].Value = demands.ElementAt(i).Material_Group;
                    materialGridView.Rows[i].Cells["demandCount"].Value = demands.ElementAt(i).Demand_Count;
                    materialGridView.Rows[i].Cells["measurement"].Value = demands.ElementAt(i).Measurement;
                    materialGridView.Rows[i].Cells["stockId"].Value = demands.ElementAt(i).Stock_ID;
                }
            }
            catch(DBException ex)
            {
                MessageBox.Show("数据加载失败");
            }
           
        }
        /// <summary>
        /// xinjian
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnNewPlan_Click(object sender, EventArgs e)
        {
            NewSummaryDemand newSummaryDemand = new NewSummaryDemand();
            newSummaryDemand.Show();
        }

        private void btnModifyPlans_Click(object sender, EventArgs e)
        {
            if(summaryDemand.Demand_ID==null)
            {
                MessageBox.Show("请先选择一个订单号", "提示");
                return;
            }
            if (summaryDemand.State.Equals("待审核"))
            {
                EditStandardDemand editSummaryDemandForm = new EditStandardDemand(summaryDemand);
                editSummaryDemandForm.Show();
            }
            else
            {

                MessageBoxButtons messButton = MessageBoxButtons.OKCancel;
                DialogResult dr = MessageBox.Show("选中计划订单已通过审核，编辑后需重新审核。\r\r是否确定重新编辑?", "", messButton);
                if (dr == DialogResult.OK)
                {

                    EditStandardDemand editSummaryDemand_Form = new EditStandardDemand(summaryDemand);
                    editSummaryDemand_Form.Show();
                }
                else
                {
                    MessageBox.Show("已取消编辑！");
                }
            }
        }

        private void btnViewPlan_Click(object sender, EventArgs e)
        {
            if (summaryDemand.Demand_ID == null)
            {
                MessageBox.Show("请先选择一个订单号", "提示");
                return;
            }
            else
            {
                DetailSummaryDemand detailSummaryDemand_Form = new DetailSummaryDemand(summaryDemand);
                detailSummaryDemand_Form.Show();
            }
        }

        private void btnDelPlan_Click(object sender, EventArgs e)
        {
            if (summaryDemand.Demand_ID == null)
            {
                MessageBox.Show("请先选择一个订单号", "提示");
                return;
            }
            else
            {
                if (MessageBox.Show("确定删除需求单号" + summaryDemand.Demand_ID + "?", "提示", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    Summary_DemandBLL summaryDemandBll = new Summary_DemandBLL();
                    Demand_MaterialBLL demandMaterialBll = new Demand_MaterialBLL();
                    try
                    {
                        demandMaterialBll.deleteDemandMaterial(summaryDemand.Demand_ID, null);
                        summaryDemandBll.deleteSummaryDemandById(summaryDemand.Demand_ID);
                        MessageBox.Show("删除成功");
                        BtnReset_Click(sender,e);
                        try
                        {
                            addItemsToCMB();
                        }
                        catch(DBException ex)
                        {
                            MessageBox.Show("订单号加载失败");
                        }
                    }
                    catch(DBException ex)
                    {
                        MessageBox.Show("删除失败");
                    }
                }
            }
        }

        private void 刷新_Click(object sender, EventArgs e)
        {
            try
            {
                addItemsToCMB();
            }
            catch(DBException ex)
            {
                MessageBox.Show("订单号加载失败");
            }
           
        }
        /// <summary>
        /// 重置
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnReset_Click(object sender, EventArgs e)
        {
            try
            {
                string sql = @"SELECT
	                            Purchase_Type as 采购类型 ,
                                State as 状态,
	                            Demand_ID as 需求单号,
	                            Proposer_ID as 申请人,
	                            Create_Time as 申请日期,
                                ReviewTime as 审核日期 
                            FROM
	                            Summary_Demand";
                DataTable dt = DBHelper.ExecuteQueryDT(sql);
                this.demandGridView.DataSource = dt;
                addItemsToCMB();
            }
            catch(DBException ex)
            {
                MessageBox.Show("数据加载失败");
            }
        }

        private void initData()
        {
            DataTable dt = new DataTable();
            try
            {
                if (this.demandGridView.Rows.Count == 0) return;
                string demandID = this.demandGridView.CurrentRow.Cells["demandId"].Value.ToString();
                if (string.IsNullOrEmpty(demandID))
                {
                    MessageBox.Show("没有选中任何行");
                    return;
                }
                string sql = @"SELECT
	                                Purchase_Type AS 采购类型,
	                                LogisticsMode AS 物流方式,
	                                State AS 状态,
	                                Demand_ID AS 需求单号,
	                                Department AS 申请部门,
                                    Proposer_ID AS 申请人,
	                                PhoneNum AS 电话,
	                                Create_Time AS 申请日期,
	                                ReviewTime AS 审核日期,
                                    ReviewAdvice as 审核意见,
                                    Purchase_Price as 采购价格
                                FROM
	                                Summary_Demand where Demand_ID='" + demandID + "' ";
                dt = DBHelper.ExecuteQueryDT(sql); 
              
            }
            catch (DBException ex)
            {
                MessageBox.Show("数据加载失败");
            }
            if (dt == null || dt.Rows.Count == 0)
            {
                return;
            }
            materialGridView.TopLeftHeaderCell.Value = "序号";
            summaryDemand.Purchase_Type = dt.Rows[0][0].ToString();
            summaryDemand.LogisticsMode = dt.Rows[0][1].ToString();
            summaryDemand.State = dt.Rows[0][2].ToString();
            summaryDemand.Demand_ID = dt.Rows[0][3].ToString();
            summaryDemand.Department = dt.Rows[0][4].ToString();
            summaryDemand.Proposer_ID = dt.Rows[0][5].ToString();
            summaryDemand.PhoneNum = dt.Rows[0][6].ToString();
            summaryDemand.Create_Time.ToString(dt.Rows[0][7].ToString());
            summaryDemand.ReviewTime = dt.Rows[0][8].ToString() ;
            summaryDemand.ReviewAdvice = dt.Rows[0][9].ToString();
            summaryDemand.Purchase_Price = dt.Rows[0][10].ToString();
            fillMaterialGridView();
        }

        private void ManagerPlansForm_Load(object sender, EventArgs e)
        {
            BtnReset_Click(sender, e);
            initData();
        }

        private void demandGridView_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            initData();
            fillMaterialGridView();
        }
    }
}
