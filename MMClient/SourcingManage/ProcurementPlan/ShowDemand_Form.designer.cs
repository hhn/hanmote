﻿namespace MMClient.SourcingManage.ProcurementPlan
{
    partial class ShowDemand_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            this.type_cmb = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.wlbh_cmb = new System.Windows.Forms.ComboBox();
            this.xqdh_cmb = new System.Windows.Forms.ComboBox();
            this.query_bt = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.closeButton = new System.Windows.Forms.Button();
            this.flushButton = new System.Windows.Forms.Button();
            this.sqr_cmb = new System.Windows.Forms.ComboBox();
            this.demandGridView = new System.Windows.Forms.DataGridView();
            this.materialGridView = new System.Windows.Forms.DataGridView();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.pageTool = new pager.pagetool.pageNext();
            this.pageNext1 = new pager.pagetool.pageNext();
            this.forecast_pb = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.materialId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.materialName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.materialGroup = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.demandCount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.measurement = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.stockId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.demandId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.purchaseType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.proposerId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.state = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.factoryId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.applyTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.reviewTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.demandGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.materialGridView)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // type_cmb
            // 
            this.type_cmb.FormattingEnabled = true;
            this.type_cmb.ItemHeight = 12;
            this.type_cmb.Location = new System.Drawing.Point(777, 58);
            this.type_cmb.Name = "type_cmb";
            this.type_cmb.Size = new System.Drawing.Size(150, 20);
            this.type_cmb.TabIndex = 97;
            this.type_cmb.Text = "待审核";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("宋体", 9F);
            this.label2.Location = new System.Drawing.Point(742, 61);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 12);
            this.label2.TabIndex = 95;
            this.label2.Text = "状态";
            // 
            // wlbh_cmb
            // 
            this.wlbh_cmb.FormattingEnabled = true;
            this.wlbh_cmb.ItemHeight = 12;
            this.wlbh_cmb.Location = new System.Drawing.Point(370, 58);
            this.wlbh_cmb.Name = "wlbh_cmb";
            this.wlbh_cmb.Size = new System.Drawing.Size(150, 20);
            this.wlbh_cmb.TabIndex = 93;
            // 
            // xqdh_cmb
            // 
            this.xqdh_cmb.FormattingEnabled = true;
            this.xqdh_cmb.ItemHeight = 12;
            this.xqdh_cmb.Location = new System.Drawing.Point(148, 57);
            this.xqdh_cmb.Name = "xqdh_cmb";
            this.xqdh_cmb.Size = new System.Drawing.Size(150, 20);
            this.xqdh_cmb.TabIndex = 92;
            // 
            // query_bt
            // 
            this.query_bt.Location = new System.Drawing.Point(963, 55);
            this.query_bt.Name = "query_bt";
            this.query_bt.Size = new System.Drawing.Size(63, 25);
            this.query_bt.TabIndex = 90;
            this.query_bt.Text = "查询";
            this.query_bt.UseVisualStyleBackColor = true;
            this.query_bt.Click += new System.EventHandler(this.query_bt_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("宋体", 9F);
            this.label1.Location = new System.Drawing.Point(531, 61);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 12);
            this.label1.TabIndex = 89;
            this.label1.Text = "申请人";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("宋体", 9F);
            this.label11.Location = new System.Drawing.Point(311, 62);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(53, 12);
            this.label11.TabIndex = 88;
            this.label11.Text = "物料编号";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("宋体", 9F);
            this.label12.Location = new System.Drawing.Point(89, 61);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(53, 12);
            this.label12.TabIndex = 87;
            this.label12.Text = "需求单号";
            // 
            // closeButton
            // 
            this.closeButton.Location = new System.Drawing.Point(91, 2);
            this.closeButton.Margin = new System.Windows.Forms.Padding(2);
            this.closeButton.Name = "closeButton";
            this.closeButton.Size = new System.Drawing.Size(64, 30);
            this.closeButton.TabIndex = 103;
            this.closeButton.Text = "关闭";
            this.closeButton.UseVisualStyleBackColor = true;
            this.closeButton.Click += new System.EventHandler(this.closeButton_Click);
            // 
            // flushButton
            // 
            this.flushButton.Location = new System.Drawing.Point(7, 2);
            this.flushButton.Margin = new System.Windows.Forms.Padding(2);
            this.flushButton.Name = "flushButton";
            this.flushButton.Size = new System.Drawing.Size(64, 30);
            this.flushButton.TabIndex = 103;
            this.flushButton.Text = "刷新";
            this.flushButton.UseVisualStyleBackColor = true;
            this.flushButton.Click += new System.EventHandler(this.flushButton_Click);
            // 
            // sqr_cmb
            // 
            this.sqr_cmb.FormattingEnabled = true;
            this.sqr_cmb.ItemHeight = 12;
            this.sqr_cmb.Location = new System.Drawing.Point(578, 58);
            this.sqr_cmb.Name = "sqr_cmb";
            this.sqr_cmb.Size = new System.Drawing.Size(150, 20);
            this.sqr_cmb.TabIndex = 221;
            // 
            // demandGridView
            // 
            this.demandGridView.AllowUserToAddRows = false;
            this.demandGridView.BackgroundColor = System.Drawing.SystemColors.Control;
            this.demandGridView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.demandGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.demandGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.demandId,
            this.purchaseType,
            this.proposerId,
            this.state,
            this.factoryId,
            this.applyTime,
            this.reviewTime});
            this.demandGridView.EnableHeadersVisualStyles = false;
            this.demandGridView.Location = new System.Drawing.Point(-1, 18);
            this.demandGridView.Margin = new System.Windows.Forms.Padding(2);
            this.demandGridView.Name = "demandGridView";
            this.demandGridView.RowTemplate.Height = 27;
            this.demandGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.demandGridView.Size = new System.Drawing.Size(933, 250);
            this.demandGridView.TabIndex = 222;
            this.demandGridView.CellMouseEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.demandGridView_CellMouseEnter);
            this.demandGridView.CellMouseLeave += new System.Windows.Forms.DataGridViewCellEventHandler(this.demandGridView_CellMouseLeave);
            this.demandGridView.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.demandGridView_RowPostPaint);
            this.demandGridView.SelectionChanged += new System.EventHandler(this.demandGridView_SelectionChanged);
            // 
            // materialGridView
            // 
            this.materialGridView.AllowUserToAddRows = false;
            this.materialGridView.BackgroundColor = System.Drawing.SystemColors.Control;
            this.materialGridView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.materialGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.materialGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.materialId,
            this.materialName,
            this.materialGroup,
            this.demandCount,
            this.measurement,
            this.stockId});
            this.materialGridView.EnableHeadersVisualStyles = false;
            this.materialGridView.Location = new System.Drawing.Point(5, 19);
            this.materialGridView.Margin = new System.Windows.Forms.Padding(2);
            this.materialGridView.Name = "materialGridView";
            this.materialGridView.RowTemplate.Height = 27;
            this.materialGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.materialGridView.Size = new System.Drawing.Size(918, 202);
            this.materialGridView.TabIndex = 223;
            this.materialGridView.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.materialGridView_RowPostPaint);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label3.Location = new System.Drawing.Point(11, 89);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 12);
            this.label3.TabIndex = 224;
            this.label3.Text = "需求计划";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label4.Location = new System.Drawing.Point(10, 457);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(57, 12);
            this.label4.TabIndex = 225;
            this.label4.Text = "物料信息";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.materialGridView);
            this.groupBox1.Location = new System.Drawing.Point(22, 481);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(929, 226);
            this.groupBox1.TabIndex = 226;
            this.groupBox1.TabStop = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.demandGridView);
            this.groupBox2.Location = new System.Drawing.Point(13, 116);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(938, 273);
            this.groupBox2.TabIndex = 227;
            this.groupBox2.TabStop = false;
            // 
            // pageTool
            // 
            this.pageTool.Location = new System.Drawing.Point(183, 395);
            this.pageTool.Name = "pageTool";
            this.pageTool.PageIndex = 1;
            this.pageTool.PageSize = 100;
            this.pageTool.RecordCount = 0;
            this.pageTool.Size = new System.Drawing.Size(685, 44);
            this.pageTool.TabIndex = 228;
            this.pageTool.Load += new System.EventHandler(this.pageTool_Load);
            // 
            // pageNext1
            // 
            this.pageNext1.Location = new System.Drawing.Point(183, 713);
            this.pageNext1.Name = "pageNext1";
            this.pageNext1.PageIndex = 1;
            this.pageNext1.PageSize = 100;
            this.pageNext1.RecordCount = 0;
            this.pageNext1.Size = new System.Drawing.Size(685, 44);
            this.pageNext1.TabIndex = 229;
            this.pageNext1.OnPageChanged += new System.EventHandler(this.pageNext1_OnPageChanged);
            this.pageNext1.Load += new System.EventHandler(this.pageNext1_Load);
            // 
            // forecast_pb
            // 
            this.forecast_pb.Location = new System.Drawing.Point(183, 2);
            this.forecast_pb.Name = "forecast_pb";
            this.forecast_pb.Size = new System.Drawing.Size(64, 30);
            this.forecast_pb.TabIndex = 104;
            this.forecast_pb.Text = "预测";
            this.forecast_pb.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.BackColor = System.Drawing.SystemColors.Control;
            this.panel1.Controls.Add(this.pageNext1);
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Controls.Add(this.pageTool);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.forecast_pb);
            this.panel1.Controls.Add(this.groupBox2);
            this.panel1.Controls.Add(this.flushButton);
            this.panel1.Controls.Add(this.closeButton);
            this.panel1.Controls.Add(this.label12);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.sqr_cmb);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.type_cmb);
            this.panel1.Controls.Add(this.query_bt);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.xqdh_cmb);
            this.panel1.Controls.Add(this.wlbh_cmb);
            this.panel1.Location = new System.Drawing.Point(0, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1192, 796);
            this.panel1.TabIndex = 86;
            // 
            // dataGridViewTextBoxColumn1
            // 
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle13;
            this.dataGridViewTextBoxColumn1.HeaderText = "物料编号";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Width = 150;
            // 
            // dataGridViewTextBoxColumn2
            // 
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewTextBoxColumn2.DefaultCellStyle = dataGridViewCellStyle14;
            this.dataGridViewTextBoxColumn2.HeaderText = "物料名称";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.Width = 150;
            // 
            // dataGridViewTextBoxColumn3
            // 
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewTextBoxColumn3.DefaultCellStyle = dataGridViewCellStyle15;
            this.dataGridViewTextBoxColumn3.HeaderText = "物料组";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // dataGridViewTextBoxColumn4
            // 
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewTextBoxColumn4.DefaultCellStyle = dataGridViewCellStyle16;
            this.dataGridViewTextBoxColumn4.HeaderText = "需求数量";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            // 
            // dataGridViewTextBoxColumn5
            // 
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewTextBoxColumn5.DefaultCellStyle = dataGridViewCellStyle17;
            this.dataGridViewTextBoxColumn5.HeaderText = "计量单位";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.HeaderText = "仓库编号";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            // 
            // dataGridViewTextBoxColumn7
            // 
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewTextBoxColumn7.DefaultCellStyle = dataGridViewCellStyle18;
            this.dataGridViewTextBoxColumn7.HeaderText = "需求单号";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.Width = 130;
            // 
            // dataGridViewTextBoxColumn8
            // 
            dataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewTextBoxColumn8.DefaultCellStyle = dataGridViewCellStyle19;
            this.dataGridViewTextBoxColumn8.HeaderText = "采购类型";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.Width = 150;
            // 
            // dataGridViewTextBoxColumn9
            // 
            dataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewTextBoxColumn9.DefaultCellStyle = dataGridViewCellStyle20;
            this.dataGridViewTextBoxColumn9.HeaderText = "申请人";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            // 
            // dataGridViewTextBoxColumn10
            // 
            dataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewTextBoxColumn10.DefaultCellStyle = dataGridViewCellStyle21;
            this.dataGridViewTextBoxColumn10.HeaderText = "状态";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            // 
            // dataGridViewTextBoxColumn11
            // 
            dataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewTextBoxColumn11.DefaultCellStyle = dataGridViewCellStyle22;
            this.dataGridViewTextBoxColumn11.HeaderText = "工厂编号";
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            // 
            // dataGridViewTextBoxColumn12
            // 
            dataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewTextBoxColumn12.DefaultCellStyle = dataGridViewCellStyle23;
            this.dataGridViewTextBoxColumn12.HeaderText = "申请日期";
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            this.dataGridViewTextBoxColumn12.Width = 120;
            // 
            // dataGridViewTextBoxColumn13
            // 
            dataGridViewCellStyle24.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewTextBoxColumn13.DefaultCellStyle = dataGridViewCellStyle24;
            this.dataGridViewTextBoxColumn13.HeaderText = "审核日期";
            this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
            this.dataGridViewTextBoxColumn13.Width = 120;
            // 
            // materialId
            // 
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.materialId.DefaultCellStyle = dataGridViewCellStyle8;
            this.materialId.HeaderText = "物料编号";
            this.materialId.Name = "materialId";
            this.materialId.Width = 150;
            // 
            // materialName
            // 
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.materialName.DefaultCellStyle = dataGridViewCellStyle9;
            this.materialName.HeaderText = "物料名称";
            this.materialName.Name = "materialName";
            this.materialName.Width = 150;
            // 
            // materialGroup
            // 
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.materialGroup.DefaultCellStyle = dataGridViewCellStyle10;
            this.materialGroup.HeaderText = "物料组";
            this.materialGroup.Name = "materialGroup";
            // 
            // demandCount
            // 
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.demandCount.DefaultCellStyle = dataGridViewCellStyle11;
            this.demandCount.HeaderText = "需求数量";
            this.demandCount.Name = "demandCount";
            // 
            // measurement
            // 
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.measurement.DefaultCellStyle = dataGridViewCellStyle12;
            this.measurement.HeaderText = "计量单位";
            this.measurement.Name = "measurement";
            // 
            // stockId
            // 
            this.stockId.HeaderText = "仓库编号";
            this.stockId.Name = "stockId";
            // 
            // demandId
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.demandId.DefaultCellStyle = dataGridViewCellStyle1;
            this.demandId.HeaderText = "需求单号";
            this.demandId.Name = "demandId";
            this.demandId.Width = 130;
            // 
            // purchaseType
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.purchaseType.DefaultCellStyle = dataGridViewCellStyle2;
            this.purchaseType.HeaderText = "采购类型";
            this.purchaseType.Name = "purchaseType";
            this.purchaseType.Width = 150;
            // 
            // proposerId
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.proposerId.DefaultCellStyle = dataGridViewCellStyle3;
            this.proposerId.HeaderText = "申请人";
            this.proposerId.Name = "proposerId";
            // 
            // state
            // 
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.state.DefaultCellStyle = dataGridViewCellStyle4;
            this.state.HeaderText = "状态";
            this.state.Name = "state";
            // 
            // factoryId
            // 
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.factoryId.DefaultCellStyle = dataGridViewCellStyle5;
            this.factoryId.HeaderText = "工厂编号";
            this.factoryId.Name = "factoryId";
            // 
            // applyTime
            // 
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.applyTime.DefaultCellStyle = dataGridViewCellStyle6;
            this.applyTime.HeaderText = "申请日期";
            this.applyTime.Name = "applyTime";
            this.applyTime.Width = 120;
            // 
            // reviewTime
            // 
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.reviewTime.DefaultCellStyle = dataGridViewCellStyle7;
            this.reviewTime.HeaderText = "审核日期";
            this.reviewTime.Name = "reviewTime";
            this.reviewTime.Width = 120;
            // 
            // ShowDemand_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1204, 820);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "ShowDemand_Form";
            this.Text = "采购计划管理";
            ((System.ComponentModel.ISupportInitialize)(this.demandGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.materialGridView)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.ComboBox wlbh_cmb;
        private System.Windows.Forms.ComboBox xqdh_cmb;
        private System.Windows.Forms.Button query_bt;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox type_cmb;
        private System.Windows.Forms.ComboBox sqr_cmb;
        private System.Windows.Forms.Button flushButton;
        private System.Windows.Forms.Button closeButton;
        private System.Windows.Forms.DataGridView demandGridView;
        private System.Windows.Forms.DataGridView materialGridView;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridViewTextBoxColumn units1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridViewTextBoxColumn demandId;
        private System.Windows.Forms.DataGridViewTextBoxColumn purchaseType;
        private System.Windows.Forms.DataGridViewTextBoxColumn proposerId;
        private System.Windows.Forms.DataGridViewTextBoxColumn state;
        private System.Windows.Forms.DataGridViewTextBoxColumn factoryId;
        private System.Windows.Forms.DataGridViewTextBoxColumn applyTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn reviewTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn materialId;
        private System.Windows.Forms.DataGridViewTextBoxColumn materialName;
        private System.Windows.Forms.DataGridViewTextBoxColumn materialGroup;
        private System.Windows.Forms.DataGridViewTextBoxColumn demandCount;
        private System.Windows.Forms.DataGridViewTextBoxColumn measurement;
        private System.Windows.Forms.DataGridViewTextBoxColumn stockId;
        private pager.pagetool.pageNext pageTool;
        private pager.pagetool.pageNext pageNext1;
        private System.Windows.Forms.Button forecast_pb;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
    }
}