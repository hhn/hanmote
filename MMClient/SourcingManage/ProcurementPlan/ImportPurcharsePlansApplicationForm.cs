﻿using Lib.Bll.MDBll.General;
using Lib.Bll.SourcingManage.ProcurementPlan;
using Lib.Common.CommonUtils;
using Lib.Common.MMCException.IDAL;
using Lib.Model.SourcingManage.ProcurementPlan;
using Lib.SqlServerDAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MMClient.SourcingManage.ProcurementPlan
{
    public partial class ImportPurcharsePlansApplicationForm : Form
    {
        private string OledbConnString = "Provider = Microsoft.Jet.OLEDB.4.0 ; Data Source = {0};Extended Properties='Excel 8.0;HDR=Yes;IMEX=1;'";
        private List<string> listSQL = null;
        private ConvenientTools Tools = new ConvenientTools();
        private Summary_DemandBLL demandBll = new Summary_DemandBLL();
        private GeneralBLL generallBll = new GeneralBLL();
        public ImportPurcharsePlansApplicationForm()
        {
            InitializeComponent();
            //初始化需求单号
            demandIdText.Text = Tools.systemTimeToStr();
            materialGridView.TopLeftHeaderCell.Value = "序号";
            //初始化工厂编号
            initFactory();
            listSQL = new List<string>();//初始化SQL
        }

        private void initFactory()
        {
            List<string> factoryID = generallBll.GetAllFactory();
           // cmb_factory.DataSource = factoryID;
        }

        /// <summary>
        /// 选择数据源
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnoutImport_Click(object sender, EventArgs e)
        {
            listSQL = new List<string>();//初始化SQL
            DialogResult dr1 = this.openFileDialog1.ShowDialog();
            if (DialogResult.OK == dr1) //判断是否选择文件  
            {
                string path = this.openFileDialog1.FileName.Trim();
                if (string.IsNullOrEmpty(path))
                {
                    MessageBox.Show("请选择要导入的EXCEL文件！！！", "信息");
                    return;
                }
                if (!File.Exists(path))  //判断文件是否存在  
                {
                    MessageBox.Show("信息", "找不到对应的Excel文件，请重新选择。");
                    return;
                }
                DataTable excelTbl = this.GetExcelTable(path);  //调用函数获取Excel中的信息  
                
                if (excelTbl == null)
                {
                    return;
                }
                this.materialGridView.DataSource = excelTbl;
            }
        }

        private DataTable GetExcelTable(string path)
        {
            try
            {
                //获取excel数据  
                DataTable dt1 = new DataTable();
                if (!(path.ToLower().IndexOf(".xlsx") < 0))
                {
                    OledbConnString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source='{0}';Extended Properties='Excel 12.0;HDR=YES'";
                }
                string strConn = string.Format(OledbConnString, path);
                OleDbConnection conn = new OleDbConnection(strConn);
                conn.Open();
                DataTable dt = conn.GetSchema("Tables");
                //判断excel的sheet页数量，查询第1页  
                if (dt.Rows.Count > 0)
                {
                    string selSqlStr = string.Format("select * from [{0}]", dt.Rows[0]["TABLE_NAME"]);
                    OleDbDataAdapter oleDa = new OleDbDataAdapter(selSqlStr, conn);
                    oleDa.Fill(dt1);
                }
                conn.Close();
                //现先对dataTable按物料类型排序
                dt1.DefaultView.Sort = "申请数量 ASC";
                dt1 = dt1.DefaultView.ToTable();
                return dt1;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Excel转换DataTable出错：" + ex.Message);
                return null;
            }
        }
        /// <summary>
        /// 保存信息到数据库
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            MessageBoxButtons messButton = MessageBoxButtons.OKCancel;
            DialogResult dr = MessageBox.Show("是否确定将此表格的\n数据写入数据库", "注  意", messButton);
            if (dr == DialogResult.OK)
            {
                string MessageFail = "";
                Summary_Demand demand = new Summary_Demand();
                Demand_MaterialBLL demandMaterialBll = new Demand_MaterialBLL();
                demand.Demand_ID = demandIdText.Text;
                demand.Purchase_Type = purchaseTypeCB.Text;
                demand.LogisticsMode = logisticsCB.Text;
                demand.Department = txtdepartment.Text;
                demand.Proposer_ID = txt_Proposer.Text;
                demand.PhoneNum = phoneText.Text;
                demand.Create_Time = DateTime.Parse(dtp_apply.Text);
                
                demand.State = "待审核";

                string sql = @"INSERT INTO dbo.Summary_Demand (
	                                Demand_ID,
	                                Purchase_Type,
	                                State,
	                                Create_Time,
	                                PhoneNum,
	                                Department,
	                                LogisticsMode,
                                    Proposer_ID)
                                VALUES('" + demand.Demand_ID + "','" + demand.Purchase_Type + "','" + demand.State + "','" + demand.Create_Time + "','" + demand.PhoneNum + "','" + demand.Department + "','" + demand.LogisticsMode + "','"+ demand.Proposer_ID + "')";
                try
                {
                    DBHelper.ExecuteNonQuery(sql);
                }
                catch (DBException ec0)
                {
                    MessageBox.Show("需求订单插入失败！！！");
                }
                foreach (DataGridViewRow dgvr in materialGridView.Rows)
                {
                    Demand_Material demand_material = new Demand_Material();
                    demand_material.Demand_ID = demandIdText.Text;
                    demand_material.Material_ID = dgvr.Cells[0].Value.ToString();
                    demand_material.Material_Name = dgvr.Cells[1].Value.ToString();
                    demand_material.Material_Group = dgvr.Cells[2].Value.ToString();
                    demand_material.Demand_Count = Convert.ToInt32(dgvr.Cells[3].Value.ToString());
                    demand_material.Measurement = dgvr.Cells[4].Value.ToString();
                    demand_material.Stock_ID = dgvr.Cells[5].Value.ToString();
                    demand_material.Factory_ID = dgvr.Cells[6].Value.ToString();
                    demand_material.DeliveryStartTime = DateTime.Parse(dgvr.Cells[7].Value.ToString());
                    demand_material.DeliveryEndTime = DateTime.Parse(dgvr.Cells[8].Value.ToString());
                    //将需求计划添加到数据库
                    try
                    {
                        string insertsql = @"INSERT INTO dbo.Demand_Material (
	                                    Demand_ID,
	                                    Material_ID,
	                                    Material_Name,
	                                    Material_Group,
	                                    Demand_Count,
	                                    Measurement,
	                                    Stock_ID,
	                                    Factory_ID,
	                                    DeliveryStartTime,
	                                    DeliveryEndTime
                                    )
                                    VALUES
	                                    (
		                                    '"+demand_material.Demand_ID +@"',
                                            '"+demand_material.Material_ID+@"' ,
                                            '"+demand_material.Material_Name+@"' ,
                                            '"+demand_material.Material_Group+@"' ,
                                            '"+demand_material.Demand_Count+@"' ,
                                            '"+demand_material.Measurement+@"' ,
                                            '"+demand_material.Stock_ID+@"' ,
                                            '"+demand_material.Factory_ID+@"' ,
                                            '"+demand_material.DeliveryStartTime+@"' ,
                                            '"+demand_material.DeliveryEndTime+@"'
	                                    )";
                        DBHelper.ExecuteNonQuery(insertsql);
                    }
                    catch (DBException ex)
                    {
                        MessageFail += demandIdText.Text + ",";
                        continue;
                    }
                }
                if (MessageFail == "")
                {
                    MessageBox.Show("需求计划新建成功", "提示");
                    this.Close();
                }
                else
                {
                    MessageBox.Show("订单" + MessageFail + "创建失败:", "提示");
                }
            }
        }
        //检查数据
        private void checkBtn_Click(object sender, EventArgs e)
        {
            if(string.IsNullOrEmpty(purchaseTypeCB.Text.ToString()))
            {
                MessageBox.Show("采购类型不能为空", "提示");
                return;
            }
            if (string.IsNullOrEmpty(logisticsCB.Text.ToString()))
            {
                MessageBox.Show("物流方式不能为空", "提示");
                return;
            }
            if (string.IsNullOrEmpty(txtdepartment.Text.ToString()))
            {
                MessageBox.Show("部门不能为空", "提示");
                return;
            }
            if (string.IsNullOrEmpty(txt_Proposer.Text.ToString()))
            {
                MessageBox.Show("申请人不能为空", "提示");
                return;
            }
            if (string.IsNullOrEmpty(phoneText.Text.ToString()) )
            {
                MessageBox.Show("手机号不合法", "提示");
                phoneText.Text = "";
                return;
            }
            int i = 0;
            foreach (DataGridViewRow dgvr in materialGridView.Rows)
            {
                i++;
                DataTable dt = new DataTable();
                string  MMID = "";
                string  MMName = "";
                string MMGroup = "";
                string measuerment = "";
                string stockID = "";
                string factotyID = "";
                if (dgvr.Cells[0].Value == null || dgvr.Cells[0].Value.ToString().Trim().Equals(""))
                {
                    MessageBox.Show("物料编码不能为空", "提示");
                    return;
                }
                else
                {
                    MMID = dgvr.Cells[0].Value.ToString().Trim();
                    string sqlcheck = "select Material_Group as 物料组 ,Material_Name as 物料名称,Measurement as 计量单位 from Material where Material_ID='" + MMID + "'";
                    try
                    {
                        dt = DBHelper.ExecuteQueryDT(sqlcheck);
                        if (dt.Rows.Count == 0)
                        {
                            MessageBox.Show("第 " + i + " 行物料编码缺失，请检查");
                            return;
                        }
                    }
                    catch (DBException ex)
                    {
                        MessageBox.Show("数据库检查异常");
                        return;
                    }
                }
                if (dgvr.Cells[1].Value == null || dgvr.Cells[1].Value.ToString().Trim().Equals(""))
                {
                    MessageBox.Show("物料名称不能为空", "提示");
                    return;
                }
                else
                {
                    MMName = dgvr.Cells[1].Value.ToString().Trim();
                    if (!MMName.Equals(dt.Rows[0][1].ToString()))
                    {
                        MessageBox.Show("第 " + i + " 行物料名称错误，请检查\n 建议值：" + dt.Rows[0][1].ToString());
                        return;
                    }
                }
                if (dgvr.Cells[2].Value == null || dgvr.Cells[2].Value.ToString().Trim().Equals(""))
                {
                    MessageBox.Show("物料组不能为空", "提示");
                    return;
                }
                else
                {
                    MMGroup = dgvr.Cells[2].Value.ToString().Trim();
                    if (!MMGroup.Equals(dt.Rows[0][0].ToString()))
                    {
                        MessageBox.Show("第" + i + "行物料组，请检查\n 建议值：" + dt.Rows[0][0].ToString());
                        return;
                    }
                }
                if (dgvr.Cells[3].Value == null || dgvr.Cells[3].Value.ToString().Trim().Equals(""))
                {
                    MessageBox.Show("第 " + i + " 行申请数量不能为空", "提示");
                    return;
                }
                else
                {
                    try
                    {
                        int.Parse(dgvr.Cells[3].Value.ToString().Trim());
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("第 " + i + " 行申请数量只能是数字", "提示");
                        return;
                    }
                }
                
                if (dgvr.Cells[4].Value == null || dgvr.Cells[4].Value.ToString().Trim().Equals(""))
                {
                    MessageBox.Show("单位名称不能为空", "提示");
                    return;
                }
                else
                {
                    measuerment = dgvr.Cells[4].Value.ToString().Trim();
                    if (!measuerment.Equals(dt.Rows[0][2].ToString()))
                    {
                        MessageBox.Show("第 " + i + " 行计量单位错误，请检查\n 建议值："+ dt.Rows[0][2].ToString());
                        return;
                    }
                }
                if (dgvr.Cells[5].Value == null || dgvr.Cells[5].Value.ToString().Trim().Equals(""))
                {
                    MessageBox.Show("库存编号不能为空", "提示");
                    return;
                }
                else
                {
                    dt = null;
                    stockID = dgvr.Cells[5].Value.ToString().Trim();
                    try
                    {
                        dt = DBHelper.ExecuteQueryDT("select Factory_ID from Stock  where Stock_ID = '" + stockID + "' ");
                        if (dt.Rows.Count==0)
                        {
                            MessageBox.Show("第" + i +"行，库存与工厂关系对应有误");
                            return;
                        }
                    }
                    catch(DBException ex)
                    {
                        MessageBox.Show("数据库检查异常");
                        return;
                    }
                }
                if (dgvr.Cells[6].Value == null || dgvr.Cells[6].Value.ToString().Trim().Equals(""))
                {
                    MessageBox.Show("工厂编号不能为空", "提示");
                    return;
                }
                else
                {
                    factotyID = dgvr.Cells[6].Value.ToString().Trim();
                    if(!factotyID.Equals(dt.Rows[0][0]))
                    {
                        MessageBox.Show("第" + i + "行，库存与工厂关系对应有误");
                        return;
                    }
                }
                if (dgvr.Cells[7].Value == null || dgvr.Cells[7].Value.ToString().Trim().Equals(""))
                {
                    MessageBox.Show("交货开始时间不能为空", "提示");
                    return;
                }
                if (dgvr.Cells[8].Value == null || dgvr.Cells[8].Value.ToString().Trim().Equals(""))
                {
                    MessageBox.Show("交货结束时间不能为空", "提示");
                    return;
                }

            }
            if (phoneText.Text.ToString().Trim().Equals(""))
            {
                MessageBox.Show("联系电话不能为空", "提示");
                return;
            }
            MessageBox.Show("申请信息无误");
            submitBtn.Visible = true;
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            string path = "C:\\采购计划导入模板.xlsx";
            DataTable table = new DataTable();
            table.Columns.Add("物料编号");
            table.Columns.Add("物料名称");
            table.Columns.Add("物料组编号");
            table.Columns.Add("申请数量");
            table.Columns.Add("计量单位");
            table.Columns.Add("仓库编号");
            table.Columns.Add("工厂编号");
            table.Columns.Add("交货开始时间");
            table.Columns.Add("交货结束时间");
            DialogResult dr;
            dr = MessageBox.Show("是否已有模板?", "检查", MessageBoxButtons.YesNoCancel,
                     MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2);
            if (dr == DialogResult.Yes)
            {
                this.BtnoutImport.Visible = true;
                return;
            }
            else if (dr == DialogResult.No)
            {
                try
                {
                    FileImportOrExport.TableToExcel(table, path);
                    this.button1.Enabled = false;
                    this.BtnoutImport.Visible = true;
                    this.LbpathShow.Text = "模板已存在路径： " + path;
                    this.LbpathShow.Visible = true;
                   
                }
                catch (Exception ex)
                {
                    MessageBox.Show("模板生成失败");
                }
            }
        }
    }
}
