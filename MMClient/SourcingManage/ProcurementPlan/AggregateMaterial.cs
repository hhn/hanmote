﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;
using Lib.Bll.SourcingManage.ProcurementPlan;
using Lib.Model.SourcingManage.ProcurementPlan;
using MMClient.SourcingManage.SourcingManagement;
using Lib.Common.MMCException.IDAL;
using Lib.Common.CommonUtils;
using Lib.SqlServerDAL;
using Lib.Common.MMCException.Bll;

namespace MMClient.SourcingManage.ProcurementPlan
{
    public partial class AggregateMaterial : DockContent
    {
        private CheckAllPurChasePlansForm checkAllPurChasePlansForm = null;
        private string pageCondition = "";
        public AggregateMaterial()
        {
            InitializeComponent();
           
        }
        /// <summary>
        /// 查看汇总信息
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if(checkAllPurChasePlansForm == null || checkAllPurChasePlansForm.IsDisposed)
            {
                checkAllPurChasePlansForm = new CheckAllPurChasePlansForm();
            }
            checkAllPurChasePlansForm.Show();
        }

        private void btnQuery_Click(object sender, EventArgs e)
        {
            string startTime = "";
            string endTime = "";
            try
            {
                 startTime = Convert.ToDateTime(dtp_startTime.Text.ToString()).ToString("yyyy-MM-dd hh:mm:ss");
                 endTime = Convert.ToDateTime(dtp_endTime.Text.ToString()).ToString("yyyy-MM-dd hh:mm:ss");
            }
            catch(Exception exc)
            {
                MessageBox.Show("日期转换失败",exc.Message);
                return;
            }
            string condition = " Create_Time >= '" + startTime + "' and Create_Time<= '" + endTime + "'";
            if(!string.IsNullOrEmpty(this.cbFactory.Text.ToString()))
            {
                condition += " AND  Factory_ID  = '" + this.cbFactory.Text.ToString()+"'";
            }
           if(materialGridView.Rows.Count>0)
            {
                DataTable dt = (DataTable)materialGridView.DataSource;
                dt = null;
                materialGridView.DataSource = dt;
            }
            pageCondition = condition;
            LoadData(condition);
            pageTool_Load(sender,e);

        }
        /// <summary>
        /// 加载
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AggregateMaterial_Load(object sender, EventArgs e)
        {
            loadCBMData();
            pageCondition = "";
            LoadData("");
            pageTool_Load(sender, e);
        }
        /// <summary>
        /// load 
        /// </summary>
        private void loadCBMData()
        {
            string sql = "select Factory_ID as id from Factory";
            try
            {
                this.cbFactory.DataSource = DBHelper.ExecuteQueryDT(sql);
                this.cbFactory.DisplayMember = "id";
                this.cbFactory.ValueMember = "id";
            }
            catch (DBException ex)
            {
                MessageBox.Show("数据加载失败");
            }
        }
        /// <summary>
        /// 加载库存地
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cbFactory_SelectedIndexChanged(object sender, EventArgs e)
        {
            string sql = "select Stock_ID as id from Stock WHERE Factory_ID = '" + this.cbFactory.SelectedValue + "'";
            try
            {
                this.cbStock.DataSource = DBHelper.ExecuteQueryDT(sql);
                this.cbStock.DisplayMember = "id";
                this.cbStock.ValueMember = "id";
            }
            catch (DBException ex)
            {
                MessageBox.Show("数据加载失败");
            }
        }

        private void pageTool_OnPageChanged(object sender, EventArgs e)
        {
            LoadData("");
        }
        private void LoadData(string condition)
        {
            try
            {
                demandGridView.DataSource = FindCountryInforByPage(this.pageTool.PageSize, this.pageTool.PageIndex, condition);
                fillinformation();
            }
            catch (BllException ex)
            {
                MessageUtil.ShowError(ex.Message);
            }
        }

        private object FindCountryInforByPage(int pageSize, int pageIndex, string condition)
        {
            try
            {
                if (!string.IsNullOrEmpty(condition))
                {
                    condition += "  and  ";
                }
                string sql = @"SELECT top " + pageSize + " Demand_ID as 需求单号 ,Purchase_Type as 采购类型,Proposer_ID as 申请人,State as 申请状态 ,Create_Time as 申请日期, flag as 汇总状态 from Summary_Demand  where flag = 0 and " + condition + " Demand_ID not in(select top " + pageSize * (pageIndex - 1) + " Demand_ID from Summary_Demand ORDER BY Demand_ID ASC)ORDER BY Demand_ID";
                return DBHelper.ExecuteQueryDT(sql);
            }
            catch (DBException ex)
            {
                throw new BllException("当前无数据", ex);
            }
        }

        private void pageTool_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(pageCondition))
            {
                pageCondition = " and " + pageCondition;
            }
            string sql = "SELECT count(*) from Summary_Demand where flag = 0 " + pageCondition;
            try
            {
                int i = int.Parse(DBHelper.ExecuteQueryDT(sql).Rows[0][0].ToString());
                int totalNum = i;
                pageTool.DrawControl(totalNum);
                //事件改变调用函数
                pageTool.OnPageChanged += pageTool_OnPageChanged;
            }
            catch (Lib.Common.MMCException.Bll.BllException ex)
            {
                MessageUtil.ShowTips("数据库异常，请稍后重试");
                return;
            }
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            AggregateMaterial_Load(sender, e);
        }
        /// <summary>
        /// 当点击鼠标是刷出对应的wuliaoTable
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void demandGridView_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                if(demandGridView.CurrentRow.Cells[0].Value!=null)
                {
                    string Demand_ID = demandGridView.CurrentRow.Cells[0].Value.ToString();
                    if(!string.IsNullOrEmpty(Demand_ID))
                    {
                        string sql = "select Material_ID as 物料编号,Material_Name as 物料名称,Demand_Count as 需求数量,Measurement as 计量单位 from Demand_Material where Demand_ID='" + Demand_ID + "'";
                        DataTable dt = DBHelper.ExecuteQueryDT(sql);
                        this.materialGridView.DataSource = dt;
                        int sum = 0;
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            try
                            {
                                int a = Convert.ToInt32(dt.Rows[i][2].ToString());
                                sum += a;
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("数字转换异常");
                            }
                        }
                        if (sum != 0)
                        {
                            this.result.Text = sum + "";
                        }
                    }
                }
                
               
            }
            catch(Exception exc)
            {
                MessageBox.Show("数据加载失败");
                return;
            }
           
        }
        /// <summary>
        /// 生成汇总信息
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSumPlans_Click(object sender, EventArgs e)
        {
            if (this.result.Text == "0")
            {
                MessageBox.Show("至少有一条记录");
                return;
            }
           
            try
            {
                if(demandGridView.CurrentRow.Cells[0].Value!=null)
                {
                    String demandId = demandGridView.CurrentRow.Cells[0].Value.ToString();
                    if(!string.IsNullOrEmpty(demandId))
                    {
                        MessageBoxButtons messButton = MessageBoxButtons.OKCancel;
                        DialogResult dr = MessageBox.Show("生成汇总信息。\r\r是否确定生成汇总?", "", messButton);
                        if (dr == DialogResult.OK)
                        {
                            string sql = "UPDATE Summary_Demand set flag = '1',Demand_Count ='" + this.result.Text + "' where Demand_ID = '" + demandId + "'";
                            DBHelper.ExecuteQueryDS(sql);
                            MessageBox.Show("汇总成功");
                            LoadData("");
                            sql = null;
                        }
                       
                    }
                }
            }
            catch (DBException ex)
            {
                MessageBox.Show("汇总失败");
            }

        }

        private void fillinformation()
        {
            try
            {
                if (demandGridView.Rows[0].Cells[0].Value != null)
                {
                    string Demand_ID = demandGridView.Rows[0].Cells[0].Value.ToString();
                    
                        string sql = "select Material_ID as 物料编号,Material_Name as 物料名称,Demand_Count as 需求数量,Measurement as 计量单位 from Demand_Material where Demand_ID='" + Demand_ID + "'";
                        DataTable dt = DBHelper.ExecuteQueryDT(sql);
                        this.materialGridView.DataSource = dt;
                        int sum = 0;
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            try
                            {
                                int a = Convert.ToInt32(dt.Rows[i][2].ToString());
                                sum += a;
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("数字转换异常");
                            }
                        }
                        if (sum != 0)
                        {
                            this.result.Text = sum + "";
                        }
                    
                }


            }
            catch (Exception exc)
            {
                MessageBox.Show("数据加载失败");
                return;
            }
        }

        private void demandGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
