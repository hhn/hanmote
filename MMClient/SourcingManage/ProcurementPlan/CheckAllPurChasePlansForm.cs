﻿using Lib.Common.CommonUtils;
using Lib.Common.MMCException.Bll;
using Lib.Common.MMCException.IDAL;
using Lib.SqlServerDAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MMClient.SourcingManage.ProcurementPlan
{
    public partial class CheckAllPurChasePlansForm : Form
    {
        private string pageCondition = "";
        public CheckAllPurChasePlansForm()
        {
            InitializeComponent();
        }

        private void pageTool_Load(object sender, EventArgs e)
        {
            if(!string.IsNullOrEmpty(pageCondition))
            {
                pageCondition = " where " + pageCondition;
            }
            string sql = "SELECT count(Material_ID)  FROM Demand_Material "+ pageCondition + " GROUP BY Material_ID " ;
            try
            {
                DataTable dt = DBHelper.ExecuteQueryDT(sql);
                pageTool.DrawControl(dt.Rows.Count);
                //事件改变调用函数
                pageTool.OnPageChanged += pageTool_OnPageChanged;
            }
            catch (Lib.Common.MMCException.Bll.BllException ex)
            {
                MessageBox.Show("数据库异常，请稍后重试");
                return;
            }
        }

        private void pageTool_OnPageChanged(object sender, EventArgs e)
        {
            LoadData("");
        }
        private void LoadData(string condition)
        {
            try
            {
                aggregateGridView.DataSource = FindCountryInforByPage(this.pageTool.PageSize, this.pageTool.PageIndex,condition);
                filldetails();
            }
            catch (BllException ex)
            {
                MessageUtil.ShowError(ex.Message);
            }
        }

        private object FindCountryInforByPage(int pageSize, int pageIndex,string condition)
        {
            try
            {
                if(!string.IsNullOrEmpty(condition))
                {
                    condition += "  and  ";
                }
                string sql = @"SELECT top " + pageSize + "   Material_ID as 物料编号,Material_Name as 物料名称,Material_Group as 物料组,Factory_ID as 工厂编号,Stock_ID as 库存编号, Measurement  as 计量单位, sum(Demand_Count) as   需求数量 from Demand_Material  where " + condition + " Aggregate_ID not in(select top " + pageSize * (pageIndex - 1) + " Aggregate_ID from Aggregate_Material ORDER BY Aggregate_ID ASC) GROUP BY Material_ID,Material_Group,Material_Name,Measurement,Factory_ID,Stock_ID ORDER BY sum(Demand_Count) DESC";
                return DBHelper.ExecuteQueryDT(sql);
            }
            catch (DBException ex)
            {
                throw new BllException("当前无数据", ex);
            }
        }

        private void CheckAllPurChasePlansForm_Load(object sender, EventArgs e)
        {
            loadCBMData();
            LoadData("");
        }
        /// <summary>
        /// 加载数据填充
        /// </summary>
        private void loadCBMData()
        {
            string sql = "select Factory_ID as id from Factory";
            try
            {
                this.cbFactory.DataSource = DBHelper.ExecuteQueryDT(sql);
                this.cbFactory.DisplayMember = "id";
                this.cbFactory.ValueMember = "id";
            }
            catch (DBException ex)
            {
                MessageBox.Show("数据加载失败");
            }
        }

        /// <summary>
        /// 模糊查询
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnQuery_Click(object sender, EventArgs e)
        {
            string condition = "";
            if(!string.IsNullOrEmpty(this.cbFactory.Text.ToString()))
            {
                condition += " Factory_ID = '" + this.cbFactory.Text.ToString() + "'  ";
            }
            if (!string.IsNullOrEmpty(this.cbStock.Text.ToString()))
            {
                condition += " and Stock_ID = '" + this.cbStock.Text.ToString() + "' ";
            }
            pageCondition = condition ;
            LoadData(condition);
            pageTool_Load(sender,e);
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            pageCondition = "";
            LoadData("");
            pageTool_Load(sender, e);
        }

        private void aggregateGridView_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (aggregateGridView.Rows.Count == 0) return;
            if(aggregateGridView.CurrentRow.Cells[0].Value!=null)
            {
                string Material_ID = aggregateGridView.CurrentRow.Cells[0].Value.ToString();
                this.详情.Text = Material_ID + "编号对应详情如下：";
                if (!string.IsNullOrEmpty(Material_ID))
                {
                    string sql = @"SELECT
	                        Demand_ID as 需求单号,
	                        Material_Name AS 物料名称,
	                        Material_Group AS 物料组,
	                        Factory_ID AS 工厂编号,
	                        Stock_ID AS 库存编号,
	                        Measurement AS 计量单位,
	                         Demand_Count AS 需求数量
                        FROM
	                        Demand_Material 
                        where Material_ID ='"+Material_ID+"' ";
                    try
                    {
                        this.dataGridView1.DataSource = DBHelper.ExecuteQueryDT(sql);

                    }catch(DBException exc)
                    {
                        MessageBox.Show("数据加载失败");
                    }
                }
            }
            
        }

        private void cbFactory_TextChanged(object sender, EventArgs e)
        {
            string sql = "select Stock_ID as id from Stock WHERE Factory_ID = '" + this.cbFactory.SelectedValue + "'";
            try
            {
                this.cbStock.DataSource = DBHelper.ExecuteQueryDT(sql);
                this.cbStock.DisplayMember = "id";
                this.cbStock.ValueMember = "id";
            }
            catch (DBException ex)
            {
                MessageBox.Show("数据加载失败");
            }
        }


        private void filldetails()
        {
            if (aggregateGridView.Rows.Count == 0) return;
            if (aggregateGridView.Rows[0].Cells[0].Value != null)
            {
                string Material_ID = aggregateGridView.Rows[0].Cells[0].Value.ToString();
                this.详情.Text = Material_ID + "编号对应详情如下：";
                if (!string.IsNullOrEmpty(Material_ID))
                {
                    string sql = @"SELECT
	                        Demand_ID as 需求单号,
	                        Material_Name AS 物料名称,
	                        Material_Group AS 物料组,
	                        Factory_ID AS 工厂编号,
	                        Stock_ID AS 库存编号,
	                        Measurement AS 计量单位,
	                         Demand_Count AS 需求数量
                        FROM
	                        Demand_Material 
                        where Material_ID ='" + Material_ID + "' ";
                    try
                    {
                        this.dataGridView1.DataSource = DBHelper.ExecuteQueryDT(sql);

                    }
                    catch (DBException exc)
                    {
                        MessageBox.Show("数据加载失败");
                    }
                }
            }
        }
    }
}
