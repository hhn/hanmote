﻿namespace MMClient.SourcingManage.ProcurementPlan
{
    partial class AuditCommand
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.shkl_tb = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.cancel_bt = new System.Windows.Forms.Button();
            this.submit_bt = new System.Windows.Forms.Button();
            this.shzh_tb = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // shkl_tb
            // 
            this.shkl_tb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.shkl_tb.Location = new System.Drawing.Point(139, 73);
            this.shkl_tb.Name = "shkl_tb";
            this.shkl_tb.PasswordChar = '*';
            this.shkl_tb.Size = new System.Drawing.Size(161, 23);
            this.shkl_tb.TabIndex = 168;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("宋体", 9F);
            this.label12.Location = new System.Drawing.Point(68, 78);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(65, 12);
            this.label12.TabIndex = 167;
            this.label12.Text = "审核口令：";
            // 
            // cancel_bt
            // 
            this.cancel_bt.Location = new System.Drawing.Point(219, 116);
            this.cancel_bt.Name = "cancel_bt";
            this.cancel_bt.Size = new System.Drawing.Size(100, 30);
            this.cancel_bt.TabIndex = 170;
            this.cancel_bt.Text = "取消";
            this.cancel_bt.UseVisualStyleBackColor = true;
            this.cancel_bt.Click += new System.EventHandler(this.cancel_bt_Click);
            // 
            // submit_bt
            // 
            this.submit_bt.Location = new System.Drawing.Point(57, 116);
            this.submit_bt.Name = "submit_bt";
            this.submit_bt.Size = new System.Drawing.Size(100, 30);
            this.submit_bt.TabIndex = 169;
            this.submit_bt.Text = "确定";
            this.submit_bt.UseVisualStyleBackColor = true;
            this.submit_bt.Click += new System.EventHandler(this.submit_bt_Click);
            // 
            // shzh_tb
            // 
            this.shzh_tb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.shzh_tb.Location = new System.Drawing.Point(139, 30);
            this.shzh_tb.Name = "shzh_tb";
            this.shzh_tb.Size = new System.Drawing.Size(161, 23);
            this.shzh_tb.TabIndex = 172;
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("宋体", 9F);
            this.label1.Location = new System.Drawing.Point(68, 35);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(65, 12);
            this.label1.TabIndex = 171;
            this.label1.Text = "审核账户：";
            // 
            // AuditCommand
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(371, 167);
            this.Controls.Add(this.shzh_tb);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cancel_bt);
            this.Controls.Add(this.submit_bt);
            this.Controls.Add(this.shkl_tb);
            this.Controls.Add(this.label12);
            this.Name = "AuditCommand";
            this.Text = "AuditCommand";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox shkl_tb;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button cancel_bt;
        private System.Windows.Forms.Button submit_bt;
        private System.Windows.Forms.TextBox shzh_tb;
        private System.Windows.Forms.Label label1;
    }
}