﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;
using Lib.Model.MD.MT;
using Lib.Model.SourcingManage.ProcurementPlan;
using Lib.Bll.SourcingManage.ProcurementPlan;
using Lib.Bll.MDBll.General;
using Lib.Common.MMCException.IDAL;
using Lib.SqlServerDAL;

namespace MMClient.SourcingManage.ProcurementPlan
{
    public partial class NewSummaryDemand : Form
    {
        //工具类
        private ConvenientTools Tools = new ConvenientTools();
        private ImportPurcharsePlansApplicationForm importPurcharsePlansApplicationForm = null;
        private Summary_DemandBLL demandBll = new Summary_DemandBLL();
        private GeneralBLL generallBll = new GeneralBLL();

        public NewSummaryDemand()
        {
            InitializeComponent();
            initData();
        }

        /// <summary>
        /// 初始化数据
        /// </summary>
        private void initData()
        {
            //初始化需求单号
            demandIdText.Text = Tools.systemTimeToStr();
            materialGridView.TopLeftHeaderCell.Value = "序号";
            //初始化工厂编号
            initFactory();
        }

        private void initFactory()
        {
            List<string> factoryID = generallBll.GetAllFactory();
            cmb_factory.DataSource = factoryID;
        }


        /// <summary>
        /// 批量添加物料
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void addBtn_Click(object sender, EventArgs e)
        {
            MaterialInfo materialInfo = null;
            if (materialInfo == null|| materialInfo.IsDisposed)
            {
                materialInfo = new MaterialInfo(this);
            }
            materialInfo.Show();
        }

        //画行号
        private void materialGridView_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            var dgv = sender as DataGridView;
            if (dgv != null)
            {
                Rectangle rect = new Rectangle(e.RowBounds.Location.X, e.RowBounds.Location.Y, (dgv.RowHeadersWidth + 12) / 2, e.RowBounds.Height);
                TextRenderer.DrawText(e.Graphics, (e.RowIndex + 1).ToString(), dgv.RowHeadersDefaultCellStyle.Font, rect, dgv.RowHeadersDefaultCellStyle.ForeColor, TextFormatFlags.VerticalCenter | TextFormatFlags.Right);
            }
        }

        /// <summary>
        /// 添加物料信息
        /// </summary>
        /// <param name="materialList"></param>
        public void fillMaterialBases(List<MaterialBase> materialList)
        {
            materialGridView.Rows.Clear();

            if (materialList != null)
            {
                try
                {
                    string sql = "select Stock_ID from Stock WHERE Factory_ID = '"+this.cmb_factory.Text.ToString() + "'";
                    DataTable dt = DBHelper.ExecuteQueryDT(sql);
                    List<string> list = new List<string>();
                    if (dt != null && dt.Rows.Count >= 1)
                    {
                        for (int j = 0; j < dt.Rows.Count; j++)
                        {
                            list.Add(dt.Rows[j][0].ToString());
                        }
                    }
                        ((DataGridViewComboBoxColumn)materialGridView.Columns["stockId"]).DataSource = list;
                }
                catch (DBException ex)
                {
                    materialGridView.Rows.Clear();
                    return;
                }
                int i = 0;
                foreach (MaterialBase mb in materialList)
                {
                    materialGridView.Rows.Add();
                    materialGridView.Rows[i].Cells["materialId"].Value = mb.Material_ID;
                    materialGridView.Rows[i].Cells["materialName"].Value = mb.Material_Name;
                    materialGridView.Rows[i].Cells["materialGroup"].Value = mb.Material_Group;
                    materialGridView.Rows[i].Cells["measurement"].Value = mb.Measurement;
                    i++;
                }
            }
        }

        /// <summary>
        /// 检查按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void checkBtn_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(purchaseTypeCB.Text.ToString()))
            {
                MessageBox.Show("采购类型不能为空", "提示");
                return;
            }
            if (string.IsNullOrEmpty(logisticsCB.Text.ToString()))
            {
                MessageBox.Show("物流方式不能为空", "提示");
                return;
            }
            if (string.IsNullOrEmpty(txtdepartment.Text.ToString()))
            {
                MessageBox.Show("部门不能为空", "提示");
                return;
            }
            if (string.IsNullOrEmpty(txt_Proposer.Text.ToString()))
            {
                MessageBox.Show("申请人不能为空", "提示");
                return;
            }
            if(string.IsNullOrEmpty(cmb_factory.Text.ToString()))
            {
                MessageBox.Show("工厂不能为空", "提示");
                return;
            }
            if (string.IsNullOrEmpty(phoneText.Text.ToString()) || phoneText.Text.ToString().Length != 11)
            {
                MessageBox.Show("手机号不合法", "提示");
                phoneText.Text = "";
                return;
            }
            foreach (DataGridViewRow dgvr in materialGridView.Rows)
            {
                if (dgvr.Cells["applyNum"].Value == null|| dgvr.Cells["applyNum"].Value.ToString().Trim().Equals(""))
                {
                    MessageBox.Show("申请数量不能为空", "提示");
                    return;
                }
                if (dgvr.Cells["materialId"].Value == null || dgvr.Cells["materialId"].Value.ToString().Trim().Equals(""))
                {
                    MessageBox.Show("物料编码不能为空", "提示");
                    return;
                }
                if (dgvr.Cells["materialName"].Value == null || dgvr.Cells["materialName"].Value.ToString().Trim().Equals(""))
                {
                    MessageBox.Show("物料名称不能为空", "提示");
                    return;
                }
                if (dgvr.Cells["materialGroup"].Value == null || dgvr.Cells["materialGroup"].Value.ToString().Trim().Equals(""))
                {
                    MessageBox.Show("物料组不能为空", "提示");
                    return;
                }
                if (dgvr.Cells["measurement"].Value == null || dgvr.Cells["measurement"].Value.ToString().Trim().Equals(""))
                {
                    MessageBox.Show("单位名称不能为空", "提示");
                    return;
                }
                if (dgvr.Cells["stockId"].Value == null || dgvr.Cells["stockId"].Value.ToString().Trim().Equals(""))
                {
                    MessageBox.Show("库存编号不能为空", "提示");
                    return;
                }
                if (dgvr.Cells["DeliveryStartTime"].Value == null || dgvr.Cells["DeliveryStartTime"].Value.ToString().Trim().Equals(""))
                {
                    MessageBox.Show("交货开始时间不能为空", "提示");
                    return;
                }
                if (dgvr.Cells["DeliveryEndTime"].Value == null || dgvr.Cells["DeliveryEndTime"].Value.ToString().Trim().Equals(""))
                {
                    MessageBox.Show("交货结束时间不能为空", "提示");
                    return;
                }

            }
            if (phoneText.Text.ToString().Trim().Equals(""))
            {
                MessageBox.Show("联系电话不能为空","提示");
                return;
            }
            MessageBox.Show("申请信息无误");
            submitBtn.Visible = true;
        }

        /// <summary>
        /// 保存按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void submitBtn_Click(object sender, EventArgs e)
        {
            string  MessageFail = "";
            Summary_Demand demand = new Summary_Demand();
            Demand_MaterialBLL demandMaterialBll = new Demand_MaterialBLL();
            demand.Demand_ID = demandIdText.Text;
            demand.Purchase_Type = purchaseTypeCB.Text;
            demand.LogisticsMode = logisticsCB.Text;
            demand.Department = txtdepartment.Text;
            demand.Proposer_ID = txt_Proposer.Text;
            demand.PhoneNum = phoneText.Text;
            demand.Create_Time = DateTime.Parse(dtp_apply.Text); 
            demand.State = "待审核";
            string insertsql = @"INSERT INTO dbo.Summary_Demand (
	                        Demand_ID,
	                        Purchase_Type,
	                        State,
	                        Create_Time,
	                        PhoneNum,
	                        Department,
	                        LogisticsMode,
                            Proposer_ID
                        )
                        VALUES('" + demand.Demand_ID + "','"+ demand.Purchase_Type + "','"+ demand.State + "','"+ demand.Create_Time + "','"+ demand.PhoneNum + "','"+ demand.Department + "','"+ demand.LogisticsMode + "','"+ demand.Proposer_ID + "')";
            try
            {
                DBHelper.ExecuteNonQuery(insertsql);
            } catch (DBException ec0)
            {
                MessageBox.Show("需求订单插入失败！！！");
            }
            foreach (DataGridViewRow dgvr in materialGridView.Rows)
            {
                Demand_Material demand_material = new Demand_Material();
                demand_material.Demand_ID = demandIdText.Text;
                demand_material.Material_ID = dgvr.Cells["materialId"].Value.ToString();
                demand_material.Material_Name = dgvr.Cells["materialName"].Value.ToString();
                demand_material.Material_Group = dgvr.Cells["materialGroup"].Value.ToString();
                demand_material.Demand_Count = Convert.ToInt32(dgvr.Cells["applyNum"].Value.ToString());
                demand_material.Measurement = dgvr.Cells["measurement"].Value.ToString();
                demand_material.Stock_ID = dgvr.Cells["stockId"].Value.ToString();
                demand_material.Factory_ID = cmb_factory.Text;
                demand_material.DeliveryStartTime = DateTime.Parse(dgvr.Cells["DeliveryStartTime"].Value.ToString());
                demand_material.DeliveryEndTime = DateTime.Parse(dgvr.Cells["DeliveryEndTime"].Value.ToString());
                //将需求计划添加到数据库
                try
                {
                    demandMaterialBll.addSummaryMaterials(demand_material);

                }catch(DBException ex)
                {
                    MessageFail += demandIdText.Text+",";
                    continue;
                }
            }
            if(MessageFail=="")
            {
                MessageBox.Show("需求计划新建成功", "提示");
                this.Close();
            }
            else
            {
                MessageBox.Show("订单"+ MessageFail + "创建失败:", "提示");
            }
           
        }

        private void chk_DeliveryTime_CheckedChanged(object sender, EventArgs e)
        {
            if (chk_DeliveryTime.CheckState==CheckState.Checked)
            {
                for (int i = 1; i < materialGridView.Rows.Count; i++)
                {
                    materialGridView.Rows[i].Cells["DeliveryStartTime"].Value = DateTime.Parse(materialGridView.Rows[0].Cells["DeliveryStartTime"].Value.ToString());
                    materialGridView.Rows[i].Cells["DeliveryEndTime"].Value = DateTime.Parse(materialGridView.Rows[0].Cells["DeliveryEndTime"].Value.ToString());
                }
            }
            if (chk_DeliveryTime.CheckState == CheckState.Unchecked)
            {
                for (int i = 1; i < materialGridView.Rows.Count; i++)
                {
                    materialGridView.Rows[i].Cells["DeliveryStartTime"].Value = null;
                    materialGridView.Rows[i].Cells["DeliveryEndTime"].Value = null;
                }
            }
        }
        /// <summary>
        /// 外部导入采购计划
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnImportData_Click(object sender, EventArgs e)
        {
            if (importPurcharsePlansApplicationForm == null || importPurcharsePlansApplicationForm.IsDisposed)
            {
                importPurcharsePlansApplicationForm = new ImportPurcharsePlansApplicationForm();
            }
            importPurcharsePlansApplicationForm.Show();
            this.Close();
           
        }
    }
}
