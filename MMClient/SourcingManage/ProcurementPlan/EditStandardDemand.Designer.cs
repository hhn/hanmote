﻿namespace MMClient.SourcingManage.ProcurementPlan
{
    partial class EditStandardDemand
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.materialGridView = new System.Windows.Forms.DataGridView();
            this.addBtn = new System.Windows.Forms.Button();
            this.logisticsCB = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.purchaseTypeCB = new System.Windows.Forms.ComboBox();
            this.purchaseTypeLabel = new System.Windows.Forms.Label();
            this.applyTime = new System.Windows.Forms.Label();
            this.txt_Proposer = new System.Windows.Forms.TextBox();
            this.proposer = new System.Windows.Forms.Label();
            this.departmentLabel = new System.Windows.Forms.Label();
            this.phoneText = new System.Windows.Forms.TextBox();
            this.phoneLabel = new System.Windows.Forms.Label();
            this.demandIdText = new System.Windows.Forms.TextBox();
            this.demandIdLabel = new System.Windows.Forms.Label();
            this.btn_allDelete = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.submitBtn = new System.Windows.Forms.Button();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.calendarColumn1 = new Lib.Common.CommonControls.CalendarColumn();
            this.calendarColumn2 = new Lib.Common.CommonControls.CalendarColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txt_ApplyTime = new System.Windows.Forms.TextBox();
            this.txtdepartment = new System.Windows.Forms.TextBox();
            this.materialId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.materialName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.materialGroup = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.applyNum = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.measurement = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Factory_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.stockId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DeliveryStartTime = new Lib.Common.CommonControls.CalendarColumn();
            this.DeliveryEndTime = new Lib.Common.CommonControls.CalendarColumn();
            this.删除 = new System.Windows.Forms.DataGridViewLinkColumn();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.materialGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.materialGridView);
            this.groupBox1.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.groupBox1.Location = new System.Drawing.Point(19, 162);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox1.Size = new System.Drawing.Size(1106, 344);
            this.groupBox1.TabIndex = 54;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "物料信息";
            // 
            // materialGridView
            // 
            this.materialGridView.AllowUserToAddRows = false;
            this.materialGridView.BackgroundColor = System.Drawing.SystemColors.Control;
            this.materialGridView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.materialGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.materialGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.materialGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.materialId,
            this.materialName,
            this.materialGroup,
            this.applyNum,
            this.measurement,
            this.Factory_ID,
            this.stockId,
            this.DeliveryStartTime,
            this.DeliveryEndTime,
            this.删除});
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.materialGridView.DefaultCellStyle = dataGridViewCellStyle9;
            this.materialGridView.EnableHeadersVisualStyles = false;
            this.materialGridView.Location = new System.Drawing.Point(13, 15);
            this.materialGridView.Margin = new System.Windows.Forms.Padding(2);
            this.materialGridView.Name = "materialGridView";
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.materialGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle10;
            this.materialGridView.RowTemplate.Height = 27;
            this.materialGridView.Size = new System.Drawing.Size(1093, 325);
            this.materialGridView.TabIndex = 2;
            this.materialGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.materialGridView_CellContentClick_1);
            // 
            // addBtn
            // 
            this.addBtn.Location = new System.Drawing.Point(18, 525);
            this.addBtn.Margin = new System.Windows.Forms.Padding(2);
            this.addBtn.Name = "addBtn";
            this.addBtn.Size = new System.Drawing.Size(64, 23);
            this.addBtn.TabIndex = 53;
            this.addBtn.Text = "批量添加";
            this.addBtn.UseVisualStyleBackColor = true;
            this.addBtn.Click += new System.EventHandler(this.addBtn_Click);
            // 
            // logisticsCB
            // 
            this.logisticsCB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.logisticsCB.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.logisticsCB.FormattingEnabled = true;
            this.logisticsCB.Items.AddRange(new object[] {
            "物流中心式",
            "统谈统签直送式",
            "统谈分签式"});
            this.logisticsCB.Location = new System.Drawing.Point(319, 45);
            this.logisticsCB.Margin = new System.Windows.Forms.Padding(2);
            this.logisticsCB.Name = "logisticsCB";
            this.logisticsCB.Size = new System.Drawing.Size(151, 22);
            this.logisticsCB.TabIndex = 51;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(265, 49);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 50;
            this.label1.Text = "物流方式";
            // 
            // purchaseTypeCB
            // 
            this.purchaseTypeCB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.purchaseTypeCB.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.purchaseTypeCB.FormattingEnabled = true;
            this.purchaseTypeCB.Items.AddRange(new object[] {
            "标准采购类型",
            "框架协议采购类型",
            "委外加工采购类型",
            "寄售采购类型"});
            this.purchaseTypeCB.Location = new System.Drawing.Point(72, 45);
            this.purchaseTypeCB.Margin = new System.Windows.Forms.Padding(2);
            this.purchaseTypeCB.Name = "purchaseTypeCB";
            this.purchaseTypeCB.Size = new System.Drawing.Size(151, 22);
            this.purchaseTypeCB.TabIndex = 49;
            // 
            // purchaseTypeLabel
            // 
            this.purchaseTypeLabel.AutoSize = true;
            this.purchaseTypeLabel.Location = new System.Drawing.Point(16, 49);
            this.purchaseTypeLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.purchaseTypeLabel.Name = "purchaseTypeLabel";
            this.purchaseTypeLabel.Size = new System.Drawing.Size(53, 12);
            this.purchaseTypeLabel.TabIndex = 48;
            this.purchaseTypeLabel.Text = "采购类型";
            // 
            // applyTime
            // 
            this.applyTime.AutoSize = true;
            this.applyTime.Location = new System.Drawing.Point(711, 135);
            this.applyTime.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.applyTime.Name = "applyTime";
            this.applyTime.Size = new System.Drawing.Size(53, 12);
            this.applyTime.TabIndex = 47;
            this.applyTime.Text = "申请日期";
            // 
            // txt_Proposer
            // 
            this.txt_Proposer.Location = new System.Drawing.Point(373, 130);
            this.txt_Proposer.Margin = new System.Windows.Forms.Padding(2);
            this.txt_Proposer.Name = "txt_Proposer";
            this.txt_Proposer.Size = new System.Drawing.Size(64, 21);
            this.txt_Proposer.TabIndex = 45;
            // 
            // proposer
            // 
            this.proposer.AutoSize = true;
            this.proposer.Location = new System.Drawing.Point(329, 133);
            this.proposer.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.proposer.Name = "proposer";
            this.proposer.Size = new System.Drawing.Size(41, 12);
            this.proposer.TabIndex = 44;
            this.proposer.Text = "申请人";
            // 
            // departmentLabel
            // 
            this.departmentLabel.AutoSize = true;
            this.departmentLabel.Location = new System.Drawing.Point(16, 132);
            this.departmentLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.departmentLabel.Name = "departmentLabel";
            this.departmentLabel.Size = new System.Drawing.Size(53, 12);
            this.departmentLabel.TabIndex = 43;
            this.departmentLabel.Text = "申请部门";
            // 
            // phoneText
            // 
            this.phoneText.Location = new System.Drawing.Point(554, 130);
            this.phoneText.Margin = new System.Windows.Forms.Padding(2);
            this.phoneText.Name = "phoneText";
            this.phoneText.Size = new System.Drawing.Size(115, 21);
            this.phoneText.TabIndex = 42;
            // 
            // phoneLabel
            // 
            this.phoneLabel.AutoSize = true;
            this.phoneLabel.Location = new System.Drawing.Point(498, 135);
            this.phoneLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.phoneLabel.Name = "phoneLabel";
            this.phoneLabel.Size = new System.Drawing.Size(53, 12);
            this.phoneLabel.TabIndex = 41;
            this.phoneLabel.Text = "联系电话";
            // 
            // demandIdText
            // 
            this.demandIdText.Location = new System.Drawing.Point(72, 88);
            this.demandIdText.Margin = new System.Windows.Forms.Padding(2);
            this.demandIdText.Name = "demandIdText";
            this.demandIdText.ReadOnly = true;
            this.demandIdText.Size = new System.Drawing.Size(203, 21);
            this.demandIdText.TabIndex = 40;
            // 
            // demandIdLabel
            // 
            this.demandIdLabel.AutoSize = true;
            this.demandIdLabel.Location = new System.Drawing.Point(17, 90);
            this.demandIdLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.demandIdLabel.Name = "demandIdLabel";
            this.demandIdLabel.Size = new System.Drawing.Size(53, 12);
            this.demandIdLabel.TabIndex = 39;
            this.demandIdLabel.Text = "需求单号";
            // 
            // btn_allDelete
            // 
            this.btn_allDelete.Location = new System.Drawing.Point(962, 525);
            this.btn_allDelete.Name = "btn_allDelete";
            this.btn_allDelete.Size = new System.Drawing.Size(75, 23);
            this.btn_allDelete.TabIndex = 55;
            this.btn_allDelete.Text = "全部删除";
            this.btn_allDelete.UseVisualStyleBackColor = true;
            this.btn_allDelete.Click += new System.EventHandler(this.btn_allDelete_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(19, 12);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(64, 23);
            this.button1.TabIndex = 56;
            this.button1.Text = "检查";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // submitBtn
            // 
            this.submitBtn.Location = new System.Drawing.Point(103, 12);
            this.submitBtn.Name = "submitBtn";
            this.submitBtn.Size = new System.Drawing.Size(61, 23);
            this.submitBtn.TabIndex = 57;
            this.submitBtn.Text = "保存修改";
            this.submitBtn.UseVisualStyleBackColor = true;
            this.submitBtn.Visible = false;
            this.submitBtn.Click += new System.EventHandler(this.submitBtn_Click);
            // 
            // dataGridViewTextBoxColumn1
            // 
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle11;
            this.dataGridViewTextBoxColumn1.HeaderText = "物料编号";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn1.Width = 150;
            // 
            // dataGridViewTextBoxColumn2
            // 
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewTextBoxColumn2.DefaultCellStyle = dataGridViewCellStyle12;
            this.dataGridViewTextBoxColumn2.HeaderText = "物料名称";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.Width = 150;
            // 
            // dataGridViewTextBoxColumn3
            // 
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewTextBoxColumn3.DefaultCellStyle = dataGridViewCellStyle13;
            this.dataGridViewTextBoxColumn3.HeaderText = "物料组";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // dataGridViewTextBoxColumn4
            // 
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewTextBoxColumn4.DefaultCellStyle = dataGridViewCellStyle14;
            this.dataGridViewTextBoxColumn4.HeaderText = "申请数量";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            // 
            // dataGridViewTextBoxColumn5
            // 
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewTextBoxColumn5.DefaultCellStyle = dataGridViewCellStyle15;
            this.dataGridViewTextBoxColumn5.HeaderText = "单位";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.Width = 70;
            // 
            // dataGridViewTextBoxColumn6
            // 
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewTextBoxColumn6.DefaultCellStyle = dataGridViewCellStyle16;
            this.dataGridViewTextBoxColumn6.HeaderText = "仓库编号";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            // 
            // calendarColumn1
            // 
            this.calendarColumn1.HeaderText = "交易开始时间";
            this.calendarColumn1.Name = "calendarColumn1";
            this.calendarColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.calendarColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.calendarColumn1.Width = 140;
            // 
            // calendarColumn2
            // 
            this.calendarColumn2.HeaderText = "交易结束时间";
            this.calendarColumn2.Name = "calendarColumn2";
            this.calendarColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.calendarColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.calendarColumn2.Width = 140;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.HeaderText = "工厂编号";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            // 
            // txt_ApplyTime
            // 
            this.txt_ApplyTime.Location = new System.Drawing.Point(773, 130);
            this.txt_ApplyTime.Name = "txt_ApplyTime";
            this.txt_ApplyTime.ReadOnly = true;
            this.txt_ApplyTime.Size = new System.Drawing.Size(154, 21);
            this.txt_ApplyTime.TabIndex = 60;
            // 
            // txtdepartment
            // 
            this.txtdepartment.Location = new System.Drawing.Point(75, 129);
            this.txtdepartment.Name = "txtdepartment";
            this.txtdepartment.Size = new System.Drawing.Size(200, 21);
            this.txtdepartment.TabIndex = 61;
            // 
            // materialId
            // 
            this.materialId.DataPropertyName = "materialId";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.materialId.DefaultCellStyle = dataGridViewCellStyle2;
            this.materialId.Frozen = true;
            this.materialId.HeaderText = "物料编号";
            this.materialId.Name = "materialId";
            this.materialId.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.materialId.Width = 150;
            // 
            // materialName
            // 
            this.materialName.DataPropertyName = "materialName";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.materialName.DefaultCellStyle = dataGridViewCellStyle3;
            this.materialName.Frozen = true;
            this.materialName.HeaderText = "物料名称";
            this.materialName.Name = "materialName";
            // 
            // materialGroup
            // 
            this.materialGroup.DataPropertyName = "materialGroup";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.materialGroup.DefaultCellStyle = dataGridViewCellStyle4;
            this.materialGroup.Frozen = true;
            this.materialGroup.HeaderText = "物料组";
            this.materialGroup.Name = "materialGroup";
            // 
            // applyNum
            // 
            this.applyNum.DataPropertyName = "applyNum";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.applyNum.DefaultCellStyle = dataGridViewCellStyle5;
            this.applyNum.Frozen = true;
            this.applyNum.HeaderText = "申请数量";
            this.applyNum.Name = "applyNum";
            // 
            // measurement
            // 
            this.measurement.DataPropertyName = "measurement";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.measurement.DefaultCellStyle = dataGridViewCellStyle6;
            this.measurement.Frozen = true;
            this.measurement.HeaderText = "单位";
            this.measurement.Name = "measurement";
            this.measurement.Width = 70;
            // 
            // Factory_ID
            // 
            this.Factory_ID.DataPropertyName = "Factory_ID";
            this.Factory_ID.Frozen = true;
            this.Factory_ID.HeaderText = "工厂编号";
            this.Factory_ID.Name = "Factory_ID";
            // 
            // stockId
            // 
            this.stockId.DataPropertyName = "stockId";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.stockId.DefaultCellStyle = dataGridViewCellStyle7;
            this.stockId.Frozen = true;
            this.stockId.HeaderText = "仓库编号";
            this.stockId.Name = "stockId";
            this.stockId.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // DeliveryStartTime
            // 
            this.DeliveryStartTime.DataPropertyName = "DeliveryStartTime";
            this.DeliveryStartTime.Frozen = true;
            this.DeliveryStartTime.HeaderText = "交易开始时间";
            this.DeliveryStartTime.Name = "DeliveryStartTime";
            this.DeliveryStartTime.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.DeliveryStartTime.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.DeliveryStartTime.Width = 120;
            // 
            // DeliveryEndTime
            // 
            this.DeliveryEndTime.DataPropertyName = "DeliveryEndTime";
            this.DeliveryEndTime.Frozen = true;
            this.DeliveryEndTime.HeaderText = "交易结束时间";
            this.DeliveryEndTime.Name = "DeliveryEndTime";
            this.DeliveryEndTime.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.DeliveryEndTime.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.DeliveryEndTime.Width = 120;
            // 
            // 删除
            // 
            dataGridViewCellStyle8.NullValue = "删除";
            this.删除.DefaultCellStyle = dataGridViewCellStyle8;
            this.删除.Frozen = true;
            this.删除.HeaderText = "操作";
            this.删除.Name = "删除";
            this.删除.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // EditStandardDemand
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1149, 578);
            this.Controls.Add(this.txtdepartment);
            this.Controls.Add(this.txt_ApplyTime);
            this.Controls.Add(this.submitBtn);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btn_allDelete);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.addBtn);
            this.Controls.Add(this.logisticsCB);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.purchaseTypeCB);
            this.Controls.Add(this.purchaseTypeLabel);
            this.Controls.Add(this.applyTime);
            this.Controls.Add(this.txt_Proposer);
            this.Controls.Add(this.proposer);
            this.Controls.Add(this.departmentLabel);
            this.Controls.Add(this.phoneText);
            this.Controls.Add(this.phoneLabel);
            this.Controls.Add(this.demandIdText);
            this.Controls.Add(this.demandIdLabel);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "EditStandardDemand";
            this.Text = "修改采购计划";
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.materialGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView materialGridView;
        private System.Windows.Forms.Button addBtn;
        private System.Windows.Forms.ComboBox logisticsCB;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox purchaseTypeCB;
        private System.Windows.Forms.Label purchaseTypeLabel;
        private System.Windows.Forms.Label applyTime;
        private System.Windows.Forms.TextBox txt_Proposer;
        private System.Windows.Forms.Label proposer;
        private System.Windows.Forms.Label departmentLabel;
        private System.Windows.Forms.TextBox phoneText;
        private System.Windows.Forms.Label phoneLabel;
        private System.Windows.Forms.TextBox demandIdText;
        private System.Windows.Forms.Label demandIdLabel;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.Button btn_allDelete;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button submitBtn;
        private Lib.Common.CommonControls.CalendarColumn calendarColumn1;
        private Lib.Common.CommonControls.CalendarColumn calendarColumn2;
        private System.Windows.Forms.TextBox txt_ApplyTime;
        private System.Windows.Forms.TextBox txtdepartment;
        private System.Windows.Forms.DataGridViewTextBoxColumn materialId;
        private System.Windows.Forms.DataGridViewTextBoxColumn materialName;
        private System.Windows.Forms.DataGridViewTextBoxColumn materialGroup;
        private System.Windows.Forms.DataGridViewTextBoxColumn applyNum;
        private System.Windows.Forms.DataGridViewTextBoxColumn measurement;
        private System.Windows.Forms.DataGridViewTextBoxColumn Factory_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn stockId;
        private Lib.Common.CommonControls.CalendarColumn DeliveryStartTime;
        private Lib.Common.CommonControls.CalendarColumn DeliveryEndTime;
        private System.Windows.Forms.DataGridViewLinkColumn 删除;
    }
}