﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;
using Lib.Model.SourcingManage.ProcurementPlan;
using Lib.Bll.SourcingManage.ProcurementPlan;

namespace MMClient.SourcingManage.ProcurementPlan
{
    public partial class DetailSummaryDemand : Form
    {
        
        private Summary_Demand summaryDemand;

        private Demand_MaterialBLL demandMaterialBll;

        public DetailSummaryDemand(Summary_Demand summaryDemand)
        {
            InitializeComponent();
            this.summaryDemand = summaryDemand;
            demandMaterialBll = new Demand_MaterialBLL();
            initData();
        }

        /// <summary>
        /// 初始化数据
        /// </summary>
        private void initData()
        {
            materialGridView.TopLeftHeaderCell.Value = "序号";
            txt_purchase.Text = summaryDemand.Purchase_Type;
            txt_logistic.Text = summaryDemand.LogisticsMode;
            txt_factoryID.Text = summaryDemand.Factory_ID;
            txt_demandId.Text = summaryDemand.Demand_ID;
            txt_department.Text = summaryDemand.Department;
            txt_proposer.Text = summaryDemand.Proposer_ID;
            txt_phonenum.Text = summaryDemand.PhoneNum;
            txt_applyTime.Text = summaryDemand.Create_Time.ToString("yyyy-MM-dd hh:mm:ss");
            rtb_reviewAdvice.Text = summaryDemand.ReviewAdvice.ToString();
            txtPurPrice.Text = summaryDemand.Purchase_Price;
            if (summaryDemand.State.ToString().Equals("确定采购价格"))
            {
                txt_reviewResult.Text = "审核通过";
            }
            else
            {
                txt_reviewResult.Text = summaryDemand.State.ToString();
            }
            if (txt_reviewResult.Text.Equals("审核未通过"))
            {
                btn_edit.Visible = true;
            }
            fillMaterialGridView();
        }

        /// <summary>
        /// 根据选中的需求计划展示该需求计划对应的所有物料信息
        /// </summary>
        private void fillMaterialGridView()
        {
            materialGridView.Rows.Clear();
            List<Demand_Material> demands = demandMaterialBll.findDemandMaterials(summaryDemand.Demand_ID);
            if (demands.Count > materialGridView.Rows.Count)
            {
                materialGridView.Rows.Add(demands.Count - materialGridView.Rows.Count);
            }
            for (int i = 0; i < demands.Count; i++)
            {
                materialGridView.Rows[i].Cells["materialId"].Value = demands.ElementAt(i).Material_ID;
                materialGridView.Rows[i].Cells["materialName"].Value = demands.ElementAt(i).Material_Name;
                materialGridView.Rows[i].Cells["materialGroup"].Value = demands.ElementAt(i).Material_Group;
                materialGridView.Rows[i].Cells["applyNum"].Value = demands.ElementAt(i).Demand_Count;
                materialGridView.Rows[i].Cells["measurement"].Value = demands.ElementAt(i).Measurement;
                materialGridView.Rows[i].Cells["stockId"].Value = demands.ElementAt(i).Stock_ID;
            }
        }

        private void cancle_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
