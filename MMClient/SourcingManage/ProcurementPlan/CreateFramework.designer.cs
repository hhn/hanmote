﻿namespace MMClient.SourcingManage.ProcurementPlan
{
    partial class CreateFramework_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel2 = new System.Windows.Forms.Panel();
            this.wlmc2_cmb = new System.Windows.Forms.ComboBox();
            this.wlbh2_cmb = new System.Windows.Forms.ComboBox();
            this.label29 = new System.Windows.Forms.Label();
            this.qysj_dt = new System.Windows.Forms.DateTimePicker();
            this.label24 = new System.Windows.Forms.Label();
            this.xqdh_tb = new System.Windows.Forms.TextBox();
            this.xymc_tb = new System.Windows.Forms.TextBox();
            this.gysh_tb = new System.Windows.Forms.TextBox();
            this.xybh_tb = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.gysmc_tb = new System.Windows.Forms.TextBox();
            this.gysbh_tb = new System.Windows.Forms.TextBox();
            this.wlgg_tb = new System.Windows.Forms.TextBox();
            this.qqjg_tb = new System.Windows.Forms.TextBox();
            this.wlbz_tb = new System.Windows.Forms.TextBox();
            this.wlph_tb = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.xysl_tb = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.xylx_cmb = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.wlfs_cmb = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cglx_cmb = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.wlmc_cmb = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.wlbh_cmb = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.tgrq_dt = new System.Windows.Forms.DateTimePicker();
            this.shzt_cmb = new System.Windows.Forms.ComboBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.cgksrq_dt = new System.Windows.Forms.DateTimePicker();
            this.cgjsrq_dt = new System.Windows.Forms.DateTimePicker();
            this.hxcgpz_cmb = new System.Windows.Forms.ComboBox();
            this.label40 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.fpyz_cmb = new System.Windows.Forms.ComboBox();
            this.label37 = new System.Windows.Forms.Label();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.comboBox16 = new System.Windows.Forms.ComboBox();
            this.comboBox17 = new System.Windows.Forms.ComboBox();
            this.label34 = new System.Windows.Forms.Label();
            this.textBox18 = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.cgy1_cmb = new System.Windows.Forms.ComboBox();
            this.cgzz1_cmb = new System.Windows.Forms.ComboBox();
            this.label31 = new System.Windows.Forms.Label();
            this.fpbl1_tb = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.del_bt = new System.Windows.Forms.Button();
            this.add_bt = new System.Windows.Forms.Button();
            this.label30 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.cancel_bt = new System.Windows.Forms.Button();
            this.return_bt = new System.Windows.Forms.Button();
            this.submit_bt = new System.Windows.Forms.Button();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.wlmc2_cmb);
            this.panel2.Controls.Add(this.wlbh2_cmb);
            this.panel2.Controls.Add(this.label29);
            this.panel2.Controls.Add(this.qysj_dt);
            this.panel2.Controls.Add(this.label24);
            this.panel2.Controls.Add(this.xqdh_tb);
            this.panel2.Controls.Add(this.xymc_tb);
            this.panel2.Controls.Add(this.gysh_tb);
            this.panel2.Controls.Add(this.xybh_tb);
            this.panel2.Controls.Add(this.label15);
            this.panel2.Controls.Add(this.label25);
            this.panel2.Controls.Add(this.gysmc_tb);
            this.panel2.Controls.Add(this.gysbh_tb);
            this.panel2.Controls.Add(this.wlgg_tb);
            this.panel2.Controls.Add(this.qqjg_tb);
            this.panel2.Controls.Add(this.wlbz_tb);
            this.panel2.Controls.Add(this.wlph_tb);
            this.panel2.Controls.Add(this.label17);
            this.panel2.Controls.Add(this.label18);
            this.panel2.Controls.Add(this.label19);
            this.panel2.Controls.Add(this.label20);
            this.panel2.Controls.Add(this.label21);
            this.panel2.Controls.Add(this.label22);
            this.panel2.Controls.Add(this.xysl_tb);
            this.panel2.Controls.Add(this.label16);
            this.panel2.Controls.Add(this.label11);
            this.panel2.Controls.Add(this.label12);
            this.panel2.Controls.Add(this.label13);
            this.panel2.Controls.Add(this.label14);
            this.panel2.Controls.Add(this.xylx_cmb);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Location = new System.Drawing.Point(20, 139);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(655, 395);
            this.panel2.TabIndex = 159;
            // 
            // wlmc2_cmb
            // 
            this.wlmc2_cmb.FormattingEnabled = true;
            this.wlmc2_cmb.Location = new System.Drawing.Point(440, 218);
            this.wlmc2_cmb.Name = "wlmc2_cmb";
            this.wlmc2_cmb.Size = new System.Drawing.Size(200, 20);
            this.wlmc2_cmb.TabIndex = 158;
            // 
            // wlbh2_cmb
            // 
            this.wlbh2_cmb.FormattingEnabled = true;
            this.wlbh2_cmb.ItemHeight = 12;
            this.wlbh2_cmb.Location = new System.Drawing.Point(90, 218);
            this.wlbh2_cmb.Name = "wlbh2_cmb";
            this.wlbh2_cmb.Size = new System.Drawing.Size(200, 20);
            this.wlbh2_cmb.TabIndex = 156;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("宋体", 9F);
            this.label29.Location = new System.Drawing.Point(30, 222);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(53, 12);
            this.label29.TabIndex = 155;
            this.label29.Text = "物料编号";
            // 
            // qysj_dt
            // 
            this.qysj_dt.Location = new System.Drawing.Point(90, 113);
            this.qysj_dt.Name = "qysj_dt";
            this.qysj_dt.Size = new System.Drawing.Size(200, 21);
            this.qysj_dt.TabIndex = 154;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("宋体", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label24.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label24.Location = new System.Drawing.Point(-1, 337);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(87, 15);
            this.label24.TabIndex = 153;
            this.label24.Text = "供应商信息";
            // 
            // xqdh_tb
            // 
            this.xqdh_tb.Location = new System.Drawing.Point(90, 32);
            this.xqdh_tb.Name = "xqdh_tb";
            this.xqdh_tb.Size = new System.Drawing.Size(550, 21);
            this.xqdh_tb.TabIndex = 152;
            // 
            // xymc_tb
            // 
            this.xymc_tb.Location = new System.Drawing.Point(440, 72);
            this.xymc_tb.Name = "xymc_tb";
            this.xymc_tb.Size = new System.Drawing.Size(200, 21);
            this.xymc_tb.TabIndex = 151;
            // 
            // gysh_tb
            // 
            this.gysh_tb.Location = new System.Drawing.Point(90, 152);
            this.gysh_tb.Name = "gysh_tb";
            this.gysh_tb.Size = new System.Drawing.Size(200, 21);
            this.gysh_tb.TabIndex = 150;
            // 
            // xybh_tb
            // 
            this.xybh_tb.Location = new System.Drawing.Point(90, 72);
            this.xybh_tb.Name = "xybh_tb";
            this.xybh_tb.Size = new System.Drawing.Size(200, 21);
            this.xybh_tb.TabIndex = 148;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("宋体", 9F);
            this.label15.Location = new System.Drawing.Point(380, 422);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(53, 12);
            this.label15.TabIndex = 77;
            this.label15.Text = "通过日期";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("宋体", 9F);
            this.label25.Location = new System.Drawing.Point(30, 422);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(53, 12);
            this.label25.TabIndex = 76;
            this.label25.Text = "审核状态";
            // 
            // gysmc_tb
            // 
            this.gysmc_tb.Location = new System.Drawing.Point(440, 363);
            this.gysmc_tb.Name = "gysmc_tb";
            this.gysmc_tb.Size = new System.Drawing.Size(200, 21);
            this.gysmc_tb.TabIndex = 71;
            // 
            // gysbh_tb
            // 
            this.gysbh_tb.Location = new System.Drawing.Point(90, 363);
            this.gysbh_tb.Name = "gysbh_tb";
            this.gysbh_tb.Size = new System.Drawing.Size(200, 21);
            this.gysbh_tb.TabIndex = 70;
            // 
            // wlgg_tb
            // 
            this.wlgg_tb.Location = new System.Drawing.Point(440, 258);
            this.wlgg_tb.Name = "wlgg_tb";
            this.wlgg_tb.Size = new System.Drawing.Size(200, 21);
            this.wlgg_tb.TabIndex = 68;
            // 
            // qqjg_tb
            // 
            this.qqjg_tb.Location = new System.Drawing.Point(440, 298);
            this.qqjg_tb.Name = "qqjg_tb";
            this.qqjg_tb.Size = new System.Drawing.Size(200, 21);
            this.qqjg_tb.TabIndex = 67;
            // 
            // wlbz_tb
            // 
            this.wlbz_tb.Location = new System.Drawing.Point(90, 298);
            this.wlbz_tb.Name = "wlbz_tb";
            this.wlbz_tb.Size = new System.Drawing.Size(200, 21);
            this.wlbz_tb.TabIndex = 66;
            // 
            // wlph_tb
            // 
            this.wlph_tb.Location = new System.Drawing.Point(90, 258);
            this.wlph_tb.Name = "wlph_tb";
            this.wlph_tb.Size = new System.Drawing.Size(200, 21);
            this.wlph_tb.TabIndex = 65;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("宋体", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label17.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label17.Location = new System.Drawing.Point(15, 192);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(71, 15);
            this.label17.TabIndex = 62;
            this.label17.Text = "物料信息";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("宋体", 9F);
            this.label18.Location = new System.Drawing.Point(380, 302);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(53, 12);
            this.label18.TabIndex = 61;
            this.label18.Text = "前期价格";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("宋体", 9F);
            this.label19.Location = new System.Drawing.Point(30, 302);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(53, 12);
            this.label19.TabIndex = 60;
            this.label19.Text = "物料标准";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("宋体", 9F);
            this.label20.Location = new System.Drawing.Point(380, 262);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(53, 12);
            this.label20.TabIndex = 58;
            this.label20.Text = "物料规格";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("宋体", 9F);
            this.label21.Location = new System.Drawing.Point(30, 262);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(53, 12);
            this.label21.TabIndex = 57;
            this.label21.Text = "物料牌号";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("宋体", 9F);
            this.label22.Location = new System.Drawing.Point(380, 222);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(53, 12);
            this.label22.TabIndex = 56;
            this.label22.Text = "物料名称";
            // 
            // xysl_tb
            // 
            this.xysl_tb.Location = new System.Drawing.Point(440, 153);
            this.xysl_tb.Name = "xysl_tb";
            this.xysl_tb.Size = new System.Drawing.Size(200, 21);
            this.xysl_tb.TabIndex = 52;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("宋体", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label16.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label16.Location = new System.Drawing.Point(15, 7);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(71, 15);
            this.label16.TabIndex = 47;
            this.label16.Text = "协议信息";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("宋体", 9F);
            this.label11.Location = new System.Drawing.Point(368, 367);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(65, 12);
            this.label11.TabIndex = 39;
            this.label11.Text = "供应商名称";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("宋体", 9F);
            this.label12.Location = new System.Drawing.Point(18, 367);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(65, 12);
            this.label12.TabIndex = 37;
            this.label12.Text = "供应商编号";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("宋体", 9F);
            this.label13.Location = new System.Drawing.Point(380, 157);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(53, 12);
            this.label13.TabIndex = 35;
            this.label13.Text = "协议数量";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("宋体", 9F);
            this.label14.Location = new System.Drawing.Point(30, 157);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(53, 12);
            this.label14.TabIndex = 33;
            this.label14.Text = "供应商号";
            // 
            // xylx_cmb
            // 
            this.xylx_cmb.FormattingEnabled = true;
            this.xylx_cmb.Location = new System.Drawing.Point(440, 113);
            this.xylx_cmb.Name = "xylx_cmb";
            this.xylx_cmb.Size = new System.Drawing.Size(200, 20);
            this.xylx_cmb.TabIndex = 32;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("宋体", 9F);
            this.label4.Location = new System.Drawing.Point(380, 117);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 12);
            this.label4.TabIndex = 31;
            this.label4.Text = "协议类型";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("宋体", 9F);
            this.label5.Location = new System.Drawing.Point(30, 117);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 12);
            this.label5.TabIndex = 29;
            this.label5.Text = "签约时间";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("宋体", 9F);
            this.label8.Location = new System.Drawing.Point(380, 77);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(53, 12);
            this.label8.TabIndex = 27;
            this.label8.Text = "协议名称";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("宋体", 9F);
            this.label9.Location = new System.Drawing.Point(30, 77);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(53, 12);
            this.label9.TabIndex = 25;
            this.label9.Text = "协议编号";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("宋体", 9F);
            this.label10.Location = new System.Drawing.Point(30, 37);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(53, 12);
            this.label10.TabIndex = 20;
            this.label10.Text = "需求单号";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.wlfs_cmb);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.cglx_cmb);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.wlmc_cmb);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.wlbh_cmb);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Location = new System.Drawing.Point(20, 60);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(655, 80);
            this.panel1.TabIndex = 158;
            // 
            // wlfs_cmb
            // 
            this.wlfs_cmb.FormattingEnabled = true;
            this.wlfs_cmb.Location = new System.Drawing.Point(440, 51);
            this.wlfs_cmb.Name = "wlfs_cmb";
            this.wlfs_cmb.Size = new System.Drawing.Size(200, 20);
            this.wlfs_cmb.TabIndex = 32;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("宋体", 9F);
            this.label6.Location = new System.Drawing.Point(380, 55);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 12);
            this.label6.TabIndex = 31;
            this.label6.Text = "物流方式";
            // 
            // cglx_cmb
            // 
            this.cglx_cmb.FormattingEnabled = true;
            this.cglx_cmb.Location = new System.Drawing.Point(90, 51);
            this.cglx_cmb.Name = "cglx_cmb";
            this.cglx_cmb.Size = new System.Drawing.Size(200, 20);
            this.cglx_cmb.TabIndex = 30;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("宋体", 9F);
            this.label7.Location = new System.Drawing.Point(30, 55);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 12);
            this.label7.TabIndex = 29;
            this.label7.Text = "采购类型";
            // 
            // wlmc_cmb
            // 
            this.wlmc_cmb.FormattingEnabled = true;
            this.wlmc_cmb.Location = new System.Drawing.Point(440, 11);
            this.wlmc_cmb.Name = "wlmc_cmb";
            this.wlmc_cmb.Size = new System.Drawing.Size(200, 20);
            this.wlmc_cmb.TabIndex = 28;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("宋体", 9F);
            this.label3.Location = new System.Drawing.Point(380, 15);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 12);
            this.label3.TabIndex = 27;
            this.label3.Text = "物料名称";
            // 
            // wlbh_cmb
            // 
            this.wlbh_cmb.FormattingEnabled = true;
            this.wlbh_cmb.ItemHeight = 12;
            this.wlbh_cmb.Location = new System.Drawing.Point(90, 11);
            this.wlbh_cmb.Name = "wlbh_cmb";
            this.wlbh_cmb.Size = new System.Drawing.Size(200, 20);
            this.wlbh_cmb.TabIndex = 26;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("宋体", 9F);
            this.label2.Location = new System.Drawing.Point(30, 15);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 25;
            this.label2.Text = "物料编号";
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("宋体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label1.Location = new System.Drawing.Point(298, 21);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(155, 19);
            this.label1.TabIndex = 159;
            this.label1.Text = "******采购订单";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.tgrq_dt);
            this.panel3.Controls.Add(this.shzt_cmb);
            this.panel3.Controls.Add(this.label23);
            this.panel3.Controls.Add(this.label26);
            this.panel3.Location = new System.Drawing.Point(20, 533);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(655, 55);
            this.panel3.TabIndex = 160;
            // 
            // tgrq_dt
            // 
            this.tgrq_dt.Location = new System.Drawing.Point(439, 21);
            this.tgrq_dt.Name = "tgrq_dt";
            this.tgrq_dt.Size = new System.Drawing.Size(200, 21);
            this.tgrq_dt.TabIndex = 148;
            // 
            // shzt_cmb
            // 
            this.shzt_cmb.FormattingEnabled = true;
            this.shzt_cmb.Location = new System.Drawing.Point(89, 21);
            this.shzt_cmb.Name = "shzt_cmb";
            this.shzt_cmb.Size = new System.Drawing.Size(200, 20);
            this.shzt_cmb.TabIndex = 61;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("宋体", 9F);
            this.label23.Location = new System.Drawing.Point(379, 25);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(53, 12);
            this.label23.TabIndex = 58;
            this.label23.Text = "通过日期";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("宋体", 9F);
            this.label26.Location = new System.Drawing.Point(29, 25);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(53, 12);
            this.label26.TabIndex = 57;
            this.label26.Text = "审核状态";
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.cgksrq_dt);
            this.panel4.Controls.Add(this.cgjsrq_dt);
            this.panel4.Controls.Add(this.hxcgpz_cmb);
            this.panel4.Controls.Add(this.label40);
            this.panel4.Controls.Add(this.label38);
            this.panel4.Controls.Add(this.label39);
            this.panel4.Controls.Add(this.fpyz_cmb);
            this.panel4.Controls.Add(this.label37);
            this.panel4.Controls.Add(this.checkBox1);
            this.panel4.Controls.Add(this.comboBox16);
            this.panel4.Controls.Add(this.comboBox17);
            this.panel4.Controls.Add(this.label34);
            this.panel4.Controls.Add(this.textBox18);
            this.panel4.Controls.Add(this.label35);
            this.panel4.Controls.Add(this.label36);
            this.panel4.Controls.Add(this.cgy1_cmb);
            this.panel4.Controls.Add(this.cgzz1_cmb);
            this.panel4.Controls.Add(this.label31);
            this.panel4.Controls.Add(this.fpbl1_tb);
            this.panel4.Controls.Add(this.label32);
            this.panel4.Controls.Add(this.label33);
            this.panel4.Controls.Add(this.del_bt);
            this.panel4.Controls.Add(this.add_bt);
            this.panel4.Controls.Add(this.label30);
            this.panel4.Location = new System.Drawing.Point(0, 587);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(774, 242);
            this.panel4.TabIndex = 161;
            // 
            // cgksrq_dt
            // 
            this.cgksrq_dt.Location = new System.Drawing.Point(110, 171);
            this.cgksrq_dt.Name = "cgksrq_dt";
            this.cgksrq_dt.Size = new System.Drawing.Size(160, 21);
            this.cgksrq_dt.TabIndex = 147;
            // 
            // cgjsrq_dt
            // 
            this.cgjsrq_dt.Location = new System.Drawing.Point(360, 171);
            this.cgjsrq_dt.Name = "cgjsrq_dt";
            this.cgjsrq_dt.Size = new System.Drawing.Size(160, 21);
            this.cgjsrq_dt.TabIndex = 146;
            // 
            // hxcgpz_cmb
            // 
            this.hxcgpz_cmb.FormattingEnabled = true;
            this.hxcgpz_cmb.Location = new System.Drawing.Point(109, 212);
            this.hxcgpz_cmb.Name = "hxcgpz_cmb";
            this.hxcgpz_cmb.Size = new System.Drawing.Size(160, 20);
            this.hxcgpz_cmb.TabIndex = 138;
            this.hxcgpz_cmb.Text = "采购订单";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("宋体", 9F);
            this.label40.Location = new System.Drawing.Point(26, 215);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(77, 12);
            this.label40.TabIndex = 137;
            this.label40.Text = "后需采购凭证";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("宋体", 9F);
            this.label38.Location = new System.Drawing.Point(277, 176);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(77, 12);
            this.label38.TabIndex = 134;
            this.label38.Text = "采购结束日期";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("宋体", 9F);
            this.label39.Location = new System.Drawing.Point(26, 177);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(77, 12);
            this.label39.TabIndex = 133;
            this.label39.Text = "采购开始日期";
            // 
            // fpyz_cmb
            // 
            this.fpyz_cmb.FormattingEnabled = true;
            this.fpyz_cmb.Location = new System.Drawing.Point(109, 135);
            this.fpyz_cmb.Name = "fpyz_cmb";
            this.fpyz_cmb.Size = new System.Drawing.Size(160, 20);
            this.fpyz_cmb.TabIndex = 132;
            this.fpyz_cmb.Text = "百分比";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("宋体", 9F);
            this.label37.Location = new System.Drawing.Point(50, 139);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(53, 12);
            this.label37.TabIndex = 131;
            this.label37.Text = "分配原则";
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(29, 101);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(15, 14);
            this.checkBox1.TabIndex = 130;
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // comboBox16
            // 
            this.comboBox16.FormattingEnabled = true;
            this.comboBox16.Location = new System.Drawing.Point(602, 97);
            this.comboBox16.Name = "comboBox16";
            this.comboBox16.Size = new System.Drawing.Size(160, 20);
            this.comboBox16.TabIndex = 129;
            // 
            // comboBox17
            // 
            this.comboBox17.FormattingEnabled = true;
            this.comboBox17.Location = new System.Drawing.Point(109, 97);
            this.comboBox17.Name = "comboBox17";
            this.comboBox17.Size = new System.Drawing.Size(160, 20);
            this.comboBox17.TabIndex = 128;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("宋体", 9F);
            this.label34.Location = new System.Drawing.Point(555, 101);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(41, 12);
            this.label34.TabIndex = 127;
            this.label34.Text = "采购员";
            // 
            // textBox18
            // 
            this.textBox18.Location = new System.Drawing.Point(360, 97);
            this.textBox18.Name = "textBox18";
            this.textBox18.Size = new System.Drawing.Size(160, 21);
            this.textBox18.TabIndex = 126;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("宋体", 9F);
            this.label35.Location = new System.Drawing.Point(300, 101);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(53, 12);
            this.label35.TabIndex = 125;
            this.label35.Text = "分配比例";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("宋体", 9F);
            this.label36.Location = new System.Drawing.Point(50, 101);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(53, 12);
            this.label36.TabIndex = 124;
            this.label36.Text = "采购组织";
            // 
            // cgy1_cmb
            // 
            this.cgy1_cmb.FormattingEnabled = true;
            this.cgy1_cmb.Location = new System.Drawing.Point(602, 58);
            this.cgy1_cmb.Name = "cgy1_cmb";
            this.cgy1_cmb.Size = new System.Drawing.Size(160, 20);
            this.cgy1_cmb.TabIndex = 123;
            // 
            // cgzz1_cmb
            // 
            this.cgzz1_cmb.FormattingEnabled = true;
            this.cgzz1_cmb.Location = new System.Drawing.Point(109, 58);
            this.cgzz1_cmb.Name = "cgzz1_cmb";
            this.cgzz1_cmb.Size = new System.Drawing.Size(160, 20);
            this.cgzz1_cmb.TabIndex = 122;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("宋体", 9F);
            this.label31.Location = new System.Drawing.Point(555, 62);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(41, 12);
            this.label31.TabIndex = 121;
            this.label31.Text = "采购员";
            // 
            // fpbl1_tb
            // 
            this.fpbl1_tb.Location = new System.Drawing.Point(360, 58);
            this.fpbl1_tb.Name = "fpbl1_tb";
            this.fpbl1_tb.Size = new System.Drawing.Size(160, 21);
            this.fpbl1_tb.TabIndex = 120;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("宋体", 9F);
            this.label32.Location = new System.Drawing.Point(300, 62);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(53, 12);
            this.label32.TabIndex = 119;
            this.label32.Text = "分配比例";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("宋体", 9F);
            this.label33.Location = new System.Drawing.Point(50, 62);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(53, 12);
            this.label33.TabIndex = 118;
            this.label33.Text = "采购组织";
            // 
            // del_bt
            // 
            this.del_bt.Location = new System.Drawing.Point(190, 5);
            this.del_bt.Name = "del_bt";
            this.del_bt.Size = new System.Drawing.Size(60, 30);
            this.del_bt.TabIndex = 117;
            this.del_bt.Text = "删除";
            this.del_bt.UseVisualStyleBackColor = true;
            // 
            // add_bt
            // 
            this.add_bt.Location = new System.Drawing.Point(110, 5);
            this.add_bt.Name = "add_bt";
            this.add_bt.Size = new System.Drawing.Size(60, 30);
            this.add_bt.TabIndex = 116;
            this.add_bt.Text = "添加";
            this.add_bt.UseVisualStyleBackColor = true;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label30.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label30.Location = new System.Drawing.Point(13, 14);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(83, 12);
            this.label30.TabIndex = 115;
            this.label30.Text = "账户分配组织";
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.cancel_bt);
            this.panel5.Controls.Add(this.return_bt);
            this.panel5.Controls.Add(this.submit_bt);
            this.panel5.Location = new System.Drawing.Point(30, 828);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(655, 60);
            this.panel5.TabIndex = 162;
            // 
            // cancel_bt
            // 
            this.cancel_bt.Location = new System.Drawing.Point(287, 15);
            this.cancel_bt.Name = "cancel_bt";
            this.cancel_bt.Size = new System.Drawing.Size(100, 30);
            this.cancel_bt.TabIndex = 49;
            this.cancel_bt.Text = "取消";
            this.cancel_bt.UseVisualStyleBackColor = true;
            // 
            // return_bt
            // 
            this.return_bt.Location = new System.Drawing.Point(492, 15);
            this.return_bt.Name = "return_bt";
            this.return_bt.Size = new System.Drawing.Size(100, 30);
            this.return_bt.TabIndex = 48;
            this.return_bt.Text = "返回";
            this.return_bt.UseVisualStyleBackColor = true;
            // 
            // submit_bt
            // 
            this.submit_bt.Location = new System.Drawing.Point(82, 15);
            this.submit_bt.Name = "submit_bt";
            this.submit_bt.Size = new System.Drawing.Size(100, 30);
            this.submit_bt.TabIndex = 47;
            this.submit_bt.Text = "提交";
            this.submit_bt.UseVisualStyleBackColor = true;
            // 
            // CreateFramework_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(818, 881);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "CreateFramework_Form";
            this.Text = "生成采购计划（框架协议）";
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ComboBox wlmc2_cmb;
        private System.Windows.Forms.ComboBox wlbh2_cmb;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.DateTimePicker qysj_dt;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox xqdh_tb;
        private System.Windows.Forms.TextBox xymc_tb;
        private System.Windows.Forms.TextBox gysh_tb;
        private System.Windows.Forms.TextBox xybh_tb;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox gysmc_tb;
        private System.Windows.Forms.TextBox gysbh_tb;
        private System.Windows.Forms.TextBox wlgg_tb;
        private System.Windows.Forms.TextBox qqjg_tb;
        private System.Windows.Forms.TextBox wlbz_tb;
        private System.Windows.Forms.TextBox wlph_tb;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox xysl_tb;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox xylx_cmb;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ComboBox wlfs_cmb;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cglx_cmb;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox wlmc_cmb;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox wlbh_cmb;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.DateTimePicker tgrq_dt;
        private System.Windows.Forms.ComboBox shzt_cmb;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.DateTimePicker cgksrq_dt;
        private System.Windows.Forms.DateTimePicker cgjsrq_dt;
        private System.Windows.Forms.ComboBox hxcgpz_cmb;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.ComboBox fpyz_cmb;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.ComboBox comboBox16;
        private System.Windows.Forms.ComboBox comboBox17;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TextBox textBox18;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.ComboBox cgy1_cmb;
        private System.Windows.Forms.ComboBox cgzz1_cmb;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox fpbl1_tb;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Button del_bt;
        private System.Windows.Forms.Button add_bt;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Button cancel_bt;
        private System.Windows.Forms.Button return_bt;
        private System.Windows.Forms.Button submit_bt;
    }
}