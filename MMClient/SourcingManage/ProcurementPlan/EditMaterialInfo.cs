﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Model.MD.MT;
using Lib.Bll.MDBll.MT;
using Lib.Common.CommonControls;

namespace MMClient.SourcingManage.ProcurementPlan
{
    public partial class EditMaterialInfo : Form
    {
        private EditStandardDemand standardmand;
        private MaterialBLL materialBll = new MaterialBLL();
        private List<string> limitIds;
        
        public EditMaterialInfo(EditStandardDemand standardmand,List<string> limitIds)
        {
            InitializeComponent();
            this.standardmand = standardmand;
            this.limitIds = limitIds;
            dgv_editmaterials.TopLeftHeaderCell.Value = "序号";
        }

        /// <summary>
        /// 搜索
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_Search_Click(object sender, EventArgs e)
        {
            string condition = "";
            if (!string.IsNullOrEmpty(this.txtMName.Text.ToString().Trim()))
            {
                condition += "where   Material_Name like '%" + this.txtMName.Text.ToString().Trim() + "%'";
            }
            if (!string.IsNullOrEmpty(this.TxtMType.Text.ToString().Trim()))
            {
                condition += " and Material_Group like '%" + this.TxtMType.Text.ToString().Trim() + "%' ";
            }
            dgv_editmaterials.DataSource=null;
            List<MaterialBase> materials = materialBll.GetMaterialBasesInIDLimit(limitIds,condition);
            if (materials != null)
            {
                int i = 0;
                foreach (MaterialBase m in materials)
                {
                    dgv_editmaterials.Rows.Add(1);
                    dgv_editmaterials.Rows[i].Cells["materialId"].Value = m.Material_ID;
                    dgv_editmaterials.Rows[i].Cells["materialName"].Value = m.Material_Name;
                    dgv_editmaterials.Rows[i].Cells["materialGroup"].Value = m.Material_Group;
                    dgv_editmaterials.Rows[i].Cells["measurement"].Value = m.Measurement;
                    i++;
                }
            }
        }

        /// <summary>
        /// 画行号
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgv_materials_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            var dgv = sender as DataGridView;
            if (dgv != null)
            {
                Rectangle rect = new Rectangle(e.RowBounds.Location.X, e.RowBounds.Location.Y, (dgv.RowHeadersWidth + 12) / 2, e.RowBounds.Height);
                TextRenderer.DrawText(e.Graphics, (e.RowIndex + 1).ToString(), dgv.RowHeadersDefaultCellStyle.Font, rect, dgv.RowHeadersDefaultCellStyle.ForeColor, TextFormatFlags.VerticalCenter | TextFormatFlags.Right);
            }
        }

        /// <summary>
        /// 关闭按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_Cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// 确认按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_Confirm_Click(object sender, EventArgs e)
        {
            DataGridView dt = new DataGridView();
            dt.Columns.Add("materialId", "物料编号");
            dt.Columns.Add("materialName", "物料名称");
            dt.Columns.Add("materialGroup", "物料组");
            dt.Columns.Add("measurement", "单位");
            dt.Columns.Add("applyNum", "申请数量");
            dt.Columns.Add("Factory_ID", "工厂编号"); 
            dt.Columns.Add("stockId", "仓库编码");
            dt.Columns.Add("DeliveryStartTime", "交易开始时间");
            dt.Columns.Add("DeliveryEndTime", "交易结束时间");
            
            int i = 0;
            foreach (DataGridViewRow dgvr in dgv_editmaterials.Rows)
            {
                if(dgvr.Selected){
                    dt.Rows.Add();
                    dt.Rows[i].Cells["materialId"].Value = dgvr.Cells["materialId"].Value.ToString();
                    dt.Rows[i].Cells["materialName"].Value = dgvr.Cells["MaterialName"].Value.ToString();
                    dt.Rows[i].Cells["materialGroup"].Value = dgvr.Cells["materialGroup"].Value.ToString();
                    dt.Rows[i].Cells["measurement"].Value = dgvr.Cells["measurement"].Value.ToString();
                    dt.Rows[i].Cells["applyNum"].Value = "";
                    dt.Rows[i].Cells["Factory_ID"].Value = "";
                    dt.Rows[i].Cells["stockId"].Value = "";
                    dt.Rows[i].Cells["DeliveryStartTime"].Value = null;
                    dt.Rows[i].Cells["DeliveryEndTime"].Value = null ;
                    i++;
                }   
            } 
            standardmand.fillMaterialBases(GetDgvToTable(dt));
            this.Close();
        }/// <summary>
        /// 转Table
        /// </summary>
        /// <param name="dgv"></param>
        /// <returns></returns>
        public DataTable GetDgvToTable(DataGridView dgv)
        {
            DataTable dt = new DataTable();
            // 列强制转换
            for (int count = 0; count < dgv.Columns.Count; count++)
            {
                DataColumn dc = new DataColumn(dgv.Columns[count].Name.ToString());
                dt.Columns.Add(dc);
            }
            // 循环行
            for (int count = 0; count < dgv.Rows.Count; count++)
            {
                DataRow dr = dt.NewRow();
                for (int countsub = 0; countsub < dgv.Columns.Count; countsub++)
                {
                    object obj = dgv.Rows[count].Cells[countsub].Value;
                    dr[countsub] = obj;
                }
                dt.Rows.Add(dr);
            }
            return dt;
        }
        /// <summary>
        /// 鼠标进入单元格事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgv_materials_CellMouseEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
            {
                dgv_editmaterials.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.LightBlue;
                DataGridViewCellStyle style = new DataGridViewCellStyle();
                style.BackColor = Color.CornflowerBlue;
                dgv_editmaterials.Rows[e.RowIndex].HeaderCell.Style = style;
            }
        }

        /// <summary>
        /// 鼠标离开单元格事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgv_materials_CellMouseLeave(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
            {
                dgv_editmaterials.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.White;
                DataGridViewCellStyle style = new DataGridViewCellStyle();
                style.BackColor = Color.WhiteSmoke;
                dgv_editmaterials.Rows[e.RowIndex].HeaderCell.Style = style;
            }
        }

        private void EditMaterialInfo_Load(object sender, EventArgs e)
        {
            dgv_editmaterials.Rows.Clear();
            List<MaterialBase> materials = materialBll.GetMaterialBasesInIDLimit(limitIds,"");
            if (materials != null)
            {
                int i = 0;
                foreach (MaterialBase m in materials)
                {
                    dgv_editmaterials.Rows.Add(1);
                    dgv_editmaterials.Rows[i].Cells["materialId"].Value = m.Material_ID;
                    dgv_editmaterials.Rows[i].Cells["materialName"].Value = m.Material_Name;
                    dgv_editmaterials.Rows[i].Cells["materialGroup"].Value = m.Material_Group;
                    dgv_editmaterials.Rows[i].Cells["measurement"].Value = m.Measurement;
                    i++;
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.txtMName.Text = "";
            this.TxtMType.Text = "";
            btn_Search_Click(sender, e);
        }
    }
}
