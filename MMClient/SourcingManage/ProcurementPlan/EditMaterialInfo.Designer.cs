﻿namespace MMClient.SourcingManage.ProcurementPlan
{
    partial class EditMaterialInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgv_editmaterials = new System.Windows.Forms.DataGridView();
            this.materialId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MaterialName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.materialGroup = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.measurement = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lb_Notes = new System.Windows.Forms.Label();
            this.btn_Confirm = new System.Windows.Forms.Button();
            this.btn_Cancel = new System.Windows.Forms.Button();
            this.btn_Search = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtMType = new System.Windows.Forms.TextBox();
            this.txtMName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_editmaterials)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgv_editmaterials
            // 
            this.dgv_editmaterials.AllowUserToAddRows = false;
            this.dgv_editmaterials.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgv_editmaterials.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgv_editmaterials.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgv_editmaterials.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.materialId,
            this.MaterialName,
            this.materialGroup,
            this.measurement});
            this.dgv_editmaterials.EnableHeadersVisualStyles = false;
            this.dgv_editmaterials.Location = new System.Drawing.Point(19, 83);
            this.dgv_editmaterials.Margin = new System.Windows.Forms.Padding(2);
            this.dgv_editmaterials.Name = "dgv_editmaterials";
            this.dgv_editmaterials.ReadOnly = true;
            this.dgv_editmaterials.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.LightBlue;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            this.dgv_editmaterials.RowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv_editmaterials.RowTemplate.Height = 27;
            this.dgv_editmaterials.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv_editmaterials.ShowCellErrors = false;
            this.dgv_editmaterials.Size = new System.Drawing.Size(584, 409);
            this.dgv_editmaterials.TabIndex = 0;
            this.dgv_editmaterials.CellMouseEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_materials_CellMouseEnter);
            this.dgv_editmaterials.CellMouseLeave += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_materials_CellMouseLeave);
            this.dgv_editmaterials.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.dgv_materials_RowPostPaint);
            // 
            // materialId
            // 
            this.materialId.HeaderText = "物料编号";
            this.materialId.Name = "materialId";
            this.materialId.ReadOnly = true;
            this.materialId.Width = 150;
            // 
            // MaterialName
            // 
            this.MaterialName.HeaderText = "物料名称";
            this.MaterialName.Name = "MaterialName";
            this.MaterialName.ReadOnly = true;
            this.MaterialName.Width = 150;
            // 
            // materialGroup
            // 
            this.materialGroup.HeaderText = "物料组";
            this.materialGroup.Name = "materialGroup";
            this.materialGroup.ReadOnly = true;
            // 
            // measurement
            // 
            this.measurement.HeaderText = "单位";
            this.measurement.Name = "measurement";
            this.measurement.ReadOnly = true;
            // 
            // lb_Notes
            // 
            this.lb_Notes.AutoSize = true;
            this.lb_Notes.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lb_Notes.Location = new System.Drawing.Point(9, 624);
            this.lb_Notes.Name = "lb_Notes";
            this.lb_Notes.Size = new System.Drawing.Size(338, 12);
            this.lb_Notes.TabIndex = 6;
            this.lb_Notes.Text = "多选方法：鼠标选中某行上下拖拉或按住ctr键点选多行。";
            // 
            // btn_Confirm
            // 
            this.btn_Confirm.Location = new System.Drawing.Point(646, 581);
            this.btn_Confirm.Name = "btn_Confirm";
            this.btn_Confirm.Size = new System.Drawing.Size(89, 32);
            this.btn_Confirm.TabIndex = 7;
            this.btn_Confirm.Text = "确定";
            this.btn_Confirm.UseVisualStyleBackColor = true;
            this.btn_Confirm.Click += new System.EventHandler(this.btn_Confirm_Click);
            // 
            // btn_Cancel
            // 
            this.btn_Cancel.Location = new System.Drawing.Point(752, 581);
            this.btn_Cancel.Name = "btn_Cancel";
            this.btn_Cancel.Size = new System.Drawing.Size(89, 32);
            this.btn_Cancel.TabIndex = 8;
            this.btn_Cancel.Text = "关闭";
            this.btn_Cancel.UseVisualStyleBackColor = true;
            this.btn_Cancel.Click += new System.EventHandler(this.btn_Cancel_Click);
            // 
            // btn_Search
            // 
            this.btn_Search.Location = new System.Drawing.Point(417, 33);
            this.btn_Search.Name = "btn_Search";
            this.btn_Search.Size = new System.Drawing.Size(81, 25);
            this.btn_Search.TabIndex = 9;
            this.btn_Search.Text = "查 询";
            this.btn_Search.UseVisualStyleBackColor = true;
            this.btn_Search.Click += new System.EventHandler(this.btn_Search_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.TxtMType);
            this.groupBox1.Controls.Add(this.txtMName);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.dgv_editmaterials);
            this.groupBox1.Controls.Add(this.btn_Search);
            this.groupBox1.Location = new System.Drawing.Point(11, 23);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox1.Size = new System.Drawing.Size(872, 536);
            this.groupBox1.TabIndex = 10;
            this.groupBox1.TabStop = false;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(522, 34);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(81, 25);
            this.button1.TabIndex = 14;
            this.button1.Text = "重  置";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(226, 40);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 12);
            this.label2.TabIndex = 13;
            this.label2.Text = "物料组";
            // 
            // TxtMType
            // 
            this.TxtMType.Location = new System.Drawing.Point(288, 37);
            this.TxtMType.Name = "TxtMType";
            this.TxtMType.Size = new System.Drawing.Size(100, 21);
            this.TxtMType.TabIndex = 12;
            // 
            // txtMName
            // 
            this.txtMName.Location = new System.Drawing.Point(76, 37);
            this.txtMName.Name = "txtMName";
            this.txtMName.Size = new System.Drawing.Size(100, 21);
            this.txtMName.TabIndex = 11;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 10;
            this.label1.Text = "物料名称";
            // 
            // EditMaterialInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1009, 669);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btn_Cancel);
            this.Controls.Add(this.btn_Confirm);
            this.Controls.Add(this.lb_Notes);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "EditMaterialInfo";
            this.Text = "物料信息";
            this.Load += new System.EventHandler(this.EditMaterialInfo_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_editmaterials)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgv_editmaterials;
        private System.Windows.Forms.Label lb_Notes;
        private System.Windows.Forms.Button btn_Confirm;
        private System.Windows.Forms.Button btn_Cancel;
        private System.Windows.Forms.Button btn_Search;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridViewTextBoxColumn materialId;
        private System.Windows.Forms.DataGridViewTextBoxColumn MaterialName;
        private System.Windows.Forms.DataGridViewTextBoxColumn materialGroup;
        private System.Windows.Forms.DataGridViewTextBoxColumn measurement;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox TxtMType;
        private System.Windows.Forms.TextBox txtMName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
    }
}