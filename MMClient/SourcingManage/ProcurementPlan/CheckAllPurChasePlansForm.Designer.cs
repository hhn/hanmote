﻿namespace MMClient.SourcingManage.ProcurementPlan
{
    partial class CheckAllPurChasePlansForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.汇总信息 = new System.Windows.Forms.GroupBox();
            this.btnQuery = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.pageTool = new pager.pagetool.pageNext();
            this.cbStock = new System.Windows.Forms.ComboBox();
            this.aggregateGridView = new System.Windows.Forms.DataGridView();
            this.btnReset = new System.Windows.Forms.Button();
            this.cbFactory = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.详情 = new System.Windows.Forms.GroupBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.panel1.SuspendLayout();
            this.汇总信息.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.aggregateGridView)).BeginInit();
            this.详情.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.Controls.Add(this.汇总信息);
            this.panel1.Controls.Add(this.详情);
            this.panel1.Location = new System.Drawing.Point(13, 13);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1059, 780);
            this.panel1.TabIndex = 0;
            // 
            // 汇总信息
            // 
            this.汇总信息.Controls.Add(this.btnQuery);
            this.汇总信息.Controls.Add(this.label2);
            this.汇总信息.Controls.Add(this.pageTool);
            this.汇总信息.Controls.Add(this.cbStock);
            this.汇总信息.Controls.Add(this.aggregateGridView);
            this.汇总信息.Controls.Add(this.btnReset);
            this.汇总信息.Controls.Add(this.cbFactory);
            this.汇总信息.Controls.Add(this.label1);
            this.汇总信息.Location = new System.Drawing.Point(18, 4);
            this.汇总信息.Name = "汇总信息";
            this.汇总信息.Size = new System.Drawing.Size(984, 464);
            this.汇总信息.TabIndex = 30;
            this.汇总信息.TabStop = false;
            this.汇总信息.Text = "汇总信息";
            // 
            // btnQuery
            // 
            this.btnQuery.Location = new System.Drawing.Point(431, 16);
            this.btnQuery.Name = "btnQuery";
            this.btnQuery.Size = new System.Drawing.Size(64, 21);
            this.btnQuery.TabIndex = 27;
            this.btnQuery.Text = "查询";
            this.btnQuery.UseVisualStyleBackColor = true;
            this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(227, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 12);
            this.label2.TabIndex = 25;
            this.label2.Text = "库存地";
            // 
            // pageTool
            // 
            this.pageTool.Location = new System.Drawing.Point(56, 421);
            this.pageTool.Name = "pageTool";
            this.pageTool.PageIndex = 1;
            this.pageTool.PageSize = 100;
            this.pageTool.RecordCount = 0;
            this.pageTool.Size = new System.Drawing.Size(688, 37);
            this.pageTool.TabIndex = 5;
            this.pageTool.OnPageChanged += new System.EventHandler(this.pageTool_OnPageChanged);
            this.pageTool.Load += new System.EventHandler(this.pageTool_Load);
            // 
            // cbStock
            // 
            this.cbStock.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbStock.FormattingEnabled = true;
            this.cbStock.Location = new System.Drawing.Point(285, 17);
            this.cbStock.Name = "cbStock";
            this.cbStock.Size = new System.Drawing.Size(121, 20);
            this.cbStock.TabIndex = 26;
            // 
            // aggregateGridView
            // 
            this.aggregateGridView.AllowUserToAddRows = false;
            this.aggregateGridView.BackgroundColor = System.Drawing.SystemColors.Control;
            this.aggregateGridView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.aggregateGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.aggregateGridView.EnableHeadersVisualStyles = false;
            this.aggregateGridView.Location = new System.Drawing.Point(6, 47);
            this.aggregateGridView.Name = "aggregateGridView";
            this.aggregateGridView.RowTemplate.Height = 23;
            this.aggregateGridView.Size = new System.Drawing.Size(750, 355);
            this.aggregateGridView.TabIndex = 0;
            this.aggregateGridView.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.aggregateGridView_CellMouseClick);
            // 
            // btnReset
            // 
            this.btnReset.Location = new System.Drawing.Point(528, 16);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(64, 21);
            this.btnReset.TabIndex = 28;
            this.btnReset.Text = "重置";
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // cbFactory
            // 
            this.cbFactory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbFactory.FormattingEnabled = true;
            this.cbFactory.Location = new System.Drawing.Point(72, 19);
            this.cbFactory.Name = "cbFactory";
            this.cbFactory.Size = new System.Drawing.Size(121, 20);
            this.cbFactory.TabIndex = 24;
            this.cbFactory.TextChanged += new System.EventHandler(this.cbFactory_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 23;
            this.label1.Text = "工厂编号";
            // 
            // 详情
            // 
            this.详情.Controls.Add(this.dataGridView1);
            this.详情.Location = new System.Drawing.Point(24, 474);
            this.详情.Name = "详情";
            this.详情.Size = new System.Drawing.Size(973, 285);
            this.详情.TabIndex = 29;
            this.详情.TabStop = false;
            this.详情.Text = "对应详情";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.EnableHeadersVisualStyles = false;
            this.dataGridView1.Location = new System.Drawing.Point(9, 20);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.Size = new System.Drawing.Size(754, 222);
            this.dataGridView1.TabIndex = 1;
            // 
            // CheckAllPurChasePlansForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1111, 809);
            this.Controls.Add(this.panel1);
            this.Name = "CheckAllPurChasePlansForm";
            this.Text = "全部汇总信息";
            this.Load += new System.EventHandler(this.CheckAllPurChasePlansForm_Load);
            this.panel1.ResumeLayout(false);
            this.汇总信息.ResumeLayout(false);
            this.汇总信息.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.aggregateGridView)).EndInit();
            this.详情.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private pager.pagetool.pageNext pageTool;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.DataGridView aggregateGridView;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnQuery;
        private System.Windows.Forms.ComboBox cbFactory;
        private System.Windows.Forms.ComboBox cbStock;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox 详情;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.GroupBox 汇总信息;
    }
}