﻿namespace MMClient.SourcingManage.ProcurementPlan
{
    partial class AuditSubmitStandard_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.name_label = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.cancel_bt = new System.Windows.Forms.Button();
            this.return_bt = new System.Windows.Forms.Button();
            this.submit_bt = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.tgrq_dt = new System.Windows.Forms.DateTimePicker();
            this.shzt_cmb = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.sqrq_dt = new System.Windows.Forms.DateTimePicker();
            this.lxdh_tb = new System.Windows.Forms.TextBox();
            this.xqsl_tb = new System.Windows.Forms.TextBox();
            this.mindhpl_tb = new System.Windows.Forms.TextBox();
            this.qtyq_rtb = new System.Windows.Forms.RichTextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.sqr_cmb = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.sqbm_cmb = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.jldw_tb = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.ckbh_cmb = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.xqdh_tb = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.wlfs_cmb = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cglx_cmb = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.wlmc_cmb = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.wlbh_cmb = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // name_label
            // 
            this.name_label.AutoSize = true;
            this.name_label.Font = new System.Drawing.Font("宋体", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.name_label.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.name_label.Location = new System.Drawing.Point(156, 26);
            this.name_label.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.name_label.Name = "name_label";
            this.name_label.Size = new System.Drawing.Size(296, 27);
            this.name_label.TabIndex = 157;
            this.name_label.Text = "****采购需求计划审核";
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.cancel_bt);
            this.panel4.Controls.Add(this.return_bt);
            this.panel4.Controls.Add(this.submit_bt);
            this.panel4.Location = new System.Drawing.Point(27, 590);
            this.panel4.Margin = new System.Windows.Forms.Padding(4);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(873, 86);
            this.panel4.TabIndex = 156;
            // 
            // cancel_bt
            // 
            this.cancel_bt.Location = new System.Drawing.Point(383, 19);
            this.cancel_bt.Margin = new System.Windows.Forms.Padding(4);
            this.cancel_bt.Name = "cancel_bt";
            this.cancel_bt.Size = new System.Drawing.Size(133, 38);
            this.cancel_bt.TabIndex = 49;
            this.cancel_bt.Text = "取消";
            this.cancel_bt.UseVisualStyleBackColor = true;
            // 
            // return_bt
            // 
            this.return_bt.Location = new System.Drawing.Point(656, 19);
            this.return_bt.Margin = new System.Windows.Forms.Padding(4);
            this.return_bt.Name = "return_bt";
            this.return_bt.Size = new System.Drawing.Size(133, 38);
            this.return_bt.TabIndex = 48;
            this.return_bt.Text = "返回";
            this.return_bt.UseVisualStyleBackColor = true;
            this.return_bt.Click += new System.EventHandler(this.return_bt_Click);
            // 
            // submit_bt
            // 
            this.submit_bt.Location = new System.Drawing.Point(109, 19);
            this.submit_bt.Margin = new System.Windows.Forms.Padding(4);
            this.submit_bt.Name = "submit_bt";
            this.submit_bt.Size = new System.Drawing.Size(133, 38);
            this.submit_bt.TabIndex = 47;
            this.submit_bt.Text = "提交";
            this.submit_bt.UseVisualStyleBackColor = true;
            this.submit_bt.Click += new System.EventHandler(this.submit_bt_Click);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.tgrq_dt);
            this.panel3.Controls.Add(this.shzt_cmb);
            this.panel3.Controls.Add(this.label16);
            this.panel3.Controls.Add(this.label17);
            this.panel3.Location = new System.Drawing.Point(27, 522);
            this.panel3.Margin = new System.Windows.Forms.Padding(4);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(873, 69);
            this.panel3.TabIndex = 155;
            // 
            // tgrq_dt
            // 
            this.tgrq_dt.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tgrq_dt.Location = new System.Drawing.Point(585, 26);
            this.tgrq_dt.Margin = new System.Windows.Forms.Padding(4);
            this.tgrq_dt.Name = "tgrq_dt";
            this.tgrq_dt.Size = new System.Drawing.Size(265, 27);
            this.tgrq_dt.TabIndex = 148;
            this.tgrq_dt.Value = new System.DateTime(2015, 7, 22, 21, 34, 9, 0);
            // 
            // shzt_cmb
            // 
            this.shzt_cmb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.shzt_cmb.FormattingEnabled = true;
            this.shzt_cmb.Items.AddRange(new object[] {
            "通过",
            "未通过"});
            this.shzt_cmb.Location = new System.Drawing.Point(119, 26);
            this.shzt_cmb.Margin = new System.Windows.Forms.Padding(4);
            this.shzt_cmb.Name = "shzt_cmb";
            this.shzt_cmb.Size = new System.Drawing.Size(265, 25);
            this.shzt_cmb.TabIndex = 61;
            this.shzt_cmb.Text = "待审核";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("宋体", 9F);
            this.label16.Location = new System.Drawing.Point(505, 31);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(67, 15);
            this.label16.TabIndex = 58;
            this.label16.Text = "通过日期";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("宋体", 9F);
            this.label17.Location = new System.Drawing.Point(39, 31);
            this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(67, 15);
            this.label17.TabIndex = 57;
            this.label17.Text = "审核状态";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.sqrq_dt);
            this.panel2.Controls.Add(this.lxdh_tb);
            this.panel2.Controls.Add(this.xqsl_tb);
            this.panel2.Controls.Add(this.mindhpl_tb);
            this.panel2.Controls.Add(this.qtyq_rtb);
            this.panel2.Controls.Add(this.label15);
            this.panel2.Controls.Add(this.label11);
            this.panel2.Controls.Add(this.label12);
            this.panel2.Controls.Add(this.sqr_cmb);
            this.panel2.Controls.Add(this.label13);
            this.panel2.Controls.Add(this.sqbm_cmb);
            this.panel2.Controls.Add(this.label14);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.jldw_tb);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.ckbh_cmb);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Controls.Add(this.xqdh_tb);
            this.panel2.Location = new System.Drawing.Point(27, 174);
            this.panel2.Margin = new System.Windows.Forms.Padding(4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(873, 350);
            this.panel2.TabIndex = 154;
            // 
            // sqrq_dt
            // 
            this.sqrq_dt.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.sqrq_dt.Location = new System.Drawing.Point(587, 214);
            this.sqrq_dt.Margin = new System.Windows.Forms.Padding(4);
            this.sqrq_dt.Name = "sqrq_dt";
            this.sqrq_dt.Size = new System.Drawing.Size(265, 27);
            this.sqrq_dt.TabIndex = 167;
            // 
            // lxdh_tb
            // 
            this.lxdh_tb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lxdh_tb.Location = new System.Drawing.Point(120, 214);
            this.lxdh_tb.Margin = new System.Windows.Forms.Padding(4);
            this.lxdh_tb.Name = "lxdh_tb";
            this.lxdh_tb.Size = new System.Drawing.Size(265, 27);
            this.lxdh_tb.TabIndex = 166;
            this.lxdh_tb.Text = "8180974";
            // 
            // xqsl_tb
            // 
            this.xqsl_tb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xqsl_tb.Location = new System.Drawing.Point(587, 114);
            this.xqsl_tb.Margin = new System.Windows.Forms.Padding(4);
            this.xqsl_tb.Name = "xqsl_tb";
            this.xqsl_tb.Size = new System.Drawing.Size(265, 27);
            this.xqsl_tb.TabIndex = 165;
            this.xqsl_tb.Text = "1000";
            // 
            // mindhpl_tb
            // 
            this.mindhpl_tb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.mindhpl_tb.Location = new System.Drawing.Point(120, 114);
            this.mindhpl_tb.Margin = new System.Windows.Forms.Padding(4);
            this.mindhpl_tb.Name = "mindhpl_tb";
            this.mindhpl_tb.Size = new System.Drawing.Size(265, 27);
            this.mindhpl_tb.TabIndex = 164;
            this.mindhpl_tb.Text = "100";
            // 
            // qtyq_rtb
            // 
            this.qtyq_rtb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.qtyq_rtb.Location = new System.Drawing.Point(120, 265);
            this.qtyq_rtb.Margin = new System.Windows.Forms.Padding(4);
            this.qtyq_rtb.Name = "qtyq_rtb";
            this.qtyq_rtb.Size = new System.Drawing.Size(732, 74);
            this.qtyq_rtb.TabIndex = 163;
            this.qtyq_rtb.Text = "无";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("宋体", 9F);
            this.label15.Location = new System.Drawing.Point(40, 269);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(67, 15);
            this.label15.TabIndex = 162;
            this.label15.Text = "其他要求";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("宋体", 9F);
            this.label11.Location = new System.Drawing.Point(507, 219);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(67, 15);
            this.label11.TabIndex = 161;
            this.label11.Text = "申请日期";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("宋体", 9F);
            this.label12.Location = new System.Drawing.Point(40, 219);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(67, 15);
            this.label12.TabIndex = 160;
            this.label12.Text = "联系电话";
            // 
            // sqr_cmb
            // 
            this.sqr_cmb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.sqr_cmb.FormattingEnabled = true;
            this.sqr_cmb.Location = new System.Drawing.Point(587, 164);
            this.sqr_cmb.Margin = new System.Windows.Forms.Padding(4);
            this.sqr_cmb.Name = "sqr_cmb";
            this.sqr_cmb.Size = new System.Drawing.Size(265, 25);
            this.sqr_cmb.TabIndex = 159;
            this.sqr_cmb.Text = "张三";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("宋体", 9F);
            this.label13.Location = new System.Drawing.Point(523, 169);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(52, 15);
            this.label13.TabIndex = 158;
            this.label13.Text = "申请人";
            // 
            // sqbm_cmb
            // 
            this.sqbm_cmb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.sqbm_cmb.FormattingEnabled = true;
            this.sqbm_cmb.Location = new System.Drawing.Point(120, 164);
            this.sqbm_cmb.Margin = new System.Windows.Forms.Padding(4);
            this.sqbm_cmb.Name = "sqbm_cmb";
            this.sqbm_cmb.Size = new System.Drawing.Size(265, 25);
            this.sqbm_cmb.TabIndex = 157;
            this.sqbm_cmb.Text = "申请部门A";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("宋体", 9F);
            this.label14.Location = new System.Drawing.Point(40, 169);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(67, 15);
            this.label14.TabIndex = 156;
            this.label14.Text = "申请部门";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("宋体", 9F);
            this.label4.Location = new System.Drawing.Point(507, 119);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(67, 15);
            this.label4.TabIndex = 155;
            this.label4.Text = "需求数量";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("宋体", 9F);
            this.label5.Location = new System.Drawing.Point(8, 119);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(97, 15);
            this.label5.TabIndex = 154;
            this.label5.Text = "最小订货批量";
            // 
            // jldw_tb
            // 
            this.jldw_tb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.jldw_tb.FormattingEnabled = true;
            this.jldw_tb.Location = new System.Drawing.Point(587, 64);
            this.jldw_tb.Margin = new System.Windows.Forms.Padding(4);
            this.jldw_tb.Name = "jldw_tb";
            this.jldw_tb.Size = new System.Drawing.Size(265, 25);
            this.jldw_tb.TabIndex = 153;
            this.jldw_tb.Text = "TON";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("宋体", 9F);
            this.label8.Location = new System.Drawing.Point(507, 69);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(67, 15);
            this.label8.TabIndex = 152;
            this.label8.Text = "计量单位";
            // 
            // ckbh_cmb
            // 
            this.ckbh_cmb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.ckbh_cmb.FormattingEnabled = true;
            this.ckbh_cmb.Location = new System.Drawing.Point(120, 64);
            this.ckbh_cmb.Margin = new System.Windows.Forms.Padding(4);
            this.ckbh_cmb.Name = "ckbh_cmb";
            this.ckbh_cmb.Size = new System.Drawing.Size(265, 25);
            this.ckbh_cmb.TabIndex = 151;
            this.ckbh_cmb.Text = "2301";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("宋体", 9F);
            this.label9.Location = new System.Drawing.Point(40, 69);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(67, 15);
            this.label9.TabIndex = 150;
            this.label9.Text = "仓库编号";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("宋体", 9F);
            this.label10.Location = new System.Drawing.Point(40, 19);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(67, 15);
            this.label10.TabIndex = 149;
            this.label10.Text = "需求单号";
            // 
            // xqdh_tb
            // 
            this.xqdh_tb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xqdh_tb.Location = new System.Drawing.Point(120, 14);
            this.xqdh_tb.Margin = new System.Windows.Forms.Padding(4);
            this.xqdh_tb.Name = "xqdh_tb";
            this.xqdh_tb.Size = new System.Drawing.Size(732, 27);
            this.xqdh_tb.TabIndex = 148;
            this.xqdh_tb.Text = "ZSRM2015000280 ";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.wlfs_cmb);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.cglx_cmb);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.wlmc_cmb);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.wlbh_cmb);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Location = new System.Drawing.Point(27, 75);
            this.panel1.Margin = new System.Windows.Forms.Padding(4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(873, 100);
            this.panel1.TabIndex = 153;
            // 
            // wlfs_cmb
            // 
            this.wlfs_cmb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.wlfs_cmb.FormattingEnabled = true;
            this.wlfs_cmb.Location = new System.Drawing.Point(585, 19);
            this.wlfs_cmb.Margin = new System.Windows.Forms.Padding(4);
            this.wlfs_cmb.Name = "wlfs_cmb";
            this.wlfs_cmb.Size = new System.Drawing.Size(265, 25);
            this.wlfs_cmb.TabIndex = 32;
            this.wlfs_cmb.Text = "物流中心式";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("宋体", 9F);
            this.label6.Location = new System.Drawing.Point(508, 24);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(67, 15);
            this.label6.TabIndex = 31;
            this.label6.Text = "物流方式";
            // 
            // cglx_cmb
            // 
            this.cglx_cmb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.cglx_cmb.FormattingEnabled = true;
            this.cglx_cmb.Location = new System.Drawing.Point(119, 19);
            this.cglx_cmb.Margin = new System.Windows.Forms.Padding(4);
            this.cglx_cmb.Name = "cglx_cmb";
            this.cglx_cmb.Size = new System.Drawing.Size(265, 25);
            this.cglx_cmb.TabIndex = 30;
            this.cglx_cmb.Text = "标准采购类型";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("宋体", 9F);
            this.label7.Location = new System.Drawing.Point(38, 24);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(67, 15);
            this.label7.TabIndex = 29;
            this.label7.Text = "采购类型";
            // 
            // wlmc_cmb
            // 
            this.wlmc_cmb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.wlmc_cmb.FormattingEnabled = true;
            this.wlmc_cmb.Location = new System.Drawing.Point(587, 59);
            this.wlmc_cmb.Margin = new System.Windows.Forms.Padding(4);
            this.wlmc_cmb.Name = "wlmc_cmb";
            this.wlmc_cmb.Size = new System.Drawing.Size(265, 25);
            this.wlmc_cmb.TabIndex = 28;
            this.wlmc_cmb.Text = "重轨 38KG/m 12m U71Mn";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("宋体", 9F);
            this.label3.Location = new System.Drawing.Point(505, 64);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(67, 15);
            this.label3.TabIndex = 27;
            this.label3.Text = "物料名称";
            // 
            // wlbh_cmb
            // 
            this.wlbh_cmb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.wlbh_cmb.FormattingEnabled = true;
            this.wlbh_cmb.ItemHeight = 17;
            this.wlbh_cmb.Location = new System.Drawing.Point(120, 59);
            this.wlbh_cmb.Margin = new System.Windows.Forms.Padding(4);
            this.wlbh_cmb.Name = "wlbh_cmb";
            this.wlbh_cmb.Size = new System.Drawing.Size(265, 25);
            this.wlbh_cmb.TabIndex = 26;
            this.wlbh_cmb.Text = "F0000011";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("宋体", 9F);
            this.label2.Location = new System.Drawing.Point(38, 64);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 15);
            this.label2.TabIndex = 25;
            this.label2.Text = "物料编号";
            // 
            // AuditSubmitStandard_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1124, 718);
            this.Controls.Add(this.name_label);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "AuditSubmitStandard_Form";
            this.Text = "审核提交";
            this.panel4.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button cancel_bt;
        private System.Windows.Forms.Button return_bt;
        private System.Windows.Forms.Button submit_bt;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.DateTimePicker tgrq_dt;
        private System.Windows.Forms.ComboBox shzt_cmb;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DateTimePicker sqrq_dt;
        private System.Windows.Forms.TextBox lxdh_tb;
        private System.Windows.Forms.TextBox xqsl_tb;
        private System.Windows.Forms.TextBox mindhpl_tb;
        private System.Windows.Forms.RichTextBox qtyq_rtb;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox sqr_cmb;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox sqbm_cmb;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox jldw_tb;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox ckbh_cmb;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox xqdh_tb;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ComboBox wlfs_cmb;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cglx_cmb;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox wlmc_cmb;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox wlbh_cmb;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label name_label;

    }
}