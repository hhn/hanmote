﻿namespace MMClient.SourcingManage.SourcingManagement
{
    partial class VolvoInquiry0_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.demandGridView = new System.Windows.Forms.DataGridView();
            this.demandId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.purchaseType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.proposerId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.state = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.factoryId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.applyTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.reviewTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.stockId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.measurement = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.demandCount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.materialGroup = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.materialName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.materialId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.materialGridView = new System.Windows.Forms.DataGridView();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.sqr_cmb = new System.Windows.Forms.ComboBox();
            this.type_cmb = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.wlbh_cmb = new System.Windows.Forms.ComboBox();
            this.xqdh_cmb = new System.Windows.Forms.ComboBox();
            this.query_bt = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.demandGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.materialGridView)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.demandGridView);
            this.groupBox2.Location = new System.Drawing.Point(14, 157);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(938, 204);
            this.groupBox2.TabIndex = 241;
            this.groupBox2.TabStop = false;
            // 
            // demandGridView
            // 
            this.demandGridView.AllowUserToAddRows = false;
            this.demandGridView.BackgroundColor = System.Drawing.SystemColors.Control;
            this.demandGridView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.demandGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.demandGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.demandId,
            this.purchaseType,
            this.proposerId,
            this.state,
            this.factoryId,
            this.applyTime,
            this.reviewTime});
            this.demandGridView.EnableHeadersVisualStyles = false;
            this.demandGridView.Location = new System.Drawing.Point(5, 18);
            this.demandGridView.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.demandGridView.Name = "demandGridView";
            this.demandGridView.RowTemplate.Height = 27;
            this.demandGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.demandGridView.Size = new System.Drawing.Size(922, 181);
            this.demandGridView.TabIndex = 222;
            this.demandGridView.CellMouseEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.demandGridView_CellMouseEnter);
            this.demandGridView.CellMouseLeave += new System.Windows.Forms.DataGridViewCellEventHandler(this.demandGridView_CellMouseLeave);
            this.demandGridView.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.demandGridView_RowPostPaint);
            this.demandGridView.SelectionChanged += new System.EventHandler(this.demandGridView_SelectionChanged);
            this.demandGridView.Click += new System.EventHandler(this.demandGridView_Click);
            // 
            // demandId
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.demandId.DefaultCellStyle = dataGridViewCellStyle1;
            this.demandId.HeaderText = "需求单号";
            this.demandId.Name = "demandId";
            this.demandId.Width = 130;
            // 
            // purchaseType
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.purchaseType.DefaultCellStyle = dataGridViewCellStyle2;
            this.purchaseType.HeaderText = "采购类型";
            this.purchaseType.Name = "purchaseType";
            this.purchaseType.Width = 150;
            // 
            // proposerId
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.proposerId.DefaultCellStyle = dataGridViewCellStyle3;
            this.proposerId.HeaderText = "申请人";
            this.proposerId.Name = "proposerId";
            // 
            // state
            // 
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.state.DefaultCellStyle = dataGridViewCellStyle4;
            this.state.HeaderText = "状态";
            this.state.Name = "state";
            // 
            // factoryId
            // 
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.factoryId.DefaultCellStyle = dataGridViewCellStyle5;
            this.factoryId.HeaderText = "工厂编号";
            this.factoryId.Name = "factoryId";
            // 
            // applyTime
            // 
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.applyTime.DefaultCellStyle = dataGridViewCellStyle6;
            this.applyTime.HeaderText = "申请日期";
            this.applyTime.Name = "applyTime";
            this.applyTime.Width = 120;
            // 
            // reviewTime
            // 
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.reviewTime.DefaultCellStyle = dataGridViewCellStyle7;
            this.reviewTime.HeaderText = "审核日期";
            this.reviewTime.Name = "reviewTime";
            this.reviewTime.Width = 120;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label4.Location = new System.Drawing.Point(12, 384);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(57, 12);
            this.label4.TabIndex = 239;
            this.label4.Text = "物料信息";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label3.Location = new System.Drawing.Point(12, 142);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 12);
            this.label3.TabIndex = 238;
            this.label3.Text = "需求计划";
            // 
            // stockId
            // 
            this.stockId.HeaderText = "仓库编号";
            this.stockId.Name = "stockId";
            // 
            // measurement
            // 
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.measurement.DefaultCellStyle = dataGridViewCellStyle8;
            this.measurement.HeaderText = "计量单位";
            this.measurement.Name = "measurement";
            // 
            // demandCount
            // 
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.demandCount.DefaultCellStyle = dataGridViewCellStyle9;
            this.demandCount.HeaderText = "需求数量";
            this.demandCount.Name = "demandCount";
            // 
            // materialGroup
            // 
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.materialGroup.DefaultCellStyle = dataGridViewCellStyle10;
            this.materialGroup.HeaderText = "物料组";
            this.materialGroup.Name = "materialGroup";
            // 
            // materialName
            // 
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.materialName.DefaultCellStyle = dataGridViewCellStyle11;
            this.materialName.HeaderText = "物料名称";
            this.materialName.Name = "materialName";
            this.materialName.Width = 150;
            // 
            // materialId
            // 
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.materialId.DefaultCellStyle = dataGridViewCellStyle12;
            this.materialId.HeaderText = "物料编号";
            this.materialId.Name = "materialId";
            this.materialId.Width = 150;
            // 
            // materialGridView
            // 
            this.materialGridView.AllowUserToAddRows = false;
            this.materialGridView.BackgroundColor = System.Drawing.SystemColors.Control;
            this.materialGridView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.materialGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.materialGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.materialId,
            this.materialName,
            this.materialGroup,
            this.demandCount,
            this.measurement,
            this.stockId});
            this.materialGridView.EnableHeadersVisualStyles = false;
            this.materialGridView.Location = new System.Drawing.Point(5, 19);
            this.materialGridView.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.materialGridView.Name = "materialGridView";
            this.materialGridView.RowTemplate.Height = 27;
            this.materialGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.materialGridView.Size = new System.Drawing.Size(748, 159);
            this.materialGridView.TabIndex = 223;
            this.materialGridView.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.materialGridView_RowPostPaint);
            this.materialGridView.SelectionChanged += new System.EventHandler(this.materialGridView_SelectionChanged);
            this.materialGridView.Click += new System.EventHandler(this.materialGridView_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.materialGridView);
            this.groupBox1.Location = new System.Drawing.Point(14, 405);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(938, 183);
            this.groupBox1.TabIndex = 240;
            this.groupBox1.TabStop = false;
            // 
            // sqr_cmb
            // 
            this.sqr_cmb.FormattingEnabled = true;
            this.sqr_cmb.ItemHeight = 12;
            this.sqr_cmb.Location = new System.Drawing.Point(546, 98);
            this.sqr_cmb.Name = "sqr_cmb";
            this.sqr_cmb.Size = new System.Drawing.Size(150, 20);
            this.sqr_cmb.TabIndex = 237;
            this.sqr_cmb.Visible = false;
            // 
            // type_cmb
            // 
            this.type_cmb.FormattingEnabled = true;
            this.type_cmb.ItemHeight = 12;
            this.type_cmb.Location = new System.Drawing.Point(758, 98);
            this.type_cmb.Name = "type_cmb";
            this.type_cmb.Size = new System.Drawing.Size(150, 20);
            this.type_cmb.TabIndex = 236;
            this.type_cmb.Text = "待审核";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("宋体", 9F);
            this.label2.Location = new System.Drawing.Point(711, 101);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 12);
            this.label2.TabIndex = 235;
            this.label2.Text = "状态";
            // 
            // wlbh_cmb
            // 
            this.wlbh_cmb.FormattingEnabled = true;
            this.wlbh_cmb.ItemHeight = 12;
            this.wlbh_cmb.Location = new System.Drawing.Point(313, 98);
            this.wlbh_cmb.Name = "wlbh_cmb";
            this.wlbh_cmb.Size = new System.Drawing.Size(150, 20);
            this.wlbh_cmb.TabIndex = 234;
            this.wlbh_cmb.Visible = false;
            // 
            // xqdh_cmb
            // 
            this.xqdh_cmb.FormattingEnabled = true;
            this.xqdh_cmb.ItemHeight = 12;
            this.xqdh_cmb.Location = new System.Drawing.Point(74, 98);
            this.xqdh_cmb.Name = "xqdh_cmb";
            this.xqdh_cmb.Size = new System.Drawing.Size(150, 20);
            this.xqdh_cmb.TabIndex = 233;
            // 
            // query_bt
            // 
            this.query_bt.Location = new System.Drawing.Point(914, 94);
            this.query_bt.Name = "query_bt";
            this.query_bt.Size = new System.Drawing.Size(63, 25);
            this.query_bt.TabIndex = 232;
            this.query_bt.Text = "查询";
            this.query_bt.UseVisualStyleBackColor = true;
            this.query_bt.Click += new System.EventHandler(this.query_bt_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("宋体", 9F);
            this.label1.Location = new System.Drawing.Point(488, 101);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 12);
            this.label1.TabIndex = 231;
            this.label1.Text = "申请人";
            this.label1.Visible = false;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("宋体", 9F);
            this.label11.Location = new System.Drawing.Point(254, 101);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(53, 12);
            this.label11.TabIndex = 230;
            this.label11.Text = "物料编号";
            this.label11.Visible = false;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("宋体", 9F);
            this.label12.Location = new System.Drawing.Point(12, 101);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(53, 12);
            this.label12.TabIndex = 229;
            this.label12.Text = "需求单号";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(654, 601);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(113, 23);
            this.button1.TabIndex = 242;
            this.button1.Text = "创建成本分析询价";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(124, 602);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(146, 21);
            this.textBox1.TabIndex = 243;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(449, 601);
            this.textBox2.Name = "textBox2";
            this.textBox2.ReadOnly = true;
            this.textBox2.Size = new System.Drawing.Size(146, 21);
            this.textBox2.TabIndex = 244;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 607);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(89, 12);
            this.label5.TabIndex = 245;
            this.label5.Text = "选中的需求单号";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(333, 607);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(89, 12);
            this.label6.TabIndex = 246;
            this.label6.Text = "选中的物料编号";
            // 
            // VolvoInquiry0_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(747, 428);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.sqr_cmb);
            this.Controls.Add(this.type_cmb);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.wlbh_cmb);
            this.Controls.Add(this.xqdh_cmb);
            this.Controls.Add(this.query_bt);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label12);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "VolvoInquiry0_Form";
            this.Text = "创建询价单";
            this.Load += new System.EventHandler(this.VolvoInquiry0_Form_Load);
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.demandGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.materialGridView)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView demandGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn demandId;
        private System.Windows.Forms.DataGridViewTextBoxColumn purchaseType;
        private System.Windows.Forms.DataGridViewTextBoxColumn proposerId;
        private System.Windows.Forms.DataGridViewTextBoxColumn state;
        private System.Windows.Forms.DataGridViewTextBoxColumn factoryId;
        private System.Windows.Forms.DataGridViewTextBoxColumn applyTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn reviewTime;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridViewTextBoxColumn stockId;
        private System.Windows.Forms.DataGridViewTextBoxColumn measurement;
        private System.Windows.Forms.DataGridViewTextBoxColumn demandCount;
        private System.Windows.Forms.DataGridViewTextBoxColumn materialGroup;
        private System.Windows.Forms.DataGridViewTextBoxColumn materialName;
        private System.Windows.Forms.DataGridViewTextBoxColumn materialId;
        private System.Windows.Forms.DataGridView materialGridView;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox sqr_cmb;
        private System.Windows.Forms.ComboBox type_cmb;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox wlbh_cmb;
        private System.Windows.Forms.ComboBox xqdh_cmb;
        private System.Windows.Forms.Button query_bt;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
    }
}