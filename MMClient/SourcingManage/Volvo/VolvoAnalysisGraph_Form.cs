﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;
using System.Data.SqlClient;
using System.Net.Mail;
using System.IO;
using System.Web;
using System.Configuration;
using Aspose.Cells;
using Lib.SqlServerDAL.SourcingManage.ProcurementPlan;
using Lib.Model.SourcingManage.ProcurementPlan;
using Lib.Model.SourcingManage;
using Lib.Bll.SourcingManage.SourcingManagement;
using Lib.Bll.SourcingManage.SourceingRecord;
using Lib.Model.SourcingManage.SourcingManagement;
using Lib.Model.SourcingManage.SourceingRecord;
using Lib.Common.CommonUtils;
using Lib.Model.FileManage;
using MMClient.CommonForms;
using Lib.Bll.FileManageBLL;
using System.Web.UI;
using System.Web.UI.WebControls;
using Lib.SqlServerDAL;
using System.Windows.Forms.DataVisualization.Charting;

namespace MMClient.SourcingManage.SourcingManagement
{
    public partial class VolvoAnalysisGraph_Form : DockContent
    {
        public static string partNumber;
        public static string supplierId;
        string connString;
        string value;
        DataTable dt;
        DataTable ref_Price=new DataTable();
        string s;
        string materialID;

        public VolvoAnalysisGraph_Form()
        {
            InitializeComponent();
        }

        private void VolvoAnalysisGraph_Form_Load(object sender, EventArgs e)
        {
            //comboBox1.DataSource = DBHelper.ExecuteQueryDT("select distinct Part_Number from dbo.Inquiry_Sheets");
            //comboBox1.DisplayMember = "Part_Number";
            //comboBox1.ValueMember = "Part_Number";
            connString = ConfigurationManager.AppSettings["serverAddress"];

            foreach (DataGridViewColumn column in this.dataGridView1.Columns)
            {
                //设置自动排序
                column.SortMode = DataGridViewColumnSortMode.Automatic;
            }

            chart1.GetToolTipText += new EventHandler<ToolTipEventArgs>(chart_GetToolTipText);
            chart2.GetToolTipText += new EventHandler<ToolTipEventArgs>(chart_GetToolTipText);
            chart3.GetToolTipText += new EventHandler<ToolTipEventArgs>(chart_GetToolTipText);
            chart4.GetToolTipText += new EventHandler<ToolTipEventArgs>(chart_GetToolTipText);
            chart5.GetToolTipText += new EventHandler<ToolTipEventArgs>(chart_GetToolTipText);
            chart6.GetToolTipText += new EventHandler<ToolTipEventArgs>(chart_GetToolTipText);
            chart7.GetToolTipText += new EventHandler<ToolTipEventArgs>(chart_GetToolTipText);
            chart8.GetToolTipText += new EventHandler<ToolTipEventArgs>(chart_GetToolTipText);
            chart9.GetToolTipText += new EventHandler<ToolTipEventArgs>(chart_GetToolTipText);
            chart10.GetToolTipText += new EventHandler<ToolTipEventArgs>(chart_GetToolTipText);
            chart11.GetToolTipText += new EventHandler<ToolTipEventArgs>(chart_GetToolTipText);
            chart12.GetToolTipText += new EventHandler<ToolTipEventArgs>(chart_GetToolTipText);
        }

        private void chart_GetToolTipText(object sender, ToolTipEventArgs e)
        {
            //鼠标放在某个柱状图上时显示那个图的数据
            if (e.HitTestResult.ChartElementType == ChartElementType.DataPoint)
            {
                int i = e.HitTestResult.PointIndex;
                DataPoint dp = e.HitTestResult.Series.Points[i];
                //分别显示x轴和y轴的数值，其中{1:F3},表示显示的是float类型,精确到小数点后3位  
                e.Text = string.Format("供应商:{0};价格:{1:F3} ", e.HitTestResult.Series.Name, dp.YValues[0]);
            }           
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            //comboBox1里选择Part_Number，数据显示在dataGridView1里
            //value = this.comboBox1.SelectedValue.ToString();
            value = this.comboBox1.Text;


            DataTable dt = DBHelper.ExecuteQueryDT("select count(*) from dbo.Inquiry_Sheets where Part_Number = '" + value + "' and Confirmed = '1'");
            if (dt.Rows.Count > 0)
            {
                dataGridView1.DataSource = DBHelper.ExecuteQueryDT("select Part_Number as 询价号, Supplier_ID as 供应商号,Grand_Total_Piece_Cost as 总计成本,  Standard_Part_Cost as 标准件成本, " +
                "Raw_Material_Cost as 原材料成本, Purchased_Part_Ext_Oper as 购买零件或分机操作成本, " +
                "Process_Cost as 处理成本, Sub_Total as 小计, Packing_Cost as 包装成本, Landing_Cost as 着陆成本, " +
                "Other_Cost as 其他成本, Raw_Material_Alloy_Surcharge as 原材料合金附加费, Grand_Total_Tooling_Cost as 总计模具成本 from dbo.Inquiry_Summary where Part_Number ='" + value + "'");
                DataTable dt2 = new DataTable();
                dt2 = DBHelper.ExecuteQueryDT("select Material_Name,Yearly_Volume,Material_ID from dbo.Inquiry_Base where Part_Number = '" + value + "'");
                label4.Text = dt2.Rows[0][0].ToString();
                label5.Text= dt2.Rows[0][1].ToString();
                materialID= dt2.Rows[0][2].ToString();

                string sql1 = "select TOP (1) level_1_partNum,level_1_supplierID,level_standardPart_price," +
                    "level_rawMaterial_price,level_PurchasedPartsExtOper_price,level_ProcessCostMelting_price,level_ProcessCostDetails_price," +
                    "level_PackagingCost_price,level_LandingCost_price,level_OtherCost_price,level_TotalMaterialCost_price," +
                    "level_TotalHeatthreatmentCost_price,level_TotalToolMakerCost_price,level_TotalOverheadCost_price" +
                    " from dbo.Inquiry_Ref_Price  where material_ID='" + materialID + "' order by ValidDate desc";
                ref_Price = DBHelper.ExecuteQueryDT(sql1);
                if(ref_Price.Rows.Count>0)
                {
                    label6.Text = "该物料存在标杆";
                }else
                {
                    label6.Text = "该物料不存在标杆";
                }
            }
            else
            {
                label4.Text = "";
                label5.Text = "";
                label6.Text = "";
                materialID = "";
                MessageBox.Show("没有供应商报价方案");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //删除
            if (listBox1.SelectedItem != null)
            {
                listBox1.Items.Remove(listBox1.SelectedItem);
            }
            else
            {
                MessageBox.Show("请先选择供应商");
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //添加
            if (dataGridView1.Rows.Count > 0)
            {
                int index = dataGridView1.CurrentRow.Index; //获取选中行的行号
                if (index >= 0)
                {
                    //将选中的
                    string supplierIDSelected = dataGridView1.Rows[index].Cells[1].Value.ToString();
                    if (!listBox1.Items.Contains(supplierIDSelected) && !listBox1.Items.Contains(supplierIDSelected))
                    {
                        listBox1.Items.Add(supplierIDSelected);
                    }
                    else
                    {
                        MessageBox.Show("重复选择");
                    }
                }
                else
                {
                    MessageBox.Show("请先选择供应商");
                }
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            List<string> list = new List<string>();
            string supplierID = "";
            string sid = "";
            DataTable dt1 = new DataTable();
            string sql = "";

            for (int i = 0; i < listBox1.Items.Count; i++)
            {
                list.Add("'"+listBox1.Items[i].ToString()+"'");
                s = string.Join(",", list);
                supplierID += " Or Supplier_ID = " + listBox1.Items[i].ToString();
            }
            if (string.IsNullOrEmpty(supplierID))
            {
                MessageBox.Show("当前没有选择供应商");
                return;
            }
            


            //显示直方图
            SqlConnection conn1 = new SqlConnection(connString);
            conn1.Open();

            SqlDataAdapter sda1 = new SqlDataAdapter("select Supplier_ID, Standard_Part_Cost, Raw_Material_Cost, " +
            "Purchased_Part_Ext_Oper, Process_Cost, Sub_Total, Packing_Cost, Landing_Cost, " +
            "Other_Cost, Raw_Material_Alloy_Surcharge, Grand_Total_Piece_Cost, Grand_Total_Tooling_Cost from dbo.Inquiry_Summary " +
            "where Part_Number ='" + value + "' and Supplier_ID IN (" + s + ")", conn1);

            DataTable dt = new DataTable();
            sda1.Fill(dt);
            conn1.Close();

            //DataTable dt = ds1.Tables[0];
            //chart1.DataSource = dt;
            if (dt.Rows.Count == 0)
            {
                return;
            }
            chart1.Series.Clear();
            chart2.Series.Clear();
            chart3.Series.Clear();
            chart4.Series.Clear();
            chart5.Series.Clear();
            chart6.Series.Clear();
            chart7.Series.Clear();
            chart8.Series.Clear();
            chart9.Series.Clear();
            chart10.Series.Clear();
            chart11.Series.Clear();
            chart12.Series.Clear();
            //chart1.DataSource = dt;

            List<int> x = new List<int>() { 1 };
            List<float> fDownTime = new List<float>();
            for (int j = 0; j < dt.Rows.Count; j++)
            {
                //stand_part
                fDownTime.Clear();
                fDownTime.Add(float.Parse(dt.Rows[j][1].ToString()));
                chart1.Series.Add(list[j]);//填写供应商编号
                chart1.Series[list[j]].XValueType = ChartValueType.String;
                chart1.Series[list[j]].Points.DataBindXY(x, fDownTime);
                chart1.Series[list[j]].ChartType = SeriesChartType.Column;

                //raw material
                fDownTime.Clear();
                fDownTime.Add(float.Parse(dt.Rows[j][2].ToString()));
                chart2.Series.Add(list[j]);//此处要改成supplierID
                chart2.Series[list[j]].XValueType = ChartValueType.String;
                chart2.Series[list[j]].Points.DataBindXY(x, fDownTime);
                chart2.Series[list[j]].ChartType = SeriesChartType.Column;

                //ppoe
                fDownTime.Clear();
                fDownTime.Add(float.Parse(dt.Rows[j][3].ToString()));
                chart3.Series.Add(list[j]);//此处要改成supplierID
                chart3.Series[list[j]].XValueType = ChartValueType.String;
                chart3.Series[list[j]].Points.DataBindXY(x, fDownTime);
                chart3.Series[list[j]].ChartType = SeriesChartType.Column;


                //ProcessCostMelting               
                fDownTime.Clear();
                dt1.Clear();
                sql = string.Format("select sum(TotalCost) from dbo.Inquiry_Process_Cost_Melting where Part_Number='{0}' and Supplier_ID={1}", value, list[j]);
                dt1 = DBHelper.ExecuteQueryDT(sql);              
                if(dt1.Rows.Count>0)
                {
                    fDownTime.Add(float.Parse(dt1.Rows[0][0].ToString()));
                }
                else
                {
                    fDownTime.Add(0);
                }
                chart4.Series.Add(list[j]);//此处要改成supplierID
                chart4.Series[list[j]].XValueType = ChartValueType.String;
                chart4.Series[list[j]].Points.DataBindXY(x, fDownTime);
                chart4.Series[list[j]].ChartType = SeriesChartType.Column;

                //ProcessCostDetails
                fDownTime.Clear();
                dt1.Clear();
                sql = string.Format("select sum(Total_Cost) from dbo.Inquiry_Process_Cost_Details where Part_Number='{0}' and Supplier_ID={1}", value, list[j]);
                dt1 = DBHelper.ExecuteQueryDT(sql);
                if (dt1.Rows.Count > 0)
                {
                    fDownTime.Add(float.Parse(dt1.Rows[0][0].ToString()));
                }
                else
                {
                    fDownTime.Add(0);
                }
                chart5.Series.Add(list[j]);//此处要改成supplierID
                chart5.Series[list[j]].XValueType = ChartValueType.String;
                chart5.Series[list[j]].Points.DataBindXY(x, fDownTime);
                chart5.Series[list[j]].ChartType = SeriesChartType.Column;


                //Packing
                fDownTime.Clear();
                fDownTime.Add(float.Parse(dt.Rows[j][6].ToString()));
                chart6.Series.Add(list[j]);//此处要改成supplierID
                chart6.Series[list[j]].XValueType = ChartValueType.String;
                chart6.Series[list[j]].Points.DataBindXY(x, fDownTime);
                chart6.Series[list[j]].ChartType = SeriesChartType.Column;

                //landing
               
                fDownTime.Clear();
                dt1.Clear();
                sql = string.Format("select LandingCostTotal from dbo.Inquiry_Logistics where Part_Number='{0}' and Supplier_ID={1}", value, list[j]);
                dt1 = DBHelper.ExecuteQueryDT(sql);
                if (dt1.Rows.Count > 0)
                {
                    fDownTime.Add(float.Parse(dt1.Rows[0][0].ToString()));
                }
                else
                {
                    fDownTime.Add(0);
                }
                chart7.Series.Add(list[j]);//此处要改成supplierID
                chart7.Series[list[j]].XValueType = ChartValueType.String;
                chart7.Series[list[j]].Points.DataBindXY(x, fDownTime);
                chart7.Series[list[j]].ChartType = SeriesChartType.Column;

                //other
                fDownTime.Clear();
                fDownTime.Add(float.Parse(dt.Rows[j][8].ToString()));
                chart8.Series.Add(list[j]);//此处要改成supplierID
                chart8.Series[list[j]].XValueType = ChartValueType.String;
                chart8.Series[list[j]].Points.DataBindXY(x, fDownTime);
                chart8.Series[list[j]].ChartType = SeriesChartType.Column;

                //TB_MaterialCost
                fDownTime.Clear();
                dt1.Clear();
                sql = string.Format("select sum(Cost_CNY) from dbo.Inquiry_TB_MaterialCost where Part_Number='{0}' and Supplier_ID={1}", value, list[j]);
                dt1 = DBHelper.ExecuteQueryDT(sql);

                if (dt1.Rows.Count > 0)
                {
                    fDownTime.Add(float.Parse(dt1.Rows[0][0].ToString()));
                }
                else
                {
                    fDownTime.Add(0);
                }
                chart9.Series.Add(list[j]);//此处要改成supplierID
                chart9.Series[list[j]].XValueType = ChartValueType.String;
                chart9.Series[list[j]].Points.DataBindXY(x, fDownTime);
                chart9.Series[list[j]].ChartType = SeriesChartType.Column;


                //Heatthreatment_Cost
                fDownTime.Clear();
                dt1.Clear();
                sql = string.Format("select sum(Heatthreatment_Cost) from dbo.Inquiry_Tooling_Breakdown where Part_Number='{0}' and Supplier_ID={1}", value, list[j]);
                dt1 = DBHelper.ExecuteQueryDT(sql);

                if (dt1.Rows.Count > 0)
                {
                    fDownTime.Add(float.Parse(dt1.Rows[0][0].ToString()));
                }
                else
                {
                    fDownTime.Add(0);
                }
                chart10.Series.Add(list[j]);//此处要改成supplierID
                chart10.Series[list[j]].XValueType = ChartValueType.String;
                chart10.Series[list[j]].Points.DataBindXY(x, fDownTime);
                chart10.Series[list[j]].ChartType = SeriesChartType.Column;

                //Inquiry_TB_Toolmaker
                fDownTime.Clear();
                dt1.Clear();
                sql = string.Format("select sum(Total_ToolmakerCost) from dbo.Inquiry_TB_Toolmaker where Part_Number='{0}' and Supplier_ID={1}", value, list[j]);
                dt1 = DBHelper.ExecuteQueryDT(sql);

                if (dt1.Rows.Count > 0)
                {
                    fDownTime.Add(float.Parse(dt1.Rows[0][0].ToString()));
                }
                else
                {
                    fDownTime.Add(0);
                }              
                chart11.Series.Add(list[j]);//此处要改成supplierID
                chart11.Series[list[j]].XValueType = ChartValueType.String;
                chart11.Series[list[j]].Points.DataBindXY(x, fDownTime);
                chart11.Series[list[j]].ChartType = SeriesChartType.Column;


                //Inquiry_TB_OverheadCost
                fDownTime.Clear();
                dt1.Clear();
                sql = string.Format("select sum(Total_OverheadCost) from dbo.Inquiry_TB_OverheadCost where Part_Number='{0}' and Supplier_ID={1}", value, list[j]);
                dt1 = DBHelper.ExecuteQueryDT(sql);

                if (dt1.Rows.Count > 0)
                {
                    fDownTime.Add(float.Parse(dt1.Rows[0][0].ToString()));
                }
                else
                {
                    fDownTime.Add(0);
                }
                chart12.Series.Add(list[j]);//此处要改成supplierID
                chart12.Series[list[j]].XValueType = ChartValueType.String;
                chart12.Series[list[j]].Points.DataBindXY(x, fDownTime);
                chart12.Series[list[j]].ChartType = SeriesChartType.Column;

            }
           
            if (ref_Price.Rows.Count > 0)
            {
                string pn, sp;
                // DataTable dt2 = DBHelper(string.Format("select  from Inquiry_Ref_Price"));
                DataTable dt2 = DBHelper.ExecuteQueryDT(string.Format("Select * from dbo.Inquiry_Ref_Price where Material_ID='{0}'",materialID));
                sql = string.Format("select Supplier_ID, Standard_Part_Cost, Raw_Material_Cost, " +
            "Purchased_Part_Ext_Oper, Process_Cost, Sub_Total, Packing_Cost, Landing_Cost, " +
            "Other_Cost, Raw_Material_Alloy_Surcharge, Grand_Total_Piece_Cost, Grand_Total_Tooling_Cost from dbo.Inquiry_Summary " +
            "where Part_Number ='{0}' and Supplier_ID= '{1}'", dt2.Rows[0][5].ToString(), dt2.Rows[0][6].ToString());
                dt=DBHelper.ExecuteQueryDT(sql);
                fDownTime.Clear();
                fDownTime.Add(float.Parse(dt.Rows[0][1].ToString()));
                chart1.Series.Add("标杆");//填写供应商编号
                chart1.Series["标杆"].XValueType = ChartValueType.String;
                chart1.Series["标杆"].Points.DataBindXY(x, fDownTime);
                chart1.Series["标杆"].ChartType = SeriesChartType.Column;

                //raw material
                fDownTime.Clear();
                fDownTime.Add(float.Parse(dt.Rows[0][2].ToString()));
                chart2.Series.Add("标杆");//此处要改成supplierID
                chart2.Series["标杆"].XValueType = ChartValueType.String;
                chart2.Series["标杆"].Points.DataBindXY(x, fDownTime);
                chart2.Series["标杆"].ChartType = SeriesChartType.Column;

                //ppoe
                fDownTime.Clear();
                fDownTime.Add(float.Parse(dt.Rows[0][3].ToString()));
                chart3.Series.Add("标杆");//此处要改成supplierID
                chart3.Series["标杆"].XValueType = ChartValueType.String;
                chart3.Series["标杆"].Points.DataBindXY(x, fDownTime);
                chart3.Series["标杆"].ChartType = SeriesChartType.Column;


                //ProcessCostMelting               
                fDownTime.Clear();
                dt1.Clear();
                sql = string.Format("select sum(TotalCost) from dbo.Inquiry_Process_Cost_Melting where Part_Number='{0}' and Supplier_ID={1}", dt2.Rows[0][5].ToString(), dt2.Rows[0][6].ToString());
                dt1 = DBHelper.ExecuteQueryDT(sql);
                if (dt1.Rows.Count > 0)
                {
                    fDownTime.Add(float.Parse(dt1.Rows[0][0].ToString()));
                }
                else
                {
                    fDownTime.Add(0);
                }
                chart4.Series.Add("标杆");//此处要改成supplierID
                chart4.Series["标杆"].XValueType = ChartValueType.String;
                chart4.Series["标杆"].Points.DataBindXY(x, fDownTime);
                chart4.Series["标杆"].ChartType = SeriesChartType.Column;

                //ProcessCostDetails
                fDownTime.Clear();
                dt1.Clear();
                sql = string.Format("select sum(Total_Cost) from dbo.Inquiry_Process_Cost_Details where Part_Number='{0}' and Supplier_ID={1}", dt2.Rows[0][5].ToString(), dt2.Rows[0][6].ToString());
                dt1 = DBHelper.ExecuteQueryDT(sql);
                if (dt1.Rows.Count > 0)
                {
                    fDownTime.Add(float.Parse(dt1.Rows[0][0].ToString()));
                }
                else
                {
                    fDownTime.Add(0);
                }
                chart5.Series.Add("标杆");//此处要改成supplierID
                chart5.Series["标杆"].XValueType = ChartValueType.String;
                chart5.Series["标杆"].Points.DataBindXY(x, fDownTime);
                chart5.Series["标杆"].ChartType = SeriesChartType.Column;


                //Packing
                fDownTime.Clear();
                fDownTime.Add(float.Parse(dt.Rows[0][6].ToString()));
                chart6.Series.Add("标杆");//此处要改成supplierID
                chart6.Series["标杆"].XValueType = ChartValueType.String;
                chart6.Series["标杆"].Points.DataBindXY(x, fDownTime);
                chart6.Series["标杆"].ChartType = SeriesChartType.Column;

                //landing

                fDownTime.Clear();
                dt1.Clear();
                sql = string.Format("select LandingCostTotal from dbo.Inquiry_Logistics where Part_Number='{0}' and Supplier_ID={1}", dt2.Rows[0][5].ToString(), dt2.Rows[0][6].ToString());
                dt1 = DBHelper.ExecuteQueryDT(sql);
                if (dt1.Rows.Count > 0)
                {
                    fDownTime.Add(float.Parse(dt1.Rows[0][0].ToString()));
                }
                else
                {
                    fDownTime.Add(0);
                }
                chart7.Series.Add("标杆");//此处要改成supplierID
                chart7.Series["标杆"].XValueType = ChartValueType.String;
                chart7.Series["标杆"].Points.DataBindXY(x, fDownTime);
                chart7.Series["标杆"].ChartType = SeriesChartType.Column;

                //other
                fDownTime.Clear();
                fDownTime.Add(float.Parse(dt.Rows[0][8].ToString()));
                chart8.Series.Add("标杆");//此处要改成supplierID
                chart8.Series["标杆"].XValueType = ChartValueType.String;
                chart8.Series["标杆"].Points.DataBindXY(x, fDownTime);
                chart8.Series["标杆"].ChartType = SeriesChartType.Column;

                //TB_MaterialCost
                fDownTime.Clear();
                dt1.Clear();
                sql = string.Format("select sum(Cost_CNY) from dbo.Inquiry_TB_MaterialCost where Part_Number='{0}' and Supplier_ID={1}", dt2.Rows[0][5].ToString(), dt2.Rows[0][6].ToString());
                dt1 = DBHelper.ExecuteQueryDT(sql);

                if (dt1.Rows.Count > 0)
                {
                    fDownTime.Add(float.Parse(dt1.Rows[0][0].ToString()));
                }
                else
                {
                    fDownTime.Add(0);
                }
                chart9.Series.Add("标杆");//此处要改成supplierID
                chart9.Series["标杆"].XValueType = ChartValueType.String;
                chart9.Series["标杆"].Points.DataBindXY(x, fDownTime);
                chart9.Series["标杆"].ChartType = SeriesChartType.Column;


                //Heatthreatment_Cost
                fDownTime.Clear();
                dt1.Clear();
                sql = string.Format("select sum(Heatthreatment_Cost) from dbo.Inquiry_Tooling_Breakdown where Part_Number='{0}' and Supplier_ID={1}", dt2.Rows[0][5].ToString(), dt2.Rows[0][6].ToString());
                dt1 = DBHelper.ExecuteQueryDT(sql);

                if (dt1.Rows.Count > 0)
                {
                    fDownTime.Add(float.Parse(dt1.Rows[0][0].ToString()));
                }
                else
                {
                    fDownTime.Add(0);
                }
                chart10.Series.Add("标杆");//此处要改成supplierID
                chart10.Series["标杆"].XValueType = ChartValueType.String;
                chart10.Series["标杆"].Points.DataBindXY(x, fDownTime);
                chart10.Series["标杆"].ChartType = SeriesChartType.Column;

                //Inquiry_TB_Toolmaker
                fDownTime.Clear();
                dt1.Clear();
                sql = string.Format("select sum(Total_ToolmakerCost) from dbo.Inquiry_TB_Toolmaker where Part_Number='{0}' and Supplier_ID={1}", dt2.Rows[0][5].ToString(), dt2.Rows[0][6].ToString());
                dt1 = DBHelper.ExecuteQueryDT(sql);

                if (dt1.Rows.Count > 0)
                {
                    fDownTime.Add(float.Parse(dt1.Rows[0][0].ToString()));
                }
                else
                {
                    fDownTime.Add(0);
                }
                chart11.Series.Add("标杆");//此处要改成supplierID
                chart11.Series["标杆"].XValueType = ChartValueType.String;
                chart11.Series["标杆"].Points.DataBindXY(x, fDownTime);
                chart11.Series["标杆"].ChartType = SeriesChartType.Column;


                //Inquiry_TB_OverheadCost
                fDownTime.Clear();
                dt1.Clear();
                sql = string.Format("select sum(Total_OverheadCost) from dbo.Inquiry_TB_OverheadCost where Part_Number='{0}' and Supplier_ID={1}", dt2.Rows[0][5].ToString(), dt2.Rows[0][6].ToString());
                dt1 = DBHelper.ExecuteQueryDT(sql);

                if (dt1.Rows.Count > 0)
                {
                    fDownTime.Add(float.Parse(dt1.Rows[0][0].ToString()));
                }
                else
                {
                    fDownTime.Add(0);
                }
                chart12.Series.Add("标杆");//此处要改成supplierID
                chart12.Series["标杆"].XValueType = ChartValueType.String;
                chart12.Series["标杆"].Points.DataBindXY(x, fDownTime);
                chart12.Series["标杆"].ChartType = SeriesChartType.Column;


            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            //根据选出的Start_Date时间范围，筛选出Part_Number
            DateTime startTime = dateTimePicker1.Value;           
            DateTime endTime = dateTimePicker2.Value;
            string sql = string.Format("select Part_Number from dbo.Inquiry_Base where Start_Date between '{0}' and  '{1}'", startTime, endTime);
            //DBHelper.
            SqlConnection conn1 = new SqlConnection(connString);
            conn1.Open();
            SqlDataAdapter sda1 = new SqlDataAdapter(sql, conn1);
            DataTable dt = new DataTable();
            sda1.Fill(dt);
            conn1.Close();

            if(dt.Rows.Count==0)
            {
                comboBox1.Items.Clear();
                MessageBox.Show("该时间段没有询价");
                dataGridView1.DataSource = null;
                listBox1.Items.Clear();
                comboBox1.Text = "";
                label4.Text= "";
                label5.Text = "";
                materialID = "";
            }
            else
            {
                comboBox1.Items.Clear();
                for(int i=0;i<dt.Rows.Count;i++)
                {
                    comboBox1.Items.Add(dt.Rows[i][0].ToString());
                }
                MessageBox.Show(string.Format("存在{0}条记录",dt.Rows.Count.ToString()));
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            int index = dataGridView1.CurrentRow.Index;
            if (materialID!="" & index>=0)
            {
                
                string pn = dataGridView1.Rows[index].Cells[0].Value.ToString(); 
                string sp= dataGridView1.Rows[index].Cells[1].Value.ToString();
                DataTable dt1 = DBHelper.ExecuteQueryDT("select TOP (1) level_1_partNum,level_1_supplierID,level_standardPart_price," +
                    "level_rawMaterial_price,level_PurchasedPartsExtOper_price,level_ProcessCostMelting_price,level_ProcessCostDetails_price," +
                    "level_PackagingCost_price,level_LandingCost_price,level_OtherCost_price,level_TotalMaterialCost_price," +
                    "level_TotalHeatthreatmentCost_price,level_TotalToolMakerCost_price,level_TotalOverheadCost_price" +
                    " from dbo.Inquiry_Ref_Price  where material_ID='" + materialID + "' order by ValidDate desc");
                DataTable dt2 = DBHelper.ExecuteQueryDT(string.Format("select Start_Date, material_Name from dbo.Inquiry_Base where Part_Number='{0}'",pn));
                string validate = dt2.Rows[0][0].ToString();
                string materialName = dt2.Rows[0][1].ToString();
                if(dt1.Rows.Count>0)
                {
                    DBHelper.ExecuteNonQuery(string.Format("update dbo.Inquiry_Ref_Price set level_1_partNum='{0}',level_1_supplierID='{1}',ValidDate='{2}' where material_ID='{3}'",pn,sp,validate,materialID));
                    MessageBox.Show("更新成功");
                }else
                {
                    DBHelper.ExecuteNonQuery(string.Format("insert into dbo.Inquiry_Ref_Price(material_ID,level_1_partNum,level_1_supplierID, ValidDate,material_name) values('{0}','{1}','{2}','{3}','{4}')",materialID,pn,sp,validate, materialName));
                    MessageBox.Show("保存成功");
                }
                
            }
        }
    }
}
