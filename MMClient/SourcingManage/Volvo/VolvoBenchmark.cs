﻿using Lib.SqlServerDAL;
using MMClient.SourcingManage.SourcingManagement;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;

namespace MMClient.SourcingManage.Volvo
{
    public partial class VolvoBenchmark : DockContent
{
        public VolvoBenchmark()
        {
            InitializeComponent();
        }

        private void VolvoBenchmark_Load(object sender, EventArgs e)
        {
            string sql1 = "select material_ID as 物料ID,material_name as 物料名,level_1_partNum as 询价号,level_1_supplierID 供应商号,ValidDate as 时间" +
                     " from dbo.Inquiry_Ref_Price  order by ValidDate desc";
            DataTable ref_Price = DBHelper.ExecuteQueryDT(sql1);
            if (ref_Price.Rows.Count > 0)
            {
                label2.Text=ref_Price.Rows.Count.ToString();
                dataGridView1.DataSource = ref_Price;
            }
            else
            {
                label2.Text = "0";
            }
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int index = dataGridView1.CurrentRow.Index; //获取选中行的行号
            if (index >= 0)
            {
                string partNumber = dataGridView1.Rows[index].Cells[2].Value.ToString();
                string supplierId = dataGridView1.Rows[index].Cells[3].Value.ToString();
                VolvoReportView_Form frm1 = new VolvoReportView_Form(partNumber, supplierId);
                frm1.Show();
            };
        }
    }
}
