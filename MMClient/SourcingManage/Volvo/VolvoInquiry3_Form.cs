﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Net.Mail;
using Aspose.Cells;
using Lib.SqlServerDAL.SourcingManage.ProcurementPlan;
using Lib.Model.SourcingManage.ProcurementPlan;
using Lib.Model.SourcingManage;
using Lib.Bll.SourcingManage.SourcingManagement;
using Lib.Bll.SourcingManage.SourceingRecord;
using WeifenLuo.WinFormsUI.Docking;
using System.IO;
using Lib.Model.SourcingManage.SourcingManagement;
using Lib.Model.SourcingManage.SourceingRecord;
using Lib.Common.CommonUtils;
using Lib.Model.FileManage;
using MMClient.CommonForms;
using Lib.Bll.FileManageBLL;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using Lib.SqlServerDAL;

namespace MMClient.SourcingManage.SourcingManagement
{
    public partial class VolvoInquiry3_Form : DockContent
    {
        public static string partNumber;
        public static string supplierId;
        string connString;
        string value;

        public VolvoInquiry3_Form()
        {
            InitializeComponent();
        }

        private void VolvoInquiry3_Form_Load(object sender, EventArgs e)
        {
            connString = ConfigurationManager.AppSettings["serverAddress"];
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            //选中下拉框里的某个Part_Number
            //string value = this.comboBox1.SelectedValue.ToString();
            value = this.comboBox1.Text;


            //根据下拉框选取的Part_Number和Confirmed=0条件，读取dbo.Inquiry_Sheets表中的Supplier_ID，Supplier_Org_ID列绑定到没有提交的供应商的下拉框里
            //SqlConnection conn2 = new SqlConnection("server=116.196.99.18;database=MMBackup;uid=sa;pwd=b602!@#qwe");
            //DataSet ds2 = new DataSet();
            //SqlDataAdapter sda2 = new SqlDataAdapter("select Supplier_ID,Supplier_Org_ID from dbo.Inquiry_Sheets where Part_Number='" + value + "' and Confirmed='0'", conn2);
            //sda2.Fill(ds2, "em2");
            //dataGridView2.DataSource = ds2.Tables["em2"];

            dataGridView2.DataSource = DBHelper.ExecuteQueryDT("select Supplier_ID as 供应商号 from dbo.Inquiry_Sheets where Part_Number='" + value + "' and Confirmed='0'");

            //根据下拉框选取的Part_Number和Confirmed=1条件，读取dbo.Inquiry_Summary到询价单总价表里
            //SqlConnection conn3 = new SqlConnection("server=116.196.99.18;database=MMBackup;uid=sa;pwd=b602!@#qwe");
            //SqlDataAdapter sda3 = new SqlDataAdapter("select * from dbo.Inquiry_Summary where Part_Number=(select Part_Number from dbo.Inquiry_Sheets where Part_Number='" + value + "' and Confirmed='1')", conn3);
            //DataSet ds3 = new DataSet();
            //sda3.Fill(ds3, "em3");
            //dataGridView1.DataSource = ds3.Tables["em3"];
            //dataGridView1.DataSource = DBHelper.ExecuteQueryDT("select Part_Number as 询价号, Supplier_ID as 供应商号, Standard_Part_Cost as 标准件成本, Standard_Part_Cost_Percent as 标准件成本百分比, Raw_Material_Cost as 原材料成本, Raw_Material_Cost_Percent as 原材料成本百分比, Purchased_Part_Ext_Oper as 购买零件或分机操作成本, " +
            //    "Purchased_Part_Ext_Oper_Percent as 购买零件或分机操作成本百分比, Process_Cost as 处理成本, Process_Cost_Percent as 处理成本百分比, Sub_Total as 小计, Sub_Total_Percent as 小计百分比, Packing_Cost as 包装成本, Packing_Cost_Percent as 包装成本百分比, Landing_Cost as 着陆成本, Landing_Cost_Percent as 着陆成本百分比, " +
            //    "Other_Cost as 其他成本, Other_Cost_Percent as 其他成本百分比, Raw_Material_Alloy_Surcharge as 原材料合金附加费, Raw_Material_Alloy_Surcharge_Percent as 原材料合金附加费百分比, Grand_Total_Piece_Cost as 总计成本, Grand_Total_Piece_Cost_Percent as 总计成本百分比, Grand_Total_Tooling_Cost as 总计模具成本, " +
            //    "Grand_Total_Tooling_Cost_Percent as 总计模具成本百分比, Comment as 备注 from dbo.Inquiry_Summary where Part_Number=(select Part_Number from dbo.Inquiry_Sheets where Part_Number='" + value + "' and Confirmed='1')");

            DataTable dt = DBHelper.ExecuteQueryDT("select count(*) from dbo.Inquiry_Sheets where Part_Number = '" + value + "' and Confirmed = '1'");
            if (dt.Rows.Count>0)
            {
                dataGridView1.DataSource = DBHelper.ExecuteQueryDT("select Part_Number as 询价号, Supplier_ID as 供应商号, Standard_Part_Cost as 标准件成本, Raw_Material_Cost as 原材料成本, Purchased_Part_Ext_Oper as 购买零件或分机操作成本, " +
                "Process_Cost as 处理成本, Sub_Total as 小计, Packing_Cost as 包装成本, Landing_Cost as 着陆成本, " +
                "Other_Cost as 其他成本, Raw_Material_Alloy_Surcharge as 原材料合金附加费, Grand_Total_Piece_Cost as 总计成本, Grand_Total_Tooling_Cost as 总计模具成本, " +
                "Comment as 备注 from dbo.Inquiry_Summary where Part_Number ='" + value + "'");
            }
            

                //" =(select Part_Number from dbo.Inquiry_Sheets where Part_Number='" + value + "' and Confirmed='1')");
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            //点击询价单总价表里某条记录，弹出该条记录的详细信息的水晶报表页面
            int index = dataGridView1.CurrentRow.Index; //获取选中行的行号
            if (index>=0)
            {
                partNumber = dataGridView1.Rows[index].Cells[0].Value.ToString();
                supplierId = dataGridView1.Rows[index].Cells[1].Value.ToString();
                VolvoReportView_Form frm1 = new VolvoReportView_Form(partNumber,supplierId);
                frm1.Show();
            };
           
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //根据选出的Start_Date时间范围，筛选出Part_Number
            DateTime startTime = dateTimePicker1.Value;
            DateTime endTime = dateTimePicker2.Value;
            string sql = string.Format("select Part_Number from dbo.Inquiry_Base where Start_Date between '{0}' and  '{1}'", startTime, endTime);
            //DBHelper.
            SqlConnection conn1 = new SqlConnection(connString);
            conn1.Open();
            SqlDataAdapter sda1 = new SqlDataAdapter(sql, conn1);
            DataTable dt = new DataTable();
            sda1.Fill(dt);
            conn1.Close();

            if (dt.Rows.Count == 0)
            {
                comboBox1.Items.Clear();
                MessageBox.Show("该时间段没有询价");
                dataGridView1.DataSource = null;
                //listBox1.Items.Clear();
                comboBox1.Text = "";
                //label4.Text = "";
                //label5.Text = "";
               // materialID = "";
            }
            else
            {
                comboBox1.Items.Clear();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    comboBox1.Items.Add(dt.Rows[i][0].ToString());
                }
                MessageBox.Show(string.Format("存在{0}条记录", dt.Rows.Count.ToString()));
            }
        }
    }
}