﻿namespace MMClient.SourcingManage.SourcingManagement
{
    partial class _20180408form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
            this.cgdh_cmb = new System.Windows.Forms.ComboBox();
            this.Inquiry_view = new System.Windows.Forms.DataGridView();
            this.materialId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.materialName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.materialGroup = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.count = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.measurement = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.factoryId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.stockId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DeliveryStartTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DeliveryEndTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btn_addSupplier = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tab_BaseInfo = new System.Windows.Forms.TabPage();
            this.cmb_purchaseOrg = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cmb_purchaseGroup = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.dte_OfferTime = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.tab_Material = new System.Windows.Forms.TabPage();
            this.tab_Supplier = new System.Windows.Forms.TabPage();
            this.Supplier_view = new System.Windows.Forms.DataGridView();
            this.supplierId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.supplierName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mobilePhone = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.email = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nation = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.address = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gateway = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.supplierComments = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.button1 = new System.Windows.Forms.Button();
            this.dataGridViewComboBoxColumn1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn2 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn3 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn4 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn5 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn6 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.btn_check = new System.Windows.Forms.Button();
            this.send_button = new System.Windows.Forms.Button();
            this.xjdh_lbl = new System.Windows.Forms.Label();
            this.ckcg_button = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.txt_InquiryName = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txt_state = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txt_inquiryId = new System.Windows.Forms.TextBox();
            this.cgdh_lbl = new System.Windows.Forms.Label();
            this.button6 = new System.Windows.Forms.Button();
            this.ckxj_button = new System.Windows.Forms.Button();
            this.scxj_button = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.Inquiry_view)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tab_BaseInfo.SuspendLayout();
            this.tab_Material.SuspendLayout();
            this.tab_Supplier.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Supplier_view)).BeginInit();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // cgdh_cmb
            // 
            this.cgdh_cmb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.cgdh_cmb.FormattingEnabled = true;
            this.cgdh_cmb.Location = new System.Drawing.Point(437, 20);
            this.cgdh_cmb.Name = "cgdh_cmb";
            this.cgdh_cmb.Size = new System.Drawing.Size(147, 22);
            this.cgdh_cmb.TabIndex = 159;
            // 
            // Inquiry_view
            // 
            this.Inquiry_view.AllowUserToAddRows = false;
            this.Inquiry_view.BackgroundColor = System.Drawing.SystemColors.Control;
            this.Inquiry_view.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Inquiry_view.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Inquiry_view.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.materialId,
            this.materialName,
            this.materialGroup,
            this.count,
            this.measurement,
            this.factoryId,
            this.stockId,
            this.DeliveryStartTime,
            this.DeliveryEndTime});
            this.Inquiry_view.EnableHeadersVisualStyles = false;
            this.Inquiry_view.Location = new System.Drawing.Point(6, 6);
            this.Inquiry_view.Name = "Inquiry_view";
            this.Inquiry_view.RowTemplate.Height = 23;
            this.Inquiry_view.Size = new System.Drawing.Size(975, 230);
            this.Inquiry_view.TabIndex = 194;
            // 
            // materialId
            // 
            this.materialId.HeaderText = "物料编号";
            this.materialId.Name = "materialId";
            this.materialId.Width = 130;
            // 
            // materialName
            // 
            this.materialName.HeaderText = "物料名称";
            this.materialName.Name = "materialName";
            // 
            // materialGroup
            // 
            this.materialGroup.HeaderText = "物料组";
            this.materialGroup.Name = "materialGroup";
            this.materialGroup.Width = 80;
            // 
            // count
            // 
            this.count.HeaderText = "数量";
            this.count.Name = "count";
            this.count.Width = 80;
            // 
            // measurement
            // 
            this.measurement.HeaderText = "单位";
            this.measurement.Name = "measurement";
            this.measurement.Width = 60;
            // 
            // factoryId
            // 
            this.factoryId.HeaderText = "工厂编号";
            this.factoryId.Name = "factoryId";
            this.factoryId.Width = 80;
            // 
            // stockId
            // 
            this.stockId.HeaderText = "仓库";
            this.stockId.Name = "stockId";
            this.stockId.Width = 80;
            // 
            // DeliveryStartTime
            // 
            this.DeliveryStartTime.HeaderText = "交货开始日期";
            this.DeliveryStartTime.Name = "DeliveryStartTime";
            this.DeliveryStartTime.Width = 120;
            // 
            // DeliveryEndTime
            // 
            this.DeliveryEndTime.HeaderText = "交货结束日期";
            this.DeliveryEndTime.Name = "DeliveryEndTime";
            this.DeliveryEndTime.Width = 120;
            // 
            // btn_addSupplier
            // 
            this.btn_addSupplier.Location = new System.Drawing.Point(127, 267);
            this.btn_addSupplier.Name = "btn_addSupplier";
            this.btn_addSupplier.Size = new System.Drawing.Size(102, 34);
            this.btn_addSupplier.TabIndex = 195;
            this.btn_addSupplier.Text = "批量添加供应商";
            this.btn_addSupplier.UseVisualStyleBackColor = true;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tab_BaseInfo);
            this.tabControl1.Controls.Add(this.tab_Material);
            this.tabControl1.Controls.Add(this.tab_Supplier);
            this.tabControl1.Location = new System.Drawing.Point(14, 200);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1002, 309);
            this.tabControl1.TabIndex = 206;
            // 
            // tab_BaseInfo
            // 
            this.tab_BaseInfo.BackColor = System.Drawing.SystemColors.Control;
            this.tab_BaseInfo.Controls.Add(this.cmb_purchaseOrg);
            this.tab_BaseInfo.Controls.Add(this.label1);
            this.tab_BaseInfo.Controls.Add(this.cmb_purchaseGroup);
            this.tab_BaseInfo.Controls.Add(this.label2);
            this.tab_BaseInfo.Controls.Add(this.dte_OfferTime);
            this.tab_BaseInfo.Controls.Add(this.label4);
            this.tab_BaseInfo.Location = new System.Drawing.Point(4, 22);
            this.tab_BaseInfo.Name = "tab_BaseInfo";
            this.tab_BaseInfo.Padding = new System.Windows.Forms.Padding(3);
            this.tab_BaseInfo.Size = new System.Drawing.Size(994, 283);
            this.tab_BaseInfo.TabIndex = 0;
            this.tab_BaseInfo.Text = "基本信息";
            // 
            // cmb_purchaseOrg
            // 
            this.cmb_purchaseOrg.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.cmb_purchaseOrg.FormattingEnabled = true;
            this.cmb_purchaseOrg.Location = new System.Drawing.Point(197, 56);
            this.cmb_purchaseOrg.Name = "cmb_purchaseOrg";
            this.cmb_purchaseOrg.Size = new System.Drawing.Size(100, 22);
            this.cmb_purchaseOrg.TabIndex = 170;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("宋体", 9F);
            this.label1.Location = new System.Drawing.Point(138, 60);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 169;
            this.label1.Text = "采购组织";
            // 
            // cmb_purchaseGroup
            // 
            this.cmb_purchaseGroup.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.cmb_purchaseGroup.FormattingEnabled = true;
            this.cmb_purchaseGroup.Location = new System.Drawing.Point(197, 96);
            this.cmb_purchaseGroup.Name = "cmb_purchaseGroup";
            this.cmb_purchaseGroup.Size = new System.Drawing.Size(100, 22);
            this.cmb_purchaseGroup.TabIndex = 172;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("宋体", 9F);
            this.label2.Location = new System.Drawing.Point(138, 102);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 12);
            this.label2.TabIndex = 171;
            this.label2.Text = "采购组";
            // 
            // dte_OfferTime
            // 
            this.dte_OfferTime.CustomFormat = "yyyy-MM-dd HH:mm";
            this.dte_OfferTime.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.dte_OfferTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dte_OfferTime.Location = new System.Drawing.Point(536, 60);
            this.dte_OfferTime.Name = "dte_OfferTime";
            this.dte_OfferTime.Size = new System.Drawing.Size(155, 23);
            this.dte_OfferTime.TabIndex = 174;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("宋体", 9F);
            this.label4.Location = new System.Drawing.Point(476, 64);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 12);
            this.label4.TabIndex = 173;
            this.label4.Text = "报价期限";
            // 
            // tab_Material
            // 
            this.tab_Material.BackColor = System.Drawing.SystemColors.Control;
            this.tab_Material.Controls.Add(this.Inquiry_view);
            this.tab_Material.Location = new System.Drawing.Point(4, 22);
            this.tab_Material.Name = "tab_Material";
            this.tab_Material.Padding = new System.Windows.Forms.Padding(3);
            this.tab_Material.Size = new System.Drawing.Size(994, 283);
            this.tab_Material.TabIndex = 1;
            this.tab_Material.Text = "物料信息";
            // 
            // tab_Supplier
            // 
            this.tab_Supplier.BackColor = System.Drawing.SystemColors.Control;
            this.tab_Supplier.Controls.Add(this.Supplier_view);
            this.tab_Supplier.Controls.Add(this.btn_addSupplier);
            this.tab_Supplier.Location = new System.Drawing.Point(4, 22);
            this.tab_Supplier.Name = "tab_Supplier";
            this.tab_Supplier.Size = new System.Drawing.Size(994, 283);
            this.tab_Supplier.TabIndex = 2;
            this.tab_Supplier.Text = "供应商";
            // 
            // Supplier_view
            // 
            this.Supplier_view.AllowUserToAddRows = false;
            this.Supplier_view.BackgroundColor = System.Drawing.SystemColors.Control;
            this.Supplier_view.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Supplier_view.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Supplier_view.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.supplierId,
            this.supplierName,
            this.mobilePhone,
            this.email,
            this.nation,
            this.address,
            this.gateway,
            this.supplierComments});
            this.Supplier_view.EnableHeadersVisualStyles = false;
            this.Supplier_view.Location = new System.Drawing.Point(3, 3);
            this.Supplier_view.Name = "Supplier_view";
            this.Supplier_view.RowTemplate.Height = 23;
            this.Supplier_view.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.Supplier_view.Size = new System.Drawing.Size(919, 207);
            this.Supplier_view.TabIndex = 175;
            // 
            // supplierId
            // 
            dataGridViewCellStyle17.BackColor = System.Drawing.Color.White;
            this.supplierId.DefaultCellStyle = dataGridViewCellStyle17;
            this.supplierId.HeaderText = "供应商编号";
            this.supplierId.Name = "supplierId";
            this.supplierId.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.supplierId.Width = 90;
            // 
            // supplierName
            // 
            dataGridViewCellStyle18.BackColor = System.Drawing.Color.White;
            this.supplierName.DefaultCellStyle = dataGridViewCellStyle18;
            this.supplierName.HeaderText = "供应商名称";
            this.supplierName.Name = "supplierName";
            this.supplierName.ReadOnly = true;
            this.supplierName.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.supplierName.Width = 150;
            // 
            // mobilePhone
            // 
            this.mobilePhone.HeaderText = "联系电话";
            this.mobilePhone.Name = "mobilePhone";
            this.mobilePhone.ReadOnly = true;
            // 
            // email
            // 
            this.email.HeaderText = "电子信箱";
            this.email.Name = "email";
            this.email.ReadOnly = true;
            this.email.Width = 120;
            // 
            // nation
            // 
            this.nation.HeaderText = "国家";
            this.nation.Name = "nation";
            this.nation.ReadOnly = true;
            this.nation.Width = 80;
            // 
            // address
            // 
            this.address.HeaderText = "地址";
            this.address.Name = "address";
            this.address.ReadOnly = true;
            this.address.Width = 150;
            // 
            // gateway
            // 
            this.gateway.HeaderText = "门户";
            this.gateway.Name = "gateway";
            this.gateway.ReadOnly = true;
            this.gateway.Width = 120;
            // 
            // supplierComments
            // 
            this.supplierComments.HeaderText = "备注";
            this.supplierComments.Name = "supplierComments";
            this.supplierComments.Width = 60;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(387, 14);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(58, 28);
            this.button1.TabIndex = 205;
            this.button1.Text = "保存";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            // 
            // dataGridViewComboBoxColumn1
            // 
            dataGridViewCellStyle19.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle19.SelectionBackColor = System.Drawing.Color.White;
            this.dataGridViewComboBoxColumn1.DefaultCellStyle = dataGridViewCellStyle19;
            this.dataGridViewComboBoxColumn1.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn1.Frozen = true;
            this.dataGridViewComboBoxColumn1.HeaderText = "供应商编号";
            this.dataGridViewComboBoxColumn1.Name = "dataGridViewComboBoxColumn1";
            this.dataGridViewComboBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewComboBoxColumn1.Width = 80;
            // 
            // dataGridViewComboBoxColumn2
            // 
            dataGridViewCellStyle20.BackColor = System.Drawing.Color.White;
            this.dataGridViewComboBoxColumn2.DefaultCellStyle = dataGridViewCellStyle20;
            this.dataGridViewComboBoxColumn2.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn2.Frozen = true;
            this.dataGridViewComboBoxColumn2.HeaderText = "供应商名称";
            this.dataGridViewComboBoxColumn2.Name = "dataGridViewComboBoxColumn2";
            this.dataGridViewComboBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewComboBoxColumn2.Width = 160;
            // 
            // dataGridViewComboBoxColumn3
            // 
            dataGridViewCellStyle21.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle21.SelectionBackColor = System.Drawing.Color.White;
            this.dataGridViewComboBoxColumn3.DefaultCellStyle = dataGridViewCellStyle21;
            this.dataGridViewComboBoxColumn3.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn3.Frozen = true;
            this.dataGridViewComboBoxColumn3.HeaderText = "物料编号";
            this.dataGridViewComboBoxColumn3.Name = "dataGridViewComboBoxColumn3";
            this.dataGridViewComboBoxColumn3.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewComboBoxColumn3.Width = 140;
            // 
            // dataGridViewComboBoxColumn4
            // 
            dataGridViewCellStyle22.BackColor = System.Drawing.Color.White;
            this.dataGridViewComboBoxColumn4.DefaultCellStyle = dataGridViewCellStyle22;
            this.dataGridViewComboBoxColumn4.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn4.Frozen = true;
            this.dataGridViewComboBoxColumn4.HeaderText = "物料名称";
            this.dataGridViewComboBoxColumn4.Name = "dataGridViewComboBoxColumn4";
            this.dataGridViewComboBoxColumn4.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewComboBoxColumn4.Width = 140;
            // 
            // dataGridViewComboBoxColumn5
            // 
            dataGridViewCellStyle23.BackColor = System.Drawing.Color.White;
            this.dataGridViewComboBoxColumn5.DefaultCellStyle = dataGridViewCellStyle23;
            this.dataGridViewComboBoxColumn5.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn5.Frozen = true;
            this.dataGridViewComboBoxColumn5.HeaderText = "工厂";
            this.dataGridViewComboBoxColumn5.Name = "dataGridViewComboBoxColumn5";
            this.dataGridViewComboBoxColumn5.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewComboBoxColumn5.Width = 80;
            // 
            // dataGridViewComboBoxColumn6
            // 
            dataGridViewCellStyle24.BackColor = System.Drawing.Color.White;
            this.dataGridViewComboBoxColumn6.DefaultCellStyle = dataGridViewCellStyle24;
            this.dataGridViewComboBoxColumn6.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn6.Frozen = true;
            this.dataGridViewComboBoxColumn6.HeaderText = "仓库";
            this.dataGridViewComboBoxColumn6.Name = "dataGridViewComboBoxColumn6";
            this.dataGridViewComboBoxColumn6.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewComboBoxColumn6.Width = 80;
            // 
            // btn_check
            // 
            this.btn_check.Location = new System.Drawing.Point(299, 14);
            this.btn_check.Name = "btn_check";
            this.btn_check.Size = new System.Drawing.Size(63, 27);
            this.btn_check.TabIndex = 207;
            this.btn_check.Text = "检查";
            this.btn_check.UseVisualStyleBackColor = true;
            // 
            // send_button
            // 
            this.send_button.ForeColor = System.Drawing.Color.LightSeaGreen;
            this.send_button.Location = new System.Drawing.Point(182, 19);
            this.send_button.Name = "send_button";
            this.send_button.Size = new System.Drawing.Size(63, 23);
            this.send_button.TabIndex = 201;
            this.send_button.Text = "发送邀请";
            this.send_button.UseVisualStyleBackColor = true;
            this.send_button.Visible = false;
            // 
            // xjdh_lbl
            // 
            this.xjdh_lbl.AutoSize = true;
            this.xjdh_lbl.Font = new System.Drawing.Font("宋体", 9F);
            this.xjdh_lbl.Location = new System.Drawing.Point(22, 26);
            this.xjdh_lbl.Name = "xjdh_lbl";
            this.xjdh_lbl.Size = new System.Drawing.Size(53, 12);
            this.xjdh_lbl.TabIndex = 155;
            this.xjdh_lbl.Text = "询价单号";
            // 
            // ckcg_button
            // 
            this.ckcg_button.ForeColor = System.Drawing.Color.LightSeaGreen;
            this.ckcg_button.Location = new System.Drawing.Point(846, 26);
            this.ckcg_button.Name = "ckcg_button";
            this.ckcg_button.Size = new System.Drawing.Size(63, 23);
            this.ckcg_button.TabIndex = 168;
            this.ckcg_button.Text = "参考采购";
            this.ckcg_button.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.txt_InquiryName);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.txt_state);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.txt_inquiryId);
            this.panel2.Controls.Add(this.xjdh_lbl);
            this.panel2.Controls.Add(this.cgdh_lbl);
            this.panel2.Controls.Add(this.cgdh_cmb);
            this.panel2.Controls.Add(this.ckcg_button);
            this.panel2.Location = new System.Drawing.Point(14, 57);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1030, 103);
            this.panel2.TabIndex = 202;
            // 
            // txt_InquiryName
            // 
            this.txt_InquiryName.Location = new System.Drawing.Point(81, 64);
            this.txt_InquiryName.Name = "txt_InquiryName";
            this.txt_InquiryName.Size = new System.Drawing.Size(146, 21);
            this.txt_InquiryName.TabIndex = 183;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(22, 67);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 12);
            this.label6.TabIndex = 182;
            this.label6.Text = "询价名称";
            // 
            // txt_state
            // 
            this.txt_state.Location = new System.Drawing.Point(280, 20);
            this.txt_state.Name = "txt_state";
            this.txt_state.ReadOnly = true;
            this.txt_state.Size = new System.Drawing.Size(63, 21);
            this.txt_state.TabIndex = 181;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(245, 26);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 12);
            this.label3.TabIndex = 180;
            this.label3.Text = "状态";
            // 
            // txt_inquiryId
            // 
            this.txt_inquiryId.Location = new System.Drawing.Point(80, 22);
            this.txt_inquiryId.Name = "txt_inquiryId";
            this.txt_inquiryId.Size = new System.Drawing.Size(147, 21);
            this.txt_inquiryId.TabIndex = 179;
            // 
            // cgdh_lbl
            // 
            this.cgdh_lbl.AutoSize = true;
            this.cgdh_lbl.Font = new System.Drawing.Font("宋体", 9F);
            this.cgdh_lbl.Location = new System.Drawing.Point(378, 25);
            this.cgdh_lbl.Name = "cgdh_lbl";
            this.cgdh_lbl.Size = new System.Drawing.Size(53, 12);
            this.cgdh_lbl.TabIndex = 156;
            this.cgdh_lbl.Text = "采购单号";
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(405, 531);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(75, 23);
            this.button6.TabIndex = 212;
            this.button6.Text = "询价单";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // ckxj_button
            // 
            this.ckxj_button.ForeColor = System.Drawing.Color.LightSeaGreen;
            this.ckxj_button.Location = new System.Drawing.Point(94, 19);
            this.ckxj_button.Name = "ckxj_button";
            this.ckxj_button.Size = new System.Drawing.Size(63, 23);
            this.ckxj_button.TabIndex = 204;
            this.ckxj_button.Text = "查看询价";
            this.ckxj_button.UseVisualStyleBackColor = true;
            // 
            // scxj_button
            // 
            this.scxj_button.ForeColor = System.Drawing.Color.LightSeaGreen;
            this.scxj_button.Location = new System.Drawing.Point(14, 19);
            this.scxj_button.Name = "scxj_button";
            this.scxj_button.Size = new System.Drawing.Size(63, 23);
            this.scxj_button.TabIndex = 203;
            this.scxj_button.Text = "生成询价";
            this.scxj_button.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(18, 531);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 208;
            this.button2.Text = "volvo";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(111, 531);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 209;
            this.button3.Text = "分析结果图";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(203, 531);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 210;
            this.button4.Text = "下载";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(299, 531);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(75, 23);
            this.button5.TabIndex = 211;
            this.button5.Text = "flowchart";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click_1);
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(506, 531);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(75, 23);
            this.button7.TabIndex = 213;
            this.button7.Text = "询价单1";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(611, 531);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(75, 23);
            this.button8.TabIndex = 214;
            this.button8.Text = "收发邮件";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(717, 531);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(75, 23);
            this.button9.TabIndex = 215;
            this.button9.Text = "水晶报表";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(822, 531);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(79, 23);
            this.button10.TabIndex = 216;
            this.button10.Text = "供应商报价";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // button11
            // 
            this.button11.Location = new System.Drawing.Point(928, 531);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(75, 23);
            this.button11.TabIndex = 217;
            this.button11.Text = "询价单查询";
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // _20180408form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1102, 613);
            this.Controls.Add(this.button11);
            this.Controls.Add(this.button10);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btn_check);
            this.Controls.Add(this.send_button);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.ckxj_button);
            this.Controls.Add(this.scxj_button);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "_20180408form";
            this.Text = "_20180408form";
            ((System.ComponentModel.ISupportInitialize)(this.Inquiry_view)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tab_BaseInfo.ResumeLayout(false);
            this.tab_BaseInfo.PerformLayout();
            this.tab_Material.ResumeLayout(false);
            this.tab_Supplier.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Supplier_view)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox cgdh_cmb;
        private System.Windows.Forms.DataGridView Inquiry_view;
        private System.Windows.Forms.DataGridViewTextBoxColumn materialId;
        private System.Windows.Forms.DataGridViewTextBoxColumn materialName;
        private System.Windows.Forms.DataGridViewTextBoxColumn materialGroup;
        private System.Windows.Forms.DataGridViewTextBoxColumn count;
        private System.Windows.Forms.DataGridViewTextBoxColumn measurement;
        private System.Windows.Forms.DataGridViewTextBoxColumn factoryId;
        private System.Windows.Forms.DataGridViewTextBoxColumn stockId;
        private System.Windows.Forms.DataGridViewTextBoxColumn DeliveryStartTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn DeliveryEndTime;
        private System.Windows.Forms.Button btn_addSupplier;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tab_BaseInfo;
        private System.Windows.Forms.ComboBox cmb_purchaseOrg;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmb_purchaseGroup;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dte_OfferTime;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TabPage tab_Material;
        private System.Windows.Forms.TabPage tab_Supplier;
        private System.Windows.Forms.DataGridView Supplier_view;
        private System.Windows.Forms.DataGridViewTextBoxColumn supplierId;
        private System.Windows.Forms.DataGridViewTextBoxColumn supplierName;
        private System.Windows.Forms.DataGridViewTextBoxColumn mobilePhone;
        private System.Windows.Forms.DataGridViewTextBoxColumn email;
        private System.Windows.Forms.DataGridViewTextBoxColumn nation;
        private System.Windows.Forms.DataGridViewTextBoxColumn address;
        private System.Windows.Forms.DataGridViewTextBoxColumn gateway;
        private System.Windows.Forms.DataGridViewTextBoxColumn supplierComments;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn1;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn2;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn3;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn4;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn5;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn6;
        private System.Windows.Forms.Button btn_check;
        private System.Windows.Forms.Button send_button;
        private System.Windows.Forms.Label xjdh_lbl;
        private System.Windows.Forms.Button ckcg_button;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox txt_InquiryName;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txt_state;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txt_inquiryId;
        private System.Windows.Forms.Label cgdh_lbl;
        private System.Windows.Forms.Button ckxj_button;
        private System.Windows.Forms.Button scxj_button;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button11;
    }
}