﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Lib.Model.SourcingManage.SourceingRecord;
using Lib.Common.CommonUtils;
using Lib.Model.FileManage;
using MMClient.CommonForms;
using Lib.Bll.FileManageBLL;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using Lib.SqlServerDAL;
using System.Data.SqlClient;
using System.Net.Mail;
using Aspose.Cells;
using Lib.SqlServerDAL.SourcingManage.ProcurementPlan;
using Lib.Model.SourcingManage.ProcurementPlan;
using Lib.Model.SourcingManage;
using Lib.Bll.SourcingManage.SourcingManagement;
using Lib.Bll.SourcingManage.SourceingRecord;
using WeifenLuo.WinFormsUI.Docking;
using System.IO;
using Lib.Model.SourcingManage.SourcingManagement;
using System.Net;


namespace MMClient.SourcingManage.SourcingManagement
{
    public partial class VolvoInquiry1_Form : Form
    {
        string fileName1;
        byte[] wordData;
        string checkValue1;
        string checkValue2;
        string checkValue3;
        string checkValue4;
        string checkValue5;
        string checkValue6;
        string deId1;
        string maId1;
        string str2;
        string filename2;
        string connString;

        public VolvoInquiry1_Form(string a, string b, string c)
        {
            InitializeComponent();
            deId1 = a;
            maId1 = b;
            str2 = c;
        }

        private void VolvoInquiry1_Form_Load(object sender, EventArgs e)
        {
            //textBox1.Text = VolvoInquiry0_Form.str2;
            //获取询价号
            connString = ConfigurationManager.AppSettings["serverAddress"];
            textBox1.Text = str2;

            //根据选中的Demand_ID和Material_ID，读取Demand_Material表的一条记录到各个文本框里
            //DataTable db= DBHelper.ExecuteQueryDT("select ID, Demand_ID, Material_ID, Material_Name, Aggregate_ID, Demand_Count, Measurement, Stock_ID, Factory_ID, DeliveryStartTime, DeliveryEndTime from dbo.Demand_Material where Demand_ID = '" + demandid + "' and Material_ID = '" + materialid + "'");
            SqlConnection conn1 = new SqlConnection(connString);
            SqlDataAdapter sda1 = new SqlDataAdapter("select ID, Demand_ID, Material_ID, Material_Name, Aggregate_ID, Demand_Count, Measurement, Stock_ID, Factory_ID, DeliveryStartTime, DeliveryEndTime from dbo.Demand_Material where Demand_ID = '" + deId1 + "' and Material_ID = '" + maId1 + "'", conn1);
            DataSet ds1 = new DataSet();
            sda1.Fill(ds1, "em1");
            int b = ds1.Tables[0].Rows.Count;
            if(b > 0)
            {
                textBox13.Text = ds1.Tables[0].Rows[0]["ID"].ToString();
                textBox14.Text = ds1.Tables[0].Rows[0]["Material_Name"].ToString();
                textBox15.Text = ds1.Tables[0].Rows[0]["Measurement"].ToString();
                textBox16.Text = ds1.Tables[0].Rows[0]["DeliveryStartTime"].ToString();
                textBox17.Text = ds1.Tables[0].Rows[0]["Demand_ID"].ToString();
                textBox18.Text = ds1.Tables[0].Rows[0]["Aggregate_ID"].ToString();
                textBox19.Text = ds1.Tables[0].Rows[0]["Factory_ID"].ToString();
                textBox20.Text = ds1.Tables[0].Rows[0]["DeliveryEndTime"].ToString();
                textBox21.Text = ds1.Tables[0].Rows[0]["material_ID"].ToString();
                textBox22.Text = ds1.Tables[0].Rows[0]["Demand_Count"].ToString();
                textBox23.Text = ds1.Tables[0].Rows[0]["Stock_ID"].ToString();
            }
            else
            {
                MessageBox.Show("Demand_ID和Material_ID错误，检查Demand_ID和Material_ID是否为空");
            }
         
            //读取dbo.Company的Company_ID到comboBox1里
            comboBox1.DataSource = DBHelper.ExecuteQueryDT("select Company_ID from dbo.Company");
            comboBox1.DisplayMember = "Company_ID";
            comboBox1.ValueMember = "Company_ID";

            //读取dbo.Buyer_Group的Buyer_Group到comboBox2里
            comboBox2.DataSource = DBHelper.ExecuteQueryDT("select Buyer_Group from dbo.Buyer_Group");
            comboBox2.DisplayMember = "Buyer_Group";
            comboBox2.ValueMember = "Buyer_Group";

            //读取Buyer_Org的Buyer_Org到comboBox3里
            comboBox3.DataSource = DBHelper.ExecuteQueryDT("select Buyer_Org from dbo.Buyer_Org");
            comboBox3.DisplayMember = "Buyer_Org";
            comboBox3.ValueMember = "Buyer_Org";

            //发布日期
            dateTimePicker1.Format = DateTimePickerFormat.Custom;
            dateTimePicker1.CustomFormat = "yyyy-MM-dd";
            textBox2.Text = dateTimePicker1.Text;
            //textBox2.Text = dateTimePicker1.Value.ToString();
            //截至日期
            dateTimePicker2.Format = DateTimePickerFormat.Custom;
            dateTimePicker2.CustomFormat = "yyyy-MM-dd";
            textBox6.Text = dateTimePicker2.Text;
            //textBox6.Text = dateTimePicker2.Value.ToString();

        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.CheckState == CheckState.Checked)
            {
                checkValue1 = "1";
            }
            else
            {
                checkValue1 = "0";
            }
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox2.CheckState == CheckState.Checked)
            {
                checkValue2 = "1";
            }
            else
            {
                checkValue2 = "0";
            }
        }

        private void checkBox3_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox3.CheckState == CheckState.Checked)
            {
                checkValue3 = "1";
            }
            else
            {
                checkValue3 = "0";
            }
        }

        private void checkBox4_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox4.CheckState == CheckState.Checked)
            {
                checkValue4 = "1";
            }
            else
            {
                checkValue4 = "0";
            }
        }

        private void checkBox5_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox5.CheckState == CheckState.Checked)
            {
                checkValue5 = "1";
            }
            else
            {
                checkValue5 = "0";
            }
        }

        private void checkBox6_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox6.CheckState == CheckState.Checked)
            {
                checkValue6 = "1";
            }
            else
            {
                checkValue6 = "0";
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //string name = tb1.Text;
            //接收上传文件
            //Stream fileStream = FileUpload1.PostedFile.InputStream;
            //获取上传文件字节的大小
            //int fileLength = FileUpload1.PostedFile.ContentLength;

            //浏览获得文件路径和文件名到textBox27文本框里
            OpenFileDialog fileDialog = new OpenFileDialog();
            fileDialog.Title = "请选择文件";
            var result = fileDialog.ShowDialog();
            if (result == DialogResult.Cancel)
                return;
            fileName1 = fileDialog.FileName;
            textBox27.Text = fileName1;

            filename2 = System.IO.Path.GetFileName(fileName1);
            //得到文件名
            //string fileName1 = Path.GetFileName(fullFileName);

            //FileInfo fi = new FileInfo(fileName1);  
            //FileStream fs = fi.OpenRead();
            //wordData = new byte[fs.Length];
            //从流中读取字节并写入wordData
            //fs.Read(wordData, 0, Convert.ToInt32(fs.Length));

            //连接数据库
            //SqlConnection SqlConn6 = new SqlConnection("server=116.196.99.18;database=MMBackup;uid=sa;pwd=b602!@#qwe");
            //SqlConn6.Open();
            //string str3 = "insert into dbo.Inquiry_Base(Part_Number) values (@file)";
            //SqlCommand SqlCmd3 = new SqlCommand(str3, SqlConn6);
            //添加word文件
            //SqlParameter contentParam = new SqlParameter("@file", System.Data.SqlDbType.Image);
            //contentParam.Value = wordData;
            //SqlCmd1.Parameters.Add(contentParam);
            //SqlCmd3.ExecuteNonQuery();
            //SqlConn6.Close();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            
            
            string row1 = textBox1.Text;

            //获取textBox7的采购申请号的值
            string row2 = textBox17.Text;

            //获取textBox21的物料号的值
            string row3 = textBox21.Text;

            //获取textBox14的物料名的值
            string row4 = textBox14.Text;

            //获取textBox22的年需求量的值
            string row5 = textBox22.Text;

            //获取textBox15的单位的值
            string row6 = textBox15.Text;

            //获取textBox19的工厂代码的值
            string row7 = textBox19.Text;

            //获取textBox23的仓库地点的值
            string row8 = textBox23.Text;

            //获取textBox16的交货开始日期的值
            string row9 = textBox16.Text;

            //获取textBox20的交货结束日期的值
            string row10 = textBox20.Text;

            //获取textBox11的采购组织的值
            string row11 = comboBox3.Text;

            //获取textBox16的采购组的值
            string row12 = comboBox2.Text;

            if(row1!="" && row2!= "" && row3!= "" && row4!= "" && row5!= "" && row6!= "" && row7!= "" && row8!= "" && row9!= "" && row10!= "" && row11!= "" && row12!= "" )
            {
                
                dataGridView1.Rows.Clear();
                dataGridView1.Rows.Add(row1, row2, row3, row4, row5, row6, row7, row8, row9, row10, row11, row12);
                MessageBox.Show("加载完成");
            }
            else
            {
                MessageBox.Show("内容不完整");
            }   
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //把填写的信息保存到数据库的dbo.Inquiry_Base表

            //获取Valid_State的值
            string row13 = "1";

            //获取FileName的值
            //string row14 = filename2;

            int j = 0;
            //添加word文件
            //FileInfo fi = new FileInfo(fileName1);
            //string path = dataGridView3.Rows[j].Cells[21].Value.ToString();
            
            //异常判断
            try
            {
                string path = textBox27.Text;
                //判断是否上传文件
                if (dataGridView1.Rows.Count>1)
                {
                    
                    //判断文件是否超过50M
                    if (path!="" )
                    {
                        FileInfo fi = new FileInfo(path);
                        FileStream fs = fi.OpenRead();
                        if(fs.Length >= 52428800)
                        {
                            MessageBox.Show("附件必须小于50M");
                            return;
                        }
                        wordData = new byte[fs.Length];
                        //从流中读取字节并写入wordData
                        fs.Read(wordData, 0, Convert.ToInt32(fs.Length));

                        //把填完整的内容保存到dbo.Inquiry_Base表           
                        SqlConnection SqlConn4 = new SqlConnection(connString);
                        SqlConn4.Open();

                        string str1 = "insert into dbo.Inquiry_Base(Part_Number, Start_Date, Expire_Date, Valid_State, Draw_No_Issue, Material_ID, Material_Name, Yearly_Volume, Payment_Terms, " +
                        "Delivery_Conditions, Point_Of_Delivery, Parma_Code, Source_Country, Local_Currency, Buyer, Material_Process_Breakdown, Tooling_Breakdown, Flowchart, Description, " +
                        "Logistics, Misc_Information, Attachment, FileName, Misc_Information_text, Tooling_and_Utilization, Project_Type, Demand_Material_ID, CompanyID, Buyer_Group, Buyer_org) values('" + textBox1.Text + "','" + textBox2.Text + "','" + textBox6.Text + "','" + row13 + "','" +
                         textBox3.Text + "','" + textBox21.Text + "','" + textBox14.Text + "','" + textBox22.Text + "','" + textBox4.Text + "','" + textBox7.Text + "','" + textBox8.Text + "','" + textBox12.Text + "','" +
                         textBox9.Text + "','" + textBox10.Text + "','" + textBox24.Text + "','" + checkValue1 + "','" + checkValue5 + "','" + checkValue2 + "','" + textBox26.Text + "','" + checkValue4 + "','" +
                         checkValue6 + "',@file,'" + filename2 + "','" + textBox25.Text + "','" + checkValue3 + "','" + comboBox4.Text + "','" + textBox13.Text + "','" + comboBox1.Text + "','" + comboBox2.Text + "','" + comboBox3.Text + "')";/*注意中间的单双引号*/
                        SqlCommand SqlCmd1 = new SqlCommand(str1, SqlConn4);

                        SqlParameter contentParam = new SqlParameter("@file", System.Data.SqlDbType.Image);
                        contentParam.Value = wordData;
                        SqlCmd1.Parameters.Add(contentParam);
                        SqlCmd1.ExecuteNonQuery();
                        SqlConn4.Close();
                        MessageBox.Show("数据保存成功");
                        this.button2.Enabled=false;

                    }
                    else
                    {
                        
                        SqlConnection SqlConn4 = new SqlConnection(connString);
                        SqlConn4.Open();

                        string str1 = "insert into dbo.Inquiry_Base(Part_Number, Start_Date, Expire_Date, Valid_State, Draw_No_Issue, Material_ID, Material_Name, Yearly_Volume, Payment_Terms, " +
                        "Delivery_Conditions, Point_Of_Delivery, Parma_Code, Source_Country, Local_Currency, Buyer, Material_Process_Breakdown, Tooling_Breakdown, Flowchart, Description, " +
                        "Logistics, Misc_Information, FileName, Misc_Information_text, Tooling_and_Utilization, Project_Type, Demand_Material_ID, CompanyID, Buyer_Group, Buyer_org) values('" + textBox1.Text + "','" + textBox2.Text + "','" + textBox6.Text + "','" + row13 + "','" +
                         textBox3.Text + "','" + textBox21.Text + "','" + textBox14.Text + "','" + textBox22.Text + "','" + textBox4.Text + "','" + textBox7.Text + "','" + textBox8.Text + "','" + textBox12.Text + "','" +
                         textBox9.Text + "','" + textBox10.Text + "','" + textBox24.Text + "','" + checkValue1 + "','" + checkValue5 + "','" + checkValue2 + "','" + textBox26.Text + "','" + checkValue4 + "','" +
                         checkValue6 + "','" + filename2 + "','" + textBox25.Text + "','" + checkValue3 + "','" + comboBox4.Text + "','" + textBox13.Text + "','" + comboBox1.Text + "','" + comboBox2.Text + "','" + comboBox3.Text + "')";/*注意中间的单双引号*/
                        SqlCommand SqlCmd1 = new SqlCommand(str1, SqlConn4);
                                            
                        SqlCmd1.ExecuteNonQuery();
                        SqlConn4.Close();
                        MessageBox.Show("数据保存成功");
                        this.button2.Enabled=false;
                    }
                }
                else
                {
                    MessageBox.Show("请先提交候选项");
                }                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }                      
        }

        private void button3_Click(object sender, EventArgs e)
        {
            //删除dataGridView1中选中的内容
            //foreach (DataGridViewRow r in dataGridView1.SelectedRows)
            //{
            //    if (!r.IsNewRow)
            //    {
            //        dataGridView1.Rows.Remove(r);
            //        //dataGridView1.Rows.Clear();
            //        MessageBox.Show("数据删除成功");
            //    }
            //}      
            dataGridView1.Rows.Clear();

        }

        private void button4_Click(object sender, EventArgs e)
        {
            //弹出分配供应商页面
            VolvoInquiry2_Form frm7 = new VolvoInquiry2_Form();
            frm7.Show();
        }
    }
}
