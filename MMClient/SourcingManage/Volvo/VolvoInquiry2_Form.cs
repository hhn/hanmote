﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Net.Mail;
using Aspose.Cells;
using Lib.SqlServerDAL.SourcingManage.ProcurementPlan;
using Lib.Model.SourcingManage.ProcurementPlan;
using Lib.Model.SourcingManage;
using Lib.Bll.SourcingManage.SourcingManagement;
using Lib.Bll.SourcingManage.SourceingRecord;
using WeifenLuo.WinFormsUI.Docking;
using System.IO;
using Lib.Model.SourcingManage.SourcingManagement;
using Lib.Model.SourcingManage.SourceingRecord;
using Lib.Common.CommonUtils;
using Lib.Model.FileManage;
using MMClient.CommonForms;
using Lib.Bll.FileManageBLL;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Collections;
using Lib.SqlServerDAL;


namespace MMClient.SourcingManage.SourcingManagement
{
    public partial class VolvoInquiry2_Form : DockContent
    {
        string val;

        public VolvoInquiry2_Form()
        {
            InitializeComponent();
        }

        private void VolvoInquiry2_Form_Load(object sender, EventArgs e)
        {


            //dataGridView1.DataSource = DBHelper.ExecuteQueryDT("select Part_Number as 询价号, Start_Date as 发布日期, Expire_Date as 截止日期, Valid_State as 有效状态, Draw_No_Issue as 图纸编号, Material_ID as 物料号, Material_Name as 物料名, Yearly_Volume as 年化量, Payment_Terms as 付款项, " +
            //"Delivery_Conditions as 发送条件, Point_Of_Delivery as 发货点, Parma_Code as 工厂代码, Source_Country as 源国家, Local_Currency as 当地货币, Buyer as 购买单位, Material_Process_Breakdown as 材料与过程分解, Tooling_Breakdown as 工具化分解, Flowchart as 流程图, Description as 描述, " +
            //"Logistics as 后勤, Misc_Information as 其它资料, FileName as 文件名, Misc_Information_text as 其它信息, Tooling_and_Utilization as 工具化与功能, Project_Type as 项目类别,Demand_Material_ID as 需求物料号, CompanyID as 公司代码, Buyer_Group as 采购组, Buyer_org as 采购组织 from dbo.Inquiry_Base");


            dataGridView1.DataSource = DBHelper.ExecuteQueryDT("select Part_Number as 询价号, Start_Date as 发布日期, Expire_Date as 截止日期,  Material_ID as 物料号, Material_Name as 物料名, Yearly_Volume as 年化量, " +
            " Description as 描述, " +
            "  FileName as 文件名, Project_Type as 项目类别,Buyer_Group as 采购组, Buyer_org as 采购组织 from dbo.Inquiry_Base where Valid_State=1");


        }

        private void dataGridView3_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (this.dataGridView3.SelectionMode != DataGridViewSelectionMode.FullColumnSelect)
            {
                //选中供应商ID表中的Supplier_ID到textBox2
                int index = dataGridView3.CurrentRow.Index; //获取选中行的行号
                //textBox2.Text = dataGridView3.Rows[index].Cells[0].Value.ToString()+","+dataGridView3.Rows[index].Cells[21].Value.ToString();
                //textBox2.Text = dataGridView3.Rows[index].Cells[0].Value.ToString();
            }
        }

        //private void button3_Click(object sender, EventArgs e)
        //{
        //    //添加textBox2中的内容到listBox2
        //    string supplierText = this.textBox2.Text.Trim().ToString();

        //    //string supplierText1 = supplierText.Split(',')[1];
        //    //ArrayList List = new ArrayList(supplierText1);
        //    ListBox list2 = this.listBox2;
        //    if(listBox2.Items.Count==0)
        //    {
        //        list2.Items.Add(supplierText);

        //    }
        //    else
        //    {
        //        bool b=false;
        //        foreach (var str in listBox2.Items.Cast<String>())
        //        {
        //            //b = str.Contains(","+ supplierText1);
        //            b = str.Contains(supplierText);
        //        }
        //        if (b)
        //        {
        //            MessageBox.Show("同一公司的供应商ID已经添加过，无法重复添加！");
        //        }
        //        else
        //        {
        //            list2.Items.Add(supplierText);
        //        }
        //    }
            
        //}       

        //private void button4_Click(object sender, EventArgs e)
        //{
        //    //删除listBox2中选中的内容
        //    if (listBox2.SelectedItems.Count == 0)
        //    {
        //        MessageBox.Show("请选择要删除的项目");
        //    }
        //    else
        //    {
        //        listBox2.Items.Remove(listBox2.SelectedItem);
        //    }
        //}

        //private void button6_Click(object sender, EventArgs e)
        //{
        //    //把textBox3中的内容和listBox2中选中的多条内容保存到dbo.Inquiry_Sheets表
        //    int i;
        //    SqlConnection SqlConn5 = new SqlConnection("server=116.196.99.18;database=MMBackup;uid=sa;pwd=b602!@#qwe");
        //    SqlConn5.Open();
        //    string ss = "select * from dbo.Inquiry_Sheets where Part_Number='" + textBox3.Text.ToString() + "'";
        //    SqlCommand SqlCmd2 = new SqlCommand(ss, SqlConn5);
        //    int count = Convert.ToInt32(SqlCmd2.ExecuteScalar());
        //    if (count > 1) 
        //    {
        //        MessageBox.Show("数据重复！");
        //    }
        //    else
        //    {
        //        i = listBox2.Items.Count;
        //        for (int j = 0; j < i; j++)
        //        {                 
        //            //string str2 = "insert into dbo.Inquiry_Sheets(Part_Number,Supplier_ID,Supplier_Org_ID) values('" + textBox3.Text + "','" + (string)listBox2.Items[j].ToString().Split(',')[0] + "','" + (string)listBox2.Items[j].ToString().Split(',')[1] + "')";
        //            string str2 = "insert into dbo.Inquiry_Sheets(Part_Number,Supplier_ID) values('" + textBox3.Text.ToString() + "','" + (string)listBox2.Items[j].ToString() + "')";
        //            SqlCommand SqlCmd3 = new SqlCommand(str2, SqlConn5);
        //            SqlCmd3.ExecuteNonQuery();
        //        }
        //        MessageBox.Show("数据保存成功");
        //    }
        //    SqlConn5.Close();           
        //}

        

        //private void textBox3_TextChanged(object sender, EventArgs e)
        //{
        //    //SqlConnection SqlConn6 = new SqlConnection("server=116.196.99.18;database=MMBackup;uid=sa;pwd=b602!@#qwe");
        //    //string myAdapter = "select * from dbo.Inquiry_Sheets";
        //    //SqlCommand SqlCmd2 = new SqlCommand(myAdapter, SqlConn6);
        //    //SqlConn6.Open();
        //    //SqlDataReader dr = SqlCmd2.ExecuteReader();
        //    //listBox1.Items.Clear();
        //    //while (dr.Read())
        //    //{
        //    //    //string s = textBox3.Text;
        //    //    string t = dr["Supplier_ID"].ToString();
        //    //    listBox1.Items.Add(t);
        //    //}
        //    //SqlConn6.Close();
        //    //SqlConn6.Dispose();

        //    //输入已有的询价号，listBox1动态显示dbo.Inquiry_Sheets里和询价号关联的Supplier_ID
        //    listBox1.Items.Clear();
        //    SqlConnection con = new SqlConnection("server=116.196.99.18;database=MMBackup;uid=sa;pwd=b602!@#qwe");
        //    SqlDataAdapter sda = new SqlDataAdapter("select * from dbo.Inquiry_Sheets where Part_Number='" + textBox3.Text.ToString() + "'", con);
        //    DataSet ds = new DataSet();
        //    sda.Fill(ds);
        //    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
        //    {
        //        val = ds.Tables[0].Rows[i]["Supplier_ID"].ToString();
        //        listBox1.Items.Add(val);
        //    }
        //    con.Close();
        //    con.Dispose();
        //}

        private void button1_Click(object sender, EventArgs e)
        {
            int index = dataGridView1.CurrentRow.Index; //获取选中行的行号
            if (index >=0)
            {
                //选中dbo.Inquiry_Base的part number到textBox3
                
                textBox3.Text = dataGridView1.Rows[index].Cells[0].Value.ToString();

                //根据采购组织purOrgId和物料号mtId筛选出供应商到供应商ID表
                DataTable dt=DBHelper.ExecuteQueryDT("select SupplierId as 供应商编号, SupplierLevel as 供应商等级, levelDescription as 等级描述, supplierType as 供应商类型 from dbo.Tab_supplierListAndSupplier_Base where SupplierListId = " +
                "(select SupplierListid from dbo.Tab_SupplierListInfo where purOrgId=(select Buyer_org from dbo.Inquiry_Base where Part_Number= '" + textBox3.Text.ToString() + "') and mtId= (select Material_ID from dbo.Inquiry_Base where Part_Number= '" + textBox3.Text.ToString() + "') )");
                if(dt.Rows.Count>0)
                {
                    DataTable dt1 = DBHelper.ExecuteQueryDT("select Supplier_ID from dbo.Inquiry_Sheets where Part_Number= '" + textBox3.Text.ToString() +"'");
                    listBox2.Items.Clear();
                    listBox1.Items.Clear();
                    for (int i=0;i<dt1.Rows.Count;i++)
                    {
                        listBox1.Items.Add(dt1.Rows[i][0].ToString());
                    }

                    dataGridView3.DataSource = dt;
                    
                }
                else
                {
                    dataGridView3.DataSource = null;
                    listBox2.Items.Clear();
                    listBox1.Items.Clear();
                    MessageBox.Show("没有相应供应商");
                }
                
            }
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            if(dataGridView3.Rows.Count>0)
            {
            int index = dataGridView3.CurrentRow.Index; //获取选中行的行号
            if(index>=0)
            {
                //将选中的
                string supplierIDSelected= dataGridView3.Rows[index].Cells[0].Value.ToString();
                if(!listBox2.Items.Contains(supplierIDSelected) && !listBox1.Items.Contains(supplierIDSelected))
                {
                    listBox2.Items.Add(supplierIDSelected);
                }else
                {
                    MessageBox.Show("重复选择");
                }


            }
            else
            {
                MessageBox.Show("请先选择供应商");
            }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if(listBox2.SelectedItem !=null)
            {
                listBox2.Items.Remove(listBox2.SelectedItem);
            }else
            {
                MessageBox.Show("请先选择供应商");
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            if(listBox2.Items.Count>0)
            {
                try
                {
                    
                        for(int i=0;i< listBox2.Items.Count;i++)
                        {
                            string str1 = "insert into dbo.Inquiry_Sheets(Part_Number, Supplier_ID) values('" + textBox3.Text + "','" + listBox2.Items[i].ToString() + "')";/*注意中间的单双引号*/
                            DBHelper.ExecuteNonQuery(str1);
                        }
                        MessageBox.Show("保存成功");
                        listBox1.Items.Clear();
                        listBox2.Items.Clear();
                
                        
                   

                }catch(Exception ex)
                {
                    MessageBox.Show(ex.ToString());

                }

              
            }else
            {
                MessageBox.Show("没有可以添加的项");
            }
        }

        //private void button1_Click(object sender, EventArgs e)
        //{
        //    //根据物料和物料组筛选供应商
        //    dataGridView2.DataSource = DBHelper.ExecuteQueryDT("select SupplierId as 供应商号 from dbo.SR_Info where SelectedMType = (select Description from dbo.Material_Group where Material_Group = " +
        //    "(select Material_Group from dbo.Material where Material_ID = (select Material_ID from dbo.Inquiry_Base where Part_Number='" + textBox3.Text.ToString() + "')))");
        //}
    }
}
