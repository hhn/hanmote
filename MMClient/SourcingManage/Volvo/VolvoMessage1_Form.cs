﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Net.Mail;
using System.IO;
using System.Web;
using System.Configuration;
using WeifenLuo.WinFormsUI.Docking;

namespace MMClient.SourcingManage.SourcingManagement
{
    public partial class VolvoMessage1_Form : DockContent
    {
        string connString;
        public VolvoMessage1_Form()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //点击写邮件按钮，主界面右边出现写邮件界面
            panel1.Controls.Clear();
            VolvoMessageWriteEmail_Form1 sub1 = new VolvoMessageWriteEmail_Form1();
            sub1.TopLevel = false;
            sub1.Dock = DockStyle.Fill;//把子窗体设置为控件
            sub1.FormBorderStyle = FormBorderStyle.None;
            panel1.Controls.Add(sub1);
            sub1.Show();


            //if (this.VolvoMessage2_Form == null || this.VolvoMessage2_Form.IsDisposed)
            //{
            //    this.VolvoMessage2_Form = new VolvoMessage2_Form();
            //}
            //this.VolvoMessage2_Form.TopLevel = false;
            //this.VolvoMessage2_Form.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            ////this.VolvoMessage2_Form.Show(this.dockPnlForm, DockState.Document);
            //panel1.Controls.Add(this.VolvoMessage2_Form);
            //this.VolvoMessage2_Form.Show();
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }
           
        private void button2_Click(object sender, EventArgs e)
        {
            //点击收件箱按钮，主界面右边出现收件箱界面
            panel1.Controls.Clear();
            VolvoMessage3Inbox_Form sub2 = new VolvoMessage3Inbox_Form();
            sub2.TopLevel = false;
            sub2.Dock = DockStyle.Fill;//把子窗体设置为控件
            sub2.FormBorderStyle = FormBorderStyle.None;
            panel1.Controls.Add(sub2);
            sub2.Show();     
        }

        private void button3_Click(object sender, EventArgs e)
        {
            //点击发件箱按钮，主界面右边出现发件箱界面
            panel1.Controls.Clear();
            VolvoMessage4Outbox_Form sub3 = new VolvoMessage4Outbox_Form();
            sub3.TopLevel = false;
            sub3.Dock = DockStyle.Fill;//把子窗体设置为控件
            sub3.FormBorderStyle = FormBorderStyle.None;
            panel1.Controls.Add(sub3);
            sub3.Show();   
        }

        private void VolvoMessage1_Form_Load(object sender, EventArgs e)
        {
            //统计收件箱的记录数
            connString = ConfigurationManager.AppSettings["serverAddress"];
            SqlConnection SqlConn6 = new SqlConnection(connString);
            SqlConn6.Open();
            string sender1 = "1";
            string str4 = "select Count(*) from dbo.Inquiry_Message where Sender ='" + sender1 + "'";
            SqlCommand SqlCmd4 = new SqlCommand(str4, SqlConn6);
            SqlCmd4.CommandType = CommandType.Text;
            int count1 = (int)SqlCmd4.ExecuteScalar();
            SqlConn6.Close();
            button2.Text = "收件箱("+ count1 +")";

            //统计发件箱的记录数
            SqlConnection SqlConn7 = new SqlConnection(connString);
            SqlConn6.Open();
            string sender2 = "0";
            string str5 = "select Count(*) from dbo.Inquiry_Message where Sender ='" + sender2 + "'";
            SqlCommand SqlCmd5 = new SqlCommand(str5, SqlConn6);
            SqlCmd5.CommandType = CommandType.Text;
            int count2 = (int)SqlCmd5.ExecuteScalar();
            SqlConn7.Close();
            button3.Text = "发件箱("+ count2 +")";

        }
    }
}
