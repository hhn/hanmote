﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Net;
using System.Net.Mail;
using System.IO;
using System.Web;
using System.Configuration;

namespace MMClient.SourcingManage.SourcingManagement
{
    public partial class VolvoMessage2_Form : Form
    {
        string fileName1;
        public static string messageidstart1;
        public static string startnumber1;
        public static string replyto1;
        public static string attachmentfilename1;
        public static string partnumber1;
        byte[] wordData;
        public static string @file;

        public VolvoMessage2_Form()
        {
            InitializeComponent();
        }

        public VolvoMessage2_Form(string[] a)
        {
            InitializeComponent();
            string ll = "\r\n\r\n--------------------原始消息--------------------\r\n";

            for (int i = 0; i < 8; i++)
            {
                if (i < 4)
                {
                    //title, supplierid, time, context
                    {
                        string str = a[i];
                        ll += str + "\r\n";
                    }
                    textBox3.Text = ll;
                }
                else if (i == 4)
                {
                    //Message_ID
                    messageidstart1 = a[i];
                }
                else if (i == 5)
                {
                    //单号
                    textBox6.Text = a[i];
                }
                else if (i == 6)
                {
                    //收件人
                    textBox1.Text = a[i];
                }
                else
                {
                    //Start_Number
                    startnumber1 = a[i];
                }
            }
        }

        public static string GetNetDateTime()
        {
            //获取网络日期时间
            WebRequest request = null;
            WebResponse response = null;
            WebHeaderCollection headerCollection = null;
            string datetime = string.Empty;
            try
            {
                request = WebRequest.Create("https://www.baidu.com");
                request.Timeout = 3000;
                request.Credentials = CredentialCache.DefaultCredentials;
                response = (WebResponse)request.GetResponse();
                headerCollection = response.Headers;
                foreach (var h in headerCollection.AllKeys)
                {
                    if (h == "Date")
                    {
                        datetime = headerCollection[h];
                    }
                }
                return datetime;
            }
            catch (Exception)
            {
                return datetime;
            }
            finally
            {
                if (request != null)
                {
                    request.Abort();
                }
                if (response != null)
                {
                    response.Close();
                }
                if (headerCollection != null)
                {
                    headerCollection.Clear();
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //选择文件使文件路径和文件名到textBox4文本框里
            OpenFileDialog fileDialog = new OpenFileDialog();
            fileDialog.Title = "请选择文件";
            var result = fileDialog.ShowDialog();
            if (result == DialogResult.Cancel)
                return;
            fileName1 = fileDialog.FileName;

            label1.Text = fileName1;

            attachmentfilename1 = System.IO.Path.GetFileNameWithoutExtension(fileName1);
        }

        private void button2_Click(object sender, EventArgs e)
        {

            ////判断textBox1是否为空
            //if (textBox1.Text == "")
            //{
            //    MessageBox.Show("收件人不能为空");
            //    return;
            //}

            //判断textBox2是否为空
            if (textBox2.Text == "")
            {
                MessageBox.Show("邮件主题不能为空");
                return;
            }

            ////判断textBox6是否为空
            //if (textBox6.Text == "")
            //{
            //    MessageBox.Show("单号不能为空");
            //    return;
            //}

            int maxlength2 = 1000;
            if (textBox3.Text == "" || textBox3.Text.Length > maxlength2)
            {
                MessageBox.Show("内容为空或内容大小超出范围");
                return;
            }

            //调用GetNetDateTime函数，textBox5获取网络时间
            string dt = GetNetDateTime();
            //Console.WriteLine(Convert.ToDateTime(dt).ToString("yyyy-MM-dd HH:mm:ss"));
            //Console.ReadLine();
            string time1 = Convert.ToDateTime(dt).ToString("yyyy-MM-dd HH:mm:ss");

            if (string.IsNullOrEmpty(fileName1))
            {
                SqlConnection SqlConn4 = new SqlConnection("server=116.196.99.18;database=MMBackup;uid=sa;pwd=b602!@#qwe");
                SqlConn4.Open();

                string userid = "2018050400001";
                string sender1 = "0";

                replyto1 = messageidstart1;

                if (string.IsNullOrEmpty(startnumber1))
                {
                    startnumber1 = replyto1;
                }
                else
                {
                    startnumber1 = startnumber1;
                }

                string str1 = "insert into dbo.Inquiry_Message(Part_Number, User_ID, Title, Supplier_ID, Send_Time, Context, Sender, Reply_To, " +
                "Start_Number) values('" + textBox6.Text + "','" + userid + "','" + textBox2.Text + "','" + textBox1.Text + "','" + time1 + "','" + textBox3.Text + "','" 
                + sender1 + "','" + replyto1 + "','" + startnumber1 + "')";/*注意中间的单双引号*/
                SqlCommand SqlCmd1 = new SqlCommand(str1, SqlConn4);

                //SqlParameter contentParam = new SqlParameter("@file", System.Data.SqlDbType.Image);
                //contentParam.Value = wordData;
                //SqlCmd1.Parameters.Add(contentParam);
                SqlCmd1.ExecuteNonQuery();
                SqlConn4.Close();
                MessageBox.Show("邮件发送成功");
                button2.Enabled = false;
            }
            else
            {
                FileInfo fi = new FileInfo(fileName1);
                FileStream fs = fi.OpenRead();
                wordData = new byte[fs.Length];

                //限制文件大小在20M以内
                int maxlength1 = 20971520;
                if (fs.Length > maxlength1)
                {
                    MessageBox.Show("附件大小不能超过20M");
                    return;
                }
                else
                {
                    //从流中读取字节并写入wordData
                    fs.Read(wordData, 0, Convert.ToInt32(fs.Length));
                }

                SqlConnection SqlConn4 = new SqlConnection("server=116.196.99.18;database=MMBackup;uid=sa;pwd=b602!@#qwe");
                SqlConn4.Open();

                string userid = "2018050400001";
                string sender1 = "0";

                replyto1 = messageidstart1;

                if (string.IsNullOrEmpty(startnumber1))
                {
                    startnumber1 = replyto1;
                }
                else
                {
                    startnumber1 = startnumber1;
                }

                //把textBox1,textBox2,textBox3,textBox4中的内容插入到dbo.Inquiry_Message表
                //string str1 = "insert into dbo.Inquiry_Message(Title, Supplier_ID, Send_Time, Context, Attachment) values('" + textBox2.Text + "','" + textBox1.Text + "','" + textBox5.Text + "','" + textBox3.Text + "',@file)";/*注意中间的单双引号*/
                string str1 = "insert into dbo.Inquiry_Message(Part_Number, User_ID, Title, Supplier_ID, Send_Time, Context, Attachment, AttachmentFileName, Sender, Reply_To, " +
                    "Start_Number) values('" + textBox6.Text + "','" + userid + "','" + textBox2.Text + "','" + textBox1.Text + "','" + time1 + "','" + textBox3.Text + "',@file," +
                    "'" + attachmentfilename1 + "','" + sender1 + "','" + replyto1 + "','" + startnumber1 + "')";/*注意中间的单双引号*/
                SqlCommand SqlCmd1 = new SqlCommand(str1, SqlConn4);

                SqlParameter contentParam = new SqlParameter("@file", System.Data.SqlDbType.Image);
                contentParam.Value = wordData;
                SqlCmd1.Parameters.Add(contentParam);
                SqlCmd1.ExecuteNonQuery();
                SqlConn4.Close();
                MessageBox.Show("邮件发送成功");
                button2.Enabled = false;
            }
        }

        private void VolvoMessage2_Form_Load(object sender, EventArgs e)
        {

        }
    }
}
