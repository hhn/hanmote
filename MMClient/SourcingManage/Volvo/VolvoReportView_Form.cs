﻿using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Net.Mail;
using System.IO;
using System.Web;
using System.Configuration;
using MMClient.SourcingManage.Volvo;
using WeifenLuo.WinFormsUI.Docking;
using Aspose.Cells;
using Lib.SqlServerDAL.SourcingManage.ProcurementPlan;
using Lib.Model.SourcingManage.ProcurementPlan;
using Lib.Model.SourcingManage;
using Lib.Bll.SourcingManage.SourcingManagement;
using Lib.Bll.SourcingManage.SourceingRecord;
using Lib.Model.SourcingManage.SourcingManagement;
using Lib.Model.SourcingManage.SourceingRecord;
using Lib.Common.CommonUtils;
using Lib.Model.FileManage;
using MMClient.CommonForms;
using Lib.Bll.FileManageBLL;
using System.Web.UI;
using System.Web.UI.WebControls;
using Lib.SqlServerDAL;


namespace MMClient.SourcingManage.SourcingManagement
{
    public partial class VolvoReportView_Form : Form
    {
        DataSet[] d;

        DataSet ds1;
        DataSet ds2;
        DataSet ds3;
        DataSet ds4;
        DataSet ds5;
        DataSet ds6;
        DataSet ds7;
        DataSet ds8;
        DataSet ds9;
        DataSet ds10;
        DataSet ds11;
        DataSet ds12;

        string value;
        string connString;

        int a;
        int c = 1;
        public string partNumber="";
        public string supplierId="";
        public VolvoReportView_Form()
        {
            InitializeComponent();
        }
        public VolvoReportView_Form(string s1,string s2)
        {
            partNumber = s1;
            supplierId = s2;
            InitializeComponent();
        }

        private void VolvoReportView_Form_Load(object sender, EventArgs e)
        {
            connString = ConfigurationManager.AppSettings["serverAddress"];
            comboBox1.DataSource = DBHelper.ExecuteQueryDT("select ID from dbo.Inquiry_Tooling_Breakdown where Part_Number = '" + partNumber + "'and Supplier_ID = '" + supplierId + "'");
            comboBox1.DisplayMember = "ID";
            comboBox1.ValueMember = "ID";

       

            //展示dbo.Inquiry_Standard_Parts
            SqlConnection conn1 = new SqlConnection(connString);
            conn1.Open();
            //select * from dbo.Inquiry_Summary where Part_Number=(select Part_Number from dbo.Inquiry_Sheets where Part_Number='" + value + "' and Confirmed='1')
            string str1 = "select * from dbo.Inquiry_Standard_Parts where Part_Number = '" + partNumber + "'and Supplier_ID = '" + supplierId + "'";
            SqlCommand cmd1 = new SqlCommand(str1, conn1);
            cmd1.CommandType = CommandType.Text;
            SqlDataReader dr1 = cmd1.ExecuteReader();
            ds1 = new DataSet1();//新建一个临时数据库
            ds1.Tables[0].Load(dr1);
            //ds[1] = ds1;
            dr1.Close();
            conn1.Close();
            //this.reportViewer1.ProcessingMode = ProcessingMode.Local;
            //this.reportViewer1.LocalReport.ReportPath = ".../.../Report1.rdlc";
            //this.reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DataSet1", ds1.Tables[0]));
            //this.reportViewer1.RefreshReport();

            //展示dbo.Inquiry_Raw_Material
            SqlConnection conn2 = new SqlConnection(connString);
            conn2.Open();
            string str2 = "select * from dbo.Inquiry_Raw_Material where Part_Number = '" + partNumber + "'and Supplier_ID = '" + supplierId + "'";
            SqlCommand cmd2 = new SqlCommand(str2, conn2);
            cmd2.CommandType = CommandType.Text;
            SqlDataReader dr2 = cmd2.ExecuteReader();
            ds2 = new DataSet2();//新建一个临时数据库
            ds2.Tables[0].Load(dr2);
            //ds[2] = ds2;
            dr2.Close();
            conn2.Close();


            //展示dbo.Inquiry_Purchased_POEO
            SqlConnection conn3 = new SqlConnection(connString);
            conn3.Open();
            string str3 = "select * from dbo.Inquiry_Purchased_POEO where Part_Number = '" + partNumber + "'and Supplier_ID = '" + supplierId + "'";
            SqlCommand cmd3 = new SqlCommand(str3, conn3);
            cmd3.CommandType = CommandType.Text;
            SqlDataReader dr3 = cmd3.ExecuteReader();
            ds3 = new DataSet3();//新建一个临时数据库
            ds3.Tables[0].Load(dr3);
            //ds[3] = ds3;
            dr3.Close();
            conn3.Close();


            //展示dbo.Inquiry_Process_Cost_Melting
            SqlConnection conn4 = new SqlConnection(connString);
            conn4.Open();
            string str4 = "select * from dbo.Inquiry_Process_Cost_Melting where Part_Number = '" + partNumber + "'and Supplier_ID = '" + supplierId + "'";
            SqlCommand cmd4 = new SqlCommand(str4, conn4);
            cmd4.CommandType = CommandType.Text;
            SqlDataReader dr4 = cmd4.ExecuteReader();
            ds4 = new DataSet4();//新建一个临时数据库
            ds4.Tables[0].Load(dr4);
            //ds[4] = ds4;
            dr4.Close();
            conn4.Close();


            //展示dbo.Inquiry.Process_Cost_Details
            SqlConnection conn5 = new SqlConnection(connString);
            conn5.Open();
            string str5 = "select * from dbo.Inquiry_Process_Cost_Details where Part_Number = '" + partNumber + "'and Supplier_ID = '" + supplierId + "'";
            SqlCommand cmd5 = new SqlCommand(str5, conn5);
            cmd5.CommandType = CommandType.Text;
            SqlDataReader dr5 = cmd5.ExecuteReader();
            ds5 = new DataSet5();//新建一个临时数据库
            ds5.Tables[0].Load(dr5);
            //ds[5] = ds5;
            dr5.Close();
            conn5.Close();


            


            //展示dbo.Inquiry_Logistics（包括基础信息和着陆成本）
            SqlConnection conn10 = new SqlConnection(connString);
            conn10.Open();
            string str10 = "select * from dbo.Inquiry_Logistics where Part_Number = '" + partNumber + "'and Supplier_ID = '" + supplierId + "'";
            SqlCommand cmd10 = new SqlCommand(str10, conn10);
            cmd10.CommandType = CommandType.Text;
            SqlDataReader dr10 = cmd10.ExecuteReader();
            ds10 = new DataSet10();//新建一个临时数据库
            ds10.Tables[0].Load(dr10);
            //ds[10] = ds10;
            dr10.Close();
            conn10.Close();


            //展示dbo.Inquiry_Logistics_FCA
            SqlConnection conn11 = new SqlConnection(connString);
            conn11.Open();
            //string str11 = "select ID,Packagingdunnageitems,Item_Unit_Cost,Quantity_Emballage,Cost_Per_Emballage,Cost_Per_Piece from dbo.Inquiry_Logistics_FCA where Part_Number='1' and Supplier_ID='20180503215426'";
            string str11 = "select * from dbo.Inquiry_Logistics_FCA where Part_Number = '" + partNumber + "'and Supplier_ID = '" + supplierId + "'";
            SqlCommand cmd11 = new SqlCommand(str11, conn11);
            cmd11.CommandType = CommandType.Text;
            SqlDataReader dr11 = cmd11.ExecuteReader();
            ds11 = new DataSet11();//新建一个临时数据库
            ds11.Tables[0].Load(dr11);
            //ds[11] = ds11;
            dr11.Close();
            conn11.Close();


           
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ////上一步
            //if (c == 1)
            //{
            //    MessageBox.Show("已经是第一页！");
            //    return;
            //}
            //else
            //{
            //    a = c - 1;
            //    this.reportViewer1.ProcessingMode = ProcessingMode.Local;
            //    this.reportViewer1.LocalReport.ReportPath = ".../.../Report" + a + ".rdlc";
            //    this.reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DataSet" + a + "", d[a].Tables[0]));
            //    this.reportViewer1.RefreshReport();
            //    c = a;
            //}
            string CurrentPath = System.Environment.CurrentDirectory;
            //显示第一部分
            this.reportViewer1.ProcessingMode = ProcessingMode.Local;
            this.reportViewer1.LocalReport.ReportPath = CurrentPath+"/Report1.rdlc";
            this.reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DataSet1", ds1.Tables[0]));
            this.reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DataSet2", ds2.Tables[0]));
            this.reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DataSet3", ds3.Tables[0]));
            this.reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DataSet4", ds4.Tables[0]));
            this.reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DataSet5", ds5.Tables[0]));
            this.reportViewer1.RefreshReport();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            

            //展示dbo.Inquiry_Tooling_Breakdown
            SqlConnection conn6 = new SqlConnection(connString);
            conn6.Open();
            string str6 = "select * from dbo.Inquiry_Tooling_Breakdown where Part_Number = '" + partNumber + "'and " +
            "Supplier_ID = '" + supplierId + "' and ID='" + comboBox1.Text  + "'";
            SqlCommand cmd6 = new SqlCommand(str6, conn6);
            cmd6.CommandType = CommandType.Text;
            SqlDataReader dr6 = cmd6.ExecuteReader();
            ds6 = new DataSet6();//新建一个临时数据库
            ds6.Tables[0].Load(dr6);
            //ds[6] = ds6;
            dr6.Close();
            conn6.Close();

            //展示dbo.Inquiry_TB_MaterialCost
            SqlConnection conn7 = new SqlConnection(connString);
            conn7.Open();
            string str7 = "select * from dbo.Inquiry_TB_MaterialCost where Part_Number = '" + partNumber + "'" +
            "and Supplier_ID = '" + supplierId + "' and Out_ID='" + comboBox1.Text + "'";
            SqlCommand cmd7 = new SqlCommand(str7, conn7);
            cmd7.CommandType = CommandType.Text;
            SqlDataReader dr7 = cmd7.ExecuteReader();
            ds7 = new DataSet7();//新建一个临时数据库
            ds7.Tables[0].Load(dr7);
            //ds[7] = ds7;
            dr7.Close();
            conn7.Close();

            //展示dbo.Inquiry_TB_Toolmaker
            SqlConnection conn8 = new SqlConnection(connString);
            conn8.Open();
            string str8 = "select * from dbo.Inquiry_TB_Toolmaker where Part_Number = '" + partNumber + "'" +
            "and Supplier_ID = '" + supplierId + "' and Out_ID='" + comboBox1.Text + "'";
            SqlCommand cmd8 = new SqlCommand(str8, conn8);
            cmd8.CommandType = CommandType.Text;
            SqlDataReader dr8 = cmd8.ExecuteReader();
            ds8 = new DataSet8();//新建一个临时数据库
            ds8.Tables[0].Load(dr8);
            //ds[8] = ds8;
            dr8.Close();
            conn8.Close();

            //展示dbo.Inquiry_TB_OverheadCost
            SqlConnection conn9 = new SqlConnection(connString);
            conn9.Open();
            string str9 = "select * from dbo.Inquiry_TB_OverheadCost where Part_Number = '" + partNumber + "'" +
            "and Supplier_ID = '" + supplierId + "' and Out_ID='" + comboBox1.Text + "'";
            SqlCommand cmd9 = new SqlCommand(str9, conn9);
            cmd9.CommandType = CommandType.Text;
            SqlDataReader dr9 = cmd9.ExecuteReader();
            ds9 = new DataSet9();//新建一个临时数据库
            ds9.Tables[0].Load(dr9);
            //ds[9] = ds9;
            dr9.Close();
            conn9.Close();

            ////下一步
            //if (c == 4)
            //{
            //    this.reportViewer1.ProcessingMode = ProcessingMode.Local;
            //    this.reportViewer1.LocalReport.ReportPath = ".../.../Report5.rdlc";
            //    this.reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DataSet5", ds5.Tables[0]));
            //    this.reportViewer1.RefreshReport();
            //    MessageBox.Show("已经是最后一页！");
            //    return;
            //}
            //else
            //{
            //    a = c + 1;
            //    this.reportViewer1.ProcessingMode = ProcessingMode.Local;
            //    this.reportViewer1.LocalReport.ReportPath = ".../.../Report" + a + ".rdlc";
            //    this.reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DataSet" + a + "", d[a - 1].Tables[0]));
            //    this.reportViewer1.RefreshReport();
            //    c = a;
            //}

            //显示第二部分
            this.reportViewer1.LocalReport.DataSources.Clear();
            this.reportViewer1.ProcessingMode = ProcessingMode.Local;
            this.reportViewer1.LocalReport.ReportPath = "./Report2.rdlc";
            this.reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DataSet6", ds6.Tables[0]));
            this.reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DataSet7", ds7.Tables[0]));
            this.reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DataSet8", ds8.Tables[0]));
            this.reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DataSet9", ds9.Tables[0]));
            this.reportViewer1.RefreshReport();

        }

        private void button3_Click(object sender, EventArgs e)
        {
            //显示第三部分
            this.reportViewer1.ProcessingMode = ProcessingMode.Local;
            this.reportViewer1.LocalReport.ReportPath = "./Report3.rdlc";
            this.reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DataSet10", ds10.Tables[0]));
            this.reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DataSet11", ds11.Tables[0]));
            //this.reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DataSet12", ds12.Tables[0]));
            this.reportViewer1.RefreshReport();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
        }
    }
}
