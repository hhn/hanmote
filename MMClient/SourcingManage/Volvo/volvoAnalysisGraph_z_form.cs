﻿using Lib.SqlServerDAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using WeifenLuo.WinFormsUI.Docking;

namespace MMClient.SourcingManage.SourcingManagement
{
    public partial class volvoAnalysisGraph_z_form : DockContent
    {
        string material_ID="";
        string supplier_ID="";
        List<string> part_number = new List<string>();
        DataTable dt1=new DataTable();//计数
        DataTable dt2 = new DataTable();//列举

        public volvoAnalysisGraph_z_form()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DateTime startTime = dateTimePicker1.Value;

            DateTime endTime = dateTimePicker2.Value;
            string sql = string.Format("select Material_ID,Material_Name,Part_Number from dbo.Inquiry_Base where Start_Date between '{0}' and  '{1}'", startTime, endTime);
            //DBHelper.
            String sql1 = string.Format("select  Material_ID ,Material_Name,dbo.Inquiry_Summary.Part_Number,dbo.Inquiry_Summary.Supplier_ID from dbo.Inquiry_Summary left join dbo.Inquiry_Base on dbo.Inquiry_Summary.Part_Number=dbo.Inquiry_Base.Part_Number where dbo.Inquiry_Summary.Part_Number in (select Part_Number from dbo.Inquiry_Base where Start_Date between '{0}' and  '{1}') order by Start_Date", startTime, endTime);
            // DataTable dt1= DBHelper.ExecuteQueryDT(sql1);
            //string sql2= string.Format("select dbo.Inquiry_Summary.Part_Number,Material_ID,dbo.Inquiry_Summary.Supplier_ID from dbo.Inquiry_Summary left join dbo.Inquiry_Base on dbo.Inquiry_Summary.Part_Number=dbo.Inquiry_Base.Part_Number where dbo.Inquiry_Summary.Part_Number in (select Part_Number from dbo.Inquiry_Base where Start_Date between '{0}' and  '{1}') ", startTime, endTime);
            string sql2 = string.Format("select  Material_ID as 物料号,Material_Name as 物料名,dbo.Inquiry_Summary.Supplier_ID as 供应商号, count(*) as 报价方案数 from dbo.Inquiry_Summary left join dbo.Inquiry_Base on dbo.Inquiry_Summary.Part_Number=dbo.Inquiry_Base.Part_Number where dbo.Inquiry_Summary.Part_Number in (select Part_Number from dbo.Inquiry_Base where Start_Date between '{0}' and  '{1}') group by dbo.Inquiry_Summary.Supplier_ID,Material_ID,Material_Name", startTime, endTime);
         
            dt1 = DBHelper.ExecuteQueryDT(sql2);
            dt2 = DBHelper.ExecuteQueryDT(sql1);
            

            if (dt1.Rows.Count == 0)
            {
                
            }
            else
            {
                
                dataGridView1.DataSource = dt1;
                MessageBox.Show(string.Format("存在{0}条记录", dt1.Rows.Count.ToString()));
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            int index = dataGridView1.CurrentRow.Index; //获取选中行的行号
            if (index >= 0)
            {
                //将选中的
                material_ID = dataGridView1.Rows[index].Cells[0].Value.ToString();
                supplier_ID = dataGridView1.Rows[index].Cells[2].Value.ToString();
                DataRow[] row=dt2.Select("Material_ID='"+material_ID+"' and Supplier_ID='"+supplier_ID+"'");
                listBox1.Items.Clear();
                part_number.Clear();
                foreach (DataRow dr in row)
                {
                    listBox1.Items.Add(dr[2].ToString());
                    part_number.Add(dr[2].ToString());
                }

            }

        }

        private void button3_Click(object sender, EventArgs e)
        {
            chart1.Series.Clear();
            chart2.Series.Clear();
            chart3.Series.Clear();
            chart4.Series.Clear();
            chart5.Series.Clear();
            chart6.Series.Clear();
            chart7.Series.Clear();
            chart8.Series.Clear();
            chart9.Series.Clear();
            chart10.Series.Clear();
            chart11.Series.Clear();
            chart12.Series.Clear();
            DataTable dt2 = new DataTable();
            List<string> list = new List<string>();
            List<float> fDownTime = new List<float>();
            List<int> x = new List<int>() { 1 };
            for (int i=0;i<part_number.Count;i++)
            {

                //
                fDownTime.Clear();
                string sql = string.Format("select Standard_Part_Cost from dbo.Inquiry_Summary where Part_Number='{0}' and Supplier_ID='{1}'", part_number[i], supplier_ID);
                dt2 = DBHelper.ExecuteQueryDT(sql);
                if (dt2.Rows.Count > 0)
                {
                    fDownTime.Add(float.Parse(dt2.Rows[0][0].ToString()));
                }
                else
                {
                    fDownTime.Add(0);
                }
                chart1.Series.Add(part_number[i]);//此处要改成supplierID
                chart1.Series[part_number[i]].XValueType = ChartValueType.String;
                chart1.Series[part_number[i]].Points.DataBindXY(x, fDownTime);
                chart1.Series[part_number[i]].ChartType = SeriesChartType.Column;


                //
                fDownTime.Clear();
                sql = string.Format("select Raw_Material_Cost from dbo.Inquiry_Summary where Part_Number='{0}' and Supplier_ID='{1}'", part_number[i], supplier_ID);
                dt2 = DBHelper.ExecuteQueryDT(sql);
                if (dt2.Rows.Count > 0)
                {
                    fDownTime.Add(float.Parse(dt2.Rows[0][0].ToString()));
                }
                else
                {
                    fDownTime.Add(0);
                }
                chart2.Series.Add(part_number[i]);//此处要改成supplierID
                chart2.Series[part_number[i]].XValueType = ChartValueType.String;
                chart2.Series[part_number[i]].Points.DataBindXY(x, fDownTime);
                chart2.Series[part_number[i]].ChartType = SeriesChartType.Column;

                //
                fDownTime.Clear();
                sql = string.Format("select Purchased_Part_Ext_Oper from dbo.Inquiry_Summary where Part_Number='{0}' and Supplier_ID='{1}'", part_number[i], supplier_ID);
                dt2 = DBHelper.ExecuteQueryDT(sql);
                if (dt2.Rows.Count > 0)
                {
                    fDownTime.Add(float.Parse(dt2.Rows[0][0].ToString()));
                }
                else
                {
                    fDownTime.Add(0);
                }
                chart3.Series.Add(part_number[i]);//此处要改成supplierID
                chart3.Series[part_number[i]].XValueType = ChartValueType.String;
                chart3.Series[part_number[i]].Points.DataBindXY(x, fDownTime);
                chart3.Series[part_number[i]].ChartType = SeriesChartType.Column;

                //
                fDownTime.Clear();
                dt1.Clear();
                sql = string.Format("select sum(TotalCost) from dbo.Inquiry_Process_Cost_Melting where Part_Number='{0}' and Supplier_ID='{1}'", part_number[i], supplier_ID);
                dt2 = DBHelper.ExecuteQueryDT(sql);
                if (dt2.Rows.Count > 0)
                {
                    fDownTime.Add(float.Parse(dt2.Rows[0][0].ToString()));
                }
                else
                {
                    fDownTime.Add(0);
                }
                chart4.Series.Add(part_number[i]);//此处要改成supplierID
                chart4.Series[part_number[i]].XValueType = ChartValueType.String;
                chart4.Series[part_number[i]].Points.DataBindXY(x, fDownTime);
                chart4.Series[part_number[i]].ChartType = SeriesChartType.Column;

                //
                fDownTime.Clear();
                dt1.Clear();
                sql = string.Format("select sum(Total_Cost) from dbo.Inquiry_Process_Cost_Details where Part_Number='{0}' and Supplier_ID='{1}'", part_number[i], supplier_ID);
                dt2 = DBHelper.ExecuteQueryDT(sql);
                if (dt2.Rows.Count > 0)
                {
                    fDownTime.Add(float.Parse(dt2.Rows[0][0].ToString()));
                }
                else
                {
                    fDownTime.Add(0);
                }
                chart5.Series.Add(part_number[i]);//此处要改成supplierID
                chart5.Series[part_number[i]].XValueType = ChartValueType.String;
                chart5.Series[part_number[i]].Points.DataBindXY(x, fDownTime);
                chart5.Series[part_number[i]].ChartType = SeriesChartType.Column;

                //Packing
                fDownTime.Clear();
                sql = string.Format("select Packing_Cost from dbo.Inquiry_Summary where Part_Number='{0}' and Supplier_ID='{1}'", part_number[i], supplier_ID);
                dt2 = DBHelper.ExecuteQueryDT(sql);
                if (dt2.Rows.Count > 0)
                {
                    fDownTime.Add(float.Parse(dt2.Rows[0][0].ToString()));
                }
                else
                {
                    fDownTime.Add(0);
                }
                chart6.Series.Add(part_number[i]);//此处要改成supplierID
                chart6.Series[part_number[i]].XValueType = ChartValueType.String;
                chart6.Series[part_number[i]].Points.DataBindXY(x, fDownTime);
                chart6.Series[part_number[i]].ChartType = SeriesChartType.Column;

                //landing
                fDownTime.Clear();
                dt1.Clear();
                sql = string.Format("select LandingCostTotal from dbo.Inquiry_Logistics where Part_Number='{0}' and Supplier_ID='{1}'", part_number[i], supplier_ID);
                dt2 = DBHelper.ExecuteQueryDT(sql);
                if (dt2.Rows.Count > 0)
                {
                    fDownTime.Add(float.Parse(dt2.Rows[0][0].ToString()));
                }
                else
                {
                    fDownTime.Add(0);
                }
                chart7.Series.Add(part_number[i]);//此处要改成supplierID
                chart7.Series[part_number[i]].XValueType = ChartValueType.String;
                chart7.Series[part_number[i]].Points.DataBindXY(x, fDownTime);
                chart7.Series[part_number[i]].ChartType = SeriesChartType.Column;

                //other
                fDownTime.Clear();
                sql = string.Format("select Other_Cost from dbo.Inquiry_Summary where Part_Number='{0}' and Supplier_ID='{1}'", part_number[i], supplier_ID);
                dt2 = DBHelper.ExecuteQueryDT(sql);
                if (dt2.Rows.Count > 0)
                {
                    fDownTime.Add(float.Parse(dt2.Rows[0][0].ToString()));
                }
                else
                {
                    fDownTime.Add(0);
                }
                chart8.Series.Add(part_number[i]);//此处要改成supplierID
                chart8.Series[part_number[i]].XValueType = ChartValueType.String;
                chart8.Series[part_number[i]].Points.DataBindXY(x, fDownTime);
                chart8.Series[part_number[i]].ChartType = SeriesChartType.Column;

                //TB_MaterialCost
                fDownTime.Clear();
                sql = string.Format("select sum(Cost_CNY) from dbo.Inquiry_TB_MaterialCost where Part_Number='{0}' and Supplier_ID='{1}'", part_number[i], supplier_ID);
                dt2 = DBHelper.ExecuteQueryDT(sql);
                if (dt2.Rows.Count > 0)
                {
                    fDownTime.Add(float.Parse(dt2.Rows[0][0].ToString()));
                }
                else
                {
                    fDownTime.Add(0);
                }
                chart9.Series.Add(part_number[i]);//此处要改成supplierID
                chart9.Series[part_number[i]].XValueType = ChartValueType.String;
                chart9.Series[part_number[i]].Points.DataBindXY(x, fDownTime);
                chart9.Series[part_number[i]].ChartType = SeriesChartType.Column;


                //Heatthreatment_Cost
                fDownTime.Clear();
                sql = string.Format("select sum(Heatthreatment_Cost) from dbo.Inquiry_Tooling_Breakdown where Part_Number='{0}' and Supplier_ID='{1}'", part_number[i], supplier_ID);
                dt2 = DBHelper.ExecuteQueryDT(sql);
                if (dt2.Rows.Count > 0)
                {
                    fDownTime.Add(float.Parse(dt2.Rows[0][0].ToString()));
                }
                else
                {
                    fDownTime.Add(0);
                }
                chart10.Series.Add(part_number[i]);//此处要改成supplierID
                chart10.Series[part_number[i]].XValueType = ChartValueType.String;
                chart10.Series[part_number[i]].Points.DataBindXY(x, fDownTime);
                chart10.Series[part_number[i]].ChartType = SeriesChartType.Column;

                //Inquiry_TB_Toolmaker
                fDownTime.Clear();
                sql = string.Format("select  sum(Total_ToolmakerCost) from dbo.Inquiry_TB_Toolmaker where Part_Number='{0}' and Supplier_ID='{1}'", part_number[i], supplier_ID);
                dt2 = DBHelper.ExecuteQueryDT(sql);
                if (dt2.Rows.Count > 0)
                {
                    fDownTime.Add(float.Parse(dt2.Rows[0][0].ToString()));
                }
                else
                {
                    fDownTime.Add(0);
                }
                chart11.Series.Add(part_number[i]);//此处要改成supplierID
                chart11.Series[part_number[i]].XValueType = ChartValueType.String;
                chart11.Series[part_number[i]].Points.DataBindXY(x, fDownTime);
                chart11.Series[part_number[i]].ChartType = SeriesChartType.Column;

                //Inquiry_TB_OverheadCost

                fDownTime.Clear();
                sql = string.Format("select  sum(Total_OverheadCost) from dbo.Inquiry_TB_OverheadCost where Part_Number='{0}' and Supplier_ID='{1}'", part_number[i], supplier_ID);
                dt2 = DBHelper.ExecuteQueryDT(sql);
                if (dt2.Rows.Count > 0)
                {
                    fDownTime.Add(float.Parse(dt2.Rows[0][0].ToString()));
                }
                else
                {
                    fDownTime.Add(0);
                }
                chart12.Series.Add(part_number[i]);//此处要改成supplierID
                chart12.Series[part_number[i]].XValueType = ChartValueType.String;
                chart12.Series[part_number[i]].Points.DataBindXY(x, fDownTime);
                chart12.Series[part_number[i]].ChartType = SeriesChartType.Column;


            }


        }

        private void volvoAnalysisGraph_z_form_Load(object sender, EventArgs e)
        {
            chart1.GetToolTipText += new EventHandler<ToolTipEventArgs>(chart_GetToolTipText);
            chart2.GetToolTipText += new EventHandler<ToolTipEventArgs>(chart_GetToolTipText);
            chart3.GetToolTipText += new EventHandler<ToolTipEventArgs>(chart_GetToolTipText);
            chart4.GetToolTipText += new EventHandler<ToolTipEventArgs>(chart_GetToolTipText);
            chart5.GetToolTipText += new EventHandler<ToolTipEventArgs>(chart_GetToolTipText);
            chart6.GetToolTipText += new EventHandler<ToolTipEventArgs>(chart_GetToolTipText);
            chart7.GetToolTipText += new EventHandler<ToolTipEventArgs>(chart_GetToolTipText);
            chart8.GetToolTipText += new EventHandler<ToolTipEventArgs>(chart_GetToolTipText);
            chart9.GetToolTipText += new EventHandler<ToolTipEventArgs>(chart_GetToolTipText);
            chart10.GetToolTipText += new EventHandler<ToolTipEventArgs>(chart_GetToolTipText);
            chart11.GetToolTipText += new EventHandler<ToolTipEventArgs>(chart_GetToolTipText);
            chart12.GetToolTipText += new EventHandler<ToolTipEventArgs>(chart_GetToolTipText);
        }
        private void chart_GetToolTipText(object sender, ToolTipEventArgs e)
        {
            //鼠标放在某个柱状图上时显示那个图的数据
            if (e.HitTestResult.ChartElementType == ChartElementType.DataPoint)
            {
                int i = e.HitTestResult.PointIndex;
                DataPoint dp = e.HitTestResult.Series.Points[i];
                //分别显示x轴和y轴的数值，其中{1:F3},表示显示的是float类型,精确到小数点后3位  
                e.Text = string.Format("{0};价格:{1:F3} ", e.HitTestResult.Series.Name, dp.YValues[0]);
            }
        }
    }
}
