﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Net.Mail;
using Aspose.Cells;
using Lib.SqlServerDAL.SourcingManage.ProcurementPlan;
using Lib.Model.SourcingManage.ProcurementPlan;
using Lib.Model.SourcingManage;
using Lib.Bll.SourcingManage.SourcingManagement;
using Lib.Bll.SourcingManage.SourceingRecord;
using WeifenLuo.WinFormsUI.Docking;
using System.IO;
using Lib.Model.SourcingManage.SourcingManagement;
using Lib.Model.SourcingManage.SourceingRecord;
using Lib.Common.CommonUtils;
using Lib.Model.FileManage;
using MMClient.CommonForms;
using Lib.Bll.FileManageBLL;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

namespace MMClient.SourcingManage.SourcingManagement
{
    public partial class VolvoMessage4Outbox_Form : Form
    {
        public static string[] a;
        string connString;
        public VolvoMessage4Outbox_Form()
        {
            InitializeComponent();
        }

        private void VolvoMessage4Outbox_Form_Load(object sender, EventArgs e)
        {
            //读取dbo.Inquiry_Message表的Title, Supplier_ID, Send_Time, Status到发件箱记录表
            connString = ConfigurationManager.AppSettings["serverAddress"];
            SqlConnection conn1 = new SqlConnection(connString);
            SqlDataAdapter sda = new SqlDataAdapter("select Message_ID, Part_Number, Title, Supplier_ID, Send_Time, Status, Context, AttachmentFileName from dbo.Inquiry_Message where Sender='0'", conn1);
            DataSet ds = new DataSet();
            sda.Fill(ds, "em1");
            dataGridView1.DataSource = ds.Tables["em1"];
            dataGridView1.Columns[0].Visible = false;
            dataGridView1.Columns[1].Visible = false;
            dataGridView1.Columns[6].Visible = false;
            dataGridView1.Columns[7].Visible = false;
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (this.dataGridView1.SelectionMode != DataGridViewSelectionMode.FullColumnSelect)
            {
                //选中物料ID表中的Material_ID和Material_Name到textBox1
                //int index = dataGridView1.CurrentRow.Index; //获取选中行的行号
                //textBox1.Text = dataGridView1.Rows[index].Cells[1].Value.ToString();
                //textBox4.Text = dataGridView1.Rows[index].Cells[2].Value.ToString();

                //string str = dataGridView1.Rows[index].Cells["Context"].Value;
                //编辑界面
                //这个写在load事件中
                //textBox1.Text = str;

                int index = dataGridView1.CurrentRow.Index; //获取选中行的行号
                a = new string[dataGridView1.ColumnCount];
                for (int i = 0; i < dataGridView1.ColumnCount; i++)
                {
                    a[i] = dataGridView1.Rows[index].Cells[i].Value.ToString();
                }

                //查看邮件后，更新status
                SqlConnection SqlConn4 = new SqlConnection(connString);
                SqlConn4.Open();
                string str1 = "update dbo.Inquiry_Message set Status = '1' where Message_ID ='" + a[0] + "'";
                SqlCommand SqlCmd1 = new SqlCommand(str1, SqlConn4);
                SqlCmd1.ExecuteNonQuery();
                SqlConn4.Close();


                VolvoMessage4Outbox_Context_Form frm9 = new VolvoMessage4Outbox_Context_Form(a);
                frm9.Show();

            }
        }
    }
}
