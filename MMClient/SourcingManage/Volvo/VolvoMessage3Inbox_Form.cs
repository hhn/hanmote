﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Net.Mail;
using Aspose.Cells;
using Lib.SqlServerDAL.SourcingManage.ProcurementPlan;
using Lib.Model.SourcingManage.ProcurementPlan;
using Lib.Model.SourcingManage;
using Lib.Bll.SourcingManage.SourcingManagement;
using Lib.Bll.SourcingManage.SourceingRecord;
using WeifenLuo.WinFormsUI.Docking;
using System.IO;
using Lib.Model.SourcingManage.SourcingManagement;
using Lib.Model.SourcingManage.SourceingRecord;
using Lib.Common.CommonUtils;
using Lib.Model.FileManage;
using MMClient.CommonForms;
using Lib.Bll.FileManageBLL;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

namespace MMClient.SourcingManage.SourcingManagement
{
    public partial class VolvoMessage3Inbox_Form : Form
    {
        public static string[] a;
        string connString;


        public VolvoMessage3Inbox_Form()
        {
            InitializeComponent();
        }

        private void VolvoMessage3Inbox_Form_Load(object sender, EventArgs e)
        {
            connString = ConfigurationManager.AppSettings["serverAddress"];
            //读取dbo.Inquiry_Message表的Title, User_ID, Send_Time, Status到收件箱记录表
            SqlConnection conn1 = new SqlConnection(connString);
            SqlDataAdapter sda = new SqlDataAdapter("select Message_ID, Part_Number, Title, User_ID, Supplier_ID, Send_Time, Status, Context, AttachmentFileName, Start_Number from dbo.Inquiry_Message where Sender='1'", conn1);
            DataSet ds = new DataSet();
            sda.Fill(ds, "em1");
            dataGridView1.DataSource = ds.Tables["em1"];
            dataGridView1.Columns[0].Visible = false;
            dataGridView1.Columns[1].Visible = false;
            dataGridView1.Columns[3].Visible = false;
            dataGridView1.Columns[6].Visible = false;
            dataGridView1.Columns[8].Visible = false;
            dataGridView1.Columns[9].Visible = false;
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (this.dataGridView1.SelectionMode != DataGridViewSelectionMode.FullColumnSelect)
            {
                int index = dataGridView1.CurrentRow.Index; //获取选中行的行号
                a = new string[dataGridView1.ColumnCount];
                for (int i = 0; i < dataGridView1.ColumnCount; i++)
                {
                    a[i] = dataGridView1.Rows[index].Cells[i].Value.ToString();
                }

                //查看邮件后，更新status
                SqlConnection SqlConn4 = new SqlConnection(connString);
                SqlConn4.Open();
                string str1 = "update dbo.Inquiry_Message set Status = '1' where Message_ID ='" + a[0] + "'";
                SqlCommand SqlCmd1 = new SqlCommand(str1, SqlConn4);
                SqlCmd1.ExecuteNonQuery();
                SqlConn4.Close();

                VolvoMessage3Inbox_Context_Form frm8 = new VolvoMessage3Inbox_Context_Form(a);
                frm8.Show();
            }
        }
    }
}
