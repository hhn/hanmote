﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;
using Lib.Model.SourcingManage.SourceingRecord;
using Lib.Common.CommonUtils;
using Lib.Model.FileManage;
using MMClient.CommonForms;
using Lib.Bll.FileManageBLL;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using Lib.SqlServerDAL;
using System.Data.SqlClient;
using System.Net.Mail;
using Aspose.Cells;
using Lib.SqlServerDAL.SourcingManage.ProcurementPlan;
using Lib.Model.SourcingManage.ProcurementPlan;
using Lib.Model.SourcingManage;
using Lib.Bll.SourcingManage.SourcingManagement;
using Lib.Bll.SourcingManage.SourceingRecord;
using System.IO;
using Lib.Model.SourcingManage.SourcingManagement;

namespace MMClient.SourcingManage.SourcingManagement
{
    public partial class VolvoInquiry4_Form : DockContent
    {


        string attchmentfilename1;
        public static string[] a;
        public static string foldPath;
        public static string filename;
        string connString;
        FileStream fs;

        public VolvoInquiry4_Form()
        {
            InitializeComponent();
        }

        private void VolvoInquiry4_Form_Load(object sender, EventArgs e)
        {
            //读取dbo.Inquiry_Base的Part_Number到comboBox1里
            comboBox1.DataSource = DBHelper.ExecuteQueryDT("select Part_Number from dbo.Inquiry_Base");
            comboBox1.DisplayMember = "Part_Number";
            comboBox1.ValueMember = "Part_Number";        
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //根据选中的Part_Number，读取dbo.Inquiry_Base表的一条记录到各个文本框里
            //DataTable db= DBHelper.ExecuteQueryDT("select ID, Demand_ID, Material_ID, Material_Name, Aggregate_ID, Demand_Count, Measurement, Stock_ID, Factory_ID, DeliveryStartTime, DeliveryEndTime from dbo.Demand_Material where Demand_ID = '" + demandid + "' and Material_ID = '" + materialid + "'");
            //SqlConnection conn1 = new SqlConnection("server=116.196.99.18;database=MMBackup;uid=sa;pwd=b602!@#qwe");
            connString = ConfigurationManager.AppSettings["serverAddress"];
            SqlDataAdapter sda1 = new SqlDataAdapter("select Part_Number, Start_Date, Expire_Date, Valid_State, Draw_No_Issue, Material_ID, Material_Name, Yearly_Volume, Payment_Terms, " +
            "Delivery_Conditions, Point_Of_Delivery, Parma_Code, Source_Country, Local_Currency, Buyer, Material_Process_Breakdown, Tooling_Breakdown, Flowchart, Description, " +
            "Logistics, Misc_Information, Attachment, FileName, Misc_Information_text, Tooling_and_Utilization, Project_Type, Demand_Material_ID, CompanyID, Buyer_Group, Buyer_org from dbo.Inquiry_Base where Part_Number='" + comboBox1.Text + "'", connString);
            DataSet ds1 = new DataSet();
            sda1.Fill(ds1, "em1");
            int b = ds1.Tables[0].Rows.Count;
            if (b > 0)
            {
                textBox2.Text = ds1.Tables[0].Rows[0]["Start_Date"].ToString();
                textBox6.Text = ds1.Tables[0].Rows[0]["Expire_Date"].ToString();
                textBox3.Text = ds1.Tables[0].Rows[0]["Draw_No_Issue"].ToString();
                textBox21.Text = ds1.Tables[0].Rows[0]["Material_ID"].ToString();
                textBox14.Text = ds1.Tables[0].Rows[0]["Material_Name"].ToString();
                textBox22.Text = ds1.Tables[0].Rows[0]["Yearly_Volume"].ToString();
                textBox4.Text = ds1.Tables[0].Rows[0]["Payment_Terms"].ToString();
                textBox7.Text = ds1.Tables[0].Rows[0]["Delivery_Conditions"].ToString();
                textBox8.Text = ds1.Tables[0].Rows[0]["Point_Of_Delivery"].ToString();
                textBox12.Text = ds1.Tables[0].Rows[0]["Parma_Code"].ToString();
                textBox9.Text = ds1.Tables[0].Rows[0]["Source_Country"].ToString();
                textBox10.Text = ds1.Tables[0].Rows[0]["Local_Currency"].ToString();
                textBox24.Text = ds1.Tables[0].Rows[0]["Buyer"].ToString();
                filename= ds1.Tables[0].Rows[0]["FileName"].ToString();

                string material = ds1.Tables[0].Rows[0]["Material_Process_Breakdown"].ToString();
                if(material=="True")
                {
                    checkBox1.Checked = true;
                }
                else
                {
                    checkBox1.Checked = false;
                }

                string tooluti = ds1.Tables[0].Rows[0]["Tooling_and_Utilization"].ToString();
                if (tooluti == "True")
                {
                    checkBox3.Checked = true;
                }
                else
                {
                    checkBox3.Checked = false;
                }

                string toolbre = ds1.Tables[0].Rows[0]["Tooling_Breakdown"].ToString();
                if (toolbre == "True")
                {
                    checkBox5.Checked = true;
                }
                else
                {
                    checkBox5.Checked = false;
                }

                string flowchart = ds1.Tables[0].Rows[0]["Flowchart"].ToString();
                if (flowchart == "True")
                {
                    checkBox2.Checked = true;
                }
                else
                {
                    checkBox2.Checked = false; ;
                }

                string logistic = ds1.Tables[0].Rows[0]["Logistics"].ToString();
                if (logistic == "True")
                {
                    checkBox4.Checked = true;
                }
                else
                {
                    checkBox4.Checked = false; ;
                }

                string misc = ds1.Tables[0].Rows[0]["Misc_Information"].ToString();
                if (misc == "True")
                {
                    checkBox6.Checked = true;
                }
                else
                {
                    checkBox6.Checked = false; ;
                }


                textBox25.Text = ds1.Tables[0].Rows[0]["Misc_Information_text"].ToString();
                textBox26.Text = ds1.Tables[0].Rows[0]["Description"].ToString();
                //textBox27.Text = ds1.Tables[0].Rows[0]["Attachment"].ToString();                               
                textBox5.Text = ds1.Tables[0].Rows[0]["Project_Type"].ToString();
                textBox13.Text = ds1.Tables[0].Rows[0]["Demand_Material_ID"].ToString();
                textBox29.Text = ds1.Tables[0].Rows[0]["CompanyID"].ToString();
                textBox11.Text = ds1.Tables[0].Rows[0]["Buyer_Group"].ToString();
                textBox28.Text = ds1.Tables[0].Rows[0]["Buyer_org"].ToString();
            }
            else
            {
                MessageBox.Show("Part_Number为空");
            }

            //根据选中的Part_Number，读取dbo.Inquiry_Base表的一条Demand_Material_ID,再根据dbo.Demand_Material中相对应的ID，获得Demand_Material_ID的一条记录到各个文本框里
            //DataTable db= DBHelper.ExecuteQueryDT("select ID, Demand_ID, Material_ID, Material_Name, Aggregate_ID, Demand_Count, Measurement, Stock_ID, Factory_ID, DeliveryStartTime, DeliveryEndTime from dbo.Demand_Material where Demand_ID = '" + demandid + "' and Material_ID = '" + materialid + "'");
            //SqlConnection conn2 = new SqlConnection("server=116.196.99.18;database=MMBackup;uid=sa;pwd=b602!@#qwe");
            connString = ConfigurationManager.AppSettings["serverAddress"];
            SqlDataAdapter sda2 = new SqlDataAdapter("select Demand_ID, Aggregate_ID, Measurement, Stock_ID, Factory_ID, DeliveryStartTime, DeliveryEndTime from dbo.Demand_Material where ID=(select Demand_Material_ID from dbo.Inquiry_Base where Part_Number='" + comboBox1.Text + "')", connString);
            DataSet ds2 = new DataSet();
            sda2.Fill(ds2, "em2");
            int b1 = ds2.Tables[0].Rows.Count;
            if (b1 > 0)
            {
                textBox17.Text = ds2.Tables[0].Rows[0]["Demand_ID"].ToString();
                textBox18.Text = ds2.Tables[0].Rows[0]["Aggregate_ID"].ToString();
                textBox15.Text = ds2.Tables[0].Rows[0]["Measurement"].ToString();
                textBox23.Text = ds2.Tables[0].Rows[0]["Stock_ID"].ToString();
                textBox19.Text = ds2.Tables[0].Rows[0]["Factory_ID"].ToString();
                textBox16.Text = ds2.Tables[0].Rows[0]["DeliveryStartTime"].ToString();
                textBox20.Text = ds2.Tables[0].Rows[0]["DeliveryEndTime"].ToString();
            }
            else
            {
                MessageBox.Show("Part_Number为空");
            }

            //判断附件是否为空
            if (string.IsNullOrEmpty(filename))
            {
                //附件不存在，隐藏label5和button1
                button1.Visible = false;
                label12.Visible = false;
            }
            else
            {
                //附件存在，显示label5和button1
                button1.Visible = true;
                label12.Visible = true;
                label12.Text = filename;
            }
        }

        public static string GetFilePath(string path)
        {
            //浏览文件存放的路径
            FolderBrowserDialog dialog = new FolderBrowserDialog();
            dialog.Description = "请选择文件路径";
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                foldPath = dialog.SelectedPath;
            }
            return foldPath;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string filepath1 = GetFilePath(foldPath);
            attchmentfilename1 = filename;

            //从数据库里下载文件保存到本地
            SqlDataReader dr = null;
            connString = ConfigurationManager.AppSettings["serverAddress"];
            SqlConnection SqlConn6 = new SqlConnection(connString);
            SqlConn6.Open();
            string str4 = "select Attachment from dbo.Inquiry_Base where Part_Number = '" + comboBox1.Text + "'";
            SqlCommand SqlCmd4 = new SqlCommand(str4, SqlConn6);
            SqlCmd4.CommandType = CommandType.Text;

            dr = SqlCmd4.ExecuteReader();
            byte[] File = null;
            if (dr.Read())
            {
                File = (byte[])dr[0];
            }

            //判断文件是否已经存在，存在就抛出异常
            try
            {
                //判断文件夹是否存在
                if (Directory.Exists(filepath1))
                {
                    //文件夹下
                    fs = new FileStream(filepath1 + "\\" + attchmentfilename1, FileMode.CreateNew);
                }
                else
                {
                    //根目录下
                    fs = new FileStream(filepath1 + attchmentfilename1, FileMode.CreateNew);
                }
                BinaryWriter bw1 = new BinaryWriter(fs);
                bw1.Write(File, 0, File.Length);
                bw1.Close();
                fs.Close();
                SqlConn6.Close();
                MessageBox.Show("附件下载成功");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
    }
}
