﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Net.Mail;
using Aspose.Cells;
using Lib.SqlServerDAL.SourcingManage.ProcurementPlan;
using Lib.Model.SourcingManage.ProcurementPlan;
using Lib.Model.SourcingManage;
using Lib.Bll.SourcingManage.SourcingManagement;
using Lib.Bll.SourcingManage.SourceingRecord;
using WeifenLuo.WinFormsUI.Docking;
using System.IO;
using Lib.Model.SourcingManage.SourcingManagement;
using Lib.Model.SourcingManage.SourceingRecord;
using Lib.Common.CommonUtils;
using Lib.Model.FileManage;
using MMClient.CommonForms;
using Lib.Bll.FileManageBLL;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

namespace MMClient.SourcingManage.SourcingManagement
{
    public partial class VolvoMessage3Inbox_Context_Form : Form
    {
        string messageid;
        string status1;
        string attchmentfilename;
        string attchmentfilename1;
        public static string title;
        public static string supplierid;
        public static string supplierid1;
        public static string time;
        public static string context;
        public static string[] a;
        public static string replyto1;
        public static string startnumber;
        public static string userid;
        public static string userid1;
        public static string foldPath;
        FileStream fs;

        public VolvoMessage3Inbox_Context_Form()
        {
            InitializeComponent();
        }

        public VolvoMessage3Inbox_Context_Form(string[] var)
        {
            InitializeComponent();

            messageid = var[0];     //Message_ID
            textBox2.Text = var[2];  //Title
            userid = var[3];  //User_ID
            textBox3.Text = var[4];  //Supplier_ID
            textBox4.Text = var[5];   //Send_Time
            textBox6.Text = var[1];   //Part_Number
            status1 = var[6];         //Status
            textBox1.Text = var[7];    //Context
            attchmentfilename = var[8];   //AttachementFileName
            startnumber = var[9];  //Start_Number
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            ////读取dbo.Inquiry_Message表的Supplier_ID,Title,Send_Time到收件箱记录表
            //SqlConnection conn1 = new SqlConnection("server=116.196.99.18;database=MMBackup;uid=sa;pwd=b602!@#qwe");
            ////SqlDataAdapter sda = new SqlDataAdapter("select Context from dbo.Inquiry_Message", conn1);
            ////DataSet ds = new DataSet();
            ////sda.Fill(ds, "em1");
            ////dataGridView1.DataSource = ds.Tables["em1"];
            //conn1.Open();
            //SqlCommand cmd = new SqlCommand("select Context from dbo.Inquiry_Message", conn1);
            //SqlDataReader dr = cmd.ExecuteReader();
            //if (dr.Read())
            //{
            //    textBox1.Text = dr[0].ToString();
            //}
            //dr.Close();
            //cmd.Dispose();
            //conn1.Close();
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            
        }


        public static string GetFilePath(string path)
        {
            //浏览文件存放的路径
            FolderBrowserDialog dialog = new FolderBrowserDialog();
            dialog.Description = "请选择文件路径";
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                foldPath = dialog.SelectedPath;
            }
            return foldPath;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string filepath1 = GetFilePath(foldPath);
            attchmentfilename1 = attchmentfilename;

            //从数据库里下载文件保存到本地
            SqlDataReader dr = null;
            SqlConnection SqlConn6 = new SqlConnection("server=116.196.99.18;database=MMBackup;uid=sa;pwd=b602!@#qwe");
            SqlConn6.Open();
            string str4 = "select Attachment from dbo.Inquiry_Message where Message_ID = '" + messageid + "'";
            SqlCommand SqlCmd4 = new SqlCommand(str4, SqlConn6);
            SqlCmd4.CommandType = CommandType.Text;

            dr = SqlCmd4.ExecuteReader();
            byte[] File = null;
            if (dr.Read())
            {
                File = (byte[])dr[0];
            }

            //判断文件是否已经存在，存在就抛出异常
            try
            {
                //判断文件夹是否存在
                if (Directory.Exists(filepath1))
                {
                    //文件夹下
                    fs = new FileStream(filepath1 + "\\" + attchmentfilename1, FileMode.CreateNew);
                }
                else
                {
                    //根目录下
                    fs = new FileStream(filepath1 + attchmentfilename1, FileMode.CreateNew);
                }
                BinaryWriter bw1 = new BinaryWriter(fs);
                bw1.Write(File, 0, File.Length);
                bw1.Close();
                fs.Close();
                SqlConn6.Close();
                MessageBox.Show("附件下载成功");
            }
            catch (Exception ex)
            {
                MessageBox.Show("捕获异常："+ex);
            }   
        }    

        private void button3_Click(object sender, EventArgs e)
        {
            //邮件回复
            time = "发送时间："+ textBox4.Text;
            userid1 = "发件人：" + userid;
            title = "消息主题："+ textBox2.Text;
            context = "消息正文：" + "\r\n" + textBox1.Text ;
            supplierid1 = textBox3.Text;
            string partnumber = textBox6.Text;

            a = new string[] { time, userid1, title, context, messageid, partnumber, supplierid1, startnumber };
 
            VolvoMessage2_Form sub3 = new VolvoMessage2_Form(a);
            sub3.Show();
        }

        private void VolvoMessage3Inbox_Context_Form_Load(object sender, EventArgs e)
        {
            //判断附件是否为空
            if (string.IsNullOrEmpty(attchmentfilename))
            {
                //附件不存在，隐藏label5和button1
                button1.Visible = false;
                label5.Visible = false;
            }
            else
            {
                //附件存在，显示label5和button1
                button1.Visible = true;
                label5.Visible = true;
                label5.Text = attchmentfilename;
            }
        }
    }
}
