﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Net.Mail;
using Aspose.Cells;
using Lib.SqlServerDAL.SourcingManage.ProcurementPlan;
using Lib.Model.SourcingManage.ProcurementPlan;
using Lib.Model.SourcingManage;
using Lib.Bll.SourcingManage.SourcingManagement;
using Lib.Bll.SourcingManage.SourceingRecord;
using WeifenLuo.WinFormsUI.Docking;
using System.IO;
using Lib.Model.SourcingManage.SourcingManagement;
using Lib.Model.SourcingManage.SourceingRecord;
using Lib.Common.CommonUtils;
using Lib.Model.FileManage;
using MMClient.CommonForms;
using Lib.Bll.FileManageBLL;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

namespace MMClient.SourcingManage.SourcingManagement
{
    public partial class VolvoMessage4Outbox_Context_Form : Form
    {
        string messageid;
        string status1;
        string attchmentfilename;

        public VolvoMessage4Outbox_Context_Form()
        {
            InitializeComponent();
        }

        public VolvoMessage4Outbox_Context_Form(string[] var)
        {
            InitializeComponent();

            messageid = var[0];
            textBox2.Text = var[2];
            textBox3.Text = var[3];
            textBox4.Text = var[4];
            textBox6.Text = var[1];
            status1 = var[5];
            textBox1.Text = var[6];
            attchmentfilename = var[7];
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //浏览文件存放的路径
            FolderBrowserDialog dialog = new FolderBrowserDialog();
            dialog.Description = "请选择文件路径";
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                string foldPath = dialog.SelectedPath;
                this.textBox5.Text = foldPath;
                //MessageBox.Show("已选择文件夹:" + foldPath, "选择文件夹提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //从数据库里下载文件保存到本地
            SqlDataReader dr = null;
            SqlConnection SqlConn6 = new SqlConnection("server=116.196.99.18;database=MMBackup;uid=sa;pwd=b602!@#qwe");
            SqlConn6.Open();
            //string str4 = "select Attachment from dbo.Inquiry_Base where Part_Number = 11 ";
            string str4 = "select Attachment from dbo.Inquiry_Message where message_ID = '" + messageid + "'";
            SqlCommand SqlCmd4 = new SqlCommand(str4, SqlConn6);
            SqlCmd4.CommandType = CommandType.Text;
            //string filePath = @"C:\" + textBox7.Text + ".rar";
            string filePath = textBox5.Text;
            string fileName = attchmentfilename + ".rar";

            dr = SqlCmd4.ExecuteReader();
            byte[] File = null;
            if (dr.Read())
            {
                File = (byte[])dr[0];
            }
            //FileStream fs;
            //FileInfo fi = new System.IO.FileInfo(fileName);
            //fs = fi.OpenWrite();
            //fs.Write(File, 0, File.Length);
            //fs.Close();

            //FileStream fs = new FileStream("D:\\test1.rar", FileMode.CreateNew);
            FileStream fs = new FileStream(filePath + fileName, FileMode.CreateNew);
            BinaryWriter bw = new BinaryWriter(fs);
            bw.Write(File, 0, File.Length);
            bw.Close();
            fs.Close();
            SqlConn6.Close();
        }
    }
}
