﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Bll.SourcingManage.ProcurementPlan;

namespace MMClient.SourcingManage
{
    class ComboBoxFactory
    {
        ///test
        /// <summary>
        /// 从数据库加载数据至控件Items
        /// </summary>
        /// <param>控件名称</param>
        /// <returns>null</returns>
        public void addItemsToComboBox(ComboBox comboBox,string id,string idName, string itemName,string tableName)
        {



            DemandBLL DemandTool = new DemandBLL();
            List<string> ItemList = new List<string>();
            try 
            {
                ItemList = DemandTool.FindAddItems(id, idName, itemName, tableName);
                if (ItemList != null)
                {
                    if (ItemList.Count == 1)
                    {
                        comboBox.Text = ItemList.ElementAt(0);
                        return;
                    }
                    string str = "";
                    for (int n = 0; n < ItemList.Count; n++)
                    {
                        if (comboBox.Text.ToString().Equals(ItemList.ElementAt(n)))
                        {
                            str = comboBox.Text.ToString();
                            break;
                        }
                    }
                    comboBox.Items.Clear();
                    for (int n = 0; n < ItemList.Count; n++)
                    {
                        comboBox.Items.Add(ItemList.ElementAt(n));
                    }
                    comboBox.Text = str;
                    comboBox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                    comboBox.AutoCompleteSource = AutoCompleteSource.ListItems;
                }
            }
            catch(Exception e) 
            {
                MessageBox.Show(e.ToString());
            }   
        }
    }
}
