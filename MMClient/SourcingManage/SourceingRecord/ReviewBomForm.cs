﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Flybird.TreeDataGridView;
using WeifenLuo.WinFormsUI.Docking;

namespace MMClient.SourcingManage.SourceingRecord
{
    public partial class ReviewBomForm : DockContent
    {
        public ReviewBomForm()
        {
            InitializeComponent();
            load();
        }
        public void load()
        {
            
            //TreeDataGirdView DataGridView1 = DataGridView1 as TreeDataGirdView;
            TreeDataGridViewColumn col = new TreeDataGridViewColumn();
            DataGridViewTextBoxColumn col2 = new DataGridViewTextBoxColumn();
            col.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

            load2();

            for (int i = 1; i <= 17; i++)
            {
                //如果有子项
                if (i == 5)
                {
                    TreeDataGridViewCell ce = DataGridView1.Rows[i-1].Cells[0] as TreeDataGridViewCell;
                    setChildCell(DataGridView1, ce, 6, 7);
                }
                if (i == 8)
                {
                    TreeDataGridViewCell ce = DataGridView1.Rows[i-1].Cells[0] as TreeDataGridViewCell;
                    setChildCell(DataGridView1, ce, 9,11);
                }
                if (i == 12)
                {
                    TreeDataGridViewCell ce = DataGridView1.Rows[i-1].Cells[0] as TreeDataGridViewCell;
                    setChildCell(DataGridView1, ce, 13, 17);
                }
            }
        }

        public void setChildCell(TreeDataGirdView treeDGV, TreeDataGridViewCell ce, int c, int l)
        {
            TreeDataGridViewCell priCe = ce;
            for (int i = c; i <= l; i++)
            {
                TreeDataGridViewCell cel = treeDGV.Rows[i-1].Cells[0] as TreeDataGridViewCell;
                ce.fcSetChildCell(cel);
            }
        }

        public void load2()
        {
            for (int i = 0; i < 17; i++)
            {
                DataGridView1.Rows.Add();
            }
            for (int i = 0; i < 17; i++)
            {
                DataGridView1.Rows[i].Cells[3].Value = "个";
                DataGridView1.Rows[i].Cells[4].Value = "采购";
                DataGridView1.Rows[i].Cells[5].Value = "100";
                DataGridView1.Rows[i].Cells[6].Value = "1";
                DataGridView1.Rows[i].Cells[7].Value = "0";
                DataGridView1.Rows[i].Cells[9].Value = "0";
                DataGridView1.Rows[i].Cells[10].Value = "2016-01-01";
                DataGridView1.Rows[i].Cells[11].Value = "2199-01-01";
            }
            DataGridView1.Rows[0].Cells[0].Value = "21700001";
            DataGridView1.Rows[1].Cells[0].Value = "21700002";
            DataGridView1.Rows[2].Cells[0].Value = "21700003";
            DataGridView1.Rows[3].Cells[0].Value = "21700004";

            DataGridView1.Rows[4].Cells[0].Value = "12300001";
            DataGridView1.Rows[5].Cells[0].Value = "12500012";
            DataGridView1.Rows[6].Cells[0].Value = "12600015";

            DataGridView1.Rows[7].Cells[0].Value = "14500001";
            DataGridView1.Rows[8].Cells[0].Value = "15400052";
            DataGridView1.Rows[9].Cells[0].Value = "15400073";
            DataGridView1.Rows[10].Cells[0].Value = "15400121";

            DataGridView1.Rows[11].Cells[0].Value = "15700001";
            DataGridView1.Rows[12].Cells[0].Value = "16500001";
            DataGridView1.Rows[13].Cells[0].Value = "16500003";
            DataGridView1.Rows[14].Cells[0].Value = "16500004";
            DataGridView1.Rows[15].Cells[0].Value = "16500006";
            DataGridView1.Rows[16].Cells[0].Value = "16500007";

            DataGridView1.Rows[0].Cells[1].Value = "车架";
            DataGridView1.Rows[1].Cells[1].Value = "车锁";
            DataGridView1.Rows[2].Cells[1].Value = "车座";
            DataGridView1.Rows[3].Cells[1].Value = "支架";

            DataGridView1.Rows[4].Cells[1].Value = "车把系统";
            DataGridView1.Rows[5].Cells[1].Value = "车把";
            DataGridView1.Rows[6].Cells[1].Value = "车刹";

            DataGridView1.Rows[7].Cells[1].Value = "车轮系统";
            DataGridView1.Rows[8].Cells[1].Value = "车轮";
            DataGridView1.Rows[9].Cells[1].Value = "前轴";
            DataGridView1.Rows[10].Cells[1].Value = "后轴";

            DataGridView1.Rows[11].Cells[1].Value = "脚蹬系统";
            DataGridView1.Rows[12].Cells[1].Value = "脚蹬";
            DataGridView1.Rows[13].Cells[1].Value = "中轴";
            DataGridView1.Rows[14].Cells[1].Value = "链条";
            DataGridView1.Rows[15].Cells[1].Value = "档板";
            DataGridView1.Rows[16].Cells[1].Value = "齿轮";

            DataGridView1.Rows[1].Cells[8].Value = "21700016";
            DataGridView1.Rows[4].Cells[8].Value = "12500123";

        }
}
}
