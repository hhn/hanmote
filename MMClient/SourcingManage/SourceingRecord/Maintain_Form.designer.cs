﻿namespace MMClient.SourcingManage.SourceingRecord
{
    partial class Maintain_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.wlbh_tb = new System.Windows.Forms.TextBox();
            this.cgxxjl_tb = new System.Windows.Forms.TextBox();
            this.gc_tb = new System.Windows.Forms.TextBox();
            this.cgzz_tb = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.gys_cmb = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.maintain_bt = new System.Windows.Forms.Button();
            this.create_bt = new System.Windows.Forms.Button();
            this.refresh_bt = new System.Windows.Forms.Button();
            this.query_bt = new System.Windows.Forms.Button();
            this.next_bt = new System.Windows.Forms.Button();
            this.close_bt = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.label32 = new System.Windows.Forms.Label();
            this.textBox14 = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.label28 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.textBox12 = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.dateTimePicker3 = new System.Windows.Forms.DateTimePicker();
            this.label59 = new System.Windows.Forms.Label();
            this.textBox58 = new System.Windows.Forms.TextBox();
            this.label46 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.panel8 = new System.Windows.Forms.Panel();
            this.textBox46 = new System.Windows.Forms.TextBox();
            this.textBox47 = new System.Windows.Forms.TextBox();
            this.textBox48 = new System.Windows.Forms.TextBox();
            this.textBox49 = new System.Windows.Forms.TextBox();
            this.textBox50 = new System.Windows.Forms.TextBox();
            this.textBox51 = new System.Windows.Forms.TextBox();
            this.checkBox7 = new System.Windows.Forms.CheckBox();
            this.checkBox8 = new System.Windows.Forms.CheckBox();
            this.textBox52 = new System.Windows.Forms.TextBox();
            this.textBox53 = new System.Windows.Forms.TextBox();
            this.textBox54 = new System.Windows.Forms.TextBox();
            this.textBox55 = new System.Windows.Forms.TextBox();
            this.textBox56 = new System.Windows.Forms.TextBox();
            this.textBox57 = new System.Windows.Forms.TextBox();
            this.textBox30 = new System.Windows.Forms.TextBox();
            this.textBox31 = new System.Windows.Forms.TextBox();
            this.textBox32 = new System.Windows.Forms.TextBox();
            this.textBox33 = new System.Windows.Forms.TextBox();
            this.textBox34 = new System.Windows.Forms.TextBox();
            this.textBox35 = new System.Windows.Forms.TextBox();
            this.checkBox4 = new System.Windows.Forms.CheckBox();
            this.checkBox5 = new System.Windows.Forms.CheckBox();
            this.textBox38 = new System.Windows.Forms.TextBox();
            this.textBox39 = new System.Windows.Forms.TextBox();
            this.textBox40 = new System.Windows.Forms.TextBox();
            this.textBox41 = new System.Windows.Forms.TextBox();
            this.textBox44 = new System.Windows.Forms.TextBox();
            this.textBox45 = new System.Windows.Forms.TextBox();
            this.label47 = new System.Windows.Forms.Label();
            this.textBox127 = new System.Windows.Forms.TextBox();
            this.textBox130 = new System.Windows.Forms.TextBox();
            this.label49 = new System.Windows.Forms.Label();
            this.textBox131 = new System.Windows.Forms.TextBox();
            this.textBox134 = new System.Windows.Forms.TextBox();
            this.label56 = new System.Windows.Forms.Label();
            this.textBox135 = new System.Windows.Forms.TextBox();
            this.textBox138 = new System.Windows.Forms.TextBox();
            this.checkBox6 = new System.Windows.Forms.CheckBox();
            this.checkBox9 = new System.Windows.Forms.CheckBox();
            this.label57 = new System.Windows.Forms.Label();
            this.textBox28 = new System.Windows.Forms.TextBox();
            this.textBox29 = new System.Windows.Forms.TextBox();
            this.textBox36 = new System.Windows.Forms.TextBox();
            this.label61 = new System.Windows.Forms.Label();
            this.label62 = new System.Windows.Forms.Label();
            this.textBox37 = new System.Windows.Forms.TextBox();
            this.textBox42 = new System.Windows.Forms.TextBox();
            this.textBox43 = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.label40 = new System.Windows.Forms.Label();
            this.textBox20 = new System.Windows.Forms.TextBox();
            this.textBox22 = new System.Windows.Forms.TextBox();
            this.label39 = new System.Windows.Forms.Label();
            this.textBox26 = new System.Windows.Forms.TextBox();
            this.textBox27 = new System.Windows.Forms.TextBox();
            this.textBox19 = new System.Windows.Forms.TextBox();
            this.textBox18 = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.textBox16 = new System.Windows.Forms.TextBox();
            this.label43 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.label33 = new System.Windows.Forms.Label();
            this.textBox13 = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.dateTimePicker4 = new System.Windows.Forms.DateTimePicker();
            this.label38 = new System.Windows.Forms.Label();
            this.textBox15 = new System.Windows.Forms.TextBox();
            this.textBox17 = new System.Windows.Forms.TextBox();
            this.label41 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.textBox21 = new System.Windows.Forms.TextBox();
            this.label48 = new System.Windows.Forms.Label();
            this.textBox23 = new System.Windows.Forms.TextBox();
            this.textBox24 = new System.Windows.Forms.TextBox();
            this.textBox25 = new System.Windows.Forms.TextBox();
            this.label50 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.panel3 = new System.Windows.Forms.Panel();
            this.button12 = new System.Windows.Forms.Button();
            this.textBox71 = new System.Windows.Forms.TextBox();
            this.checkBox15 = new System.Windows.Forms.CheckBox();
            this.textBox72 = new System.Windows.Forms.TextBox();
            this.textBox73 = new System.Windows.Forms.TextBox();
            this.button11 = new System.Windows.Forms.Button();
            this.textBox68 = new System.Windows.Forms.TextBox();
            this.checkBox13 = new System.Windows.Forms.CheckBox();
            this.textBox69 = new System.Windows.Forms.TextBox();
            this.textBox70 = new System.Windows.Forms.TextBox();
            this.button6 = new System.Windows.Forms.Button();
            this.label60 = new System.Windows.Forms.Label();
            this.textBox59 = new System.Windows.Forms.TextBox();
            this.checkBox10 = new System.Windows.Forms.CheckBox();
            this.label63 = new System.Windows.Forms.Label();
            this.textBox60 = new System.Windows.Forms.TextBox();
            this.textBox61 = new System.Windows.Forms.TextBox();
            this.label67 = new System.Windows.Forms.Label();
            this.button7 = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.button10 = new System.Windows.Forms.Button();
            this.textBox65 = new System.Windows.Forms.TextBox();
            this.checkBox12 = new System.Windows.Forms.CheckBox();
            this.textBox66 = new System.Windows.Forms.TextBox();
            this.textBox67 = new System.Windows.Forms.TextBox();
            this.button9 = new System.Windows.Forms.Button();
            this.textBox62 = new System.Windows.Forms.TextBox();
            this.checkBox11 = new System.Windows.Forms.CheckBox();
            this.textBox63 = new System.Windows.Forms.TextBox();
            this.textBox64 = new System.Windows.Forms.TextBox();
            this.button5 = new System.Windows.Forms.Button();
            this.label64 = new System.Windows.Forms.Label();
            this.textBox87 = new System.Windows.Forms.TextBox();
            this.checkBox14 = new System.Windows.Forms.CheckBox();
            this.label65 = new System.Windows.Forms.Label();
            this.textBox89 = new System.Windows.Forms.TextBox();
            this.textBox91 = new System.Windows.Forms.TextBox();
            this.label66 = new System.Windows.Forms.Label();
            this.button8 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.label69 = new System.Windows.Forms.Label();
            this.button4 = new System.Windows.Forms.Button();
            this.label68 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.panel8.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // wlbh_tb
            // 
            this.wlbh_tb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.wlbh_tb.Location = new System.Drawing.Point(100, 180);
            this.wlbh_tb.Name = "wlbh_tb";
            this.wlbh_tb.Size = new System.Drawing.Size(200, 23);
            this.wlbh_tb.TabIndex = 182;
            this.wlbh_tb.Text = "K00000407";
            // 
            // cgxxjl_tb
            // 
            this.cgxxjl_tb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.cgxxjl_tb.Location = new System.Drawing.Point(100, 108);
            this.cgxxjl_tb.Name = "cgxxjl_tb";
            this.cgxxjl_tb.Size = new System.Drawing.Size(200, 23);
            this.cgxxjl_tb.TabIndex = 181;
            this.cgxxjl_tb.Text = "530000000004";
            // 
            // gc_tb
            // 
            this.gc_tb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.gc_tb.Location = new System.Drawing.Point(434, 255);
            this.gc_tb.Name = "gc_tb";
            this.gc_tb.Size = new System.Drawing.Size(100, 23);
            this.gc_tb.TabIndex = 180;
            this.gc_tb.Text = "2001";
            // 
            // cgzz_tb
            // 
            this.cgzz_tb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.cgzz_tb.Location = new System.Drawing.Point(100, 217);
            this.cgzz_tb.Name = "cgzz_tb";
            this.cgzz_tb.Size = new System.Drawing.Size(100, 23);
            this.cgzz_tb.TabIndex = 179;
            this.cgzz_tb.Text = "100";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("宋体", 9F);
            this.label7.Location = new System.Drawing.Point(12, 112);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(77, 12);
            this.label7.TabIndex = 176;
            this.label7.Text = "采购信息记录";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("宋体", 9F);
            this.label4.Location = new System.Drawing.Point(346, 259);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(29, 12);
            this.label4.TabIndex = 175;
            this.label4.Text = "工厂";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("宋体", 9F);
            this.label5.Location = new System.Drawing.Point(12, 222);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 12);
            this.label5.TabIndex = 174;
            this.label5.Text = "物料组";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("宋体", 9F);
            this.label3.Location = new System.Drawing.Point(12, 185);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 12);
            this.label3.TabIndex = 173;
            this.label3.Text = "物料编号";
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("宋体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label1.Location = new System.Drawing.Point(8, 70);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(129, 19);
            this.label1.TabIndex = 172;
            this.label1.Text = "寻源信息记录";
            // 
            // gys_cmb
            // 
            this.gys_cmb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.gys_cmb.FormattingEnabled = true;
            this.gys_cmb.ItemHeight = 14;
            this.gys_cmb.Location = new System.Drawing.Point(100, 144);
            this.gys_cmb.Name = "gys_cmb";
            this.gys_cmb.Size = new System.Drawing.Size(200, 22);
            this.gys_cmb.TabIndex = 171;
            this.gys_cmb.Text = "2000002";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("宋体", 9F);
            this.label2.Location = new System.Drawing.Point(12, 149);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 12);
            this.label2.TabIndex = 170;
            this.label2.Text = "供应商";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Gainsboro;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.maintain_bt);
            this.panel1.Controls.Add(this.create_bt);
            this.panel1.Controls.Add(this.refresh_bt);
            this.panel1.Controls.Add(this.query_bt);
            this.panel1.Controls.Add(this.next_bt);
            this.panel1.Controls.Add(this.close_bt);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(768, 42);
            this.panel1.TabIndex = 169;
            // 
            // maintain_bt
            // 
            this.maintain_bt.Location = new System.Drawing.Point(331, 5);
            this.maintain_bt.Name = "maintain_bt";
            this.maintain_bt.Size = new System.Drawing.Size(60, 30);
            this.maintain_bt.TabIndex = 82;
            this.maintain_bt.Text = "维护";
            this.maintain_bt.UseVisualStyleBackColor = true;
            // 
            // create_bt
            // 
            this.create_bt.Location = new System.Drawing.Point(254, 5);
            this.create_bt.Name = "create_bt";
            this.create_bt.Size = new System.Drawing.Size(60, 30);
            this.create_bt.TabIndex = 81;
            this.create_bt.Text = "新建";
            this.create_bt.UseVisualStyleBackColor = true;
            // 
            // refresh_bt
            // 
            this.refresh_bt.Location = new System.Drawing.Point(407, 5);
            this.refresh_bt.Name = "refresh_bt";
            this.refresh_bt.Size = new System.Drawing.Size(60, 30);
            this.refresh_bt.TabIndex = 80;
            this.refresh_bt.Text = "刷新";
            this.refresh_bt.UseVisualStyleBackColor = true;
            // 
            // query_bt
            // 
            this.query_bt.Location = new System.Drawing.Point(175, 5);
            this.query_bt.Name = "query_bt";
            this.query_bt.Size = new System.Drawing.Size(60, 30);
            this.query_bt.TabIndex = 79;
            this.query_bt.Text = "查询";
            this.query_bt.UseVisualStyleBackColor = true;
            // 
            // next_bt
            // 
            this.next_bt.Location = new System.Drawing.Point(96, 5);
            this.next_bt.Name = "next_bt";
            this.next_bt.Size = new System.Drawing.Size(60, 30);
            this.next_bt.TabIndex = 76;
            this.next_bt.Text = "下一步";
            this.next_bt.UseVisualStyleBackColor = true;
            // 
            // close_bt
            // 
            this.close_bt.Location = new System.Drawing.Point(16, 5);
            this.close_bt.Name = "close_bt";
            this.close_bt.Size = new System.Drawing.Size(60, 30);
            this.close_bt.TabIndex = 75;
            this.close_bt.Text = "关闭";
            this.close_bt.UseVisualStyleBackColor = true;
            // 
            // textBox1
            // 
            this.textBox1.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox1.Location = new System.Drawing.Point(100, 255);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 23);
            this.textBox1.TabIndex = 184;
            this.textBox1.Text = "2000";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("宋体", 9F);
            this.label6.Location = new System.Drawing.Point(12, 260);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 12);
            this.label6.TabIndex = 183;
            this.label6.Text = "采购组织";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("宋体", 9F);
            this.label8.Location = new System.Drawing.Point(346, 147);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(173, 12);
            this.label8.TabIndex = 185;
            this.label8.Text = "广东风华高新科技股份有限公司";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("宋体", 9F);
            this.label9.Location = new System.Drawing.Point(346, 185);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(77, 12);
            this.label9.TabIndex = 186;
            this.label9.Text = "贴片电容4N7F";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("宋体", 9F);
            this.label10.Location = new System.Drawing.Point(346, 220);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(89, 12);
            this.label10.TabIndex = 187;
            this.label10.Text = "原材料（临时）";
            // 
            // comboBox1
            // 
            this.comboBox1.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.ItemHeight = 14;
            this.comboBox1.Location = new System.Drawing.Point(571, 256);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(150, 22);
            this.comboBox1.TabIndex = 188;
            this.comboBox1.Text = "标准采购类型";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Location = new System.Drawing.Point(14, 308);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(742, 690);
            this.tabControl1.TabIndex = 189;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage1.Controls.Add(this.checkBox2);
            this.tabPage1.Controls.Add(this.label32);
            this.tabPage1.Controls.Add(this.textBox14);
            this.tabPage1.Controls.Add(this.label29);
            this.tabPage1.Controls.Add(this.label30);
            this.tabPage1.Controls.Add(this.label31);
            this.tabPage1.Controls.Add(this.dateTimePicker2);
            this.tabPage1.Controls.Add(this.dateTimePicker1);
            this.tabPage1.Controls.Add(this.checkBox1);
            this.tabPage1.Controls.Add(this.label28);
            this.tabPage1.Controls.Add(this.label20);
            this.tabPage1.Controls.Add(this.label26);
            this.tabPage1.Controls.Add(this.label27);
            this.tabPage1.Controls.Add(this.textBox9);
            this.tabPage1.Controls.Add(this.textBox10);
            this.tabPage1.Controls.Add(this.textBox11);
            this.tabPage1.Controls.Add(this.textBox12);
            this.tabPage1.Controls.Add(this.label21);
            this.tabPage1.Controls.Add(this.label22);
            this.tabPage1.Controls.Add(this.label23);
            this.tabPage1.Controls.Add(this.label24);
            this.tabPage1.Controls.Add(this.label25);
            this.tabPage1.Controls.Add(this.textBox7);
            this.tabPage1.Controls.Add(this.label18);
            this.tabPage1.Controls.Add(this.textBox8);
            this.tabPage1.Controls.Add(this.label19);
            this.tabPage1.Controls.Add(this.textBox6);
            this.tabPage1.Controls.Add(this.label17);
            this.tabPage1.Controls.Add(this.textBox2);
            this.tabPage1.Controls.Add(this.label11);
            this.tabPage1.Controls.Add(this.textBox3);
            this.tabPage1.Controls.Add(this.textBox4);
            this.tabPage1.Controls.Add(this.textBox5);
            this.tabPage1.Controls.Add(this.label12);
            this.tabPage1.Controls.Add(this.label13);
            this.tabPage1.Controls.Add(this.label14);
            this.tabPage1.Controls.Add(this.label15);
            this.tabPage1.Controls.Add(this.label16);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(734, 664);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "一般数据";
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Location = new System.Drawing.Point(125, 398);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(15, 14);
            this.checkBox2.TabIndex = 226;
            this.checkBox2.UseVisualStyleBackColor = true;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("宋体", 9F);
            this.label32.Location = new System.Drawing.Point(145, 398);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(41, 12);
            this.label32.TabIndex = 225;
            this.label32.Text = "不活动";
            // 
            // textBox14
            // 
            this.textBox14.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox14.Location = new System.Drawing.Point(110, 357);
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new System.Drawing.Size(100, 23);
            this.textBox14.TabIndex = 224;
            this.textBox14.Text = "EA";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(22, 361);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(53, 12);
            this.label29.TabIndex = 223;
            this.label29.Text = "订单单位";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label30.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label30.Location = new System.Drawing.Point(18, 319);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(127, 14);
            this.label30.TabIndex = 222;
            this.label30.Text = "订单单位（采购）";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("宋体", 9F);
            this.label31.Location = new System.Drawing.Point(22, 398);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(89, 12);
            this.label31.TabIndex = 221;
            this.label31.Text = "可变的订单单位";
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.dateTimePicker2.Location = new System.Drawing.Point(483, 281);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(200, 23);
            this.dateTimePicker2.TabIndex = 220;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.dateTimePicker1.Location = new System.Drawing.Point(483, 243);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(200, 23);
            this.dateTimePicker1.TabIndex = 219;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(403, 320);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(15, 14);
            this.checkBox1.TabIndex = 218;
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("宋体", 9F);
            this.label28.Location = new System.Drawing.Point(423, 320);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(65, 12);
            this.label28.TabIndex = 217;
            this.label28.Text = "常规供应商";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(395, 247);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(89, 12);
            this.label20.TabIndex = 214;
            this.label20.Text = "可使用起始时间";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label26.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label26.Location = new System.Drawing.Point(391, 205);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(67, 14);
            this.label26.TabIndex = 213;
            this.label26.Text = "供应选择";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("宋体", 9F);
            this.label27.Location = new System.Drawing.Point(395, 284);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(89, 12);
            this.label27.TabIndex = 212;
            this.label27.Text = "可使用终止时间";
            // 
            // textBox9
            // 
            this.textBox9.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox9.Location = new System.Drawing.Point(483, 94);
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new System.Drawing.Size(100, 23);
            this.textBox9.TabIndex = 211;
            this.textBox9.Text = "190";
            // 
            // textBox10
            // 
            this.textBox10.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox10.Location = new System.Drawing.Point(483, 130);
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new System.Drawing.Size(200, 23);
            this.textBox10.TabIndex = 209;
            this.textBox10.Text = "190-0000009876";
            // 
            // textBox11
            // 
            this.textBox11.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox11.Location = new System.Drawing.Point(483, 58);
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new System.Drawing.Size(100, 23);
            this.textBox11.TabIndex = 208;
            this.textBox11.Text = "CN";
            // 
            // textBox12
            // 
            this.textBox12.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox12.Location = new System.Drawing.Point(483, 167);
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new System.Drawing.Size(200, 23);
            this.textBox12.TabIndex = 207;
            this.textBox12.Text = "广东朝阳工业有限公司";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(395, 62);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(53, 12);
            this.label21.TabIndex = 206;
            this.label21.Text = "产地国家";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("宋体", 9F);
            this.label22.Location = new System.Drawing.Point(395, 172);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(53, 12);
            this.label22.TabIndex = 205;
            this.label22.Text = "制造厂商";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("宋体", 9F);
            this.label23.Location = new System.Drawing.Point(395, 135);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(29, 12);
            this.label23.TabIndex = 204;
            this.label23.Text = "编号";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label24.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label24.Location = new System.Drawing.Point(391, 20);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(82, 14);
            this.label24.TabIndex = 203;
            this.label24.Text = "原产地数据";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("宋体", 9F);
            this.label25.Location = new System.Drawing.Point(395, 99);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(29, 12);
            this.label25.TabIndex = 202;
            this.label25.Text = "地区";
            // 
            // textBox7
            // 
            this.textBox7.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox7.Location = new System.Drawing.Point(110, 281);
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(200, 23);
            this.textBox7.TabIndex = 201;
            this.textBox7.Text = "2000053";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("宋体", 9F);
            this.label18.Location = new System.Drawing.Point(22, 286);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(77, 12);
            this.label18.TabIndex = 200;
            this.label18.Text = "前面的供应商";
            // 
            // textBox8
            // 
            this.textBox8.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox8.Location = new System.Drawing.Point(110, 243);
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(200, 23);
            this.textBox8.TabIndex = 199;
            this.textBox8.Text = "标准退货协议";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("宋体", 9F);
            this.label19.Location = new System.Drawing.Point(22, 248);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(53, 12);
            this.label19.TabIndex = 198;
            this.label19.Text = "退货协议";
            // 
            // textBox6
            // 
            this.textBox6.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox6.Location = new System.Drawing.Point(110, 94);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(200, 23);
            this.textBox6.TabIndex = 197;
            this.textBox6.Text = "K00000407";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(216, 62);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(29, 12);
            this.label17.TabIndex = 196;
            this.label17.Text = "天内";
            // 
            // textBox2
            // 
            this.textBox2.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox2.Location = new System.Drawing.Point(110, 205);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(200, 23);
            this.textBox2.TabIndex = 195;
            this.textBox2.Text = "15892879890";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("宋体", 9F);
            this.label11.Location = new System.Drawing.Point(22, 210);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(29, 12);
            this.label11.TabIndex = 194;
            this.label11.Text = "电话";
            // 
            // textBox3
            // 
            this.textBox3.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox3.Location = new System.Drawing.Point(110, 130);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(200, 23);
            this.textBox3.TabIndex = 193;
            this.textBox3.Text = "100";
            // 
            // textBox4
            // 
            this.textBox4.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox4.Location = new System.Drawing.Point(110, 58);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(100, 23);
            this.textBox4.TabIndex = 192;
            this.textBox4.Text = "15";
            // 
            // textBox5
            // 
            this.textBox5.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox5.Location = new System.Drawing.Point(110, 167);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(200, 23);
            this.textBox5.TabIndex = 191;
            this.textBox5.Text = "丁宁";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(22, 62);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(53, 12);
            this.label12.TabIndex = 190;
            this.label12.Text = "催付天数";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("宋体", 9F);
            this.label13.Location = new System.Drawing.Point(22, 172);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(41, 12);
            this.label13.TabIndex = 189;
            this.label13.Text = "销售员";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("宋体", 9F);
            this.label14.Location = new System.Drawing.Point(22, 135);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(77, 12);
            this.label14.TabIndex = 188;
            this.label14.Text = "供应商物料组";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label15.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label15.Location = new System.Drawing.Point(18, 20);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(82, 14);
            this.label15.TabIndex = 187;
            this.label15.Text = "供应商数据";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("宋体", 9F);
            this.label16.Location = new System.Drawing.Point(22, 99);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(89, 12);
            this.label16.TabIndex = 185;
            this.label16.Text = "供应商物料编号";
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage2.Controls.Add(this.dateTimePicker3);
            this.tabPage2.Controls.Add(this.label59);
            this.tabPage2.Controls.Add(this.textBox58);
            this.tabPage2.Controls.Add(this.label46);
            this.tabPage2.Controls.Add(this.label58);
            this.tabPage2.Controls.Add(this.panel8);
            this.tabPage2.Controls.Add(this.button2);
            this.tabPage2.Controls.Add(this.button1);
            this.tabPage2.Controls.Add(this.label40);
            this.tabPage2.Controls.Add(this.textBox20);
            this.tabPage2.Controls.Add(this.textBox22);
            this.tabPage2.Controls.Add(this.label39);
            this.tabPage2.Controls.Add(this.textBox26);
            this.tabPage2.Controls.Add(this.textBox27);
            this.tabPage2.Controls.Add(this.textBox19);
            this.tabPage2.Controls.Add(this.textBox18);
            this.tabPage2.Controls.Add(this.label37);
            this.tabPage2.Controls.Add(this.textBox16);
            this.tabPage2.Controls.Add(this.label43);
            this.tabPage2.Controls.Add(this.label42);
            this.tabPage2.Controls.Add(this.label44);
            this.tabPage2.Controls.Add(this.label55);
            this.tabPage2.Controls.Add(this.checkBox3);
            this.tabPage2.Controls.Add(this.label33);
            this.tabPage2.Controls.Add(this.textBox13);
            this.tabPage2.Controls.Add(this.label34);
            this.tabPage2.Controls.Add(this.label35);
            this.tabPage2.Controls.Add(this.label36);
            this.tabPage2.Controls.Add(this.dateTimePicker4);
            this.tabPage2.Controls.Add(this.label38);
            this.tabPage2.Controls.Add(this.textBox15);
            this.tabPage2.Controls.Add(this.textBox17);
            this.tabPage2.Controls.Add(this.label41);
            this.tabPage2.Controls.Add(this.label45);
            this.tabPage2.Controls.Add(this.textBox21);
            this.tabPage2.Controls.Add(this.label48);
            this.tabPage2.Controls.Add(this.textBox23);
            this.tabPage2.Controls.Add(this.textBox24);
            this.tabPage2.Controls.Add(this.textBox25);
            this.tabPage2.Controls.Add(this.label50);
            this.tabPage2.Controls.Add(this.label51);
            this.tabPage2.Controls.Add(this.label52);
            this.tabPage2.Controls.Add(this.label53);
            this.tabPage2.Controls.Add(this.label54);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(734, 664);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "采购组织数据";
            // 
            // dateTimePicker3
            // 
            this.dateTimePicker3.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.dateTimePicker3.Location = new System.Drawing.Point(483, 602);
            this.dateTimePicker3.Name = "dateTimePicker3";
            this.dateTimePicker3.Size = new System.Drawing.Size(200, 23);
            this.dateTimePicker3.TabIndex = 286;
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Location = new System.Drawing.Point(395, 606);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(65, 12);
            this.label59.TabIndex = 285;
            this.label59.Text = "报价生效从";
            // 
            // textBox58
            // 
            this.textBox58.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox58.Location = new System.Drawing.Point(113, 600);
            this.textBox58.Name = "textBox58";
            this.textBox58.Size = new System.Drawing.Size(100, 23);
            this.textBox58.TabIndex = 284;
            this.textBox58.Text = "89050";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(25, 604);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(29, 12);
            this.label46.TabIndex = 283;
            this.label46.Text = "报价";
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label58.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label58.Location = new System.Drawing.Point(21, 562);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(37, 14);
            this.label58.TabIndex = 282;
            this.label58.Text = "参考";
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.textBox46);
            this.panel8.Controls.Add(this.textBox47);
            this.panel8.Controls.Add(this.textBox48);
            this.panel8.Controls.Add(this.textBox49);
            this.panel8.Controls.Add(this.textBox50);
            this.panel8.Controls.Add(this.textBox51);
            this.panel8.Controls.Add(this.checkBox7);
            this.panel8.Controls.Add(this.checkBox8);
            this.panel8.Controls.Add(this.textBox52);
            this.panel8.Controls.Add(this.textBox53);
            this.panel8.Controls.Add(this.textBox54);
            this.panel8.Controls.Add(this.textBox55);
            this.panel8.Controls.Add(this.textBox56);
            this.panel8.Controls.Add(this.textBox57);
            this.panel8.Controls.Add(this.textBox30);
            this.panel8.Controls.Add(this.textBox31);
            this.panel8.Controls.Add(this.textBox32);
            this.panel8.Controls.Add(this.textBox33);
            this.panel8.Controls.Add(this.textBox34);
            this.panel8.Controls.Add(this.textBox35);
            this.panel8.Controls.Add(this.checkBox4);
            this.panel8.Controls.Add(this.checkBox5);
            this.panel8.Controls.Add(this.textBox38);
            this.panel8.Controls.Add(this.textBox39);
            this.panel8.Controls.Add(this.textBox40);
            this.panel8.Controls.Add(this.textBox41);
            this.panel8.Controls.Add(this.textBox44);
            this.panel8.Controls.Add(this.textBox45);
            this.panel8.Controls.Add(this.label47);
            this.panel8.Controls.Add(this.textBox127);
            this.panel8.Controls.Add(this.textBox130);
            this.panel8.Controls.Add(this.label49);
            this.panel8.Controls.Add(this.textBox131);
            this.panel8.Controls.Add(this.textBox134);
            this.panel8.Controls.Add(this.label56);
            this.panel8.Controls.Add(this.textBox135);
            this.panel8.Controls.Add(this.textBox138);
            this.panel8.Controls.Add(this.checkBox6);
            this.panel8.Controls.Add(this.checkBox9);
            this.panel8.Controls.Add(this.label57);
            this.panel8.Controls.Add(this.textBox28);
            this.panel8.Controls.Add(this.textBox29);
            this.panel8.Controls.Add(this.textBox36);
            this.panel8.Controls.Add(this.label61);
            this.panel8.Controls.Add(this.label62);
            this.panel8.Controls.Add(this.textBox37);
            this.panel8.Controls.Add(this.textBox42);
            this.panel8.Controls.Add(this.textBox43);
            this.panel8.Location = new System.Drawing.Point(24, 352);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(659, 197);
            this.panel8.TabIndex = 281;
            // 
            // textBox46
            // 
            this.textBox46.BackColor = System.Drawing.SystemColors.Window;
            this.textBox46.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox46.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox46.Location = new System.Drawing.Point(365, 136);
            this.textBox46.Name = "textBox46";
            this.textBox46.Size = new System.Drawing.Size(80, 25);
            this.textBox46.TabIndex = 163;
            this.textBox46.Text = "——";
            this.textBox46.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox47
            // 
            this.textBox47.BackColor = System.Drawing.SystemColors.Window;
            this.textBox47.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox47.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox47.Location = new System.Drawing.Point(365, 160);
            this.textBox47.Name = "textBox47";
            this.textBox47.Size = new System.Drawing.Size(80, 25);
            this.textBox47.TabIndex = 164;
            this.textBox47.Text = "——";
            this.textBox47.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox48
            // 
            this.textBox48.BackColor = System.Drawing.SystemColors.Window;
            this.textBox48.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox48.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox48.Location = new System.Drawing.Point(301, 136);
            this.textBox48.Name = "textBox48";
            this.textBox48.Size = new System.Drawing.Size(65, 25);
            this.textBox48.TabIndex = 161;
            this.textBox48.Text = "%";
            this.textBox48.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox49
            // 
            this.textBox49.BackColor = System.Drawing.SystemColors.Window;
            this.textBox49.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox49.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox49.Location = new System.Drawing.Point(301, 160);
            this.textBox49.Name = "textBox49";
            this.textBox49.Size = new System.Drawing.Size(65, 25);
            this.textBox49.TabIndex = 162;
            this.textBox49.Text = "CNY";
            this.textBox49.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox50
            // 
            this.textBox50.BackColor = System.Drawing.SystemColors.Window;
            this.textBox50.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox50.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox50.Location = new System.Drawing.Point(222, 136);
            this.textBox50.Name = "textBox50";
            this.textBox50.Size = new System.Drawing.Size(80, 25);
            this.textBox50.TabIndex = 159;
            this.textBox50.Text = "17";
            this.textBox50.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox51
            // 
            this.textBox51.BackColor = System.Drawing.SystemColors.Window;
            this.textBox51.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox51.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox51.Location = new System.Drawing.Point(222, 160);
            this.textBox51.Name = "textBox51";
            this.textBox51.Size = new System.Drawing.Size(80, 25);
            this.textBox51.TabIndex = 160;
            this.textBox51.Text = "1000";
            this.textBox51.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // checkBox7
            // 
            this.checkBox7.AutoSize = true;
            this.checkBox7.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.checkBox7.Location = new System.Drawing.Point(13, 141);
            this.checkBox7.Name = "checkBox7";
            this.checkBox7.Size = new System.Drawing.Size(15, 14);
            this.checkBox7.TabIndex = 158;
            this.checkBox7.UseVisualStyleBackColor = false;
            // 
            // checkBox8
            // 
            this.checkBox8.AutoSize = true;
            this.checkBox8.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.checkBox8.Location = new System.Drawing.Point(13, 166);
            this.checkBox8.Name = "checkBox8";
            this.checkBox8.Size = new System.Drawing.Size(15, 14);
            this.checkBox8.TabIndex = 157;
            this.checkBox8.UseVisualStyleBackColor = false;
            // 
            // textBox52
            // 
            this.textBox52.BackColor = System.Drawing.SystemColors.Window;
            this.textBox52.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox52.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox52.Location = new System.Drawing.Point(73, 136);
            this.textBox52.Name = "textBox52";
            this.textBox52.Size = new System.Drawing.Size(150, 25);
            this.textBox52.TabIndex = 155;
            this.textBox52.Text = "税率%";
            this.textBox52.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox53
            // 
            this.textBox53.BackColor = System.Drawing.SystemColors.Window;
            this.textBox53.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox53.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox53.Location = new System.Drawing.Point(73, 160);
            this.textBox53.Name = "textBox53";
            this.textBox53.Size = new System.Drawing.Size(150, 25);
            this.textBox53.TabIndex = 156;
            this.textBox53.Text = "最小数量（金额）";
            this.textBox53.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox54
            // 
            this.textBox54.BackColor = System.Drawing.SystemColors.Window;
            this.textBox54.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox54.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox54.Location = new System.Drawing.Point(5, 136);
            this.textBox54.Name = "textBox54";
            this.textBox54.Size = new System.Drawing.Size(30, 25);
            this.textBox54.TabIndex = 151;
            this.textBox54.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox55
            // 
            this.textBox55.BackColor = System.Drawing.SystemColors.Window;
            this.textBox55.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox55.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox55.Location = new System.Drawing.Point(34, 136);
            this.textBox55.Name = "textBox55";
            this.textBox55.Size = new System.Drawing.Size(40, 25);
            this.textBox55.TabIndex = 152;
            this.textBox55.Text = "5";
            this.textBox55.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox56
            // 
            this.textBox56.BackColor = System.Drawing.SystemColors.Window;
            this.textBox56.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox56.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox56.Location = new System.Drawing.Point(5, 160);
            this.textBox56.Name = "textBox56";
            this.textBox56.Size = new System.Drawing.Size(30, 25);
            this.textBox56.TabIndex = 153;
            this.textBox56.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox57
            // 
            this.textBox57.BackColor = System.Drawing.SystemColors.Window;
            this.textBox57.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox57.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox57.Location = new System.Drawing.Point(34, 160);
            this.textBox57.Name = "textBox57";
            this.textBox57.Size = new System.Drawing.Size(40, 25);
            this.textBox57.TabIndex = 154;
            this.textBox57.Text = "6";
            this.textBox57.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox30
            // 
            this.textBox30.BackColor = System.Drawing.SystemColors.Window;
            this.textBox30.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox30.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox30.Location = new System.Drawing.Point(365, 88);
            this.textBox30.Name = "textBox30";
            this.textBox30.Size = new System.Drawing.Size(80, 25);
            this.textBox30.TabIndex = 149;
            this.textBox30.Text = "——";
            this.textBox30.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox31
            // 
            this.textBox31.BackColor = System.Drawing.SystemColors.Window;
            this.textBox31.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox31.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox31.Location = new System.Drawing.Point(365, 112);
            this.textBox31.Name = "textBox31";
            this.textBox31.Size = new System.Drawing.Size(80, 25);
            this.textBox31.TabIndex = 150;
            this.textBox31.Text = "——";
            this.textBox31.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox32
            // 
            this.textBox32.BackColor = System.Drawing.SystemColors.Window;
            this.textBox32.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox32.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox32.Location = new System.Drawing.Point(301, 88);
            this.textBox32.Name = "textBox32";
            this.textBox32.Size = new System.Drawing.Size(65, 25);
            this.textBox32.TabIndex = 147;
            this.textBox32.Text = "CNY";
            this.textBox32.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox33
            // 
            this.textBox33.BackColor = System.Drawing.SystemColors.Window;
            this.textBox33.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox33.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox33.Location = new System.Drawing.Point(301, 112);
            this.textBox33.Name = "textBox33";
            this.textBox33.Size = new System.Drawing.Size(65, 25);
            this.textBox33.TabIndex = 148;
            this.textBox33.Text = "%";
            this.textBox33.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox34
            // 
            this.textBox34.BackColor = System.Drawing.SystemColors.Window;
            this.textBox34.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox34.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox34.Location = new System.Drawing.Point(222, 88);
            this.textBox34.Name = "textBox34";
            this.textBox34.Size = new System.Drawing.Size(80, 25);
            this.textBox34.TabIndex = 145;
            this.textBox34.Text = "0.7";
            this.textBox34.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox35
            // 
            this.textBox35.BackColor = System.Drawing.SystemColors.Window;
            this.textBox35.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox35.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox35.Location = new System.Drawing.Point(222, 112);
            this.textBox35.Name = "textBox35";
            this.textBox35.Size = new System.Drawing.Size(80, 25);
            this.textBox35.TabIndex = 146;
            this.textBox35.Text = "5";
            this.textBox35.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // checkBox4
            // 
            this.checkBox4.AutoSize = true;
            this.checkBox4.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.checkBox4.Location = new System.Drawing.Point(13, 93);
            this.checkBox4.Name = "checkBox4";
            this.checkBox4.Size = new System.Drawing.Size(15, 14);
            this.checkBox4.TabIndex = 144;
            this.checkBox4.UseVisualStyleBackColor = false;
            // 
            // checkBox5
            // 
            this.checkBox5.AutoSize = true;
            this.checkBox5.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.checkBox5.Location = new System.Drawing.Point(13, 118);
            this.checkBox5.Name = "checkBox5";
            this.checkBox5.Size = new System.Drawing.Size(15, 14);
            this.checkBox5.TabIndex = 143;
            this.checkBox5.UseVisualStyleBackColor = false;
            // 
            // textBox38
            // 
            this.textBox38.BackColor = System.Drawing.SystemColors.Window;
            this.textBox38.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox38.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox38.Location = new System.Drawing.Point(73, 88);
            this.textBox38.Name = "textBox38";
            this.textBox38.Size = new System.Drawing.Size(150, 25);
            this.textBox38.TabIndex = 141;
            this.textBox38.Text = "附加费";
            this.textBox38.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox39
            // 
            this.textBox39.BackColor = System.Drawing.SystemColors.Window;
            this.textBox39.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox39.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox39.Location = new System.Drawing.Point(73, 112);
            this.textBox39.Name = "textBox39";
            this.textBox39.Size = new System.Drawing.Size(150, 25);
            this.textBox39.TabIndex = 142;
            this.textBox39.Text = "集团折扣%";
            this.textBox39.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox40
            // 
            this.textBox40.BackColor = System.Drawing.SystemColors.Window;
            this.textBox40.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox40.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox40.Location = new System.Drawing.Point(5, 88);
            this.textBox40.Name = "textBox40";
            this.textBox40.Size = new System.Drawing.Size(30, 25);
            this.textBox40.TabIndex = 137;
            this.textBox40.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox41
            // 
            this.textBox41.BackColor = System.Drawing.SystemColors.Window;
            this.textBox41.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox41.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox41.Location = new System.Drawing.Point(34, 88);
            this.textBox41.Name = "textBox41";
            this.textBox41.Size = new System.Drawing.Size(40, 25);
            this.textBox41.TabIndex = 138;
            this.textBox41.Text = "3";
            this.textBox41.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox44
            // 
            this.textBox44.BackColor = System.Drawing.SystemColors.Window;
            this.textBox44.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox44.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox44.Location = new System.Drawing.Point(5, 112);
            this.textBox44.Name = "textBox44";
            this.textBox44.Size = new System.Drawing.Size(30, 25);
            this.textBox44.TabIndex = 139;
            this.textBox44.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox45
            // 
            this.textBox45.BackColor = System.Drawing.SystemColors.Window;
            this.textBox45.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox45.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox45.Location = new System.Drawing.Point(34, 112);
            this.textBox45.Name = "textBox45";
            this.textBox45.Size = new System.Drawing.Size(40, 25);
            this.textBox45.TabIndex = 140;
            this.textBox45.Text = "4";
            this.textBox45.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label47
            // 
            this.label47.BackColor = System.Drawing.SystemColors.Window;
            this.label47.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label47.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label47.Location = new System.Drawing.Point(365, 16);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(80, 25);
            this.label47.TabIndex = 136;
            this.label47.Text = "删除指示符";
            this.label47.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox127
            // 
            this.textBox127.BackColor = System.Drawing.SystemColors.Window;
            this.textBox127.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox127.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox127.Location = new System.Drawing.Point(365, 40);
            this.textBox127.Name = "textBox127";
            this.textBox127.Size = new System.Drawing.Size(80, 25);
            this.textBox127.TabIndex = 132;
            this.textBox127.Text = "——";
            this.textBox127.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox130
            // 
            this.textBox130.BackColor = System.Drawing.SystemColors.Window;
            this.textBox130.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox130.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox130.Location = new System.Drawing.Point(365, 64);
            this.textBox130.Name = "textBox130";
            this.textBox130.Size = new System.Drawing.Size(80, 25);
            this.textBox130.TabIndex = 133;
            this.textBox130.Text = "——";
            this.textBox130.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label49
            // 
            this.label49.BackColor = System.Drawing.SystemColors.Window;
            this.label49.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label49.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label49.Location = new System.Drawing.Point(301, 16);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(65, 25);
            this.label49.TabIndex = 131;
            this.label49.Text = "单位";
            this.label49.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox131
            // 
            this.textBox131.BackColor = System.Drawing.SystemColors.Window;
            this.textBox131.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox131.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox131.Location = new System.Drawing.Point(301, 40);
            this.textBox131.Name = "textBox131";
            this.textBox131.Size = new System.Drawing.Size(65, 25);
            this.textBox131.TabIndex = 127;
            this.textBox131.Text = "CNY";
            this.textBox131.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox134
            // 
            this.textBox134.BackColor = System.Drawing.SystemColors.Window;
            this.textBox134.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox134.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox134.Location = new System.Drawing.Point(301, 64);
            this.textBox134.Name = "textBox134";
            this.textBox134.Size = new System.Drawing.Size(65, 25);
            this.textBox134.TabIndex = 128;
            this.textBox134.Text = "%";
            this.textBox134.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label56
            // 
            this.label56.BackColor = System.Drawing.SystemColors.Window;
            this.label56.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label56.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label56.Location = new System.Drawing.Point(222, 16);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(80, 25);
            this.label56.TabIndex = 126;
            this.label56.Text = "金额";
            this.label56.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox135
            // 
            this.textBox135.BackColor = System.Drawing.SystemColors.Window;
            this.textBox135.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox135.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox135.Location = new System.Drawing.Point(222, 40);
            this.textBox135.Name = "textBox135";
            this.textBox135.Size = new System.Drawing.Size(80, 25);
            this.textBox135.TabIndex = 122;
            this.textBox135.Text = "23.20";
            this.textBox135.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox138
            // 
            this.textBox138.BackColor = System.Drawing.SystemColors.Window;
            this.textBox138.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox138.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox138.Location = new System.Drawing.Point(222, 64);
            this.textBox138.Name = "textBox138";
            this.textBox138.Size = new System.Drawing.Size(80, 25);
            this.textBox138.TabIndex = 123;
            this.textBox138.Text = "3";
            this.textBox138.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // checkBox6
            // 
            this.checkBox6.AutoSize = true;
            this.checkBox6.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.checkBox6.Location = new System.Drawing.Point(13, 45);
            this.checkBox6.Name = "checkBox6";
            this.checkBox6.Size = new System.Drawing.Size(15, 14);
            this.checkBox6.TabIndex = 121;
            this.checkBox6.UseVisualStyleBackColor = false;
            // 
            // checkBox9
            // 
            this.checkBox9.AutoSize = true;
            this.checkBox9.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.checkBox9.Location = new System.Drawing.Point(13, 70);
            this.checkBox9.Name = "checkBox9";
            this.checkBox9.Size = new System.Drawing.Size(15, 14);
            this.checkBox9.TabIndex = 116;
            this.checkBox9.UseVisualStyleBackColor = false;
            // 
            // label57
            // 
            this.label57.BackColor = System.Drawing.SystemColors.Window;
            this.label57.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label57.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label57.Location = new System.Drawing.Point(73, 16);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(150, 25);
            this.label57.TabIndex = 114;
            this.label57.Text = "类型";
            this.label57.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox28
            // 
            this.textBox28.BackColor = System.Drawing.SystemColors.Window;
            this.textBox28.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox28.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox28.Location = new System.Drawing.Point(73, 40);
            this.textBox28.Name = "textBox28";
            this.textBox28.Size = new System.Drawing.Size(150, 25);
            this.textBox28.TabIndex = 108;
            this.textBox28.Text = "总价格";
            this.textBox28.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox29
            // 
            this.textBox29.BackColor = System.Drawing.SystemColors.Window;
            this.textBox29.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox29.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox29.Location = new System.Drawing.Point(73, 64);
            this.textBox29.Name = "textBox29";
            this.textBox29.Size = new System.Drawing.Size(150, 25);
            this.textBox29.TabIndex = 109;
            this.textBox29.Text = "总价折扣%";
            this.textBox29.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox36
            // 
            this.textBox36.BackColor = System.Drawing.SystemColors.Window;
            this.textBox36.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox36.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox36.Location = new System.Drawing.Point(5, 40);
            this.textBox36.Name = "textBox36";
            this.textBox36.Size = new System.Drawing.Size(30, 25);
            this.textBox36.TabIndex = 0;
            this.textBox36.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label61
            // 
            this.label61.BackColor = System.Drawing.SystemColors.Window;
            this.label61.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label61.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label61.Location = new System.Drawing.Point(5, 16);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(30, 25);
            this.label61.TabIndex = 86;
            this.label61.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label62
            // 
            this.label62.BackColor = System.Drawing.SystemColors.Window;
            this.label62.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label62.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label62.Location = new System.Drawing.Point(34, 16);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(40, 25);
            this.label62.TabIndex = 86;
            this.label62.Text = "序号";
            this.label62.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox37
            // 
            this.textBox37.BackColor = System.Drawing.SystemColors.Window;
            this.textBox37.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox37.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox37.Location = new System.Drawing.Point(34, 40);
            this.textBox37.Name = "textBox37";
            this.textBox37.Size = new System.Drawing.Size(40, 25);
            this.textBox37.TabIndex = 1;
            this.textBox37.Text = "1";
            this.textBox37.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox42
            // 
            this.textBox42.BackColor = System.Drawing.SystemColors.Window;
            this.textBox42.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox42.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox42.Location = new System.Drawing.Point(5, 64);
            this.textBox42.Name = "textBox42";
            this.textBox42.Size = new System.Drawing.Size(30, 25);
            this.textBox42.TabIndex = 4;
            this.textBox42.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox43
            // 
            this.textBox43.BackColor = System.Drawing.SystemColors.Window;
            this.textBox43.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox43.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox43.Location = new System.Drawing.Point(34, 64);
            this.textBox43.Name = "textBox43";
            this.textBox43.Size = new System.Drawing.Size(40, 25);
            this.textBox43.TabIndex = 5;
            this.textBox43.Text = "2";
            this.textBox43.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(170, 316);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(60, 30);
            this.button2.TabIndex = 280;
            this.button2.Text = "删除";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(91, 316);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(60, 30);
            this.button1.TabIndex = 279;
            this.button1.Text = "新增";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label40.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label40.Location = new System.Drawing.Point(21, 323);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(52, 14);
            this.label40.TabIndex = 278;
            this.label40.Text = "条件表";
            // 
            // textBox20
            // 
            this.textBox20.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox20.Location = new System.Drawing.Point(226, 283);
            this.textBox20.Name = "textBox20";
            this.textBox20.Size = new System.Drawing.Size(30, 23);
            this.textBox20.TabIndex = 277;
            this.textBox20.Text = "1";
            // 
            // textBox22
            // 
            this.textBox22.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox22.Location = new System.Drawing.Point(260, 283);
            this.textBox22.Name = "textBox22";
            this.textBox22.Size = new System.Drawing.Size(30, 23);
            this.textBox22.TabIndex = 276;
            this.textBox22.Text = "EA";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(211, 287);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(11, 12);
            this.label39.TabIndex = 275;
            this.label39.Text = "/";
            // 
            // textBox26
            // 
            this.textBox26.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox26.Location = new System.Drawing.Point(178, 283);
            this.textBox26.Name = "textBox26";
            this.textBox26.Size = new System.Drawing.Size(30, 23);
            this.textBox26.TabIndex = 274;
            this.textBox26.Text = "CNY";
            // 
            // textBox27
            // 
            this.textBox27.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox27.Location = new System.Drawing.Point(110, 283);
            this.textBox27.Name = "textBox27";
            this.textBox27.Size = new System.Drawing.Size(65, 23);
            this.textBox27.TabIndex = 273;
            this.textBox27.Text = "22.80";
            // 
            // textBox19
            // 
            this.textBox19.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox19.Location = new System.Drawing.Point(226, 247);
            this.textBox19.Name = "textBox19";
            this.textBox19.Size = new System.Drawing.Size(30, 23);
            this.textBox19.TabIndex = 272;
            this.textBox19.Text = "1";
            // 
            // textBox18
            // 
            this.textBox18.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox18.Location = new System.Drawing.Point(260, 247);
            this.textBox18.Name = "textBox18";
            this.textBox18.Size = new System.Drawing.Size(30, 23);
            this.textBox18.TabIndex = 271;
            this.textBox18.Text = "EA";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(211, 251);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(11, 12);
            this.label37.TabIndex = 270;
            this.label37.Text = "/";
            // 
            // textBox16
            // 
            this.textBox16.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox16.Location = new System.Drawing.Point(178, 247);
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new System.Drawing.Size(30, 23);
            this.textBox16.TabIndex = 269;
            this.textBox16.Text = "CNY";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label43.Location = new System.Drawing.Point(296, 172);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(21, 14);
            this.label43.TabIndex = 268;
            this.label43.Text = "EA";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label42.Location = new System.Drawing.Point(296, 135);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(21, 14);
            this.label42.TabIndex = 267;
            this.label42.Text = "EA";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("幼圆", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label44.Location = new System.Drawing.Point(589, 63);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(16, 16);
            this.label44.TabIndex = 266;
            this.label44.Text = "%";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Font = new System.Drawing.Font("幼圆", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label55.Location = new System.Drawing.Point(589, 100);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(16, 16);
            this.label55.TabIndex = 265;
            this.label55.Text = "%";
            // 
            // checkBox3
            // 
            this.checkBox3.AutoSize = true;
            this.checkBox3.Location = new System.Drawing.Point(401, 288);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(15, 14);
            this.checkBox3.TabIndex = 264;
            this.checkBox3.UseVisualStyleBackColor = true;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("宋体", 9F);
            this.label33.Location = new System.Drawing.Point(421, 288);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(77, 12);
            this.label33.TabIndex = 263;
            this.label33.Text = "没有现金折扣";
            // 
            // textBox13
            // 
            this.textBox13.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox13.Location = new System.Drawing.Point(110, 247);
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new System.Drawing.Size(65, 23);
            this.textBox13.TabIndex = 262;
            this.textBox13.Text = "22.10";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(22, 251);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(29, 12);
            this.label34.TabIndex = 261;
            this.label34.Text = "净价";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label35.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label35.Location = new System.Drawing.Point(18, 209);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(127, 14);
            this.label35.TabIndex = 260;
            this.label35.Text = "订单单位（采购）";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("宋体", 9F);
            this.label36.Location = new System.Drawing.Point(22, 288);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(53, 12);
            this.label36.TabIndex = 259;
            this.label36.Text = "有效价格";
            // 
            // dateTimePicker4
            // 
            this.dateTimePicker4.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.dateTimePicker4.Location = new System.Drawing.Point(483, 247);
            this.dateTimePicker4.Name = "dateTimePicker4";
            this.dateTimePicker4.Size = new System.Drawing.Size(200, 23);
            this.dateTimePicker4.TabIndex = 257;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(395, 251);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(53, 12);
            this.label38.TabIndex = 254;
            this.label38.Text = "有效期至";
            // 
            // textBox15
            // 
            this.textBox15.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox15.Location = new System.Drawing.Point(483, 94);
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new System.Drawing.Size(100, 23);
            this.textBox15.TabIndex = 251;
            this.textBox15.Text = "190";
            // 
            // textBox17
            // 
            this.textBox17.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox17.Location = new System.Drawing.Point(483, 58);
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new System.Drawing.Size(100, 23);
            this.textBox17.TabIndex = 249;
            this.textBox17.Text = "CN";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(395, 62);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(77, 12);
            this.label41.TabIndex = 247;
            this.label41.Text = "不足交货容差";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("宋体", 9F);
            this.label45.Location = new System.Drawing.Point(395, 99);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(77, 12);
            this.label45.TabIndex = 243;
            this.label45.Text = "超量假货容差";
            // 
            // textBox21
            // 
            this.textBox21.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox21.Location = new System.Drawing.Point(110, 94);
            this.textBox21.Name = "textBox21";
            this.textBox21.Size = new System.Drawing.Size(100, 23);
            this.textBox21.TabIndex = 238;
            this.textBox21.Text = "K00000407";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(216, 62);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(29, 12);
            this.label48.TabIndex = 237;
            this.label48.Text = "天数";
            // 
            // textBox23
            // 
            this.textBox23.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox23.Location = new System.Drawing.Point(110, 130);
            this.textBox23.Name = "textBox23";
            this.textBox23.Size = new System.Drawing.Size(180, 23);
            this.textBox23.TabIndex = 234;
            this.textBox23.Text = "100";
            // 
            // textBox24
            // 
            this.textBox24.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox24.Location = new System.Drawing.Point(110, 58);
            this.textBox24.Name = "textBox24";
            this.textBox24.Size = new System.Drawing.Size(100, 23);
            this.textBox24.TabIndex = 233;
            this.textBox24.Text = "15";
            // 
            // textBox25
            // 
            this.textBox25.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox25.Location = new System.Drawing.Point(110, 167);
            this.textBox25.Name = "textBox25";
            this.textBox25.Size = new System.Drawing.Size(180, 23);
            this.textBox25.TabIndex = 232;
            this.textBox25.Text = "丁宁";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(22, 62);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(77, 12);
            this.label50.TabIndex = 231;
            this.label50.Text = "计划交货时间";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Font = new System.Drawing.Font("宋体", 9F);
            this.label51.Location = new System.Drawing.Point(22, 172);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(77, 12);
            this.label51.TabIndex = 230;
            this.label51.Text = "最小订单数量";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Font = new System.Drawing.Font("宋体", 9F);
            this.label52.Location = new System.Drawing.Point(22, 135);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(77, 12);
            this.label52.TabIndex = 229;
            this.label52.Text = "标准订单数量";
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label53.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label53.Location = new System.Drawing.Point(18, 20);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(37, 14);
            this.label53.TabIndex = 228;
            this.label53.Text = "控制";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Font = new System.Drawing.Font("宋体", 9F);
            this.label54.Location = new System.Drawing.Point(22, 99);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(53, 12);
            this.label54.TabIndex = 227;
            this.label54.Text = "采购组织";
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage3.Controls.Add(this.panel3);
            this.tabPage3.Controls.Add(this.button7);
            this.tabPage3.Controls.Add(this.panel2);
            this.tabPage3.Controls.Add(this.button8);
            this.tabPage3.Controls.Add(this.button3);
            this.tabPage3.Controls.Add(this.label69);
            this.tabPage3.Controls.Add(this.button4);
            this.tabPage3.Controls.Add(this.label68);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(734, 664);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "文本";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.button12);
            this.panel3.Controls.Add(this.textBox71);
            this.panel3.Controls.Add(this.checkBox15);
            this.panel3.Controls.Add(this.textBox72);
            this.panel3.Controls.Add(this.textBox73);
            this.panel3.Controls.Add(this.button11);
            this.panel3.Controls.Add(this.textBox68);
            this.panel3.Controls.Add(this.checkBox13);
            this.panel3.Controls.Add(this.textBox69);
            this.panel3.Controls.Add(this.textBox70);
            this.panel3.Controls.Add(this.button6);
            this.panel3.Controls.Add(this.label60);
            this.panel3.Controls.Add(this.textBox59);
            this.panel3.Controls.Add(this.checkBox10);
            this.panel3.Controls.Add(this.label63);
            this.panel3.Controls.Add(this.textBox60);
            this.panel3.Controls.Add(this.textBox61);
            this.panel3.Controls.Add(this.label67);
            this.panel3.Location = new System.Drawing.Point(22, 261);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(659, 141);
            this.panel3.TabIndex = 290;
            // 
            // button12
            // 
            this.button12.Location = new System.Drawing.Point(443, 91);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(60, 20);
            this.button12.TabIndex = 296;
            this.button12.Text = "下载";
            this.button12.UseVisualStyleBackColor = true;
            // 
            // textBox71
            // 
            this.textBox71.BackColor = System.Drawing.SystemColors.Window;
            this.textBox71.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox71.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox71.Location = new System.Drawing.Point(433, 88);
            this.textBox71.Name = "textBox71";
            this.textBox71.Size = new System.Drawing.Size(80, 25);
            this.textBox71.TabIndex = 295;
            this.textBox71.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // checkBox15
            // 
            this.checkBox15.AutoSize = true;
            this.checkBox15.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.checkBox15.Location = new System.Drawing.Point(13, 93);
            this.checkBox15.Name = "checkBox15";
            this.checkBox15.Size = new System.Drawing.Size(15, 14);
            this.checkBox15.TabIndex = 294;
            this.checkBox15.UseVisualStyleBackColor = false;
            // 
            // textBox72
            // 
            this.textBox72.BackColor = System.Drawing.SystemColors.Window;
            this.textBox72.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox72.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox72.Location = new System.Drawing.Point(34, 88);
            this.textBox72.Name = "textBox72";
            this.textBox72.Size = new System.Drawing.Size(400, 25);
            this.textBox72.TabIndex = 293;
            this.textBox72.Text = "采购项目附件";
            this.textBox72.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox73
            // 
            this.textBox73.BackColor = System.Drawing.SystemColors.Window;
            this.textBox73.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox73.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox73.Location = new System.Drawing.Point(5, 88);
            this.textBox73.Name = "textBox73";
            this.textBox73.Size = new System.Drawing.Size(30, 25);
            this.textBox73.TabIndex = 292;
            this.textBox73.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // button11
            // 
            this.button11.Location = new System.Drawing.Point(443, 67);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(60, 20);
            this.button11.TabIndex = 291;
            this.button11.Text = "下载";
            this.button11.UseVisualStyleBackColor = true;
            // 
            // textBox68
            // 
            this.textBox68.BackColor = System.Drawing.SystemColors.Window;
            this.textBox68.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox68.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox68.Location = new System.Drawing.Point(433, 64);
            this.textBox68.Name = "textBox68";
            this.textBox68.Size = new System.Drawing.Size(80, 25);
            this.textBox68.TabIndex = 290;
            this.textBox68.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // checkBox13
            // 
            this.checkBox13.AutoSize = true;
            this.checkBox13.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.checkBox13.Location = new System.Drawing.Point(13, 69);
            this.checkBox13.Name = "checkBox13";
            this.checkBox13.Size = new System.Drawing.Size(15, 14);
            this.checkBox13.TabIndex = 289;
            this.checkBox13.UseVisualStyleBackColor = false;
            // 
            // textBox69
            // 
            this.textBox69.BackColor = System.Drawing.SystemColors.Window;
            this.textBox69.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox69.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox69.Location = new System.Drawing.Point(34, 64);
            this.textBox69.Name = "textBox69";
            this.textBox69.Size = new System.Drawing.Size(400, 25);
            this.textBox69.TabIndex = 288;
            this.textBox69.Text = "采购项目技术提案";
            this.textBox69.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox70
            // 
            this.textBox70.BackColor = System.Drawing.SystemColors.Window;
            this.textBox70.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox70.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox70.Location = new System.Drawing.Point(5, 64);
            this.textBox70.Name = "textBox70";
            this.textBox70.Size = new System.Drawing.Size(30, 25);
            this.textBox70.TabIndex = 287;
            this.textBox70.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(443, 43);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(60, 20);
            this.button6.TabIndex = 286;
            this.button6.Text = "下载";
            this.button6.UseVisualStyleBackColor = true;
            // 
            // label60
            // 
            this.label60.BackColor = System.Drawing.SystemColors.Window;
            this.label60.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label60.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label60.Location = new System.Drawing.Point(433, 16);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(80, 25);
            this.label60.TabIndex = 126;
            this.label60.Text = "操作";
            this.label60.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox59
            // 
            this.textBox59.BackColor = System.Drawing.SystemColors.Window;
            this.textBox59.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox59.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox59.Location = new System.Drawing.Point(433, 40);
            this.textBox59.Name = "textBox59";
            this.textBox59.Size = new System.Drawing.Size(80, 25);
            this.textBox59.TabIndex = 122;
            this.textBox59.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // checkBox10
            // 
            this.checkBox10.AutoSize = true;
            this.checkBox10.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.checkBox10.Location = new System.Drawing.Point(13, 45);
            this.checkBox10.Name = "checkBox10";
            this.checkBox10.Size = new System.Drawing.Size(15, 14);
            this.checkBox10.TabIndex = 121;
            this.checkBox10.UseVisualStyleBackColor = false;
            // 
            // label63
            // 
            this.label63.BackColor = System.Drawing.SystemColors.Window;
            this.label63.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label63.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label63.Location = new System.Drawing.Point(34, 16);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(400, 25);
            this.label63.TabIndex = 114;
            this.label63.Text = "文本名称";
            this.label63.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox60
            // 
            this.textBox60.BackColor = System.Drawing.SystemColors.Window;
            this.textBox60.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox60.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox60.Location = new System.Drawing.Point(34, 40);
            this.textBox60.Name = "textBox60";
            this.textBox60.Size = new System.Drawing.Size(400, 25);
            this.textBox60.TabIndex = 108;
            this.textBox60.Text = "采购项目电子标书";
            this.textBox60.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox61
            // 
            this.textBox61.BackColor = System.Drawing.SystemColors.Window;
            this.textBox61.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox61.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox61.Location = new System.Drawing.Point(5, 40);
            this.textBox61.Name = "textBox61";
            this.textBox61.Size = new System.Drawing.Size(30, 25);
            this.textBox61.TabIndex = 0;
            this.textBox61.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label67
            // 
            this.label67.BackColor = System.Drawing.SystemColors.Window;
            this.label67.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label67.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label67.Location = new System.Drawing.Point(5, 16);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(30, 25);
            this.label67.TabIndex = 86;
            this.label67.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(216, 225);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(60, 30);
            this.button7.TabIndex = 289;
            this.button7.Text = "删除";
            this.button7.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.button10);
            this.panel2.Controls.Add(this.textBox65);
            this.panel2.Controls.Add(this.checkBox12);
            this.panel2.Controls.Add(this.textBox66);
            this.panel2.Controls.Add(this.textBox67);
            this.panel2.Controls.Add(this.button9);
            this.panel2.Controls.Add(this.textBox62);
            this.panel2.Controls.Add(this.checkBox11);
            this.panel2.Controls.Add(this.textBox63);
            this.panel2.Controls.Add(this.textBox64);
            this.panel2.Controls.Add(this.button5);
            this.panel2.Controls.Add(this.label64);
            this.panel2.Controls.Add(this.textBox87);
            this.panel2.Controls.Add(this.checkBox14);
            this.panel2.Controls.Add(this.label65);
            this.panel2.Controls.Add(this.textBox89);
            this.panel2.Controls.Add(this.textBox91);
            this.panel2.Controls.Add(this.label66);
            this.panel2.Location = new System.Drawing.Point(22, 61);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(659, 143);
            this.panel2.TabIndex = 285;
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(443, 91);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(60, 20);
            this.button10.TabIndex = 296;
            this.button10.Text = "下载";
            this.button10.UseVisualStyleBackColor = true;
            // 
            // textBox65
            // 
            this.textBox65.BackColor = System.Drawing.SystemColors.Window;
            this.textBox65.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox65.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox65.Location = new System.Drawing.Point(433, 88);
            this.textBox65.Name = "textBox65";
            this.textBox65.Size = new System.Drawing.Size(80, 25);
            this.textBox65.TabIndex = 295;
            this.textBox65.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // checkBox12
            // 
            this.checkBox12.AutoSize = true;
            this.checkBox12.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.checkBox12.Location = new System.Drawing.Point(13, 93);
            this.checkBox12.Name = "checkBox12";
            this.checkBox12.Size = new System.Drawing.Size(15, 14);
            this.checkBox12.TabIndex = 294;
            this.checkBox12.UseVisualStyleBackColor = false;
            // 
            // textBox66
            // 
            this.textBox66.BackColor = System.Drawing.SystemColors.Window;
            this.textBox66.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox66.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox66.Location = new System.Drawing.Point(34, 88);
            this.textBox66.Name = "textBox66";
            this.textBox66.Size = new System.Drawing.Size(400, 25);
            this.textBox66.TabIndex = 293;
            this.textBox66.Text = "技术要求文件";
            this.textBox66.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox67
            // 
            this.textBox67.BackColor = System.Drawing.SystemColors.Window;
            this.textBox67.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox67.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox67.Location = new System.Drawing.Point(5, 88);
            this.textBox67.Name = "textBox67";
            this.textBox67.Size = new System.Drawing.Size(30, 25);
            this.textBox67.TabIndex = 292;
            this.textBox67.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(443, 67);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(60, 20);
            this.button9.TabIndex = 291;
            this.button9.Text = "下载";
            this.button9.UseVisualStyleBackColor = true;
            // 
            // textBox62
            // 
            this.textBox62.BackColor = System.Drawing.SystemColors.Window;
            this.textBox62.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox62.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox62.Location = new System.Drawing.Point(433, 64);
            this.textBox62.Name = "textBox62";
            this.textBox62.Size = new System.Drawing.Size(80, 25);
            this.textBox62.TabIndex = 290;
            this.textBox62.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // checkBox11
            // 
            this.checkBox11.AutoSize = true;
            this.checkBox11.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.checkBox11.Location = new System.Drawing.Point(13, 69);
            this.checkBox11.Name = "checkBox11";
            this.checkBox11.Size = new System.Drawing.Size(15, 14);
            this.checkBox11.TabIndex = 289;
            this.checkBox11.UseVisualStyleBackColor = false;
            // 
            // textBox63
            // 
            this.textBox63.BackColor = System.Drawing.SystemColors.Window;
            this.textBox63.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox63.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox63.Location = new System.Drawing.Point(34, 64);
            this.textBox63.Name = "textBox63";
            this.textBox63.Size = new System.Drawing.Size(400, 25);
            this.textBox63.TabIndex = 288;
            this.textBox63.Text = "招标要求文件";
            this.textBox63.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox64
            // 
            this.textBox64.BackColor = System.Drawing.SystemColors.Window;
            this.textBox64.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox64.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox64.Location = new System.Drawing.Point(5, 64);
            this.textBox64.Name = "textBox64";
            this.textBox64.Size = new System.Drawing.Size(30, 25);
            this.textBox64.TabIndex = 287;
            this.textBox64.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(443, 43);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(60, 20);
            this.button5.TabIndex = 286;
            this.button5.Text = "下载";
            this.button5.UseVisualStyleBackColor = true;
            // 
            // label64
            // 
            this.label64.BackColor = System.Drawing.SystemColors.Window;
            this.label64.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label64.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label64.Location = new System.Drawing.Point(433, 16);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(80, 25);
            this.label64.TabIndex = 126;
            this.label64.Text = "操作";
            this.label64.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox87
            // 
            this.textBox87.BackColor = System.Drawing.SystemColors.Window;
            this.textBox87.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox87.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox87.Location = new System.Drawing.Point(433, 40);
            this.textBox87.Name = "textBox87";
            this.textBox87.Size = new System.Drawing.Size(80, 25);
            this.textBox87.TabIndex = 122;
            this.textBox87.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // checkBox14
            // 
            this.checkBox14.AutoSize = true;
            this.checkBox14.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.checkBox14.Location = new System.Drawing.Point(13, 45);
            this.checkBox14.Name = "checkBox14";
            this.checkBox14.Size = new System.Drawing.Size(15, 14);
            this.checkBox14.TabIndex = 121;
            this.checkBox14.UseVisualStyleBackColor = false;
            // 
            // label65
            // 
            this.label65.BackColor = System.Drawing.SystemColors.Window;
            this.label65.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label65.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label65.Location = new System.Drawing.Point(34, 16);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(400, 25);
            this.label65.TabIndex = 114;
            this.label65.Text = "文本名称";
            this.label65.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox89
            // 
            this.textBox89.BackColor = System.Drawing.SystemColors.Window;
            this.textBox89.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox89.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox89.Location = new System.Drawing.Point(34, 40);
            this.textBox89.Name = "textBox89";
            this.textBox89.Size = new System.Drawing.Size(400, 25);
            this.textBox89.TabIndex = 108;
            this.textBox89.Text = "采购订单记录";
            this.textBox89.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox91
            // 
            this.textBox91.BackColor = System.Drawing.SystemColors.Window;
            this.textBox91.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox91.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox91.Location = new System.Drawing.Point(5, 40);
            this.textBox91.Name = "textBox91";
            this.textBox91.Size = new System.Drawing.Size(30, 25);
            this.textBox91.TabIndex = 0;
            this.textBox91.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label66
            // 
            this.label66.BackColor = System.Drawing.SystemColors.Window;
            this.label66.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label66.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label66.Location = new System.Drawing.Point(5, 16);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(30, 25);
            this.label66.TabIndex = 86;
            this.label66.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(137, 225);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(60, 30);
            this.button8.TabIndex = 288;
            this.button8.Text = "新增";
            this.button8.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(216, 25);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(60, 30);
            this.button3.TabIndex = 284;
            this.button3.Text = "删除";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label69.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label69.Location = new System.Drawing.Point(19, 232);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(112, 14);
            this.label69.TabIndex = 287;
            this.label69.Text = "供应商记录文本";
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(137, 25);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(60, 30);
            this.button4.TabIndex = 283;
            this.button4.Text = "新增";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label68.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label68.Location = new System.Drawing.Point(19, 32);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(112, 14);
            this.label68.TabIndex = 282;
            this.label68.Text = "采购方记录文本";
            // 
            // Maintain_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(903, 881);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.wlbh_tb);
            this.Controls.Add(this.cgxxjl_tb);
            this.Controls.Add(this.gc_tb);
            this.Controls.Add(this.cgzz_tb);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.gys_cmb);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "Maintain_Form";
            this.Text = "维护";
            this.panel1.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox wlbh_tb;
        private System.Windows.Forms.TextBox cgxxjl_tb;
        private System.Windows.Forms.TextBox gc_tb;
        private System.Windows.Forms.TextBox cgzz_tb;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox gys_cmb;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button maintain_bt;
        private System.Windows.Forms.Button create_bt;
        private System.Windows.Forms.Button refresh_bt;
        private System.Windows.Forms.Button query_bt;
        private System.Windows.Forms.Button next_bt;
        private System.Windows.Forms.Button close_bt;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.TextBox textBox14;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.TextBox textBox11;
        private System.Windows.Forms.TextBox textBox12;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.TextBox textBox20;
        private System.Windows.Forms.TextBox textBox22;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.TextBox textBox26;
        private System.Windows.Forms.TextBox textBox27;
        private System.Windows.Forms.TextBox textBox19;
        private System.Windows.Forms.TextBox textBox18;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.TextBox textBox16;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.CheckBox checkBox3;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox textBox13;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.DateTimePicker dateTimePicker4;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.TextBox textBox15;
        private System.Windows.Forms.TextBox textBox17;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.TextBox textBox21;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.TextBox textBox23;
        private System.Windows.Forms.TextBox textBox24;
        private System.Windows.Forms.TextBox textBox25;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.DateTimePicker dateTimePicker3;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.TextBox textBox58;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.TextBox textBox46;
        private System.Windows.Forms.TextBox textBox47;
        private System.Windows.Forms.TextBox textBox48;
        private System.Windows.Forms.TextBox textBox49;
        private System.Windows.Forms.TextBox textBox50;
        private System.Windows.Forms.TextBox textBox51;
        private System.Windows.Forms.CheckBox checkBox7;
        private System.Windows.Forms.CheckBox checkBox8;
        private System.Windows.Forms.TextBox textBox52;
        private System.Windows.Forms.TextBox textBox53;
        private System.Windows.Forms.TextBox textBox54;
        private System.Windows.Forms.TextBox textBox55;
        private System.Windows.Forms.TextBox textBox56;
        private System.Windows.Forms.TextBox textBox57;
        private System.Windows.Forms.TextBox textBox30;
        private System.Windows.Forms.TextBox textBox31;
        private System.Windows.Forms.TextBox textBox32;
        private System.Windows.Forms.TextBox textBox33;
        private System.Windows.Forms.TextBox textBox34;
        private System.Windows.Forms.TextBox textBox35;
        private System.Windows.Forms.CheckBox checkBox4;
        private System.Windows.Forms.CheckBox checkBox5;
        private System.Windows.Forms.TextBox textBox38;
        private System.Windows.Forms.TextBox textBox39;
        private System.Windows.Forms.TextBox textBox40;
        private System.Windows.Forms.TextBox textBox41;
        private System.Windows.Forms.TextBox textBox44;
        private System.Windows.Forms.TextBox textBox45;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.TextBox textBox127;
        private System.Windows.Forms.TextBox textBox130;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.TextBox textBox131;
        private System.Windows.Forms.TextBox textBox134;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.TextBox textBox135;
        private System.Windows.Forms.TextBox textBox138;
        private System.Windows.Forms.CheckBox checkBox6;
        private System.Windows.Forms.CheckBox checkBox9;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.TextBox textBox28;
        private System.Windows.Forms.TextBox textBox29;
        private System.Windows.Forms.TextBox textBox36;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.TextBox textBox37;
        private System.Windows.Forms.TextBox textBox42;
        private System.Windows.Forms.TextBox textBox43;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.TextBox textBox59;
        private System.Windows.Forms.CheckBox checkBox10;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.TextBox textBox60;
        private System.Windows.Forms.TextBox textBox61;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.TextBox textBox87;
        private System.Windows.Forms.CheckBox checkBox14;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.TextBox textBox89;
        private System.Windows.Forms.TextBox textBox91;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.TextBox textBox65;
        private System.Windows.Forms.CheckBox checkBox12;
        private System.Windows.Forms.TextBox textBox66;
        private System.Windows.Forms.TextBox textBox67;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.TextBox textBox62;
        private System.Windows.Forms.CheckBox checkBox11;
        private System.Windows.Forms.TextBox textBox63;
        private System.Windows.Forms.TextBox textBox64;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.TextBox textBox68;
        private System.Windows.Forms.CheckBox checkBox13;
        private System.Windows.Forms.TextBox textBox69;
        private System.Windows.Forms.TextBox textBox70;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.TextBox textBox71;
        private System.Windows.Forms.CheckBox checkBox15;
        private System.Windows.Forms.TextBox textBox72;
        private System.Windows.Forms.TextBox textBox73;
    }
}