﻿namespace MMClient.SourcingManage.SourcingManagement
{
    partial class SelectSupplier
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SupplierDGV = new System.Windows.Forms.DataGridView();
            this.OkBtn = new System.Windows.Forms.Button();
            this.select = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Supplier_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Supplier_Name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Supplier_AccountGroup = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Mobile_Phone1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Email = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.SupplierDGV)).BeginInit();
            this.SuspendLayout();
            // 
            // SupplierDGV
            // 
            this.SupplierDGV.AllowUserToAddRows = false;
            this.SupplierDGV.AllowUserToDeleteRows = false;
            this.SupplierDGV.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.SupplierDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.SupplierDGV.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.select,
            this.Supplier_ID,
            this.Supplier_Name,
            this.Supplier_AccountGroup,
            this.Mobile_Phone1,
            this.Email});
            this.SupplierDGV.Location = new System.Drawing.Point(2, 41);
            this.SupplierDGV.Name = "SupplierDGV";
            this.SupplierDGV.RowTemplate.Height = 23;
            this.SupplierDGV.Size = new System.Drawing.Size(796, 350);
            this.SupplierDGV.TabIndex = 0;
            // 
            // OkBtn
            // 
            this.OkBtn.Location = new System.Drawing.Point(723, 12);
            this.OkBtn.Name = "OkBtn";
            this.OkBtn.Size = new System.Drawing.Size(75, 23);
            this.OkBtn.TabIndex = 1;
            this.OkBtn.Text = "确定";
            this.OkBtn.UseVisualStyleBackColor = true;
            this.OkBtn.Click += new System.EventHandler(this.OkBtn_Click);
            // 
            // select
            // 
            this.select.HeaderText = "选择";
            this.select.Name = "select";
            // 
            // Supplier_ID
            // 
            this.Supplier_ID.DataPropertyName = "Supplier_ID";
            this.Supplier_ID.HeaderText = "供应商编码";
            this.Supplier_ID.Name = "Supplier_ID";
            // 
            // Supplier_Name
            // 
            this.Supplier_Name.DataPropertyName = "Supplier_Name";
            this.Supplier_Name.HeaderText = "供应商名称";
            this.Supplier_Name.Name = "Supplier_Name";
            // 
            // Supplier_AccountGroup
            // 
            this.Supplier_AccountGroup.DataPropertyName = "Supplier_AccountGroup";
            this.Supplier_AccountGroup.HeaderText = "账户组";
            this.Supplier_AccountGroup.Name = "Supplier_AccountGroup";
            // 
            // Mobile_Phone1
            // 
            this.Mobile_Phone1.DataPropertyName = "Mobile_Phone1";
            this.Mobile_Phone1.HeaderText = "移动电话";
            this.Mobile_Phone1.Name = "Mobile_Phone1";
            // 
            // Email
            // 
            this.Email.DataPropertyName = "Email";
            this.Email.HeaderText = "邮箱";
            this.Email.Name = "Email";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(2, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 12);
            this.label1.TabIndex = 2;
            this.label1.Text = "显示";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(32, 14);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(143, 20);
            this.comboBox1.TabIndex = 3;
            // 
            // SelectSupplier
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(797, 393);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.OkBtn);
            this.Controls.Add(this.SupplierDGV);
            this.Name = "SelectSupplier";
            this.Text = "确定接洽的供应商";
            ((System.ComponentModel.ISupportInitialize)(this.SupplierDGV)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView SupplierDGV;
        private System.Windows.Forms.Button OkBtn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn select;
        private System.Windows.Forms.DataGridViewTextBoxColumn Supplier_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Supplier_Name;
        private System.Windows.Forms.DataGridViewTextBoxColumn Supplier_AccountGroup;
        private System.Windows.Forms.DataGridViewTextBoxColumn Mobile_Phone1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Email;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBox1;
    }
}