﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Configuration;

namespace MMClient
{
    public partial class DownloadFileForm : Form
    {
        public DownloadFileForm()
        {
            InitializeComponent();

            string path = System.Configuration.ConfigurationManager.AppSettings["saveFilePath"];
            this.textBox1.Text = path;

            LoadListView();
        }


        /// <summary>  
        /// 初始化上传列表  
        /// </summary>  
        void LoadListView()
        {
            listView1.View = View.Details;
            listView1.CheckBoxes = true;
            listView1.GridLines = true;
            listView1.Columns.Add("文件名", 150, HorizontalAlignment.Center);
            listView1.Columns.Add("文件大小", 150, HorizontalAlignment.Center);
            listView1.Columns.Add("文件路径", 150, HorizontalAlignment.Center);
        }  

        /// <summary>        
        /// c#,.net 下载文件        
        /// </summary>        
        /// <param name="URL">下载文件地址</param>       
        /// 
        /// <param name="Filename">下载后的存放地址</param>        
        /// <param name="Prog">用于显示的进度条</param>        
        /// 
        public void DownloadFile(string URL, string filename, System.Windows.Forms.ProgressBar prog, System.Windows.Forms.Label label1)
        {
            float percent = 0;
            try
            {
                System.Net.HttpWebRequest Myrq = (System.Net.HttpWebRequest)System.Net.HttpWebRequest.Create(URL);
                System.Net.HttpWebResponse myrp = (System.Net.HttpWebResponse)Myrq.GetResponse();
                long totalBytes = myrp.ContentLength;
                if (prog != null)
                {
                    prog.Maximum = (int)totalBytes;
                }
                System.IO.Stream st = myrp.GetResponseStream();
                System.IO.Stream so = new System.IO.FileStream(filename, System.IO.FileMode.Create);
                long totalDownloadedByte = 0;
                byte[] by = new byte[1024];
                int osize = st.Read(by, 0, (int)by.Length);
                while (osize > 0)
                {
                    totalDownloadedByte = osize + totalDownloadedByte;
                    System.Windows.Forms.Application.DoEvents();
                    so.Write(by, 0, osize);
                    if (prog != null)
                    {
                        prog.Value = (int)totalDownloadedByte;
                    }
                    osize = st.Read(by, 0, (int)by.Length);

                    percent = (float)totalDownloadedByte / (float)totalBytes * 100;
                    label1.Text = "当前下载进度" + percent.ToString() + "%";
                    System.Windows.Forms.Application.DoEvents(); //必须加注这句代码，否则label1将因为循环执行太快而来不及显示信息
                }
                so.Close();
                st.Close();
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        private void changepathbtn_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            if (fbd.ShowDialog() == DialogResult.OK)
            {
                textBox1.Text = fbd.SelectedPath;
                //string newSavePath = fbd.SelectedPath;
                //System.Configuration.ConfigurationManager.AppSettings.Set("uploadFilePath", newSavePath);
            }  

        }

        private void addbtn_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Multiselect = true;
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                foreach (string filename in ofd.FileNames)
                {
                    FileInfo fi = new FileInfo(filename);
                    //ListViewItem lvi = new ListViewItem(Path.GetFileNameWithoutExtension(filename));
                    ListViewItem lvi = new ListViewItem(Path.GetFileName(filename));
                    lvi.Tag = filename;
                    lvi.SubItems.Add(fi.Length.ToString());
                    //lvi.SubItems.Add(Math.Round((fi.Length / (1024.0 * 1024.0)),2).ToString() + "M");  
                    lvi.SubItems.Add(Path.GetDirectoryName(filename));
                    lvi.Checked = true;
                    listView1.Items.Add(lvi);
                }

            }  
        }

        private void downloadbtn_Click(object sender, EventArgs e)
        {
            //string path = System.Configuration.ConfigurationManager.AppSettings["saveFilePath"];
            //DownloadFile(this.textBox1.Text.Trim(),path ,this.progressBar1,supplierLabel);

            if (textBox1.Text.Trim().Equals(""))
            {
                MessageBox.Show("请先选择存储路径..!");
                return;
            }
            else
            {
                string path = this.textBox1.Text.Trim();
                if (listView1.Items.Count > 0)
                {
                    int j = 0;
                    string count = listView1.CheckedItems.Count.ToString();
                    for (int i = 0; i < listView1.Items.Count; i++)
                    {

                        if (listView1.Items[i].Checked)
                        {
                            j++;
                            string fileName = Path.GetFileName(listView1.Items[i].Tag.ToString());
                            label1.Text = string.Format("正在下载文件:[{0}]", listView1.Items[i].Text) + "：" + j.ToString() + "：" + count;
                            FileStream des = new FileStream(Path.Combine(path, fileName), FileMode.OpenOrCreate, FileAccess.Write);
                            FileStream fir = new FileStream(listView1.Items[i].Tag.ToString(), FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                            byte[] buffer = new byte[10240];
                            int size = 0; int ren = 0;
                            while (ren < fir.Length)
                            {
                                Application.DoEvents();
                                size = fir.Read(buffer, 0, buffer.Length);
                                des.Write(buffer, 0, size);
                                ren += size;
                                SetTextMessage(ren);
                            }
                            listView1.Items[i].Checked = false;
                        }
                        else
                        {
                            continue;
                        }
                    }
                    MessageBox.Show("下载成功!");
                }
                else
                {
                    return;
                }
            }       
        }

        private delegate void SetPos(int ipos);

        private void SetTextMessage(int ipos)
        {
            if (this.InvokeRequired)
            {
                SetPos setpos = new SetPos(SetTextMessage);
                this.Invoke(setpos, new object[] { ipos });
            }
            else
            {
                foreach (ListViewItem lvi in listView1.CheckedItems)
                {
                    string total = lvi.SubItems[1].Text;
                    int pro = (int)((float)ipos / long.Parse(total) * 100);
                    if (pro <= progressBar1.Maximum)
                    {
                        this.progressBar1.Value = pro;
                        progressBar1.Text = label1.Text.Split('：')[0].ToString() + Environment.NewLine + string.Format("上传进度:{0}%", pro) + Environment.NewLine + string.Format("已上传文件数：{0}/{1}", label1.Text.Split('：')[1].ToString(), label1.Text.Split('：')[2].ToString());

                    }
                }
            }
        }

        private void deletebtn_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem lvi in listView1.CheckedItems)
            {
                lvi.Remove();
            }  
        }
    }
}
