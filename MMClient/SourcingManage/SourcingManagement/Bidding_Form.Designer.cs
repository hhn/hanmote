﻿namespace MMClient.SourcingManage.SourcingManagement
{
    partial class Bidding_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            this.xjdh_lbl = new System.Windows.Forms.Label();
            this.txt_BidName = new System.Windows.Forms.TextBox();
            this.txt_BidId = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.BiddingTab = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.cmb_TransType = new System.Windows.Forms.ComboBox();
            this.label46 = new System.Windows.Forms.Label();
            this.txt_commBidStartTime = new System.Windows.Forms.TextBox();
            this.label41 = new System.Windows.Forms.Label();
            this.txt_enrollStartTime = new System.Windows.Forms.TextBox();
            this.txt_enrollEndTime = new System.Windows.Forms.TextBox();
            this.txt_buyEndTime = new System.Windows.Forms.TextBox();
            this.txt_techBidStartTime = new System.Windows.Forms.TextBox();
            this.dtp_enrollEndDate = new System.Windows.Forms.DateTimePicker();
            this.dtp_enrollStartDate = new System.Windows.Forms.DateTimePicker();
            this.txt_bidCost = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.dtp_buyEndDate = new System.Windows.Forms.DateTimePicker();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.dtp_techBidStartDate = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dtp_commBidStartDate = new System.Windows.Forms.DateTimePicker();
            this.txt_serviceType = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.cmb_currency = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this.cmb_timeZone = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.dtp_startBeginTime = new System.Windows.Forms.DateTimePicker();
            this.label13 = new System.Windows.Forms.Label();
            this.dtp_startTime = new System.Windows.Forms.DateTimePicker();
            this.label11 = new System.Windows.Forms.Label();
            this.dtp_limitTime = new System.Windows.Forms.DateTimePicker();
            this.label10 = new System.Windows.Forms.Label();
            this.cmb_purchaseGroup = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.cmb_purchaseOrg = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.cmb_displayType = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.cmb_catalogue = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.dtp_endTime = new System.Windows.Forms.DateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btn_DeleteReviewer = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.reviewerName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btn_AddReviewer = new System.Windows.Forms.Button();
            this.txt_callerName = new System.Windows.Forms.TextBox();
            this.txt_callerId = new System.Windows.Forms.TextBox();
            this.label40 = new System.Windows.Forms.Label();
            this.txt_dropPointName = new System.Windows.Forms.TextBox();
            this.txt_dropPointId = new System.Windows.Forms.TextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.txt_receiverName = new System.Windows.Forms.TextBox();
            this.txt_receiverId = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.txt_requesterName = new System.Windows.Forms.TextBox();
            this.txt_requesterId = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.button3 = new System.Windows.Forms.Button();
            this.btn_addSupplier = new System.Windows.Forms.Button();
            this.SupplierGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn26 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bidSupplierId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bidSupplierName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bidContact = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bidEmail = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bidAddress = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bidState = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txt_longBidText = new System.Windows.Forms.RichTextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.txt_approvalComment = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.txt_shortBidText = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.fileGridView = new System.Windows.Forms.DataGridView();
            this.fileName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fileSize = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btn_upload = new System.Windows.Forms.Button();
            this.btn_ChooseFile = new System.Windows.Forms.Button();
            this.tabPage8 = new System.Windows.Forms.TabPage();
            this.tac_EvalMethod = new System.Windows.Forms.TabControl();
            this.tab_LowPrice = new System.Windows.Forms.TabPage();
            this.label26 = new System.Windows.Forms.Label();
            this.tab_WeightScore = new System.Windows.Forms.TabPage();
            this.gbx_WeightInfo = new System.Windows.Forms.GroupBox();
            this.chb_Weight = new System.Windows.Forms.CheckBox();
            this.btn_DeleteWeight = new System.Windows.Forms.Button();
            this.btn_AddWeight = new System.Windows.Forms.Button();
            this.WeightScoreGridView = new System.Windows.Forms.DataGridView();
            this.weightEvalName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.weightEvalNum = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tab_LowTotalCost = new System.Windows.Forms.TabPage();
            this.gbx_leastTotalCost = new System.Windows.Forms.GroupBox();
            this.button2 = new System.Windows.Forms.Button();
            this.btn_AddCost = new System.Windows.Forms.Button();
            this.cmb_Costs = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.allCostGridView = new System.Windows.Forms.DataGridView();
            this.CostItem = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.First = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Second = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Third = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.forth = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fifth = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sixth = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.seventh = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.eighth = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ninth = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tenth = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tab_ValueAssess = new System.Windows.Forms.TabPage();
            this.cmb_EvalMethod = new System.Windows.Forms.ComboBox();
            this.label27 = new System.Windows.Forms.Label();
            this.tabPage12 = new System.Windows.Forms.TabPage();
            this.button4 = new System.Windows.Forms.Button();
            this.cmb_Materials = new System.Windows.Forms.ComboBox();
            this.label24 = new System.Windows.Forms.Label();
            this.materialSourceView = new System.Windows.Forms.DataGridView();
            this.bidId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bidName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.createTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.State = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage10 = new System.Windows.Forms.TabPage();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label25 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.MaterialGridview = new System.Windows.Forms.DataGridView();
            this.demandNum = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.bidMaterialId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bidMaterialName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bidDemandCount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bidDemandMeasurement = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.factoryId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.stockId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DeliveryStartTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DeliveryEndTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnCheck = new System.Windows.Forms.Button();
            this.base_save = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btn_model = new System.Windows.Forms.Button();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn20 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn21 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn22 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn23 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn24 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.calendarColumn1 = new Lib.Common.CommonControls.CalendarColumn();
            this.txt_State = new System.Windows.Forms.TextBox();
            this.label45 = new System.Windows.Forms.Label();
            this.dataGridViewComboBoxColumn1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn2 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn3 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn4 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn5 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn6 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.BiddingTab.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage6.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SupplierGridView)).BeginInit();
            this.tabPage4.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fileGridView)).BeginInit();
            this.tabPage8.SuspendLayout();
            this.tac_EvalMethod.SuspendLayout();
            this.tab_LowPrice.SuspendLayout();
            this.tab_WeightScore.SuspendLayout();
            this.gbx_WeightInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.WeightScoreGridView)).BeginInit();
            this.tab_LowTotalCost.SuspendLayout();
            this.gbx_leastTotalCost.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.allCostGridView)).BeginInit();
            this.tabPage12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.materialSourceView)).BeginInit();
            this.tabPage10.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MaterialGridview)).BeginInit();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // xjdh_lbl
            // 
            this.xjdh_lbl.AutoSize = true;
            this.xjdh_lbl.Font = new System.Drawing.Font("宋体", 9F);
            this.xjdh_lbl.Location = new System.Drawing.Point(12, 56);
            this.xjdh_lbl.Name = "xjdh_lbl";
            this.xjdh_lbl.Size = new System.Drawing.Size(53, 12);
            this.xjdh_lbl.TabIndex = 155;
            this.xjdh_lbl.Text = "招标编号";
            // 
            // txt_BidName
            // 
            this.txt_BidName.Location = new System.Drawing.Point(332, 53);
            this.txt_BidName.Name = "txt_BidName";
            this.txt_BidName.Size = new System.Drawing.Size(151, 21);
            this.txt_BidName.TabIndex = 197;
            // 
            // txt_BidId
            // 
            this.txt_BidId.Location = new System.Drawing.Point(72, 53);
            this.txt_BidId.Name = "txt_BidId";
            this.txt_BidId.ReadOnly = true;
            this.txt_BidId.Size = new System.Drawing.Size(152, 21);
            this.txt_BidId.TabIndex = 196;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("宋体", 9F);
            this.label3.Location = new System.Drawing.Point(273, 56);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 12);
            this.label3.TabIndex = 194;
            this.label3.Text = "招标名称";
            // 
            // BiddingTab
            // 
            this.BiddingTab.Controls.Add(this.tabPage1);
            this.BiddingTab.Controls.Add(this.tabPage6);
            this.BiddingTab.Controls.Add(this.tabPage3);
            this.BiddingTab.Controls.Add(this.tabPage4);
            this.BiddingTab.Controls.Add(this.tabPage8);
            this.BiddingTab.Controls.Add(this.tabPage12);
            this.BiddingTab.Controls.Add(this.tabPage10);
            this.BiddingTab.Controls.Add(this.tabPage2);
            this.BiddingTab.Location = new System.Drawing.Point(-4, 18);
            this.BiddingTab.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.BiddingTab.Name = "BiddingTab";
            this.BiddingTab.Padding = new System.Drawing.Point(12, 6);
            this.BiddingTab.SelectedIndex = 0;
            this.BiddingTab.Size = new System.Drawing.Size(1170, 520);
            this.BiddingTab.TabIndex = 195;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage1.Controls.Add(this.cmb_TransType);
            this.tabPage1.Controls.Add(this.label46);
            this.tabPage1.Controls.Add(this.txt_commBidStartTime);
            this.tabPage1.Controls.Add(this.label41);
            this.tabPage1.Controls.Add(this.txt_enrollStartTime);
            this.tabPage1.Controls.Add(this.txt_enrollEndTime);
            this.tabPage1.Controls.Add(this.txt_buyEndTime);
            this.tabPage1.Controls.Add(this.txt_techBidStartTime);
            this.tabPage1.Controls.Add(this.dtp_enrollEndDate);
            this.tabPage1.Controls.Add(this.dtp_enrollStartDate);
            this.tabPage1.Controls.Add(this.txt_bidCost);
            this.tabPage1.Controls.Add(this.label23);
            this.tabPage1.Controls.Add(this.label22);
            this.tabPage1.Controls.Add(this.label21);
            this.tabPage1.Controls.Add(this.label20);
            this.tabPage1.Controls.Add(this.label19);
            this.tabPage1.Controls.Add(this.dtp_buyEndDate);
            this.tabPage1.Controls.Add(this.label18);
            this.tabPage1.Controls.Add(this.label17);
            this.tabPage1.Controls.Add(this.dtp_techBidStartDate);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.dtp_commBidStartDate);
            this.tabPage1.Controls.Add(this.txt_serviceType);
            this.tabPage1.Controls.Add(this.label15);
            this.tabPage1.Controls.Add(this.cmb_currency);
            this.tabPage1.Controls.Add(this.label16);
            this.tabPage1.Controls.Add(this.cmb_timeZone);
            this.tabPage1.Controls.Add(this.label14);
            this.tabPage1.Controls.Add(this.dtp_startBeginTime);
            this.tabPage1.Controls.Add(this.label13);
            this.tabPage1.Controls.Add(this.dtp_startTime);
            this.tabPage1.Controls.Add(this.label11);
            this.tabPage1.Controls.Add(this.dtp_limitTime);
            this.tabPage1.Controls.Add(this.label10);
            this.tabPage1.Controls.Add(this.cmb_purchaseGroup);
            this.tabPage1.Controls.Add(this.label8);
            this.tabPage1.Controls.Add(this.cmb_purchaseOrg);
            this.tabPage1.Controls.Add(this.label9);
            this.tabPage1.Controls.Add(this.cmb_displayType);
            this.tabPage1.Controls.Add(this.label7);
            this.tabPage1.Controls.Add(this.cmb_catalogue);
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Controls.Add(this.dtp_endTime);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Location = new System.Drawing.Point(4, 28);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage1.Size = new System.Drawing.Size(1162, 488);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "基本数据";
            // 
            // cmb_TransType
            // 
            this.cmb_TransType.FormattingEnabled = true;
            this.cmb_TransType.Items.AddRange(new object[] {
            "谈判",
            "等级"});
            this.cmb_TransType.Location = new System.Drawing.Point(116, 115);
            this.cmb_TransType.Name = "cmb_TransType";
            this.cmb_TransType.Size = new System.Drawing.Size(101, 20);
            this.cmb_TransType.TabIndex = 227;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(24, 115);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(53, 12);
            this.label46.TabIndex = 226;
            this.label46.Text = "交易类型";
            // 
            // txt_commBidStartTime
            // 
            this.txt_commBidStartTime.Location = new System.Drawing.Point(627, 18);
            this.txt_commBidStartTime.Name = "txt_commBidStartTime";
            this.txt_commBidStartTime.Size = new System.Drawing.Size(100, 21);
            this.txt_commBidStartTime.TabIndex = 225;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(510, 23);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(89, 12);
            this.label41.TabIndex = 224;
            this.label41.Text = "商务标开标时间";
            // 
            // txt_enrollStartTime
            // 
            this.txt_enrollStartTime.Location = new System.Drawing.Point(627, 262);
            this.txt_enrollStartTime.Name = "txt_enrollStartTime";
            this.txt_enrollStartTime.Size = new System.Drawing.Size(100, 21);
            this.txt_enrollStartTime.TabIndex = 223;
            // 
            // txt_enrollEndTime
            // 
            this.txt_enrollEndTime.Location = new System.Drawing.Point(627, 196);
            this.txt_enrollEndTime.Name = "txt_enrollEndTime";
            this.txt_enrollEndTime.Size = new System.Drawing.Size(100, 21);
            this.txt_enrollEndTime.TabIndex = 222;
            // 
            // txt_buyEndTime
            // 
            this.txt_buyEndTime.Location = new System.Drawing.Point(627, 140);
            this.txt_buyEndTime.Name = "txt_buyEndTime";
            this.txt_buyEndTime.Size = new System.Drawing.Size(100, 21);
            this.txt_buyEndTime.TabIndex = 221;
            // 
            // txt_techBidStartTime
            // 
            this.txt_techBidStartTime.Location = new System.Drawing.Point(627, 82);
            this.txt_techBidStartTime.Name = "txt_techBidStartTime";
            this.txt_techBidStartTime.Size = new System.Drawing.Size(100, 21);
            this.txt_techBidStartTime.TabIndex = 220;
            // 
            // dtp_enrollEndDate
            // 
            this.dtp_enrollEndDate.CustomFormat = "yyyy-MM-dd";
            this.dtp_enrollEndDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtp_enrollEndDate.Location = new System.Drawing.Point(627, 229);
            this.dtp_enrollEndDate.Name = "dtp_enrollEndDate";
            this.dtp_enrollEndDate.Size = new System.Drawing.Size(102, 21);
            this.dtp_enrollEndDate.TabIndex = 216;
            // 
            // dtp_enrollStartDate
            // 
            this.dtp_enrollStartDate.CustomFormat = "yyyy-MM-dd";
            this.dtp_enrollStartDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtp_enrollStartDate.Location = new System.Drawing.Point(629, 292);
            this.dtp_enrollStartDate.Name = "dtp_enrollStartDate";
            this.dtp_enrollStartDate.Size = new System.Drawing.Size(102, 21);
            this.dtp_enrollStartDate.TabIndex = 214;
            // 
            // txt_bidCost
            // 
            this.txt_bidCost.Location = new System.Drawing.Point(627, 324);
            this.txt_bidCost.Name = "txt_bidCost";
            this.txt_bidCost.Size = new System.Drawing.Size(100, 21);
            this.txt_bidCost.TabIndex = 213;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(512, 331);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(41, 12);
            this.label23.TabIndex = 212;
            this.label23.Text = "投标费";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(512, 302);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(77, 12);
            this.label22.TabIndex = 211;
            this.label22.Text = "报名开始日期";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(510, 266);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(77, 12);
            this.label21.TabIndex = 210;
            this.label21.Text = "报名开始时间";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(510, 238);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(77, 12);
            this.label20.TabIndex = 209;
            this.label20.Text = "报名结束日期";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(510, 206);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(77, 12);
            this.label19.TabIndex = 208;
            this.label19.Text = "报名结束时间";
            // 
            // dtp_buyEndDate
            // 
            this.dtp_buyEndDate.CustomFormat = "yyyy-MM-dd";
            this.dtp_buyEndDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtp_buyEndDate.Location = new System.Drawing.Point(627, 169);
            this.dtp_buyEndDate.Name = "dtp_buyEndDate";
            this.dtp_buyEndDate.Size = new System.Drawing.Size(100, 21);
            this.dtp_buyEndDate.TabIndex = 207;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(510, 178);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(113, 12);
            this.label18.TabIndex = 206;
            this.label18.Text = "购买标书的截至日期";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(512, 144);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(113, 12);
            this.label17.TabIndex = 204;
            this.label17.Text = "购买标书的截止时间";
            // 
            // dtp_techBidStartDate
            // 
            this.dtp_techBidStartDate.CustomFormat = "yyyy-MM-dd";
            this.dtp_techBidStartDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtp_techBidStartDate.Location = new System.Drawing.Point(627, 109);
            this.dtp_techBidStartDate.Name = "dtp_techBidStartDate";
            this.dtp_techBidStartDate.Size = new System.Drawing.Size(102, 21);
            this.dtp_techBidStartDate.TabIndex = 203;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(510, 115);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(89, 12);
            this.label4.TabIndex = 202;
            this.label4.Text = "技术标开标日期";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(510, 85);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 12);
            this.label2.TabIndex = 200;
            this.label2.Text = "技术标开标时间";
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(510, 54);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(89, 12);
            this.label1.TabIndex = 199;
            this.label1.Text = "商务标开标日期";
            // 
            // dtp_commBidStartDate
            // 
            this.dtp_commBidStartDate.CustomFormat = "yyyy-MM-dd";
            this.dtp_commBidStartDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtp_commBidStartDate.Location = new System.Drawing.Point(625, 45);
            this.dtp_commBidStartDate.Name = "dtp_commBidStartDate";
            this.dtp_commBidStartDate.Size = new System.Drawing.Size(102, 21);
            this.dtp_commBidStartDate.TabIndex = 198;
            // 
            // txt_serviceType
            // 
            this.txt_serviceType.Location = new System.Drawing.Point(116, 21);
            this.txt_serviceType.Name = "txt_serviceType";
            this.txt_serviceType.Size = new System.Drawing.Size(100, 21);
            this.txt_serviceType.TabIndex = 196;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("宋体", 9F);
            this.label15.Location = new System.Drawing.Point(22, 366);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(29, 12);
            this.label15.TabIndex = 193;
            this.label15.Text = "货币";
            // 
            // cmb_currency
            // 
            this.cmb_currency.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.cmb_currency.FormattingEnabled = true;
            this.cmb_currency.Items.AddRange(new object[] {
            "欧元",
            "美元",
            "人民币"});
            this.cmb_currency.Location = new System.Drawing.Point(117, 366);
            this.cmb_currency.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cmb_currency.Name = "cmb_currency";
            this.cmb_currency.Size = new System.Drawing.Size(100, 22);
            this.cmb_currency.TabIndex = 194;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("宋体", 9F);
            this.label16.Location = new System.Drawing.Point(22, 333);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(29, 12);
            this.label16.TabIndex = 191;
            this.label16.Text = "时区";
            // 
            // cmb_timeZone
            // 
            this.cmb_timeZone.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.cmb_timeZone.FormattingEnabled = true;
            this.cmb_timeZone.Items.AddRange(new object[] {
            "UTF-8",
            "UTF-9",
            "UTF-10"});
            this.cmb_timeZone.Location = new System.Drawing.Point(117, 332);
            this.cmb_timeZone.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cmb_timeZone.Name = "cmb_timeZone";
            this.cmb_timeZone.Size = new System.Drawing.Size(100, 22);
            this.cmb_timeZone.TabIndex = 192;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("宋体", 9F);
            this.label14.Location = new System.Drawing.Point(22, 273);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(53, 12);
            this.label14.TabIndex = 189;
            this.label14.Text = "起始日期";
            // 
            // dtp_startBeginTime
            // 
            this.dtp_startBeginTime.CustomFormat = "yyyy-MM-dd HH:mm:ss";
            this.dtp_startBeginTime.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.dtp_startBeginTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtp_startBeginTime.Location = new System.Drawing.Point(116, 265);
            this.dtp_startBeginTime.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dtp_startBeginTime.Name = "dtp_startBeginTime";
            this.dtp_startBeginTime.Size = new System.Drawing.Size(153, 23);
            this.dtp_startBeginTime.TabIndex = 190;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("宋体", 9F);
            this.label13.Location = new System.Drawing.Point(22, 213);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(53, 12);
            this.label13.TabIndex = 187;
            this.label13.Text = "开始日期";
            // 
            // dtp_startTime
            // 
            this.dtp_startTime.CustomFormat = "yyyy-MM-dd HH:mm:ss";
            this.dtp_startTime.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.dtp_startTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtp_startTime.Location = new System.Drawing.Point(116, 205);
            this.dtp_startTime.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dtp_startTime.Name = "dtp_startTime";
            this.dtp_startTime.Size = new System.Drawing.Size(153, 23);
            this.dtp_startTime.TabIndex = 188;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("宋体", 9F);
            this.label11.Location = new System.Drawing.Point(22, 301);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(89, 12);
            this.label11.TabIndex = 183;
            this.label11.Text = "限定期段的结束";
            // 
            // dtp_limitTime
            // 
            this.dtp_limitTime.CustomFormat = "yyyy-MM-dd";
            this.dtp_limitTime.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.dtp_limitTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtp_limitTime.Location = new System.Drawing.Point(118, 294);
            this.dtp_limitTime.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dtp_limitTime.Name = "dtp_limitTime";
            this.dtp_limitTime.Size = new System.Drawing.Size(99, 23);
            this.dtp_limitTime.TabIndex = 184;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("宋体", 9F);
            this.label10.Location = new System.Drawing.Point(22, 179);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(41, 12);
            this.label10.TabIndex = 181;
            this.label10.Text = "采购组";
            // 
            // cmb_purchaseGroup
            // 
            this.cmb_purchaseGroup.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.cmb_purchaseGroup.FormattingEnabled = true;
            this.cmb_purchaseGroup.Location = new System.Drawing.Point(116, 175);
            this.cmb_purchaseGroup.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cmb_purchaseGroup.Name = "cmb_purchaseGroup";
            this.cmb_purchaseGroup.Size = new System.Drawing.Size(100, 22);
            this.cmb_purchaseGroup.TabIndex = 182;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("宋体", 9F);
            this.label8.Location = new System.Drawing.Point(22, 149);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(53, 12);
            this.label8.TabIndex = 179;
            this.label8.Text = "采购组织";
            // 
            // cmb_purchaseOrg
            // 
            this.cmb_purchaseOrg.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.cmb_purchaseOrg.FormattingEnabled = true;
            this.cmb_purchaseOrg.Location = new System.Drawing.Point(116, 145);
            this.cmb_purchaseOrg.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cmb_purchaseOrg.Name = "cmb_purchaseOrg";
            this.cmb_purchaseOrg.Size = new System.Drawing.Size(100, 22);
            this.cmb_purchaseOrg.TabIndex = 180;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("宋体", 9F);
            this.label9.Location = new System.Drawing.Point(22, 84);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(53, 12);
            this.label9.TabIndex = 177;
            this.label9.Text = "发布类型";
            // 
            // cmb_displayType
            // 
            this.cmb_displayType.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.cmb_displayType.FormattingEnabled = true;
            this.cmb_displayType.Items.AddRange(new object[] {
            "单一货源",
            "限制招标",
            "公开招标",
            "两步招标"});
            this.cmb_displayType.Location = new System.Drawing.Point(116, 80);
            this.cmb_displayType.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cmb_displayType.Name = "cmb_displayType";
            this.cmb_displayType.Size = new System.Drawing.Size(100, 22);
            this.cmb_displayType.TabIndex = 178;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("宋体", 9F);
            this.label7.Location = new System.Drawing.Point(22, 54);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 12);
            this.label7.TabIndex = 175;
            this.label7.Text = "产品目录";
            // 
            // cmb_catalogue
            // 
            this.cmb_catalogue.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.cmb_catalogue.FormattingEnabled = true;
            this.cmb_catalogue.Location = new System.Drawing.Point(116, 50);
            this.cmb_catalogue.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cmb_catalogue.Name = "cmb_catalogue";
            this.cmb_catalogue.Size = new System.Drawing.Size(100, 22);
            this.cmb_catalogue.TabIndex = 176;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("宋体", 9F);
            this.label6.Location = new System.Drawing.Point(22, 241);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 12);
            this.label6.TabIndex = 173;
            this.label6.Text = "结束日期";
            // 
            // dtp_endTime
            // 
            this.dtp_endTime.CustomFormat = "yyyy-MM-dd HH:mm:ss";
            this.dtp_endTime.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.dtp_endTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtp_endTime.Location = new System.Drawing.Point(116, 235);
            this.dtp_endTime.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dtp_endTime.Name = "dtp_endTime";
            this.dtp_endTime.Size = new System.Drawing.Size(153, 23);
            this.dtp_endTime.TabIndex = 174;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("宋体", 9F);
            this.label5.Location = new System.Drawing.Point(22, 24);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 12);
            this.label5.TabIndex = 171;
            this.label5.Text = "业务类型";
            // 
            // tabPage6
            // 
            this.tabPage6.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage6.Controls.Add(this.groupBox3);
            this.tabPage6.Controls.Add(this.txt_callerName);
            this.tabPage6.Controls.Add(this.txt_callerId);
            this.tabPage6.Controls.Add(this.label40);
            this.tabPage6.Controls.Add(this.txt_dropPointName);
            this.tabPage6.Controls.Add(this.txt_dropPointId);
            this.tabPage6.Controls.Add(this.label38);
            this.tabPage6.Controls.Add(this.txt_receiverName);
            this.tabPage6.Controls.Add(this.txt_receiverId);
            this.tabPage6.Controls.Add(this.label37);
            this.tabPage6.Controls.Add(this.txt_requesterName);
            this.tabPage6.Controls.Add(this.txt_requesterId);
            this.tabPage6.Controls.Add(this.label36);
            this.tabPage6.Controls.Add(this.label35);
            this.tabPage6.Controls.Add(this.label34);
            this.tabPage6.Controls.Add(this.label33);
            this.tabPage6.Location = new System.Drawing.Point(4, 28);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage6.Size = new System.Drawing.Size(1162, 488);
            this.tabPage6.TabIndex = 5;
            this.tabPage6.Text = "合作伙伴";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btn_DeleteReviewer);
            this.groupBox3.Controls.Add(this.dataGridView1);
            this.groupBox3.Controls.Add(this.btn_AddReviewer);
            this.groupBox3.Location = new System.Drawing.Point(108, 235);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(657, 186);
            this.groupBox3.TabIndex = 16;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "评审专家";
            // 
            // btn_DeleteReviewer
            // 
            this.btn_DeleteReviewer.Location = new System.Drawing.Point(507, 59);
            this.btn_DeleteReviewer.Name = "btn_DeleteReviewer";
            this.btn_DeleteReviewer.Size = new System.Drawing.Size(97, 23);
            this.btn_DeleteReviewer.TabIndex = 15;
            this.btn_DeleteReviewer.Text = "删除评审专家";
            this.btn_DeleteReviewer.UseVisualStyleBackColor = true;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.reviewerName});
            this.dataGridView1.Location = new System.Drawing.Point(17, 20);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.Size = new System.Drawing.Size(449, 150);
            this.dataGridView1.TabIndex = 14;
            // 
            // reviewerName
            // 
            this.reviewerName.HeaderText = "专家名称";
            this.reviewerName.Name = "reviewerName";
            // 
            // btn_AddReviewer
            // 
            this.btn_AddReviewer.Location = new System.Drawing.Point(507, 20);
            this.btn_AddReviewer.Name = "btn_AddReviewer";
            this.btn_AddReviewer.Size = new System.Drawing.Size(97, 23);
            this.btn_AddReviewer.TabIndex = 13;
            this.btn_AddReviewer.Text = "添加评审专家";
            this.btn_AddReviewer.UseVisualStyleBackColor = true;
            // 
            // txt_callerName
            // 
            this.txt_callerName.Location = new System.Drawing.Point(460, 189);
            this.txt_callerName.Name = "txt_callerName";
            this.txt_callerName.Size = new System.Drawing.Size(100, 21);
            this.txt_callerName.TabIndex = 15;
            // 
            // txt_callerId
            // 
            this.txt_callerId.Location = new System.Drawing.Point(242, 189);
            this.txt_callerId.Name = "txt_callerId";
            this.txt_callerId.Size = new System.Drawing.Size(100, 21);
            this.txt_callerId.TabIndex = 14;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(112, 192);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(41, 12);
            this.label40.TabIndex = 13;
            this.label40.Text = "开标人";
            // 
            // txt_dropPointName
            // 
            this.txt_dropPointName.Location = new System.Drawing.Point(460, 144);
            this.txt_dropPointName.Name = "txt_dropPointName";
            this.txt_dropPointName.Size = new System.Drawing.Size(100, 21);
            this.txt_dropPointName.TabIndex = 11;
            // 
            // txt_dropPointId
            // 
            this.txt_dropPointId.Location = new System.Drawing.Point(242, 141);
            this.txt_dropPointId.Name = "txt_dropPointId";
            this.txt_dropPointId.Size = new System.Drawing.Size(100, 21);
            this.txt_dropPointId.TabIndex = 10;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(112, 144);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(41, 12);
            this.label38.TabIndex = 9;
            this.label38.Text = "交货点";
            // 
            // txt_receiverName
            // 
            this.txt_receiverName.Location = new System.Drawing.Point(460, 96);
            this.txt_receiverName.Name = "txt_receiverName";
            this.txt_receiverName.Size = new System.Drawing.Size(100, 21);
            this.txt_receiverName.TabIndex = 8;
            // 
            // txt_receiverId
            // 
            this.txt_receiverId.Location = new System.Drawing.Point(242, 96);
            this.txt_receiverId.Name = "txt_receiverId";
            this.txt_receiverId.Size = new System.Drawing.Size(100, 21);
            this.txt_receiverId.TabIndex = 7;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(110, 106);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(41, 12);
            this.label37.TabIndex = 6;
            this.label37.Text = "收货人";
            // 
            // txt_requesterName
            // 
            this.txt_requesterName.Location = new System.Drawing.Point(460, 55);
            this.txt_requesterName.Name = "txt_requesterName";
            this.txt_requesterName.Size = new System.Drawing.Size(100, 21);
            this.txt_requesterName.TabIndex = 5;
            // 
            // txt_requesterId
            // 
            this.txt_requesterId.Location = new System.Drawing.Point(242, 55);
            this.txt_requesterId.Name = "txt_requesterId";
            this.txt_requesterId.Size = new System.Drawing.Size(100, 21);
            this.txt_requesterId.TabIndex = 4;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(489, 18);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(29, 12);
            this.label36.TabIndex = 3;
            this.label36.Text = "名称";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(282, 19);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(29, 12);
            this.label35.TabIndex = 2;
            this.label35.Text = "编号";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(106, 19);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(29, 12);
            this.label34.TabIndex = 1;
            this.label34.Text = "功能";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(108, 58);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(41, 12);
            this.label33.TabIndex = 0;
            this.label33.Text = "请求者";
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage3.Controls.Add(this.button3);
            this.tabPage3.Controls.Add(this.btn_addSupplier);
            this.tabPage3.Controls.Add(this.SupplierGridView);
            this.tabPage3.Location = new System.Drawing.Point(4, 28);
            this.tabPage3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(1162, 488);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "投标人";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(124, 297);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 180;
            this.button3.Text = "删除投标人";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // btn_addSupplier
            // 
            this.btn_addSupplier.Location = new System.Drawing.Point(22, 297);
            this.btn_addSupplier.Name = "btn_addSupplier";
            this.btn_addSupplier.Size = new System.Drawing.Size(75, 23);
            this.btn_addSupplier.TabIndex = 179;
            this.btn_addSupplier.Text = "添加投标人";
            this.btn_addSupplier.UseVisualStyleBackColor = true;
            this.btn_addSupplier.Click += new System.EventHandler(this.btn_addSupplier_Click);
            // 
            // SupplierGridView
            // 
            this.SupplierGridView.AllowUserToAddRows = false;
            this.SupplierGridView.BackgroundColor = System.Drawing.SystemColors.Control;
            this.SupplierGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.SupplierGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn26,
            this.bidSupplierId,
            this.bidSupplierName,
            this.bidContact,
            this.bidEmail,
            this.bidAddress,
            this.bidState});
            this.SupplierGridView.Location = new System.Drawing.Point(22, 15);
            this.SupplierGridView.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.SupplierGridView.Name = "SupplierGridView";
            this.SupplierGridView.RowHeadersVisible = false;
            this.SupplierGridView.RowTemplate.Height = 23;
            this.SupplierGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.SupplierGridView.Size = new System.Drawing.Size(854, 277);
            this.SupplierGridView.TabIndex = 178;
            // 
            // dataGridViewTextBoxColumn26
            // 
            dataGridViewCellStyle1.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.dataGridViewTextBoxColumn26.DefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridViewTextBoxColumn26.Frozen = true;
            this.dataGridViewTextBoxColumn26.HeaderText = "";
            this.dataGridViewTextBoxColumn26.Name = "dataGridViewTextBoxColumn26";
            this.dataGridViewTextBoxColumn26.ReadOnly = true;
            this.dataGridViewTextBoxColumn26.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn26.Width = 30;
            // 
            // bidSupplierId
            // 
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.White;
            this.bidSupplierId.DefaultCellStyle = dataGridViewCellStyle2;
            this.bidSupplierId.Frozen = true;
            this.bidSupplierId.HeaderText = "供应商编号";
            this.bidSupplierId.Name = "bidSupplierId";
            this.bidSupplierId.ReadOnly = true;
            this.bidSupplierId.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // bidSupplierName
            // 
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.White;
            this.bidSupplierName.DefaultCellStyle = dataGridViewCellStyle3;
            this.bidSupplierName.Frozen = true;
            this.bidSupplierName.HeaderText = "供应商名称";
            this.bidSupplierName.Name = "bidSupplierName";
            this.bidSupplierName.ReadOnly = true;
            this.bidSupplierName.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.bidSupplierName.Width = 120;
            // 
            // bidContact
            // 
            this.bidContact.Frozen = true;
            this.bidContact.HeaderText = "联系人";
            this.bidContact.Name = "bidContact";
            this.bidContact.ReadOnly = true;
            this.bidContact.Width = 95;
            // 
            // bidEmail
            // 
            this.bidEmail.Frozen = true;
            this.bidEmail.HeaderText = "电子信箱";
            this.bidEmail.Name = "bidEmail";
            this.bidEmail.ReadOnly = true;
            // 
            // bidAddress
            // 
            this.bidAddress.Frozen = true;
            this.bidAddress.HeaderText = "地址";
            this.bidAddress.Name = "bidAddress";
            this.bidAddress.ReadOnly = true;
            this.bidAddress.Width = 130;
            // 
            // bidState
            // 
            this.bidState.HeaderText = "投标状态";
            this.bidState.Name = "bidState";
            // 
            // tabPage4
            // 
            this.tabPage4.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage4.Controls.Add(this.groupBox4);
            this.tabPage4.Controls.Add(this.groupBox2);
            this.tabPage4.Controls.Add(this.groupBox1);
            this.tabPage4.Location = new System.Drawing.Point(4, 28);
            this.tabPage4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(1162, 488);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "凭证";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.button1);
            this.groupBox4.Location = new System.Drawing.Point(517, 310);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(417, 170);
            this.groupBox4.TabIndex = 3;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "协同";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(32, 38);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "创建";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txt_longBidText);
            this.groupBox2.Controls.Add(this.label32);
            this.groupBox2.Controls.Add(this.txt_approvalComment);
            this.groupBox2.Controls.Add(this.label31);
            this.groupBox2.Controls.Add(this.txt_shortBidText);
            this.groupBox2.Controls.Add(this.label30);
            this.groupBox2.Controls.Add(this.label29);
            this.groupBox2.Controls.Add(this.label28);
            this.groupBox2.Location = new System.Drawing.Point(48, 23);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(886, 271);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "文本总览";
            // 
            // txt_longBidText
            // 
            this.txt_longBidText.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt_longBidText.Location = new System.Drawing.Point(215, 145);
            this.txt_longBidText.Name = "txt_longBidText";
            this.txt_longBidText.Size = new System.Drawing.Size(611, 96);
            this.txt_longBidText.TabIndex = 7;
            this.txt_longBidText.Text = "";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(26, 148);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(53, 12);
            this.label32.TabIndex = 6;
            this.label32.Text = "投标文本";
            // 
            // txt_approvalComment
            // 
            this.txt_approvalComment.Location = new System.Drawing.Point(215, 105);
            this.txt_approvalComment.Name = "txt_approvalComment";
            this.txt_approvalComment.Size = new System.Drawing.Size(611, 21);
            this.txt_approvalComment.TabIndex = 5;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(28, 105);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(53, 12);
            this.label31.TabIndex = 4;
            this.label31.Text = "批准注释";
            // 
            // txt_shortBidText
            // 
            this.txt_shortBidText.Location = new System.Drawing.Point(215, 65);
            this.txt_shortBidText.Name = "txt_shortBidText";
            this.txt_shortBidText.Size = new System.Drawing.Size(611, 21);
            this.txt_shortBidText.TabIndex = 3;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(28, 68);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(53, 12);
            this.label30.TabIndex = 2;
            this.label30.Text = "投标文本";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(213, 33);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(53, 12);
            this.label29.TabIndex = 1;
            this.label29.Text = "文本预览";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(28, 33);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(53, 12);
            this.label28.TabIndex = 0;
            this.label28.Text = "文本类型";
            // 
            // supplierInforGroupBox
            // 
            this.groupBox1.Controls.Add(this.fileGridView);
            this.groupBox1.Controls.Add(this.btn_upload);
            this.groupBox1.Controls.Add(this.btn_ChooseFile);
            this.groupBox1.Location = new System.Drawing.Point(48, 310);
            this.groupBox1.Name = "supplierInforGroupBox";
            this.groupBox1.Size = new System.Drawing.Size(463, 227);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "附件";
            // 
            // fileGridView
            // 
            this.fileGridView.AllowUserToAddRows = false;
            this.fileGridView.AllowUserToResizeColumns = false;
            this.fileGridView.AllowUserToResizeRows = false;
            this.fileGridView.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.fileGridView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.fileGridView.ColumnHeadersHeight = 21;
            this.fileGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.fileGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.fileName,
            this.fileSize});
            this.fileGridView.EnableHeadersVisualStyles = false;
            this.fileGridView.Location = new System.Drawing.Point(10, 17);
            this.fileGridView.Name = "fileGridView";
            this.fileGridView.RowTemplate.Height = 23;
            this.fileGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.fileGridView.Size = new System.Drawing.Size(364, 150);
            this.fileGridView.TabIndex = 3;
            this.fileGridView.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.fileGridView_RowPostPaint);
            // 
            // fileName
            // 
            this.fileName.HeaderText = "文件名称";
            this.fileName.Name = "fileName";
            this.fileName.Width = 180;
            // 
            // fileSize
            // 
            this.fileSize.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.fileSize.HeaderText = "文件大小";
            this.fileSize.Name = "fileSize";
            // 
            // btn_upload
            // 
            this.btn_upload.Location = new System.Drawing.Point(380, 144);
            this.btn_upload.Name = "btn_upload";
            this.btn_upload.Size = new System.Drawing.Size(75, 23);
            this.btn_upload.TabIndex = 1;
            this.btn_upload.Text = "上传";
            this.btn_upload.UseVisualStyleBackColor = true;
            this.btn_upload.Click += new System.EventHandler(this.btn_upload_Click);
            // 
            // btn_ChooseFile
            // 
            this.btn_ChooseFile.Location = new System.Drawing.Point(382, 20);
            this.btn_ChooseFile.Name = "btn_ChooseFile";
            this.btn_ChooseFile.Size = new System.Drawing.Size(75, 23);
            this.btn_ChooseFile.TabIndex = 2;
            this.btn_ChooseFile.Text = "选择文件";
            this.btn_ChooseFile.UseVisualStyleBackColor = true;
            this.btn_ChooseFile.Click += new System.EventHandler(this.btn_ChooseFile_Click);
            // 
            // tabPage8
            // 
            this.tabPage8.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage8.Controls.Add(this.tac_EvalMethod);
            this.tabPage8.Controls.Add(this.cmb_EvalMethod);
            this.tabPage8.Controls.Add(this.label27);
            this.tabPage8.Location = new System.Drawing.Point(4, 28);
            this.tabPage8.Name = "tabPage8";
            this.tabPage8.Size = new System.Drawing.Size(1162, 488);
            this.tabPage8.TabIndex = 7;
            this.tabPage8.Text = "评价标准";
            // 
            // tac_EvalMethod
            // 
            this.tac_EvalMethod.Controls.Add(this.tab_LowPrice);
            this.tac_EvalMethod.Controls.Add(this.tab_WeightScore);
            this.tac_EvalMethod.Controls.Add(this.tab_LowTotalCost);
            this.tac_EvalMethod.Controls.Add(this.tab_ValueAssess);
            this.tac_EvalMethod.Location = new System.Drawing.Point(45, 56);
            this.tac_EvalMethod.Name = "tac_EvalMethod";
            this.tac_EvalMethod.SelectedIndex = 0;
            this.tac_EvalMethod.Size = new System.Drawing.Size(904, 343);
            this.tac_EvalMethod.TabIndex = 8;
            // 
            // tab_LowPrice
            // 
            this.tab_LowPrice.BackColor = System.Drawing.SystemColors.Control;
            this.tab_LowPrice.Controls.Add(this.label26);
            this.tab_LowPrice.Location = new System.Drawing.Point(4, 22);
            this.tab_LowPrice.Name = "tab_LowPrice";
            this.tab_LowPrice.Padding = new System.Windows.Forms.Padding(3);
            this.tab_LowPrice.Size = new System.Drawing.Size(896, 317);
            this.tab_LowPrice.TabIndex = 0;
            this.tab_LowPrice.Text = "最低价格法";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(74, 34);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(185, 12);
            this.label26.TabIndex = 0;
            this.label26.Text = "说明：当前评价标准为最低价格法";
            // 
            // tab_WeightScore
            // 
            this.tab_WeightScore.BackColor = System.Drawing.SystemColors.Control;
            this.tab_WeightScore.Controls.Add(this.gbx_WeightInfo);
            this.tab_WeightScore.Location = new System.Drawing.Point(4, 22);
            this.tab_WeightScore.Name = "tab_WeightScore";
            this.tab_WeightScore.Padding = new System.Windows.Forms.Padding(3);
            this.tab_WeightScore.Size = new System.Drawing.Size(896, 317);
            this.tab_WeightScore.TabIndex = 1;
            this.tab_WeightScore.Text = "加权评分法";
            // 
            // gbx_WeightInfo
            // 
            this.gbx_WeightInfo.Controls.Add(this.chb_Weight);
            this.gbx_WeightInfo.Controls.Add(this.btn_DeleteWeight);
            this.gbx_WeightInfo.Controls.Add(this.btn_AddWeight);
            this.gbx_WeightInfo.Controls.Add(this.WeightScoreGridView);
            this.gbx_WeightInfo.Location = new System.Drawing.Point(43, 27);
            this.gbx_WeightInfo.Name = "gbx_WeightInfo";
            this.gbx_WeightInfo.Size = new System.Drawing.Size(712, 272);
            this.gbx_WeightInfo.TabIndex = 5;
            this.gbx_WeightInfo.TabStop = false;
            this.gbx_WeightInfo.Text = "评估报价的指标与权重";
            // 
            // chb_Weight
            // 
            this.chb_Weight.AutoSize = true;
            this.chb_Weight.Location = new System.Drawing.Point(454, 150);
            this.chb_Weight.Name = "chb_Weight";
            this.chb_Weight.Size = new System.Drawing.Size(96, 16);
            this.chb_Weight.TabIndex = 4;
            this.chb_Weight.Text = "权重是否可见";
            this.chb_Weight.UseVisualStyleBackColor = true;
            // 
            // btn_DeleteWeight
            // 
            this.btn_DeleteWeight.Location = new System.Drawing.Point(547, 36);
            this.btn_DeleteWeight.Name = "btn_DeleteWeight";
            this.btn_DeleteWeight.Size = new System.Drawing.Size(75, 23);
            this.btn_DeleteWeight.TabIndex = 2;
            this.btn_DeleteWeight.Text = "删除";
            this.btn_DeleteWeight.UseVisualStyleBackColor = true;
            this.btn_DeleteWeight.Click += new System.EventHandler(this.btn_DeleteWeight_Click);
            // 
            // btn_AddWeight
            // 
            this.btn_AddWeight.Location = new System.Drawing.Point(454, 36);
            this.btn_AddWeight.Name = "btn_AddWeight";
            this.btn_AddWeight.Size = new System.Drawing.Size(75, 23);
            this.btn_AddWeight.TabIndex = 1;
            this.btn_AddWeight.Text = "添加";
            this.btn_AddWeight.UseVisualStyleBackColor = true;
            this.btn_AddWeight.Click += new System.EventHandler(this.btn_AddWeight_Click);
            // 
            // WeightScoreGridView
            // 
            this.WeightScoreGridView.AllowUserToAddRows = false;
            this.WeightScoreGridView.BackgroundColor = System.Drawing.SystemColors.Control;
            this.WeightScoreGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.WeightScoreGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.weightEvalName,
            this.weightEvalNum});
            this.WeightScoreGridView.Location = new System.Drawing.Point(58, 36);
            this.WeightScoreGridView.Name = "WeightScoreGridView";
            this.WeightScoreGridView.RowTemplate.Height = 23;
            this.WeightScoreGridView.Size = new System.Drawing.Size(346, 150);
            this.WeightScoreGridView.TabIndex = 0;
            // 
            // weightEvalName
            // 
            this.weightEvalName.HeaderText = "评估标准";
            this.weightEvalName.Name = "weightEvalName";
            this.weightEvalName.Width = 200;
            // 
            // weightEvalNum
            // 
            this.weightEvalNum.HeaderText = "权重";
            this.weightEvalNum.Name = "weightEvalNum";
            // 
            // tab_LowTotalCost
            // 
            this.tab_LowTotalCost.BackColor = System.Drawing.SystemColors.Control;
            this.tab_LowTotalCost.Controls.Add(this.gbx_leastTotalCost);
            this.tab_LowTotalCost.Location = new System.Drawing.Point(4, 22);
            this.tab_LowTotalCost.Name = "tab_LowTotalCost";
            this.tab_LowTotalCost.Padding = new System.Windows.Forms.Padding(3);
            this.tab_LowTotalCost.Size = new System.Drawing.Size(896, 317);
            this.tab_LowTotalCost.TabIndex = 2;
            this.tab_LowTotalCost.Text = "最低所有权总成本法";
            // 
            // gbx_leastTotalCost
            // 
            this.gbx_leastTotalCost.Controls.Add(this.button2);
            this.gbx_leastTotalCost.Controls.Add(this.btn_AddCost);
            this.gbx_leastTotalCost.Controls.Add(this.cmb_Costs);
            this.gbx_leastTotalCost.Controls.Add(this.label12);
            this.gbx_leastTotalCost.Controls.Add(this.allCostGridView);
            this.gbx_leastTotalCost.Location = new System.Drawing.Point(9, 24);
            this.gbx_leastTotalCost.Name = "gbx_leastTotalCost";
            this.gbx_leastTotalCost.Size = new System.Drawing.Size(881, 287);
            this.gbx_leastTotalCost.TabIndex = 8;
            this.gbx_leastTotalCost.TabStop = false;
            this.gbx_leastTotalCost.Text = "最低所有权总成本";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(746, 26);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(59, 23);
            this.button2.TabIndex = 18;
            this.button2.Text = "保存";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // btn_AddCost
            // 
            this.btn_AddCost.Location = new System.Drawing.Point(267, 21);
            this.btn_AddCost.Name = "btn_AddCost";
            this.btn_AddCost.Size = new System.Drawing.Size(75, 23);
            this.btn_AddCost.TabIndex = 14;
            this.btn_AddCost.Text = "添加成本项";
            this.btn_AddCost.UseVisualStyleBackColor = true;
            this.btn_AddCost.Click += new System.EventHandler(this.btn_AddCost_Click);
            // 
            // cmb_Costs
            // 
            this.cmb_Costs.FormattingEnabled = true;
            this.cmb_Costs.Items.AddRange(new object[] {
            "采购价格",
            "维护成本",
            "运营成本",
            "处置成本"});
            this.cmb_Costs.Location = new System.Drawing.Point(108, 23);
            this.cmb_Costs.Name = "cmb_Costs";
            this.cmb_Costs.Size = new System.Drawing.Size(121, 20);
            this.cmb_Costs.TabIndex = 12;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(61, 26);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(41, 12);
            this.label12.TabIndex = 11;
            this.label12.Text = "成本项";
            // 
            // allCostGridView
            // 
            this.allCostGridView.AllowUserToAddRows = false;
            this.allCostGridView.BackgroundColor = System.Drawing.SystemColors.Control;
            this.allCostGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.allCostGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CostItem,
            this.First,
            this.Second,
            this.Third,
            this.forth,
            this.fifth,
            this.sixth,
            this.seventh,
            this.eighth,
            this.ninth,
            this.tenth});
            this.allCostGridView.Location = new System.Drawing.Point(6, 65);
            this.allCostGridView.Name = "allCostGridView";
            this.allCostGridView.RowTemplate.Height = 23;
            this.allCostGridView.Size = new System.Drawing.Size(851, 216);
            this.allCostGridView.TabIndex = 10;
            // 
            // CostItem
            // 
            this.CostItem.HeaderText = "成本项";
            this.CostItem.Name = "CostItem";
            this.CostItem.Width = 80;
            // 
            // First
            // 
            this.First.HeaderText = "第一年";
            this.First.Name = "First";
            this.First.Width = 70;
            // 
            // Second
            // 
            this.Second.HeaderText = "第二年";
            this.Second.Name = "Second";
            this.Second.Width = 70;
            // 
            // Third
            // 
            this.Third.HeaderText = "第三年";
            this.Third.Name = "Third";
            this.Third.Width = 70;
            // 
            // forth
            // 
            this.forth.HeaderText = "第四年";
            this.forth.Name = "forth";
            this.forth.Width = 70;
            // 
            // fifth
            // 
            this.fifth.HeaderText = "第五年";
            this.fifth.Name = "fifth";
            this.fifth.Width = 70;
            // 
            // sixth
            // 
            this.sixth.HeaderText = "第六年";
            this.sixth.Name = "sixth";
            this.sixth.Width = 70;
            // 
            // seventh
            // 
            this.seventh.HeaderText = "第七年";
            this.seventh.Name = "seventh";
            this.seventh.Width = 70;
            // 
            // eighth
            // 
            this.eighth.HeaderText = "第八年";
            this.eighth.Name = "eighth";
            this.eighth.Width = 70;
            // 
            // ninth
            // 
            this.ninth.HeaderText = "第九年";
            this.ninth.Name = "ninth";
            this.ninth.Width = 70;
            // 
            // tenth
            // 
            this.tenth.HeaderText = "第十年";
            this.tenth.Name = "tenth";
            this.tenth.Width = 70;
            // 
            // tab_ValueAssess
            // 
            this.tab_ValueAssess.Location = new System.Drawing.Point(4, 22);
            this.tab_ValueAssess.Name = "tab_ValueAssess";
            this.tab_ValueAssess.Padding = new System.Windows.Forms.Padding(3);
            this.tab_ValueAssess.Size = new System.Drawing.Size(896, 317);
            this.tab_ValueAssess.TabIndex = 3;
            this.tab_ValueAssess.Text = "价值评估法";
            this.tab_ValueAssess.UseVisualStyleBackColor = true;
            // 
            // cmb_EvalMethod
            // 
            this.cmb_EvalMethod.FormattingEnabled = true;
            this.cmb_EvalMethod.Items.AddRange(new object[] {
            "最低价格法",
            "加权评分法",
            "最低所有权总成本法",
            "价值评估法"});
            this.cmb_EvalMethod.Location = new System.Drawing.Point(75, 13);
            this.cmb_EvalMethod.Name = "cmb_EvalMethod";
            this.cmb_EvalMethod.Size = new System.Drawing.Size(119, 20);
            this.cmb_EvalMethod.TabIndex = 6;
            this.cmb_EvalMethod.SelectedIndexChanged += new System.EventHandler(this.cmb_EvalMethod_SelectedIndexChanged);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(16, 16);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(53, 12);
            this.label27.TabIndex = 5;
            this.label27.Text = "评估方法";
            // 
            // tabPage12
            // 
            this.tabPage12.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage12.Controls.Add(this.button4);
            this.tabPage12.Controls.Add(this.cmb_Materials);
            this.tabPage12.Controls.Add(this.label24);
            this.tabPage12.Controls.Add(this.materialSourceView);
            this.tabPage12.Location = new System.Drawing.Point(4, 28);
            this.tabPage12.Name = "tabPage12";
            this.tabPage12.Size = new System.Drawing.Size(1162, 488);
            this.tabPage12.TabIndex = 11;
            this.tabPage12.Text = "历史记录";
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(434, 30);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 3;
            this.button4.Text = "查看";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // cmb_Materials
            // 
            this.cmb_Materials.FormattingEnabled = true;
            this.cmb_Materials.Location = new System.Drawing.Point(261, 33);
            this.cmb_Materials.Name = "cmb_Materials";
            this.cmb_Materials.Size = new System.Drawing.Size(121, 20);
            this.cmb_Materials.TabIndex = 2;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(188, 36);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(53, 12);
            this.label24.TabIndex = 1;
            this.label24.Text = "物料编号";
            // 
            // materialSourceView
            // 
            this.materialSourceView.AllowUserToAddRows = false;
            this.materialSourceView.BackgroundColor = System.Drawing.SystemColors.Control;
            this.materialSourceView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.materialSourceView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.bidId,
            this.bidName,
            this.createTime,
            this.State});
            this.materialSourceView.Location = new System.Drawing.Point(190, 75);
            this.materialSourceView.Name = "materialSourceView";
            this.materialSourceView.RowTemplate.Height = 23;
            this.materialSourceView.Size = new System.Drawing.Size(576, 225);
            this.materialSourceView.TabIndex = 0;
            // 
            // bidId
            // 
            this.bidId.HeaderText = "招标编号";
            this.bidId.Name = "bidId";
            this.bidId.Width = 120;
            // 
            // bidName
            // 
            this.bidName.HeaderText = "招标名称";
            this.bidName.Name = "bidName";
            // 
            // createTime
            // 
            this.createTime.HeaderText = "创建时间";
            this.createTime.Name = "createTime";
            this.createTime.Width = 150;
            // 
            // State
            // 
            this.State.HeaderText = "状态";
            this.State.Name = "State";
            // 
            // tabPage10
            // 
            this.tabPage10.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage10.Controls.Add(this.comboBox1);
            this.tabPage10.Controls.Add(this.label25);
            this.tabPage10.Location = new System.Drawing.Point(4, 28);
            this.tabPage10.Name = "tabPage10";
            this.tabPage10.Size = new System.Drawing.Size(1162, 488);
            this.tabPage10.TabIndex = 9;
            this.tabPage10.Text = "批准预览";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "一级审批",
            "二级审批",
            "三级审批"});
            this.comboBox1.Location = new System.Drawing.Point(214, 60);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(121, 20);
            this.comboBox1.TabIndex = 1;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(140, 63);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(53, 12);
            this.label25.TabIndex = 0;
            this.label25.Text = "审批级别";
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage2.Controls.Add(this.MaterialGridview);
            this.tabPage2.Location = new System.Drawing.Point(4, 28);
            this.tabPage2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage2.Size = new System.Drawing.Size(1162, 488);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "项目数据";
            // 
            // MaterialGridview
            // 
            this.MaterialGridview.AllowUserToAddRows = false;
            this.MaterialGridview.BackgroundColor = System.Drawing.SystemColors.Control;
            this.MaterialGridview.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.MaterialGridview.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.demandNum,
            this.bidMaterialId,
            this.bidMaterialName,
            this.bidDemandCount,
            this.bidDemandMeasurement,
            this.factoryId,
            this.stockId,
            this.DeliveryStartTime,
            this.DeliveryEndTime});
            this.MaterialGridview.Location = new System.Drawing.Point(19, 22);
            this.MaterialGridview.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.MaterialGridview.Name = "MaterialGridview";
            this.MaterialGridview.RowHeadersVisible = false;
            this.MaterialGridview.RowTemplate.Height = 23;
            this.MaterialGridview.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.MaterialGridview.Size = new System.Drawing.Size(927, 266);
            this.MaterialGridview.TabIndex = 200;
            // 
            // demandNum
            // 
            this.demandNum.Frozen = true;
            this.demandNum.HeaderText = "";
            this.demandNum.Name = "demandNum";
            this.demandNum.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.demandNum.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.demandNum.Width = 30;
            // 
            // bidMaterialId
            // 
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.White;
            this.bidMaterialId.DefaultCellStyle = dataGridViewCellStyle4;
            this.bidMaterialId.Frozen = true;
            this.bidMaterialId.HeaderText = "物料编号";
            this.bidMaterialId.Name = "bidMaterialId";
            this.bidMaterialId.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.bidMaterialId.Width = 140;
            // 
            // bidMaterialName
            // 
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.White;
            this.bidMaterialName.DefaultCellStyle = dataGridViewCellStyle5;
            this.bidMaterialName.Frozen = true;
            this.bidMaterialName.HeaderText = "物料名称";
            this.bidMaterialName.Name = "bidMaterialName";
            this.bidMaterialName.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.bidMaterialName.Width = 140;
            // 
            // bidDemandCount
            // 
            this.bidDemandCount.Frozen = true;
            this.bidDemandCount.HeaderText = "数量";
            this.bidDemandCount.Name = "bidDemandCount";
            this.bidDemandCount.Width = 80;
            // 
            // bidDemandMeasurement
            // 
            this.bidDemandMeasurement.Frozen = true;
            this.bidDemandMeasurement.HeaderText = "单位";
            this.bidDemandMeasurement.Name = "bidDemandMeasurement";
            this.bidDemandMeasurement.ReadOnly = true;
            this.bidDemandMeasurement.Width = 60;
            // 
            // factoryId
            // 
            this.factoryId.HeaderText = "工厂编号";
            this.factoryId.Name = "factoryId";
            // 
            // stockId
            // 
            this.stockId.HeaderText = "仓存编号";
            this.stockId.Name = "stockId";
            // 
            // DeliveryStartTime
            // 
            this.DeliveryStartTime.HeaderText = "交易开始日期";
            this.DeliveryStartTime.Name = "DeliveryStartTime";
            this.DeliveryStartTime.Width = 120;
            // 
            // DeliveryEndTime
            // 
            this.DeliveryEndTime.HeaderText = "交易结束日期";
            this.DeliveryEndTime.Name = "DeliveryEndTime";
            this.DeliveryEndTime.Width = 120;
            // 
            // btnCheck
            // 
            this.btnCheck.ForeColor = System.Drawing.Color.LightSeaGreen;
            this.btnCheck.Location = new System.Drawing.Point(14, 11);
            this.btnCheck.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnCheck.Name = "btnCheck";
            this.btnCheck.Size = new System.Drawing.Size(63, 22);
            this.btnCheck.TabIndex = 197;
            this.btnCheck.Text = "检查";
            this.btnCheck.UseVisualStyleBackColor = true;
            this.btnCheck.Click += new System.EventHandler(this.btnCheck_Click);
            // 
            // base_save
            // 
            this.base_save.ForeColor = System.Drawing.Color.LightSeaGreen;
            this.base_save.Location = new System.Drawing.Point(106, 11);
            this.base_save.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.base_save.Name = "base_save";
            this.base_save.Size = new System.Drawing.Size(63, 22);
            this.base_save.TabIndex = 195;
            this.base_save.Text = "保存";
            this.base_save.UseVisualStyleBackColor = true;
            this.base_save.Visible = false;
            this.base_save.Click += new System.EventHandler(this.base_save_Click);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.BiddingTab);
            this.panel3.Location = new System.Drawing.Point(14, 79);
            this.panel3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1075, 526);
            this.panel3.TabIndex = 196;
            // 
            // btn_model
            // 
            this.btn_model.Location = new System.Drawing.Point(785, 51);
            this.btn_model.Name = "btn_model";
            this.btn_model.Size = new System.Drawing.Size(75, 23);
            this.btn_model.TabIndex = 198;
            this.btn_model.Text = "创建模版";
            this.btn_model.UseVisualStyleBackColor = true;
            // 
            // dataGridViewTextBoxColumn1
            // 
            dataGridViewCellStyle6.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.dataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle6;
            this.dataGridViewTextBoxColumn1.Frozen = true;
            this.dataGridViewTextBoxColumn1.HeaderText = "";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn1.Width = 30;
            // 
            // dataGridViewTextBoxColumn2
            // 
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.White;
            this.dataGridViewTextBoxColumn2.DefaultCellStyle = dataGridViewCellStyle7;
            this.dataGridViewTextBoxColumn2.Frozen = true;
            this.dataGridViewTextBoxColumn2.HeaderText = "供应商编号";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // dataGridViewTextBoxColumn3
            // 
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.White;
            this.dataGridViewTextBoxColumn3.DefaultCellStyle = dataGridViewCellStyle8;
            this.dataGridViewTextBoxColumn3.Frozen = true;
            this.dataGridViewTextBoxColumn3.HeaderText = "供应商名称";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn3.Width = 120;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.Frozen = true;
            this.dataGridViewTextBoxColumn4.HeaderText = "供应商评级";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.Frozen = true;
            this.dataGridViewTextBoxColumn5.HeaderText = "联系人";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            this.dataGridViewTextBoxColumn5.Width = 95;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.Frozen = true;
            this.dataGridViewTextBoxColumn6.HeaderText = "电子信箱";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.Frozen = true;
            this.dataGridViewTextBoxColumn7.HeaderText = "地址";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            this.dataGridViewTextBoxColumn7.Width = 130;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.HeaderText = "投标状态";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.HeaderText = "操作";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.HeaderText = "";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.Width = 50;
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn11.HeaderText = "文件名称";
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.HeaderText = "文件大小";
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            this.dataGridViewTextBoxColumn12.Width = 200;
            // 
            // dataGridViewTextBoxColumn13
            // 
            this.dataGridViewTextBoxColumn13.HeaderText = "";
            this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
            this.dataGridViewTextBoxColumn13.Width = 30;
            // 
            // dataGridViewTextBoxColumn14
            // 
            this.dataGridViewTextBoxColumn14.HeaderText = "名称";
            this.dataGridViewTextBoxColumn14.Name = "dataGridViewTextBoxColumn14";
            // 
            // dataGridViewTextBoxColumn15
            // 
            this.dataGridViewTextBoxColumn15.HeaderText = "加权比例";
            this.dataGridViewTextBoxColumn15.Name = "dataGridViewTextBoxColumn15";
            // 
            // dataGridViewTextBoxColumn16
            // 
            this.dataGridViewTextBoxColumn16.HeaderText = "";
            this.dataGridViewTextBoxColumn16.Name = "dataGridViewTextBoxColumn16";
            // 
            // dataGridViewTextBoxColumn17
            // 
            this.dataGridViewTextBoxColumn17.HeaderText = "招标编号";
            this.dataGridViewTextBoxColumn17.Name = "dataGridViewTextBoxColumn17";
            // 
            // dataGridViewTextBoxColumn18
            // 
            this.dataGridViewTextBoxColumn18.HeaderText = "招标名称";
            this.dataGridViewTextBoxColumn18.Name = "dataGridViewTextBoxColumn18";
            // 
            // dataGridViewTextBoxColumn19
            // 
            this.dataGridViewTextBoxColumn19.HeaderText = "创建时间";
            this.dataGridViewTextBoxColumn19.Name = "dataGridViewTextBoxColumn19";
            this.dataGridViewTextBoxColumn19.Width = 80;
            // 
            // dataGridViewTextBoxColumn20
            // 
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.White;
            this.dataGridViewTextBoxColumn20.DefaultCellStyle = dataGridViewCellStyle9;
            this.dataGridViewTextBoxColumn20.Frozen = true;
            this.dataGridViewTextBoxColumn20.HeaderText = "状态";
            this.dataGridViewTextBoxColumn20.Name = "dataGridViewTextBoxColumn20";
            this.dataGridViewTextBoxColumn20.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn20.Width = 140;
            // 
            // dataGridViewTextBoxColumn21
            // 
            dataGridViewCellStyle10.BackColor = System.Drawing.Color.White;
            this.dataGridViewTextBoxColumn21.DefaultCellStyle = dataGridViewCellStyle10;
            this.dataGridViewTextBoxColumn21.Frozen = true;
            this.dataGridViewTextBoxColumn21.HeaderText = "物料编号";
            this.dataGridViewTextBoxColumn21.Name = "dataGridViewTextBoxColumn21";
            this.dataGridViewTextBoxColumn21.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn21.Width = 140;
            // 
            // dataGridViewTextBoxColumn22
            // 
            dataGridViewCellStyle11.BackColor = System.Drawing.Color.White;
            this.dataGridViewTextBoxColumn22.DefaultCellStyle = dataGridViewCellStyle11;
            this.dataGridViewTextBoxColumn22.Frozen = true;
            this.dataGridViewTextBoxColumn22.HeaderText = "物料名称";
            this.dataGridViewTextBoxColumn22.Name = "dataGridViewTextBoxColumn22";
            this.dataGridViewTextBoxColumn22.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn22.Width = 140;
            // 
            // dataGridViewTextBoxColumn23
            // 
            this.dataGridViewTextBoxColumn23.Frozen = true;
            this.dataGridViewTextBoxColumn23.HeaderText = "数量";
            this.dataGridViewTextBoxColumn23.Name = "dataGridViewTextBoxColumn23";
            this.dataGridViewTextBoxColumn23.ReadOnly = true;
            this.dataGridViewTextBoxColumn23.Width = 80;
            // 
            // dataGridViewTextBoxColumn24
            // 
            this.dataGridViewTextBoxColumn24.Frozen = true;
            this.dataGridViewTextBoxColumn24.HeaderText = "单位";
            this.dataGridViewTextBoxColumn24.Name = "dataGridViewTextBoxColumn24";
            this.dataGridViewTextBoxColumn24.ReadOnly = true;
            this.dataGridViewTextBoxColumn24.Width = 60;
            // 
            // calendarColumn1
            // 
            dataGridViewCellStyle12.Format = "yyyy-MM-dd hh:mm:ss";
            dataGridViewCellStyle12.NullValue = null;
            this.calendarColumn1.DefaultCellStyle = dataGridViewCellStyle12;
            this.calendarColumn1.Frozen = true;
            this.calendarColumn1.HeaderText = "交货日期";
            this.calendarColumn1.Name = "calendarColumn1";
            this.calendarColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.calendarColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // txt_State
            // 
            this.txt_State.Location = new System.Drawing.Point(561, 53);
            this.txt_State.Name = "txt_State";
            this.txt_State.ReadOnly = true;
            this.txt_State.Size = new System.Drawing.Size(80, 21);
            this.txt_State.TabIndex = 211;
            this.txt_State.Text = "待保存";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(526, 56);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(29, 12);
            this.label45.TabIndex = 210;
            this.label45.Text = "状态";
            // 
            // dataGridViewComboBoxColumn1
            // 
            dataGridViewCellStyle13.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle13.SelectionBackColor = System.Drawing.Color.White;
            this.dataGridViewComboBoxColumn1.DefaultCellStyle = dataGridViewCellStyle13;
            this.dataGridViewComboBoxColumn1.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn1.Frozen = true;
            this.dataGridViewComboBoxColumn1.HeaderText = "供应商编号";
            this.dataGridViewComboBoxColumn1.Name = "dataGridViewComboBoxColumn1";
            this.dataGridViewComboBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewComboBoxColumn1.Width = 80;
            // 
            // dataGridViewComboBoxColumn2
            // 
            dataGridViewCellStyle14.BackColor = System.Drawing.Color.White;
            this.dataGridViewComboBoxColumn2.DefaultCellStyle = dataGridViewCellStyle14;
            this.dataGridViewComboBoxColumn2.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn2.Frozen = true;
            this.dataGridViewComboBoxColumn2.HeaderText = "供应商名称";
            this.dataGridViewComboBoxColumn2.Name = "dataGridViewComboBoxColumn2";
            this.dataGridViewComboBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewComboBoxColumn2.Width = 160;
            // 
            // dataGridViewComboBoxColumn3
            // 
            dataGridViewCellStyle15.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle15.SelectionBackColor = System.Drawing.Color.White;
            this.dataGridViewComboBoxColumn3.DefaultCellStyle = dataGridViewCellStyle15;
            this.dataGridViewComboBoxColumn3.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn3.Frozen = true;
            this.dataGridViewComboBoxColumn3.HeaderText = "物料编号";
            this.dataGridViewComboBoxColumn3.Name = "dataGridViewComboBoxColumn3";
            this.dataGridViewComboBoxColumn3.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewComboBoxColumn3.Width = 140;
            // 
            // dataGridViewComboBoxColumn4
            // 
            dataGridViewCellStyle16.BackColor = System.Drawing.Color.White;
            this.dataGridViewComboBoxColumn4.DefaultCellStyle = dataGridViewCellStyle16;
            this.dataGridViewComboBoxColumn4.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn4.Frozen = true;
            this.dataGridViewComboBoxColumn4.HeaderText = "物料名称";
            this.dataGridViewComboBoxColumn4.Name = "dataGridViewComboBoxColumn4";
            this.dataGridViewComboBoxColumn4.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewComboBoxColumn4.Width = 140;
            // 
            // dataGridViewComboBoxColumn5
            // 
            dataGridViewCellStyle17.BackColor = System.Drawing.Color.White;
            this.dataGridViewComboBoxColumn5.DefaultCellStyle = dataGridViewCellStyle17;
            this.dataGridViewComboBoxColumn5.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn5.Frozen = true;
            this.dataGridViewComboBoxColumn5.HeaderText = "工厂";
            this.dataGridViewComboBoxColumn5.Name = "dataGridViewComboBoxColumn5";
            this.dataGridViewComboBoxColumn5.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewComboBoxColumn5.Width = 80;
            // 
            // dataGridViewComboBoxColumn6
            // 
            dataGridViewCellStyle18.BackColor = System.Drawing.Color.White;
            this.dataGridViewComboBoxColumn6.DefaultCellStyle = dataGridViewCellStyle18;
            this.dataGridViewComboBoxColumn6.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn6.Frozen = true;
            this.dataGridViewComboBoxColumn6.HeaderText = "仓库";
            this.dataGridViewComboBoxColumn6.Name = "dataGridViewComboBoxColumn6";
            this.dataGridViewComboBoxColumn6.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewComboBoxColumn6.Width = 80;
            // 
            // Bidding_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1211, 627);
            this.Controls.Add(this.txt_State);
            this.Controls.Add(this.label45);
            this.Controls.Add(this.btn_model);
            this.Controls.Add(this.txt_BidName);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txt_BidId);
            this.Controls.Add(this.xjdh_lbl);
            this.Controls.Add(this.base_save);
            this.Controls.Add(this.btnCheck);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "Bidding_Form";
            this.Text = "创建招标";
            this.BiddingTab.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage6.ResumeLayout(false);
            this.tabPage6.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.tabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SupplierGridView)).EndInit();
            this.tabPage4.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.fileGridView)).EndInit();
            this.tabPage8.ResumeLayout(false);
            this.tabPage8.PerformLayout();
            this.tac_EvalMethod.ResumeLayout(false);
            this.tab_LowPrice.ResumeLayout(false);
            this.tab_LowPrice.PerformLayout();
            this.tab_WeightScore.ResumeLayout(false);
            this.gbx_WeightInfo.ResumeLayout(false);
            this.gbx_WeightInfo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.WeightScoreGridView)).EndInit();
            this.tab_LowTotalCost.ResumeLayout(false);
            this.gbx_leastTotalCost.ResumeLayout(false);
            this.gbx_leastTotalCost.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.allCostGridView)).EndInit();
            this.tabPage12.ResumeLayout(false);
            this.tabPage12.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.materialSourceView)).EndInit();
            this.tabPage10.ResumeLayout(false);
            this.tabPage10.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.MaterialGridview)).EndInit();
            this.panel3.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label xjdh_lbl;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn1;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn2;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn3;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn4;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn5;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn6;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TabControl BiddingTab;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.DataGridView SupplierGridView;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ComboBox cmb_currency;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ComboBox cmb_timeZone;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.DateTimePicker dtp_startBeginTime;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.DateTimePicker dtp_startTime;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.DateTimePicker dtp_limitTime;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox cmb_purchaseGroup;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cmb_purchaseOrg;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox cmb_displayType;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cmb_catalogue;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DateTimePicker dtp_endTime;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button base_save;
        private System.Windows.Forms.DataGridView MaterialGridview;
        private System.Windows.Forms.TextBox txt_serviceType;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.TabPage tabPage8;
        private System.Windows.Forms.TabPage tabPage12;
        private System.Windows.Forms.TabPage tabPage10;
        private System.Windows.Forms.DateTimePicker dtp_commBidStartDate;
        private System.Windows.Forms.DateTimePicker dtp_enrollEndDate;
        private System.Windows.Forms.DateTimePicker dtp_enrollStartDate;
        private System.Windows.Forms.TextBox txt_bidCost;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.DateTimePicker dtp_buyEndDate;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.DateTimePicker dtp_techBidStartDate;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox txt_shortBidText;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txt_approvalComment;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.RichTextBox txt_longBidText;
        private System.Windows.Forms.TextBox txt_BidId;
        private System.Windows.Forms.Button btnCheck;
        private System.Windows.Forms.DataGridView materialSourceView;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.TextBox txt_requesterName;
        private System.Windows.Forms.TextBox txt_requesterId;
        private System.Windows.Forms.TextBox txt_dropPointName;
        private System.Windows.Forms.TextBox txt_dropPointId;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.TextBox txt_receiverName;
        private System.Windows.Forms.TextBox txt_receiverId;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.TextBox txt_callerName;
        private System.Windows.Forms.TextBox txt_callerId;
        private System.Windows.Forms.TextBox txt_BidName;
        private System.Windows.Forms.TextBox txt_enrollEndTime;
        private System.Windows.Forms.TextBox txt_buyEndTime;
        private System.Windows.Forms.TextBox txt_techBidStartTime;
        private System.Windows.Forms.TextBox txt_enrollStartTime;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.TextBox txt_commBidStartTime;
        private System.Windows.Forms.Button btn_addSupplier;
        private System.Windows.Forms.Button btn_model;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btn_upload;
        private System.Windows.Forms.Button btn_ChooseFile;
        private System.Windows.Forms.DataGridView fileGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn14;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn15;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn16;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn17;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn18;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn19;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn20;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn21;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn22;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn23;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn24;
        private Lib.Common.CommonControls.CalendarColumn calendarColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn fileName;
        private System.Windows.Forms.DataGridViewTextBoxColumn fileSize;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.ComboBox cmb_EvalMethod;
        private System.Windows.Forms.TabControl tac_EvalMethod;
        private System.Windows.Forms.TabPage tab_LowPrice;
        private System.Windows.Forms.TabPage tab_WeightScore;
        private System.Windows.Forms.GroupBox gbx_WeightInfo;
        private System.Windows.Forms.Button btn_DeleteWeight;
        private System.Windows.Forms.Button btn_AddWeight;
        private System.Windows.Forms.DataGridView WeightScoreGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn weightEvalName;
        private System.Windows.Forms.DataGridViewTextBoxColumn weightEvalNum;
        private System.Windows.Forms.TabPage tab_LowTotalCost;
        private System.Windows.Forms.GroupBox gbx_leastTotalCost;
        private System.Windows.Forms.TabPage tab_ValueAssess;
        private System.Windows.Forms.CheckBox chb_Weight;
        private System.Windows.Forms.DataGridView allCostGridView;
        private System.Windows.Forms.ComboBox cmb_Costs;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button btn_AddCost;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.ComboBox cmb_TransType;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.DataGridViewTextBoxColumn CostItem;
        private System.Windows.Forms.DataGridViewTextBoxColumn First;
        private System.Windows.Forms.DataGridViewTextBoxColumn Second;
        private System.Windows.Forms.DataGridViewTextBoxColumn Third;
        private System.Windows.Forms.DataGridViewTextBoxColumn forth;
        private System.Windows.Forms.DataGridViewTextBoxColumn fifth;
        private System.Windows.Forms.DataGridViewTextBoxColumn sixth;
        private System.Windows.Forms.DataGridViewTextBoxColumn seventh;
        private System.Windows.Forms.DataGridViewTextBoxColumn eighth;
        private System.Windows.Forms.DataGridViewTextBoxColumn ninth;
        private System.Windows.Forms.DataGridViewTextBoxColumn tenth;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.ComboBox cmb_Materials;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.DataGridViewTextBoxColumn bidId;
        private System.Windows.Forms.DataGridViewTextBoxColumn bidName;
        private System.Windows.Forms.DataGridViewTextBoxColumn createTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn State;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn26;
        private System.Windows.Forms.DataGridViewTextBoxColumn bidSupplierId;
        private System.Windows.Forms.DataGridViewTextBoxColumn bidSupplierName;
        private System.Windows.Forms.DataGridViewTextBoxColumn bidContact;
        private System.Windows.Forms.DataGridViewTextBoxColumn bidEmail;
        private System.Windows.Forms.DataGridViewTextBoxColumn bidAddress;
        private System.Windows.Forms.DataGridViewTextBoxColumn bidState;
        private System.Windows.Forms.TextBox txt_State;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btn_AddReviewer;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Button btn_DeleteReviewer;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn reviewerName;
        private System.Windows.Forms.DataGridViewCheckBoxColumn demandNum;
        private System.Windows.Forms.DataGridViewTextBoxColumn bidMaterialId;
        private System.Windows.Forms.DataGridViewTextBoxColumn bidMaterialName;
        private System.Windows.Forms.DataGridViewTextBoxColumn bidDemandCount;
        private System.Windows.Forms.DataGridViewTextBoxColumn bidDemandMeasurement;
        private System.Windows.Forms.DataGridViewTextBoxColumn factoryId;
        private System.Windows.Forms.DataGridViewTextBoxColumn stockId;
        private System.Windows.Forms.DataGridViewTextBoxColumn DeliveryStartTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn DeliveryEndTime;
        private System.Windows.Forms.Label label26;
    }
}