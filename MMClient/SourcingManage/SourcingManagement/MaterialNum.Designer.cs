﻿namespace MMClient.SourcingManage.SourcingManagement
{
    partial class MaterialNum
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.MaterialNum_MaterialId = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.MaterialNum_MaterialGroup = new System.Windows.Forms.TextBox();
            this.MaterialNum_MaterialType = new System.Windows.Forms.TextBox();
            this.MaterialNum_MaterialName = new System.Windows.Forms.TextBox();
            this.MaterialNum_DemandCount = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.MaterialNum_DemandId = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.MaterialNum_FactoryId = new System.Windows.Forms.TextBox();
            this.MaterialNum_StockId = new System.Windows.Forms.TextBox();
            this.MaterialNum_Unit = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.MaterialNum_DeliveryStartTime = new System.Windows.Forms.DateTimePicker();
            this.MaterialNum_DeliveryEndTime = new System.Windows.Forms.DateTimePicker();
            this.label12 = new System.Windows.Forms.Label();
            this.MaterialNum_SourceId = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // MaterialNum_MaterialId
            // 
            this.MaterialNum_MaterialId.Location = new System.Drawing.Point(96, 10);
            this.MaterialNum_MaterialId.Name = "MaterialNum_MaterialId";
            this.MaterialNum_MaterialId.Size = new System.Drawing.Size(121, 21);
            this.MaterialNum_MaterialId.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(25, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 1;
            this.label1.Text = "物料编码";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(261, 99);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 2;
            this.label2.Text = "需求数量";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(37, 99);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 12);
            this.label3.TabIndex = 3;
            this.label3.Text = "物料组";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(508, 15);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 12);
            this.label4.TabIndex = 4;
            this.label4.Text = "物料类型";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(261, 13);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 12);
            this.label5.TabIndex = 5;
            this.label5.Text = "物料名称";
            // 
            // MaterialNum_MaterialGroup
            // 
            this.MaterialNum_MaterialGroup.Location = new System.Drawing.Point(96, 96);
            this.MaterialNum_MaterialGroup.Name = "MaterialNum_MaterialGroup";
            this.MaterialNum_MaterialGroup.Size = new System.Drawing.Size(121, 21);
            this.MaterialNum_MaterialGroup.TabIndex = 6;
            // 
            // MaterialNum_MaterialType
            // 
            this.MaterialNum_MaterialType.Location = new System.Drawing.Point(576, 10);
            this.MaterialNum_MaterialType.Name = "MaterialNum_MaterialType";
            this.MaterialNum_MaterialType.Size = new System.Drawing.Size(137, 21);
            this.MaterialNum_MaterialType.TabIndex = 7;
            // 
            // MaterialNum_MaterialName
            // 
            this.MaterialNum_MaterialName.Location = new System.Drawing.Point(320, 10);
            this.MaterialNum_MaterialName.Name = "MaterialNum_MaterialName";
            this.MaterialNum_MaterialName.Size = new System.Drawing.Size(113, 21);
            this.MaterialNum_MaterialName.TabIndex = 8;
            // 
            // MaterialNum_DemandCount
            // 
            this.MaterialNum_DemandCount.Location = new System.Drawing.Point(320, 96);
            this.MaterialNum_DemandCount.Name = "MaterialNum_DemandCount";
            this.MaterialNum_DemandCount.Size = new System.Drawing.Size(113, 21);
            this.MaterialNum_DemandCount.TabIndex = 9;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(508, 99);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 12);
            this.label6.TabIndex = 10;
            this.label6.Text = "需求编号";
            // 
            // MaterialNum_DemandId
            // 
            this.MaterialNum_DemandId.Location = new System.Drawing.Point(576, 96);
            this.MaterialNum_DemandId.Name = "MaterialNum_DemandId";
            this.MaterialNum_DemandId.Size = new System.Drawing.Size(137, 21);
            this.MaterialNum_DemandId.TabIndex = 11;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(37, 198);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 12);
            this.label7.TabIndex = 12;
            this.label7.Text = "工厂编号";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(520, 198);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(41, 12);
            this.label8.TabIndex = 13;
            this.label8.Text = "单  位";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(261, 198);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(53, 12);
            this.label9.TabIndex = 14;
            this.label9.Text = "仓库编号";
            // 
            // MaterialNum_FactoryId
            // 
            this.MaterialNum_FactoryId.Location = new System.Drawing.Point(96, 195);
            this.MaterialNum_FactoryId.Name = "MaterialNum_FactoryId";
            this.MaterialNum_FactoryId.Size = new System.Drawing.Size(121, 21);
            this.MaterialNum_FactoryId.TabIndex = 15;
            // 
            // MaterialNum_StockId
            // 
            this.MaterialNum_StockId.Location = new System.Drawing.Point(320, 195);
            this.MaterialNum_StockId.Name = "MaterialNum_StockId";
            this.MaterialNum_StockId.Size = new System.Drawing.Size(113, 21);
            this.MaterialNum_StockId.TabIndex = 16;
            // 
            // MaterialNum_Unit
            // 
            this.MaterialNum_Unit.Location = new System.Drawing.Point(576, 195);
            this.MaterialNum_Unit.Name = "MaterialNum_Unit";
            this.MaterialNum_Unit.Size = new System.Drawing.Size(137, 21);
            this.MaterialNum_Unit.TabIndex = 17;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(13, 305);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(77, 12);
            this.label10.TabIndex = 18;
            this.label10.Text = "交货开始时间";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(237, 305);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(77, 12);
            this.label11.TabIndex = 20;
            this.label11.Text = "交货结束时间";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(703, 415);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 22;
            this.button1.Text = "保存";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // MaterialNum_DeliveryStartTime
            // 
            this.MaterialNum_DeliveryStartTime.Location = new System.Drawing.Point(96, 299);
            this.MaterialNum_DeliveryStartTime.Name = "MaterialNum_DeliveryStartTime";
            this.MaterialNum_DeliveryStartTime.Size = new System.Drawing.Size(121, 21);
            this.MaterialNum_DeliveryStartTime.TabIndex = 23;
            // 
            // MaterialNum_DeliveryEndTime
            // 
            this.MaterialNum_DeliveryEndTime.Location = new System.Drawing.Point(320, 299);
            this.MaterialNum_DeliveryEndTime.Name = "MaterialNum_DeliveryEndTime";
            this.MaterialNum_DeliveryEndTime.Size = new System.Drawing.Size(113, 21);
            this.MaterialNum_DeliveryEndTime.TabIndex = 24;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(508, 305);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(53, 12);
            this.label12.TabIndex = 25;
            this.label12.Text = "询价单号";
            // 
            // MaterialNum_SourceId
            // 
            this.MaterialNum_SourceId.Location = new System.Drawing.Point(576, 299);
            this.MaterialNum_SourceId.Name = "MaterialNum_SourceId";
            this.MaterialNum_SourceId.Size = new System.Drawing.Size(137, 21);
            this.MaterialNum_SourceId.TabIndex = 26;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(606, 415);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 27;
            this.button2.Text = "关闭";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // MaterialNum
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.MaterialNum_SourceId);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.MaterialNum_DeliveryEndTime);
            this.Controls.Add(this.MaterialNum_DeliveryStartTime);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.MaterialNum_Unit);
            this.Controls.Add(this.MaterialNum_StockId);
            this.Controls.Add(this.MaterialNum_FactoryId);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.MaterialNum_DemandId);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.MaterialNum_DemandCount);
            this.Controls.Add(this.MaterialNum_MaterialName);
            this.Controls.Add(this.MaterialNum_MaterialType);
            this.Controls.Add(this.MaterialNum_MaterialGroup);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.MaterialNum_MaterialId);
            this.Name = "MaterialNum";
            this.Text = "MaterialNum";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox MaterialNum_MaterialId;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox MaterialNum_MaterialGroup;
        private System.Windows.Forms.TextBox MaterialNum_MaterialType;
        private System.Windows.Forms.TextBox MaterialNum_MaterialName;
        private System.Windows.Forms.TextBox MaterialNum_DemandCount;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox MaterialNum_DemandId;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox MaterialNum_FactoryId;
        private System.Windows.Forms.TextBox MaterialNum_StockId;
        private System.Windows.Forms.TextBox MaterialNum_Unit;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DateTimePicker MaterialNum_DeliveryStartTime;
        private System.Windows.Forms.DateTimePicker MaterialNum_DeliveryEndTime;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox MaterialNum_SourceId;
        private System.Windows.Forms.Button button2;
    }
}