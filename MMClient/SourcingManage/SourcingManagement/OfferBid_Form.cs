﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;
using Lib.Model.SourcingManage.SourcingManagement;
using Lib.Bll.SourcingManage.SourcingManagement;

namespace MMClient.SourcingManage.SourcingManagement
{
    public partial class OfferBid_Form :DockContent
    {
        private UserUI userUI;
        private SourceBid bid;
        private int allCount;

        SourceBLL sourceBll = new SourceBLL();
        SourceBidBLL bidBLL = new SourceBidBLL();
        BidPartnerBLL bidPartnerBLL = new BidPartnerBLL();
        BidWeightScoreBLL bidWeightScoreBLL = new BidWeightScoreBLL();
        BidSupplierWeightScoreBLL bidSupplierWeightScoreBLL = new BidSupplierWeightScoreBLL();
        SourceMaterialBLL sourceMaterialBll = new SourceMaterialBLL();
        SourceSupplierBLL sourceSupplierBLL = new SourceSupplierBLL();
        SourceSupplierMaterialBLL ssmBll = new SourceSupplierMaterialBLL();
        LowestCostBLL lowestCostBll = new LowestCostBLL();

        SourceConditionBLL sourceConditionBll = new SourceConditionBLL();
        SourceTimeSectionBLL sourceTimeSectionBll = new SourceTimeSectionBLL();
        SourceNumberSectionBLL sourceNumberSectionBll = new SourceNumberSectionBLL();

        public OfferBid_Form()
        {
            InitializeComponent();
        }

        public OfferBid_Form(UserUI userUI, SourceBid bid)
        {
            InitializeComponent();
            this.userUI = userUI;
            this.bid = bid;
            initData(bid);
            initSupplier(bid);
            initLowCost();
        }

        
        private string getStringByNumber(int num)
        {
            string str = null;
            if (num == 0)
            {
                str = "未投标";
            }
            if (num == 1)
            {
                str = "已投标";
            }
            if (num == 2)
            {
                str = "已接受";
            }
            if (num == 3)
            {
                str = "已拒绝";
            }
            return str;
        }

        private void initSupplier(SourceBid bid)
        {
            //初始化供应商数据
            List<SourceSupplier> list = sourceSupplierBLL.getBidSuppliersByBidId(bid.BidId);
            allCount = list.Count;
            for (int i = 0; i < list.Count;i++)
            {
                cmb_Supplier.Items.Add(list.ElementAt(i).Supplier_ID);
                cmb_SupplierId.Items.Add(list.ElementAt(i).Supplier_ID);
            }
        }

        private void initData(SourceBid bid)
        {
            //招标基本信息
            txt_BidId.Text = bid.BidId;
            Source source = sourceBll.findSourceById(bid.BidId);
            txt_BidName.Text = source.Source_Name;
            txt_State.Text = getStateByInt(bid.BidState);
            txt_serviceType.Text = bid.ServiceType;
            txt_catalogue.Text = bid.Catalogue;
            txt_displayType.Text = bid.DisplayType;
            txt_transType.Text = bid.TransType;
            txt_purchaseOrg.Text = source.PurchaseOrg;
            txt_purchaseGroup.Text = source.PurchaseGroup;
            txt_StartTime.Text = bid.StartTime.ToString("yyyy-MM-dd HH:mm:ss");
            txt_EndTime.Text = bid.EndTime.ToString("yyyy-MM-dd HH:mm:ss");
            txt_startBeginTime.Text = bid.StartBeginTime.ToString("yyyy-MM-dd HH:mm:ss");
            txt_limitTime.Text = bid.LimitTime.ToString("yyyy-MM-dd");
            txt_timeZone.Text = bid.TimeZone.ToString();
            txt_currency.Text = bid.Currency.ToString();
            txt_commBidStartDate.Text = bid.CommBidStartDate.ToString("yyyy-MM-dd");
            txt_commBidStartTime.Text = bid.CommBidStartTime.ToString("HH:mm:ss");
            txt_techBidStartDate.Text = bid.TechBidStartDate.ToString("yyyy-MM-dd");
            txt_techBidStartTime.Text = bid.TechBidStartTime.ToString("HH:mm:ss");
            txt_buyEndDate.Text = bid.BuyEndDate.ToString("yyyy-MM-dd");
            txt_buyEndTime.Text = bid.BuyEndTime.ToString("HH:mm:ss");
            txt_enrollEndDate.Text = bid.EnrollEndDate.ToString("yyyy-MM-dd");
            txt_enrollEndTime.Text = bid.EnrollEndTime.ToString("HH:mm:ss");
            txt_enrollStartDate.Text = bid.EnrollStartDate.ToString("yyyy-MM-dd");
            txt_enrollStartTime.Text = bid.EnrollStartTime.ToString("HH:mm:ss");
            txt_bidCost.Text = bid.BidCost.ToString();
            //合作伙伴
            BidPartner bp = bidPartnerBLL.findBidPartnerByBidId(bid.BidId);
            if (bp != null)
            {
                txt_requesterId.Text = bp.RequesterId.ToString();
                txt_requesterName.Text = bp.RequesterName.ToString();
                txt_receiverId.Text = bp.ReceiverId.ToString();
                txt_receiverName.Text = bp.ReceiverName.ToString();
                txt_dropPointId.Text = bp.DropPointId.ToString();
                txt_dropPointName.Text = bp.DropPointName.ToString();
                txt_reviewerId.Text = bp.ReviewerId.ToString();
                txt_ReviewerName.Text = bp.ReviewerName.ToString();
                txt_callerId.Text = bp.CallerId.ToString();
                txt_callerName.Text = bp.CallerName.ToString();
            }
            //投标人
            List<SourceSupplier> list = sourceSupplierBLL.getBidSuppliersByBidId(bid.BidId);
            SupplierGridView.Rows.Clear();
            if (list.Count > SupplierGridView.Rows.Count)
            {
                SupplierGridView.Rows.Add(list.Count - SupplierGridView.Rows.Count);
            }
            for (int i = 0; i < list.Count; i++)
            {
                SupplierGridView.Rows[i].Cells["bidSupplierId"].Value = list.ElementAt(i).Supplier_ID;
                SupplierGridView.Rows[i].Cells["bidSupplierName"].Value = list.ElementAt(i).Supplier_Name;
                SupplierGridView.Rows[i].Cells["bidClassificationResult"].Value = list.ElementAt(i).ClassificationResult;
                SupplierGridView.Rows[i].Cells["bidContact"].Value = list.ElementAt(i).Contact;
                SupplierGridView.Rows[i].Cells["bidEmail"].Value = list.ElementAt(i).Email;
                SupplierGridView.Rows[i].Cells["bidAddress"].Value = list.ElementAt(i).Address;
                SupplierGridView.Rows[i].Cells["bidState"].Value = list.ElementAt(i).State;
            }

            //凭证
            BidCertificateBLL bidCertificatebll = new BidCertificateBLL();
            BidCertificate bidCer =  bidCertificatebll.getBidCertificateByBidId(txt_BidId.Text);
            txt_shortBidText.Text = bidCer.ShortBidText;
            txt_approvalComment.Text = bidCer.ApprovalComment;
            txt_longBidText.Text = bidCer.LongBidText;

            //评价标准
            tab_EvalMethod.SelectedIndex = bid.EvalMethId;
            txt_EvalMethod.Text = tab_EvalMethod.SelectedTab.Text;
            if (bid.EvalMethId == 0)
            {
                tab_WeightScore.Parent = null;
                tab_LeastTotalCost.Parent = null;
                tab_ValueAsses.Parent = null;
            }
            else if(bid.EvalMethId==1)//加权评分法
            {
                tab_Lowprice.Parent = null;
                tab_LeastTotalCost.Parent = null;
                tab_ValueAsses.Parent = null;

                string supplierId = cmb_Supplier.Text;
                List<BidWeightScore> listBws = bidWeightScoreBLL.findWeightScoresByBidId(bid.BidId);
                SupplierScoreGridView.Rows.Clear();
                if (listBws.Count > SupplierScoreGridView.Rows.Count)
                {
                    SupplierScoreGridView.Rows.Add(listBws.Count - SupplierScoreGridView.Rows.Count);
                }
                for (int i = 0; i < listBws.Count; i++)
                {
                    SupplierScoreGridView.Rows[i].Cells["weightName"].Value = listBws.ElementAt(i).WeightScoreName;
                    SupplierScoreGridView.Rows[i].Cells["weightNum"].Value = listBws.ElementAt(i).WeightScoreNum;
                }
            }
            else if (bid.EvalMethId == 2)
            {
                tab_Lowprice.Parent = null;
                tab_WeightScore.Parent = null;
                tab_ValueAsses.Parent = null;
            }
            else if(bid.EvalMethId==3)
            {
                tab_Lowprice.Parent = null;
                tab_WeightScore.Parent = null;
                tab_LeastTotalCost.Parent = null;
            }

            //项目数据
            List<SourceMaterial> listMaterials = sourceMaterialBll.findSourceMaterialsBySourceId(bid.BidId);
            MaterialGridview.Rows.Clear();
            if (listMaterials.Count > MaterialGridview.Rows.Count)
            {
                MaterialGridview.Rows.Add(listMaterials.Count - MaterialGridview.Rows.Count);
            }
            for (int i = 0; i < listMaterials.Count; i++)
            {
                MaterialGridview.Rows[i].Cells["bidMaterialId"].Value = listMaterials.ElementAt(i).Material_ID;
                MaterialGridview.Rows[i].Cells["bidMaterialName"].Value = listMaterials.ElementAt(i).Material_Name;
                MaterialGridview.Rows[i].Cells["bidDemandCount"].Value = listMaterials.ElementAt(i).Demand_Count;
                MaterialGridview.Rows[i].Cells["bidDemandMeasurement"].Value = listMaterials.ElementAt(i).Unit;
                MaterialGridview.Rows[i].Cells["factoryId"].Value = listMaterials.ElementAt(i).FactoryId;
                MaterialGridview.Rows[i].Cells["stockId"].Value = listMaterials.ElementAt(i).StockId;
                MaterialGridview.Rows[i].Cells["DeliveryStartTime"].Value = listMaterials.ElementAt(i).DeliveryStartTime.ToString("yyyy-MM-dd");
                MaterialGridview.Rows[i].Cells["DeliveryEndTime"].Value = listMaterials.ElementAt(i).DeliveryEndTime.ToString("yyyy-MM-dd");

            }
        }

        private string getStateByInt(int num)
        {
            string str = "";
            if (num == 0)
            {
                str = "待保存";
            }
            else if (num == 1)
            {
                str = "待发布";
            }
            else if (num == 2)
            {
                str = "待报价";
            }else if(num == 3)
            {
                str = "待评标";
            }
            else if (num == 4)
            {
                str = "已完成";
            }
            return str;
        }

        private void initLowCost()
        {
            List<LowestCost> list = lowestCostBll.getAllLowestBySId(txt_BidId.Text);
            allCostGridView.Rows.Clear();
            if (list.Count > allCostGridView.Rows.Count)
            {
                allCostGridView.Rows.Add(list.Count - allCostGridView.Rows.Count);
            }
            for (int i = 0; i < list.Count; i++)
            {
                allCostGridView.Rows[i].Cells["CostItem"].Value = list.ElementAt(i).CostItem;
                allCostGridView.Rows[i].Cells["First"].Value = list.ElementAt(i).First;
                allCostGridView.Rows[i].Cells["Second"].Value = list.ElementAt(i).Second;
                allCostGridView.Rows[i].Cells["Third"].Value = list.ElementAt(i).Third;
                allCostGridView.Rows[i].Cells["forth"].Value = list.ElementAt(i).Forth;
                allCostGridView.Rows[i].Cells["fifth"].Value = list.ElementAt(i).Fifth;
                allCostGridView.Rows[i].Cells["sixth"].Value = list.ElementAt(i).Sixth;
                allCostGridView.Rows[i].Cells["seventh"].Value = list.ElementAt(i).Seventh;
                allCostGridView.Rows[i].Cells["eighth"].Value = list.ElementAt(i).Eighth;
                allCostGridView.Rows[i].Cells["ninth"].Value = list.ElementAt(i).Ninth;
                allCostGridView.Rows[i].Cells["tenth"].Value = list.ElementAt(i).Tenth;
            }
        }

        private void cmb_EvalMethod_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        //保存供应商的报价信息
        private void button4_Click(object sender, EventArgs e)
        {
            int index = cmb_Supplier.SelectedIndex;
            string supplierId = cmb_Supplier.SelectedItem.ToString();
            foreach (DataGridViewRow dgvr in SupplierScoreGridView.Rows)
            {
                BidSupplierWeightScore bsws = new BidSupplierWeightScore();
                bsws.BidId = bid.BidId;
                bsws.SupplierId = cmb_Supplier.SelectedItem.ToString();
                bsws.WeightName = dgvr.Cells["WeightName"].Value.ToString();
                bsws.Score = Convert.ToInt32(dgvr.Cells["score"].Value.ToString());
                bsws.Cause = dgvr.Cells["cause"].Value.ToString();
                bidSupplierWeightScoreBLL.addBidSupplierWeightScore(bsws);
            }
            cmb_Supplier.Items.RemoveAt(index);
            if (cmb_Supplier.Items.Count == 0)
            {
                bid.BidState = 3;//0 未发布；1-已发布；2-待处理；3-待评标；
                bidBLL.updateBidState(bid);
                MessageBox.Show("评标完成！");
                this.Close();
            }
        }

        //选择供应商
        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            string supplierId = cmb_Supplier.Text;
            List<BidWeightScore> listBws = bidWeightScoreBLL.findWeightScoresByBidId(bid.BidId);
            SupplierScoreGridView.Rows.Clear();
            if (listBws.Count > SupplierScoreGridView.Rows.Count)
            {
                SupplierScoreGridView.Rows.Add(listBws.Count - SupplierScoreGridView.Rows.Count);
            }

            for (int i = 0; i < listBws.Count; i++)
            {
                SupplierScoreGridView.Rows[i].Cells["weightName"].Value = listBws.ElementAt(i).WeightScoreName;
                SupplierScoreGridView.Rows[i].Cells["weightNum"].Value = listBws.ElementAt(i).WeightScoreNum;
                BidSupplierWeightScore bsws =  bidSupplierWeightScoreBLL.getBSWSByBIdSIdWN(txt_BidId.Text,supplierId,listBws.ElementAt(i).WeightScoreName);
                SupplierScoreGridView.Rows[i].Cells["supplierAnswer"].Value = bsws.SupplierAnswer;
            }
        }

        /// <summary>
        /// 最低价格法
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_LowPrice_Click(object sender, EventArgs e)
        {
            bid.BidState = 3;//0 未发布；1-已发布；2-待处理；3-待评标；
            bidBLL.updateBidState(bid);
            MessageBox.Show("评标完成！");
            this.Close();
        }

        //显示供应商报的结果数据
        private void btn_Show_Click(object sender, EventArgs e)
        {
            conditionGridView.Rows.Clear();
            timeGridView.Rows.Clear();
            numberGridView.Rows.Clear();
            string bidId = txt_BidId.Text;
            
            if (MaterialGridview.CurrentRow == null)
            {
                MessageBox.Show("请选中待查看的物料");
                return;
            }
            int index = MaterialGridview.CurrentRow.Index;
            Object obj = MaterialGridview.Rows[index].Cells[0].Value;
            if (obj == null)
            {
                return;
            }
            string materialId = obj.ToString();
            if (!(cmb_SupplierId.SelectedIndex >= 0 && cmb_SupplierId.SelectedIndex < cmb_SupplierId.Items.Count))
            {
                MessageBox.Show("请选中一个需查看的供应商");
                return;
            }
            string supplierId = cmb_SupplierId.Text;
            List<SourceCondition> conditionList = sourceConditionBll.getSourceConByBidSpMaId(bidId,supplierId,materialId);
            
            if (conditionList.Count > conditionGridView.Rows.Count)
            {
                conditionGridView.Rows.Add(conditionList.Count - conditionGridView.Rows.Count);
            }
            for (int i = 0; i < conditionList.Count; i++)
            {
                conditionGridView.Rows[i].Cells["conditionNum"].Value = conditionList.ElementAt(i).ConditionId;
                //conditionGridView.Rows[i].Cells["priceType"].Value = conditionList.ElementAt(i).;
                //conditionGridView.Rows[i].Cells["calType"].Value = conditionList.ElementAt(i).;
                //conditionGridView.Rows[i].Cells["disType"].Value = conditionList.ElementAt(i).;
                //conditionGridView.Rows[i].Cells["pn"].Value = conditionList.ElementAt(i).;
                conditionGridView.Rows[i].Cells["number"].Value = conditionList.ElementAt(i).Num;

            }
            List<SourceTimeSection> sTSList = sourceTimeSectionBll.getSouTmsBySouSupMaId(bidId, supplierId, materialId);
            if (sTSList.Count > timeGridView.Rows.Count)
            {
                timeGridView.Rows.Add(sTSList.Count - timeGridView.Rows.Count);
            }
            for (int j = 0; j < sTSList.Count; j++)
            {
                timeGridView.Rows[j].Cells["startTime"].Value = sTSList.ElementAt(j).StartTime;
                timeGridView.Rows[j].Cells["endTime"].Value = sTSList.ElementAt(j).EndTime;
                timeGridView.Rows[j].Cells["timePrice"].Value = sTSList.ElementAt(j).Num;
            }
            List<SourceNumberSection> sNSList = sourceNumberSectionBll.getSouNumSecsBySouSupMaId(bidId, supplierId, materialId);
            if (sNSList.Count > numberGridView.Rows.Count)
            {
                numberGridView.Rows.Add(sNSList.Count - numberGridView.Rows.Count);
            }
            for (int i = 0; i < sNSList.Count; i++)
            {
                numberGridView.Rows[i].Cells["startNumber"].Value = sNSList.ElementAt(i).StartNumber;
                numberGridView.Rows[i].Cells["endNumber"].Value = sNSList.ElementAt(i).EndNumber;
                numberGridView.Rows[i].Cells["numberPrice"].Value = sNSList.ElementAt(i).Num;
            }

            SourceSupplierMaterial ssmt =  ssmBll.getSSMBySIdSIdMId(bidId,supplierId,materialId);  
            txt_NetPrice.Text = ssmt.Price.ToString();
        }
    }
}
