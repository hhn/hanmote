﻿namespace MMClient.SourcingManage.SourcingManagement
{
    partial class ModifyInqueryForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel2 = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.txt_BuyerName = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.dateTimePicker_Offer_Time = new System.Windows.Forms.DateTimePicker();
            this.label8 = new System.Windows.Forms.Label();
            this.txt_Purchase_Group = new System.Windows.Forms.TextBox();
            this.txt_Purchase_Org = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.dateTimePicker_CreateTime = new System.Windows.Forms.DateTimePicker();
            this.label7 = new System.Windows.Forms.Label();
            this.txt_InquiryName = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txt_state = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txt_inquiryId = new System.Windows.Forms.TextBox();
            this.xjdh_lbl = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.button8 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.SourceMaterialListDGV = new System.Windows.Forms.DataGridView();
            this.Material_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Material_Name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Material_Group = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Measurement = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Material_Type = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Type_Name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel3 = new System.Windows.Forms.Panel();
            this.button7 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.SupplierListDGV = new System.Windows.Forms.DataGridView();
            this.Supplier_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Supplier_Name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Contact = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.email = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nation = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.address = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IsTrans = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SourceMaterialListDGV)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SupplierListDGV)).BeginInit();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.panel2.Controls.Add(this.button1);
            this.panel2.Controls.Add(this.txt_BuyerName);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.dateTimePicker_Offer_Time);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.txt_Purchase_Group);
            this.panel2.Controls.Add(this.txt_Purchase_Org);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.dateTimePicker_CreateTime);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.txt_InquiryName);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.txt_state);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.txt_inquiryId);
            this.panel2.Controls.Add(this.xjdh_lbl);
            this.panel2.Location = new System.Drawing.Point(7, 1);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(932, 114);
            this.panel2.TabIndex = 181;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.button1.Location = new System.Drawing.Point(824, 87);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 194;
            this.button1.Text = "保存";
            this.button1.UseVisualStyleBackColor = false;
            // 
            // txt_BuyerName
            // 
            this.txt_BuyerName.Location = new System.Drawing.Point(394, 84);
            this.txt_BuyerName.Name = "txt_BuyerName";
            this.txt_BuyerName.Size = new System.Drawing.Size(186, 21);
            this.txt_BuyerName.TabIndex = 193;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(347, 93);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(41, 12);
            this.label9.TabIndex = 192;
            this.label9.Text = "申请人";
            // 
            // dateTimePicker_Offer_Time
            // 
            this.dateTimePicker_Offer_Time.CustomFormat = "yyyy-MM-dd HH:mm";
            this.dateTimePicker_Offer_Time.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker_Offer_Time.Location = new System.Drawing.Point(74, 87);
            this.dateTimePicker_Offer_Time.Name = "dateTimePicker_Offer_Time";
            this.dateTimePicker_Offer_Time.Size = new System.Drawing.Size(220, 21);
            this.dateTimePicker_Offer_Time.TabIndex = 191;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(15, 93);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(53, 12);
            this.label8.TabIndex = 190;
            this.label8.Text = "报价期限";
            // 
            // txt_Purchase_Group
            // 
            this.txt_Purchase_Group.Location = new System.Drawing.Point(719, 52);
            this.txt_Purchase_Group.Name = "txt_Purchase_Group";
            this.txt_Purchase_Group.Size = new System.Drawing.Size(180, 21);
            this.txt_Purchase_Group.TabIndex = 189;
            // 
            // txt_Purchase_Org
            // 
            this.txt_Purchase_Org.Location = new System.Drawing.Point(719, 10);
            this.txt_Purchase_Org.Name = "txt_Purchase_Org";
            this.txt_Purchase_Org.Size = new System.Drawing.Size(180, 21);
            this.txt_Purchase_Org.TabIndex = 188;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(672, 55);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 12);
            this.label5.TabIndex = 187;
            this.label5.Text = "采购组";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(660, 13);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 12);
            this.label4.TabIndex = 186;
            this.label4.Text = "采购组织";
            // 
            // dateTimePicker_CreateTime
            // 
            this.dateTimePicker_CreateTime.CustomFormat = "yyyy-MM-dd HH:mm";
            this.dateTimePicker_CreateTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker_CreateTime.Location = new System.Drawing.Point(394, 52);
            this.dateTimePicker_CreateTime.Name = "dateTimePicker_CreateTime";
            this.dateTimePicker_CreateTime.Size = new System.Drawing.Size(186, 21);
            this.dateTimePicker_CreateTime.TabIndex = 185;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(335, 55);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 12);
            this.label7.TabIndex = 184;
            this.label7.Text = "创建时间";
            // 
            // txt_InquiryName
            // 
            this.txt_InquiryName.Location = new System.Drawing.Point(74, 52);
            this.txt_InquiryName.Name = "txt_InquiryName";
            this.txt_InquiryName.Size = new System.Drawing.Size(220, 21);
            this.txt_InquiryName.TabIndex = 183;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(15, 55);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 12);
            this.label6.TabIndex = 182;
            this.label6.Text = "询价名称";
            // 
            // txt_state
            // 
            this.txt_state.Location = new System.Drawing.Point(394, 10);
            this.txt_state.Name = "txt_state";
            this.txt_state.ReadOnly = true;
            this.txt_state.Size = new System.Drawing.Size(186, 21);
            this.txt_state.TabIndex = 181;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(335, 14);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 12);
            this.label3.TabIndex = 180;
            this.label3.Text = "状    态";
            // 
            // txt_inquiryId
            // 
            this.txt_inquiryId.Location = new System.Drawing.Point(73, 10);
            this.txt_inquiryId.Name = "txt_inquiryId";
            this.txt_inquiryId.ReadOnly = true;
            this.txt_inquiryId.Size = new System.Drawing.Size(221, 21);
            this.txt_inquiryId.TabIndex = 179;
            // 
            // xjdh_lbl
            // 
            this.xjdh_lbl.AutoSize = true;
            this.xjdh_lbl.Font = new System.Drawing.Font("宋体", 9F);
            this.xjdh_lbl.Location = new System.Drawing.Point(15, 14);
            this.xjdh_lbl.Name = "xjdh_lbl";
            this.xjdh_lbl.Size = new System.Drawing.Size(53, 12);
            this.xjdh_lbl.TabIndex = 155;
            this.xjdh_lbl.Text = "询价单号";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.panel1.Controls.Add(this.button8);
            this.panel1.Controls.Add(this.button5);
            this.panel1.Controls.Add(this.button4);
            this.panel1.Controls.Add(this.button2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.SourceMaterialListDGV);
            this.panel1.Location = new System.Drawing.Point(7, 149);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(932, 224);
            this.panel1.TabIndex = 186;
            // 
            // button8
            // 
            this.button8.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.button8.ForeColor = System.Drawing.SystemColors.ControlText;
            this.button8.Location = new System.Drawing.Point(537, 5);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(75, 23);
            this.button8.TabIndex = 198;
            this.button8.Text = "编辑";
            this.button8.UseVisualStyleBackColor = false;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.button5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.button5.Location = new System.Drawing.Point(638, 5);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(75, 23);
            this.button5.TabIndex = 197;
            this.button5.Text = "去除";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.button4.ForeColor = System.Drawing.SystemColors.ControlText;
            this.button4.Location = new System.Drawing.Point(734, 5);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 196;
            this.button4.Text = "增加";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.button2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.button2.Location = new System.Drawing.Point(824, 5);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 195;
            this.button2.Text = "保存";
            this.button2.UseVisualStyleBackColor = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("宋体", 9F);
            this.label1.Location = new System.Drawing.Point(3, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 12);
            this.label1.TabIndex = 186;
            this.label1.Text = "物料";
            // 
            // SourceMaterialListDGV
            // 
            this.SourceMaterialListDGV.AllowDrop = true;
            this.SourceMaterialListDGV.AllowUserToAddRows = false;
            this.SourceMaterialListDGV.AllowUserToResizeColumns = false;
            this.SourceMaterialListDGV.AllowUserToResizeRows = false;
            this.SourceMaterialListDGV.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.SourceMaterialListDGV.BackgroundColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.SourceMaterialListDGV.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.SourceMaterialListDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.SourceMaterialListDGV.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Material_ID,
            this.Material_Name,
            this.Material_Group,
            this.Measurement,
            this.Material_Type,
            this.Type_Name});
            this.SourceMaterialListDGV.EnableHeadersVisualStyles = false;
            this.SourceMaterialListDGV.Location = new System.Drawing.Point(5, 32);
            this.SourceMaterialListDGV.Name = "SourceMaterialListDGV";
            this.SourceMaterialListDGV.RowTemplate.Height = 23;
            this.SourceMaterialListDGV.Size = new System.Drawing.Size(924, 189);
            this.SourceMaterialListDGV.TabIndex = 195;
            // 
            // Material_ID
            // 
            this.Material_ID.DataPropertyName = "Material_ID";
            this.Material_ID.HeaderText = "物料编号";
            this.Material_ID.Name = "Material_ID";
            // 
            // Material_Name
            // 
            this.Material_Name.DataPropertyName = "Material_Name";
            this.Material_Name.HeaderText = "物料名称";
            this.Material_Name.Name = "Material_Name";
            // 
            // Material_Group
            // 
            this.Material_Group.DataPropertyName = "Material_Group";
            this.Material_Group.HeaderText = "物料组";
            this.Material_Group.Name = "Material_Group";
            // 
            // Measurement
            // 
            this.Measurement.DataPropertyName = "Measurement";
            this.Measurement.HeaderText = "计量单位";
            this.Measurement.Name = "Measurement";
            // 
            // Material_Type
            // 
            this.Material_Type.DataPropertyName = "Material_Type";
            this.Material_Type.HeaderText = "物料类型";
            this.Material_Type.Name = "Material_Type";
            // 
            // Type_Name
            // 
            this.Type_Name.DataPropertyName = "Type_Name";
            this.Type_Name.HeaderText = "物料类型名称";
            this.Type_Name.Name = "Type_Name";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.panel3.Controls.Add(this.button7);
            this.panel3.Controls.Add(this.button6);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Controls.Add(this.button3);
            this.panel3.Controls.Add(this.SupplierListDGV);
            this.panel3.Location = new System.Drawing.Point(7, 407);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(932, 213);
            this.panel3.TabIndex = 186;
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.button7.ForeColor = System.Drawing.SystemColors.ControlText;
            this.button7.Location = new System.Drawing.Point(638, 12);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(75, 23);
            this.button7.TabIndex = 199;
            this.button7.Text = "去除";
            this.button7.UseVisualStyleBackColor = false;
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.button6.ForeColor = System.Drawing.SystemColors.ControlText;
            this.button6.Location = new System.Drawing.Point(734, 12);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(75, 23);
            this.button6.TabIndex = 198;
            this.button6.Text = "增加";
            this.button6.UseVisualStyleBackColor = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("宋体", 9F);
            this.label2.Location = new System.Drawing.Point(3, 12);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 12);
            this.label2.TabIndex = 196;
            this.label2.Text = "供应商";
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.button3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.button3.Location = new System.Drawing.Point(827, 12);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 196;
            this.button3.Text = "保存";
            this.button3.UseVisualStyleBackColor = false;
            // 
            // SupplierListDGV
            // 
            this.SupplierListDGV.AllowUserToAddRows = false;
            this.SupplierListDGV.AllowUserToResizeColumns = false;
            this.SupplierListDGV.AllowUserToResizeRows = false;
            this.SupplierListDGV.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.SupplierListDGV.BackgroundColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.SupplierListDGV.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.SupplierListDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.SupplierListDGV.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Supplier_ID,
            this.Supplier_Name,
            this.Contact,
            this.email,
            this.nation,
            this.address,
            this.IsTrans});
            this.SupplierListDGV.EnableHeadersVisualStyles = false;
            this.SupplierListDGV.Location = new System.Drawing.Point(3, 38);
            this.SupplierListDGV.Name = "SupplierListDGV";
            this.SupplierListDGV.RowTemplate.Height = 23;
            this.SupplierListDGV.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.SupplierListDGV.Size = new System.Drawing.Size(926, 172);
            this.SupplierListDGV.TabIndex = 176;
            // 
            // Supplier_ID
            // 
            this.Supplier_ID.DataPropertyName = "Supplier_ID";
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.White;
            this.Supplier_ID.DefaultCellStyle = dataGridViewCellStyle1;
            this.Supplier_ID.HeaderText = "供应商编号";
            this.Supplier_ID.Name = "Supplier_ID";
            this.Supplier_ID.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // Supplier_Name
            // 
            this.Supplier_Name.DataPropertyName = "Supplier_Name";
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.White;
            this.Supplier_Name.DefaultCellStyle = dataGridViewCellStyle2;
            this.Supplier_Name.HeaderText = "供应商名称";
            this.Supplier_Name.Name = "Supplier_Name";
            this.Supplier_Name.ReadOnly = true;
            this.Supplier_Name.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // Contact
            // 
            this.Contact.DataPropertyName = "Contact";
            this.Contact.HeaderText = "联系人";
            this.Contact.Name = "Contact";
            this.Contact.ReadOnly = true;
            // 
            // email
            // 
            this.email.DataPropertyName = "Email";
            this.email.HeaderText = "邮箱";
            this.email.Name = "email";
            this.email.ReadOnly = true;
            // 
            // nation
            // 
            this.nation.DataPropertyName = "Address";
            this.nation.HeaderText = "地址";
            this.nation.Name = "nation";
            this.nation.ReadOnly = true;
            // 
            // address
            // 
            this.address.DataPropertyName = "State";
            this.address.HeaderText = "状态";
            this.address.Name = "address";
            this.address.ReadOnly = true;
            // 
            // IsTrans
            // 
            this.IsTrans.DataPropertyName = "IsTrans";
            this.IsTrans.HeaderText = "是否谈判";
            this.IsTrans.Name = "IsTrans";
            this.IsTrans.ReadOnly = true;
            // 
            // ModifyInqueryForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(944, 621);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel2);
            this.Name = "ModifyInqueryForm";
            this.Text = "修改询价";
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SourceMaterialListDGV)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SupplierListDGV)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DateTimePicker dateTimePicker_CreateTime;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txt_InquiryName;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txt_state;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txt_inquiryId;
        private System.Windows.Forms.Label xjdh_lbl;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.DataGridView SourceMaterialListDGV;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView SupplierListDGV;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txt_Purchase_Group;
        private System.Windows.Forms.TextBox txt_Purchase_Org;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DateTimePicker dateTimePicker_Offer_Time;
        private System.Windows.Forms.TextBox txt_BuyerName;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Supplier_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Supplier_Name;
        private System.Windows.Forms.DataGridViewTextBoxColumn Contact;
        private System.Windows.Forms.DataGridViewTextBoxColumn email;
        private System.Windows.Forms.DataGridViewTextBoxColumn nation;
        private System.Windows.Forms.DataGridViewTextBoxColumn address;
        private System.Windows.Forms.DataGridViewTextBoxColumn IsTrans;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Material_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Material_Name;
        private System.Windows.Forms.DataGridViewTextBoxColumn Material_Group;
        private System.Windows.Forms.DataGridViewTextBoxColumn Measurement;
        private System.Windows.Forms.DataGridViewTextBoxColumn Material_Type;
        private System.Windows.Forms.DataGridViewTextBoxColumn Type_Name;
    }
}