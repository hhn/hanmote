﻿namespace MMClient.SourcingManage.SourcingManagement.Inquiry
{
    partial class AllInquiry
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btn_Sure = new System.Windows.Forms.Button();
            this.cmb_inquiryState = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.InqueryPriceDGV = new System.Windows.Forms.DataGridView();
            this.Inquery_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Inquiry_Name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.createTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Offer_Time = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Purchase_Org = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Purchase_Group = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.State = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Buyer_Name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.InqueryPriceDGV)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btn_Sure);
            this.groupBox1.Controls.Add(this.cmb_inquiryState);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.InqueryPriceDGV);
            this.groupBox1.Location = new System.Drawing.Point(43, 26);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(965, 427);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "询价单列表";
            // 
            // btn_Sure
            // 
            this.btn_Sure.Location = new System.Drawing.Point(414, 33);
            this.btn_Sure.Name = "btn_Sure";
            this.btn_Sure.Size = new System.Drawing.Size(75, 23);
            this.btn_Sure.TabIndex = 3;
            this.btn_Sure.Text = "查看";
            this.btn_Sure.UseVisualStyleBackColor = true;
            this.btn_Sure.Click += new System.EventHandler(this.btn_Sure_Click);
            // 
            // cmb_inquiryState
            // 
            this.cmb_inquiryState.FormattingEnabled = true;
            this.cmb_inquiryState.Items.AddRange(new object[] {
            "待执行",
            "待报价",
            "已报价",
            "未报价"});
            this.cmb_inquiryState.Location = new System.Drawing.Point(257, 33);
            this.cmb_inquiryState.Name = "cmb_inquiryState";
            this.cmb_inquiryState.Size = new System.Drawing.Size(121, 20);
            this.cmb_inquiryState.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(177, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 12);
            this.label1.TabIndex = 1;
            this.label1.Text = "询价单状态";
            // 
            // InqueryPriceDGV
            // 
            this.InqueryPriceDGV.AllowUserToAddRows = false;
            this.InqueryPriceDGV.BackgroundColor = System.Drawing.SystemColors.Control;
            this.InqueryPriceDGV.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.InqueryPriceDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.InqueryPriceDGV.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Inquery_ID,
            this.Inquiry_Name,
            this.createTime,
            this.Offer_Time,
            this.Purchase_Org,
            this.Purchase_Group,
            this.State,
            this.Buyer_Name});
            this.InqueryPriceDGV.EnableHeadersVisualStyles = false;
            this.InqueryPriceDGV.Location = new System.Drawing.Point(0, 82);
            this.InqueryPriceDGV.Name = "InqueryPriceDGV";
            this.InqueryPriceDGV.RowTemplate.Height = 23;
            this.InqueryPriceDGV.Size = new System.Drawing.Size(965, 262);
            this.InqueryPriceDGV.TabIndex = 0;
            // 
            // Inquery_ID
            // 
            this.Inquery_ID.DataPropertyName = "Source_ID";
            this.Inquery_ID.HeaderText = "询价单号";
            this.Inquery_ID.Name = "Inquery_ID";
            this.Inquery_ID.Width = 120;
            // 
            // Inquiry_Name
            // 
            this.Inquiry_Name.DataPropertyName = "Inquiry_Name";
            this.Inquiry_Name.HeaderText = "询价单名称";
            this.Inquiry_Name.Name = "Inquiry_Name";
            this.Inquiry_Name.Width = 110;
            // 
            // createTime
            // 
            this.createTime.DataPropertyName = "CreateTime";
            this.createTime.HeaderText = "创建时间";
            this.createTime.Name = "createTime";
            this.createTime.Width = 145;
            // 
            // Offer_Time
            // 
            this.Offer_Time.DataPropertyName = "Offer_Time";
            this.Offer_Time.HeaderText = "报价截止时间";
            this.Offer_Time.Name = "Offer_Time";
            this.Offer_Time.Width = 145;
            // 
            // Purchase_Org
            // 
            this.Purchase_Org.DataPropertyName = "Purchase_Org";
            this.Purchase_Org.HeaderText = "采购组织";
            this.Purchase_Org.Name = "Purchase_Org";
            // 
            // Purchase_Group
            // 
            this.Purchase_Group.DataPropertyName = "Purchase_Group";
            this.Purchase_Group.HeaderText = "采购组";
            this.Purchase_Group.Name = "Purchase_Group";
            // 
            // State
            // 
            this.State.DataPropertyName = "State";
            this.State.HeaderText = "询价状态";
            this.State.Name = "State";
            // 
            // Buyer_Name
            // 
            this.Buyer_Name.DataPropertyName = "Buyer_Name";
            this.Buyer_Name.HeaderText = "申请人姓名";
            this.Buyer_Name.Name = "Buyer_Name";
            // 
            // AllInquiry
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1014, 515);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "AllInquiry";
            this.Text = "查看询价";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.InqueryPriceDGV)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView InqueryPriceDGV;
        private System.Windows.Forms.Button btn_Sure;
        private System.Windows.Forms.ComboBox cmb_inquiryState;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Inquery_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Inquiry_Name;
        private System.Windows.Forms.DataGridViewTextBoxColumn createTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn Offer_Time;
        private System.Windows.Forms.DataGridViewTextBoxColumn Purchase_Org;
        private System.Windows.Forms.DataGridViewTextBoxColumn Purchase_Group;
        private System.Windows.Forms.DataGridViewTextBoxColumn State;
        private System.Windows.Forms.DataGridViewTextBoxColumn Buyer_Name;
    }
}