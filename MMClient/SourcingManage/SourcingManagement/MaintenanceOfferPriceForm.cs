﻿using Lib.Bll.ServiceBll;
using Lib.Model.ServiceModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;

namespace MMClient.SourcingManage.SourcingManagement
{
    public partial class MaintenanceOfferPriceForm : DockContent
    {
        private ServiceBill serviceBill = new ServiceBill();

        public MaintenanceOfferPriceForm()
        {
            InitializeComponent();
            this.InitData();
        }

        //初始化
        private void InitData()
        {
            this.ClearDGV(this.OfferPriceDGV);
            DataTable dt = serviceBill.GetAllOfferPrice();
            this.OfferPriceDGV.DataSource = dt;
        }

        //搜索按钮
        private void searchButton_Click(object sender, EventArgs e)
        {
            string materialId = this.materialNameText.Text;
            if (!"".Equals(materialId))
            {
                this.ClearDGV(this.OfferPriceDGV);
                DataTable dt = serviceBill.GetOfferPriceLikeMaterialId(materialId);
                this.OfferPriceDGV.DataSource = dt;
            }
            else
            {
                this.InitData();
            }
        }

        //清空DGV数据
        private void ClearDGV(DataGridView dgv)
        {
            if(dgv != null)
            {
                object obj = dgv.DataSource;
                if(obj != null && obj is DataTable)
                {
                    DataTable dt = (DataTable)obj;
                    dt.Clear();
                }
            }
        }

        //新建按钮
        private void newButton_Click(object sender, EventArgs e)
        {
            NewOfferPriceInfoForm offerPriceInfoForm = new NewOfferPriceInfoForm();
            offerPriceInfoForm.Show();
        }

        //修改按钮
        private void modifyButton_Click(object sender, EventArgs e)
        {
            DataGridViewRow curRow = this.OfferPriceDGV.CurrentRow;
            if(curRow != null && curRow.Index >= 0)
            {
                string Source_ID = curRow.Cells["Source_ID"].Value.ToString();
                string Supplier_ID = curRow.Cells["Supplier_ID"].Value.ToString();
                string Material_ID = curRow.Cells["Material_ID"].Value.ToString();
                string PriceStr = curRow.Cells["Price"].Value.ToString();
                float Price = Convert.ToSingle(PriceStr);
                OfferPrice offerPrice = new OfferPrice(Source_ID,Supplier_ID,Material_ID,Price);
                //跳转到修改界面
                NewOfferPriceInfoForm offerPriceInfoForm = new NewOfferPriceInfoForm(offerPrice);
                offerPriceInfoForm.Show();
            }
        }

        //删除按钮
        private void deleteButton_Click(object sender, EventArgs e)
        {
            DataGridViewRow curRow = this.OfferPriceDGV.CurrentRow;
            if(curRow != null && curRow.Index >= 0)
            {
                OfferPrice offerPrice = new OfferPrice();
                string Source_ID = curRow.Cells["Source_ID"].Value.ToString();
                string Supplier_ID = curRow.Cells["Supplier_ID"].Value.ToString();
                string Material_ID = curRow.Cells["Material_ID"].Value.ToString();
                offerPrice.Source_ID = Source_ID;
                offerPrice.Supplier_ID = Supplier_ID;
                offerPrice.Material_ID = Material_ID;
                serviceBill.RemoveOfferPrice(offerPrice);
            }
        }
    }
}
