﻿using Lib.Bll.ServiceBll;
using Lib.Common.CommonUtils;
using Lib.Common.MMCException.Bll;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MMClient.SourcingManage.SourcingManagement
{
    public partial class MaterialInfo : Form
    {
        //业务对象
        private ServiceBill serviceBill = new ServiceBill();

        //创建询价窗体
        private Inquiry_Form iquiry_Form = null;

        //修改询价窗体
        private ModifyInqueryForm modifyInqueryForm = null;

        public MaterialInfo(Inquiry_Form inquiry_Form)
        {
            InitializeComponent();
            //拿到创建询价窗体对象的引用
            this.iquiry_Form = inquiry_Form;
            InitData();
        }

        public MaterialInfo(ModifyInqueryForm modifyInqueryForm)
        {
            InitializeComponent();
            //拿到修改询价窗体对象的引用
            this.modifyInqueryForm = modifyInqueryForm;
            InitData();
        }

        //初始化界面数据
        private void InitData()
        {
            this.ClearDGV(this.MaterialListDGV);
            //填充数据
            DataTable dt = serviceBill.GetAllMaterialInfo();
            if(dt != null && dt.Rows.Count > 0)
            {
                this.MaterialListDGV.DataSource = dt;
            }
        }

        //清空DGV数据
        private void ClearDGV(DataGridView dgv)
        {
            if(dgv != null)
            {
                object obj = dgv.DataSource;
                if(obj != null && (obj is DataTable))
                {
                    DataTable dt = (DataTable)obj;
                    dt.Clear();
                }
            }
        }

        //物料信息页面确定按钮
        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                DataGridViewRow curRow = this.MaterialListDGV.CurrentRow;
                if(curRow != null && curRow.Index >= 0)
                {
                    string Material_ID = curRow.Cells["Material_ID"].Value.ToString();
                    //是否是 创建询价
                    if(this.iquiry_Form != null)
                    {
                        if (this.iquiry_Form.IsContains(Material_ID))
                        {
                            MessageUtil.ShowWarning("该物料已经添加过，无需重复添加！");
                            return;
                        }
                        else
                        {
                            //添加物料 , 加载物料 Inquiry_Form
                            this.iquiry_Form.AddMaterial(Material_ID);
                        }
                    }

                    //是否是 修改询价
                    if(this.modifyInqueryForm != null)
                    {
                        if (this.modifyInqueryForm.IsContainsMaterialId(Material_ID))
                        {
                            MessageUtil.ShowWarning("该物料已经添加过，无需重复添加！");
                            return;
                        }
                        else
                        {
                            this.modifyInqueryForm.AddMaterialId(Material_ID);
                        }
                    }

                    //关闭并销毁资源
                    this.Dispose();
                }
                else
                {
                    MessageUtil.ShowWarning("请选择有效的行！");
                }
            }
            catch (BllException ex)
            {
                MessageUtil.ShowError(ex.Msg);
            }
        }
    }
}
