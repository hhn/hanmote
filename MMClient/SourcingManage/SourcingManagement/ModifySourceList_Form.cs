﻿using Lib.Bll.SourcingManage.SourcingManagement;
using Lib.Common.CommonUtils;
using Lib.Common.MMCException.Bll;
using Lib.Model.SourcingManage.SourcingManagement;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MMClient.SourcingManage.SourcingManagement
{
    public partial class ModifySourceList_Form : Form
    {
        private SourceListBLL source_ListBLL = new SourceListBLL();
        public ModifySourceList_Form()
        {
            InitializeComponent();
        }


        public ModifySourceList_Form(string sourceListId)
        {
            InitializeComponent();
            Init(sourceListId);
        }

        private void Init(string sourceListId)
        {
            if(sourceListId != null)
            {
                SourceList sourceList = source_ListBLL.getSourceListBySId(sourceListId);
                if(sourceList != null)
                {
                    this.sourceListId.Text = sourceList.SourceListId;
                    this.materialId.Text = sourceList.Material_ID;
                    this.factoryId.Text = sourceList.Factory_ID;
                    this.supplierId.Text = sourceList.Supplier_ID;
                    this.startTime.Text = sourceList.StartTime.ToString();
                    this.endTime.Text = sourceList.EndTime.ToString();
                    this.fix.Checked = sourceList.Fix == 0 ? false : true;
                    this.blk.Checked = sourceList.Blk == 0 ? false : true;
                    this.mrp.Checked = sourceList.MRP == 0 ? false : true;
                    this.protocolId.Text = sourceList.ProtocolId;
                    this.purchaseOrg.Text = sourceList.PurchaseOrg;
                    this.ppl.Text = sourceList.PPL;
                }
            }
        }

        /// <summary>
        /// 保存修改
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            SourceList sourceList = new SourceList();
            sourceList.SourceListId = this.sourceListId.Text;
            sourceList.Material_ID = this.materialId.Text;
            sourceList.Factory_ID = this.materialId.Text;
            sourceList.Supplier_ID = this.supplierId.Text;
            sourceList.StartTime = DateTime.Parse(this.startTime.Text);
            sourceList.EndTime = DateTime.Parse(this.endTime.Text);
            sourceList.Fix = this.fix.Checked == false ? 0 : 1;
            sourceList.Blk = this.blk.Checked == false ? 0 : 1;
            sourceList.MRP = this.mrp.Checked == false ? 0 : 1;
            sourceList.ProtocolId = this.protocolId.Text;
            sourceList.PurchaseOrg = this.purchaseOrg.Text;
            sourceList.PPL = this.ppl.Text;
            try
            {
                //更新货源清单
                source_ListBLL.updateSourceList(sourceList);
            }
            catch (BllException ex)
            {
                Console.WriteLine(ex.Msg);
                MessageUtil.ShowError(ex.Msg);
            }
            MessageUtil.ShowTips("修改成功！");
            this.Dispose(true);
        }

        /// <summary>
        /// 取消修改
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            this.Dispose(true);
        }
    }
}
