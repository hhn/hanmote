﻿namespace MMClient.SourcingManage.SourcingManagement
{
    partial class SupplierInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.SupplierListDGV = new System.Windows.Forms.DataGridView();
            this.supplierId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.supplierName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.supplierPhone = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.email = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.country = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btn_Cancel = new System.Windows.Forms.Button();
            this.btn_Confirm = new System.Windows.Forms.Button();
            this.lb_Notes = new System.Windows.Forms.Label();
            this.btn_search = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SupplierListDGV)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.SupplierListDGV);
            this.groupBox1.Location = new System.Drawing.Point(137, 66);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox1.Size = new System.Drawing.Size(713, 352);
            this.groupBox1.TabIndex = 14;
            this.groupBox1.TabStop = false;
            // 
            // SupplierListDGV
            // 
            this.SupplierListDGV.AllowUserToAddRows = false;
            this.SupplierListDGV.BackgroundColor = System.Drawing.SystemColors.Control;
            this.SupplierListDGV.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.SupplierListDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.SupplierListDGV.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.supplierId,
            this.supplierName,
            this.supplierPhone,
            this.email,
            this.country});
            this.SupplierListDGV.EnableHeadersVisualStyles = false;
            this.SupplierListDGV.Location = new System.Drawing.Point(25, 18);
            this.SupplierListDGV.Margin = new System.Windows.Forms.Padding(2);
            this.SupplierListDGV.Name = "SupplierListDGV";
            this.SupplierListDGV.ReadOnly = true;
            this.SupplierListDGV.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.LightBlue;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            this.SupplierListDGV.RowsDefaultCellStyle = dataGridViewCellStyle1;
            this.SupplierListDGV.RowTemplate.Height = 27;
            this.SupplierListDGV.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.SupplierListDGV.ShowCellErrors = false;
            this.SupplierListDGV.Size = new System.Drawing.Size(662, 312);
            this.SupplierListDGV.TabIndex = 0;
            this.SupplierListDGV.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.dgv_suppliers_RowPostPaint);
            // 
            // supplierId
            // 
            this.supplierId.HeaderText = "供应商编号";
            this.supplierId.Name = "supplierId";
            this.supplierId.ReadOnly = true;
            this.supplierId.Width = 150;
            // 
            // supplierName
            // 
            this.supplierName.HeaderText = "供应商名称";
            this.supplierName.Name = "supplierName";
            this.supplierName.ReadOnly = true;
            this.supplierName.Width = 150;
            // 
            // supplierPhone
            // 
            this.supplierPhone.HeaderText = "联系电话";
            this.supplierPhone.Name = "supplierPhone";
            this.supplierPhone.ReadOnly = true;
            // 
            // email
            // 
            this.email.HeaderText = "电子邮箱";
            this.email.Name = "email";
            this.email.ReadOnly = true;
            // 
            // country
            // 
            this.country.HeaderText = "国家";
            this.country.Name = "country";
            this.country.ReadOnly = true;
            // 
            // btn_Cancel
            // 
            this.btn_Cancel.Location = new System.Drawing.Point(794, 420);
            this.btn_Cancel.Name = "btn_Cancel";
            this.btn_Cancel.Size = new System.Drawing.Size(65, 32);
            this.btn_Cancel.TabIndex = 13;
            this.btn_Cancel.Text = "关闭";
            this.btn_Cancel.UseVisualStyleBackColor = true;
            this.btn_Cancel.Click += new System.EventHandler(this.btn_Cancel_Click);
            // 
            // btn_Confirm
            // 
            this.btn_Confirm.Location = new System.Drawing.Point(714, 420);
            this.btn_Confirm.Name = "btn_Confirm";
            this.btn_Confirm.Size = new System.Drawing.Size(56, 32);
            this.btn_Confirm.TabIndex = 12;
            this.btn_Confirm.Text = "确定";
            this.btn_Confirm.UseVisualStyleBackColor = true;
            this.btn_Confirm.Click += new System.EventHandler(this.btn_Confirm_Click);
            // 
            // lb_Notes
            // 
            this.lb_Notes.AutoSize = true;
            this.lb_Notes.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lb_Notes.Location = new System.Drawing.Point(12, 430);
            this.lb_Notes.Name = "lb_Notes";
            this.lb_Notes.Size = new System.Drawing.Size(338, 12);
            this.lb_Notes.TabIndex = 11;
            this.lb_Notes.Text = "多选方法：鼠标选中某行上下拖拉或按住ctr键点选多行。";
            // 
            // btn_search
            // 
            this.btn_search.Location = new System.Drawing.Point(815, 25);
            this.btn_search.Name = "btn_search";
            this.btn_search.Size = new System.Drawing.Size(68, 36);
            this.btn_search.TabIndex = 15;
            this.btn_search.Text = "搜索";
            this.btn_search.UseVisualStyleBackColor = true;
            // 
            // SupplierInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 464);
            this.Controls.Add(this.btn_search);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btn_Cancel);
            this.Controls.Add(this.btn_Confirm);
            this.Controls.Add(this.lb_Notes);
            this.Name = "SupplierInfo";
            this.Text = "供应商信息";
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SupplierListDGV)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView SupplierListDGV;
        private System.Windows.Forms.Button btn_Cancel;
        private System.Windows.Forms.Button btn_Confirm;
        private System.Windows.Forms.Label lb_Notes;
        private System.Windows.Forms.Button btn_search;
        private System.Windows.Forms.DataGridViewTextBoxColumn supplierId;
        private System.Windows.Forms.DataGridViewTextBoxColumn supplierName;
        private System.Windows.Forms.DataGridViewTextBoxColumn supplierPhone;
        private System.Windows.Forms.DataGridViewTextBoxColumn email;
        private System.Windows.Forms.DataGridViewTextBoxColumn country;
    }
}