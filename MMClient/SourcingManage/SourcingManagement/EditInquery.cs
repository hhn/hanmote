﻿using Lib.Bll.ServiceBll;
using Lib.Common.CommonUtils;
using Lib.Common.MMCException.Bll;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;

namespace MMClient.SourcingManage.SourcingManagement
{
    public partial class EditInquery : DockContent
    {
        private ServiceBill serviceBill = new ServiceBill();
        public EditInquery()
        {
            InitializeComponent();
            InitData();
        }

        //初始化
        private void InitData()
        {
            try
            {
                this.ClearDGV(this.InqueryPriceDGV);
                DataTable dt = serviceBill.GetAllInqueryPriceInfo();
                this.InqueryPriceDGV.DataSource = dt;
            }
            catch (BllException ex)
            {
                MessageUtil.ShowError(ex.Msg);
            }
        }

        //搜索按钮
        private void search_Click(object sender, EventArgs e)
        {
            string Inquiry_Name = this.InqueryNameTextBox.Text;
            if (!"".Equals(Inquiry_Name))
            {
                this.ClearDGV(this.InqueryPriceDGV);
                DataTable dt = serviceBill.GetInqueryPriceLikeName(Inquiry_Name);
                this.InqueryPriceDGV.DataSource = dt;
            }
            else
            {
                InitData();
            }
        }

        //清空DGV数据
        private void ClearDGV(DataGridView dgv)
        {
            if(dgv != null)
            {
                object obj = dgv.DataSource;
                if(obj != null && obj is DataTable){
                    DataTable dt = (DataTable)dgv.DataSource;
                    dt.Clear();
                }
            }
        }

        //修改按钮
        private void modify_Click(object sender, EventArgs e)
        {
            DataGridViewRow curRow = this.InqueryPriceDGV.CurrentRow;
            if(curRow != null && curRow.Index >= 0)
            {
                string Inquery_ID = curRow.Cells["Inquery_ID"].Value.ToString();
                ModifyInqueryForm modifyInqueryForm = new ModifyInqueryForm(Inquery_ID);
                modifyInqueryForm.Show();
            }
            else
            {
                MessageUtil.ShowTips("请选择有效的询价单！");
            }
        }
    }
}
