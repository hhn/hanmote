﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Model.SourcingManage.SourcingManagement;
using WeifenLuo.WinFormsUI.Docking;
using Lib.Bll.SourcingManage.SourcingManagement;
using Lib.Model.MD.MT;
using Lib.Bll.MDBll.MT;
using Lib.Model.SourcingManage.SourceingRecord;
using Lib.Model.MD.SP;
using Lib.Bll.MDBll.SP;
using Lib.Bll.SourcingManage.SourceingRecord;
using System.Data.SqlClient;
using Lib.SqlServerDAL;

namespace MMClient.SourcingManage.SourcingManagement
{
    public partial class DealTransact_Form : DockContent
    {
        private UserUI userUI;
        private SourceBid sourceBid;

        private SourceBLL sourceBll = new SourceBLL();
        private TransactCostBLL transactCostBll = new TransactCostBLL();
        private TransactItemBLL transactItemBll = new TransactItemBLL();
        private SourceMaterialBLL sourceMaterialBll = new SourceMaterialBLL();
        private SourceSupplierBLL sourceSupplierBll = new SourceSupplierBLL();
        private SourceResultBLL sourceResultBll = new SourceResultBLL();
        private MaterialBLL materialBll = new MaterialBLL();
        private SupplierBaseBLL supplierBaseBLL = new SupplierBaseBLL();
        private RecordInfoBLL recordInfoBLL = new RecordInfoBLL();
        private SourceBidBLL sourceBidBll = new SourceBidBLL();

        private SourceListBLL sourceListBll = new SourceListBLL();

        ConvenientTools tools = new ConvenientTools();

        public DealTransact_Form()
        {
            InitializeComponent();
        }

        public DealTransact_Form(UserUI userUI, SourceBid sourceBid)
        {
            InitializeComponent();
            this.userUI = userUI;
            this.sourceBid = sourceBid;
            init();
        }

        private void init()
        {
            initBaseInfo();
            initSupplierInfo();
            initMaterial();
            initItems();
        }

        /// <summary>
        /// 初始化招标基本信息
        /// </summary>
        private void initBaseInfo()
        {
            txt_BidId.Text = sourceBid.BidId;
            txt_BidName.Text = sourceBll.findSourceById(sourceBid.BidId).Source_Name;
        }

        private void initSupplierInfo()
        {
            SourceSupplier ss = sourceSupplierBll.getSupplierByBidState(sourceBid.BidId);
            if (ss != null)
            {
                txt_SupplierId.Text = ss.Supplier_ID;
            }
        }

        private void initMaterial()
        {
            materialGridView.Rows.Clear();
            List<TransactCost> list = transactCostBll.getTransCostsBySId(sourceBid.BidId,txt_SupplierId.Text);
            if (list.Count > materialGridView.Rows.Count)
            {
                materialGridView.Rows.Add(list.Count - materialGridView.Rows.Count);
            }
            for (int i = 0; i < list.Count; i++)
            {
                materialGridView.Rows[i].Cells["materialId"].Value = list.ElementAt(i).MaterialId;
                materialGridView.Rows[i].Cells["materialName"].Value = list.ElementAt(i).MaterialName;
                materialGridView.Rows[i].Cells["count"].Value = list.ElementAt(i).Count;
                materialGridView.Rows[i].Cells["unit"].Value = list.ElementAt(i).Unit;
                materialGridView.Rows[i].Cells["targetPrice"].Value = list.ElementAt(i).TargetPrice;
                materialGridView.Rows[i].Cells["targetTotal"].Value = list.ElementAt(i).TargetTotal;
                materialGridView.Rows[i].Cells["supplierPrice"].Value = list.ElementAt(i).SupplierPrice;
                materialGridView.Rows[i].Cells["supplierTotal"].Value = list.ElementAt(i).SupplierTotal;
                materialGridView.Rows[i].Cells["anno"].Value = list.ElementAt(i).Note;
            }
        }

        private void initItems()
        {
            itemsGridView.Rows.Clear();
            List<TransactItem> list = transactItemBll.getTransactItemsByBId(sourceBid.BidId,txt_SupplierId.Text);
            if (list.Count > itemsGridView.Rows.Count)
            {
                itemsGridView.Rows.Add(list.Count - itemsGridView.Rows.Count);
            }
            for (int i = 0; i < list.Count; i++)
            {
                itemsGridView.Rows[i].Cells["item"].Value = list.ElementAt(i).ItemName;
                itemsGridView.Rows[i].Cells["goalDescription"].Value = list.ElementAt(i).GoalDescription;
                itemsGridView.Rows[i].Cells["supplierAnswer"].Value = list.ElementAt(i).SupplierAnswer;
                itemsGridView.Rows[i].Cells["note"].Value = list.ElementAt(i).Note;
            }
        }

        //保存谈判结果
        private void btn_Save_Click(object sender, EventArgs e)
        {
            int res = 0;
            int  result = cmb_TransResult.SelectedIndex;
            if (result == 0)
            {
                res = 3;//接受谈判
            }
            else if (result == 1)
            {
                res = 4;//拒绝谈判
            }
            int sf = sourceSupplierBll.updateTransState(txt_BidId.Text,txt_SupplierId.Text,res);
            if (res == 3)
            {
                sourceBid.BidState = 6;
            }
            else
            {
                sourceBid.BidState = 4;
            }
            sourceBidBll.updateBidState(sourceBid);
            if (res == 4)
            {
                this.Close();
                return;
            }
            foreach (DataGridViewRow dgvr in materialGridView.Rows)
            {
                //插入寻源结果
                SourceResult sim = new SourceResult();
                sim.Item_ID = "TX" + tools.systemTimeToStr();
                sim.Source_ID = txt_BidId.Text;
                sim.Supplier_ID = txt_SupplierId.Text;
                sim.Material_ID = dgvr.Cells["materialId"].Value.ToString();
                MaterialBase mb = materialBll.getMaterialBaseById(sim.Material_ID);
                sim.Material_Name = mb.Material_Name;
                sim.Material_Group = mb.Material_Group;
                sim.Measurement = mb.Measurement;
                sim.Net_Price_Unit = mb.Measurement;
                sim.Buy_Number = int.Parse(dgvr.Cells["count"].Value.ToString());
                sim.Provider_Number = int.Parse(dgvr.Cells["count"].Value.ToString());
                sim.Net_Price = float.Parse(dgvr.Cells["supplierPrice"].Value.ToString());
                List<SourceMaterial> list = sourceMaterialBll.findSourceMaterialsBySourceId(txt_BidId.Text);
                for(int i=0;i<list.Count;i++){
                    if(list.ElementAt(i).Material_ID.Equals(sim.Material_ID)){
                        sim.Factory_ID = list.ElementAt(i).FactoryId;
                        sim.Stock_ID = list.ElementAt(i).StockId;
                    }
                }
                sourceResultBll.addSourceResult(sim);

                //将结果信息写入采购信息记录表
                RecordInfo ri = new RecordInfo();
                ri.RecordInfoId = tools.systemTimeToStr();//生成一个采购信息记录编号
                ri.SupplierId = txt_SupplierId.Text;
                SupplierBase sb = supplierBaseBLL.GetSupplierBasicInformation(ri.SupplierId);
                ri.SupplierName = sb.Supplier_Name;//根据供应商编号查询供应商名称
                Source source = sourceBll.findSourceById(sourceBid.BidId);
                ri.PurchaseOrg = source.PurchaseOrg;
                ri.PurchaseGroup = source.PurchaseGroup;
                ri.MaterialId = dgvr.Cells["materialId"].Value.ToString();
                ri.MaterialName = dgvr.Cells["materialName"].Value.ToString();
                ri.FactoryId = sim.Factory_ID;
                ri.StockId = sim.Stock_ID;
                ri.NetPrice = sim.Net_Price;
                ri.DemandCount = sim.Buy_Number;
                ri.FromType = 1;
                ri.FromId = sourceBid.BidId;
                ri.ContactState = "无效";
                ri.StartTime = sourceMaterialBll.findSMBySIdMId(txt_BidId.Text,ri.MaterialId).DeliveryStartTime;
                ri.EndTime = sourceMaterialBll.findSMBySIdMId(txt_BidId.Text, ri.MaterialId).DeliveryEndTime;

                recordInfoBLL.addRecordInfo(ri);
                //招标状态为6，表示该招标已经完成
                

                //生成货源清单
                SourceList sl = new SourceList();
                sl.SourceListId = "S" + tools.systemTimeToStr();
                sl.Material_ID = ri.MaterialId;
                sl.Supplier_ID = ri.SupplierId;
                sl.Factory_ID = ri.FactoryId;
                sl.Fix = 1;
                sl.Blk = 0;
                sl.MRP = 0;
                sl.PurchaseOrg = ri.PurchaseOrg;
                sl.StartTime = ri.StartTime;
                sl.EndTime = ri.EndTime;
                sourceListBll.addSourceList(sl);

                //生成采购申请
                PRSupplier prs = new PRSupplier();
                prs.PR_ID = tools.systemTimeToStr();
                prs.Material_ID = ri.MaterialId;
                prs.Supplier_ID = ri.SupplierId;
                prs.Number = ri.DemandCount;
                string tempTab = "dbo.PR_Supplier";
                StringBuilder ssb = new StringBuilder("insert into " + tempTab + "(PR_ID,Material_ID,Supplier_ID,Number,Finished_Mark) values(@PR_ID,@Material_ID,@Supplier_ID,@Number,@Finished_Mark)");
                SqlParameter[] sqlParas = new SqlParameter[]
            {
                new SqlParameter("@PR_ID",SqlDbType.VarChar),
                new SqlParameter("@Material_ID",SqlDbType.VarChar),
                new SqlParameter("@Supplier_ID",SqlDbType.VarChar),
                new SqlParameter("@Number",SqlDbType.Int),
                new SqlParameter("@Finished_Mark",SqlDbType.Int)
            };
                sqlParas[0].Value = prs.PR_ID;
                sqlParas[1].Value = prs.Material_ID;
                sqlParas[2].Value = prs.Supplier_ID;
                sqlParas[3].Value = prs.Number;
                sqlParas[4].Value = 0;
                string sqlTextt = ssb.ToString();
                DBHelper.ExecuteNonQuery(sqlTextt, sqlParas);
            }

            MessageBox.Show("采购信息记录创建成功");
            MessageBox.Show("货源清单记录成功");
            this.Close();
        }
    }
}
