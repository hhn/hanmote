﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;
using Lib.Bll.SourcingManage.SourcingManagement;
using Lib.Model.SourcingManage.SourcingManagement;

namespace MMClient.SourcingManage.SourcingManagement
{
    public partial class ShowBid_Form : DockContent
    {
        SourceBLL sourceBll = new SourceBLL();
        SourceBidBLL bidBLL = new SourceBidBLL();
        SourceBidBLL sourceBidBll = new SourceBidBLL();

        public SourceBid bid;

        public ShowBid_Form()
        {
            InitializeComponent();
            initBids();
            bidsGridView.TopLeftHeaderCell.Value = "序号";
        }

        private void initBids()
        {
            bidsGridView.Rows.Clear();
            fillBidGridView(getIntByState("待发布"));
            cmb_State.SelectedIndex = 0; 
        }

        private string getStringByNumber(int num)
        {
            string str = null;
            if (num == 0)
            {
                str = "未发布";
            }
            if (num == 1)
            {
                str = "待发布";
            }
            if(num==2)
            {
                str = "待报价";
            }
            if (num == 3)
            {
                str = "待评标";
            }
            if (num == 4)
            {
                str = "已完成";
            }
            return str;
        }


        private void bidsGridView_SelectionChanged(object sender, EventArgs e)
        {
            int index = bidsGridView.CurrentRow.Index;
            Object obj = bidsGridView.Rows[index].Cells[0].Value;
            if (obj == null)
            {
                return;
            } 
            //根据选中行id查找对应的需求计划
            bid = bidBLL.findBidByBidId(obj.ToString());
        }

        /// <summary>
        /// 查看
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            bidsGridView.Rows.Clear();
            string state = cmb_State.Text;
            int intState = getIntByState(state);
            fillBidGridView(intState);
              
        }

        private void fillBidGridView(int state)
        {
            List<SourceBid> list = bidBLL.getSourceBidsByState(state);
            if (list.Count > bidsGridView.Rows.Count)
            {
                bidsGridView.Rows.Add(list.Count - bidsGridView.Rows.Count);
            }
            for (int i = 0; i < list.Count; i++)
            {
                bidsGridView.Rows[i].Cells["bidId"].Value = list.ElementAt(i).BidId;
                Source source = sourceBll.findSourceById(list.ElementAt(i).BidId);
                bidsGridView.Rows[i].Cells["bidName"].Value = source.Source_Name;
                SourceBid sb = sourceBidBll.findBidByBidId(list.ElementAt(i).BidId);
                bidsGridView.Rows[i].Cells["bidCreateTime"].Value = sb.StartTime.ToString("yyyy-MM-dd HH:mm:ss");
                bidsGridView.Rows[i].Cells["bidCreateTime"].Value = source.Create_Time.ToString("yyyy-MM-dd HH:mm:ss");
                bidsGridView.Rows[i].Cells["bidState"].Value = getStringByNumber(list.ElementAt(i).BidState);
            }
        }

        private int getIntByState(string state)
        {
            int res = 0;
            if (state.Equals("待保存"))
            {
                res = 0;
            }
            else if (state.Equals("待发布"))
            {
                res = 1;
            }
            else if (state.Equals("待报价"))
            {
                res = 2;
            }
            else if (state.Equals("待评标"))
            {
                res = 3;
            }
            else if (state.Equals("已完成"))
            {
                res = 6;
            }
            return res;
        }

        private void bidsGridView_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            var dgv = sender as DataGridView;
            if (dgv != null)
            {
                Rectangle rect = new Rectangle(e.RowBounds.Location.X, e.RowBounds.Location.Y, (dgv.RowHeadersWidth + 12) / 2, e.RowBounds.Height);
                TextRenderer.DrawText(e.Graphics, (e.RowIndex + 1).ToString(), dgv.RowHeadersDefaultCellStyle.Font, rect, dgv.RowHeadersDefaultCellStyle.ForeColor, TextFormatFlags.VerticalCenter | TextFormatFlags.Right);
            }
        }
    }
}
