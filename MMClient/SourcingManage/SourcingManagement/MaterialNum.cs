﻿using Lib.Bll.ServiceBll;
using Lib.Common.CommonUtils;
using Lib.Common.MMCException.Bll;
using Lib.Model.ServiceModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MMClient.SourcingManage.SourcingManagement
{
    public partial class MaterialNum : Form
    {
        //业务对象
        private ServiceBill serviceBill = new ServiceBill();
        private Inquiry_Form inquiry_Form = null;
        private ModifyInqueryForm modifyInqueryForm = null;
        
        ////物料编码
        //private string Material_ID;
        ////物料名称
        //private string Material_Name;
        ////物料类型
        //private string Material_Type;
        ////物料组
        //private string Material_Group;
        ////需求数量
        //private int Demand_Count;
        ////需求单号
        //private string Demand_Number;
        ////工厂编号
        //private string Factory_ID;
        ////仓库编号
        //private string Stock_ID;
        ////单位
        //private string Unit;
        ////交货开始时间
        //private DateTime DeliveryStartTime;
        ////交货结束时间
        //private DateTime DeliveryEndTime;
        ////询价单号
        //private string Source_ID;

        //创建询价单
        public MaterialNum(Inquiry_Form inquiry_Form,string demand_Number, string source_ID, string material_ID, string material_Name, string material_Type, string material_Group)
        {
            InitializeComponent();
            this.inquiry_Form = inquiry_Form;
            this.InitData(demand_Number, material_ID, material_Name, material_Group, material_Type, source_ID);
        }

        //修改询价单
        public MaterialNum(ModifyInqueryForm modifyInqueryForm,string material_Type, string material_Group, string material_ID, string source_ID)
        {
            InitializeComponent();
            this.modifyInqueryForm = modifyInqueryForm;
            this.InitData(material_Type,material_Group, material_ID, source_ID);
        }

        //初始化界面数据（添加）
        private void InitData(string demand_Number,string material_ID,string material_Name,string material_Group,string material_Type,string source_ID)
        {
            this.MaterialNum_MaterialId.Text = material_ID;
            this.MaterialNum_MaterialName.Text = material_Name;
            this.MaterialNum_MaterialGroup.Text = material_Group;
            this.MaterialNum_MaterialType.Text = material_Type;
            this.MaterialNum_SourceId.Text = source_ID;

            if(demand_Number != null)
            {
                //TODO
                //加载需求单对应的数据
            }
        }

        //初始化界面数据（修改）
        private void InitData(string material_Type, string material_Group,string Material_ID , string Source_ID)
        {
            SourceMaterial sourceMaterial = serviceBill.GetSourceMaterialByMaterialIdAndInqueryId(Material_ID, Source_ID);
            this.MaterialNum_MaterialId.Text = sourceMaterial.Material_ID;
            this.MaterialNum_MaterialName.Text = sourceMaterial.Material_Name;
            this.MaterialNum_MaterialType.Text = material_Type;
            this.MaterialNum_MaterialGroup.Text = material_Group;
            this.MaterialNum_DemandCount.Text = sourceMaterial.Demand_Count.ToString();
            this.MaterialNum_DemandId.Text = sourceMaterial.Demand_ID;
            this.MaterialNum_FactoryId.Text = sourceMaterial.Factory_ID;
            this.MaterialNum_StockId.Text = sourceMaterial.Stock_ID;
            this.MaterialNum_Unit.Text = sourceMaterial.Unit;
            this.MaterialNum_SourceId.Text = sourceMaterial.Source_ID;
        }

        //保存按钮
        private void button1_Click(object sender, EventArgs e)
        {
            //Source_Material（寻源物料表）
            //Material_ID
            //Material_Name
            //Demand_Count
            //Demand_ID
            //Unit
            //Factory_ID
            //Stock_ID
            //DeliveryStartTime
            //DeliveryEndTime
            //Source_ID
            string Material_ID = this.MaterialNum_MaterialId.Text;
            string Material_Name = this.MaterialNum_MaterialName.Text;
            string Demand_CountStr = this.MaterialNum_DemandCount.Text; //int
            int Demand_Count = Demand_CountStr == "" ? 0 : Convert.ToInt32(Demand_CountStr);
            string Demand_ID = this.MaterialNum_DemandId.Text;
            string Unit = this.MaterialNum_Unit.Text;
            string Factory_ID = this.MaterialNum_FactoryId.Text;
            string Stock_ID = this.MaterialNum_StockId.Text;
            string DeliveryStartTimeStr = this.MaterialNum_DeliveryStartTime.Text;
            DateTime DeliveryStartTime = Convert.ToDateTime(DeliveryStartTimeStr);
            string DeliveryEndTimeStr = this.MaterialNum_DeliveryEndTime.Text;
            DateTime DeliveryEndTime = Convert.ToDateTime(DeliveryEndTimeStr);
            string Source_ID = this.MaterialNum_SourceId.Text;
            SourceMaterial sourceMaterial = new SourceMaterial(Material_ID, Material_Name, Demand_Count, Demand_ID, Unit, Factory_ID, Stock_ID, DeliveryStartTime, DeliveryEndTime, Source_ID);
            if(this.inquiry_Form != null)
            {
                //this.inquiry_Form.sourceMaterialList.Add(sourceMaterial);
            }
            if(this.modifyInqueryForm != null)
            {
                this.modifyInqueryForm.AddSourceMaterial(sourceMaterial);
            }
            //serviceBill.SaveSourceMaterial(sourceMaterial);
            MessageUtil.ShowTips("保存成功！");
        }

        //关闭按钮
        private void button2_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }
    }
}
