﻿namespace MMClient.SourcingManage.SourcingManagement
{
    partial class AddCondition_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txt_ConditionName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.cmb_ConditionCategory = new System.Windows.Forms.ComboBox();
            this.cmb_RoundType = new System.Windows.Forms.ComboBox();
            this.cmb_Pn = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cmb_CalType = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txt_ConditionId = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.rtb_Description = new System.Windows.Forms.RichTextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(55, 77);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "条件类型";
            // 
            // txt_ConditionName
            // 
            this.txt_ConditionName.Location = new System.Drawing.Point(115, 74);
            this.txt_ConditionName.Name = "txt_ConditionName";
            this.txt_ConditionName.Size = new System.Drawing.Size(121, 21);
            this.txt_ConditionName.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(57, 116);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 2;
            this.label2.Text = "定价类别";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(57, 208);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 12);
            this.label3.TabIndex = 2;
            this.label3.Text = "四舍五入";
            // 
            // cmb_ConditionCategory
            // 
            this.cmb_ConditionCategory.FormattingEnabled = true;
            this.cmb_ConditionCategory.Items.AddRange(new object[] {
            "价格",
            "折扣",
            "附加费",
            "税收"});
            this.cmb_ConditionCategory.Location = new System.Drawing.Point(115, 116);
            this.cmb_ConditionCategory.Name = "cmb_ConditionCategory";
            this.cmb_ConditionCategory.Size = new System.Drawing.Size(121, 20);
            this.cmb_ConditionCategory.TabIndex = 3;
            // 
            // cmb_RoundType
            // 
            this.cmb_RoundType.FormattingEnabled = true;
            this.cmb_RoundType.Items.AddRange(new object[] {
            "四舍五入",
            "较高值",
            "较低值"});
            this.cmb_RoundType.Location = new System.Drawing.Point(115, 205);
            this.cmb_RoundType.Name = "cmb_RoundType";
            this.cmb_RoundType.Size = new System.Drawing.Size(121, 20);
            this.cmb_RoundType.TabIndex = 3;
            // 
            // cmb_Pn
            // 
            this.cmb_Pn.FormattingEnabled = true;
            this.cmb_Pn.Items.AddRange(new object[] {
            "+",
            "-"});
            this.cmb_Pn.Location = new System.Drawing.Point(115, 246);
            this.cmb_Pn.Name = "cmb_Pn";
            this.cmb_Pn.Size = new System.Drawing.Size(121, 20);
            this.cmb_Pn.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(57, 254);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 12);
            this.label4.TabIndex = 2;
            this.label4.Text = "正/负";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(264, 392);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 4;
            this.button1.Text = "添加";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cmb_CalType);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.txt_ConditionId);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.rtb_Description);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.txt_ConditionName);
            this.groupBox1.Controls.Add(this.cmb_Pn);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.cmb_RoundType);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.cmb_ConditionCategory);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Location = new System.Drawing.Point(46, 21);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(474, 480);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "添加条件类型";
            // 
            // cmb_CalType
            // 
            this.cmb_CalType.FormattingEnabled = true;
            this.cmb_CalType.Items.AddRange(new object[] {
            "数量",
            "百分比"});
            this.cmb_CalType.Location = new System.Drawing.Point(119, 159);
            this.cmb_CalType.Name = "cmb_CalType";
            this.cmb_CalType.Size = new System.Drawing.Size(121, 20);
            this.cmb_CalType.TabIndex = 10;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(60, 162);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 12);
            this.label7.TabIndex = 9;
            this.label7.Text = "计算类型";
            // 
            // txt_ConditionId
            // 
            this.txt_ConditionId.Location = new System.Drawing.Point(115, 37);
            this.txt_ConditionId.Name = "txt_ConditionId";
            this.txt_ConditionId.Size = new System.Drawing.Size(121, 21);
            this.txt_ConditionId.TabIndex = 8;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(57, 40);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 12);
            this.label6.TabIndex = 7;
            this.label6.Text = "条件编号";
            // 
            // rtb_Description
            // 
            this.rtb_Description.Location = new System.Drawing.Point(115, 286);
            this.rtb_Description.Name = "rtb_Description";
            this.rtb_Description.Size = new System.Drawing.Size(224, 63);
            this.rtb_Description.TabIndex = 6;
            this.rtb_Description.Text = "";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(57, 289);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(29, 12);
            this.label5.TabIndex = 5;
            this.label5.Text = "描述";
            // 
            // AddCondition_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(936, 546);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "AddCondition_Form";
            this.Text = "添加条件类型";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txt_ConditionName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cmb_ConditionCategory;
        private System.Windows.Forms.ComboBox cmb_RoundType;
        private System.Windows.Forms.ComboBox cmb_Pn;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txt_ConditionId;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.RichTextBox rtb_Description;
        private System.Windows.Forms.ComboBox cmb_CalType;
        private System.Windows.Forms.Label label7;
    }
}