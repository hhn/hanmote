﻿namespace MMClient.SourcingManage.SourcingManagement
{
    partial class ModifySourceList_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.sourceListId = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.materialId = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.factoryId = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.supplierId = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.startTime = new System.Windows.Forms.DateTimePicker();
            this.label7 = new System.Windows.Forms.Label();
            this.endTime = new System.Windows.Forms.DateTimePicker();
            this.fix = new System.Windows.Forms.CheckBox();
            this.blk = new System.Windows.Forms.CheckBox();
            this.mrp = new System.Windows.Forms.CheckBox();
            this.label8 = new System.Windows.Forms.Label();
            this.protocolId = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.purchaseOrg = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.ppl = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // sourceListId
            // 
            this.sourceListId.Location = new System.Drawing.Point(100, 29);
            this.sourceListId.Name = "sourceListId";
            this.sourceListId.ReadOnly = true;
            this.sourceListId.Size = new System.Drawing.Size(135, 21);
            this.sourceListId.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(5, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 12);
            this.label1.TabIndex = 1;
            this.label1.Text = "货源清单编号";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(267, 35);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 2;
            this.label2.Text = "物料编号";
            // 
            // materialId
            // 
            this.materialId.Location = new System.Drawing.Point(326, 29);
            this.materialId.Name = "materialId";
            this.materialId.Size = new System.Drawing.Size(146, 21);
            this.materialId.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(520, 35);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 12);
            this.label3.TabIndex = 4;
            this.label3.Text = "工厂编号";
            // 
            // factoryId
            // 
            this.factoryId.Location = new System.Drawing.Point(588, 29);
            this.factoryId.Name = "factoryId";
            this.factoryId.Size = new System.Drawing.Size(130, 21);
            this.factoryId.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(17, 124);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 12);
            this.label4.TabIndex = 6;
            this.label4.Text = "供应商编号";
            // 
            // supplierId
            // 
            this.supplierId.Location = new System.Drawing.Point(100, 118);
            this.supplierId.Name = "supplierId";
            this.supplierId.Size = new System.Drawing.Size(135, 21);
            this.supplierId.TabIndex = 7;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(267, 127);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(0, 12);
            this.label5.TabIndex = 8;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(267, 127);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 12);
            this.label6.TabIndex = 10;
            this.label6.Text = "开始时间";
            // 
            // startTime
            // 
            this.startTime.Location = new System.Drawing.Point(326, 118);
            this.startTime.Name = "startTime";
            this.startTime.Size = new System.Drawing.Size(146, 21);
            this.startTime.TabIndex = 11;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(520, 127);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 12);
            this.label7.TabIndex = 12;
            this.label7.Text = "结束时间";
            // 
            // endTime
            // 
            this.endTime.Location = new System.Drawing.Point(588, 121);
            this.endTime.Name = "endTime";
            this.endTime.Size = new System.Drawing.Size(130, 21);
            this.endTime.TabIndex = 13;
            // 
            // fix
            // 
            this.fix.AutoSize = true;
            this.fix.Location = new System.Drawing.Point(46, 219);
            this.fix.Name = "fix";
            this.fix.Size = new System.Drawing.Size(48, 16);
            this.fix.TabIndex = 15;
            this.fix.Text = "固定";
            this.fix.UseVisualStyleBackColor = true;
            // 
            // blk
            // 
            this.blk.AutoSize = true;
            this.blk.Location = new System.Drawing.Point(272, 219);
            this.blk.Name = "blk";
            this.blk.Size = new System.Drawing.Size(48, 16);
            this.blk.TabIndex = 16;
            this.blk.Text = "排除";
            this.blk.UseVisualStyleBackColor = true;
            // 
            // mrp
            // 
            this.mrp.AutoSize = true;
            this.mrp.Location = new System.Drawing.Point(531, 219);
            this.mrp.Name = "mrp";
            this.mrp.Size = new System.Drawing.Size(42, 16);
            this.mrp.TabIndex = 17;
            this.mrp.Text = "mrp";
            this.mrp.UseVisualStyleBackColor = true;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(41, 291);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(41, 12);
            this.label8.TabIndex = 18;
            this.label8.Text = "协议号";
            // 
            // protocolId
            // 
            this.protocolId.Location = new System.Drawing.Point(100, 288);
            this.protocolId.Name = "protocolId";
            this.protocolId.Size = new System.Drawing.Size(135, 21);
            this.protocolId.TabIndex = 19;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(270, 291);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(53, 12);
            this.label9.TabIndex = 20;
            this.label9.Text = "采购组织";
            // 
            // purchaseOrg
            // 
            this.purchaseOrg.Location = new System.Drawing.Point(326, 288);
            this.purchaseOrg.Name = "purchaseOrg";
            this.purchaseOrg.Size = new System.Drawing.Size(146, 21);
            this.purchaseOrg.TabIndex = 21;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(550, 297);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(23, 12);
            this.label10.TabIndex = 22;
            this.label10.Text = "ppl";
            // 
            // ppl
            // 
            this.ppl.Location = new System.Drawing.Point(588, 288);
            this.ppl.Name = "ppl";
            this.ppl.Size = new System.Drawing.Size(130, 21);
            this.ppl.TabIndex = 23;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(606, 397);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 24;
            this.button1.Text = "保存修改";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(713, 397);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 25;
            this.button2.Text = "取消修改";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // ModifySourceList_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.ppl);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.purchaseOrg);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.protocolId);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.mrp);
            this.Controls.Add(this.blk);
            this.Controls.Add(this.fix);
            this.Controls.Add(this.endTime);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.startTime);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.supplierId);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.factoryId);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.materialId);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.sourceListId);
            this.Name = "ModifySourceList_Form";
            this.Text = "修改货源清单";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox sourceListId;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox materialId;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox factoryId;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox supplierId;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DateTimePicker startTime;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DateTimePicker endTime;
        private System.Windows.Forms.CheckBox fix;
        private System.Windows.Forms.CheckBox blk;
        private System.Windows.Forms.CheckBox mrp;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox protocolId;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox purchaseOrg;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox ppl;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
    }
}