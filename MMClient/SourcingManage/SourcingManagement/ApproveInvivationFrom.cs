﻿using Lib.Bll.ServiceBll;
using Lib.Common.CommonUtils;
using Lib.Model.CommonModel;
using Lib.Model.ServiceModel;
using Lib.Model.SystemConfig;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;

namespace MMClient.SourcingManage.SourcingManagement
{
    public partial class ApproveInvivationFrom : DockContent
    {
        //业务组件
        private ServiceBill serviceBill = new ServiceBill();
        public ApproveInvivationFrom()
        {
            InitializeComponent();
            Init();
        }

        //初始化
        private void Init()
        {
            string roleId = SingleUserInstance.getCurrentUserInstance().Role_ID;
            string roleName = serviceBill.GetRoleNameById(roleId);
            this.RoleText.Text = roleName;

            this.ApproveCB.SelectedIndex = 0;
            this.CheckResultCB.SelectedIndex = 0;
            this.ApproveResultCB.SelectedIndex = 0;
        }

        //状态下拉框
        private void ApproveCB_SelectedIndexChanged(object sender, EventArgs e)
        {
            //用户角色
            string roleName = CurUserRole();
            //审批状态
            string approveState = this.ApproveCB.SelectedItem.ToString();

            //清空原来表中的数据
            this.Clear(this.TotalDGV);

            if (!"请选择".Equals(approveState))
            {
                switch (roleName)
                {
                    /*
                         职员（相关部门）：审核所有物料，但是不能审批任何物料。
                     */
                    case "职员": this.ClerkAction(approveState); break;
                    /*
                        部门总监：审核瓶颈物料和杠杆物料。审批一般物料。
                    */
                    case "部门总监": this.ChiefInspectorAction(approveState); break;
                    /*
                         采购副总裁：审核战略物料，审批杠杆物料，瓶颈物料
                     */
                    case "采购副总裁": this.VicePresidentProcurement(approveState); break;
                    /*
                         采购高级副总裁：不审核任何物料，审批战略物料。
                     */
                    case "采购高级副总裁": this.HighVicePresidentProcurement(approveState); break;

                    default:
                        break;
                }
            }
        }

        //清空DGV数据表中的数据
        private void Clear(DataGridView dgv)
        {
            if(dgv != null && dgv.DataSource != null)
            {
                object obj = dgv.DataSource;
                if(obj != null && obj is DataTable)
                {
                    DataTable dt = (DataTable)obj;
                    dt.Clear();
                }
            }
        }

        //职员
        private void ClerkAction(string approveState)
        {
            if (!"待审批".Equals(approveState))
            {
                DataTable dt = serviceBill.GetInqueryPriceByState(approveState,0 , null);
                this.TotalDGV.DataSource = dt;
            }
            else
            {
                MessageUtil.ShowWarning("对不起！您没有权限批准，但是您有权限审核。");
            }
        }

        //部门总监
        private void ChiefInspectorAction(string approveState)
        {
            DataTable dt = null;
            if ("待审核".Equals(approveState))
            {
                dt = serviceBill.GetInqueryPriceByState(approveState, 1, "!战略物料");
            }else if ("待审批".Equals(approveState))
            {
                dt = serviceBill.GetInqueryPriceByState(approveState, 2, "一般物料");
            }
            this.TotalDGV.DataSource = dt;
        }

        //采购副总裁
        private void VicePresidentProcurement(string approveState)
        {
            DataTable dt = null;
            if ("待审核".Equals(approveState))
            {
                //只审核战略物料
                dt = serviceBill.GetInqueryPriceByState(approveState,1,"战略物料");
            }
            else if("待审批".Equals(approveState))
            {
                //审批杠杆物料，瓶颈物料
                DataTable dt1 = serviceBill.GetInqueryPriceByState(approveState, 2, "杠杆物料");
                DataTable dt2 = serviceBill.GetInqueryPriceByState(approveState, 2, "瓶颈物料");
                dt = MergeDataTable(dt1, dt2);
            }
            this.TotalDGV.DataSource = dt;
        }

        //采购高级副总裁
        private void HighVicePresidentProcurement(string approveState)
        {
            if ("待审核".Equals(approveState))
            {
                MessageUtil.ShowWarning("您无需审核任务类型询价单！");
            }else if ("待审批".Equals(approveState))
            {
                DataTable dt = serviceBill.GetInqueryPriceByState(approveState,2,"战略物料");
                this.TotalDGV.DataSource = dt;
            }
        }

        //合并两个DataTable
        private DataTable MergeDataTable(DataTable dt1 , DataTable dt2)
        {
            DataTable newDataTable = dt1.Clone();

            object[] obj = new object[newDataTable.Columns.Count];
            //添加DataTable1的数据
            for (int i = 0; i < dt1.Rows.Count; i++)
            {
                dt1.Rows[i].ItemArray.CopyTo(obj, 0);
                newDataTable.Rows.Add(obj);
            }
            //添加DataTable2的数据
            for (int i = 0; i < dt2.Rows.Count; i++)
            {
                dt2.Rows[i].ItemArray.CopyTo(obj, 0);
                newDataTable.Rows.Add(obj);
            }
            return newDataTable;
        }

        //返回当前用户的角色
        private string CurUserRole()
        {
            return this.RoleText.Text;
        }

        //返回当前审批状态
        private string CurApproveState()
        {
            if(this.ApproveCB.SelectedItem != null)
            {
                return this.ApproveCB.SelectedItem.ToString();
            }
            return null;
        }

        //审核意见
        private void richTextBox1_MouseClick(object sender, MouseEventArgs e)
        {
            string roleName = CurUserRole();
            string approveState = CurApproveState();
            switch (roleName)
            {
                case "部门总监":
                    this.ChiefInspectorPower(approveState, 1);
                    break;

                case "采购副总裁":
                    this.VicePresidentProcurementPower(approveState, 1);
                    break;

                case "采购高级副总裁":
                    this.HighVicePresidentProcurementPower(approveState, 1);
                    break;
            }
        }

        //审核结果下拉框
        private void CheckResultCB_MouseClick(object sender, MouseEventArgs e)
        {
            string roleName = CurUserRole();
            string approveState = CurApproveState();
            switch (roleName)
            {
                case "部门总监":
                    this.ChiefInspectorPower(approveState, 1);
                    break;

                case "采购副总裁":
                    this.VicePresidentProcurementPower(approveState, 1);
                    break;

                case "采购高级副总裁":
                    this.HighVicePresidentProcurementPower(approveState, 1);
                    break;
            }
        }

        //审批意见点击事件
        private void ApproveRText_MouseClick(object sender, MouseEventArgs e)
        {
            string roleName = CurUserRole();
            string approveState = CurApproveState();
            switch (roleName)
            {
                case "职员":
                    this.ClerkPower();
                    break;
                case "部门总监":
                    this.ChiefInspectorPower(approveState, 0);
                    break;

                case "采购副总裁":
                    this.VicePresidentProcurementPower(approveState, 0);
                    break;

                case "采购高级副总裁":
                    this.HighVicePresidentProcurementPower(approveState, 0);
                    break;
            }
        }

        //审批结果下拉框
        private void ApproveResultCB_MouseClick(object sender, MouseEventArgs e)
        {
            string roleName = CurUserRole();
            string approveState = CurApproveState();
            switch (roleName)
            {
                case "职员":
                    this.ClerkPower();
                    break;
                case "部门总监":
                    this.ChiefInspectorPower(approveState, 0);
                    break;
                case "采购副总裁":
                    this.VicePresidentProcurementPower(approveState, 0);
                    break;

                case "采购高级副总裁":
                    this.HighVicePresidentProcurementPower(approveState, 0);
                    break;
            }
        }

        //获取选中行的询价单号
        private string SelectedRowSourceId()
        {
            //获取选中的数据行
            DataGridViewRow curRow = this.TotalDGV.CurrentRow;
            if (curRow != null && curRow.Index >= 0)
            {
                //询价单号
                return curRow.Cells["Source_ID"].Value.ToString();
            }
            return null;
        }

        //职员权限
        private void ClerkPower()
        {
            this.ApproveRText.ReadOnly = true;
            this.ApproveResultCB.SelectedIndex = 0;
            MessageUtil.ShowWarning("对不起！您没有权限进行审批。");
        }


        /// <summary>
        /// 部门总监权限
        /// </summary>
        /// <param name="approveState"></param>
        /// <param name="approveType">0 代表点击审批，1代表点击审核</param>
        private void ChiefInspectorPower(string approveState , int approveType)
        {
            if ("待审核".Equals(approveState) && approveType == 0)
            {
                this.ApproveRText.ReadOnly = true;
                this.CheckResultCB.SelectedIndex = 0;
                MessageUtil.ShowWarning("此为审核项，请填写审核相关内容。");
            }else if("待审批".Equals(approveState) && approveType == 1)
            {
                this.richTextBox1.ReadOnly = true;
                this.CheckResultCB.SelectedIndex = 0;
                MessageUtil.ShowWarning("此为审批项，请填写审批相关内容。");
            }
        }

        //采购副总裁
        private void VicePresidentProcurementPower(string approveState , int approveType)
        {
            this.ChiefInspectorPower(approveState, approveType);
        }

        //采购高级副总裁
        private void HighVicePresidentProcurementPower(string approveState , int approveType)
        {
            this.ChiefInspectorPower(approveState, approveType);
        }

        //提交按钮
        private void CommitBtn_Click(object sender, EventArgs e)
        {
            try
            {
                string sourceId = SelectedRowSourceId();
                InqueryPrice inqueryPrice = serviceBill.GetInqueryPriceById(sourceId);

                //审批状态类型
                string approveState = this.ApproveCB.SelectedItem.ToString();
                if ("待审核".Equals(approveState))
                {
                    //获取审核结果
                    string checkResult = this.CheckResultCB.SelectedItem.ToString();
                    if ("审核通过".Equals(checkResult))
                    {
                        //更新审核次数（+1）
                        inqueryPrice.Approve_Count += 1;
                        //更新审核状态
                        if (inqueryPrice.Approve_Count == 2)
                        {
                            inqueryPrice.State = "待审批";
                        }
                    }
                    else if ("审核不通过".Equals(checkResult))
                    {
                        //更新审核次数（置零）
                        inqueryPrice.Approve_Count = 0;
                        //更新审核状态
                        inqueryPrice.State = "待审核";
                    }
                    else if ("请选择".Equals(checkResult))
                    {
                        MessageUtil.ShowWarning("请选择审核结果再提交！");
                        return;
                    }
                }
                else if ("待审批".Equals(approveState))
                {
                    //获取审批结果
                    string approveResult = this.ApproveResultCB.SelectedItem.ToString();
                    if ("批准".Equals(approveResult))
                    {
                        //更新审核次数
                        inqueryPrice.Approve_Count += 1;
                        //更新审核状态
                        inqueryPrice.State = "已批准";
                    }
                    else if ("不批准".Equals(approveResult))
                    {
                        //更新审核次数（置零）
                        inqueryPrice.Approve_Count = 0;
                        //更新审核状态
                        inqueryPrice.State = "待审核";
                    }
                    else if ("请选择".Equals(approveResult))
                    {
                        MessageUtil.ShowWarning("请选择审批结果再提交！");
                        return;
                    }
                }

                //保存或更新到数据库
                serviceBill.UpdateInqueryPrice(inqueryPrice);
                this.Freshen();
                MessageUtil.ShowTips("保存成功！");
            }
            catch(Exception ex)
            {
                MessageUtil.ShowError("保存失败！原因为" + ex.Message);
            }
        }

        //刷新
        private void Freshen()
        {
            this.ApproveCB.SelectedIndex = 0;
            this.Clear(this.TotalDGV);

            this.richTextBox1.Text = "";
            this.CheckResultCB.SelectedIndex = 0;

            this.ApproveRText.Text = "";
            this.ApproveResultCB.SelectedIndex = 0;
        }

        private void CloseBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
