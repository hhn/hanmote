﻿using Lib.Bll.ServiceBll;
using Lib.Common.CommonUtils;
using Lib.Model.ServiceModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MMClient.SourcingManage.SourcingManagement
{
    public partial class ModifyInqueryForm : Form
    {
        private ServiceBill serviceBill = new ServiceBill();

        //询价单已有的待寻源物料ID数据
        private ISet<string> sourceMaterialIdSet = null;

        //新增的待询价的物料ID数据
        private ISet<string> newSourceMaterialIdSet = null;

        //待删除寻源物料ID
        private ISet<string> deleteSourceMaterialIdSet = null;

        //增加的待插入的寻源物料数据
        private List<SourceMaterial> addSourceMaterialList = null;

        //修改的待更新的寻源物料数据
        private List<SourceMaterial> updateSourceMaterialList = null;

        //询价单编号
        private string Inquery_ID;
        public ModifyInqueryForm(string Inquery_ID)
        {
            InitializeComponent();
            this.Inquery_ID = Inquery_ID;
            InitData();
        }

        //初始化
        private void InitData()
        {
            //查询询价单基本信息
            InqueryPrice inqueryPrice = serviceBill.GetInqueryPriceById(this.Inquery_ID);
            this.txt_inquiryId.Text = inqueryPrice.Source_ID;
            this.txt_Purchase_Org.Text = inqueryPrice.Purchase_Org;
            this.txt_InquiryName.Text = inqueryPrice.Inquiry_Name;
            this.txt_BuyerName.Text = inqueryPrice.Buyer_Name;
            this.dateTimePicker_CreateTime.Text = inqueryPrice.CreateTime.ToString();
            this.dateTimePicker_Offer_Time.Text = inqueryPrice.Offer_Time.ToString();
            this.txt_state.Text = inqueryPrice.State;
            this.txt_Purchase_Group.Text = inqueryPrice.Purchase_Group;
            this.txt_BuyerName.Text = inqueryPrice.Buyer_Name;

            //查询该询价单下的物料
            DataTable sourceMaterialDt = serviceBill.GetSourceMaterialByInqueryId(this.Inquery_ID);
            //添加到物料字典集合
            this.AddToSourceMaterialSet(sourceMaterialDt);
            SourceMaterialListDGV.DataSource = sourceMaterialDt;

            //查询该询价单下的供应商

        }

        //裁剪DataTable
        private void CutDataTable(DataTable dt ,string colName)
        {
            if(dt != null && dt.Columns.Count > 0)
            {
                dt.Columns.Remove(colName);
            }
        }

        //刷新待寻源物料数据
        private void FreshMaterial()
        {
            this.ClearDGV(this.SourceMaterialListDGV);
            if(this.sourceMaterialIdSet != null && this.newSourceMaterialIdSet != null)
            {
                ISet<string> totalSet = new HashSet<string>();
                foreach(string smId in this.sourceMaterialIdSet)
                {
                    totalSet.Add(smId);
                }
                foreach(string smId in this.newSourceMaterialIdSet)
                {
                    totalSet.Add(smId);
                }
                DataTable dt = serviceBill.GetMaterialByMaterialIdList(totalSet);
                this.SourceMaterialListDGV.DataSource = dt;
            }
            
            if(this.sourceMaterialIdSet != null && this.newSourceMaterialIdSet == null)
            {
                DataTable dt = serviceBill.GetMaterialByMaterialIdList(this.sourceMaterialIdSet);
                this.SourceMaterialListDGV.DataSource = dt;
            }

            if(this.sourceMaterialIdSet == null && this.newSourceMaterialIdSet != null)
            {
                DataTable dt = serviceBill.GetMaterialByMaterialIdList(this.newSourceMaterialIdSet);
                this.SourceMaterialListDGV.DataSource = dt;
            }
        }

        //清空DGV的数据
        private void ClearDGV(DataGridView dgv)
        {
            if(dgv != null)
            {
                object obj = dgv.DataSource;
                if(obj != null && (obj is DataTable))
                {
                    DataTable dt = (DataTable)obj;
                    dt.Clear();
                }
            }
        }

        //判断是否包含该物料
        public bool IsContainsMaterialId(string materialId)
        {
            bool sourceMaterialBool = false;
            bool newSourceMaterialBool = false;
            if (this.sourceMaterialIdSet != null)
            {
                sourceMaterialBool = this.sourceMaterialIdSet.Contains(materialId);
            }

            if(this.newSourceMaterialIdSet != null)
            {
                newSourceMaterialBool = this.newSourceMaterialIdSet.Contains(materialId);
            }
            return (sourceMaterialBool || newSourceMaterialBool);
        }

        //添加物料ID
        public void AddMaterialId(string materialId)
        {
            if(this.deleteSourceMaterialIdSet != null && this.deleteSourceMaterialIdSet.Contains(materialId))
            {
                this.deleteSourceMaterialIdSet.Remove(materialId);
                this.sourceMaterialIdSet.Add(materialId);
            }
            else
            {
                if (this.newSourceMaterialIdSet == null)
                {
                    this.newSourceMaterialIdSet = new HashSet<string>();
                }
                this.newSourceMaterialIdSet.Add(materialId);
            }

            this.FreshMaterial();
        }

        //添加寻源物料对象
        public void AddSourceMaterial(SourceMaterial sourceMaterial)
        {
            string material_ID = sourceMaterial.Material_ID;
            if(this.sourceMaterialIdSet != null && this.sourceMaterialIdSet.Contains(material_ID))
            {
                if(this.updateSourceMaterialList == null)
                {
                    this.updateSourceMaterialList = new List<SourceMaterial>();
                }else if (this.updateSourceMaterialList.Contains(sourceMaterial))
                {
                    this.updateSourceMaterialList.Remove(sourceMaterial);
                }
                this.updateSourceMaterialList.Add(sourceMaterial);
            }
            else
            {
                if(this.addSourceMaterialList == null)
                {
                    this.addSourceMaterialList = new List<SourceMaterial>();
                }else if (this.addSourceMaterialList.Contains(sourceMaterial))
                {
                    this.addSourceMaterialList.Remove(sourceMaterial);
                }
                this.addSourceMaterialList.Add(sourceMaterial);
            }
        }


        //将DataTable转存到集合中
        private void AddToSourceMaterialSet(DataTable dt)
        {
            if(dt != null && dt.Rows.Count > 0)
            {
                if(this.sourceMaterialIdSet == null)
                {
                    this.sourceMaterialIdSet = new HashSet<string>();
                }
                for(int i = 0;i < dt.Rows.Count; i++)
                {
                    DataRow dataRow = dt.Rows[i];
                    //SourceMaterial sourceMaterial = new SourceMaterial();
                    //sourceMaterial.Material_ID = dataRow["Material_ID"].ToString();
                    //sourceMaterial.Material_Name = dataRow["Material_Name"].ToString();
                    //sourceMaterial.Demand_Count = Convert.ToInt32(dataRow["Demand_Count"].ToString());
                    //sourceMaterial.Demand_ID = dataRow["Demand_ID"].ToString();
                    //sourceMaterial.Unit = dataRow["Unit"].ToString();
                    //sourceMaterial.Factory_ID = dataRow["Factory_ID"].ToString();
                    //sourceMaterial.Stock_ID = dataRow["Stock_ID"].ToString();
                    //sourceMaterial.DeliveryStartTime = Convert.ToDateTime(dataRow["DeliveryStartTime"].ToString());
                    //sourceMaterial.DeliveryEndTime = Convert.ToDateTime(dataRow["DeliveryEndTime"].ToString());
                    string Material_ID = dataRow["Material_ID"].ToString();
                    this.sourceMaterialIdSet.Add(Material_ID);
                }
            }
        }

        //去除按钮（物料）
        private void button5_Click(object sender, EventArgs e)
        {
            DataGridViewRow curRow = this.SourceMaterialListDGV.CurrentRow;
            if(curRow != null && (curRow.Index >= 0))
            {
                object obj = this.SourceMaterialListDGV.DataSource;
                if(obj != null && (obj is DataTable))
                {
                    //更新物料数据集合
                    string Material_ID = curRow.Cells["Material_ID"].Value.ToString();
                    this.DeleteMaterialByMaterialId(Material_ID);
                    //移除当前行
                    this.SourceMaterialListDGV.Rows.RemoveAt(curRow.Index);
                }
                else
                {
                    MessageUtil.ShowError("没有确定的行可以去除！");
                }
            }
            else
            {
                MessageUtil.ShowError("请选择确定的行！");
            }
        }

        //根据物料Id移除物料
        private void DeleteMaterialByMaterialId(string materialId)
        {
            //询价单原来的物料id集合
            if(this.sourceMaterialIdSet != null && this.sourceMaterialIdSet.Contains(materialId))
            {
                this.sourceMaterialIdSet.Remove(materialId);
                this.AddDeleteMaterialId(materialId);
                //同时移除待更新的物料数据对象
                if(this.updateSourceMaterialList != null && this.updateSourceMaterialList.Count > 0)
                {
                    foreach(SourceMaterial sm in this.updateSourceMaterialList)
                    {
                        if (sm.Material_ID.Equals(materialId))
                        {
                            this.updateSourceMaterialList.Remove(sm);
                        }
                    }
                }
            }

            //新增的物料ID集合
            if(this.newSourceMaterialIdSet != null && this.newSourceMaterialIdSet.Contains(materialId))
            {
                this.newSourceMaterialIdSet.Remove(materialId);
                //同时移除物料对象
                if(this.addSourceMaterialList != null && this.addSourceMaterialList.Count > 0)
                {
                    foreach(SourceMaterial sm in this.addSourceMaterialList)
                    {
                        if (sm.Material_ID.Equals(materialId))
                        {
                            this.addSourceMaterialList.Remove(sm);
                        }
                    }
                }
            }

        }

        //添加待删除物料ID
        private void AddDeleteMaterialId(string materialId)
        {
            if(this.deleteSourceMaterialIdSet == null)
            {
                this.deleteSourceMaterialIdSet = new HashSet<string>();
            }
            this.deleteSourceMaterialIdSet.Add(materialId);
        }

        //增加按钮（物料）
        private void button4_Click(object sender, EventArgs e)
        {
            MaterialInfo materialInfo = new MaterialInfo(this);
            materialInfo.Show();
        }

        //编辑按钮
        private void button8_Click(object sender, EventArgs e)
        {
            DataGridViewRow currentRow = this.SourceMaterialListDGV.CurrentRow;
            if(currentRow != null && currentRow.Index >= 0)
            {
                string Material_ID = currentRow.Cells["Material_ID"].Value.ToString();
                string Material_Name = currentRow.Cells["Material_Name"].Value.ToString();
                string Material_Type = currentRow.Cells["Material_Type"].Value.ToString();
                string Material_Group = currentRow.Cells["Material_Group"].Value.ToString();
                string Source_ID = this.txt_inquiryId.Text;
                MaterialNum materialNum = new MaterialNum(this, Material_Type, Material_Group, Material_ID, Source_ID);
                materialNum.Show();
            }
        }
    }


}
