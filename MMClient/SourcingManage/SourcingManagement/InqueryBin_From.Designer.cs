﻿namespace MMClient.SourcingManage.SourcingManagement
{
    partial class InqueryBin_From
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SummaryDemandTotalDGV = new System.Windows.Forms.DataGridView();
            this.selectTotal = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Demand_IDTotal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Purchase_TypeTotal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Proposer_IDTotal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.StateTotal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PhoneNumTotal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DepartmentTotal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ReviewAdviceTotal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Demand_CountTotal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Material_NameTotal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Supplier_IDTotal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Factory_IDTotal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.StateCB = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.createInqueryPriceBtn = new System.Windows.Forms.Button();
            this.createAuctionBtn = new System.Windows.Forms.Button();
            this.createBidBtn = new System.Windows.Forms.Button();
            this.delete = new System.Windows.Forms.Button();
            this.addBtn = new System.Windows.Forms.Button();
            this.freshBtn = new System.Windows.Forms.Button();
            this.SummaryDemandWorkDGV = new System.Windows.Forms.DataGridView();
            this.selectWork = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Demand_IDWork = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Purchase_TypeWork = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Proposer_IDWork = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.StateWork = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PhoneNumWork = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DepartmentWork = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ReviewAdviceWork = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Demand_CountWork = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Material_NameWork = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Supplier_IDWork = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Factory_IDWork = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.SummaryDemandTotalDGV)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SummaryDemandWorkDGV)).BeginInit();
            this.SuspendLayout();
            // 
            // SummaryDemandTotalDGV
            // 
            this.SummaryDemandTotalDGV.AllowUserToAddRows = false;
            this.SummaryDemandTotalDGV.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.SummaryDemandTotalDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.SummaryDemandTotalDGV.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.selectTotal,
            this.Demand_IDTotal,
            this.Purchase_TypeTotal,
            this.Proposer_IDTotal,
            this.StateTotal,
            this.PhoneNumTotal,
            this.DepartmentTotal,
            this.ReviewAdviceTotal,
            this.Demand_CountTotal,
            this.Material_NameTotal,
            this.Supplier_IDTotal,
            this.Factory_IDTotal});
            this.SummaryDemandTotalDGV.Location = new System.Drawing.Point(5, 46);
            this.SummaryDemandTotalDGV.Name = "SummaryDemandTotalDGV";
            this.SummaryDemandTotalDGV.RowTemplate.Height = 23;
            this.SummaryDemandTotalDGV.Size = new System.Drawing.Size(984, 179);
            this.SummaryDemandTotalDGV.TabIndex = 0;
            // 
            // selectTotal
            // 
            this.selectTotal.HeaderText = "选择";
            this.selectTotal.Name = "selectTotal";
            this.selectTotal.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.selectTotal.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // Demand_IDTotal
            // 
            this.Demand_IDTotal.DataPropertyName = "Demand_ID";
            this.Demand_IDTotal.HeaderText = "需求单号";
            this.Demand_IDTotal.Name = "Demand_IDTotal";
            // 
            // Purchase_TypeTotal
            // 
            this.Purchase_TypeTotal.DataPropertyName = "Purchase_Type";
            this.Purchase_TypeTotal.HeaderText = "采购类型";
            this.Purchase_TypeTotal.Name = "Purchase_TypeTotal";
            // 
            // Proposer_IDTotal
            // 
            this.Proposer_IDTotal.DataPropertyName = "Proposer_ID";
            this.Proposer_IDTotal.HeaderText = "申请人";
            this.Proposer_IDTotal.Name = "Proposer_IDTotal";
            // 
            // StateTotal
            // 
            this.StateTotal.DataPropertyName = "State";
            this.StateTotal.HeaderText = "状态";
            this.StateTotal.Name = "StateTotal";
            // 
            // PhoneNumTotal
            // 
            this.PhoneNumTotal.DataPropertyName = "PhoneNum";
            this.PhoneNumTotal.HeaderText = "电话号码";
            this.PhoneNumTotal.Name = "PhoneNumTotal";
            // 
            // DepartmentTotal
            // 
            this.DepartmentTotal.DataPropertyName = "Department";
            this.DepartmentTotal.HeaderText = "申请部门";
            this.DepartmentTotal.Name = "DepartmentTotal";
            // 
            // ReviewAdviceTotal
            // 
            this.ReviewAdviceTotal.DataPropertyName = "ReviewAdvice";
            this.ReviewAdviceTotal.HeaderText = "审核意见";
            this.ReviewAdviceTotal.Name = "ReviewAdviceTotal";
            // 
            // Demand_CountTotal
            // 
            this.Demand_CountTotal.DataPropertyName = "Demand_Count";
            this.Demand_CountTotal.HeaderText = "需求数量";
            this.Demand_CountTotal.Name = "Demand_CountTotal";
            // 
            // Material_NameTotal
            // 
            this.Material_NameTotal.DataPropertyName = "Material_Name";
            this.Material_NameTotal.HeaderText = "物料编码";
            this.Material_NameTotal.Name = "Material_NameTotal";
            // 
            // Supplier_IDTotal
            // 
            this.Supplier_IDTotal.DataPropertyName = "Supplier_ID";
            this.Supplier_IDTotal.HeaderText = "供应商";
            this.Supplier_IDTotal.Name = "Supplier_IDTotal";
            // 
            // Factory_IDTotal
            // 
            this.Factory_IDTotal.DataPropertyName = "Factory_ID";
            this.Factory_IDTotal.HeaderText = "工厂";
            this.Factory_IDTotal.Name = "Factory_IDTotal";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 12);
            this.label1.TabIndex = 1;
            this.label1.Text = "显示";
            // 
            // StateCB
            // 
            this.StateCB.FormattingEnabled = true;
            this.StateCB.Location = new System.Drawing.Point(36, 10);
            this.StateCB.Name = "StateCB";
            this.StateCB.Size = new System.Drawing.Size(154, 20);
            this.StateCB.TabIndex = 2;
            this.StateCB.SelectedIndexChanged += new System.EventHandler(this.StateCB_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.SystemColors.ControlDark;
            this.label2.Location = new System.Drawing.Point(5, 33);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(984, 12);
            this.label2.TabIndex = 3;
            this.label2.Text = "需求总览";
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.SystemColors.ControlDark;
            this.label3.Location = new System.Drawing.Point(5, 266);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(984, 12);
            this.label3.TabIndex = 4;
            this.label3.Text = "工作范围";
            // 
            // createInqueryPriceBtn
            // 
            this.createInqueryPriceBtn.Location = new System.Drawing.Point(5, 442);
            this.createInqueryPriceBtn.Name = "createInqueryPriceBtn";
            this.createInqueryPriceBtn.Size = new System.Drawing.Size(75, 23);
            this.createInqueryPriceBtn.TabIndex = 6;
            this.createInqueryPriceBtn.Text = "创建询价";
            this.createInqueryPriceBtn.UseVisualStyleBackColor = true;
            this.createInqueryPriceBtn.Click += new System.EventHandler(this.createInqueryPriceBtn_Click);
            // 
            // createAuctionBtn
            // 
            this.createAuctionBtn.Location = new System.Drawing.Point(86, 442);
            this.createAuctionBtn.Name = "createAuctionBtn";
            this.createAuctionBtn.Size = new System.Drawing.Size(75, 23);
            this.createAuctionBtn.TabIndex = 7;
            this.createAuctionBtn.Text = "创建拍卖";
            this.createAuctionBtn.UseVisualStyleBackColor = true;
            this.createAuctionBtn.Click += new System.EventHandler(this.createAuctionBtn_Click);
            // 
            // createBidBtn
            // 
            this.createBidBtn.Location = new System.Drawing.Point(167, 442);
            this.createBidBtn.Name = "createBidBtn";
            this.createBidBtn.Size = new System.Drawing.Size(75, 23);
            this.createBidBtn.TabIndex = 8;
            this.createBidBtn.Text = "创建招标";
            this.createBidBtn.UseVisualStyleBackColor = true;
            this.createBidBtn.Click += new System.EventHandler(this.createBidBtn_Click);
            // 
            // delete
            // 
            this.delete.Location = new System.Drawing.Point(248, 442);
            this.delete.Name = "delete";
            this.delete.Size = new System.Drawing.Size(111, 23);
            this.delete.TabIndex = 9;
            this.delete.Text = "从工作范围中删除";
            this.delete.UseVisualStyleBackColor = true;
            this.delete.Click += new System.EventHandler(this.delete_Click);
            // 
            // addBtn
            // 
            this.addBtn.Location = new System.Drawing.Point(5, 231);
            this.addBtn.Name = "addBtn";
            this.addBtn.Size = new System.Drawing.Size(97, 23);
            this.addBtn.TabIndex = 10;
            this.addBtn.Text = "添加到工作范围";
            this.addBtn.UseVisualStyleBackColor = true;
            this.addBtn.Click += new System.EventHandler(this.AddBtn_Click);
            // 
            // freshBtn
            // 
            this.freshBtn.Location = new System.Drawing.Point(196, 8);
            this.freshBtn.Name = "freshBtn";
            this.freshBtn.Size = new System.Drawing.Size(75, 23);
            this.freshBtn.TabIndex = 11;
            this.freshBtn.Text = "刷新";
            this.freshBtn.UseVisualStyleBackColor = true;
            this.freshBtn.Click += new System.EventHandler(this.FreshBtn_Click);
            // 
            // SummaryDemandWorkDGV
            // 
            this.SummaryDemandWorkDGV.AllowUserToAddRows = false;
            this.SummaryDemandWorkDGV.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.SummaryDemandWorkDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.SummaryDemandWorkDGV.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.selectWork,
            this.Demand_IDWork,
            this.Purchase_TypeWork,
            this.Proposer_IDWork,
            this.StateWork,
            this.PhoneNumWork,
            this.DepartmentWork,
            this.ReviewAdviceWork,
            this.Demand_CountWork,
            this.Material_NameWork,
            this.Supplier_IDWork,
            this.Factory_IDWork});
            this.SummaryDemandWorkDGV.Location = new System.Drawing.Point(5, 279);
            this.SummaryDemandWorkDGV.Name = "SummaryDemandWorkDGV";
            this.SummaryDemandWorkDGV.RowTemplate.Height = 23;
            this.SummaryDemandWorkDGV.Size = new System.Drawing.Size(984, 157);
            this.SummaryDemandWorkDGV.TabIndex = 12;
            // 
            // selectWork
            // 
            this.selectWork.HeaderText = "选择";
            this.selectWork.Name = "selectWork";
            this.selectWork.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.selectWork.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // Demand_IDWork
            // 
            this.Demand_IDWork.DataPropertyName = "Demand_ID";
            this.Demand_IDWork.HeaderText = "需求单号";
            this.Demand_IDWork.Name = "Demand_IDWork";
            // 
            // Purchase_TypeWork
            // 
            this.Purchase_TypeWork.DataPropertyName = "Purchase_Type";
            this.Purchase_TypeWork.HeaderText = "采购类型";
            this.Purchase_TypeWork.Name = "Purchase_TypeWork";
            // 
            // Proposer_IDWork
            // 
            this.Proposer_IDWork.DataPropertyName = "Proposer_ID";
            this.Proposer_IDWork.HeaderText = "申请人";
            this.Proposer_IDWork.Name = "Proposer_IDWork";
            // 
            // StateWork
            // 
            this.StateWork.DataPropertyName = "State";
            this.StateWork.HeaderText = "状态";
            this.StateWork.Name = "StateWork";
            // 
            // PhoneNumWork
            // 
            this.PhoneNumWork.DataPropertyName = "PhoneNum";
            this.PhoneNumWork.HeaderText = "电话号码";
            this.PhoneNumWork.Name = "PhoneNumWork";
            // 
            // DepartmentWork
            // 
            this.DepartmentWork.DataPropertyName = "Department";
            this.DepartmentWork.HeaderText = "申请部门";
            this.DepartmentWork.Name = "DepartmentWork";
            // 
            // ReviewAdviceWork
            // 
            this.ReviewAdviceWork.DataPropertyName = "ReviewAdvice";
            this.ReviewAdviceWork.HeaderText = "审核意见";
            this.ReviewAdviceWork.Name = "ReviewAdviceWork";
            // 
            // Demand_CountWork
            // 
            this.Demand_CountWork.DataPropertyName = "Demand_Count";
            this.Demand_CountWork.HeaderText = "需求数量";
            this.Demand_CountWork.Name = "Demand_CountWork";
            // 
            // Material_NameWork
            // 
            this.Material_NameWork.DataPropertyName = "Material_Name";
            this.Material_NameWork.HeaderText = "物料编码";
            this.Material_NameWork.Name = "Material_NameWork";
            // 
            // Supplier_IDWork
            // 
            this.Supplier_IDWork.DataPropertyName = "Supplier_ID";
            this.Supplier_IDWork.HeaderText = "供应商";
            this.Supplier_IDWork.Name = "Supplier_IDWork";
            // 
            // Factory_IDWork
            // 
            this.Factory_IDWork.DataPropertyName = "Factory_ID";
            this.Factory_IDWork.HeaderText = "工厂";
            this.Factory_IDWork.Name = "Factory_IDWork";
            // 
            // InqueryBin_From
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(993, 546);
            this.Controls.Add(this.SummaryDemandWorkDGV);
            this.Controls.Add(this.freshBtn);
            this.Controls.Add(this.addBtn);
            this.Controls.Add(this.delete);
            this.Controls.Add(this.createBidBtn);
            this.Controls.Add(this.createAuctionBtn);
            this.Controls.Add(this.createInqueryPriceBtn);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.StateCB);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.SummaryDemandTotalDGV);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "InqueryBin_From";
            this.Text = "寻源管理仓";
            ((System.ComponentModel.ISupportInitialize)(this.SummaryDemandTotalDGV)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SummaryDemandWorkDGV)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView SummaryDemandTotalDGV;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox StateCB;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button createInqueryPriceBtn;
        private System.Windows.Forms.Button createAuctionBtn;
        private System.Windows.Forms.Button createBidBtn;
        private System.Windows.Forms.Button delete;
        private System.Windows.Forms.Button addBtn;
        private System.Windows.Forms.Button freshBtn;
        private System.Windows.Forms.DataGridView SummaryDemandWorkDGV;
        private System.Windows.Forms.DataGridViewCheckBoxColumn selectTotal;
        private System.Windows.Forms.DataGridViewTextBoxColumn Demand_IDTotal;
        private System.Windows.Forms.DataGridViewTextBoxColumn Purchase_TypeTotal;
        private System.Windows.Forms.DataGridViewTextBoxColumn Proposer_IDTotal;
        private System.Windows.Forms.DataGridViewTextBoxColumn StateTotal;
        private System.Windows.Forms.DataGridViewTextBoxColumn PhoneNumTotal;
        private System.Windows.Forms.DataGridViewTextBoxColumn DepartmentTotal;
        private System.Windows.Forms.DataGridViewTextBoxColumn ReviewAdviceTotal;
        private System.Windows.Forms.DataGridViewTextBoxColumn Demand_CountTotal;
        private System.Windows.Forms.DataGridViewTextBoxColumn Material_NameTotal;
        private System.Windows.Forms.DataGridViewTextBoxColumn Supplier_IDTotal;
        private System.Windows.Forms.DataGridViewTextBoxColumn Factory_IDTotal;
        private System.Windows.Forms.DataGridViewCheckBoxColumn selectWork;
        private System.Windows.Forms.DataGridViewTextBoxColumn Demand_IDWork;
        private System.Windows.Forms.DataGridViewTextBoxColumn Purchase_TypeWork;
        private System.Windows.Forms.DataGridViewTextBoxColumn Proposer_IDWork;
        private System.Windows.Forms.DataGridViewTextBoxColumn StateWork;
        private System.Windows.Forms.DataGridViewTextBoxColumn PhoneNumWork;
        private System.Windows.Forms.DataGridViewTextBoxColumn DepartmentWork;
        private System.Windows.Forms.DataGridViewTextBoxColumn ReviewAdviceWork;
        private System.Windows.Forms.DataGridViewTextBoxColumn Demand_CountWork;
        private System.Windows.Forms.DataGridViewTextBoxColumn Material_NameWork;
        private System.Windows.Forms.DataGridViewTextBoxColumn Supplier_IDWork;
        private System.Windows.Forms.DataGridViewTextBoxColumn Factory_IDWork;
    }
}