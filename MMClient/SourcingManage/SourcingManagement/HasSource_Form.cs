﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Model.SourcingManage.ProcurementPlan;
using Lib.Bll.SourcingManage.SourcingManagement;
using Lib.Model.SourcingManage.SourcingManagement;
using Lib.Bll.MDBll.SP;
using Lib.Model.MD.SP;

namespace MMClient.SourcingManage.SourcingManagement
{
    public partial class HasSource_Form : Form
    {
        private List<Demand_Material> hasSourceDM;

        ReadQuotaBLL readQutoBll = new ReadQuotaBLL();
        SourceListBLL sourceListBll = new SourceListBLL();
        SupplierBaseBLL supplierBaseBll = new SupplierBaseBLL();
        PRSupplierBLL prSupplierBll = new PRSupplierBLL();

        ConvenientTools tools = new ConvenientTools();
        Random random = new Random();
        

        public HasSource_Form()
        {
            InitializeComponent();
        }

        public HasSource_Form(List<Demand_Material> hasSourceDM)
        {
            InitializeComponent();
            this.hasSourceDM = hasSourceDM;
            init();
        }

        private void init()
        {
            initMaterialGrid(hasSourceDM);
        }

        private void initMaterialGrid(List<Demand_Material> hasSourceDM)
        {
            materialGridView.Rows.Clear();
            if (hasSourceDM.Count > materialGridView.Rows.Count)
            {
                materialGridView.Rows.Add(hasSourceDM.Count-materialGridView.Rows.Count);
            }
            for (int i = 0; i < hasSourceDM.Count; i++)
            {
                Demand_Material dm = hasSourceDM.ElementAt(i);
                materialGridView.Rows[i].Cells["materialId"].Value = dm.Material_ID;
                materialGridView.Rows[i].Cells["materialName"].Value = dm.Material_Name;
                materialGridView.Rows[i].Cells["demandCount"].Value = dm.Demand_Count;
                materialGridView.Rows[i].Cells["unit"].Value = dm.Measurement;
                materialGridView.Rows[i].Cells["factoryId"].Value = dm.Factory_ID;
                materialGridView.Rows[i].Cells["stockId"].Value = dm.Stock_ID;
                materialGridView.Rows[i].Cells["DeliveryStartTime"].Value = dm.DeliveryStartTime.ToString("yyyy-MM-dd");
                materialGridView.Rows[i].Cells["DeliveryEndTime"].Value = dm.DeliveryEndTime.ToString("yyyy-MM-dd");

            }
        }

        /// <summary>
        /// 物料选择变化
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void materialGridView_SelectionChanged(object sender, EventArgs e)
        {
            if (materialGridView.CurrentRow == null)
            {
                return;
            }
            int index = materialGridView.CurrentRow.Index;
            Object obj = materialGridView.Rows[index].Cells[0].Value;
            if (obj == null)
            {
                return;
            }
            String materialId = materialGridView.Rows[index].Cells[0].Value.ToString();//物料编号
            string factoryId = materialGridView.Rows[index].Cells["factoryId"].Value.ToString();//工厂编号
            int demandCount = int.Parse(materialGridView.Rows[index].Cells["demandCount"].Value.ToString());
            supplierGridView.Rows.Clear();
            List<Quota_Arrangement_Item> list = readQutoBll.getInfoByMaterialId(materialId);//读取配额协议的数据
            if (list.Count> 0)//配额协议中有数据
            {
                
                if (list.Count > supplierGridView.Rows.Count)
                {
                    supplierGridView.Rows.Add(list.Count - supplierGridView.Rows.Count);
                }
                for (int i = 0; i < list.Count; i++)
                {
                    supplierGridView.Rows[i].Cells["supplierId"].Value = list.ElementAt(i).Supplier_ID;
                    supplierGridView.Rows[i].Cells["percentage"].Value = list.ElementAt(i).Quota_Percentage;
                    SupplierBase supplierBase = supplierBaseBll.GetSupplierBasicInformation(list.ElementAt(i).Supplier_ID);
                    if (supplierBase != null)
                    {
                        supplierGridView.Rows[i].Cells["supplierName"].Value = supplierBase.Supplier_Name;
                        supplierGridView.Rows[i].Cells["count"].Value = Math.Round(list.ElementAt(i).Quota_Percentage * demandCount / 100);
                        supplierGridView.Rows[i].Cells["address"].Value = supplierBase.Address;
                        supplierGridView.Rows[i].Cells["email"].Value = supplierBase.Email;
                    }
                }
            }
            else//配额协议中没有数据
            {
                string startTime = materialGridView.Rows[index].Cells["DeliveryStartTime"].Value.ToString();
                string endTime = materialGridView.Rows[index].Cells["DeliveryEndTime"].Value.ToString();
                //功能未完成，用null初始化List的值
                //List<SourceList> sourceLists = sourceListBll.getSourceList(materialId, factoryId,startTime,endTime);
                List<SourceList> sourceLists = new List<SourceList>();
                if (sourceLists.Count > supplierGridView.Rows.Count)
                {
                    supplierGridView.Rows.Add(sourceLists.Count - supplierGridView.Rows.Count);
                }
                for(int i=0;i< sourceLists.Count; i++)
                {
                    supplierGridView.Rows[i].Cells["supplierId"].Value = sourceLists.ElementAt(i).Supplier_ID;
                    supplierGridView.Rows[i].Cells["percentage"].Value = 100;
                    SupplierBase supplierBase = supplierBaseBll.GetSupplierBasicInformation(sourceLists.ElementAt(i).Supplier_ID);
                    if (supplierBase != null)
                    {
                        supplierGridView.Rows[i].Cells["supplierName"].Value = supplierBase.Supplier_Name;
                        supplierGridView.Rows[i].Cells["count"].Value = int.Parse(materialGridView.Rows[index].Cells["demandCount"].Value.ToString());
                        supplierGridView.Rows[i].Cells["address"].Value = supplierBase.Address;
                        supplierGridView.Rows[i].Cells["email"].Value = supplierBase.Email;
                    }
                }
            }
            
        }

        /// <summary>
        /// 确定按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_Order_Click(object sender, EventArgs e)
        {
            int index = materialGridView.CurrentRow.Index;
            Object obj = materialGridView.Rows[index].Cells[0].Value;
            if (obj == null)
            {
                return;
            }
            String materialId = materialGridView.Rows[index].Cells[0].Value.ToString();
            foreach (DataGridViewRow dgvr in supplierGridView.Rows)
            {
                PRSupplier prs = new PRSupplier();
                prs.PR_ID = tools.systemTimeToStr();
                prs.Material_ID = materialId;
                prs.Supplier_ID = dgvr.Cells["supplierId"].Value.ToString();
                prs.Number = int.Parse(dgvr.Cells["count"].Value.ToString());
                prs.Finished_Mark = Convert.ToBoolean(0);
                int res = prSupplierBll.addPRSupplier(prs);
                    

            }
            MessageBox.Show(materialId + "采购申请创建成功");
            for (int i = materialGridView.SelectedRows.Count; i >= 0; i--)
            {
                if (materialGridView.SelectedRows.Count <= 0)
                {
                    break;
                }
                materialGridView.Rows.RemoveAt(materialGridView.SelectedRows[i - 1].Index);
            }
        }
    }
}
