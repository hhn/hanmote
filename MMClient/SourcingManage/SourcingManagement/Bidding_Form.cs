﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Mail;
using Aspose.Cells;
using System.Windows.Forms;
using Lib.SqlServerDAL.SourcingManage.ProcurementPlan;
using Lib.Model.SourcingManage.ProcurementPlan;
using Lib.Model.SourcingManage;
using Lib.Bll.SourcingManage.SourcingManagement;
using Lib.Bll.SourcingManage.SourceingRecord;
using WeifenLuo.WinFormsUI.Docking;
using System.IO;
using Lib.Model.SourcingManage.SourcingManagement;
using Lib.Model.SourcingManage.SourceingRecord;
using Lib.Common.CommonUtils;
using Lib.Model.FileManage;
using MMClient.CommonForms;
using Lib.Bll.FileManageBLL;

namespace MMClient.SourcingManage.SourcingManagement
{
    public partial class Bidding_Form : DockContent
    {
        private SourceBid bid;
        DataTable costDT = new DataTable();
        //查询工具

        SourceBLL sourceBll = new SourceBLL();
        SourceBidBLL bidBLL = new SourceBidBLL();
        BidPartnerBLL bidPartnerBLL = new BidPartnerBLL();
        SourceSupplierBLL sourceSupplierBll = new SourceSupplierBLL();
        BidCertificateBLL bidCertificateBLL = new BidCertificateBLL();
        SourceMaterialBLL sourceMaterialBll = new SourceMaterialBLL();
        BidWeightScoreBLL bidWeightScoreBLL = new BidWeightScoreBLL();
        PurchaseOrgBLL purchaseOrgBLL = new PurchaseOrgBLL();
        PurchaseGroupBLL purchaseGroupBLL = new PurchaseGroupBLL();
        MaterialGroupBLL materialGroupBLL = new MaterialGroupBLL();

        LowestCostBLL lowestCostBll = new LowestCostBLL();

        private FileBLL fileTool = new FileBLL();               //文件操作工具
        private List<File_Attachment_Info> fileAttachmentList =
            new List<File_Attachment_Info>();       //文件附件列表(和dgvAttachmentInfo保存同步)
        private string sourceName = "寻源管理";
        private string dirName = "招标";

        //控件工具
        ConvenientTools tools = new ConvenientTools();

        /**
         * 新建招标窗体
         * */
        public Bidding_Form()
        {
            InitializeComponent();
            fileGridView.TopLeftHeaderCell.Value = "序号";
        }

        /** 
         * 新建招标窗体
         * */
        public Bidding_Form(List<Demand_Material> demandMaterials, Summary_Demand summaryDemand)
        {
            InitializeComponent();
            init(demandMaterials);
            fileGridView.TopLeftHeaderCell.Value = "序号";
        }

        private void init(List<Demand_Material> demandMaterials)
        {
            initBidInfo();
            initCatalogue();
            initPurchase();
           // initBidSuppliers();
            initBidMaterials(demandMaterials);
        }

        /**
         * 初始化招标编号
         * */
        private void initBidInfo()
        {
            txt_BidId.Text ="B"+ tools.systemTimeToStr(); 
        }

        private void initCatalogue()
        {
            List<MaterialGroup> list = materialGroupBLL.getAllMaterialGroup();
            for (int i = 0; i < list.Count; i++)
            {
                cmb_catalogue.Items.Add(list.ElementAt(i).MaterialGroupName);
            }
        }

        private void initPurchase()
        {
            List<PurchaseOrg> list = purchaseOrgBLL.getAllPurchaseOrg();
            for (int i = 0; i < list.Count; i++)
            {
                cmb_purchaseOrg.Items.Add(list.ElementAt(i).PurchaseOrgName);
            }
            List<PurchaseGroup> listGroup = purchaseGroupBLL.getAllPurchaseGroup();
            for (int i = 0; i < listGroup.Count; i++)
            {
                cmb_purchaseGroup.Items.Add(listGroup.ElementAt(i).PurchaseGroupName);
            }
        }

        //初始化供应商信息
        private void initBidSuppliers()
        {
            SupplierGridView.Rows.Clear();
            List<SourceSupplier> list = sourceSupplierBll.getBidSupplier();
            if (list.Count == 0)
            {
                return;
            }
            if (list.Count > SupplierGridView.Rows.Count)
            {
                SupplierGridView.Rows.Add(list.Count - SupplierGridView.Rows.Count);
            }
            for (int i = 0; i < list.Count; i++)
            {
                SupplierGridView.Rows[i].Cells["bidSupplierId"].Value = list.ElementAt(i).Supplier_ID;
                SupplierGridView.Rows[i].Cells["bidSupplierName"].Value = list.ElementAt(i).Supplier_Name;
                SupplierGridView.Rows[i].Cells["bidContact"].Value = list.ElementAt(i).Contact;
                SupplierGridView.Rows[i].Cells["bidEmail"].Value = list.ElementAt(i).Email;
                SupplierGridView.Rows[i].Cells["bidAddress"].Value = list.ElementAt(i).Address;
                SupplierGridView.Rows[i].Cells["bidState"].Value = getStringByNumber(list.ElementAt(i).State);
            }
        }

        private void initBidMaterials(List<Demand_Material> demandMaterials)
        {
            MaterialGridview.Rows.Clear();
            if (demandMaterials.Count > MaterialGridview.Rows.Count)
            {
                MaterialGridview.Rows.Add(demandMaterials.Count - MaterialGridview.Rows.Count);
            }
            for (int i = 0; i < demandMaterials.Count; i++)
            {
                MaterialGridview.Rows[i].Cells["bidMaterialId"].Value = demandMaterials.ElementAt(i).Material_ID;
                MaterialGridview.Rows[i].Cells["bidMaterialName"].Value = demandMaterials.ElementAt(i).Material_Name;
                MaterialGridview.Rows[i].Cells["bidDemandCount"].Value = demandMaterials.ElementAt(i).Demand_Count;
                MaterialGridview.Rows[i].Cells["bidDemandMeasurement"].Value = demandMaterials.ElementAt(i).Measurement;
                MaterialGridview.Rows[i].Cells["factoryId"].Value = demandMaterials.ElementAt(i).Factory_ID;
                MaterialGridview.Rows[i].Cells["stockId"].Value = demandMaterials.ElementAt(i).Stock_ID;
                MaterialGridview.Rows[i].Cells["DeliveryStartTime"].Value = demandMaterials.ElementAt(i).DeliveryStartTime.ToString("yyyy-MM-dd");
                MaterialGridview.Rows[i].Cells["DeliveryEndTime"].Value = demandMaterials.ElementAt(i).DeliveryEndTime.ToString("yyyy-MM-dd");
                cmb_Materials.Items.Add(demandMaterials.ElementAt(i).Material_ID);
            }
        }
        private string getStringByNumber(int num)
        {
            string str = null;
            if (num == 0)
            {
                str = "未投标";
            }
            if (num == 1)
            {
                str = "已投标";
            }
            if (num == 2)
            {
                str = "已接受";
            }
            if (num == 3)
            {
                str = "已拒绝";
            }
            return str;
        }

        private int getNumberByString(string str)
        {
            int res = -1;
            if (str.Equals("未投标"))
            {
                res = 0;
            }
            if (str.Equals("已投标"))
            {
                res = 1;
            }
            if (str.Equals("已接受"))
            {
                res = 2;
            }
            if (str.Equals("已拒绝"))
            {
                res = 3;
            }
            return res;
        }


        /**
         * 新建招标信息
         * */
        private void base_save_Click(object sender, EventArgs e)
        {
            //插入寻源基本信息
            Source source = new Source();
            source.Source_ID = txt_BidId.Text;
            source.Source_Name = txt_BidName.Text;
            source.Source_Type = "招标";
            source.PurchaseOrg = cmb_purchaseOrg.Text;
            source.PurchaseGroup = cmb_purchaseGroup.Text;
            sourceBll.addSource(source);

            //以下为招标详细数据
            bid = new SourceBid();
            bid.BidId = txt_BidId.Text;
            bid.ServiceType = txt_serviceType.Text;
            bid.Catalogue = cmb_catalogue.Text;
            bid.DisplayType = cmb_displayType.Text;
            bid.TransType = cmb_TransType.Text;
            bid.StartTime = DateTime.Parse(dtp_startTime.Text);
            bid.EndTime = DateTime.Parse(dtp_endTime.Text);
            bid.StartBeginTime = DateTime.Parse(dtp_startBeginTime.Text);
            bid.LimitTime = DateTime.Parse(dtp_limitTime.Text);
            bid.TimeZone = cmb_timeZone.SelectedItem.ToString() ;
            bid.Currency = cmb_currency.SelectedItem.ToString();
            bid.CommBidStartDate = DateTime.Parse(dtp_commBidStartDate.Text);
            bid.CommBidStartTime = DateTime.Parse(txt_commBidStartTime.Text);
            bid.TechBidStartDate = DateTime.Parse(dtp_techBidStartDate.Text);
            bid.TechBidStartTime = DateTime.Parse(txt_techBidStartTime.Text);
            bid.BuyEndDate = DateTime.Parse(dtp_buyEndDate.Text);
            bid.BuyEndTime = DateTime.Parse(txt_buyEndTime.Text);
            bid.EnrollEndDate = DateTime.Parse(dtp_enrollEndDate.Text);
            bid.EnrollEndTime = DateTime.Parse(txt_enrollEndTime.Text);
            bid.EnrollStartDate = DateTime.Parse(dtp_enrollStartDate.Text);
            bid.EnrollStartTime = DateTime.Parse(txt_enrollEndTime.Text);
            bid.BidCost = Double.Parse(txt_bidCost.Text);
            bid.BidState = 1;//待发布
            bid.EvalMethId = tac_EvalMethod.SelectedIndex;//评估方法
            if (chb_Weight.Checked && tac_EvalMethod.SelectedIndex == 1)
            {
                bid.IsShow = 1;
            }
            else
            {
                bid.IsShow = 0;
            }
            int bidres = bidBLL.addBid(bid);//添加招标
            //以下为招标合作人数据
            BidPartner bp = new BidPartner();
            bp.BidId = txt_BidId.Text;
            bp.RequesterId = txt_requesterId.Text;
            bp.RequesterName = txt_requesterName.Text;
            bp.ReceiverId = txt_receiverId.Text;
            bp.ReceiverName = txt_receiverName.Text;
            bp.DropPointId = txt_dropPointId.Text;
            bp.DropPointName = txt_dropPointName.Text;
            //bp.ReviewerId = txt_reviewerId.Text;
            //bp.ReviewerName = txt_ReviewerName.Text;
            bp.CallerId = txt_callerId.Text;
            bp.CallerName = txt_callerName.Text;
            bidPartnerBLL.addBidPartner(bp);
            //以下为招标投标人数据
            foreach (DataGridViewRow dgvr in SupplierGridView.Rows)
            {
                SourceSupplier ss = new SourceSupplier();
                ss.Source_ID = txt_BidId.Text;
                ss.Supplier_ID = dgvr.Cells["bidSupplierId"].Value.ToString();
                ss.Supplier_Name = dgvr.Cells["bidSupplierName"].Value.ToString();
                //ss.ClassificationResult = dgvr.Cells["bidClassificationResult"].Value.ToString();
                ss.Contact = dgvr.Cells["bidContact"].Value.ToString();
                ss.Email = dgvr.Cells["bidEmail"].Value.ToString();
                ss.Address = dgvr.Cells["bidAddress"].Value.ToString();
                ss.State = getNumberByString(dgvr.Cells["bidState"].Value.ToString());
                if(bid.TransType.Equals("等级")){
                    ss.IsTrans = 0; //如果是等级方式，是否谈判状态标记为0
                }else if(bid.TransType.Equals("谈判")){
                    ss.IsTrans = 1;//如果是谈判方式，1表示未谈判；2表示谈判中；3表示已接受；4表示已拒绝。
                }
                int bsres = sourceSupplierBll.addSourceSupplier(ss);
            }
            //以下为招标凭证
            BidCertificate bc = new BidCertificate();
            bc.BidId = txt_BidId.Text;
            bc.ShortBidText = txt_shortBidText.Text;
            bc.ApprovalComment = txt_approvalComment.Text;
            bc.LongBidText = txt_longBidText.Text;
            bidCertificateBLL.addBidCertificate(bc);

            //如果评估方法是加权评分，执行以下代码
            if (tac_EvalMethod.SelectedIndex == 1)
            {
                foreach (DataGridViewRow dgvr in WeightScoreGridView.Rows)
                {
                    BidWeightScore bws = new BidWeightScore();
                    bws.BidId = txt_BidId.Text;
                    bws.WeightScoreName = dgvr.Cells["weightEvalName"].Value.ToString();
                    bws.WeightScoreNum = Convert.ToInt32(dgvr.Cells["weightEvalNum"].Value.ToString());
                    bidWeightScoreBLL.addBidWeightScore(bws);
                }
            }

            //以下为招标物料数据
            foreach (DataGridViewRow dgvr in MaterialGridview.Rows)
            {
                SourceMaterial sm = new SourceMaterial();
                sm.Source_ID = txt_BidId.Text;
                sm.Material_ID = dgvr.Cells["bidMaterialId"].Value.ToString();
                sm.Material_Name = dgvr.Cells["bidMaterialName"].Value.ToString();
                sm.Demand_Count = Convert.ToInt32(dgvr.Cells["bidDemandCount"].Value.ToString());
                sm.Unit = dgvr.Cells["bidDemandMeasurement"].Value.ToString();
                sm.FactoryId = dgvr.Cells["factoryId"].Value.ToString();
                sm.StockId = dgvr.Cells["stockId"].Value.ToString();
                sm.DeliveryStartTime = DateTime.Parse(dgvr.Cells["DeliveryStartTime"].Value.ToString());
                sm.DeliveryEndTime = DateTime.Parse(dgvr.Cells["DeliveryEndTime"].Value.ToString());
                int bmres = sourceMaterialBll.addSourceMaterials(sm);
            }
            MessageBox.Show("招标创建成功","提示");
            this.Close();
        }

        
        
        //添加投标人
        private void btn_addSupplier_Click(object sender, EventArgs e)
        {
            AddTenderer_Form addTendererForm = null;
            if (addTendererForm == null || addTendererForm.IsDisposed)
            {
                addTendererForm = new AddTenderer_Form(this);
            }
            addTendererForm.Show();
        }

        //检查
        private void btnCheck_Click(object sender, EventArgs e)
        {
            if (txt_BidName.Text.Trim()!="")
            {
                MessageBox.Show("检查合格");
                base_save.Visible = true;
            }
            else
            {
                MessageBox.Show("招标名称不能为空！");
            }
           
        }


        //选择文件
        private void btn_ChooseFile_Click(object sender, EventArgs e)
        {
            OpenFileDialog fileDialog = new OpenFileDialog();
            fileDialog.Title = "请选择文件";
            var result = fileDialog.ShowDialog();
            if (result == DialogResult.Cancel)
                return;
            string fileName = fileDialog.FileName;
            if (fileName.Equals(String.Empty))
                return;
            //防止文件名重复
            string safeFileName = fileDialog.SafeFileName;
            if (isHave(safeFileName))
            {
                MessageUtil.ShowError("文件已重复");
                return;
            }
            if (safeFileName.Length > 150)
            {
                MessageUtil.ShowError("文件名过长!");
                return;
            }
            //加入到fileAttachmentList中(这里不记录文件信息，暂时记录附件信息)
            var size = new FileInfo(fileName).Length;
            File_Attachment_Info fileAttachment = new File_Attachment_Info();
            fileAttachment.Attachment_Name = safeFileName;
            //服务器端为"", 表示未上传到服务器
            fileAttachment.Attachment_Location = "";
            fileAttachment.Attachment_Local_Location = fileName;
            fileAttachment.Attachment_Download_Times = 0;
            fileAttachment.Attachment_Size = size;
            fileAttachmentList.Add(fileAttachment);
            fileGridView.Rows.Add();
            int rowCount = this.fileGridView.Rows.Count;
            DataGridViewRow row = this.fileGridView.Rows[rowCount-1];
            row.Cells[0].Value = safeFileName;
            row.Cells[1].Value = StringUtil.convertFileSizeToString((double)size);
        }

        private bool isHave(String str)
        {
            bool result = false;
            for (int i = 0; i < fileAttachmentList.Count; i++)
            {
                File_Attachment_Info fa = fileAttachmentList.ElementAt(i);
                if (fa.Attachment_Name.Equals(str))
                {
                    result = true;
                    break;
                }
            }
            return result;
        }

        //上传文件
        private void btn_upload_Click(object sender, EventArgs e)
        {
            if (this.fileGridView.Rows.Count == 0)
            {
                MessageUtil.ShowError("请上传附件！");
                return;
            }
            FTPTool ftp = FTPTool.getInstance();
            //测试FTP服务器是否可连
            bool isConnect = ftp.serverIsConnect();
            if (!isConnect)
            {
                MessageUtil.ShowError("无法连接服务器!");
                return;
            }

            Dictionary<string, string> uploadRLDic =
                new Dictionary<string, string>();        //待上传文件词典
            double allFilesSize = 0.0;                   //待上传文件大小总和
            foreach (File_Attachment_Info fileAttachment in fileAttachmentList)
            {
                //未上传
                if (fileAttachment.Attachment_Location.Equals(""))
                {
                    fileAttachment.Attachment_Location =
                        sourceName + "/" + dirName + "/"+txt_BidName.Text+"/"
                        + fileAttachment.Attachment_Name;//服务器上传目录
                    fileAttachment.Upload_Time = System.DateTime.Now;
                    uploadRLDic.Add(fileAttachment.Attachment_Location, fileAttachment.Attachment_Local_Location);
                    allFilesSize += fileAttachment.Attachment_Size;
                }
            }
                //上传
                LinkedList<string> uploadFailList = new LinkedList<string>();
                ProgressForm progress = new ProgressForm(uploadFailList);
                progress.RLFileDic = uploadRLDic;
                progress.AllFilesSize = allFilesSize;
                progress.OpType = "upload";
                progress.ShowDialog();
                if (uploadFailList.Count == 0)
                    MessageBox.Show("保存成功！");
                else
                {
                    MessageUtil.ShowError(uploadFailList.Count + "个文件上传失败");
                }
        }

        private void fileGridView_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            var dgv = sender as DataGridView;
            if (dgv != null)
            {
                Rectangle rect = new Rectangle(e.RowBounds.Location.X, e.RowBounds.Location.Y, (dgv.RowHeadersWidth + 12) / 2, e.RowBounds.Height);
                TextRenderer.DrawText(e.Graphics, (e.RowIndex + 1).ToString(), dgv.RowHeadersDefaultCellStyle.Font, rect, dgv.RowHeadersDefaultCellStyle.ForeColor, TextFormatFlags.VerticalCenter | TextFormatFlags.Right);
            }
        }

        //删除投标人
        private void button3_Click(object sender, EventArgs e)
        {
            for (int i = this.SupplierGridView.SelectedRows.Count; i > 0; i--)
            {
                SupplierGridView.Rows.RemoveAt(SupplierGridView.SelectedRows[i - 1].Index);
            }
        }

        //评估方法更改
        private void cmb_EvalMethod_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmb_EvalMethod.SelectedIndex == 0)
            {
                tab_WeightScore.Hide();
                tab_LowTotalCost.Hide();
                tab_ValueAssess.Hide();
            }
            else if (cmb_EvalMethod.SelectedIndex == 1)//加权评分法
            {
                tab_LowPrice.Hide();
                tab_LowTotalCost.Hide();
                tab_ValueAssess.Hide();
                
            }
            else if (cmb_EvalMethod.SelectedIndex == 2)
            {
                tab_LowPrice.Hide();
                tab_WeightScore.Hide();
                tab_ValueAssess.Hide();
            }
            else if (cmb_EvalMethod.SelectedIndex == 3)
            {
                tab_LowPrice.Hide();
                tab_WeightScore.Hide();
                tab_LowTotalCost.Hide();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        //删除选中的加权评分项
        private void btn_DeleteWeight_Click(object sender, EventArgs e)
        {
            for (int i = this.WeightScoreGridView.SelectedRows.Count; i > 0; i--)
            {
                WeightScoreGridView.Rows.RemoveAt(WeightScoreGridView.SelectedRows[i - 1].Index);
            }
        }

        //添加加权评分项
        private void btn_AddWeight_Click(object sender, EventArgs e)
        {
            WeightScoreGridView.Rows.Add();
        }
        
        /// <summary>
        /// 添加投标人
        /// </summary>
        /// <param name="supbList"></param>
        public void fillMaterialBases(List<SourceSupplier> supbList)
        {
            for (int i = 0; i < supbList.Count; i++)
            {
                SupplierGridView.Rows.Add();
                int rowCount = SupplierGridView.Rows.Count;
                DataGridViewRow row = SupplierGridView.Rows[rowCount - 1];
                row.Cells["bidSupplierId"].Value = supbList.ElementAt(i).Supplier_ID;
                row.Cells["bidSupplierName"].Value = supbList.ElementAt(i).Supplier_Name;
                //curRow.Cells["bidClassificationResult"].Value = "-";
                row.Cells["bidContact"].Value = supbList.ElementAt(i).Contact;
                row.Cells["bidEmail"].Value = supbList.ElementAt(i).Email;
                row.Cells["bidAddress"].Value = supbList.ElementAt(i).Address;
                row.Cells["bidState"].Value = getStringByNumber(0); 
            }
        }


        /// <summary>
        /// 最低所有权总成本法添加成本项
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_AddCost_Click(object sender, EventArgs e)
        {
            allCostGridView.Rows.Add();
            String item = cmb_Costs.Text;
            int rowCount = this.allCostGridView.Rows.Count;
            DataGridViewRow row = this.allCostGridView.Rows[rowCount - 1];
            row.Cells[0].Value = item;
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            foreach (DataGridViewRow dgvr in allCostGridView.Rows)
            {
                LowestCost lc = new LowestCost();
                lc.SourceId = txt_BidId.Text;
                lc.CostItem = dgvr.Cells["CostItem"].Value.ToString();
                lc.First = getRes(dgvr.Cells["First"].Value);
                lc.Second = getRes(dgvr.Cells["Second"].Value);
                lc.Third = getRes(dgvr.Cells["Third"].Value);
                lc.Forth = getRes(dgvr.Cells["forth"].Value);
                lc.Fifth = getRes(dgvr.Cells["fifth"].Value);
                lc.Sixth = getRes(dgvr.Cells["sixth"].Value);
                lc.Seventh = getRes(dgvr.Cells["seventh"].Value);
                lc.Eighth = getRes(dgvr.Cells["eighth"].Value);
                lc.Ninth = getRes(dgvr.Cells["ninth"].Value);
                lc.Tenth = getRes(dgvr.Cells["tenth"].Value);
                lowestCostBll.addLowestCost(lc);
            }
        }

        private float getRes(Object obj)
        {
            float result = 0;
            if (obj == null)
            {
                result = 0;
            }
            else
            {
                result = float.Parse(obj.ToString());
            }
            return result;
        }

        /// <summary>
        /// 查看物料的历史招标信息
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button4_Click(object sender, EventArgs e)
        {
            string materialId = cmb_Materials.Text;
            List<SourceMaterial> list = sourceMaterialBll.findSourcesByMaterialId(materialId);
            materialSourceView.Rows.Clear();
            if (list.Count > materialSourceView.Rows.Count)
            {
                materialSourceView.Rows.Add(list.Count - materialSourceView.Rows.Count);
            }
            for (int i = 0; i < list.Count; i++)
            {
                materialSourceView.Rows[i].Cells["bidId"].Value = list.ElementAt(i).Source_ID;
                Source source = sourceBll.findSourceById(list.ElementAt(i).Source_ID);
                materialSourceView.Rows[i].Cells["bidName"].Value = source.Source_Name;
                materialSourceView.Rows[i].Cells["createTime"].Value = source.Create_Time.ToString("yyyy-MM-dd hh:mm:ss");
                materialSourceView.Rows[i].Cells["State"].Value = source.Source_State;

            }
        }

       
    }
}
