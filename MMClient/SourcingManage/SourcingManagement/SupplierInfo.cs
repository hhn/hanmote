﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Bll.MDBll.General;
using Lib.Bll.MDBll.SP;
using Lib.Common.CommonUtils;
using Lib.Model.MD.SP;

namespace MMClient.SourcingManage.SourcingManagement
{
    public partial class SupplierInfo : Form
    {
        private GeneralBLL generalBll = new GeneralBLL();
        private Inquiry_Form inquiry_Form;

        public SupplierInfo(Inquiry_Form inquiry_Form)
        {
            InitializeComponent();
            this.inquiry_Form = inquiry_Form;
            //表格最左一列表头
            SupplierListDGV.TopLeftHeaderCell.Value = "序号";
            //加载供应商信息列表
            LoadSupplierInfo();
        }

        /// <summary>
        /// 加载供应商信息
        /// </summary>
        private void LoadSupplierInfo()
        {
            SupplierListDGV.Rows.Clear();
            List<SupplierBase> supplers = generalBll.GetAllSuppliers();
            if (supplers != null)
            {
                int i = 0;
                foreach (SupplierBase spb in supplers)
                {
                    SupplierListDGV.Rows.Add(1);
                    SupplierListDGV.Rows[i].Cells["supplierId"].Value = spb.Supplier_ID;
                    SupplierListDGV.Rows[i].Cells["supplierName"].Value = spb.Supplier_Name;
                    SupplierListDGV.Rows[i].Cells["supplierPhone"].Value = spb.Mobile_Phone1;
                    SupplierListDGV.Rows[i].Cells["email"].Value = spb.Email;
                    SupplierListDGV.Rows[i].Cells["country"].Value = spb.Nation;
                    i++;
                }
            }
        }

        /// <summary>
        /// 确定按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_Confirm_Click(object sender, EventArgs e)
        {
            DataGridViewRow curRow = this.SupplierListDGV.CurrentRow;
            
        }

        /// <summary>
        /// 关闭
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_Cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// 画行号
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgv_suppliers_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            var dgv = sender as DataGridView;
            if (dgv != null)
            {
                Rectangle rect = new Rectangle(e.RowBounds.Location.X, e.RowBounds.Location.Y, (dgv.RowHeadersWidth + 12) / 2, e.RowBounds.Height);
                TextRenderer.DrawText(e.Graphics, (e.RowIndex + 1).ToString(), dgv.RowHeadersDefaultCellStyle.Font, rect, dgv.RowHeadersDefaultCellStyle.ForeColor, TextFormatFlags.VerticalCenter | TextFormatFlags.Right);
            }
        }
    }
}
