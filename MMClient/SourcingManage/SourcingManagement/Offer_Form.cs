﻿using Lib.Bll.ServiceBll;
using Lib.Common.CommonUtils;
using Lib.Common.MMCException.Bll;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;

namespace MMClient.SourcingManage.SourcingManagement
{
    public partial class Offer_Form : DockContent
    {
        ServiceBill serviceBill = new ServiceBill();
        public Offer_Form()
        {
            InitializeComponent();
            InitData();
        }

        //初始化数据
        private void InitData()
        {
            try
            {
                DataTable dt = serviceBill.GetAllInqueryPriceInfo();
                this.InqueryPriceInfoDGV.DataSource = dt;
            }
            catch (BllException e)
            {
                MessageUtil.ShowError(e.Msg);
            }
        }

        //查看按钮
        private void LookButton_Click(object sender, EventArgs e)
        {
            string state = this.StateCombox.Text;
            if(!"".Equals(state))
            {
                this.ClearDGV(this.InqueryPriceInfoDGV);
                DataTable dt = serviceBill.GetInqueryPriceByState(state,-1,null);
                this.InqueryPriceInfoDGV.DataSource = dt;
            }
        }

        //清空DGV表
        private void ClearDGV(DataGridView dgv)
        {
            if(dgv != null)
            {
                DataTable dt = (DataTable)dgv.DataSource;
                dt.Clear();
            }
        }

        //执行询价按钮
        private void button1_Click(object sender, EventArgs e)
        {
            DataGridViewRow curRow = this.InqueryPriceInfoDGV.CurrentRow;
            if(curRow != null && curRow.Index >= 0)
            {
                MessageUtil.ShowTips("向该询价单下供应商发送邮件成功！");
            }
            else
            {
                MessageUtil.ShowTips("请选择待执行的询价单！");
            }
        }
    }
}
