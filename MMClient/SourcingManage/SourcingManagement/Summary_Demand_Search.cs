﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Net.Mail;
using System.Net.Sockets;
using Aspose.Cells;
using System.Windows.Forms;
using Lib.SqlServerDAL;
using Lib.SqlServerDAL.SourcingManage.ProcurementPlan;
using Lib.Model.SourcingManage.ProcurementPlan;
using Lib.Model.SourcingManage.SourcingManagement;
using MMClient.SourcingManage;
using WeifenLuo.WinFormsUI.Docking;
using System.IO;
using Lib.Bll.SourcingManage.ProcurementPlan;
using Lib.Bll.SourcingManage.SourcingManagement;
using Lib.Common.MMCException.IDAL;
using Lib.Common.CommonUtils;
using Lib.Common.MMCException.Bll;

namespace MMClient.SourcingManage.SourcingManagement
{
    public partial class Summary_Demand_Search : DockContent
    {
        Summary_Demand summaryDemand = null;
        //选中的计划
        List<Demand_Material> demandMaterials = new List<Demand_Material>();
        List<Demand_Material> noSourceDM = new List<Demand_Material>();
        List<Demand_Material> hasSourceDM = new List<Demand_Material>();
        //货源清单
        List<Summary_Demand> demandList = new List<Summary_Demand>();

        List<SourceList> xjxList = new List<SourceList>();
        //查询工具
        SourceListBLL source_ListBLL = new SourceListBLL();
        ReadQuotaBLL readQutoBll = new ReadQuotaBLL();

        Summary_DemandBLL demandBLL = new Summary_DemandBLL();
        Demand_MaterialBLL demandMaterialBll = new Demand_MaterialBLL();
        //控件工具
        ConvenientTools tools = new ConvenientTools();

        //复选框标记数组
        private byte[] flag = new byte[20];

        public Summary_Demand_Search()
        {
            InitializeComponent();
            Init();
        }

        public Summary_Demand_Search(Summary_Demand summaryDemand) : this()
        {
            //将选中数据提交至创建寻源界面
            initData(summaryDemand);
            this.summaryDemand = summaryDemand;
        }

        //初始化采购需求数据：需求计划编号、采购类型、物料类型、最小购买批量、物料编号、物料名称、物料组、需求数量、计量单位
        private void Init()
        {
            try
            {
                DataTable dt = demandBLL.GetSummaryDemandViewData();
                //demandBLL.GetSummaryDemandByState();
                this.source_view.DataSource = dt;
            }
            catch (BllException ex)
            {
                Console.WriteLine(ex.StackTrace);
                MessageUtil.ShowError(ex.Msg);
            }
        }

        private void initData(Summary_Demand summaryDemand)
        {
            source_view.Rows.Clear();
            List<Demand_Material> demandMaterials = demandMaterialBll.findDemandMaterials(summaryDemand.Demand_ID);
            if (demandMaterials.Count > source_view.Rows.Count)
            {
                source_view.Rows.Add(demandMaterials.Count - source_view.Rows.Count);
            }
            for (int i = 0; i < demandMaterials.Count; i++)
            {
                Demand_Material am = demandMaterials.ElementAt(i);
                source_view.Rows[i].Cells["materialId"].Value = am.Material_ID;
                source_view.Rows[i].Cells["materialName"].Value = am.Material_Name;
                source_view.Rows[i].Cells["materialGroup"].Value = am.Material_Group;
                source_view.Rows[i].Cells["factoryId"].Value = summaryDemand.Factory_ID;
                source_view.Rows[i].Cells["stockId"].Value = am.Stock_ID;
                source_view.Rows[i].Cells["count"].Value = am.Demand_Count;
                source_view.Rows[i].Cells["measurement"].Value = am.Measurement;
                source_view.Rows[i].Cells["DeliveryStartTime"].Value = am.DeliveryStartTime.ToString("yyyy-MM-dd");
                source_view.Rows[i].Cells["DeliveryEndTime"].Value = am.DeliveryEndTime.ToString("yyyy-MM-dd");
            }
        }

        /// <summary>
        /// 确定货源
        /// </summary>
        private void analyseMaterials()
        {
            hasSourceDM.Clear();//存放有货源的物料
            //物料信息
            List<Demand_Material> demandMaterials = demandMaterialBll.findDemandMaterials(summaryDemand.Demand_ID);

            for (int i = 0; i < demandMaterials.Count; i++)
            {
                Demand_Material dm = demandMaterials.ElementAt(i);
                //先读取配额协议
                List<Quota_Arrangement_Item> list = readQutoBll.getInfoByMaterialId(dm.Material_ID);
                if (list == null || list.Count == 0)//在配额协议中没有找到该物料信息
                {
                    //下一步在货源清单中寻找物料信息
                    List<SourceList> sourceLists = source_ListBLL.getSourceList(dm.Material_ID, dm.Factory_ID);
                    if (sourceLists == null || sourceLists.Count == 0)//货源清单中没有该物料的源清单
                    {
                        noSourceDM.Add(dm);//配额协议和货源清单都没有该物料的信息
                    }
                    else
                    {
                        for (int j = 0; j < sourceLists.Count; j++)
                        {
                            if (sourceLists.ElementAt(j).StartTime > dm.DeliveryEndTime || sourceLists.ElementAt(j).EndTime < dm.DeliveryStartTime)
                            {
                                noSourceDM.Add(dm);//清单中的有效时间不包含物料的信息
                            }
                            else
                            {
                                hasSourceDM.Add(dm);//货源清单中存在该物料数据
                                break;
                            }
                        }
                    }
                }
                else
                {
                    hasSourceDM.Add(dm);//配额协议中有该物料的信息
                }
            }

        }


        /// <summary>
        /// 加入到工作范围按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void select_button_Click(object sender, EventArgs e)
        {
            demandMaterials.Clear();//准备确定货源的物料
            foreach (DataGridViewRow row in source_view.Rows)
            {
                if (row.Cells["num"].Value != null)
                {
                    if (row.Cells["num"].Value.ToString() == "True")
                    {
                        Demand_Material dm = new Demand_Material();
                        dm.Material_ID = row.Cells["materialId"].Value.ToString();
                        dm.Material_Name = row.Cells["materialName"].Value.ToString();
                        dm.Material_Group = row.Cells["materialGroup"].Value.ToString();
                        dm.Demand_Count = Convert.ToInt32(row.Cells["count"].Value.ToString());
                        dm.Factory_ID = row.Cells["factoryId"].Value.ToString();
                        dm.Stock_ID = row.Cells["stockId"].Value.ToString();
                        dm.Measurement = row.Cells["measurement"].Value.ToString();
                        demandMaterials.Add(dm);
                    }
                }
            }

            analyseMaterials();
            grp_no.Visible = true;
            //判断是否有货源
            if (noSourceDM.Count > noGridView.Rows.Count)
            {
                noGridView.Rows.Add(noSourceDM.Count - noGridView.Rows.Count);
            }
            for (int i = 0; i < noSourceDM.Count; i++)
            {
                noGridView.Rows[i].Cells["noMaterialId"].Value = noSourceDM.ElementAt(i).Material_ID;
                noGridView.Rows[i].Cells["noMaterialName"].Value = noSourceDM.ElementAt(i).Material_Name;
                noGridView.Rows[i].Cells["noDemandCount"].Value = noSourceDM.ElementAt(i).Demand_Count;
                noGridView.Rows[i].Cells["unit"].Value = noSourceDM.ElementAt(i).Measurement;
                noGridView.Rows[i].Cells["nsFactoryId"].Value = noSourceDM.ElementAt(i).Factory_ID;
                noGridView.Rows[i].Cells["nsStockId"].Value = noSourceDM.ElementAt(i).Stock_ID;
                noGridView.Rows[i].Cells["noSourceStartTime"].Value = noSourceDM.ElementAt(i).DeliveryStartTime.ToString("yyyy-MM-dd");
                noGridView.Rows[i].Cells["noSourceEndTime"].Value = noSourceDM.ElementAt(i).DeliveryEndTime.ToString("yyyy-MM-dd");
            }

            HasSource_Form hasSource_Form = null;
            if (hasSource_Form == null || hasSource_Form.IsDisposed)
            {
                hasSource_Form = new HasSource_Form(hasSourceDM);
            }
            hasSource_Form.Show();
        }

        /// <summary>
        /// 创建询价
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            //Inquiry_Form inquiry_Form = new Inquiry_Form(noSourceDM, summaryDemand);
            //SingletonUserUI.addToUserUI(inquiry_Form);
        }

        /// <summary>
        /// 创建招标
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button3_Click(object sender, EventArgs e)
        {
            Bidding_Form bid_Form = new Bidding_Form(noSourceDM, summaryDemand);
            SingletonUserUI.addToUserUI(bid_Form);
        }
    }
}
