﻿using Lib.Bll.ServiceBll;
using Lib.Common.CommonUtils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;

namespace MMClient.SourcingManage.SourcingManagement
{
    public partial class InqueryBin_From : DockContent
    {
        private ServiceBill serviceBill = new ServiceBill();

        public InqueryBin_From()
        {
            InitializeComponent();
            InitData();
        }

        //初始化界面
        private void InitData()
        {
            //初始化状态下拉框
            LoadDataForStateCB();
        }

        //需求状态下拉框
        private void LoadDataForStateCB()
        {
            //List<string> stateList = serviceBill.GetAllState();
            List<string> stateList = new List<string>();
            stateList.Add("显示所有"); //默认
            stateList.Add("待审核");
            stateList.Add("审核通过");
            stateList.Add("审核未通过");
            FormUtils.FillCombox(this.StateCB,stateList);
        }

        //为DGV加载数据
        private void FreshDataForDGV(DataGridView dgv , string state)
        {
            ClearDGV(dgv);
            if(dgv != null)
            {
                DataTable dt = serviceBill.GetSummaryDemandByState(state);
                dgv.DataSource = dt;
            }
        }

        //清空DGV
        private void ClearDGV(DataGridView dgv)
        {
            if(dgv != null)
            {
                object dataSourceObj = dgv.DataSource;
                if(dataSourceObj != null && dataSourceObj is DataTable)
                {
                    DataTable dt = (DataTable)dataSourceObj;
                    dt.Clear();
                }
                else
                {
                    dgv.Rows.Clear();
                }
            }
        }

        //状态 下拉列表框值变化
        private void StateCB_SelectedIndexChanged(object sender, EventArgs e)
        {
            object selectedItem = this.StateCB.SelectedItem;
            if(selectedItem != null && selectedItem is string)
            {
                string state = (string)selectedItem;
                //更新DGV中的数据
                FreshDataForDGV(this.SummaryDemandTotalDGV, state);
            }
        }

        //添加到工作范围按钮
        private void AddBtn_Click(object sender, EventArgs e)
        {
            List<DataGridViewRow> deletedRow = DeleteSelectedRow(this.SummaryDemandTotalDGV,"selectTotal");
            AddRowToTargetDGV(deletedRow, this.SummaryDemandWorkDGV);
        }

        /// <summary>
        /// 将数据行添加到指定的DGV
        /// </summary>
        /// <param name="list">待添加的数据行</param>
        /// <param name="targetDgv">目标DGV</param>
        private void ReloadDataForTargetDGV(List<string> list,DataGridView targetDgv)
        {
            if(list == null || targetDgv == null)
            {
                throw new ArgumentNullException("参数不能为空");
            }
            object dataSourceObj = targetDgv.DataSource;
            if(dataSourceObj != null && dataSourceObj is DataTable)
            {
                DataTable dt = (DataTable)dataSourceObj;
                int rows = dt.Rows.Count;
                for(int i = 0;i < rows; i++)
                {
                    string demandId = dt.Rows[i]["Demand_ID"].ToString();
                    list.Add(demandId);
                }
            }
            DataTable newDt = serviceBill.GetSummaryDemandByIdList(list);
            targetDgv.DataSource = newDt;
        }

        /// <summary>
        /// 删除DGV选中的行,并将删除的行返回（新建的行，只是数据和原来被删除的行一样）
        /// </summary>
        /// <param name="dgv">DataGridView对象</param>
        /// <returns>已删除行的List集合</returns>
        private List<DataGridViewRow> DeleteSelectedRow(DataGridView dgv , string checkBoxColName)
        {
            if(dgv != null)
            {
                List<DataGridViewRow> list = new List<DataGridViewRow>();
                for (int i = dgv.Rows.Count - 1;i >= 0; i--)
                {
                    //判断行是否被选中
                    if(dgv.Rows[i].Cells[checkBoxColName].EditedFormattedValue.ToString() == "True")
                    {
                        DataGridViewRow dataGridViewRow = dgv.Rows[i];
                        DataGridViewRow newRow = CreateRowFrom(dataGridViewRow);
                        list.Add(newRow);
                        dgv.Rows.Remove(dataGridViewRow);
                    }
                }
                if(list.Count > 0)
                {
                    return list;
                }
            }
            return null;
        }

        /// <summary>
        /// 根据给定的CheckBox列的列名判断某一行是否被选中，删除选中的行
        /// </summary>
        /// <param name="dgv"></param>
        /// <param name="checkBoxColName"></param>
        private void DeleteSelectedRowNoCopy(DataGridView dgv, string checkBoxColName)
        {
            if(checkBoxColName == null || dgv == null)
            {
                throw new Exception("参数错误");
            }

            for (int i = dgv.Rows.Count - 1; i >= 0; i--)
            {
                //判断行是否被选中
                if (dgv.Rows[i].Cells[checkBoxColName].EditedFormattedValue.ToString() == "True")
                {
                    DataGridViewRow dataGridViewRow = dgv.Rows[i];
                    dgv.Rows.Remove(dataGridViewRow);
                }
            }
        }

        /// <summary>
        /// 根据给定的DataGridViewRow创建新的DataGridViewRow
        /// </summary>
        /// <param name="sourceRow">源DataGridViewRow</param>
        /// <returns>新的DataGridViewRow</returns>
        private DataGridViewRow CreateRowFrom(DataGridViewRow sourceRow)
        {
            if(sourceRow == null)
            {
                return null;
            }
            DataGridViewRow dataGridViewRow = new DataGridViewRow();
            DataGridViewCellCollection collection = sourceRow.Cells;
            DataGridViewCell[] array = new DataGridViewCell[collection.Count];
            for(int i = 0;i < collection.Count; i++)
            {
                DataGridViewCell cell = collection[i];
                if(cell != null && (cell is DataGridViewCheckBoxCell))
                {
                    DataGridViewCheckBoxCell dataGridViewCheckBoxCell = new DataGridViewCheckBoxCell();
                    dataGridViewRow.Cells.Add(dataGridViewCheckBoxCell);
                }
                else
                {
                    DataGridViewTextBoxCell dataGridViewTextBoxCell = new DataGridViewTextBoxCell
                    {
                        Value = cell.Value.ToString()
                    };
                    dataGridViewRow.Cells.Add(dataGridViewTextBoxCell);
                }
            }
            return dataGridViewRow;
        }

        /// <summary>
        /// 将多行添加到目标DGV
        /// </summary>
        /// <param name="list">要添加的行</param>
        /// <param name="dgv">目标DGV</param>
        private void AddRowToTargetDGV(List<DataGridViewRow> list,DataGridView dgv)
        {
            if (list == null || dgv == null)
            {
                return;
            }

            foreach(DataGridViewRow row in list)
            {
                dgv.Rows.Add(row);
            }
        }

        //刷新按钮
        private void FreshBtn_Click(object sender, EventArgs e)
        {
            //清空工作范围的数据
            ClearDGV(this.SummaryDemandWorkDGV);

            //将显示下拉框设置为默认（显示所有）
            this.StateCB.SelectedIndex = GetDefaultIndexFromStateCB();
            string state = this.StateCB.SelectedItem.ToString();

            //重新加载需求数据
            FreshDataForDGV(this.SummaryDemandTotalDGV, state);

        }

        //默认索引
        private int GetDefaultIndexFromStateCB()
        {
            return 0;
        }

        //从工作范围删除按钮
        private void delete_Click(object sender, EventArgs e)
        {
            DeleteSelectedRowNoCopy(this.SummaryDemandWorkDGV,"selectWork");
        }

        //创建询价按钮
        private void createInqueryPriceBtn_Click(object sender, EventArgs e)
        {
            List<string> list = GetSelectedRowFromDGV(this.SummaryDemandWorkDGV,"selectWork");
            if(list != null && list.Count > 0)
            {
                Inquiry_Form inquiry_Form = new Inquiry_Form(list[0]);
                inquiry_Form.Show();
            }
        }

        //返回指定列为选中状态的物料编码数据
        private List<string> GetSelectedRowFromDGV(DataGridView dgv , string checkBoxColName)
        {
            if(dgv != null)
            {
                List<string> list = new List<string>();
                for(int i = 0;i < dgv.RowCount; i++)
                {
                    if (dgv.Rows[i].Cells[checkBoxColName].EditedFormattedValue.ToString() == "True")
                    {
                        list.Add(dgv.Rows[i].Cells["Demand_IDWork"].Value.ToString());
                    }
                }
                if(list.Count > 0)
                {
                    return list;
                }
            }
            return null;
        }

        //创建招标
        private void createBidBtn_Click(object sender, EventArgs e)
        {
            Bidding_Form bidding_Form = new Bidding_Form();
            bidding_Form.Show();
        }

        //创建拍卖
        private void createAuctionBtn_Click(object sender, EventArgs e)
        {

        }
    }
}
