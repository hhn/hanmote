﻿namespace MMClient.SourcingManage.SourcingManagement
{
    partial class Condition_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.conditionGridView = new System.Windows.Forms.DataGridView();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.txt_NetPrice = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.number = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn2 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn3 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn4 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn5 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.conditionNum = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.priceType = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.disType = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.pn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.calType = new System.Windows.Forms.DataGridViewComboBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.conditionGridView)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // conditionGridView
            // 
            this.conditionGridView.AllowUserToAddRows = false;
            this.conditionGridView.BackgroundColor = System.Drawing.SystemColors.Control;
            this.conditionGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.conditionGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.conditionNum,
            this.priceType,
            this.disType,
            this.pn,
            this.calType,
            this.number});
            this.conditionGridView.Location = new System.Drawing.Point(39, 87);
            this.conditionGridView.Name = "conditionGridView";
            this.conditionGridView.RowTemplate.Height = 23;
            this.conditionGridView.Size = new System.Drawing.Size(651, 134);
            this.conditionGridView.TabIndex = 0;
            // 
            // supplierInforGroupBox
            // 
            this.groupBox1.Controls.Add(this.textBox3);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.button3);
            this.groupBox1.Controls.Add(this.button2);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.txt_NetPrice);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.textBox2);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.textBox1);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.conditionGridView);
            this.groupBox1.Location = new System.Drawing.Point(57, 32);
            this.groupBox1.Name = "supplierInforGroupBox";
            this.groupBox1.Size = new System.Drawing.Size(702, 374);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "条件类型";
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(590, 253);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(100, 21);
            this.textBox3.TabIndex = 11;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(542, 256);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 12);
            this.label4.TabIndex = 10;
            this.label4.Text = "有效价";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(615, 47);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 9;
            this.button3.Text = "删除";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(506, 47);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 8;
            this.button2.Text = "新增";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(615, 304);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 7;
            this.button1.Text = "生成价格";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // txt_NetPrice
            // 
            this.txt_NetPrice.Location = new System.Drawing.Point(440, 253);
            this.txt_NetPrice.Name = "txt_NetPrice";
            this.txt_NetPrice.Size = new System.Drawing.Size(76, 21);
            this.txt_NetPrice.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(405, 256);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 12);
            this.label3.TabIndex = 5;
            this.label3.Text = "净价";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(297, 253);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(77, 21);
            this.textBox2.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(262, 256);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 12);
            this.label2.TabIndex = 3;
            this.label2.Text = "价格";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(105, 253);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(121, 21);
            this.textBox1.TabIndex = 2;
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(47, 256);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 1;
            this.label1.Text = "物料编号";
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "值/比例";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // number
            // 
            this.number.HeaderText = "值/比例";
            this.number.Name = "number";
            // 
            // dataGridViewComboBoxColumn1
            // 
            this.dataGridViewComboBoxColumn1.HeaderText = "条件类型";
            this.dataGridViewComboBoxColumn1.Items.AddRange(new object[] {
            "价格",
            "折扣",
            "折上折",
            "附加费用",
            "运费"});
            this.dataGridViewComboBoxColumn1.Name = "dataGridViewComboBoxColumn1";
            this.dataGridViewComboBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // dataGridViewComboBoxColumn2
            // 
            this.dataGridViewComboBoxColumn2.HeaderText = "定价类型";
            this.dataGridViewComboBoxColumn2.Items.AddRange(new object[] {
            "价格",
            "折扣",
            "附加费",
            "税收"});
            this.dataGridViewComboBoxColumn2.Name = "dataGridViewComboBoxColumn2";
            this.dataGridViewComboBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // dataGridViewComboBoxColumn3
            // 
            this.dataGridViewComboBoxColumn3.HeaderText = "舍入规则";
            this.dataGridViewComboBoxColumn3.Items.AddRange(new object[] {
            "四舍五入",
            "较高值",
            "较低值"});
            this.dataGridViewComboBoxColumn3.Name = "dataGridViewComboBoxColumn3";
            this.dataGridViewComboBoxColumn3.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // dataGridViewComboBoxColumn4
            // 
            this.dataGridViewComboBoxColumn4.HeaderText = "正/负";
            this.dataGridViewComboBoxColumn4.Items.AddRange(new object[] {
            "+",
            "-"});
            this.dataGridViewComboBoxColumn4.Name = "dataGridViewComboBoxColumn4";
            this.dataGridViewComboBoxColumn4.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // dataGridViewComboBoxColumn5
            // 
            this.dataGridViewComboBoxColumn5.HeaderText = "计算类型";
            this.dataGridViewComboBoxColumn5.Items.AddRange(new object[] {
            "数量",
            "百分比"});
            this.dataGridViewComboBoxColumn5.Name = "dataGridViewComboBoxColumn5";
            this.dataGridViewComboBoxColumn5.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // conditionNum
            // 
            this.conditionNum.HeaderText = "条件类型";
            this.conditionNum.Items.AddRange(new object[] {
            "价格",
            "折扣",
            "折上折",
            "附加费用",
            "运费"});
            this.conditionNum.Name = "conditionNum";
            this.conditionNum.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.conditionNum.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // priceType
            // 
            this.priceType.HeaderText = "定价类型";
            this.priceType.Items.AddRange(new object[] {
            "价格",
            "折扣",
            "附加费",
            "税收"});
            this.priceType.Name = "priceType";
            this.priceType.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.priceType.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // disType
            // 
            this.disType.HeaderText = "舍入规则";
            this.disType.Items.AddRange(new object[] {
            "四舍五入",
            "较高值",
            "较低值"});
            this.disType.Name = "disType";
            this.disType.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.disType.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // pn
            // 
            this.pn.HeaderText = "正/负";
            this.pn.Items.AddRange(new object[] {
            "+",
            "-"});
            this.pn.Name = "pn";
            this.pn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.pn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // calType
            // 
            this.calType.HeaderText = "计算类型";
            this.calType.Items.AddRange(new object[] {
            "数量",
            "百分比"});
            this.calType.Name = "calType";
            this.calType.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.calType.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // Condition_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(952, 461);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "Condition_Form";
            this.Text = "条件类型";
            ((System.ComponentModel.ISupportInitialize)(this.conditionGridView)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView conditionGridView;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txt_NetPrice;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.DataGridViewComboBoxColumn conditionNum;
        private System.Windows.Forms.DataGridViewComboBoxColumn priceType;
        private System.Windows.Forms.DataGridViewComboBoxColumn disType;
        private System.Windows.Forms.DataGridViewComboBoxColumn pn;
        private System.Windows.Forms.DataGridViewComboBoxColumn calType;
        private System.Windows.Forms.DataGridViewTextBoxColumn number;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn1;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn2;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn3;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn4;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn5;

    }
}