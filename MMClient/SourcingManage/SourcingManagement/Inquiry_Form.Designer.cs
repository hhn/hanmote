﻿namespace MMClient.SourcingManage.SourcingManagement
{
    partial class Inquiry_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            this.xjdh_lbl = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.dte_OfferTime = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.DemandCountText = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.textBox_BuyerName = new System.Windows.Forms.TextBox();
            this.BuyGroupCB = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.dateTimePicker_CreateTime = new System.Windows.Forms.DateTimePicker();
            this.label7 = new System.Windows.Forms.Label();
            this.txt_InquiryName = new System.Windows.Forms.TextBox();
            this.PurOrgText = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txt_state = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txt_inquiryId = new System.Windows.Forms.TextBox();
            this.MaterialListDGV = new System.Windows.Forms.DataGridView();
            this.Material_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Material_Name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Material_Group = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.measurement = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Material_Type = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Type_Name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn2 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn3 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn4 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn5 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn6 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label9 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.ClearBtn = new System.Windows.Forms.Button();
            this.SupplierDGV = new System.Windows.Forms.DataGridView();
            this.Supplier_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Supplier_Name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Supplier_AccountGroup = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Mobile_Phone1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Email = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SelectSupplierBtn = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.CreateInqueryPriceBtn = new System.Windows.Forms.Button();
            this.CloseBtn = new System.Windows.Forms.Button();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MaterialListDGV)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SupplierDGV)).BeginInit();
            this.SuspendLayout();
            // 
            // xjdh_lbl
            // 
            this.xjdh_lbl.AutoSize = true;
            this.xjdh_lbl.Font = new System.Drawing.Font("宋体", 9F);
            this.xjdh_lbl.Location = new System.Drawing.Point(15, 25);
            this.xjdh_lbl.Name = "xjdh_lbl";
            this.xjdh_lbl.Size = new System.Drawing.Size(53, 12);
            this.xjdh_lbl.TabIndex = 155;
            this.xjdh_lbl.Text = "询价单号";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("宋体", 9F);
            this.label1.Location = new System.Drawing.Point(335, 63);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 169;
            this.label1.Text = "采购组织";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("宋体", 9F);
            this.label2.Location = new System.Drawing.Point(15, 104);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 171;
            this.label2.Text = "采 购 组";
            // 
            // dte_OfferTime
            // 
            this.dte_OfferTime.CustomFormat = "yyyy-MM-dd HH:mm";
            this.dte_OfferTime.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.dte_OfferTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dte_OfferTime.Location = new System.Drawing.Point(709, 57);
            this.dte_OfferTime.Name = "dte_OfferTime";
            this.dte_OfferTime.Size = new System.Drawing.Size(204, 23);
            this.dte_OfferTime.TabIndex = 174;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("宋体", 9F);
            this.label4.Location = new System.Drawing.Point(650, 63);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 12);
            this.label4.TabIndex = 173;
            this.label4.Text = "报价期限";
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.DemandCountText);
            this.panel2.Controls.Add(this.label11);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.textBox_BuyerName);
            this.panel2.Controls.Add(this.BuyGroupCB);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.dateTimePicker_CreateTime);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.txt_InquiryName);
            this.panel2.Controls.Add(this.PurOrgText);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.dte_OfferTime);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.txt_state);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.txt_inquiryId);
            this.panel2.Controls.Add(this.xjdh_lbl);
            this.panel2.Location = new System.Drawing.Point(12, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(932, 135);
            this.panel2.TabIndex = 180;
            // 
            // DemandCountText
            // 
            this.DemandCountText.BackColor = System.Drawing.SystemColors.MenuBar;
            this.DemandCountText.Location = new System.Drawing.Point(709, 101);
            this.DemandCountText.Name = "DemandCountText";
            this.DemandCountText.Size = new System.Drawing.Size(204, 21);
            this.DemandCountText.TabIndex = 204;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("宋体", 9F);
            this.label11.Location = new System.Drawing.Point(650, 104);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(53, 12);
            this.label11.TabIndex = 203;
            this.label11.Text = "需求总量";
            // 
            // label8
            // 
            this.label8.BackColor = System.Drawing.SystemColors.ControlDark;
            this.label8.Location = new System.Drawing.Point(1, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(930, 19);
            this.label8.TabIndex = 202;
            this.label8.Text = "基本信息";
            // 
            // textBox_BuyerName
            // 
            this.textBox_BuyerName.BackColor = System.Drawing.SystemColors.MenuBar;
            this.textBox_BuyerName.Location = new System.Drawing.Point(394, 101);
            this.textBox_BuyerName.Name = "textBox_BuyerName";
            this.textBox_BuyerName.Size = new System.Drawing.Size(186, 21);
            this.textBox_BuyerName.TabIndex = 184;
            // 
            // BuyGroupCB
            // 
            this.BuyGroupCB.BackColor = System.Drawing.SystemColors.MenuBar;
            this.BuyGroupCB.FormattingEnabled = true;
            this.BuyGroupCB.Location = new System.Drawing.Point(74, 101);
            this.BuyGroupCB.Name = "BuyGroupCB";
            this.BuyGroupCB.Size = new System.Drawing.Size(221, 20);
            this.BuyGroupCB.TabIndex = 201;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("宋体", 9F);
            this.label5.Location = new System.Drawing.Point(335, 104);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 12);
            this.label5.TabIndex = 193;
            this.label5.Text = "申 请 人";
            // 
            // dateTimePicker_CreateTime
            // 
            this.dateTimePicker_CreateTime.CalendarMonthBackground = System.Drawing.SystemColors.MenuBar;
            this.dateTimePicker_CreateTime.CustomFormat = "yyyy-MM-dd HH:mm";
            this.dateTimePicker_CreateTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker_CreateTime.Location = new System.Drawing.Point(74, 57);
            this.dateTimePicker_CreateTime.Name = "dateTimePicker_CreateTime";
            this.dateTimePicker_CreateTime.Size = new System.Drawing.Size(221, 21);
            this.dateTimePicker_CreateTime.TabIndex = 185;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(15, 63);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 12);
            this.label7.TabIndex = 184;
            this.label7.Text = "创建时间";
            // 
            // txt_InquiryName
            // 
            this.txt_InquiryName.BackColor = System.Drawing.SystemColors.MenuBar;
            this.txt_InquiryName.Location = new System.Drawing.Point(709, 22);
            this.txt_InquiryName.Name = "txt_InquiryName";
            this.txt_InquiryName.Size = new System.Drawing.Size(204, 21);
            this.txt_InquiryName.TabIndex = 183;
            // 
            // PurOrgText
            // 
            this.PurOrgText.BackColor = System.Drawing.SystemColors.MenuBar;
            this.PurOrgText.Location = new System.Drawing.Point(394, 57);
            this.PurOrgText.Name = "PurOrgText";
            this.PurOrgText.Size = new System.Drawing.Size(186, 21);
            this.PurOrgText.TabIndex = 200;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(650, 25);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 12);
            this.label6.TabIndex = 182;
            this.label6.Text = "询价名称";
            // 
            // txt_state
            // 
            this.txt_state.BackColor = System.Drawing.SystemColors.MenuBar;
            this.txt_state.Location = new System.Drawing.Point(394, 22);
            this.txt_state.Name = "txt_state";
            this.txt_state.ReadOnly = true;
            this.txt_state.Size = new System.Drawing.Size(186, 21);
            this.txt_state.TabIndex = 181;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(335, 25);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 12);
            this.label3.TabIndex = 180;
            this.label3.Text = "状    态";
            // 
            // txt_inquiryId
            // 
            this.txt_inquiryId.BackColor = System.Drawing.SystemColors.MenuBar;
            this.txt_inquiryId.Location = new System.Drawing.Point(74, 22);
            this.txt_inquiryId.Name = "txt_inquiryId";
            this.txt_inquiryId.ReadOnly = true;
            this.txt_inquiryId.Size = new System.Drawing.Size(221, 21);
            this.txt_inquiryId.TabIndex = 179;
            // 
            // MaterialListDGV
            // 
            this.MaterialListDGV.AllowDrop = true;
            this.MaterialListDGV.AllowUserToAddRows = false;
            this.MaterialListDGV.AllowUserToResizeColumns = false;
            this.MaterialListDGV.AllowUserToResizeRows = false;
            this.MaterialListDGV.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.MaterialListDGV.BackgroundColor = System.Drawing.SystemColors.Control;
            this.MaterialListDGV.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.MaterialListDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.MaterialListDGV.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Material_ID,
            this.Material_Name,
            this.Material_Group,
            this.measurement,
            this.Material_Type,
            this.Type_Name});
            this.MaterialListDGV.EnableHeadersVisualStyles = false;
            this.MaterialListDGV.Location = new System.Drawing.Point(-1, 20);
            this.MaterialListDGV.Name = "MaterialListDGV";
            this.MaterialListDGV.RowTemplate.Height = 23;
            this.MaterialListDGV.Size = new System.Drawing.Size(932, 167);
            this.MaterialListDGV.TabIndex = 194;
            // 
            // Material_ID
            // 
            this.Material_ID.DataPropertyName = "Material_ID";
            this.Material_ID.HeaderText = "物料编号";
            this.Material_ID.Name = "Material_ID";
            // 
            // Material_Name
            // 
            this.Material_Name.DataPropertyName = "Material_Name";
            this.Material_Name.HeaderText = "物料名称";
            this.Material_Name.Name = "Material_Name";
            // 
            // Material_Group
            // 
            this.Material_Group.DataPropertyName = "Material_Group";
            this.Material_Group.HeaderText = "物料组";
            this.Material_Group.Name = "Material_Group";
            // 
            // measurement
            // 
            this.measurement.DataPropertyName = "Measurement";
            this.measurement.HeaderText = "计量单位";
            this.measurement.Name = "measurement";
            // 
            // Material_Type
            // 
            this.Material_Type.DataPropertyName = "Material_Type";
            this.Material_Type.HeaderText = "物料类型";
            this.Material_Type.Name = "Material_Type";
            // 
            // Type_Name
            // 
            this.Type_Name.DataPropertyName = "Type_Name";
            this.Type_Name.HeaderText = "物料类型名称";
            this.Type_Name.Name = "Type_Name";
            // 
            // dataGridViewComboBoxColumn1
            // 
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.White;
            this.dataGridViewComboBoxColumn1.DefaultCellStyle = dataGridViewCellStyle7;
            this.dataGridViewComboBoxColumn1.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn1.Frozen = true;
            this.dataGridViewComboBoxColumn1.HeaderText = "供应商编号";
            this.dataGridViewComboBoxColumn1.Name = "dataGridViewComboBoxColumn1";
            this.dataGridViewComboBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewComboBoxColumn1.Width = 80;
            // 
            // dataGridViewComboBoxColumn2
            // 
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.White;
            this.dataGridViewComboBoxColumn2.DefaultCellStyle = dataGridViewCellStyle8;
            this.dataGridViewComboBoxColumn2.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn2.Frozen = true;
            this.dataGridViewComboBoxColumn2.HeaderText = "供应商名称";
            this.dataGridViewComboBoxColumn2.Name = "dataGridViewComboBoxColumn2";
            this.dataGridViewComboBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewComboBoxColumn2.Width = 160;
            // 
            // dataGridViewComboBoxColumn3
            // 
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.Color.White;
            this.dataGridViewComboBoxColumn3.DefaultCellStyle = dataGridViewCellStyle9;
            this.dataGridViewComboBoxColumn3.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn3.Frozen = true;
            this.dataGridViewComboBoxColumn3.HeaderText = "物料编号";
            this.dataGridViewComboBoxColumn3.Name = "dataGridViewComboBoxColumn3";
            this.dataGridViewComboBoxColumn3.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewComboBoxColumn3.Width = 140;
            // 
            // dataGridViewComboBoxColumn4
            // 
            dataGridViewCellStyle10.BackColor = System.Drawing.Color.White;
            this.dataGridViewComboBoxColumn4.DefaultCellStyle = dataGridViewCellStyle10;
            this.dataGridViewComboBoxColumn4.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn4.Frozen = true;
            this.dataGridViewComboBoxColumn4.HeaderText = "物料名称";
            this.dataGridViewComboBoxColumn4.Name = "dataGridViewComboBoxColumn4";
            this.dataGridViewComboBoxColumn4.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewComboBoxColumn4.Width = 140;
            // 
            // dataGridViewComboBoxColumn5
            // 
            dataGridViewCellStyle11.BackColor = System.Drawing.Color.White;
            this.dataGridViewComboBoxColumn5.DefaultCellStyle = dataGridViewCellStyle11;
            this.dataGridViewComboBoxColumn5.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn5.Frozen = true;
            this.dataGridViewComboBoxColumn5.HeaderText = "工厂";
            this.dataGridViewComboBoxColumn5.Name = "dataGridViewComboBoxColumn5";
            this.dataGridViewComboBoxColumn5.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewComboBoxColumn5.Width = 80;
            // 
            // dataGridViewComboBoxColumn6
            // 
            dataGridViewCellStyle12.BackColor = System.Drawing.Color.White;
            this.dataGridViewComboBoxColumn6.DefaultCellStyle = dataGridViewCellStyle12;
            this.dataGridViewComboBoxColumn6.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn6.Frozen = true;
            this.dataGridViewComboBoxColumn6.HeaderText = "仓库";
            this.dataGridViewComboBoxColumn6.Name = "dataGridViewComboBoxColumn6";
            this.dataGridViewComboBoxColumn6.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewComboBoxColumn6.Width = 80;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.MaterialListDGV);
            this.panel1.Location = new System.Drawing.Point(12, 144);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(932, 201);
            this.panel1.TabIndex = 202;
            // 
            // label9
            // 
            this.label9.BackColor = System.Drawing.SystemColors.ControlDark;
            this.label9.Location = new System.Drawing.Point(1, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(930, 19);
            this.label9.TabIndex = 203;
            this.label9.Text = "物料信息";
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.ClearBtn);
            this.panel3.Controls.Add(this.SupplierDGV);
            this.panel3.Controls.Add(this.SelectSupplierBtn);
            this.panel3.Controls.Add(this.label10);
            this.panel3.Location = new System.Drawing.Point(12, 359);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(932, 162);
            this.panel3.TabIndex = 203;
            // 
            // ClearBtn
            // 
            this.ClearBtn.Location = new System.Drawing.Point(772, -1);
            this.ClearBtn.Name = "ClearBtn";
            this.ClearBtn.Size = new System.Drawing.Size(42, 21);
            this.ClearBtn.TabIndex = 206;
            this.ClearBtn.Text = "清空";
            this.ClearBtn.UseVisualStyleBackColor = true;
            this.ClearBtn.Click += new System.EventHandler(this.ClearBtn_Click);
            // 
            // SupplierDGV
            // 
            this.SupplierDGV.AllowDrop = true;
            this.SupplierDGV.AllowUserToAddRows = false;
            this.SupplierDGV.AllowUserToResizeColumns = false;
            this.SupplierDGV.AllowUserToResizeRows = false;
            this.SupplierDGV.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.SupplierDGV.BackgroundColor = System.Drawing.SystemColors.Control;
            this.SupplierDGV.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.SupplierDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.SupplierDGV.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Supplier_ID,
            this.Supplier_Name,
            this.Supplier_AccountGroup,
            this.Mobile_Phone1,
            this.Email});
            this.SupplierDGV.EnableHeadersVisualStyles = false;
            this.SupplierDGV.Location = new System.Drawing.Point(-1, 20);
            this.SupplierDGV.Name = "SupplierDGV";
            this.SupplierDGV.RowTemplate.Height = 23;
            this.SupplierDGV.Size = new System.Drawing.Size(932, 129);
            this.SupplierDGV.TabIndex = 204;
            // 
            // Supplier_ID
            // 
            this.Supplier_ID.DataPropertyName = "Supplier_ID";
            this.Supplier_ID.HeaderText = "供应商编码";
            this.Supplier_ID.Name = "Supplier_ID";
            // 
            // Supplier_Name
            // 
            this.Supplier_Name.DataPropertyName = "Supplier_Name";
            this.Supplier_Name.HeaderText = "供应商名称";
            this.Supplier_Name.Name = "Supplier_Name";
            // 
            // Supplier_AccountGroup
            // 
            this.Supplier_AccountGroup.DataPropertyName = "Supplier_AccountGroup";
            this.Supplier_AccountGroup.HeaderText = "账户组";
            this.Supplier_AccountGroup.Name = "Supplier_AccountGroup";
            // 
            // Mobile_Phone1
            // 
            this.Mobile_Phone1.DataPropertyName = "Mobile_Phone1";
            this.Mobile_Phone1.HeaderText = "移动电话";
            this.Mobile_Phone1.Name = "Mobile_Phone1";
            // 
            // Email
            // 
            this.Email.DataPropertyName = "Email";
            this.Email.HeaderText = "邮箱";
            this.Email.Name = "Email";
            // 
            // SelectSupplierBtn
            // 
            this.SelectSupplierBtn.Location = new System.Drawing.Point(818, -1);
            this.SelectSupplierBtn.Name = "SelectSupplierBtn";
            this.SelectSupplierBtn.Size = new System.Drawing.Size(111, 21);
            this.SelectSupplierBtn.TabIndex = 205;
            this.SelectSupplierBtn.Text = "确定接洽的供应商";
            this.SelectSupplierBtn.UseVisualStyleBackColor = true;
            this.SelectSupplierBtn.Click += new System.EventHandler(this.SelectSupplierBtn_Click);
            // 
            // label10
            // 
            this.label10.BackColor = System.Drawing.SystemColors.ControlDark;
            this.label10.Location = new System.Drawing.Point(-1, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(930, 19);
            this.label10.TabIndex = 204;
            this.label10.Text = "供应商信息";
            // 
            // CreateInqueryPriceBtn
            // 
            this.CreateInqueryPriceBtn.Location = new System.Drawing.Point(875, 527);
            this.CreateInqueryPriceBtn.Name = "CreateInqueryPriceBtn";
            this.CreateInqueryPriceBtn.Size = new System.Drawing.Size(69, 21);
            this.CreateInqueryPriceBtn.TabIndex = 207;
            this.CreateInqueryPriceBtn.Text = "创建询价";
            this.CreateInqueryPriceBtn.UseVisualStyleBackColor = true;
            this.CreateInqueryPriceBtn.Click += new System.EventHandler(this.CreateInqueryPriceBtn_Click);
            // 
            // CloseBtn
            // 
            this.CloseBtn.Location = new System.Drawing.Point(795, 527);
            this.CloseBtn.Name = "CloseBtn";
            this.CloseBtn.Size = new System.Drawing.Size(69, 21);
            this.CloseBtn.TabIndex = 208;
            this.CloseBtn.Text = "关闭";
            this.CloseBtn.UseVisualStyleBackColor = true;
            this.CloseBtn.Click += new System.EventHandler(this.CloseBtn_Click);
            // 
            // Inquiry_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(956, 553);
            this.Controls.Add(this.CloseBtn);
            this.Controls.Add(this.CreateInqueryPriceBtn);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel2);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "Inquiry_Form";
            this.Text = "创建询价";
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MaterialListDGV)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SupplierDGV)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label xjdh_lbl;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dte_OfferTime;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn1;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn2;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn3;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn4;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn5;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn6;
        private System.Windows.Forms.DataGridView MaterialListDGV;
        private System.Windows.Forms.TextBox txt_inquiryId;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txt_state;
        private System.Windows.Forms.TextBox txt_InquiryName;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBox_BuyerName;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DateTimePicker dateTimePicker_CreateTime;
        private System.Windows.Forms.TextBox PurOrgText;
        private System.Windows.Forms.ComboBox BuyGroupCB;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button SelectSupplierBtn;
        private System.Windows.Forms.DataGridView SupplierDGV;
        private System.Windows.Forms.DataGridViewTextBoxColumn Supplier_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Supplier_Name;
        private System.Windows.Forms.DataGridViewTextBoxColumn Supplier_AccountGroup;
        private System.Windows.Forms.DataGridViewTextBoxColumn Mobile_Phone1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Email;
        private System.Windows.Forms.Button ClearBtn;
        private System.Windows.Forms.Button CreateInqueryPriceBtn;
        private System.Windows.Forms.DataGridViewTextBoxColumn Material_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Material_Name;
        private System.Windows.Forms.DataGridViewTextBoxColumn Material_Group;
        private System.Windows.Forms.DataGridViewTextBoxColumn measurement;
        private System.Windows.Forms.DataGridViewTextBoxColumn Material_Type;
        private System.Windows.Forms.DataGridViewTextBoxColumn Type_Name;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox DemandCountText;
        private System.Windows.Forms.Button CloseBtn;
    }
}