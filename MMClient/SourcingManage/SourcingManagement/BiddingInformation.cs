﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MMClient.ContractManage;
using WeifenLuo.WinFormsUI.Docking;

namespace MMClient.SourcingManage.SourcingManagement
{
    public partial class BiddingInformation_Form : DockContent
    {
        public BiddingInformation_Form()
        {
            InitializeComponent();
            zdjgf_panel.Visible = false;
            zdsyqcbf_panel.Visible = false;
            this.tabControl1.Height = 750;
            this.tabPage2.Height = 700;
            this.tabPage3.Height = 700;
        }

        /// <summary>
        ///选择评估方法
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pgff_cmb_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (pgff_cmb.SelectedItem.Equals("加权平均法"))
            {
                sjbg_panel.Visible = true;
                zdjgf_panel.Visible = false;
                zdsyqcbf_panel.Visible = false; 
            }
            else if (pgff_cmb.SelectedItem.Equals("最低价格法"))
            {
                sjbg_panel.Visible = false;
                zdjgf_panel.Visible = true;
                zdjgf_panel.Location = new Point(zdjgf_panel.Location.X, 40);
                zdsyqcbf_panel.Visible = false; 
            }
            else if (pgff_cmb.SelectedItem.Equals("最低所有权成本法"))
            {
                sjbg_panel.Visible = false;
                zdjgf_panel.Visible = false;
                zdsyqcbf_panel.Visible = true;
                zdsyqcbf_panel.Location = new Point(zdsyqcbf_panel.Location.X, 40);
            }
        }
    }
}
