﻿namespace MMClient.SourcingManage.SourcingManagement
{
    partial class Offer_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.StateCombox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.InqueryPriceInfoDGV = new System.Windows.Forms.DataGridView();
            this.Source_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Inquiry_Name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Buyer_Name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.State = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Purchase_Org = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Purchase_Group = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CreateTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.button1 = new System.Windows.Forms.Button();
            this.LookButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.InqueryPriceInfoDGV)).BeginInit();
            this.SuspendLayout();
            // 
            // StateCombox
            // 
            this.StateCombox.FormattingEnabled = true;
            this.StateCombox.Items.AddRange(new object[] {
            "待审核",
            "待审批",
            "已批准",
            "未报价",
            "已报价"});
            this.StateCombox.Location = new System.Drawing.Point(83, 14);
            this.StateCombox.Name = "StateCombox";
            this.StateCombox.Size = new System.Drawing.Size(121, 20);
            this.StateCombox.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 12);
            this.label1.TabIndex = 1;
            this.label1.Text = "询价单状态";
            // 
            // InqueryPriceInfoDGV
            // 
            this.InqueryPriceInfoDGV.AllowDrop = true;
            this.InqueryPriceInfoDGV.AllowUserToAddRows = false;
            this.InqueryPriceInfoDGV.AllowUserToResizeColumns = false;
            this.InqueryPriceInfoDGV.AllowUserToResizeRows = false;
            this.InqueryPriceInfoDGV.BackgroundColor = System.Drawing.SystemColors.Control;
            this.InqueryPriceInfoDGV.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.InqueryPriceInfoDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.InqueryPriceInfoDGV.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Source_ID,
            this.Inquiry_Name,
            this.Buyer_Name,
            this.State,
            this.Purchase_Org,
            this.Purchase_Group,
            this.CreateTime});
            this.InqueryPriceInfoDGV.EnableHeadersVisualStyles = false;
            this.InqueryPriceInfoDGV.Location = new System.Drawing.Point(0, 43);
            this.InqueryPriceInfoDGV.Name = "InqueryPriceInfoDGV";
            this.InqueryPriceInfoDGV.RowTemplate.Height = 23;
            this.InqueryPriceInfoDGV.Size = new System.Drawing.Size(788, 216);
            this.InqueryPriceInfoDGV.TabIndex = 195;
            // 
            // Source_ID
            // 
            this.Source_ID.DataPropertyName = "Source_ID";
            this.Source_ID.HeaderText = "询价单号";
            this.Source_ID.Name = "Source_ID";
            this.Source_ID.Width = 145;
            // 
            // Inquiry_Name
            // 
            this.Inquiry_Name.DataPropertyName = "Inquiry_Name";
            this.Inquiry_Name.HeaderText = "询价名称";
            this.Inquiry_Name.Name = "Inquiry_Name";
            // 
            // Buyer_Name
            // 
            this.Buyer_Name.DataPropertyName = "Buyer_Name";
            this.Buyer_Name.HeaderText = "申请人";
            this.Buyer_Name.Name = "Buyer_Name";
            // 
            // State
            // 
            this.State.DataPropertyName = "State";
            this.State.HeaderText = "询价状态";
            this.State.Name = "State";
            // 
            // Purchase_Org
            // 
            this.Purchase_Org.DataPropertyName = "Purchase_Org";
            this.Purchase_Org.HeaderText = "采购组织";
            this.Purchase_Org.Name = "Purchase_Org";
            // 
            // Purchase_Group
            // 
            this.Purchase_Group.DataPropertyName = "Purchase_Group";
            this.Purchase_Group.HeaderText = "采购组";
            this.Purchase_Group.Name = "Purchase_Group";
            // 
            // CreateTime
            // 
            this.CreateTime.DataPropertyName = "CreateTime";
            this.CreateTime.HeaderText = "创建时间";
            this.CreateTime.Name = "CreateTime";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(713, 11);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 196;
            this.button1.Text = "执行询价";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // LookButton
            // 
            this.LookButton.Location = new System.Drawing.Point(210, 14);
            this.LookButton.Name = "LookButton";
            this.LookButton.Size = new System.Drawing.Size(75, 23);
            this.LookButton.TabIndex = 197;
            this.LookButton.Text = "查看";
            this.LookButton.UseVisualStyleBackColor = true;
            this.LookButton.Click += new System.EventHandler(this.LookButton_Click);
            // 
            // Offer_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(791, 460);
            this.Controls.Add(this.LookButton);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.InqueryPriceInfoDGV);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.StateCombox);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "Offer_Form";
            this.Text = "执行询价";
            ((System.ComponentModel.ISupportInitialize)(this.InqueryPriceInfoDGV)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox StateCombox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView InqueryPriceInfoDGV;
        private System.Windows.Forms.DataGridViewTextBoxColumn Source_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Inquiry_Name;
        private System.Windows.Forms.DataGridViewTextBoxColumn Buyer_Name;
        private System.Windows.Forms.DataGridViewTextBoxColumn State;
        private System.Windows.Forms.DataGridViewTextBoxColumn Purchase_Org;
        private System.Windows.Forms.DataGridViewTextBoxColumn Purchase_Group;
        private System.Windows.Forms.DataGridViewTextBoxColumn CreateTime;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button LookButton;
    }
}