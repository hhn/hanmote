﻿using Lib.Bll.ServiceBll;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MMClient.SourcingManage.SourcingManagement
{
    public partial class SelectSupplier : Form
    {
        private ServiceBill serviceBill = new ServiceBill();
        private Inquiry_Form inquiry_Form = null;

        public SelectSupplier(Inquiry_Form inquiry_Form)
        {
            InitializeComponent();
            this.inquiry_Form = inquiry_Form;
            Init();
        }

        private void Init()
        {
            DataTable dt = serviceBill.GetAllSupplier();
            this.SupplierDGV.DataSource = dt;
        }

        //确定按钮
        private void OkBtn_Click(object sender, EventArgs e)
        {
            //获取选中状态的供应商编码
            List<string> list = SelectedSupplier(this.SupplierDGV);
            //刷新供应商数据
            inquiry_Form.FreshSupplierList(list);
            //关闭窗口
            this.Close();
        }

        //已选中的供应商ID
        private List<string> SelectedSupplier(DataGridView dgv)
        {
            if(dgv != null)
            {
                List<string> list = new List<string>();
                for(int i = 0;i < dgv.Rows.Count; i++)
                {
                    if (dgv.Rows[i].Cells["select"].EditedFormattedValue.ToString() == "True")
                    {
                        list.Add(dgv.Rows[i].Cells["Supplier_ID"].Value.ToString());
                    }
                }
                if(list.Count > 0)
                {
                    return list;
                }
            }
            return null;
        }
    }
}
