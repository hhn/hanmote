﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Bll.MDBll.SP;
using Lib.Bll.SourcingManage.SourceingRecord;
using Lib.Bll.SourcingManage.SourcingManagement;
using Lib.Model.MD.SP;
using Lib.Model.SourcingManage.SourceingRecord;
using Lib.Model.SourcingManage.SourcingManagement;
using WeifenLuo.WinFormsUI.Docking;
using MMClient.SupplierPerformance.SPReport;
using System.Data.SqlClient;
using Lib.SqlServerDAL;

namespace MMClient.SourcingManage.SourcingManagement
{
    public partial class CompareBid_Form : DockContent
    {

        private UserUI userUI;
        private SourceBid bid;

        /// <summary>
        /// 存放选中的标准,和名称
        /// </summary>
        private Dictionary<string, string> standardNameMap = null;

        /// <summary>
        /// 存放选中的供应商列表
        /// </summary>
        private Dictionary<string, string> supplierMap = null;

        SourceBLL sourceBll = new SourceBLL();
        SourceBidBLL bidBLL = new SourceBidBLL();
        BidPartnerBLL bidPartnerBLL = new BidPartnerBLL();
        SourceSupplierBLL sourceSupplierBll = new SourceSupplierBLL();
        SourceMaterialBLL sourceMaterialBll = new SourceMaterialBLL();
        BidWeightScoreBLL bidWeightScoreBLL = new BidWeightScoreBLL();
        BidSupplierWeightScoreBLL bidSupplierWeightScoreBLL = new BidSupplierWeightScoreBLL();
        RecordInfoBLL recordInfoBLL = new RecordInfoBLL();
        SourceConditionBLL sourceConditionBLL = new SourceConditionBLL();
        SourceTimeSectionBLL sourceTimeSectionBLL = new SourceTimeSectionBLL();
        SourceNumberSectionBLL sourceNumberSectionBLL = new SourceNumberSectionBLL();
        SourceResultBLL sourceResultBll = new SourceResultBLL();
        SourceSupplierBLL sourceSupplierBLL = new SourceSupplierBLL();
        SourceSupplierMaterialBLL sourceSMBll = new SourceSupplierMaterialBLL();

        List<string> needSidList = new List<string>();

        SourceListBLL sourceListBll = new SourceListBLL();

        SupplierBaseBLL supplierBaseBLL = new SupplierBaseBLL();
        LowestCostBLL lowestCostBll = new LowestCostBLL();

        //控件工具
        ConvenientTools tools = new ConvenientTools();

        public CompareBid_Form()
        {
            InitializeComponent();
        }

        public CompareBid_Form(UserUI userUI, SourceBid bid)
        {
            InitializeComponent();
            this.userUI = userUI;
            this.bid = bid;
            initData(bid);
            init();
            initMaterialId();
            initSupplier(bid);
            //initSupplierEvlaView(bid);
            initLowCost();
        }

        private void initMaterialId()
        {
            List<SourceMaterial> listMaterials = sourceMaterialBll.findSourceMaterialsBySourceId(bid.BidId);
            for (int i = 0; i < listMaterials.Count; i++)
            {
                cmb_MaterialId.Items.Add(listMaterials.ElementAt(i).Material_ID);
                cmb_LPMaterialId.Items.Add(listMaterials.ElementAt(i).Material_ID);
            }

        }

        private void initLowCost()
        {
            List<LowestCost> list = lowestCostBll.getAllLowestBySId(txt_BidId.Text);
            allCostGridView.Rows.Clear();
            if (list.Count > allCostGridView.Rows.Count)
            {
                allCostGridView.Rows.Add(list.Count - allCostGridView.Rows.Count);
            }
            for (int i = 0; i < list.Count; i++)
            {
                allCostGridView.Rows[i].Cells["CostItem"].Value = list.ElementAt(i).CostItem;
                allCostGridView.Rows[i].Cells["First"].Value = list.ElementAt(i).First;
                allCostGridView.Rows[i].Cells["Second"].Value = list.ElementAt(i).Second;
                allCostGridView.Rows[i].Cells["Third"].Value = list.ElementAt(i).Third;
                allCostGridView.Rows[i].Cells["forth"].Value = list.ElementAt(i).Forth;
                allCostGridView.Rows[i].Cells["fifth"].Value = list.ElementAt(i).Fifth;
                allCostGridView.Rows[i].Cells["sixth"].Value = list.ElementAt(i).Sixth;
                allCostGridView.Rows[i].Cells["seventh"].Value = list.ElementAt(i).Seventh;
                allCostGridView.Rows[i].Cells["eighth"].Value = list.ElementAt(i).Eighth;
                allCostGridView.Rows[i].Cells["ninth"].Value = list.ElementAt(i).Ninth;
                allCostGridView.Rows[i].Cells["tenth"].Value = list.ElementAt(i).Tenth;
            }
        }

        /*
        private void initSupplierEvlaView(SourceBid bid)
        {
            supplierEvlaView.Rows.Clear();
            List<SourceSupplier> list = sourceSupplierBLL.getBidSupplier();
            if (list.Count > supplierEvlaView.Rows.Count)
            {
                supplierEvlaView.Rows.Add(list.Count - supplierEvlaView.Rows.Count);
            }
            for (int i = 0; i < list.Count; i++)
            {
                supplierEvlaView.Rows[i].Cells["supplierId"].Value = list.ElementAt(i).Supplier_ID;
                supplierEvlaView.Rows[i].Cells["supplierName"].Value = list.ElementAt(i).Supplier_Name;
                supplierEvlaView.Rows[i].Cells["classificationResult"].Value = list.ElementAt(i).ClassificationResult;
                supplierEvlaView.Rows[i].Cells["contact"].Value = list.ElementAt(i).Contact;
                supplierEvlaView.Rows[i].Cells["email"].Value = list.ElementAt(i).Email;
                supplierEvlaView.Rows[i].Cells["address"].Value = list.ElementAt(i).Address;
                supplierEvlaView.Rows[i].Cells["state"].Value = getStringByNumber(list.ElementAt(i).State);
            }

        }
         * */
        private string getStringByNumber(int num)
        {
            string str = null;
            if (num == 0)
            {
                str = "未投标";
            }
            if (num == 1)
            {
                str = "已投标";
            }
            if (num == 2)
            {
                str = "已接受";
            }
            if (num == 3)
            {
                str = "已拒绝";
            }
            return str;
        }

        private void initData(SourceBid bid)
        {
            //招标基本信息
            txt_BidId.Text = bid.BidId;
            Source source = sourceBll.findSourceById(bid.BidId);
            txt_BidName.Text = source.Source_Name;
            txt_State.Text = getStateByInt(bid.BidState);
            txt_serviceType.Text = bid.ServiceType;
            txt_catalogue.Text = bid.Catalogue;
            txt_displayType.Text = bid.DisplayType;
            txt_transType.Text = bid.TransType;
            if (bid.TransType.Equals("谈判"))
            {
                btn_addSupplier.Visible = false;
                btn_WeightSave.Visible = false;
                btn_Transact.Visible = true;
            }
            else if (bid.TransType.Equals("等级"))
            {
                btn_addSupplier.Visible = true;
                btn_WeightSave.Visible = false;
                btn_Transact.Visible = false;
            }
            txt_purchaseOrg.Text = source.PurchaseOrg;
            txt_purchaseGroup.Text = source.PurchaseGroup;
            txt_StartTime.Text = bid.StartTime.ToString("yyyy-MM-dd HH:mm:ss");
            txt_EndTime.Text = bid.EndTime.ToString("yyyy-MM-dd HH:mm:ss");
            txt_startBeginTime.Text = bid.StartBeginTime.ToString("yyyy-MM-dd HH:mm:ss");
            txt_limitTime.Text = bid.LimitTime.ToString("yyyy-MM-dd");
            txt_timeZone.Text = bid.TimeZone.ToString();
            txt_currency.Text = bid.Currency.ToString();
            txt_commBidStartDate.Text = bid.CommBidStartDate.ToString("yyyy-MM-dd");
            txt_commBidStartTime.Text = bid.CommBidStartTime.ToString("HH:mm:ss");
            txt_techBidStartDate.Text = bid.TechBidStartDate.ToString("yyyy-MM-dd");
            txt_techBidStartTime.Text = bid.TechBidStartTime.ToString("HH:mm:ss");
            txt_buyEndDate.Text = bid.BuyEndDate.ToString("yyyy-MM-dd");
            txt_buyEndTime.Text = bid.BuyEndTime.ToString("HH:mm:ss");
            txt_enrollEndDate.Text = bid.EnrollEndDate.ToString("yyyy-MM-dd");
            txt_enrollEndTime.Text = bid.EnrollEndTime.ToString("HH:mm:ss");
            txt_enrollStartDate.Text = bid.EnrollStartDate.ToString("yyyy-MM-dd");
            txt_enrollStartTime.Text = bid.EnrollStartTime.ToString("HH:mm:ss");
            txt_bidCost.Text = bid.BidCost.ToString();
            //合作伙伴
            BidPartner bp = bidPartnerBLL.findBidPartnerByBidId(bid.BidId);
            if (bp != null)
            {
                txt_requesterId.Text = bp.RequesterId.ToString();
                txt_requesterName.Text = bp.RequesterName.ToString();
                txt_receiverId.Text = bp.ReceiverId.ToString();
                txt_receiverName.Text = bp.ReceiverName.ToString();
                txt_dropPointId.Text = bp.DropPointId.ToString();
                txt_dropPointName.Text = bp.DropPointName.ToString();
                txt_reviewerId.Text = bp.ReviewerId.ToString();
                txt_ReviewerName.Text = bp.ReviewerName.ToString();
                txt_callerId.Text = bp.CallerId.ToString();
                txt_callerName.Text = bp.CallerName.ToString();
            }
            //投标人
            List<SourceSupplier> list = sourceSupplierBll.getBidSuppliersByBidId(bid.BidId);
            SupplierGridView.Rows.Clear();
            if (list.Count > SupplierGridView.Rows.Count)
            {
                SupplierGridView.Rows.Add(list.Count - SupplierGridView.Rows.Count);
            }
            for (int i = 0; i < list.Count; i++)
            {
                SupplierGridView.Rows[i].Cells["bidSupplierId"].Value = list.ElementAt(i).Supplier_ID;
                SupplierGridView.Rows[i].Cells["bidSupplierName"].Value = list.ElementAt(i).Supplier_Name;
                SupplierGridView.Rows[i].Cells["bidClassificationResult"].Value = list.ElementAt(i).ClassificationResult;
                SupplierGridView.Rows[i].Cells["bidContact"].Value = list.ElementAt(i).Contact;
                SupplierGridView.Rows[i].Cells["bidEmail"].Value = list.ElementAt(i).Email;
                SupplierGridView.Rows[i].Cells["bidAddress"].Value = list.ElementAt(i).Address;
                SupplierGridView.Rows[i].Cells["bidState"].Value = list.ElementAt(i).State;
            }
            //评价标准
            tab_EvalMethod.SelectedIndex = bid.EvalMethId;
            txt_EvalMethod.Text = tab_EvalMethod.SelectedTab.Text;
            if (bid.EvalMethId == 0)
            {
                tab_WeightScore.Parent = null;
                tab_LeastTotalCost.Parent = null;
                tab_ValueAsses.Parent = null;
            }
            else if (bid.EvalMethId == 1)//加权评分法
            {
                tab_Lowprice.Parent = null;
                tab_LeastTotalCost.Parent = null;
                tab_ValueAsses.Parent = null;
                /*
                List<BidWeightScore> listBws = bidWeightScoreBLL.findWeightScoresByBidId(bid.BidId);
                SupplierScoreGridView.Rows.Clear();
                if (listBws.Count > SupplierScoreGridView.Rows.Count)
                {
                    SupplierScoreGridView.Rows.Add(listBws.Count - SupplierScoreGridView.Rows.Count);
                }
                for (int i = 0; i < listBws.Count; i++)
                {
                    SupplierScoreGridView.Rows[i].Cells["weightName"].Value = listBws.ElementAt(i).WeightScoreName;
                    SupplierScoreGridView.Rows[i].Cells["weightNum"].Value = listBws.ElementAt(i).WeightScoreNum;
                }
                 * **/
            }
            else if (bid.EvalMethId == 2)
            {
                tab_Lowprice.Parent = null;
                tab_WeightScore.Parent = null;
                tab_ValueAsses.Parent = null;
            }
            else if (bid.EvalMethId == 3)
            {
                tab_Lowprice.Parent = null;
                tab_WeightScore.Parent = null;
                tab_LeastTotalCost.Parent = null;
            }

            //项目数据
            List<SourceMaterial> listMaterials = sourceMaterialBll.findSourceMaterialsBySourceId(bid.BidId);

            MaterialGridview.Rows.Clear();
            if (listMaterials.Count > MaterialGridview.Rows.Count)
            {
                MaterialGridview.Rows.Add(listMaterials.Count - MaterialGridview.Rows.Count);
            }
            for (int i = 0; i < listMaterials.Count; i++)
            {
                MaterialGridview.Rows[i].Cells["bidMaterialId"].Value = listMaterials.ElementAt(i).Material_ID;
                MaterialGridview.Rows[i].Cells["bidMaterialName"].Value = listMaterials.ElementAt(i).Material_Name;
                MaterialGridview.Rows[i].Cells["bidDemandCount"].Value = listMaterials.ElementAt(i).Demand_Count;
                MaterialGridview.Rows[i].Cells["bidDemandMeasurement"].Value = listMaterials.ElementAt(i).Unit;
                MaterialGridview.Rows[i].Cells["factoryId"].Value = listMaterials.ElementAt(i).FactoryId;
                MaterialGridview.Rows[i].Cells["stockId"].Value = listMaterials.ElementAt(i).StockId;
                MaterialGridview.Rows[i].Cells["DeliveryStartTime"].Value = listMaterials.ElementAt(i).DeliveryStartTime.ToString("yyyy-MM-dd");
                MaterialGridview.Rows[i].Cells["DeliveryEndTime"].Value = listMaterials.ElementAt(i).DeliveryEndTime.ToString("yyyy-MM-dd");
            }
        }

        private string getStateByInt(int num)
        {
            string str = "";
            if (num == 0)
            {
                str = "待保存";
            }
            else if (num == 1)
            {
                str = "待发布";
            }
            else if (num == 2)
            {
                str = "待报价";
            }
            else if (num == 3)
            {
                str = "待评标";
            }
            else if (num == 4)
            {
                str = "已完成";
            }
            return str;
        }


        //初始化列值
        private void init()
        {
            DataTable dt = new DataTable();
            List<SourceSupplier> list = sourceSupplierBll.getBidSuppliersByBidId(bid.BidId);
            DataColumn dc = new DataColumn("weightName", typeof(string));
            dt.Columns.Add(dc);
            List<string> temp = new List<string>();//保存供应商
            Dictionary<string, int> dic = new Dictionary<string, int>();
            for (int i = 0; i < list.Count;i++)
            {
                dt.Columns.Add(new DataColumn(list.ElementAt(i).Supplier_ID, typeof(string)));
                temp.Add(list.ElementAt(i).Supplier_ID);
                dic.Add(list.ElementAt(i).Supplier_ID, 0);//保存每个供应商的得分
            }
            List<BidWeightScore> listWs = bidWeightScoreBLL.findWeightScoresByBidId(bid.BidId);
            DataRow dr;
            for (int i = 0; i < listWs.Count; i++)
            {
                dr = dt.NewRow();
                string wn = listWs.ElementAt(i).WeightScoreName;
                dr["weightName"] = wn;//评价标准
                for (int j = 0; j < temp.Count; j++)
                {
                    string sid = temp.ElementAt(j);
                    int res = bidSupplierWeightScoreBLL.getResultByBidSIdWeightName(bid.BidId,sid,wn);//供应商得分
                    dr[sid] = listWs.ElementAt(i).WeightScoreNum * res;
                    dic[sid] = dic[sid] + Convert.ToInt32(dr[sid].ToString());
                }
                dt.Rows.Add(dr);
            }
            dr = dt.NewRow();
            dr["weightName"] = "合计";
            string si = null;
            int max = 0;
            int total = 0;
            foreach (KeyValuePair<string, int> pair in dic)
            {
                total += pair.Value;
            }
            foreach (KeyValuePair<string, int> pair in dic)
            {
                dr[pair.Key] = Math.Round(pair.Value*100.0/total,2)+"%";
                if(pair.Value>max)
                {
                    max = pair.Value;
                    si = pair.Key;
                }
            }
            dt.Rows.Add(dr);
            resultGridView.DataSource = dt;
            //cmb_SupplierId.Text = si;
        }

        private void initSupplier(SourceBid bid)
        {
            //投标人
            List<SourceSupplier> list = sourceSupplierBll.getBidSuppliersByBidId(bid.BidId);
            if (list.Count > resSupplierGridView.Rows.Count)
            {
                resSupplierGridView.Rows.Add(list.Count-resSupplierGridView.Rows.Count);
            }
            for (int i = 0; i < list.Count; i++)
            {
                resSupplierGridView.Rows[i].Cells["resSupplierId"].Value = list.ElementAt(i).Supplier_ID;
                cmb_ResSupplier.Items.Add(list.ElementAt(i).Supplier_ID);
                cmb_AnalySupplier.Items.Add(list.ElementAt(i).Supplier_ID);
            }
        }

        //保存按钮
        private void btn_WeightSave_Click(object sender, EventArgs e)
        {
            int count = needSidList.Count;
            if (count == 0)
            {
                MessageBox.Show("还未选择中标供应商");
                return;
            }
            for (int i = 0; i < count; i++)
            {
                foreach (DataGridViewRow dgvr in MaterialGridview.Rows)
                {
                    //将寻源结果信息写入数据表
                    SourceResult sr = new SourceResult();
                    sr.Item_ID = "TX" + tools.systemTimeToStr();
                    sr.Source_ID = bid.BidId;
                    sr.Supplier_ID = needSidList.ElementAt(i);
                    sr.Material_ID = dgvr.Cells["bidMaterialId"].Value.ToString();
                    sr.Material_Name = dgvr.Cells["bidMaterialName"].Value.ToString();
                    sr.Factory_ID = dgvr.Cells["factoryId"].Value.ToString();
                    sr.Stock_ID = dgvr.Cells["stockId"].Value.ToString();
                    sr.Buy_Number = Convert.ToInt32(dgvr.Cells["bidDemandCount"].Value.ToString());
                    sourceResultBll.addSourceResult(sr);

                    //将结果信息写入采购信息记录表
                    RecordInfo ri = new RecordInfo();
                    ri.RecordInfoId = tools.systemTimeToStr();//生成一个采购信息记录编号
                    ri.SupplierId = needSidList.ElementAt(i);
                    SupplierBase sb = supplierBaseBLL.GetSupplierBasicInformation(ri.SupplierId);
                    ri.SupplierName = sb.Supplier_Name;//根据供应商编号查询供应商名称
                    Source source = sourceBll.findSourceById(bid.BidId);
                    ri.PurchaseOrg = source.PurchaseOrg;
                    ri.PurchaseGroup = source.PurchaseGroup;
                    ri.MaterialId = dgvr.Cells["bidMaterialId"].Value.ToString();
                    ri.MaterialName = dgvr.Cells["bidMaterialName"].Value.ToString();
                    ri.FactoryId = dgvr.Cells["factoryId"].Value.ToString();
                    ri.StockId = dgvr.Cells["stockId"].Value.ToString();
                    double temp = Math.Ceiling(Math.Round(Convert.ToInt32(dgvr.Cells["bidDemandCount"].Value.ToString())*100.0 / count, 2)/100);
                    ri.DemandCount = int.Parse(temp + "") ;
                    ri.FromType = 1;
                    ri.FromId = bid.BidId;
                    ri.ContactState = "无效";
                    ri.StartTime = DateTime.Parse(dgvr.Cells["DeliveryStartTime"].Value.ToString());
                    ri.EndTime = DateTime.Parse(dgvr.Cells["DeliveryEndTime"].Value.ToString());
                    recordInfoBLL.addRecordInfo(ri);
                    bid.BidState = 6;
                    bidBLL.updateBidState(bid);

                    //生成货源清单
                    SourceList sl = new SourceList();
                    sl.SourceListId = "S" + tools.systemTimeToStr();
                    sl.Material_ID = ri.MaterialId;
                    sl.Supplier_ID = ri.SupplierId;
                    sl.Factory_ID = ri.FactoryId;
                    sl.Blk = 0;
                    sl.MRP = 0;
                    sl.PurchaseOrg = ri.PurchaseOrg;
                    sl.StartTime = DateTime.Parse(dgvr.Cells["DeliveryStartTime"].Value.ToString());
                    sl.EndTime = DateTime.Parse(dgvr.Cells["DeliveryEndTime"].Value.ToString());
                    sourceListBll.addSourceList(sl);
                    

                    PRSupplier prs = new PRSupplier();
                    prs.PR_ID = tools.systemTimeToStr();
                    prs.Material_ID = ri.MaterialId;
                    prs.Supplier_ID = ri.SupplierId;
                    prs.Number = ri.DemandCount;
                    string tempTab = "dbo.PR_Supplier";
                    StringBuilder ssb = new StringBuilder("insert into " + tempTab + "(PR_ID,Material_ID,Supplier_ID,Number,Finished_Mark) values(@PR_ID,@Material_ID,@Supplier_ID,@Number,@Finished_Mark)");
                    SqlParameter[] sqlParas = new SqlParameter[]
                {
                new SqlParameter("@PR_ID",SqlDbType.VarChar),
                new SqlParameter("@Material_ID",SqlDbType.VarChar),
                new SqlParameter("@Supplier_ID",SqlDbType.VarChar),
                new SqlParameter("@Number",SqlDbType.Int),
                new SqlParameter("@Finished_Mark",SqlDbType.Int)
                };
                    sqlParas[0].Value = prs.PR_ID;
                    sqlParas[1].Value = prs.Material_ID;
                    sqlParas[2].Value = prs.Supplier_ID;
                    sqlParas[3].Value = prs.Number;
                    sqlParas[4].Value = 0;
                    string sqlTextt = ssb.ToString();
                    DBHelper.ExecuteNonQuery(sqlTextt, sqlParas);

                }
            }
            MessageBox.Show("采购信息记录生成成功");
            MessageBox.Show("货源清单记录成功");



        }

        //展示结果
        private void btn_ShowResult_Click(object sender, EventArgs e)
        {
            string sourceId = bid.BidId;
            string supplierId = cmb_ResSupplier.Text;
            int index = MaterialGridview.CurrentRow.Index;
            string materialId = MaterialGridview.Rows[index].Cells[0].Value.ToString();
            List<SourceCondition> listConditions = sourceConditionBLL.getSourceConByBidSpMaId(sourceId, supplierId, materialId);
            conditionGridView.Rows.Clear();
            if (listConditions.Count > conditionGridView.Rows.Count)
            {
                conditionGridView.Rows.Add(listConditions.Count - conditionGridView.Rows.Count);
            }
            for (int i = 0; i < listConditions.Count; i++)
            {
                string conditionId = listConditions.ElementAt(i).ConditionId;
                //根据编号查找对应的条件类型
                conditionGridView.Rows[i].Cells["Num"].Value = listConditions.ElementAt(i).Num;
            }
            List<SourceTimeSection> listTimeSections = sourceTimeSectionBLL.getSouTmsBySouSupMaId(sourceId, supplierId, materialId);
            timeGridView.Rows.Clear();
            if (listTimeSections.Count > timeGridView.Rows.Count)
            {
                timeGridView.Rows.Add(listTimeSections.Count - timeGridView.Rows.Count);
            }
            for (int i = 0; i < listTimeSections.Count; i++)
            {
                timeGridView.Rows[i].Cells["startTime"].Value = listTimeSections.ElementAt(i).StartTime;
                timeGridView.Rows[i].Cells["endTime"].Value = listTimeSections.ElementAt(i).EndTime;
                timeGridView.Rows[i].Cells["timePrice"].Value = listTimeSections.ElementAt(i).Num;
            }
            List<SourceNumberSection> listNumberSections = sourceNumberSectionBLL.getSouNumSecsBySouSupMaId(sourceId, supplierId, materialId);
            numberGridView.Rows.Clear();
            if (listNumberSections.Count > numberGridView.Rows.Count)
            {
                numberGridView.Rows.Add(listNumberSections.Count - numberGridView.Rows.Count);
            }
            for (int i = 0; i < listNumberSections.Count; i++)
            {
                numberGridView.Rows[i].Cells["startNumber"].Value = listNumberSections.ElementAt(i).StartNumber;
                numberGridView.Rows[i].Cells["endNumber"].Value = listNumberSections.ElementAt(i).EndNumber;
                numberGridView.Rows[i].Cells["numberPrice"].Value = listNumberSections.ElementAt(i).Num;
            }
        }

        /// <summary>
        /// 进入谈判页面
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        
        private void btn_Transact_Click(object sender, EventArgs e)
        {
            bid.BidState = 4;
            bidBLL.updateBidState(bid);
            if (this.userUI.showTransact_Form == null || this.userUI.showTransact_Form.IsDisposed)
            {
                this.userUI.showTransact_Form = new ShowTransact_Form();
            }
            this.userUI.showTransact_Form.TopLevel = false;
            this.userUI.showTransact_Form.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.userUI.showTransact_Form.Show(this.userUI.dockPnlForm, DockState.Document);
        }

        /// <summary>
        /// 查看
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button5_Click(object sender, EventArgs e)
        {
            string sid = cmb_AnalySupplier.Text;
            List<SourceSupplier> list = sourceSupplierBLL.getBidsBySupplierId(sid);
            supplierHistoryView.Rows.Clear();
            if (list.Count > supplierHistoryView.Rows.Count)
            {
                supplierHistoryView.Rows.Add(list.Count - supplierHistoryView.Rows.Count);
            }
            for (int i = 0; i < list.Count; i++)
            {
                supplierHistoryView.Rows[i].Cells["bidId"].Value = list.ElementAt(i).Source_ID;
                Source source = sourceBll.findSourceById(list.ElementAt(i).Source_ID);
                supplierHistoryView.Rows[i].Cells["bidName"].Value = source.Source_Name;
                supplierHistoryView.Rows[i].Cells["CreateTime"].Value = source.Create_Time;
                supplierHistoryView.Rows[i].Cells["bidResult"].Value = list.ElementAt(i).State;
            }
        }

        /// <summary>
        /// 比较价格
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_Compare_Click(object sender, EventArgs e)
        {
            string materialId = cmb_MaterialId.Text;
            List<RecordInfo> list = recordInfoBLL.getRecordsByMaterialId(materialId);
            priceGridView.Rows.Clear();
            if (list.Count > priceGridView.Rows.Count)
            {
                priceGridView.Rows.Add(list.Count - priceGridView.Rows.Count);
            }
            for (int i = 0; i < list.Count; i++)
            {
                priceGridView.Rows[i].Cells["recordInfoId"].Value = list.ElementAt(i).RecordInfoId;
                priceGridView.Rows[i].Cells["compareSupplierId"].Value = list.ElementAt(i).SupplierId;
                priceGridView.Rows[i].Cells["compareSupplierName"].Value = list.ElementAt(i).SupplierName;
                priceGridView.Rows[i].Cells["PriceNum"].Value = list.ElementAt(i).NetPrice;
                priceGridView.Rows[i].Cells["priceTime"].Value = list.ElementAt(i).CreatTime;

            }

        }

        /// <summary>
        /// 生成
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_Sure_Click(object sender, EventArgs e)
        {
            if (checkInputValidity())   //检查输入的合法性
            {
                //Frm_SPCompareRadarChartResult frm_SPCompareRadarChartResult = new Frm_SPCompareRadarChartResult(this.saveInterfaceAllValue());
                //frm_SPCompareRadarChartResult.TopLevel = false;
                //frm_SPCompareRadarChartResult.Dock = DockStyle.Fill;
                //frm_SPCompareRadarChartResult.Location = new Point(0, 40);
                //frm_SPCompareRadarChartResult.Show(SPReportGlobalVariable.userUI.dockPnlForm, DockState.Document);
            }
        }

        private bool checkInputValidity()
        {
            //先清除标准选择
            if (standardNameMap != null)
            {
                standardNameMap.Clear();
            }
            Dictionary<string, string> tempStandardNameMap = this.saveSelectedStandardName();
            if (tempStandardNameMap == null || tempStandardNameMap.Count <= 0)
            {
                MessageBox.Show(this,
                                "您还没有选择任何评价标准，请核对！",
                                "评估标准信息警告",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Warning);
                return false;
            }
            //先清空Map信息
            if (supplierMap != null)
            {
                supplierMap.Clear();
            }
            Dictionary<string, string> tempSelectedSupplierMap = saveSelectedSupplierMap();
            if (tempSelectedSupplierMap == null || tempSelectedSupplierMap.Count <= 0)
            {
                MessageBox.Show(this,
                                "您还没有选择任何供应商，请核对！",
                                "供应商信息警告",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Warning);
                return false;
            }
            return true;
        }

        /// <summary>
        /// 保存选择的标准名称
        /// </summary>
        /// <returns></returns>
        private Dictionary<string, string> saveSelectedStandardName()
        {
            //存放选中的标准
            if (this.standardNameMap == null)
            {
                this.standardNameMap = new Dictionary<string, string>();
            }
            //遍历标准选择区的所有被选中的标准
            foreach (Control ctr in this.gb_Standard.Controls)
            {
                //只遍历CheckBox
                if (ctr is CheckBox)
                {
                    CheckBox cb = ctr as CheckBox;
                    if (cb.Checked)
                    {
                        this.standardNameMap.Add(cb.Name, cb.Text);
                    }
                }
            }
            return this.standardNameMap;
        }

        /// <summary>
        /// 保存选择的供应商列表信息
        /// </summary>
        /// <returns></returns>
        private Dictionary<string, string> saveSelectedSupplierMap()
        {
            if (this.dgv_SupplierList.Rows.Count > 0)
            {
                //存放选中的供应商列表
                if (this.supplierMap == null)
                {
                    this.supplierMap = new Dictionary<string, string>();
                }
                //遍历供应商列表
                foreach (DataGridViewRow currentRow in this.dgv_SupplierList.Rows)
                {
                    if (Convert.ToBoolean(currentRow.Cells["sSelection"].Value))  //如果被选中则存入Map中
                    {
                        this.supplierMap.Add(currentRow.Cells["sId"].Value.ToString(), currentRow.Cells["sName"].Value.ToString());
                    }
                }
                return this.supplierMap;
            }
            else
            {
                return null;
            }
        }



        /// <summary>
        /// 查看供应商
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button4_Click(object sender, EventArgs e)
        {
            float all = 0;
            string materialId = cmb_LPMaterialId.Text;
            List<SourceSupplierMaterial> list = sourceSMBll.getSSMBySuIdMId(txt_BidId.Text, materialId);
            supplierGridViewLP.Rows.Clear();
            if (list.Count > supplierGridViewLP.Rows.Count)
            {
                supplierGridViewLP.Rows.Add(list.Count-supplierGridViewLP.Rows.Count);
            }
            for (int i = 0; i < list.Count; i++)
            {
                supplierGridViewLP.Rows[i].Cells["supplierId"].Value = list.ElementAt(i).Supplier_ID;
                int count = sourceMaterialBll.findSMBySIdMId(txt_BidId.Text, materialId).Demand_Count;
                supplierGridViewLP.Rows[i].Cells["supplierCount"].Value = count;
                supplierGridViewLP.Rows[i].Cells["supplierPrice"].Value = list.ElementAt(i).Price;
                float total = list.ElementAt(i).Price * count;
                supplierGridViewLP.Rows[i].Cells["supplierTotal"].Value = total;
                all += total;
            }
            /*
            float max = 0;
            int flag = 0;
            for (int i = 0; i < list.Count; i++)
            {
                double temp = Math.Round(float.Parse(supplierGridViewLP.Rows[i].Cells["supplierTotal"].Value.ToString())*100.0 / all,2);
                if (temp > max)
                {
                    flag = i; 
                }
                supplierGridViewLP.Rows[i].Cells["rate"].Value = temp+"%";
            }
            cmb_LPSupplier.Text = supplierGridViewLP.Rows[flag].Cells["supplierId"].Value.ToString();
             **/
        }

        //保存结果
        private void btn_LowPriceSave_Click(object sender, EventArgs e)
        {
            SourceMaterial sm = sourceMaterialBll.findSMBySIdMId(txt_BidId.Text, cmb_LPMaterialId.Text);
            //将寻源结果信息写入数据表
            SourceResult sr = new SourceResult();
            sr.Item_ID = "TX" + tools.systemTimeToStr();
            sr.Source_ID = bid.BidId;
            sr.Supplier_ID = cmb_LPSupplier.Text;
            sr.Material_ID = cmb_LPMaterialId.Text;
            sr.Material_Name = sm.Material_Name;
            sr.Factory_ID = sm.FactoryId;
            sr.Stock_ID = sm.FactoryId;
            sr.Buy_Number = sm.Demand_Count;
            sourceResultBll.addSourceResult(sr);

            //将结果信息写入采购信息记录表
            RecordInfo ri = new RecordInfo();
            ri.RecordInfoId = tools.systemTimeToStr();//生成一个采购信息记录编号
            ri.SupplierId = cmb_LPSupplier.Text;
            SupplierBase sb = supplierBaseBLL.GetSupplierBasicInformation(ri.SupplierId);
            ri.SupplierName = sb.Supplier_Name;//根据供应商编号查询供应商名称
            Source source = sourceBll.findSourceById(bid.BidId);
            ri.PurchaseOrg = source.PurchaseOrg;
            ri.PurchaseGroup = source.PurchaseGroup;
            ri.MaterialId = cmb_LPMaterialId.Text;
            ri.MaterialName = sm.Material_Name;
            ri.FactoryId = sm.FactoryId;
            ri.StockId = sm.StockId;
            ri.DemandCount = sm.Demand_Count;
            ri.NetPrice = float.Parse(txt_NetPrice.ToString());
            ri.FromType = 1;
            ri.FromId = bid.BidId;
            ri.ContactState = "无效";
            ri.StartTime = sm.DeliveryStartTime;
            ri.EndTime = sm.DeliveryEndTime;
            recordInfoBLL.addRecordInfo(ri);
            MessageBox.Show("采购信息记录添加成功");
            bid.BidState = 6;
            bidBLL.updateBidState(bid);

            //生成货源清单
            SourceList sl = new SourceList();
            sl.SourceListId = "S" + tools.systemTimeToStr();
            sl.Material_ID = ri.MaterialId;
            sl.Supplier_ID = ri.SupplierId;
            sl.Factory_ID = ri.FactoryId;
            if (needSidList.Count == 1)
            {
                sl.Fix = 1;
            }
            else
            {
                sl.Fix = 0;
            }
            sl.Blk = 0;
            sl.MRP = 0;
            sl.PurchaseOrg = ri.PurchaseOrg;
            sl.StartTime = sm.DeliveryStartTime;
            sl.EndTime = sm.DeliveryEndTime;
            sourceListBll.addSourceList(sl);
            MessageBox.Show("货源清单记录成功");
        }

        private void btn_SureSupplier_Click(object sender, EventArgs e)
        {
            needSidList.Clear();
            foreach (DataGridViewRow row in resSupplierGridView.Rows)
            {
                if (row.Cells["num"].Value != null)
                {
                    if (row.Cells["num"].Value.ToString() == "True")
                    {
                        needSidList.Add(row.Cells["resSupplierId"].Value.ToString());
                    }
                }
            }
            if (needSidList.Count == 0)
            {
                MessageBox.Show("还未选择中标的供应商");
                return;
            }
            else
            {
                MessageBox.Show("选择了"+needSidList.Count+"个中标供应商");
            }
            btn_WeightSave.Visible = true;
        }
    }
}
