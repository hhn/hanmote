﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Net.Mail;
//using LumiSoft.Net.POP3.Client;
//using LumiSoft.Net.Mail;
using System.Net.Sockets;
using Aspose.Cells;
using System.Windows.Forms;
using Lib.SqlServerDAL;
using Lib.SqlServerDAL.SourcingManage.ProcurementPlan;
using Lib.Model.SourcingManage.ProcurementPlan;
using Lib.Model.SourcingManage.SourcingManagement;
using Lib.Model.MD.SP;
using Lib.Bll.SourcingManage.SourcingManagement;
using Lib.Bll.MDBll.General;
using MMClient.SourcingManage;
using MMClient.SourcingManage.SourcingManagement;
using WeifenLuo.WinFormsUI.Docking;
using Lib.Model.SourcingManage.SourceingRecord;
using Lib.Bll.SourcingManage.SourceingRecord;
using MMClient.SourcingManage.ProcurementPlan;
using Lib.Bll.ServiceBll;
using Lib.Common.CommonUtils;
using Lib.Common.MMCException.Bll;
using System.Collections;
using Lib.Model.CommonModel;
using SourceMaterial = Lib.Model.ServiceModel.SourceMaterial;
using Lib.Model.ServiceModel;

namespace MMClient.SourcingManage.SourcingManagement
{
    public partial class Inquiry_Form : DockContent
    { 
        //业务对象
        ServiceBill serviceBill = new ServiceBill();

        //物料编号列表
        private ISet<string> wlbhSet = new HashSet<string>();

        private SummaryDemand summaryDemand = null;


        public Inquiry_Form() 
        {
            InitializeComponent();
            Init();
        }

        public Inquiry_Form(string Demand_ID)
        {
            InitializeComponent();
            Init(Demand_ID);
        }

        private void Init(string Demand_ID)
        {
            Init();
            SummaryDemand SummaryDemand = serviceBill.GetSummaryDemandById(Demand_ID);
            this.summaryDemand = SummaryDemand;
            string factoryId = SummaryDemand.Factory_ID;
            //获取采购组织Id
            string purOrgId = serviceBill.GetPurOrgIdByFactoryId(factoryId);
            //采购组织名称
            string purOrgName = serviceBill.GetPurOrgNameById(purOrgId);
            this.PurOrgText.Text = purOrgName;

            //获取采购组
            List<string> buyGroupNameList = serviceBill.GetAllBuyGroupByPurId(purOrgId);
            FormUtils.FillCombox(this.BuyGroupCB,buyGroupNameList);

            this.textBox_BuyerName.Text = SummaryDemand.Proposer_ID;
            this.DemandCountText.Text = Convert.ToString(SummaryDemand.Demand_Count);
            AddMaterial(SummaryDemand.Material_Name);
        }

        //刷新所接洽的供应商列表
        public void FreshSupplierList(List<string> supplierList)
        {
            if(supplierList != null && supplierList.Count > 0)
            {
                DataTable dt = serviceBill.GetAllSupplierByList(supplierList);
                this.SupplierDGV.DataSource = dt;
            }
        }

        //初始化询价单号和询价状态
        private void Init()
        {
            //自动生成询价ID（寻源ID）
            string Source_ID = System.Guid.NewGuid().ToString("N");
            this.txt_inquiryId.Text = Source_ID;
            //询价单刚创建的初始状态
            this.txt_state.Text = "待审核";
        }

        //添加物料
        public void AddMaterial(string MaterialId)
        {
            this.wlbhSet.Add(MaterialId);
            this.LoadMaterialList();
        }

        //判断该物料是否已存在
        public bool IsContains(string MaterialId)
        {
            return this.wlbhSet.Contains(MaterialId);
        }

        /// <summary>
        /// 加载待询价的物料信息
        /// </summary>
        private void LoadMaterialList()
        {
            if(this.wlbhSet.Count > 0)
            {
                DataTable dt = serviceBill.GetMaterialByMaterialIdList(wlbhSet);
                this.MaterialListDGV.DataSource = dt;
            }
        }


        //创建询价按钮
        private void scxj_button_Click_1(object sender, EventArgs e)
        {
            //创建询价记录，并插入数据库  Source表、Source_Inquiry询价详细信息表
            //询价单号
            string Source_ID = this.txt_inquiryId.Text;
            //询价名称
            string Inquiry_Name = this.txt_InquiryName.Text;
            //询价状态
            string State = this.txt_state.Text;
            //创建时间
            string CreateTimeStr = this.dateTimePicker_CreateTime.Text;
            DateTime CreateTime = Convert.ToDateTime(CreateTimeStr);
            //采购组织
            string Purchase_Org = this.PurOrgText.Text;
            //采购组
            string Purchase_Group = this.BuyGroupCB.SelectedItem.ToString();
            //报价时间
            string OfferTimeStr = this.dte_OfferTime.Text;
            DateTime Offer_Time = Convert.ToDateTime(OfferTimeStr);
            //申请人
            string buyer_Name = this.textBox_BuyerName.Text;
            InqueryPrice inqueryPrice = new InqueryPrice(Source_ID, Purchase_Org, buyer_Name, CreateTime, Offer_Time, State, Purchase_Group, Inquiry_Name,null,0);
      
            //保存询价单
            //serviceBill.SaveInqueryPrice(inqueryPrice);
        }

        //确定接洽的供应商按钮
        private void SelectSupplierBtn_Click(object sender, EventArgs e)
        {
            //DialogResult dialogResult = MessageUtil.ShowYesNoCancelAndTips("是否采用系统定义的规则自动确定接洽的供应商？点击是表示将进入自动确定接洽的供应商界面，点击否表示将进入手动确定接洽的供应商界面。");
            //if(dialogResult == DialogResult.OK)
            //{
            //    //采用自动

            //}else if(dialogResult == DialogResult.No)
            //{
                
            //}
            //采用手动
            SelectSupplier selectSupplier = new SelectSupplier(this);
            selectSupplier.Show();
        }

        //清空按钮
        private void ClearBtn_Click(object sender, EventArgs e)
        {
            int totalRow = this.SupplierDGV.RowCount;
            for(int i = totalRow-1;i >= 0; i--)
            {
                this.SupplierDGV.Rows.RemoveAt(i);
            }
        }

        //创建询价按钮
        private void CreateInqueryPriceBtn_Click(object sender, EventArgs e)
        {
            try
            {
                //保存数据到询价表中  Source_Inquiry(询价详细信息表)  Source_Material(寻源物料表)
                string Source_ID = this.txt_inquiryId.Text;
                string Purchase_Org = this.PurOrgText.Text;
                string Buyer_Name = this.textBox_BuyerName.Text;
                string Offer_TimeStr = this.dte_OfferTime.Text;
                DateTime Offer_Time = Convert.ToDateTime(Offer_TimeStr);
                string State = this.txt_state.Text;
                string Purchase_Group = this.BuyGroupCB.SelectedItem.ToString();
                string Inquiry_Name = this.txt_InquiryName.Text;
                string CreateTimeStr = this.dateTimePicker_CreateTime.Text;
                DateTime CreateTime = Convert.ToDateTime(CreateTimeStr);
                //此处为测试，物料定位接口开发完毕再做更改
                string[] arr = { "一般物料","杠杆物料","瓶颈物料","战略物料"};
                Random random = new Random();
                InqueryPrice inqueryPrice = new InqueryPrice
                {
                    Source_ID = Source_ID,
                    Purchase_Org = Purchase_Org,
                    Buyer_Name = Buyer_Name,
                    Offer_Time = Offer_Time,
                    State = State,
                    Purchase_Group = Purchase_Group,
                    Inquiry_Name = Inquiry_Name,
                    CreateTime = CreateTime,
                    Material_Location = arr[random.Next(0,3)],
                    Approve_Count = 0
                };
                //保存询价详细信息
                serviceBill.SaveInqueryPrice(inqueryPrice);

                //保存询价物料信息 Source_Material
                DataGridViewRow curRow = this.MaterialListDGV.CurrentRow;
                if(curRow != null)
                {
                    string Material_ID = curRow.Cells["Material_ID"].Value.ToString();
                    string Material_Name = curRow.Cells["Material_Name"].Value.ToString();
                    string Material_Group = curRow.Cells["Material_Group"].Value.ToString();
                    string Demand_CountStr = this.DemandCountText.Text;
                    int Demand_Count = Convert.ToInt32(Demand_CountStr);
                    string Demand_ID = this.summaryDemand.Demand_ID;
                    string Unit = curRow.Cells["measurement"].Value.ToString();
                    string Factory_ID = summaryDemand.Factory_ID;
                    DateTime DeliveryStartTime = summaryDemand.Begin_Time;
                    DateTime DeliveryEndTime = summaryDemand.Delivery_Date;
                    SourceMaterial sourceMaterial = new SourceMaterial
                    {
                        Material_ID = Material_ID,
                        Material_Name = Material_Name,
                        Demand_Count = Demand_Count,
                        Demand_ID = Demand_ID,
                        Unit = Unit,
                        Factory_ID = Factory_ID,
                        Source_ID = Source_ID
                    };
                    serviceBill.SaveSourceMaterial(sourceMaterial);
                }

                MessageUtil.ShowTips("保存成功！");
            }
            catch (BllException ex)
            {
                MessageUtil.ShowError(ex.Msg);
            }
        }

        //关闭按钮
        private void CloseBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
