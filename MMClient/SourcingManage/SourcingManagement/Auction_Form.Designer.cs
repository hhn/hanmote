﻿namespace MMClient.SourcingManage.SourcingManagement
{
    partial class Auction_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Auction_Form));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            this.xjsj_dt = new System.Windows.Forms.DateTimePicker();
            this.xjdh_cmb = new System.Windows.Forms.ComboBox();
            this.cgdh_cmb = new System.Windows.Forms.ComboBox();
            this.label32 = new System.Windows.Forms.Label();
            this.cgdh_lbl = new System.Windows.Forms.Label();
            this.xjdh_lbl = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.close_lbl = new System.Windows.Forms.Label();
            this.close_pb = new System.Windows.Forms.PictureBox();
            this.read_lbl = new System.Windows.Forms.Label();
            this.edit_lbl = new System.Windows.Forms.Label();
            this.read_pb = new System.Windows.Forms.PictureBox();
            this.edit_pb = new System.Windows.Forms.PictureBox();
            this.activate_tb = new System.Windows.Forms.TextBox();
            this.ckcg_button = new System.Windows.Forms.Button();
            this.cgzz_cmb = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cgz_cmb = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.bjqx_dt = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.Supplier_view = new System.Windows.Forms.DataGridView();
            this.Supplier_view0 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Supplier_view1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Supplier_view2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Supplier_view3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Supplier_view4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Supplier_view5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Supplier_view6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Supplier_view7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Supplier_view8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label23 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.send_button = new System.Windows.Forms.Button();
            this.xjzt_cmb = new System.Windows.Forms.ComboBox();
            this.Inquiry_view = new System.Windows.Forms.DataGridView();
            this.Inquiry_view0 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Inquiry_view1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Inquiry_view2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Inquiry_view3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Inquiry_view4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Inquiry_view11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Inquiry_view5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Inquiry_view6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Inquiry_view7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Inquiry_view8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Inquiry_view9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Inquiry_view12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Inquiry_view13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Inquiry_view10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.scxj_button = new System.Windows.Forms.Button();
            this.ckxj_button = new System.Windows.Forms.Button();
            this.saveOfferButton = new System.Windows.Forms.Button();
            this.dataGridViewComboBoxColumn1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn2 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn3 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn4 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn5 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn6 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.compare_panel = new System.Windows.Forms.Panel();
            this.button2 = new System.Windows.Forms.Button();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.qtyq_rtb = new System.Windows.Forms.RichTextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.comboBox5 = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.comboBox4 = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.comboBox3 = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.compare_View = new System.Windows.Forms.DataGridView();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.close_pb)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.read_pb)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.edit_pb)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Supplier_view)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Inquiry_view)).BeginInit();
            this.compare_panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.compare_View)).BeginInit();
            this.SuspendLayout();
            // 
            // xjsj_dt
            // 
            this.xjsj_dt.CustomFormat = "yyyy-MM-dd HH:mm";
            this.xjsj_dt.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xjsj_dt.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.xjsj_dt.Location = new System.Drawing.Point(439, 48);
            this.xjsj_dt.Name = "xjsj_dt";
            this.xjsj_dt.Size = new System.Drawing.Size(180, 23);
            this.xjsj_dt.TabIndex = 166;
            // 
            // xjdh_cmb
            // 
            this.xjdh_cmb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xjdh_cmb.Location = new System.Drawing.Point(65, 9);
            this.xjdh_cmb.Name = "xjdh_cmb";
            this.xjdh_cmb.Size = new System.Drawing.Size(180, 22);
            this.xjdh_cmb.TabIndex = 163;
            this.xjdh_cmb.SelectedIndexChanged += new System.EventHandler(this.xjdh_cmb_SelectedIndexChanged);
            // 
            // cgdh_cmb
            // 
            this.cgdh_cmb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.cgdh_cmb.FormattingEnabled = true;
            this.cgdh_cmb.Location = new System.Drawing.Point(682, 10);
            this.cgdh_cmb.Name = "cgdh_cmb";
            this.cgdh_cmb.Size = new System.Drawing.Size(180, 22);
            this.cgdh_cmb.TabIndex = 159;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("宋体", 9F);
            this.label32.Location = new System.Drawing.Point(355, 52);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(77, 12);
            this.label32.TabIndex = 157;
            this.label32.Text = "拍卖开始时间";
            // 
            // cgdh_lbl
            // 
            this.cgdh_lbl.AutoSize = true;
            this.cgdh_lbl.Font = new System.Drawing.Font("宋体", 9F);
            this.cgdh_lbl.Location = new System.Drawing.Point(622, 14);
            this.cgdh_lbl.Name = "cgdh_lbl";
            this.cgdh_lbl.Size = new System.Drawing.Size(53, 12);
            this.cgdh_lbl.TabIndex = 156;
            this.cgdh_lbl.Text = "采购单号";
            // 
            // xjdh_lbl
            // 
            this.xjdh_lbl.AutoSize = true;
            this.xjdh_lbl.Font = new System.Drawing.Font("宋体", 9F);
            this.xjdh_lbl.Location = new System.Drawing.Point(5, 14);
            this.xjdh_lbl.Name = "xjdh_lbl";
            this.xjdh_lbl.Size = new System.Drawing.Size(53, 12);
            this.xjdh_lbl.TabIndex = 155;
            this.xjdh_lbl.Text = "拍卖单号";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.Control;
            this.panel1.Controls.Add(this.close_lbl);
            this.panel1.Controls.Add(this.close_pb);
            this.panel1.Controls.Add(this.read_lbl);
            this.panel1.Controls.Add(this.edit_lbl);
            this.panel1.Controls.Add(this.read_pb);
            this.panel1.Controls.Add(this.edit_pb);
            this.panel1.Controls.Add(this.activate_tb);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(622, 63);
            this.panel1.TabIndex = 167;
            // 
            // close_lbl
            // 
            this.close_lbl.AutoSize = true;
            this.close_lbl.Font = new System.Drawing.Font("宋体", 9F);
            this.close_lbl.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.close_lbl.Location = new System.Drawing.Point(95, 48);
            this.close_lbl.Name = "close_lbl";
            this.close_lbl.Size = new System.Drawing.Size(29, 12);
            this.close_lbl.TabIndex = 106;
            this.close_lbl.Text = "关闭";
            // 
            // close_pb
            // 
            this.close_pb.Image = ((System.Drawing.Image)(resources.GetObject("close_pb.Image")));
            this.close_pb.Location = new System.Drawing.Point(89, 5);
            this.close_pb.Name = "close_pb";
            this.close_pb.Size = new System.Drawing.Size(40, 40);
            this.close_pb.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.close_pb.TabIndex = 105;
            this.close_pb.TabStop = false;
            this.close_pb.Click += new System.EventHandler(this.close_pb_Click);
            // 
            // read_lbl
            // 
            this.read_lbl.AutoSize = true;
            this.read_lbl.Font = new System.Drawing.Font("宋体", 9F);
            this.read_lbl.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.read_lbl.Location = new System.Drawing.Point(53, 48);
            this.read_lbl.Name = "read_lbl";
            this.read_lbl.Size = new System.Drawing.Size(29, 12);
            this.read_lbl.TabIndex = 102;
            this.read_lbl.Text = "拍卖";
            // 
            // edit_lbl
            // 
            this.edit_lbl.AutoSize = true;
            this.edit_lbl.Font = new System.Drawing.Font("宋体", 9F);
            this.edit_lbl.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.edit_lbl.Location = new System.Drawing.Point(9, 48);
            this.edit_lbl.Name = "edit_lbl";
            this.edit_lbl.Size = new System.Drawing.Size(29, 12);
            this.edit_lbl.TabIndex = 97;
            this.edit_lbl.Text = "刷新";
            // 
            // read_pb
            // 
            this.read_pb.Image = ((System.Drawing.Image)(resources.GetObject("read_pb.Image")));
            this.read_pb.Location = new System.Drawing.Point(47, 5);
            this.read_pb.Name = "read_pb";
            this.read_pb.Size = new System.Drawing.Size(40, 40);
            this.read_pb.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.read_pb.TabIndex = 92;
            this.read_pb.TabStop = false;
            this.read_pb.Click += new System.EventHandler(this.read_pb_Click);
            // 
            // edit_pb
            // 
            this.edit_pb.Image = ((System.Drawing.Image)(resources.GetObject("edit_pb.Image")));
            this.edit_pb.Location = new System.Drawing.Point(5, 5);
            this.edit_pb.Name = "edit_pb";
            this.edit_pb.Size = new System.Drawing.Size(40, 40);
            this.edit_pb.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.edit_pb.TabIndex = 87;
            this.edit_pb.TabStop = false;
            this.edit_pb.Click += new System.EventHandler(this.edit_pb_Click);
            // 
            // activate_tb
            // 
            this.activate_tb.BackColor = System.Drawing.SystemColors.MenuBar;
            this.activate_tb.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.activate_tb.ForeColor = System.Drawing.SystemColors.MenuBar;
            this.activate_tb.Location = new System.Drawing.Point(7, 5);
            this.activate_tb.Name = "activate_tb";
            this.activate_tb.Size = new System.Drawing.Size(100, 14);
            this.activate_tb.TabIndex = 107;
            // 
            // ckcg_button
            // 
            this.ckcg_button.ForeColor = System.Drawing.Color.LightSeaGreen;
            this.ckcg_button.Location = new System.Drawing.Point(532, 10);
            this.ckcg_button.Name = "ckcg_button";
            this.ckcg_button.Size = new System.Drawing.Size(63, 23);
            this.ckcg_button.TabIndex = 168;
            this.ckcg_button.Text = "参考采购";
            this.ckcg_button.UseVisualStyleBackColor = true;
            this.ckcg_button.Click += new System.EventHandler(this.ckcg_button_Click);
            // 
            // cgzz_cmb
            // 
            this.cgzz_cmb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.cgzz_cmb.FormattingEnabled = true;
            this.cgzz_cmb.Location = new System.Drawing.Point(65, 48);
            this.cgzz_cmb.Name = "cgzz_cmb";
            this.cgzz_cmb.Size = new System.Drawing.Size(100, 22);
            this.cgzz_cmb.TabIndex = 170;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("宋体", 9F);
            this.label1.Location = new System.Drawing.Point(5, 52);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 169;
            this.label1.Text = "采购组织";
            // 
            // cgz_cmb
            // 
            this.cgz_cmb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.cgz_cmb.FormattingEnabled = true;
            this.cgz_cmb.Location = new System.Drawing.Point(232, 48);
            this.cgz_cmb.Name = "cgz_cmb";
            this.cgz_cmb.Size = new System.Drawing.Size(100, 22);
            this.cgz_cmb.TabIndex = 172;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("宋体", 9F);
            this.label2.Location = new System.Drawing.Point(185, 53);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 12);
            this.label2.TabIndex = 171;
            this.label2.Text = "采购组";
            // 
            // bjqx_dt
            // 
            this.bjqx_dt.CustomFormat = "yyyy-MM-dd HH:mm";
            this.bjqx_dt.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.bjqx_dt.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.bjqx_dt.Location = new System.Drawing.Point(730, 45);
            this.bjqx_dt.Name = "bjqx_dt";
            this.bjqx_dt.Size = new System.Drawing.Size(180, 23);
            this.bjqx_dt.TabIndex = 174;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("宋体", 9F);
            this.label4.Location = new System.Drawing.Point(643, 52);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(77, 12);
            this.label4.TabIndex = 173;
            this.label4.Text = "拍卖结束时间";
            // 
            // Supplier_view
            // 
            this.Supplier_view.BackgroundColor = System.Drawing.Color.White;
            this.Supplier_view.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Supplier_view.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Supplier_view0,
            this.Supplier_view1,
            this.Supplier_view2,
            this.Supplier_view3,
            this.Supplier_view4,
            this.Supplier_view5,
            this.Supplier_view6,
            this.Supplier_view7,
            this.Supplier_view8,
            this.Column1,
            this.Column2});
            this.Supplier_view.Location = new System.Drawing.Point(7, 454);
            this.Supplier_view.Name = "Supplier_view";
            this.Supplier_view.RowHeadersVisible = false;
            this.Supplier_view.RowTemplate.Height = 23;
            this.Supplier_view.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.Supplier_view.Size = new System.Drawing.Size(1000, 240);
            this.Supplier_view.TabIndex = 175;
            this.Supplier_view.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.Supplier_view_CellEndEdit);
            this.Supplier_view.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.Supplier_view_CellValueChanged);
            this.Supplier_view.Click += new System.EventHandler(this.Supplier_view_Click);
            // 
            // Supplier_view0
            // 
            dataGridViewCellStyle1.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Supplier_view0.DefaultCellStyle = dataGridViewCellStyle1;
            this.Supplier_view0.Frozen = true;
            this.Supplier_view0.HeaderText = "num";
            this.Supplier_view0.Name = "Supplier_view0";
            this.Supplier_view0.ReadOnly = true;
            this.Supplier_view0.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Supplier_view0.Width = 30;
            // 
            // Supplier_view1
            // 
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.White;
            this.Supplier_view1.DefaultCellStyle = dataGridViewCellStyle2;
            this.Supplier_view1.Frozen = true;
            this.Supplier_view1.HeaderText = "供应商编号";
            this.Supplier_view1.Name = "Supplier_view1";
            this.Supplier_view1.ReadOnly = true;
            this.Supplier_view1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // Supplier_view2
            // 
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.White;
            this.Supplier_view2.DefaultCellStyle = dataGridViewCellStyle3;
            this.Supplier_view2.Frozen = true;
            this.Supplier_view2.HeaderText = "供应商名称";
            this.Supplier_view2.Name = "Supplier_view2";
            this.Supplier_view2.ReadOnly = true;
            this.Supplier_view2.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Supplier_view2.Width = 120;
            // 
            // Supplier_view3
            // 
            this.Supplier_view3.Frozen = true;
            this.Supplier_view3.HeaderText = "联系电话";
            this.Supplier_view3.Name = "Supplier_view3";
            this.Supplier_view3.ReadOnly = true;
            this.Supplier_view3.Width = 95;
            // 
            // Supplier_view4
            // 
            this.Supplier_view4.Frozen = true;
            this.Supplier_view4.HeaderText = "电子信箱";
            this.Supplier_view4.Name = "Supplier_view4";
            this.Supplier_view4.ReadOnly = true;
            // 
            // Supplier_view5
            // 
            this.Supplier_view5.Frozen = true;
            this.Supplier_view5.HeaderText = "国家";
            this.Supplier_view5.Name = "Supplier_view5";
            this.Supplier_view5.ReadOnly = true;
            this.Supplier_view5.Width = 60;
            // 
            // Supplier_view6
            // 
            this.Supplier_view6.Frozen = true;
            this.Supplier_view6.HeaderText = "地址";
            this.Supplier_view6.Name = "Supplier_view6";
            this.Supplier_view6.ReadOnly = true;
            this.Supplier_view6.Width = 130;
            // 
            // Supplier_view7
            // 
            this.Supplier_view7.Frozen = true;
            this.Supplier_view7.HeaderText = "门户";
            this.Supplier_view7.Name = "Supplier_view7";
            this.Supplier_view7.ReadOnly = true;
            this.Supplier_view7.Width = 120;
            // 
            // Supplier_view8
            // 
            this.Supplier_view8.Frozen = true;
            this.Supplier_view8.HeaderText = "报价单";
            this.Supplier_view8.Name = "Supplier_view8";
            this.Supplier_view8.ReadOnly = true;
            this.Supplier_view8.Width = 80;
            // 
            // Column1
            // 
            this.Column1.Frozen = true;
            this.Column1.HeaderText = "支付条款";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 80;
            // 
            // Column2
            // 
            this.Column2.Frozen = true;
            this.Column2.HeaderText = "条件类型";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 80;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("宋体", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label23.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label23.Location = new System.Drawing.Point(7, 158);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(55, 15);
            this.label23.TabIndex = 176;
            this.label23.Text = "询价项";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("宋体", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label6.Location = new System.Drawing.Point(7, 432);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(87, 15);
            this.label6.TabIndex = 178;
            this.label6.Text = "供应商列表";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.send_button);
            this.panel2.Controls.Add(this.xjzt_cmb);
            this.panel2.Controls.Add(this.xjdh_lbl);
            this.panel2.Controls.Add(this.cgdh_lbl);
            this.panel2.Controls.Add(this.label32);
            this.panel2.Controls.Add(this.cgdh_cmb);
            this.panel2.Controls.Add(this.bjqx_dt);
            this.panel2.Controls.Add(this.xjdh_cmb);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.xjsj_dt);
            this.panel2.Controls.Add(this.cgz_cmb);
            this.panel2.Controls.Add(this.ckcg_button);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.cgzz_cmb);
            this.panel2.Location = new System.Drawing.Point(0, 60);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(942, 84);
            this.panel2.TabIndex = 180;
            // 
            // send_button
            // 
            this.send_button.ForeColor = System.Drawing.Color.LightSeaGreen;
            this.send_button.Location = new System.Drawing.Point(357, 9);
            this.send_button.Name = "send_button";
            this.send_button.Size = new System.Drawing.Size(63, 23);
            this.send_button.TabIndex = 178;
            this.send_button.Text = "发送邀请";
            this.send_button.UseVisualStyleBackColor = true;
            this.send_button.Visible = false;
            this.send_button.Click += new System.EventHandler(this.send_button_Click);
            // 
            // xjzt_cmb
            // 
            this.xjzt_cmb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xjzt_cmb.FormattingEnabled = true;
            this.xjzt_cmb.Items.AddRange(new object[] {
            "待比价",
            "已比价",
            "已完成"});
            this.xjzt_cmb.Location = new System.Drawing.Point(257, 9);
            this.xjzt_cmb.Name = "xjzt_cmb";
            this.xjzt_cmb.Size = new System.Drawing.Size(75, 22);
            this.xjzt_cmb.TabIndex = 177;
            this.xjzt_cmb.SelectedIndexChanged += new System.EventHandler(this.xjzt_cmb_SelectedIndexChanged);
            // 
            // Inquiry_view
            // 
            this.Inquiry_view.BackgroundColor = System.Drawing.Color.White;
            this.Inquiry_view.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Inquiry_view.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Inquiry_view0,
            this.Inquiry_view1,
            this.Inquiry_view2,
            this.Inquiry_view3,
            this.Inquiry_view4,
            this.Inquiry_view11,
            this.Inquiry_view5,
            this.Inquiry_view6,
            this.Inquiry_view7,
            this.Inquiry_view8,
            this.Inquiry_view9,
            this.Inquiry_view12,
            this.Inquiry_view13,
            this.Inquiry_view10});
            this.Inquiry_view.Location = new System.Drawing.Point(7, 180);
            this.Inquiry_view.Name = "Inquiry_view";
            this.Inquiry_view.RowHeadersVisible = false;
            this.Inquiry_view.RowTemplate.Height = 23;
            this.Inquiry_view.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.Inquiry_view.Size = new System.Drawing.Size(1000, 228);
            this.Inquiry_view.TabIndex = 181;
            this.Inquiry_view.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.Inquiry_view_CellEndEdit);
            this.Inquiry_view.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.Inquiry_view_CellValueChanged);
            // 
            // Inquiry_view0
            // 
            this.Inquiry_view0.Frozen = true;
            this.Inquiry_view0.HeaderText = "num";
            this.Inquiry_view0.Name = "Inquiry_view0";
            this.Inquiry_view0.ReadOnly = true;
            this.Inquiry_view0.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Inquiry_view0.Width = 30;
            // 
            // Inquiry_view1
            // 
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.White;
            this.Inquiry_view1.DefaultCellStyle = dataGridViewCellStyle4;
            this.Inquiry_view1.Frozen = true;
            this.Inquiry_view1.HeaderText = "物料编号";
            this.Inquiry_view1.Name = "Inquiry_view1";
            this.Inquiry_view1.ReadOnly = true;
            this.Inquiry_view1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // Inquiry_view2
            // 
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.White;
            this.Inquiry_view2.DefaultCellStyle = dataGridViewCellStyle5;
            this.Inquiry_view2.Frozen = true;
            this.Inquiry_view2.HeaderText = "物料名称";
            this.Inquiry_view2.Name = "Inquiry_view2";
            this.Inquiry_view2.ReadOnly = true;
            this.Inquiry_view2.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // Inquiry_view3
            // 
            this.Inquiry_view3.Frozen = true;
            this.Inquiry_view3.HeaderText = "物料标准";
            this.Inquiry_view3.Name = "Inquiry_view3";
            this.Inquiry_view3.ReadOnly = true;
            this.Inquiry_view3.Width = 80;
            // 
            // Inquiry_view4
            // 
            this.Inquiry_view4.Frozen = true;
            this.Inquiry_view4.HeaderText = "数量";
            this.Inquiry_view4.Name = "Inquiry_view4";
            this.Inquiry_view4.ReadOnly = true;
            this.Inquiry_view4.Width = 60;
            // 
            // Inquiry_view11
            // 
            this.Inquiry_view11.Frozen = true;
            this.Inquiry_view11.HeaderText = "价格";
            this.Inquiry_view11.Name = "Inquiry_view11";
            this.Inquiry_view11.Width = 60;
            // 
            // Inquiry_view5
            // 
            this.Inquiry_view5.Frozen = true;
            this.Inquiry_view5.HeaderText = "单位";
            this.Inquiry_view5.Name = "Inquiry_view5";
            this.Inquiry_view5.ReadOnly = true;
            this.Inquiry_view5.Width = 60;
            // 
            // Inquiry_view6
            // 
            this.Inquiry_view6.Frozen = true;
            this.Inquiry_view6.HeaderText = "交货日期";
            this.Inquiry_view6.Name = "Inquiry_view6";
            this.Inquiry_view6.Width = 95;
            // 
            // Inquiry_view7
            // 
            this.Inquiry_view7.Frozen = true;
            this.Inquiry_view7.HeaderText = "物料组";
            this.Inquiry_view7.Name = "Inquiry_view7";
            this.Inquiry_view7.ReadOnly = true;
            this.Inquiry_view7.Width = 70;
            // 
            // Inquiry_view8
            // 
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.White;
            this.Inquiry_view8.DefaultCellStyle = dataGridViewCellStyle6;
            this.Inquiry_view8.Frozen = true;
            this.Inquiry_view8.HeaderText = "工厂";
            this.Inquiry_view8.Name = "Inquiry_view8";
            this.Inquiry_view8.ReadOnly = true;
            this.Inquiry_view8.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Inquiry_view8.Width = 60;
            // 
            // Inquiry_view9
            // 
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.White;
            this.Inquiry_view9.DefaultCellStyle = dataGridViewCellStyle7;
            this.Inquiry_view9.Frozen = true;
            this.Inquiry_view9.HeaderText = "仓库";
            this.Inquiry_view9.Name = "Inquiry_view9";
            this.Inquiry_view9.ReadOnly = true;
            this.Inquiry_view9.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Inquiry_view9.Width = 60;
            // 
            // Inquiry_view12
            // 
            this.Inquiry_view12.Frozen = true;
            this.Inquiry_view12.HeaderText = "支付条款";
            this.Inquiry_view12.Name = "Inquiry_view12";
            this.Inquiry_view12.Width = 80;
            // 
            // Inquiry_view13
            // 
            this.Inquiry_view13.Frozen = true;
            this.Inquiry_view13.HeaderText = "条件类型";
            this.Inquiry_view13.Name = "Inquiry_view13";
            this.Inquiry_view13.Width = 80;
            // 
            // Inquiry_view10
            // 
            this.Inquiry_view10.Frozen = true;
            this.Inquiry_view10.HeaderText = "状态";
            this.Inquiry_view10.Name = "Inquiry_view10";
            this.Inquiry_view10.ReadOnly = true;
            this.Inquiry_view10.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Inquiry_view10.Width = 60;
            // 
            // scxj_button
            // 
            this.scxj_button.ForeColor = System.Drawing.Color.LightSeaGreen;
            this.scxj_button.Location = new System.Drawing.Point(68, 154);
            this.scxj_button.Name = "scxj_button";
            this.scxj_button.Size = new System.Drawing.Size(63, 23);
            this.scxj_button.TabIndex = 192;
            this.scxj_button.Text = "生成询价";
            this.scxj_button.UseVisualStyleBackColor = true;
            this.scxj_button.Visible = false;
            this.scxj_button.Click += new System.EventHandler(this.scxj_button_Click);
            // 
            // ckxj_button
            // 
            this.ckxj_button.ForeColor = System.Drawing.Color.LightSeaGreen;
            this.ckxj_button.Location = new System.Drawing.Point(137, 154);
            this.ckxj_button.Name = "ckxj_button";
            this.ckxj_button.Size = new System.Drawing.Size(63, 23);
            this.ckxj_button.TabIndex = 193;
            this.ckxj_button.Text = "查看询价";
            this.ckxj_button.UseVisualStyleBackColor = true;
            this.ckxj_button.Visible = false;
            this.ckxj_button.Click += new System.EventHandler(this.ckxj_button_Click);
            // 
            // saveOfferButton
            // 
            this.saveOfferButton.ForeColor = System.Drawing.Color.LightSeaGreen;
            this.saveOfferButton.Location = new System.Drawing.Point(944, 154);
            this.saveOfferButton.Name = "saveOfferButton";
            this.saveOfferButton.Size = new System.Drawing.Size(63, 23);
            this.saveOfferButton.TabIndex = 194;
            this.saveOfferButton.Text = "批量保存";
            this.saveOfferButton.UseVisualStyleBackColor = true;
            this.saveOfferButton.Click += new System.EventHandler(this.saveOfferButton_Click);
            // 
            // dataGridViewComboBoxColumn1
            // 
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.Color.White;
            this.dataGridViewComboBoxColumn1.DefaultCellStyle = dataGridViewCellStyle8;
            this.dataGridViewComboBoxColumn1.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn1.Frozen = true;
            this.dataGridViewComboBoxColumn1.HeaderText = "供应商编号";
            this.dataGridViewComboBoxColumn1.Name = "dataGridViewComboBoxColumn1";
            this.dataGridViewComboBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewComboBoxColumn1.Width = 80;
            // 
            // dataGridViewComboBoxColumn2
            // 
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.White;
            this.dataGridViewComboBoxColumn2.DefaultCellStyle = dataGridViewCellStyle9;
            this.dataGridViewComboBoxColumn2.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn2.Frozen = true;
            this.dataGridViewComboBoxColumn2.HeaderText = "供应商名称";
            this.dataGridViewComboBoxColumn2.Name = "dataGridViewComboBoxColumn2";
            this.dataGridViewComboBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewComboBoxColumn2.Width = 160;
            // 
            // dataGridViewComboBoxColumn3
            // 
            dataGridViewCellStyle10.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.Color.White;
            this.dataGridViewComboBoxColumn3.DefaultCellStyle = dataGridViewCellStyle10;
            this.dataGridViewComboBoxColumn3.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn3.Frozen = true;
            this.dataGridViewComboBoxColumn3.HeaderText = "物料编号";
            this.dataGridViewComboBoxColumn3.Name = "dataGridViewComboBoxColumn3";
            this.dataGridViewComboBoxColumn3.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewComboBoxColumn3.Width = 140;
            // 
            // dataGridViewComboBoxColumn4
            // 
            dataGridViewCellStyle11.BackColor = System.Drawing.Color.White;
            this.dataGridViewComboBoxColumn4.DefaultCellStyle = dataGridViewCellStyle11;
            this.dataGridViewComboBoxColumn4.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn4.Frozen = true;
            this.dataGridViewComboBoxColumn4.HeaderText = "物料名称";
            this.dataGridViewComboBoxColumn4.Name = "dataGridViewComboBoxColumn4";
            this.dataGridViewComboBoxColumn4.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewComboBoxColumn4.Width = 140;
            // 
            // dataGridViewComboBoxColumn5
            // 
            dataGridViewCellStyle12.BackColor = System.Drawing.Color.White;
            this.dataGridViewComboBoxColumn5.DefaultCellStyle = dataGridViewCellStyle12;
            this.dataGridViewComboBoxColumn5.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn5.Frozen = true;
            this.dataGridViewComboBoxColumn5.HeaderText = "工厂";
            this.dataGridViewComboBoxColumn5.Name = "dataGridViewComboBoxColumn5";
            this.dataGridViewComboBoxColumn5.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewComboBoxColumn5.Width = 80;
            // 
            // dataGridViewComboBoxColumn6
            // 
            dataGridViewCellStyle13.BackColor = System.Drawing.Color.White;
            this.dataGridViewComboBoxColumn6.DefaultCellStyle = dataGridViewCellStyle13;
            this.dataGridViewComboBoxColumn6.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn6.Frozen = true;
            this.dataGridViewComboBoxColumn6.HeaderText = "仓库";
            this.dataGridViewComboBoxColumn6.Name = "dataGridViewComboBoxColumn6";
            this.dataGridViewComboBoxColumn6.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewComboBoxColumn6.Width = 80;
            // 
            // compare_panel
            // 
            this.compare_panel.Controls.Add(this.button2);
            this.compare_panel.Controls.Add(this.richTextBox1);
            this.compare_panel.Controls.Add(this.qtyq_rtb);
            this.compare_panel.Controls.Add(this.label12);
            this.compare_panel.Controls.Add(this.comboBox5);
            this.compare_panel.Controls.Add(this.label11);
            this.compare_panel.Controls.Add(this.comboBox4);
            this.compare_panel.Controls.Add(this.label10);
            this.compare_panel.Controls.Add(this.comboBox3);
            this.compare_panel.Controls.Add(this.label9);
            this.compare_panel.Controls.Add(this.comboBox2);
            this.compare_panel.Controls.Add(this.label8);
            this.compare_panel.Controls.Add(this.button1);
            this.compare_panel.Controls.Add(this.label7);
            this.compare_panel.Controls.Add(this.comboBox1);
            this.compare_panel.Controls.Add(this.label5);
            this.compare_panel.Controls.Add(this.label3);
            this.compare_panel.Controls.Add(this.compare_View);
            this.compare_panel.Location = new System.Drawing.Point(7, 150);
            this.compare_panel.Name = "compare_panel";
            this.compare_panel.Size = new System.Drawing.Size(1500, 545);
            this.compare_panel.TabIndex = 195;
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.button2.ForeColor = System.Drawing.Color.LightSeaGreen;
            this.button2.Location = new System.Drawing.Point(478, 511);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(63, 23);
            this.button2.TabIndex = 198;
            this.button2.Text = "发送";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // richTextBox1
            // 
            this.richTextBox1.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.richTextBox1.Location = new System.Drawing.Point(8, 508);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(450, 30);
            this.richTextBox1.TabIndex = 197;
            this.richTextBox1.Text = "物料2171491101628的拍卖即将结束，将开始下一物料的拍卖。";
            // 
            // qtyq_rtb
            // 
            this.qtyq_rtb.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.qtyq_rtb.Location = new System.Drawing.Point(8, 190);
            this.qtyq_rtb.Name = "qtyq_rtb";
            this.qtyq_rtb.Size = new System.Drawing.Size(550, 312);
            this.qtyq_rtb.TabIndex = 196;
            this.qtyq_rtb.Text = resources.GetString("qtyq_rtb.Text");
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("宋体", 9F);
            this.label12.Location = new System.Drawing.Point(290, 129);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(53, 12);
            this.label12.TabIndex = 194;
            this.label12.Text = "历史价格";
            // 
            // comboBox5
            // 
            this.comboBox5.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.comboBox5.Location = new System.Drawing.Point(350, 124);
            this.comboBox5.Name = "comboBox5";
            this.comboBox5.Size = new System.Drawing.Size(94, 22);
            this.comboBox5.TabIndex = 195;
            this.comboBox5.Text = "40";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("宋体", 9F);
            this.label11.Location = new System.Drawing.Point(22, 129);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(53, 12);
            this.label11.TabIndex = 192;
            this.label11.Text = "平均报价";
            // 
            // comboBox4
            // 
            this.comboBox4.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.comboBox4.Location = new System.Drawing.Point(82, 124);
            this.comboBox4.Name = "comboBox4";
            this.comboBox4.Size = new System.Drawing.Size(94, 22);
            this.comboBox4.TabIndex = 193;
            this.comboBox4.Text = "39.2";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("宋体", 9F);
            this.label10.Location = new System.Drawing.Point(275, 89);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(65, 12);
            this.label10.TabIndex = 190;
            this.label10.Text = "最低报价商";
            // 
            // comboBox3
            // 
            this.comboBox3.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.comboBox3.Location = new System.Drawing.Point(347, 84);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(180, 22);
            this.comboBox3.TabIndex = 191;
            this.comboBox3.Text = "SP001001";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("宋体", 9F);
            this.label9.Location = new System.Drawing.Point(22, 89);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(53, 12);
            this.label9.TabIndex = 188;
            this.label9.Text = "最低报价";
            // 
            // comboBox2
            // 
            this.comboBox2.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.comboBox2.Location = new System.Drawing.Point(82, 84);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(94, 22);
            this.comboBox2.TabIndex = 189;
            this.comboBox2.Text = "27";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label8.Location = new System.Drawing.Point(276, 48);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(140, 14);
            this.label8.TabIndex = 187;
            this.label8.Text = "防锈防冻漆     6 /7";
            // 
            // button1
            // 
            this.button1.ForeColor = System.Drawing.Color.LightSeaGreen;
            this.button1.Location = new System.Drawing.Point(422, 43);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(63, 23);
            this.button1.TabIndex = 179;
            this.button1.Text = "下一项";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("宋体", 9F);
            this.label7.Location = new System.Drawing.Point(22, 49);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 12);
            this.label7.TabIndex = 185;
            this.label7.Text = "拍卖物料";
            // 
            // comboBox1
            // 
            this.comboBox1.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.comboBox1.Location = new System.Drawing.Point(82, 44);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(180, 22);
            this.comboBox1.TabIndex = 186;
            this.comboBox1.Text = "21714911001628";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("宋体", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label5.Location = new System.Drawing.Point(5, 167);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(71, 15);
            this.label5.TabIndex = 184;
            this.label5.Text = "拍卖现场";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("宋体", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label3.Location = new System.Drawing.Point(5, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(103, 15);
            this.label3.TabIndex = 183;
            this.label3.Text = "当前拍卖信息";
            // 
            // compare_View
            // 
            this.compare_View.AllowUserToAddRows = false;
            this.compare_View.BackgroundColor = System.Drawing.Color.White;
            this.compare_View.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.compare_View.ColumnHeadersVisible = false;
            this.compare_View.Location = new System.Drawing.Point(583, 30);
            this.compare_View.Name = "compare_View";
            this.compare_View.RowHeadersVisible = false;
            this.compare_View.RowTemplate.Height = 23;
            this.compare_View.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.compare_View.Size = new System.Drawing.Size(850, 509);
            this.compare_View.TabIndex = 182;
            // 
            // Auction_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(930, 516);
            this.Controls.Add(this.compare_panel);
            this.Controls.Add(this.saveOfferButton);
            this.Controls.Add(this.ckxj_button);
            this.Controls.Add(this.scxj_button);
            this.Controls.Add(this.Inquiry_view);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.Supplier_view);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "Auction_Form";
            this.Text = "竞价拍卖";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.close_pb)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.read_pb)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.edit_pb)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Supplier_view)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Inquiry_view)).EndInit();
            this.compare_panel.ResumeLayout(false);
            this.compare_panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.compare_View)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DateTimePicker xjsj_dt;
        private System.Windows.Forms.ComboBox xjdh_cmb;
        private System.Windows.Forms.ComboBox cgdh_cmb;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label cgdh_lbl;
        private System.Windows.Forms.Label xjdh_lbl;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label read_lbl;
        private System.Windows.Forms.Label edit_lbl;
        private System.Windows.Forms.PictureBox read_pb;
        private System.Windows.Forms.PictureBox edit_pb;
        private System.Windows.Forms.Button ckcg_button;
        private System.Windows.Forms.ComboBox cgzz_cmb;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cgz_cmb;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker bjqx_dt;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridView Supplier_view;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DataGridView Inquiry_view;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn1;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn2;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn3;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn4;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn5;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn6;
        private System.Windows.Forms.Label close_lbl;
        private System.Windows.Forms.PictureBox close_pb;
        private System.Windows.Forms.ComboBox xjzt_cmb;
        private System.Windows.Forms.Button send_button;
        private System.Windows.Forms.Button scxj_button;
        private System.Windows.Forms.Button ckxj_button;
        private System.Windows.Forms.TextBox activate_tb;
        private System.Windows.Forms.Button saveOfferButton;
        private System.Windows.Forms.DataGridViewTextBoxColumn Inquiry_view0;
        private System.Windows.Forms.DataGridViewTextBoxColumn Inquiry_view1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Inquiry_view2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Inquiry_view3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Inquiry_view4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Inquiry_view11;
        private System.Windows.Forms.DataGridViewTextBoxColumn Inquiry_view5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Inquiry_view6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Inquiry_view7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Inquiry_view8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Inquiry_view9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Inquiry_view12;
        private System.Windows.Forms.DataGridViewTextBoxColumn Inquiry_view13;
        private System.Windows.Forms.DataGridViewTextBoxColumn Inquiry_view10;
        private System.Windows.Forms.DataGridViewTextBoxColumn Supplier_view0;
        private System.Windows.Forms.DataGridViewTextBoxColumn Supplier_view1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Supplier_view2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Supplier_view3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Supplier_view4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Supplier_view5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Supplier_view6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Supplier_view7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Supplier_view8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.Panel compare_panel;
        private System.Windows.Forms.DataGridView compare_View;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox comboBox5;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox comboBox4;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox comboBox3;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.RichTextBox qtyq_rtb;
    }
}