﻿namespace MMClient.SourcingManage.SourcingManagement
{
    partial class ApproveInvivationFrom
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.ApproveCB = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.TotalDGV = new System.Windows.Forms.DataGridView();
            this.Source_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Purchase_Org = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Buyer_Name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Offer_Time = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.State = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Purchase_Group = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Inquiry_Name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CreateTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Material_Location = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CheckResultCB = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.ApproveResultCB = new System.Windows.Forms.ComboBox();
            this.CommitBtn = new System.Windows.Forms.Button();
            this.CloseBtn = new System.Windows.Forms.Button();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.ApproveRText = new System.Windows.Forms.RichTextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.RoleText = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.TotalDGV)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(2, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "显示";
            // 
            // ApproveCB
            // 
            this.ApproveCB.FormattingEnabled = true;
            this.ApproveCB.Items.AddRange(new object[] {
            "请选择",
            "待审核",
            "待审批"});
            this.ApproveCB.Location = new System.Drawing.Point(34, 4);
            this.ApproveCB.Name = "ApproveCB";
            this.ApproveCB.Size = new System.Drawing.Size(121, 20);
            this.ApproveCB.TabIndex = 1;
            this.ApproveCB.SelectedIndexChanged += new System.EventHandler(this.ApproveCB_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.label2.Location = new System.Drawing.Point(2, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(796, 16);
            this.label2.TabIndex = 2;
            this.label2.Text = "邀请函总览";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(162, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 3;
            this.button1.Text = "刷新";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // TotalDGV
            // 
            this.TotalDGV.AllowUserToAddRows = false;
            this.TotalDGV.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.TotalDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.TotalDGV.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Source_ID,
            this.Purchase_Org,
            this.Buyer_Name,
            this.Offer_Time,
            this.State,
            this.Purchase_Group,
            this.Inquiry_Name,
            this.CreateTime,
            this.Material_Location});
            this.TotalDGV.Location = new System.Drawing.Point(4, 48);
            this.TotalDGV.Name = "TotalDGV";
            this.TotalDGV.RowTemplate.Height = 23;
            this.TotalDGV.Size = new System.Drawing.Size(794, 170);
            this.TotalDGV.TabIndex = 4;
            // 
            // Source_ID
            // 
            this.Source_ID.DataPropertyName = "Source_ID";
            this.Source_ID.HeaderText = "询价单号";
            this.Source_ID.Name = "Source_ID";
            // 
            // Purchase_Org
            // 
            this.Purchase_Org.DataPropertyName = "Purchase_Org";
            this.Purchase_Org.HeaderText = "采购组织";
            this.Purchase_Org.Name = "Purchase_Org";
            // 
            // Buyer_Name
            // 
            this.Buyer_Name.DataPropertyName = "Buyer_Name";
            this.Buyer_Name.HeaderText = "申请人";
            this.Buyer_Name.Name = "Buyer_Name";
            // 
            // Offer_Time
            // 
            this.Offer_Time.DataPropertyName = "Offer_Time";
            this.Offer_Time.HeaderText = "截止时间";
            this.Offer_Time.Name = "Offer_Time";
            // 
            // State
            // 
            this.State.DataPropertyName = "State";
            this.State.HeaderText = "状态";
            this.State.Name = "State";
            // 
            // Purchase_Group
            // 
            this.Purchase_Group.DataPropertyName = "Purchase_Group";
            this.Purchase_Group.HeaderText = "采购组";
            this.Purchase_Group.Name = "Purchase_Group";
            // 
            // Inquiry_Name
            // 
            this.Inquiry_Name.DataPropertyName = "Inquiry_Name";
            this.Inquiry_Name.HeaderText = "询价名称";
            this.Inquiry_Name.Name = "Inquiry_Name";
            // 
            // CreateTime
            // 
            this.CreateTime.DataPropertyName = "CreateTime";
            this.CreateTime.HeaderText = "创建时间";
            this.CreateTime.Name = "CreateTime";
            // 
            // Material_Location
            // 
            this.Material_Location.DataPropertyName = "Material_Location";
            this.Material_Location.HeaderText = "物料定位";
            this.Material_Location.Name = "Material_Location";
            // 
            // CheckResultCB
            // 
            this.CheckResultCB.FormattingEnabled = true;
            this.CheckResultCB.Items.AddRange(new object[] {
            "请选择",
            "审核通过",
            "审核不通过"});
            this.CheckResultCB.Location = new System.Drawing.Point(55, 294);
            this.CheckResultCB.Name = "CheckResultCB";
            this.CheckResultCB.Size = new System.Drawing.Size(125, 20);
            this.CheckResultCB.TabIndex = 5;
            this.CheckResultCB.MouseClick += new System.Windows.Forms.MouseEventHandler(this.CheckResultCB_MouseClick);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(4, 229);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 12);
            this.label3.TabIndex = 7;
            this.label3.Text = "审核意见";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(2, 297);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 12);
            this.label4.TabIndex = 8;
            this.label4.Text = "审核结果";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(4, 338);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 12);
            this.label5.TabIndex = 9;
            this.label5.Text = "审批意见";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(4, 400);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 12);
            this.label6.TabIndex = 10;
            this.label6.Text = "审批结果";
            // 
            // ApproveResultCB
            // 
            this.ApproveResultCB.FormattingEnabled = true;
            this.ApproveResultCB.Items.AddRange(new object[] {
            "请选择",
            "批准",
            "不批准"});
            this.ApproveResultCB.Location = new System.Drawing.Point(55, 397);
            this.ApproveResultCB.Name = "ApproveResultCB";
            this.ApproveResultCB.Size = new System.Drawing.Size(125, 20);
            this.ApproveResultCB.TabIndex = 12;
            this.ApproveResultCB.MouseClick += new System.Windows.Forms.MouseEventHandler(this.ApproveResultCB_MouseClick);
            // 
            // CommitBtn
            // 
            this.CommitBtn.Location = new System.Drawing.Point(4, 424);
            this.CommitBtn.Name = "CommitBtn";
            this.CommitBtn.Size = new System.Drawing.Size(75, 23);
            this.CommitBtn.TabIndex = 13;
            this.CommitBtn.Text = "提交";
            this.CommitBtn.UseVisualStyleBackColor = true;
            this.CommitBtn.Click += new System.EventHandler(this.CommitBtn_Click);
            // 
            // CloseBtn
            // 
            this.CloseBtn.Location = new System.Drawing.Point(105, 424);
            this.CloseBtn.Name = "CloseBtn";
            this.CloseBtn.Size = new System.Drawing.Size(75, 23);
            this.CloseBtn.TabIndex = 14;
            this.CloseBtn.Text = "关闭";
            this.CloseBtn.UseVisualStyleBackColor = true;
            this.CloseBtn.Click += new System.EventHandler(this.CloseBtn_Click);
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(55, 229);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(743, 54);
            this.richTextBox1.TabIndex = 15;
            this.richTextBox1.Text = "";
            this.richTextBox1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.richTextBox1_MouseClick);
            // 
            // ApproveRText
            // 
            this.ApproveRText.Location = new System.Drawing.Point(55, 338);
            this.ApproveRText.Name = "ApproveRText";
            this.ApproveRText.Size = new System.Drawing.Size(743, 54);
            this.ApproveRText.TabIndex = 16;
            this.ApproveRText.Text = "";
            this.ApproveRText.MouseClick += new System.Windows.Forms.MouseEventHandler(this.ApproveRText_MouseClick);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(622, 9);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(77, 12);
            this.label7.TabIndex = 17;
            this.label7.Text = "当前用户职位";
            // 
            // RoleText
            // 
            this.RoleText.Location = new System.Drawing.Point(698, 4);
            this.RoleText.Name = "RoleText";
            this.RoleText.Size = new System.Drawing.Size(100, 21);
            this.RoleText.TabIndex = 18;
            // 
            // ApproveInvivationFrom
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.RoleText);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.ApproveRText);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.CloseBtn);
            this.Controls.Add(this.CommitBtn);
            this.Controls.Add(this.ApproveResultCB);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.CheckResultCB);
            this.Controls.Add(this.TotalDGV);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.ApproveCB);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "ApproveInvivationFrom";
            this.Text = "审批";
            ((System.ComponentModel.ISupportInitialize)(this.TotalDGV)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox ApproveCB;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridView TotalDGV;
        private System.Windows.Forms.ComboBox CheckResultCB;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox ApproveResultCB;
        private System.Windows.Forms.Button CommitBtn;
        private System.Windows.Forms.Button CloseBtn;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.RichTextBox ApproveRText;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox RoleText;
        private System.Windows.Forms.DataGridViewTextBoxColumn Source_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Purchase_Org;
        private System.Windows.Forms.DataGridViewTextBoxColumn Buyer_Name;
        private System.Windows.Forms.DataGridViewTextBoxColumn Offer_Time;
        private System.Windows.Forms.DataGridViewTextBoxColumn State;
        private System.Windows.Forms.DataGridViewTextBoxColumn Purchase_Group;
        private System.Windows.Forms.DataGridViewTextBoxColumn Inquiry_Name;
        private System.Windows.Forms.DataGridViewTextBoxColumn CreateTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn Material_Location;
    }
}