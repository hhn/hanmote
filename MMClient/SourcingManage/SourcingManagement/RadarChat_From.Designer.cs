﻿namespace MMClient.SourcingManage.SourcingManagement
{
    partial class RadarChat_From
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btn_Sure = new System.Windows.Forms.Button();
            this.btn_Clear = new System.Windows.Forms.Button();
            this.btn_Close = new System.Windows.Forms.Button();
            this.grp_Condition = new System.Windows.Forms.GroupBox();
            this.gb_Standard = new System.Windows.Forms.GroupBox();
            this.Price_Score = new System.Windows.Forms.CheckBox();
            this.PriceLevel_Score = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.PriceHistory_Score = new System.Windows.Forms.CheckBox();
            this.Quality_Score = new System.Windows.Forms.CheckBox();
            this.GoodReceipt_Score = new System.Windows.Forms.CheckBox();
            this.QualityAudit_Score = new System.Windows.Forms.CheckBox();
            this.ComplaintAndReject_Score = new System.Windows.Forms.CheckBox();
            this.Delivery_Score = new System.Windows.Forms.CheckBox();
            this.OnTimeDelivery_Score = new System.Windows.Forms.CheckBox();
            this.ConfirmDate_Score = new System.Windows.Forms.CheckBox();
            this.QuantityReliability_Score = new System.Windows.Forms.CheckBox();
            this.Shipment_Score = new System.Windows.Forms.CheckBox();
            this.GeneralServiceAndSupport_Score = new System.Windows.Forms.CheckBox();
            this.ExternalService_Score = new System.Windows.Forms.CheckBox();
            this.lb_SecondStandard = new System.Windows.Forms.Label();
            this.lb_MainStandard = new System.Windows.Forms.Label();
            this.gb_Supplier = new System.Windows.Forms.GroupBox();
            this.dgv_SupplierList = new System.Windows.Forms.DataGridView();
            this.sSelection = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.sId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sIndustry = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sAddress = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.grp_Condition.SuspendLayout();
            this.gb_Standard.SuspendLayout();
            this.gb_Supplier.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_SupplierList)).BeginInit();
            this.SuspendLayout();
            // 
            // btn_Sure
            // 
            this.btn_Sure.Location = new System.Drawing.Point(27, 22);
            this.btn_Sure.Name = "btn_Sure";
            this.btn_Sure.Size = new System.Drawing.Size(75, 23);
            this.btn_Sure.TabIndex = 0;
            this.btn_Sure.Text = "确定";
            this.btn_Sure.UseVisualStyleBackColor = true;
            // 
            // btn_Clear
            // 
            this.btn_Clear.Location = new System.Drawing.Point(131, 22);
            this.btn_Clear.Name = "btn_Clear";
            this.btn_Clear.Size = new System.Drawing.Size(75, 23);
            this.btn_Clear.TabIndex = 1;
            this.btn_Clear.Text = "清除";
            this.btn_Clear.UseVisualStyleBackColor = true;
            // 
            // btn_Close
            // 
            this.btn_Close.Location = new System.Drawing.Point(246, 21);
            this.btn_Close.Name = "btn_Close";
            this.btn_Close.Size = new System.Drawing.Size(75, 23);
            this.btn_Close.TabIndex = 2;
            this.btn_Close.Text = "关闭";
            this.btn_Close.UseVisualStyleBackColor = true;
            // 
            // grp_Condition
            // 
            this.grp_Condition.Controls.Add(this.gb_Supplier);
            this.grp_Condition.Controls.Add(this.gb_Standard);
            this.grp_Condition.Location = new System.Drawing.Point(27, 101);
            this.grp_Condition.Name = "grp_Condition";
            this.grp_Condition.Size = new System.Drawing.Size(1024, 503);
            this.grp_Condition.TabIndex = 3;
            this.grp_Condition.TabStop = false;
            this.grp_Condition.Text = "条件设置";
            // 
            // gb_Standard
            // 
            this.gb_Standard.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.gb_Standard.BackColor = System.Drawing.SystemColors.Control;
            this.gb_Standard.Controls.Add(this.Price_Score);
            this.gb_Standard.Controls.Add(this.PriceLevel_Score);
            this.gb_Standard.Controls.Add(this.label6);
            this.gb_Standard.Controls.Add(this.label7);
            this.gb_Standard.Controls.Add(this.label9);
            this.gb_Standard.Controls.Add(this.label8);
            this.gb_Standard.Controls.Add(this.label5);
            this.gb_Standard.Controls.Add(this.PriceHistory_Score);
            this.gb_Standard.Controls.Add(this.Quality_Score);
            this.gb_Standard.Controls.Add(this.GoodReceipt_Score);
            this.gb_Standard.Controls.Add(this.QualityAudit_Score);
            this.gb_Standard.Controls.Add(this.ComplaintAndReject_Score);
            this.gb_Standard.Controls.Add(this.Delivery_Score);
            this.gb_Standard.Controls.Add(this.OnTimeDelivery_Score);
            this.gb_Standard.Controls.Add(this.ConfirmDate_Score);
            this.gb_Standard.Controls.Add(this.QuantityReliability_Score);
            this.gb_Standard.Controls.Add(this.Shipment_Score);
            this.gb_Standard.Controls.Add(this.GeneralServiceAndSupport_Score);
            this.gb_Standard.Controls.Add(this.ExternalService_Score);
            this.gb_Standard.Controls.Add(this.lb_SecondStandard);
            this.gb_Standard.Controls.Add(this.lb_MainStandard);
            this.gb_Standard.Location = new System.Drawing.Point(15, 20);
            this.gb_Standard.Name = "gb_Standard";
            this.gb_Standard.Size = new System.Drawing.Size(258, 430);
            this.gb_Standard.TabIndex = 12;
            this.gb_Standard.TabStop = false;
            this.gb_Standard.Text = "标准选择";
            // 
            // Price_Score
            // 
            this.Price_Score.AutoSize = true;
            this.Price_Score.Checked = true;
            this.Price_Score.CheckState = System.Windows.Forms.CheckState.Checked;
            this.Price_Score.Location = new System.Drawing.Point(19, 69);
            this.Price_Score.Name = "Price_Score";
            this.Price_Score.Size = new System.Drawing.Size(48, 16);
            this.Price_Score.TabIndex = 33;
            this.Price_Score.Text = "价格";
            this.Price_Score.UseVisualStyleBackColor = true;
            // 
            // PriceLevel_Score
            // 
            this.PriceLevel_Score.AutoSize = true;
            this.PriceLevel_Score.Location = new System.Drawing.Point(120, 58);
            this.PriceLevel_Score.Name = "PriceLevel_Score";
            this.PriceLevel_Score.Size = new System.Drawing.Size(72, 16);
            this.PriceLevel_Score.TabIndex = 31;
            this.PriceLevel_Score.Text = "价格水平";
            this.PriceLevel_Score.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label6.Location = new System.Drawing.Point(8, 44);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(244, 1);
            this.label6.TabIndex = 14;
            // 
            // label7
            // 
            this.label7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label7.Location = new System.Drawing.Point(8, 108);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(244, 1);
            this.label7.TabIndex = 18;
            // 
            // label9
            // 
            this.label9.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label9.Location = new System.Drawing.Point(8, 369);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(244, 1);
            this.label9.TabIndex = 32;
            // 
            // label8
            // 
            this.label8.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label8.Location = new System.Drawing.Point(8, 314);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(244, 1);
            this.label8.TabIndex = 30;
            // 
            // label5
            // 
            this.label5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label5.Location = new System.Drawing.Point(8, 206);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(244, 1);
            this.label5.TabIndex = 24;
            // 
            // PriceHistory_Score
            // 
            this.PriceHistory_Score.AutoSize = true;
            this.PriceHistory_Score.Location = new System.Drawing.Point(120, 82);
            this.PriceHistory_Score.Name = "PriceHistory_Score";
            this.PriceHistory_Score.Size = new System.Drawing.Size(72, 16);
            this.PriceHistory_Score.TabIndex = 16;
            this.PriceHistory_Score.Text = "价格历史";
            this.PriceHistory_Score.UseVisualStyleBackColor = true;
            // 
            // Quality_Score
            // 
            this.Quality_Score.AutoSize = true;
            this.Quality_Score.Checked = true;
            this.Quality_Score.CheckState = System.Windows.Forms.CheckState.Checked;
            this.Quality_Score.Location = new System.Drawing.Point(19, 149);
            this.Quality_Score.Name = "Quality_Score";
            this.Quality_Score.Size = new System.Drawing.Size(48, 16);
            this.Quality_Score.TabIndex = 15;
            this.Quality_Score.Text = "质量";
            this.Quality_Score.UseVisualStyleBackColor = true;
            // 
            // GoodReceipt_Score
            // 
            this.GoodReceipt_Score.AutoSize = true;
            this.GoodReceipt_Score.Location = new System.Drawing.Point(120, 120);
            this.GoodReceipt_Score.Name = "GoodReceipt_Score";
            this.GoodReceipt_Score.Size = new System.Drawing.Size(48, 16);
            this.GoodReceipt_Score.TabIndex = 17;
            this.GoodReceipt_Score.Text = "收货";
            this.GoodReceipt_Score.UseVisualStyleBackColor = true;
            // 
            // QualityAudit_Score
            // 
            this.QualityAudit_Score.AutoSize = true;
            this.QualityAudit_Score.Location = new System.Drawing.Point(120, 147);
            this.QualityAudit_Score.Name = "QualityAudit_Score";
            this.QualityAudit_Score.Size = new System.Drawing.Size(72, 16);
            this.QualityAudit_Score.TabIndex = 20;
            this.QualityAudit_Score.Text = "质量审计";
            this.QualityAudit_Score.UseVisualStyleBackColor = true;
            // 
            // ComplaintAndReject_Score
            // 
            this.ComplaintAndReject_Score.AutoSize = true;
            this.ComplaintAndReject_Score.Location = new System.Drawing.Point(120, 174);
            this.ComplaintAndReject_Score.Name = "ComplaintAndReject_Score";
            this.ComplaintAndReject_Score.Size = new System.Drawing.Size(102, 16);
            this.ComplaintAndReject_Score.TabIndex = 19;
            this.ComplaintAndReject_Score.Text = "抱怨/拒绝水平";
            this.ComplaintAndReject_Score.UseVisualStyleBackColor = true;
            // 
            // Delivery_Score
            // 
            this.Delivery_Score.AutoSize = true;
            this.Delivery_Score.Checked = true;
            this.Delivery_Score.CheckState = System.Windows.Forms.CheckState.Checked;
            this.Delivery_Score.Location = new System.Drawing.Point(19, 253);
            this.Delivery_Score.Name = "Delivery_Score";
            this.Delivery_Score.Size = new System.Drawing.Size(48, 16);
            this.Delivery_Score.TabIndex = 21;
            this.Delivery_Score.Text = "交货";
            this.Delivery_Score.UseVisualStyleBackColor = true;
            // 
            // OnTimeDelivery_Score
            // 
            this.OnTimeDelivery_Score.AutoSize = true;
            this.OnTimeDelivery_Score.Location = new System.Drawing.Point(120, 213);
            this.OnTimeDelivery_Score.Name = "OnTimeDelivery_Score";
            this.OnTimeDelivery_Score.Size = new System.Drawing.Size(108, 16);
            this.OnTimeDelivery_Score.TabIndex = 22;
            this.OnTimeDelivery_Score.Text = "按时交货的表现";
            this.OnTimeDelivery_Score.UseVisualStyleBackColor = true;
            // 
            // ConfirmDate_Score
            // 
            this.ConfirmDate_Score.AutoSize = true;
            this.ConfirmDate_Score.Location = new System.Drawing.Point(120, 239);
            this.ConfirmDate_Score.Name = "ConfirmDate_Score";
            this.ConfirmDate_Score.Size = new System.Drawing.Size(72, 16);
            this.ConfirmDate_Score.TabIndex = 28;
            this.ConfirmDate_Score.Text = "确认日期";
            this.ConfirmDate_Score.UseVisualStyleBackColor = true;
            // 
            // QuantityReliability_Score
            // 
            this.QuantityReliability_Score.AutoSize = true;
            this.QuantityReliability_Score.Location = new System.Drawing.Point(120, 264);
            this.QuantityReliability_Score.Name = "QuantityReliability_Score";
            this.QuantityReliability_Score.Size = new System.Drawing.Size(84, 16);
            this.QuantityReliability_Score.TabIndex = 27;
            this.QuantityReliability_Score.Text = "数量可靠性";
            this.QuantityReliability_Score.UseVisualStyleBackColor = true;
            // 
            // Shipment_Score
            // 
            this.Shipment_Score.AutoSize = true;
            this.Shipment_Score.Location = new System.Drawing.Point(120, 288);
            this.Shipment_Score.Name = "Shipment_Score";
            this.Shipment_Score.Size = new System.Drawing.Size(72, 16);
            this.Shipment_Score.TabIndex = 29;
            this.Shipment_Score.Text = "装运须知";
            this.Shipment_Score.UseVisualStyleBackColor = true;
            // 
            // GeneralServiceAndSupport_Score
            // 
            this.GeneralServiceAndSupport_Score.AutoSize = true;
            this.GeneralServiceAndSupport_Score.Checked = true;
            this.GeneralServiceAndSupport_Score.CheckState = System.Windows.Forms.CheckState.Checked;
            this.GeneralServiceAndSupport_Score.Location = new System.Drawing.Point(19, 331);
            this.GeneralServiceAndSupport_Score.Name = "GeneralServiceAndSupport_Score";
            this.GeneralServiceAndSupport_Score.Size = new System.Drawing.Size(102, 16);
            this.GeneralServiceAndSupport_Score.TabIndex = 26;
            this.GeneralServiceAndSupport_Score.Text = "一般服务/支持";
            this.GeneralServiceAndSupport_Score.UseVisualStyleBackColor = true;
            // 
            // ExternalService_Score
            // 
            this.ExternalService_Score.AutoSize = true;
            this.ExternalService_Score.Checked = true;
            this.ExternalService_Score.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ExternalService_Score.Location = new System.Drawing.Point(19, 388);
            this.ExternalService_Score.Name = "ExternalService_Score";
            this.ExternalService_Score.Size = new System.Drawing.Size(72, 16);
            this.ExternalService_Score.TabIndex = 25;
            this.ExternalService_Score.Text = "外部服务";
            this.ExternalService_Score.UseVisualStyleBackColor = true;
            // 
            // lb_SecondStandard
            // 
            this.lb_SecondStandard.AutoSize = true;
            this.lb_SecondStandard.Location = new System.Drawing.Point(127, 22);
            this.lb_SecondStandard.Name = "lb_SecondStandard";
            this.lb_SecondStandard.Size = new System.Drawing.Size(41, 12);
            this.lb_SecondStandard.TabIndex = 13;
            this.lb_SecondStandard.Text = "次标准";
            // 
            // lb_MainStandard
            // 
            this.lb_MainStandard.AutoSize = true;
            this.lb_MainStandard.Location = new System.Drawing.Point(22, 22);
            this.lb_MainStandard.Name = "lb_MainStandard";
            this.lb_MainStandard.Size = new System.Drawing.Size(41, 12);
            this.lb_MainStandard.TabIndex = 12;
            this.lb_MainStandard.Text = "主标准";
            // 
            // gb_Supplier
            // 
            this.gb_Supplier.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.gb_Supplier.Controls.Add(this.dgv_SupplierList);
            this.gb_Supplier.Location = new System.Drawing.Point(292, 20);
            this.gb_Supplier.Margin = new System.Windows.Forms.Padding(2);
            this.gb_Supplier.Name = "gb_Supplier";
            this.gb_Supplier.Padding = new System.Windows.Forms.Padding(2);
            this.gb_Supplier.Size = new System.Drawing.Size(700, 430);
            this.gb_Supplier.TabIndex = 35;
            this.gb_Supplier.TabStop = false;
            this.gb_Supplier.Text = "选择供应商";
            // 
            // dgv_SupplierList
            // 
            this.dgv_SupplierList.AllowUserToAddRows = false;
            this.dgv_SupplierList.AllowUserToDeleteRows = false;
            this.dgv_SupplierList.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv_SupplierList.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv_SupplierList.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            this.dgv_SupplierList.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgv_SupplierList.ColumnHeadersHeight = 25;
            this.dgv_SupplierList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgv_SupplierList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.sSelection,
            this.sId,
            this.sName,
            this.sIndustry,
            this.sAddress});
            this.dgv_SupplierList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_SupplierList.EnableHeadersVisualStyles = false;
            this.dgv_SupplierList.Location = new System.Drawing.Point(2, 16);
            this.dgv_SupplierList.Margin = new System.Windows.Forms.Padding(2);
            this.dgv_SupplierList.MultiSelect = false;
            this.dgv_SupplierList.Name = "dgv_SupplierList";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.GradientActiveCaption;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_SupplierList.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgv_SupplierList.RowHeadersWidth = 47;
            this.dgv_SupplierList.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Black;
            this.dgv_SupplierList.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this.dgv_SupplierList.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.Black;
            this.dgv_SupplierList.RowTemplate.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.Black;
            this.dgv_SupplierList.RowTemplate.Height = 27;
            this.dgv_SupplierList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv_SupplierList.Size = new System.Drawing.Size(696, 412);
            this.dgv_SupplierList.TabIndex = 35;
            // 
            // sSelection
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.NullValue = false;
            dataGridViewCellStyle2.Padding = new System.Windows.Forms.Padding(7, 0, 0, 0);
            this.sSelection.DefaultCellStyle = dataGridViewCellStyle2;
            this.sSelection.HeaderText = "OK";
            this.sSelection.Name = "sSelection";
            this.sSelection.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.sSelection.Width = 30;
            // 
            // sId
            // 
            this.sId.DataPropertyName = "Supplier_ID";
            this.sId.HeaderText = "供应商编号";
            this.sId.Name = "sId";
            this.sId.ReadOnly = true;
            this.sId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // sName
            // 
            this.sName.DataPropertyName = "Supplier_Name";
            this.sName.HeaderText = "供应商名称";
            this.sName.Name = "sName";
            this.sName.ReadOnly = true;
            this.sName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.sName.Width = 250;
            // 
            // sIndustry
            // 
            this.sIndustry.DataPropertyName = "SupplierIndustry_Id";
            this.sIndustry.HeaderText = "行业";
            this.sIndustry.Name = "sIndustry";
            this.sIndustry.ReadOnly = true;
            this.sIndustry.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // sAddress
            // 
            this.sAddress.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.sAddress.DataPropertyName = "Address";
            this.sAddress.HeaderText = "供应商地址";
            this.sAddress.Name = "sAddress";
            this.sAddress.ReadOnly = true;
            this.sAddress.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // RadarChat_From
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1063, 616);
            this.Controls.Add(this.grp_Condition);
            this.Controls.Add(this.btn_Close);
            this.Controls.Add(this.btn_Clear);
            this.Controls.Add(this.btn_Sure);
            this.Name = "RadarChat_From";
            this.Text = "供应商对比雷达图";
            this.grp_Condition.ResumeLayout(false);
            this.gb_Standard.ResumeLayout(false);
            this.gb_Standard.PerformLayout();
            this.gb_Supplier.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_SupplierList)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn_Sure;
        private System.Windows.Forms.Button btn_Clear;
        private System.Windows.Forms.Button btn_Close;
        private System.Windows.Forms.GroupBox grp_Condition;
        private System.Windows.Forms.GroupBox gb_Supplier;
        private System.Windows.Forms.DataGridView dgv_SupplierList;
        private System.Windows.Forms.DataGridViewCheckBoxColumn sSelection;
        private System.Windows.Forms.DataGridViewTextBoxColumn sId;
        private System.Windows.Forms.DataGridViewTextBoxColumn sName;
        private System.Windows.Forms.DataGridViewTextBoxColumn sIndustry;
        private System.Windows.Forms.DataGridViewTextBoxColumn sAddress;
        private System.Windows.Forms.GroupBox gb_Standard;
        private System.Windows.Forms.CheckBox Price_Score;
        private System.Windows.Forms.CheckBox PriceLevel_Score;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox PriceHistory_Score;
        private System.Windows.Forms.CheckBox Quality_Score;
        private System.Windows.Forms.CheckBox GoodReceipt_Score;
        private System.Windows.Forms.CheckBox QualityAudit_Score;
        private System.Windows.Forms.CheckBox ComplaintAndReject_Score;
        private System.Windows.Forms.CheckBox Delivery_Score;
        private System.Windows.Forms.CheckBox OnTimeDelivery_Score;
        private System.Windows.Forms.CheckBox ConfirmDate_Score;
        private System.Windows.Forms.CheckBox QuantityReliability_Score;
        private System.Windows.Forms.CheckBox Shipment_Score;
        private System.Windows.Forms.CheckBox GeneralServiceAndSupport_Score;
        private System.Windows.Forms.CheckBox ExternalService_Score;
        private System.Windows.Forms.Label lb_SecondStandard;
        private System.Windows.Forms.Label lb_MainStandard;
    }
}