﻿namespace MMClient.SourcingManage.SourcingManagement
{
    partial class NewOfferPriceInfoForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.inqueryPriceIdcomboBox = new System.Windows.Forms.ComboBox();
            this.supplierIdCombox = new System.Windows.Forms.ComboBox();
            this.materialIdCombox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.priceText = new System.Windows.Forms.TextBox();
            this.saveButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // inqueryPriceIdcomboBox
            // 
            this.inqueryPriceIdcomboBox.FormattingEnabled = true;
            this.inqueryPriceIdcomboBox.Location = new System.Drawing.Point(85, 22);
            this.inqueryPriceIdcomboBox.Name = "inqueryPriceIdcomboBox";
            this.inqueryPriceIdcomboBox.Size = new System.Drawing.Size(211, 20);
            this.inqueryPriceIdcomboBox.TabIndex = 0;
            this.inqueryPriceIdcomboBox.SelectedIndexChanged += new System.EventHandler(this.inqueryPriceIdcomboBox_SelectedIndexChanged);
            // 
            // supplierIdCombox
            // 
            this.supplierIdCombox.FormattingEnabled = true;
            this.supplierIdCombox.Location = new System.Drawing.Point(436, 22);
            this.supplierIdCombox.Name = "supplierIdCombox";
            this.supplierIdCombox.Size = new System.Drawing.Size(211, 20);
            this.supplierIdCombox.TabIndex = 1;
            // 
            // materialIdCombox
            // 
            this.materialIdCombox.FormattingEnabled = true;
            this.materialIdCombox.Location = new System.Drawing.Point(85, 145);
            this.materialIdCombox.Name = "materialIdCombox";
            this.materialIdCombox.Size = new System.Drawing.Size(211, 20);
            this.materialIdCombox.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(24, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 3;
            this.label1.Text = "询价单号";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(365, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 12);
            this.label2.TabIndex = 4;
            this.label2.Text = "供应商编码";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(24, 148);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 12);
            this.label3.TabIndex = 5;
            this.label3.Text = "物料编码";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(365, 147);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 12);
            this.label4.TabIndex = 6;
            this.label4.Text = "价     格";
            // 
            // priceText
            // 
            this.priceText.Location = new System.Drawing.Point(436, 144);
            this.priceText.Name = "priceText";
            this.priceText.Size = new System.Drawing.Size(211, 21);
            this.priceText.TabIndex = 7;
            // 
            // saveButton
            // 
            this.saveButton.Location = new System.Drawing.Point(713, 415);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(75, 23);
            this.saveButton.TabIndex = 8;
            this.saveButton.Text = "保存";
            this.saveButton.UseVisualStyleBackColor = true;
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // NewOfferPriceInfoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.saveButton);
            this.Controls.Add(this.priceText);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.materialIdCombox);
            this.Controls.Add(this.supplierIdCombox);
            this.Controls.Add(this.inqueryPriceIdcomboBox);
            this.Name = "NewOfferPriceInfoForm";
            this.Text = "新建报价单";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox inqueryPriceIdcomboBox;
        private System.Windows.Forms.ComboBox supplierIdCombox;
        private System.Windows.Forms.ComboBox materialIdCombox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox priceText;
        private System.Windows.Forms.Button saveButton;
    }
}