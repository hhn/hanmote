﻿namespace MMClient.SourcingManage.SourcingManagement
{
    partial class MaterialInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.MaterialListDGV = new System.Windows.Forms.DataGridView();
            this.Material_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Material_Name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Material_Type = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Material_Group = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Measurement = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MaterialListDGV)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.MaterialListDGV);
            this.groupBox1.Location = new System.Drawing.Point(44, 49);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox1.Size = new System.Drawing.Size(699, 352);
            this.groupBox1.TabIndex = 15;
            this.groupBox1.TabStop = false;
            // 
            // MaterialListDGV
            // 
            this.MaterialListDGV.AllowUserToAddRows = false;
            this.MaterialListDGV.BackgroundColor = System.Drawing.SystemColors.Control;
            this.MaterialListDGV.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.MaterialListDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.MaterialListDGV.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Material_ID,
            this.Material_Name,
            this.Material_Type,
            this.Material_Group,
            this.Measurement});
            this.MaterialListDGV.EnableHeadersVisualStyles = false;
            this.MaterialListDGV.Location = new System.Drawing.Point(25, 18);
            this.MaterialListDGV.Margin = new System.Windows.Forms.Padding(2);
            this.MaterialListDGV.Name = "MaterialListDGV";
            this.MaterialListDGV.ReadOnly = true;
            this.MaterialListDGV.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.LightBlue;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            this.MaterialListDGV.RowsDefaultCellStyle = dataGridViewCellStyle1;
            this.MaterialListDGV.RowTemplate.Height = 27;
            this.MaterialListDGV.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.MaterialListDGV.ShowCellErrors = false;
            this.MaterialListDGV.Size = new System.Drawing.Size(670, 312);
            this.MaterialListDGV.TabIndex = 0;
            // 
            // Material_ID
            // 
            this.Material_ID.DataPropertyName = "Material_ID";
            this.Material_ID.HeaderText = "物料编码";
            this.Material_ID.Name = "Material_ID";
            this.Material_ID.ReadOnly = true;
            this.Material_ID.Width = 150;
            // 
            // Material_Name
            // 
            this.Material_Name.DataPropertyName = "Material_Name";
            this.Material_Name.HeaderText = "物料名称";
            this.Material_Name.Name = "Material_Name";
            this.Material_Name.ReadOnly = true;
            this.Material_Name.Width = 150;
            // 
            // Material_Type
            // 
            this.Material_Type.DataPropertyName = "Material_Type";
            this.Material_Type.HeaderText = "物料类型";
            this.Material_Type.Name = "Material_Type";
            this.Material_Type.ReadOnly = true;
            // 
            // Material_Group
            // 
            this.Material_Group.DataPropertyName = "Material_Group";
            this.Material_Group.HeaderText = "物料组";
            this.Material_Group.Name = "Material_Group";
            this.Material_Group.ReadOnly = true;
            // 
            // Measurement
            // 
            this.Measurement.DataPropertyName = "Measurement";
            this.Measurement.HeaderText = "计量单位";
            this.Measurement.Name = "Measurement";
            this.Measurement.ReadOnly = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(668, 406);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 16;
            this.button1.Text = "确定";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // MaterialInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.groupBox1);
            this.Name = "MaterialInfo";
            this.Text = "选择物料";
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.MaterialListDGV)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView MaterialListDGV;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Material_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Material_Name;
        private System.Windows.Forms.DataGridViewTextBoxColumn Material_Type;
        private System.Windows.Forms.DataGridViewTextBoxColumn Material_Group;
        private System.Windows.Forms.DataGridViewTextBoxColumn Measurement;
    }
}