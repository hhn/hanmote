﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;
using Lib.Bll.SourcingManage.SourcingManagement;
using Lib.Model.SourcingManage.SourcingManagement;
using Lib.Bll.ServiceBll;
using Lib.Common.CommonUtils;
using Lib.Common.MMCException.Bll;

namespace MMClient.SourcingManage.SourcingManagement.Inquiry
{
    public partial class AllInquiry : DockContent
    {
        private SourceBLL sourceBll;
        private SourceInquiryBLL sourceInquiryBll;
        private ServiceBill serviceBill = new ServiceBill();
        public SourceInquiry offerSource;
        public SourceInquiry compareSource;

        public AllInquiry()
        {
            InitializeComponent();

            initData();
        }

        /// <summary>
        /// 初始化数据
        /// </summary>
        private void initData()
        {
            try
            {
                DataTable dt = serviceBill.GetAllInqueryPriceInfo();
                if (dt != null && dt.Rows.Count > 0)
                {
                    this.InqueryPriceDGV.DataSource = dt;
                }
            }
            catch (BllException e)
            {
                MessageUtil.ShowError(e.Msg);
            }
        }

        //查看按钮
        private void btn_Sure_Click(object sender, EventArgs e)
        {
            string state = this.cmb_inquiryState.Text;
            if (!"".Equals(state))
            {
                this.ClearDFV(this.InqueryPriceDGV);
                DataTable dt = serviceBill.GetInqueryPriceByState(state,-1,null);
                this.InqueryPriceDGV.DataSource = dt;
            }
            else
            {
                //获取所有询价单
                initData();
            }
        }

        //清空DGV
        private void ClearDFV(DataGridView dgv)
        {
            if(dgv != null)
            {
                DataTable dt = (DataTable)dgv.DataSource;
                dt.Rows.Clear();
            }
        }

        private void fillBidGridView(int state)
        {
            List<SourceInquiry> list = sourceInquiryBll.findSourcesByState(state);
            if (list.Count > InqueryPriceDGV.Rows.Count)
            {
                InqueryPriceDGV.Rows.Add(list.Count - InqueryPriceDGV.Rows.Count);
            }
            for (int i = 0; i < list.Count; i++)
            {
                InqueryPriceDGV.Rows[i].Cells["inquiryId"].Value = list.ElementAt(i).Source_ID;
                Source source = sourceBll.findSourceById(list.ElementAt(i).Source_ID);
                InqueryPriceDGV.Rows[i].Cells["InquiryName"].Value = source.Source_Name;
                InqueryPriceDGV.Rows[i].Cells["CreateTime"].Value = source.Create_Time.ToString("yyyy-MM-dd HH:mm:ss");
                // inquiryGridView.Rows[i].Cells["bidState"].Value = getStringByNumber(list.ElementAt(i).BidState);
            }
        }

        private int getIntByState(string state)
        {
            int res = 0;
            if (state.Equals("待维护"))
            {
                res = 0;
            }
            else if (state.Equals("待邀请"))
            {
                res = 1;
            }
            else if (state.Equals("待报价"))
            {
                res = 2;
            }
            else if (state.Equals("待比价"))
            {
                res = 3;
            }
            else if (state.Equals("已完成"))
            {
                res = 4;
            }
            return res;
        }

        private void inquiryGridView_SelectionChanged(object sender, EventArgs e)
        {
            int index = InqueryPriceDGV.CurrentRow.Index;
            Object obj = InqueryPriceDGV.Rows[index].Cells[0].Value;
            if (obj == null)
            {
                return;
            }
            String sourceId = InqueryPriceDGV.Rows[index].Cells[0].Value.ToString();
            offerSource = sourceInquiryBll.findSourceById(sourceId);
            compareSource = sourceInquiryBll.findSourceById(sourceId);
        }
    }
}
