﻿namespace MMClient.SourcingManage.SourcingManagement
{
    partial class EditInquery
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.InqueryPriceDGV = new System.Windows.Forms.DataGridView();
            this.Inquery_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Inquiry_Name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.createTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Offer_Time = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Purchase_Org = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Purchase_Group = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.State = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Buyer_Name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.InqueryId = new System.Windows.Forms.Label();
            this.InqueryNameTextBox = new System.Windows.Forms.TextBox();
            this.search = new System.Windows.Forms.Button();
            this.modify = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.InqueryPriceDGV)).BeginInit();
            this.SuspendLayout();
            // 
            // InqueryPriceDGV
            // 
            this.InqueryPriceDGV.AllowUserToAddRows = false;
            this.InqueryPriceDGV.BackgroundColor = System.Drawing.SystemColors.Control;
            this.InqueryPriceDGV.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.InqueryPriceDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.InqueryPriceDGV.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Inquery_ID,
            this.Inquiry_Name,
            this.createTime,
            this.Offer_Time,
            this.Purchase_Org,
            this.Purchase_Group,
            this.State,
            this.Buyer_Name});
            this.InqueryPriceDGV.EnableHeadersVisualStyles = false;
            this.InqueryPriceDGV.Location = new System.Drawing.Point(12, 41);
            this.InqueryPriceDGV.Name = "InqueryPriceDGV";
            this.InqueryPriceDGV.RowTemplate.Height = 23;
            this.InqueryPriceDGV.Size = new System.Drawing.Size(967, 262);
            this.InqueryPriceDGV.TabIndex = 1;
            // 
            // Inquery_ID
            // 
            this.Inquery_ID.DataPropertyName = "Source_ID";
            this.Inquery_ID.HeaderText = "询价单号";
            this.Inquery_ID.Name = "Inquery_ID";
            this.Inquery_ID.Width = 120;
            // 
            // Inquiry_Name
            // 
            this.Inquiry_Name.DataPropertyName = "Inquiry_Name";
            this.Inquiry_Name.HeaderText = "询价单名称";
            this.Inquiry_Name.Name = "Inquiry_Name";
            this.Inquiry_Name.Width = 110;
            // 
            // createTime
            // 
            this.createTime.DataPropertyName = "CreateTime";
            this.createTime.HeaderText = "创建时间";
            this.createTime.Name = "createTime";
            this.createTime.Width = 145;
            // 
            // Offer_Time
            // 
            this.Offer_Time.DataPropertyName = "Offer_Time";
            this.Offer_Time.HeaderText = "报价截止时间";
            this.Offer_Time.Name = "Offer_Time";
            this.Offer_Time.Width = 145;
            // 
            // Purchase_Org
            // 
            this.Purchase_Org.DataPropertyName = "Purchase_Org";
            this.Purchase_Org.HeaderText = "采购组织";
            this.Purchase_Org.Name = "Purchase_Org";
            // 
            // Purchase_Group
            // 
            this.Purchase_Group.DataPropertyName = "Purchase_Group";
            this.Purchase_Group.HeaderText = "采购组";
            this.Purchase_Group.Name = "Purchase_Group";
            // 
            // State
            // 
            this.State.DataPropertyName = "State";
            this.State.HeaderText = "询价状态";
            this.State.Name = "State";
            // 
            // Buyer_Name
            // 
            this.Buyer_Name.DataPropertyName = "Buyer_Name";
            this.Buyer_Name.HeaderText = "申请人姓名";
            this.Buyer_Name.Name = "Buyer_Name";
            // 
            // InqueryId
            // 
            this.InqueryId.AutoSize = true;
            this.InqueryId.Location = new System.Drawing.Point(12, 14);
            this.InqueryId.Name = "InqueryId";
            this.InqueryId.Size = new System.Drawing.Size(53, 12);
            this.InqueryId.TabIndex = 2;
            this.InqueryId.Text = "询价名称";
            // 
            // InqueryNameTextBox
            // 
            this.InqueryNameTextBox.Location = new System.Drawing.Point(71, 11);
            this.InqueryNameTextBox.Name = "InqueryNameTextBox";
            this.InqueryNameTextBox.Size = new System.Drawing.Size(127, 21);
            this.InqueryNameTextBox.TabIndex = 3;
            // 
            // search
            // 
            this.search.Location = new System.Drawing.Point(235, 11);
            this.search.Name = "search";
            this.search.Size = new System.Drawing.Size(75, 23);
            this.search.TabIndex = 4;
            this.search.Text = "搜索";
            this.search.UseVisualStyleBackColor = true;
            this.search.Click += new System.EventHandler(this.search_Click);
            // 
            // modify
            // 
            this.modify.Location = new System.Drawing.Point(895, 11);
            this.modify.Name = "modify";
            this.modify.Size = new System.Drawing.Size(75, 23);
            this.modify.TabIndex = 5;
            this.modify.Text = "修改";
            this.modify.UseVisualStyleBackColor = true;
            this.modify.Click += new System.EventHandler(this.modify_Click);
            // 
            // EditInquery
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1008, 451);
            this.Controls.Add(this.modify);
            this.Controls.Add(this.search);
            this.Controls.Add(this.InqueryNameTextBox);
            this.Controls.Add(this.InqueryId);
            this.Controls.Add(this.InqueryPriceDGV);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "EditInquery";
            this.Text = "更改询价";
            ((System.ComponentModel.ISupportInitialize)(this.InqueryPriceDGV)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView InqueryPriceDGV;
        private System.Windows.Forms.DataGridViewTextBoxColumn Inquery_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Inquiry_Name;
        private System.Windows.Forms.DataGridViewTextBoxColumn createTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn Offer_Time;
        private System.Windows.Forms.DataGridViewTextBoxColumn Purchase_Org;
        private System.Windows.Forms.DataGridViewTextBoxColumn Purchase_Group;
        private System.Windows.Forms.DataGridViewTextBoxColumn State;
        private System.Windows.Forms.DataGridViewTextBoxColumn Buyer_Name;
        private System.Windows.Forms.Label InqueryId;
        private System.Windows.Forms.TextBox InqueryNameTextBox;
        private System.Windows.Forms.Button search;
        private System.Windows.Forms.Button modify;
    }
}