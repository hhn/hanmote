﻿using Lib.Bll.ServiceBll;
using Lib.Common.CommonUtils;
using Lib.Model.ServiceModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MMClient.SourcingManage.SourcingManagement
{
    public partial class NewOfferPriceInfoForm : Form
    {
        private ServiceBill serviceBill = new ServiceBill();

        private OfferPrice offerPrice;

        //新建报价单
        public NewOfferPriceInfoForm()
        {
            InitializeComponent();
            this.InitData();
        }

        public NewOfferPriceInfoForm(OfferPrice offerPrice)
        {
            InitializeComponent();
            this.InitData();
            this.offerPrice = offerPrice;
            this.inqueryPriceIdcomboBox.Text = offerPrice.Source_ID;
            this.supplierIdCombox.Text = offerPrice.Supplier_ID;
            this.materialIdCombox.Text = offerPrice.Material_ID;
            this.priceText.Text = Convert.ToString(offerPrice.Price);
        }

        //初始化询价单id列表
        public void InitData()
        {
            //初始化询价单数据
            List<string> inqueryIdList = serviceBill.GetAllInqueryId();
            FormUtils.FillCombox(this.inqueryPriceIdcomboBox,inqueryIdList);
        }

        //询价单列表值改变
        private void inqueryPriceIdcomboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            object obj = this.inqueryPriceIdcomboBox.SelectedItem;
            if(obj != null && obj is string)
            {
                string inqueryPriceId = (string)obj;
                //加载询价单下的物料信息
                List<string> materialIdList = this.LoadMaterialIdByInqueryPriceId(inqueryPriceId);
                FormUtils.FillCombox(this.materialIdCombox,materialIdList);
            }
        }

        //根据询价单id查询该询价单下的物料id列表
        private List<string> LoadMaterialIdByInqueryPriceId(string inqueryPriceId)
        {
            return serviceBill.GetAllMaterialIdByInqueryPriceId(inqueryPriceId);
        }

        //保存按钮
        private void saveButton_Click(object sender, EventArgs e)
        {
            string inqueryPriceId = (string)this.inqueryPriceIdcomboBox.SelectedItem;
            string materialId = (string)this.materialIdCombox.SelectedItem;
            string supplierId = (string)this.supplierIdCombox.SelectedItem;
            string priceStr = this.priceText.Text;
            if (!"".Equals(priceStr))
            {
                OfferPrice offerPrice = new OfferPrice();
                offerPrice.Source_ID = inqueryPriceId;
                offerPrice.Material_ID = materialId;
                offerPrice.Supplier_ID = supplierId;
                offerPrice.Price = Convert.ToSingle(priceStr);
                //TODO 存入数据库
                MessageUtil.ShowTips("保存成功！");
            }
            else
            {
                MessageUtil.ShowWarning("请输入价格！");
            }
        }
    }
}
