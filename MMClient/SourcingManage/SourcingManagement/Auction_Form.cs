﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Net.Mail;
using System.Net.Sockets;
using Aspose.Cells;
using System.Windows.Forms;
using Lib.SqlServerDAL;
using Lib.SqlServerDAL.SourcingManage.ProcurementPlan;
using Lib.Model.SourcingManage.ProcurementPlan;
using Lib.Model.SourcingManage.SourcingManagement;
using Lib.Bll.SourcingManage.SourcingManagement;
using MMClient.SourcingManage;
using WeifenLuo.WinFormsUI.Docking;
using System.IO;

namespace MMClient.SourcingManage.SourcingManagement
{
    public partial class Auction_Form : DockContent
    {
        //询价单表头
        SourceInquiry inquiry_table = new SourceInquiry();
        //询价单
        List<Enquiry> enquiryList = new List<Enquiry>();
        List<Enquiry> xjxList = new List<Enquiry>();
        List<Enquiry> gysList = new List<Enquiry>();
        //查询工具
        EnquiryBLL enquiryBLL = new EnquiryBLL();
        //控件工具
        ConvenientTools tools = new ConvenientTools();
        //DGV默认行数
        int DGVnumber = 9;
        //物料及供应商编号itemslist
        List<string> wlbhItems = null;
        List<string> gysbhItems = null;
        List<string> xjdhItems = null;
        //物料及供应商名称itemslist
        List<string> wlmcItems = null;
        List<string> gysmcItems = null;
        bool isXJDBTsaved = false;
        bool isXJXsaved = false;
        bool isGYSsaved = false;
        bool isXJDsaved = false;
        bool isFromSourceMM = false;

        public Auction_Form()
        {
            InitializeComponent();
            Init(Supplier_view);
            Init(Inquiry_view);
            xjdhItems = tools.addItemsStringList("", "", "Inquiry_ID", "Inquiry_Table");
            tools.addItemsToCMB(xjdh_cmb, "", "", "Inquiry_ID", "Inquiry_Table");
            tools.addItemsToCMB(cgzz_cmb, "", "", "Department", "Buyer");
            tools.addItemsToCMB(cgz_cmb, "", "", "Buyer_Name", "Buyer");
            //xjdh_cmb.Text = xjdhItems.ElementAt(xjdhItems.Count-1);
            readClick();
        }


        private void Init(DataGridView view)
        {
            view.AllowUserToAddRows = true;
            view.AutoGenerateColumns = false;
            view.AutoSize = false;
            //view.RowHeadersVisible = false;
            //view.GridColor = System.Drawing.ColorTranslator.FromHtml("#F8F8FF");
            view.ColumnHeadersHeight = 60;
            view.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.None;
            view.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.Single;
            view.EditMode = DataGridViewEditMode.EditOnEnter;

            DataGridViewCellPainting(view);
            DataGridViewDataSource_Load(view);
            view.Parent = this;
            view.EditingControlShowing += new DataGridViewEditingControlShowingEventHandler(view_EditingControlShowing);
            //view.CellPainting -= new DataGridViewCellPaintingEventHandler(view_CellPainting);
            //view.CellPainting += new DataGridViewCellPaintingEventHandler(view_CellPainting);
        }

        /// <summary>
        ///datagridview绘图
        /// </summary>
        private void DataGridViewCellPainting(DataGridView view)
        {
            for(int j=0;j<DGVnumber;j++)
                view.Rows.Add();
            view.AllowUserToAddRows = false;
        }

        /// <summary>
        ///datagridview绑定数据源
        /// </summary>
        private void DataGridViewDataSource_Load(DataGridView view)
        {  
            wlbhItems = tools.addItemsStringList("", "", "Material_ID", "Material_Type");

            wlmcItems = tools.addItemsStringList("", "", "Material_Name", "Material_Type");

            gysbhItems = tools.addItemsStringList("", "", "Supplier_ID", "Supplier_Base");

            gysmcItems = tools.addItemsStringList("", "", "Supplier_Name", "Supplier_Base");

            //DataGridViewCheckBoxCell cbox = null;
            //for (int i = 0; i < DGVnumber; i++)
            //{
            //    cbox = view.Rows[i].Cells[0] as DataGridViewCheckBoxCell;
            //    cbox.Value = false;
            //}
        }

        /// <summary>
        /// 合并单元格重绘表格
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void view_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {

            if (e.RowIndex == 1 && e.ColumnIndex == 1)
            {

                Brush backColorBrush = new SolidBrush(e.CellStyle.BackColor);

                e.Paint(e.CellBounds, DataGridViewPaintParts.All);

                e.Graphics.FillRectangle(backColorBrush, e.CellBounds);

                //绘制背景色(被选中状态下)  

                if (e.State == (DataGridViewElementStates.Displayed | DataGridViewElementStates.Selected | DataGridViewElementStates.Visible))

                    e.PaintBackground(e.CellBounds, false);

                //分别绘制原文本和现在改变颜色的文本  

                Brush fontColor = new SolidBrush(e.CellStyle.ForeColor);

                // e.Graphics.DrawString("", this.Font, fontColor, e.CellBounds, StringFormat.GenericDefault);

                //绘制下边框线

                Brush gridBrush = new SolidBrush(this.Supplier_view.GridColor);

                Pen pen = new Pen(gridBrush);

                e.Graphics.DrawLine(pen, e.CellBounds.Left, e.CellBounds.Bottom - 1,

                e.CellBounds.Right - 1, e.CellBounds.Bottom - 1);

                DataGridViewCell cell = this.Supplier_view.Rows[e.RowIndex].Cells[e.ColumnIndex];

                cell.Value = "";

                e.Handled = true;

            }

            if (e.RowIndex == 1 && e.ColumnIndex == 2)
            {

                Brush backColorBrush = new SolidBrush(e.CellStyle.BackColor);

                e.Paint(e.CellBounds, DataGridViewPaintParts.All);

                e.Graphics.FillRectangle(backColorBrush, e.CellBounds);

                //绘制背景色(被选中状态下)  

                if (e.State == (DataGridViewElementStates.Displayed | DataGridViewElementStates.Selected | DataGridViewElementStates.Visible))

                    e.PaintBackground(e.CellBounds, true);

                //分别绘制原文本和现在改变颜色的文本  

                Brush fontColor = new SolidBrush(e.CellStyle.ForeColor);

                // e.Graphics.DrawString("", this.Font, fontColor, e.CellBounds, StringFormat.GenericDefault);

                //绘制下边框线

                Brush gridBrush = new SolidBrush(this.Supplier_view.GridColor);

                Pen pen = new Pen(gridBrush);

                e.Graphics.DrawLine(pen, e.CellBounds.Left, e.CellBounds.Bottom - 1,

                e.CellBounds.Right - 1, e.CellBounds.Bottom - 1);

                //绘制右边框线

                e.Graphics.DrawLine(pen, e.CellBounds.Right - 1,

                e.CellBounds.Top, e.CellBounds.Right - 1,

                e.CellBounds.Bottom - 1);

                DataGridViewCell cell = this.Supplier_view.Rows[e.RowIndex].Cells[e.ColumnIndex];

                cell.Value = "";

                cell.Tag = "ccccc";

                Rectangle rectanle = e.CellBounds;

                rectanle.X = rectanle.X - 15;

                rectanle.Y = rectanle.Y + 5;

                //这里需要注意的是我没有用 e.Graphics 原因是这个只能在当前单元格绘制

                //而我是在DataGridView上面建一个绘图Graphics对象这样就可以看上去在两个单元格里面绘制了

                //这里你们也可以自己试一试

                Graphics graphics = this.Supplier_view.CreateGraphics();

                //分别绘制原文本和现在改变颜色的文本  

                graphics.DrawString("cccc", this.Font, new SolidBrush(e.CellStyle.ForeColor), rectanle, StringFormat.GenericDefault);

                e.Handled = true;

            }

        }


        /// <summary>
        /// 编辑单元格添加控件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void view_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            Control[] CS = null;
            Button B = null;
            DateTimePicker T = null;
            ComboBox CMB = null;
            #region 物料/供应商编号添加详细按钮
            if (Inquiry_view.EditingControl != null)
            {
                CS = Inquiry_view.EditingPanel.Controls.Find("__BUTTON__", true);
                if (CS.Length > 0)
                    Inquiry_view.EditingPanel.Controls.Remove(CS[0]);
                if (Inquiry_view.CurrentCell.ColumnIndex == 1)
                {
                    B = new Button();
                    B.Name = "__BUTTON__";
                    B.BackColor = SystemColors.Control;
                    B.Text = "详";
                    B.ForeColor = Color.CornflowerBlue;
                    B.Width = 22;
                    B.Height = 22;
                    B.Parent = Inquiry_view.EditingPanel;
                    B.Dock = DockStyle.Right;
                    e.Control.Dock = DockStyle.Fill;
                    B.Click += new EventHandler(B_Click);
                }
            }
            if (Supplier_view.EditingControl != null)
            {
                CS = Supplier_view.EditingPanel.Controls.Find("__BUTTON__", true);
                if (CS.Length > 0)
                    Supplier_view.EditingPanel.Controls.Remove(CS[0]);
                if (Supplier_view.CurrentCell.ColumnIndex == 1)
                //if (view.CurrentCell.ColumnIndex == 0) 
                {
                    B = new Button();
                    B.Name = "__BUTTON__";
                    B.BackColor = SystemColors.Control;
                    B.Text = "详";
                    B.ForeColor = Color.CornflowerBlue;
                    B.Width = 22;
                    B.Height = 22;
                    B.Parent = Supplier_view.EditingPanel;
                    B.Dock = DockStyle.Right;
                    e.Control.Dock = DockStyle.Fill;
                    B.Click += new EventHandler(B_Click);
                }
            }
            #endregion

            #region 物料/供应商编号添加Combobox
            if (Inquiry_view.EditingControl != null)
            {
                CS = Inquiry_view.EditingPanel.Controls.Find("__combobox__", true);
                if (CS.Length > 0)
                    Inquiry_view.EditingPanel.Controls.Remove(CS[0]);
                if (Inquiry_view.CurrentCell.ColumnIndex == 1)
                {
                    CMB = new ComboBox();
                    CMB.Name = "__combobox__";
                    CMB.BackColor = SystemColors.Window;
                    CMB.Width = e.Control.Width;
                    CMB.Height = e.Control.Height;
                    for (int n = 0; n < wlbhItems.Count; n++)
                    {
                        CMB.Items.Add(wlbhItems.ElementAt(n));
                    }
                    if (Inquiry_view.CurrentCell.Value != null && !Inquiry_view.CurrentCell.Value.ToString().Equals(""))
                        CMB.Text = Inquiry_view.CurrentCell.Value.ToString();
                    CMB.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                    CMB.AutoCompleteSource = AutoCompleteSource.ListItems;
                    CMB.Parent = Inquiry_view.EditingPanel;
                    CMB.Dock = DockStyle.Right;
                    e.Control.Dock = DockStyle.Fill;
                    CMB.TextChanged += new EventHandler(CMB_TextChanged);
                }
            }
            if (Supplier_view.EditingControl != null)
            {
                CS = Supplier_view.EditingPanel.Controls.Find("__combobox__", true);
                if (CS.Length > 0)
                    Supplier_view.EditingPanel.Controls.Remove(CS[0]);
                if (Supplier_view.CurrentCell.ColumnIndex == 1)
                {
                    CMB = new ComboBox();
                    CMB.Name = "__combobox__";
                    CMB.BackColor = SystemColors.Window;
                    CMB.Width = e.Control.Width;
                    CMB.Height = e.Control.Height;
                    for (int n = 0; n < gysbhItems.Count; n++)
                    {
                        CMB.Items.Add(gysbhItems.ElementAt(n));
                    }
                    if (Supplier_view.CurrentCell.Value != null && !Supplier_view.CurrentCell.Value.ToString().Equals(""))
                        CMB.Text = Supplier_view.CurrentCell.Value.ToString();
                    CMB.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                    CMB.AutoCompleteSource = AutoCompleteSource.ListItems;
                    CMB.Parent = Supplier_view.EditingPanel;
                    CMB.Dock = DockStyle.Right;
                    e.Control.Dock = DockStyle.Fill;
                    CMB.TextChanged += new EventHandler(CMB_TextChanged);
                }
            }
            #endregion

            #region 物料/供应商名称添加Combobox
            if (Inquiry_view.EditingControl != null)
            {
                CS = Inquiry_view.EditingPanel.Controls.Find("_combobox_", true);
                if (CS.Length > 0)
                    Inquiry_view.EditingPanel.Controls.Remove(CS[0]);
                if (Inquiry_view.CurrentCell.ColumnIndex == 2)
                {
                    CMB = new ComboBox();
                    CMB.Name = "_combobox_";
                    CMB.BackColor = SystemColors.Window;
                    CMB.Width = e.Control.Width;
                    CMB.Height = e.Control.Height;
                    for (int n = 0; n < wlmcItems.Count; n++)
                    {
                        CMB.Items.Add(wlmcItems.ElementAt(n));
                    }
                    if (Inquiry_view.CurrentCell.Value != null && !Inquiry_view.CurrentCell.Value.ToString().Equals(""))
                        CMB.Text = Inquiry_view.CurrentCell.Value.ToString();
                    CMB.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                    CMB.AutoCompleteSource = AutoCompleteSource.ListItems;
                    CMB.Parent = Inquiry_view.EditingPanel;
                    CMB.Dock = DockStyle.Right;
                    e.Control.Dock = DockStyle.Fill;
                    CMB.TextChanged += new EventHandler(CMB_TextChanged);
                }
            }
            if (Supplier_view.EditingControl != null)
            {
                CS = Supplier_view.EditingPanel.Controls.Find("_combobox_", true);
                if (CS.Length > 0)
                    Supplier_view.EditingPanel.Controls.Remove(CS[0]);
                if (Supplier_view.CurrentCell.ColumnIndex == 2)
                {
                    CMB = new ComboBox();
                    CMB.Name = "_combobox_";
                    CMB.BackColor = SystemColors.Window;
                    CMB.Width = e.Control.Width;
                    CMB.Height = e.Control.Height;
                    for (int n = 0; n < gysmcItems.Count; n++)
                    {
                        CMB.Items.Add(gysmcItems.ElementAt(n));
                    }
                    if (Supplier_view.CurrentCell.Value != null && !Supplier_view.CurrentCell.Value.ToString().Equals(""))
                        CMB.Text = Supplier_view.CurrentCell.Value.ToString();
                    CMB.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                    CMB.AutoCompleteSource = AutoCompleteSource.ListItems;
                    CMB.Parent = Supplier_view.EditingPanel;
                    CMB.Dock = DockStyle.Right;
                    e.Control.Dock = DockStyle.Fill;
                    CMB.TextChanged += new EventHandler(CMB_TextChanged);
                }
            }
            #endregion

            #region 交货日期添加日期控件

            CS = Inquiry_view.EditingPanel.Controls.Find("__DATETIME__", true);
            if (CS.Length > 0)
                Inquiry_view.EditingPanel.Controls.Remove(CS[0]);
            if (Inquiry_view.CurrentCell.ColumnIndex == 7)
            {
                T = new DateTimePicker();
                T.Name = "__DATETIME__";
                T.BackColor = SystemColors.Window;
                T.Width = e.Control.Width;
                T.Height = e.Control.Height;
                T.Parent = Inquiry_view.EditingPanel;
                T.Dock = DockStyle.Right;
                e.Control.Dock = DockStyle.Fill;
                T.ValueChanged += new EventHandler(T_ValueChanged);
            }
            #endregion
        }


        /// <summary>
        /// 编号列详细按钮事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void B_Click(object sender, EventArgs e)
        {
            if (Supplier_view.EditingControl!=null)
                MessageBox.Show(Supplier_view.EditingControl.Text);
            if(Inquiry_view.EditingControl!=null)
                MessageBox.Show(Inquiry_view.EditingControl.Text);
        }

        /// <summary>
        /// 交货日期列时间事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void T_ValueChanged(object sender, EventArgs e)
        {
            DateTimePicker time = sender as DateTimePicker;
            if (Inquiry_view.EditingControl != null)
            {
                if (tools.dateDiff(time.Value, bjqx_dt.Value).Days >= 1 && time.Value.CompareTo(bjqx_dt.Value) < 0)
                {
                    Inquiry_view.CurrentCell.Value = time.Value.ToString();
                }
                else
                    MessageBox.Show("交货日期必须晚于报价期限至少一天！");
            }
        }
        
        /// <summary>
        /// COMBOBOX值改变触发事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void CMB_TextChanged(object sender, EventArgs e)
        {
            ComboBox cmb = sender as ComboBox;
            string str = cmb.Text.ToString().Trim();
            if (str.Equals(""))
                return;
            if (Inquiry_view.EditingControl != null)
            {
                if (cmb.Name.Equals("_combobox_") && wlmcItems.Contains(str))
                {
                    Inquiry_view.CurrentCell.Value = str;
                }
                else if (cmb.Name.Equals("__combobox__") && wlbhItems.Contains(str))
                {
                    Inquiry_view.CurrentCell.Value = str;
                    ReadDataByID(Inquiry_view, str, Inquiry_view.CurrentCell.RowIndex);
                }
            }
            if (Supplier_view.EditingControl != null)
            {
                if (cmb.Name.Equals("_combobox_") && gysmcItems.Contains(str))
                {
                    Supplier_view.CurrentCell.Value = str;
                }
                else if (cmb.Name.Equals("__combobox__") && gysbhItems.Contains(str))
                {
                    Supplier_view.CurrentCell.Value = str;
                    ReadDataByID(Supplier_view, str, Supplier_view.CurrentCell.RowIndex);
                }
            } 
        }


        /// <summary>
        /// 参考采购按钮事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ckcg_button_Click(object sender, EventArgs e)
        {
            if (cgdh_cmb.Visible == false)
            {
                cgdh_cmb.Visible = true;
                cgdh_lbl.Visible = true;
                xjdh_cmb.Text = "A" + tools.systemTimeToStr();
            }
            else
            {
                cgdh_cmb.Visible = false;
                cgdh_lbl.Visible = false;
                xjdh_cmb.Text = "M" + tools.systemTimeToStr();
            }
        }


        /// <summary>
        /// DataGridView单元格编辑完成事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CellEndEdit(DataGridView view, int RowIndex, int ColumnIndex)
        {
            //物料编号行改变数据处理
            if (ColumnIndex == 1 && view.Rows[RowIndex].Cells[ColumnIndex].Value != null)
            {
                DataGridViewTextBoxCell cmb = view.Rows[RowIndex].Cells[ColumnIndex] as DataGridViewTextBoxCell;
                if (cmb == null || cmb.Value == null)
                    return;
                string str = cmb.Value.ToString().Trim();
                if (view.Name.Equals("Inquiry_view") && !wlbhItems.Contains(str))
                {
                    view.Rows[RowIndex].Cells[ColumnIndex].Value = "";
                    return;
                }
                else if (view.Name.Equals("Supplier_view") && !gysbhItems.Contains(str))
                {
                    view.Rows[RowIndex].Cells[ColumnIndex].Value = ""; ;
                    return;
                }
                if (str.Length > 0)
                {
                    ReadDataByID(view, str, RowIndex);
                }
                return;
            }

            //名称列与itemslist匹配
            if (ColumnIndex == 2)
            {
                DataGridViewTextBoxCell cmb = view.Rows[RowIndex].Cells[ColumnIndex] as DataGridViewTextBoxCell;
                if (cmb == null || cmb.Value == null)
                    return;
                string str = cmb.Value.ToString().Trim();
                if (!wlmcItems.Contains(str))
                {
                    view.CurrentCell.Value = "";
                }
            }

            //数量列只能输入数字
            if (ColumnIndex == 4 || ColumnIndex == 5)
            {
                if (view.Name.Equals("Inquiry_view"))
                {
                    DataGridViewTextBoxCell textBox = (DataGridViewTextBoxCell)view.Rows[RowIndex].Cells[ColumnIndex];
                    if (textBox.Value != null)
                        tools.onlyNumber(textBox);
                    return;
                }
            }
        }
        private void Inquiry_view_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            CellEndEdit(Inquiry_view, e.RowIndex, e.ColumnIndex);
        }
        private void Supplier_view_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            CellEndEdit(Supplier_view, e.RowIndex, e.ColumnIndex);
        }
        

        /// <summary>
        /// DataGridView根据编号读取数据
        /// </summary>
        /// <param name="view">DGV</param>
        /// <param name="str">编号</param>
        /// <param name="row">行号</param>
        private void ReadDataByID(DataGridView view, string str, int row)
        {
            Summary_DemandDAL newStandardDAL = new Summary_DemandDAL();
            DataTable dt = new DataTable();
            if (view.Name.Equals("Inquiry_view"))
            {
                for (int i = 0; i < view.Rows.Count; i++)
                {
                    if (view.Rows[i].Cells[1].Value == null)
                        break;
                    if (view.Rows[i].Cells[1].Value.Equals(str) && i != row)
                    {
                        MessageBox.Show("物料编号：" + str + "已重复，请重新选择！");
                        return;
                    }
                }
                //DataGridViewComboBoxColumn cmbox = Supplier_view.Columns["Supplier_view1"] as DataGridViewComboBoxColumn;
                //选定后移除
                //List<string> items = (List<string>) cmbox.DataSource;
                //items.Remove(str);
                //cmbox.DataSource = items;
                dt = newStandardDAL.FindMaterial(str, "Material_Type");
                view.Rows[row].Cells[2].Value = dt.Rows[0][1].ToString();
                view.Rows[row].Cells[3].Value = dt.Rows[0][5].ToString();
                view.Rows[row].Cells[6].Value = dt.Rows[0][4].ToString();
                view.Rows[row].Cells[8].Value = dt.Rows[0][2].ToString();

            }
            else
            {
                for (int i = 0; i < view.Rows.Count; i++)
                {
                    if (view.Rows[i].Cells[1].Value == null)
                        break;
                    if (view.Rows[i].Cells[1].Value.Equals(str) && i != row)
                    {
                        MessageBox.Show("供应商编号：" + str + "已重复，请重新选择！");
                        return;
                    }
                }
                //DataGridViewComboBoxColumn cmbox = Supplier_view.Columns["Supplier_view1"] as DataGridViewComboBoxColumn;
                //选定后移除
                //List<string> items = (List<string>) cmbox.DataSource;
                //items.Remove(str);
                //cmbox.DataSource = items;
                dt = newStandardDAL.Supplier_Base(1, str, "Supplier_Base");
                view.Rows[row].Cells[2].Value = dt.Rows[0][3].ToString();
                view.Rows[row].Cells[3].Value = dt.Rows[0][17].ToString();
                view.Rows[row].Cells[4].Value = dt.Rows[0][25].ToString();
                view.Rows[row].Cells[5].Value = dt.Rows[0][5].ToString();
                view.Rows[row].Cells[6].Value = dt.Rows[0][6].ToString();
                view.Rows[row].Cells[7].Value = dt.Rows[0][7].ToString();
            }
        }



        // 行选中值改变后的处理事件
        private void CellValueChanged(DataGridView view, object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {
                DataGridViewRow DGVrow = view.Rows[e.RowIndex] as DataGridViewRow;
                DGVrow.DefaultCellStyle.BackColor = Color.LightSkyBlue;
                xjxList.Clear();
                Dictionary<string, string> conditions = new Dictionary<string, string>();
                conditions.Add("Inquiry_ID", xjdh_cmb.Text.ToString().Trim());
                conditions.Add("Supplier_ID", Supplier_view.Rows[e.RowIndex].Cells[1].Value.ToString());
                conditions.Add("", "  len(Material_ID) >0 ");
                xjxList = enquiryBLL.findEnquiry(conditions, "Enquiry");
                if (xjxList != null)
                {
                    DGVnumber = xjxList.Count;
                    viewRemove(Inquiry_view);
                    for (int i = 0; i < xjxList.Count; i++)
                    {
                        Inquiry_view.Rows[i].Cells[0].Value = i.ToString();
                        Inquiry_view.Rows[i].Cells[1].Value = xjxList.ElementAt(i).Material_ID;
                        CellEndEdit(Inquiry_view, i, 1);
                        Inquiry_view.Rows[i].Cells[4].Value = xjxList.ElementAt(i).Number;
                        Inquiry_view.Rows[i].Cells[5].Value = xjxList.ElementAt(i).Price;
                        Inquiry_view.Rows[i].Cells[7].Value = xjxList.ElementAt(i).Delivery_Time.ToString();
                        Inquiry_view.Rows[i].Cells[9].Value = xjxList.ElementAt(i).Factory_ID;
                        Inquiry_view.Rows[i].Cells[10].Value = xjxList.ElementAt(i).Stock_ID;
                        Inquiry_view.Rows[i].Cells[11].Value = xjxList.ElementAt(i).Clause_ID;
                        Inquiry_view.Rows[i].Cells[12].Value = xjxList.ElementAt(i).Condition_ID;
                    }
                }
                else
                {
                    DGVrow.DefaultCellStyle.BackColor = Color.White;
                }
                return;
            }
        }
        private void Inquiry_view_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            //CellValueChanged(Inquiry_view, sender, e);
        }
        private void Supplier_view_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            //CellValueChanged(Supplier_view, sender, e);
        }
        private void Supplier_view_Click(object sender, EventArgs e)
        {
            Supplier_view_Click(Supplier_view.CurrentCell.RowIndex);
            if (Supplier_view.CurrentCell.ColumnIndex == 8)
            {
                Offer_Click(Supplier_view.CurrentCell.RowIndex);
            }
        }
        /// <summary>
        /// 根据供应商列表选中行更新报价信息
        /// </summary>
        /// <param name="rowindex"></param>
        private void Supplier_view_Click(int rowindex)
        {
            xjxList.Clear();
            Dictionary<string, string> conditions = new Dictionary<string, string>();
            try 
            {
                conditions.Add("Inquiry_ID", xjdh_cmb.Text.ToString().Trim());
                conditions.Add("Supplier_ID", Supplier_view.Rows[rowindex].Cells[1].Value.ToString());
            }
            catch(Exception e)
            {} 
            conditions.Add("", "  len(Material_ID) >0 ");
            xjxList = enquiryBLL.findEnquiry(conditions, "Enquiry");
            if (xjxList != null)
            {
                DGVnumber = xjxList.Count;
                viewRemove(Inquiry_view);
                for (int i = 0; i < xjxList.Count; i++)
                {
                    Inquiry_view.Rows[i].Cells[0].Value = i.ToString();
                    Inquiry_view.Rows[i].Cells[1].Value = xjxList.ElementAt(i).Material_ID;
                    CellEndEdit(Inquiry_view, i, 1);
                    Inquiry_view.Rows[i].Cells[4].Value = xjxList.ElementAt(i).Number;
                    Inquiry_view.Rows[i].Cells[5].Value = xjxList.ElementAt(i).Price;
                    Inquiry_view.Rows[i].Cells[7].Value = xjxList.ElementAt(i).Delivery_Time.ToString();
                    Inquiry_view.Rows[i].Cells[9].Value = xjxList.ElementAt(i).Factory_ID;
                    Inquiry_view.Rows[i].Cells[10].Value = xjxList.ElementAt(i).Stock_ID;
                    Inquiry_view.Rows[i].Cells[11].Value = xjxList.ElementAt(i).Clause_ID;
                    Inquiry_view.Rows[i].Cells[12].Value = xjxList.ElementAt(i).Condition_ID;
                    Inquiry_view.Rows[i].Cells[13].Value = xjxList.ElementAt(i).State;
                    DGVRowChangeColor(Inquiry_view.Rows[i]);
                }
            }
        }

        /// <summary>
        /// 报价单信息
        /// </summary>
        /// <param name="rowindex"></param>
        private void Offer_Click(int rowindex)
        {
            if (Supplier_view.Rows[rowindex].Cells[8].Value == null
                || Supplier_view.Rows[rowindex].Cells[8].Value.ToString().Equals(""))
            {
                OpenFileDialog fileDialog = new OpenFileDialog();
                fileDialog.RestoreDirectory = true;
                fileDialog.Filter = "Excel文件(*.xls,xlsx)|*.xls;*.xlsx";
                fileDialog.FilterIndex = 1;
                if (fileDialog.ShowDialog() == DialogResult.OK)
                {
                    string newName = tools.systemTimeToStr();
                    string oldPath = fileDialog.FileName;
                    string fileFormat = oldPath.Substring(oldPath.LastIndexOf(".")); //获取文件格式
                    String FilePath = oldPath.Substring(0, oldPath.LastIndexOf("\\")+1);//获取文件路径，不带文件名
                    string newPath = FilePath + newName + fileFormat;
                    // 改名方法
                    FileInfo fi = new FileInfo(oldPath);
                    fi.MoveTo(Path.Combine(newPath));
                    //文档路径写入数据库
                    List<Enquiry> supplier = new List<Enquiry>();
                    gysList.ElementAt(rowindex).Price = newPath;
                    supplier.Add(gysList.ElementAt(rowindex));
                    int result = enquiryBLL.updateEnquiryOffer(supplier, inquiry_table);
                    if (result > 0)
                        Supplier_view.Rows[rowindex].Cells[8].Value = newPath;
                    else
                        MessageBox.Show("报价单写入数据库失败！");
                }
            }
            else 
            {
                System.Diagnostics.Process.Start(gysList.ElementAt(rowindex).Price);
                //判断当前供应商是否已报价
                bool isNotOffer = false;
                for (int i = 0; i < xjxList.Count; i++)
                {
                    if (Inquiry_view.Rows[i].Cells[5].Value == null ||
                        Inquiry_view.Rows[i].Cells[5].Value.ToString().Equals(""))
                    {
                        isNotOffer = true;
                        break;
                    }
                }
                if (isNotOffer)
                {
                    DialogResult result = MessageBox.Show("报价单写入报价请按确认键，不写入请按取消键!", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Asterisk);
                    if (result == DialogResult.OK)
                    {
                        Workbook excel = new Workbook(gysList.ElementAt(rowindex).Price);
                        Worksheet sheet = excel.Worksheets[0];
                        Cells cells = sheet.Cells;
                        int rowcount = cells.MaxRow;
                        int columncount = cells.MaxColumn;
                        for (int i = 0; i < xjxList.Count; i++)
                        {
                            Inquiry_view.Rows[i].Cells[5].Value = cells[3 + i, 4].StringValue.Trim();
                        }
                        saveOffer();
                    }
                }
            }
        }

        #region 保存按钮事件

        // 报价批量保存按钮事件
        private void saveOfferButton_Click(object sender, EventArgs e)
        {
            saveOffer();
        }
        /// <summary>
        /// 报价批量保存
        /// </summary>
        private void saveOffer()
        {
            for (int i = 0; i < xjxList.Count; i++)
            {
                xjxList.ElementAt(i).Price = tools.boxToString(Inquiry_view.Rows[i].Cells[5]);
                if (Inquiry_view.Rows[i].Cells[1].Value != null && Inquiry_view.Rows[i].Cells[1].Value.ToString().Equals(""))
                    xjxList.ElementAt(i).Delivery_Time = tools.boxToTime(Inquiry_view.Rows[i].Cells[1]);
                xjxList.ElementAt(i).Clause_ID = "";
                xjxList.ElementAt(i).Condition_ID = "";
                if (!xjxList.ElementAt(i).Price.Equals(""))
                    xjxList.ElementAt(i).State = "已报价";
                else
                    xjxList.ElementAt(i).State = "待报价";
                xjxList.ElementAt(i).Update_Time = DateTime.Now;
            }
            int result = enquiryBLL.updateEnquiryOffer(xjxList, inquiry_table);
            if (result > 0)
            {
                MessageBox.Show("报价单" + inquiry_table.Source_ID + "已保存");
                for (int i = 0; i < xjxList.Count; i++)
                {
                    if (!xjxList.ElementAt(i).Price.Equals(""))
                        Inquiry_view.Rows[i].Cells[13].Value = "已报价";
                    else
                        Inquiry_view.Rows[i].Cells[13].Value = "待报价";
                    DGVRowChangeColor(Inquiry_view.Rows[i]);
                }
                if (enquiryBLL.enquiryCount(xjxList.ElementAt(0)) > 0)
                    inquiry_table.State = 2;
                else
                    inquiry_table.State = 3;
                enquiryBLL.updateInquiry_Table(inquiry_table);
            }
        }
        private void DGVRowChangeColor(DataGridViewRow dgvRow)
        {
            if (!dgvRow.Cells[5].Value.Equals(""))
                dgvRow.DefaultCellStyle.BackColor = Color.LightSkyBlue;
            else
                dgvRow.DefaultCellStyle.BackColor = Color.White;
        }
        
        #endregion

        //菜单栏panel1的状态控制
        private void panel1Contorls(PictureBox pb,Label lbl)
        {
            if (pb.Name.Equals("new_pb"))
                xjzt_cmb.Enabled = false;
            else
                xjzt_cmb.Enabled = true;
            for (int i = 0; i < panel1.Controls.Count; i++)
            {
                if (panel1.Controls[i].Width == 35)
                {
                    panel1.Controls[i].Width = 40;
                    panel1.Controls[i].Height = 40;
                }
                else if (panel1.Controls[i].Font.Bold)
                {
                    //文本加粗
                    panel1.Controls[i].Font = new System.Drawing.Font(panel1.Controls[i].Font, panel1.Controls[i].Font.Style & ~FontStyle.Bold);
                }
            }
            lbl.Font = new System.Drawing.Font(lbl.Font, lbl.Font.Style | FontStyle.Bold);
            pb.Width = 35;
            pb.Height = 35;
        }

        //点击查看按钮控件状态改变
        private void readClick()
        {
            panel1Contorls(read_pb, read_lbl);
            xjdh_Refresh();

            compare_panel.Visible = false;
            cgzz_cmb.Enabled = false;
            cgz_cmb.Enabled = false;
            xjsj_dt.Enabled = false;
            bjqx_dt.Enabled = false;
            ckcg_button.Visible = false;

            xjzt_Changed();

            if (xjdh_cmb.Text == null || xjdh_cmb.Text.ToString().Equals("") || xjdh_cmb.Text.ToString().StartsWith("M"))
            {
                cgdh_cmb.Visible = false;
                cgdh_lbl.Visible = false;
            }
            else
            {
                cgdh_cmb.Visible = true;
                cgdh_lbl.Visible = true;
            }

            Inquiry_view.Enabled = false;
        }
        private void read_pb_Click(object sender, EventArgs e)
        {
            readClick();
        }
        //点击更改按钮保存按钮状态改变为可操作
        private void edit_pb_Click(object sender, EventArgs e)
        {
            isXJDBTsaved = false;
            isXJDsaved = false;
            for (int i = 0; i < this.Controls.Count; i++)
            {
                this.Controls[i].Enabled = true;
            }
            panel1Contorls(edit_pb, edit_lbl);
            //xjzt_Changed();
            ckcg_button.Visible = false;

            compare_panel.Visible=true;
            addCompare_view();
        }
        /// <summary>
        /// 加载比价表格
        /// </summary>
        private void addCompare_view()
        {
            compare_View.EndEdit();
            for (int i = compare_View.Rows.Count; i > 0; i--)
            {
                compare_View.Rows.RemoveAt(i - 1);
            }
            for (int i = compare_View.Columns.Count; i > 0; i--)
            {
                compare_View.Columns.RemoveAt(i - 1);
            }
            //compare_View.AutoGenerateColumns = false;
            compare_View.AutoSize = false;
            //view.RowHeadersVisible = false;
            //view.GridColor = System.Drawing.ColorTranslator.FromHtml("#F8F8FF");
            compare_View.ColumnHeadersHeight = 60;
            //compare_View.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.None;
            compare_View.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.Single;
            compare_View.EditMode = DataGridViewEditMode.EditOnEnter;
            //compare_View.ReadOnly = true;

            DataGridViewTextBoxColumn textColumn = new DataGridViewTextBoxColumn();
            textColumn.Width = 120;
            compare_View.Columns.Add(textColumn);
            textColumn = new DataGridViewTextBoxColumn();
            textColumn.Width = 100;
            compare_View.Columns.Add(textColumn);
            for (int i = 0; i < gysList.Count; i++)
            {
                textColumn = new DataGridViewTextBoxColumn();
                textColumn.Width = 100;
                compare_View.Columns.Add(textColumn);
            }
            for (int i = 0; i < (xjxList.Count + 3) * 3; i++)
            {
                compare_View.Rows.Add();
                if (i % 3 == 0)
                    compare_View.Rows[i].DefaultCellStyle.BackColor = Color.GhostWhite;
                else if (i % 3 == 1)
                    compare_View.Rows[i].DefaultCellStyle.BackColor = Color.AliceBlue;
                else
                    compare_View.Rows[i].DefaultCellStyle.BackColor = Color.Azure;
            }
            addCompare_viewData();
        }

        /// <summary>
        /// 加载比价数据
        /// </summary>
        private void addCompare_viewData()
        {
            //第一列
            compare_View.Rows[0].Cells[0].Value = "物料";
            compare_View.Rows[1].Cells[0].Value = "文本";
            compare_View.Rows[2].Cells[0].Value = "基本单位中的数量";
            compare_View.Rows[(1+xjxList.Count)*3+1].Cells[0].Value = "总报价";
            //第二列
            compare_View.Rows[0].Cells[1].Value = "报价：";
            compare_View.Rows[1].Cells[1].Value = "投标人：";
            compare_View.Rows[2].Cells[1].Value = "投标邀请号：";
            for (int i = 0; i < xjxList.Count+1; i++)
            {
                compare_View.Rows[(i + 1) * 3 + 0].Cells[1].Value = "值：";
                compare_View.Rows[(i + 1) * 3 + 1].Cells[1].Value = "价格：";
                compare_View.Rows[(i + 1) * 3 + 2].Cells[1].Value = "排列：";
            }
            //供应商信息
            for (int i = 0; i < gysList.Count; i++)
            {
                if (gysList.ElementAt(i).Price == null || gysList.ElementAt(i).Price.Equals(""))
                    compare_View.Rows[0].Cells[i + 2].Value = "";
                else
                    compare_View.Rows[0].Cells[i + 2].Value = gysList.ElementAt(i).Price;
                        //.Substring(gysList.ElementAt(i).Price.LastIndexOf("\\") + 1, gysList.ElementAt(i).Price.LastIndexOf("."));
                compare_View.Rows[1].Cells[i + 2].Value = gysList.ElementAt(i).Supplier_ID;
                compare_View.Rows[2].Cells[i + 2].Value = gysList.ElementAt(i).Inquiry_ID;
            }
            //物料信息
            for (int i = 0; i < xjxList.Count; i++)
            {
                compare_View.Rows[(i + 1) * 3 + 0].Cells[0].Value = Inquiry_view.Rows[i].Cells[1].Value.ToString().Trim();
                compare_View.Rows[(i + 1) * 3 + 1].Cells[0].Value = Inquiry_view.Rows[i].Cells[2].Value.ToString().Trim();
                compare_View.Rows[(i + 1) * 3 + 2].Cells[0].Value =
                    Inquiry_view.Rows[i].Cells[4].Value.ToString().Trim() + "  " + Inquiry_view.Rows[i].Cells[6].Value.ToString().Trim();
                compare_View.Rows[(i + 1) * 3 + 2].Cells[0].Style.Alignment = DataGridViewContentAlignment.MiddleRight;
            }
            //值、价格
            for (int i = 0; i < gysList.Count; i++)
            {
                Supplier_view_Click(i);
                for (int j = 0; j < xjxList.Count; j++)
                {
                    compare_View.Rows[(j + 1) * 3 + 0].Cells[i+2].Style.Alignment = DataGridViewContentAlignment.MiddleRight;
                    compare_View.Rows[(j + 1) * 3 + 1].Cells[i + 2].Style.Alignment = DataGridViewContentAlignment.MiddleRight;
                    compare_View.Rows[(j + 1) * 3 + 0].Cells[i + 2].Value =
                        xjxList.ElementAt(j).Number * Math.Round(tools.strToDouble(xjxList.ElementAt(j).Price), 2);
                    if (compare_View.Rows[(j + 1) * 3 + 0].Cells[i + 2].Value.ToString().Equals("0"))
                        compare_View.Rows[(j + 1) * 3 + 0].Cells[i + 2].Value = "";
                    compare_View.Rows[(j + 1) * 3 + 1].Cells[i+2].Value = xjxList.ElementAt(j).Price;   
                }
            }

            //排列
            double[,] percent = new double[xjxList.Count + 1, gysList.Count];
            int[,] rank = new int[xjxList.Count + 1, gysList.Count];
            //计算百分比
            for (int i = 0; i < xjxList.Count; i++)
            {
                int count = 0;
                double sum = 0.0;
                for (int j = 0; j < gysList.Count; j++)
                {
                    if (tools.boxToDouble(compare_View.Rows[(i + 1) * 3 + 0].Cells[j + 2]) > 0)
                    {
                        count++;
                        sum += tools.boxToDouble(compare_View.Rows[(i + 1) * 3 + 0].Cells[j + 2]);
                    }
                }
                if (count > 0)
                {
                    sum = sum / (double)count;
                    for(int j = 0; j < gysList.Count; j++)
                    {
                        percent[i, j] = tools.boxToDouble(compare_View.Rows[(i + 1) * 3 + 0].Cells[j + 2]) / sum;
                    }
                }
            }
            //计算报价总和
            double[] supplierSum = new double[gysList.Count];
            for (int j = 0; j < gysList.Count; j++)
            {
                supplierSum[j] = 0.0;
                for (int i = 0; i < xjxList.Count; i++)
                {
                    supplierSum[j] += tools.boxToDouble(compare_View.Rows[(i + 1) * 3 + 0].Cells[j + 2]);
                }
                if (supplierSum[j]<=0)
                    compare_View.Rows[(xjxList.Count + 1) * 3 + 0].Cells[j + 2].Value = "";
                else
                    compare_View.Rows[(xjxList.Count + 1) * 3 + 0].Cells[j + 2].Value = supplierSum[j];
                compare_View.Rows[(xjxList.Count + 1) * 3 + 0].Cells[j + 2].Style.Alignment = DataGridViewContentAlignment.MiddleRight;
            }
            int supCount = 0;
            double supSum = 0.0;
            for (int j = 0; j < gysList.Count; j++)
            {
                if (supplierSum[j] > 0)
                {
                    supCount++;
                    supSum += supplierSum[j];
                }
            }
            if (supCount > 0)
            {
                supSum = supSum / (double)supCount;
                for (int j = 0; j < gysList.Count; j++)
                {
                    percent[xjxList.Count, j] = tools.boxToDouble(compare_View.Rows[(xjxList.Count + 1) * 3 + 0].Cells[j + 2]) / supSum;
                }
            }
            //排名
            for (int i = 0; i < xjxList.Count+1; i++)
            {
                for (int j = 0; j < gysList.Count; j++)
                {
                    rank[i,j]=1;
                    for (int k = 0; k < gysList.Count; k++)
                    {
                        if (percent[i, j] > percent[i, k])
                            rank[i, j]++;
                    }
                }
            }
            //显示排列
            for (int i = 0; i < xjxList.Count+1; i++)
            {
                for (int j = 0; j < gysList.Count; j++)
                {
                    compare_View.Rows[(i + 1) * 3 + 2].Cells[j + 2].Style.Alignment = DataGridViewContentAlignment.MiddleRight;
                    if (percent[i, j] > 0)
                        compare_View.Rows[(i + 1) * 3 + 2].Cells[j + 2].Value =
                            rank[i, j] + "    " + Math.Round(percent[i, j],2)*100 + " " + "%";
                    else
                        compare_View.Rows[(i + 1) * 3 + 2].Cells[j + 2].Value = "";
                    if (rank[i, j] == 1)
                        compare_View.Rows[(i + 1) * 3 + 2].Cells[j + 2].Style.ForeColor = Color.Red;
                }
            }


        }

        //关闭form事件
        private void close_pb_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// 删除行按钮事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void viewRemove(DataGridView view)
        {
            for (int i = 0; i < view.Rows.Count; i++)
            {
                for (int j = 1; j < view.Columns.Count; j++)
                {
                    view.Rows[i].Cells[j].Value = "";
                }
            }
            if (view.Rows.Count > DGVnumber)
            {
                for (int i = view.Rows.Count; i > DGVnumber; i--)
                {
                    view.Rows.RemoveAt(i-1);
                }
            }
            else
            {
                for (int i = view.Rows.Count; i < DGVnumber; i++)
                {
                    view.Rows.Add();
                }
            }
        }

        //询价单号选值改变时读取数据
        private void xjdh_cmb_SelectedIndexChanged(object sender, EventArgs e)
        {
            xjdh_Refresh();
        }
        private void xjdh_Refresh()
        {
            Dictionary<string, string> conditions = new Dictionary<string,string>();
            if (!isFromSourceMM)
            {
                conditions.Add("Inquiry_ID", tools.cmbToString(xjdh_cmb));
                inquiry_table = enquiryBLL.findInquiryTable(conditions, "Inquiry_Table");
            }
            else
            {
                conditions.Add("Inquiry_ID", tools.cmbToString(xjdh_cmb));
                isFromSourceMM = false;
            }

            if (inquiry_table == null)
            {
                cgdh_cmb.Text = "";
                cgzz_cmb.Text = "";
                cgz_cmb.Text = "";
                xjsj_dt.Value = DateTime.Now;
                bjqx_dt.Value = DateTime.Now;
                inquiry_table = new SourceInquiry();
                enquiryList.Clear();
                xjxList.Clear();
                gysList.Clear();
                viewRemove(Supplier_view);
                viewRemove(Inquiry_view);
                return;
            }
            xjdh_cmb.Text = inquiry_table.Source_ID;
            cgdh_cmb.Visible = true;
            cgdh_lbl.Visible = true;
            /*
            if (inquiry_table.Inquiry_Name.StartsWith("A"))
            {
                cgdh_cmb.Visible = true;
                cgdh_lbl.Visible = true;
            }
            else
            {
                cgdh_cmb.Visible = false;
                cgdh_lbl.Visible = false;
            }
             * */
            xjzt_cmb.Text = inquiry_table.State.ToString();
            cgzz_cmb.Text = inquiry_table.Transaction_Type;
            bjqx_dt.Value=inquiry_table.Offer_Time;
            //加载供应商列表
            conditions.Add("State", "供应商");
            gysList = enquiryBLL.findEnquiry(conditions, "Enquiry");
            if (gysList != null)
            {
                DGVnumber = gysList.Count;
                viewRemove(Supplier_view);
                for (int i = 0; i < gysList.Count; i++)
                {
                    Supplier_view.Rows[i].Cells[0].Value = i.ToString();
                    Supplier_view.Rows[i].Cells[1].Value = gysList.ElementAt(i).Supplier_ID;
                    Supplier_view.Rows[i].Cells[8].Value = gysList.ElementAt(i).Price;
                    Supplier_view.Rows[i].Cells[9].Value = gysList.ElementAt(i).Clause_ID;
                    Supplier_view.Rows[i].Cells[10].Value = gysList.ElementAt(i).Condition_ID;
                    CellEndEdit(Supplier_view, i, 1);
                }
                if (Supplier_view.CurrentCell != null)
                {
                    Supplier_view.Focus();
                    Supplier_view.CurrentCell = Supplier_view.Rows[0].Cells[0];
                }
                Supplier_view_Click(0);
                addCompare_view();
            }
        }


        //询价单表头状态改变
        private void xjzt_cmb_SelectedIndexChanged(object sender, EventArgs e)
        {
            xjzt_Changed();
        }
        private void xjzt_Changed()
        {
            if (xjzt_cmb.Text == null || xjzt_cmb.Text.ToString().Trim().Equals(""))
                return;
            else
            {
                xjdh_cmb.Items.Clear();
                tools.addItemsToCMB(xjdh_cmb, xjzt_cmb.Text.ToString().Trim(), "State", "Inquiry_ID", "Inquiry_Table");
            }
            
            if (read_pb.Width == 35) 
            {
                if (xjzt_cmb.Text.ToString().Trim().Equals("生成询价"))
                {
                    send_button.Enabled = false;
                    send_button.Visible = false;
                    scxj_button.Enabled = true;
                    scxj_button.Visible = true;
                    ckxj_button.Visible = false;
                    ckxj_button.Enabled = false;
                }
                else if (xjzt_cmb.Text.ToString().Trim().Equals("待邀请"))
                {
                    send_button.Enabled = true;
                    send_button.Visible = true;
                    scxj_button.Enabled = false;
                    scxj_button.Visible = false;
                    ckxj_button.Visible = true;
                    ckxj_button.Enabled = true;
                }
                else if (xjzt_cmb.Text.ToString().Trim().Equals("待报价"))
                {
                    send_button.Enabled = false;
                    send_button.Visible = false;
                    scxj_button.Enabled = false;
                    scxj_button.Visible = false;
                    ckxj_button.Visible = true;
                    ckxj_button.Enabled = true;
                }
                else
                {
                    send_button.Enabled = false;
                    send_button.Visible = false;
                    scxj_button.Enabled = false;
                    scxj_button.Visible = false;
                    ckxj_button.Visible = false;
                    ckxj_button.Enabled = false;
                }
            }
            else
            {
                send_button.Enabled = false;
                send_button.Visible = false;
                scxj_button.Enabled = false;
                scxj_button.Visible = false;
                ckxj_button.Visible = false;
                ckxj_button.Enabled = false;
            }
        }

        /// <summary>
        /// 根据寻源编号打开询价单
        /// </summary>
        /// <param name="inquiryTable"></param>
        public void openByXJDH(SourceInquiry inquiryTable)
        {
            isFromSourceMM = true;
            inquiry_table = inquiryTable;
            this.xjdh_cmb.Text = inquiryTable.Source_ID;
        }

        public void Export()
        {
            Workbook workbook = new Workbook();
            Worksheet sheet = (Worksheet)workbook.Worksheets[0];//表0
            sheet.Name = "汉默特询价单" + xjdh_cmb.Text.ToString().Trim();
            Cells cells = sheet.Cells;//单元格
            for (int i = 0; i < 10; i++)
            {
                if (i == 0)
                    cells.SetColumnWidthPixel(i, 105);//设置列宽
                else if (i == 1)
                    cells.SetColumnWidthPixel(i, 115);//设置列宽
                else if (i == 2)
                    cells.SetColumnWidthPixel(i, 90);//设置列宽
                else if (i == 3)
                    cells.SetColumnWidthPixel(i, 65);//设置列宽
                else
                    cells.SetColumnWidthPixel(i, 80);//设置列宽
            }
            for (int j = 0; j < xjxList.Count + gysList.Count + 6; j++)
            {
                cells.SetRowHeightPixel(j, 25);//设置行高 
                if (j == 2)
                {
                    for (int i = 0; i < 10; i++)
                        sheet.Cells[j,i].SetStyle(tools.SettingCellStyle(workbook, cells, true));
                }
                else
                {
                    for (int i = 0; i < 10; i++)
                        sheet.Cells[j,i].SetStyle(tools.SettingCellStyle(workbook, cells, false));
                }
            }

            sheet.Cells[0, 0].SetStyle(tools.SettingCellStyle(workbook, cells, true));
            sheet.Cells[0, 2].SetStyle(tools.SettingCellStyle(workbook, cells, true));
            sheet.Cells[0, 4].SetStyle(tools.SettingCellStyle(workbook, cells, true));
            sheet.Cells[0, 6].SetStyle(tools.SettingCellStyle(workbook, cells, true));
            sheet.Cells[xjxList.Count + 4, 0].SetStyle(tools.SettingCellStyle(workbook, cells, true));
            sheet.Cells[xjxList.Count + 4, 1].SetStyle(tools.SettingCellStyle(workbook, cells, true));

            sheet.Cells[0, 0].PutValue("询价单号");
            sheet.Cells[0, 1].PutValue(inquiry_table.Source_ID);
            sheet.Cells[0, 2].PutValue("采购组织");
            sheet.Cells[0, 3].PutValue(inquiry_table.Transaction_Type);
            sheet.Cells[0, 4].PutValue("报价期限");
            sheet.Cells[0, 5].PutValue(inquiry_table.Offer_Time.ToString().Trim());

            sheet.Cells[2, 0].PutValue("物料编号");
            sheet.Cells[2, 1].PutValue("物料名称");
            sheet.Cells[2, 2].PutValue("物料标准");
            sheet.Cells[2, 3].PutValue("询价数量");
            sheet.Cells[2, 4].PutValue("价格");
            sheet.Cells[2, 5].PutValue("单位");
            sheet.Cells[2, 6].PutValue("交货日期");
            sheet.Cells[2, 7].PutValue("备注1");
            sheet.Cells[2, 8].PutValue("备注2");
            sheet.Cells[2, 9].PutValue("备注3");
            for (int j = 0; j < xjxList.Count; j++)
            {
                sheet.Cells[j + 3, 0].PutValue(tools.boxToString(Inquiry_view.Rows[j].Cells[1]));
                sheet.Cells[j + 3, 1].PutValue(tools.boxToString(Inquiry_view.Rows[j].Cells[2]));
                sheet.Cells[j + 3, 2].PutValue(tools.boxToString(Inquiry_view.Rows[j].Cells[3]));
                sheet.Cells[j + 3, 3].PutValue(tools.boxToString(Inquiry_view.Rows[j].Cells[4]));
                sheet.Cells[j + 3, 4].PutValue("");
                sheet.Cells[j + 3, 5].PutValue(tools.boxToString(Inquiry_view.Rows[j].Cells[5]));
                sheet.Cells[j + 3, 6].PutValue(tools.boxToString(Inquiry_view.Rows[j].Cells[6]));
                sheet.Cells[j + 3, 7].PutValue("");
                sheet.Cells[j + 3, 8].PutValue("");
                sheet.Cells[j + 3, 9].PutValue("");
            }
            sheet.Cells[xjxList.Count + 4, 0].PutValue("供应商编号");
            sheet.Cells[xjxList.Count + 4, 1].PutValue("供应商名称");
            for (int j = 0; j < gysList.Count; j++)
            {
                sheet.Cells[j + xjxList.Count + 6, 0].PutValue(tools.boxToString(Supplier_view.Rows[j].Cells[1]));
                sheet.Cells[j + xjxList.Count + 6, 1].PutValue(tools.boxToString(Supplier_view.Rows[j].Cells[2]));
            }

            SaveFileDialog frm = new SaveFileDialog();
            frm.RestoreDirectory = true;//保存上次打开的路径
            frm.FileName = inquiry_table.Source_ID;
            frm.Filter = "Excel文件(*.xls,xlsx)|*.xls;*.xlsx";
            //点了保存按钮进入 
            string localFilePath = null;
            if (frm.ShowDialog() == DialogResult.OK)
            {
                localFilePath = frm.FileName.ToString(); //获得文件路径 
                string fileNameExt = localFilePath.Substring(localFilePath.LastIndexOf("\\") + 1); //获取文件名，不带路径
                String FilePath = localFilePath.Substring(0, localFilePath.LastIndexOf("\\"));//获取文件路径，不带文件名 
                //fileNameExt = DateTime.Now.ToString() + fileNameExt;//给文件名前加上时间 
                //saveFileDialog1.FileName.Insert(1,"dameng");//在文件名里加字符 
                //System.IO.FileStream fs = (System.IO.FileStream)sfd.OpenFile();//输出文件
                workbook.Save(@localFilePath);
                MessageBox.Show("文件 " + fileNameExt + " 已成功保存至:" + FilePath, "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                localFilePath = "E:\\" + inquiry_table.Source_ID + ".xls";
                workbook.Save(@"E:\\" + ".xls");
                MessageBox.Show("文件已成功保存至: " + "E:\\", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                
            }
            inquiry_table.Inquiry_Path = localFilePath;
            inquiry_table.State = 1;
            int result = enquiryBLL.updateInquiry_Table(inquiry_table);
            if (result > 0)
            {
                MessageBox.Show("询价单保存成功！");
                xjzt_cmb.Text = "待邀请";
            }

        }

        //向供应商发送邀请
        private void send_button_Click(object sender, EventArgs e)
        {
            MailMessage message = new MailMessage();
            //获取供应商列表中的邮件地址
            string address = null;
            for (int i = 0; i < gysList.Count; i++)
            {
                address = Supplier_view.Rows[i].Cells[4].Value.ToString().Trim();
                if (tools.isMailAddress(address))
                    message.To.Add(address);
            }
            Attachment att = new Attachment(inquiry_table.Inquiry_Path);
            message.Attachments.Add(att);//添加附件
            bool send = tools.SendEmail(message);
            inquiry_table.State = 2;
            int result = enquiryBLL.updateInquiry_Table(inquiry_table);
            if (result > 0 && send)
            {
                MessageBox.Show("询价发送成功！");
                xjzt_cmb.Text = "待报价";
                send_button.Visible = false;
                ckxj_button.Visible = true;
                ckxj_button.Enabled = true;
            }
            else
            {
                MessageBox.Show("询价发送失败！");
            }
        }

        //生成询价单
        private void scxj_button_Click(object sender, EventArgs e)
        {
            Export();
        }

        private void ckxj_button_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start(inquiry_table.Inquiry_Path);
        }

        

        

    }
}
