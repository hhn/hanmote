﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Bll.SourcingManage.SourcingManagement;
using WeifenLuo.WinFormsUI.Docking;
using Lib.Model.SourcingManage.SourcingManagement;

namespace MMClient.SourcingManage.SourcingManagement
{
    public partial class ReleaseBid_Form : DockContent
    {
        private UserUI userUI;
        private SourceBid bid;

        private SourceBLL sourceBll = new SourceBLL();
        private SourceBidBLL bidBLL = new SourceBidBLL();
        private BidPartnerBLL bidPartnerBLL = new BidPartnerBLL();
        private SourceSupplierBLL sourceSupplierBll = new SourceSupplierBLL();
        private SourceMaterialBLL sourceMaterialBll = new SourceMaterialBLL();

        public ReleaseBid_Form()
        {
            InitializeComponent();
        }

        public ReleaseBid_Form(UserUI userUI, SourceBid bid)
        {
            InitializeComponent();
            this.userUI = userUI;
            this.bid = bid;
            init();
        }

        private void init()
        {
            initData(bid);
        }

        private void initData(SourceBid bid)
        {
            //招标基本信息
            txt_BidId.Text = bid.BidId;
            Source source = sourceBll.findSourceById(bid.BidId);
            txt_BidName.Text = source.Source_Name;
            txt_State.Text = getStateByInt(bid.BidState);
            txt_serviceType.Text = bid.ServiceType;
            txt_catalogue.Text = bid.Catalogue;
            txt_displayType.Text = bid.DisplayType;
            txt_transType.Text = bid.TransType;
            if (bid.TransType.Equals("谈判"))
            {
                btn_WeightSave.Visible = false;
                btn_Transact.Visible = true;
            }
            else if (bid.TransType.Equals("等级"))
            {
                btn_WeightSave.Visible = true;
                btn_Transact.Visible = false;
            }
            txt_purchaseOrg.Text = source.PurchaseOrg;
            txt_purchaseGroup.Text = source.PurchaseGroup;
            txt_StartTime.Text = bid.StartTime.ToString("yyyy-MM-dd HH:mm:ss");
            txt_EndTime.Text = bid.EndTime.ToString("yyyy-MM-dd HH:mm:ss");
            txt_startBeginTime.Text = bid.StartBeginTime.ToString("yyyy-MM-dd HH:mm:ss");
            txt_limitTime.Text = bid.LimitTime.ToString("yyyy-MM-dd");
            txt_timeZone.Text = bid.TimeZone.ToString();
            txt_currency.Text = bid.Currency.ToString();
            txt_commBidStartDate.Text = bid.CommBidStartDate.ToString("yyyy-MM-dd");
            txt_commBidStartTime.Text = bid.CommBidStartTime.ToString("HH:mm:ss");
            txt_techBidStartDate.Text = bid.TechBidStartDate.ToString("yyyy-MM-dd");
            txt_techBidStartTime.Text = bid.TechBidStartTime.ToString("HH:mm:ss");
            txt_buyEndDate.Text = bid.BuyEndDate.ToString("yyyy-MM-dd");
            txt_buyEndTime.Text = bid.BuyEndTime.ToString("HH:mm:ss");
            txt_enrollEndDate.Text = bid.EnrollEndDate.ToString("yyyy-MM-dd");
            txt_enrollEndTime.Text = bid.EnrollEndTime.ToString("HH:mm:ss");
            txt_enrollStartDate.Text = bid.EnrollStartDate.ToString("yyyy-MM-dd");
            txt_enrollStartTime.Text = bid.EnrollStartTime.ToString("HH:mm:ss");
            txt_bidCost.Text = bid.BidCost.ToString();
            //合作伙伴
            BidPartner bp = bidPartnerBLL.findBidPartnerByBidId(bid.BidId);
            if (bp != null)
            {
                txt_requesterId.Text = bp.RequesterId.ToString();
                txt_requesterName.Text = bp.RequesterName.ToString();
                txt_receiverId.Text = bp.ReceiverId.ToString();
                txt_receiverName.Text = bp.ReceiverName.ToString();
                txt_dropPointId.Text = bp.DropPointId.ToString();
                txt_dropPointName.Text = bp.DropPointName.ToString();
                txt_reviewerId.Text = bp.ReviewerId.ToString();
                txt_ReviewerName.Text = bp.ReviewerName.ToString();
                txt_callerId.Text = bp.CallerId.ToString();
                txt_callerName.Text = bp.CallerName.ToString();
            }
            //投标人
            List<SourceSupplier> list = sourceSupplierBll.getBidSuppliersByBidId(bid.BidId);
            SupplierGridView.Rows.Clear();
            if (list.Count > SupplierGridView.Rows.Count)
            {
                SupplierGridView.Rows.Add(list.Count - SupplierGridView.Rows.Count);
            }
            for (int i = 0; i < list.Count; i++)
            {
                SupplierGridView.Rows[i].Cells["bidSupplierId"].Value = list.ElementAt(i).Supplier_ID;
                SupplierGridView.Rows[i].Cells["bidSupplierName"].Value = list.ElementAt(i).Supplier_Name;
                SupplierGridView.Rows[i].Cells["bidClassificationResult"].Value = list.ElementAt(i).ClassificationResult;
                SupplierGridView.Rows[i].Cells["bidContact"].Value = list.ElementAt(i).Contact;
                SupplierGridView.Rows[i].Cells["bidEmail"].Value = list.ElementAt(i).Email;
                SupplierGridView.Rows[i].Cells["bidAddress"].Value = list.ElementAt(i).Address;
                SupplierGridView.Rows[i].Cells["bidState"].Value = list.ElementAt(i).State;
            }
            //评价标准
            tab_EvalMethod.SelectedIndex = bid.EvalMethId;
            txt_EvalMethod.Text = tab_EvalMethod.SelectedTab.Text;
            if (bid.EvalMethId == 0)
            {
                tab_WeightScore.Parent = null;
                tab_LeastTotalCost.Parent = null;
                tab_ValueAsses.Parent = null;
            }
            else if (bid.EvalMethId == 1)//加权评分法
            {
                tab_Lowprice.Parent = null;
                tab_LeastTotalCost.Parent = null;
                tab_ValueAsses.Parent = null;
                /*
                List<BidWeightScore> listBws = bidWeightScoreBLL.findWeightScoresByBidId(bid.BidId);
                SupplierScoreGridView.Rows.Clear();
                if (listBws.Count > SupplierScoreGridView.Rows.Count)
                {
                    SupplierScoreGridView.Rows.Add(listBws.Count - SupplierScoreGridView.Rows.Count);
                }
                for (int i = 0; i < listBws.Count; i++)
                {
                    SupplierScoreGridView.Rows[i].Cells["weightName"].Value = listBws.ElementAt(i).WeightScoreName;
                    SupplierScoreGridView.Rows[i].Cells["weightNum"].Value = listBws.ElementAt(i).WeightScoreNum;
                }
                 * **/
            }
            else if (bid.EvalMethId == 2)
            {
                tab_Lowprice.Parent = null;
                tab_WeightScore.Parent = null;
                tab_ValueAsses.Parent = null;
            }
            else if (bid.EvalMethId == 3)
            {
                tab_Lowprice.Parent = null;
                tab_WeightScore.Parent = null;
                tab_LeastTotalCost.Parent = null;
            }

            //项目数据
            List<SourceMaterial> listMaterials = sourceMaterialBll.findSourceMaterialsBySourceId(bid.BidId);

            MaterialGridview.Rows.Clear();
            if (listMaterials.Count > MaterialGridview.Rows.Count)
            {
                MaterialGridview.Rows.Add(listMaterials.Count - MaterialGridview.Rows.Count);
            }
            for (int i = 0; i < listMaterials.Count; i++)
            {
                MaterialGridview.Rows[i].Cells["bidMaterialId"].Value = listMaterials.ElementAt(i).Material_ID;
                MaterialGridview.Rows[i].Cells["bidMaterialName"].Value = listMaterials.ElementAt(i).Material_Name;
                MaterialGridview.Rows[i].Cells["bidDemandCount"].Value = listMaterials.ElementAt(i).Demand_Count;
                MaterialGridview.Rows[i].Cells["bidDemandMeasurement"].Value = listMaterials.ElementAt(i).Unit;
                MaterialGridview.Rows[i].Cells["factoryId"].Value = listMaterials.ElementAt(i).FactoryId;
                MaterialGridview.Rows[i].Cells["stockId"].Value = listMaterials.ElementAt(i).StockId;
                MaterialGridview.Rows[i].Cells["DeliveryStartTime"].Value = listMaterials.ElementAt(i).DeliveryStartTime.ToString("yyyy-MM-dd");
                MaterialGridview.Rows[i].Cells["DeliveryEndTime"].Value = listMaterials.ElementAt(i).DeliveryEndTime.ToString("yyyy-MM-dd");
            }
        }

        private string getStateByInt(int num)
        {
            string str = "";
            if (num == 0)
            {
                str = "待保存";
            }
            else if (num == 1)
            {
                str = "待发布";
            }
            else if (num == 2)
            {
                str = "待报价";
            }
            else if (num == 3)
            {
                str = "待评标";
            }
            else if (num == 4)
            {
                str = "已完成";
            }
            return str;
        }

        private void btn_Release_Click(object sender, EventArgs e)
        {
            bid.BidState = 2;
            bidBLL.updateBidState(bid);
            bidBLL.updateBidReleaseTime(bid.BidId);
            MessageBox.Show("招标发布成功");
            this.Close();
        }
    }
}
