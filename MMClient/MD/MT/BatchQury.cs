﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Bll;
using Lib.Common.CommonUtils;
using Lib.Common.MMCException.Bll;
using Lib.Common.MMCException.IDAL;
using Lib.SqlServerDAL;

namespace MMClient.MD.MT
{
    public partial class BatchQury : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        public BatchQury()
        {
            InitializeComponent();
        }

        private void btn_查询_Click(object sender, EventArgs e)
        {
            dgv_批次特性.DataSource = FindBatch_FeatureInforByPage(this.pageTool.PageSize, this.pageTool.PageIndex, this.tbx_批次编码.Text.ToString());
        }

        private void pageNext1_Load(object sender, EventArgs e)
        {
            string sql = "select count(*) from Batch_Feature";
            try
            {
                int i = int.Parse(Lib.SqlServerDAL.DBHelper.ExecuteQueryDT(sql).Rows[0][0].ToString());
                int totalNum = i;
                pageTool.DrawControl(totalNum);
                //事件改变调用函数
                pageTool.OnPageChanged += pageTool_OnPageChanged;
            }
            catch (BllException ex)
            {
                MessageUtil.ShowTips("数据库异常，请稍后重试");
                return;
            }
            
        }
        private void LoadData()
        {
            try
            {
                dgv_批次特性.DataSource = FindBatch_FeatureInforByPage(this.pageTool.PageSize, this.pageTool.PageIndex, this.tbx_批次编码.Text.ToString());
            }
            catch (BllException ex)
            {
                MessageUtil.ShowError(ex.Message);
            }
        }

        private object FindBatch_FeatureInforByPage(int pageSize, int pageIndex, string code)
        {
            try
            {
                string sql;
                if (code == "" || code == null)
                {
                    sql = @"SELECT top " + pageSize + "Batch_ID  ,Feature_Name ,Feature_Value FROM  Batch_Feature  where Batch_ID not in(select top " + pageSize * (pageIndex - 1) + " Batch_ID from Batch_Feature ORDER BY Batch_ID ASC)ORDER BY Batch_ID";
                }
                else
                {
                    sql = @"SELECT top " + pageSize + "Batch_ID  ,Feature_Name, Feature_Value FROM  Batch_Feature  where Batch_ID like '%" + code + "%' and Batch_ID not in(select top " + pageSize * (pageIndex - 1) + " Batch_ID from Batch_Feature ORDER BY Batch_ID ASC)ORDER BY Batch_ID";
                }

                return DBHelper.ExecuteQueryDT(sql);
            }
            catch (DBException ex)
            {
                throw new BllException("当前无数据", ex);
            }
        }

        private void pageTool_OnPageChanged(object sender, EventArgs e)
        {
            LoadData();
        }

        private void BatchQury_Load(object sender, EventArgs e)
        {
            LoadData(); 
        }
    }
}
