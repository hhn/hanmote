﻿using Lib.Bll.MDBll.General;
using Lib.Bll.MDBll.MT;
using Lib.Common.CommonUtils;
using Lib.Model.MD.MT;
using Lib.SqlServerDAL;
using MMClient.MD.FI;
using MMClient.MD.GN;
using MMClient.MD.MT;
using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;




namespace MMClient.MD
{
    public partial class MTMaintain : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        GeneralBLL gn = new GeneralBLL();
        ComboBoxItem cbm = new ComboBoxItem();
        MaterialBLL mtb = new MaterialBLL();
        FormHelper formh = new Lib.Common.CommonUtils.FormHelper();
        CmbAutoSize cas = new CmbAutoSize();
        string mtid;
        public MTMaintain()
        {
            InitializeComponent();
        }

        private void cbb_物料ID_SelectedIndexChanged(object sender, EventArgs e)
        {
            string str = cbb_物料编号.Text;
            string[] atrArr = str.Split(' ');
            cbb_物料编号.Text = atrArr[0];
            mtid = cbb_物料编号.Text;
            cbb_物料编号.Text = mtid;
            MaterialBase mrb = new MaterialBase();
            mrb = mtb.GetMaterialBasicInformation(mtid);
            BeginInvoke(new EventHandler(InvokeAfter), sender, e);
            dtp_起始期.Value = DateTime.Parse(mrb.EI_Period.ToString());
            cbb_权限组.Text = mrb.ID.ToString();
            cbb_物料名称.Text = mrb.Material_Name;
            cbb_计量单位.Text = mrb.Measurement;
            cbb_产品组.Text = mrb.Division;
            comboBox6.Text = mrb.Net_Weight.ToString();
            comboBox5.Text = mrb.Gross_Weight.ToString();
            comboBox4.Text = mrb.Order_Unit;
            comboBox3.Text = mrb.Variable_Unit;
            cbb_物料状态.Text = mrb.Material_Status;
            tbx_跨工厂物料状态.Text = mrb.Status_Information;
            tbx_行业标准描述.Text = mrb.Material_Standard;
            ckb_市场管制标识.Checked = mrb.Market_Regulation_Identity;
            cbb_物料级别.Text = mrb.Material_Level;
            ckb_生产性物料标识.Checked = mrb.Material_Purchase_Type;
           
        }

        /// <summary>
        /// 物料状态解释
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cbb_物料状态_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch ( cbb_物料状态.Text )
            {
　            case "01" :
                //不能下采购订单和库存移动
　　            tbx_跨工厂物料状态.Text = " 因采购/仓库而被冻结 ";
　　            break;
　            case "02" :
              //禁止与BOM有关的操作，库存可用，可采购
　　            tbx_跨工厂物料状态.Text = " 因任务空间/BOM而被冻结 ";
　　            break;
              case "DC":
              //禁止一切业务操作
                tbx_跨工厂物料状态.Text = " 失效状态 ";
                break;
              //新建物料，只可用于BOM的建立，不可进行其他业务操作
               case "EC":
               tbx_跨工厂物料状态.Text = " 初始状态 ";
               break;
　            default :
               //正常状态
               tbx_跨工厂物料状态.Text = " 正常状态 ";
　　            break;
            }
                    }

        private void MTMaintain_Load(object sender, EventArgs e)
        {
     
            List<string> list1 = gn.GetAllGroupName();
            cbm.FuzzyQury(cbb_外部物料组, list1);
            List<string> list3 = gn.GetAllMeasurement();
            cbm.FuzzyQury(cbb_计量单位, list3);
            cbm.FuzzyQury(comboBox4, list3);
            cbm.FuzzyQury(comboBox3, list3);
        }





        private void cbb_物料编号_TextChanged(object sender, EventArgs e)
        {
            string str = cbb_物料编号.Text;
            string[] atrArr = str.Split(' ');
            cbb_物料编号.Text = atrArr[0];
            mtid = cbb_物料编号.Text;
            cbb_物料编号.Text = mtid;
            MaterialBase mrb = new MaterialBase();
            mrb = mtb.GetMaterialBasicInformation(mtid);
            BeginInvoke(new EventHandler(InvokeAfter), sender, e);
            try
            {
                dtp_起始期.Value = mrb.EI_Period;
            }
            catch (Exception ex)
            {
                dtp_起始期.Value = Convert.ToDateTime("1900.1.1");
            }

            cbb_权限组.Text = mrb.ID.ToString();
            cbb_物料名称.Text = mrb.Material_Name;
            cbb_计量单位.Text = mrb.Measurement;
            cbb_产品组.Text = mrb.Division;
            comboBox6.Text = mrb.Net_Weight.ToString();
            comboBox5.Text = mrb.Gross_Weight.ToString();
            comboBox4.Text = mrb.Order_Unit;
            comboBox3.Text = mrb.Variable_Unit;
            cbb_物料状态.Text = mrb.Material_Status;
            tbx_跨工厂物料状态.Text = mrb.Status_Information;
            tbx_行业标准描述.Text = mrb.Material_Standard;
            ckb_市场管制标识.Checked = mrb.Market_Regulation_Identity;
            cbb_物料级别.Text = mrb.Material_Level;
            tbx_物料特性类.Text = mrb.Material_Class;
            ckb_生产性物料标识.Checked = mrb.Material_Purchase_Type;
            string sql = "SELECT * FROM [Material] where Material_ID='" + mtid + "' ";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            if(dt.Rows.Count<=0)
            {
                return;
            }
            if (dt.Rows[0][0] != null && dt.Rows[0][0].ToString() != "")
            {
                cbb_权限组.Text = dt.Rows[0][0].ToString();
            }
            if (dt.Rows[0][2] != null && dt.Rows[0][2].ToString() != "")
            {
                cbb_物料名称.Text = dt.Rows[0][2].ToString();
            }
        }
 
        void InvokeAfter(object sender, EventArgs e)
        {
            cbb_物料编号.Text = mtid;
        }

        private void toolStripButton1_Click_1(object sender, EventArgs e)
        {
            MaterialChose mtc = new MaterialChose(0, this);
            mtc.Show();
        }

        private void toolStripButton4_Click_1(object sender, EventArgs e)
        {
            int flag = 0;
            List<string> list1 = gn.GetAllMeasurement();
            List<string> list2 = mtb.GetAllMaterialID();
            List<string> list4 = mtb.GetAllMaterialNameMPN();
            List<string> list3 = gn.GetAllGroupName();//得到所有物料组
            if (cbb_物料编号.Text.ToString() == "")
            {
                MessageUtil.ShowError("请选择正确的物料编码");
                flag = 1;
            }

            if (cbb_计量单位.Text == "" || gn.IDInTheList(list1, cbb_计量单位.Text) == true)
            {
                MessageUtil.ShowError("请选择正确的计量单位");
                cbb_物料编号.Focus();
                flag = 1;
            }
            if (comboBox4.Text != "" && gn.IDInTheList(list1, comboBox4.Text) == true)
            {
                MessageUtil.ShowError("请选择正确的订单单位");
                cbb_物料编号.Focus();
                flag = 1;
            }
            if (comboBox3.Text != "" && gn.IDInTheList(list1, comboBox3.Text) == true)
            {
                MessageUtil.ShowError("请选择正确的可变单位");
                cbb_物料编号.Focus();
                flag = 1;
            }
            try
            {
                if (comboBox6.Text != "" && comboBox5.Text != "" && Convert.ToDecimal(comboBox6.Text) > Convert.ToDecimal(comboBox5.Text))
                {
                    MessageUtil.ShowError("错误：净重应小于毛重");
                    comboBox5.Focus();
                    flag = 1;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("信息有误");
                return;
            }
            if (cbb_外部物料组.Text != "" && gn.IDInTheList(list3, cbb_外部物料组.Text) == true)
            {
                MessageUtil.ShowError("请选择正确的外部物料组");
                cbb_外部物料组.Focus();
                flag = 1;
            }

            if (flag == 0 && MessageUtil.ShowYesNoAndTips("是否确定修改") == DialogResult.Yes)
            {


                MaterialBase mab = new MaterialBase();
                mab.Material_ID = cbb_物料编号.Text;
                mab.Material_Name = cbb_物料名称.Text;
                try
                {
                    mab.EI_Period = dtp_起始期.Value;
                }
                catch (Exception ex)
                {
                    mab.EI_Period = Convert.ToDateTime("1900.1.1");
                }
                mab.Measurement = cbb_计量单位.Text;
                mab.Division = cbb_产品组.Text;
                try
                {
                    mab.Net_Weight = Convert.ToDecimal(comboBox6.Text);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("净重为空");
                    return;
                }
                try
                {
                    mab.Gross_Weight = Convert.ToDecimal(comboBox5.Text);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("毛重为空");
                    return;
                }
                
                mab.Order_Unit = comboBox4.Text;
                mab.Variable_Unit = comboBox3.Text;
                mab.Material_Status = cbb_物料状态.Text;
                mab.Status_Information = tbx_跨工厂物料状态.Text;
                mab.Material_Standard = tbx_行业标准描述.Text;
                mab.Market_Regulation_Identity = ckb_市场管制标识.Checked;
                mab.Material_Level = cbb_物料级别.Text;
                mab.Material_ID = cbb_物料编号.Text;
                mab.Material_Purchase_Type = ckb_生产性物料标识.Checked;
                mtb.UpdateBasicInformation(mab);
                MessageUtil.ShowTips("维护成功");




            }
        }

        private void toolStripButton7_Click_1(object sender, EventArgs e)
        {
            
            cbb_产品组.Text = "";
            cbb_权限组.Text = "";
            cbb_物料编号.Text = "";
            cbb_物料名称.Text = "";
            cbb_计量单位.Text = "";
            cbb_物料状态.Text = "";
            tbx_跨工厂物料状态.Text = "";
            ckb_市场管制标识.Checked = false;
            tbx_行业标准描述.Text = "";
            cbb_物料级别.Text = "";
            comboBox4.Text = "";
            comboBox3.Text = "";
            comboBox6.Text = "";
            comboBox5.Text = "";
            cbb_物料编号.Items.Clear();
        }

        private void toolStripButton8_Click(object sender, EventArgs e)
        {
            this.Close();
        }
      
        private void comboBox6_Leave(object sender, EventArgs e)
        {
            Dictionary<string, string> dict_temp = new Dictionary<string, string>();
            string key = "净重";
            dict_temp.Add(key, "");
            formh.onlyNumber(comboBox6, key, dict_temp);
        }


        private void comboBox5_Leave(object sender, EventArgs e)
        {
            Dictionary<string, string> dict_temp = new Dictionary<string, string>();
            string key = "毛重";
            dict_temp.Add(key, "");
            formh.onlyNumber(comboBox5, key, dict_temp);
        }
    }
}
