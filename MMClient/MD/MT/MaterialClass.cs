﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.SqlServerDAL.MDDAL.MT;
using Lib.SqlServerDAL;
using Lib.Model.MD.MT;
using Lib.Common.CommonUtils;
using MMClient.MD.GN;

namespace MMClient.MD.MT
{
    public partial class MaterialClass : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        public MaterialClass()
        {
            InitializeComponent();
        }
        MaterialClassDAL mcdal = new MaterialClassDAL();
        /// <summary>
        /// 新建时候用
        /// </summary>
        string classid;
        /// <summary>
        /// 维护时候用
        /// </summary>
        string classid1;
        int rowcount = 0;
        int flag = 0;
        private void MaterialClassfy_Load(object sender, EventArgs e)
        {

            dgv_类特性.Rows.Add(100);
        }

        private void cbb_物料编号_MouseClick(object sender, MouseEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            MaterialChose mtc = new MaterialChose(0, this);
            mtc.Show();
        }

        private void btn_维护特征_Click(object sender, EventArgs e)
        {
            ///读取分类编码
            string sql = "SELECT DISTINCT Class_ID FROM [Class_Feature] WHERE Class_Name='" + cbb_特性类.Text + "'";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            classid = dt.Rows[0][0].ToString();
            List<Class_Feature> list = mcdal.GetFeatureInfoByClassID(classid);
            for (int i = 0; i < list.Count; i++)
            {
                dgv_类特性.Rows[i].Cells["cln_特征描述"].Value = list[i].Feature_Name;
                dgv_类特性.Rows[i].Cells["cln_特性编码"].Value = list[i].Feature_ID;
                rowcount++;
            }

        }

        private void btn_维护值_Click(object sender, EventArgs e)
        {
            if (flag == 0)
            {
                List<Material_Feature> listmf = new List<Material_Feature>();
                string mtid = cbb_物料编号.Text;
                for (int i = 0; i < rowcount; i++)
                {
                    Material_Feature mtfe = new Material_Feature();
                    mtfe.Class_ID = classid;
                    mtfe.Feature_ID = dgv_类特性.Rows[i].Cells["cln_特性编码"].Value.ToString();
                    mtfe.Feature_Name = dgv_类特性.Rows[i].Cells["cln_特征描述"].Value.ToString();
                    mtfe.Feature_Value = dgv_类特性.Rows[i].Cells["cln_值"].Value.ToString();
                    mtfe.Material_ID = mtid;
                    listmf.Add(mtfe);
                }
                mcdal.InsertFeatureInfo(listmf);
            }
            else
            {
                List<Material_Feature> listmf = new List<Material_Feature>();
                string mtid = cbb_物料编号.Text;
                for (int i = 0; i < rowcount; i++)
                {
                    Material_Feature mtfe = new Material_Feature();
                    mtfe.Class_ID = classid1;
                    mtfe.Feature_ID = dgv_类特性.Rows[i].Cells["cln_特性编码"].Value.ToString();
                    mtfe.Feature_Name = dgv_类特性.Rows[i].Cells["cln_特征描述"].Value.ToString();
                    mtfe.Feature_Value = dgv_类特性.Rows[i].Cells["cln_值"].Value.ToString();
                    mtfe.Material_ID = mtid;
                    listmf.Add(mtfe);
                }
                mcdal.UpdateFeatureInfo(listmf);
            }
            MessageUtil.ShowTips("维护成功");

        }

        private void cbb_物料编号_TextChanged(object sender, EventArgs e)
        {
            string SQL = "SELECT * FROM [Material_Feature] WHERE Material_ID = '" + cbb_物料编号.Text + "'";
            DataTable dt = DBHelper.ExecuteQueryDT(SQL);
            if (dt.Rows.Count == 0)
            {
                flag = 0;
                DataTable dt1 = mcdal.GetDistinctClassInfo();
                List<string> list = new List<string>();
                for (int i = 0; i < dt1.Rows.Count; i++)
                {
                    string mtid = dt1.Rows[i]["Class_Name"].ToString();
                    list.Add(mtid);
                }
                cbb_特性类.DataSource = list;
                cbb_特性类.Text = "";
            }
            else
            {
                classid1 = dt.Rows[0]["Class_ID"].ToString();
                string sql2 = "SELECT DISTINCT Class_Name FROM [Class_Feature] WHERE Class_ID = '" + classid1 + "'";
                DataTable dt2 = DBHelper.ExecuteQueryDT(sql2);
                string classname = dt2.Rows[0][0].ToString();
                cbb_特性类.Text = classname;
                cbb_特性类.Enabled = false;
                List<Material_Feature> listmtfe = new List<Material_Feature>();
                listmtfe = mcdal.GetFeatureInfoByClassIDMtID(classid1, cbb_物料编号.Text);
                for (int i = 0; i < listmtfe.Count; i++)
                {
                    dgv_类特性.Rows[i].Cells["cln_特征描述"].Value = listmtfe[i].Feature_Name;
                    dgv_类特性.Rows[i].Cells["cln_特性编码"].Value = listmtfe[i].Feature_ID;
                    dgv_类特性.Rows[i].Cells["cln_值"].Value = listmtfe[i].Feature_Value;
                    rowcount++;
                }
                btn_维护特征.Visible = false;
                flag = 1;
            }
        }

        private void toolStripButton7_Click(object sender, EventArgs e)
        {
            MaterialChose mtc = new MaterialChose(0, this);
            mtc.Show();
        }

        private void toolStripButton4_Click(object sender, EventArgs e)
        {
            if (cbb_物料编号.Text == null || cbb_物料编号.Text.ToString()=="")
            {
                MessageUtil.ShowTips("请先选择物料！"); 
                return;
            }
            if (MessageUtil.ShowYesNoAndTips("是否确定录入数据") == DialogResult.Yes)
            {
                cbb_特性类.Focus();
                if (flag == 0)
                {
                    List<Material_Feature> listmf = new List<Material_Feature>();
                    string mtid = cbb_物料编号.Text;
                    for (int i = 0; i < rowcount; i++)
                    {
                        Material_Feature mtfe = new Material_Feature();
                        mtfe.Class_ID = classid;
                        if (dgv_类特性.Rows[i].Cells["cln_特性编码"].Value == null || dgv_类特性.Rows[i].Cells["cln_特征描述"].Value == null || dgv_类特性.Rows[i].Cells["cln_值"].Value==null)
                        {
                            MessageUtil.ShowTips("数据写入不完整");
                            return;
                        }
                        mtfe.Feature_ID = dgv_类特性.Rows[i].Cells["cln_特性编码"].Value.ToString();
                        mtfe.Feature_Name = dgv_类特性.Rows[i].Cells["cln_特征描述"].Value.ToString();
                        mtfe.Feature_Value = dgv_类特性.Rows[i].Cells["cln_值"].Value.ToString();
                        if(mtfe.Feature_Value!=null && mtfe.Feature_Value !="" && mtfe.Feature_Name!=null && mtfe.Feature_Name!="")
                        {
                            mtfe.Material_ID = mtid;
                        }
                        else
                        {
                            MessageUtil.ShowTips("数据写入不完整");
                            return  ;

                        }
                        listmf.Add(mtfe);
                        mcdal.InsertFeatureInfo(listmf);
                    }
                   
                }
                else
                {
                    List<Material_Feature> listmf = new List<Material_Feature>();
                    string mtid = cbb_物料编号.Text;
                    for (int i = 0; i < rowcount; i++)
                    {
                        Material_Feature mtfe = new Material_Feature();
                        mtfe.Class_ID = classid1;
                        mtfe.Feature_ID = dgv_类特性.Rows[i].Cells["cln_特性编码"].Value.ToString();
                        mtfe.Feature_Name = dgv_类特性.Rows[i].Cells["cln_特征描述"].Value.ToString();
                        mtfe.Feature_Value = dgv_类特性.Rows[i].Cells["cln_值"].Value.ToString();
                        mtfe.Material_ID = mtid;
                        listmf.Add(mtfe);
                    }
                    mcdal.UpdateFeatureInfo(listmf);
                }
                MessageUtil.ShowTips("维护成功");
            }

        }

       
  

        private void toolStripButton8_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            if (cbb_物料编号.Text == null || cbb_物料编号.Text.ToString() == "")
            {
                MessageUtil.ShowTips("请先选择物料！");
                return;
            }
            NewClass nw = new NewClass();
            nw.Show();
        }

        private void toolStripButton6_Click(object sender, EventArgs e)
        {
            if (cbb_物料编号.Text == null || cbb_物料编号.Text.ToString() == "")
            {
                MessageUtil.ShowTips("请先选择物料！");
                return;
            }
            ///读取分类编码
            string sql = "SELECT DISTINCT Class_ID FROM [Class_Feature] WHERE Class_Name='" + cbb_特性类.Text + "'";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            classid = dt.Rows[0][0].ToString();
            List<Class_Feature> list = mcdal.GetFeatureInfoByClassID(classid);
            for (int i = 0; i < list.Count; i++)
            {
                dgv_类特性.Rows[i].Cells["cln_特征描述"].Value = list[i].Feature_Name;
                dgv_类特性.Rows[i].Cells["cln_特性编码"].Value = list[i].Feature_ID;
                rowcount++;
            }
        }
    }
}
