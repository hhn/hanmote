﻿namespace MMClient.MD
{
    partial class MTMaintain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MTMaintain));
            this.label1 = new System.Windows.Forms.Label();
            this.cbb_物料编号 = new System.Windows.Forms.ComboBox();
            this.cbb_物料名称 = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dtp_起始期 = new System.Windows.Forms.DateTimePicker();
            this.label21 = new System.Windows.Forms.Label();
            this.tbx_行业标准描述 = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.tbx_跨工厂物料状态 = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.cbb_物料状态 = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.cbb_外部物料组 = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.cbb_计量单位 = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tbx_物料特性类 = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.ckb_生产性物料标识 = new System.Windows.Forms.CheckBox();
            this.ckb_市场管制标识 = new System.Windows.Forms.CheckBox();
            this.物料级别 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.comboBox5 = new System.Windows.Forms.TextBox();
            this.comboBox6 = new System.Windows.Forms.TextBox();
            this.comboBox3 = new System.Windows.Forms.ComboBox();
            this.comboBox4 = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.label20 = new System.Windows.Forms.Label();
            this.toolStripExecutionMonitorSetting = new System.Windows.Forms.ToolStrip();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton7 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton8 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton4 = new System.Windows.Forms.ToolStripButton();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.txPcerPageCount = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.txPcerVersion = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.txPcerPageSize = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.txPcerPageChangeNo = new System.Windows.Forms.TextBox();
            this.txPcerPageNo = new System.Windows.Forms.TextBox();
            this.txPcerType = new System.Windows.Forms.TextBox();
            this.txPcert = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.TextBox();
            this.cbb_产品组 = new System.Windows.Forms.TextBox();
            this.cbb_产品分配 = new System.Windows.Forms.TextBox();
            this.cbb_权限组 = new System.Windows.Forms.TextBox();
            this.cbb_物料级别 = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.toolStripExecutionMonitorSetting.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(16, 95);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "物料";
            // 
            // cbb_物料编号
            // 
            this.cbb_物料编号.DropDownWidth = 10000;
            this.cbb_物料编号.Enabled = false;
            this.cbb_物料编号.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.cbb_物料编号.FormattingEnabled = true;
            this.cbb_物料编号.Location = new System.Drawing.Point(164, 95);
            this.cbb_物料编号.Name = "cbb_物料编号";
            this.cbb_物料编号.Size = new System.Drawing.Size(167, 25);
            this.cbb_物料编号.TabIndex = 1;
            this.cbb_物料编号.SelectedIndexChanged += new System.EventHandler(this.cbb_物料ID_SelectedIndexChanged);
            this.cbb_物料编号.TextChanged += new System.EventHandler(this.cbb_物料编号_TextChanged);
            // 
            // cbb_物料名称
            // 
            this.cbb_物料名称.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.cbb_物料名称.FormattingEnabled = true;
            this.cbb_物料名称.Location = new System.Drawing.Point(387, 95);
            this.cbb_物料名称.Name = "cbb_物料名称";
            this.cbb_物料名称.Size = new System.Drawing.Size(121, 25);
            this.cbb_物料名称.TabIndex = 2;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cbb_产品分配);
            this.groupBox1.Controls.Add(this.cbb_产品组);
            this.groupBox1.Controls.Add(this.comboBox1);
            this.groupBox1.Controls.Add(this.dtp_起始期);
            this.groupBox1.Controls.Add(this.label21);
            this.groupBox1.Controls.Add(this.tbx_行业标准描述);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.tbx_跨工厂物料状态);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.cbb_物料状态);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.cbb_外部物料组);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.cbb_计量单位);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Font = new System.Drawing.Font("微软雅黑", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(4, 124);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1245, 267);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "一般数据";
            // 
            // dtp_起始期
            // 
            this.dtp_起始期.Location = new System.Drawing.Point(463, 184);
            this.dtp_起始期.Name = "dtp_起始期";
            this.dtp_起始期.Size = new System.Drawing.Size(200, 25);
            this.dtp_起始期.TabIndex = 24;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.label21.ForeColor = System.Drawing.Color.Red;
            this.label21.Location = new System.Drawing.Point(212, 44);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(56, 17);
            this.label21.TabIndex = 23;
            this.label21.Text = "“必选”";
            // 
            // tbx_行业标准描述
            // 
            this.tbx_行业标准描述.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbx_行业标准描述.Location = new System.Drawing.Point(463, 229);
            this.tbx_行业标准描述.Name = "tbx_行业标准描述";
            this.tbx_行业标准描述.Size = new System.Drawing.Size(352, 23);
            this.tbx_行业标准描述.TabIndex = 22;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(378, 231);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(80, 17);
            this.label9.TabIndex = 21;
            this.label9.Text = "行业标准描述";
            // 
            // tbx_跨工厂物料状态
            // 
            this.tbx_跨工厂物料状态.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbx_跨工厂物料状态.Location = new System.Drawing.Point(175, 228);
            this.tbx_跨工厂物料状态.Name = "tbx_跨工厂物料状态";
            this.tbx_跨工厂物料状态.ReadOnly = true;
            this.tbx_跨工厂物料状态.Size = new System.Drawing.Size(153, 23);
            this.tbx_跨工厂物料状态.TabIndex = 20;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(378, 188);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(68, 17);
            this.label11.TabIndex = 18;
            this.label11.Text = "有效起始期";
            // 
            // cbb_物料状态
            // 
            this.cbb_物料状态.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbb_物料状态.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbb_物料状态.FormattingEnabled = true;
            this.cbb_物料状态.Items.AddRange(new object[] {
            "01",
            "02",
            "DC",
            "EC"});
            this.cbb_物料状态.Location = new System.Drawing.Point(129, 228);
            this.cbb_物料状态.Name = "cbb_物料状态";
            this.cbb_物料状态.Size = new System.Drawing.Size(40, 25);
            this.cbb_物料状态.TabIndex = 17;
            this.cbb_物料状态.SelectedIndexChanged += new System.EventHandler(this.cbb_物料状态_SelectedIndexChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(18, 231);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(104, 17);
            this.label10.TabIndex = 16;
            this.label10.Text = "跨工厂的物料状态";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(18, 184);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(56, 17);
            this.label8.TabIndex = 12;
            this.label8.Text = "产品分配";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(18, 135);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(44, 17);
            this.label6.TabIndex = 8;
            this.label6.Text = "产品组";
            // 
            // cbb_外部物料组
            // 
            this.cbb_外部物料组.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbb_外部物料组.FormattingEnabled = true;
            this.cbb_外部物料组.Location = new System.Drawing.Point(463, 86);
            this.cbb_外部物料组.Name = "cbb_外部物料组";
            this.cbb_外部物料组.Size = new System.Drawing.Size(121, 25);
            this.cbb_外部物料组.TabIndex = 7;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(378, 89);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(68, 17);
            this.label5.TabIndex = 6;
            this.label5.Text = "外部物料组";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(18, 89);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 17);
            this.label4.TabIndex = 4;
            this.label4.Text = "旧物料号";
            // 
            // cbb_计量单位
            // 
            this.cbb_计量单位.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbb_计量单位.FormattingEnabled = true;
            this.cbb_计量单位.Location = new System.Drawing.Point(129, 41);
            this.cbb_计量单位.Name = "cbb_计量单位";
            this.cbb_计量单位.Size = new System.Drawing.Size(77, 25);
            this.cbb_计量单位.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(18, 44);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 17);
            this.label2.TabIndex = 0;
            this.label2.Text = "基本计量数据";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.cbb_物料级别);
            this.groupBox2.Controls.Add(this.cbb_权限组);
            this.groupBox2.Controls.Add(this.tbx_物料特性类);
            this.groupBox2.Controls.Add(this.label22);
            this.groupBox2.Controls.Add(this.ckb_生产性物料标识);
            this.groupBox2.Controls.Add(this.ckb_市场管制标识);
            this.groupBox2.Controls.Add(this.物料级别);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Font = new System.Drawing.Font("微软雅黑", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(4, 397);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1245, 89);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "物料授权组";
            // 
            // tbx_物料特性类
            // 
            this.tbx_物料特性类.Enabled = false;
            this.tbx_物料特性类.Location = new System.Drawing.Point(443, 52);
            this.tbx_物料特性类.Name = "tbx_物料特性类";
            this.tbx_物料特性类.Size = new System.Drawing.Size(100, 25);
            this.tbx_物料特性类.TabIndex = 16;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label22.Location = new System.Drawing.Point(371, 57);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(68, 17);
            this.label22.TabIndex = 15;
            this.label22.Text = "物料特性类";
            // 
            // ckb_生产性物料标识
            // 
            this.ckb_生产性物料标识.AutoSize = true;
            this.ckb_生产性物料标识.Location = new System.Drawing.Point(715, 52);
            this.ckb_生产性物料标识.Name = "ckb_生产性物料标识";
            this.ckb_生产性物料标识.Size = new System.Drawing.Size(126, 24);
            this.ckb_生产性物料标识.TabIndex = 14;
            this.ckb_生产性物料标识.Text = "生产性物料标识";
            this.ckb_生产性物料标识.UseVisualStyleBackColor = true;
            // 
            // ckb_市场管制标识
            // 
            this.ckb_市场管制标识.AutoSize = true;
            this.ckb_市场管制标识.Location = new System.Drawing.Point(576, 52);
            this.ckb_市场管制标识.Name = "ckb_市场管制标识";
            this.ckb_市场管制标识.Size = new System.Drawing.Size(112, 24);
            this.ckb_市场管制标识.TabIndex = 13;
            this.ckb_市场管制标识.Text = "市场管制标识";
            this.ckb_市场管制标识.UseVisualStyleBackColor = true;
            // 
            // 物料级别
            // 
            this.物料级别.AutoSize = true;
            this.物料级别.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.物料级别.Location = new System.Drawing.Point(170, 55);
            this.物料级别.Name = "物料级别";
            this.物料级别.Size = new System.Drawing.Size(56, 17);
            this.物料级别.TabIndex = 11;
            this.物料级别.Text = "物料级别";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label12.Location = new System.Drawing.Point(12, 55);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(44, 17);
            this.label12.TabIndex = 9;
            this.label12.Text = "权限组";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.comboBox5);
            this.groupBox4.Controls.Add(this.comboBox6);
            this.groupBox4.Controls.Add(this.comboBox3);
            this.groupBox4.Controls.Add(this.comboBox4);
            this.groupBox4.Controls.Add(this.label16);
            this.groupBox4.Controls.Add(this.label17);
            this.groupBox4.Controls.Add(this.label18);
            this.groupBox4.Controls.Add(this.label19);
            this.groupBox4.Font = new System.Drawing.Font("微软雅黑", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox4.Location = new System.Drawing.Point(4, 492);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(1245, 113);
            this.groupBox4.TabIndex = 12;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "量纲";
            // 
            // comboBox5
            // 
            this.comboBox5.Location = new System.Drawing.Point(63, 71);
            this.comboBox5.Name = "comboBox5";
            this.comboBox5.Size = new System.Drawing.Size(121, 25);
            this.comboBox5.TabIndex = 9;
            this.comboBox5.Leave += new System.EventHandler(this.comboBox5_Leave);
            // 
            // comboBox6
            // 
            this.comboBox6.Location = new System.Drawing.Point(62, 23);
            this.comboBox6.Name = "comboBox6";
            this.comboBox6.Size = new System.Drawing.Size(122, 25);
            this.comboBox6.TabIndex = 8;
            this.comboBox6.Leave += new System.EventHandler(this.comboBox6_Leave);
            // 
            // comboBox3
            // 
            this.comboBox3.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.comboBox3.FormattingEnabled = true;
            this.comboBox3.Location = new System.Drawing.Point(315, 71);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(121, 25);
            this.comboBox3.TabIndex = 7;
            // 
            // comboBox4
            // 
            this.comboBox4.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.comboBox4.FormattingEnabled = true;
            this.comboBox4.Location = new System.Drawing.Point(315, 23);
            this.comboBox4.Name = "comboBox4";
            this.comboBox4.Size = new System.Drawing.Size(121, 25);
            this.comboBox4.TabIndex = 6;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label16.Location = new System.Drawing.Point(242, 74);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(56, 17);
            this.label16.TabIndex = 5;
            this.label16.Text = "可变单位";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label17.Location = new System.Drawing.Point(242, 26);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(56, 17);
            this.label17.TabIndex = 4;
            this.label17.Text = "订单单位";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label18.Location = new System.Drawing.Point(18, 77);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(32, 17);
            this.label18.TabIndex = 1;
            this.label18.Text = "毛重";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label19.Location = new System.Drawing.Point(18, 29);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(32, 17);
            this.label19.TabIndex = 0;
            this.label19.Text = "净重";
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.ForeColor = System.Drawing.Color.Red;
            this.label20.Location = new System.Drawing.Point(335, 98);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(39, 13);
            this.label20.TabIndex = 13;
            this.label20.Text = "“必选”";
            // 
            // toolStripExecutionMonitorSetting
            // 
            this.toolStripExecutionMonitorSetting.ImageScalingSize = new System.Drawing.Size(40, 40);
            this.toolStripExecutionMonitorSetting.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton1,
            this.toolStripButton7,
            this.toolStripButton8,
            this.toolStripButton4});
            this.toolStripExecutionMonitorSetting.Location = new System.Drawing.Point(0, 0);
            this.toolStripExecutionMonitorSetting.Name = "toolStripExecutionMonitorSetting";
            this.toolStripExecutionMonitorSetting.Size = new System.Drawing.Size(1303, 64);
            this.toolStripExecutionMonitorSetting.TabIndex = 22;
            this.toolStripExecutionMonitorSetting.Text = "toolStrip1";
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(60, 61);
            this.toolStripButton1.Text = "选择物料";
            this.toolStripButton1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click_1);
            // 
            // toolStripButton7
            // 
            this.toolStripButton7.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton7.Image")));
            this.toolStripButton7.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton7.Name = "toolStripButton7";
            this.toolStripButton7.Size = new System.Drawing.Size(44, 61);
            this.toolStripButton7.Text = "刷新";
            this.toolStripButton7.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton7.Click += new System.EventHandler(this.toolStripButton7_Click_1);
            // 
            // toolStripButton8
            // 
            this.toolStripButton8.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton8.Name = "toolStripButton8";
            this.toolStripButton8.Size = new System.Drawing.Size(36, 61);
            this.toolStripButton8.Text = "关闭";
            this.toolStripButton8.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton8.Click += new System.EventHandler(this.toolStripButton8_Click);
            // 
            // toolStripButton4
            // 
            this.toolStripButton4.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton4.Name = "toolStripButton4";
            this.toolStripButton4.Size = new System.Drawing.Size(36, 61);
            this.toolStripButton4.Text = "确定";
            this.toolStripButton4.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton4.Click += new System.EventHandler(this.toolStripButton4_Click_1);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.txPcerPageCount);
            this.groupBox3.Controls.Add(this.label25);
            this.groupBox3.Controls.Add(this.txPcerVersion);
            this.groupBox3.Controls.Add(this.label24);
            this.groupBox3.Controls.Add(this.txPcerPageSize);
            this.groupBox3.Controls.Add(this.label23);
            this.groupBox3.Controls.Add(this.txPcerPageChangeNo);
            this.groupBox3.Controls.Add(this.txPcerPageNo);
            this.groupBox3.Controls.Add(this.txPcerType);
            this.groupBox3.Controls.Add(this.txPcert);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this.label14);
            this.groupBox3.Controls.Add(this.label15);
            this.groupBox3.Font = new System.Drawing.Font("微软雅黑", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(4, 611);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(1245, 113);
            this.groupBox3.TabIndex = 23;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "设计图";
            // 
            // txPcerPageCount
            // 
            this.txPcerPageCount.Location = new System.Drawing.Point(543, 67);
            this.txPcerPageCount.Name = "txPcerPageCount";
            this.txPcerPageCount.Size = new System.Drawing.Size(43, 25);
            this.txPcerPageCount.TabIndex = 17;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label25.Location = new System.Drawing.Point(478, 72);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(32, 17);
            this.label25.TabIndex = 16;
            this.label25.Text = "页数";
            // 
            // txPcerVersion
            // 
            this.txPcerVersion.Location = new System.Drawing.Point(543, 20);
            this.txPcerVersion.Name = "txPcerVersion";
            this.txPcerVersion.Size = new System.Drawing.Size(43, 25);
            this.txPcerVersion.TabIndex = 15;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label24.Location = new System.Drawing.Point(474, 26);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(56, 17);
            this.label24.TabIndex = 14;
            this.label24.Text = "凭证版本";
            // 
            // txPcerPageSize
            // 
            this.txPcerPageSize.Location = new System.Drawing.Point(390, 70);
            this.txPcerPageSize.Name = "txPcerPageSize";
            this.txPcerPageSize.Size = new System.Drawing.Size(43, 25);
            this.txPcerPageSize.TabIndex = 13;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label23.Location = new System.Drawing.Point(322, 74);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(56, 17);
            this.label23.TabIndex = 12;
            this.label23.Text = "页面大小";
            // 
            // txPcerPageChangeNo
            // 
            this.txPcerPageChangeNo.Location = new System.Drawing.Point(198, 69);
            this.txPcerPageChangeNo.Name = "txPcerPageChangeNo";
            this.txPcerPageChangeNo.Size = new System.Drawing.Size(43, 25);
            this.txPcerPageChangeNo.TabIndex = 11;
            // 
            // txPcerPageNo
            // 
            this.txPcerPageNo.Location = new System.Drawing.Point(63, 69);
            this.txPcerPageNo.Name = "txPcerPageNo";
            this.txPcerPageNo.Size = new System.Drawing.Size(43, 25);
            this.txPcerPageNo.TabIndex = 10;
            // 
            // txPcerType
            // 
            this.txPcerType.Location = new System.Drawing.Point(393, 21);
            this.txPcerType.Name = "txPcerType";
            this.txPcerType.Size = new System.Drawing.Size(43, 25);
            this.txPcerType.TabIndex = 9;
            // 
            // txPcert
            // 
            this.txPcert.Location = new System.Drawing.Point(62, 23);
            this.txPcert.Name = "txPcert";
            this.txPcert.Size = new System.Drawing.Size(179, 25);
            this.txPcert.TabIndex = 8;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label7.Location = new System.Drawing.Point(122, 75);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(68, 17);
            this.label7.TabIndex = 5;
            this.label7.Text = "文档变更号";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label13.Location = new System.Drawing.Point(320, 29);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(56, 17);
            this.label13.TabIndex = 4;
            this.label13.Text = "凭证类型";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label14.Location = new System.Drawing.Point(18, 74);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(32, 17);
            this.label14.TabIndex = 1;
            this.label14.Text = "页号";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label15.Location = new System.Drawing.Point(18, 29);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(32, 17);
            this.label15.TabIndex = 0;
            this.label15.Text = "凭证";
            // 
            // comboBox1
            // 
            this.comboBox1.Location = new System.Drawing.Point(129, 83);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(167, 25);
            this.comboBox1.TabIndex = 25;
            // 
            // cbb_产品组
            // 
            this.cbb_产品组.Location = new System.Drawing.Point(129, 129);
            this.cbb_产品组.Name = "cbb_产品组";
            this.cbb_产品组.Size = new System.Drawing.Size(77, 25);
            this.cbb_产品组.TabIndex = 26;
            // 
            // cbb_产品分配
            // 
            this.cbb_产品分配.Location = new System.Drawing.Point(129, 178);
            this.cbb_产品分配.Name = "cbb_产品分配";
            this.cbb_产品分配.Size = new System.Drawing.Size(167, 25);
            this.cbb_产品分配.TabIndex = 27;
            // 
            // cbb_权限组
            // 
            this.cbb_权限组.Location = new System.Drawing.Point(63, 49);
            this.cbb_权限组.Name = "cbb_权限组";
            this.cbb_权限组.Size = new System.Drawing.Size(75, 25);
            this.cbb_权限组.TabIndex = 17;
            // 
            // cbb_物料级别
            // 
            this.cbb_物料级别.Location = new System.Drawing.Point(232, 49);
            this.cbb_物料级别.Name = "cbb_物料级别";
            this.cbb_物料级别.Size = new System.Drawing.Size(74, 25);
            this.cbb_物料级别.TabIndex = 18;
            // 
            // MTMaintain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1303, 733);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.toolStripExecutionMonitorSetting);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.cbb_物料名称);
            this.Controls.Add(this.cbb_物料编号);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "MTMaintain";
            this.Text = "基本视图";
            this.Load += new System.EventHandler(this.MTMaintain_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.toolStripExecutionMonitorSetting.ResumeLayout(false);
            this.toolStripExecutionMonitorSetting.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbb_物料名称;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox cbb_物料状态;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cbb_外部物料组;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cbb_计量单位;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox tbx_跨工厂物料状态;
        private System.Windows.Forms.Label 物料级别;
        private System.Windows.Forms.TextBox tbx_行业标准描述;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.ComboBox comboBox3;
        private System.Windows.Forms.ComboBox comboBox4;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.CheckBox ckb_市场管制标识;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.CheckBox ckb_生产性物料标识;
        public System.Windows.Forms.ComboBox cbb_物料编号;
        private System.Windows.Forms.TextBox tbx_物料特性类;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.ToolStrip toolStripExecutionMonitorSetting;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripButton toolStripButton7;
        private System.Windows.Forms.ToolStripButton toolStripButton8;
        private System.Windows.Forms.ToolStripButton toolStripButton4;
        private System.Windows.Forms.DateTimePicker dtp_起始期;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox txPcerPageCount;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox txPcerVersion;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox txPcerPageSize;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox txPcerPageChangeNo;
        private System.Windows.Forms.TextBox txPcerPageNo;
        private System.Windows.Forms.TextBox txPcerType;
        private System.Windows.Forms.TextBox txPcert;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox comboBox6;
        private System.Windows.Forms.TextBox comboBox5;
        private System.Windows.Forms.TextBox comboBox1;
        private System.Windows.Forms.TextBox cbb_产品组;
        private System.Windows.Forms.TextBox cbb_产品分配;
        private System.Windows.Forms.TextBox cbb_权限组;
        private System.Windows.Forms.TextBox cbb_物料级别;
    }
}