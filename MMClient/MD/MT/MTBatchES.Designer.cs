﻿namespace MMClient.MD
{
    partial class MTBatch
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cbb_到货日期 = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.cbb_物料编码 = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.btn_清除 = new System.Windows.Forms.Button();
            this.btn_录入 = new System.Windows.Forms.Button();
            this.cbb_出货日期 = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cbb_收货日期 = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.cbb_生产日期 = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cbb_批次级别 = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cbb_批次名称 = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cbb_批次ID = new System.Windows.Forms.ComboBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(26, 35);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(49, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "批次ID";
            // 
            // supplierInforGroupBox
            // 
            this.groupBox1.Controls.Add(this.cbb_到货日期);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.cbb_物料编码);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.btn_清除);
            this.groupBox1.Controls.Add(this.btn_录入);
            this.groupBox1.Controls.Add(this.cbb_出货日期);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.cbb_收货日期);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.cbb_生产日期);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.cbb_批次级别);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.cbb_批次名称);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.cbb_批次ID);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(5, 5);
            this.groupBox1.Name = "supplierInforGroupBox";
            this.groupBox1.Size = new System.Drawing.Size(1045, 503);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "数据录入";
            // 
            // cbb_到货日期
            // 
            this.cbb_到货日期.FormattingEnabled = true;
            this.cbb_到货日期.Location = new System.Drawing.Point(571, 182);
            this.cbb_到货日期.Name = "cbb_到货日期";
            this.cbb_到货日期.Size = new System.Drawing.Size(121, 24);
            this.cbb_到货日期.TabIndex = 17;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(430, 185);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(64, 17);
            this.label8.TabIndex = 16;
            this.label8.Text = "到货日期";
            // 
            // cbb_物料编码
            // 
            this.cbb_物料编码.FormattingEnabled = true;
            this.cbb_物料编码.Location = new System.Drawing.Point(117, 182);
            this.cbb_物料编码.Name = "cbb_物料编码";
            this.cbb_物料编码.Size = new System.Drawing.Size(121, 24);
            this.cbb_物料编码.TabIndex = 15;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(29, 185);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(64, 17);
            this.label7.TabIndex = 14;
            this.label7.Text = "物料编码";
            // 
            // btn_清除
            // 
            this.btn_清除.Location = new System.Drawing.Point(759, 431);
            this.btn_清除.Name = "btn_清除";
            this.btn_清除.Size = new System.Drawing.Size(75, 23);
            this.btn_清除.TabIndex = 13;
            this.btn_清除.Text = "清除";
            this.btn_清除.UseVisualStyleBackColor = true;
            this.btn_清除.Click += new System.EventHandler(this.btn_清除_Click);
            // 
            // btn_录入
            // 
            this.btn_录入.Location = new System.Drawing.Point(645, 431);
            this.btn_录入.Name = "btn_录入";
            this.btn_录入.Size = new System.Drawing.Size(75, 23);
            this.btn_录入.TabIndex = 12;
            this.btn_录入.Text = "录入";
            this.btn_录入.UseVisualStyleBackColor = true;
            this.btn_录入.Click += new System.EventHandler(this.btn_录入_Click);
            // 
            // cbb_出货日期
            // 
            this.cbb_出货日期.FormattingEnabled = true;
            this.cbb_出货日期.Location = new System.Drawing.Point(571, 132);
            this.cbb_出货日期.Name = "cbb_出货日期";
            this.cbb_出货日期.Size = new System.Drawing.Size(121, 24);
            this.cbb_出货日期.TabIndex = 11;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(430, 141);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(64, 17);
            this.label6.TabIndex = 10;
            this.label6.Text = "出货日期";
            // 
            // cbb_收货日期
            // 
            this.cbb_收货日期.FormattingEnabled = true;
            this.cbb_收货日期.Location = new System.Drawing.Point(571, 81);
            this.cbb_收货日期.Name = "cbb_收货日期";
            this.cbb_收货日期.Size = new System.Drawing.Size(121, 24);
            this.cbb_收货日期.TabIndex = 9;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(430, 84);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(64, 17);
            this.label5.TabIndex = 8;
            this.label5.Text = "收货日期";
            // 
            // cbb_生产日期
            // 
            this.cbb_生产日期.FormattingEnabled = true;
            this.cbb_生产日期.Location = new System.Drawing.Point(571, 32);
            this.cbb_生产日期.Name = "cbb_生产日期";
            this.cbb_生产日期.Size = new System.Drawing.Size(121, 24);
            this.cbb_生产日期.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(430, 35);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(64, 17);
            this.label4.TabIndex = 6;
            this.label4.Text = "生产日期";
            // 
            // cbb_批次级别
            // 
            this.cbb_批次级别.FormattingEnabled = true;
            this.cbb_批次级别.Location = new System.Drawing.Point(117, 134);
            this.cbb_批次级别.Name = "cbb_批次级别";
            this.cbb_批次级别.Size = new System.Drawing.Size(121, 24);
            this.cbb_批次级别.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(29, 134);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 17);
            this.label3.TabIndex = 4;
            this.label3.Text = "批次级别";
            // 
            // cbb_批次名称
            // 
            this.cbb_批次名称.FormattingEnabled = true;
            this.cbb_批次名称.Location = new System.Drawing.Point(117, 81);
            this.cbb_批次名称.Name = "cbb_批次名称";
            this.cbb_批次名称.Size = new System.Drawing.Size(121, 24);
            this.cbb_批次名称.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(29, 84);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "批次名称";
            // 
            // cbb_批次ID
            // 
            this.cbb_批次ID.FormattingEnabled = true;
            this.cbb_批次ID.Location = new System.Drawing.Point(117, 32);
            this.cbb_批次ID.Name = "cbb_批次ID";
            this.cbb_批次ID.Size = new System.Drawing.Size(121, 24);
            this.cbb_批次ID.TabIndex = 1;
            // 
            // MTBatch
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1041, 504);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "MTBatch";
            this.Text = "MTBatch";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox cbb_出货日期;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cbb_收货日期;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cbb_生产日期;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cbb_批次级别;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cbb_批次名称;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cbb_批次ID;
        private System.Windows.Forms.Button btn_录入;
        private System.Windows.Forms.Button btn_清除;
        private System.Windows.Forms.ComboBox cbb_物料编码;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cbb_到货日期;
        private System.Windows.Forms.Label label8;
    }
}