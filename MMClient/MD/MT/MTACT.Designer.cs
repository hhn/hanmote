﻿namespace MMClient.MD.MT
{
    partial class MTACT
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MTACT));
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.cbb_工厂名称 = new System.Windows.Forms.ComboBox();
            this.cbb_工厂编码 = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cbb_物料名称 = new System.Windows.Forms.ComboBox();
            this.cbb_物料编号 = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.label14 = new System.Windows.Forms.Label();
            this.lb_价格确定 = new System.Windows.Forms.Label();
            this.lb_本期 = new System.Windows.Forms.Label();
            this.lb_评估类别 = new System.Windows.Forms.Label();
            this.cbb_产品组 = new System.Windows.Forms.ComboBox();
            this.lb_产品组 = new System.Windows.Forms.Label();
            this.cbb_货币 = new System.Windows.Forms.ComboBox();
            this.lb_货币 = new System.Windows.Forms.Label();
            this.cbb_计量单位 = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.cbb_评估类 = new System.Windows.Forms.ComboBox();
            this.gbx_当前估价 = new System.Windows.Forms.GroupBox();
            this.cbb_标准价格 = new System.Windows.Forms.TextBox();
            this.cbb_移动平均价 = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.cbb_价格单位 = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lb_运输组 = new System.Windows.Forms.Label();
            this.lb_税收状态 = new System.Windows.Forms.Label();
            this.cbb_价格控制 = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.btn_确定 = new System.Windows.Forms.Button();
            this.btn_重置 = new System.Windows.Forms.Button();
            this.toolStripExecutionMonitorSetting = new System.Windows.Forms.ToolStrip();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton7 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton8 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton4 = new System.Windows.Forms.ToolStripButton();
            this.cbb_未来价格 = new System.Windows.Forms.TextBox();
            this.cbb_总库存 = new System.Windows.Forms.TextBox();
            this.cbb_总价值 = new System.Windows.Forms.TextBox();
            this.cbb_销售订单库存 = new System.Windows.Forms.TextBox();
            this.cbb_评估分类 = new System.Windows.Forms.TextBox();
            this.cbb_评估类别 = new System.Windows.Forms.TextBox();
            this.cbb_本期 = new System.Windows.Forms.TextBox();
            this.cbb_价格确定 = new System.Windows.Forms.TextBox();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.gbx_当前估价.SuspendLayout();
            this.toolStripExecutionMonitorSetting.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox2
            // 
            this.groupBox2.AutoSize = true;
            this.groupBox2.Controls.Add(this.cbb_工厂名称);
            this.groupBox2.Controls.Add(this.cbb_工厂编码);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.cbb_物料名称);
            this.groupBox2.Controls.Add(this.cbb_物料编号);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Font = new System.Drawing.Font("微软雅黑", 10F);
            this.groupBox2.Location = new System.Drawing.Point(12, 70);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1249, 135);
            this.groupBox2.TabIndex = 12;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "会计视图";
            // 
            // cbb_工厂名称
            // 
            this.cbb_工厂名称.FormattingEnabled = true;
            this.cbb_工厂名称.Location = new System.Drawing.Point(246, 61);
            this.cbb_工厂名称.Name = "cbb_工厂名称";
            this.cbb_工厂名称.Size = new System.Drawing.Size(205, 27);
            this.cbb_工厂名称.TabIndex = 6;
            // 
            // cbb_工厂编码
            // 
            this.cbb_工厂编码.FormattingEnabled = true;
            this.cbb_工厂编码.Location = new System.Drawing.Point(63, 61);
            this.cbb_工厂编码.Name = "cbb_工厂编码";
            this.cbb_工厂编码.Size = new System.Drawing.Size(163, 27);
            this.cbb_工厂编码.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(22, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 17);
            this.label2.TabIndex = 4;
            this.label2.Text = "工厂";
            // 
            // cbb_物料名称
            // 
            this.cbb_物料名称.Enabled = false;
            this.cbb_物料名称.FormattingEnabled = true;
            this.cbb_物料名称.Location = new System.Drawing.Point(246, 24);
            this.cbb_物料名称.Name = "cbb_物料名称";
            this.cbb_物料名称.Size = new System.Drawing.Size(205, 27);
            this.cbb_物料名称.TabIndex = 2;
            // 
            // cbb_物料编号
            // 
            this.cbb_物料编号.Enabled = false;
            this.cbb_物料编号.FormattingEnabled = true;
            this.cbb_物料编号.Location = new System.Drawing.Point(63, 24);
            this.cbb_物料编号.Name = "cbb_物料编号";
            this.cbb_物料编号.Size = new System.Drawing.Size(163, 27);
            this.cbb_物料编号.TabIndex = 1;
            this.cbb_物料编号.SelectedIndexChanged += new System.EventHandler(this.cbb_物料编号_SelectedIndexChanged);
            this.cbb_物料编号.MouseClick += new System.Windows.Forms.MouseEventHandler(this.cbb_物料编号_MouseClick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(22, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "物料";
            // 
            // groupBox1
            // 
            this.groupBox1.AutoSize = true;
            this.groupBox1.Controls.Add(this.cbb_价格确定);
            this.groupBox1.Controls.Add(this.cbb_本期);
            this.groupBox1.Controls.Add(this.cbb_评估类别);
            this.groupBox1.Controls.Add(this.dateTimePicker1);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.lb_价格确定);
            this.groupBox1.Controls.Add(this.lb_本期);
            this.groupBox1.Controls.Add(this.lb_评估类别);
            this.groupBox1.Controls.Add(this.cbb_产品组);
            this.groupBox1.Controls.Add(this.lb_产品组);
            this.groupBox1.Controls.Add(this.cbb_货币);
            this.groupBox1.Controls.Add(this.lb_货币);
            this.groupBox1.Controls.Add(this.cbb_计量单位);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.cbb_评估类);
            this.groupBox1.Font = new System.Drawing.Font("微软雅黑", 10F);
            this.groupBox1.Location = new System.Drawing.Point(12, 211);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1249, 164);
            this.groupBox1.TabIndex = 13;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "一般数据";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(651, 61);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(200, 25);
            this.dateTimePicker1.TabIndex = 25;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(574, 69);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(68, 17);
            this.label14.TabIndex = 22;
            this.label14.Text = "有效起始期";
            // 
            // lb_价格确定
            // 
            this.lb_价格确定.AutoSize = true;
            this.lb_价格确定.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lb_价格确定.Location = new System.Drawing.Point(292, 107);
            this.lb_价格确定.Name = "lb_价格确定";
            this.lb_价格确定.Size = new System.Drawing.Size(56, 17);
            this.lb_价格确定.TabIndex = 16;
            this.lb_价格确定.Text = "价格确定";
            // 
            // lb_本期
            // 
            this.lb_本期.AutoSize = true;
            this.lb_本期.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lb_本期.Location = new System.Drawing.Point(292, 67);
            this.lb_本期.Name = "lb_本期";
            this.lb_本期.Size = new System.Drawing.Size(32, 17);
            this.lb_本期.TabIndex = 14;
            this.lb_本期.Text = "本期";
            // 
            // lb_评估类别
            // 
            this.lb_评估类别.AutoSize = true;
            this.lb_评估类别.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lb_评估类别.Location = new System.Drawing.Point(292, 27);
            this.lb_评估类别.Name = "lb_评估类别";
            this.lb_评估类别.Size = new System.Drawing.Size(56, 17);
            this.lb_评估类别.TabIndex = 10;
            this.lb_评估类别.Text = "评估类别";
            // 
            // cbb_产品组
            // 
            this.cbb_产品组.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.cbb_产品组.FormattingEnabled = true;
            this.cbb_产品组.Location = new System.Drawing.Point(107, 104);
            this.cbb_产品组.Name = "cbb_产品组";
            this.cbb_产品组.Size = new System.Drawing.Size(121, 25);
            this.cbb_产品组.TabIndex = 5;
            // 
            // lb_产品组
            // 
            this.lb_产品组.AutoSize = true;
            this.lb_产品组.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lb_产品组.Location = new System.Drawing.Point(22, 102);
            this.lb_产品组.Name = "lb_产品组";
            this.lb_产品组.Size = new System.Drawing.Size(44, 17);
            this.lb_产品组.TabIndex = 4;
            this.lb_产品组.Text = "产品组";
            // 
            // cbb_货币
            // 
            this.cbb_货币.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.cbb_货币.FormattingEnabled = true;
            this.cbb_货币.Location = new System.Drawing.Point(107, 64);
            this.cbb_货币.Name = "cbb_货币";
            this.cbb_货币.Size = new System.Drawing.Size(121, 25);
            this.cbb_货币.TabIndex = 3;
            this.cbb_货币.Leave += new System.EventHandler(this.cbb_货币_Leave);
            // 
            // lb_货币
            // 
            this.lb_货币.AutoSize = true;
            this.lb_货币.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lb_货币.Location = new System.Drawing.Point(22, 62);
            this.lb_货币.Name = "lb_货币";
            this.lb_货币.Size = new System.Drawing.Size(32, 17);
            this.lb_货币.TabIndex = 2;
            this.lb_货币.Text = "货币";
            // 
            // cbb_计量单位
            // 
            this.cbb_计量单位.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.cbb_计量单位.FormattingEnabled = true;
            this.cbb_计量单位.Location = new System.Drawing.Point(107, 24);
            this.cbb_计量单位.Name = "cbb_计量单位";
            this.cbb_计量单位.Size = new System.Drawing.Size(121, 25);
            this.cbb_计量单位.TabIndex = 1;
            this.cbb_计量单位.Leave += new System.EventHandler(this.cbb_计量单位_Leave);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label3.Location = new System.Drawing.Point(22, 25);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 17);
            this.label3.TabIndex = 0;
            this.label3.Text = "基本计量单位";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(576, 27);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(44, 17);
            this.label13.TabIndex = 0;
            this.label13.Text = "评估类";
            // 
            // cbb_评估类
            // 
            this.cbb_评估类.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbb_评估类.FormattingEnabled = true;
            this.cbb_评估类.Location = new System.Drawing.Point(651, 23);
            this.cbb_评估类.Name = "cbb_评估类";
            this.cbb_评估类.Size = new System.Drawing.Size(121, 25);
            this.cbb_评估类.TabIndex = 1;
            this.cbb_评估类.Leave += new System.EventHandler(this.cbb_评估类_Leave);
            // 
            // gbx_当前估价
            // 
            this.gbx_当前估价.AutoSize = true;
            this.gbx_当前估价.Controls.Add(this.cbb_评估分类);
            this.gbx_当前估价.Controls.Add(this.cbb_销售订单库存);
            this.gbx_当前估价.Controls.Add(this.cbb_总价值);
            this.gbx_当前估价.Controls.Add(this.cbb_总库存);
            this.gbx_当前估价.Controls.Add(this.cbb_未来价格);
            this.gbx_当前估价.Controls.Add(this.cbb_标准价格);
            this.gbx_当前估价.Controls.Add(this.cbb_移动平均价);
            this.gbx_当前估价.Controls.Add(this.label9);
            this.gbx_当前估价.Controls.Add(this.label12);
            this.gbx_当前估价.Controls.Add(this.label4);
            this.gbx_当前估价.Controls.Add(this.cbb_价格单位);
            this.gbx_当前估价.Controls.Add(this.label5);
            this.gbx_当前估价.Controls.Add(this.label6);
            this.gbx_当前估价.Controls.Add(this.lb_运输组);
            this.gbx_当前估价.Controls.Add(this.lb_税收状态);
            this.gbx_当前估价.Controls.Add(this.cbb_价格控制);
            this.gbx_当前估价.Controls.Add(this.label7);
            this.gbx_当前估价.Controls.Add(this.label11);
            this.gbx_当前估价.Font = new System.Drawing.Font("微软雅黑", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbx_当前估价.Location = new System.Drawing.Point(12, 381);
            this.gbx_当前估价.Name = "gbx_当前估价";
            this.gbx_当前估价.Size = new System.Drawing.Size(1249, 237);
            this.gbx_当前估价.TabIndex = 14;
            this.gbx_当前估价.TabStop = false;
            this.gbx_当前估价.Text = "当前估价";
            // 
            // cbb_标准价格
            // 
            this.cbb_标准价格.Location = new System.Drawing.Point(449, 128);
            this.cbb_标准价格.Name = "cbb_标准价格";
            this.cbb_标准价格.Size = new System.Drawing.Size(121, 25);
            this.cbb_标准价格.TabIndex = 23;
            this.cbb_标准价格.Leave += new System.EventHandler(this.cbb_标准价格_Leave);
            // 
            // cbb_移动平均价
            // 
            this.cbb_移动平均价.Location = new System.Drawing.Point(124, 128);
            this.cbb_移动平均价.Name = "cbb_移动平均价";
            this.cbb_移动平均价.Size = new System.Drawing.Size(120, 25);
            this.cbb_移动平均价.TabIndex = 22;
            this.cbb_移动平均价.Leave += new System.EventHandler(this.cbb_移动平均价_Leave);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(578, 33);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(44, 17);
            this.label9.TabIndex = 20;
            this.label9.Text = "总价值";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(300, 180);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(44, 17);
            this.label12.TabIndex = 18;
            this.label12.Text = "总库存";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(300, 134);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 17);
            this.label4.TabIndex = 16;
            this.label4.Text = "标准价格";
            // 
            // cbb_价格单位
            // 
            this.cbb_价格单位.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbb_价格单位.FormattingEnabled = true;
            this.cbb_价格单位.Location = new System.Drawing.Point(446, 81);
            this.cbb_价格单位.Name = "cbb_价格单位";
            this.cbb_价格单位.Size = new System.Drawing.Size(51, 25);
            this.cbb_价格单位.TabIndex = 15;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(297, 81);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 17);
            this.label5.TabIndex = 14;
            this.label5.Text = "价格单位";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(297, 38);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(116, 17);
            this.label6.TabIndex = 10;
            this.label6.Text = "项目库存的评估分类";
            // 
            // lb_运输组
            // 
            this.lb_运输组.AutoSize = true;
            this.lb_运输组.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_运输组.Location = new System.Drawing.Point(27, 183);
            this.lb_运输组.Name = "lb_运输组";
            this.lb_运输组.Size = new System.Drawing.Size(56, 17);
            this.lb_运输组.TabIndex = 8;
            this.lb_运输组.Text = "未来价格";
            // 
            // lb_税收状态
            // 
            this.lb_税收状态.AutoSize = true;
            this.lb_税收状态.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_税收状态.Location = new System.Drawing.Point(27, 134);
            this.lb_税收状态.Name = "lb_税收状态";
            this.lb_税收状态.Size = new System.Drawing.Size(68, 17);
            this.lb_税收状态.TabIndex = 6;
            this.lb_税收状态.Text = "移动平均价";
            // 
            // cbb_价格控制
            // 
            this.cbb_价格控制.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbb_价格控制.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbb_价格控制.FormattingEnabled = true;
            this.cbb_价格控制.Items.AddRange(new object[] {
            "V",
            "S"});
            this.cbb_价格控制.Location = new System.Drawing.Point(123, 83);
            this.cbb_价格控制.Name = "cbb_价格控制";
            this.cbb_价格控制.Size = new System.Drawing.Size(121, 25);
            this.cbb_价格控制.TabIndex = 5;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(22, 81);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(56, 17);
            this.label7.TabIndex = 4;
            this.label7.Text = "价格控制";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(17, 41);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(99, 17);
            this.label11.TabIndex = 2;
            this.label11.Text = "VC:销售订单库存";
            // 
            // btn_确定
            // 
            this.btn_确定.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_确定.Location = new System.Drawing.Point(693, 576);
            this.btn_确定.Name = "btn_确定";
            this.btn_确定.Size = new System.Drawing.Size(75, 26);
            this.btn_确定.TabIndex = 15;
            this.btn_确定.Text = "确定";
            this.btn_确定.UseVisualStyleBackColor = true;
            this.btn_确定.Visible = false;
            this.btn_确定.Click += new System.EventHandler(this.btn_确定_Click);
            // 
            // btn_重置
            // 
            this.btn_重置.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_重置.Location = new System.Drawing.Point(774, 576);
            this.btn_重置.Name = "btn_重置";
            this.btn_重置.Size = new System.Drawing.Size(75, 26);
            this.btn_重置.TabIndex = 16;
            this.btn_重置.Text = "重置";
            this.btn_重置.UseVisualStyleBackColor = true;
            this.btn_重置.Visible = false;
            this.btn_重置.Click += new System.EventHandler(this.btn_重置_Click);
            // 
            // toolStripExecutionMonitorSetting
            // 
            this.toolStripExecutionMonitorSetting.ImageScalingSize = new System.Drawing.Size(40, 40);
            this.toolStripExecutionMonitorSetting.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton1,
            this.toolStripButton7,
            this.toolStripButton8,
            this.toolStripButton4});
            this.toolStripExecutionMonitorSetting.Location = new System.Drawing.Point(0, 0);
            this.toolStripExecutionMonitorSetting.Name = "toolStripExecutionMonitorSetting";
            this.toolStripExecutionMonitorSetting.Size = new System.Drawing.Size(1282, 64);
            this.toolStripExecutionMonitorSetting.TabIndex = 18;
            this.toolStripExecutionMonitorSetting.Text = "toolStrip1";
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(60, 61);
            this.toolStripButton1.Text = "选择物料";
            this.toolStripButton1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // toolStripButton7
            // 
            this.toolStripButton7.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton7.Image")));
            this.toolStripButton7.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton7.Name = "toolStripButton7";
            this.toolStripButton7.Size = new System.Drawing.Size(44, 61);
            this.toolStripButton7.Text = "刷新";
            this.toolStripButton7.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton7.Click += new System.EventHandler(this.toolStripButton7_Click);
            // 
            // toolStripButton8
            // 
            this.toolStripButton8.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton8.Name = "toolStripButton8";
            this.toolStripButton8.Size = new System.Drawing.Size(36, 61);
            this.toolStripButton8.Text = "关闭";
            this.toolStripButton8.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton8.Click += new System.EventHandler(this.toolStripButton8_Click);
            // 
            // toolStripButton4
            // 
            this.toolStripButton4.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton4.Name = "toolStripButton4";
            this.toolStripButton4.Size = new System.Drawing.Size(36, 61);
            this.toolStripButton4.Text = "确定";
            this.toolStripButton4.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton4.Click += new System.EventHandler(this.toolStripButton4_Click);
            // 
            // cbb_未来价格
            // 
            this.cbb_未来价格.Location = new System.Drawing.Point(123, 177);
            this.cbb_未来价格.Name = "cbb_未来价格";
            this.cbb_未来价格.Size = new System.Drawing.Size(121, 25);
            this.cbb_未来价格.TabIndex = 24;
            this.cbb_未来价格.Leave += new System.EventHandler(this.cbb_未来价格_Leave);
            // 
            // cbb_总库存
            // 
            this.cbb_总库存.Location = new System.Drawing.Point(449, 177);
            this.cbb_总库存.Name = "cbb_总库存";
            this.cbb_总库存.Size = new System.Drawing.Size(121, 25);
            this.cbb_总库存.TabIndex = 25;
            this.cbb_总库存.Leave += new System.EventHandler(this.cbb_总库存_Leave);
            // 
            // cbb_总价值
            // 
            this.cbb_总价值.Location = new System.Drawing.Point(651, 30);
            this.cbb_总价值.Name = "cbb_总价值";
            this.cbb_总价值.Size = new System.Drawing.Size(121, 25);
            this.cbb_总价值.TabIndex = 26;
            this.cbb_总价值.Leave += new System.EventHandler(this.cbb_总价值_Leave);
            // 
            // cbb_销售订单库存
            // 
            this.cbb_销售订单库存.Location = new System.Drawing.Point(122, 35);
            this.cbb_销售订单库存.Name = "cbb_销售订单库存";
            this.cbb_销售订单库存.Size = new System.Drawing.Size(122, 25);
            this.cbb_销售订单库存.TabIndex = 27;
            this.cbb_销售订单库存.TextChanged += new System.EventHandler(this.cbb_销售订单库存_TextChanged);
            // 
            // cbb_评估分类
            // 
            this.cbb_评估分类.Location = new System.Drawing.Point(446, 32);
            this.cbb_评估分类.Name = "cbb_评估分类";
            this.cbb_评估分类.Size = new System.Drawing.Size(51, 25);
            this.cbb_评估分类.TabIndex = 28;
            // 
            // cbb_评估类别
            // 
            this.cbb_评估类别.Location = new System.Drawing.Point(363, 20);
            this.cbb_评估类别.Name = "cbb_评估类别";
            this.cbb_评估类别.Size = new System.Drawing.Size(50, 25);
            this.cbb_评估类别.TabIndex = 26;
            // 
            // cbb_本期
            // 
            this.cbb_本期.Location = new System.Drawing.Point(363, 62);
            this.cbb_本期.Name = "cbb_本期";
            this.cbb_本期.Size = new System.Drawing.Size(88, 25);
            this.cbb_本期.TabIndex = 27;
            // 
            // cbb_价格确定
            // 
            this.cbb_价格确定.Location = new System.Drawing.Point(363, 102);
            this.cbb_价格确定.Name = "cbb_价格确定";
            this.cbb_价格确定.Size = new System.Drawing.Size(125, 25);
            this.cbb_价格确定.TabIndex = 28;
            // 
            // MTACT
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1299, 603);
            this.Controls.Add(this.toolStripExecutionMonitorSetting);
            this.Controls.Add(this.btn_重置);
            this.Controls.Add(this.btn_确定);
            this.Controls.Add(this.gbx_当前估价);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox2);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "MTACT";
            this.Text = "会计视图";
            this.Load += new System.EventHandler(this.MTACT_Load);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.gbx_当前估价.ResumeLayout(false);
            this.gbx_当前估价.PerformLayout();
            this.toolStripExecutionMonitorSetting.ResumeLayout(false);
            this.toolStripExecutionMonitorSetting.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox2;
        public System.Windows.Forms.ComboBox cbb_工厂名称;
        public System.Windows.Forms.ComboBox cbb_工厂编码;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cbb_物料名称;
        public System.Windows.Forms.ComboBox cbb_物料编号;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        public System.Windows.Forms.Label lb_价格确定;
        public System.Windows.Forms.Label lb_本期;
        private System.Windows.Forms.Label lb_评估类别;
        public System.Windows.Forms.ComboBox cbb_产品组;
        public System.Windows.Forms.Label lb_产品组;
        public System.Windows.Forms.ComboBox cbb_货币;
        public System.Windows.Forms.Label lb_货币;
        public System.Windows.Forms.ComboBox cbb_计量单位;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label12;
        public System.Windows.Forms.Label label4;
        public System.Windows.Forms.ComboBox cbb_价格单位;
        public System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        public System.Windows.Forms.Label lb_运输组;
        public System.Windows.Forms.Label lb_税收状态;
        public System.Windows.Forms.ComboBox cbb_价格控制;
        public System.Windows.Forms.Label label7;
        public System.Windows.Forms.Label label11;
        public System.Windows.Forms.ComboBox cbb_评估类;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button btn_确定;
        private System.Windows.Forms.Button btn_重置;
        public System.Windows.Forms.GroupBox gbx_当前估价;
        private System.Windows.Forms.ToolStrip toolStripExecutionMonitorSetting;
        private System.Windows.Forms.ToolStripButton toolStripButton7;
        private System.Windows.Forms.ToolStripButton toolStripButton8;
        private System.Windows.Forms.ToolStripButton toolStripButton4;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        public System.Windows.Forms.TextBox cbb_移动平均价;
        public System.Windows.Forms.TextBox cbb_标准价格;
        public System.Windows.Forms.TextBox cbb_未来价格;
        public System.Windows.Forms.TextBox cbb_总库存;
        public System.Windows.Forms.TextBox cbb_总价值;
        public System.Windows.Forms.TextBox cbb_销售订单库存;
        public System.Windows.Forms.TextBox cbb_评估分类;
        public System.Windows.Forms.TextBox cbb_评估类别;
        public System.Windows.Forms.TextBox cbb_本期;
        public System.Windows.Forms.TextBox cbb_价格确定;
    }
}