﻿using System;
using System.Data;
using System.Windows.Forms;
using Lib.SqlServerDAL;
using Lib.Common.CommonUtils;
using Lib.Common.MMCException.IDAL;
using Lib.Common.MMCException.Bll;

namespace MMClient.MD.MT
{
    public partial class MaterialChose : Form
    {
        int typenumber;
        string mtid;
        MTMaintain mtm;
        MTACT mta;
        MaterialClass mtc;
        MTBUYER mtb;
        存储视图 mts;
       
        public MaterialChose()
        {
            InitializeComponent();
        }
        public MaterialChose(int number,MTMaintain mtm)
        {
            //number值：0无组织机构级别；1工厂级别；2库存地级别
            InitializeComponent();
            typenumber = number;
            this.mtm = mtm;
            if (number == 0)
            {
                dgv_项目.DataSource = FindMaterialInforByPage(this.pageTool.PageSize, this.pageTool.PageIndex, this.tbx_物料名称.Text.ToString());
            } 
        }
        public MaterialChose(int number, MaterialClass mtc)
        {
            //number值：0无组织机构级别；1工厂级别；2库存地级别
            InitializeComponent();
            typenumber = number;
            this.mtc = mtc;
            if (number == 0)
            {
                dgv_项目.DataSource = FindMaterialInforByPage(this.pageTool.PageSize, this.pageTool.PageIndex, this.tbx_物料名称.Text.ToString());
            }
        }
         

        private DataTable FindMaterialInforByPage(int pageSize, int pageIndex ,string MaterialName)
        {
            try
            {
                string sql;
                if (MaterialName=="" || MaterialName==null)
                {
                    sql = @"SELECT top " + pageSize + " Material_ID ,Material_Name , Material_Type  , Material_Group from Material where Material_ID not in(select top " + pageSize * (pageIndex - 1) + " Material_ID from Material ORDER BY Material_ID ASC)ORDER BY Material_ID";
                }
                else
                {
                    sql = @"SELECT top " + pageSize + " Material_ID ,Material_Name , Material_Type , Material_Group  from Material where Material_Name like '%" + MaterialName+"%' and Material_ID not in(select top " + pageSize * (pageIndex - 1) + " Material_ID from Material ORDER BY Material_ID ASC)ORDER BY Material_ID";
                }
               
                return  DBHelper.ExecuteQueryDT(sql);
            }
            catch (DBException ex)
            {
                throw new BllException("当前无数据", ex);
            }
        }
        //窗体打开，调用分页函数加载数据并列表显示
        private void SupplierChose_Load(object sender, EventArgs e)
        {
            dgv_项目.DataSource = FindMaterialInforByPage(this.pageTool.PageSize, this.pageTool.PageIndex, this.tbx_物料名称.Text.ToString());
        }

        public MaterialChose(int number, MTACT mta)
        {
            //number值：0无组织机构级别；1工厂级别；2库存地级别
            InitializeComponent();
            typenumber = number;
            this.mta = mta;
            if (number == 0)
            {
                dgv_项目.DataSource = FindMaterialInforByPage(this.pageTool.PageSize, this.pageTool.PageIndex, this.tbx_物料名称.Text.ToString());
            }
           
        }
        public MaterialChose(int number, MTBUYER mtb)
        {
            //number值：0无组织机构级别；1工厂级别；2库存地级别
            InitializeComponent();
            typenumber = number;
            this.mtb = mtb;
            if (number == 0)
            {
                dgv_项目.DataSource = FindMaterialInforByPage(this.pageTool.PageSize, this.pageTool.PageIndex, this.tbx_物料名称.Text.ToString());
            }
        }
             public MaterialChose(int number, 存储视图 mts)
        {
            //number值：0无组织机构级别；1工厂级别；2库存地级别
            InitializeComponent();
            typenumber = number;
            this.mts = mts;
            if (number == 0)
            {
                dgv_项目.DataSource = FindMaterialInforByPage(this.pageTool.PageSize, this.pageTool.PageIndex, this.tbx_物料名称.Text.ToString());
            }


        }
        /// <summary>
        /// 模糊查询
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            dgv_项目.DataSource =  FindMaterialInforByPage(this.pageTool.PageSize, this.pageTool.PageIndex, this.tbx_物料名称.Text.ToString());
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (dgv_项目.CurrentRow == null)
            {
                MessageUtil.ShowError("请选择要维护的物料数据");
            }
            else
            {
                string materialID = dgv_项目.CurrentRow.Cells[1].Value.ToString();
                string materialname = dgv_项目.CurrentRow.Cells[2].Value.ToString();
                mtid = materialID;
                if(this.mtm != null)
                this.mtm.cbb_物料编号.Text = mtid;
                if (this.mta != null)
                this.mta.cbb_物料编号.Text = mtid;
                if (this.mtc != null)
                {
                    this.mtc.cbb_物料编号.Text = mtid;
                    this.mtc.cbb_物料名称.Text = materialname;
                }
                this.Close();
            }
           
        }

        private void toolStripButton4_Click(object sender, EventArgs e)
        {
            if (dgv_项目.CurrentRow == null)
            {
                MessageUtil.ShowError("请选择要维护的物料数据");
            }
            else
            {
                string materialID = dgv_项目.CurrentRow.Cells[0].Value.ToString();
                string materialname = dgv_项目.CurrentRow.Cells[1].Value.ToString();
                mtid = materialID;
                if (this.mtm != null)
                    this.mtm.cbb_物料编号.Text = mtid;
                if (this.mta != null)
                    this.mta.cbb_物料编号.Text = mtid;
                if (this.mtc != null)
                {
                    this.mtc.cbb_物料编号.Text = mtid;
                    this.mtc.cbb_物料名称.Text = materialname;
                }
                if (this.mtb != null)
                {
                    this.mtb.cbb_物料编号.Text = mtid;
                    this.mtb.cbb_物料名称.Text = materialname;
                    //得到物料特性
                    string sql = "select qualityClass from Material where Material_ID = '"+ mtid + "' ";
                    this.mtb.cbqualityClass.Text = DBHelper.ExecuteQueryDT(sql).Rows[0][0].ToString();
                }
                if (this.mts != null)
                {
                    this.mts.cbb_物料编号.Text = mtid;
                    
                }
                this.Close();
            }
        }

        private void btn_新增物料_Click(object sender, EventArgs e)
        {

        }

        private void toolStripButton8_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void pageTool_Load(object sender, EventArgs e)
        {
            string sql = "select count(*) from Material";
            try
            {
                int i = int.Parse(DBHelper.ExecuteQueryDT(sql).Rows[0][0].ToString());
                int totalNum = i;
                pageTool.DrawControl(totalNum);
                //事件改变调用函数
                pageTool.OnPageChanged += pageTool_OnPageChanged;
            }
            catch(BllException ex)
            {
                MessageUtil.ShowTips("数据库异常，请稍后重试");
                return;
            }
           
           
        }

        private void LoadData()
        {
            try
            {
                dgv_项目.DataSource = FindMaterialInforByPage(this.pageTool.PageSize, this.pageTool.PageIndex, this.tbx_物料名称.Text.ToString());
            }
            catch (BllException ex)
            {
                MessageUtil.ShowError(ex.Message);
            }
        }

        private void pageTool_OnPageChanged(object sender, EventArgs e)
        {
            LoadData();
        }
    }
}
