﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.SqlServerDAL;
using System.Windows.Forms.DataVisualization.Charting;
using Lib.Bll.MDBll.General;
using Lib.Common.CommonUtils;
using Lib.Bll.MDBll.MT;
using Lib.Bll;
using Lib.Model.MD.MT;



namespace MMClient.MD.MT
{
    public partial class MTACT : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        public MTACT()
        {
            InitializeComponent();
        }
        GeneralBLL gn = new GeneralBLL();
        MaterialBLL mt = new MaterialBLL();
        MaterialAccountBLL mab = new MaterialAccountBLL();
        ComboBoxItem cbm = new ComboBoxItem();
        AccountBLL act = new AccountBLL();
        FormHelper formh = new Lib.Common.CommonUtils.FormHelper();
        

        //private void btn_刷新_Click(object sender, EventArgs e)
        //{
           
            
            
        //    //string sql = "SELECT * FROM [Material]";
        //    //DataTable dt = DBHelper.ExecuteQueryDT(sql);
        //    //int i;
        //    //for (i = 0; i < Convert.ToInt32(dt.Rows.Count.ToString()); i++)
        //    //{
        //    //    cbb_物料编号.Items.Add(dt.Rows[i][1]);
        //    //}
        //    //string sql1 = "SELECT * FROM [Material_Group]";
        //    //DataTable dt1 = DBHelper.ExecuteQueryDT(sql1);
        //    //int k;
        //    //for (k = 0; k < Convert.ToInt32(dt1.Rows.Count.ToString())-1; k++)
        //    //{
        //    //    cbb_产品组.Items.Add(dt.Rows[k][1].ToString());
        //    //}
            
            
        //}

        private void cbb_物料编号_SelectedIndexChanged(object sender, EventArgs e)
        {
           
                Chosefymta chf = new Chosefymta(this);
                chf.Show(this);
                List<string> list0 = gn.GetAllFactory();
                cbm.FuzzyQury(chf.cbb_工厂, list0);
                MaterialBase mtb = new MaterialBase();
                mtb = mab.GetBasicInformation(cbb_物料编号.Text);
                cbb_物料名称.Text = mtb.Material_Name;
                cbb_产品组.Text = mtb.Division;
                cbb_计量单位.Text = mtb.Measurement;
           
           

            //string sql1 = "SELECT * FROM [Factory]";
            //string sql2 = "SELECT * FROM [Material] WHERE Material_ID='" + cbb_物料编号.Text + "'";
            ////DataTable dt1 = DBHelper.ExecuteQueryDT(sql1);
            //DataTable dt2 = DBHelper.ExecuteQueryDT(sql2);
            //int j;
            //for (j = 0; j < Convert.ToInt32(dt1.Rows.Count.ToString()); j++)
            //{
            //    chf.cbb_工厂.Items.Add(dt1.Rows[j][1]);
            //}
            //cbb_物料名称.Text = dt2.Rows[0][2].ToString();
            //cbb_产品组.Text = dt2.Rows[0][16].ToString();
            //cbb_计量单位.Text = dt2.Rows[0][4].ToString();
        }

        public void btn_重置_Click(object sender, EventArgs e)
        {
            foreach (Control control in this.Controls)
            {
                if (control.GetType().Name == "ComboBox")
                {
                    ((ComboBox)control).Text = string.Empty;
                }
            }
            cbb_本期.Text = "";
            cbb_标准价格.Text = "0.0";
            cbb_产品组.Text = "";
            cbb_工厂编码.Text = "";
            cbb_工厂名称.Text = "";
            cbb_货币.Text = "";
            cbb_计量单位.Text = "";
            cbb_价格单位.Text = "";
            cbb_价格控制.Text = "";
            cbb_价格确定.Text = "";
            cbb_评估分类.Text = "";
            cbb_评估类.Text = "";
            cbb_评估类别.Text = "";
            cbb_未来价格.Text = "0.0";
            cbb_物料编号.Text = "";
            cbb_物料名称.Text = "";
            cbb_销售订单库存.Text = "";
            cbb_移动平均价.Text = "0.0";
            cbb_总价值.Text = "0.0";
            cbb_总库存.Text = "";
            cbb_本期.Visible = true;
            cbb_标准价格.Visible = true;
            cbb_工厂编码.Visible = true;
            cbb_工厂名称.Visible = true;
            cbb_货币.Visible = true;
            cbb_价格单位.Visible = true;
            cbb_价格控制.Visible = true;
            cbb_价格确定.Visible = true;
            cbb_评估分类.Visible = true;
            cbb_评估类.Visible = true;
            cbb_评估类别.Visible = true;
            cbb_未来价格.Visible = true;
            cbb_销售订单库存.Visible = true;
            cbb_移动平均价.Visible = true;
            cbb_总价值.Visible = true;
            cbb_总库存.Visible = true;
            cbb_本期.Enabled = true;
            cbb_标准价格.Enabled = true;
            cbb_货币.Enabled = true;
            cbb_价格单位.Enabled = true;
            cbb_价格控制.Enabled = true;
            cbb_价格确定.Enabled = true;
            cbb_评估分类.Enabled = true;
            cbb_评估类.Enabled = true;
            cbb_评估类别.Enabled = true;
            cbb_未来价格.Enabled = true;
            cbb_销售订单库存.Enabled = true;
            cbb_移动平均价.Enabled = true;
            cbb_总价值.Enabled = true;
            cbb_总库存.Enabled = true;
            cbb_产品组.Enabled = true;
            cbb_计量单位.Enabled = true;
            gbx_当前估价.Visible = true;

        }

        private void btn_确定_Click(object sender, EventArgs e)
        {
            //int flag = 0;
           
            //List<string> list2 = mt.GetAllMaterialID();
            //if (cbb_物料编号.Text == "" || gn.IDInTheList(list2, cbb_物料编号.Text) == true)
            //{
            //    MessageUtil.ShowError("请选择正确的物料编号");
            //    cbb_物料编号.Focus();
            //    flag = 1;
            //}
            //if (flag == 0 && MessageUtil.ShowYesNoAndTips("是否确定修改") == DialogResult.Yes)
            //{
            //    if (cbb_工厂编码.Text == "")
            //    {
            //        MaterialBase mb = new MaterialBase();
            //        mb.Material_Name = cbb_物料名称.Text;
            //        mb.Measurement = cbb_计量单位.Text;
            //        mb.Division = cbb_产品组.Text;
            //        mb.Material_ID = cbb_物料编号.Text;
            //        mab.UpdateBasicInformation(mb);
            //        MessageUtil.ShowTips("修改成功");

            //        //string sql = "UPDATE [Material] SET Material_Name='" + cbb_物料名称.Text + "',Division='" + cbb_产品组.Text + "'WHERE Material_ID='" + cbb_物料编号.Text + "'";
            //        //DBHelper.ExecuteNonQuery(sql);
            //        //MessageBox.Show("修改成功");
            //    }
            //    else
            //    {
            //        MaterialFactory mtfty = new MaterialFactory();
            //        mtfty.Assessment_Category = cbb_评估类别.Text;
            //        mtfty.Currency_Unit = cbb_货币.Text;
            //        mtfty.Evaluation_Class = cbb_评估类.Text;
            //        mtfty.Factory_ID = cbb_工厂编码.Text;
            //        mtfty.Factory_Name = cbb_工厂名称.Text;
            //        if (cbb_未来价格.Text == "")
            //            mtfty.Future_Price = 0;
            //        else
            //        mtfty.Future_Price =(float)Convert.ToDouble( cbb_未来价格.Text);
            //        if (cbb_总库存.Text == "")
            //            mtfty.Gross_Inventory = 0;
            //        mtfty.Gross_Inventory =Convert.ToInt16( cbb_总库存.Text);
            //        mtfty.Material_ID = cbb_物料编号.Text;
            //        mtfty.Material_Name = cbb_物料名称.Text;
            //        if (cbb_移动平均价.Text == "")
            //            mtfty.Moving_AGPrice = 0;
            //        else
            //        mtfty.Moving_AGPrice =(float)Convert.ToDouble( cbb_移动平均价.Text);
            //        if (cbb_标准价格.Text == "")
            //            mtfty.Normal_Price = 0;
            //        else
            //        mtfty.Normal_Price = (float)Convert.ToDouble(cbb_标准价格.Text);
            //        mtfty.Price_Unit = cbb_价格单位.Text;
            //        if (cbb_总价值.Text == "")
            //            mtfty.TotalValue = 0;
            //        else

            //        mtfty.TotalValue =(float)Convert.ToDouble( cbb_总价值.Text);
            //        mab.UpdateMtFtyInformation(mtfty);
            //        //try
            //        //{
            //        //    string sql2 = " INSERT INTO [MT_FTY] (Mtory_Naterial_ID,Material_Name,Factory_ID,Factory_Name,Currency_Unit,Assessment_Category,Current_Period ,Price_Determination,EI_Period,Evaluation_Class,Sales_Order,Price_Control,Moving_AGPrice,Gross_Inventory,EVA_Classification,Price_Unit,Normal_Price,Total_Value,Future_Price) VALUES ('" + cbb_物料编号.Text + "','" + cbb_物料名称.Text + "','" + cbb_工厂编码.Text + "','" + cbb_工厂名称.Text + "','" + cbb_货币.Text + "', '" + cbb_评估类别.Text + "','" + cbb_本期.Text + "','" + cbb_价格确定.Text + "','" + cbb_起始期.Text + "','" + cbb_评估类.Text + "','" + cbb_销售订单库存.Text + "','" + cbb_价格控制.Text + "','" + cbb_移动平均价.Text + "','" + cbb_总库存.Text + "','" + cbb_评估分类.Text + "','" + cbb_价格单位.Text + "','" + cbb_标准价格.Text + "','" + cbb_总价值.Text + "','" + cbb_未来价格.Text + "') SELECT * FROM [MT_FTY] WHERE Not exists(select 1 from [MT_FTY] WHERE Material_ID='" + cbb_物料编号.Text + "' and Factory_ID='" + cbb_工厂编码.Text + "') ";
            //        //    DBHelper.ExecuteNonQuery(sql2);
            //        //}
            //        //catch (Exception ex)
            //        //{
            //        //    string sql1 = " UPDATE [MT_FTY] SET Currency_Unit = '" + cbb_货币.Text + "',Assessment_Category = '" + cbb_评估类别.Text + "',Current_Period = '" + cbb_本期.Text + "',Price_Determination = '" + cbb_价格确定.Text + "',EI_Period = '" + cbb_起始期.Text + "',Evaluation_Class='" + cbb_评估类.Text + "',Sales_Order='" + cbb_销售订单库存.Text + "',Price_Control='" + cbb_价格控制.Text + "',Moving_AGPrice='" + cbb_移动平均价.Text + "',Gross_Inventory='" + cbb_总库存.Text + "',EVA_Classification='" + cbb_评估分类.Text + "',Price_Unit='" + cbb_价格单位.Text + "',Normal_Price='" + cbb_标准价格.Text + "',Total_Value='" + cbb_总价值.Text + "',Future_Price='" + cbb_未来价格.Text + "'  WHERE Material_ID='" + cbb_物料编号.Text + "'and Factory_ID='" + cbb_工厂编码.Text + "'";
            //        //    DBHelper.ExecuteNonQuery(sql1);
            //        //}

            //        MessageUtil.ShowTips("修改成功");
            //    }
            //}
        }

        private void MTACT_Load(object sender, EventArgs e)
        {
            //foreach (Control control in this.Controls)
            //{
            //    if (control.GetType().Name == "Label")
            //    {
            //        ((Label)control).Font = new Font("隶书", 9,label3.Font.Style);
            //    }
            //}

            List<string> list0 = mt.GetAllMaterialID();
            cbm.FuzzyQury(cbb_物料编号, list0);
            List<string> list1 = gn.GetAllMeasurement();
            cbm.FuzzyQury(cbb_计量单位, list1);
            cbm.FuzzyQury(cbb_价格单位, list1);
            List<string> list2 = act.GetCurrencyType();
            cbm.FuzzyQury(cbb_货币, list2);
            cbb_工厂编码.Enabled = false;
            cbb_工厂名称.Enabled = false;
            List<string> list3 = gn.GetAllDivision();
            cbm.FuzzyQury(cbb_产品组, list3);
            List<string> list4 = gn.GetAllEvaluationClass();
            cbm.FuzzyQury(cbb_评估类, list4);

            
            ////录入货币信息
            //string sql1 = "SELECT * FROM [Currency]";
            //DataTable dt1 = DBHelper.ExecuteQueryDT(sql1);
            //int j;
            //for (j = 0; j < Convert.ToInt32(dt1.Rows.Count.ToString()); j++)
            //{
            //    cbb_货币.Items.Add(dt1.Rows[j][2]);
            //}
            //cbb_物料编号.Items.Clear();
            //string sql = "SELECT * FROM [Material]";
            //DataTable dt = DBHelper.ExecuteQueryDT(sql);
            //int i;
            //for (i = 0; i < Convert.ToInt32(dt.Rows.Count.ToString()); i++)
            //{
            //    cbb_物料编号.Items.Add(dt.Rows[i][1]);
            //}

        }

        private void label20_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (cbb_物料编号.Text != "")
            {
                MaintainMarketPrice mmp = new MaintainMarketPrice(cbb_物料编号.Text);
                mmp.Show();
            }
            else
            {
                MessageUtil.ShowTips("请选择维护物料");
            }
        }



        private void cbb_货币_Leave(object sender, EventArgs e)
        {
            List<string> list = act.GetCurrencyType();
            if (cbb_货币.Text != "" && gn.IDInTheList(list, cbb_货币.Text) == true)
            {
                MessageUtil.ShowError("请选择正确的货币");
                cbb_货币.Focus();
            }
        }

        private void cbb_计量单位_Leave(object sender, EventArgs e)
        {
            List<string> list = gn.GetAllMeasurement();
            if (cbb_计量单位.Text != "" && gn.IDInTheList(list, cbb_计量单位.Text) == true)
            {
                MessageUtil.ShowError("请选择正确的计量单位");
                cbb_计量单位.Focus();
            }

        }

        private void cbb_评估类_Leave(object sender, EventArgs e)
        {
            List<string> list = gn.GetAllEvaluationClass();
            if (cbb_评估类.Text != "" && gn.IDInTheList(list, cbb_评估类.Text) == true)
            {
                MessageUtil.ShowError("请选择正确的评估类");
                cbb_评估类.Focus();
            }
        }

        private void cbb_物料编号_MouseClick(object sender, MouseEventArgs e)
        {
            
        }

        private void toolStripButton4_Click(object sender, EventArgs e)
        {
            int flag = 0;

            List<string> list2 = mt.GetAllMaterialID();
            if (cbb_物料编号.Text == "" || gn.IDInTheList(list2, cbb_物料编号.Text) == true)
            {
                MessageUtil.ShowError("请选择正确的物料编号");
                cbb_物料编号.Focus();
                flag = 1;
            }
            if (flag == 0 && MessageUtil.ShowYesNoAndTips("是否确定修改") == DialogResult.Yes)
            {
                if (cbb_工厂编码.Text == "")
                {
                    MaterialBase mb = new MaterialBase();
                    mb.Material_Name = cbb_物料名称.Text;
                    mb.Measurement = cbb_计量单位.Text;
                    mb.Division = cbb_产品组.Text;
                    mb.Material_ID = cbb_物料编号.Text;
                    mab.UpdateBasicInformation(mb);
                    MessageUtil.ShowTips("修改成功");

                    //string sql = "UPDATE [Material] SET Material_Name='" + cbb_物料名称.Text + "',Division='" + cbb_产品组.Text + "'WHERE Material_ID='" + cbb_物料编号.Text + "'";
                    //DBHelper.ExecuteNonQuery(sql);
                    //MessageBox.Show("修改成功");
                }
                else
                {
                    MaterialFactory mtfty = new MaterialFactory();
                    mtfty.Assessment_Category = cbb_评估类别.Text;
                    mtfty.Currency_Unit = cbb_货币.Text;
                    mtfty.Evaluation_Class = cbb_评估类.Text;
                    mtfty.Factory_ID = cbb_工厂编码.Text;
                    mtfty.Factory_Name = cbb_工厂名称.Text;
                    try
                    {
                        if (cbb_未来价格.Text == "")
                            mtfty.Future_Price = 0;
                        else
                            mtfty.Future_Price = (float)Convert.ToDouble(cbb_未来价格.Text);
                        if (cbb_总库存.Text == "")
                            mtfty.Gross_Inventory = 0;
                        else
                            mtfty.Gross_Inventory = Convert.ToInt16(cbb_总库存.Text);
                        mtfty.Material_ID = cbb_物料编号.Text;
                        mtfty.Material_Name = cbb_物料名称.Text;
                        if (cbb_移动平均价.Text == "")
                            mtfty.Moving_AGPrice = 0;
                        else
                            mtfty.Moving_AGPrice = (float)Convert.ToDouble(cbb_移动平均价.Text);
                        if (cbb_标准价格.Text == "")
                            mtfty.Normal_Price = 0;
                        else
                            mtfty.Normal_Price = (float)Convert.ToDouble(cbb_标准价格.Text);
                        mtfty.Price_Unit = cbb_价格单位.Text;
                        if (cbb_总价值.Text == "")
                            mtfty.TotalValue = 0;
                        else

                            mtfty.TotalValue = (float)Convert.ToDouble(cbb_总价值.Text);
                        mtfty.Price_Control = cbb_价格控制.Text;
                        mab.UpdateMtFtyInformation(mtfty);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("信息有误");
                        return;
                    }
                    //try
                    //{
                    //    string sql2 = " INSERT INTO [MT_FTY] (Mtory_Naterial_ID,Material_Name,Factory_ID,Factory_Name,Currency_Unit,Assessment_Category,Current_Period ,Price_Determination,EI_Period,Evaluation_Class,Sales_Order,Price_Control,Moving_AGPrice,Gross_Inventory,EVA_Classification,Price_Unit,Normal_Price,Total_Value,Future_Price) VALUES ('" + cbb_物料编号.Text + "','" + cbb_物料名称.Text + "','" + cbb_工厂编码.Text + "','" + cbb_工厂名称.Text + "','" + cbb_货币.Text + "', '" + cbb_评估类别.Text + "','" + cbb_本期.Text + "','" + cbb_价格确定.Text + "','" + cbb_起始期.Text + "','" + cbb_评估类.Text + "','" + cbb_销售订单库存.Text + "','" + cbb_价格控制.Text + "','" + cbb_移动平均价.Text + "','" + cbb_总库存.Text + "','" + cbb_评估分类.Text + "','" + cbb_价格单位.Text + "','" + cbb_标准价格.Text + "','" + cbb_总价值.Text + "','" + cbb_未来价格.Text + "') SELECT * FROM [MT_FTY] WHERE Not exists(select 1 from [MT_FTY] WHERE Material_ID='" + cbb_物料编号.Text + "' and Factory_ID='" + cbb_工厂编码.Text + "') ";
                    //    DBHelper.ExecuteNonQuery(sql2);
                    //}
                    //catch (Exception ex)
                    //{
                    //    string sql1 = " UPDATE [MT_FTY] SET Currency_Unit = '" + cbb_货币.Text + "',Assessment_Category = '" + cbb_评估类别.Text + "',Current_Period = '" + cbb_本期.Text + "',Price_Determination = '" + cbb_价格确定.Text + "',EI_Period = '" + cbb_起始期.Text + "',Evaluation_Class='" + cbb_评估类.Text + "',Sales_Order='" + cbb_销售订单库存.Text + "',Price_Control='" + cbb_价格控制.Text + "',Moving_AGPrice='" + cbb_移动平均价.Text + "',Gross_Inventory='" + cbb_总库存.Text + "',EVA_Classification='" + cbb_评估分类.Text + "',Price_Unit='" + cbb_价格单位.Text + "',Normal_Price='" + cbb_标准价格.Text + "',Total_Value='" + cbb_总价值.Text + "',Future_Price='" + cbb_未来价格.Text + "'  WHERE Material_ID='" + cbb_物料编号.Text + "'and Factory_ID='" + cbb_工厂编码.Text + "'";
                    //    DBHelper.ExecuteNonQuery(sql1);
                    //}

                    MessageUtil.ShowTips("修改成功");
                }
            }
        }

        private void toolStripButton7_Click(object sender, EventArgs e)
        {
            foreach (Control control in this.Controls)
            {
                if (control.GetType().Name == "ComboBox")
                {
                    ((ComboBox)control).Text = string.Empty;
                }
            }
            cbb_本期.Text = "";
            cbb_标准价格.Text = "0.0";
            cbb_产品组.Text = "";
            cbb_工厂编码.Text = "";
            cbb_工厂名称.Text = "";
            cbb_货币.Text = "";
            cbb_计量单位.Text = "";
            cbb_价格单位.Text = "";
            cbb_价格控制.Text = "";
            cbb_价格确定.Text = "";
            cbb_评估分类.Text = "";
            cbb_评估类.Text = "";
            cbb_评估类别.Text = "";
            cbb_未来价格.Text = "0.0";
            cbb_物料编号.Text = "";
            cbb_物料名称.Text = "";
            cbb_销售订单库存.Text = "";
            cbb_移动平均价.Text = "0.0";
            cbb_总价值.Text = "0.0";
            cbb_总库存.Text = "";
            cbb_本期.Visible = true;
            cbb_标准价格.Visible = true;
            cbb_工厂编码.Visible = true;
            cbb_工厂名称.Visible = true;
            cbb_货币.Visible = true;
            cbb_价格单位.Visible = true;
            cbb_价格控制.Visible = true;
            cbb_价格确定.Visible = true;
            cbb_评估分类.Visible = true;
            cbb_评估类.Visible = true;
            cbb_评估类别.Visible = true;
            cbb_未来价格.Visible = true;
            cbb_销售订单库存.Visible = true;
            cbb_移动平均价.Visible = true;
            cbb_总价值.Visible = true;
            cbb_总库存.Visible = true;
            cbb_本期.Enabled = true;
            cbb_标准价格.Enabled = true;
            cbb_货币.Enabled = true;
            cbb_价格单位.Enabled = true;
            cbb_价格控制.Enabled = true;
            cbb_价格确定.Enabled = true;
            cbb_评估分类.Enabled = true;
            cbb_评估类.Enabled = true;
            cbb_评估类别.Enabled = true;
            cbb_未来价格.Enabled = true;
            cbb_销售订单库存.Enabled = true;
            cbb_移动平均价.Enabled = true;
            cbb_总价值.Enabled = true;
            cbb_总库存.Enabled = true;
            cbb_产品组.Enabled = true;
            cbb_计量单位.Enabled = true;
            gbx_当前估价.Visible = true;

        }

        private void toolStripButton8_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            MaterialChose mtc = new MaterialChose(0, this);
            mtc.Show();
        }

        private void cbb_价格确定_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void cbb_移动平均价_Leave(object sender, EventArgs e)
        {
            Dictionary<string, string> dict_temp = new Dictionary<string, string>();
            string key = "移动平均价";
            dict_temp.Add(key, "");
            formh.onlyNumber(cbb_移动平均价, key, dict_temp);
        }

        private void cbb_标准价格_Leave(object sender, EventArgs e)
        {
            Dictionary<string, string> dict_temp = new Dictionary<string, string>();
            string key = "标准价格";
            dict_temp.Add(key, "");
            formh.onlyNumber(cbb_标准价格, key, dict_temp);
        }

        private void cbb_未来价格_Leave(object sender, EventArgs e)
        {
            Dictionary<string, string> dict_temp = new Dictionary<string, string>();
            string key = "未来价格";
            dict_temp.Add(key, "");
            formh.onlyNumber(cbb_未来价格, key, dict_temp);
        }

        private void cbb_总库存_Leave(object sender, EventArgs e)
        {
            Dictionary<string, string> dict_temp = new Dictionary<string, string>();
            string key = "总库存";
            dict_temp.Add(key, "");
            formh.onlyNumber(cbb_总库存, key, dict_temp);
        }

        private void cbb_总价值_Leave(object sender, EventArgs e)
        {
            Dictionary<string, string> dict_temp = new Dictionary<string, string>();
            string key = "总价值";
            dict_temp.Add(key, "");
            formh.onlyNumber(cbb_总价值, key, dict_temp);
        }

        private void cbb_销售订单库存_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
