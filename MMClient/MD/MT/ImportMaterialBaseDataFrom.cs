﻿using Lib.Common.MMCException.IDAL;
using Lib.SqlServerDAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MMClient.MD.MT
{  
    public partial class ImportMaterialBaseDataFrom : Form
    {
        private string OledbConnString = "Provider = Microsoft.Jet.OLEDB.4.0 ; Data Source = {0};Extended Properties='Excel 8.0;HDR=Yes;IMEX=1;'";
        private List<string> listSQL = null;
        public ImportMaterialBaseDataFrom()
        {
            InitializeComponent();
            setNULL(); 
        }
        /// <summary>
        /// 指控
        /// </summary>
        private void setNULL()
        {
            listSQL = new List<string>();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            setNULL();
            DialogResult dr1 = this.openFileDialog1.ShowDialog();
            if (DialogResult.OK == dr1) //判断是否选择文件  
            {
                string path = this.openFileDialog1.FileName.Trim();
                if (string.IsNullOrEmpty(path))
                {
                    MessageBox.Show("请选择要导入的EXCEL文件！！！", "信息");
                    return;
                }
                if (!File.Exists(path))  //判断文件是否存在  
                {
                    MessageBox.Show("信息", "找不到对应的Excel文件，请重新选择。");
                    return;
                }
                DataTable excelTbl = this.GetExcelTable(path);  //调用函数获取Excel中的信息  
                
                if (excelTbl == null)
                {
                    return;
                }
                //List
                List<string> listID = new List<string>();
                List<string> nonlistID = new List<string>();
                List<string> MMType = new List<string>();
                MMType.Add("");
                for (int i = 0; i < excelTbl.Rows.Count; i++)
                {
                    MMType.Add(excelTbl.Rows[i][2].ToString());
                }
                int count = 0;
                try {
                    for(int i = 0;i<excelTbl.Rows.Count;i++)
                    {
                        string matreialName = ""; 
                        string materialGroup = "";
                        string materialType = ""; 
                        string mateialField = "";
                        if (excelTbl.Rows[i][0] != null)
                            matreialName = excelTbl.Rows[i][0].ToString();
                        if (excelTbl.Rows[i][1] != null)
                            materialGroup = excelTbl.Rows[i][1].ToString();
                        if (excelTbl.Rows[i][2] != null)
                            materialType = excelTbl.Rows[i][2].ToString();
                        if (excelTbl.Rows[i][3] != null)
                            mateialField = excelTbl.Rows[i][3].ToString();
                        count++;
                        //判断物料组与物料的关系
                        string sqlCategory = "select Category from Material_Group WHERE Material_Group='"+ materialGroup + "'";
                        DataTable dtCategory = DBHelper.ExecuteQueryDT(sqlCategory);
                        if (dtCategory.Rows.Count == 0)
                        {
                            MessageBox.Show("第 "+ count + " 行，该物料组不属于任何物料类型，请先维护\n物料组："+ materialGroup + " 与物料类型："+ materialType + "的关系", "检查");
                            return;
                        }
                        
                        if(!string.IsNullOrEmpty(matreialName) && !string.IsNullOrEmpty(materialGroup)
                            && !string.IsNullOrEmpty(materialType) && !string.IsNullOrEmpty(mateialField))
                        {
                            string sql = "select outSupport ,innnerStart,innnerEnd,outterRangeStart,outterRangeEnd ,innnerLength,outterLength from Bigclassfy WHERE ClassfyID='" + materialType + "'";
                            DataTable dt = DBHelper.ExecuteQueryDT(sql);
                            if (dt.Rows.Count > 0)
                            {
                                string startRange = "";
                                int outterLength = Convert.ToInt32(dt.Rows[0][6].ToString());
                                string maxStrID = "";
                                for (int j = 0; j < outterLength; j++) maxStrID += "9";
                                //得到起始ID
                                DataTable hadMaterialIDTable = DBHelper.ExecuteQueryDT("select Material_ID from Material where Material_Type = '" + materialType + "' AND isOut = '1'  ORDER BY Material_ID DESC  ");
                                //如果hadMaterialIDTable有起始ID 
                                if (hadMaterialIDTable.Rows.Count >= 1)
                                {
                                    string diffType = "";
                                    if (listID.Count <= 0 || (MMType[i] != MMType[i+1]))
                                    {
                                        diffType = hadMaterialIDTable.Rows[0][0].ToString().Trim().Substring(5);
                                    }
                                    startRange = addOne(diffType == "" ? listID[i-1] : diffType);
                                    listID.Add(startRange);
                                    nonlistID.Add("");
                                    if (listID[i].Equals(maxStrID))
                                    {
                                        MessageBox.Show("已达最大长度，添加被截断");
                                        return;
                                    }
                                    
                                    string Material_ID = materialType + startRange;
                                    //检查是否存在
                                    bool isCheck = checkExsis(Material_ID);
                                    while (isCheck)
                                    {
                                        Material_ID = addOne(Material_ID);
                                        isCheck = checkExsis(Material_ID);
                                    }
                                    string insertSql = "INSERT INTO Material (Material_ID,Material_Name,Material_Group,Material_Type,Industry_Category,isOut) VALUES('" + Material_ID + "','" + matreialName + "','" + materialGroup + "','" + materialType + "','" + mateialField + "','1') ";
                                    listSQL.Add(insertSql);
                                   
                                    DataGridViewRow row = new DataGridViewRow();
                                    DataGridViewTextBoxCell textboxcell = new DataGridViewTextBoxCell();
                                    textboxcell.Value = matreialName;
                                    row.Cells.Add(textboxcell);
                                    DataGridViewTextBoxCell textboxcel2 = new DataGridViewTextBoxCell();
                                    textboxcel2.Value = materialGroup;
                                    row.Cells.Add(textboxcel2);
                                    DataGridViewTextBoxCell textboxcel3 = new DataGridViewTextBoxCell();
                                    textboxcel3.Value = materialType;
                                    row.Cells.Add(textboxcel3);
                                    DataGridViewTextBoxCell textboxcel4 = new DataGridViewTextBoxCell();
                                    textboxcel4.Value = mateialField;
                                    row.Cells.Add(textboxcel4);
                                    dgv_MaterialBaseData.Rows.Add(row);
                                    textboxcell = null;
                                    textboxcel2 = null;
                                    textboxcel3 = null;
                                    textboxcel4 = null;
                                    row = null; 
                                }
                                //数据表关于此类型的物料数据为空
                                else
                                {
                                    string diffType = "";
                                    if (nonlistID.Count <= 0 ||(MMType[i] != MMType[i + 1]))
                                    {
                                        diffType = dt.Rows[0][3].ToString();
                                    }
                                    startRange = addOne(diffType == "" ? nonlistID[i-1] : diffType);
                                    nonlistID.Add(startRange);
                                    listID.Add("");
                                    if (nonlistID[i].Equals(maxStrID))
                                    {
                                        MessageBox.Show("已达最大长度，添加被截断");
                                        return;
                                    }
                                    string Material_ID = materialType + "1" + startRange;
                                    bool isCheck = checkExsis(Material_ID);
                                    while (isCheck)
                                    {
                                        Material_ID = addOne(Material_ID);
                                        isCheck = checkExsis(Material_ID);
                                    }
                                    string insertSql = "INSERT INTO Material (Material_ID,Material_Name,Material_Group,Material_Type,Industry_Category,isOut) VALUES('" + Material_ID + "','" + matreialName + "','" + materialGroup + "','" + materialType + "','" + mateialField + "','1') ";
                                    listSQL.Add(insertSql);
                                    DataGridViewRow row = new DataGridViewRow();
                                    DataGridViewTextBoxCell textboxcell = new DataGridViewTextBoxCell();
                                    textboxcell.Value = matreialName;
                                    row.Cells.Add(textboxcell);
                                    DataGridViewTextBoxCell textboxcel2 = new DataGridViewTextBoxCell();
                                    textboxcel2.Value = materialGroup;
                                    row.Cells.Add(textboxcel2);
                                    DataGridViewTextBoxCell textboxcel3 = new DataGridViewTextBoxCell();
                                    textboxcel3.Value = materialType;
                                    row.Cells.Add(textboxcel3);
                                    DataGridViewTextBoxCell textboxcel4 = new DataGridViewTextBoxCell();
                                    textboxcel4.Value = mateialField;
                                    row.Cells.Add(textboxcel4);
                                    dgv_MaterialBaseData.Rows.Add(row);
                                    textboxcell = null;
                                    textboxcel2 = null;
                                    textboxcel3 = null;
                                    textboxcel4 = null;
                                    row = null;
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("程序异常终止");
                    return;
                }
                if (dgv_MaterialBaseData == null || dgv_MaterialBaseData.Rows.Count<=0)
                {
                    MessageBox.Show("导入的数据全部不符合数据要求");
                    return;
                }
            }
        }

        private bool checkExsis(string material_ID)
        {
            string checksql = "select * from Material where Material_ID = '" + material_ID + "'";
            DataTable dataTable = DBHelper.ExecuteQueryDT(checksql);
            //已存在这样的物料编号则＋1
            if (dataTable.Rows.Count > 0 && !string.IsNullOrEmpty(dataTable.Rows[0][0].ToString()))
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// 字符串+1
        /// </summary>
        /// <param name="baseStr"></param>
        /// <returns></returns>
        private string addOne(string baseStr)
        {
            int index = 0;
            int newInt = 0; ;
            for (int i = baseStr.Length - 1; i > 0; i--)
            {
                if (baseStr[i] != '9')
                {
                    index = i;
                    break;
                }
            }
            newInt = Convert.ToInt32(baseStr.Substring(index)) + 1;
            return baseStr.Substring(0, index) + newInt.ToString();
        }
        /// <summary>
        /// 转成Table
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        private DataTable GetExcelTable(string path)
        {
            try
            {
                //获取excel数据  
                DataTable dt1 = new DataTable();
                if (!(path.ToLower().IndexOf(".xlsx") < 0))
                {
                    OledbConnString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source='{0}';Extended Properties='Excel 12.0;HDR=YES'";
                }
                string strConn = string.Format(OledbConnString, path);
                OleDbConnection conn = new OleDbConnection(strConn);
                conn.Open();
                DataTable dt = conn.GetSchema("Tables");
                //判断excel的sheet页数量，查询第1页  
                if (dt.Rows.Count > 0)
                {
                    string selSqlStr = string.Format("select * from [{0}]", dt.Rows[0]["TABLE_NAME"]);
                    OleDbDataAdapter oleDa = new OleDbDataAdapter(selSqlStr, conn);
                    oleDa.Fill(dt1);
                }
                conn.Close();
                //现先对dataTable按物料类型排序
                dt1.DefaultView.Sort = "物料类型编号 ASC";
                dt1 = dt1.DefaultView.ToTable();
                return dt1;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Excel转换DataTable出错：" + ex.Message);
                return null;
            }
        }
        /// <summary>
        /// 保存
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            if (listSQL.Count > 0)
            {
                MessageBoxButtons messButton = MessageBoxButtons.OKCancel;
                DialogResult dr = MessageBox.Show("是否确定将此表格的\n数据写入数据库", "注  意", messButton);
                if (dr == DialogResult.OK)
                {
                    int[] arrError = new int[listSQL.Count];
                    int errorFlag = 0;
                    for (int i = 0; i < listSQL.Count; i++)
                    {
                        arrError[i] = -1;//初始化值
                        try
                        {
                            DBHelper.ExecuteQueryDS(listSQL[i]);
                        }
                        catch (DBException exc)
                        {
                            arrError[i] = -1 ;
                            errorFlag = i;
                            MessageBox.Show("插入失败");
                        }
                    }
                    if (errorFlag == 0)
                    {
                        MessageBox.Show("保存成功");
                        this.button1.Visible = false;
                        this.Close();
                    }
                    else
                    {//删除正确行
                        for(int i= 0;i<arrError.Length;i++)
                        {
                            if(arrError[i]!=-1)
                            {
                                DataGridViewRow row = dgv_MaterialBaseData.Rows[i];
                                dgv_MaterialBaseData.Rows.Remove(row);
                                row = null;
                            }
                        }
                        this.errorMessageLb.Visible = true;
                        this.errorMessageLb.Text = "注意：当前表格所包含的数据是不被接受插入的数据";
                    }
                }
            }
            else
            {
                MessageBox.Show("无数据可保存","注意");
            }
        }
    }
}
