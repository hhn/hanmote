﻿namespace MMClient.MD.MT.StrategInfo
{
    partial class StrateInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.CB_strategyTypeName = new System.Windows.Forms.ComboBox();
            this.dgv_strategyInfo = new System.Windows.Forms.DataGridView();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.删除ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.保存ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.区分类型维护ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.保存ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lb_typeName = new System.Windows.Forms.Label();
            this.pageTool = new pager.pagetool.pageNext();
            this.panel1 = new System.Windows.Forms.Panel();
            this.strategyId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.strategyInfo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_strategyInfo)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(31, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "区分类型";
            // 
            // CB_strategyTypeName
            // 
            this.CB_strategyTypeName.FormattingEnabled = true;
            this.CB_strategyTypeName.Location = new System.Drawing.Point(90, 37);
            this.CB_strategyTypeName.Name = "CB_strategyTypeName";
            this.CB_strategyTypeName.Size = new System.Drawing.Size(121, 20);
            this.CB_strategyTypeName.TabIndex = 1;
            this.CB_strategyTypeName.TextChanged += new System.EventHandler(this.CB_strategyTypeName_TextChanged);
            // 
            // dgv_strategyInfo
            // 
            this.dgv_strategyInfo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.dgv_strategyInfo.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_strategyInfo.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.dgv_strategyInfo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgv_strategyInfo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_strategyInfo.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.strategyId,
            this.strategyInfo});
            this.dgv_strategyInfo.Location = new System.Drawing.Point(3, 0);
            this.dgv_strategyInfo.Name = "dgv_strategyInfo";
            this.dgv_strategyInfo.RowTemplate.Height = 23;
            this.dgv_strategyInfo.Size = new System.Drawing.Size(992, 520);
            this.dgv_strategyInfo.TabIndex = 2;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.删除ToolStripMenuItem,
            this.保存ToolStripMenuItem1});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(101, 48);
            // 
            // 删除ToolStripMenuItem
            // 
            this.删除ToolStripMenuItem.Name = "删除ToolStripMenuItem";
            this.删除ToolStripMenuItem.Size = new System.Drawing.Size(100, 22);
            this.删除ToolStripMenuItem.Text = "删除";
            this.删除ToolStripMenuItem.Click += new System.EventHandler(this.删除ToolStripMenuItem_Click);
            // 
            // 保存ToolStripMenuItem1
            // 
            this.保存ToolStripMenuItem1.Name = "保存ToolStripMenuItem1";
            this.保存ToolStripMenuItem1.Size = new System.Drawing.Size(100, 22);
            this.保存ToolStripMenuItem1.Text = "保存";
            this.保存ToolStripMenuItem1.Click += new System.EventHandler(this.保存ToolStripMenuItem1_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.区分类型维护ToolStripMenuItem,
            this.保存ToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1082, 25);
            this.menuStrip1.TabIndex = 3;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // 区分类型维护ToolStripMenuItem
            // 
            this.区分类型维护ToolStripMenuItem.Name = "区分类型维护ToolStripMenuItem";
            this.区分类型维护ToolStripMenuItem.Size = new System.Drawing.Size(92, 21);
            this.区分类型维护ToolStripMenuItem.Text = "维护区分类型";
            this.区分类型维护ToolStripMenuItem.Click += new System.EventHandler(this.区分类型维护ToolStripMenuItem_Click);
            // 
            // 保存ToolStripMenuItem
            // 
            this.保存ToolStripMenuItem.Name = "保存ToolStripMenuItem";
            this.保存ToolStripMenuItem.Size = new System.Drawing.Size(44, 21);
            this.保存ToolStripMenuItem.Text = "保存";
            this.保存ToolStripMenuItem.Click += new System.EventHandler(this.保存ToolStripMenuItem_Click);
            // 
            // lb_typeName
            // 
            this.lb_typeName.AutoSize = true;
            this.lb_typeName.Location = new System.Drawing.Point(217, 40);
            this.lb_typeName.Name = "lb_typeName";
            this.lb_typeName.Size = new System.Drawing.Size(0, 12);
            this.lb_typeName.TabIndex = 5;
            // 
            // pageTool
            // 
            this.pageTool.Location = new System.Drawing.Point(129, 542);
            this.pageTool.Name = "pageTool";
            this.pageTool.PageIndex = 1;
            this.pageTool.PageSize = 100;
            this.pageTool.RecordCount = 0;
            this.pageTool.Size = new System.Drawing.Size(685, 28);
            this.pageTool.TabIndex = 23;
            this.pageTool.Load += new System.EventHandler(this.pageTool_Load);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.dgv_strategyInfo);
            this.panel1.Controls.Add(this.pageTool);
            this.panel1.Location = new System.Drawing.Point(33, 84);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(998, 627);
            this.panel1.TabIndex = 25;
            // 
            // strategyId
            // 
            this.strategyId.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.strategyId.ContextMenuStrip = this.contextMenuStrip1;
            this.strategyId.DataPropertyName = "strategyId";
            this.strategyId.HeaderText = "编号";
            this.strategyId.Name = "strategyId";
            this.strategyId.Width = 54;
            // 
            // strategyInfo
            // 
            this.strategyInfo.ContextMenuStrip = this.contextMenuStrip1;
            this.strategyInfo.DataPropertyName = "strategyInfo";
            this.strategyInfo.HeaderText = "子项";
            this.strategyInfo.Name = "strategyInfo";
            // 
            // StrateInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1082, 778);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.lb_typeName);
            this.Controls.Add(this.CB_strategyTypeName);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.menuStrip1);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "StrateInfo";
            this.Text = "维护策略信息库";
            this.Load += new System.EventHandler(this.StrateInfo_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_strategyInfo)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox CB_strategyTypeName;
        private System.Windows.Forms.DataGridView dgv_strategyInfo;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 区分类型维护ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 保存ToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 删除ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 保存ToolStripMenuItem1;
        private System.Windows.Forms.Label lb_typeName;
        private pager.pagetool.pageNext pageTool;
        private System.Windows.Forms.DataGridViewTextBoxColumn strategyId;
        private System.Windows.Forms.DataGridViewTextBoxColumn strategyInfo;
        private System.Windows.Forms.Panel panel1;
    }
}