﻿using Lib.Common.MMCException.IDAL;
using Lib.SqlServerDAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MMClient.MD.MT.StrategInfo
{
    public partial class addPurDiciedFactoryForm : Form
    {
        DiviedPurOrgnitionsForm diviedPurOrgnitionsForm = null;
        public addPurDiciedFactoryForm(DiviedPurOrgnitionsForm diviedPurOrgnitionsForm)
        {
            this.diviedPurOrgnitionsForm = diviedPurOrgnitionsForm;
            InitializeComponent();
        }

        private void addPurDiciedFactoryForm_Load(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = DBHelper.ExecuteQueryDT("select Buyer_Org as id ,Buyer_Org_Name as name  from Buyer_Org");
                this.comboBox1.DataSource = dt;
                this.comboBox1.DisplayMember = "name";
                this.comboBox1.ValueMember = "id";
                if (dt != null && dt.Rows.Count > 0)
                {
                    string Buyer_Org = this.comboBox1.SelectedValue.ToString();
                    DataTable dt2 = DBHelper.ExecuteQueryDT("select Factory_ID as id ,Factory_Name as name  from Factory  where Factory_ID not in (select FactoryID from Factory_Purcharse_Organization )");
                    this.comboBox2.DataSource = dt2;
                    this.comboBox2.DisplayMember = "name";
                    this.comboBox2.ValueMember = "id";
                    if (dt2 != null && dt2.Rows.Count > 0)
                        this.comboBox2.SelectedIndex = 0;
                }
            }
            catch (DBException ex)
            {
                MessageBox.Show("数据加载失败");
                return;
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string Buyer_Org = this.comboBox1.SelectedValue.ToString();
                DataTable dt = DBHelper.ExecuteQueryDT("select Factory_ID as id ,Factory_Name as name  from Factory  where Factory_ID not in (select FactoryID from Factory_Purcharse_Organization )");
                this.comboBox2.DataSource = dt;
                this.comboBox2.DisplayMember = "name";
                this.comboBox2.ValueMember = "id";
                if (dt != null && dt.Rows.Count > 0)
                    this.comboBox2.SelectedIndex = 0;
            }
            catch (DBException ex)
            {
                MessageBox.Show("数据加载失败");
                return;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(this.comboBox1.SelectedValue.ToString()))
            {
                MessageBox.Show("检查数据完整性");
                return;
            }
            if (string.IsNullOrEmpty(this.comboBox2.Text.ToString().Trim()) || this.comboBox2.SelectedValue == null || string.IsNullOrEmpty(this.comboBox2.SelectedValue.ToString()))
            {
                MessageBox.Show("检查数据完整性");
                return;
            }
            string inSQL = "INSERT INTO [MMBackup].[dbo].[Factory_Purcharse_Organization] ([FactoryID], [PurcharseOrgazationID]) VALUES ('" + this.comboBox2.SelectedValue.ToString() + "', '" + this.comboBox1.SelectedValue.ToString() + "')";
            try
            {
                DBHelper.ExecuteNonQuery(inSQL);
                MessageBox.Show("插入成功");
                addPurDiciedFactoryForm_Load(sender,e);
            }
            catch (DBException ex)
            {
                MessageBox.Show("数据插入失败");
            }
        }
    }
}
