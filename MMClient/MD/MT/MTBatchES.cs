﻿using System;
using System.Windows.Forms;
using Lib.SqlServerDAL;

namespace MMClient.MD
{
    public partial class MTBatch : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        public MTBatch()
        {
            InitializeComponent();
        }

        private void btn_清除_Click(object sender, EventArgs e)
        {
            cbb_出货日期.Text = "";
            cbb_批次ID.Text = "";
            cbb_批次级别.Text = "";
            cbb_批次名称.Text = "";
            cbb_生产日期.Text = "";
            cbb_收货日期.Text = "";
            cbb_物料编码.Text = "";
            cbb_到货日期.Text = "";
        }

        private void btn_录入_Click(object sender, EventArgs e)
        {
            string spdate = cbb_出货日期.Text;
            string bhid = cbb_批次ID.Text;
            string bhlevel = cbb_批次级别.Text;
            string bhname = cbb_批次名称.Text;
            string prdate = cbb_生产日期.Text;
            string redate = cbb_收货日期.Text;
            string mtid = cbb_物料编码.Text;
            string dudate = cbb_到货日期.Text;
            string sql = "INSERT INTO [MM].[dbo].[Batch](Delivery_Date,Batch_ID,Batch_Grade,Batch_Name,Production_Date,Receipt_Date,Material_ID,Due_Date)VALUES('" + spdate + "','" + bhid + "','" + bhlevel + "','" + bhname + "','" + prdate + "','" + redate + "','" + mtid + "','" + dudate + "')";
            DBHelper.ExecuteNonQuery(sql);
            MessageBox.Show("创建成功");
        }
    }
}
