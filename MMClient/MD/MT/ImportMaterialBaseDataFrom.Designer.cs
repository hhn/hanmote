﻿namespace MMClient.MD.MT
{
    partial class ImportMaterialBaseDataFrom
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.errorMessageLb = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.dgv_MaterialBaseData = new System.Windows.Forms.DataGridView();
            this.物料名称 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.物料组编号 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.物料类型编号 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.领域 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.button1 = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_MaterialBaseData)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.errorMessageLb);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.button2);
            this.panel1.Controls.Add(this.dgv_MaterialBaseData);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(980, 565);
            this.panel1.TabIndex = 0;
            // 
            // errorMessageLb
            // 
            this.errorMessageLb.AutoSize = true;
            this.errorMessageLb.ForeColor = System.Drawing.Color.Red;
            this.errorMessageLb.Location = new System.Drawing.Point(12, 542);
            this.errorMessageLb.Name = "errorMessageLb";
            this.errorMessageLb.Size = new System.Drawing.Size(83, 12);
            this.errorMessageLb.TabIndex = 2;
            this.errorMessageLb.Text = "errorMessage:";
            this.errorMessageLb.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(173, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(161, 12);
            this.label1.TabIndex = 10;
            this.label1.Text = "因数据较多导入后请耐心等待";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(14, 3);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(126, 34);
            this.button2.TabIndex = 9;
            this.button2.Text = "选择数据源";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // dgv_MaterialBaseData
            // 
            this.dgv_MaterialBaseData.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgv_MaterialBaseData.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.dgv_MaterialBaseData.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgv_MaterialBaseData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_MaterialBaseData.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.物料名称,
            this.物料组编号,
            this.物料类型编号,
            this.领域});
            this.dgv_MaterialBaseData.Location = new System.Drawing.Point(3, 43);
            this.dgv_MaterialBaseData.Name = "dgv_MaterialBaseData";
            this.dgv_MaterialBaseData.RowTemplate.Height = 23;
            this.dgv_MaterialBaseData.Size = new System.Drawing.Size(974, 490);
            this.dgv_MaterialBaseData.TabIndex = 8;
            // 
            // 物料名称
            // 
            this.物料名称.HeaderText = "物料名称";
            this.物料名称.Name = "物料名称";
            // 
            // 物料组编号
            // 
            this.物料组编号.DataPropertyName = "Country_Eng";
            this.物料组编号.HeaderText = "物料组编号";
            this.物料组编号.Name = "物料组编号";
            // 
            // 物料类型编号
            // 
            this.物料类型编号.HeaderText = "物料类型编号";
            this.物料类型编号.Name = "物料类型编号";
            // 
            // 领域
            // 
            this.领域.HeaderText = "行业领域";
            this.领域.Name = "领域";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(439, 583);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(117, 36);
            this.button1.TabIndex = 1;
            this.button1.Text = "保存";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "物料名称";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "Country_Eng";
            this.dataGridViewTextBoxColumn2.HeaderText = "物料组";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "物料类型";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.HeaderText = "领域";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            // 
            // ImportMaterialBaseDataFrom
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1004, 664);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.panel1);
            this.Name = "ImportMaterialBaseDataFrom";
            this.Text = "导入物料基本数据";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_MaterialBaseData)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.DataGridView dgv_MaterialBaseData;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridViewTextBoxColumn 物料名称;
        private System.Windows.Forms.DataGridViewTextBoxColumn 物料组编号;
        private System.Windows.Forms.DataGridViewTextBoxColumn 物料类型编号;
        private System.Windows.Forms.DataGridViewTextBoxColumn 领域;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.Label errorMessageLb;
    }
}