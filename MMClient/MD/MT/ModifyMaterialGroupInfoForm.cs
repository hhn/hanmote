﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Lib.Bll.MDBll.General;
using Lib.Common.CommonUtils;
using Lib.Common.MMCException.Bll;
using Lib.Model.MT_GroupModel;
using Lib.SqlServerDAL;

namespace MMClient.MD.MT
{
    public partial class ModifyMaterialGroupInfoForm : Form
    {
        private MaterialGroupModel materialGroupModel;
        private GeneralBLL gn = new GeneralBLL();
        public ModifyMaterialGroupInfoForm()
        {
            InitializeComponent();
        }

        public ModifyMaterialGroupInfoForm(MaterialGroupModel materialGroupModel)
        {
            this.materialGroupModel = materialGroupModel;
            InitializeComponent();
        }

        private void ModifyMaterialGroupInfoForm_Load(object sender, EventArgs e)
        {
            string type = "SELECT ClassfyID as id , Bigclassfy_Name as name from Bigclassfy";
            try
            {
                this.comboBox1.DataSource = DBHelper.ExecuteQueryDT(type);
                comboBox1.ValueMember = "id";
                comboBox1.DisplayMember = "name";

            }
            catch (BllException ex)
            {
                MessageBox.Show("数据加载");
                return;
            }
            initData();
        }

        private void initData()
        {
            tbx_物料组编号.Text = materialGroupModel.MtGroupId ;
            tbx_物料组描述.Text = materialGroupModel.MtGroupName ;
            comboBox1.SelectedValue = materialGroupModel.MtGroupClass;
            CB_mGroupLevel.Text = materialGroupModel.MtGroupLevel;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_确定_Click(object sender, EventArgs e)
        {
            if (MessageUtil.ShowYesNoAndTips("是否确定修改") == DialogResult.Yes)
            {
                materialGroupModel.MtGroupId = tbx_物料组编号.Text.ToString();
                materialGroupModel.MtGroupName = tbx_物料组描述.Text.ToString();
                materialGroupModel.MtGroupClass = comboBox1.SelectedValue.ToString();
                materialGroupModel.MtGroupLevel = CB_mGroupLevel.Text.ToString();
                try
                {
                    gn.UpdateMaterialGroup(materialGroupModel);
                    MessageBox.Show("修改成功");
                    this.Close();
                }
                catch (BllException ex)
                {
                    MessageBox.Show("修改失败");
                }
            }
            
        }
    }
}
