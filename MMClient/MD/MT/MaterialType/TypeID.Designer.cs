﻿namespace MMClient.MD.MT
{
    partial class TypeID
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cbb_原类型 = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btn_重置 = new System.Windows.Forms.Button();
            this.btn_确定 = new System.Windows.Forms.Button();
            this.cbb_新类型 = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.cbb_类型名称 = new System.Windows.Forms.ComboBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // cbb_原类型
            // 
            this.cbb_原类型.FormattingEnabled = true;
            this.cbb_原类型.Location = new System.Drawing.Point(303, 79);
            this.cbb_原类型.Name = "cbb_原类型";
            this.cbb_原类型.Size = new System.Drawing.Size(121, 24);
            this.cbb_原类型.TabIndex = 0;
            // 
            // supplierInforGroupBox
            // 
            this.groupBox1.Controls.Add(this.cbb_类型名称);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.btn_重置);
            this.groupBox1.Controls.Add(this.btn_确定);
            this.groupBox1.Controls.Add(this.cbb_新类型);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.cbb_原类型);
            this.groupBox1.Location = new System.Drawing.Point(5, 4);
            this.groupBox1.Name = "supplierInforGroupBox";
            this.groupBox1.Size = new System.Drawing.Size(1023, 407);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "更改类型ID";
            // 
            // btn_重置
            // 
            this.btn_重置.Location = new System.Drawing.Point(535, 236);
            this.btn_重置.Name = "btn_重置";
            this.btn_重置.Size = new System.Drawing.Size(64, 38);
            this.btn_重置.TabIndex = 5;
            this.btn_重置.Text = "重置";
            this.btn_重置.UseVisualStyleBackColor = true;
            this.btn_重置.Click += new System.EventHandler(this.btn_重置_Click);
            // 
            // btn_确定
            // 
            this.btn_确定.Location = new System.Drawing.Point(424, 236);
            this.btn_确定.Name = "btn_确定";
            this.btn_确定.Size = new System.Drawing.Size(75, 38);
            this.btn_确定.TabIndex = 4;
            this.btn_确定.Text = "确定";
            this.btn_确定.UseVisualStyleBackColor = true;
            this.btn_确定.Click += new System.EventHandler(this.btn_确定_Click);
            // 
            // cbb_新类型
            // 
            this.cbb_新类型.FormattingEnabled = true;
            this.cbb_新类型.Location = new System.Drawing.Point(303, 131);
            this.cbb_新类型.Name = "cbb_新类型";
            this.cbb_新类型.Size = new System.Drawing.Size(121, 24);
            this.cbb_新类型.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(130, 134);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "新类型ID";
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(130, 82);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(63, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "原类型ID";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(130, 33);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 17);
            this.label3.TabIndex = 6;
            this.label3.Text = "类型名称";
            // 
            // cbb_类型名称
            // 
            this.cbb_类型名称.FormattingEnabled = true;
            this.cbb_类型名称.Location = new System.Drawing.Point(303, 30);
            this.cbb_类型名称.Name = "cbb_类型名称";
            this.cbb_类型名称.Size = new System.Drawing.Size(121, 24);
            this.cbb_类型名称.TabIndex = 7;
            // 
            // TypeID
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1031, 414);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "TypeID";
            this.Text = "TypeID";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox cbb_新类型;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btn_重置;
        private System.Windows.Forms.Button btn_确定;
        public System.Windows.Forms.ComboBox cbb_原类型;
        private System.Windows.Forms.Label label3;
        public System.Windows.Forms.ComboBox cbb_类型名称;


    }
}