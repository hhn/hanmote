﻿namespace MMClient.MD.MT
{
    partial class MTType
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btn_刷新数据 = new System.Windows.Forms.Button();
            this.pageTool = new pager.pagetool.pageNext();
            this.dgv_物料类型 = new System.Windows.Forms.DataGridView();
            this.类型编码 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.类型描述 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.范围起始码 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.范围终止码 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.外部给号 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.物料起始码 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.物料终止码 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.英文缩写 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.button1 = new System.Windows.Forms.Button();
            this.btn_新条目 = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_物料类型)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.AutoSize = true;
            this.groupBox1.Controls.Add(this.btn_刷新数据);
            this.groupBox1.Controls.Add(this.pageTool);
            this.groupBox1.Controls.Add(this.dgv_物料类型);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.btn_新条目);
            this.groupBox1.Location = new System.Drawing.Point(3, 1);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1467, 554);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "修改视图-物料类型";
            // 
            // btn_刷新数据
            // 
            this.btn_刷新数据.Location = new System.Drawing.Point(27, 131);
            this.btn_刷新数据.Name = "btn_刷新数据";
            this.btn_刷新数据.Size = new System.Drawing.Size(109, 48);
            this.btn_刷新数据.TabIndex = 7;
            this.btn_刷新数据.Text = "刷新数据";
            this.btn_刷新数据.UseVisualStyleBackColor = true;
            this.btn_刷新数据.Click += new System.EventHandler(this.btn_刷新数据_Click_1);
            // 
            // pageTool
            // 
            this.pageTool.Location = new System.Drawing.Point(416, 457);
            this.pageTool.Name = "pageTool";
            this.pageTool.PageIndex = 1;
            this.pageTool.PageSize = 100;
            this.pageTool.RecordCount = 0;
            this.pageTool.Size = new System.Drawing.Size(660, 37);
            this.pageTool.TabIndex = 5;
            this.pageTool.OnPageChanged += new System.EventHandler(this.pageTool_OnPageChanged);
            this.pageTool.Load += new System.EventHandler(this.pageNext1_Load);
            // 
            // dgv_物料类型
            // 
            this.dgv_物料类型.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgv_物料类型.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.dgv_物料类型.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgv_物料类型.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_物料类型.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.类型编码,
            this.类型描述,
            this.范围起始码,
            this.范围终止码,
            this.外部给号,
            this.物料起始码,
            this.物料终止码,
            this.英文缩写});
            this.dgv_物料类型.Location = new System.Drawing.Point(165, 18);
            this.dgv_物料类型.Name = "dgv_物料类型";
            this.dgv_物料类型.RowTemplate.Height = 23;
            this.dgv_物料类型.Size = new System.Drawing.Size(1243, 433);
            this.dgv_物料类型.TabIndex = 4;
            this.dgv_物料类型.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgv_物料类型_CellMouseDoubleClick);
            // 
            // 类型编码
            // 
            this.类型编码.DataPropertyName = "ClassfyID";
            this.类型编码.HeaderText = "类型编码";
            this.类型编码.Name = "类型编码";
            this.类型编码.Width = 150;
            // 
            // 类型描述
            // 
            this.类型描述.DataPropertyName = "Bigclassfy_Name";
            this.类型描述.HeaderText = "类型名称";
            this.类型描述.Name = "类型描述";
            this.类型描述.Width = 150;
            // 
            // 范围起始码
            // 
            this.范围起始码.DataPropertyName = "innnerStart";
            this.范围起始码.HeaderText = "内部起始码";
            this.范围起始码.Name = "范围起始码";
            this.范围起始码.Width = 150;
            // 
            // 范围终止码
            // 
            this.范围终止码.DataPropertyName = "innnerEnd";
            this.范围终止码.HeaderText = "内部终止码";
            this.范围终止码.Name = "范围终止码";
            this.范围终止码.Width = 150;
            // 
            // 外部给号
            // 
            this.外部给号.DataPropertyName = "outSupport";
            this.外部给号.HeaderText = "外部给号";
            this.外部给号.Name = "外部给号";
            // 
            // 物料起始码
            // 
            this.物料起始码.DataPropertyName = "outterRangeStart";
            this.物料起始码.HeaderText = "外部起始码";
            this.物料起始码.Name = "物料起始码";
            // 
            // 物料终止码
            // 
            this.物料终止码.DataPropertyName = "outterRangeEnd";
            this.物料终止码.HeaderText = "外部终止码";
            this.物料终止码.Name = "物料终止码";
            this.物料终止码.Width = 150;
            // 
            // 英文缩写
            // 
            this.英文缩写.DataPropertyName = "description";
            this.英文缩写.HeaderText = "类型描述";
            this.英文缩写.Name = "英文缩写";
            this.英文缩写.Width = 250;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(27, 304);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(109, 48);
            this.button1.TabIndex = 3;
            this.button1.Text = "删除";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btn_新条目
            // 
            this.btn_新条目.Location = new System.Drawing.Point(27, 215);
            this.btn_新条目.Name = "btn_新条目";
            this.btn_新条目.Size = new System.Drawing.Size(109, 48);
            this.btn_新条目.TabIndex = 1;
            this.btn_新条目.Text = "新条目";
            this.btn_新条目.UseVisualStyleBackColor = true;
            this.btn_新条目.Click += new System.EventHandler(this.btn_新条目_Click);
            // 
            // MTType
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1502, 572);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "MTType";
            this.Text = "物料类型";
            this.Load += new System.EventHandler(this.MTType_Load);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_物料类型)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btn_新条目;
        private System.Windows.Forms.Button button1;
        private pager.pagetool.pageNext pageTool;
        private System.Windows.Forms.DataGridView dgv_物料类型;
        private System.Windows.Forms.Button btn_刷新数据;
        private System.Windows.Forms.DataGridViewTextBoxColumn 类型编码;
        private System.Windows.Forms.DataGridViewTextBoxColumn 类型描述;
        private System.Windows.Forms.DataGridViewTextBoxColumn 范围起始码;
        private System.Windows.Forms.DataGridViewTextBoxColumn 范围终止码;
        private System.Windows.Forms.DataGridViewTextBoxColumn 外部给号;
        private System.Windows.Forms.DataGridViewTextBoxColumn 物料起始码;
        private System.Windows.Forms.DataGridViewTextBoxColumn 物料终止码;
        private System.Windows.Forms.DataGridViewTextBoxColumn 英文缩写;
    }
}