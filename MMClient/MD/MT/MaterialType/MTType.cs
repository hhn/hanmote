﻿using System;
using System.Windows.Forms;
using Lib.SqlServerDAL;
using Lib.Common.CommonUtils;
using Lib.Common.MMCException.Bll;
using Lib.Common.MMCException.IDAL;
using MMClient.MD.MT.MaterialType;

namespace MMClient.MD.MT
{
    public partial class MTType : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        private MatrialTypeDetailsorModifyiedForm matrialTypeDetailsorModifyiedForm = null;
        public MTType()
        {
            InitializeComponent();
        }

        private void btn_刷新数据_Click(object sender, EventArgs e)
        {
            LoadData();
        }
        
        private void btn_新条目_Click(object sender, EventArgs e)
        {
            NewType ntp = new NewType();
            ntp.Show();
            LoadData();
        }

        private void MTType_Load(object sender, EventArgs e)
        {
            LoadData();
        }

        private void pageNext1_Load(object sender, EventArgs e)
        {
            string sql = "select count(*) from Bigclassfy";
            try
            {
                int i = int.Parse(DBHelper.ExecuteQueryDT(sql).Rows[0][0].ToString());
                int totalNum = i;
                pageTool.DrawControl(totalNum);
                //事件改变调用函数
                pageTool.OnPageChanged += pageTool_OnPageChanged;
            }
            catch (Lib.Common.MMCException.Bll.BllException ex)
            {
                MessageUtil.ShowTips("数据库异常，请稍后重试");
                return;
            }


        }

        private void LoadData()
        {
            try
            {
                dgv_物料类型.DataSource = FindBigclassfyInforByPage(this.pageTool.PageSize, this.pageTool.PageIndex);
            }
            catch (BllException ex)
            {
                MessageUtil.ShowError(ex.Message);
            }
        }

        private object FindBigclassfyInforByPage(int pageSize, int pageIndex)
        {
            try
            {
                string sql  = @"SELECT top " + pageSize + " Bigclassfy_Name,ClassfyID,innnerStart,innnerEnd,outSupport ,outterRangeStart ,outterRangeEnd,description FROM Bigclassfy  where ClassfyID not in(select top " + pageSize * (pageIndex - 1) + " ClassfyID from Bigclassfy ORDER BY ClassfyID ASC)ORDER BY ClassfyID";
                return DBHelper.ExecuteQueryDT(sql);
            }
            catch (DBException ex)
            {
                throw new BllException("当前无数据", ex);
            };
        }

        private void pageTool_OnPageChanged(object sender, EventArgs e)
        {
            LoadData();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int currentIndex = getCurrentSelectedRowIndex();
            DataGridViewRow row = this.dgv_物料类型.Rows[currentIndex];
            String ClassfyID = Convert.ToString(row.Cells["类型编码"].Value);
            string sql = "DELETE from Bigclassfy WHERE ClassfyID = '" + ClassfyID + "' ";
            try
            {
                DBHelper.ExecuteNonQuery(sql);
                LoadData();
            }catch(BllException ex)
            {
                MessageUtil.ShowTips("数据库异，请稍后重试");
            }
            pageNext1_Load(sender, e);
        }
        /// <summary>
        /// 当前行
        /// </summary>
        /// <returns></returns>
        private int getCurrentSelectedRowIndex()
        {
            if (this.dgv_物料类型.Rows.Count <= 0)
            {
                MessageUtil.ShowWarning("用户表为空，无法执行操作");
                return -1;
            }

            if (this.dgv_物料类型.CurrentRow.Index < 0)
            {
                MessageUtil.ShowWarning("请选择一行记录，然后进行操作！");
                return -1;
            }

            return this.dgv_物料类型.CurrentRow.Index;
        }

        private void btn_刷新数据_Click_1(object sender, EventArgs e)
        {
            btn_刷新数据_Click(sender,e);
            pageNext1_Load(sender,e);
        }
        /// <summary>
        /// 双击修改
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgv_物料类型_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            int currentIndex = getCurrentSelectedRowIndex();
            DataGridViewRow currentRow = this.dgv_物料类型.Rows[currentIndex];
            string typeID = currentRow.Cells["类型编码"].Value.ToString();
            if (typeID == "" || typeID.Equals(""))
            {
                MessageBox.Show("类型编号为空，无法进入页面");
                return;
            }
            if (matrialTypeDetailsorModifyiedForm == null)
            {
                matrialTypeDetailsorModifyiedForm = new MatrialTypeDetailsorModifyiedForm(typeID);
                matrialTypeDetailsorModifyiedForm.Show();
                matrialTypeDetailsorModifyiedForm = null;
            }
            LoadData();
        }

        
    }
}
