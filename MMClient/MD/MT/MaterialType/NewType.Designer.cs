﻿namespace MMClient.MD.MT
{
    partial class NewType
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btncheckOnlyOne = new System.Windows.Forms.Button();
            this.cbb_类型编号 = new System.Windows.Forms.TextBox();
            this.cbb_英文缩写 = new System.Windows.Forms.TextBox();
            this.cbb_类型名称 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.labelx1 = new System.Windows.Forms.Label();
            this.selfTypeLen = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.labelx5 = new System.Windows.Forms.Label();
            this.labelx6 = new System.Windows.Forms.Label();
            this.labelx4 = new System.Windows.Forms.Label();
            this.labelx3 = new System.Windows.Forms.Label();
            this.labelx2 = new System.Windows.Forms.Label();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.materNumCount = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.materialEnd = new System.Windows.Forms.TextBox();
            this.materialStart = new System.Windows.Forms.TextBox();
            this.materialTypeEnd = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.MaterialTypeStart = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.countTypeNum = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.cbTypeLen = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.btn_确定 = new System.Windows.Forms.Button();
            this.cbRowMaterial = new System.Windows.Forms.CheckBox();
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cbRowMaterial);
            this.groupBox1.Controls.Add(this.btncheckOnlyOne);
            this.groupBox1.Controls.Add(this.cbb_类型编号);
            this.groupBox1.Controls.Add(this.cbb_英文缩写);
            this.groupBox1.Controls.Add(this.cbb_类型名称);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.panel1);
            this.groupBox1.Controls.Add(this.btn_确定);
            this.groupBox1.Location = new System.Drawing.Point(14, 5);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(537, 630);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "新建物料类型";
            // 
            // btncheckOnlyOne
            // 
            this.btncheckOnlyOne.Location = new System.Drawing.Point(308, 109);
            this.btncheckOnlyOne.Name = "btncheckOnlyOne";
            this.btncheckOnlyOne.Size = new System.Drawing.Size(98, 23);
            this.btncheckOnlyOne.TabIndex = 20;
            this.btncheckOnlyOne.Text = "唯一性检查";
            this.btncheckOnlyOne.UseVisualStyleBackColor = true;
            this.btncheckOnlyOne.Click += new System.EventHandler(this.btncheckOnlyOne_Click);
            // 
            // cbb_类型编号
            // 
            this.cbb_类型编号.Location = new System.Drawing.Point(136, 109);
            this.cbb_类型编号.Name = "cbb_类型编号";
            this.cbb_类型编号.Size = new System.Drawing.Size(141, 19);
            this.cbb_类型编号.TabIndex = 19;
            this.cbb_类型编号.TextChanged += new System.EventHandler(this.cbb_类型编号_TextChanged);
            // 
            // cbb_英文缩写
            // 
            this.cbb_英文缩写.Location = new System.Drawing.Point(136, 67);
            this.cbb_英文缩写.Name = "cbb_英文缩写";
            this.cbb_英文缩写.Size = new System.Drawing.Size(141, 19);
            this.cbb_英文缩写.TabIndex = 18;
            // 
            // cbb_类型名称
            // 
            this.cbb_类型名称.Location = new System.Drawing.Point(136, 27);
            this.cbb_类型名称.Name = "cbb_类型名称";
            this.cbb_类型名称.Size = new System.Drawing.Size(141, 19);
            this.cbb_类型名称.TabIndex = 17;
            this.cbb_类型名称.TextChanged += new System.EventHandler(this.cbb_类型名称_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(28, 112);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 13);
            this.label3.TabIndex = 16;
            this.label3.Text = "编码组：";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(28, 70);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 13);
            this.label2.TabIndex = 15;
            this.label2.Text = "类型描述：";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(28, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(91, 13);
            this.label1.TabIndex = 14;
            this.label1.Text = "物料类型名称：";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.labelx1);
            this.panel1.Controls.Add(this.selfTypeLen);
            this.panel1.Controls.Add(this.button2);
            this.panel1.Controls.Add(this.groupBox2);
            this.panel1.Controls.Add(this.cbTypeLen);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Location = new System.Drawing.Point(15, 145);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(513, 433);
            this.panel1.TabIndex = 9;
            // 
            // labelx1
            // 
            this.labelx1.AutoSize = true;
            this.labelx1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelx1.ForeColor = System.Drawing.Color.DarkRed;
            this.labelx1.Location = new System.Drawing.Point(491, 31);
            this.labelx1.Name = "labelx1";
            this.labelx1.Size = new System.Drawing.Size(15, 13);
            this.labelx1.TabIndex = 5;
            this.labelx1.Text = "X";
            this.labelx1.Visible = false;
            // 
            // selfTypeLen
            // 
            this.selfTypeLen.Location = new System.Drawing.Point(388, 28);
            this.selfTypeLen.Name = "selfTypeLen";
            this.selfTypeLen.ReadOnly = true;
            this.selfTypeLen.Size = new System.Drawing.Size(100, 19);
            this.selfTypeLen.TabIndex = 4;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(259, 26);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(118, 23);
            this.button2.TabIndex = 3;
            this.button2.Text = "自定义段号长度";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.labelx5);
            this.groupBox2.Controls.Add(this.labelx6);
            this.groupBox2.Controls.Add(this.labelx4);
            this.groupBox2.Controls.Add(this.labelx3);
            this.groupBox2.Controls.Add(this.labelx2);
            this.groupBox2.Controls.Add(this.button4);
            this.groupBox2.Controls.Add(this.button3);
            this.groupBox2.Controls.Add(this.materNumCount);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.materialEnd);
            this.groupBox2.Controls.Add(this.materialStart);
            this.groupBox2.Controls.Add(this.materialTypeEnd);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.MaterialTypeStart);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.countTypeNum);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Location = new System.Drawing.Point(37, 55);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(469, 375);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "设置";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(22, 292);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(91, 13);
            this.label5.TabIndex = 22;
            this.label5.Text = "物料类型末位：";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(22, 253);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(91, 13);
            this.label4.TabIndex = 21;
            this.label4.Text = "物料类型始位：";
            // 
            // labelx5
            // 
            this.labelx5.AutoSize = true;
            this.labelx5.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelx5.ForeColor = System.Drawing.Color.DarkRed;
            this.labelx5.Location = new System.Drawing.Point(361, 253);
            this.labelx5.Name = "labelx5";
            this.labelx5.Size = new System.Drawing.Size(15, 13);
            this.labelx5.TabIndex = 20;
            this.labelx5.Text = "X";
            this.labelx5.Visible = false;
            // 
            // labelx6
            // 
            this.labelx6.AutoSize = true;
            this.labelx6.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelx6.ForeColor = System.Drawing.Color.DarkRed;
            this.labelx6.Location = new System.Drawing.Point(361, 292);
            this.labelx6.Name = "labelx6";
            this.labelx6.Size = new System.Drawing.Size(15, 13);
            this.labelx6.TabIndex = 19;
            this.labelx6.Text = "X";
            this.labelx6.Visible = false;
            // 
            // labelx4
            // 
            this.labelx4.AutoSize = true;
            this.labelx4.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelx4.ForeColor = System.Drawing.Color.DarkRed;
            this.labelx4.Location = new System.Drawing.Point(361, 129);
            this.labelx4.Name = "labelx4";
            this.labelx4.Size = new System.Drawing.Size(15, 13);
            this.labelx4.TabIndex = 18;
            this.labelx4.Text = "X";
            this.labelx4.Visible = false;
            // 
            // labelx3
            // 
            this.labelx3.AutoSize = true;
            this.labelx3.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelx3.ForeColor = System.Drawing.Color.DarkRed;
            this.labelx3.Location = new System.Drawing.Point(361, 88);
            this.labelx3.Name = "labelx3";
            this.labelx3.Size = new System.Drawing.Size(15, 13);
            this.labelx3.TabIndex = 17;
            this.labelx3.Text = "X";
            this.labelx3.Visible = false;
            // 
            // labelx2
            // 
            this.labelx2.AutoSize = true;
            this.labelx2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelx2.ForeColor = System.Drawing.Color.DarkRed;
            this.labelx2.Location = new System.Drawing.Point(197, 38);
            this.labelx2.Name = "labelx2";
            this.labelx2.Size = new System.Drawing.Size(15, 13);
            this.labelx2.TabIndex = 16;
            this.labelx2.Text = "X";
            this.labelx2.Visible = false;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(235, 201);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(118, 23);
            this.button4.TabIndex = 14;
            this.button4.Text = "自定义范围";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(235, 33);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(118, 23);
            this.button3.TabIndex = 13;
            this.button3.Text = "自定义范围";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // materNumCount
            // 
            this.materNumCount.Location = new System.Drawing.Point(133, 203);
            this.materNumCount.Name = "materNumCount";
            this.materNumCount.ReadOnly = true;
            this.materNumCount.Size = new System.Drawing.Size(58, 19);
            this.materNumCount.TabIndex = 11;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(24, 206);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(79, 13);
            this.label7.TabIndex = 10;
            this.label7.Text = "外部占位数：";
            // 
            // materialEnd
            // 
            this.materialEnd.Location = new System.Drawing.Point(133, 289);
            this.materialEnd.Name = "materialEnd";
            this.materialEnd.ReadOnly = true;
            this.materialEnd.Size = new System.Drawing.Size(220, 19);
            this.materialEnd.TabIndex = 9;
            this.materialEnd.TextChanged += new System.EventHandler(this.materialEnd_TextChanged);
            // 
            // materialStart
            // 
            this.materialStart.Location = new System.Drawing.Point(133, 250);
            this.materialStart.Name = "materialStart";
            this.materialStart.ReadOnly = true;
            this.materialStart.Size = new System.Drawing.Size(220, 19);
            this.materialStart.TabIndex = 7;
            this.materialStart.TextChanged += new System.EventHandler(this.materialStart_TextChanged);
            // 
            // materialTypeEnd
            // 
            this.materialTypeEnd.Location = new System.Drawing.Point(133, 127);
            this.materialTypeEnd.Name = "materialTypeEnd";
            this.materialTypeEnd.ReadOnly = true;
            this.materialTypeEnd.Size = new System.Drawing.Size(220, 19);
            this.materialTypeEnd.TabIndex = 5;
            this.materialTypeEnd.TextChanged += new System.EventHandler(this.materialTypeEnd_TextChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(24, 130);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(91, 13);
            this.label8.TabIndex = 4;
            this.label8.Text = "物料类型末位：";
            // 
            // MaterialTypeStart
            // 
            this.MaterialTypeStart.Location = new System.Drawing.Point(133, 85);
            this.MaterialTypeStart.Name = "MaterialTypeStart";
            this.MaterialTypeStart.ReadOnly = true;
            this.MaterialTypeStart.Size = new System.Drawing.Size(220, 19);
            this.MaterialTypeStart.TabIndex = 3;
            this.MaterialTypeStart.TextChanged += new System.EventHandler(this.MaterialTypeStart_TextChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(24, 88);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(91, 13);
            this.label9.TabIndex = 2;
            this.label9.Text = "物料类型始位：";
            // 
            // countTypeNum
            // 
            this.countTypeNum.Location = new System.Drawing.Point(133, 35);
            this.countTypeNum.Name = "countTypeNum";
            this.countTypeNum.Size = new System.Drawing.Size(58, 19);
            this.countTypeNum.TabIndex = 1;
            this.countTypeNum.TextChanged += new System.EventHandler(this.countTypeNum_TextChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(24, 38);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(79, 13);
            this.label10.TabIndex = 0;
            this.label10.Text = "内部占位数：";
            // 
            // cbTypeLen
            // 
            this.cbTypeLen.FormattingEnabled = true;
            this.cbTypeLen.Items.AddRange(new object[] {
            "10",
            "15",
            "20",
            "25",
            "30"});
            this.cbTypeLen.Location = new System.Drawing.Point(118, 28);
            this.cbTypeLen.Name = "cbTypeLen";
            this.cbTypeLen.Size = new System.Drawing.Size(87, 21);
            this.cbTypeLen.TabIndex = 1;
            this.cbTypeLen.Text = "20";
            this.cbTypeLen.SelectedIndexChanged += new System.EventHandler(this.cbTypeLen_SelectedIndexChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(45, 31);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(67, 13);
            this.label11.TabIndex = 0;
            this.label11.Text = "段号长度：";
            // 
            // btn_确定
            // 
            this.btn_确定.Location = new System.Drawing.Point(240, 584);
            this.btn_确定.Name = "btn_确定";
            this.btn_确定.Size = new System.Drawing.Size(73, 23);
            this.btn_确定.TabIndex = 6;
            this.btn_确定.Text = "保存";
            this.btn_确定.UseVisualStyleBackColor = true;
            this.btn_确定.Click += new System.EventHandler(this.btn_确定_Click);
            // 
            // cbRowMaterial
            // 
            this.cbRowMaterial.AutoSize = true;
            this.cbRowMaterial.Checked = true;
            this.cbRowMaterial.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbRowMaterial.Location = new System.Drawing.Point(331, 32);
            this.cbRowMaterial.Name = "cbRowMaterial";
            this.cbRowMaterial.Size = new System.Drawing.Size(80, 17);
            this.cbRowMaterial.TabIndex = 21;
            this.cbRowMaterial.Text = "      原材料";
            this.cbRowMaterial.UseVisualStyleBackColor = true;
            // 
            // NewType
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(592, 655);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "NewType";
            this.Text = "新建物料类型";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btn_确定;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label labelx1;
        private System.Windows.Forms.TextBox selfTypeLen;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label labelx5;
        private System.Windows.Forms.Label labelx6;
        private System.Windows.Forms.Label labelx4;
        private System.Windows.Forms.Label labelx3;
        private System.Windows.Forms.Label labelx2;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TextBox materNumCount;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox materialEnd;
        private System.Windows.Forms.TextBox materialStart;
        private System.Windows.Forms.TextBox materialTypeEnd;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox MaterialTypeStart;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox countTypeNum;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox cbTypeLen;
        internal System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button btncheckOnlyOne;
        private System.Windows.Forms.TextBox cbb_类型编号;
        private System.Windows.Forms.TextBox cbb_英文缩写;
        private System.Windows.Forms.TextBox cbb_类型名称;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox cbRowMaterial;
    }
}