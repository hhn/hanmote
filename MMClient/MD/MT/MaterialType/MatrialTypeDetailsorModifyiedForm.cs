﻿using Lib.Common.MMCException.IDAL;
using Lib.SqlServerDAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MMClient.MD.MT.MaterialType
{
    public partial class MatrialTypeDetailsorModifyiedForm : Form
    {
        int flag = 0;
        int length = 0;
        int typeLen = 0;
        int flagbtn3 = 0;
        int flagbtn4 = 0;
        int blockChange = 0;
        int onlyone = 0;
        string MaterialTypeID = ""; 
        public MatrialTypeDetailsorModifyiedForm(string MaterialTypeID)
        {
            this.MaterialTypeID = MaterialTypeID;
            InitializeComponent();
        }
        
        /// <summary>
        /// 当改变时 数据占位全部改变
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void countTypeNum_TextChanged(object sender, EventArgs e)
        {
            this.labelx2.Hide();
            this.labelx3.Hide();
            this.labelx4.Hide();
            this.labelx5.Hide();
            this.labelx6.Hide();
            if (string.IsNullOrEmpty(this.countTypeNum.Text.ToString().Trim()))
            {

                this.labelx2.Show();
                this.labelx3.Show();
                this.labelx4.Show();
                this.labelx5.Show();
                this.labelx6.Show();
                return;
            }
            if(blockChange==0)
            {
                return;
            }
                this.labelx2.Hide();
                length = Convert.ToInt32(this.selfTypeLen.Text.ToString());
                try
                {
                    typeLen = Convert.ToInt32(this.countTypeNum.Text.ToString().Trim());
                }
                catch (Exception ex)
                {
                    this.labelx2.Show();
                    this.MaterialTypeStart.Text = "";
                    this.materialTypeEnd.Text = "";
                    this.materNumCount.Text = "";
                    this.materialStart.Text = "";
                    this.materialEnd.Text = "";
                    return;
                }
                if (typeLen == 0)
                {
                    this.labelx2.Show();
                    this.MaterialTypeStart.Text = "";
                    this.materialTypeEnd.Text = "";
                    this.materNumCount.Text = "";
                    this.materialStart.Text = "";
                    this.materialEnd.Text = "";
                    return;
                }
                if (typeLen > length)
                {
                    this.labelx2.Show();
                    this.MaterialTypeStart.Text = "";
                    this.materialTypeEnd.Text = "";
                    this.materNumCount.Text = "";
                    this.materialStart.Text = "";
                    this.materialEnd.Text = "";
                    return;
                }
                //默认长度设置
                if (flag == 0)
                {
                    this.materNumCount.Text = length - typeLen + "";
                    fillTextBox();
                }
                //自定义长度调整
                else
                {
                    length = Convert.ToInt32(this.selfTypeLen.Text.ToString());
                    this.materNumCount.Text = length - typeLen + "";
                    fillTextBox();
                }
            if (this.materNumCount.Text.ToString() == "0" || "0".Equals(this.materNumCount.Text.ToString()))
            {
                this.labelx5.Hide();
                this.labelx6.Hide();
            }
        }
        /// <summary>
        /// 默认分段填充
        /// </summary>
        /// <param name="length"></param>
        /// <param name="typeLen"></param>
        private void fillTextBox()
        {
            //根据长度设置直接读出物料类型始未和末尾
            StringBuilder strTypeNumStart = new StringBuilder();
            StringBuilder strTypeNumEnd = new StringBuilder();
            for (int i = 0; i < typeLen; i++)
            {
                strTypeNumStart.Append("0");
                strTypeNumEnd.Append("9");
            }
            this.MaterialTypeStart.Text = strTypeNumStart.ToString();
            this.materialTypeEnd.Text = strTypeNumEnd.ToString();
            strTypeNumStart = null;
            strTypeNumEnd = null;
            //根据长度设置直接读出物料始未和末尾

            StringBuilder strNumStart = new StringBuilder();
            StringBuilder strNumEnd = new StringBuilder();
            int count = length - typeLen;
            for (int i = 0; i < count; i++)
            {
                strNumStart.Append("0");
                strNumEnd.Append("9");
            }
            this.materialStart.Text = strNumStart.ToString();
            this.materialEnd.Text = strNumEnd.ToString();
            strNumStart = null;
            strNumEnd = null;
        }
        
        /// <summary>
        /// 自定义物料类型编号shimo
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button3_Click(object sender, EventArgs e)
        {
            this.labelx3.Show();
            this.labelx4.Show();
            if (flagbtn3 == 0)
            {
                this.MaterialTypeStart.ReadOnly = false;
                this.materialTypeEnd.ReadOnly = false;
                this.MaterialTypeStart.Text = "";
                this.materialTypeEnd.Text = "";
                this.button3.Text = "取消自定义设置";
                flagbtn3 = 1;
            }
            else
            {
                this.MaterialTypeStart.ReadOnly = true;
                this.materialTypeEnd.ReadOnly = true;
                this.MaterialTypeStart.Text = "";
                this.materialTypeEnd.Text = "";
                this.button3.Text = "自定义范围";
                flagbtn3 = 0;
            }

        }

        /// <summary>
        /// 自定义物料编号始末
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button4_Click(object sender, EventArgs e)
        {
            this.labelx5.Show();
            this.labelx6.Show();
            if (flagbtn4 == 0)
            {
                this.materialStart.ReadOnly = false;
                this.materialEnd.ReadOnly = false;
                this.materialStart.Text = "";
                this.materialEnd.Text = "";
                this.button4.Text = "取消自定义范围";
                flagbtn4 = 1;
            }
            else
            {
                this.materialStart.ReadOnly = true;
                this.materialEnd.ReadOnly = true;
                this.materialStart.Text = "";
                this.materialEnd.Text = "";
                this.button4.Text = "自定义范围";
                flagbtn4 = 0;
            }
        }
        /// <summary>
        /// 字段保存
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSaveTypeBtn_Click(object sender, EventArgs e)
        {

        }
        /// <summary>
        /// 类型段号自定义检查
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCheckRange_Click(object sender, EventArgs e)
        {
            //检查设定范围是否已被占用
        }
        /// <summary>
        /// 物料段号自定义检查
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button5_Click(object sender, EventArgs e)
        {
            //检查设定范围是否已被占用
        }
       

        private void MaterialTypeStart_TextChanged(object sender, EventArgs e)
        {
            this.labelx3.Hide();
            if (this.MaterialTypeStart.Text.ToString().Trim().Length > typeLen)
            {
                this.labelx3.Show();
                return;
            }
            this.labelx3.Hide();
            try
            {
                for (int i = 0; i < typeLen; i++)
                    Convert.ToInt32(this.MaterialTypeStart.Text.ToString().Trim()[i]);
            }
            catch (Exception ex)
            {
                this.labelx3.Show();
                return;
            }
            if (typeLen != this.MaterialTypeStart.Text.ToString().Trim().Length)
            {
                this.labelx3.Show();
            }
        }

        private void materialTypeEnd_TextChanged(object sender, EventArgs e)
        {
            this.labelx4.Hide();
            if (this.materialTypeEnd.Text.ToString().Trim().Length > typeLen)
            {
                this.labelx4.Show();
                return;
            }
            this.labelx4.Hide();
            try
            {
                for (int i = 0; i < typeLen; i++)
                    Convert.ToInt32(this.materialTypeEnd.Text.ToString().Trim()[i]);
            }
            catch (Exception ex)
            {
                this.labelx4.Show();
                return;
            }
            if (typeLen != this.materialTypeEnd.Text.ToString().Trim().Length)
            {
                this.labelx4.Show();
            }
        }

        private void materialStart_TextChanged(object sender, EventArgs e)
        {
            this.labelx5.Hide();
            if (this.materialStart.Text.ToString().Trim().Length > (length - typeLen))
            {
                this.labelx5.Show();
                return;
            }
            this.labelx5.Hide();
            try
            {
                for (int i = 0; i < (length - typeLen); i++)
                    Convert.ToInt32(this.materialStart.Text.ToString().Trim()[i]);
            }
            catch (Exception ex)
            {
                this.labelx5.Show();
                return;
            }
            if ((length - typeLen) != this.materialStart.Text.ToString().Trim().Length)
            {
                this.labelx5.Show();
            }
        }

        private void materialEnd_TextChanged(object sender, EventArgs e)
        {
            this.labelx6.Hide();
            if (this.materialEnd.Text.ToString().Trim().Length > (length - typeLen))
            {
                this.labelx6.Show();
                return;
            }
            this.labelx6.Hide();
            try
            {
                for (int i = 0; i < (length - typeLen); i++)
                    Convert.ToInt32(this.materialEnd.Text.ToString().Trim()[i]);
            }
            catch (Exception ex)
            {
                this.labelx6.Show();
                return;
            }
            if ((length - typeLen) != this.materialEnd.Text.ToString().Trim().Length)
            {
                this.labelx6.Show();
            }
        }
        /// <summary>
        /// 加载数据
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MatrialTypeDetailsorModifyiedForm_Load(object sender, EventArgs e)
        {
            initialData();
            this.labelx1.Hide();
            this.labelx2.Hide();
            this.labelx3.Hide();
            this.labelx4.Hide();
            this.labelx5.Hide();
            this.labelx6.Hide();
            length = Convert.ToInt32(this.selfTypeLen.Text.ToString());
            typeLen = Convert.ToInt32(this.countTypeNum.Text.ToString());
            blockChange++;
        }
        /// <summary>
        /// 填充数据
        /// </summary>
        private void initialData()
        {
            if (MaterialTypeID == "") {
                MessageBox.Show("初始化失败");
                return;
            }
            string sql = "select * from  Bigclassfy where ClassfyID = '"+MaterialTypeID+"'";
            try
            {
                DataTable dataTable = DBHelper.ExecuteQueryDT(sql);
                setNUllValue();
                if (dataTable != null && dataTable.Rows.Count > 0)
                {
                    if (dataTable.Rows[0][0] != null) this.cbb_类型名称.Text = dataTable.Rows[0][0].ToString(); 
                    if (dataTable.Rows[0][1] != null) this.cbb_类型编号.Text = dataTable.Rows[0][1].ToString();
                    if (dataTable.Rows[0][2] != null) this.cbb_英文缩写.Text = dataTable.Rows[0][2].ToString();
                    if (dataTable.Rows[0][3] != null) this.MaterialTypeStart.Text = dataTable.Rows[0][3].ToString();
                    if (dataTable.Rows[0][4] != null) this.materialTypeEnd.Text = dataTable.Rows[0][4].ToString();
                    if (dataTable.Rows[0][5] != null) this.materialStart.Text = dataTable.Rows[0][5].ToString();
                    if (dataTable.Rows[0][6] != null) this.materialEnd.Text = dataTable.Rows[0][6].ToString();
                    if (dataTable.Rows[0][7] != null) this.selfTypeLen.Text = dataTable.Rows[0][7].ToString();
                    if (dataTable.Rows[0][8] != null) this.countTypeNum.Text = dataTable.Rows[0][8].ToString();
                    if (dataTable.Rows[0][9] != null) this.materNumCount.Text = dataTable.Rows[0][9].ToString();
                }
                blockChange++;
            }
            catch (DBException ex)
            {
                MessageBox.Show("数据查询失败");
            }
        }
        /// <summary>
        /// 输入实时检查
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void selfTypeLen_TextChanged(object sender, EventArgs e)
        {
            if (blockChange == 0)
                return;
            this.labelx1.Hide();
            try
            {
                length = Convert.ToInt32(this.selfTypeLen.Text.ToString().Trim());
                setNUllValue();
            }
            catch (Exception ex)
            {
                this.labelx1.Show();
                return;
            }
        }
        /// <summary>
        /// 置空
        /// </summary>
        private void setNUllValue()
        {
            this.MaterialTypeStart.Text = "";
            this.materialTypeEnd.Text = "";
            this.materNumCount.Text = "";
            this.materialStart.Text = "";
            this.materialEnd.Text = "";
            this.countTypeNum.Text = "";


        }

        private void btncheckOnlyOne_Click(object sender, EventArgs e)
        {
            if (this.cbb_类型编号.Text.ToString().Trim() == "" || this.cbb_类型编号.Text.ToString().Trim().Equals(""))
            {
                MessageBox.Show("类型编码不能为空");
                return;
            }
            if (this.cbb_类型名称.Text.ToString().Trim() == "" || this.cbb_类型名称.Text.ToString().Trim().Equals(""))
            {
                MessageBox.Show("类型名称不能为空");
                return;
            }
            try
            {
                string sql = "select ClassfyID from Bigclassfy where ClassfyID != '"+ MaterialTypeID + "'";
                DataTable dt = DBHelper.ExecuteQueryDT(sql);
                bool ID = checkOnlyOne(dt, this.cbb_类型编号.Text.ToString().Trim());
                sql = "";
                dt = null;
                if (ID == false)
                {
                    MessageBox.Show("类型编码不唯一");
                    this.cbb_类型编号.Text = "";
                    return;
                }
                sql = "select Bigclassfy_Name from Bigclassfy where ClassfyID != '" + MaterialTypeID + "'";
                dt = DBHelper.ExecuteQueryDT(sql);
                bool name = checkOnlyOne(dt, this.cbb_类型名称.Text.ToString().Trim());
                if (name == false)
                {
                    MessageBox.Show("类型名称不唯一");
                    this.cbb_类型名称.Text = "";
                    return;
                }
                sql = "";
                dt = null;
            }
            catch (DBException ex)
            {
                MessageBox.Show("数据库查询失败");
                return;
            }
            MessageBox.Show("检查无误");
            onlyone = 1;
        }
        /// <summary>
        /// 唯一性检查
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="str"></param>
        private bool checkOnlyOne(DataTable dt, string str)
        {
            if (dt.Rows.Count == 0)
            {
                return true;
            }
            else
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (str == dt.Rows[i][0].ToString().Trim() || str.Equals(dt.Rows[i][0].ToString().Trim()))
                    {
                        return false;
                    }
                }
            }
            return true;
        }
        private void btn_确定_Click(object sender, EventArgs e)
        {
            if (cbb_类型编号.Text.ToString() == "" || cbb_类型名称.Text.ToString() == "" || cbb_英文缩写.Text.ToString() == ""
               || this.MaterialTypeStart.Text.ToString() == "" || this.materialTypeEnd.Text.ToString() == "")
            {
                MessageBox.Show("数据填写不完整", "注意");
                return;
            }
            if (this.materNumCount.Text.ToString()!="0" && !string.IsNullOrEmpty(this.materNumCount.Text.ToString()))
            {
                if (this.materialStart.Text.ToString() == "" || this.materialEnd.Text.ToString() == "")
                {
                    MessageBox.Show("外部给号填写不完整", "注意");
                    return;
                }
            }
            if (onlyone==0)
            {
                MessageBox.Show("未检查数据唯一性", "注意");
                return;
            }
           
            string tpid = cbb_类型编号.Text;
            string tpname = cbb_类型名称.Text;
            string tpeng = cbb_英文缩写.Text;
            string rangeStart = this.MaterialTypeStart.Text.ToString();
            string rangeEnd = this.materialTypeEnd.Text.ToString();
            string MaterialRangeStart = this.materialStart.Text.ToString();
            string MaterialRangeEnd = this.materialEnd.Text.ToString();
            string codeLength = this.selfTypeLen.Text.ToString();
            string typeLength = this.countTypeNum.Text.ToString();
            string materialLength = this.materNumCount.Text.ToString();
            string outSupport = "1";
            if(tpid.Length!=5)
            {
                MessageBox.Show("类型编码长度固定为5", "注意");
                return;
            }
            if (materialLength == "0" || string.IsNullOrEmpty(materialLength))
            {
                outSupport = "0";
            }
            bool rationalTypeRange = isRationalRange(rangeStart, rangeEnd);
            if(rationalTypeRange == false)
            {
                MessageBox.Show("类型范围设置不合理", "注意");
                return;
            }
            bool rationalOutterTypeRange = isRationalRange(MaterialRangeStart, MaterialRangeEnd);
            if (rationalTypeRange == false)
            {
                MessageBox.Show("类型范围设置不合理", "注意");
                return;
            }
            try
            {
                if (Convert.ToInt32(codeLength) != (Convert.ToInt32(typeLength) + Convert.ToInt32(materialLength)))
                {
                    MessageBox.Show("类型长度不匹配");
                    return;
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show("类型长度为整数");
                return;
            }
            if(this.MaterialTypeStart.Text.ToString().Trim().Length!= typeLen || this.materialTypeEnd.Text.ToString().Trim().Length != typeLen)
            {
                MessageBox.Show("类型长度不匹配");
                return;
            }
            if (this.materialStart.Text.ToString().Trim().Length != (length - typeLen) || this.materialEnd.Text.ToString().Trim().Length != (length - typeLen))
            {
                MessageBox.Show("类型长度不匹配");
                return;
            }
            try
            {
                string sql = "UPDATE [Bigclassfy] SET ClassfyID = '" + tpid + "', outSupport = '" + outSupport + "',codeLength = '" + codeLength + "', innnerLength='" + typeLength + "',outterLength='" + materialLength + "',Bigclassfy_Name = '" + tpname + "', description = '" + tpeng + "', innnerStart = '" + rangeStart + "', innnerEnd = '" + rangeEnd + "', outterRangeStart= '" + MaterialRangeStart + "',outterRangeEnd = '" + MaterialRangeEnd + "' where ClassfyID = '" + this.MaterialTypeID + "'";
                DBHelper.ExecuteNonQuery(sql);
                MessageBox.Show("修改成功");
                this.Close();
            }
            catch (DBException ex)
            {
                MessageBox.Show("修改失败");
            }
        }

        private bool isRationalRange(string rangeStart, string rangeEnd)
        {
            if (string.IsNullOrEmpty(rangeStart) || string.IsNullOrEmpty(rangeEnd)) return false;
            if (rangeStart.Equals(rangeEnd)) return false;
            if (rangeEnd.Length != rangeStart.Length) return false;
            for (int i = 0; i < rangeStart.Length; i++)
            {
                try
                {
                    int a = Convert.ToInt32(rangeStart[i]);
                    int b = Convert.ToInt32(rangeEnd[i]);
                    if (a < b)
                    {
                        return true;
                    }
                    else if (a == b)
                    {
                        //doing nothing
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
            return false;
        }

        private void cbb_类型名称_TextChanged(object sender, EventArgs e)
        {
            onlyone = 0;
        }

        private void cbb_类型编号_TextChanged(object sender, EventArgs e)
        {
            onlyone = 0;
        }
    }
}
