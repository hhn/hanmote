﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Bll.MDBll.General.Material_Type;

namespace MMClient.MD.MT.MaterialType
{
    public partial class MTtypeB : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        public MTtypeB()
        {
            InitializeComponent();
        }

        private void MTtypeB_Load(object sender, EventArgs e)
        {
            MTTypeBBLL mtb = new MTTypeBBLL();
            dgv_物料类型.DataSource = mtb.GetAllLittleClassfy();

        }

        private void btn_刷新数据_Click(object sender, EventArgs e)
        {
            MTTypeBBLL mtb = new MTTypeBBLL();
            dgv_物料类型.DataSource = mtb.GetAllLittleClassfy();
        }

        private void btn_新条目_Click(object sender, EventArgs e)
        {
            NewTypeB ntb = new NewTypeB();
            ntb.Show();
        }

    }
}
