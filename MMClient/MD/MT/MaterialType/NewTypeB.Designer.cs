﻿namespace MMClient.MD.MT.MaterialType
{
    partial class NewTypeB
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btn_重置 = new System.Windows.Forms.Button();
            this.btn_确定 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.tbx_编号 = new System.Windows.Forms.TextBox();
            this.tbx_小分类 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cbb_大分类 = new System.Windows.Forms.ComboBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // supplierInforGroupBox
            // 
            this.groupBox1.Controls.Add(this.cbb_大分类);
            this.groupBox1.Controls.Add(this.btn_重置);
            this.groupBox1.Controls.Add(this.btn_确定);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.tbx_编号);
            this.groupBox1.Controls.Add(this.tbx_小分类);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(3, 5);
            this.groupBox1.Name = "supplierInforGroupBox";
            this.groupBox1.Size = new System.Drawing.Size(875, 332);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "新建分类";
            // 
            // btn_重置
            // 
            this.btn_重置.Location = new System.Drawing.Point(577, 256);
            this.btn_重置.Name = "btn_重置";
            this.btn_重置.Size = new System.Drawing.Size(75, 42);
            this.btn_重置.TabIndex = 11;
            this.btn_重置.Text = "重置";
            this.btn_重置.UseVisualStyleBackColor = true;
            // 
            // btn_确定
            // 
            this.btn_确定.Location = new System.Drawing.Point(473, 256);
            this.btn_确定.Name = "btn_确定";
            this.btn_确定.Size = new System.Drawing.Size(75, 42);
            this.btn_确定.TabIndex = 10;
            this.btn_确定.Text = "确定";
            this.btn_确定.UseVisualStyleBackColor = true;
            this.btn_确定.Click += new System.EventHandler(this.btn_确定_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(306, 135);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(120, 17);
            this.label4.TabIndex = 9;
            this.label4.Text = "（输入三位数字）";
            // 
            // tbx_编号
            // 
            this.tbx_编号.Location = new System.Drawing.Point(200, 132);
            this.tbx_编号.Name = "tbx_编号";
            this.tbx_编号.Size = new System.Drawing.Size(100, 22);
            this.tbx_编号.TabIndex = 4;
            // 
            // tbx_小分类
            // 
            this.tbx_小分类.Location = new System.Drawing.Point(200, 71);
            this.tbx_小分类.Name = "tbx_小分类";
            this.tbx_小分类.Size = new System.Drawing.Size(100, 22);
            this.tbx_小分类.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(50, 191);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(106, 17);
            this.label3.TabIndex = 2;
            this.label3.Text = "所属大分类名称";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(50, 135);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(78, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "小分类编号";
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(50, 71);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(78, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "小分类名称";
            // 
            // cbb_大分类
            // 
            this.cbb_大分类.FormattingEnabled = true;
            this.cbb_大分类.Location = new System.Drawing.Point(200, 191);
            this.cbb_大分类.Name = "cbb_大分类";
            this.cbb_大分类.Size = new System.Drawing.Size(100, 24);
            this.cbb_大分类.TabIndex = 12;
            // 
            // NewTypeB
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(884, 344);
            this.Controls.Add(this.groupBox1);
            this.Name = "NewTypeB";
            this.Text = "新建物料小分类";
            this.Load += new System.EventHandler(this.NewTypeB_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbx_编号;
        private System.Windows.Forms.TextBox tbx_小分类;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btn_确定;
        private System.Windows.Forms.Button btn_重置;
        private System.Windows.Forms.ComboBox cbb_大分类;
    }
}