﻿namespace MMClient.MD.MT
{
    partial class ModifyMaterialGroupInfoForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label3 = new System.Windows.Forms.Label();
            this.CB_mGroupLevel = new System.Windows.Forms.ComboBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.tbx_物料组描述 = new System.Windows.Forms.TextBox();
            this.tbx_物料组编号 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btn_确定 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(453, 182);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 12);
            this.label3.TabIndex = 78;
            this.label3.Text = "物料组等级";
            // 
            // CB_mGroupLevel
            // 
            this.CB_mGroupLevel.FormattingEnabled = true;
            this.CB_mGroupLevel.Items.AddRange(new object[] {
            "A",
            "B",
            "C",
            "D"});
            this.CB_mGroupLevel.Location = new System.Drawing.Point(533, 177);
            this.CB_mGroupLevel.Name = "CB_mGroupLevel";
            this.CB_mGroupLevel.Size = new System.Drawing.Size(121, 20);
            this.CB_mGroupLevel.TabIndex = 77;
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(168, 176);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(147, 20);
            this.comboBox1.TabIndex = 76;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(106, 180);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 12);
            this.label7.TabIndex = 75;
            this.label7.Text = "所属类型";
            // 
            // tbx_物料组描述
            // 
            this.tbx_物料组描述.Location = new System.Drawing.Point(537, 61);
            this.tbx_物料组描述.Margin = new System.Windows.Forms.Padding(2);
            this.tbx_物料组描述.Name = "tbx_物料组描述";
            this.tbx_物料组描述.Size = new System.Drawing.Size(117, 21);
            this.tbx_物料组描述.TabIndex = 74;
            // 
            // tbx_物料组编号
            // 
            this.tbx_物料组编号.Location = new System.Drawing.Point(168, 60);
            this.tbx_物料组编号.Margin = new System.Windows.Forms.Padding(2);
            this.tbx_物料组编号.Name = "tbx_物料组编号";
            this.tbx_物料组编号.ReadOnly = true;
            this.tbx_物料组编号.Size = new System.Drawing.Size(147, 21);
            this.tbx_物料组编号.TabIndex = 73;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(453, 63);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 12);
            this.label2.TabIndex = 72;
            this.label2.Text = "物料组描述";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(95, 64);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 12);
            this.label1.TabIndex = 71;
            this.label1.Text = "物料组编号";
            // 
            // btn_确定
            // 
            this.btn_确定.Location = new System.Drawing.Point(347, 315);
            this.btn_确定.Margin = new System.Windows.Forms.Padding(2);
            this.btn_确定.Name = "btn_确定";
            this.btn_确定.Size = new System.Drawing.Size(59, 27);
            this.btn_确定.TabIndex = 79;
            this.btn_确定.Text = "确定";
            this.btn_确定.UseVisualStyleBackColor = true;
            this.btn_确定.Click += new System.EventHandler(this.btn_确定_Click);
            // 
            // ModifyMaterialGroupInfoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btn_确定);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.CB_mGroupLevel);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.tbx_物料组描述);
            this.Controls.Add(this.tbx_物料组编号);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "ModifyMaterialGroupInfoForm";
            this.Text = "修改物料组信息";
            this.Load += new System.EventHandler(this.ModifyMaterialGroupInfoForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox CB_mGroupLevel;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tbx_物料组描述;
        private System.Windows.Forms.TextBox tbx_物料组编号;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btn_确定;
    }
}