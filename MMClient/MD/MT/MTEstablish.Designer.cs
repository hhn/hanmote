﻿namespace MMClient.MD
{
    partial class MTEstablish
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.cbb_批次管理 = new System.Windows.Forms.ComboBox();
            this.cbb_物料名称 = new System.Windows.Forms.ComboBox();
            this.cbb_采购组 = new System.Windows.Forms.ComboBox();
            this.cbb_物料类型 = new System.Windows.Forms.ComboBox();
            this.cbb_物料组 = new System.Windows.Forms.ComboBox();
            this.cbb_计量单位 = new System.Windows.Forms.ComboBox();
            this.cbb_存货仓库 = new System.Windows.Forms.ComboBox();
            this.cbb_工业标识 = new System.Windows.Forms.ComboBox();
            this.cbb_物料编码 = new System.Windows.Forms.ComboBox();
            this.btn_创建 = new System.Windows.Forms.Button();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.btn_重置 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.cbb_材料标准 = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.cbb_产品特殊要求 = new System.Windows.Forms.ComboBox();
            this.cbb_材料规格 = new System.Windows.Forms.ComboBox();
            this.cbb_用途 = new System.Windows.Forms.ComboBox();
            this.cbb_供应商电话 = new System.Windows.Forms.ComboBox();
            this.cbb_数据是否修改 = new System.Windows.Forms.ComboBox();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.cbb_数据是否修改);
            this.groupBox3.Controls.Add(this.cbb_供应商电话);
            this.groupBox3.Controls.Add(this.cbb_用途);
            this.groupBox3.Controls.Add(this.cbb_材料规格);
            this.groupBox3.Controls.Add(this.cbb_产品特殊要求);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.cbb_材料标准);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.cbb_批次管理);
            this.groupBox3.Controls.Add(this.cbb_物料名称);
            this.groupBox3.Controls.Add(this.cbb_采购组);
            this.groupBox3.Controls.Add(this.cbb_物料类型);
            this.groupBox3.Controls.Add(this.cbb_物料组);
            this.groupBox3.Controls.Add(this.cbb_计量单位);
            this.groupBox3.Controls.Add(this.cbb_存货仓库);
            this.groupBox3.Controls.Add(this.cbb_工业标识);
            this.groupBox3.Controls.Add(this.cbb_物料编码);
            this.groupBox3.Controls.Add(this.btn_创建);
            this.groupBox3.Controls.Add(this.label19);
            this.groupBox3.Controls.Add(this.label18);
            this.groupBox3.Controls.Add(this.label17);
            this.groupBox3.Controls.Add(this.label16);
            this.groupBox3.Controls.Add(this.label15);
            this.groupBox3.Controls.Add(this.label14);
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Location = new System.Drawing.Point(12, 12);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(944, 652);
            this.groupBox3.TabIndex = 1;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "创建新数据";
            // 
            // cbb_批次管理
            // 
            this.cbb_批次管理.FormattingEnabled = true;
            this.cbb_批次管理.Location = new System.Drawing.Point(489, 218);
            this.cbb_批次管理.Name = "cbb_批次管理";
            this.cbb_批次管理.Size = new System.Drawing.Size(137, 23);
            this.cbb_批次管理.TabIndex = 29;
            // 
            // cbb_物料名称
            // 
            this.cbb_物料名称.FormattingEnabled = true;
            this.cbb_物料名称.Location = new System.Drawing.Point(489, 153);
            this.cbb_物料名称.Name = "cbb_物料名称";
            this.cbb_物料名称.Size = new System.Drawing.Size(137, 23);
            this.cbb_物料名称.TabIndex = 28;
            // 
            // cbb_采购组
            // 
            this.cbb_采购组.FormattingEnabled = true;
            this.cbb_采购组.Location = new System.Drawing.Point(489, 87);
            this.cbb_采购组.Name = "cbb_采购组";
            this.cbb_采购组.Size = new System.Drawing.Size(137, 23);
            this.cbb_采购组.TabIndex = 27;
            // 
            // cbb_物料类型
            // 
            this.cbb_物料类型.FormattingEnabled = true;
            this.cbb_物料类型.Location = new System.Drawing.Point(489, 23);
            this.cbb_物料类型.Name = "cbb_物料类型";
            this.cbb_物料类型.Size = new System.Drawing.Size(137, 23);
            this.cbb_物料类型.TabIndex = 26;
            // 
            // cbb_物料组
            // 
            this.cbb_物料组.FormattingEnabled = true;
            this.cbb_物料组.Location = new System.Drawing.Point(128, 285);
            this.cbb_物料组.Name = "cbb_物料组";
            this.cbb_物料组.Size = new System.Drawing.Size(137, 23);
            this.cbb_物料组.TabIndex = 25;
            // 
            // cbb_计量单位
            // 
            this.cbb_计量单位.FormattingEnabled = true;
            this.cbb_计量单位.Location = new System.Drawing.Point(128, 215);
            this.cbb_计量单位.Name = "cbb_计量单位";
            this.cbb_计量单位.Size = new System.Drawing.Size(137, 23);
            this.cbb_计量单位.TabIndex = 24;
            // 
            // cbb_存货仓库
            // 
            this.cbb_存货仓库.FormattingEnabled = true;
            this.cbb_存货仓库.Location = new System.Drawing.Point(128, 144);
            this.cbb_存货仓库.Name = "cbb_存货仓库";
            this.cbb_存货仓库.Size = new System.Drawing.Size(137, 23);
            this.cbb_存货仓库.TabIndex = 23;
            // 
            // cbb_工业标识
            // 
            this.cbb_工业标识.FormattingEnabled = true;
            this.cbb_工业标识.Location = new System.Drawing.Point(128, 87);
            this.cbb_工业标识.Name = "cbb_工业标识";
            this.cbb_工业标识.Size = new System.Drawing.Size(137, 23);
            this.cbb_工业标识.TabIndex = 22;
            // 
            // cbb_物料编码
            // 
            this.cbb_物料编码.FormattingEnabled = true;
            this.cbb_物料编码.Location = new System.Drawing.Point(128, 23);
            this.cbb_物料编码.Name = "cbb_物料编码";
            this.cbb_物料编码.Size = new System.Drawing.Size(137, 23);
            this.cbb_物料编码.TabIndex = 21;
            // 
            // btn_创建
            // 
            this.btn_创建.Location = new System.Drawing.Point(618, 402);
            this.btn_创建.Name = "btn_创建";
            this.btn_创建.Size = new System.Drawing.Size(75, 23);
            this.btn_创建.TabIndex = 20;
            this.btn_创建.Text = "创建";
            this.btn_创建.UseVisualStyleBackColor = true;
            this.btn_创建.Click += new System.EventHandler(this.btn_创建_Click);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(7, 288);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(52, 15);
            this.label19.TabIndex = 16;
            this.label19.Text = "物料组";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(338, 218);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(67, 15);
            this.label18.TabIndex = 14;
            this.label18.Text = "批次管理";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(7, 218);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(67, 15);
            this.label17.TabIndex = 12;
            this.label17.Text = "计量单位";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(338, 156);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(67, 15);
            this.label16.TabIndex = 10;
            this.label16.Text = "物料名称";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(7, 152);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(67, 15);
            this.label15.TabIndex = 8;
            this.label15.Text = "存货仓库";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(338, 87);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(52, 15);
            this.label14.TabIndex = 6;
            this.label14.Text = "采购组";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(6, 87);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(67, 15);
            this.label13.TabIndex = 4;
            this.label13.Text = "工业标识";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(338, 24);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(67, 15);
            this.label12.TabIndex = 2;
            this.label12.Text = "物料类型";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(7, 26);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(67, 15);
            this.label11.TabIndex = 0;
            this.label11.Text = "物料编码";
            // 
            // btn_重置
            // 
            this.btn_重置.Location = new System.Drawing.Point(734, 414);
            this.btn_重置.Name = "btn_重置";
            this.btn_重置.Size = new System.Drawing.Size(75, 23);
            this.btn_重置.TabIndex = 21;
            this.btn_重置.Text = "重置";
            this.btn_重置.UseVisualStyleBackColor = true;
            this.btn_重置.Click += new System.EventHandler(this.btn_重置_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(705, 26);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 15);
            this.label2.TabIndex = 32;
            this.label2.Text = "材料标准";
            // 
            // cbb_材料标准
            // 
            this.cbb_材料标准.FormattingEnabled = true;
            this.cbb_材料标准.Location = new System.Drawing.Point(801, 23);
            this.cbb_材料标准.Name = "cbb_材料标准";
            this.cbb_材料标准.Size = new System.Drawing.Size(137, 23);
            this.cbb_材料标准.TabIndex = 33;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(705, 87);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(67, 15);
            this.label3.TabIndex = 34;
            this.label3.Text = "材料规格";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(705, 156);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(37, 15);
            this.label4.TabIndex = 35;
            this.label4.Text = "用途";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(705, 221);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(82, 15);
            this.label5.TabIndex = 36;
            this.label5.Text = "供应商电话";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(705, 288);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(97, 15);
            this.label6.TabIndex = 37;
            this.label6.Text = "数据是否修改";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(338, 296);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(97, 15);
            this.label7.TabIndex = 38;
            this.label7.Text = "产品特殊要求";
            // 
            // cbb_产品特殊要求
            // 
            this.cbb_产品特殊要求.FormattingEnabled = true;
            this.cbb_产品特殊要求.Location = new System.Drawing.Point(489, 288);
            this.cbb_产品特殊要求.Name = "cbb_产品特殊要求";
            this.cbb_产品特殊要求.Size = new System.Drawing.Size(137, 23);
            this.cbb_产品特殊要求.TabIndex = 39;
            // 
            // cbb_材料规格
            // 
            this.cbb_材料规格.FormattingEnabled = true;
            this.cbb_材料规格.Location = new System.Drawing.Point(801, 87);
            this.cbb_材料规格.Name = "cbb_材料规格";
            this.cbb_材料规格.Size = new System.Drawing.Size(137, 23);
            this.cbb_材料规格.TabIndex = 40;
            // 
            // cbb_用途
            // 
            this.cbb_用途.FormattingEnabled = true;
            this.cbb_用途.Location = new System.Drawing.Point(801, 153);
            this.cbb_用途.Name = "cbb_用途";
            this.cbb_用途.Size = new System.Drawing.Size(137, 23);
            this.cbb_用途.TabIndex = 41;
            // 
            // cbb_供应商电话
            // 
            this.cbb_供应商电话.FormattingEnabled = true;
            this.cbb_供应商电话.Location = new System.Drawing.Point(801, 218);
            this.cbb_供应商电话.Name = "cbb_供应商电话";
            this.cbb_供应商电话.Size = new System.Drawing.Size(137, 23);
            this.cbb_供应商电话.TabIndex = 42;
            // 
            // cbb_数据是否修改
            // 
            this.cbb_数据是否修改.FormattingEnabled = true;
            this.cbb_数据是否修改.Location = new System.Drawing.Point(801, 285);
            this.cbb_数据是否修改.Name = "cbb_数据是否修改";
            this.cbb_数据是否修改.Size = new System.Drawing.Size(137, 23);
            this.cbb_数据是否修改.TabIndex = 43;
            // 
            // MTEstablish
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(962, 742);
            this.Controls.Add(this.btn_重置);
            this.Controls.Add(this.groupBox3);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "MTEstablish";
            this.Text = "Establish";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MTEstablish_FormClosed);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btn_重置;
        private System.Windows.Forms.Button btn_创建;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox cbb_物料编码;
        private System.Windows.Forms.ComboBox cbb_批次管理;
        private System.Windows.Forms.ComboBox cbb_物料名称;
        private System.Windows.Forms.ComboBox cbb_采购组;
        private System.Windows.Forms.ComboBox cbb_物料类型;
        private System.Windows.Forms.ComboBox cbb_物料组;
        private System.Windows.Forms.ComboBox cbb_计量单位;
        private System.Windows.Forms.ComboBox cbb_存货仓库;
        private System.Windows.Forms.ComboBox cbb_工业标识;
        private System.Windows.Forms.ComboBox cbb_数据是否修改;
        private System.Windows.Forms.ComboBox cbb_供应商电话;
        private System.Windows.Forms.ComboBox cbb_用途;
        private System.Windows.Forms.ComboBox cbb_材料规格;
        private System.Windows.Forms.ComboBox cbb_产品特殊要求;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cbb_材料标准;
        private System.Windows.Forms.Label label2;
    }
}