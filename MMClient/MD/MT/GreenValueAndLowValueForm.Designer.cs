﻿namespace MMClient.MD.MT
{
    partial class GreenValueAndLowValueForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.TB_PFLowValue = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.TB_PFGreenValue = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.TB_CLowValue = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.TB_CGreenValue = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.button1);
            this.groupBox2.Controls.Add(this.TB_PFLowValue);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.TB_PFGreenValue);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.TB_CLowValue);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.TB_CGreenValue);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Location = new System.Drawing.Point(12, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(621, 426);
            this.groupBox2.TabIndex = 74;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "目标值与最低值";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(220, 293);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 82;
            this.button1.Text = "保存";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // TB_PFLowValue
            // 
            this.TB_PFLowValue.Location = new System.Drawing.Point(242, 193);
            this.TB_PFLowValue.Margin = new System.Windows.Forms.Padding(2);
            this.TB_PFLowValue.Name = "TB_PFLowValue";
            this.TB_PFLowValue.Size = new System.Drawing.Size(147, 21);
            this.TB_PFLowValue.TabIndex = 81;
            this.TB_PFLowValue.Leave += new System.EventHandler(this.TB_PFLowValue_Leave);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(158, 202);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(65, 12);
            this.label8.TabIndex = 80;
            this.label8.Text = "绩效最低值";
            // 
            // TB_PFGreenValue
            // 
            this.TB_PFGreenValue.Location = new System.Drawing.Point(242, 154);
            this.TB_PFGreenValue.Margin = new System.Windows.Forms.Padding(2);
            this.TB_PFGreenValue.Name = "TB_PFGreenValue";
            this.TB_PFGreenValue.Size = new System.Drawing.Size(147, 21);
            this.TB_PFGreenValue.TabIndex = 79;
            this.TB_PFGreenValue.Leave += new System.EventHandler(this.TB_PFGreenValue_Leave);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(158, 157);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(65, 12);
            this.label9.TabIndex = 78;
            this.label9.Text = "绩效目标值";
            // 
            // TB_CLowValue
            // 
            this.TB_CLowValue.Location = new System.Drawing.Point(242, 108);
            this.TB_CLowValue.Margin = new System.Windows.Forms.Padding(2);
            this.TB_CLowValue.Name = "TB_CLowValue";
            this.TB_CLowValue.Size = new System.Drawing.Size(147, 21);
            this.TB_CLowValue.TabIndex = 77;
            this.TB_CLowValue.Leave += new System.EventHandler(this.TB_CLowValue_Leave);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(158, 111);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(65, 12);
            this.label6.TabIndex = 76;
            this.label6.Text = "认证最低值";
            // 
            // TB_CGreenValue
            // 
            this.TB_CGreenValue.Location = new System.Drawing.Point(242, 63);
            this.TB_CGreenValue.Margin = new System.Windows.Forms.Padding(2);
            this.TB_CGreenValue.Name = "TB_CGreenValue";
            this.TB_CGreenValue.Size = new System.Drawing.Size(147, 21);
            this.TB_CGreenValue.TabIndex = 75;
            this.TB_CGreenValue.Leave += new System.EventHandler(this.TB_CGreenValue_Leave);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(158, 66);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(65, 12);
            this.label5.TabIndex = 74;
            this.label5.Text = "认证目标值";
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // GreenValueAndLowValueForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(635, 450);
            this.Controls.Add(this.groupBox2);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "GreenValueAndLowValueForm";
            this.Text = "维护目标值和最低值";
            this.Load += new System.EventHandler(this.GreenValueAndLowValueForm_Load);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox TB_PFLowValue;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox TB_PFGreenValue;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox TB_CLowValue;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox TB_CGreenValue;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.ErrorProvider errorProvider1;
    }
}