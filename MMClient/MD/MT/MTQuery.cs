﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.SqlServerDAL;
using System.Windows.Forms.DataVisualization.Charting;

namespace MMClient.MD
{
    public partial class MTQuery : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        public MTQuery()
        {
            InitializeComponent();
        }

        private void MTQuery_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Dispose();
        }

        private void btn_清除_Click(object sender, EventArgs e)
        {
            cbB_物资类别1.Text = "";
            cbB_物资类别2.Text = "";
            cbB_物料名称1.Text = "";
            cbB_物料名称2.Text = "";
            cbB_采购组1.Text = "";
            cbB_采购组2.Text = "";
            tB_其他.Text = "";
            tB_最大命中数.Text = "";
        }

        private void btn_查询_Click(object sender, EventArgs e)
        {
            string mttype1 = cbB_物资类别1.Text;
            string mttype2 = cbB_物资类别2.Text;
            string mtname1 = cbB_物料名称1.Text;
            string mtname2 = cbB_物料名称2.Text;
            string bg1 = cbB_采购组1.Text;
            string bg2 = cbB_采购组2.Text;
            string other = tB_其他.Text;
            string top = tB_最大命中数.Text;
            try
            {
                if (tB_最大命中数.Text == "")
                {
                    MessageBox.Show("请填写最大命中条数");
                }
                else
                {
                    string sql = "SELECT top " + top + " material_ID as '物料编码',Material_Name as '物料名称',Material_Type as '物料类型',[Buyer_Group] as '采购组',[Format] as '其他' FROM [MM].[dbo].[Material] where (Material_Name='" + mtname1 + "' or  Material_Name='" + mtname2 + "')  and (Material_ID='" + mttype1 + "' or  Material_ID='" + mttype1 + "')";
                    dgV_数据结果.DataSource = DBHelper.ExecuteQueryDT(sql);
                }
            }
            catch (Exception s)
            {
                Console.WriteLine(s.Message);
                //Message返回异常对象中包含的错误信息 
            } 
           
        }

        private void dgV_数据结果_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int rowindex = e.RowIndex;
            string s = "";
            DataTable dt = new DataTable();
            //DataRow dr = dt.NewRow();//产生一个新行  
            //for (int i = 0; i < dgV_数据结果.Columns.Count; i++)
            //{
               // DataColumn dataColumn = new DataColumn();
               // dataColumn.DataType = typeof(string);//数据类型  
               // dataColumn.ColumnName = "列名" + i.ToString();//列名  
               // dt.Columns.Add(dataColumn);//列添加到tempTable  
                s = dgV_数据结果.Rows[rowindex].Cells[0].Value.ToString();
                //dr["列名" + i.ToString()] = s;
                string sql = "SELECT *  FROM [MM].[dbo].[Material] where material_ID='" + s + "'";
                dt= DBHelper.ExecuteQueryDT(sql);
                this.dataGridView2.DataSource = dt;//绑定到dataGridView1
            //}
            //dt.Rows.Add(dr);//行添加到tempTable  
            
            //41251400
        }

        private void button4_Click(object sender, EventArgs e)
        {
            string mttype1 = cbB_物资类别1.Text;
            string mttype2 = cbB_物资类别2.Text;
            string mtname1 = cbB_物料名称1.Text;
            string mtname2 = cbB_物料名称2.Text;
            string bg1 = cbB_采购组1.Text;
            string bg2 = cbB_采购组2.Text;
            string other = tB_其他.Text;
            string top = tB_最大命中数.Text;
            string sql = "SELECT  Material_ID as '物料编码' ,Count(Material_ID) as '总计' FROM [MM].[dbo].[Material]  group by Material_ID";
            //Console.Write(sql);
            //where Material_Name='" + mtname1 + "'
            DataTable dataTable1 = new System.Data.DataTable();
            dataTable1 = DBHelper.ExecuteQueryDT(sql);

            chart1.Series.Clear();//清空表中的数据
            ////第一个表中的数据
            Series dataTable1Series = new Series("物料编码总计数量");
            dataTable1Series.Points.DataBind(dataTable1.AsEnumerable(), "物料编码", "总计", "");
            //dataTable1Series.XValueType = ChartValueType.DateTime; //设置X轴类型为时间
            //dataTable1Series.ChartType = SeriesChartType.Line;  //设置Y轴为折线
            chart1.Series.Add(dataTable1Series);
            ////第二个表中的数据
            //Series dataTable2Series = new Series("dataTable2");
            //dataTable2Series.Points.DataBind(dataTable2.AsEnumerable(), "日期", "日发展", "");
            //dataTable2Series.XValueType = ChartValueType.DateTime;//设置X轴类型为时间
            //dataTable2Series.ChartType = SeriesChartType.Line;  //设置Y轴为折线
            //chart1.Series.Add(dataTable2Series);
            ////第三个表中的数据
            //Series dataTable3Series = new Series("dataTable3");
            //dataTable3Series.Points.DataBind(dataTable3.AsEnumerable(), "日期", "日发展", "");
            //dataTable3Series.XValueType = ChartValueType.DateTime;//设置X轴类型为时间
            //dataTable3Series.ChartType = SeriesChartType.Line;  //设置Y轴为折线
            //chart1.Series.Add(dataTable3Series);
        }

        private void btn_修改_Click(object sender, EventArgs e)
        {
            MTModify mtm = new MTModify();

            mtm.Show();

           //this.Close();

        }

        private void btn_删除_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("您真的要删除吗？", "删除提示", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) == DialogResult.Yes)
            {
                string mtid = dgV_数据结果.CurrentRow.Cells[0].Value.ToString();//获取选中行的第一个单元格的值
                string sql = "Delete from [MM].[dbo].[Material] where Material_ID='" + mtid + "'";
                DBHelper.ExecuteNonQuery(sql);
                MessageBox.Show("删除成功");
            }
            else
            {
                //就不用删除了
            }


            
        }

       
        

       
        
    }
}
