﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Lib.Bll.MDBll.General;
using Lib.Common.CommonUtils;
using Lib.Bll.MDBll.MT;
using Lib.Model.MD.MT;
namespace MMClient.MD.MT
{
    public partial class 存储视图 : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        public 存储视图()
        {
            InitializeComponent();
        }
        GeneralBLL gn = new GeneralBLL();
        MaterialBLL mt = new MaterialBLL();
        ComboBoxItem cbm = new ComboBoxItem();
        MaterialStorageBLL mts = new MaterialStorageBLL();
        FormHelper formh = new Lib.Common.CommonUtils.FormHelper();
        private void cbb_物料编号_SelectedIndexChanged(object sender, EventArgs e)
        {
            
            Choseftymts chf;
            chf = new Choseftymts(this);
            chf.Show(this);
            List<string> list = gn.GetAllMeasurement();
            cbm.FuzzyQury(cbb_计量单位, list);
            MaterialBase mtb = mts.GetBasicInformation(cbb_物料编号.Text);
            cbb_物料名称.Text = mtb.Material_Name;
            cbb_计量单位.Text = mtb.Measurement;
            cbb_净重.Text = mtb.Net_Weight.ToString();
            cbb_毛重.Text = mtb.Gross_Weight.ToString();
            cbb_温度条件.Text = mtb.Temperature_Condition;
            cbb_集装箱需求.Text = mtb.Container_Demand;
            cbb_标号类型.Text = mtb.Label_Type;
            cbb_存储条件.Text = mtb.Storage_Condition;
            cbb_危险物料号.Text = mtb.Dangerous_MTNumber;
            cbb_收货单据数.Text = mtb.Document_Number.ToString();
            tbx_最小剩余货架寿命.Text = mtb.MIN_SLlife;
            tbx_总货架寿命.Text = mtb.Total_SLlife;
            cbb_SLED期间标识.Text = mtb.SLED_Identify;
            cbb_SLED舍入规则.Text = mtb.SLED_Rule;
            cbb_重量单位.Text = mtb.Weight_Unit;
            cbb_体积单位.Text = mtb.Volume_Unit;

        }

        private void 存储视图_Load(object sender, EventArgs e)
        {
            cbb_工厂编码.Enabled = false;
            cbb_工厂名称.Enabled = false;
            cbb_仓库编码.Enabled = false;
            cbb_仓库名称.Enabled = false;
            List<string> list = mt.GetAllMaterialID();
            cbm.FuzzyQury(cbb_物料编号, list);
        }



        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            MaterialChose mtc = new MaterialChose(0, this);
            mtc.Show();
        }

        private void toolStripButton4_Click(object sender, EventArgs e)
        {
            int flag = 0;

            List<string> list2 = mt.GetAllMaterialID();
            if (cbb_物料编号.Text == "" || gn.IDInTheList(list2, cbb_物料编号.Text) == true)
            {
                MessageUtil.ShowError("请选择正确的物料编号");
                cbb_物料编号.Focus();
                flag = 1;
            }
            try
            {
                if (cbb_净重.Text != "" && cbb_毛重.Text != "" && Convert.ToDecimal(cbb_净重.Text) > Convert.ToDecimal(cbb_毛重.Text))
                {
                    MessageUtil.ShowError("错误：净重应小于毛重");
                    cbb_毛重.Focus();
                    flag = 1;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("信息有误");
                return;
            }
            if (flag == 0 && MessageUtil.ShowYesNoAndTips("是否确定修改") == DialogResult.Yes)
            {
                if (cbb_工厂编码.Text == "")
                {
                    MaterialBase mtb = new MaterialBase();
                    mtb.Material_ID = cbb_物料编号.Text;
                    mtb.Measurement = cbb_计量单位.Text;
                    try
                    {
                        mtb.Net_Weight = Convert.ToDecimal(cbb_净重.Text);
                    }
                    catch (Exception ex)
                    {
                        MessageUtil.ShowError("请输入正确的数值");
                        return;
                    }
                    try
                    {
                        mtb.Gross_Weight = Convert.ToDecimal(cbb_毛重.Text);
                    }
                    catch (Exception ex)
                    {
                        MessageUtil.ShowError("请输入正确的数值");
                        return;
                    }
                    mtb.Temperature_Condition = cbb_温度条件.Text;
                    mtb.Container_Demand = cbb_集装箱需求.Text;
                    mtb.Label_Type = cbb_标号类型.Text;
                    mtb.Storage_Condition = cbb_存储条件.Text;
                    mtb.Dangerous_MTNumber = cbb_危险物料号.Text;
                    try
                    {
                        mtb.Document_Number = Convert.ToInt16(cbb_收货单据数.Text);
                    }
                    catch (Exception ex)
                    {
                        MessageUtil.ShowError("请输入正确的数值");
                        return;
                    }
                    mtb.MIN_SLlife = tbx_最小剩余货架寿命.Text;
                    mtb.Total_SLlife = tbx_总货架寿命.Text;
                    mtb.SLED_Identify = cbb_SLED期间标识.Text;
                    mtb.SLED_Rule = cbb_SLED舍入规则.Text;
                    mtb.Weight_Unit = cbb_重量单位.Text;
                    mtb.Volume_Unit = cbb_体积单位.Text;
                    mts.UpdateBasicInformation(mtb);
                    MessageUtil.ShowTips("维护成功");
                }
                else
                {
                    if (cbb_工厂编码.Text != "" && cbb_仓库编码.Text == "")
                    {
                        MaterialFactory mtfty = new MaterialFactory();
                        mtfty.Material_ID = cbb_物料编号.Text;
                        mtfty.Material_Name = cbb_物料名称.Text;
                        mtfty.Factory_ID = cbb_工厂编码.Text;
                        mtfty.Factory_Name = cbb_工厂名称.Text;
                        mtfty.Check_Mark = ckb_盘点标识.Checked;
                        mtfty.Delivery_Unit = cbb_发货单位.Text;
                        try
                        {
                            mtfty.MAX_STPeriod = (float)Convert.ToDouble(cbb_最大仓储时间.Text);
                        }
                        catch (Exception ex)
                        {
                            MessageUtil.ShowError("请输入正确的数值");
                            return;
                        }
                        mtfty.Hourly_Basis = cbb_时间单位.Text;
                        mts.UpdateMtFtyInformation(mtfty);
                        MessageUtil.ShowTips("维护成功");
                    }
                    else
                    {
                        MaterialStorage mtste = new MaterialStorage();
                        mtste.Material_ID = cbb_物料编号.Text;
                        mtste.Material_Name = cbb_物料名称.Text;
                        mtste.Factory_ID = cbb_工厂编码.Text;
                        mtste.Factory_Name = cbb_工厂名称.Text;
                        mtste.Stock_ID = cbb_仓库编码.Text;
                        mtste.Stock_Name = cbb_仓库名称.Text;
                        mtste.Check_Mark = ckb_盘点标识.Checked;
                        mtste.Delivery_Unit = cbb_发货单位.Text;
                        mtste.MAX_STPeriod = cbb_最大仓储时间.Text;
                        mtste.Hourly_Basis = cbb_时间单位.Text;
                        mtste.Inventory_Position = cbb_库存仓位.Text;
                        mts.UpdateMTSteInformation(mtste);
                    }
                    MessageUtil.ShowTips("维护成功");
                }


            }
        }

        private void toolStripButton7_Click(object sender, EventArgs e)
        {
            foreach (Control control in this.Controls)
            {
                if (control.GetType().Name == "ComboBox")
                {
                    ((ComboBox)control).Text = string.Empty;
                }
            }
            cbb_标号类型.Visible = true;
            cbb_仓库名称.Visible = true;
            cbb_发货单位.Visible = true;
            cbb_工厂编码.Visible = true;
            cbb_工厂名称.Visible = true;
            cbb_时间单位.Visible = true;
            cbb_仓库编码.Visible = true;
            lb_标号类型.Visible = true;
            lb_盘点标识.Visible = true;
            lb_发货单位.Visible = true;
            cbb_存储条件.Enabled = true;
            cbb_集装箱需求.Enabled = true;
            cbb_计量单位.Enabled = true;
            cbb_收货单据数.Enabled = true;
            cbb_危险物料号.Enabled = true;
            cbb_温度条件.Enabled = true;
            tbx_总货架寿命.Enabled = true;
            tbx_最小剩余货架寿命.Enabled = true;
            cbb_标号类型.Text = "";
            cbb_仓库编码.Text = "";
            cbb_仓库名称.Text = "";
            cbb_存储条件.Text = "";
            cbb_重量单位.Text = "";
            cbb_发货单位.Text = "";
            cbb_工厂编码.Text = "";
            cbb_工厂名称.Text = "";
            cbb_集装箱需求.Text = "";
            cbb_计量单位.Text = "";
            cbb_净重.Text = "";
            cbb_体积单位.Text = "";
            cbb_毛重.Text = "";
            cbb_时间单位.Text = "";
            cbb_收货单据数.Text = "";
            cbb_危险物料号.Text = "";
            cbb_温度条件.Text = "";
            cbb_物料编号.Text = "";
            cbb_物料名称.Text = "";
            tbx_总货架寿命.Text = "";
            tbx_最小剩余货架寿命.Text = "";
            ckb_盘点标识.Checked = false;
        }

        private void toolStripButton8_Click(object sender, EventArgs e)
        {
            this.Close();
        }

       
        private void cbb_净重_Leave(object sender, EventArgs e)
        {
            Dictionary<string, string> dict_temp = new Dictionary<string, string>();
            string key = "净重";
            dict_temp.Add(key, "");
            formh.onlyNumber(cbb_净重, key, dict_temp);
        }

        private void cbb_毛重_Leave(object sender, EventArgs e)
        {
            Dictionary<string, string> dict_temp = new Dictionary<string, string>();
            string key = "毛重";
            dict_temp.Add(key, "");
            formh.onlyNumber(cbb_毛重, key, dict_temp);
        }
    }
}
