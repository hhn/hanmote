﻿namespace MMClient.MD.MT
{
    partial class DiviedPurOrgnitionsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.pageTool = new pager.pagetool.pageNext();
            this.dgv_data = new System.Windows.Forms.DataGridView();
            this.采购组织 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.工厂代码 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btn_删除 = new System.Windows.Forms.Button();
            this.btn_刷新 = new System.Windows.Forms.Button();
            this.btn_新条目 = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_data)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.pageTool);
            this.groupBox1.Controls.Add(this.dgv_data);
            this.groupBox1.Controls.Add(this.btn_删除);
            this.groupBox1.Controls.Add(this.btn_刷新);
            this.groupBox1.Controls.Add(this.btn_新条目);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(921, 568);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "信息";
            // 
            // pageTool
            // 
            this.pageTool.Location = new System.Drawing.Point(170, 427);
            this.pageTool.Name = "pageTool";
            this.pageTool.PageIndex = 1;
            this.pageTool.PageSize = 100;
            this.pageTool.RecordCount = 0;
            this.pageTool.Size = new System.Drawing.Size(685, 40);
            this.pageTool.TabIndex = 8;
            this.pageTool.OnPageChanged += new System.EventHandler(this.pageTool_OnPageChanged);
            this.pageTool.Load += new System.EventHandler(this.pageTool_Load);
            // 
            // dgv_data
            // 
            this.dgv_data.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgv_data.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.dgv_data.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgv_data.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_data.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.采购组织,
            this.工厂代码});
            this.dgv_data.Location = new System.Drawing.Point(170, 18);
            this.dgv_data.Name = "dgv_data";
            this.dgv_data.RowTemplate.Height = 23;
            this.dgv_data.Size = new System.Drawing.Size(653, 403);
            this.dgv_data.TabIndex = 7;
            // 
            // 采购组织
            // 
            this.采购组织.DataPropertyName = "采购组织";
            this.采购组织.HeaderText = "采购组织";
            this.采购组织.Name = "采购组织";
            // 
            // 工厂代码
            // 
            this.工厂代码.DataPropertyName = "工厂代码";
            this.工厂代码.HeaderText = "工厂代码";
            this.工厂代码.Name = "工厂代码";
            // 
            // btn_删除
            // 
            this.btn_删除.Location = new System.Drawing.Point(27, 311);
            this.btn_删除.Name = "btn_删除";
            this.btn_删除.Size = new System.Drawing.Size(109, 48);
            this.btn_删除.TabIndex = 3;
            this.btn_删除.Text = "删除";
            this.btn_删除.UseVisualStyleBackColor = true;
            this.btn_删除.Click += new System.EventHandler(this.btn_删除_Click);
            // 
            // btn_刷新
            // 
            this.btn_刷新.Location = new System.Drawing.Point(27, 131);
            this.btn_刷新.Name = "btn_刷新";
            this.btn_刷新.Size = new System.Drawing.Size(109, 48);
            this.btn_刷新.TabIndex = 2;
            this.btn_刷新.Text = "刷新";
            this.btn_刷新.UseVisualStyleBackColor = true;
            this.btn_刷新.Click += new System.EventHandler(this.btn_刷新_Click);
            // 
            // btn_新条目
            // 
            this.btn_新条目.Location = new System.Drawing.Point(27, 216);
            this.btn_新条目.Name = "btn_新条目";
            this.btn_新条目.Size = new System.Drawing.Size(109, 48);
            this.btn_新条目.TabIndex = 1;
            this.btn_新条目.Text = "新条目";
            this.btn_新条目.UseVisualStyleBackColor = true;
            this.btn_新条目.Click += new System.EventHandler(this.btn_新条目_Click);
            // 
            // DiviedPurOrgnitionsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1215, 670);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "DiviedPurOrgnitionsForm";
            this.Text = "给个工厂分配采购组织";
            this.Load += new System.EventHandler(this.DiviedPurOrgnitionsForm_Load);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_data)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.GroupBox groupBox1;
        private pager.pagetool.pageNext pageTool;
        private System.Windows.Forms.DataGridView dgv_data;
        private System.Windows.Forms.DataGridViewTextBoxColumn 采购组织;
        private System.Windows.Forms.DataGridViewTextBoxColumn 工厂代码;
        private System.Windows.Forms.Button btn_删除;
        private System.Windows.Forms.Button btn_刷新;
        private System.Windows.Forms.Button btn_新条目;
    }
}