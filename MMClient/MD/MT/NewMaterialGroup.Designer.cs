﻿namespace MMClient.MD.MT
{
    partial class NewMaterialGroup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tbx_物料组描述 = new System.Windows.Forms.TextBox();
            this.tbx_物料组编号 = new System.Windows.Forms.TextBox();
            this.CB_mGroupLevel = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.btn_确定 = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.tbx_物料组描述);
            this.groupBox1.Controls.Add(this.tbx_物料组编号);
            this.groupBox1.Controls.Add(this.CB_mGroupLevel);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.comboBox1);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.btn_确定);
            this.groupBox1.Location = new System.Drawing.Point(5, 4);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox1.Size = new System.Drawing.Size(575, 482);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "新建物料组";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.ForeColor = System.Drawing.Color.Red;
            this.label10.Location = new System.Drawing.Point(145, 98);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(203, 12);
            this.label10.TabIndex = 74;
            this.label10.Text = "物料组编号由字母和数字表示且长为5";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(310, 204);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 12);
            this.label3.TabIndex = 70;
            this.label3.Text = "物料组等级";
            // 
            // tbx_物料组描述
            // 
            this.tbx_物料组描述.Location = new System.Drawing.Point(394, 61);
            this.tbx_物料组描述.Margin = new System.Windows.Forms.Padding(2);
            this.tbx_物料组描述.Name = "tbx_物料组描述";
            this.tbx_物料组描述.Size = new System.Drawing.Size(117, 21);
            this.tbx_物料组描述.TabIndex = 7;
            // 
            // tbx_物料组编号
            // 
            this.tbx_物料组编号.Location = new System.Drawing.Point(146, 60);
            this.tbx_物料组编号.Margin = new System.Windows.Forms.Padding(2);
            this.tbx_物料组编号.Name = "tbx_物料组编号";
            this.tbx_物料组编号.Size = new System.Drawing.Size(147, 21);
            this.tbx_物料组编号.TabIndex = 6;
            // 
            // CB_mGroupLevel
            // 
            this.CB_mGroupLevel.FormattingEnabled = true;
            this.CB_mGroupLevel.Items.AddRange(new object[] {
            "A",
            "B",
            "C",
            "D"});
            this.CB_mGroupLevel.Location = new System.Drawing.Point(390, 199);
            this.CB_mGroupLevel.Name = "CB_mGroupLevel";
            this.CB_mGroupLevel.Size = new System.Drawing.Size(121, 20);
            this.CB_mGroupLevel.TabIndex = 69;
            this.toolTip1.SetToolTip(this.CB_mGroupLevel, "物料等级为审批等级：\r\nA:4\r\nB:3\r\nC:2\r\nD:1");
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(310, 63);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 12);
            this.label2.TabIndex = 1;
            this.label2.Text = "物料组描述";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(146, 198);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(147, 20);
            this.comboBox1.TabIndex = 68;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(73, 64);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "物料组编号";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(84, 202);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 12);
            this.label7.TabIndex = 67;
            this.label7.Text = "所属类型";
            // 
            // btn_确定
            // 
            this.btn_确定.Location = new System.Drawing.Point(248, 358);
            this.btn_确定.Margin = new System.Windows.Forms.Padding(2);
            this.btn_确定.Name = "btn_确定";
            this.btn_确定.Size = new System.Drawing.Size(59, 27);
            this.btn_确定.TabIndex = 8;
            this.btn_确定.Text = "确定";
            this.btn_确定.UseVisualStyleBackColor = true;
            this.btn_确定.Click += new System.EventHandler(this.btn_确定_Click);
            // 
            // NewMaterialGroup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(591, 497);
            this.Controls.Add(this.groupBox1);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "NewMaterialGroup";
            this.Text = "新建物料组";
            this.Load += new System.EventHandler(this.NewMaterialGroup_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox tbx_物料组描述;
        private System.Windows.Forms.TextBox tbx_物料组编号;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btn_确定;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox CB_mGroupLevel;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label7;
    }
}