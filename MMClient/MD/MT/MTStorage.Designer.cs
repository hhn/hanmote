﻿namespace MMClient.MD.MT
{
    partial class 存储视图
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(存储视图));
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label20 = new System.Windows.Forms.Label();
            this.cbb_仓库名称 = new System.Windows.Forms.ComboBox();
            this.cbb_仓库编码 = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cbb_工厂名称 = new System.Windows.Forms.ComboBox();
            this.cbb_工厂编码 = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cbb_物料名称 = new System.Windows.Forms.ComboBox();
            this.cbb_物料编号 = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label11 = new System.Windows.Forms.Label();
            this.cbb_收货单据数 = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.lb_标号类型 = new System.Windows.Forms.Label();
            this.lb_盘点标识 = new System.Windows.Forms.Label();
            this.cbb_危险物料号 = new System.Windows.Forms.ComboBox();
            this.lb_价格确定 = new System.Windows.Forms.Label();
            this.cbb_存储条件 = new System.Windows.Forms.ComboBox();
            this.lb_本期 = new System.Windows.Forms.Label();
            this.lb_发货单位 = new System.Windows.Forms.Label();
            this.cbb_集装箱需求 = new System.Windows.Forms.ComboBox();
            this.lb_产品组 = new System.Windows.Forms.Label();
            this.cbb_温度条件 = new System.Windows.Forms.ComboBox();
            this.lb_货币 = new System.Windows.Forms.Label();
            this.cbb_计量单位 = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.cbb_毛重 = new System.Windows.Forms.TextBox();
            this.cbb_净重 = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.净重 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.cbb_SLED舍入规则 = new System.Windows.Forms.TextBox();
            this.cbb_SLED期间标识 = new System.Windows.Forms.TextBox();
            this.ckb_盘点标识 = new System.Windows.Forms.CheckBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.tbx_总货架寿命 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.tbx_最小剩余货架寿命 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.toolStripExecutionMonitorSetting = new System.Windows.Forms.ToolStrip();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton7 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton8 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton4 = new System.Windows.Forms.ToolStripButton();
            this.cbb_库存仓位 = new System.Windows.Forms.TextBox();
            this.cbb_最大仓储时间 = new System.Windows.Forms.TextBox();
            this.cbb_重量单位 = new System.Windows.Forms.TextBox();
            this.cbb_体积单位 = new System.Windows.Forms.TextBox();
            this.cbb_发货单位 = new System.Windows.Forms.TextBox();
            this.cbb_时间单位 = new System.Windows.Forms.TextBox();
            this.cbb_标号类型 = new System.Windows.Forms.TextBox();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.toolStripExecutionMonitorSetting.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label20);
            this.groupBox2.Controls.Add(this.cbb_仓库名称);
            this.groupBox2.Controls.Add(this.cbb_仓库编码);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.cbb_工厂名称);
            this.groupBox2.Controls.Add(this.cbb_工厂编码);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.cbb_物料名称);
            this.groupBox2.Controls.Add(this.cbb_物料编号);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Font = new System.Drawing.Font("微软雅黑", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.groupBox2.Location = new System.Drawing.Point(12, 67);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1211, 129);
            this.groupBox2.TabIndex = 13;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "存储视图";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.ForeColor = System.Drawing.Color.Red;
            this.label20.Location = new System.Drawing.Point(263, 24);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(65, 20);
            this.label20.TabIndex = 15;
            this.label20.Text = "“必选”";
            // 
            // cbb_仓库名称
            // 
            this.cbb_仓库名称.Enabled = false;
            this.cbb_仓库名称.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.cbb_仓库名称.FormattingEnabled = true;
            this.cbb_仓库名称.Location = new System.Drawing.Point(347, 92);
            this.cbb_仓库名称.Name = "cbb_仓库名称";
            this.cbb_仓库名称.Size = new System.Drawing.Size(205, 25);
            this.cbb_仓库名称.TabIndex = 13;
            // 
            // cbb_仓库编码
            // 
            this.cbb_仓库编码.Enabled = false;
            this.cbb_仓库编码.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.cbb_仓库编码.FormattingEnabled = true;
            this.cbb_仓库编码.Location = new System.Drawing.Point(95, 89);
            this.cbb_仓库编码.Name = "cbb_仓库编码";
            this.cbb_仓库编码.Size = new System.Drawing.Size(163, 25);
            this.cbb_仓库编码.TabIndex = 12;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label3.Location = new System.Drawing.Point(22, 90);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(44, 17);
            this.label3.TabIndex = 11;
            this.label3.Text = "库存地";
            // 
            // cbb_工厂名称
            // 
            this.cbb_工厂名称.Enabled = false;
            this.cbb_工厂名称.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.cbb_工厂名称.FormattingEnabled = true;
            this.cbb_工厂名称.Location = new System.Drawing.Point(347, 58);
            this.cbb_工厂名称.Name = "cbb_工厂名称";
            this.cbb_工厂名称.Size = new System.Drawing.Size(205, 25);
            this.cbb_工厂名称.TabIndex = 6;
            // 
            // cbb_工厂编码
            // 
            this.cbb_工厂编码.Enabled = false;
            this.cbb_工厂编码.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.cbb_工厂编码.FormattingEnabled = true;
            this.cbb_工厂编码.Location = new System.Drawing.Point(95, 58);
            this.cbb_工厂编码.Name = "cbb_工厂编码";
            this.cbb_工厂编码.Size = new System.Drawing.Size(163, 25);
            this.cbb_工厂编码.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.Location = new System.Drawing.Point(22, 56);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 17);
            this.label2.TabIndex = 4;
            this.label2.Text = "工厂";
            // 
            // cbb_物料名称
            // 
            this.cbb_物料名称.Enabled = false;
            this.cbb_物料名称.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.cbb_物料名称.FormattingEnabled = true;
            this.cbb_物料名称.Location = new System.Drawing.Point(347, 24);
            this.cbb_物料名称.Name = "cbb_物料名称";
            this.cbb_物料名称.Size = new System.Drawing.Size(205, 25);
            this.cbb_物料名称.TabIndex = 2;
            // 
            // cbb_物料编号
            // 
            this.cbb_物料编号.Enabled = false;
            this.cbb_物料编号.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.cbb_物料编号.FormattingEnabled = true;
            this.cbb_物料编号.Location = new System.Drawing.Point(95, 24);
            this.cbb_物料编号.Name = "cbb_物料编号";
            this.cbb_物料编号.Size = new System.Drawing.Size(163, 25);
            this.cbb_物料编号.TabIndex = 1;
            this.cbb_物料编号.SelectedIndexChanged += new System.EventHandler(this.cbb_物料编号_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(22, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "物料";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cbb_标号类型);
            this.groupBox1.Controls.Add(this.cbb_时间单位);
            this.groupBox1.Controls.Add(this.cbb_发货单位);
            this.groupBox1.Controls.Add(this.cbb_最大仓储时间);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.cbb_收货单据数);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.lb_标号类型);
            this.groupBox1.Controls.Add(this.lb_盘点标识);
            this.groupBox1.Controls.Add(this.cbb_危险物料号);
            this.groupBox1.Controls.Add(this.lb_价格确定);
            this.groupBox1.Controls.Add(this.cbb_存储条件);
            this.groupBox1.Controls.Add(this.lb_本期);
            this.groupBox1.Controls.Add(this.lb_发货单位);
            this.groupBox1.Controls.Add(this.cbb_集装箱需求);
            this.groupBox1.Controls.Add(this.lb_产品组);
            this.groupBox1.Controls.Add(this.cbb_温度条件);
            this.groupBox1.Controls.Add(this.lb_货币);
            this.groupBox1.Controls.Add(this.cbb_计量单位);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Font = new System.Drawing.Font("微软雅黑", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(12, 193);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1258, 179);
            this.groupBox1.TabIndex = 14;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "一般数据";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label11.Location = new System.Drawing.Point(772, 43);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(80, 17);
            this.label11.TabIndex = 25;
            this.label11.Text = "最大仓储时间";
            // 
            // cbb_收货单据数
            // 
            this.cbb_收货单据数.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.cbb_收货单据数.FormattingEnabled = true;
            this.cbb_收货单据数.Location = new System.Drawing.Point(611, 136);
            this.cbb_收货单据数.Name = "cbb_收货单据数";
            this.cbb_收货单据数.Size = new System.Drawing.Size(121, 25);
            this.cbb_收货单据数.TabIndex = 23;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label7.Location = new System.Drawing.Point(535, 143);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(68, 17);
            this.label7.TabIndex = 22;
            this.label7.Text = "收货单据数";
            // 
            // lb_标号类型
            // 
            this.lb_标号类型.AutoSize = true;
            this.lb_标号类型.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lb_标号类型.Location = new System.Drawing.Point(536, 92);
            this.lb_标号类型.Name = "lb_标号类型";
            this.lb_标号类型.Size = new System.Drawing.Size(56, 17);
            this.lb_标号类型.TabIndex = 20;
            this.lb_标号类型.Text = "标号类型";
            // 
            // lb_盘点标识
            // 
            this.lb_盘点标识.AutoSize = true;
            this.lb_盘点标识.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lb_盘点标识.Location = new System.Drawing.Point(536, 45);
            this.lb_盘点标识.Name = "lb_盘点标识";
            this.lb_盘点标识.Size = new System.Drawing.Size(56, 17);
            this.lb_盘点标识.TabIndex = 18;
            this.lb_盘点标识.Text = "时间单位";
            // 
            // cbb_危险物料号
            // 
            this.cbb_危险物料号.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.cbb_危险物料号.FormattingEnabled = true;
            this.cbb_危险物料号.Location = new System.Drawing.Point(365, 137);
            this.cbb_危险物料号.Name = "cbb_危险物料号";
            this.cbb_危险物料号.Size = new System.Drawing.Size(121, 25);
            this.cbb_危险物料号.TabIndex = 17;
            // 
            // lb_价格确定
            // 
            this.lb_价格确定.AutoSize = true;
            this.lb_价格确定.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lb_价格确定.Location = new System.Drawing.Point(285, 140);
            this.lb_价格确定.Name = "lb_价格确定";
            this.lb_价格确定.Size = new System.Drawing.Size(68, 17);
            this.lb_价格确定.TabIndex = 16;
            this.lb_价格确定.Text = "危险物料号";
            // 
            // cbb_存储条件
            // 
            this.cbb_存储条件.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.cbb_存储条件.FormattingEnabled = true;
            this.cbb_存储条件.Location = new System.Drawing.Point(365, 89);
            this.cbb_存储条件.Name = "cbb_存储条件";
            this.cbb_存储条件.Size = new System.Drawing.Size(121, 25);
            this.cbb_存储条件.TabIndex = 15;
            // 
            // lb_本期
            // 
            this.lb_本期.AutoSize = true;
            this.lb_本期.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lb_本期.Location = new System.Drawing.Point(285, 92);
            this.lb_本期.Name = "lb_本期";
            this.lb_本期.Size = new System.Drawing.Size(56, 17);
            this.lb_本期.TabIndex = 14;
            this.lb_本期.Text = "存储条件";
            // 
            // lb_发货单位
            // 
            this.lb_发货单位.AutoSize = true;
            this.lb_发货单位.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lb_发货单位.Location = new System.Drawing.Point(285, 45);
            this.lb_发货单位.Name = "lb_发货单位";
            this.lb_发货单位.Size = new System.Drawing.Size(56, 17);
            this.lb_发货单位.TabIndex = 10;
            this.lb_发货单位.Text = "发货单位";
            // 
            // cbb_集装箱需求
            // 
            this.cbb_集装箱需求.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.cbb_集装箱需求.FormattingEnabled = true;
            this.cbb_集装箱需求.Location = new System.Drawing.Point(116, 139);
            this.cbb_集装箱需求.Name = "cbb_集装箱需求";
            this.cbb_集装箱需求.Size = new System.Drawing.Size(121, 25);
            this.cbb_集装箱需求.TabIndex = 5;
            // 
            // lb_产品组
            // 
            this.lb_产品组.AutoSize = true;
            this.lb_产品组.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lb_产品组.Location = new System.Drawing.Point(19, 139);
            this.lb_产品组.Name = "lb_产品组";
            this.lb_产品组.Size = new System.Drawing.Size(68, 17);
            this.lb_产品组.TabIndex = 4;
            this.lb_产品组.Text = "集装箱需求";
            // 
            // cbb_温度条件
            // 
            this.cbb_温度条件.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.cbb_温度条件.FormattingEnabled = true;
            this.cbb_温度条件.Location = new System.Drawing.Point(116, 88);
            this.cbb_温度条件.Name = "cbb_温度条件";
            this.cbb_温度条件.Size = new System.Drawing.Size(121, 25);
            this.cbb_温度条件.TabIndex = 3;
            // 
            // lb_货币
            // 
            this.lb_货币.AutoSize = true;
            this.lb_货币.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lb_货币.Location = new System.Drawing.Point(19, 91);
            this.lb_货币.Name = "lb_货币";
            this.lb_货币.Size = new System.Drawing.Size(56, 17);
            this.lb_货币.TabIndex = 2;
            this.lb_货币.Text = "温度条件";
            // 
            // cbb_计量单位
            // 
            this.cbb_计量单位.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.cbb_计量单位.FormattingEnabled = true;
            this.cbb_计量单位.Location = new System.Drawing.Point(116, 41);
            this.cbb_计量单位.Name = "cbb_计量单位";
            this.cbb_计量单位.Size = new System.Drawing.Size(121, 25);
            this.cbb_计量单位.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label4.Location = new System.Drawing.Point(19, 44);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(80, 17);
            this.label4.TabIndex = 0;
            this.label4.Text = "基本计量单位";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label5.Location = new System.Drawing.Point(538, 42);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 17);
            this.label5.TabIndex = 24;
            this.label5.Text = "库存仓位";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.cbb_体积单位);
            this.groupBox3.Controls.Add(this.cbb_重量单位);
            this.groupBox3.Controls.Add(this.cbb_毛重);
            this.groupBox3.Controls.Add(this.cbb_净重);
            this.groupBox3.Controls.Add(this.label15);
            this.groupBox3.Controls.Add(this.label14);
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this.净重);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(18, 488);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(1252, 113);
            this.groupBox3.TabIndex = 15;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "量纲";
            // 
            // cbb_毛重
            // 
            this.cbb_毛重.Location = new System.Drawing.Point(57, 73);
            this.cbb_毛重.Name = "cbb_毛重";
            this.cbb_毛重.Size = new System.Drawing.Size(121, 23);
            this.cbb_毛重.TabIndex = 9;
            this.cbb_毛重.Leave += new System.EventHandler(this.cbb_毛重_Leave);
            // 
            // cbb_净重
            // 
            this.cbb_净重.Location = new System.Drawing.Point(57, 39);
            this.cbb_净重.Name = "cbb_净重";
            this.cbb_净重.Size = new System.Drawing.Size(121, 23);
            this.cbb_净重.TabIndex = 8;
            this.cbb_净重.Leave += new System.EventHandler(this.cbb_净重_Leave);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(287, 77);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(55, 15);
            this.label15.TabIndex = 5;
            this.label15.Text = "体积单位";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(287, 43);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(55, 15);
            this.label14.TabIndex = 4;
            this.label14.Text = "重量单位";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(18, 77);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(31, 15);
            this.label13.TabIndex = 1;
            this.label13.Text = "毛重";
            // 
            // 净重
            // 
            this.净重.AutoSize = true;
            this.净重.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.净重.Location = new System.Drawing.Point(18, 43);
            this.净重.Name = "净重";
            this.净重.Size = new System.Drawing.Size(31, 15);
            this.净重.TabIndex = 0;
            this.净重.Text = "净重";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.cbb_库存仓位);
            this.groupBox4.Controls.Add(this.cbb_SLED舍入规则);
            this.groupBox4.Controls.Add(this.cbb_SLED期间标识);
            this.groupBox4.Controls.Add(this.ckb_盘点标识);
            this.groupBox4.Controls.Add(this.label10);
            this.groupBox4.Controls.Add(this.label9);
            this.groupBox4.Controls.Add(this.label5);
            this.groupBox4.Controls.Add(this.tbx_总货架寿命);
            this.groupBox4.Controls.Add(this.label8);
            this.groupBox4.Controls.Add(this.tbx_最小剩余货架寿命);
            this.groupBox4.Controls.Add(this.label6);
            this.groupBox4.Font = new System.Drawing.Font("微软雅黑", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox4.Location = new System.Drawing.Point(12, 369);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(1258, 113);
            this.groupBox4.TabIndex = 18;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "货架寿命数据";
            // 
            // cbb_SLED舍入规则
            // 
            this.cbb_SLED舍入规则.Location = new System.Drawing.Point(383, 73);
            this.cbb_SLED舍入规则.Name = "cbb_SLED舍入规则";
            this.cbb_SLED舍入规则.Size = new System.Drawing.Size(121, 25);
            this.cbb_SLED舍入规则.TabIndex = 28;
            // 
            // cbb_SLED期间标识
            // 
            this.cbb_SLED期间标识.Location = new System.Drawing.Point(130, 73);
            this.cbb_SLED期间标识.Name = "cbb_SLED期间标识";
            this.cbb_SLED期间标识.Size = new System.Drawing.Size(121, 25);
            this.cbb_SLED期间标识.TabIndex = 27;
            // 
            // ckb_盘点标识
            // 
            this.ckb_盘点标识.AutoSize = true;
            this.ckb_盘点标识.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.ckb_盘点标识.Location = new System.Drawing.Point(542, 79);
            this.ckb_盘点标识.Name = "ckb_盘点标识";
            this.ckb_盘点标识.Size = new System.Drawing.Size(99, 21);
            this.ckb_盘点标识.TabIndex = 26;
            this.ckb_盘点标识.Text = "周期盘点标识";
            this.ckb_盘点标识.UseVisualStyleBackColor = true;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label10.Location = new System.Drawing.Point(289, 79);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(85, 17);
            this.label10.TabIndex = 6;
            this.label10.Text = "SLED舍入规则";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label9.Location = new System.Drawing.Point(21, 79);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(85, 17);
            this.label9.TabIndex = 4;
            this.label9.Text = "SLED期间标识";
            // 
            // tbx_总货架寿命
            // 
            this.tbx_总货架寿命.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tbx_总货架寿命.Location = new System.Drawing.Point(383, 39);
            this.tbx_总货架寿命.Name = "tbx_总货架寿命";
            this.tbx_总货架寿命.Size = new System.Drawing.Size(121, 23);
            this.tbx_总货架寿命.TabIndex = 3;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label8.Location = new System.Drawing.Point(289, 46);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(68, 17);
            this.label8.TabIndex = 2;
            this.label8.Text = "总货架寿命";
            // 
            // tbx_最小剩余货架寿命
            // 
            this.tbx_最小剩余货架寿命.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tbx_最小剩余货架寿命.Location = new System.Drawing.Point(130, 43);
            this.tbx_最小剩余货架寿命.Name = "tbx_最小剩余货架寿命";
            this.tbx_最小剩余货架寿命.Size = new System.Drawing.Size(121, 23);
            this.tbx_最小剩余货架寿命.TabIndex = 1;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label6.Location = new System.Drawing.Point(21, 49);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(104, 17);
            this.label6.TabIndex = 0;
            this.label6.Text = "最小剩余货架寿命";
            // 
            // toolStripExecutionMonitorSetting
            // 
            this.toolStripExecutionMonitorSetting.ImageScalingSize = new System.Drawing.Size(40, 40);
            this.toolStripExecutionMonitorSetting.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton1,
            this.toolStripButton7,
            this.toolStripButton8,
            this.toolStripButton4});
            this.toolStripExecutionMonitorSetting.Location = new System.Drawing.Point(0, 0);
            this.toolStripExecutionMonitorSetting.Name = "toolStripExecutionMonitorSetting";
            this.toolStripExecutionMonitorSetting.Size = new System.Drawing.Size(1282, 64);
            this.toolStripExecutionMonitorSetting.TabIndex = 19;
            this.toolStripExecutionMonitorSetting.Text = "toolStrip1";
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(60, 61);
            this.toolStripButton1.Text = "选择物料";
            this.toolStripButton1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // toolStripButton7
            // 
            this.toolStripButton7.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton7.Image")));
            this.toolStripButton7.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton7.Name = "toolStripButton7";
            this.toolStripButton7.Size = new System.Drawing.Size(44, 61);
            this.toolStripButton7.Text = "刷新";
            this.toolStripButton7.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton7.Click += new System.EventHandler(this.toolStripButton7_Click);
            // 
            // toolStripButton8
            // 
            this.toolStripButton8.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton8.Name = "toolStripButton8";
            this.toolStripButton8.Size = new System.Drawing.Size(36, 61);
            this.toolStripButton8.Text = "关闭";
            this.toolStripButton8.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton8.Click += new System.EventHandler(this.toolStripButton8_Click);
            // 
            // toolStripButton4
            // 
            this.toolStripButton4.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton4.Name = "toolStripButton4";
            this.toolStripButton4.Size = new System.Drawing.Size(36, 61);
            this.toolStripButton4.Text = "确定";
            this.toolStripButton4.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton4.Click += new System.EventHandler(this.toolStripButton4_Click);
            // 
            // cbb_库存仓位
            // 
            this.cbb_库存仓位.Location = new System.Drawing.Point(611, 40);
            this.cbb_库存仓位.Name = "cbb_库存仓位";
            this.cbb_库存仓位.Size = new System.Drawing.Size(121, 25);
            this.cbb_库存仓位.TabIndex = 29;
            // 
            // cbb_最大仓储时间
            // 
            this.cbb_最大仓储时间.Location = new System.Drawing.Point(873, 39);
            this.cbb_最大仓储时间.Name = "cbb_最大仓储时间";
            this.cbb_最大仓储时间.Size = new System.Drawing.Size(109, 25);
            this.cbb_最大仓储时间.TabIndex = 26;
            // 
            // cbb_重量单位
            // 
            this.cbb_重量单位.Location = new System.Drawing.Point(351, 39);
            this.cbb_重量单位.Name = "cbb_重量单位";
            this.cbb_重量单位.Size = new System.Drawing.Size(121, 23);
            this.cbb_重量单位.TabIndex = 10;
            // 
            // cbb_体积单位
            // 
            this.cbb_体积单位.Location = new System.Drawing.Point(351, 73);
            this.cbb_体积单位.Name = "cbb_体积单位";
            this.cbb_体积单位.Size = new System.Drawing.Size(121, 23);
            this.cbb_体积单位.TabIndex = 11;
            // 
            // cbb_发货单位
            // 
            this.cbb_发货单位.Location = new System.Drawing.Point(365, 41);
            this.cbb_发货单位.Name = "cbb_发货单位";
            this.cbb_发货单位.Size = new System.Drawing.Size(121, 25);
            this.cbb_发货单位.TabIndex = 27;
            // 
            // cbb_时间单位
            // 
            this.cbb_时间单位.Location = new System.Drawing.Point(611, 41);
            this.cbb_时间单位.Name = "cbb_时间单位";
            this.cbb_时间单位.Size = new System.Drawing.Size(121, 25);
            this.cbb_时间单位.TabIndex = 28;
            // 
            // cbb_标号类型
            // 
            this.cbb_标号类型.Location = new System.Drawing.Point(611, 86);
            this.cbb_标号类型.Name = "cbb_标号类型";
            this.cbb_标号类型.Size = new System.Drawing.Size(121, 25);
            this.cbb_标号类型.TabIndex = 29;
            // 
            // 存储视图
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1282, 603);
            this.Controls.Add(this.toolStripExecutionMonitorSetting);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox2);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "存储视图";
            this.Text = "存储视图";
            this.Load += new System.EventHandler(this.存储视图_Load);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.toolStripExecutionMonitorSetting.ResumeLayout(false);
            this.toolStripExecutionMonitorSetting.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox2;
        public System.Windows.Forms.ComboBox cbb_工厂名称;
        public System.Windows.Forms.ComboBox cbb_工厂编码;
        private System.Windows.Forms.Label label2;
        public System.Windows.Forms.ComboBox cbb_物料编号;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.ComboBox cbb_仓库名称;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox1;
        public System.Windows.Forms.ComboBox cbb_危险物料号;
        public System.Windows.Forms.Label lb_价格确定;
        public System.Windows.Forms.ComboBox cbb_存储条件;
        public System.Windows.Forms.Label lb_本期;
        public System.Windows.Forms.ComboBox cbb_集装箱需求;
        public System.Windows.Forms.Label lb_产品组;
        public System.Windows.Forms.ComboBox cbb_温度条件;
        public System.Windows.Forms.Label lb_货币;
        public System.Windows.Forms.ComboBox cbb_计量单位;
        private System.Windows.Forms.Label label4;
        public System.Windows.Forms.ComboBox cbb_收货单据数;
        public System.Windows.Forms.Label label7;
        public System.Windows.Forms.Label lb_标号类型;
        public System.Windows.Forms.Label lb_盘点标识;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label 净重;
        public System.Windows.Forms.ComboBox cbb_仓库编码;
        public System.Windows.Forms.Label lb_发货单位;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label6;
        public System.Windows.Forms.CheckBox ckb_盘点标识;
        private System.Windows.Forms.Label label11;
        public System.Windows.Forms.TextBox tbx_总货架寿命;
        public System.Windows.Forms.TextBox tbx_最小剩余货架寿命;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.ToolStrip toolStripExecutionMonitorSetting;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripButton toolStripButton7;
        private System.Windows.Forms.ToolStripButton toolStripButton8;
        private System.Windows.Forms.ToolStripButton toolStripButton4;
        public System.Windows.Forms.ComboBox cbb_物料名称;
        private System.Windows.Forms.TextBox cbb_净重;
        private System.Windows.Forms.TextBox cbb_毛重;
        private System.Windows.Forms.TextBox cbb_SLED期间标识;
        private System.Windows.Forms.TextBox cbb_SLED舍入规则;
        public System.Windows.Forms.TextBox cbb_库存仓位;
        public System.Windows.Forms.TextBox cbb_最大仓储时间;
        public System.Windows.Forms.TextBox cbb_重量单位;
        public System.Windows.Forms.TextBox cbb_体积单位;
        public System.Windows.Forms.TextBox cbb_发货单位;
        public System.Windows.Forms.TextBox cbb_时间单位;
        public System.Windows.Forms.TextBox cbb_标号类型;
    }
}