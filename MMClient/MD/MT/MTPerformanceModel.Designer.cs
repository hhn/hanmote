﻿namespace MMClient.MD.MT
{
    partial class MTPerformanceModel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.CB_materialType = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.CB_modelType = new System.Windows.Forms.ComboBox();
            this.TB_person = new System.Windows.Forms.TextBox();
            this.TB_purOrgName = new System.Windows.Forms.TextBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dgv_secondStandard = new System.Windows.Forms.DataGridView();
            this.isCheck = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.secondStandardId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SubStandardDes = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sencondRate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dgv_MainStandard = new System.Windows.Forms.DataGridView();
            this.isCheckedMain = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.mainStandardId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mainStandardText = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox3.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_secondStandard)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_MainStandard)).BeginInit();
            this.panel1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(463, 23);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 12);
            this.label2.TabIndex = 11;
            this.label2.Text = "业务负责人";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(63, 23);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 12);
            this.label3.TabIndex = 8;
            this.label3.Text = "所属公司";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.CB_materialType);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.CB_modelType);
            this.groupBox3.Controls.Add(this.TB_person);
            this.groupBox3.Controls.Add(this.TB_purOrgName);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Location = new System.Drawing.Point(12, 13);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(1162, 98);
            this.groupBox3.TabIndex = 12;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "基本信息";
            // 
            // CB_materialType
            // 
            this.CB_materialType.FormattingEnabled = true;
            this.CB_materialType.Location = new System.Drawing.Point(122, 69);
            this.CB_materialType.Name = "CB_materialType";
            this.CB_materialType.Size = new System.Drawing.Size(168, 20);
            this.CB_materialType.TabIndex = 15;
            this.CB_materialType.SelectedIndexChanged += new System.EventHandler(this.CB_materialType_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(458, 72);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 12);
            this.label4.TabIndex = 17;
            this.label4.Text = "模板类型";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(63, 72);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 12);
            this.label5.TabIndex = 14;
            this.label5.Text = "物料类型";
            // 
            // CB_modelType
            // 
            this.CB_modelType.FormattingEnabled = true;
            this.CB_modelType.Items.AddRange(new object[] {
            "01",
            "02"});
            this.CB_modelType.Location = new System.Drawing.Point(534, 69);
            this.CB_modelType.Name = "CB_modelType";
            this.CB_modelType.Size = new System.Drawing.Size(151, 20);
            this.CB_modelType.TabIndex = 16;
            this.toolTip1.SetToolTip(this.CB_modelType, "01默认模板\\n02自定义模板");
            this.CB_modelType.SelectedIndexChanged += new System.EventHandler(this.CB_modelType_SelectedIndexChanged);
            this.CB_modelType.MouseHover += new System.EventHandler(this.CB_modelType_MouseHover);
            // 
            // TB_person
            // 
            this.TB_person.Enabled = false;
            this.TB_person.Location = new System.Drawing.Point(534, 20);
            this.TB_person.Name = "TB_person";
            this.TB_person.Size = new System.Drawing.Size(151, 21);
            this.TB_person.TabIndex = 13;
            // 
            // TB_purOrgName
            // 
            this.TB_purOrgName.Enabled = false;
            this.TB_purOrgName.Location = new System.Drawing.Point(122, 20);
            this.TB_purOrgName.Name = "TB_purOrgName";
            this.TB_purOrgName.Size = new System.Drawing.Size(168, 21);
            this.TB_purOrgName.TabIndex = 12;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.groupBox2);
            this.groupBox5.Controls.Add(this.groupBox1);
            this.groupBox5.Location = new System.Drawing.Point(19, 6);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(1134, 704);
            this.groupBox5.TabIndex = 14;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "标准信息";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dgv_secondStandard);
            this.groupBox2.Location = new System.Drawing.Point(521, 20);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(607, 684);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "次标准";
            // 
            // dgv_secondStandard
            // 
            this.dgv_secondStandard.AllowUserToAddRows = false;
            this.dgv_secondStandard.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.dgv_secondStandard.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.dgv_secondStandard.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgv_secondStandard.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_secondStandard.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.isCheck,
            this.secondStandardId,
            this.SubStandardDes,
            this.sencondRate});
            this.dgv_secondStandard.Location = new System.Drawing.Point(7, 20);
            this.dgv_secondStandard.Name = "dgv_secondStandard";
            this.dgv_secondStandard.RowHeadersVisible = false;
            this.dgv_secondStandard.RowTemplate.Height = 23;
            this.dgv_secondStandard.Size = new System.Drawing.Size(595, 658);
            this.dgv_secondStandard.TabIndex = 2;
            this.dgv_secondStandard.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_secondStandard_CellContentClick);
            this.dgv_secondStandard.CurrentCellDirtyStateChanged += new System.EventHandler(this.dgv_secondStandard_CurrentCellDirtyStateChanged);
            // 
            // isCheck
            // 
            this.isCheck.FalseValue = "0";
            this.isCheck.HeaderText = "选择";
            this.isCheck.Name = "isCheck";
            this.isCheck.TrueValue = "1";
            this.isCheck.Width = 50;
            // 
            // secondStandardId
            // 
            this.secondStandardId.DataPropertyName = "SubStandardCode";
            this.secondStandardId.HeaderText = "次标准编号";
            this.secondStandardId.Name = "secondStandardId";
            this.secondStandardId.ReadOnly = true;
            // 
            // SubStandardDes
            // 
            this.SubStandardDes.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.SubStandardDes.DataPropertyName = "SubStandardDes";
            this.SubStandardDes.HeaderText = "次标准";
            this.SubStandardDes.Name = "SubStandardDes";
            this.SubStandardDes.ReadOnly = true;
            // 
            // sencondRate
            // 
            this.sencondRate.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.sencondRate.DataPropertyName = "sencondRate";
            this.sencondRate.HeaderText = "权重";
            this.sencondRate.Name = "sencondRate";
            this.sencondRate.ToolTipText = "百分数";
            this.sencondRate.Width = 54;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dgv_MainStandard);
            this.groupBox1.Location = new System.Drawing.Point(7, 20);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(508, 678);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "主标准";
            // 
            // dgv_MainStandard
            // 
            this.dgv_MainStandard.AllowUserToAddRows = false;
            this.dgv_MainStandard.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.dgv_MainStandard.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.dgv_MainStandard.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgv_MainStandard.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_MainStandard.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.isCheckedMain,
            this.mainStandardId,
            this.mainStandardText,
            this.rate});
            this.dgv_MainStandard.Location = new System.Drawing.Point(6, 16);
            this.dgv_MainStandard.Name = "dgv_MainStandard";
            this.dgv_MainStandard.RowHeadersVisible = false;
            this.dgv_MainStandard.RowTemplate.Height = 23;
            this.dgv_MainStandard.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv_MainStandard.Size = new System.Drawing.Size(496, 656);
            this.dgv_MainStandard.TabIndex = 2;
            this.dgv_MainStandard.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_MainStandard_CellContentClick);
            this.dgv_MainStandard.CurrentCellDirtyStateChanged += new System.EventHandler(this.dgv_MainStandard_CurrentCellDirtyStateChanged);
            this.dgv_MainStandard.SelectionChanged += new System.EventHandler(this.dgv_MainStandard_SelectionChanged);
            // 
            // isCheckedMain
            // 
            this.isCheckedMain.HeaderText = "勾选";
            this.isCheckedMain.Name = "isCheckedMain";
            this.isCheckedMain.ToolTipText = "勾选主标准";
            this.isCheckedMain.Width = 50;
            // 
            // mainStandardId
            // 
            this.mainStandardId.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.mainStandardId.DataPropertyName = "mainStandardId";
            this.mainStandardId.HeaderText = "主标准编号";
            this.mainStandardId.Name = "mainStandardId";
            this.mainStandardId.ReadOnly = true;
            this.mainStandardId.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.mainStandardId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // mainStandardText
            // 
            this.mainStandardText.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.mainStandardText.DataPropertyName = "mainStandardText";
            this.mainStandardText.HeaderText = "主标准";
            this.mainStandardText.Name = "mainStandardText";
            this.mainStandardText.ReadOnly = true;
            // 
            // rate
            // 
            this.rate.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.rate.DataPropertyName = "mainStandardRate";
            this.rate.HeaderText = "权重";
            this.rate.Name = "rate";
            this.rate.ToolTipText = "输入主标准权重";
            this.rate.Width = 54;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.button3);
            this.panel1.Controls.Add(this.button2);
            this.panel1.Location = new System.Drawing.Point(26, 716);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1110, 45);
            this.panel1.TabIndex = 15;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(994, 16);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 1;
            this.button3.Text = "返回";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(889, 16);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 0;
            this.button2.Text = "保存";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(12, 117);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1167, 793);
            this.tabControl1.TabIndex = 16;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupBox5);
            this.tabPage1.Controls.Add(this.panel1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1159, 767);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "创建评估模型";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1159, 767);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "查看评估模型";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // MTPerformanceModel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1186, 922);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.groupBox3);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "MTPerformanceModel";
            this.Text = "维护评估模板";
            this.Load += new System.EventHandler(this.MTPerformanceModel_Load);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_secondStandard)).EndInit();
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_MainStandard)).EndInit();
            this.panel1.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox TB_person;
        private System.Windows.Forms.TextBox TB_purOrgName;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView dgv_secondStandard;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView dgv_MainStandard;
        private System.Windows.Forms.ComboBox CB_materialType;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox CB_modelType;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.DataGridViewCheckBoxColumn isCheck;
        private System.Windows.Forms.DataGridViewTextBoxColumn secondStandardId;
        private System.Windows.Forms.DataGridViewTextBoxColumn SubStandardDes;
        private System.Windows.Forms.DataGridViewTextBoxColumn sencondRate;
        private System.Windows.Forms.DataGridViewCheckBoxColumn isCheckedMain;
        private System.Windows.Forms.DataGridViewTextBoxColumn mainStandardId;
        private System.Windows.Forms.DataGridViewTextBoxColumn mainStandardText;
        private System.Windows.Forms.DataGridViewTextBoxColumn rate;
    }
}