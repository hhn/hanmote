﻿namespace MMClient.MD.MT
{
    partial class Material_Group
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Material_Group));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.pageNext1 = new pager.pagetool.pageNext();
            this.panel1 = new System.Windows.Forms.Panel();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.btn_刷新数据 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.btn_新条目 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.btn_删除 = new System.Windows.Forms.Button();
            this.dgv_物料组 = new System.Windows.Forms.DataGridView();
            this.编号 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.删除ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.名称 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.物料组类型 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.分级 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.分级描述 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.认证绿色目标 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.认证最低目标 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.绩效绿色目标 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.绩效最低目标 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.dataGridViewComboBoxColumn1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_物料组)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox1.Controls.Add(this.pageNext1);
            this.groupBox1.Controls.Add(this.panel1);
            this.groupBox1.Controls.Add(this.dgv_物料组);
            this.groupBox1.Location = new System.Drawing.Point(2, 1);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(791, 619);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "维护物料组";
            // 
            // pageNext1
            // 
            this.pageNext1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.pageNext1.Location = new System.Drawing.Point(10, 576);
            this.pageNext1.Name = "pageNext1";
            this.pageNext1.PageIndex = 1;
            this.pageNext1.PageSize = 100;
            this.pageNext1.RecordCount = 0;
            this.pageNext1.Size = new System.Drawing.Size(760, 37);
            this.pageNext1.TabIndex = 9;
            this.pageNext1.Load += new System.EventHandler(this.pageNext1_Load);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.button4);
            this.panel1.Controls.Add(this.button3);
            this.panel1.Controls.Add(this.btn_刷新数据);
            this.panel1.Controls.Add(this.button2);
            this.panel1.Controls.Add(this.btn_新条目);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.btn_删除);
            this.panel1.Location = new System.Drawing.Point(0, 18);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(784, 82);
            this.panel1.TabIndex = 8;
            // 
            // button4
            // 
            this.button4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button4.Location = new System.Drawing.Point(635, 36);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(92, 27);
            this.button4.TabIndex = 9;
            this.button4.Text = "保存";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Visible = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(504, 36);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(92, 27);
            this.button3.TabIndex = 8;
            this.button3.Text = "批量导入";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // btn_刷新数据
            // 
            this.btn_刷新数据.Location = new System.Drawing.Point(381, 34);
            this.btn_刷新数据.Name = "btn_刷新数据";
            this.btn_刷新数据.Size = new System.Drawing.Size(92, 31);
            this.btn_刷新数据.TabIndex = 3;
            this.btn_刷新数据.Text = "刷新数据";
            this.btn_刷新数据.UseVisualStyleBackColor = true;
            this.btn_刷新数据.Click += new System.EventHandler(this.btn_刷新数据_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(678, 3);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(92, 27);
            this.button2.TabIndex = 7;
            this.button2.Text = "角色信息录入";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Visible = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // btn_新条目
            // 
            this.btn_新条目.Location = new System.Drawing.Point(253, 34);
            this.btn_新条目.Name = "btn_新条目";
            this.btn_新条目.Size = new System.Drawing.Size(92, 28);
            this.btn_新条目.TabIndex = 4;
            this.btn_新条目.Text = "新条目";
            this.btn_新条目.UseVisualStyleBackColor = true;
            this.btn_新条目.Click += new System.EventHandler(this.btn_新条目_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 34);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(92, 27);
            this.button1.TabIndex = 6;
            this.button1.Text = "修改";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btn_删除
            // 
            this.btn_删除.Location = new System.Drawing.Point(136, 34);
            this.btn_删除.Name = "btn_删除";
            this.btn_删除.Size = new System.Drawing.Size(92, 27);
            this.btn_删除.TabIndex = 5;
            this.btn_删除.Text = "删除";
            this.btn_删除.UseVisualStyleBackColor = true;
            this.btn_删除.Click += new System.EventHandler(this.btn_删除_Click);
            // 
            // dgv_物料组
            // 
            this.dgv_物料组.AllowUserToAddRows = false;
            this.dgv_物料组.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.dgv_物料组.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_物料组.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgv_物料组.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgv_物料组.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_物料组.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.编号,
            this.名称,
            this.物料组类型,
            this.分级,
            this.分级描述,
            this.认证绿色目标,
            this.认证最低目标,
            this.绩效绿色目标,
            this.绩效最低目标});
            this.dgv_物料组.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dgv_物料组.Location = new System.Drawing.Point(0, 106);
            this.dgv_物料组.Name = "dgv_物料组";
            this.dgv_物料组.RowTemplate.Height = 24;
            this.dgv_物料组.Size = new System.Drawing.Size(785, 464);
            this.dgv_物料组.TabIndex = 0;
            this.dgv_物料组.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_物料组_CellValueChanged);
            // 
            // 编号
            // 
            this.编号.ContextMenuStrip = this.contextMenuStrip1;
            this.编号.DataPropertyName = "编号";
            this.编号.HeaderText = "编号";
            this.编号.Name = "编号";
            this.编号.ReadOnly = true;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.删除ToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(101, 26);
            // 
            // 删除ToolStripMenuItem
            // 
            this.删除ToolStripMenuItem.Name = "删除ToolStripMenuItem";
            this.删除ToolStripMenuItem.Size = new System.Drawing.Size(100, 22);
            this.删除ToolStripMenuItem.Text = "删除";
            this.删除ToolStripMenuItem.Click += new System.EventHandler(this.删除ToolStripMenuItem_Click);
            // 
            // 名称
            // 
            this.名称.ContextMenuStrip = this.contextMenuStrip1;
            this.名称.DataPropertyName = "名称";
            this.名称.HeaderText = "名称";
            this.名称.Name = "名称";
            this.名称.ReadOnly = true;
            // 
            // 物料组类型
            // 
            this.物料组类型.ContextMenuStrip = this.contextMenuStrip1;
            this.物料组类型.DataPropertyName = "物料组类型";
            this.物料组类型.HeaderText = "物料组类型";
            this.物料组类型.Name = "物料组类型";
            this.物料组类型.ReadOnly = true;
            // 
            // 分级
            // 
            this.分级.DataPropertyName = "分级";
            this.分级.HeaderText = "分级";
            this.分级.Items.AddRange(new object[] {
            "A",
            "B",
            "C",
            "D"});
            this.分级.Name = "分级";
            this.分级.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.分级.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // 分级描述
            // 
            this.分级描述.DataPropertyName = "分级描述";
            this.分级描述.HeaderText = "分级描述";
            this.分级描述.Name = "分级描述";
            this.分级描述.ReadOnly = true;
            // 
            // 认证绿色目标
            // 
            this.认证绿色目标.DataPropertyName = "认证绿色目标";
            this.认证绿色目标.HeaderText = "认证绿色目标";
            this.认证绿色目标.Name = "认证绿色目标";
            this.认证绿色目标.Visible = false;
            // 
            // 认证最低目标
            // 
            this.认证最低目标.DataPropertyName = "认证最低目标";
            this.认证最低目标.HeaderText = "认证最低目标";
            this.认证最低目标.Name = "认证最低目标";
            this.认证最低目标.Visible = false;
            // 
            // 绩效绿色目标
            // 
            this.绩效绿色目标.DataPropertyName = "绩效绿色目标";
            this.绩效绿色目标.HeaderText = "绩效绿色目标";
            this.绩效绿色目标.Name = "绩效绿色目标";
            this.绩效绿色目标.Visible = false;
            // 
            // 绩效最低目标
            // 
            this.绩效最低目标.DataPropertyName = "绩效最低目标";
            this.绩效最低目标.HeaderText = "绩效最低目标";
            this.绩效最低目标.Name = "绩效最低目标";
            this.绩效最低目标.Visible = false;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.Filter = "Excel文件(*.xsl)|*.xsl|Excel文件(*.xlsx)|*.xlsx";
            // 
            // dataGridViewComboBoxColumn1
            // 
            this.dataGridViewComboBoxColumn1.DataPropertyName = "分级";
            this.dataGridViewComboBoxColumn1.HeaderText = "分级";
            this.dataGridViewComboBoxColumn1.Items.AddRange(new object[] {
            "A",
            "B",
            "C",
            "D"});
            this.dataGridViewComboBoxColumn1.Name = "dataGridViewComboBoxColumn1";
            this.dataGridViewComboBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewComboBoxColumn1.Width = 143;
            // 
            // Material_Group
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(798, 658);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Material_Group";
            this.Text = "维护物料组";
            this.Load += new System.EventHandler(this.Material_Group_Load);
            this.groupBox1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_物料组)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btn_删除;
        private System.Windows.Forms.Button btn_新条目;
        private System.Windows.Forms.Button btn_刷新数据;
        private System.Windows.Forms.DataGridView dgv_物料组;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn1;
        private pager.pagetool.pageNext pageNext1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 删除ToolStripMenuItem;
        private System.Windows.Forms.DataGridViewTextBoxColumn 编号;
        private System.Windows.Forms.DataGridViewTextBoxColumn 名称;
        private System.Windows.Forms.DataGridViewTextBoxColumn 物料组类型;
        private System.Windows.Forms.DataGridViewComboBoxColumn 分级;
        private System.Windows.Forms.DataGridViewTextBoxColumn 分级描述;
        private System.Windows.Forms.DataGridViewTextBoxColumn 认证绿色目标;
        private System.Windows.Forms.DataGridViewTextBoxColumn 认证最低目标;
        private System.Windows.Forms.DataGridViewTextBoxColumn 绩效绿色目标;
        private System.Windows.Forms.DataGridViewTextBoxColumn 绩效最低目标;
    }
}