﻿namespace MMClient.MD
{
    partial class MTModify
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.btn_清除 = new System.Windows.Forms.Button();
            this.btn_修改 = new System.Windows.Forms.Button();
            this.cbb_采购组2 = new System.Windows.Forms.ComboBox();
            this.cbb_采购组1 = new System.Windows.Forms.ComboBox();
            this.cbb_ID = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cbb_物资级别2 = new System.Windows.Forms.ComboBox();
            this.cbb_物资级别1 = new System.Windows.Forms.ComboBox();
            this.cbb_物资类别2 = new System.Windows.Forms.ComboBox();
            this.cbb_物资类别1 = new System.Windows.Forms.ComboBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.btn_清除);
            this.groupBox4.Controls.Add(this.btn_修改);
            this.groupBox4.Controls.Add(this.cbb_采购组2);
            this.groupBox4.Controls.Add(this.cbb_采购组1);
            this.groupBox4.Controls.Add(this.cbb_ID);
            this.groupBox4.Controls.Add(this.label1);
            this.groupBox4.Controls.Add(this.cbb_物资级别2);
            this.groupBox4.Controls.Add(this.cbb_物资级别1);
            this.groupBox4.Controls.Add(this.cbb_物资类别2);
            this.groupBox4.Controls.Add(this.cbb_物资类别1);
            this.groupBox4.Controls.Add(this.label21);
            this.groupBox4.Controls.Add(this.label22);
            this.groupBox4.Controls.Add(this.label23);
            this.groupBox4.Controls.Add(this.label24);
            this.groupBox4.Controls.Add(this.label25);
            this.groupBox4.Controls.Add(this.label26);
            this.groupBox4.Location = new System.Drawing.Point(13, 3);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox4.Size = new System.Drawing.Size(1130, 711);
            this.groupBox4.TabIndex = 2;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "修改新数据";
            // 
            // btn_清除
            // 
            this.btn_清除.Location = new System.Drawing.Point(676, 389);
            this.btn_清除.Name = "btn_清除";
            this.btn_清除.Size = new System.Drawing.Size(64, 40);
            this.btn_清除.TabIndex = 45;
            this.btn_清除.Text = "清除";
            this.btn_清除.UseVisualStyleBackColor = true;
            this.btn_清除.Click += new System.EventHandler(this.btn_清除_Click);
            // 
            // btn_修改
            // 
            this.btn_修改.Location = new System.Drawing.Point(553, 389);
            this.btn_修改.Name = "btn_修改";
            this.btn_修改.Size = new System.Drawing.Size(75, 40);
            this.btn_修改.TabIndex = 44;
            this.btn_修改.Text = "修改";
            this.btn_修改.UseVisualStyleBackColor = true;
            this.btn_修改.Click += new System.EventHandler(this.btn_修改_Click);
            // 
            // cbb_采购组2
            // 
            this.cbb_采购组2.FormattingEnabled = true;
            this.cbb_采购组2.Location = new System.Drawing.Point(571, 210);
            this.cbb_采购组2.Name = "cbb_采购组2";
            this.cbb_采购组2.Size = new System.Drawing.Size(159, 24);
            this.cbb_采购组2.TabIndex = 43;
            // 
            // cbb_采购组1
            // 
            this.cbb_采购组1.FormattingEnabled = true;
            this.cbb_采购组1.Location = new System.Drawing.Point(175, 210);
            this.cbb_采购组1.Name = "cbb_采购组1";
            this.cbb_采购组1.Size = new System.Drawing.Size(159, 24);
            this.cbb_采购组1.TabIndex = 42;
            // 
            // cbb_ID
            // 
            this.cbb_ID.FormattingEnabled = true;
            this.cbb_ID.Location = new System.Drawing.Point(175, 28);
            this.cbb_ID.Name = "cbb_ID";
            this.cbb_ID.Size = new System.Drawing.Size(159, 24);
            this.cbb_ID.TabIndex = 41;
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(23, 31);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(105, 17);
            this.label1.TabIndex = 40;
            this.label1.Text = "待修改物料的ID";
            // 
            // cbb_物资级别2
            // 
            this.cbb_物资级别2.FormattingEnabled = true;
            this.cbb_物资级别2.Location = new System.Drawing.Point(571, 140);
            this.cbb_物资级别2.Name = "cbb_物资级别2";
            this.cbb_物资级别2.Size = new System.Drawing.Size(159, 24);
            this.cbb_物资级别2.TabIndex = 39;
            // 
            // cbb_物资级别1
            // 
            this.cbb_物资级别1.FormattingEnabled = true;
            this.cbb_物资级别1.Location = new System.Drawing.Point(175, 144);
            this.cbb_物资级别1.Name = "cbb_物资级别1";
            this.cbb_物资级别1.Size = new System.Drawing.Size(159, 24);
            this.cbb_物资级别1.TabIndex = 38;
            // 
            // cbb_物资类别2
            // 
            this.cbb_物资类别2.FormattingEnabled = true;
            this.cbb_物资类别2.Location = new System.Drawing.Point(571, 87);
            this.cbb_物资类别2.Name = "cbb_物资类别2";
            this.cbb_物资类别2.Size = new System.Drawing.Size(159, 24);
            this.cbb_物资类别2.TabIndex = 37;
            // 
            // cbb_物资类别1
            // 
            this.cbb_物资类别1.FormattingEnabled = true;
            this.cbb_物资类别1.Location = new System.Drawing.Point(175, 87);
            this.cbb_物资类别1.Name = "cbb_物资类别1";
            this.cbb_物资类别1.Size = new System.Drawing.Size(159, 24);
            this.cbb_物资类别1.TabIndex = 36;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(429, 213);
            this.label21.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(50, 17);
            this.label21.TabIndex = 34;
            this.label21.Text = "修改至";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(23, 213);
            this.label22.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(50, 17);
            this.label22.TabIndex = 32;
            this.label22.Text = "采购组";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(429, 143);
            this.label23.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(50, 17);
            this.label23.TabIndex = 30;
            this.label23.Text = "修改至";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(23, 147);
            this.label24.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(64, 17);
            this.label24.TabIndex = 28;
            this.label24.Text = "物资级别";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(429, 90);
            this.label25.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(50, 17);
            this.label25.TabIndex = 26;
            this.label25.Text = "修改至";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(23, 90);
            this.label26.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(64, 17);
            this.label26.TabIndex = 24;
            this.label26.Text = "物资类别";
            // 
            // MTModify
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1156, 453);
            this.Controls.Add(this.groupBox4);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "MTModify";
            this.Text = "Modify";
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.ComboBox cbb_物资级别1;
        private System.Windows.Forms.ComboBox cbb_物资类别2;
        private System.Windows.Forms.ComboBox cbb_物资类别1;
        private System.Windows.Forms.ComboBox cbb_物资级别2;
        private System.Windows.Forms.Button btn_清除;
        private System.Windows.Forms.Button btn_修改;
        private System.Windows.Forms.ComboBox cbb_采购组2;
        private System.Windows.Forms.ComboBox cbb_采购组1;
        private System.Windows.Forms.ComboBox cbb_ID;
        private System.Windows.Forms.Label label1;
    }
}