﻿using System;
using System.Data;
using System.IO;
using System.Windows.Forms;
using Lib.Bll.MDBll.General;
using Lib.Common.CommonUtils;
using Lib.Common.MMCException.IDAL;
using Lib.Model.MT_GroupModel;
using Lib.SqlServerDAL;

namespace MMClient.MD.MT
{
    public partial class Material_Group : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        int toalNum =  DBHelper.ExecuteQueryDT("select * from Material_Group").Rows.Count;
        ModifyMaterialGroupInfoForm modifyMaterialGroupInfoForm = null;
        public Material_Group()
        {
            InitializeComponent();
        }

        private void btn_刷新数据_Click(object sender, EventArgs e)
        {
            GeneralBLL gn = new GeneralBLL();
            DataTable dt = gn.GetAllMaterialGroup(this.pageNext1.PageSize,this.pageNext1.PageIndex);
            int toalNum = DBHelper.ExecuteQueryDT("select * from Material_Group").Rows.Count;
            pageNext1.DrawControl(toalNum);
            dgv_物料组.DataSource = dt;
            pageNext1_Load(sender,e);
        }

        private void btn_新条目_Click(object sender, EventArgs e)
        {
            NewMaterialGroup nmg = new NewMaterialGroup();
            nmg.Show();
        }

        private void Material_Group_Load(object sender, EventArgs e)
        {
            GeneralBLL gn = new GeneralBLL();
            DataTable dt = gn.GetAllMaterialGroup(this.pageNext1.PageSize,this.pageNext1.PageIndex);
            DataTable temp = new DataTable();
            temp = dt.DefaultView.ToTable(false, "分级");
            this.分级.DisplayMember = "分级";
            this.分级.ValueMember = "分级";

            this.分级.DataPropertyName = "分级";

            dgv_物料组.DataSource = dt;
        }

        private void btn_删除_Click(object sender, EventArgs e)
        {
            GeneralBLL gn = new GeneralBLL();
            if (dgv_物料组.CurrentRow == null)
                MessageUtil.ShowError("请选择要删除的行");
            else
            {
                if (MessageUtil.ShowYesNoAndTips("是否确定删除") == DialogResult.Yes)
                {
                    try
                    {
                        string GroupID = dgv_物料组.CurrentRow.Cells["编号"].Value.ToString();
                        gn.DeleteMaterialGroup(GroupID);
                        dgv_物料组.Rows.Remove(dgv_物料组.CurrentRow);
                        MessageBox.Show("删除成功");
                        pageNext1_Load(sender, e);
                    }
                    catch(DBException ex)
                    {
                        MessageBox.Show("删除失败");
                    }
                }

            }
        }

    

        private void button1_Click(object sender, EventArgs e)
        {
            MaterialGroupModel materialGroupModel = new MaterialGroupModel();
            GeneralBLL gn = new GeneralBLL();
            if (dgv_物料组.CurrentRow == null)
                MessageUtil.ShowError("请选择要修改的行");
            else
            {
                materialGroupModel.MtGroupId = dgv_物料组.CurrentRow.Cells["编号"] == null ? "" : dgv_物料组.CurrentRow.Cells["编号"].Value.ToString();
                materialGroupModel.MtGroupName = dgv_物料组.CurrentRow.Cells["名称"] == null ? "" : dgv_物料组.CurrentRow.Cells["名称"].Value.ToString();
                materialGroupModel.MtGroupClass = dgv_物料组.CurrentRow.Cells["物料组类型"] == null ? "" : dgv_物料组.CurrentRow.Cells["物料组类型"].Value.ToString();
                materialGroupModel.MtGroupLevel = dgv_物料组.CurrentRow.Cells["分级"] == null ? "" : dgv_物料组.CurrentRow.Cells["分级"].Value.ToString();
                materialGroupModel.MtGroupLevelValue = dgv_物料组.CurrentRow.Cells["分级描述"] == null ? "" : dgv_物料组.CurrentRow.Cells["分级描述"].Value.ToString();
                materialGroupModel.FactoryId = "";
                if (modifyMaterialGroupInfoForm == null || modifyMaterialGroupInfoForm.IsDisposed)
                {
                    modifyMaterialGroupInfoForm = new ModifyMaterialGroupInfoForm(materialGroupModel);
                }
                modifyMaterialGroupInfoForm.Show();

            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Addcharacter nm = new Addcharacter();
            nm.Show();
        }

      

        private void dgv_物料组_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            string level = "";
            if (dgv_物料组.Columns[e.ColumnIndex].Name=="分级") {
                level = dgv_物料组.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString();
                switch (level) {
                    case "A":
                        dgv_物料组.Rows[e.RowIndex].Cells["分级描述"].Value = "4";break;
                    case "B":
                        dgv_物料组.Rows[e.RowIndex].Cells["分级描述"].Value = "3"; break;
                    case "C":
                        dgv_物料组.Rows[e.RowIndex].Cells["分级描述"].Value = "2"; break;
                    case "D":
                        dgv_物料组.Rows[e.RowIndex].Cells["分级描述"].Value = "1"; break;
                    default:break;
                        

                }

            }
        }
        /// <summary>
        /// 导入信息
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button3_Click(object sender, EventArgs e)
        {
            DialogResult dr1 = this.openFileDialog1.ShowDialog();
            if (DialogResult.OK == dr1) //判断是否选择文件  
            {
                string path = this.openFileDialog1.FileName.Trim();
                if (string.IsNullOrEmpty(path))
                {
                    MessageBox.Show("请选择要导入的EXCEL文件。", "信息");
                    return;
                }
                if (!File.Exists(path))  //判断文件是否存在  
                {
                    MessageBox.Show("信息", "找不到对应的Excel文件，请重新选择。");
                    this.button3.Focus();
                    return;
                }
                DataTable excelTbl = ExcelUtil.GetExcelTable(path);  //调用函数获取Excel中的信息  
                if (excelTbl == null)
                {
                    MessageUtil.ShowError("文件中无数据！重新上传");
                    return;
                }

                dgv_物料组.DataSource = excelTbl;
                MessageUtil.ShowTips("导入成功！请点击保存按钮保存导入数据");

                this.button4.Visible = true;
                this.button4.Focus();

            }

            }
        /// <summary>
        /// 保存导入信息
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button4_Click(object sender, EventArgs e)
        {
            bool flag = true;
            GeneralBLL gn = new GeneralBLL();
            MaterialGroupModel materialGroupModel = new MaterialGroupModel();
            for (int i=0;i < dgv_物料组.Rows.Count;i++) {
                materialGroupModel.MtGroupClass = dgv_物料组.Rows[i].Cells["物料组类型"].Value.ToString();
                materialGroupModel.MtGroupId = dgv_物料组.Rows[i].Cells["编号"].Value.ToString();
                materialGroupModel.MtGroupName = dgv_物料组.Rows[i].Cells["名称"].Value.ToString();
                materialGroupModel.MtGroupLevel = dgv_物料组.Rows[i].Cells["分级"].Value.ToString();
                materialGroupModel.MtGroupLevelValue = dgv_物料组.Rows[i].Cells["分级描述"].Value.ToString();
                //materialGroupModel.CGreenValue= dgv_物料组.Rows[i].Cells["认证绿色目标"].Value.ToString();
               // materialGroupModel.CLowValue = dgv_物料组.Rows[i].Cells["认证最低目标"].Value.ToString();
               // materialGroupModel.PGreenValue = dgv_物料组.Rows[i].Cells["绩效绿色目标"].Value.ToString();
                //materialGroupModel.PLowValue = dgv_物料组.Rows[i].Cells["绩效最低目标"].Value.ToString();

                flag = flag&&gn.NewMaterialGroup(materialGroupModel);
            }
            if (flag) {
                MessageUtil.ShowTips("保存成功！");
            }
            this.button4.Visible = false;
        }

        private void pageNext1_Load(object sender, EventArgs e)
        {
            pageNext1.DrawControl(toalNum);
            pageNext1.OnPageChanged += PageNext1_OnPageChanged;
        }

        private void PageNext1_OnPageChanged(object sender, EventArgs e)
        {
            loadData();
        }
        /// <summary>
        /// 加载数据
        /// </summary>
        public void loadData() {
            GeneralBLL gn = new GeneralBLL();
            dgv_物料组.DataSource = gn.GetAllMaterialGroup(this.pageNext1.PageSize, this.pageNext1.PageIndex);
        }

        private void 删除ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            btn_删除_Click(sender, e);
        }
    }
}
