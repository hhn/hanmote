﻿using Lib.Bll.MDBll.CertiFileBll;
using Lib.Common.CommonUtils;
using Lib.Common.MMCException.Bll;
using Lib.Common.MMCException.IDAL;
using Lib.SqlServerDAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MMClient.MD.MT
{
    public partial class CertiTypeForm : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        CertiFileBll certiFileBll = new CertiFileBll();

        public CertiTypeForm()
        {
            InitializeComponent();
        }

        private void CertiTypeForm_Load(object sender, EventArgs e)
        {
            //读取证书信息
            dgv_CertiFileInfo.DataSource = certiFileBll.getCertiFileTypeInfo();

        }

        private void pageTool_Load(object sender, EventArgs e)
        {
            string sql = "select count(*) from TabCertiFileType";
            try
            {
                int i = int.Parse(DBHelper.ExecuteQueryDT(sql).Rows[0][0].ToString());
                int totalNum = i;
                pageTool.DrawControl(totalNum);
                //事件改变调用函数
                pageTool.OnPageChanged += pageTool_OnPageChanged;
            }
            catch (Lib.Common.MMCException.Bll.BllException ex)
            {
                MessageUtil.ShowTips("数据库异常，请稍后重试");
                return;
            }
        }
        private void pageTool_OnPageChanged(object sender, EventArgs e)
        {
            LoadData();
        }
        private void LoadData()
        {
            try
            {
                dgv_CertiFileInfo.DataSource = findConditionalInforByPage(this.pageTool.PageSize, this.pageTool.PageIndex);
            }
            catch (BllException ex)
            {
                MessageUtil.ShowError(ex.Message);
            }
        }
        private object findConditionalInforByPage(int pageSize, int pageIndex)
        {
            try
            {
                string sql = @"SELECT top " + pageSize + " CertiFileID,CertiFileType,CertiFileStorePath from TabCertiFileType  where CertiFileID not in(select top " + pageSize * (pageIndex - 1) + " CertiFileID from TabCertiFileType ORDER BY CertiFileID ASC)ORDER BY CertiFileID";
                return DBHelper.ExecuteQueryDT(sql);
            }
            catch (DBException ex)
            {
                throw new BllException("当前无数据", ex);
            };
        }

        private void btn_刷新_Click(object sender, EventArgs e)
        {
            LoadData();
            pageTool_Load(sender,e);
        }

        private void btn_新条目_Click(object sender, EventArgs e)
        {
            AddCertiTypeForm addCertiTypeForm = new AddCertiTypeForm();
            if (addCertiTypeForm.ShowDialog() == DialogResult.Cancel)
            {
                LoadData();
            }
           
        }

        private void btn_删除_Click(object sender, EventArgs e)
        {
            if (dgv_CertiFileInfo.Columns["choose"].Visible != true)
            {
                MessageUtil.ShowTips("请选择要删除数据！");
                dgv_CertiFileInfo.Columns["choose"].Visible = true;
            }
            else
            {

                int flag = 0;
                foreach (DataGridViewRow r in dgv_CertiFileInfo.Rows)
                {
                    if ((bool)r.Cells["choose"].EditedFormattedValue == true)
                    {
                        flag = certiFileBll.delCertiFileTypeInfo(r.Cells["CertiFileId"].Value.ToString());
                        if (flag > 0)
                        {

                            dgv_CertiFileInfo.Rows.Remove(r);
                            MessageUtil.ShowTips("删除成功！");
                            pageTool_Load(sender, e);
                        }

                    }


                }

                dgv_CertiFileInfo.Columns["choose"].Visible = false;
            }
        }
    }
}
