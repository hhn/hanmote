﻿namespace MMClient.MD.MT
{
    partial class MaterialClass
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MaterialClass));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.cbb_物料名称 = new System.Windows.Forms.ComboBox();
            this.cbb_物料编号 = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dgv_类特性 = new System.Windows.Forms.DataGridView();
            this.cln_特征描述 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cln_特性编码 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cln_值 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.cbb_特性类 = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btn_维护特征 = new System.Windows.Forms.Button();
            this.btn_维护值 = new System.Windows.Forms.Button();
            this.toolStripExecutionMonitorSetting = new System.Windows.Forms.ToolStrip();
            this.toolStripButton7 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton8 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton4 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton6 = new System.Windows.Forms.ToolStripButton();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_类特性)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.toolStripExecutionMonitorSetting.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.cbb_物料名称);
            this.groupBox1.Controls.Add(this.cbb_物料编号);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(7, 70);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(760, 82);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "分类视图";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(1058, 29);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(93, 37);
            this.button1.TabIndex = 4;
            this.button1.Text = "选择物料";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // cbb_物料名称
            // 
            this.cbb_物料名称.Enabled = false;
            this.cbb_物料名称.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbb_物料名称.FormattingEnabled = true;
            this.cbb_物料名称.Location = new System.Drawing.Point(223, 26);
            this.cbb_物料名称.Name = "cbb_物料名称";
            this.cbb_物料名称.Size = new System.Drawing.Size(205, 25);
            this.cbb_物料名称.TabIndex = 3;
            // 
            // cbb_物料编号
            // 
            this.cbb_物料编号.Enabled = false;
            this.cbb_物料编号.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbb_物料编号.FormattingEnabled = true;
            this.cbb_物料编号.Location = new System.Drawing.Point(54, 26);
            this.cbb_物料编号.Name = "cbb_物料编号";
            this.cbb_物料编号.Size = new System.Drawing.Size(163, 25);
            this.cbb_物料编号.TabIndex = 2;
            this.cbb_物料编号.TextChanged += new System.EventHandler(this.cbb_物料编号_TextChanged);
            this.cbb_物料编号.MouseClick += new System.Windows.Forms.MouseEventHandler(this.cbb_物料编号_MouseClick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(9, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "物料";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dgv_类特性);
            this.groupBox2.Location = new System.Drawing.Point(7, 244);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(760, 309);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "常规";
            // 
            // dgv_类特性
            // 
            this.dgv_类特性.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_类特性.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_类特性.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.cln_特征描述,
            this.cln_特性编码,
            this.cln_值});
            this.dgv_类特性.Location = new System.Drawing.Point(9, 21);
            this.dgv_类特性.Name = "dgv_类特性";
            this.dgv_类特性.RowTemplate.Height = 24;
            this.dgv_类特性.Size = new System.Drawing.Size(751, 532);
            this.dgv_类特性.TabIndex = 0;
            // 
            // cln_特征描述
            // 
            this.cln_特征描述.HeaderText = "特征描述";
            this.cln_特征描述.Name = "cln_特征描述";
            this.cln_特征描述.ReadOnly = true;
            // 
            // cln_特性编码
            // 
            this.cln_特性编码.HeaderText = "特性编码";
            this.cln_特性编码.Name = "cln_特性编码";
            this.cln_特性编码.Visible = false;
            // 
            // cln_值
            // 
            this.cln_值.HeaderText = "值";
            this.cln_值.Name = "cln_值";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.cbb_特性类);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.btn_维护特征);
            this.groupBox3.Location = new System.Drawing.Point(16, 158);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(742, 80);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "分配";
            // 
            // cbb_特性类
            // 
            this.cbb_特性类.FormattingEnabled = true;
            this.cbb_特性类.Location = new System.Drawing.Point(62, 35);
            this.cbb_特性类.Name = "cbb_特性类";
            this.cbb_特性类.Size = new System.Drawing.Size(151, 21);
            this.cbb_特性类.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 35);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "特性类";
            // 
            // btn_维护特征
            // 
            this.btn_维护特征.Location = new System.Drawing.Point(1054, 35);
            this.btn_维护特征.Name = "btn_维护特征";
            this.btn_维护特征.Size = new System.Drawing.Size(93, 37);
            this.btn_维护特征.TabIndex = 1;
            this.btn_维护特征.Text = "维护特征";
            this.btn_维护特征.UseVisualStyleBackColor = true;
            this.btn_维护特征.Visible = false;
            this.btn_维护特征.Click += new System.EventHandler(this.btn_维护特征_Click);
            // 
            // btn_维护值
            // 
            this.btn_维护值.Location = new System.Drawing.Point(674, 559);
            this.btn_维护值.Name = "btn_维护值";
            this.btn_维护值.Size = new System.Drawing.Size(93, 37);
            this.btn_维护值.TabIndex = 3;
            this.btn_维护值.Text = "维护特性值";
            this.btn_维护值.UseVisualStyleBackColor = true;
            this.btn_维护值.Visible = false;
            this.btn_维护值.Click += new System.EventHandler(this.btn_维护值_Click);
            // 
            // toolStripExecutionMonitorSetting
            // 
            this.toolStripExecutionMonitorSetting.ImageScalingSize = new System.Drawing.Size(40, 40);
            this.toolStripExecutionMonitorSetting.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton7,
            this.toolStripButton8,
            this.toolStripButton4,
            this.toolStripButton1,
            this.toolStripButton6});
            this.toolStripExecutionMonitorSetting.Location = new System.Drawing.Point(0, 0);
            this.toolStripExecutionMonitorSetting.Name = "toolStripExecutionMonitorSetting";
            this.toolStripExecutionMonitorSetting.Size = new System.Drawing.Size(1282, 64);
            this.toolStripExecutionMonitorSetting.TabIndex = 19;
            this.toolStripExecutionMonitorSetting.Text = "toolStrip1";
            // 
            // toolStripButton7
            // 
            this.toolStripButton7.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton7.Image")));
            this.toolStripButton7.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton7.Name = "toolStripButton7";
            this.toolStripButton7.Size = new System.Drawing.Size(60, 61);
            this.toolStripButton7.Text = "选择物料";
            this.toolStripButton7.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton7.Click += new System.EventHandler(this.toolStripButton7_Click);
            // 
            // toolStripButton8
            // 
            this.toolStripButton8.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton8.Name = "toolStripButton8";
            this.toolStripButton8.Size = new System.Drawing.Size(36, 61);
            this.toolStripButton8.Text = "关闭";
            this.toolStripButton8.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton8.Click += new System.EventHandler(this.toolStripButton8_Click);
            // 
            // toolStripButton4
            // 
            this.toolStripButton4.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton4.Name = "toolStripButton4";
            this.toolStripButton4.Size = new System.Drawing.Size(36, 61);
            this.toolStripButton4.Text = "确定";
            this.toolStripButton4.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton4.Click += new System.EventHandler(this.toolStripButton4_Click);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(44, 61);
            this.toolStripButton1.Text = "分类";
            this.toolStripButton1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // toolStripButton6
            // 
            this.toolStripButton6.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton6.Image")));
            this.toolStripButton6.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton6.Name = "toolStripButton6";
            this.toolStripButton6.Size = new System.Drawing.Size(60, 61);
            this.toolStripButton6.Text = "维护特征";
            this.toolStripButton6.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton6.Click += new System.EventHandler(this.toolStripButton6_Click);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "特征描述";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 354;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "特性编码";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.Visible = false;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "值";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.Width = 354;
            // 
            // MaterialClass
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1282, 603);
            this.Controls.Add(this.toolStripExecutionMonitorSetting);
            this.Controls.Add(this.btn_维护值);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "MaterialClass";
            this.Text = "物料主数据分类视图";
            this.Load += new System.EventHandler(this.MaterialClassfy_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_类特性)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.toolStripExecutionMonitorSetting.ResumeLayout(false);
            this.toolStripExecutionMonitorSetting.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.ComboBox cbb_物料编号;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView dgv_类特性;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btn_维护特征;
        private System.Windows.Forms.Button btn_维护值;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ComboBox cbb_特性类;
        private System.Windows.Forms.Label label2;
        public System.Windows.Forms.ComboBox cbb_物料名称;
        private System.Windows.Forms.DataGridViewTextBoxColumn cln_特征描述;
        private System.Windows.Forms.DataGridViewTextBoxColumn cln_特性编码;
        private System.Windows.Forms.DataGridViewTextBoxColumn cln_值;
        private System.Windows.Forms.ToolStrip toolStripExecutionMonitorSetting;
        private System.Windows.Forms.ToolStripButton toolStripButton7;
        private System.Windows.Forms.ToolStripButton toolStripButton8;
        private System.Windows.Forms.ToolStripButton toolStripButton4;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.ToolStripButton toolStripButton6;
    }
}