﻿using Lib.Common.CommonUtils;
using Lib.SqlServerDAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Lib.Model.MD.MT;
using Lib.Model.MD;
using System.Data.SqlClient;

namespace MMClient.MD.MT
{
    public partial class MTPerformanceModel : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        private string userName = SingleUserInstance.getCurrentUserInstance().Username;
        private string userId = SingleUserInstance.getCurrentUserInstance().User_ID;
        private PerformanceModel performanceModel = new PerformanceModel();
        
        public MTPerformanceModel()
        {
            InitializeComponent();
        }

        private void MTPerformanceModel_Load(object sender, EventArgs e)
        {
            try
            {
                //加载基本信息
                LoadBaseIfo();
                //加载控件
                loadExistedModel();
            }
            catch (Exception)
            {

                MessageUtil.ShowError("数据加载错误！");
            }
           
            
        }
        /// <summary>
        /// 初始化信息
        /// </summary>
        private void LoadBaseIfo()
        {
            //业务负责人
            TB_person.Text = userName;
            //采购组织
            DataTable dt = GetPurInfo(userId);
            if (dt.Rows.Count != 0) {
                TB_purOrgName.Text = dt.Rows[0]["purName"].ToString();
                TB_purOrgName.Tag = dt.Rows[0]["purId"].ToString();
            }
            //加载物料类型
            CB_materialType.DisplayMember = "Bigclassfy_Name";
            CB_materialType.ValueMember = "ClassfyID";
            CB_materialType.DataSource =GetMaterialTypeInfo();
            this.CB_modelType.SelectedIndex = 0;
            //加载主标准
            loadMainStandard();
            


        }
        /// <summary>
        /// 加载主标准
        /// </summary>
        private void loadMainStandard()
        {
            string str = @"SELECT Description as mainStandardText,EvalItemCode as mainStandardId FROM [dbo].[TabEvalItem]";
            DataTable dt = DBHelper.ExecuteQueryDT(str);
            dgv_MainStandard.AutoGenerateColumns = false;
            dgv_MainStandard.DataSource = dt;
            dt =getExistedMainModel(this.CB_materialType.SelectedValue.ToString(), this.CB_modelType.Text);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                for (int j = 0; j < dgv_MainStandard.Rows.Count; j++)
                {
                    if (dt.Rows[i]["EvalItemCode"].ToString().Equals(dgv_MainStandard.Rows[j].Cells["mainStandardId"].Value.ToString())) {
                        dgv_MainStandard.Rows[j].Cells["isCheckedMain"].Value = true;
                        dgv_MainStandard.Rows[j].Cells["rate"].Value = dt.Rows[i]["mainStandardRate"].ToString();
                        break;
                    }

                }
            }



        }

        //获取物料类型信息
        private DataTable GetMaterialTypeInfo()
        {
            string str = @"SELECT Bigclassfy_Name,ClassfyID FROM [dbo].[Bigclassfy]";
            DataTable dt = DBHelper.ExecuteQueryDT(str);
            return dt;
        }

        /// <summary>
        /// 查询公司信息
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        private DataTable GetPurInfo(string userId)
        {
            string str = @"SELECT org.Organization_ID as purId ,org.Organization_Name as purName  from [dbo].[Hanmote_Base_User] usr,Base_Organization org where usr.User_ID='"+userId+"'  and org.Organization_ID=usr.User_ID";
            DataTable dt = DBHelper.ExecuteQueryDT(str);
            return dt;
        }

        private void CB_modelType_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(CB_modelType, "01默认模板\n02自定义模板");
        }
        
        /// <summary>
        /// 加载次标准
        /// </summary>
        private void loadSecondStandard(string mainStandardId)
        {
            string str = @"select SubStandardCode,SubStandardDes from TabSubStandard where MainStandardCode='"+ mainStandardId + "'";
            DataTable dt = DBHelper.ExecuteQueryDT(str);
            dgv_secondStandard.AutoGenerateColumns = false;
            dgv_secondStandard.DataSource = dt;
            dt = getExistedSecondModel(this.CB_materialType.SelectedValue.ToString(), this.CB_modelType.Text, mainStandardId);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                for (int j = 0; j < dgv_secondStandard.Rows.Count; j++)
                {
                    if (dt.Rows[i]["secondaryStandard"].ToString().Equals(dgv_secondStandard.Rows[j].Cells["secondStandardId"].Value.ToString()))
                    {
                        dgv_secondStandard.Rows[j].Cells["isCheck"].Value = true;
                        dgv_secondStandard.Rows[j].Cells["sencondRate"].Value = dt.Rows[i]["secondaryStandardRate"].ToString();
                        break;
                    }

                }
            }




        }

        private void dgv_MainStandard_SelectionChanged(object sender, EventArgs e)
        {
            //加载次标准
            try
            {
                if(dgv_MainStandard.CurrentRow!=null)
                loadSecondStandard(dgv_MainStandard.CurrentRow.Cells["mainStandardId"].Value.ToString());
            }
            catch (Exception)
            {

                MessageUtil.ShowError("次标准加载失败");
            }
        }
        /// <summary>
        /// 保存数据
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            saveAllModel();
        }
        /// <summary>
        /// 验证输入
        /// </summary>
        private bool ValidateInput()
        {
            //验证主标准
            if (dgv_secondStandard.Rows.Count == 0)
            {
                MessageUtil.ShowWarning("无主标准！无法建立模板");
                return false;
            }
            if (this.CB_materialType.SelectedValue == null) {
                MessageUtil.ShowWarning("请选中物料类型");
                return false;
            };
            if (dgv_MainStandard.CurrentRow == null|| dgv_MainStandard.CurrentRow.Cells["rate"].Value==null)
            {
                MessageUtil.ShowWarning("请输入主标准权重");
                dgv_secondStandard.CurrentCell.Value = false;
                dgv_secondStandard.EndEdit();
                return false;
            }
            //验证次标准
            if (dgv_secondStandard.Rows.Count == 0) {
                MessageUtil.ShowWarning("无次标准！无法建立模板");
                return false; 
            }

            return true;
            
        }

        /// <summary>
        /// 保存数据
        /// </summary>
        /// <param name="performanceModel"></param>
        private bool SaveModel(PerformanceModel performanceModel)
        {
            string str = @"IF EXISTS ( SELECT * FROM Tab_PerformanceModel WHERE materialTypeId = @materialTypeId 
	                            AND mainStandardId = @mainStandardId
	                            AND modelType = @modelType 
	                            AND secondaryStandard = @secondaryStandardRate ) DELETE 
                            FROM
	                            Tab_PerformanceModel 
                            WHERE
	                            materialTypeId = @materialTypeId 
	                            AND mainStandardId = @mainStandardId
	                            AND modelType = @modelType 
	                            AND secondaryStandard = @secondaryStandardRate ELSE INSERT INTO Tab_PerformanceModel ( materialTypeId, mainStandardId, secondaryStandard, mainStandardRate, secondaryStandardRate, modelType, createPerson, createTime )
                            VALUES
	                            (
	                            @materialTypeId,@mainStandardId,@secondaryStandard,@mainStandardRate,@secondaryStandardRate,@modelType,@createPerson,
	                            GETDATE())";

            SqlParameter[] sqlParamater = new SqlParameter[] {
                new SqlParameter("@materialTypeId",performanceModel.MaterialType1),
                new SqlParameter("@mainStandardId",performanceModel.MainEvalFactor1.Code),
                new SqlParameter("@mainStandardRate",performanceModel.MainEvalFactor1.Rate),
                new SqlParameter("@secondaryStandard",performanceModel.SubEvalFactor1.SubCode),
                new SqlParameter("@secondaryStandardRate",performanceModel.SubEvalFactor1.Rate),
                new SqlParameter("@modelType",performanceModel.ModelType),
                new SqlParameter("@createPerson",performanceModel.Creator)
            };
            int flag = DBHelper.ExecuteNonQuery(str,sqlParamater);

            return true;
        }
        /// <summary>
        /// 加载已经存在的模板信息
        /// </summary>
        /// <param name="materailTypeId"></param>
        public void loadExistedModel() {

            this.tabPage2.Controls.Clear();

            DataTable mainModel = getExistedMainModel(this.CB_materialType.SelectedValue.ToString(),this.CB_modelType.Text);
            DataTable secondModel = null;
            for (int i = 0; i < mainModel.Rows.Count; i++)
            {
                GroupBox gbox= AddGroupBox(i, mainModel);
                //加载次标准
                secondModel = getExistedSecondModel(this.CB_materialType.SelectedValue.ToString(), this.CB_modelType.Text,mainModel.Rows[i]["EvalItemCode"].ToString());

                for (int j = 0; j < secondModel.Rows.Count; j++)
                {
                    AddTxt(gbox,j, secondModel.Rows[j]);
                }


            }
        }
        //获取次标准
        private DataTable getExistedSecondModel(string materialType, string modelType, string mainModel)
        {
            string str = @"select DISTINCT model.secondaryStandard,sub.SubStandardDes,model.secondaryStandardRate from Tab_PerformanceModel model,TabSubStandard sub   where model.materialTypeId='" + materialType + "' and model.modelType='"+ modelType + "' and  model.mainStandardId='"+ mainModel + "' and sub.SubStandardCode=model.secondaryStandard";
            DataTable dt = DBHelper.ExecuteQueryDT(str);
            return dt;
        }

        private DataTable getExistedMainModel(string materailTypeId,string modelType)
        {
            string str = @"SELECT DISTINCT eval.EvalItemCode,eval.Description,model.mainStandardRate FROM [dbo].[Tab_PerformanceModel] model,TabEvalItem Eval where model.mainStandardId=eval.EvalItemCode and materialTypeId='" + materailTypeId + "' and modelType='"+ modelType + "'";
            DataTable dt = DBHelper.ExecuteQueryDT(str);
            return dt;
        }




        //添加控件
        public GroupBox AddGroupBox(int i,DataTable mainModel)
        {
                string name = mainModel.Rows[i]["Description"].ToString();
                GroupBox gbox = new GroupBox();
                gbox.Name = name;
                gbox.Text = i+1+".主标准："+name+"   权重:"+ mainModel.Rows[i]["mainStandardRate"].ToString();
                if (i == 0)
                {
                    gbox.Location = new Point(32, 20 + i * 150);
                }
                else {
                    string upName = mainModel.Rows[i - 1]["Description"].ToString();
                    Control g = this.tabPage2.Controls.Find(upName, false)[0];
                    int y = g.Location.Y+g.Height;
                    gbox.Location = new Point(32, y+20);
                }
                gbox.Width = this.tabPage2.Width;
                gbox.Height = 50;
               
                this.tabPage2.Controls.Add(gbox);
                return gbox;


        }
        //添加文本控件
        public void AddTxt(GroupBox gb, int i, DataRow dataRow)
        {
                string name = "txt";
                
                TextBox txt = new TextBox();
                txt.Name = dataRow["SubStandardDes"].ToString();
                txt.Text = i+1+".次标准："+dataRow["SubStandardDes"].ToString()+"     权重："+ dataRow["secondaryStandardRate"].ToString();
                txt.BorderStyle = BorderStyle.None;
  
                txt.Width = gb.Width-20;
                gb.Height += 20;
                txt.Location = new Point(20, 20 + i * 30);
                gb.Controls.Add(txt);
        }

        private void CB_materialType_SelectedIndexChanged(object sender, EventArgs e)
        {
            //加载控件
            loadExistedModel();
            //加载主标准
            loadMainStandard();
        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.tabControl1.SelectedIndex == 1) {
                loadExistedModel();
            }
        }

        private void CB_modelType_SelectedIndexChanged(object sender, EventArgs e)
        {
            loadExistedModel();
            //加载主标准
            loadMainStandard();
        }

        private void dgv_MainStandard_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1&&dgv_MainStandard.Columns[e.ColumnIndex].Name.Equals("isCheckedMain")) {

                if ((bool)dgv_MainStandard.CurrentCell.EditedFormattedValue != true)
                {
                    if (MessageUtil.ShowYesNoAndTips("是否保存更改！") == DialogResult.Yes)
                    {
                        try
                        {
                            deleteMainModel(this.CB_materialType.SelectedValue.ToString(), this.CB_modelType.Text, dgv_MainStandard.CurrentRow.Cells["mainStandardId"].Value.ToString());
                            loadSecondStandard(dgv_MainStandard.CurrentRow.Cells["mainStandardId"].Value.ToString());
                        }
                        catch (Exception ex)
                        {
                            MessageUtil.ShowError(ex.Message);
                        }
                    } 
                }
                else {

                    if (dgv_MainStandard.CurrentRow.Cells["rate"].Value == null)
                    {
                        MessageUtil.ShowTips("请输入主标准的权重");
                        dgv_MainStandard.CurrentCell.Value = false;
                        dgv_MainStandard.EndEdit();
                    }
                    
                }
            }
        }
     /// <summary>
     /// 
     /// </summary>
     /// <param name="materialType">物料类型编号</param>
     /// <param name="modelType">模板类型，01：自定义，02：默认</param>
     /// <param name="mainStandardId">主标准编号</param>
        private void deleteMainModel(string materialType, string modelType, string mainStandardId)
        {
            string str = @"delete  from Tab_PerformanceModel where materialTypeId='"+ materialType + "' and mainStandardId='"+ mainStandardId + "' and modelType='"+ modelType + "'";
            DBHelper.ExecuteNonQuery(str);
        }

        private void modifyMainModel(string v, string text)
        {
            throw new NotImplementedException();
        }

        private void dgv_secondStandard_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgv_secondStandard.Columns[e.ColumnIndex].Name.Equals("isCheck")&&e.RowIndex!=-1)
            {
                if ((bool)dgv_secondStandard.CurrentCell.EditedFormattedValue != true)
                {
                    if (MessageUtil.ShowYesNoAndTips("是否保存更改！") == DialogResult.Yes)
                    {
                        try
                        {
                            deleteSecondModel(this.CB_materialType.SelectedValue.ToString(), this.CB_modelType.Text, dgv_MainStandard.CurrentRow.Cells["mainStandardId"].Value.ToString(), dgv_secondStandard.CurrentRow.Cells["secondStandardId"].Value.ToString());

                        }
                        catch (Exception ex)
                        {
                            MessageUtil.ShowError(ex.Message);
                        }
                    }
                    else
                    {

                        dgv_secondStandard.CurrentCell.Value = true;
                        dgv_secondStandard.EndEdit();
                    }
                }
                else {
                    if (dgv_secondStandard.CurrentRow.Cells["sencondRate"].Value == null)
                    {
                        MessageUtil.ShowTips(dgv_secondStandard.CurrentRow.Cells["SubStandardDes"].Value + "未输入权重信息！");
                        dgv_secondStandard.CurrentCell.Value = false;
                        dgv_secondStandard.EndEdit();
                    }
                    else {
                        saveSingleModel();
                    }
                    
                }
            }
        }

        private void saveSingleModel()
        {
            performanceModel.MainEvalFactor1 = new MainEvalFactor();
            performanceModel.SubEvalFactor1 = new SubEvalFactor();
            try
            {
                //验证输入
                if (!ValidateInput())
                {
                    return;
                }
                DataGridViewRow item = dgv_secondStandard.CurrentRow;
                if (item.Cells["sencondRate"].Value == null)
                {
                    MessageUtil.ShowTips(item.Cells["SubStandardDes"] + "未输入权重信息！");
                    dgv_secondStandard.CurrentCell.Value = false;
                    dgv_secondStandard.EndEdit();
                    return;
                }
                else {
                    //保存数据
                    performanceModel.MaterialType1 = this.CB_materialType.SelectedValue.ToString();
                    performanceModel.Creator = userId;
                    performanceModel.MainEvalFactor1.Code = dgv_MainStandard.CurrentRow.Cells["mainStandardId"].Value.ToString();
                    performanceModel.MainEvalFactor1.Desc = dgv_MainStandard.CurrentRow.Cells["mainStandardText"].Value.ToString();
                    performanceModel.MainEvalFactor1.Rate = dgv_MainStandard.CurrentRow.Cells["rate"].Value.ToString();
                    performanceModel.ModelType = this.CB_modelType.Text;
                    performanceModel.SubEvalFactor1.Rate = item.Cells["sencondRate"].Value.ToString();
                    performanceModel.SubEvalFactor1.SubCode = item.Cells["secondStandardId"].Value.ToString();
                    performanceModel.SubEvalFactor1.Desc = item.Cells["SubStandardDes"].Value.ToString();
                    SaveModel(performanceModel);
                }
                
            }
            catch (Exception ex)
            {

                MessageUtil.ShowError("保存失败！");
            }


        }

        private void saveAllModel()
        {
            performanceModel.MainEvalFactor1 = new MainEvalFactor();
            performanceModel.SubEvalFactor1 = new SubEvalFactor();
            try
            {
                //验证输入
                if (!ValidateInput())
                {
                    return;
                }

                //保存数据
                performanceModel.MaterialType1 = this.CB_materialType.SelectedValue.ToString();
                performanceModel.Creator = userId;
                performanceModel.MainEvalFactor1.Code = dgv_MainStandard.CurrentRow.Cells["mainStandardId"].Value.ToString();
                performanceModel.MainEvalFactor1.Desc = dgv_MainStandard.CurrentRow.Cells["mainStandardText"].Value.ToString();
                performanceModel.MainEvalFactor1.Rate = dgv_MainStandard.CurrentRow.Cells["rate"].Value.ToString();
                performanceModel.ModelType = this.CB_modelType.Text;
                foreach (DataGridViewRow item in dgv_secondStandard.Rows)
                {

                    if ((bool)item.Cells["isCheck"].EditedFormattedValue == true)
                    {
                        performanceModel.SubEvalFactor1.SubCode = item.Cells["secondStandardId"].Value.ToString();
                        performanceModel.SubEvalFactor1.Desc = item.Cells["SubStandardDes"].Value.ToString();
                        if (item.Cells["sencondRate"].Value == null)
                        {
                            MessageUtil.ShowTips(performanceModel.SubEvalFactor1.Desc + "未输入权重信息！");
                            return;
                        }
                        performanceModel.SubEvalFactor1.Rate = item.Cells["sencondRate"].Value.ToString();


                        SaveModel(performanceModel);
                    }


                }
                MessageUtil.ShowTips("保存成功！");
            }
            catch (Exception ex)
            {

                MessageUtil.ShowError("保存失败！");
            }
        }

        /// <summary>
        /// 删除次标准
        /// </summary>
        /// <param name="materailType"></param>
        /// <param name="modelType"></param>
        /// <param name="mainStandardId"></param>
        /// <param name="secondStandardId"></param>
        private void deleteSecondModel(string materailType, string modelType, string mainStandardId, string secondStandardId)
        {
            string str = @"delete  from Tab_PerformanceModel where materialTypeId='"+ materailType + "' and mainStandardId='"+ mainStandardId + "' and modelType='"+ modelType + "' and secondaryStandard='"+ secondStandardId + "'";
            DBHelper.ExecuteNonQuery(str);
        }

        private void dgv_MainStandard_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            if (this.dgv_MainStandard.IsCurrentCellDirty) //有未提交的更//改
            {
                this.dgv_MainStandard.CommitEdit(DataGridViewDataErrorContexts.Commit);
            }
        }

        private void dgv_secondStandard_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            if (this.dgv_secondStandard.IsCurrentCellDirty) //有未提交的更//改
            {
                this.dgv_secondStandard.CommitEdit(DataGridViewDataErrorContexts.Commit);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
