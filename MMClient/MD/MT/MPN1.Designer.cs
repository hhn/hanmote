﻿namespace MMClient.MD.MT
{
    partial class MPN1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btn_确定 = new System.Windows.Forms.Button();
            this.tbx_制造商 = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tbx_特性描述 = new System.Windows.Forms.TextBox();
            this.tbx_产地 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tbx_制造商物料号 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cbb_制造商 = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cbb_物料编号 = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // supplierInforGroupBox
            // 
            this.groupBox1.Controls.Add(this.btn_确定);
            this.groupBox1.Controls.Add(this.tbx_制造商);
            this.groupBox1.Controls.Add(this.groupBox3);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this.tbx_制造商物料号);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.cbb_制造商);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.cbb_物料编号);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(4, 3);
            this.groupBox1.Name = "supplierInforGroupBox";
            this.groupBox1.Size = new System.Drawing.Size(1266, 588);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "MPN";
            // 
            // btn_确定
            // 
            this.btn_确定.Location = new System.Drawing.Point(976, 115);
            this.btn_确定.Name = "btn_确定";
            this.btn_确定.Size = new System.Drawing.Size(71, 31);
            this.btn_确定.TabIndex = 9;
            this.btn_确定.Text = "确定";
            this.btn_确定.UseVisualStyleBackColor = true;
            this.btn_确定.Click += new System.EventHandler(this.btn_确定_Click);
            // 
            // tbx_制造商
            // 
            this.tbx_制造商.Location = new System.Drawing.Point(262, 77);
            this.tbx_制造商.Name = "tbx_制造商";
            this.tbx_制造商.Size = new System.Drawing.Size(137, 22);
            this.tbx_制造商.TabIndex = 8;
            // 
            // groupBox3
            // 
            this.groupBox3.Location = new System.Drawing.Point(4, 329);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(1256, 237);
            this.groupBox3.TabIndex = 7;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "其它";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.tbx_特性描述);
            this.groupBox2.Controls.Add(this.tbx_产地);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Location = new System.Drawing.Point(5, 150);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1255, 173);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "基本信息";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(1, 102);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(64, 17);
            this.label5.TabIndex = 3;
            this.label5.Text = "特性描述";
            // 
            // tbx_特性描述
            // 
            this.tbx_特性描述.Location = new System.Drawing.Point(130, 99);
            this.tbx_特性描述.Name = "tbx_特性描述";
            this.tbx_特性描述.Size = new System.Drawing.Size(720, 22);
            this.tbx_特性描述.TabIndex = 2;
            // 
            // tbx_产地
            // 
            this.tbx_产地.Location = new System.Drawing.Point(130, 50);
            this.tbx_产地.Name = "tbx_产地";
            this.tbx_产地.Size = new System.Drawing.Size(121, 22);
            this.tbx_产地.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(1, 53);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(36, 17);
            this.label4.TabIndex = 0;
            this.label4.Text = "产地";
            // 
            // tbx_制造商物料号
            // 
            this.tbx_制造商物料号.Location = new System.Drawing.Point(135, 115);
            this.tbx_制造商物料号.Name = "tbx_制造商物料号";
            this.tbx_制造商物料号.Size = new System.Drawing.Size(205, 22);
            this.tbx_制造商物料号.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 115);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(92, 17);
            this.label3.TabIndex = 4;
            this.label3.Text = "制造商物料号";
            // 
            // cbb_制造商
            // 
            this.cbb_制造商.FormattingEnabled = true;
            this.cbb_制造商.Location = new System.Drawing.Point(135, 75);
            this.cbb_制造商.Name = "cbb_制造商";
            this.cbb_制造商.Size = new System.Drawing.Size(121, 24);
            this.cbb_制造商.TabIndex = 3;
            this.cbb_制造商.SelectedIndexChanged += new System.EventHandler(this.cbb_制造商_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 78);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "制造商";
            // 
            // cbb_物料编号
            // 
            this.cbb_物料编号.FormattingEnabled = true;
            this.cbb_物料编号.Location = new System.Drawing.Point(135, 37);
            this.cbb_物料编号.Name = "cbb_物料编号";
            this.cbb_物料编号.Size = new System.Drawing.Size(264, 24);
            this.cbb_物料编号.TabIndex = 1;
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 40);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(64, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "物料信息";
            // 
            // MPN1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1282, 603);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "MPN1";
            this.Text = "制造商物料管理";
            this.Load += new System.EventHandler(this.MPN1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox tbx_制造商物料号;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cbb_制造商;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cbb_物料编号;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbx_制造商;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tbx_特性描述;
        private System.Windows.Forms.TextBox tbx_产地;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btn_确定;
    }
}