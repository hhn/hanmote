﻿using System;
using System.Data;
using System.Windows.Forms;
using Lib.SqlServerDAL;
using Lib.Common.CommonUtils;
using Lib.Common.MMCException.IDAL;
using Lib.Common.MMCException.Bll;

namespace MMClient.MD.MT
{
    public partial class MTEstablish1 : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        private int outSupport = 0;
        private int Flag = 0;
        ImportMaterialBaseDataFrom importMaterialBaseDataFrom = null;
        public MTEstablish1()
        {
            InitializeComponent();
            LoadData();
        }

        private void toolStripButton4_Click(object sender, EventArgs e)
        {
            outSupport = 0;
            if (isNullOrEmpty())
            {
                MessageBox.Show("数据不完整");
                return;
            }
            if(checkOnlyOne())
            {
                MessageBox.Show("已存在该条件下的物料");
                this.txtMaterialName.Text = "";
                return;
            }
            try
            {
                //得到物料起始号
                string MaterialTypeID = this.cbMaterialType.SelectedValue.ToString();
                string sql = "select outSupport ,innnerStart,innnerEnd,outterRangeStart,outterRangeEnd from Bigclassfy WHERE ClassfyID='" + MaterialTypeID + "'";
                DataTable dt = DBHelper.ExecuteQueryDT(sql);
                if (dt.Rows.Count <= 0)
                {
                    MessageBox.Show("无数据");
                    return;
                }
                string startRange = dt.Rows[0][1].ToString();
                startRange = addOne("0" + startRange);
                string Material_ID = MaterialTypeID + startRange;
               //检查是否存在
                bool isCheck = checkExsis(Material_ID);
                while (isCheck)
                {
                    Material_ID = addOne(Material_ID);
                    isCheck = checkExsis(Material_ID);
                }
                if(Flag==0)
                {
                    MessageBox.Show("请先选择物料组");
                    return;
                }
                sql = "INSERT INTO Material (Material_ID,Material_Name,Material_Group,Material_Type,Industry_Category,isOut) VALUES('" + Material_ID + "','" + this.txtMaterialName.Text.ToString() + "','" + this.cbMaterialGroup.SelectedValue.ToString() + "','" + this.cbMaterialType.SelectedValue.ToString() + "','" + this.cbApplicationFiled.SelectedValue.ToString() + "','0') ";
                DBHelper.ExecuteQueryDS(sql);
                MessageBox.Show("保存成功");
                this.txtMaterialName.Text = "";
            }
            catch(Exception ex)
            {
                MessageBox.Show("保存失败");
                return;
            }

        }

        private bool checkExsis(string material_ID)
        {
            string checksql = "select * from Material where Material_ID = '" + material_ID + "'  AND isOut = '0'";
            DataTable dataTable = DBHelper.ExecuteQueryDT(checksql);
            //已存在这样的物料编号则＋1
            if (dataTable.Rows.Count > 0 && !string.IsNullOrEmpty(dataTable.Rows[0][0].ToString()))
            {
                return true;
            }
            return false;
        }

        private string addOne(string str)
        {
            int index = 0;
            int newInt = 0; ;
            for (int i = str.Length - 1; i > 0; i--)
            {
                if (str[i] != '9')
                {
                    index = i;
                    break;
                }
            }
            newInt = Convert.ToInt32(str.Substring(index)) + 1;
            string Newstr = str.Substring(0, index) + newInt.ToString();
            return Newstr;
        }

        private bool checkOnlyOne()
        {
            try
            {
                string sql = "SELECT Material_Name from Material where Material_Type = '" + this.cbMaterialType.SelectedValue.ToString()+ "' AND Material_Name = '"+this.txtMaterialName.Text.ToString().Trim() + "'";
                DataTable dt = DBHelper.ExecuteQueryDT(sql);
                if(dt.Rows.Count>0)
                {
                    return true;
                }
            }
            catch (DBException ex)
            {
                MessageBox.Show("检查失败");
            }
            return false;
        }

        private bool isNullOrEmpty()
        {
            if (this.txtMaterialName.Text == "" || this.cbApplicationFiled.Text == ""
                || this.cbMaterialGroup.Text == "" || this.cbMaterialType.Text=="")
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// zhaungzaishuju 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MTEstablish1_Load(object sender, EventArgs e)
        {
            string type = "SELECT ClassfyID as id , Bigclassfy_Name as name from Bigclassfy";
            
            string field = "SELECT Industry_ID as id,Industry_Category as name from Industry_Category";
            this.cbMaterialType.DataSource = DBHelper.ExecuteQueryDT(type);
            this.cbMaterialType.DisplayMember = "name";
            this.cbMaterialType.ValueMember = "id";
           
            this.cbApplicationFiled.DataSource = DBHelper.ExecuteQueryDT(field);
            this.cbApplicationFiled.DisplayMember = "name";
            this.cbApplicationFiled.ValueMember = "id";

        }

        private void toolStripButton7_Click(object sender, EventArgs e)
        {
            this.txtMaterialName.Text = "";
            MTEstablish1_Load(sender,e);
            LoadData();
            pageTool_Load(sender, e);
            outSupport = 0;//默认内部排号
        }

        private void BtnoutImport_Click(object sender, EventArgs e)
        {
            outSupport = 1;
            if (importMaterialBaseDataFrom == null || importMaterialBaseDataFrom.IsDisposed)
            {
                importMaterialBaseDataFrom = new ImportMaterialBaseDataFrom();
            }
            importMaterialBaseDataFrom.Show();
        }

        private void toolStripButton8_Click(object sender, EventArgs e)
        {
            this.Close();

        }

        private void pageTool_OnPageChanged(object sender, EventArgs e)
        {
            LoadData();
        }

        private void pageTool_Load(object sender, EventArgs e)
        {
            string sql = "SELECT count(*) FROM [Material]";
            try
            {
                int i = int.Parse(DBHelper.ExecuteQueryDT(sql).Rows[0][0].ToString());
                int totalNum = i;
                pageTool.DrawControl(totalNum);
                //事件改变调用函数
                pageTool.OnPageChanged += pageTool_OnPageChanged;
            }
            catch (Lib.Common.MMCException.Bll.BllException ex)
            {
                MessageUtil.ShowTips("数据库异常，请稍后重试");
                return;
            }
        }
        private void LoadData()
        {
            try
            {
                dgv_MaterialBaseData.DataSource = findConditionalInforByPage(this.pageTool.PageSize, this.pageTool.PageIndex);
            }
            catch (BllException ex)
            {
                MessageUtil.ShowError(ex.Message);
            }
        }
        private object findConditionalInforByPage(int pageSize, int pageIndex)
        {
            try
            {
                string sql = @"SELECT top " + pageSize + " Material_ID as 物料编号 ,Material_Name as 物料名称, Material_Type as 物料类型,Material_Group as 物料组 ,Industry_Category as 行业领域  from Material  where Material_ID not in(select top " + pageSize * (pageIndex - 1) + " Material_ID from Material ORDER BY Material_ID ASC)ORDER BY Material_ID";
                return DBHelper.ExecuteQueryDT(sql);
            }
            catch (DBException ex)
            {
                throw new BllException("当前无数据", ex);
            };
        }
        /// <summary>
        /// 生成模板先
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnTools_Click(object sender, EventArgs e)
        {
            string path = "C:\\物料导入模板.xlsx";
            DataTable table = new DataTable();
            table.Columns.Add("物料名称");
            table.Columns.Add("物料组编号");
            table.Columns.Add("物料类型编号");
            table.Columns.Add("行业领域");
            DialogResult dr;
            dr = MessageBox.Show("是否已有模板?", "检查", MessageBoxButtons.YesNoCancel,
                     MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2);
            if (dr == DialogResult.Yes)
            {
                this.BtnoutImport.Visible = true;
                return;
            }
            else if (dr == DialogResult.No)
            {
                try
                {
                    FileImportOrExport.TableToExcel(table, path);
                    this.btnTools.Enabled = false;
                    this.BtnoutImport.Visible = true;
                    this.LbpathShow.Text = "模板已存在路径： " + path;
                    this.LbpathShow.Visible = true;
                }
                catch (Exception ex)
                { 
                    MessageBox.Show("模板生成失败");
                }
            }
        }
        /// <summary>
        /// 填充物料 
        /// </summary>g
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cbMaterialType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string group = " SELECT Material_Group as id , Description as name  from Material_Group where Category = '" + this.cbMaterialType.SelectedValue.ToString() + "'";
                this.cbMaterialGroup.DataSource = DBHelper.ExecuteQueryDT(group);
                this.cbMaterialGroup.DisplayMember = "name";
                this.cbMaterialGroup.ValueMember = "id";
            }
            catch (DBException ex)
            {
                MessageBox.Show("数据加载失败");
            }
        }

        private void cbMaterialGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            Flag = 1;
        }

        private void toolStripExecutionMonitorSetting_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void dgv_MaterialBaseData_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
