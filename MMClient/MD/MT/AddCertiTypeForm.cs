﻿using Lib.Bll.MDBll.CertiFileBll;
using Lib.Common.CommonUtils;
using Lib.Model.MD;
using Lib.SqlServerDAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MMClient.MD.MT
{
    public partial class AddCertiTypeForm : Form
    {
        CertiFileBll certiFileBll = new CertiFileBll();
        public AddCertiTypeForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            CertifileModel certifileModel = new CertifileModel();
            certifileModel.CertiFileTypeId = this.TB_Id.Text.ToString();
            certifileModel.CertiFileTypeName = this.TB_Name.Text.ToString();
            certifileModel.CertiFileStorePath = this.TB_Path.Text.ToString();


            string sqlText = @"INSERT into  TabCertiFileType(CertiFileID,CertiFileType,CertiFileStorePath) values('" + certifileModel.CertiFileTypeId + "','" + certifileModel.CertiFileTypeName + "','" + certifileModel.CertiFileStorePath + "')";
            try
            {
                int flag = DBHelper.ExecuteNonQuery(sqlText);
                if (flag > 0)
                {

                    if (MessageUtil.ShowOKCancelAndQuestion("保存成功！是否继续添加？") == DialogResult.Cancel)
                    {
                        this.Close();

                    }
                    else
                    {
                         TB_Id.Text = "";
                         TB_Name.Text = "";
                         TB_Path.Text = "";
                    }
                }
            }
            catch (Exception ex)
            {

                MessageUtil.ShowTips("保存失败！"+ex.Message);
            }

            
            

        }
    }
}
