﻿using Lib.Common.CommonUtils;
using Lib.Common.MMCException.Bll;
using Lib.Common.MMCException.IDAL;
using Lib.SqlServerDAL;
using MMClient.MD.MT.StrategInfo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MMClient.MD.MT
{
    public partial class DiviedPurOrgnitionsForm : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        addPurDiciedFactoryForm addForm = null;
        public DiviedPurOrgnitionsForm()
        {
            InitializeComponent();
        }
        /// <summary>
        /// 初始化
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DiviedPurOrgnitionsForm_Load(object sender, EventArgs e)
        {
            btn_刷新_Click(sender, e);
        }

        private void btn_新条目_Click(object sender, EventArgs e)
        {
            if (this.addForm == null || this.addForm.IsDisposed)
            {
                this.addForm = new addPurDiciedFactoryForm(this);
            }
            this.addForm.Show();
        }

        private void pageTool_Load(object sender, EventArgs e)
        {
            string sql = "SELECT count(*) from Factory_Purcharse_Organization";
            try
            {
                int i = int.Parse(DBHelper.ExecuteQueryDT(sql).Rows[0][0].ToString());
                int totalNum = i;
                pageTool.DrawControl(totalNum);
                //事件改变调用函数
                pageTool.OnPageChanged += pageTool_OnPageChanged;
            }
            catch (Lib.Common.MMCException.Bll.BllException ex)
            {
                MessageUtil.ShowTips("数据库异常，请稍后重试");
                return;
            }
        }

        private void pageTool_OnPageChanged(object sender, EventArgs e)
        {
            LoadData();
        }
        private void LoadData()
        {
            try
            {
                dgv_data.DataSource = FindCountryInforByPage(this.pageTool.PageSize, this.pageTool.PageIndex);
            }
            catch (BllException ex)
            {
                MessageUtil.ShowError(ex.Message);
            }
        }

        private object FindCountryInforByPage(int pageSize, int pageIndex)
        {
            try
            {
                string sql = @"SELECT top " + pageSize + " f.FactoryID as 工厂代码,b.Buyer_Org_Name  as 采购组织 from Factory_Purcharse_Organization f , Buyer_Org as b where f.PurcharseOrgazationID = b.Buyer_Org AND  f.FactoryID not in(select top " + pageSize * (pageIndex - 1) + " f.FactoryID as 工厂代码,b.Buyer_Org_Name  as 采购组织 from Factory_Purcharse_Organization f , Buyer_Org as b where f.PurcharseOrgazationID = b.Buyer_Org ORDER BY Country_ID ASC)ORDER BY f.FactoryID";
                return DBHelper.ExecuteQueryDT(sql);
            }
            catch (DBException ex)
            {
                throw new BllException("当前无数据", ex);
            }
        }

        private void btn_删除_Click(object sender, EventArgs e)
        {
            string codeF = dgv_data.CurrentRow.Cells[0].Value.ToString();
            try
            {
                DataTable dt = DBHelper.ExecuteQueryDT("select Buyer_Org from Buyer_Org  where Buyer_Org_Name = '" + dgv_data.CurrentRow.Cells[1].Value.ToString().Trim() + "'");
                if(dt.Rows.Count>0)
                {
                    string codeP = dt.Rows[0][0].ToString() ;
                    string sql = " DELETE FROM [Factory_Purcharse_Organization] WHERE FactoryID='" + codeF + "' AND  PurcharseOrgazationID = '" + codeP + "' ";
                    DBHelper.ExecuteNonQuery(sql);
                    pageTool_Load(sender, e);
                }
            }
            catch(DBException ex)
            {
                MessageBox.Show("删除失败");
            }
        }

        private void btn_刷新_Click(object sender, EventArgs e)
        {
            string sql = "SELECT f.FactoryID as 工厂代码,b.Buyer_Org_Name  as 采购组织 from Factory_Purcharse_Organization f , Buyer_Org as b where f.PurcharseOrgazationID = b.Buyer_Org ";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            dgv_data.DataSource = dt;
            pageTool_Load(sender, e);
        }
    }
}
