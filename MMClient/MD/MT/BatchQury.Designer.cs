﻿namespace MMClient.MD.MT
{
    partial class BatchQury
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbx_批次编码 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.pageTool = new pager.pagetool.pageNext();
            this.dgv_批次特性 = new System.Windows.Forms.DataGridView();
            this.批次编号 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.批次名称 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.特性值 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btn_查询 = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_批次特性)).BeginInit();
            this.SuspendLayout();
            // 
            // tbx_批次编码
            // 
            this.tbx_批次编码.Location = new System.Drawing.Point(117, 31);
            this.tbx_批次编码.Name = "tbx_批次编码";
            this.tbx_批次编码.Size = new System.Drawing.Size(131, 19);
            this.tbx_批次编码.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(28, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "批次编码";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.pageTool);
            this.groupBox1.Controls.Add(this.dgv_批次特性);
            this.groupBox1.Controls.Add(this.btn_查询);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.tbx_批次编码);
            this.groupBox1.Location = new System.Drawing.Point(13, 9);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(700, 457);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "查询批次信息";
            // 
            // pageTool
            // 
            this.pageTool.Location = new System.Drawing.Point(6, 398);
            this.pageTool.Name = "pageTool";
            this.pageTool.PageIndex = 1;
            this.pageTool.PageSize = 100;
            this.pageTool.RecordCount = 0;
            this.pageTool.Size = new System.Drawing.Size(688, 37);
            this.pageTool.TabIndex = 4;
            this.pageTool.OnPageChanged += new System.EventHandler(this.pageTool_OnPageChanged);
            this.pageTool.Load += new System.EventHandler(this.pageNext1_Load);
            // 
            // dgv_批次特性
            // 
            this.dgv_批次特性.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgv_批次特性.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.dgv_批次特性.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgv_批次特性.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_批次特性.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.批次编号,
            this.批次名称,
            this.特性值});
            this.dgv_批次特性.Location = new System.Drawing.Point(6, 87);
            this.dgv_批次特性.Name = "dgv_批次特性";
            this.dgv_批次特性.RowTemplate.Height = 23;
            this.dgv_批次特性.Size = new System.Drawing.Size(688, 305);
            this.dgv_批次特性.TabIndex = 3;
            // 
            // 批次编号
            // 
            this.批次编号.DataPropertyName = "Batch_ID";
            this.批次编号.HeaderText = "批次编号";
            this.批次编号.Name = "批次编号";
            this.批次编号.Width = 250;
            // 
            // 批次名称
            // 
            this.批次名称.DataPropertyName = "Feature_Name";
            this.批次名称.HeaderText = "批次名称";
            this.批次名称.Name = "批次名称";
            this.批次名称.Width = 200;
            // 
            // 特性值
            // 
            this.特性值.DataPropertyName = "Feature_Value";
            this.特性值.HeaderText = "特性值";
            this.特性值.Name = "特性值";
            this.特性值.Width = 200;
            // 
            // btn_查询
            // 
            this.btn_查询.Location = new System.Drawing.Point(352, 27);
            this.btn_查询.Name = "btn_查询";
            this.btn_查询.Size = new System.Drawing.Size(87, 31);
            this.btn_查询.TabIndex = 2;
            this.btn_查询.Text = "查询";
            this.btn_查询.UseVisualStyleBackColor = true;
            this.btn_查询.Click += new System.EventHandler(this.btn_查询_Click);
            // 
            // BatchQury
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(714, 500);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "BatchQury";
            this.Text = "查询批次";
            this.Load += new System.EventHandler(this.BatchQury_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_批次特性)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox tbx_批次编码;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btn_查询;
        private System.Windows.Forms.DataGridView dgv_批次特性;
        private pager.pagetool.pageNext pageTool;
        private System.Windows.Forms.DataGridViewTextBoxColumn 批次编号;
        private System.Windows.Forms.DataGridViewTextBoxColumn 批次名称;
        private System.Windows.Forms.DataGridViewTextBoxColumn 特性值;
    }
}