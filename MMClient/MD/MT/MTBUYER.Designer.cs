﻿namespace MMClient.MD.MT
{
    partial class MTBUYER
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MTBUYER));
            this.label1 = new System.Windows.Forms.Label();
            this.cbb_物料编号 = new System.Windows.Forms.ComboBox();
            this.cbb_物料名称 = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cbb_工厂编码 = new System.Windows.Forms.ComboBox();
            this.cbb_工厂名称 = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cbb_计量单位 = new System.Windows.Forms.ComboBox();
            this.lb_采购组 = new System.Windows.Forms.Label();
            this.cbb_采购组 = new System.Windows.Forms.ComboBox();
            this.lb_物料状态 = new System.Windows.Forms.Label();
            this.cbb_物料状态 = new System.Windows.Forms.ComboBox();
            this.lb_税收状态 = new System.Windows.Forms.Label();
            this.lb_运输组 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.cbb_订单单位 = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cbb_运输组 = new System.Windows.Forms.TextBox();
            this.cbb_税收状态 = new System.Windows.Forms.TextBox();
            this.cbb_起始期 = new System.Windows.Forms.DateTimePicker();
            this.tbx_工厂物料状态 = new System.Windows.Forms.TextBox();
            this.ckb_源清单 = new System.Windows.Forms.CheckBox();
            this.ckb_MPN = new System.Windows.Forms.CheckBox();
            this.ckb_批次管理 = new System.Windows.Forms.CheckBox();
            this.cbb_资格 = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.lb_有效起始期 = new System.Windows.Forms.Label();
            this.cbb_物料组 = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.cbb_可变单位 = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.gbx_采购值 = new System.Windows.Forms.GroupBox();
            this.comboBox7 = new System.Windows.Forms.TextBox();
            this.comboBox5 = new System.Windows.Forms.TextBox();
            this.comboBox4 = new System.Windows.Forms.TextBox();
            this.comboBox6 = new System.Windows.Forms.TextBox();
            this.cbb_价值代码 = new System.Windows.Forms.TextBox();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.label23 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.label7 = new System.Windows.Forms.Label();
            this.cbqualityClass = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.minDeliveryNum = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btn_维护制造商物料 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.toolStripExecutionMonitorSetting = new System.Windows.Forms.ToolStrip();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton7 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton8 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton4 = new System.Windows.Forms.ToolStripButton();
            this.groupBox1.SuspendLayout();
            this.gbx_采购值.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.toolStripExecutionMonitorSetting.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(22, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "物料";
            // 
            // cbb_物料编号
            // 
            this.cbb_物料编号.Enabled = false;
            this.cbb_物料编号.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbb_物料编号.FormattingEnabled = true;
            this.cbb_物料编号.Location = new System.Drawing.Point(65, 18);
            this.cbb_物料编号.Name = "cbb_物料编号";
            this.cbb_物料编号.Size = new System.Drawing.Size(163, 25);
            this.cbb_物料编号.TabIndex = 1;
            this.cbb_物料编号.SelectedIndexChanged += new System.EventHandler(this.cbb_物料编号_SelectedIndexChanged);
            // 
            // cbb_物料名称
            // 
            this.cbb_物料名称.Enabled = false;
            this.cbb_物料名称.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbb_物料名称.FormattingEnabled = true;
            this.cbb_物料名称.Location = new System.Drawing.Point(301, 18);
            this.cbb_物料名称.Name = "cbb_物料名称";
            this.cbb_物料名称.Size = new System.Drawing.Size(205, 25);
            this.cbb_物料名称.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(24, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 17);
            this.label2.TabIndex = 4;
            this.label2.Text = "工厂";
            // 
            // cbb_工厂编码
            // 
            this.cbb_工厂编码.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbb_工厂编码.FormattingEnabled = true;
            this.cbb_工厂编码.Location = new System.Drawing.Point(65, 60);
            this.cbb_工厂编码.Name = "cbb_工厂编码";
            this.cbb_工厂编码.Size = new System.Drawing.Size(163, 25);
            this.cbb_工厂编码.TabIndex = 5;
            // 
            // cbb_工厂名称
            // 
            this.cbb_工厂名称.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbb_工厂名称.FormattingEnabled = true;
            this.cbb_工厂名称.Location = new System.Drawing.Point(301, 57);
            this.cbb_工厂名称.Name = "cbb_工厂名称";
            this.cbb_工厂名称.Size = new System.Drawing.Size(205, 25);
            this.cbb_工厂名称.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label3.Location = new System.Drawing.Point(27, 44);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 17);
            this.label3.TabIndex = 0;
            this.label3.Text = "基本计量单位";
            // 
            // cbb_计量单位
            // 
            this.cbb_计量单位.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.cbb_计量单位.FormattingEnabled = true;
            this.cbb_计量单位.Location = new System.Drawing.Point(124, 41);
            this.cbb_计量单位.Name = "cbb_计量单位";
            this.cbb_计量单位.Size = new System.Drawing.Size(121, 25);
            this.cbb_计量单位.TabIndex = 1;
            // 
            // lb_采购组
            // 
            this.lb_采购组.AutoSize = true;
            this.lb_采购组.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lb_采购组.Location = new System.Drawing.Point(27, 91);
            this.lb_采购组.Name = "lb_采购组";
            this.lb_采购组.Size = new System.Drawing.Size(44, 17);
            this.lb_采购组.TabIndex = 2;
            this.lb_采购组.Text = "采购组";
            // 
            // cbb_采购组
            // 
            this.cbb_采购组.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.cbb_采购组.FormattingEnabled = true;
            this.cbb_采购组.Location = new System.Drawing.Point(124, 91);
            this.cbb_采购组.Name = "cbb_采购组";
            this.cbb_采购组.Size = new System.Drawing.Size(121, 25);
            this.cbb_采购组.TabIndex = 3;
            this.cbb_采购组.Leave += new System.EventHandler(this.cbb_采购组_Leave);
            // 
            // lb_物料状态
            // 
            this.lb_物料状态.AutoSize = true;
            this.lb_物料状态.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lb_物料状态.Location = new System.Drawing.Point(27, 220);
            this.lb_物料状态.Name = "lb_物料状态";
            this.lb_物料状态.Size = new System.Drawing.Size(80, 17);
            this.lb_物料状态.TabIndex = 4;
            this.lb_物料状态.Text = "工厂物料状态";
            // 
            // cbb_物料状态
            // 
            this.cbb_物料状态.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbb_物料状态.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.cbb_物料状态.FormattingEnabled = true;
            this.cbb_物料状态.Items.AddRange(new object[] {
            "01",
            "02",
            "DC",
            "EC"});
            this.cbb_物料状态.Location = new System.Drawing.Point(124, 217);
            this.cbb_物料状态.Name = "cbb_物料状态";
            this.cbb_物料状态.Size = new System.Drawing.Size(42, 25);
            this.cbb_物料状态.TabIndex = 5;
            this.cbb_物料状态.SelectedIndexChanged += new System.EventHandler(this.cbb_物料状态_SelectedIndexChanged);
            // 
            // lb_税收状态
            // 
            this.lb_税收状态.AutoSize = true;
            this.lb_税收状态.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lb_税收状态.Location = new System.Drawing.Point(27, 139);
            this.lb_税收状态.Name = "lb_税收状态";
            this.lb_税收状态.Size = new System.Drawing.Size(92, 17);
            this.lb_税收状态.TabIndex = 6;
            this.lb_税收状态.Text = "物料的税收状态";
            // 
            // lb_运输组
            // 
            this.lb_运输组.AutoSize = true;
            this.lb_运输组.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lb_运输组.Location = new System.Drawing.Point(27, 183);
            this.lb_运输组.Name = "lb_运输组";
            this.lb_运输组.Size = new System.Drawing.Size(68, 17);
            this.lb_运输组.TabIndex = 8;
            this.lb_运输组.Text = "物料运输组";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label8.Location = new System.Drawing.Point(360, 44);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(56, 17);
            this.label8.TabIndex = 10;
            this.label8.Text = "订单单位";
            // 
            // cbb_订单单位
            // 
            this.cbb_订单单位.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.cbb_订单单位.FormattingEnabled = true;
            this.cbb_订单单位.Location = new System.Drawing.Point(468, 41);
            this.cbb_订单单位.Name = "cbb_订单单位";
            this.cbb_订单单位.Size = new System.Drawing.Size(51, 25);
            this.cbb_订单单位.TabIndex = 11;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cbb_运输组);
            this.groupBox1.Controls.Add(this.cbb_税收状态);
            this.groupBox1.Controls.Add(this.cbb_起始期);
            this.groupBox1.Controls.Add(this.tbx_工厂物料状态);
            this.groupBox1.Controls.Add(this.ckb_源清单);
            this.groupBox1.Controls.Add(this.ckb_MPN);
            this.groupBox1.Controls.Add(this.ckb_批次管理);
            this.groupBox1.Controls.Add(this.cbb_资格);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.lb_有效起始期);
            this.groupBox1.Controls.Add(this.cbb_物料组);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.cbb_可变单位);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.cbb_订单单位);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.lb_运输组);
            this.groupBox1.Controls.Add(this.lb_税收状态);
            this.groupBox1.Controls.Add(this.cbb_物料状态);
            this.groupBox1.Controls.Add(this.lb_物料状态);
            this.groupBox1.Controls.Add(this.cbb_采购组);
            this.groupBox1.Controls.Add(this.lb_采购组);
            this.groupBox1.Controls.Add(this.cbb_计量单位);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Font = new System.Drawing.Font("微软雅黑", 10F);
            this.groupBox1.Location = new System.Drawing.Point(6, 169);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1276, 254);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "常规数据";
            // 
            // cbb_运输组
            // 
            this.cbb_运输组.Location = new System.Drawing.Point(124, 178);
            this.cbb_运输组.Name = "cbb_运输组";
            this.cbb_运输组.Size = new System.Drawing.Size(121, 25);
            this.cbb_运输组.TabIndex = 27;
            // 
            // cbb_税收状态
            // 
            this.cbb_税收状态.Location = new System.Drawing.Point(124, 134);
            this.cbb_税收状态.Name = "cbb_税收状态";
            this.cbb_税收状态.Size = new System.Drawing.Size(121, 25);
            this.cbb_税收状态.TabIndex = 26;
            // 
            // cbb_起始期
            // 
            this.cbb_起始期.Location = new System.Drawing.Point(468, 136);
            this.cbb_起始期.Name = "cbb_起始期";
            this.cbb_起始期.Size = new System.Drawing.Size(121, 25);
            this.cbb_起始期.TabIndex = 25;
            // 
            // tbx_工厂物料状态
            // 
            this.tbx_工厂物料状态.Enabled = false;
            this.tbx_工厂物料状态.Location = new System.Drawing.Point(172, 216);
            this.tbx_工厂物料状态.Name = "tbx_工厂物料状态";
            this.tbx_工厂物料状态.Size = new System.Drawing.Size(143, 25);
            this.tbx_工厂物料状态.TabIndex = 24;
            // 
            // ckb_源清单
            // 
            this.ckb_源清单.AutoSize = true;
            this.ckb_源清单.Location = new System.Drawing.Point(630, 176);
            this.ckb_源清单.Name = "ckb_源清单";
            this.ckb_源清单.Size = new System.Drawing.Size(84, 24);
            this.ckb_源清单.TabIndex = 23;
            this.ckb_源清单.Text = "货源清单";
            this.ckb_源清单.UseVisualStyleBackColor = true;
            this.ckb_源清单.CheckedChanged += new System.EventHandler(this.ckb_源清单_CheckedChanged);
            // 
            // ckb_MPN
            // 
            this.ckb_MPN.AutoSize = true;
            this.ckb_MPN.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.ckb_MPN.Location = new System.Drawing.Point(630, 135);
            this.ckb_MPN.Name = "ckb_MPN";
            this.ckb_MPN.Size = new System.Drawing.Size(80, 21);
            this.ckb_MPN.TabIndex = 21;
            this.ckb_MPN.Text = "MPN标识";
            this.ckb_MPN.UseVisualStyleBackColor = true;
            // 
            // ckb_批次管理
            // 
            this.ckb_批次管理.AutoSize = true;
            this.ckb_批次管理.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.ckb_批次管理.Location = new System.Drawing.Point(630, 94);
            this.ckb_批次管理.Name = "ckb_批次管理";
            this.ckb_批次管理.Size = new System.Drawing.Size(123, 21);
            this.ckb_批次管理.TabIndex = 20;
            this.ckb_批次管理.Text = "是否进行批次管理";
            this.ckb_批次管理.UseVisualStyleBackColor = true;
            // 
            // cbb_资格
            // 
            this.cbb_资格.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.cbb_资格.FormattingEnabled = true;
            this.cbb_资格.Location = new System.Drawing.Point(468, 183);
            this.cbb_资格.Name = "cbb_资格";
            this.cbb_资格.Size = new System.Drawing.Size(121, 25);
            this.cbb_资格.TabIndex = 19;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label12.Location = new System.Drawing.Point(360, 186);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(104, 17);
            this.label12.TabIndex = 18;
            this.label12.Text = "免税货物折扣资格";
            // 
            // lb_有效起始期
            // 
            this.lb_有效起始期.AutoSize = true;
            this.lb_有效起始期.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lb_有效起始期.Location = new System.Drawing.Point(360, 139);
            this.lb_有效起始期.Name = "lb_有效起始期";
            this.lb_有效起始期.Size = new System.Drawing.Size(68, 17);
            this.lb_有效起始期.TabIndex = 16;
            this.lb_有效起始期.Text = "有效起始期";
            // 
            // cbb_物料组
            // 
            this.cbb_物料组.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.cbb_物料组.FormattingEnabled = true;
            this.cbb_物料组.Location = new System.Drawing.Point(468, 91);
            this.cbb_物料组.Name = "cbb_物料组";
            this.cbb_物料组.Size = new System.Drawing.Size(121, 25);
            this.cbb_物料组.TabIndex = 15;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label10.Location = new System.Drawing.Point(360, 91);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(44, 17);
            this.label10.TabIndex = 14;
            this.label10.Text = "物料组";
            // 
            // cbb_可变单位
            // 
            this.cbb_可变单位.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.cbb_可变单位.FormattingEnabled = true;
            this.cbb_可变单位.Location = new System.Drawing.Point(688, 36);
            this.cbb_可变单位.Name = "cbb_可变单位";
            this.cbb_可变单位.Size = new System.Drawing.Size(51, 25);
            this.cbb_可变单位.TabIndex = 13;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label9.Location = new System.Drawing.Point(623, 44);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(56, 17);
            this.label9.TabIndex = 12;
            this.label9.Text = "可变单位";
            // 
            // gbx_采购值
            // 
            this.gbx_采购值.Controls.Add(this.comboBox7);
            this.gbx_采购值.Controls.Add(this.comboBox5);
            this.gbx_采购值.Controls.Add(this.comboBox4);
            this.gbx_采购值.Controls.Add(this.comboBox6);
            this.gbx_采购值.Controls.Add(this.cbb_价值代码);
            this.gbx_采购值.Controls.Add(this.checkBox3);
            this.gbx_采购值.Controls.Add(this.label23);
            this.gbx_采购值.Controls.Add(this.textBox2);
            this.gbx_采购值.Controls.Add(this.label24);
            this.gbx_采购值.Controls.Add(this.label11);
            this.gbx_采购值.Controls.Add(this.textBox1);
            this.gbx_采购值.Controls.Add(this.label22);
            this.gbx_采购值.Controls.Add(this.checkBox2);
            this.gbx_采购值.Controls.Add(this.checkBox1);
            this.gbx_采购值.Controls.Add(this.label7);
            this.gbx_采购值.Controls.Add(this.cbqualityClass);
            this.gbx_采购值.Controls.Add(this.label6);
            this.gbx_采购值.Controls.Add(this.minDeliveryNum);
            this.gbx_采购值.Controls.Add(this.label5);
            this.gbx_采购值.Controls.Add(this.label21);
            this.gbx_采购值.Controls.Add(this.label20);
            this.gbx_采购值.Controls.Add(this.label19);
            this.gbx_采购值.Controls.Add(this.label18);
            this.gbx_采购值.Controls.Add(this.label17);
            this.gbx_采购值.Controls.Add(this.label16);
            this.gbx_采购值.Controls.Add(this.label15);
            this.gbx_采购值.Controls.Add(this.label14);
            this.gbx_采购值.Controls.Add(this.label13);
            this.gbx_采购值.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.gbx_采购值.Location = new System.Drawing.Point(4, 429);
            this.gbx_采购值.Name = "gbx_采购值";
            this.gbx_采购值.Size = new System.Drawing.Size(1278, 162);
            this.gbx_采购值.TabIndex = 7;
            this.gbx_采购值.TabStop = false;
            this.gbx_采购值.Text = "采购值";
            // 
            // comboBox7
            // 
            this.comboBox7.Location = new System.Drawing.Point(450, 116);
            this.comboBox7.Name = "comboBox7";
            this.comboBox7.Size = new System.Drawing.Size(68, 23);
            this.comboBox7.TabIndex = 37;
            // 
            // comboBox5
            // 
            this.comboBox5.Location = new System.Drawing.Point(450, 79);
            this.comboBox5.Name = "comboBox5";
            this.comboBox5.Size = new System.Drawing.Size(68, 23);
            this.comboBox5.TabIndex = 36;
            // 
            // comboBox4
            // 
            this.comboBox4.Location = new System.Drawing.Point(450, 43);
            this.comboBox4.Name = "comboBox4";
            this.comboBox4.Size = new System.Drawing.Size(68, 23);
            this.comboBox4.TabIndex = 35;
            // 
            // comboBox6
            // 
            this.comboBox6.Location = new System.Drawing.Point(156, 79);
            this.comboBox6.Name = "comboBox6";
            this.comboBox6.Size = new System.Drawing.Size(83, 23);
            this.comboBox6.TabIndex = 34;
            // 
            // cbb_价值代码
            // 
            this.cbb_价值代码.Location = new System.Drawing.Point(156, 39);
            this.cbb_价值代码.Name = "cbb_价值代码";
            this.cbb_价值代码.Size = new System.Drawing.Size(83, 23);
            this.cbb_价值代码.TabIndex = 33;
            // 
            // checkBox3
            // 
            this.checkBox3.AutoSize = true;
            this.checkBox3.Location = new System.Drawing.Point(631, 44);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(111, 21);
            this.checkBox3.TabIndex = 32;
            this.checkBox3.Text = "过账到检验库存";
            this.checkBox3.UseVisualStyleBackColor = true;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label23.Location = new System.Drawing.Point(1015, 85);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(19, 17);
            this.label23.TabIndex = 31;
            this.label23.Text = "%";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(915, 81);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(83, 23);
            this.textBox2.TabIndex = 30;
            this.textBox2.Text = "60";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label24.Location = new System.Drawing.Point(814, 86);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(80, 17);
            this.label24.TabIndex = 29;
            this.label24.Text = "过量交货限额";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label11.Location = new System.Drawing.Point(1015, 43);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(19, 17);
            this.label11.TabIndex = 28;
            this.label11.Text = "%";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(915, 39);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(83, 23);
            this.textBox1.TabIndex = 27;
            this.textBox1.Text = "60";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label22.Location = new System.Drawing.Point(814, 44);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(80, 17);
            this.label22.TabIndex = 26;
            this.label22.Text = "交货不足限额";
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Location = new System.Drawing.Point(631, 119);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(75, 21);
            this.checkBox2.TabIndex = 25;
            this.checkBox2.Text = "配额安排";
            this.checkBox2.UseVisualStyleBackColor = true;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(631, 83);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(91, 21);
            this.checkBox1.TabIndex = 24;
            this.checkBox1.Text = "JIT计划标识";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(820, 125);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(56, 17);
            this.label7.TabIndex = 18;
            this.label7.Text = "关键组件";
            // 
            // cbqualityClass
            // 
            this.cbqualityClass.AutoCompleteCustomSource.AddRange(new string[] {
            "一般组件",
            "重要组件",
            "关键组件"});
            this.cbqualityClass.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.cbqualityClass.FormattingEnabled = true;
            this.cbqualityClass.Items.AddRange(new object[] {
            "一般特性",
            "重要特性",
            "关键特性"});
            this.cbqualityClass.Location = new System.Drawing.Point(913, 117);
            this.cbqualityClass.Name = "cbqualityClass";
            this.cbqualityClass.Size = new System.Drawing.Size(85, 25);
            this.cbqualityClass.TabIndex = 17;
            this.cbqualityClass.Text = "一般特性";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label6.Location = new System.Drawing.Point(243, 119);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(19, 17);
            this.label6.TabIndex = 16;
            this.label6.Text = "%";
            // 
            // minDeliveryNum
            // 
            this.minDeliveryNum.Location = new System.Drawing.Point(156, 116);
            this.minDeliveryNum.Name = "minDeliveryNum";
            this.minDeliveryNum.Size = new System.Drawing.Size(83, 23);
            this.minDeliveryNum.TabIndex = 15;
            this.minDeliveryNum.Text = "60";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label5.Location = new System.Drawing.Point(29, 122);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(103, 17);
            this.label5.TabIndex = 14;
            this.label5.Text = "以%最小交货数量";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label21.Location = new System.Drawing.Point(524, 127);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(32, 17);
            this.label21.TabIndex = 13;
            this.label21.Text = "天数";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label20.Location = new System.Drawing.Point(241, 82);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(32, 17);
            this.label20.TabIndex = 12;
            this.label20.Text = "天数";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label19.Location = new System.Drawing.Point(524, 88);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(32, 17);
            this.label19.TabIndex = 11;
            this.label19.Text = "天数";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label18.Location = new System.Drawing.Point(524, 45);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(32, 17);
            this.label18.TabIndex = 10;
            this.label18.Text = "天数";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label17.Location = new System.Drawing.Point(24, 84);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(128, 17);
            this.label17.TabIndex = 8;
            this.label17.Text = "标准日期交货日期偏差";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label16.Location = new System.Drawing.Point(364, 123);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(80, 17);
            this.label16.TabIndex = 4;
            this.label16.Text = "第三封催询单";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label15.Location = new System.Drawing.Point(363, 87);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(80, 17);
            this.label15.TabIndex = 3;
            this.label15.Text = "第二封催询单";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label14.Location = new System.Drawing.Point(363, 47);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(80, 17);
            this.label14.TabIndex = 2;
            this.label14.Text = "第一封催询单";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label13.Location = new System.Drawing.Point(24, 46);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(80, 17);
            this.label13.TabIndex = 0;
            this.label13.Text = "采购价值代码";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btn_维护制造商物料);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.cbb_工厂名称);
            this.groupBox2.Controls.Add(this.cbb_工厂编码);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.cbb_物料名称);
            this.groupBox2.Controls.Add(this.cbb_物料编号);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Font = new System.Drawing.Font("微软雅黑", 10F);
            this.groupBox2.Location = new System.Drawing.Point(6, 70);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1276, 93);
            this.groupBox2.TabIndex = 11;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "采购视图";
            // 
            // btn_维护制造商物料
            // 
            this.btn_维护制造商物料.Location = new System.Drawing.Point(685, 53);
            this.btn_维护制造商物料.Name = "btn_维护制造商物料";
            this.btn_维护制造商物料.Size = new System.Drawing.Size(206, 29);
            this.btn_维护制造商物料.TabIndex = 17;
            this.btn_维护制造商物料.Text = "维护制造商物料";
            this.btn_维护制造商物料.UseVisualStyleBackColor = true;
            this.btn_维护制造商物料.Visible = false;
            this.btn_维护制造商物料.Click += new System.EventHandler(this.btn_维护制造商物料_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(234, 21);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 20);
            this.label4.TabIndex = 15;
            this.label4.Text = "“必选”";
            // 
            // toolStripExecutionMonitorSetting
            // 
            this.toolStripExecutionMonitorSetting.ImageScalingSize = new System.Drawing.Size(40, 40);
            this.toolStripExecutionMonitorSetting.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton1,
            this.toolStripButton7,
            this.toolStripButton8,
            this.toolStripButton4});
            this.toolStripExecutionMonitorSetting.Location = new System.Drawing.Point(0, 0);
            this.toolStripExecutionMonitorSetting.Name = "toolStripExecutionMonitorSetting";
            this.toolStripExecutionMonitorSetting.Size = new System.Drawing.Size(1301, 64);
            this.toolStripExecutionMonitorSetting.TabIndex = 19;
            this.toolStripExecutionMonitorSetting.Text = "toolStrip1";
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(60, 61);
            this.toolStripButton1.Text = "选择物料";
            this.toolStripButton1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // toolStripButton7
            // 
            this.toolStripButton7.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton7.Image")));
            this.toolStripButton7.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton7.Name = "toolStripButton7";
            this.toolStripButton7.Size = new System.Drawing.Size(44, 61);
            this.toolStripButton7.Text = "刷新";
            this.toolStripButton7.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton7.Click += new System.EventHandler(this.toolStripButton7_Click);
            // 
            // toolStripButton8
            // 
            this.toolStripButton8.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton8.Name = "toolStripButton8";
            this.toolStripButton8.Size = new System.Drawing.Size(36, 61);
            this.toolStripButton8.Text = "关闭";
            this.toolStripButton8.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton8.Click += new System.EventHandler(this.toolStripButton8_Click);
            // 
            // toolStripButton4
            // 
            this.toolStripButton4.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton4.Name = "toolStripButton4";
            this.toolStripButton4.Size = new System.Drawing.Size(36, 61);
            this.toolStripButton4.Text = "确定";
            this.toolStripButton4.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton4.Click += new System.EventHandler(this.toolStripButton4_Click);
            // 
            // MTBUYER
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1301, 603);
            this.Controls.Add(this.toolStripExecutionMonitorSetting);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.gbx_采购值);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "MTBUYER";
            this.Text = "采购视图";
            this.Load += new System.EventHandler(this.MTBUYER_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.gbx_采购值.ResumeLayout(false);
            this.gbx_采购值.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.toolStripExecutionMonitorSetting.ResumeLayout(false);
            this.toolStripExecutionMonitorSetting.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        public System.Windows.Forms.ComboBox cbb_工厂编码;
        public System.Windows.Forms.ComboBox cbb_工厂名称;
        public System.Windows.Forms.ComboBox cbb_采购组;
        public System.Windows.Forms.ComboBox cbb_物料状态;
        public System.Windows.Forms.Label lb_采购组;
        public System.Windows.Forms.Label lb_物料状态;
        public System.Windows.Forms.Label lb_税收状态;
        public System.Windows.Forms.Label lb_运输组;
        public System.Windows.Forms.Label label10;
        public System.Windows.Forms.Label lb_有效起始期;
        public System.Windows.Forms.ComboBox cbb_计量单位;
        public System.Windows.Forms.ComboBox cbb_订单单位;
        public System.Windows.Forms.ComboBox cbb_可变单位;
        public System.Windows.Forms.ComboBox cbb_物料组;
        public System.Windows.Forms.ComboBox cbb_物料编号;
        private System.Windows.Forms.GroupBox groupBox2;
        public System.Windows.Forms.GroupBox gbx_采购值;
        public System.Windows.Forms.CheckBox ckb_MPN;
        public System.Windows.Forms.CheckBox ckb_批次管理;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btn_维护制造商物料;
        private System.Windows.Forms.ToolStrip toolStripExecutionMonitorSetting;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripButton toolStripButton7;
        private System.Windows.Forms.ToolStripButton toolStripButton8;
        private System.Windows.Forms.ToolStripButton toolStripButton4;
        public System.Windows.Forms.ComboBox cbb_物料名称;
        public System.Windows.Forms.CheckBox ckb_源清单;
        private System.Windows.Forms.TextBox tbx_工厂物料状态;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox minDeliveryNum;
        private System.Windows.Forms.DateTimePicker cbb_起始期;
        public System.Windows.Forms.CheckBox checkBox3;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label22;
        public System.Windows.Forms.CheckBox checkBox2;
        public System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Label label7;
        public System.Windows.Forms.ComboBox cbqualityClass;
        private System.Windows.Forms.TextBox cbb_价值代码;
        private System.Windows.Forms.TextBox comboBox6;
        private System.Windows.Forms.TextBox comboBox4;
        private System.Windows.Forms.TextBox comboBox7;
        private System.Windows.Forms.TextBox comboBox5;
        public System.Windows.Forms.ComboBox cbb_资格;
        public System.Windows.Forms.TextBox cbb_税收状态;
        public System.Windows.Forms.TextBox cbb_运输组;
    }
}