﻿namespace MMClient.MD.MT
{
    partial class MTEstablish1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MTEstablish1));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.LbpathShow = new System.Windows.Forms.Label();
            this.cbApplicationFiled = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cbMaterialGroup = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cbMaterialType = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtMaterialName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnTools = new System.Windows.Forms.Button();
            this.BtnoutImport = new System.Windows.Forms.Button();
            this.toolStripExecutionMonitorSetting = new System.Windows.Forms.ToolStrip();
            this.toolStripButton7 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton8 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton4 = new System.Windows.Forms.ToolStripButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dgv_MaterialBaseData = new System.Windows.Forms.DataGridView();
            this.物料编号 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.物料名称 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.物料组 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.物料类型 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.行业领域 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pageTool = new pager.pagetool.pageNext();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.groupBox1.SuspendLayout();
            this.toolStripExecutionMonitorSetting.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_MaterialBaseData)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.LbpathShow);
            this.groupBox1.Controls.Add(this.cbApplicationFiled);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.cbMaterialGroup);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.cbMaterialType);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txtMaterialName);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(3, 36);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(838, 212);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "创建新数据";
            // 
            // LbpathShow
            // 
            this.LbpathShow.AutoSize = true;
            this.LbpathShow.ForeColor = System.Drawing.Color.Red;
            this.LbpathShow.Location = new System.Drawing.Point(521, 38);
            this.LbpathShow.Name = "LbpathShow";
            this.LbpathShow.Size = new System.Drawing.Size(35, 13);
            this.LbpathShow.TabIndex = 10;
            this.LbpathShow.Text = "label5";
            this.LbpathShow.Visible = false;
            // 
            // cbApplicationFiled
            // 
            this.cbApplicationFiled.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbApplicationFiled.FormattingEnabled = true;
            this.cbApplicationFiled.Location = new System.Drawing.Point(145, 172);
            this.cbApplicationFiled.Name = "cbApplicationFiled";
            this.cbApplicationFiled.Size = new System.Drawing.Size(209, 21);
            this.cbApplicationFiled.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(54, 175);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(67, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "应用领域：";
            // 
            // cbMaterialGroup
            // 
            this.cbMaterialGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbMaterialGroup.FormattingEnabled = true;
            this.cbMaterialGroup.Items.AddRange(new object[] {
            "---选择物料组---"});
            this.cbMaterialGroup.Location = new System.Drawing.Point(145, 129);
            this.cbMaterialGroup.Name = "cbMaterialGroup";
            this.cbMaterialGroup.Size = new System.Drawing.Size(209, 21);
            this.cbMaterialGroup.TabIndex = 5;
            this.cbMaterialGroup.SelectedIndexChanged += new System.EventHandler(this.cbMaterialGroup_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(54, 132);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "物料组：";
            // 
            // cbMaterialType
            // 
            this.cbMaterialType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbMaterialType.FormattingEnabled = true;
            this.cbMaterialType.Location = new System.Drawing.Point(145, 84);
            this.cbMaterialType.Name = "cbMaterialType";
            this.cbMaterialType.Size = new System.Drawing.Size(209, 21);
            this.cbMaterialType.TabIndex = 3;
            this.cbMaterialType.SelectedIndexChanged += new System.EventHandler(this.cbMaterialType_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(54, 92);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "物料类型：";
            // 
            // txtMaterialName
            // 
            this.txtMaterialName.Location = new System.Drawing.Point(145, 38);
            this.txtMaterialName.Name = "txtMaterialName";
            this.txtMaterialName.Size = new System.Drawing.Size(209, 19);
            this.txtMaterialName.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(54, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "物料名称：";
            // 
            // btnTools
            // 
            this.btnTools.Location = new System.Drawing.Point(401, 3);
            this.btnTools.Name = "btnTools";
            this.btnTools.Size = new System.Drawing.Size(94, 33);
            this.btnTools.TabIndex = 9;
            this.btnTools.Text = "模板生成";
            this.btnTools.UseVisualStyleBackColor = true;
            this.btnTools.Click += new System.EventHandler(this.btnTools_Click);
            // 
            // BtnoutImport
            // 
            this.BtnoutImport.Location = new System.Drawing.Point(527, 3);
            this.BtnoutImport.Name = "BtnoutImport";
            this.BtnoutImport.Size = new System.Drawing.Size(94, 33);
            this.BtnoutImport.TabIndex = 8;
            this.BtnoutImport.Text = "外部导入";
            this.BtnoutImport.UseVisualStyleBackColor = true;
            this.BtnoutImport.Visible = false;
            this.BtnoutImport.Click += new System.EventHandler(this.BtnoutImport_Click);
            // 
            // toolStripExecutionMonitorSetting
            // 
            this.toolStripExecutionMonitorSetting.ImageScalingSize = new System.Drawing.Size(40, 40);
            this.toolStripExecutionMonitorSetting.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton7,
            this.toolStripButton8,
            this.toolStripButton4});
            this.toolStripExecutionMonitorSetting.Location = new System.Drawing.Point(0, 0);
            this.toolStripExecutionMonitorSetting.Name = "toolStripExecutionMonitorSetting";
            this.toolStripExecutionMonitorSetting.Size = new System.Drawing.Size(979, 64);
            this.toolStripExecutionMonitorSetting.TabIndex = 20;
            this.toolStripExecutionMonitorSetting.Text = "toolStrip1";
            this.toolStripExecutionMonitorSetting.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.toolStripExecutionMonitorSetting_ItemClicked);
            // 
            // toolStripButton7
            // 
            this.toolStripButton7.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton7.Image")));
            this.toolStripButton7.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton7.Name = "toolStripButton7";
            this.toolStripButton7.Size = new System.Drawing.Size(44, 61);
            this.toolStripButton7.Text = "刷新";
            this.toolStripButton7.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton7.Click += new System.EventHandler(this.toolStripButton7_Click);
            // 
            // toolStripButton8
            // 
            this.toolStripButton8.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton8.Name = "toolStripButton8";
            this.toolStripButton8.Size = new System.Drawing.Size(36, 61);
            this.toolStripButton8.Text = "关闭";
            this.toolStripButton8.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton8.Click += new System.EventHandler(this.toolStripButton8_Click);
            // 
            // toolStripButton4
            // 
            this.toolStripButton4.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton4.Name = "toolStripButton4";
            this.toolStripButton4.Size = new System.Drawing.Size(36, 61);
            this.toolStripButton4.Text = "确定";
            this.toolStripButton4.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton4.Click += new System.EventHandler(this.toolStripButton4_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dgv_MaterialBaseData);
            this.groupBox2.Location = new System.Drawing.Point(1, 272);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(846, 379);
            this.groupBox2.TabIndex = 21;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "历史数据";
            // 
            // dgv_MaterialBaseData
            // 
            this.dgv_MaterialBaseData.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgv_MaterialBaseData.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.dgv_MaterialBaseData.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgv_MaterialBaseData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_MaterialBaseData.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.物料编号,
            this.物料名称,
            this.物料组,
            this.物料类型,
            this.行业领域});
            this.dgv_MaterialBaseData.Location = new System.Drawing.Point(9, 18);
            this.dgv_MaterialBaseData.Name = "dgv_MaterialBaseData";
            this.dgv_MaterialBaseData.RowTemplate.Height = 23;
            this.dgv_MaterialBaseData.Size = new System.Drawing.Size(840, 355);
            this.dgv_MaterialBaseData.TabIndex = 23;
            this.dgv_MaterialBaseData.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_MaterialBaseData_CellContentClick);
            // 
            // 物料编号
            // 
            this.物料编号.DataPropertyName = "物料编号";
            this.物料编号.HeaderText = "物料编号";
            this.物料编号.Name = "物料编号";
            this.物料编号.Width = 200;
            // 
            // 物料名称
            // 
            this.物料名称.DataPropertyName = "物料名称";
            this.物料名称.HeaderText = "物料名称";
            this.物料名称.Name = "物料名称";
            this.物料名称.Width = 150;
            // 
            // 物料组
            // 
            this.物料组.DataPropertyName = "物料组";
            this.物料组.HeaderText = "物料组";
            this.物料组.Name = "物料组";
            // 
            // 物料类型
            // 
            this.物料类型.DataPropertyName = "物料类型";
            this.物料类型.HeaderText = "物料类型";
            this.物料类型.Name = "物料类型";
            this.物料类型.Width = 150;
            // 
            // 行业领域
            // 
            this.行业领域.DataPropertyName = "行业领域";
            this.行业领域.HeaderText = "行业领域";
            this.行业领域.Name = "行业领域";
            this.行业领域.Width = 150;
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.Controls.Add(this.pageTool);
            this.panel1.Controls.Add(this.BtnoutImport);
            this.panel1.Controls.Add(this.btnTools);
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Controls.Add(this.groupBox2);
            this.panel1.Location = new System.Drawing.Point(13, 68);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(850, 703);
            this.panel1.TabIndex = 22;
            // 
            // pageTool
            // 
            this.pageTool.Location = new System.Drawing.Point(60, 657);
            this.pageTool.Name = "pageTool";
            this.pageTool.PageIndex = 1;
            this.pageTool.PageSize = 100;
            this.pageTool.RecordCount = 0;
            this.pageTool.Size = new System.Drawing.Size(685, 41);
            this.pageTool.TabIndex = 24;
            this.pageTool.OnPageChanged += new System.EventHandler(this.pageTool_OnPageChanged);
            this.pageTool.Load += new System.EventHandler(this.pageTool_Load);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // MTEstablish1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(996, 770);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.toolStripExecutionMonitorSetting);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "MTEstablish1";
            this.Text = "创建物料";
            this.Load += new System.EventHandler(this.MTEstablish1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.toolStripExecutionMonitorSetting.ResumeLayout(false);
            this.toolStripExecutionMonitorSetting.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_MaterialBaseData)).EndInit();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ToolStrip toolStripExecutionMonitorSetting;
        private System.Windows.Forms.ToolStripButton toolStripButton7;
        private System.Windows.Forms.ToolStripButton toolStripButton8;
        private System.Windows.Forms.ToolStripButton toolStripButton4;
        private System.Windows.Forms.TextBox txtMaterialName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbApplicationFiled;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cbMaterialGroup;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cbMaterialType;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button BtnoutImport;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridView dgv_MaterialBaseData;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private pager.pagetool.pageNext pageTool;
        private System.Windows.Forms.DataGridViewTextBoxColumn 物料编号;
        private System.Windows.Forms.DataGridViewTextBoxColumn 物料名称;
        private System.Windows.Forms.DataGridViewTextBoxColumn 物料组;
        private System.Windows.Forms.DataGridViewTextBoxColumn 物料类型;
        private System.Windows.Forms.DataGridViewTextBoxColumn 行业领域;
        private System.Windows.Forms.Button btnTools;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Label LbpathShow;
    }
}