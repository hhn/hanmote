﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Bll.MDBll.General;
using Lib.Bll;
using System.IO;
using Lib.SqlServerDAL;
using Lib.Common.CommonUtils;
using Lib.Model.MT_GroupModel;
using Lib.Common.MMCException.Bll;
using Lib.Common.MMCException.IDAL;

namespace MMClient.MD.MT
{
    public partial class NewMaterialGroup : Form
    {
        FTPHelper fTPHelper = new FTPHelper("");
        public NewMaterialGroup()
        {
            InitializeComponent();
        }

        private void NewMaterialGroup_Load(object sender, EventArgs e)
        {
            string type = "SELECT ClassfyID as id , Bigclassfy_Name as name from Bigclassfy";
            try
            {
                this.comboBox1.DataSource = DBHelper.ExecuteQueryDT(type);
                comboBox1.ValueMember = "id";
                comboBox1.DisplayMember = "name";
            }
            catch(BllException ex)
            {
                MessageUtil.ShowTips("数据加载");
                return;
            }
        }

        private void btn_确定_Click(object sender, EventArgs e)
        {
            MaterialGroupModel materialGroupModel = new MaterialGroupModel();
            GeneralBLL gn = new GeneralBLL();
            materialGroupModel.MtGroupId = tbx_物料组编号.Text;
            
            //string fid = cbb_所属工厂编号.Text;
            string fid = "10001";
            string evaluate = "";//评级
            string weight= "";
            string standard = "";//合格标准
            string filepath = "" ;//路径
            //standard = "A级_" + A1.Text + "_" + A2.Text + "," + "B级_" + B1.Text + "_" + B2.Text + "," + "C级_" + C1.Text + "_" + C2.Text;
            string evalid= "";//评级标准

            evaluate = CB_mGroupLevel.Text.ToString();
            switch (evaluate) {
                case "A":evalid ="4";break;
                case "B": evalid = "3"; break;
                case "C": evalid = "2"; break;
                case "D": evalid = "1"; break;
                case "E": evalid = "1"; break;
                default:break;
            }
            //物料组层级值
            materialGroupModel.MtGroupLevelValue = evalid;
            //物料组类型
           
            materialGroupModel.MtGroupClass = comboBox1.SelectedValue.ToString();
            //物料组编号
            materialGroupModel.MtGroupId = tbx_物料组编号.Text;
            //物料组等级
            materialGroupModel.MtGroupLevel = evaluate;
            //物料组名称
            materialGroupModel.MtGroupName = tbx_物料组描述.Text;
            bool b = checkNull();
            if(!b)
            {
                MessageBox.Show("检查数据完整");
                return;
            }
            if (tbx_物料组编号.Text.ToString().Length != 5)
            {
                MessageBox.Show("物料组编号由字母和数字表示且长为5");
                return;
            }
            string insertSQL = "insert into Material_Group (Material_Group,Description,MtGroupClass,Category) VALUES ('" + materialGroupModel.MtGroupId + "','"+ materialGroupModel.MtGroupName + "','"+ materialGroupModel.MtGroupLevel + "','"+materialGroupModel.MtGroupClass+"');";
            try
            {
                DBHelper.ExecuteQueryDS(insertSQL);
                MessageBox.Show("新建成功");
            }
            catch (DBException ex)
            {
                MessageBox.Show("新建失败");
                return;
            }
        }

        private bool checkNull()
        {
            if(string.IsNullOrEmpty(tbx_物料组编号.Text.ToString()) || string.IsNullOrEmpty(tbx_物料组描述.Text.ToString()) || string.IsNullOrEmpty(comboBox1.Text.ToString()) || string.IsNullOrEmpty(CB_mGroupLevel.Text.ToString()))
            {
                return false;
            }
            return true;
        }
        
    }
}
