﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Bll;
using Lib.SqlServerDAL;



namespace MMClient.MD.FI
{
    public partial class actsummy : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        public actsummy()
        {
            InitializeComponent();
        }

      

        private void actsummy_Load(object sender, EventArgs e)
        {
            AccountBLL ACT = new AccountBLL();
            cbb_科目.DataSource = ACT.GetAllAccount();
            cbb_科目.Text = "";
        }

        private void cbb_科目_SelectedIndexChanged(object sender, EventArgs e)
        {
            string account = cbb_科目.Text;
            DataTable dt=null;
            string sql;
            dgv_项目.DataSource = null;
            switch (cbb_科目.Text)
            {
                case "原材料":
                    sql = "SELECT Account_Date as '记账日期',Certificate_Number as '凭证编码',Debit as '借记',Credit as'贷记',Factory as'工厂',Stock as '仓库',Material_Type as'物料类型',Balance as'结余' FROM [Raw_Material_Account]";
                    dt = DBHelper.ExecuteQueryDT(sql);
                    this.dgv_项目.DataSource = dt;
                    break;
                case "服务类":
                    sql = "SELECT * FROM [Service_Class_Account]";
                    dt = DBHelper.ExecuteQueryDT(sql);
                    this.dgv_项目.DataSource = dt;
                    break;
                case "成品":
                    sql = "SELECT * FROM [Finished_Product_Account]";
                    dt = DBHelper.ExecuteQueryDT(sql);
                    this.dgv_项目.DataSource = dt;
                    break;
                case "应付账款":
                    sql = "SELECT * FROM [Accounts_Payable_Account]";
                    dt = DBHelper.ExecuteQueryDT(sql);
                    this.dgv_项目.DataSource = dt;
                    break;
                case "应交税费":
                    sql = "SELECT * FROM [Tax_Payable_Account]";
                    dt = DBHelper.ExecuteQueryDT(sql);
                    this.dgv_项目.DataSource = dt;
                    break;
                case "银行存款":
                    sql = "SELECT * FROM [Bank_Deposit_Account]";
                    dt = DBHelper.ExecuteQueryDT(sql);
                    this.dgv_项目.DataSource = dt;
                    break;
                case "在途物资":
                    sql = "SELECT * FROM [Afloat_Materials_Account]";
                    dt = DBHelper.ExecuteQueryDT(sql);
                    this.dgv_项目.DataSource = dt;
                    break;
                case "材料成本差异":
                    sql = "SELECT * FROM [Material_Cost_Variance_Account]";
                    dt = DBHelper.ExecuteQueryDT(sql);
                    this.dgv_项目.DataSource = dt;
                    break;
                case "GR_IR":
                    sql = "SELECT * FROM [GR_IR_Account]";
                    dt = DBHelper.ExecuteQueryDT(sql);
                    this.dgv_项目.DataSource = dt;
                    break;
                case "质检库存":
                    sql = "SELECT * FROM [Check_Inventory_Account]";
                    dt = DBHelper.ExecuteQueryDT(sql);
                    this.dgv_项目.DataSource = dt;
                    break;
                case "报废库存":
                    sql = "SELECT * FROM [Scrap_Inventory_Account]";
                    dt = DBHelper.ExecuteQueryDT(sql);
                    this.dgv_项目.DataSource = dt;
                    break;
            }
        }

       
    }
}
