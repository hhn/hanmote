﻿namespace MMClient.MD.FI
{
    partial class Order
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgv_采购项目 = new System.Windows.Forms.DataGridView();
            this.cln_状态 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.cln_物料编码 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cln_物料描述 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cln_数量 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cln_单位 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cln_净价 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cln_价格确定编号 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cln_货币 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cln_物料组 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cln_工厂 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cln_库存地点 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cln_批次 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cln_请求号码 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cln_信息记录 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btn_提交订单 = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dgv_定价项目 = new System.Windows.Forms.DataGridView();
            this.cln_类型名称 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cln_金额 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cln_单位1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cln_定价值 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cln_货币1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cbb_项目 = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn20 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn21 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn22 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn23 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn24 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn26 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn27 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn28 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn29 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn30 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn31 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn32 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn33 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn34 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn35 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn36 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn37 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn38 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn39 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn40 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn41 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn42 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn43 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn44 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn45 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn46 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn47 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn48 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn49 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn50 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn51 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn52 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn53 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tpg_头数据 = new System.Windows.Forms.TabPage();
            this.lbRecordCode = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.btnQueryOrder = new System.Windows.Forms.Button();
            this.dtp_交货时间 = new System.Windows.Forms.DateTimePicker();
            this.label8 = new System.Windows.Forms.Label();
            this.cbb_订单状态 = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.tbx_总价值 = new System.Windows.Forms.TextBox();
            this.tbx_交货方式 = new System.Windows.Forms.TextBox();
            this.tbx_交货地点 = new System.Windows.Forms.TextBox();
            this.tbx_订单编码 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.dtp_创建订单时间 = new System.Windows.Forms.DateTimePicker();
            this.label9 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.cbb_订单类型 = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tpg_交货开票 = new System.Windows.Forms.TabPage();
            this.textBox18 = new System.Windows.Forms.TextBox();
            this.textBox17 = new System.Windows.Forms.TextBox();
            this.textBox16 = new System.Windows.Forms.TextBox();
            this.tbx_开发票金额 = new System.Windows.Forms.TextBox();
            this.textBox14 = new System.Windows.Forms.TextBox();
            this.tbx_开发票数量 = new System.Windows.Forms.TextBox();
            this.textBox12 = new System.Windows.Forms.TextBox();
            this.tbx_需交货金额 = new System.Windows.Forms.TextBox();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.tbx_需交货数量 = new System.Windows.Forms.TextBox();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.tbx_已交货金额 = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.tbx_已交货数量 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.tbx_已订购金额 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.tbx_已订购数量 = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.tpg_发票信息 = new System.Windows.Forms.TabPage();
            this.dgv_发票信息 = new System.Windows.Forms.DataGridView();
            this.Invoice_Code = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Invoice_Number = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Certificate_Type = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Certificate_Code = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Invoice_Makeout_Time = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Create_Time = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Payment_Type = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sum = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Currency = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Sum_Number = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tpg_收货信息 = new System.Windows.Forms.TabPage();
            this.dgv_收货信息 = new System.Windows.Forms.DataGridView();
            this.StockDocumentId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Order_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Delivery_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ReceiptNote_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Posting_Date = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Document_Date = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.StockManager = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Move_Type = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Reversed = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Total_Number = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Total_Value = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_采购项目)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_定价项目)).BeginInit();
            this.panel1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tpg_头数据.SuspendLayout();
            this.tpg_交货开票.SuspendLayout();
            this.tpg_发票信息.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_发票信息)).BeginInit();
            this.tpg_收货信息.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_收货信息)).BeginInit();
            this.SuspendLayout();
            // 
            // dgv_采购项目
            // 
            this.dgv_采购项目.AllowUserToAddRows = false;
            this.dgv_采购项目.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dgv_采购项目.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_采购项目.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.cln_状态,
            this.cln_物料编码,
            this.cln_物料描述,
            this.cln_数量,
            this.cln_单位,
            this.cln_净价,
            this.cln_价格确定编号,
            this.cln_货币,
            this.cln_物料组,
            this.cln_工厂,
            this.cln_库存地点,
            this.cln_批次,
            this.cln_请求号码,
            this.cln_信息记录});
            this.dgv_采购项目.Location = new System.Drawing.Point(0, 18);
            this.dgv_采购项目.Name = "dgv_采购项目";
            this.dgv_采购项目.RowTemplate.Height = 24;
            this.dgv_采购项目.Size = new System.Drawing.Size(1547, 254);
            this.dgv_采购项目.TabIndex = 0;
            // 
            // cln_状态
            // 
            this.cln_状态.HeaderText = "勾选";
            this.cln_状态.Name = "cln_状态";
            this.cln_状态.Width = 50;
            // 
            // cln_物料编码
            // 
            this.cln_物料编码.HeaderText = "物料编码";
            this.cln_物料编码.Name = "cln_物料编码";
            this.cln_物料编码.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.cln_物料编码.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.cln_物料编码.Width = 200;
            // 
            // cln_物料描述
            // 
            this.cln_物料描述.HeaderText = "物料描述";
            this.cln_物料描述.Name = "cln_物料描述";
            this.cln_物料描述.Width = 150;
            // 
            // cln_数量
            // 
            this.cln_数量.HeaderText = "数量";
            this.cln_数量.Name = "cln_数量";
            // 
            // cln_单位
            // 
            this.cln_单位.HeaderText = "单位";
            this.cln_单位.Name = "cln_单位";
            this.cln_单位.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.cln_单位.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // cln_净价
            // 
            this.cln_净价.HeaderText = "净价";
            this.cln_净价.Name = "cln_净价";
            // 
            // cln_价格确定编号
            // 
            this.cln_价格确定编号.HeaderText = "价格确定编号";
            this.cln_价格确定编号.Name = "cln_价格确定编号";
            this.cln_价格确定编号.Visible = false;
            // 
            // cln_货币
            // 
            this.cln_货币.HeaderText = "货币";
            this.cln_货币.Name = "cln_货币";
            this.cln_货币.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.cln_货币.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // cln_物料组
            // 
            this.cln_物料组.HeaderText = "物料组";
            this.cln_物料组.Name = "cln_物料组";
            this.cln_物料组.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.cln_物料组.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // cln_工厂
            // 
            this.cln_工厂.HeaderText = "工厂编号";
            this.cln_工厂.Name = "cln_工厂";
            // 
            // cln_库存地点
            // 
            this.cln_库存地点.HeaderText = "库存编号";
            this.cln_库存地点.Name = "cln_库存地点";
            // 
            // cln_批次
            // 
            this.cln_批次.HeaderText = "批次";
            this.cln_批次.Name = "cln_批次";
            // 
            // cln_请求号码
            // 
            this.cln_请求号码.HeaderText = "请求号码";
            this.cln_请求号码.Name = "cln_请求号码";
            // 
            // cln_信息记录
            // 
            this.cln_信息记录.HeaderText = "信息记录ID";
            this.cln_信息记录.Name = "cln_信息记录";
            this.cln_信息记录.Width = 130;
            // 
            // btn_提交订单
            // 
            this.btn_提交订单.Location = new System.Drawing.Point(761, 850);
            this.btn_提交订单.Name = "btn_提交订单";
            this.btn_提交订单.Size = new System.Drawing.Size(122, 37);
            this.btn_提交订单.TabIndex = 4;
            this.btn_提交订单.Text = "修改提交";
            this.btn_提交订单.UseVisualStyleBackColor = true;
            this.btn_提交订单.Click += new System.EventHandler(this.btn_提交订单_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dgv_定价项目);
            this.groupBox1.Controls.Add(this.cbb_项目);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Location = new System.Drawing.Point(9, 567);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1548, 260);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "定价信息";
            // 
            // dgv_定价项目
            // 
            this.dgv_定价项目.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_定价项目.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dgv_定价项目.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_定价项目.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.cln_类型名称,
            this.cln_金额,
            this.cln_单位1,
            this.cln_定价值,
            this.cln_货币1});
            this.dgv_定价项目.Location = new System.Drawing.Point(0, 43);
            this.dgv_定价项目.Name = "dgv_定价项目";
            this.dgv_定价项目.RowTemplate.Height = 24;
            this.dgv_定价项目.Size = new System.Drawing.Size(1544, 211);
            this.dgv_定价项目.TabIndex = 0;
            // 
            // cln_类型名称
            // 
            this.cln_类型名称.HeaderText = "类型名称";
            this.cln_类型名称.Name = "cln_类型名称";
            // 
            // cln_金额
            // 
            this.cln_金额.HeaderText = "金额";
            this.cln_金额.Name = "cln_金额";
            // 
            // cln_单位1
            // 
            this.cln_单位1.HeaderText = "单位";
            this.cln_单位1.Name = "cln_单位1";
            // 
            // cln_定价值
            // 
            this.cln_定价值.HeaderText = "定价值";
            this.cln_定价值.Name = "cln_定价值";
            // 
            // cln_货币1
            // 
            this.cln_货币1.HeaderText = "货币";
            this.cln_货币1.Name = "cln_货币1";
            // 
            // cbb_项目
            // 
            this.cbb_项目.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbb_项目.FormattingEnabled = true;
            this.cbb_项目.Location = new System.Drawing.Point(101, 16);
            this.cbb_项目.Name = "cbb_项目";
            this.cbb_项目.Size = new System.Drawing.Size(175, 21);
            this.cbb_项目.TabIndex = 6;
            this.cbb_项目.TextChanged += new System.EventHandler(this.cbb_项目_TextChanged);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(16, 19);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(79, 13);
            this.label15.TabIndex = 5;
            this.label15.Text = "确认定价编号";
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "物料编码";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn1.Width = 136;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "物料描述";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.Width = 136;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "数量";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.Width = 136;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.HeaderText = "单位";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn4.Width = 136;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.HeaderText = "净价";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.Width = 136;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.HeaderText = "货币";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn6.Width = 99;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.HeaderText = "物料组";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn7.Width = 99;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.HeaderText = "工厂";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            this.dataGridViewTextBoxColumn8.Width = 99;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.HeaderText = "库存地点";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.ReadOnly = true;
            this.dataGridViewTextBoxColumn9.Width = 99;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.HeaderText = "批次";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.Width = 99;
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.HeaderText = "请求号码";
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            this.dataGridViewTextBoxColumn11.Width = 99;
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.HeaderText = "信息记录";
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            this.dataGridViewTextBoxColumn12.Width = 99;
            // 
            // dataGridViewTextBoxColumn13
            // 
            this.dataGridViewTextBoxColumn13.HeaderText = "工厂";
            this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
            this.dataGridViewTextBoxColumn13.Width = 99;
            // 
            // dataGridViewTextBoxColumn14
            // 
            this.dataGridViewTextBoxColumn14.HeaderText = "库存地";
            this.dataGridViewTextBoxColumn14.Name = "dataGridViewTextBoxColumn14";
            this.dataGridViewTextBoxColumn14.Width = 99;
            // 
            // dataGridViewTextBoxColumn15
            // 
            this.dataGridViewTextBoxColumn15.HeaderText = "发票代码";
            this.dataGridViewTextBoxColumn15.Name = "dataGridViewTextBoxColumn15";
            this.dataGridViewTextBoxColumn15.Width = 122;
            // 
            // dataGridViewTextBoxColumn16
            // 
            this.dataGridViewTextBoxColumn16.HeaderText = "发票号码";
            this.dataGridViewTextBoxColumn16.Name = "dataGridViewTextBoxColumn16";
            this.dataGridViewTextBoxColumn16.Width = 122;
            // 
            // dataGridViewTextBoxColumn17
            // 
            this.dataGridViewTextBoxColumn17.HeaderText = "订单类型";
            this.dataGridViewTextBoxColumn17.Name = "dataGridViewTextBoxColumn17";
            this.dataGridViewTextBoxColumn17.Width = 121;
            // 
            // dataGridViewTextBoxColumn18
            // 
            this.dataGridViewTextBoxColumn18.HeaderText = "订单编码";
            this.dataGridViewTextBoxColumn18.Name = "dataGridViewTextBoxColumn18";
            this.dataGridViewTextBoxColumn18.Width = 122;
            // 
            // dataGridViewTextBoxColumn19
            // 
            this.dataGridViewTextBoxColumn19.HeaderText = "开票日期";
            this.dataGridViewTextBoxColumn19.Name = "dataGridViewTextBoxColumn19";
            this.dataGridViewTextBoxColumn19.Width = 122;
            // 
            // dataGridViewTextBoxColumn20
            // 
            this.dataGridViewTextBoxColumn20.HeaderText = "创建日期";
            this.dataGridViewTextBoxColumn20.Name = "dataGridViewTextBoxColumn20";
            this.dataGridViewTextBoxColumn20.Width = 122;
            // 
            // dataGridViewTextBoxColumn21
            // 
            this.dataGridViewTextBoxColumn21.HeaderText = "付款方式";
            this.dataGridViewTextBoxColumn21.Name = "dataGridViewTextBoxColumn21";
            this.dataGridViewTextBoxColumn21.Width = 122;
            // 
            // dataGridViewTextBoxColumn22
            // 
            this.dataGridViewTextBoxColumn22.HeaderText = "发票总金额";
            this.dataGridViewTextBoxColumn22.Name = "dataGridViewTextBoxColumn22";
            this.dataGridViewTextBoxColumn22.Width = 121;
            // 
            // dataGridViewTextBoxColumn23
            // 
            this.dataGridViewTextBoxColumn23.HeaderText = "货币单位";
            this.dataGridViewTextBoxColumn23.Name = "dataGridViewTextBoxColumn23";
            this.dataGridViewTextBoxColumn23.Width = 122;
            // 
            // dataGridViewTextBoxColumn24
            // 
            this.dataGridViewTextBoxColumn24.HeaderText = "发票总数量";
            this.dataGridViewTextBoxColumn24.Name = "dataGridViewTextBoxColumn24";
            this.dataGridViewTextBoxColumn24.Width = 122;
            // 
            // dataGridViewTextBoxColumn25
            // 
            this.dataGridViewTextBoxColumn25.HeaderText = "凭证编码";
            this.dataGridViewTextBoxColumn25.Name = "dataGridViewTextBoxColumn25";
            this.dataGridViewTextBoxColumn25.Width = 110;
            // 
            // dataGridViewTextBoxColumn26
            // 
            this.dataGridViewTextBoxColumn26.HeaderText = "订单号";
            this.dataGridViewTextBoxColumn26.Name = "dataGridViewTextBoxColumn26";
            this.dataGridViewTextBoxColumn26.Width = 111;
            // 
            // dataGridViewTextBoxColumn27
            // 
            this.dataGridViewTextBoxColumn27.HeaderText = "提货单号";
            this.dataGridViewTextBoxColumn27.Name = "dataGridViewTextBoxColumn27";
            this.dataGridViewTextBoxColumn27.Width = 110;
            // 
            // dataGridViewTextBoxColumn28
            // 
            this.dataGridViewTextBoxColumn28.HeaderText = "交货单号";
            this.dataGridViewTextBoxColumn28.Name = "dataGridViewTextBoxColumn28";
            this.dataGridViewTextBoxColumn28.Width = 111;
            // 
            // dataGridViewTextBoxColumn29
            // 
            this.dataGridViewTextBoxColumn29.HeaderText = "记账日期";
            this.dataGridViewTextBoxColumn29.Name = "dataGridViewTextBoxColumn29";
            this.dataGridViewTextBoxColumn29.Width = 110;
            // 
            // dataGridViewTextBoxColumn30
            // 
            this.dataGridViewTextBoxColumn30.HeaderText = "凭证日期";
            this.dataGridViewTextBoxColumn30.Name = "dataGridViewTextBoxColumn30";
            this.dataGridViewTextBoxColumn30.Width = 111;
            // 
            // dataGridViewTextBoxColumn31
            // 
            this.dataGridViewTextBoxColumn31.HeaderText = "收货人";
            this.dataGridViewTextBoxColumn31.Name = "dataGridViewTextBoxColumn31";
            this.dataGridViewTextBoxColumn31.Width = 110;
            // 
            // dataGridViewTextBoxColumn32
            // 
            this.dataGridViewTextBoxColumn32.HeaderText = "移动类型";
            this.dataGridViewTextBoxColumn32.Name = "dataGridViewTextBoxColumn32";
            this.dataGridViewTextBoxColumn32.Width = 111;
            // 
            // dataGridViewTextBoxColumn33
            // 
            this.dataGridViewTextBoxColumn33.HeaderText = "是否冲销";
            this.dataGridViewTextBoxColumn33.Name = "dataGridViewTextBoxColumn33";
            this.dataGridViewTextBoxColumn33.Width = 110;
            // 
            // dataGridViewTextBoxColumn34
            // 
            this.dataGridViewTextBoxColumn34.HeaderText = "收货总数量";
            this.dataGridViewTextBoxColumn34.Name = "dataGridViewTextBoxColumn34";
            this.dataGridViewTextBoxColumn34.Width = 111;
            // 
            // dataGridViewTextBoxColumn35
            // 
            this.dataGridViewTextBoxColumn35.HeaderText = "收货总价值";
            this.dataGridViewTextBoxColumn35.Name = "dataGridViewTextBoxColumn35";
            this.dataGridViewTextBoxColumn35.Width = 110;
            // 
            // dataGridViewTextBoxColumn36
            // 
            this.dataGridViewTextBoxColumn36.HeaderText = "物料编码";
            this.dataGridViewTextBoxColumn36.Name = "dataGridViewTextBoxColumn36";
            this.dataGridViewTextBoxColumn36.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn36.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn37
            // 
            this.dataGridViewTextBoxColumn37.HeaderText = "物料描述";
            this.dataGridViewTextBoxColumn37.Name = "dataGridViewTextBoxColumn37";
            // 
            // dataGridViewTextBoxColumn38
            // 
            this.dataGridViewTextBoxColumn38.HeaderText = "数量";
            this.dataGridViewTextBoxColumn38.Name = "dataGridViewTextBoxColumn38";
            // 
            // dataGridViewTextBoxColumn39
            // 
            this.dataGridViewTextBoxColumn39.HeaderText = "单位";
            this.dataGridViewTextBoxColumn39.Name = "dataGridViewTextBoxColumn39";
            this.dataGridViewTextBoxColumn39.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn39.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn40
            // 
            this.dataGridViewTextBoxColumn40.HeaderText = "净价";
            this.dataGridViewTextBoxColumn40.Name = "dataGridViewTextBoxColumn40";
            // 
            // dataGridViewTextBoxColumn41
            // 
            this.dataGridViewTextBoxColumn41.HeaderText = "价格确定编号";
            this.dataGridViewTextBoxColumn41.Name = "dataGridViewTextBoxColumn41";
            this.dataGridViewTextBoxColumn41.Visible = false;
            // 
            // dataGridViewTextBoxColumn42
            // 
            this.dataGridViewTextBoxColumn42.HeaderText = "货币";
            this.dataGridViewTextBoxColumn42.Name = "dataGridViewTextBoxColumn42";
            this.dataGridViewTextBoxColumn42.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn42.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn43
            // 
            this.dataGridViewTextBoxColumn43.HeaderText = "物料组";
            this.dataGridViewTextBoxColumn43.Name = "dataGridViewTextBoxColumn43";
            this.dataGridViewTextBoxColumn43.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn43.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn44
            // 
            this.dataGridViewTextBoxColumn44.HeaderText = "工厂";
            this.dataGridViewTextBoxColumn44.Name = "dataGridViewTextBoxColumn44";
            // 
            // dataGridViewTextBoxColumn45
            // 
            this.dataGridViewTextBoxColumn45.HeaderText = "库存地点";
            this.dataGridViewTextBoxColumn45.Name = "dataGridViewTextBoxColumn45";
            // 
            // dataGridViewTextBoxColumn46
            // 
            this.dataGridViewTextBoxColumn46.HeaderText = "批次";
            this.dataGridViewTextBoxColumn46.Name = "dataGridViewTextBoxColumn46";
            // 
            // dataGridViewTextBoxColumn47
            // 
            this.dataGridViewTextBoxColumn47.HeaderText = "请求号码";
            this.dataGridViewTextBoxColumn47.Name = "dataGridViewTextBoxColumn47";
            // 
            // dataGridViewTextBoxColumn48
            // 
            this.dataGridViewTextBoxColumn48.HeaderText = "信息记录";
            this.dataGridViewTextBoxColumn48.Name = "dataGridViewTextBoxColumn48";
            // 
            // dataGridViewTextBoxColumn49
            // 
            this.dataGridViewTextBoxColumn49.HeaderText = "类型名称";
            this.dataGridViewTextBoxColumn49.Name = "dataGridViewTextBoxColumn49";
            this.dataGridViewTextBoxColumn49.Width = 232;
            // 
            // dataGridViewTextBoxColumn50
            // 
            this.dataGridViewTextBoxColumn50.HeaderText = "金额";
            this.dataGridViewTextBoxColumn50.Name = "dataGridViewTextBoxColumn50";
            this.dataGridViewTextBoxColumn50.Width = 233;
            // 
            // dataGridViewTextBoxColumn51
            // 
            this.dataGridViewTextBoxColumn51.HeaderText = "单位";
            this.dataGridViewTextBoxColumn51.Name = "dataGridViewTextBoxColumn51";
            this.dataGridViewTextBoxColumn51.Width = 232;
            // 
            // dataGridViewTextBoxColumn52
            // 
            this.dataGridViewTextBoxColumn52.HeaderText = "定价值";
            this.dataGridViewTextBoxColumn52.Name = "dataGridViewTextBoxColumn52";
            this.dataGridViewTextBoxColumn52.Width = 233;
            // 
            // dataGridViewTextBoxColumn53
            // 
            this.dataGridViewTextBoxColumn53.HeaderText = "货币";
            this.dataGridViewTextBoxColumn53.Name = "dataGridViewTextBoxColumn53";
            this.dataGridViewTextBoxColumn53.Width = 232;
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.Controls.Add(this.groupBox2);
            this.panel1.Controls.Add(this.btn_提交订单);
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Controls.Add(this.tabControl1);
            this.panel1.Location = new System.Drawing.Point(12, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1577, 907);
            this.panel1.TabIndex = 6;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dgv_采购项目);
            this.groupBox2.Location = new System.Drawing.Point(6, 283);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1551, 278);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "订单条目";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tpg_头数据);
            this.tabControl1.Controls.Add(this.tpg_交货开票);
            this.tabControl1.Controls.Add(this.tpg_发票信息);
            this.tabControl1.Controls.Add(this.tpg_收货信息);
            this.tabControl1.Location = new System.Drawing.Point(6, 9);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1551, 272);
            this.tabControl1.TabIndex = 1;
            // 
            // tpg_头数据
            // 
            this.tpg_头数据.Controls.Add(this.lbRecordCode);
            this.tpg_头数据.Controls.Add(this.button1);
            this.tpg_头数据.Controls.Add(this.btnQueryOrder);
            this.tpg_头数据.Controls.Add(this.dtp_交货时间);
            this.tpg_头数据.Controls.Add(this.label8);
            this.tpg_头数据.Controls.Add(this.cbb_订单状态);
            this.tpg_头数据.Controls.Add(this.label7);
            this.tpg_头数据.Controls.Add(this.tbx_总价值);
            this.tpg_头数据.Controls.Add(this.tbx_交货方式);
            this.tpg_头数据.Controls.Add(this.tbx_交货地点);
            this.tpg_头数据.Controls.Add(this.tbx_订单编码);
            this.tpg_头数据.Controls.Add(this.label5);
            this.tpg_头数据.Controls.Add(this.label11);
            this.tpg_头数据.Controls.Add(this.label10);
            this.tpg_头数据.Controls.Add(this.dtp_创建订单时间);
            this.tpg_头数据.Controls.Add(this.label9);
            this.tpg_头数据.Controls.Add(this.label6);
            this.tpg_头数据.Controls.Add(this.cbb_订单类型);
            this.tpg_头数据.Controls.Add(this.label3);
            this.tpg_头数据.Controls.Add(this.label4);
            this.tpg_头数据.Location = new System.Drawing.Point(4, 22);
            this.tpg_头数据.Name = "tpg_头数据";
            this.tpg_头数据.Padding = new System.Windows.Forms.Padding(3);
            this.tpg_头数据.Size = new System.Drawing.Size(1543, 246);
            this.tpg_头数据.TabIndex = 0;
            this.tpg_头数据.Text = "订单头数据";
            this.tpg_头数据.UseVisualStyleBackColor = true;
            // 
            // lbRecordCode
            // 
            this.lbRecordCode.AutoSize = true;
            this.lbRecordCode.ForeColor = System.Drawing.Color.Red;
            this.lbRecordCode.Location = new System.Drawing.Point(80, 157);
            this.lbRecordCode.Name = "lbRecordCode";
            this.lbRecordCode.Size = new System.Drawing.Size(96, 13);
            this.lbRecordCode.TabIndex = 29;
            this.lbRecordCode.Text = "已参照记录ID: 无";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(83, 120);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(154, 23);
            this.button1.TabIndex = 28;
            this.button1.Text = "选择参照记录";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnQueryOrder
            // 
            this.btnQueryOrder.Location = new System.Drawing.Point(13, 17);
            this.btnQueryOrder.Name = "btnQueryOrder";
            this.btnQueryOrder.Size = new System.Drawing.Size(143, 24);
            this.btnQueryOrder.TabIndex = 27;
            this.btnQueryOrder.Text = "选择一个订单";
            this.btnQueryOrder.UseVisualStyleBackColor = true;
            this.btnQueryOrder.Click += new System.EventHandler(this.btnQueryOrder_Click);
            // 
            // dtp_交货时间
            // 
            this.dtp_交货时间.Location = new System.Drawing.Point(871, 196);
            this.dtp_交货时间.Name = "dtp_交货时间";
            this.dtp_交货时间.Size = new System.Drawing.Size(200, 19);
            this.dtp_交货时间.TabIndex = 26;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(804, 201);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(55, 13);
            this.label8.TabIndex = 25;
            this.label8.Text = "交货时间";
            // 
            // cbb_订单状态
            // 
            this.cbb_订单状态.FormattingEnabled = true;
            this.cbb_订单状态.Items.AddRange(new object[] {
            "待审批",
            "执行中",
            "已完成",
            "已取消"});
            this.cbb_订单状态.Location = new System.Drawing.Point(871, 121);
            this.cbb_订单状态.Name = "cbb_订单状态";
            this.cbb_订单状态.Size = new System.Drawing.Size(200, 21);
            this.cbb_订单状态.TabIndex = 24;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(804, 124);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(55, 13);
            this.label7.TabIndex = 23;
            this.label7.Text = "订单状态";
            // 
            // tbx_总价值
            // 
            this.tbx_总价值.Location = new System.Drawing.Point(871, 55);
            this.tbx_总价值.Name = "tbx_总价值";
            this.tbx_总价值.Size = new System.Drawing.Size(200, 19);
            this.tbx_总价值.TabIndex = 22;
            // 
            // tbx_交货方式
            // 
            this.tbx_交货方式.Location = new System.Drawing.Point(487, 198);
            this.tbx_交货方式.Name = "tbx_交货方式";
            this.tbx_交货方式.Size = new System.Drawing.Size(200, 19);
            this.tbx_交货方式.TabIndex = 20;
            // 
            // tbx_交货地点
            // 
            this.tbx_交货地点.Location = new System.Drawing.Point(487, 124);
            this.tbx_交货地点.Name = "tbx_交货地点";
            this.tbx_交货地点.Size = new System.Drawing.Size(200, 19);
            this.tbx_交货地点.TabIndex = 18;
            // 
            // tbx_订单编码
            // 
            this.tbx_订单编码.Location = new System.Drawing.Point(83, 57);
            this.tbx_订单编码.Name = "tbx_订单编码";
            this.tbx_订单编码.Size = new System.Drawing.Size(156, 19);
            this.tbx_订单编码.TabIndex = 1;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(804, 57);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(43, 13);
            this.label5.TabIndex = 21;
            this.label5.Text = "总价值";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(389, 201);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(55, 13);
            this.label11.TabIndex = 19;
            this.label11.Text = "交货方式";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(389, 127);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(55, 13);
            this.label10.TabIndex = 17;
            this.label10.Text = "交货地点";
            // 
            // dtp_创建订单时间
            // 
            this.dtp_创建订单时间.Location = new System.Drawing.Point(487, 57);
            this.dtp_创建订单时间.Name = "dtp_创建订单时间";
            this.dtp_创建订单时间.Size = new System.Drawing.Size(200, 19);
            this.dtp_创建订单时间.TabIndex = 16;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(389, 60);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(79, 13);
            this.label9.TabIndex = 15;
            this.label9.Text = "创建订单时间";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(13, 124);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(31, 13);
            this.label6.TabIndex = 7;
            this.label6.Text = "参照";
            // 
            // cbb_订单类型
            // 
            this.cbb_订单类型.FormattingEnabled = true;
            this.cbb_订单类型.Items.AddRange(new object[] {
            "标准采购订单",
            "寄售",
            "委外加工"});
            this.cbb_订单类型.Location = new System.Drawing.Point(83, 198);
            this.cbb_订单类型.Name = "cbb_订单类型";
            this.cbb_订单类型.Size = new System.Drawing.Size(156, 21);
            this.cbb_订单类型.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 201);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "订单类型";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 57);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(55, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "订单编码";
            // 
            // tpg_交货开票
            // 
            this.tpg_交货开票.Controls.Add(this.textBox18);
            this.tpg_交货开票.Controls.Add(this.textBox17);
            this.tpg_交货开票.Controls.Add(this.textBox16);
            this.tpg_交货开票.Controls.Add(this.tbx_开发票金额);
            this.tpg_交货开票.Controls.Add(this.textBox14);
            this.tpg_交货开票.Controls.Add(this.tbx_开发票数量);
            this.tpg_交货开票.Controls.Add(this.textBox12);
            this.tpg_交货开票.Controls.Add(this.tbx_需交货金额);
            this.tpg_交货开票.Controls.Add(this.textBox10);
            this.tpg_交货开票.Controls.Add(this.tbx_需交货数量);
            this.tpg_交货开票.Controls.Add(this.textBox8);
            this.tpg_交货开票.Controls.Add(this.tbx_已交货金额);
            this.tpg_交货开票.Controls.Add(this.textBox6);
            this.tpg_交货开票.Controls.Add(this.tbx_已交货数量);
            this.tpg_交货开票.Controls.Add(this.textBox4);
            this.tpg_交货开票.Controls.Add(this.tbx_已订购金额);
            this.tpg_交货开票.Controls.Add(this.textBox2);
            this.tpg_交货开票.Controls.Add(this.tbx_已订购数量);
            this.tpg_交货开票.Controls.Add(this.label20);
            this.tpg_交货开票.Controls.Add(this.label19);
            this.tpg_交货开票.Controls.Add(this.label18);
            this.tpg_交货开票.Controls.Add(this.label16);
            this.tpg_交货开票.Controls.Add(this.label17);
            this.tpg_交货开票.Location = new System.Drawing.Point(4, 22);
            this.tpg_交货开票.Name = "tpg_交货开票";
            this.tpg_交货开票.Padding = new System.Windows.Forms.Padding(3);
            this.tpg_交货开票.Size = new System.Drawing.Size(1543, 246);
            this.tpg_交货开票.TabIndex = 4;
            this.tpg_交货开票.Text = "交货状态/开票状态";
            this.tpg_交货开票.UseVisualStyleBackColor = true;
            // 
            // textBox18
            // 
            this.textBox18.Location = new System.Drawing.Point(680, 69);
            this.textBox18.Name = "textBox18";
            this.textBox18.Size = new System.Drawing.Size(37, 19);
            this.textBox18.TabIndex = 23;
            this.textBox18.Text = "RMB";
            // 
            // textBox17
            // 
            this.textBox17.Location = new System.Drawing.Point(574, 69);
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new System.Drawing.Size(100, 19);
            this.textBox17.TabIndex = 22;
            this.textBox17.Text = "0";
            // 
            // textBox16
            // 
            this.textBox16.Location = new System.Drawing.Point(859, 20);
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new System.Drawing.Size(37, 19);
            this.textBox16.TabIndex = 20;
            this.textBox16.Text = "RMB";
            // 
            // tbx_开发票金额
            // 
            this.tbx_开发票金额.Location = new System.Drawing.Point(753, 20);
            this.tbx_开发票金额.Name = "tbx_开发票金额";
            this.tbx_开发票金额.Size = new System.Drawing.Size(100, 19);
            this.tbx_开发票金额.TabIndex = 19;
            this.tbx_开发票金额.Text = "0";
            // 
            // textBox14
            // 
            this.textBox14.Location = new System.Drawing.Point(680, 20);
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new System.Drawing.Size(27, 19);
            this.textBox14.TabIndex = 18;
            this.textBox14.Text = "PC";
            // 
            // tbx_开发票数量
            // 
            this.tbx_开发票数量.Location = new System.Drawing.Point(574, 20);
            this.tbx_开发票数量.Name = "tbx_开发票数量";
            this.tbx_开发票数量.Size = new System.Drawing.Size(100, 19);
            this.tbx_开发票数量.TabIndex = 17;
            this.tbx_开发票数量.Text = "0";
            // 
            // textBox12
            // 
            this.textBox12.Location = new System.Drawing.Point(385, 129);
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new System.Drawing.Size(40, 19);
            this.textBox12.TabIndex = 15;
            this.textBox12.Text = "RMB";
            // 
            // tbx_需交货金额
            // 
            this.tbx_需交货金额.Location = new System.Drawing.Point(279, 129);
            this.tbx_需交货金额.Name = "tbx_需交货金额";
            this.tbx_需交货金额.Size = new System.Drawing.Size(100, 19);
            this.tbx_需交货金额.TabIndex = 14;
            this.tbx_需交货金额.Text = "0";
            // 
            // textBox10
            // 
            this.textBox10.Location = new System.Drawing.Point(199, 129);
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new System.Drawing.Size(27, 19);
            this.textBox10.TabIndex = 13;
            this.textBox10.Text = "PC";
            // 
            // tbx_需交货数量
            // 
            this.tbx_需交货数量.Location = new System.Drawing.Point(93, 129);
            this.tbx_需交货数量.Name = "tbx_需交货数量";
            this.tbx_需交货数量.Size = new System.Drawing.Size(100, 19);
            this.tbx_需交货数量.TabIndex = 12;
            this.tbx_需交货数量.Text = "0";
            // 
            // textBox8
            // 
            this.textBox8.Location = new System.Drawing.Point(385, 72);
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(40, 19);
            this.textBox8.TabIndex = 10;
            this.textBox8.Text = "RMB";
            // 
            // tbx_已交货金额
            // 
            this.tbx_已交货金额.Location = new System.Drawing.Point(279, 72);
            this.tbx_已交货金额.Name = "tbx_已交货金额";
            this.tbx_已交货金额.Size = new System.Drawing.Size(100, 19);
            this.tbx_已交货金额.TabIndex = 9;
            this.tbx_已交货金额.Text = "0";
            // 
            // textBox6
            // 
            this.textBox6.Location = new System.Drawing.Point(199, 72);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(27, 19);
            this.textBox6.TabIndex = 8;
            this.textBox6.Text = "PC";
            // 
            // tbx_已交货数量
            // 
            this.tbx_已交货数量.Location = new System.Drawing.Point(93, 72);
            this.tbx_已交货数量.Name = "tbx_已交货数量";
            this.tbx_已交货数量.Size = new System.Drawing.Size(100, 19);
            this.tbx_已交货数量.TabIndex = 7;
            this.tbx_已交货数量.Text = "0";
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(385, 20);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(40, 19);
            this.textBox4.TabIndex = 5;
            this.textBox4.Text = "RMB";
            // 
            // tbx_已订购金额
            // 
            this.tbx_已订购金额.Location = new System.Drawing.Point(279, 20);
            this.tbx_已订购金额.Name = "tbx_已订购金额";
            this.tbx_已订购金额.Size = new System.Drawing.Size(100, 19);
            this.tbx_已订购金额.TabIndex = 4;
            this.tbx_已订购金额.Text = "0";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(199, 20);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(27, 19);
            this.textBox2.TabIndex = 3;
            this.textBox2.Text = "PC";
            // 
            // tbx_已订购数量
            // 
            this.tbx_已订购数量.Location = new System.Drawing.Point(93, 20);
            this.tbx_已订购数量.Name = "tbx_已订购数量";
            this.tbx_已订购数量.Size = new System.Drawing.Size(100, 19);
            this.tbx_已订购数量.TabIndex = 2;
            this.tbx_已订购数量.Text = "0";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(491, 75);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(55, 13);
            this.label20.TabIndex = 21;
            this.label20.Text = "预付定金";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(491, 23);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(43, 13);
            this.label19.TabIndex = 16;
            this.label19.Text = "开发票";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(20, 132);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(55, 13);
            this.label18.TabIndex = 11;
            this.label18.Text = "仍要交货";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(24, 77);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(43, 13);
            this.label16.TabIndex = 6;
            this.label16.Text = "已交货";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(24, 20);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(43, 13);
            this.label17.TabIndex = 1;
            this.label17.Text = "已订购";
            // 
            // tpg_发票信息
            // 
            this.tpg_发票信息.Controls.Add(this.dgv_发票信息);
            this.tpg_发票信息.Location = new System.Drawing.Point(4, 22);
            this.tpg_发票信息.Name = "tpg_发票信息";
            this.tpg_发票信息.Size = new System.Drawing.Size(1543, 246);
            this.tpg_发票信息.TabIndex = 5;
            this.tpg_发票信息.Text = "查看发票信息";
            this.tpg_发票信息.UseVisualStyleBackColor = true;
            // 
            // dgv_发票信息
            // 
            this.dgv_发票信息.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_发票信息.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dgv_发票信息.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_发票信息.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Invoice_Code,
            this.Invoice_Number,
            this.Certificate_Type,
            this.Certificate_Code,
            this.Invoice_Makeout_Time,
            this.Create_Time,
            this.Payment_Type,
            this.sum,
            this.Currency,
            this.Sum_Number});
            this.dgv_发票信息.Location = new System.Drawing.Point(0, 3);
            this.dgv_发票信息.Name = "dgv_发票信息";
            this.dgv_发票信息.RowTemplate.Height = 24;
            this.dgv_发票信息.Size = new System.Drawing.Size(1543, 243);
            this.dgv_发票信息.TabIndex = 0;
            // 
            // Invoice_Code
            // 
            this.Invoice_Code.HeaderText = "发票代码";
            this.Invoice_Code.Name = "Invoice_Code";
            // 
            // Invoice_Number
            // 
            this.Invoice_Number.HeaderText = "发票号码";
            this.Invoice_Number.Name = "Invoice_Number";
            // 
            // Certificate_Type
            // 
            this.Certificate_Type.HeaderText = "订单类型";
            this.Certificate_Type.Name = "Certificate_Type";
            // 
            // Certificate_Code
            // 
            this.Certificate_Code.HeaderText = "订单编码";
            this.Certificate_Code.Name = "Certificate_Code";
            // 
            // Invoice_Makeout_Time
            // 
            this.Invoice_Makeout_Time.HeaderText = "开票日期";
            this.Invoice_Makeout_Time.Name = "Invoice_Makeout_Time";
            // 
            // Create_Time
            // 
            this.Create_Time.HeaderText = "创建日期";
            this.Create_Time.Name = "Create_Time";
            // 
            // Payment_Type
            // 
            this.Payment_Type.HeaderText = "付款方式";
            this.Payment_Type.Name = "Payment_Type";
            // 
            // sum
            // 
            this.sum.HeaderText = "发票总金额";
            this.sum.Name = "sum";
            // 
            // Currency
            // 
            this.Currency.HeaderText = "货币单位";
            this.Currency.Name = "Currency";
            // 
            // Sum_Number
            // 
            this.Sum_Number.HeaderText = "发票总数量";
            this.Sum_Number.Name = "Sum_Number";
            // 
            // tpg_收货信息
            // 
            this.tpg_收货信息.Controls.Add(this.dgv_收货信息);
            this.tpg_收货信息.Location = new System.Drawing.Point(4, 22);
            this.tpg_收货信息.Name = "tpg_收货信息";
            this.tpg_收货信息.Size = new System.Drawing.Size(1543, 246);
            this.tpg_收货信息.TabIndex = 6;
            this.tpg_收货信息.Text = "查看收货信息";
            this.tpg_收货信息.UseVisualStyleBackColor = true;
            // 
            // dgv_收货信息
            // 
            this.dgv_收货信息.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_收货信息.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dgv_收货信息.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_收货信息.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.StockDocumentId,
            this.Order_ID,
            this.Delivery_ID,
            this.ReceiptNote_ID,
            this.Posting_Date,
            this.Document_Date,
            this.StockManager,
            this.Move_Type,
            this.Reversed,
            this.Total_Number,
            this.Total_Value});
            this.dgv_收货信息.Location = new System.Drawing.Point(4, 1);
            this.dgv_收货信息.Name = "dgv_收货信息";
            this.dgv_收货信息.RowTemplate.Height = 24;
            this.dgv_收货信息.Size = new System.Drawing.Size(1536, 315);
            this.dgv_收货信息.TabIndex = 0;
            // 
            // StockDocumentId
            // 
            this.StockDocumentId.HeaderText = "凭证编码";
            this.StockDocumentId.Name = "StockDocumentId";
            // 
            // Order_ID
            // 
            this.Order_ID.HeaderText = "订单号";
            this.Order_ID.Name = "Order_ID";
            // 
            // Delivery_ID
            // 
            this.Delivery_ID.HeaderText = "提货单号";
            this.Delivery_ID.Name = "Delivery_ID";
            // 
            // ReceiptNote_ID
            // 
            this.ReceiptNote_ID.HeaderText = "交货单号";
            this.ReceiptNote_ID.Name = "ReceiptNote_ID";
            // 
            // Posting_Date
            // 
            this.Posting_Date.HeaderText = "记账日期";
            this.Posting_Date.Name = "Posting_Date";
            // 
            // Document_Date
            // 
            this.Document_Date.HeaderText = "凭证日期";
            this.Document_Date.Name = "Document_Date";
            // 
            // StockManager
            // 
            this.StockManager.HeaderText = "收货人";
            this.StockManager.Name = "StockManager";
            // 
            // Move_Type
            // 
            this.Move_Type.HeaderText = "移动类型";
            this.Move_Type.Name = "Move_Type";
            // 
            // Reversed
            // 
            this.Reversed.HeaderText = "是否冲销";
            this.Reversed.Name = "Reversed";
            // 
            // Total_Number
            // 
            this.Total_Number.HeaderText = "收货总数量";
            this.Total_Number.Name = "Total_Number";
            // 
            // Total_Value
            // 
            this.Total_Value.HeaderText = "收货总价值";
            this.Total_Value.Name = "Total_Value";
            // 
            // Order
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1611, 914);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "Order";
            this.Text = "采购订单";
            ((System.ComponentModel.ISupportInitialize)(this.dgv_采购项目)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_定价项目)).EndInit();
            this.panel1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tpg_头数据.ResumeLayout(false);
            this.tpg_头数据.PerformLayout();
            this.tpg_交货开票.ResumeLayout(false);
            this.tpg_交货开票.PerformLayout();
            this.tpg_发票信息.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_发票信息)).EndInit();
            this.tpg_收货信息.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_收货信息)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.DataGridView dgv_采购项目;
        private System.Windows.Forms.Button btn_提交订单;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private System.Windows.Forms.GroupBox groupBox1;
        public System.Windows.Forms.ComboBox cbb_项目;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn14;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn15;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn16;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn17;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn18;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn19;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn20;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn21;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn22;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn23;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn24;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn25;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn26;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn27;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn28;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn29;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn30;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn31;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn32;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn33;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn34;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn35;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn36;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn37;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn38;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn39;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn40;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn41;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn42;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn43;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn44;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn45;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn46;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn47;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn48;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn49;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn50;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn51;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn52;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn53;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridView dgv_定价项目;
        private System.Windows.Forms.DataGridViewTextBoxColumn cln_类型名称;
        private System.Windows.Forms.DataGridViewTextBoxColumn cln_金额;
        private System.Windows.Forms.DataGridViewTextBoxColumn cln_单位1;
        private System.Windows.Forms.DataGridViewTextBoxColumn cln_定价值;
        private System.Windows.Forms.DataGridViewTextBoxColumn cln_货币1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridViewCheckBoxColumn cln_状态;
        private System.Windows.Forms.DataGridViewTextBoxColumn cln_物料编码;
        private System.Windows.Forms.DataGridViewTextBoxColumn cln_物料描述;
        private System.Windows.Forms.DataGridViewTextBoxColumn cln_数量;
        private System.Windows.Forms.DataGridViewTextBoxColumn cln_单位;
        private System.Windows.Forms.DataGridViewTextBoxColumn cln_净价;
        private System.Windows.Forms.DataGridViewTextBoxColumn cln_价格确定编号;
        private System.Windows.Forms.DataGridViewTextBoxColumn cln_货币;
        private System.Windows.Forms.DataGridViewTextBoxColumn cln_物料组;
        private System.Windows.Forms.DataGridViewTextBoxColumn cln_工厂;
        private System.Windows.Forms.DataGridViewTextBoxColumn cln_库存地点;
        private System.Windows.Forms.DataGridViewTextBoxColumn cln_批次;
        private System.Windows.Forms.DataGridViewTextBoxColumn cln_请求号码;
        private System.Windows.Forms.DataGridViewTextBoxColumn cln_信息记录;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tpg_头数据;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnQueryOrder;
        private System.Windows.Forms.DateTimePicker dtp_交货时间;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cbb_订单状态;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tbx_总价值;
        private System.Windows.Forms.TextBox tbx_交货方式;
        private System.Windows.Forms.TextBox tbx_交货地点;
        private System.Windows.Forms.TextBox tbx_订单编码;
        private System.Windows.Forms.Label label5;
        public System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.DateTimePicker dtp_创建订单时间;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cbb_订单类型;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TabPage tpg_交货开票;
        private System.Windows.Forms.TextBox textBox18;
        private System.Windows.Forms.TextBox textBox17;
        private System.Windows.Forms.TextBox textBox16;
        private System.Windows.Forms.TextBox tbx_开发票金额;
        private System.Windows.Forms.TextBox textBox14;
        private System.Windows.Forms.TextBox tbx_开发票数量;
        private System.Windows.Forms.TextBox textBox12;
        private System.Windows.Forms.TextBox tbx_需交货金额;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.TextBox tbx_需交货数量;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.TextBox tbx_已交货金额;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.TextBox tbx_已交货数量;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox tbx_已订购金额;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox tbx_已订购数量;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TabPage tpg_发票信息;
        private System.Windows.Forms.DataGridView dgv_发票信息;
        private System.Windows.Forms.DataGridViewTextBoxColumn Invoice_Code;
        private System.Windows.Forms.DataGridViewTextBoxColumn Invoice_Number;
        private System.Windows.Forms.DataGridViewTextBoxColumn Certificate_Type;
        private System.Windows.Forms.DataGridViewTextBoxColumn Certificate_Code;
        private System.Windows.Forms.DataGridViewTextBoxColumn Invoice_Makeout_Time;
        private System.Windows.Forms.DataGridViewTextBoxColumn Create_Time;
        private System.Windows.Forms.DataGridViewTextBoxColumn Payment_Type;
        private System.Windows.Forms.DataGridViewTextBoxColumn sum;
        private System.Windows.Forms.DataGridViewTextBoxColumn Currency;
        private System.Windows.Forms.DataGridViewTextBoxColumn Sum_Number;
        private System.Windows.Forms.TabPage tpg_收货信息;
        private System.Windows.Forms.DataGridView dgv_收货信息;
        private System.Windows.Forms.DataGridViewTextBoxColumn StockDocumentId;
        private System.Windows.Forms.DataGridViewTextBoxColumn Order_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Delivery_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ReceiptNote_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Posting_Date;
        private System.Windows.Forms.DataGridViewTextBoxColumn Document_Date;
        private System.Windows.Forms.DataGridViewTextBoxColumn StockManager;
        private System.Windows.Forms.DataGridViewTextBoxColumn Move_Type;
        private System.Windows.Forms.DataGridViewTextBoxColumn Reversed;
        private System.Windows.Forms.DataGridViewTextBoxColumn Total_Number;
        private System.Windows.Forms.DataGridViewTextBoxColumn Total_Value;
        public System.Windows.Forms.Label lbRecordCode;
    }
}