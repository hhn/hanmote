﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Bll.MDBll.General;
using Lib.Bll.MDBll.SP;
using Lib.Model.MD.SP;
using Lib.Bll;
using Lib.SqlServerDAL;
using Lib.Common.CommonUtils;
using Lib.Model.MD;

namespace MMClient.MD.FI
{
    public partial class ManualPosting : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        public ManualPosting()
        {
            InitializeComponent();
        }
        ComboBoxItem cbm = new ComboBoxItem();
        SupplierBaseBLL sbb = new SupplierBaseBLL();
        AccountBLL act = new AccountBLL();
        FormHelper fmh = new FormHelper();
        GeneralBLL gn = new GeneralBLL();
        //当前操作单元格的行索引和列索引
        int rindex, cindex,x,y;
        //按钮行索引
        int btnrindex;
        //科目行数计数
        int count;
        DataTable dt;
        PaymentClause clausemodel = new PaymentClause();
        //营业外收入
        double income=0;


        private void dgv_项目_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            //if (gpx_头数据.Visible == true)
            //{
            //    int height = gpx_头数据.Height;
            //    dgv_项目.Top = dgv_项目.Top - height;
            //    dgv_项目.BringToFront();
            //    gpx_详细信息.Visible = true;
              
                
            //    //gpx_头数据.Enabled = false;

                
                
            //}
            if (e.ColumnIndex==0)
            {
                btnrindex = e.RowIndex;
                if (dgv_项目.Rows[btnrindex].Cells[1].Value != null)
                {
                    tbc_详细信息.Visible = true;
                    tbc_详细信息.SelectedTab = tpg_详细信息;
                    
                }
                if (dgv_项目.Rows[btnrindex].Cells[5].Value != null)
                {
                    tbc_详细信息.Visible = true;
                    tbc_详细信息.SelectedTab = tpg_项目;

                }
            }
            if (e.ColumnIndex== 9)
            {
                dgv_项目.Rows.Remove(dgv_项目.Rows[e.RowIndex]);
                dgv_项目.Rows.Add();
            }
            

        }

        private void dgv_项目_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void ManualPosting_Load(object sender, EventArgs e)
        {
            dgv_项目.Rows.Add(100);
            
            List<string> list1 = gn.GetAllCompany();
            DataTable dt1 = gn.GetAllCompanyInformation();
            cbm.FuzzyQury(cbb_公司代码, list1, dt1);
            List<string> list2 = act.GetCurrencyType();
            cbm.FuzzyQury(cbb_货币, list2);
            //cbm.FuzzyQury(cbb_公司代码, list1);
            
            //Dictionary<string, string> dict_temp = new Dictionary<string, string>();
            //string key = "temp";
            //dict_temp.Add(key, "");
            //fmh.onlyNumber(tbx_temp, key, dict_temp);
            //递归生成科目树
            string sql = "SELECT Account_ID,Account_Name,Parent_ID FROM [Account]";
            dt = DBHelper.ExecuteQueryDT(sql);
            BindRoot();
            List<string> list3 = gn.GetAllPaymentClauseName();
            cbm.FuzzyQury(cbb_付款条件1, list3);
            cbb_凭证类型.DataSource = gn.GetAllProofType();
            //订单列表添加
            string sql2 = "SELECT * FROM [Order_Factory_Front]";
          DataTable dt2 =  DBHelper.ExecuteQueryDT(sql2);
          List<string> listorder = new List<string>();
            for(int j = 0;j<dt2.Rows.Count;j++)
                listorder.Add(dt2.Rows[j][0].ToString());
            cbm.FuzzyQury(cbb_所属订单号, listorder);
        }
        private void BindRoot()
        {
            //从dt表中查询出parentId='RA'的行
            DataRow[] ro = dt.Select("Parent_ID='RA'");
            foreach (DataRow dr in ro)
            {
                //if (dr["parentId"].ToString() == "ABC")
                //{
                TreeNode node = new TreeNode();
                node.Tag = dr;
                node.Text =dr["Account_Name"].ToString()+"-"+dr["Account_ID"].ToString();
                trv_科目.Nodes.Add(node);
                AddChild(node);
                //}
            }
        }
        private void AddChild(TreeNode node)
        {
            DataRow dr2 = (DataRow)node.Tag;//获取与根节点关联的数据行
            string pId = dr2["Account_ID"].ToString();//获取根节点的deptId

            //从dt表中查询出parentId=deptId的节点
            DataRow[] rows = dt.Select("Parent_ID='" + pId + "'");
            if (rows.Length == 0)
                return;
            foreach (DataRow drow in rows) //再次遍历，添加子节点
            {
                TreeNode node3 = new TreeNode();
                node3.Tag = drow;
                node3.Text = drow["Account_Name"].ToString()+"-"+drow["Account_ID"].ToString();
                node.Nodes.Add(node3);
                //递归
                AddChild(node3);
            }
        }

        private void dgv_项目_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            //if (dgv_项目.CurrentCell.ColumnIndex == 1)
            //{
            //    object cbb = new object();
            //    DataGridViewCell curCell = this.dgv_项目.CurrentCell;
            //    if (curCell is DataGridViewComboBoxCell) {
            //        DataGridViewComboBoxCell tmp = curCell as DataGridViewComboBoxCell;
            //        tmp.s
            //    }
            //}
            cbb_temp.Text = "";
            cbb_temp.Items.Clear();
            tbx_temp.Text = "";
            cbb_temp.Visible = false;
            tbx_temp.Visible = false;
            Rectangle rec = this.dgv_项目.GetCellDisplayRectangle(this.dgv_项目.CurrentCell.ColumnIndex, this.dgv_项目.CurrentCell.RowIndex, false);
            rindex = dgv_项目.CurrentCell.RowIndex;
            if (rindex >= 4)
            {
                dgv_项目.Rows.Add();
            }
            if (dgv_项目.CurrentCell.ColumnIndex == 1)
            {
                
                cbb_temp.Visible = true;
                cbb_temp.Focus();
                rindex = dgv_项目.CurrentCell.RowIndex;
                cindex = dgv_项目.CurrentCell.ColumnIndex;
                //cbb_temp.Left = this.dgv_项目.Left + this.dgv_项目.RowHeadersWidth + rec.X;
                cbb_temp.Left = rec.X;
                x = rec.Width;
                //cbb_temp.Top = this.dgv_项目.Top + this.dgv_项目.ColumnHeadersHeight + rec.Y;
                cbb_temp.Top = rec.Y;
                y = rec.Height;
                cbb_temp.Size = new System.Drawing.Size(rec.Width, rec.Height);
                cbb_temp.Items.Clear();
                List<string> list1 = sbb.GetAllSupplierID();
                DataTable dt1 = sbb.GetAllSupplierBasicInformation();
                cbm.FuzzyQury(cbb_temp, list1, dt1);
                cbb_temp.Size = new System.Drawing.Size(5 * x, y);
               

              

            }
            if (dgv_项目.CurrentCell.ColumnIndex == 3)
            {
                
                cbb_temp.Visible = true;
                cbb_temp.Focus();
                rindex = dgv_项目.CurrentCell.RowIndex;
                cindex = dgv_项目.CurrentCell.ColumnIndex;
                //cbb_temp.Left = this.dgv_项目.Left + this.dgv_项目.RowHeadersWidth + rec.X;
                cbb_temp.Left = rec.X;
                x = rec.Width;
                //cbb_temp.Top = this.dgv_项目.Top + this.dgv_项目.ColumnHeadersHeight + rec.Y;
                cbb_temp.Top = rec.Y;
                y = rec.Height;
                cbb_temp.Size = new System.Drawing.Size(rec.Width, rec.Height);
                cbb_temp.Items.Clear();
                List<string> list2 = act.GetAllAccount();
                DataTable dt2 = act.GetALLAccountInformation();
                cbm.FuzzyQury(cbb_temp, list2, dt2);
                cbb_temp.Size = new System.Drawing.Size(3 * x, y);
            }
            if (dgv_项目.CurrentCell.ColumnIndex == 7)
            {
                dgv_项目.CurrentCell.Value = "";
                dgv_项目.Rows[dgv_项目.CurrentCell.RowIndex].Cells[8].Value = "";
       
                cbb_temp.Visible = true;
                cbb_temp.Focus();
                rindex = dgv_项目.CurrentCell.RowIndex;
                cindex = dgv_项目.CurrentCell.ColumnIndex;
                //cbb_temp.Left = this.dgv_项目.Left + this.dgv_项目.RowHeadersWidth + rec.X;
                cbb_temp.Left = rec.X;
                x = rec.Width;
                //cbb_temp.Top = this.dgv_项目.Top + this.dgv_项目.ColumnHeadersHeight + rec.Y;
                cbb_temp.Top = rec.Y;
                y = rec.Height;
                cbb_temp.Size = new System.Drawing.Size(rec.Width, rec.Height);
                cbb_temp.Items.Clear();
                List<string> list3 = act.GetTaxCode();
                DataTable dt3 = act.GetAllTaxCodeInformation();
                cbm.FuzzyQury(cbb_temp, list3, dt3);
                cbb_temp.Size = new System.Drawing.Size(3 * x, y);
            }
            if (dgv_项目.CurrentCell.ColumnIndex == 5)
            {
               
                tbx_temp.Visible = true;
                tbx_temp.Focus();
                rindex = dgv_项目.CurrentCell.RowIndex;
                cindex = dgv_项目.CurrentCell.ColumnIndex;
                tbx_temp.Left = rec.X;
                x = rec.Width;
                tbx_temp.Top = rec.Y;
                y = rec.Height;
                tbx_temp.Size = new System.Drawing.Size(rec.Width, rec.Height);
            }
            if (dgv_项目.CurrentCell.ColumnIndex == 6)
            {

                tbx_temp.Visible = true;
                tbx_temp.Focus();
                rindex = dgv_项目.CurrentCell.RowIndex;
                cindex = dgv_项目.CurrentCell.ColumnIndex;
                tbx_temp.Left = rec.X;
                x = rec.Width;
                tbx_temp.Top = rec.Y;
                y = rec.Height;
                tbx_temp.Size = new System.Drawing.Size(rec.Width, rec.Height);
            }

        }

        private void cbb_temp_SelectedIndexChanged(object sender, EventArgs e)
        {
            
                string str = cbb_temp.Text;
                string[] atrArr = str.Split(' ');
                dgv_项目[cindex, rindex].Value = atrArr[0];
                if (cindex == 1)
                {
                    SupplierBase spb = new SupplierBase();
                   spb = sbb.GetSupplierBasicInformation(atrArr[0]);
                   dgv_项目[cindex + 1, rindex].Value = spb.Supplier_Name;
                }
                if (cindex == 3)
                {
                    string sql = "SELECT Account_Name FROM [Account] WHERE Account_ID='" + atrArr[0] + "'";
                  DataTable dt =  DBHelper.ExecuteQueryDT(sql);
                  dgv_项目[cindex + 1, rindex].Value = dt.Rows[0][0];
                }
                if (cindex == 7)
                {
                    string sql = "SELECT Description FROM [Tax_Code] WHERE Tax_Code='" + atrArr[0] + "'";
                    DataTable dt = DBHelper.ExecuteQueryDT(sql);
                    dgv_项目[cindex + 1, rindex].Value = dt.Rows[0][0];
                }

                //dgv_项目[cindex + 1, rindex].Value = atrArr[5];
                cbb_temp.Visible = false;
               
            
        }

        private void cbb_temp_TextChanged(object sender, EventArgs e)
        {
           
        }

        private void cbb_temp_KeyDown(object sender, KeyEventArgs e)
        {
            if (cindex == 1)
            {
                
            }
        }

        private void btn_过账_Click(object sender, EventArgs e)
        {
            cbb_temp.Visible = false;
            tbx_temp.Visible = false;
            int flag = 0;
            for (int p = 0; ; p++)
            {
                //string a = dgv_项目.Rows[i].Cells[5].Value.ToString();
                //string b = dgv_项目.Rows[i].Cells[6].Value.ToString();
                try
                {
                    dgv_项目.Rows[p].Cells[3].Value.ToString();
                }

                catch (Exception ex)
                {
                    count = p;
                    break;
                }
            }

                if (tbx_凭证编码.Text == "")
                {
                    MessageUtil.ShowError("请输入正确的凭证编码");
                    flag = 1;
                }
                //int k;
                //double totalcredit = 0, totaldebit = 0;
                //for (k = 0; k < count; k++)
                //{
                //    if (dgv_项目.Rows[k].Cells[5].Value != null)
                //        totalcredit = totalcredit + Convert.ToDouble(dgv_项目.Rows[k].Cells[5].Value.ToString());
                //    else
                //        totalcredit = totalcredit;
                //    if (dgv_项目.Rows[k].Cells[6].Value != null)
                //        totaldebit = totaldebit + Convert.ToDouble(dgv_项目.Rows[k].Cells[6].Value.ToString());
                //    else
                //        totaldebit = totaldebit;
                //}
                //if (totalcredit != totaldebit)
                //{
                //    flag = 1;
                //    MessageUtil.ShowError("借方和贷方不相等");
                //}
                if (flag == 0)
                {
                    for (int i = 0; ; i++)
                    {
                        //string a = dgv_项目.Rows[i].Cells[5].Value.ToString();
                        //string b = dgv_项目.Rows[i].Cells[6].Value.ToString();
                        try
                        {
                            dgv_项目.Rows[i].Cells[3].Value.ToString();
                        }

                        catch (Exception ex)
                        {
                            count = i;
                            break;
                        }
                    }
                    double tax;
                    double totaltax=0;
                    string code;
                    string supplierID;
                    string AccountID;
                    float debit;
                    float tempdebit;
                    float credit;
                    int rownumber;
                    DataTable dt;
                    code = tbx_凭证编码.Text;
                    for (int j = 0; j < count; j++)
                    {
                        if (dgv_项目.Rows[j].Cells[7].Value == null||dgv_项目.Rows[j].Cells[7].Value.ToString() == "")
                            tax = 0;
                        else
                        {
                            dt = act.GetTaxByTaxCode(dgv_项目.Rows[j].Cells[7].Value.ToString());
                            tax =Convert.ToDouble( dt.Rows[0][0].ToString());
                        }
                         
                        if (dgv_项目.Rows[j].Cells[5].Value == null || dgv_项目.Rows[j].Cells[5].Value.ToString()=="0")
                        {
                            debit = 0;
                            credit = (float)Convert.ToDouble(dgv_项目.Rows[j].Cells[6].Value.ToString());
                            
                        }
                        else
                        {
                            credit = 0;
                            debit = (float)Convert.ToDouble(dgv_项目.Rows[j].Cells[5].Value.ToString());
                            if (tax == 0)
                            {
                                debit = debit;
                                totaltax = totaltax;
                            }
                            else
                            {
                                tempdebit = debit;
                                debit =(float)(debit / (1 + tax / 100));
                                totaltax = totaltax + (tempdebit - debit);
                                dgv_项目.Rows[j].Cells[5].Value = debit;
                            }
                        }
                        if (dgv_项目.Rows[j].Cells[1].Value == null || dgv_项目.Rows[j].Cells[5].Value.ToString() == "0")
                        {
                            supplierID = "";
                        }
                        else
                        {
                            supplierID = dgv_项目.Rows[j].Cells[1].Value.ToString();
                        }
                        AccountID = dgv_项目.Rows[j].Cells[3].Value.ToString();
                        //act.WriteAccountQuery(code, class1, supplierID, AccountID, debit, credit, "", "");
                    }
                    int lock1 = 0;
                    if (lock1 == 0)
                    {
                        dgv_项目.Rows[count].Cells[3].Value = "0007";
                        dgv_项目.Rows[count].Cells[4].Value = "应交税费";
                        dgv_项目.Rows[count].Cells[5].Value = totaltax;
                        dgv_项目.Rows[count + 1].Cells[3].Value = "0008";
                        dgv_项目.Rows[count + 1].Cells[4].Value = "营业外收入";
                        dgv_项目.Rows[count + 1].Cells[6].Value = income;
                        lock1 =1;
                    }
                    double ico = income;
                    if (lock1 == 1)
                    {
                        int k;
                        double totalcredit = 0, totaldebit = 0;
                        for (k = 0; k < count; k++)
                        {
                            if (dgv_项目.Rows[k].Cells[5].Value != null)
                                totalcredit = totalcredit + Convert.ToDouble(dgv_项目.Rows[k].Cells[5].Value.ToString());
                            else
                                totalcredit = totalcredit;
                            if (dgv_项目.Rows[k].Cells[6].Value != null)
                                totaldebit = totaldebit + Convert.ToDouble(dgv_项目.Rows[k].Cells[6].Value.ToString());
                            else
                                totaldebit = totaldebit;
                        }
                        double judge = Math.Abs((totalcredit + totaltax) - (totaldebit + ico));
                        if (judge>0.1)
                        {
                            flag = 1;
                            MessageUtil.ShowError("借方和贷方不相等");
                        }
                        if (flag == 0)
                        {
                            MessageUtil.ShowTips("模拟过账成功");
                            dgv_项目.Enabled = false;
                            btn_模拟过账.Enabled = false;
                            btn_过账.Enabled = true;
                        }
                    }
                }
            }
        

        private void textBox5_VisibleChanged(object sender, EventArgs e)
        {

        }

        private void textBox5_TextChanged(object sender, EventArgs e)
        {
            //Dictionary<string, string> dict_temp = new Dictionary<string, string>();
            //string key = "毛重";
            //dict_temp.Add(key, "");
            //fmh.onlyNumber(textBox5, key, dict_temp);
        }

        private void tbx_temp_TextChanged(object sender, EventArgs e)
        {
            Dictionary<string, string> dict_temp = new Dictionary<string, string>();
            string key = "temp";
            dict_temp.Add(key, "");
            fmh.onlyNumber(tbx_temp, key, dict_temp);
        }

        private void tbx_temp_Leave(object sender, EventArgs e)
        {
            //tbx_temp.Visible = false;
            if (tbx_temp.Text == "")
                tbx_temp.Text = "0";
            dgv_项目[cindex, rindex].Value = tbx_temp.Text;
        }

        private void tbx_temp_VisibleChanged(object sender, EventArgs e)
        {
            //dgv_项目[cindex, rindex].Value = tbx_temp.Text;
        }

        private void dgv_项目_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {

        }

        private void dgv_科目表层次结构_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            //List<string> list = sbb.GetAllSupplierID();
            //Column1.DataSource = list;
            ComboBox cb = e.Control as ComboBox;

            //if (cb != null)
            //{

            //    // first remove event handler to keep from attaching multiple:

            //    cb.SelectedIndexChanged -= new
            //   EventHandler(cb_SelectedIndexChanged);



            //    // now attach the event handler

            //    cb.SelectedIndexChanged += new
            //   EventHandler(cb_SelectedIndexChanged);

            //}
        }
        void cb_SelectedIndexChanged(object sender, EventArgs e)
        {

            //MessageBox.Show("Selected index changed");
        }

        private void dgv_科目表层次结构_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void btn_过账_Click_1(object sender, EventArgs e)
        {
            int flag = 0;
            if (cbb_公司代码.Text == "") 
            {
                flag = 1;
                MessageUtil.ShowError("请选择正确的公司代码");
            }
            if (flag==0&&cbb_货币.Text == "") 
            {
                flag = 1;
                MessageUtil.ShowError("请选择货币");
            }
            if (flag == 0 && tbx_抬头文本.Text == "") 
            {
                flag = 1;
                MessageUtil.ShowError("请输入正确的抬头文本");
            }
            if (flag == 0)
            {
                string code;
                int rownumber;
                string supplierID;
                string AccountID;
                float debit;
                float credit;
                DataTable dt;
                int j;

                code = tbx_凭证编码.Text;
                for (j = 0; j < count+2; j++)
                {
                    rownumber = j;
                    //if (dgv_项目.Rows[j].Cells[7].Value == null || dgv_项目.Rows[j].Cells[7].Value.ToString() == "")
                    //{
                    //    tax = 0;
                    //    taxcode = "";
                    //}
                    //else
                    //{
                    //    taxcode = dgv_项目.Rows[j].Cells[7].Value.ToString();
                    //    dt = act.GetTaxByTaxCode(dgv_项目.Rows[j].Cells[7].Value.ToString());
                    //    tax = Convert.ToDouble(dt.Rows[0][0].ToString());
                    //}

                    if (dgv_项目.Rows[j].Cells[5].Value == null || dgv_项目.Rows[j].Cells[5].Value.ToString() == "0")
                    {
                        debit = 0;
                        credit = (float)Convert.ToDouble(dgv_项目.Rows[j].Cells[6].Value.ToString());

                    }
                    else
                    {
                        credit = 0;
                        debit = (float)Convert.ToDouble(dgv_项目.Rows[j].Cells[5].Value.ToString());
                        //if (tax == 0)
                        //{
                        //    debit = debit;
                        //    totaltax = totaltax;
                        //}
                        //else
                        //{
                        //    tempdebit = debit;
                        //    debit = (float)(debit / (1 + tax / 100));
                        //    totaltax = totaltax + (tempdebit - debit);
                        //    dgv_项目.Rows[j].Cells[5].Value = debit;
                        //}
                    }
                    if (dgv_项目.Rows[j].Cells[1].Value == null || dgv_项目.Rows[j].Cells[5].Value.ToString() == "0")
                    {
                        supplierID = "";
                    }
                    else
                    {
                        supplierID = dgv_项目.Rows[j].Cells[1].Value.ToString();
                    }
                    AccountID = dgv_项目.Rows[j].Cells[3].Value.ToString();
                    try
                    {
                        act.WriteAccountQuery(code, rownumber, supplierID, AccountID, debit, credit, "");
                    }
                    catch (Exception ex)
                    {
                        MessageUtil.ShowError("系统中已存在该会计凭证");
                    }
                }
                //supplierID = "";
                //AccountID = "00007";
                //debit = (float)totaltax;
                //credit = 0;
                //act.WriteAccountQuery(code, j+1, supplierID, AccountID, debit, credit,"");
                MessageUtil.ShowTips("过账成功");
            }
        }

        private void cbb_付款条件_SelectedIndexChanged(object sender, EventArgs e)
        {
            string str = cbb_付款条件.Text;
            string[] atrArr = str.Split(' ');
            string clausename = atrArr[0];
            clausemodel = gn.GetClause(clausename);
            tbx_天数百分比.Text = clausemodel.First_Date.ToString() + "    " + clausemodel.First_Discount.ToString() + "    " + clausemodel.Second_Date.ToString() + "    " + clausemodel.Second_Discount.ToString();
        }

        private void trv_科目_NodeMouseDoubleClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            int i;
            for ( i = 0; ; i++)
            {
                if (i >= 5)
                    dgv_项目.Rows.Add();
                if (dgv_项目.Rows[i].Cells[3].Value == null)
                    break;
            }
            string nodetext = e.Node.Text;
            string[] atrArr = nodetext.Split('-');
            dgv_项目.Rows[i].Cells[3].Value = atrArr[1];
            dgv_项目.Rows[i].Cells[4].Value = atrArr[0];
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DateTime d1 = dtp_过账日期.Value;
            DateTime d2 = dtp_基准日期.Value;
            TimeSpan d3 = d1.Subtract(d2);
            int days = Convert.ToInt16(d3.Days.ToString());
            if (days > clausemodel.Second_Date)
            {
                income = income;
            }
            else
            {
                if (clausemodel.First_Date < days && days < clausemodel.Second_Date)
                {
                    double money = Convert.ToDouble(dgv_项目.Rows[btnrindex].Cells[6].Value.ToString());
                    income += money * clausemodel.Second_Discount/100;
                    money = money - money * clausemodel.Second_Discount / 100;
                    dgv_项目.Rows[btnrindex].Cells[6].Value = money;
                }
                else
                {
                    if (days < clausemodel.First_Date)
                    {
                        double money = Convert.ToDouble(dgv_项目.Rows[btnrindex].Cells[6].Value.ToString());
                        income += money * clausemodel.First_Discount/100;
                        money = money - money * clausemodel.First_Discount / 100;
                        dgv_项目.Rows[btnrindex].Cells[6].Value = money;
                    }
                }
            }
            tbc_详细信息.Visible = false;
        }

        private void btn_清除科目_Click(object sender, EventArgs e)
        {
           while (dgv_项目.Rows.Count != 0)
           {
           dgv_项目.Rows.RemoveAt(0);
           }
           dgv_项目.Rows.Add();
           dgv_项目.Rows.Add();
           dgv_项目.Rows.Add();
           dgv_项目.Rows.Add();
           dgv_项目.Rows.Add();
           dgv_项目.Rows.Add();
           dgv_项目.Rows.Add();
           dgv_项目.Rows.Add();
           dgv_项目.Rows.Add();
           dgv_项目.Rows.Add();
           dgv_项目.Enabled = true;
        }

        private void btn_应用2_Click(object sender, EventArgs e)
        {
            tbc_详细信息.Visible = false;
        }

        private void comboBox4_SelectedIndexChanged(object sender, EventArgs e)
        {
            string str = cbb_付款条件1.Text;
            string[] atrArr = str.Split(' ');
            string clausename = atrArr[0];
            clausemodel = gn.GetClause(clausename);
            tbx_天数百分比1.Text = clausemodel.First_Date.ToString() + "    " + clausemodel.First_Discount.ToString() + "    " + clausemodel.Second_Date.ToString() + "    " + clausemodel.Second_Discount.ToString();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DateTime d1 = dtp_过账日期.Value;
            DateTime d2 = dtp_基准日期1.Value;
            TimeSpan d3 = d1.Subtract(d2);
            int days = Convert.ToInt16(d3.Days.ToString());
            if (days > clausemodel.Second_Date)
            {
                income = income;
            }
            else
            {
                if (clausemodel.First_Date < days && days < clausemodel.Second_Date)
                {
                    double money = Convert.ToDouble(dgv_项目.Rows[btnrindex].Cells[6].Value.ToString());
                    income += money * clausemodel.Second_Discount / 100;
                    money = money - money * clausemodel.Second_Discount / 100;
                    dgv_项目.Rows[btnrindex].Cells[6].Value = money;
                }
                else
                {
                    if (days < clausemodel.First_Date)
                    {
                        double money = Convert.ToDouble(dgv_项目.Rows[btnrindex].Cells[6].Value.ToString());
                        income += money * clausemodel.First_Discount / 100;
                        money = money - money * clausemodel.First_Discount / 100;
                        dgv_项目.Rows[btnrindex].Cells[6].Value = money;
                    }
                }
            }
            tbc_详细信息.Visible = false;
        }

        private void cbb_凭证类型_SelectedIndexChanged(object sender, EventArgs e)
        {
            tbx_凭证类型.Text = gn.GetProofNameByType(cbb_凭证类型.Text);
        }

        private void tpg_详细信息_Click(object sender, EventArgs e)
        {

        }
    }
}
