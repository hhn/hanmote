﻿namespace MMClient.MD.FI
{
    partial class QuryOrder
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.button3 = new System.Windows.Forms.Button();
            this.pageTool = new pager.pagetool.pageNext();
            this.dgv_查询结果 = new System.Windows.Forms.DataGridView();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.button2 = new System.Windows.Forms.Button();
            this.订单数量2 = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.tbx_订单数量1 = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.tbx_总金额2 = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.tbx_总金额1 = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.tbx_供应商编码2 = new System.Windows.Forms.TextBox();
            this.tbx_供应商编码1 = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.tbx_订单编码2 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tbx_订单编码1 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.cbb_精确订单编码 = new System.Windows.Forms.ComboBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_查询结果)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.AutoSize = true;
            this.groupBox1.Controls.Add(this.groupBox5);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Location = new System.Drawing.Point(2, 4);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox1.Size = new System.Drawing.Size(995, 897);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "订单-凭证信息查询";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.button3);
            this.groupBox5.Controls.Add(this.pageTool);
            this.groupBox5.Controls.Add(this.dgv_查询结果);
            this.groupBox5.Location = new System.Drawing.Point(5, 196);
            this.groupBox5.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox5.Size = new System.Drawing.Size(965, 683);
            this.groupBox5.TabIndex = 1;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "查询结果";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(430, 632);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(100, 29);
            this.button3.TabIndex = 21;
            this.button3.Text = "确  定";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // pageTool
            // 
            this.pageTool.Location = new System.Drawing.Point(139, 565);
            this.pageTool.Name = "pageTool";
            this.pageTool.PageIndex = 1;
            this.pageTool.PageSize = 100;
            this.pageTool.RecordCount = 0;
            this.pageTool.Size = new System.Drawing.Size(685, 38);
            this.pageTool.TabIndex = 20;
            this.pageTool.OnPageChanged += new System.EventHandler(this.pageTool_OnPageChanged);
            this.pageTool.Load += new System.EventHandler(this.pageTool_Load);
            // 
            // dgv_查询结果
            // 
            this.dgv_查询结果.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgv_查询结果.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.dgv_查询结果.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgv_查询结果.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_查询结果.Location = new System.Drawing.Point(10, 19);
            this.dgv_查询结果.Name = "dgv_查询结果";
            this.dgv_查询结果.RowTemplate.Height = 23;
            this.dgv_查询结果.Size = new System.Drawing.Size(950, 540);
            this.dgv_查询结果.TabIndex = 19;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.groupBox4);
            this.groupBox2.Controls.Add(this.groupBox3);
            this.groupBox2.Location = new System.Drawing.Point(5, 16);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox2.Size = new System.Drawing.Size(946, 178);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "查询条件";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.button2);
            this.groupBox4.Controls.Add(this.订单数量2);
            this.groupBox4.Controls.Add(this.label17);
            this.groupBox4.Controls.Add(this.label16);
            this.groupBox4.Controls.Add(this.tbx_订单数量1);
            this.groupBox4.Controls.Add(this.label15);
            this.groupBox4.Controls.Add(this.tbx_总金额2);
            this.groupBox4.Controls.Add(this.label14);
            this.groupBox4.Controls.Add(this.tbx_总金额1);
            this.groupBox4.Controls.Add(this.label13);
            this.groupBox4.Controls.Add(this.label12);
            this.groupBox4.Controls.Add(this.label11);
            this.groupBox4.Controls.Add(this.tbx_供应商编码2);
            this.groupBox4.Controls.Add(this.tbx_供应商编码1);
            this.groupBox4.Controls.Add(this.label10);
            this.groupBox4.Controls.Add(this.label9);
            this.groupBox4.Controls.Add(this.tbx_订单编码2);
            this.groupBox4.Controls.Add(this.label5);
            this.groupBox4.Controls.Add(this.tbx_订单编码1);
            this.groupBox4.Controls.Add(this.label4);
            this.groupBox4.Controls.Add(this.label3);
            this.groupBox4.Location = new System.Drawing.Point(4, 68);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox4.Size = new System.Drawing.Size(934, 108);
            this.groupBox4.TabIndex = 4;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "模糊查询";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(821, 76);
            this.button2.Margin = new System.Windows.Forms.Padding(2);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(106, 30);
            this.button2.TabIndex = 26;
            this.button2.Text = "查询";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // 订单数量2
            // 
            this.订单数量2.Location = new System.Drawing.Point(696, 17);
            this.订单数量2.Margin = new System.Windows.Forms.Padding(2);
            this.订单数量2.Name = "订单数量2";
            this.订单数量2.Size = new System.Drawing.Size(107, 21);
            this.订单数量2.TabIndex = 25;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(670, 22);
            this.label17.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(17, 12);
            this.label17.TabIndex = 24;
            this.label17.Text = "到";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(522, 23);
            this.label16.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(17, 12);
            this.label16.TabIndex = 23;
            this.label16.Text = "从";
            // 
            // tbx_订单数量1
            // 
            this.tbx_订单数量1.Location = new System.Drawing.Point(543, 17);
            this.tbx_订单数量1.Margin = new System.Windows.Forms.Padding(2);
            this.tbx_订单数量1.Name = "tbx_订单数量1";
            this.tbx_订单数量1.Size = new System.Drawing.Size(107, 21);
            this.tbx_订单数量1.TabIndex = 22;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(459, 23);
            this.label15.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(53, 12);
            this.label15.TabIndex = 21;
            this.label15.Text = "订单数量";
            // 
            // tbx_总金额2
            // 
            this.tbx_总金额2.Location = new System.Drawing.Point(696, 53);
            this.tbx_总金额2.Margin = new System.Windows.Forms.Padding(2);
            this.tbx_总金额2.Name = "tbx_总金额2";
            this.tbx_总金额2.Size = new System.Drawing.Size(107, 21);
            this.tbx_总金额2.TabIndex = 20;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(670, 58);
            this.label14.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(17, 12);
            this.label14.TabIndex = 19;
            this.label14.Text = "到";
            // 
            // tbx_总金额1
            // 
            this.tbx_总金额1.Location = new System.Drawing.Point(543, 53);
            this.tbx_总金额1.Margin = new System.Windows.Forms.Padding(2);
            this.tbx_总金额1.Name = "tbx_总金额1";
            this.tbx_总金额1.Size = new System.Drawing.Size(107, 21);
            this.tbx_总金额1.TabIndex = 18;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(522, 56);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(17, 12);
            this.label13.TabIndex = 17;
            this.label13.Text = "从";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(460, 58);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(41, 12);
            this.label12.TabIndex = 16;
            this.label12.Text = "总金额";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(242, 58);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(17, 12);
            this.label11.TabIndex = 14;
            this.label11.Text = "到";
            // 
            // tbx_供应商编码2
            // 
            this.tbx_供应商编码2.Location = new System.Drawing.Point(267, 55);
            this.tbx_供应商编码2.Margin = new System.Windows.Forms.Padding(2);
            this.tbx_供应商编码2.Name = "tbx_供应商编码2";
            this.tbx_供应商编码2.Size = new System.Drawing.Size(120, 21);
            this.tbx_供应商编码2.TabIndex = 13;
            // 
            // tbx_供应商编码1
            // 
            this.tbx_供应商编码1.Location = new System.Drawing.Point(106, 55);
            this.tbx_供应商编码1.Margin = new System.Windows.Forms.Padding(2);
            this.tbx_供应商编码1.Name = "tbx_供应商编码1";
            this.tbx_供应商编码1.Size = new System.Drawing.Size(107, 21);
            this.tbx_供应商编码1.TabIndex = 12;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(85, 55);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(17, 12);
            this.label10.TabIndex = 11;
            this.label10.Text = "从";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(9, 58);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(65, 12);
            this.label9.TabIndex = 10;
            this.label9.Text = "供应商编码";
            // 
            // tbx_订单编码2
            // 
            this.tbx_订单编码2.Location = new System.Drawing.Point(267, 18);
            this.tbx_订单编码2.Margin = new System.Windows.Forms.Padding(2);
            this.tbx_订单编码2.Name = "tbx_订单编码2";
            this.tbx_订单编码2.Size = new System.Drawing.Size(120, 21);
            this.tbx_订单编码2.TabIndex = 4;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(242, 20);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(17, 12);
            this.label5.TabIndex = 3;
            this.label5.Text = "到";
            // 
            // tbx_订单编码1
            // 
            this.tbx_订单编码1.Location = new System.Drawing.Point(106, 18);
            this.tbx_订单编码1.Margin = new System.Windows.Forms.Padding(2);
            this.tbx_订单编码1.Name = "tbx_订单编码1";
            this.tbx_订单编码1.Size = new System.Drawing.Size(107, 21);
            this.tbx_订单编码1.TabIndex = 2;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(85, 23);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(17, 12);
            this.label4.TabIndex = 1;
            this.label4.Text = "从";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 24);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 12);
            this.label3.TabIndex = 0;
            this.label3.Text = "订单编码";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.cbb_精确订单编码);
            this.groupBox3.Controls.Add(this.button1);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Location = new System.Drawing.Point(4, 14);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox3.Size = new System.Drawing.Size(937, 50);
            this.groupBox3.TabIndex = 3;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "精确查询";
            // 
            // cbb_精确订单编码
            // 
            this.cbb_精确订单编码.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbb_精确订单编码.FormattingEnabled = true;
            this.cbb_精确订单编码.Location = new System.Drawing.Point(70, 22);
            this.cbb_精确订单编码.Margin = new System.Windows.Forms.Padding(2);
            this.cbb_精确订单编码.Name = "cbb_精确订单编码";
            this.cbb_精确订单编码.Size = new System.Drawing.Size(107, 20);
            this.cbb_精确订单编码.TabIndex = 1;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(821, 12);
            this.button1.Margin = new System.Windows.Forms.Padding(2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(106, 30);
            this.button1.TabIndex = 15;
            this.button1.Text = "查询";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 25);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "订单编码";
            // 
            // QuryOrder
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1067, 894);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "QuryOrder";
            this.Text = "订单-凭证信息查询";
            this.groupBox1.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_查询结果)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ComboBox cbb_精确订单编码;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbx_订单编码2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tbx_订单编码1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox tbx_供应商编码2;
        private System.Windows.Forms.TextBox tbx_供应商编码1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox tbx_总金额2;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox tbx_总金额1;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox 订单数量2;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox tbx_订单数量1;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.DataGridView dgv_查询结果;
        private pager.pagetool.pageNext pageTool;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
    }
}