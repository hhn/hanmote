﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Bll.MDBll.General;
using Lib.SqlServerDAL;
using Lib.Common.CommonUtils;
using Lib.Model.MD.Order;
using Lib.Bll.MDBll.Order;
using Lib.Bll.MDBll.MT;
using Lib.SqlServerDAL.MDDAL.Order;
using Lib.Common.MMCException.IDAL;

namespace MMClient.MD.FI
{
    public partial class Order : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        QuryOrder quryOrder = null;
        public string orderid;
        ComboBoxItem cbm = new ComboBoxItem();
        GeneralBLL gn = new GeneralBLL();
        List<OrderItem> listo = new List<OrderItem>();
        OrderBLL odbl = new OrderBLL();
        OrderDAL odal = new OrderDAL();
        private RecordInfoForm infoForm = null;
        int rowcount = 0;//
        Dictionary<string, string> dic = new Dictionary<string, string>();


        public Order()
        {
            InitializeComponent();
        }

        private void btn_提交订单_Click(object sender, EventArgs e)
        {
            bool errorflag = true;
            OrderInfo odif = new OrderInfo();

            float sum = 0;
            long sumpc = 0;
            odif.Create_Time = Convert.ToDateTime(dtp_创建订单时间.Value);
            odif.Delivery_Points = tbx_交货地点.Text;
            odif.Delivery_Ttime = Convert.ToDateTime(dtp_交货时间.Value);

            odif.Delivery_Type = tbx_交货方式.Text;
            if (tbx_订单编码.Text == "")
            {
                MessageUtil.ShowError("订单编码不能为空");
                errorflag = false;
            }
            else
                odif.Order_ID = tbx_订单编码.Text;

            odif.Order_Status = cbb_订单状态.Text;
            odif.Order_Type = cbb_订单类型.Text;


            //以下设计待定
            odif.Total_Value = 0;
            odif.Value_Paid = 0;
            odif.Value_Received = 0;
            int count = 0;
            for (int i = 0; i < dgv_采购项目.Rows.Count; i++)
            {
                OrderItem odim = new OrderItem();
                odim.Delivery_Time = Convert.ToDateTime(dtp_交货时间.Value);
                string Select_Value = dgv_采购项目.Rows[i].Cells["cln_状态"].EditedFormattedValue.ToString();
                if (Select_Value == "True")
                {
                    count++;
                    try
                    {
                        odim.Batch_ID = dgv_采购项目.Rows[i].Cells["cln_批次"].Value.ToString();
                    }
                    catch (Exception ex)
                    {
                        odim.Batch_ID = "";
                    }
                    try
                    {
                        odim.Factory_ID = dgv_采购项目.Rows[i].Cells["cln_工厂"].Value.ToString();
                    }
                    catch (Exception ex)
                    {
                        odim.Factory_ID = "";
                    }
                    try
                    {
                        odim.Info_Number = dgv_采购项目.Rows[i].Cells["cln_信息记录"].Value.ToString();
                    }
                    catch (Exception ex)
                    {
                        odim.Info_Number = "";
                    }
                    try
                    {
                        odim.Material_Group = dgv_采购项目.Rows[i].Cells["cln_物料组"].Value.ToString();
                    }
                    catch (Exception ex)
                    {
                        odim.Material_Group = "";
                    }
                    try
                    {
                        odim.Material_ID = dgv_采购项目.Rows[i].Cells["cln_物料编码"].Value.ToString();
                    }
                    catch (Exception ex)
                    {
                        MessageUtil.ShowError("物料编码录入错误");
                        errorflag = false;
                        break;
                    }
                    try
                    {
                        odim.Net_Price = (float)Convert.ToDouble(dgv_采购项目.Rows[i].Cells["cln_净价"].Value.ToString());
                    }
                    catch (Exception ex)
                    {
                        MessageUtil.ShowError("净价录入错误");
                        errorflag = false;
                        break;
                    }
                    try
                    {
                        odim.Number = Convert.ToInt16(dgv_采购项目.Rows[i].Cells["cln_数量"].Value.ToString());
                    }
                    catch (Exception ex)
                    {
                        MessageUtil.ShowError("数量录入错误");
                        errorflag = false;
                        break;
                    }
                    odim.Order_ID = tbx_订单编码.Text;
                    //货源确定号从信息记录读取
                    odim.PR_ID = "";
                    //
                    try
                    {
                        odim.Stock_ID = dgv_采购项目.Rows[i].Cells["cln_库存地点"].Value.ToString();
                    }
                    catch (Exception ex)
                    {
                        odim.Stock_ID = "";
                    }
                    //odim.Total_Price = dgv_采购项目.Rows[i].Cells["cln_净价"]
                    try
                    {
                        odim.Unit = dgv_采购项目.Rows[i].Cells["cln_单位"].Value.ToString();
                    }
                    catch (Exception ex)
                    {
                        odim.Unit = "EA";
                    }
                    try
                    {
                        odim.Purchase_ID = dgv_采购项目.Rows[i].Cells["cln_请求号码"].Value.ToString();
                    }
                    catch (Exception ex)
                    {
                        odim.Purchase_ID = "";
                    }
                   
                    sum += odim.Net_Price * odim.Number;
                    sumpc += odim.Number;
                    //将订单项目加入列表
                    if (errorflag == true)
                    {
                        listo.Add(odim);
                    }
                }
            }
            if (count == 0)
            {
                MessageBox.Show("没有选中任何项目");
                return;
            }
            else
            {
                if (errorflag == true)
                {
                    odif.Total_Value = sum;
                    tbx_总价值.Text = Convert.ToString(sum);
                    tbx_已订购数量.Text = Convert.ToString(sumpc);
                    tbx_需交货数量.Text = Convert.ToString(sumpc);
                    tbx_已订购金额.Text = Convert.ToString(sum);
                    tbx_需交货金额.Text = Convert.ToString(sum);
                    //插入信息记录
                    try
                    {
                        odal.InsertOrderInfo(odif, listo);
                        MessageBox.Show("修改成功");
                    }
                    catch (DBException ex)
                    {
                        MessageBox.Show("修改失败");
                    }

                }
            }
        }
       
        /// <summary>
        /// 选择一个订单
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnQueryOrder_Click(object sender, EventArgs e)
        {
            if (quryOrder == null || quryOrder.IsDisposed)
            {
                quryOrder = new QuryOrder(this);
            }
            quryOrder.Show();
        }
        /// <summary>
        /// Edit 
        /// </summary>
        public void initData(string orderid)
        {
            if (string.IsNullOrEmpty(orderid))
            {
                MessageBox.Show("无订单号可用");
                return;
            }
            this.orderid = orderid;
            int summtdocnu = 0;
            float summtdocvl = 0;
            int suminvonu = 0;
            float suminvovl = 0;
            tbx_订单编码.Text = orderid;
            List<InvoiceFront> listinvo = odal.GetInvoiceFrontByID(orderid);
            if (listinvo.Count == 0)
            {
                MessageBox.Show("无数据");
                return;
            }
            for (int i = 0; i < listinvo.Count; i++)
            {
                dgv_发票信息.Rows.Add();
                dgv_发票信息.Rows[i].Cells["Invoice_Code"].Value = listinvo[i].Invoice_Code;
                dgv_发票信息.Rows[i].Cells["Invoice_Number"].Value = listinvo[i].Invoice_Number;
                dgv_发票信息.Rows[i].Cells["Certificate_Type"].Value = listinvo[i].Certificate_Type;
                dgv_发票信息.Rows[i].Cells["Certificate_Code"].Value = listinvo[i].Certificate_Code;
                dgv_发票信息.Rows[i].Cells["Invoice_Makeout_Time"].Value = listinvo[i].Invoice_Makeout_Time;
                dgv_发票信息.Rows[i].Cells["Create_Time"].Value = listinvo[i].Create_Time;
                dgv_发票信息.Rows[i].Cells["Payment_Type"].Value = listinvo[i].Payment_Type;
                dgv_发票信息.Rows[i].Cells["sum"].Value = listinvo[i].sum;
                dgv_发票信息.Rows[i].Cells["Currency"].Value = listinvo[i].Currency;
                dgv_发票信息.Rows[i].Cells["Sum_Number"].Value = listinvo[i].Sum_Number;
                suminvonu += listinvo[i].Sum_Number;
                suminvovl += (float)Convert.ToDouble(listinvo[i].sum);
            }
            List<MaterialDocumentMM> listmtdoc = odal.GetMaterialDocumentMMByID(orderid);
            if (listmtdoc.Count <= 0)
            {
                MessageBox.Show("无数据");
                return;
            }
            for (int j = 0; j < listmtdoc.Count; j++)
            {
                dgv_收货信息.Rows.Add();
                dgv_收货信息.Rows[j].Cells["StockDocumentId"].Value = listmtdoc[j].StockDocumentId;
                dgv_收货信息.Rows[j].Cells["Order_ID"].Value = listmtdoc[j].Order_ID;
                dgv_收货信息.Rows[j].Cells["Delivery_ID"].Value = listmtdoc[j].Delivery_ID;
                dgv_收货信息.Rows[j].Cells["ReceiptNote_ID"].Value = listmtdoc[j].ReceiptNote_ID;
                dgv_收货信息.Rows[j].Cells["Posting_Date"].Value = listmtdoc[j].Posting_Date;
                dgv_收货信息.Rows[j].Cells["Document_Date"].Value = listmtdoc[j].Document_Date;
                dgv_收货信息.Rows[j].Cells["StockManager"].Value = listmtdoc[j].StockManager;
                dgv_收货信息.Rows[j].Cells["Move_Type"].Value = listmtdoc[j].Move_Type;
                dgv_收货信息.Rows[j].Cells["Reversed"].Value = listmtdoc[j].Reversed;
                dgv_收货信息.Rows[j].Cells["Total_Number"].Value = listmtdoc[j].Total_Number;
                dgv_收货信息.Rows[j].Cells["Total_Value"].Value = listmtdoc[j].Total_Value;
                summtdocnu += listmtdoc[j].Total_Number;
                summtdocvl += listmtdoc[j].Total_Value;
            }
            List<OrderItem> listodim = odal.GetOrderItemByID(orderid);
            if (listodim.Count == 0)
            {
                MessageBox.Show("无数据");
                return;
            }

            for (int k = 0; k < listodim.Count; k++)
            {
                dgv_采购项目.Rows.Add();
                dgv_采购项目.Rows[k].Cells["cln_物料编码"].Value = listodim[k].Material_ID;
                dgv_采购项目.Rows[k].Cells["cln_物料描述"].Value = "";
                dgv_采购项目.Rows[k].Cells["cln_数量"].Value = listodim[k].Number;
                dgv_采购项目.Rows[k].Cells["cln_单位"].Value = listodim[k].Unit;
                dgv_采购项目.Rows[k].Cells["cln_净价"].Value = listodim[k].Net_Price;
                dgv_采购项目.Rows[k].Cells["cln_货币"].Value = "RMB";
                dgv_采购项目.Rows[k].Cells["cln_物料组"].Value = listodim[k].Material_Group;
                dgv_采购项目.Rows[k].Cells["cln_工厂"].Value = listodim[k].Factory_ID;
                dgv_采购项目.Rows[k].Cells["cln_库存地点"].Value = listodim[k].Stock_ID;
                dgv_采购项目.Rows[k].Cells["cln_批次"].Value = listodim[k].Batch_ID;
                dgv_采购项目.Rows[k].Cells["cln_请求号码"].Value = listodim[k].Purchase_ID;
                dgv_采购项目.Rows[k].Cells["cln_信息记录"].Value = listodim[k].Info_Number;


            }
            OrderInfo odinfo = odal.GetOrderInfoByID(orderid);
            dtp_创建订单时间.Value = odinfo.Create_Time;
            tbx_交货地点.Text = odinfo.Delivery_Points;
            try
            {
                dtp_交货时间.Value = odinfo.Delivery_Ttime;
            }
            catch (Exception)
            {
                dtp_交货时间.Value = DateTime.Now;
            }
            int demandnum = odinfo.Total_Number - summtdocnu;
            float demandvalue = odinfo.Total_Value - summtdocvl;
            tbx_交货方式.Text = odinfo.Delivery_Type;
            cbb_订单状态.Text = odinfo.Order_Status;
            cbb_订单类型.Text = "标准采购订单";
            tbx_总价值.Text = Convert.ToString(odinfo.Total_Value);
            tbx_已订购数量.Text = odinfo.Total_Number.ToString();
            tbx_已订购金额.Text = Convert.ToString(odinfo.Total_Value);
            tbx_已交货数量.Text = summtdocnu.ToString();
            tbx_已交货金额.Text = summtdocvl.ToString();
            tbx_需交货数量.Text = demandnum.ToString();
            tbx_需交货金额.Text = demandvalue.ToString();
            tbx_开发票数量.Text = suminvonu.ToString();
            tbx_开发票金额.Text = suminvovl.ToString();

        }
       
        /// <summary>
        /// 参照记录信息
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            if(this.tbx_订单编码.Text=="")
            {
                MessageBox.Show("请先选择一个订单");
                return;
            }
            if(infoForm==null|| infoForm.IsDisposed)
            {
                infoForm = new RecordInfoForm(this);
            }
            infoForm.Show();
        }

        private void cbb_项目_TextChanged(object sender, EventArgs e)
        {
            dgv_定价项目.Refresh();
            string determineid = this.cbb_项目.Text.ToString();
            try
            {
                string sql1 = "SELECT * FROM [PriceDetermine] WHERE priceDeterminedId='" + determineid + "'";
                DataTable dt = DBHelper.ExecuteQueryDT(sql1);
                int count = dt.Rows.Count;
                for (int i = 0; i < count; i++)
                {
                    dgv_定价项目.Rows.Add();
                    dgv_定价项目.Rows[i].Cells["cln_类型名称"].Value = dt.Rows[i]["itemName"].ToString();
                    dgv_定价项目.Rows[i].Cells["cln_金额"].Value = dt.Rows[i]["value"].ToString();
                    dgv_定价项目.Rows[i].Cells["cln_单位1"].Value = "PC";
                    dgv_定价项目.Rows[i].Cells["cln_定价值"].Value = dt.Rows[i]["value"].ToString();
                    dgv_定价项目.Rows[i].Cells["cln_货币1"].Value = "CNY";
                }
            }
            catch (DBException ex)
            {
                MessageBox.Show("数据加载失败");
                return;
            }

        }
    }
}
