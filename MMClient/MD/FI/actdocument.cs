﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MMClient.MD;
//using MMClient.RA;
//using CSharpChartExplorer;
using MMClient.MD.MT;
using MMClient.MD.SP;
using MMClient.MD.GN;
using Lib.Bll;
using Lib.Bll.MDBll.General;


namespace MMClient.MD.FI
{
    public partial class actdocument : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        public actdocument()
        {
            InitializeComponent();
        }
        public actdocument(DataTable dt,string e)
        {
            InitializeComponent();
            this.tbx_移动类型.Text = e;
            this.dgv_物料凭证.DataSource = dt;

            

            
        }

        private void actdocument_Load(object sender, EventArgs e)
        {
            AccountBLL ACT = new AccountBLL();
            GeneralBLL gn = new GeneralBLL();

            cbb_公司代码.DataSource = gn.GetAllCompany();
            cbb_公司代码.Text = "";
            cbb_货币编码.DataSource = ACT.GetCurrencyType();
            cbb_货币编码.Text = "";
            cln_评估类.DataSource = ACT.GetEvaluationClass();
            cln_供应商.DataSource = ACT.GetAllSupplier();
            cln_税码.DataSource = ACT.GetTaxCode();
            cln_工厂.DataSource = ACT.GetAllFactory();
            cln_会计科目.DataSource = ACT.GetAllAccount();
            
           
        }

        private void dgv_项目_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            AccountBLL ACT = new AccountBLL();
            if (dgv_项目.CurrentCell!=null && dgv_项目.CurrentCell.ColumnIndex == 1)
                dgv_项目.Rows[dgv_项目.CurrentCell.RowIndex].Cells[2].Value = ACT.GetSuppplierDescription(dgv_项目.CurrentCell.Value.ToString());
            if (dgv_项目.CurrentCell != null && dgv_项目.CurrentCell.ColumnIndex == 9)
            {
                DataGridViewComboBoxCell d = dgv_项目.Rows[dgv_项目.CurrentCell.RowIndex].Cells[10] as DataGridViewComboBoxCell;
                d.DataSource = ACT.GetAllStock(dgv_项目.CurrentCell.Value.ToString());
                //Console.Write(dgv_项目.CurrentCell.ToString());
                //List<string> list = ACT.GetAllStock(dgv_项目.CurrentCell.Value.ToString());
                //Console.Write(list);

            }

        }

        private void btn_load_Click(object sender, EventArgs e)
        {
            int index = this.dgv_项目.Rows.Add();
            //移动类型控制更新科目
            if (tbx_移动类型.Text.ToString() == "101")
            {
                this.dgv_项目.Rows[1].Cells[5].Value = "";
                this.dgv_项目.Rows[1].Cells[5].ReadOnly = true;
                this.dgv_项目.Rows[1].Cells[3].Value = "应付账款";
                this.dgv_项目.Rows[1].Cells[4].Value = "应付账款";
                this.dgv_项目.Rows[1].Cells[0].ReadOnly = true;
                this.dgv_项目.Rows[1].Cells[1].ReadOnly = true;
                this.dgv_项目.Rows[1].Cells[2].ReadOnly = true;
                this.dgv_项目.Rows[1].Cells[3].ReadOnly = true;
                this.dgv_项目.Rows[1].Cells[4].ReadOnly = true;
                this.dgv_项目.Rows[0].Cells[6].Value = "";
                this.dgv_项目.Rows[0].Cells[6].ReadOnly = true;
                

            }
            if (tbx_移动类型.Text.ToString() == "102")
            {
                this.dgv_项目.Rows[1].Cells[6].Value = "";
                this.dgv_项目.Rows[1].Cells[6].ReadOnly = true;
                this.dgv_项目.Rows[1].Cells[3].Value = "应付账款";
                this.dgv_项目.Rows[1].Cells[4].Value = "应付账款";
                this.dgv_项目.Rows[1].Cells[0].ReadOnly = true;
                this.dgv_项目.Rows[1].Cells[1].ReadOnly = true;
                this.dgv_项目.Rows[1].Cells[2].ReadOnly = true;
                this.dgv_项目.Rows[1].Cells[3].ReadOnly = true;
                this.dgv_项目.Rows[1].Cells[4].ReadOnly = true;
                this.dgv_项目.Rows[0].Cells[5].Value = "";
                this.dgv_项目.Rows[0].Cells[5].ReadOnly = true; 
            }
            if (tbx_移动类型.Text.ToString() == "103")
            {
                this.dgv_项目.Rows[0].Cells[6].Value = "";
                this.dgv_项目.Rows[0].Cells[6].ReadOnly = true;
                this.dgv_项目.Rows[0].Cells[3].Value = "GR/IR";
                this.dgv_项目.Rows[0].Cells[4].Value = "GR/IR科目";
                this.dgv_项目.Rows[0].Cells[3].ReadOnly = true;
                this.dgv_项目.Rows[0].Cells[4].ReadOnly = true;
                this.dgv_项目.Rows[1].Cells[5].Value = "";
                this.dgv_项目.Rows[1].Cells[5].ReadOnly = true;
                this.dgv_项目.Rows[1].Cells[3].Value = "应付账款";
                this.dgv_项目.Rows[1].Cells[4].Value = "应付账款";
                this.dgv_项目.Rows[1].Cells[0].ReadOnly = true;
                this.dgv_项目.Rows[1].Cells[1].ReadOnly = true;
                this.dgv_项目.Rows[1].Cells[2].ReadOnly = true;
                this.dgv_项目.Rows[1].Cells[3].ReadOnly = true;
                this.dgv_项目.Rows[1].Cells[4].ReadOnly = true;


            }
            if (tbx_移动类型.Text.ToString() == "104")
            {
                this.dgv_项目.Rows[1].Cells[6].Value = "";
                this.dgv_项目.Rows[1].Cells[6].ReadOnly = true;
                this.dgv_项目.Rows[1].Cells[3].Value = "GR/IR";
                this.dgv_项目.Rows[1].Cells[4].Value = "GR/IR科目";
                this.dgv_项目.Rows[1].Cells[3].ReadOnly = true;
                this.dgv_项目.Rows[1].Cells[4].ReadOnly = true;
                this.dgv_项目.Rows[0].Cells[5].Value = "";
                this.dgv_项目.Rows[0].Cells[5].ReadOnly = true;
                this.dgv_项目.Rows[0].Cells[3].Value = "应付账款";
                this.dgv_项目.Rows[0].Cells[4].Value = "应付账款";
                this.dgv_项目.Rows[0].Cells[0].ReadOnly = true;
                this.dgv_项目.Rows[0].Cells[1].ReadOnly = true;
                this.dgv_项目.Rows[0].Cells[2].ReadOnly = true;
                this.dgv_项目.Rows[0].Cells[3].ReadOnly = true;
                this.dgv_项目.Rows[0].Cells[4].ReadOnly = true;
            }
            if (tbx_移动类型.Text.ToString() == "105")
            {
                this.dgv_项目.Rows[1].Cells[5].Value = "";
                this.dgv_项目.Rows[1].Cells[5].ReadOnly = true;
                this.dgv_项目.Rows[1].Cells[3].Value = "GR/IR";
                this.dgv_项目.Rows[1].Cells[4].Value = "GR/IR";
                this.dgv_项目.Rows[1].Cells[0].ReadOnly = true;
                this.dgv_项目.Rows[1].Cells[1].ReadOnly = true;
                this.dgv_项目.Rows[1].Cells[2].ReadOnly = true;
                this.dgv_项目.Rows[1].Cells[3].ReadOnly = true;
                this.dgv_项目.Rows[1].Cells[4].ReadOnly = true;
                this.dgv_项目.Rows[0].Cells[6].Value = "";
                this.dgv_项目.Rows[0].Cells[6].ReadOnly = true;
            }
            if (tbx_移动类型.Text.ToString() == "106")
            {
                this.dgv_项目.Rows[1].Cells[6].Value = "";
                this.dgv_项目.Rows[1].Cells[6].ReadOnly = true;
                this.dgv_项目.Rows[1].Cells[3].Value = "GR/IR";
                this.dgv_项目.Rows[1].Cells[4].Value = "GR/IR";
                this.dgv_项目.Rows[1].Cells[0].ReadOnly = true;
                this.dgv_项目.Rows[1].Cells[1].ReadOnly = true;
                this.dgv_项目.Rows[1].Cells[2].ReadOnly = true;
                this.dgv_项目.Rows[1].Cells[3].ReadOnly = true;
                this.dgv_项目.Rows[1].Cells[4].ReadOnly = true;
                this.dgv_项目.Rows[0].Cells[5].Value = "";
                this.dgv_项目.Rows[0].Cells[5].ReadOnly = true;
            }
            if (tbx_移动类型.Text.ToString() == "122")
            {
                this.dgv_项目.Rows[1].Cells[6].Value = "";
                this.dgv_项目.Rows[1].Cells[6].ReadOnly = true;
                this.dgv_项目.Rows[1].Cells[3].Value = "银行存款";
                this.dgv_项目.Rows[1].Cells[4].Value = "银行存款";
                this.dgv_项目.Rows[1].Cells[0].ReadOnly = true;
                this.dgv_项目.Rows[1].Cells[1].ReadOnly = true;
                this.dgv_项目.Rows[1].Cells[2].ReadOnly = true;
                this.dgv_项目.Rows[1].Cells[3].ReadOnly = true;
                this.dgv_项目.Rows[1].Cells[4].ReadOnly = true;
                this.dgv_项目.Rows[0].Cells[5].Value = "";
                this.dgv_项目.Rows[0].Cells[5].ReadOnly = true;
            }
            if (tbx_移动类型.Text.ToString() == "123")
            {
                this.dgv_项目.Rows[1].Cells[5].Value = "";
                this.dgv_项目.Rows[1].Cells[5].ReadOnly = true;
                this.dgv_项目.Rows[1].Cells[3].Value = "应付账款";
                this.dgv_项目.Rows[1].Cells[4].Value = "应付账款";
                this.dgv_项目.Rows[1].Cells[0].ReadOnly = true;
                this.dgv_项目.Rows[1].Cells[1].ReadOnly = true;
                this.dgv_项目.Rows[1].Cells[2].ReadOnly = true;
                this.dgv_项目.Rows[1].Cells[3].ReadOnly = true;
                this.dgv_项目.Rows[1].Cells[4].ReadOnly = true;
                this.dgv_项目.Rows[0].Cells[6].Value = "";
                this.dgv_项目.Rows[0].Cells[6].ReadOnly = true;


            }
            if (tbx_移动类型.Text.ToString() == "124")
            {

            }
        }

        private void btn_过账_Click(object sender, EventArgs e)
        {
            AccountBLL ACT = new AccountBLL();
            string a = dgv_项目.Rows[0].Cells[5].Value.ToString()+dgv_项目.Rows[0].Cells[6].Value.ToString();
            string b = dgv_项目.Rows[1].Cells[5].Value.ToString()+dgv_项目.Rows[1].Cells[6].Value.ToString();
            DateTime time;
            string number;
            float dmoney;
            float cmoney;
            string factory;
            string stock;
            string type="111";
            string supplierID;
            string AccountID1;
            string AccountID2;
            string account0=dgv_项目.Rows[0].Cells[3].Value.ToString();
            string account1=dgv_项目.Rows[1].Cells[3].Value.ToString();
            //string account2=dgv_项目.Rows[2].Cells[3].Value.ToString();
            time = dtb_凭证日期.Value.Date;
            number = tbx_凭证编码.Text;
            string ccode = cbb_公司代码.Text;

            bool bl = ACT.IsEqual(a, b);
            if (bl == false)
            {
                MessageBox.Show("借方和贷方不相等");
            }
            else
            {
                if (tbx_移动类型.Text.ToString() == "101")
                {
                   
                    dmoney = (float)Convert.ToDouble(this.dgv_项目.Rows[0].Cells[5].Value);
                    cmoney = 0;
                    factory = this.dgv_项目.Rows[0].Cells[9].Value.ToString();
                    stock = this.dgv_项目.Rows[0].Cells[10].Value.ToString();
                    //type = this.dgv_项目.Rows[0].Cells[0].Value.ToString();
                    supplierID = this.dgv_项目.Rows[0].Cells[2].Value.ToString();
                    AccountID1 = this.dgv_项目.Rows[0].Cells[3].Value.ToString();
                    AccountID2 = this.dgv_项目.Rows[1].Cells[3].Value.ToString();
                    if (account0 == "原材料")
                    {
                        ACT.DebitRawMaterial(time, number, dmoney,cmoney, factory, stock, type);
                        ACT.CreditAccountsPayable(time, number, dmoney);
                    }
                    if (account0 == "服务类")
                    {
                        ACT.DebitServiceClass(time, number, dmoney, type);
                        ACT.CreditAccountsPayable(time, number, dmoney);
                    }
                    if (account0 == "成品")
                    {
                        ACT.DebitFinishedProduct(time, number, dmoney, factory, stock, type);
                        ACT.CreditAccountsPayable(time, number, dmoney);
                    }
                    //写入会计凭证查询表
                    ACT.WriteAccountFrount(number, time, "", ccode);
                    //ACT.WriteAccountQuery(number,"",supplierID,AccountID1,dmoney,0,factory,stock);
                    //ACT.WriteAccountQuery(number, "", "", AccountID2, 0, dmoney, "", "");
                }
                if (tbx_移动类型.Text.ToString() == "102")
                {
                    dmoney = 0;
                    cmoney = (float)Convert.ToDouble(this.dgv_项目.Rows[0].Cells[6].Value);
                    factory = this.dgv_项目.Rows[0].Cells[9].Value.ToString();
                    stock = this.dgv_项目.Rows[0].Cells[10].Value.ToString();
                    //type = this.dgv_项目.Rows[0].Cells[0].Value.ToString();
                    supplierID = this.dgv_项目.Rows[0].Cells[2].Value.ToString();
                    AccountID1 = this.dgv_项目.Rows[0].Cells[3].Value.ToString();
                    AccountID2 = this.dgv_项目.Rows[1].Cells[3].Value.ToString();
                    if (account0 == "原材料")
                    {
                        ACT.DebitRawMaterial(time, number, dmoney,cmoney, factory, stock, type);
                        ACT.CreditAccountsPayable(time, number, cmoney);
                    }
                    if (account0 == "服务类")
                    {
                        ACT.DebitServiceClass(time, number, cmoney, type);
                        ACT.CreditAccountsPayable(time, number, cmoney);
                    }
                    if (account0 == "成品")
                    {
                        ACT.DebitFinishedProduct(time, number, cmoney, factory, stock, type);
                        ACT.CreditAccountsPayable(time, number, cmoney);
                    }
                    //写入会计凭证查询表
                    //ACT.WriteAccountFrount(number, time, "", ccode);
                    //ACT.WriteAccountQuery(number, "", supplierID, AccountID1, cmoney, 0, factory, stock);
                    //ACT.WriteAccountQuery(number, "", "", AccountID2, 0, cmoney, "", "");
                }
                MessageBox.Show("过账成功");
               
            }

        }

       
       
       

       
         
        
    }
}
