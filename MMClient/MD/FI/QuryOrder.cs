﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.SqlServerDAL;
using Lib.Common.CommonUtils;
using Lib.Common.MMCException.Bll;
using Lib.Common.MMCException.IDAL;

namespace MMClient.MD.FI
{
    public partial class QuryOrder : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        private string condition = "where 1=1";
        private Order orderQuery = null;
        public QuryOrder()
        {
            InitializeComponent();
            initLoad();
        }
        /// <summary>
        /// 传递参数orderId
        /// </summary>
        /// <param name="order"></param>
        public QuryOrder(Order order)
        {
            this.orderQuery= order;
            InitializeComponent();
            initLoad();
            
        }

        /// <summary>
        /// 加载订单编码
        /// </summary>
        private void initLoad()
        {
            string sql = "select Order_ID  as id from Order_Info";
            try
            {
                this.cbb_精确订单编码.DataSource = DBHelper.ExecuteQueryDT(sql);
                this.cbb_精确订单编码.DisplayMember = "id";
                this.cbb_精确订单编码.ValueMember = "id";
            }
            catch (DBException ex)
            {
                MessageBox.Show("数据加载失败");
            }
            string sql2 = "SELECT Order_ID as 订单编号,Order_Type as 订单类型,Order_Status as 订单状态 ,Purchase_Organization as 采购组织,Supplier_ID as 供应商编号, Total_Number as 订单总数,Total_Value as 总金额,Delivery_Type as 物流方式  FROM [Order_Info]";
            try
            {
                DataTable dt = DBHelper.ExecuteQueryDT(sql2);
                if (dt.Rows.Count == 0)
                {
                    MessageBox.Show("当前无数据");
                }
                else
                    dgv_查询结果.DataSource = dt;
            }
            catch (DBException ex)
            {
                MessageBox.Show("查询失败");
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (cbb_精确订单编码.Text != null)
            {
                condition = "where 1=1";
                condition += " AND Order_ID= '" + cbb_精确订单编码.Text + "'";
                string sql = "SELECT Order_ID as 订单编号,Order_Type as 订单类型,Order_Status as 订单状态 ,Purchase_Organization as 采购组织,Supplier_ID as 供应商编号, Total_Number as 订单总数,Total_Value as 总金额,Delivery_Type as 物流方式  FROM [Order_Info] "+condition;
               try
                {
                    DataTable dt = DBHelper.ExecuteQueryDT(sql);
                    if (dt.Rows.Count == 0)
                    {
                        MessageUtil.ShowError("没有查询的订单编码");
                    }
                    else
                        dgv_查询结果.DataSource = dt;
                }
                catch (DBException ex)
                {
                    MessageBox.Show("查询失败");
                }
            }
            pageTool_Load(sender, e);
            condition = "where 1=1";
        }
        /// <summary>
        /// 查看凭证信息
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_查看凭证信息_Click(object sender, EventArgs e)
        {
            ProofRelationship prt = new ProofRelationship();
            prt.Show();
        }

        private void pageTool_Load(object sender, EventArgs e)
        {
            string sql = "SELECT count(*)  FROM [Order_Info] " + condition;
            try
            {
                int i = int.Parse(DBHelper.ExecuteQueryDT(sql).Rows[0][0].ToString());
                int totalNum = i;
                pageTool.DrawControl(totalNum);
                //事件改变调用函数
                pageTool.OnPageChanged += pageTool_OnPageChanged;
            }
            catch (Exception ex)
            {
                MessageBox.Show("数据库异常，请稍后重试");
                return;
            }
        }

        private void pageTool_OnPageChanged(object sender, EventArgs e)
        {
            LoadData();
        }
        private void LoadData()
        {
            try
            {
                dgv_查询结果.DataSource = findConditionalInforByPage(this.pageTool.PageSize, this.pageTool.PageIndex);
            }
            catch (BllException ex)
            {
                MessageUtil.ShowError(ex.Message);
            }
        }
        private object findConditionalInforByPage(int pageSize, int pageIndex)
        {
            try
            {
                string sql = @"SELECT top " + pageSize + " Order_ID as 订单编号,Order_Type as 订单类型 ,Order_Status as 订单状态 ,Purchase_Organization as 采购组织,Supplier_ID as 供应商编号, Total_Number as 订单总数,Total_Value as 总金额,Delivery_Type as 物流方式  FROM [Order_Info]  where Order_ID not in(select top " + pageSize * (pageIndex - 1) + " Order_ID from Order_Info ORDER BY Order_ID ASC)ORDER BY Order_ID";
                return DBHelper.ExecuteQueryDT(sql);
            }
            catch (DBException ex)
            {
                throw new BllException("当前无数据", ex);
            };
        }

        private void button2_Click(object sender, EventArgs e)
        {

            string sql = " SELECT Order_ID as 订单编号,Order_Type as 订单类型,Order_Status as 订单状态 ,Purchase_Organization as 采购组织,Supplier_ID as 供应商编号, Total_Number as 订单总数,Total_Value as 总金额,Delivery_Type as 物流方式  FROM [Order_Info]   ";
           
            if (tbx_订单编码1.Text != "" && tbx_订单编码2.Text != "")
            {
                condition += " AND Order_ID BETWEEN '" + tbx_订单编码1.Text + "' AND '" + tbx_订单编码2.Text + "'";
            }
            if (tbx_订单数量1.Text != "" && 订单数量2.Text != "")
            {
                condition += " AND Total_Number BETWEEN '" + tbx_订单数量1.Text + "' AND '" + 订单数量2.Text + "'";
            }
            if (tbx_总金额1.Text != "" && tbx_总金额2.Text != "")
            {
                condition += " AND Total_Value BETWEEN '" + tbx_总金额1.Text + "' AND '" + tbx_总金额2.Text + "'";
            }
            if (tbx_供应商编码1.Text != "" && tbx_供应商编码2.Text != "")
            { 
                condition += " AND Supplier_ID Between '" + tbx_供应商编码1.Text + "'AND '" + tbx_供应商编码2.Text + "'";
            }
            try
            {
                if(!condition.Equals("where 1=1") && "where 1=1" != condition)
                {
                    sql += condition;
                }
                DataTable dt = DBHelper.ExecuteQueryDT(sql);
                if (dt.Rows.Count == 0)
                {
                    MessageBox.Show("没有查询的订单编码");
                }
                else
                    dgv_查询结果.DataSource = dt;
            }
            catch (DBException ex)
            {
                MessageBox.Show("查询失败");
            }
            pageTool_Load(sender, e);
            condition = "where 1=1";
        }
        /// <summary>
        /// 选择订单
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button3_Click(object sender, EventArgs e)
        {
            if (dgv_查询结果.CurrentRow == null)
            {
                MessageUtil.ShowError("没有选中任何项目");
                return;
            }
            else
            {
                int rowindex = dgv_查询结果.CurrentRow.Index;
                if (dgv_查询结果.Rows[rowindex].Cells[0] != null)
                {
                    string odid = dgv_查询结果.Rows[rowindex].Cells[0].Value.ToString();
                    if (!string.IsNullOrEmpty(odid) && orderQuery != null)
                    {
                        orderQuery.orderid = odid;
                        orderQuery.initData(orderQuery.orderid);
                        this.Close();
                    }
                }
            }
        }
    }
}
