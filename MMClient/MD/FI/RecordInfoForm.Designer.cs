﻿namespace MMClient.MD.FI
{
    partial class RecordInfoForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.BtnReset = new System.Windows.Forms.Button();
            this.pageTool = new pager.pagetool.pageNext();
            this.dgv_信息记录 = new System.Windows.Forms.DataGridView();
            this.Record_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Supplier_ID1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Supplier_Name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Material_ID1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Material_Name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Net_Price = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.validationPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Price_Determine = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Factory_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Stock_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btn_参照选定的信息记录 = new System.Windows.Forms.Button();
            this.btnQueryInfo = new System.Windows.Forms.Button();
            this.tbx_参照编码 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cbb_参照类型 = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_信息记录)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.BtnReset);
            this.panel1.Controls.Add(this.pageTool);
            this.panel1.Controls.Add(this.dgv_信息记录);
            this.panel1.Controls.Add(this.btn_参照选定的信息记录);
            this.panel1.Controls.Add(this.btnQueryInfo);
            this.panel1.Controls.Add(this.tbx_参照编码);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.cbb_参照类型);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(13, 13);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1089, 637);
            this.panel1.TabIndex = 0;
            // 
            // BtnReset
            // 
            this.BtnReset.Location = new System.Drawing.Point(768, 23);
            this.BtnReset.Name = "BtnReset";
            this.BtnReset.Size = new System.Drawing.Size(113, 26);
            this.BtnReset.TabIndex = 22;
            this.BtnReset.Text = "重置";
            this.BtnReset.UseVisualStyleBackColor = true;
            this.BtnReset.Click += new System.EventHandler(this.BtnReset_Click);
            // 
            // pageTool
            // 
            this.pageTool.Location = new System.Drawing.Point(201, 501);
            this.pageTool.Name = "pageTool";
            this.pageTool.PageIndex = 1;
            this.pageTool.PageSize = 100;
            this.pageTool.RecordCount = 0;
            this.pageTool.Size = new System.Drawing.Size(685, 38);
            this.pageTool.TabIndex = 21;
            this.pageTool.OnPageChanged += new System.EventHandler(this.pageTool_OnPageChanged);
            this.pageTool.Load += new System.EventHandler(this.pageTool_Load);
            // 
            // dgv_信息记录
            // 
            this.dgv_信息记录.AllowUserToDeleteRows = false;
            this.dgv_信息记录.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_信息记录.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllHeaders;
            this.dgv_信息记录.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dgv_信息记录.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_信息记录.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Record_ID,
            this.Supplier_ID1,
            this.Supplier_Name,
            this.Material_ID1,
            this.Material_Name,
            this.Net_Price,
            this.validationPrice,
            this.Price_Determine,
            this.Factory_ID,
            this.Stock_ID});
            this.dgv_信息记录.Location = new System.Drawing.Point(14, 96);
            this.dgv_信息记录.MultiSelect = false;
            this.dgv_信息记录.Name = "dgv_信息记录";
            this.dgv_信息记录.ReadOnly = true;
            this.dgv_信息记录.RowTemplate.Height = 24;
            this.dgv_信息记录.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv_信息记录.Size = new System.Drawing.Size(1053, 388);
            this.dgv_信息记录.TabIndex = 13;
            // 
            // Record_ID
            // 
            this.Record_ID.DataPropertyName = "记录编码";
            this.Record_ID.HeaderText = "记录编码";
            this.Record_ID.Name = "Record_ID";
            this.Record_ID.ReadOnly = true;
            // 
            // Supplier_ID1
            // 
            this.Supplier_ID1.DataPropertyName = "供应商编码";
            this.Supplier_ID1.HeaderText = "供应商编码";
            this.Supplier_ID1.Name = "Supplier_ID1";
            this.Supplier_ID1.ReadOnly = true;
            // 
            // Supplier_Name
            // 
            this.Supplier_Name.DataPropertyName = "供应商名称";
            this.Supplier_Name.HeaderText = "供应商名称";
            this.Supplier_Name.Name = "Supplier_Name";
            this.Supplier_Name.ReadOnly = true;
            // 
            // Material_ID1
            // 
            this.Material_ID1.DataPropertyName = "物料编码";
            this.Material_ID1.HeaderText = "物料编码";
            this.Material_ID1.Name = "Material_ID1";
            this.Material_ID1.ReadOnly = true;
            // 
            // Material_Name
            // 
            this.Material_Name.DataPropertyName = "物料名称";
            this.Material_Name.HeaderText = "物料名称";
            this.Material_Name.Name = "Material_Name";
            this.Material_Name.ReadOnly = true;
            // 
            // Net_Price
            // 
            this.Net_Price.DataPropertyName = "价格";
            this.Net_Price.HeaderText = "确定价";
            this.Net_Price.Name = "Net_Price";
            this.Net_Price.ReadOnly = true;
            // 
            // validationPrice
            // 
            this.validationPrice.DataPropertyName = "有效价";
            this.validationPrice.HeaderText = "有效价";
            this.validationPrice.Name = "validationPrice";
            this.validationPrice.ReadOnly = true;
            // 
            // Price_Determine
            // 
            this.Price_Determine.DataPropertyName = "价格确定编号";
            this.Price_Determine.HeaderText = "价格确定编号";
            this.Price_Determine.Name = "Price_Determine";
            this.Price_Determine.ReadOnly = true;
            // 
            // Factory_ID
            // 
            this.Factory_ID.DataPropertyName = "工厂编号";
            this.Factory_ID.HeaderText = "工厂编号";
            this.Factory_ID.Name = "Factory_ID";
            this.Factory_ID.ReadOnly = true;
            // 
            // Stock_ID
            // 
            this.Stock_ID.DataPropertyName = "库存编号";
            this.Stock_ID.HeaderText = "库存编号";
            this.Stock_ID.Name = "Stock_ID";
            this.Stock_ID.ReadOnly = true;
            // 
            // btn_参照选定的信息记录
            // 
            this.btn_参照选定的信息记录.Location = new System.Drawing.Point(488, 566);
            this.btn_参照选定的信息记录.Name = "btn_参照选定的信息记录";
            this.btn_参照选定的信息记录.Size = new System.Drawing.Size(122, 38);
            this.btn_参照选定的信息记录.TabIndex = 12;
            this.btn_参照选定的信息记录.Text = "确  定";
            this.btn_参照选定的信息记录.UseVisualStyleBackColor = true;
            this.btn_参照选定的信息记录.Click += new System.EventHandler(this.btn_参照选定的信息记录_Click);
            // 
            // btnQueryInfo
            // 
            this.btnQueryInfo.Location = new System.Drawing.Point(628, 21);
            this.btnQueryInfo.Name = "btnQueryInfo";
            this.btnQueryInfo.Size = new System.Drawing.Size(113, 26);
            this.btnQueryInfo.TabIndex = 11;
            this.btnQueryInfo.Text = "查询信息记录";
            this.btnQueryInfo.UseVisualStyleBackColor = true;
            this.btnQueryInfo.Click += new System.EventHandler(this.btnQueryInfo_Click);
            // 
            // tbx_参照编码
            // 
            this.tbx_参照编码.Location = new System.Drawing.Point(441, 25);
            this.tbx_参照编码.Name = "tbx_参照编码";
            this.tbx_参照编码.Size = new System.Drawing.Size(121, 21);
            this.tbx_参照编码.TabIndex = 10;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(297, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(113, 12);
            this.label2.TabIndex = 9;
            this.label2.Text = "参照的信息记录编码";
            // 
            // cbb_参照类型
            // 
            this.cbb_参照类型.FormattingEnabled = true;
            this.cbb_参照类型.Location = new System.Drawing.Point(145, 24);
            this.cbb_参照类型.Name = "cbb_参照类型";
            this.cbb_参照类型.Size = new System.Drawing.Size(121, 20);
            this.cbb_参照类型.TabIndex = 8;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(113, 12);
            this.label1.TabIndex = 7;
            this.label1.Text = "参照的信息记录类型";
            // 
            // RecordInfoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1153, 662);
            this.Controls.Add(this.panel1);
            this.Name = "RecordInfoForm";
            this.Text = "选择信息记录";
            this.Load += new System.EventHandler(this.RecordInfoForm_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_信息记录)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btn_参照选定的信息记录;
        private System.Windows.Forms.Button btnQueryInfo;
        private System.Windows.Forms.TextBox tbx_参照编码;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cbb_参照类型;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dgv_信息记录;
        private System.Windows.Forms.DataGridViewTextBoxColumn Record_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Supplier_ID1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Supplier_Name;
        private System.Windows.Forms.DataGridViewTextBoxColumn Material_ID1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Material_Name;
        private System.Windows.Forms.DataGridViewTextBoxColumn Net_Price;
        private System.Windows.Forms.DataGridViewTextBoxColumn validationPrice;
        private System.Windows.Forms.DataGridViewTextBoxColumn Price_Determine;
        private System.Windows.Forms.DataGridViewTextBoxColumn Factory_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Stock_ID;
        private pager.pagetool.pageNext pageTool;
        private System.Windows.Forms.Button BtnReset;
    }
}