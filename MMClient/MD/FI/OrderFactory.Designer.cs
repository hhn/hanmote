﻿namespace MMClient.MD.FI
{
    partial class OrderFactory
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dtp_交货时间 = new System.Windows.Forms.DateTimePicker();
            this.dtp_开始时间 = new System.Windows.Forms.DateTimePicker();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.tbx_库存地 = new System.Windows.Forms.TextBox();
            this.cbb_库存地 = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.cbb_合同编号 = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.tbx_采购组织 = new System.Windows.Forms.TextBox();
            this.cbb_采购组织 = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tbx_短文本 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tbx_供应商 = new System.Windows.Forms.TextBox();
            this.cbb_供应商 = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tbx_工厂 = new System.Windows.Forms.TextBox();
            this.cbb_工厂 = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cbb_订单编码 = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dgv_采购项目 = new System.Windows.Forms.DataGridView();
            this.btn_保存订单 = new System.Windows.Forms.Button();
            this.cln_状态 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.cln_物料编码 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cln_物料描述 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cln_物料组 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cln_数量 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cln_单位 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cln_单价 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cln_货币 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cln_合同总数量 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cln_已完成数量 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_采购项目)).BeginInit();
            this.SuspendLayout();
            // 
            // supplierInforGroupBox
            // 
            this.groupBox1.Controls.Add(this.dtp_交货时间);
            this.groupBox1.Controls.Add(this.dtp_开始时间);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.tbx_库存地);
            this.groupBox1.Controls.Add(this.cbb_库存地);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.cbb_合同编号);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.tbx_采购组织);
            this.groupBox1.Controls.Add(this.cbb_采购组织);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.tbx_短文本);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.tbx_供应商);
            this.groupBox1.Controls.Add(this.cbb_供应商);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.tbx_工厂);
            this.groupBox1.Controls.Add(this.cbb_工厂);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.cbb_订单编码);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(4, 5);
            this.groupBox1.Name = "supplierInforGroupBox";
            this.groupBox1.Size = new System.Drawing.Size(1266, 150);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "基本头数据";
            // 
            // dtp_交货时间
            // 
            this.dtp_交货时间.Location = new System.Drawing.Point(849, 110);
            this.dtp_交货时间.Name = "dtp_交货时间";
            this.dtp_交货时间.Size = new System.Drawing.Size(156, 22);
            this.dtp_交货时间.TabIndex = 19;
            // 
            // dtp_开始时间
            // 
            this.dtp_开始时间.Location = new System.Drawing.Point(849, 74);
            this.dtp_开始时间.Name = "dtp_开始时间";
            this.dtp_开始时间.Size = new System.Drawing.Size(156, 22);
            this.dtp_开始时间.TabIndex = 18;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(753, 112);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(64, 17);
            this.label9.TabIndex = 17;
            this.label9.Text = "交货时间";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(753, 76);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(64, 17);
            this.label8.TabIndex = 16;
            this.label8.Text = "开始时间";
            // 
            // tbx_库存地
            // 
            this.tbx_库存地.Location = new System.Drawing.Point(599, 71);
            this.tbx_库存地.Name = "tbx_库存地";
            this.tbx_库存地.Size = new System.Drawing.Size(100, 22);
            this.tbx_库存地.TabIndex = 13;
            // 
            // cbb_库存地
            // 
            this.cbb_库存地.FormattingEnabled = true;
            this.cbb_库存地.Location = new System.Drawing.Point(472, 69);
            this.cbb_库存地.Name = "cbb_库存地";
            this.cbb_库存地.Size = new System.Drawing.Size(121, 24);
            this.cbb_库存地.TabIndex = 12;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(390, 74);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(50, 17);
            this.label7.TabIndex = 15;
            this.label7.Text = "库存地";
            // 
            // cbb_合同编号
            // 
            this.cbb_合同编号.FormattingEnabled = true;
            this.cbb_合同编号.Location = new System.Drawing.Point(849, 31);
            this.cbb_合同编号.Name = "cbb_合同编号";
            this.cbb_合同编号.Size = new System.Drawing.Size(121, 24);
            this.cbb_合同编号.TabIndex = 14;
            this.cbb_合同编号.SelectedIndexChanged += new System.EventHandler(this.cbb_合同编号_SelectedIndexChanged);
            this.cbb_合同编号.ValueMemberChanged += new System.EventHandler(this.cbb_合同编号_ValueMemberChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(753, 34);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(64, 17);
            this.label6.TabIndex = 13;
            this.label6.Text = "合同编号";
            // 
            // tbx_采购组织
            // 
            this.tbx_采购组织.Location = new System.Drawing.Point(599, 109);
            this.tbx_采购组织.Name = "tbx_采购组织";
            this.tbx_采购组织.Size = new System.Drawing.Size(100, 22);
            this.tbx_采购组织.TabIndex = 12;
            // 
            // cbb_采购组织
            // 
            this.cbb_采购组织.FormattingEnabled = true;
            this.cbb_采购组织.Location = new System.Drawing.Point(472, 107);
            this.cbb_采购组织.Name = "cbb_采购组织";
            this.cbb_采购组织.Size = new System.Drawing.Size(121, 24);
            this.cbb_采购组织.TabIndex = 11;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(390, 112);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(64, 17);
            this.label5.TabIndex = 10;
            this.label5.Text = "采购组织";
            // 
            // tbx_短文本
            // 
            this.tbx_短文本.Location = new System.Drawing.Point(472, 31);
            this.tbx_短文本.Name = "tbx_短文本";
            this.tbx_短文本.Size = new System.Drawing.Size(170, 22);
            this.tbx_短文本.TabIndex = 9;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(390, 34);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(50, 17);
            this.label4.TabIndex = 8;
            this.label4.Text = "短文本";
            // 
            // tbx_供应商
            // 
            this.tbx_供应商.Location = new System.Drawing.Point(221, 109);
            this.tbx_供应商.Name = "tbx_供应商";
            this.tbx_供应商.Size = new System.Drawing.Size(100, 22);
            this.tbx_供应商.TabIndex = 7;
            // 
            // cbb_供应商
            // 
            this.cbb_供应商.FormattingEnabled = true;
            this.cbb_供应商.Location = new System.Drawing.Point(94, 107);
            this.cbb_供应商.Name = "cbb_供应商";
            this.cbb_供应商.Size = new System.Drawing.Size(121, 24);
            this.cbb_供应商.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 110);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(50, 17);
            this.label3.TabIndex = 5;
            this.label3.Text = "供应商";
            // 
            // tbx_工厂
            // 
            this.tbx_工厂.Location = new System.Drawing.Point(221, 69);
            this.tbx_工厂.Name = "tbx_工厂";
            this.tbx_工厂.Size = new System.Drawing.Size(100, 22);
            this.tbx_工厂.TabIndex = 4;
            // 
            // cbb_工厂
            // 
            this.cbb_工厂.FormattingEnabled = true;
            this.cbb_工厂.Location = new System.Drawing.Point(94, 69);
            this.cbb_工厂.Name = "cbb_工厂";
            this.cbb_工厂.Size = new System.Drawing.Size(121, 24);
            this.cbb_工厂.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 74);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(36, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "工厂";
            // 
            // cbb_订单编码
            // 
            this.cbb_订单编码.FormattingEnabled = true;
            this.cbb_订单编码.Location = new System.Drawing.Point(94, 31);
            this.cbb_订单编码.Name = "cbb_订单编码";
            this.cbb_订单编码.Size = new System.Drawing.Size(121, 24);
            this.cbb_订单编码.TabIndex = 1;
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 34);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(64, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "订单编码";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dgv_采购项目);
            this.groupBox2.Location = new System.Drawing.Point(1, 167);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1269, 370);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "采购项目";
            // 
            // dgv_采购项目
            // 
            this.dgv_采购项目.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_采购项目.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.cln_状态,
            this.cln_物料编码,
            this.cln_物料描述,
            this.cln_物料组,
            this.cln_数量,
            this.cln_单位,
            this.cln_单价,
            this.cln_货币,
            this.cln_合同总数量,
            this.cln_已完成数量});
            this.dgv_采购项目.Location = new System.Drawing.Point(8, 21);
            this.dgv_采购项目.Name = "dgv_采购项目";
            this.dgv_采购项目.RowTemplate.Height = 24;
            this.dgv_采购项目.Size = new System.Drawing.Size(1152, 343);
            this.dgv_采购项目.TabIndex = 0;
            // 
            // btn_保存订单
            // 
            this.btn_保存订单.Location = new System.Drawing.Point(985, 550);
            this.btn_保存订单.Name = "btn_保存订单";
            this.btn_保存订单.Size = new System.Drawing.Size(113, 41);
            this.btn_保存订单.TabIndex = 2;
            this.btn_保存订单.Text = "保存订单";
            this.btn_保存订单.UseVisualStyleBackColor = true;
            this.btn_保存订单.Click += new System.EventHandler(this.btn_保存订单_Click);
            // 
            // cln_状态
            // 
            this.cln_状态.HeaderText = "状态";
            this.cln_状态.Name = "cln_状态";
            // 
            // cln_物料编码
            // 
            this.cln_物料编码.HeaderText = "物料编码";
            this.cln_物料编码.Name = "cln_物料编码";
            this.cln_物料编码.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.cln_物料编码.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // cln_物料描述
            // 
            this.cln_物料描述.HeaderText = "物料描述";
            this.cln_物料描述.Name = "cln_物料描述";
            // 
            // cln_物料组
            // 
            this.cln_物料组.HeaderText = "物料组";
            this.cln_物料组.Name = "cln_物料组";
            this.cln_物料组.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.cln_物料组.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // cln_数量
            // 
            this.cln_数量.HeaderText = "数量";
            this.cln_数量.Name = "cln_数量";
            // 
            // cln_单位
            // 
            this.cln_单位.HeaderText = "单位";
            this.cln_单位.Name = "cln_单位";
            this.cln_单位.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.cln_单位.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // cln_单价
            // 
            this.cln_单价.HeaderText = "单价";
            this.cln_单价.Name = "cln_单价";
            // 
            // cln_货币
            // 
            this.cln_货币.HeaderText = "货币";
            this.cln_货币.Name = "cln_货币";
            this.cln_货币.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.cln_货币.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // cln_合同总数量
            // 
            this.cln_合同总数量.HeaderText = "合同总数量";
            this.cln_合同总数量.Name = "cln_合同总数量";
            this.cln_合同总数量.ReadOnly = true;
            // 
            // cln_已完成数量
            // 
            this.cln_已完成数量.HeaderText = "已完成数量";
            this.cln_已完成数量.Name = "cln_已完成数量";
            this.cln_已完成数量.ReadOnly = true;
            // 
            // OrderFactory
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1282, 603);
            this.Controls.Add(this.btn_保存订单);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "OrderFactory";
            this.Text = "采购订单";
            this.Load += new System.EventHandler(this.OrderFactory_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_采购项目)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox tbx_供应商;
        private System.Windows.Forms.ComboBox cbb_供应商;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbx_工厂;
        private System.Windows.Forms.ComboBox cbb_工厂;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cbb_订单编码;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbx_库存地;
        private System.Windows.Forms.ComboBox cbb_库存地;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cbb_合同编号;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tbx_采购组织;
        private System.Windows.Forms.ComboBox cbb_采购组织;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tbx_短文本;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker dtp_交货时间;
        private System.Windows.Forms.DateTimePicker dtp_开始时间;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView dgv_采购项目;
        private System.Windows.Forms.Button btn_保存订单;
        private System.Windows.Forms.DataGridViewCheckBoxColumn cln_状态;
        private System.Windows.Forms.DataGridViewTextBoxColumn cln_物料编码;
        private System.Windows.Forms.DataGridViewTextBoxColumn cln_物料描述;
        private System.Windows.Forms.DataGridViewTextBoxColumn cln_物料组;
        private System.Windows.Forms.DataGridViewTextBoxColumn cln_数量;
        private System.Windows.Forms.DataGridViewTextBoxColumn cln_单位;
        private System.Windows.Forms.DataGridViewTextBoxColumn cln_单价;
        private System.Windows.Forms.DataGridViewTextBoxColumn cln_货币;
        private System.Windows.Forms.DataGridViewTextBoxColumn cln_合同总数量;
        private System.Windows.Forms.DataGridViewTextBoxColumn cln_已完成数量;
    }
}