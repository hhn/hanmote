﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.SqlServerDAL;
using Lib.Common.CommonUtils;
using Lib.Bll.MDBll.General;

namespace MMClient.MD.FI
{
    public partial class OrderFactory : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        string ContractID;
        public OrderFactory()
        {
            InitializeComponent();
        }
        public OrderFactory(string contractid)
        {
            InitializeComponent();
            this.ContractID = contractid;
            //根据合同编号获取合同信息
            GetContractItem(ContractID);

        }
        ComboBoxItem cbm = new ComboBoxItem();
        void GetContractItem(string contractid)
        {
            string sql1 = "SELECT * FROM[Agreement_Contract] WHERE Agreement_Contract_ID='" + contractid + "'";
            DataTable dt1 = DBHelper.ExecuteQueryDT(sql1);
            DataRow row ;
            if (dt1.Rows.Count!=0)
            {
                row = dt1.Rows[0];
                cbb_工厂.Text = row["Factory"].ToString();
                cbb_供应商.Text = row["Supplier_ID"].ToString();
                cbb_库存地.Text = row["Storage_Location"].ToString();
                cbb_采购组织.Text = row["Purchase_Organization"].ToString();
                cbb_合同编号.Text = contractid;
            }
            else
                MessageUtil.ShowError("没有该项合同");
            string sql2 = "SELECT * FROM [Agreement_Contract_Item] WHERE Agreement_Contract_ID='" + contractid + "'";
            DataTable dt2 = DBHelper.ExecuteQueryDT(sql2);
            if (dt2.Rows.Count == 0)
                MessageUtil.ShowError("该合同没有项目");
            else
            {
                int i;
                for (i = 0; i < dt2.Rows.Count; i++)
                {
                    dgv_采购项目.Rows.Add();
                    row = dt2.Rows[i];
                    dgv_采购项目.Rows[i].Cells[1].Value = row["Material_ID"].ToString();
                    dgv_采购项目.Rows[i].Cells[2].Value = row["Short_Text"].ToString();
                    dgv_采购项目.Rows[i].Cells[3].Value = row["Material_Group"].ToString();
                    dgv_采购项目.Rows[i].Cells[5].Value = row["OUn"].ToString();
                    dgv_采购项目.Rows[i].Cells[6].Value = row["Net_Price"].ToString();
                    dgv_采购项目.Rows[i].Cells[8].Value = row["Target_Quantity"].ToString();
                    dgv_采购项目.Rows[i].Cells[9].Value = row["Finished_Quantity"].ToString();


                    
                }
            }

            

        }

        private void btn_保存订单_Click(object sender, EventArgs e)
        {
            int flag = 1;
     

            if (cbb_订单编码.Text == "")
            {
                MessageUtil.ShowError("订单编码不能为空");
                flag = 0;
            }
            string orderID = cbb_订单编码.Text;
            string materialid;
            string materialname;
            string materialgroup;
            double number;
            string unit;
            double price;
            double totalnumber;
            double finishednumber;
            int count=0;
            int finish = 1;
            DataGridViewCheckBoxCell checkcell;
            if (flag == 1)
            {
                //录入订单头数据
                string sql1 = "INSERT INTO [Order_Factory_Front] (Order_ID,Supplier_ID,Supplier_Name,Porg_ID,Porg_Name,Order_Type,";
                sql1 += "Factory_ID,Factory_Name,Stock_ID,Stock_Name,Begin_Time,End_Time,Contract_ID,Order_Text)";
                sql1 += "VALUES ('" + cbb_订单编码.Text + "','" + cbb_供应商.Text + "','" + tbx_供应商.Text + "','" + cbb_采购组织.Text + "','" + tbx_采购组织.Text + "',";
                sql1 += "'" + cbb_订单编码.Text + "','" + cbb_工厂.Text + "','" + tbx_工厂.Text + "','" + cbb_库存地.Text + "','" + tbx_库存地.Text + "','" + Convert.ToDateTime(dtp_开始时间.Value) + "','" + Convert.ToDateTime(dtp_交货时间.Value) + "',";
                sql1 += "'" + cbb_合同编号.Text + "','" + tbx_短文本.Text + "')";
                DBHelper.ExecuteNonQuery(sql1);
                //录入订单项目
                for (int j = 0; j < dgv_采购项目.Rows.Count; j++)
                {
                    checkcell = (DataGridViewCheckBoxCell)dgv_采购项目.Rows[j].Cells[0];
                    if (Convert.ToBoolean(checkcell.Value) == true)
                    {
                        count++;
                        number =Convert.ToDouble( dgv_采购项目.Rows[j].Cells["cln_数量"].Value.ToString());
                        totalnumber = Convert.ToDouble(dgv_采购项目.Rows[j].Cells["cln_合同总数量"].Value.ToString());
                        finishednumber = Convert.ToDouble(dgv_采购项目.Rows[j].Cells["cln_已完成数量"].Value.ToString());
                        if (number < (totalnumber - finishednumber) || number == (totalnumber - finishednumber))
                        {
                            materialid = dgv_采购项目.Rows[j].Cells["cln_物料编码"].Value.ToString();
                            materialname = dgv_采购项目.Rows[j].Cells["cln_物料描述"].Value.ToString();
                            materialgroup = dgv_采购项目.Rows[j].Cells["cln_物料组"].Value.ToString();
                            number = Convert.ToDouble(dgv_采购项目.Rows[j].Cells["cln_数量"].Value.ToString());
                            unit = dgv_采购项目.Rows[j].Cells["cln_单位"].Value.ToString();
                            price = Convert.ToDouble(dgv_采购项目.Rows[j].Cells["cln_单价"].Value.ToString());
                            string sql = "INSERT INTO [Order_Factory_Inner](Order_ID,Material_ID,Material_Name,Material_Group,Number,Unit,Price)";
                            sql += "VALUES ('" + orderID + "','" + materialid + "','" + materialname + "','" + materialgroup + "','" + number + "','" + unit + "','" + price + "')";
                            DBHelper.ExecuteNonQuery(sql);
                            string sql2 = " UPDATE [Agreement_Contract_Item] SET Finished_Quantity='" + (number + finishednumber) + "' WHERE Agreement_Contract_ID='"+cbb_合同编号.Text+"'AND Material_ID='"+materialid+"'";
                            DBHelper.ExecuteNonQuery(sql2);
                            
                                //MessageUtil.ShowTips("已生成订单信息");
                            
                        }
                        else
                        {
                            MessageUtil.ShowError("采购数量大于合同剩余数量");
                            finish = 0;
                            
                        }

                    }
                }
                if (finish == 1)
                {
                    MessageUtil.ShowTips("订单录入成功");
                }
              
            }
        }

        private void cbb_合同编号_ValueMemberChanged(object sender, EventArgs e)
        {

        }

        private void cbb_合同编号_SelectedIndexChanged(object sender, EventArgs e)
        {
            string cid = cbb_合同编号.Text;
            GetContractItem(cid);
        }

        private void OrderFactory_Load(object sender, EventArgs e)
        {
            string sql = "SELECT *FROM [Agreement_Contract]";
          DataTable dt =  DBHelper.ExecuteQueryDT(sql);
          List<string> list = new List<string>();
          string str;
          for (int i = 0; i < dt.Rows.Count; i++)
          {
              str = dt.Rows[i][0].ToString();
              list.Add(str);
          }
          cbm.FuzzyQury(cbb_合同编号, list);

        }
    }
}
