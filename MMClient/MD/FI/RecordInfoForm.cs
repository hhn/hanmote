﻿using Lib.Common.MMCException.Bll;
using Lib.Common.MMCException.IDAL;
using Lib.SqlServerDAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MMClient.MD.FI
{
    public partial class RecordInfoForm : Form
    {
        private string condition = "";
        private Order order;

        public RecordInfoForm(Order order)
        {
            InitializeComponent();
            this.order = order;
        }

        private void pageTool_OnPageChanged(object sender, EventArgs e)
        {
            LoadData();
        }

        private void pageTool_Load(object sender, EventArgs e)
        {
           
            string sql = "SELECT count(*)  FROM [PurcharseInfoRecord] where 1 = 1  " + condition;
            try
            {
                int i = int.Parse(DBHelper.ExecuteQueryDT(sql).Rows[0][0].ToString());
                int totalNum = i;
                pageTool.DrawControl(totalNum);
                //事件改变调用函数
                pageTool.OnPageChanged += pageTool_OnPageChanged;
            }
            catch (DBException ex)
            {
                MessageBox.Show("数据库异常，请稍后重试");
                return;
            }
        }
        private void LoadData()
        {
            try
            {
                dgv_信息记录.DataSource = findConditionalInforByPage(this.pageTool.PageSize, this.pageTool.PageIndex);
            }
            catch (BllException ex)
            {
                MessageBox.Show("数据查询失败");
            }
        }
        private object findConditionalInforByPage(int pageSize, int pageIndex)
        {
            try
            {
                string sql = @"SELECT top " + pageSize + " mtGroupName as 物料名称 ,recordCode AS 记录编码,  supplierID AS 供应商编码, supplierName AS 供应商名称, mtName AS 物料编码, price AS 价格, validationPrice AS 有效价, factoryID AS 工厂编号, stockId AS 库存编号, Price_Determine as Price_Determine FROM PurcharseInfoRecord where 1 = 1 " + condition+ " AND  recordCode not in(select top " + pageSize * (pageIndex - 1) + " recordCode from PurcharseInfoRecord ORDER BY recordCode ASC)ORDER BY recordCode";
                return DBHelper.ExecuteQueryDT(sql);
            }
            catch (DBException ex)
            {
                throw new BllException("当前无数据");
            };
        }
        
        /// <summary>
        /// chaunchanshu 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_参照选定的信息记录_Click(object sender, EventArgs e)
        {
            order.cbb_项目.Items.Clear();
            if (this.dgv_信息记录.CurrentRow ==null || this.dgv_信息记录.CurrentRow.Cells["Price_Determine"].Value == null)
            {
                MessageBox.Show("无信息可选");
                return;
            }
            if(string.IsNullOrEmpty(this.dgv_信息记录.CurrentRow.Cells["Price_Determine"].Value.ToString()))
            {
                MessageBox.Show("无效的价格确定编号");
                return;
            }
            string Id = this.dgv_信息记录.CurrentRow.Cells["Price_Determine"].Value.ToString();
            string MaterialId = this.dgv_信息记录.CurrentRow.Cells["Material_ID1"].Value.ToString();
            DialogResult dr;
            dr = MessageBox.Show("是否参考编号为："+Id +"的价格确定", "确认", MessageBoxButtons.YesNoCancel,
                     MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2);
            if (dr == DialogResult.Yes)
            {
                order.lbRecordCode.Text = "已参照记录ID: " + Id;
                order.cbb_项目.Items.Add(Id);
                this.Close();
            }
                
        }

        private void RecordInfoForm_Load(object sender, EventArgs e)
        {
            string sql = "SELECT recodeType as name ,recodeCode as id  FROM RecodeType";
            try
            {
                DataTable dataTable = DBHelper.ExecuteQueryDT(sql);
                this.cbb_参照类型.DataSource = dataTable;
                this.cbb_参照类型.DisplayMember = "name";
                this.cbb_参照类型.ValueMember = "id";
            }
            catch (DBException ex)
            {
                MessageBox.Show("记录类型加载失败");
                return;
            }
        }
        /// <summary>
        ///  reset
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnReset_Click(object sender, EventArgs e)
        {
            condition = "";
            LoadData();
        }
        /// <summary>
        /// query
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnQueryInfo_Click(object sender, EventArgs e)
        {
            string type = this.cbb_参照类型.SelectedValue.ToString();
            string ID = this.tbx_参照编码.Text.ToString().Trim();
            condition = "";
            if (!string.IsNullOrEmpty(type))
            {
                condition += "AND recordClass = '" + type + "'  ";
            }
            if (!string.IsNullOrEmpty(ID))
            {
                condition += "AND recordCode = '" + ID + "'  ";
            }
            string sql = @"SELECT
	                            recordCode AS 记录编码,
	                            supplierID AS 供应商编码,
	                            supplierName AS 供应商名称,
	                            mtName AS 物料编码,
	                            price AS 价格,
	                            validationPrice AS 有效价,
	                            factoryID AS 工厂编号,
	                            stockId AS 库存编号,
                                mtGroupName as 物料名称,
                                Price_Determine as 价格确定编号
                            FROM
	                            PurcharseInfoRecord where 1 = 1 " + condition;
            try
            {
                DataTable dt = DBHelper.ExecuteQueryDT(sql);
                this.dgv_信息记录.DataSource = dt;
                pageTool_Load(sender, e);
            }
            catch (DBException ex)
            {
                MessageBox.Show("数据加载失败");
                return;
            }
        }
    }
}
