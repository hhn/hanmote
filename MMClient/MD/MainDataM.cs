﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MMClient.MD;
using MMClient.MD.SP;
using MMClient.MD.SE;
using MMClient.MD.GN;
using MMClient.MD.OG;
using MMClient.MD.FI;
using WeifenLuo.WinFormsUI.Docking;
using MMClient.MD.MT;

namespace MMClient
{
    public partial class MainDataM : Form
    {
        //初始化form；
        //物料主数据部分；
        MTQuery mtq = new MTQuery();
        MTRecord mtr = new MTRecord();
        MTModify mtm = new MTModify();
        MTMaintain mtmn = new MTMaintain();
        BatchFeature mtbes = new BatchFeature();
        //MTBatchIO mtbio = new MTBatchIO();
        //MTBatchMY mtbmy = new MTBatchMY();
        BatchQury mtbqy = new BatchQury();
        MTDistribution mtd = new MTDistribution();
        MTEngineering mte = new MTEngineering();
        MTEstablish1 mtet = new MTEstablish1();
        MTBUYER mtby = new MTBUYER();
        MTACT mta = new MTACT();
        存储视图 mts = new 存储视图();
        MTMRP mtmr = new MTMRP();

       

        //供应商主数据部分；
        SPDistribution spd = new SPDistribution();
        SPEstablish spe = new SPEstablish();
        SPMaintain spm = new SPMaintain();
        SPModify spmd = new SPModify();
        SPMPN spmp = new SPMPN();
        SPNMPN spn = new SPNMPN();
        SPQuery spq = new SPQuery();
        SPCPY1 spy1 = new SPCPY1();
        SPORG spg = new SPORG();

        //专家主数据部分；
        SEDistribution sed = new SEDistribution();
        SEEstablish see = new SEEstablish();
        SEMaintain sem = new SEMaintain();
        SEModify semd = new SEModify();
        SEQuery seq = new SEQuery();

        //价格主数据部分；
        //PRDistribution prd = new PRDistribution();
        //PREstablish pre = new PREstablish();
        //PRMaintain prm = new PRMaintain();
        //PRModify prmd = new PRModify();
        //PRQuery prq = new PRQuery();

        //一般设置部分；
        Currency cny = new Currency();
        Country cnty = new Country();
        //组织架构部分；
        OGCompany ogc = new OGCompany();
        OGFactory ogf = new OGFactory();
        OGPurchasing ogp = new OGPurchasing();
        OGStorage ogs = new OGStorage();
        //财务主数据部分
        actquery aty = new actquery();
        actdocument att = new actdocument();
        actsummy atm = new actsummy();
       
        public MainDataM()
        {
            InitializeComponent();


            //mtq.Show(this.dockPanel1, DockState.DockLeft);
            //mtet.Show(this.dockPanel1, DockState.DockLeft);
        }

        private void MainDataM_Load(object sender, EventArgs e)
        {


            
        }

        

        
        private void tV_md_AfterSelect(object sender, TreeViewEventArgs e)
        {
            //string ss = null;
            //ss = tV_md.SelectedNode.Text;

            //switch (ss.Trim())
            //{
            ////case "2":
            //    {
            //        mtr.TopLevel = false;
            //        mtr.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            //        mtr.Show(this.dockPanel1, DockState.Document);


            //    //        break;
            //    //    }
            //    case "MT_查询":
            //        {
                        
            //            mtq.TopLevel = false;
            //            mtq.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            //            mtq.Show(this.dockPanel1, DockState.Document);
            //            break;
            //        }
            //    case "MT_创建":
            //        {
                       
            //            mtet.TopLevel = false;
            //            mtet.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            //            mtet.Show(this.dockPanel1, DockState.Document);
            //            break;
            //        }
            //    case "MT_修改":
            //        {

            //            mtm.TopLevel = false;
            //            mtm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            //            mtm.Show(this.dockPanel1, DockState.Document);
            //            break;
            //        }
            //    case "MT_维护":
            //        {

            //            mtmn.TopLevel = false;
            //            mtmn.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            //            mtmn.Show(this.dockPanel1, DockState.Document);
            //            break;
            //        }
            //    case "MT_分发":
            //        {

            //            mtd.TopLevel = false;
            //            mtd.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            //            mtd.Show(this.dockPanel1, DockState.Document);
            //            break;
            //        }
            //    case "SP_查询":
            //        {

            //           spq.TopLevel = false;
            //            spq.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            //            spq.Show(this.dockPanel1, DockState.Document);
            //            break;
            //        }
            //}

            int tag = e.Node.Tag != null ? Convert.ToInt32(e.Node.Tag) : 0;
            switch (tag)
            {
                case 0:
                    break;


                case 10:

                    mtq.TopLevel = false;
                    mtq.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    mtq.Show(this.dockPanel1, DockState.Document);
                    break;


                case 11:
                    mtet.TopLevel = false;
                    mtet.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    mtet.Show(this.dockPanel1, DockState.Document);
                    break;
                case 12:
                    mtm.TopLevel = false;
                    mtm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    mtm.Show(this.dockPanel1, DockState.Document);
                    break;
                case 130:
                    mtmn.TopLevel = false;
                    mtmn.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    mtmn.Show(this.dockPanel1, DockState.Document);
                    break;
                case 131:
                    mtby.TopLevel = false;
                    mtby.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    mtby.Show(this.dockPanel1, DockState.Document);
                    break;
                case 132:
                    mta.TopLevel = false;
                    mta.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    mta.Show(this.dockPanel1, DockState.Document);
                    break;
                case 133:
                    mts.TopLevel = false;
                    mts.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    mts.Show(this.dockPanel1, DockState.Document);
                    break;
                case 134:
                    mtmr.TopLevel = false;
                    mtmr.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    mtmr.Show(this.dockPanel1, DockState.Document);
                    break;
                case 14:
                    mtd.TopLevel = false;
                    mtd.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    mtd.Show(this.dockPanel1, DockState.Document);
                    break;
                case 150:
                    mtbes.TopLevel = false;
                    mtbes.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    mtbes.Show(this.dockPanel1, DockState.Document);
                    break;
                //case 151:
                //    mtbmy.TopLevel = false;
                //    mtbmy.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                //    mtbmy.Show(this.dockPanel1, DockState.Document);
                //    break;
                case 152:
                    mtbqy.TopLevel = false;
                    mtbqy.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    mtbqy.Show(this.dockPanel1, DockState.Document);
                    break;
                //case 153:
                //    mtbio.TopLevel = false;
                //    mtbio.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                //    mtbio.Show(this.dockPanel1, DockState.Document);
                //    break;
                case 16:
                    mtr.TopLevel = false;
                    mtr.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    mtr.Show(this.dockPanel1, DockState.Document);
                    break;
                case 17:

                    mte.TopLevel = false;
                    mte.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    mte.Show(this.dockPanel1, DockState.Document);
                    break;
                case 20:
                    spq.TopLevel = false;
                    spq.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    spq.Show(this.dockPanel1, DockState.Document);
                    break;
                case 21:
                    spe.TopLevel = false;
                    spe.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    spe.Show(this.dockPanel1, DockState.Document);
                    break;
                case 22:

                    spmd.TopLevel = false;
                    spmd.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    spmd.Show(this.dockPanel1, DockState.Document);
                    break;
                case 23:
                    spd.TopLevel = false;
                    spd.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    spd.Show(this.dockPanel1, DockState.Document);
                    break;
                case 240:
                    spm.TopLevel = false;
                    spm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    spm.Show(this.dockPanel1, DockState.Document);
                    break;
                case 241:
                    spg.TopLevel = false;
                    spg.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    spg.Show(this.dockPanel1, DockState.Document);
                    break;
                case 242:
                    spy1.TopLevel = false;
                    spy1.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    spy1.Show(this.dockPanel1, DockState.Document);
                    break;
                case 250:
                    spmp.TopLevel = false;
                    spmp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    spmp.Show(this.dockPanel1, DockState.Document);
                    break;
                case 251:
                    spn.TopLevel = false;
                    spn.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    spn.Show(this.dockPanel1, DockState.Document);
                    break;
                case 30:
                    seq.TopLevel = false;
                    seq.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    seq.Show(this.dockPanel1, DockState.Document);
                    break;
                case 31:
                    see.TopLevel = false;
                    see.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    see.Show(this.dockPanel1, DockState.Document);
                    break;
                case 32:
                    semd.TopLevel = false;
                    semd.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    semd.Show(this.dockPanel1, DockState.Document);
                    break;
                case 33:
                    sed.TopLevel = false;
                    sed.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    sed.Show(this.dockPanel1, DockState.Document);
                    break;
                case 34:
                    sem.TopLevel = false;
                    sem.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    sem.Show(this.dockPanel1, DockState.Document);
                    break;
                //case 40:
                //    prq.TopLevel = false;
                //    prq.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                //    prq.Show(this.dockPanel1, DockState.Document);
                //    break;
                //case 41:
                //    pre.TopLevel = false;
                //    pre.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                //    pre.Show(this.dockPanel1, DockState.Document);
                //    break;
                //case 42:
                //    prmd.TopLevel = false;
                //    prmd.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                //    prmd.Show(this.dockPanel1, DockState.Document);
                //    break;
                //case 43:
                //    prd.TopLevel = false;
                //    prd.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                //    prd.Show(this.dockPanel1, DockState.Document);
                //    break;
                //case 44:
                //    prm.TopLevel = false;
                //    prm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                //    prm.Show(this.dockPanel1, DockState.Document);
                //    break;
                
                case 51:
                    cny.TopLevel = false;
                    cny.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    cny.Show(this.dockPanel1, DockState.Document);
                    break;
                case 52:
                    cnty.TopLevel = false;
                    cnty.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    cnty.Show(this.dockPanel1, DockState.Document);
                    break;
                case 60:
                    ogc.TopLevel = false;
                    ogc.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    ogc.Show(this.dockPanel1, DockState.Document);
                    break;
                case 61:
                    ogf.TopLevel = false;
                    ogf.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    ogf.Show(this.dockPanel1, DockState.Document);
                    break;
                case 62:
                    ogs.TopLevel = false;
                    ogs.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    ogs.Show(this.dockPanel1, DockState.Document);
                    break;
                case 63:
                    ogp.TopLevel = false;
                    ogp.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    ogp.Show(this.dockPanel1, DockState.Document);
                    break;
                case 70:
                      att.TopLevel = false;
                    att.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    att.Show(this.dockPanel1, DockState.Document);
                    break;
                case 71:
                    aty.TopLevel = false;
                    aty.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    aty.Show(this.dockPanel1, DockState.Document);
                    break;
                case 72:
                    atm.TopLevel = false;
                    atm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    atm.Show(this.dockPanel1, DockState.Document);
                    break;
            }

        }

        //private void tV_md_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        //{
        //    int tag = e.Node.Tag != null ? Convert.ToInt32(e.Node.Tag) : 0;
        //    switch (tag)
        //    {
        //        case 1:
        //            MessageBox.Show("1");
        //            break;
        //        case 2:
        //            MessageBox.Show("2");
        //            break;
        //    }

        //}


       


//        private void showform(int i,int j)
//        if(treeView1.SelectedNode==treeView1.Node[0].Nodes[0]) 
  
//{ 
  
//   subForm1.TopLevel=false; 
  
//   subForm1.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None; 
  
//   tableLayoutPanel1.Controls.Add(subForm1); 
  
//   tableLayoutPanel1.SetRow(subForm1, 1); 
  
//   subForm1.Show() 
  
//} 
  
//if(treeView1.SelectedNode==treeView1.Node[0].Nodes[1]) 
  
//{ 
  
//   subForm2.TopLevel=false; 
  
//   subForm2.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None; 
  
//   tableLayoutPanel1.Controls.Add(subForm2); 
  
//   tableLayoutPanel1.SetRow(subForm2, 1); 
  
//   subForm2.Show() 
  

    }
       


















    
}
