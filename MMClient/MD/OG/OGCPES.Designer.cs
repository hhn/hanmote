﻿namespace MMClient.MD.OG
{
    partial class OGCPES
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gp = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cbb_公司编码 = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cbb_公司名称 = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cbb_公司级别 = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cbb_公司性质 = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.cbb_公司地址 = new System.Windows.Forms.ComboBox();
            this.cbb_下属工厂 = new System.Windows.Forms.ComboBox();
            this.btn_新建 = new System.Windows.Forms.Button();
            this.btn_重置 = new System.Windows.Forms.Button();
            this.gp.SuspendLayout();
            this.SuspendLayout();
            // 
            // gp
            // 
            this.gp.Controls.Add(this.btn_重置);
            this.gp.Controls.Add(this.btn_新建);
            this.gp.Controls.Add(this.cbb_下属工厂);
            this.gp.Controls.Add(this.cbb_公司地址);
            this.gp.Controls.Add(this.label6);
            this.gp.Controls.Add(this.label5);
            this.gp.Controls.Add(this.cbb_公司性质);
            this.gp.Controls.Add(this.label4);
            this.gp.Controls.Add(this.cbb_公司级别);
            this.gp.Controls.Add(this.label3);
            this.gp.Controls.Add(this.cbb_公司名称);
            this.gp.Controls.Add(this.label2);
            this.gp.Controls.Add(this.cbb_公司编码);
            this.gp.Controls.Add(this.label1);
            this.gp.Location = new System.Drawing.Point(6, 4);
            this.gp.Name = "gp";
            this.gp.Size = new System.Drawing.Size(1146, 430);
            this.gp.TabIndex = 0;
            this.gp.TabStop = false;
            this.gp.Text = "新建公司数据";
            
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(27, 54);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(64, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "公司编码";
            // 
            // cbb_公司编码
            // 
            this.cbb_公司编码.FormattingEnabled = true;
            this.cbb_公司编码.Location = new System.Drawing.Point(136, 51);
            this.cbb_公司编码.Name = "cbb_公司编码";
            this.cbb_公司编码.Size = new System.Drawing.Size(121, 24);
            this.cbb_公司编码.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(27, 133);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "公司名称";
            // 
            // cbb_公司名称
            // 
            this.cbb_公司名称.FormattingEnabled = true;
            this.cbb_公司名称.Location = new System.Drawing.Point(136, 133);
            this.cbb_公司名称.Name = "cbb_公司名称";
            this.cbb_公司名称.Size = new System.Drawing.Size(121, 24);
            this.cbb_公司名称.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(27, 207);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 17);
            this.label3.TabIndex = 4;
            this.label3.Text = "公司级别";
            // 
            // cbb_公司级别
            // 
            this.cbb_公司级别.FormattingEnabled = true;
            this.cbb_公司级别.Location = new System.Drawing.Point(136, 204);
            this.cbb_公司级别.Name = "cbb_公司级别";
            this.cbb_公司级别.Size = new System.Drawing.Size(121, 24);
            this.cbb_公司级别.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(459, 54);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(64, 17);
            this.label4.TabIndex = 6;
            this.label4.Text = "公司性质";
            // 
            // cbb_公司性质
            // 
            this.cbb_公司性质.FormattingEnabled = true;
            this.cbb_公司性质.Location = new System.Drawing.Point(586, 51);
            this.cbb_公司性质.Name = "cbb_公司性质";
            this.cbb_公司性质.Size = new System.Drawing.Size(121, 24);
            this.cbb_公司性质.TabIndex = 7;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(459, 133);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(64, 17);
            this.label5.TabIndex = 8;
            this.label5.Text = "公司地址";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(459, 211);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(64, 17);
            this.label6.TabIndex = 9;
            this.label6.Text = "下属工厂";
            // 
            // cbb_公司地址
            // 
            this.cbb_公司地址.FormattingEnabled = true;
            this.cbb_公司地址.Location = new System.Drawing.Point(586, 133);
            this.cbb_公司地址.Name = "cbb_公司地址";
            this.cbb_公司地址.Size = new System.Drawing.Size(121, 24);
            this.cbb_公司地址.TabIndex = 10;
            // 
            // cbb_下属工厂
            // 
            this.cbb_下属工厂.FormattingEnabled = true;
            this.cbb_下属工厂.Location = new System.Drawing.Point(586, 208);
            this.cbb_下属工厂.Name = "cbb_下属工厂";
            this.cbb_下属工厂.Size = new System.Drawing.Size(121, 24);
            this.cbb_下属工厂.TabIndex = 11;
            // 
            // btn_新建
            // 
            this.btn_新建.Location = new System.Drawing.Point(605, 386);
            this.btn_新建.Name = "btn_新建";
            this.btn_新建.Size = new System.Drawing.Size(63, 38);
            this.btn_新建.TabIndex = 12;
            this.btn_新建.Text = "新建";
            this.btn_新建.UseVisualStyleBackColor = true;
            this.btn_新建.Click += new System.EventHandler(this.btn_新建_Click);
            // 
            // btn_重置
            // 
            this.btn_重置.Location = new System.Drawing.Point(703, 386);
            this.btn_重置.Name = "btn_重置";
            this.btn_重置.Size = new System.Drawing.Size(58, 38);
            this.btn_重置.TabIndex = 13;
            this.btn_重置.Text = "重置";
            this.btn_重置.UseVisualStyleBackColor = true;
            this.btn_重置.Click += new System.EventHandler(this.btn_重置_Click);
            // 
            // OGCPES
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1148, 434);
            this.Controls.Add(this.gp);
            this.Name = "OGCPES";
            this.Text = "OGCPES";
            this.gp.ResumeLayout(false);
            this.gp.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gp;
        private System.Windows.Forms.ComboBox cbb_公司级别;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cbb_公司名称;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cbb_公司编码;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbb_下属工厂;
        private System.Windows.Forms.ComboBox cbb_公司地址;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cbb_公司性质;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btn_重置;
        private System.Windows.Forms.Button btn_新建;
    }
}