﻿namespace MMClient.MD.NewMMarketPrice
{
    internal class MarketPriceModel
    {
        //采购组织
        private string purname;
        //物料组
        private string mtGroupname;
        //物料编号
        private string mtId;
        //物料名称
        private string mtname;
        //记录类型
        private string busType;
        //价格数
        private string priceNum;
        //价格单位
        private string priceUnit;
        //计量单位
        private string countUnit;
        //开始时间
        private string startTime;
        //结束时间
        private string endTime;
        



        public string Purname { get => purname; set => purname = value; }
        public string MtGroupname { get => mtGroupname; set => mtGroupname = value; }
        public string MtId { get => mtId; set => mtId = value; }
        public string Mtname { get => mtname; set => mtname = value; }
        public string BusType { get => busType; set => busType = value; }
        public string PriceNum { get => priceNum; set => priceNum = value; }
        public string PriceUnit { get => priceUnit; set => priceUnit = value; }
        public string CountUnit { get => countUnit; set => countUnit = value; }
        public string StartTime { get => startTime; set => startTime = value; }
        public string EndTime { get => endTime; set => endTime = value; }
    }
}