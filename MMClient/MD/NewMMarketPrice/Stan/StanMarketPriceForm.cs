﻿using Lib.Bll.ServiceBll;
using Lib.Common.CommonUtils;
using Lib.Common.MMCException.Bll;
using Lib.Common.MMCException.IDAL;
using Lib.SqlServerDAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MMClient.MD.NewMMarketPrice
{
    public partial class StanMarketPriceForm : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        private ServiceBill sll = new ServiceBill();
        private StanAddMarketPriceForm addMarketPriceForm = null;
        private ModifyMarketPriceForm modifyMarketPriceForm = null;
        private imporMarketPriceApplicationForm imporMarketPriceForm = null;
        private string recordtype;
        private string pageCondition = "";
        public StanMarketPriceForm()
        {
            InitializeComponent();
            //初始化采购组织下拉列表，连带初始化采购组下拉列表
            Init();
            
            //this.stanMarketPriceTbl.DataSource = null;
            //FillMarketPriceTbl();
        }

        private void FillMarketPriceTbl(string purGroupName, string mtGroupName, string busType)
        {
            //首先置空
            this.stanMarketPriceTbl.DataSource = null;
            DataTable dt = sll.GetMarketPrice(purGroupName, mtGroupName, busType);
            this.stanMarketPriceTbl.AutoGenerateColumns = false;
            if(dt != null && dt.Rows.Count > 0)
            {
                this.stanMarketPriceTbl.DataSource = dt;
            }
            
        }

        private void FillMarketPriceTbl()
        {
            //首先置空
            this.stanMarketPriceTbl.DataSource = null;
            //查询相关数据并填充表格
            string purGroupName = this.PurGroupName.Text.ToString();
            string mtGroupName = this.MtGroupName.Text.ToString();
            string busType = this.BusType.Text.ToString();
            
            DataTable dt = sll.GetMarketPrice(purGroupName, mtGroupName, busType);
            this.stanMarketPriceTbl.AutoGenerateColumns = false;
            if (dt != null && dt.Rows.Count > 0)
            {
                this.stanMarketPriceTbl.DataSource = dt;
            }
        }

        

        /// <summary>
        /// 初始化下来列表的值
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Init()
        {
            //填充记录类型下路列表框
            string sql = "select recodeType from RecodeType";
            DataTable RecordeTypeDt = DBHelper.ExecuteQueryDT(sql);
            FormUtils.FillCombox(this.BusType, RecordeTypeDt);

            string PurGroupNameQuery = null;
            string MtGroupNameQuery = null;
            string BusTypeQuey = null;
            sql = "select DISTINCT Porg_Name as name from Porg_MtGroup_Relationship";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            if(dt != null && dt.Rows.Count > 0)
            {
                PurGroupNameQuery = dt.Rows[0][0].ToString();
                //填充数据到采购组织下拉列表框
                PurGroupName.DataSource = dt;
                this.PurGroupName.DisplayMember = "name";
                this.PurGroupName.ValueMember = "name";

                //填充采购组下拉列表框
                sql = "select DISTINCT MtGroup_Name as name from Porg_MtGroup_Relationship where Porg_Name='" + dt.Rows[0][0].ToString() + "'";
                dt = DBHelper.ExecuteQueryDT(sql);
                if (dt != null && dt.Rows.Count > 0)
                {
                    MtGroupNameQuery = dt.Rows[0][0].ToString();
                    this.MtGroupName.DataSource = dt;
                    this.MtGroupName.DisplayMember = "name";
                    this.MtGroupName.ValueMember = "name";
                }
            }

            BusTypeQuey = this.BusType.Text.ToString();
           
            FillMarketPriceTbl(PurGroupNameQuery, MtGroupNameQuery, BusTypeQuey);
            
        }

        //添加市场价格记录按钮出发函数
        private void button2_Click(object sender, EventArgs e)
        {
            this.recordtype = this.BusType.Text.ToString();
            if (this.addMarketPriceForm == null || this.addMarketPriceForm.IsDisposed)
            {
                 this.addMarketPriceForm = new StanAddMarketPriceForm(recordtype);
                 
            }
            addMarketPriceForm.Show();
            
        }
        /// <summary>
        /// 采购组织
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PurGroupName_SelectedIndexChanged(object sender, EventArgs e)
        {
            string sql = "select DISTINCT MtGroup_Name as name from Porg_MtGroup_Relationship where Porg_Name='"+this.PurGroupName.Text.ToString()+"'";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            this.MtGroupName.DataSource = dt;
            this.MtGroupName.DisplayMember = "name";
            this.MtGroupName.ValueMember = "name";
            
            
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private int getCurrentSelectedRowIndex()
        {
            if (this.stanMarketPriceTbl.Rows.Count <= 0)
            {
                MessageUtil.ShowWarning("用户表为空，无法执行操作");
                return -1;
            }

            if (this.stanMarketPriceTbl.CurrentRow.Index < 0)
            {
                MessageUtil.ShowWarning("请选择一行记录，然后进行操作！");
                return -1;
            }

            return this.stanMarketPriceTbl.CurrentRow.Index;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MarketPriceTbl_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                int currentIndex = getCurrentSelectedRowIndex();
                DataGridViewRow row = this.stanMarketPriceTbl.Rows[currentIndex];
                int MarketPriceRecordID = Convert.ToInt32(row.Cells["id"].Value);
                String Purname = Convert.ToString(row.Cells["采购组织"].Value);
                String MtGroupname = Convert.ToString(row.Cells["物料组"].Value);
                String Mtname = Convert.ToString(row.Cells["物料名称"].Value);
                String BusType = Convert.ToString(row.Cells["记录类型"].Value);
                String PriceNum = Convert.ToString(row.Cells["价格数"].Value);
                String PriceUnit = Convert.ToString(row.Cells["价格单位"].Value);
                String CountUnit = Convert.ToString(row.Cells["计量单位"].Value);
                String StartTime = Convert.ToString(row.Cells["开始时间"].Value);
                String EndTime = Convert.ToString(row.Cells["结束时间"].Value);
                if (this.stanMarketPriceTbl.Columns[e.ColumnIndex].Name == "修改")
                {
                    //修改窗体
                    if (this.modifyMarketPriceForm == null || this.modifyMarketPriceForm.IsDisposed)
                    {

                        this.modifyMarketPriceForm = new ModifyMarketPriceForm(MarketPriceRecordID ,Purname, MtGroupname, Mtname, BusType, PriceUnit, PriceNum, CountUnit, StartTime, EndTime);
                        modifyMarketPriceForm.Show();
                    }
                    this.modifyMarketPriceForm = null;
                }

                if (this.stanMarketPriceTbl.Columns[e.ColumnIndex].Name == "删除")
                {
                    try
                    {
                        
                        string sql = "delete FROM New_MarketPrice  WHERE MarketPriceRecordID = '" + MarketPriceRecordID + "'";
                       
                        DBHelper.ExecuteNonQuery(sql);
                       
                        MessageBox.Show("删除成功");
                    }
                    catch
                    {
                        MessageBox.Show("删除失败");
                    }
                    
                    FillMarketPriceTbl();
                }
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (imporMarketPriceForm == null || imporMarketPriceForm.IsDisposed)
            {
                imporMarketPriceForm = new imporMarketPriceApplicationForm();
            }
            imporMarketPriceForm.Show();
            
        }

        private void pageTool_OnPageChanged(object sender, EventArgs e)
        {
            LoadData("");
        }
        private void LoadData(string condition)
        {
            try
            {
                stanMarketPriceTbl.DataSource = FindCountryInforByPage(this.pageTool.PageSize, this.pageTool.PageIndex, condition);
                FillMarketPriceTbl();
            }
            catch (BllException ex)
            {
                MessageUtil.ShowError(ex.Message);
            }
        }

        private object FindCountryInforByPage(int pageSize, int pageIndex, string condition)
        {
            try
            {
                if (!string.IsNullOrEmpty(condition))
                {
                    condition += "  and  ";
                }
                string sql = @"SELECT top " + pageSize + "   PurGroupName as 采购组织 ,MtGroupName as 物料组,MtName as 物料名称,BusType as 记录类型 ,PriceNum as 价格数, PriceUnit as 价格单位 from New_MarketPrice  where " + condition + " MarketPriceRecordID not in(select top " + pageSize * (pageIndex - 1) + " MarketPriceRecordID from New_MarketPrice ORDER BY MarketPriceRecordID ASC) GROUP BY MarketPriceRecordID,PurGroupName,MtGroupName,BusType,MtName,PriceNum,PriceUnit";
                return DBHelper.ExecuteQueryDT(sql);
            }
            catch (DBException ex)
            {
                throw new BllException("当前无数据", ex);
            }
        }
      
        private void button1_Click(object sender, EventArgs e)
        {
            string condition = "";
            if (!string.IsNullOrEmpty(this.PurGroupName.Text.ToString()))
            {
                condition += " PurGroupName = '" + this.PurGroupName.Text.ToString() + "'  ";
            }
            if (!string.IsNullOrEmpty(this.MtGroupName.Text.ToString()))
            {
                condition += " and MtGroupName = '" + this.MtGroupName.Text.ToString() + "' ";
            }
            if (!string.IsNullOrEmpty(this.BusType.Text.ToString()))
            {
                condition += " and BusType = '" + this.BusType.Text.ToString() + "' ";
            }
            pageCondition = condition;
            LoadData(condition);
            pageTool_Load(sender, e);

        }

        private void txtBusType_SelectedIndexChanged(object sender, EventArgs e)
        {
            
           
        }

        private void cbMtGroups_SelectedIndexChanged(object sender, EventArgs e)
        {
            
            
        }

        private void pageTool_Load(object sender, EventArgs e)
        {

            if (!string.IsNullOrEmpty(pageCondition))
            {
                pageCondition = " where " + pageCondition;
            }
            string sql = "SELECT count(MarketPriceRecordID)  FROM New_MarketPrice " + pageCondition + " GROUP BY MarketPriceRecordID ";
            try
            {
                DataTable dt = DBHelper.ExecuteQueryDT(sql);
                pageTool.DrawControl(dt.Rows.Count);
                //事件改变调用函数
                pageTool.OnPageChanged += pageTool_OnPageChanged;
            }
            catch (Lib.Common.MMCException.Bll.BllException ex)
            {
                MessageBox.Show("数据库异常，请稍后重试");
                return;
            }
        }
    }
}
