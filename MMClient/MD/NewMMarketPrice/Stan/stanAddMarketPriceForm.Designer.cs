﻿namespace MMClient.MD.NewMMarketPrice
{
    partial class StanAddMarketPriceForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.savebtn = new System.Windows.Forms.Button();
            this.TimeEndTP = new System.Windows.Forms.DateTimePicker();
            this.label6 = new System.Windows.Forms.Label();
            this.TimeStartTP = new System.Windows.Forms.DateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.txtPrice = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.comCurr = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtCountUnit = new System.Windows.Forms.TextBox();
            this.txtRecordType = new System.Windows.Forms.ComboBox();
            this.cbMtNames = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.MtGroupNames = new System.Windows.Forms.ComboBox();
            this.PurGroupNames = new System.Windows.Forms.ComboBox();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(51, 283);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(29, 12);
            this.label8.TabIndex = 15;
            this.label8.Text = "单位";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(28, 155);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 12);
            this.label7.TabIndex = 13;
            this.label7.Text = "记录类型";
            // 
            // savebtn
            // 
            this.savebtn.Location = new System.Drawing.Point(458, 366);
            this.savebtn.Name = "savebtn";
            this.savebtn.Size = new System.Drawing.Size(75, 23);
            this.savebtn.TabIndex = 12;
            this.savebtn.Text = "保  存";
            this.savebtn.UseVisualStyleBackColor = true;
            this.savebtn.Click += new System.EventHandler(this.SaveClick);
            // 
            // TimeEndTP
            // 
            this.TimeEndTP.Location = new System.Drawing.Point(332, 325);
            this.TimeEndTP.Name = "TimeEndTP";
            this.TimeEndTP.Size = new System.Drawing.Size(150, 21);
            this.TimeEndTP.TabIndex = 11;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(276, 331);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(17, 12);
            this.label6.TabIndex = 10;
            this.label6.Text = "至";
            // 
            // TimeStartTP
            // 
            this.TimeStartTP.Location = new System.Drawing.Point(86, 325);
            this.TimeStartTP.Name = "TimeStartTP";
            this.TimeStartTP.Size = new System.Drawing.Size(152, 21);
            this.TimeStartTP.TabIndex = 9;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(27, 331);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 12);
            this.label5.TabIndex = 8;
            this.label5.Text = "有效时间";
            // 
            // txtPrice
            // 
            this.txtPrice.Location = new System.Drawing.Point(87, 239);
            this.txtPrice.Name = "txtPrice";
            this.txtPrice.Size = new System.Drawing.Size(121, 21);
            this.txtPrice.TabIndex = 7;
            this.txtPrice.TextChanged += new System.EventHandler(this.txtPrice_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(40, 242);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 12);
            this.label4.TabIndex = 6;
            this.label4.Text = "市场价";
            // 
            // comCurr
            // 
            this.comCurr.FormattingEnabled = true;
            this.comCurr.Location = new System.Drawing.Point(87, 195);
            this.comCurr.Name = "comCurr";
            this.comCurr.Size = new System.Drawing.Size(121, 20);
            this.comCurr.TabIndex = 5;
            this.comCurr.SelectedIndexChanged += new System.EventHandler(this.comCurr_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(51, 198);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 12);
            this.label3.TabIndex = 4;
            this.label3.Text = "货币";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(39, 73);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 12);
            this.label2.TabIndex = 1;
            this.label2.Text = "物料组";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(27, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "采购组织";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.txtCountUnit);
            this.panel1.Controls.Add(this.txtRecordType);
            this.panel1.Controls.Add(this.cbMtNames);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.MtGroupNames);
            this.panel1.Controls.Add(this.PurGroupNames);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.savebtn);
            this.panel1.Controls.Add(this.TimeEndTP);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.TimeStartTP);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.txtPrice);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.comCurr);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(4, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(603, 406);
            this.panel1.TabIndex = 2;
            // 
            // txtCountUnit
            // 
            this.txtCountUnit.Location = new System.Drawing.Point(87, 280);
            this.txtCountUnit.Name = "txtCountUnit";
            this.txtCountUnit.ReadOnly = true;
            this.txtCountUnit.Size = new System.Drawing.Size(121, 21);
            this.txtCountUnit.TabIndex = 26;
            // 
            // txtRecordType
            // 
            this.txtRecordType.FormattingEnabled = true;
            this.txtRecordType.Items.AddRange(new object[] {
            "标准信息记录",
            "委托加工记录",
            "寄售信息记录",
            "管道信息记录"});
            this.txtRecordType.Location = new System.Drawing.Point(87, 155);
            this.txtRecordType.Name = "txtRecordType";
            this.txtRecordType.Size = new System.Drawing.Size(121, 20);
            this.txtRecordType.TabIndex = 25;
            this.txtRecordType.Text = "标准信息记录";
            this.txtRecordType.SelectedIndexChanged += new System.EventHandler(this.txtRecordType_SelectedIndexChanged);
            // 
            // cbMtNames
            // 
            this.cbMtNames.FormattingEnabled = true;
            this.cbMtNames.Location = new System.Drawing.Point(87, 115);
            this.cbMtNames.Name = "cbMtNames";
            this.cbMtNames.Size = new System.Drawing.Size(121, 20);
            this.cbMtNames.TabIndex = 24;
            this.cbMtNames.SelectedIndexChanged += new System.EventHandler(this.cbMtNames_SelectedIndexChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(51, 115);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(29, 12);
            this.label10.TabIndex = 23;
            this.label10.Text = "物料";
            // 
            // MtGroupNames
            // 
            this.MtGroupNames.FormattingEnabled = true;
            this.MtGroupNames.Location = new System.Drawing.Point(87, 73);
            this.MtGroupNames.Name = "MtGroupNames";
            this.MtGroupNames.Size = new System.Drawing.Size(121, 20);
            this.MtGroupNames.TabIndex = 22;
            this.MtGroupNames.SelectedIndexChanged += new System.EventHandler(this.MtGroupNames_SelectedIndexChanged_1);
            // 
            // PurGroupNames
            // 
            this.PurGroupNames.FormattingEnabled = true;
            this.PurGroupNames.Location = new System.Drawing.Point(86, 30);
            this.PurGroupNames.Name = "PurGroupNames";
            this.PurGroupNames.Size = new System.Drawing.Size(121, 20);
            this.PurGroupNames.TabIndex = 21;
            this.PurGroupNames.SelectedIndexChanged += new System.EventHandler(this.MtGroupNames_SelectedIndexChanged);
            // 
            // StanAddMarketPriceForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(606, 420);
            this.Controls.Add(this.panel1);
            this.Name = "StanAddMarketPriceForm";
            this.Text = "添加信息";
            this.Load += new System.EventHandler(this.PurGroupName_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button savebtn;
        private System.Windows.Forms.DateTimePicker TimeEndTP;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DateTimePicker TimeStartTP;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtPrice;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox comCurr;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ComboBox PurGroupNames;
        private System.Windows.Forms.ComboBox MtGroupNames;
        private System.Windows.Forms.ComboBox cbMtNames;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox txtRecordType;
        private System.Windows.Forms.TextBox txtCountUnit;
    }
}