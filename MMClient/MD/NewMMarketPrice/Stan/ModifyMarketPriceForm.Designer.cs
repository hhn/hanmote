﻿namespace MMClient.MD.NewMMarketPrice
{
    partial class ModifyMarketPriceForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label9 = new System.Windows.Forms.Label();
            this.txtCountUnit = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.savebtn = new System.Windows.Forms.Button();
            this.TimeEndTP = new System.Windows.Forms.DateTimePicker();
            this.label6 = new System.Windows.Forms.Label();
            this.TimeStartTP = new System.Windows.Forms.DateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.txtPrice = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.comCurr = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtMtName = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtMtGroupName = new System.Windows.Forms.TextBox();
            this.txtPurName = new System.Windows.Forms.TextBox();
            this.txtBusType = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(263, 278);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(203, 12);
            this.label9.TabIndex = 17;
            this.label9.Text = "单位可以为：元/件、元/桶、元/公斤";
            // 
            // txtCountUnit
            // 
            this.txtCountUnit.Location = new System.Drawing.Point(85, 275);
            this.txtCountUnit.Name = "txtCountUnit";
            this.txtCountUnit.ReadOnly = true;
            this.txtCountUnit.Size = new System.Drawing.Size(121, 21);
            this.txtCountUnit.TabIndex = 16;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(50, 275);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(29, 12);
            this.label8.TabIndex = 15;
            this.label8.Text = "单位";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(27, 155);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 12);
            this.label7.TabIndex = 13;
            this.label7.Text = "记录类型";
            // 
            // savebtn
            // 
            this.savebtn.Location = new System.Drawing.Point(457, 358);
            this.savebtn.Name = "savebtn";
            this.savebtn.Size = new System.Drawing.Size(75, 23);
            this.savebtn.TabIndex = 12;
            this.savebtn.Text = "保  存";
            this.savebtn.UseVisualStyleBackColor = true;
            this.savebtn.Click += new System.EventHandler(this.savebtn_Click);
            // 
            // TimeEndTP
            // 
            this.TimeEndTP.Location = new System.Drawing.Point(331, 317);
            this.TimeEndTP.Name = "TimeEndTP";
            this.TimeEndTP.Size = new System.Drawing.Size(150, 21);
            this.TimeEndTP.TabIndex = 11;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(275, 323);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(17, 12);
            this.label6.TabIndex = 10;
            this.label6.Text = "至";
            // 
            // TimeStartTP
            // 
            this.TimeStartTP.Location = new System.Drawing.Point(85, 317);
            this.TimeStartTP.Name = "TimeStartTP";
            this.TimeStartTP.Size = new System.Drawing.Size(152, 21);
            this.TimeStartTP.TabIndex = 9;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(26, 323);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 12);
            this.label5.TabIndex = 8;
            this.label5.Text = "有效时间";
            // 
            // txtPrice
            // 
            this.txtPrice.Location = new System.Drawing.Point(86, 231);
            this.txtPrice.Name = "txtPrice";
            this.txtPrice.Size = new System.Drawing.Size(121, 21);
            this.txtPrice.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(51, 231);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(29, 12);
            this.label4.TabIndex = 6;
            this.label4.Text = "净价";
            // 
            // comCurr
            // 
            this.comCurr.FormattingEnabled = true;
            this.comCurr.Location = new System.Drawing.Point(86, 187);
            this.comCurr.Name = "comCurr";
            this.comCurr.Size = new System.Drawing.Size(121, 20);
            this.comCurr.TabIndex = 5;
            this.comCurr.SelectedIndexChanged += new System.EventHandler(this.comCurr_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(50, 190);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 12);
            this.label3.TabIndex = 4;
            this.label3.Text = "货币";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.txtMtName);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.txtMtGroupName);
            this.panel1.Controls.Add(this.txtPurName);
            this.panel1.Controls.Add(this.txtBusType);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.txtCountUnit);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.savebtn);
            this.panel1.Controls.Add(this.TimeEndTP);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.TimeStartTP);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.txtPrice);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.comCurr);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(1, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(584, 443);
            this.panel1.TabIndex = 1;
            // 
            // txtMtName
            // 
            this.txtMtName.Location = new System.Drawing.Point(84, 112);
            this.txtMtName.Name = "txtMtName";
            this.txtMtName.ReadOnly = true;
            this.txtMtName.Size = new System.Drawing.Size(121, 21);
            this.txtMtName.TabIndex = 22;
            this.txtMtName.TextChanged += new System.EventHandler(this.txtMtName_TextChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(49, 121);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(29, 12);
            this.label10.TabIndex = 21;
            this.label10.Text = "物料";
            // 
            // txtMtGroupName
            // 
            this.txtMtGroupName.Location = new System.Drawing.Point(85, 70);
            this.txtMtGroupName.Name = "txtMtGroupName";
            this.txtMtGroupName.ReadOnly = true;
            this.txtMtGroupName.Size = new System.Drawing.Size(121, 21);
            this.txtMtGroupName.TabIndex = 20;
            // 
            // txtPurName
            // 
            this.txtPurName.Location = new System.Drawing.Point(86, 30);
            this.txtPurName.Name = "txtPurName";
            this.txtPurName.ReadOnly = true;
            this.txtPurName.Size = new System.Drawing.Size(121, 21);
            this.txtPurName.TabIndex = 19;
            // 
            // txtBusType
            // 
            this.txtBusType.Location = new System.Drawing.Point(86, 152);
            this.txtBusType.Name = "txtBusType";
            this.txtBusType.ReadOnly = true;
            this.txtBusType.Size = new System.Drawing.Size(121, 21);
            this.txtBusType.TabIndex = 18;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(39, 73);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 12);
            this.label2.TabIndex = 1;
            this.label2.Text = "物料组";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(27, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "采购组织";
            // 
            // ModifyMarketPriceForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(586, 448);
            this.Controls.Add(this.panel1);
            this.Name = "ModifyMarketPriceForm";
            this.Text = "修改信息";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtCountUnit;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button savebtn;
        private System.Windows.Forms.DateTimePicker TimeEndTP;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DateTimePicker TimeStartTP;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtPrice;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox comCurr;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox txtBusType;
        private System.Windows.Forms.TextBox txtMtGroupName;
        private System.Windows.Forms.TextBox txtPurName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtMtName;
        private System.Windows.Forms.Label label10;
    }
}