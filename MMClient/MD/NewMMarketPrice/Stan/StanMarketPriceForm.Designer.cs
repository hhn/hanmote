﻿namespace MMClient.MD.NewMMarketPrice
{
    partial class StanMarketPriceForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel3 = new System.Windows.Forms.Panel();
            this.BusType = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.MtGroupName = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.flush = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.PurGroupName = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.stanMarketPriceTbl = new System.Windows.Forms.DataGridView();
            this.id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.采购组织 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.物料组 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.物料名称 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.记录类型 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.价格数 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.价格单位 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.计量单位 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.开始时间 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.结束时间 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.修改时间 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.修改 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.删除 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.pageTool = new pager.pagetool.pageNext();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.stanMarketPriceTbl)).BeginInit();
            this.SuspendLayout();
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.BusType);
            this.panel3.Controls.Add(this.label7);
            this.panel3.Controls.Add(this.MtGroupName);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Controls.Add(this.flush);
            this.panel3.Controls.Add(this.button3);
            this.panel3.Controls.Add(this.button2);
            this.panel3.Controls.Add(this.PurGroupName);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Location = new System.Drawing.Point(1, 2);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(990, 113);
            this.panel3.TabIndex = 1;
            // 
            // BusType
            // 
            this.BusType.FormattingEnabled = true;
            this.BusType.Location = new System.Drawing.Point(107, 69);
            this.BusType.Name = "BusType";
            this.BusType.Size = new System.Drawing.Size(145, 20);
            this.BusType.TabIndex = 27;
            this.BusType.SelectedIndexChanged += new System.EventHandler(this.txtBusType_SelectedIndexChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(31, 72);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 12);
            this.label7.TabIndex = 26;
            this.label7.Text = "记录类型";
            // 
            // MtGroupName
            // 
            this.MtGroupName.FormattingEnabled = true;
            this.MtGroupName.Location = new System.Drawing.Point(321, 19);
            this.MtGroupName.Name = "MtGroupName";
            this.MtGroupName.Size = new System.Drawing.Size(121, 20);
            this.MtGroupName.TabIndex = 8;
            this.MtGroupName.SelectedIndexChanged += new System.EventHandler(this.cbMtGroups_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(274, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 12);
            this.label1.TabIndex = 7;
            this.label1.Text = "物料组";
            // 
            // flush
            // 
            this.flush.Location = new System.Drawing.Point(510, 17);
            this.flush.Name = "flush";
            this.flush.Size = new System.Drawing.Size(75, 23);
            this.flush.TabIndex = 6;
            this.flush.Text = "查询";
            this.flush.UseVisualStyleBackColor = true;
            this.flush.Click += new System.EventHandler(this.button1_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(718, 67);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(155, 23);
            this.button3.TabIndex = 5;
            this.button3.Text = "导入市场价格记录";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(718, 17);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(155, 23);
            this.button2.TabIndex = 4;
            this.button2.Text = "添加市场价格记录";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // PurGroupName
            // 
            this.PurGroupName.FormattingEnabled = true;
            this.PurGroupName.Location = new System.Drawing.Point(107, 19);
            this.PurGroupName.Name = "PurGroupName";
            this.PurGroupName.Size = new System.Drawing.Size(145, 20);
            this.PurGroupName.TabIndex = 1;
            this.PurGroupName.SelectedIndexChanged += new System.EventHandler(this.PurGroupName_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(31, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 0;
            this.label2.Text = "采购组织";
            // 
            // stanMarketPriceTbl
            // 
            this.stanMarketPriceTbl.AllowUserToAddRows = false;
            this.stanMarketPriceTbl.BackgroundColor = System.Drawing.SystemColors.Window;
            this.stanMarketPriceTbl.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.stanMarketPriceTbl.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.stanMarketPriceTbl.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.stanMarketPriceTbl.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id,
            this.采购组织,
            this.物料组,
            this.物料名称,
            this.记录类型,
            this.价格数,
            this.价格单位,
            this.计量单位,
            this.开始时间,
            this.结束时间,
            this.修改时间,
            this.修改,
            this.删除});
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.stanMarketPriceTbl.DefaultCellStyle = dataGridViewCellStyle4;
            this.stanMarketPriceTbl.GridColor = System.Drawing.SystemColors.ControlLight;
            this.stanMarketPriceTbl.Location = new System.Drawing.Point(1, 121);
            this.stanMarketPriceTbl.Name = "stanMarketPriceTbl";
            this.stanMarketPriceTbl.RowTemplate.Height = 23;
            this.stanMarketPriceTbl.Size = new System.Drawing.Size(990, 312);
            this.stanMarketPriceTbl.TabIndex = 6;
            this.stanMarketPriceTbl.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.MarketPriceTbl_CellContentClick);
            // 
            // id
            // 
            this.id.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.id.DataPropertyName = "MarketPriceRecordID";
            this.id.HeaderText = "id";
            this.id.Name = "id";
            this.id.Visible = false;
            // 
            // 采购组织
            // 
            this.采购组织.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.采购组织.DataPropertyName = "PurGroupName";
            this.采购组织.HeaderText = "采购组织";
            this.采购组织.Name = "采购组织";
            this.采购组织.Width = 80;
            // 
            // 物料组
            // 
            this.物料组.DataPropertyName = "MtGroupName";
            this.物料组.HeaderText = "物料组";
            this.物料组.Name = "物料组";
            this.物料组.Width = 92;
            // 
            // 物料名称
            // 
            this.物料名称.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.物料名称.DataPropertyName = "MtName";
            this.物料名称.HeaderText = "物料名称";
            this.物料名称.Name = "物料名称";
            this.物料名称.Width = 80;
            // 
            // 记录类型
            // 
            this.记录类型.DataPropertyName = "BusType";
            this.记录类型.HeaderText = "记录类型";
            this.记录类型.Name = "记录类型";
            this.记录类型.Width = 93;
            // 
            // 价格数
            // 
            this.价格数.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.价格数.DataPropertyName = "PriceNum";
            this.价格数.HeaderText = "价格数";
            this.价格数.Name = "价格数";
            this.价格数.Width = 70;
            // 
            // 价格单位
            // 
            this.价格单位.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.价格单位.DataPropertyName = "PriceUnit";
            this.价格单位.HeaderText = "价格单位";
            this.价格单位.Name = "价格单位";
            this.价格单位.Width = 80;
            // 
            // 计量单位
            // 
            this.计量单位.DataPropertyName = "countUnit";
            this.计量单位.HeaderText = "计量单位";
            this.计量单位.Name = "计量单位";
            this.计量单位.Width = 92;
            // 
            // 开始时间
            // 
            this.开始时间.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.开始时间.DataPropertyName = "StartTime";
            this.开始时间.HeaderText = "开始时间";
            this.开始时间.Name = "开始时间";
            this.开始时间.Width = 80;
            // 
            // 结束时间
            // 
            this.结束时间.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.结束时间.DataPropertyName = "EndTime";
            this.结束时间.HeaderText = "结束时间";
            this.结束时间.Name = "结束时间";
            this.结束时间.Width = 80;
            // 
            // 修改时间
            // 
            this.修改时间.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.修改时间.DataPropertyName = "createTime";
            this.修改时间.HeaderText = "修改时间";
            this.修改时间.Name = "修改时间";
            this.修改时间.Width = 80;
            // 
            // 修改
            // 
            this.修改.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.NullValue = "修改";
            this.修改.DefaultCellStyle = dataGridViewCellStyle2;
            this.修改.HeaderText = "修改";
            this.修改.Name = "修改";
            this.修改.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.修改.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.修改.Text = "修改";
            this.修改.Width = 60;
            // 
            // 删除
            // 
            this.删除.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.NullValue = "删除";
            this.删除.DefaultCellStyle = dataGridViewCellStyle3;
            this.删除.HeaderText = "删除";
            this.删除.Name = "删除";
            this.删除.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.删除.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.删除.Text = "删除";
            this.删除.Width = 60;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // pageTool
            // 
            this.pageTool.Location = new System.Drawing.Point(142, 454);
            this.pageTool.Name = "pageTool";
            this.pageTool.PageIndex = 1;
            this.pageTool.PageSize = 100;
            this.pageTool.RecordCount = 0;
            this.pageTool.Size = new System.Drawing.Size(688, 37);
            this.pageTool.TabIndex = 13;
            this.pageTool.Load += new System.EventHandler(this.pageTool_Load);
            // 
            // StanMarketPriceForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1019, 513);
            this.Controls.Add(this.pageTool);
            this.Controls.Add(this.stanMarketPriceTbl);
            this.Controls.Add(this.panel3);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "StanMarketPriceForm";
            this.Text = "维护市场价格-标准信息记录";
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.stanMarketPriceTbl)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.ComboBox PurGroupName;
        private System.Windows.Forms.Label label2;
        public System.Windows.Forms.DataGridView stanMarketPriceTbl;
        private System.Windows.Forms.Button flush;
        private System.Windows.Forms.ComboBox MtGroupName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox BusType;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private pager.pagetool.pageNext pageTool;
        private System.Windows.Forms.DataGridViewTextBoxColumn id;
        private System.Windows.Forms.DataGridViewTextBoxColumn 采购组织;
        private System.Windows.Forms.DataGridViewTextBoxColumn 物料组;
        private System.Windows.Forms.DataGridViewTextBoxColumn 物料名称;
        private System.Windows.Forms.DataGridViewTextBoxColumn 记录类型;
        private System.Windows.Forms.DataGridViewTextBoxColumn 价格数;
        private System.Windows.Forms.DataGridViewTextBoxColumn 价格单位;
        private System.Windows.Forms.DataGridViewTextBoxColumn 计量单位;
        private System.Windows.Forms.DataGridViewTextBoxColumn 开始时间;
        private System.Windows.Forms.DataGridViewTextBoxColumn 结束时间;
        private System.Windows.Forms.DataGridViewTextBoxColumn 修改时间;
        private System.Windows.Forms.DataGridViewButtonColumn 修改;
        private System.Windows.Forms.DataGridViewButtonColumn 删除;
    }
}