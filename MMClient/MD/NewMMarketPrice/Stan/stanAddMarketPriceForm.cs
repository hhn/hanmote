﻿using Lib.Bll.MDBll.General;
using Lib.SqlServerDAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MMClient.MD.NewMMarketPrice
{
    public partial class StanAddMarketPriceForm : Form
    {
        private GeneralBLL gn = new GeneralBLL();
        private string PurGroupName;
        private string MtGroupName;
        private string PriceUnit;
        private string PriceNum;
        private string countUnit;
        private string StartTime;
        private string EndTime;
        private string BusType;
        private string MtName;
        private string MtId;

        public StanAddMarketPriceForm()
        {
            InitializeComponent();
        }

        public StanAddMarketPriceForm(string recordtype)
        {
            InitializeComponent();
            this.BusType = recordtype;
            this.txtRecordType.Text = recordtype;
            List<string> AllMeasurementList = gn.GetAllMeasurement();
            
        }

        /// <summary>
        /// 初始化下来列表的值
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PurGroupName_Load(object sender, EventArgs e)
        {
            string sql = "select DISTINCT Porg_Name as name from Porg_MtGroup_Relationship";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            PurGroupNames.DataSource = dt;
            this.PurGroupNames.DisplayMember = "name";
            this.PurGroupNames.ValueMember = "name";
            dt = null;
            List<string> list1 = gn.GetAllMeasurement();
            gn.IDInTheList(list1, txtCountUnit.Text);
            

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MtGroupNames_SelectedIndexChanged(object sender, EventArgs e)
        {
            string sql = "select DISTINCT MtGroup_Name as name from Porg_MtGroup_Relationship where Porg_Name='" + this.PurGroupNames.Text.ToString() + "'";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            this.MtGroupNames.DataSource = dt;
            this.MtGroupNames.DisplayMember = "name";
            this.MtGroupNames.ValueMember = "name";
        }

        /// <summary>
        /// 保存市场价格记录按钮调用的函数
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SaveClick(object sender, EventArgs e)
        {
            if(isNotNull()==2)
            {
                try
                {
                    string sql = "INSERT INTO New_MarketPrice([PurGroupName], [MtGroupName], [PriceUnit],[PriceNum],[StartTime],[EndTime],[BusType],[countUnit],[MtName])VALUES('" + PurGroupName + "','" + MtGroupName + "','" + PriceUnit + "','" + PriceNum + "','" + StartTime + "','" + EndTime + "','" + this.txtRecordType.Text.ToString() + "','" + countUnit + "','" + MtName + "')";
                    DBHelper.ExecuteNonQuery(sql);
                    MessageBox.Show("添加成功");

                    this.Close();
                }
                catch (Lib.Common.MMCException.IDAL.DBException ex)
                {
                    MessageBox.Show("添加失败,存储数据时出错！");
                }
            }
            else if(isNotNull()==1)
            {
                MessageBox.Show("信息不完整或该物料组下没有可添加的记录");
            }
            else{
                MessageBox.Show("净价只能是数字,请重新输入！！！");
                this.txtPrice.Text = "";
            }
        }

        private int isNotNull()
        {
            PurGroupName = this.PurGroupNames.Text.ToString();
            MtGroupName = this.MtGroupNames.Text.ToString();
            MtName = this.cbMtNames.Text.ToString();
            
            BusType = this.txtRecordType.Text.ToString();
            countUnit = this.txtCountUnit.Text.ToString();
            PriceUnit = this.comCurr.Text.ToString();
            StartTime = this.TimeStartTP.Value.ToString();
            EndTime = this.TimeEndTP.Value.ToString();
            PriceNum = this.txtPrice.Text.ToString();
            try
            {
                int priceInt = int.Parse(PriceNum);
            }
            catch
            {
                return 0;
            }
           
            if (String.IsNullOrEmpty(MtName)||String.IsNullOrEmpty(countUnit)||String.IsNullOrEmpty(PurGroupName) || String.IsNullOrEmpty(MtGroupName) || String.IsNullOrEmpty(PriceUnit) || String.IsNullOrEmpty(countUnit) || String.IsNullOrEmpty(EndTime) || String.IsNullOrEmpty(BusType) || String.IsNullOrEmpty(PriceNum) || String.IsNullOrEmpty(StartTime))
            {
                return 1;
            }
            return 2;
        }

        private void MtGroupNames_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            //
            //DataTable dt = gn.GetAllMaterialNameByMaterialGroup(this.MtGroupNames.Text);
            string sql = "select DISTINCT Material_Name as name from Material where Material_Group=(select Material_Group from Material_Group where Description='" + this.MtGroupNames.Text.ToString() + "')";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);

            if (dt != null && dt.Rows.Count > 0)
            {
                this.cbMtNames.DataSource = dt;
                this.cbMtNames.DisplayMember = "name";
                this.cbMtNames.ValueMember = "name";
            }
        }

        private void txtCountUnit_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void cbMtNames_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.txtCountUnit.Text = "";
            string sql = "select Measurement from Material where Material_Name='" + this.cbMtNames.SelectedValue.ToString() + "'";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            try
            {
                this.txtCountUnit.Text = dt.Rows[0][0].ToString();
            }
            catch
            {
                this.txtCountUnit.Text = "";
            }

            string sql_1 = "select Currency_Name as name from Currency";
            DataTable dt_1 = DBHelper.ExecuteQueryDT(sql_1);

            if (dt_1 != null && dt_1.Rows.Count > 0)
            {
                this.comCurr.DataSource = dt_1;
                this.comCurr.DisplayMember = "name";
                this.comCurr.ValueMember = "name";
            }

        }

        private void comCurr_SelectedIndexChanged(object sender, EventArgs e)
        {
           
        }

        private void txtRecordType_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void txtPrice_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
