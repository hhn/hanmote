﻿using Lib.SqlServerDAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MMClient.MD.NewMMarketPrice
{
    public partial class ModifyMarketPriceForm : Form
    {
        private int MarketPriceRecordID;
        private string purname;
        private string mtgroupname;
        private string mtname;
        private string bustype;
        private string priceunit;
        private string price;
        private string countUnit;
        private string starttime;
        private string endtime;

        public ModifyMarketPriceForm()
        {
            InitializeComponent();
            BaseInfoFill();
        }
        /// <summary>
        /// 初始化构造方法
        /// </summary>
        /// <param name="purname"></param>
        /// <param name="mtname"></param>
        /// <param name="bustype"></param>
        /// <param name="priceunit"></param>
        /// <param name="price"></param>
        /// <param name="countUnit"></param>
        /// <param name="starttime"></param>
        /// <param name="endtime"></param>
        public ModifyMarketPriceForm(int MarketPriceRecordID ,string purname, string mtgroupname, string mtname, string bustype, string priceunit, string price, string countUnit, string starttime, string endtime)
        {
            this.MarketPriceRecordID = MarketPriceRecordID;
            this.purname = purname;
            this.mtgroupname = mtgroupname;
            this.mtname = mtname;
            this.bustype = bustype;
            this.priceunit = priceunit;
            this.price = price;
            this.countUnit = countUnit;
            this.starttime = starttime;
            this.endtime = endtime;
            InitializeComponent();
            BaseInfoFill();
        }
        /// <summary>
        /// 填充基本信息
        /// </summary>
        private void BaseInfoFill()
        {
            this.txtPurName.Text = purname;
            this.txtMtGroupName.Text = mtgroupname;
            this.txtBusType.Text = bustype;
            this.txtCountUnit.Text = countUnit;
            this.comCurr.Text = priceunit;
            this.TimeStartTP.Text = starttime;
            this.TimeStartTP.Text = endtime;
            this.txtPrice.Text = price;
            this.txtMtName.Text = mtname;
        }

        private void savebtn_Click(object sender, EventArgs e)
        {
            string NewCountUnit = this.txtCountUnit.Text.ToString();
            string NewPriceunit = this.comCurr.Text.ToString();
            string NewStarttime = this.TimeStartTP.Value.ToString();
            string NewEndtime = this.TimeStartTP.Value.ToString();
            string NewPrice = this.txtPrice.Text.ToString();

            if (notNull())
            {
                string sql = @"UPDATE New_MarketPrice SET PriceNum = '" + NewPrice + @"',
                                     PriceUnit = '" + NewPriceunit + @"',
                                     StartTime = '" + NewStarttime + @"',
                                     EndTime = '" + NewEndtime + @"',
                                     countUnit = '" + NewCountUnit + @"'
                                    WHERE MarketPriceRecordID = '" + MarketPriceRecordID + "'";


                try
                {
                    DBHelper.ExecuteNonQuery(sql);
                }
                catch
                {
                    MessageBox.Show("更新失败");
                    return;
                }
                MessageBox.Show("更新成功");
                this.Close();
            }
            else
            {
                MessageBox.Show("信息不完整");
                return;
            }

          
        }

        private bool notNull()
        {
            if (String.IsNullOrEmpty(purname) || String.IsNullOrEmpty(mtgroupname) || String.IsNullOrEmpty(priceunit) || String.IsNullOrEmpty(countUnit) || String.IsNullOrEmpty(endtime) || String.IsNullOrEmpty(bustype) || String.IsNullOrEmpty(price) || String.IsNullOrEmpty(starttime))
            {
                return false;
            }
            return true;
        }

        private void txtMtName_TextChanged(object sender, EventArgs e)
        {
            string sql_1 = "select Currency_Name as name from Currency";
            DataTable dt_1 = DBHelper.ExecuteQueryDT(sql_1);

            if (dt_1 != null && dt_1.Rows.Count > 0)
            {
                this.comCurr.DataSource = dt_1;
                this.comCurr.DisplayMember = "name";
                this.comCurr.ValueMember = "name";
            }
        }

        private void comCurr_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
