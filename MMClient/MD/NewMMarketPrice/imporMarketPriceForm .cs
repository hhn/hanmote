﻿using Lib.Common.CommonUtils;
using Lib.SqlServerDAL;
using MMClient.Material_group_positioning.GoalAndAim;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MMClient.MD.NewMMarketPrice
{
    public partial class imporMarketPriceApplicationForm : importExcleData
    {
        private MarketPriceModel marketPriceModel;

        public imporMarketPriceApplicationForm()
        {
            InitializeComponent();
        }

        public override void getModel()
        {
            List<string> list = new List<string> { "采购组织", "物料组", "物料编号","物料名称", "记录类型", "价格数", "价格单位", "计量单位", "开始时间", "结束时间" };
            string fileName = "市场价格信息数据导入模板.xlsx";
            base.produceModel(list, fileName);
        }


        /// <summary>
        ///重写数据
        /// </summary>
        public override void loadData()
        {
            //todo:重写
            int x = 1,m=1;
            string erroInfo = "";
            if (marketPriceModel == null)
            {
                marketPriceModel = new MarketPriceModel();

            }
            DataTable dt = new DataTable();
            foreach (DataRow row in this.excelTbl.Rows)
            {
                marketPriceModel.Purname = row["采购组织"].ToString();
                marketPriceModel.MtGroupname = row["物料组"].ToString();
                marketPriceModel.MtId = row["物料编号"].ToString();
                marketPriceModel.Mtname = row["物料名称"].ToString();
                marketPriceModel.BusType = row["记录类型"].ToString();
                marketPriceModel.PriceNum = row["价格数"].ToString();
                marketPriceModel.PriceUnit = row["价格单位"].ToString();
                marketPriceModel.CountUnit = row["计量单位"].ToString();
                marketPriceModel.StartTime = row["开始时间"].ToString();
                marketPriceModel.EndTime = row["结束时间"].ToString();


                //判断采购组织
                dt = null;
                string cg = "select * from Buyer_Org where Buyer_Org_Name='"+marketPriceModel.Purname+"'";
                dt = DBHelper.ExecuteQueryDT(cg);
                if (marketPriceModel.Purname ==""||dt.Rows.Count==0)
                {
                    MessageBox.Show("采购组织第"+m+"行数据有误");
                    x = 0;
                    break;
                }
                if (marketPriceModel.MtGroupname =="")
                {
                    MessageBox.Show("物料组第" + m + "行数据为空");
                    x = 0;
                    break;
                }
                if (marketPriceModel.MtId == "")
                {
                    MessageBox.Show("物料编号第" + m + "行数据为空");
                    x = 0;
                    break;
                }
                if (marketPriceModel.Mtname =="")
                {
                    MessageBox.Show("物料名称第" + m + "行数据为空");
                    x = 0;
                    break;
                }
                //判断记录类型
                dt = null;
                string lx = "select * from RecodeType where recodeType='"+marketPriceModel.BusType+"'";
                dt = DBHelper.ExecuteQueryDT(lx);
                if (marketPriceModel.BusType ==""||dt.Rows.Count==0)
                {
                    MessageBox.Show("记录类型第" + m + "行数据有误");
                    x = 0;
                    break;
                }
                //判断价格
                double tmp;
                if (marketPriceModel.PriceNum ==""||!double.TryParse(marketPriceModel.PriceNum,out tmp))
                {
                    MessageBox.Show("价格数第" + m + "行数据有误");
                    x = 0;
                    break;
                }

                //判断价格单位
                dt = null;
                string hb = "select * from Currency where Currency_Name='"+marketPriceModel.PriceUnit+"'";
                dt = DBHelper.ExecuteQueryDT(hb);
                if (marketPriceModel.PriceUnit ==""||dt.Rows.Count==0)
                {
                    MessageBox.Show("价格单位第" + m + "行数据有误");
                    x = 0;
                    break;
                }
                if (marketPriceModel.CountUnit =="")
                {
                    MessageBox.Show("计量单位第" + m + "行数据为空");
                    x = 0;
                    break;
                }
                DateTime dtDate;
                if (marketPriceModel.StartTime ==""||(!DateTime.TryParse(marketPriceModel.StartTime,out dtDate)))
                {
                    MessageBox.Show("开始时间第" + m + "行数据不合法");
                    x = 0;
                    break;
                }
                if (marketPriceModel.EndTime ==""||(!DateTime.TryParse(marketPriceModel.EndTime,out dtDate)))
                {
                    MessageBox.Show("结束时间第" + m + "行数据不合法");
                    x = 0;
                    break;
                }



                //判断物料组和物料关系

                dt = null;
                string m1 = "select * from Material,Material_Group where Material.Material_Group=Material_Group.Material_Group and Description='" + marketPriceModel.MtGroupname + "' and Material_Name='" + marketPriceModel.Mtname + "'";
                dt = DBHelper.ExecuteQueryDT(m1);
                if (dt.Rows.Count == 0)
                {
                    MessageBox.Show("第" + m + "行物料组和物料关系有误");
                    x = 0;
                    break;
                }
                

                //判断物料编号和物料名称是否对应
                dt = null;
                string m2 = "select * from Material where Material_Name='"+marketPriceModel.Mtname+"' and Material_ID='"+marketPriceModel.MtId+"'";
                dt = DBHelper.ExecuteQueryDT(m2);
                if (dt.Rows.Count == 0)
                {
                    MessageBox.Show("第"+m+"行物料编号和物料名称不匹配");
                    x = 0;
                    break;
                }
                
                //判断物料的单位
                dt = null;
                string dw = "select * from Material where Material_Name='"+marketPriceModel.Mtname+"' and Measurement='"+marketPriceModel.CountUnit+"'";
                dt = DBHelper.ExecuteQueryDT(dw);
                if (dt.Rows.Count == 0)
                {
                    MessageBox.Show("第"+m+"行物料单位不匹配");
                    x = 0;
                    break;
                }
                
                m++;

            }
            if (x == 1)
            {
                foreach (DataRow row in this.excelTbl.Rows)
                {
                    marketPriceModel.Purname = row["采购组织"].ToString();
                    marketPriceModel.MtGroupname = row["物料组"].ToString();
                    marketPriceModel.MtId = row["物料编号"].ToString();
                    marketPriceModel.Mtname = row["物料名称"].ToString();
                    marketPriceModel.BusType = row["记录类型"].ToString();
                    marketPriceModel.PriceNum = row["价格数"].ToString();
                    marketPriceModel.PriceUnit = row["价格单位"].ToString();
                    marketPriceModel.CountUnit = row["计量单位"].ToString();
                    marketPriceModel.StartTime = row["开始时间"].ToString();
                    marketPriceModel.EndTime = row["结束时间"].ToString();

                    string sql = "INSERT INTO New_MarketPrice(PurGroupName,MtGroupName,PriceUnit,PriceNum,StartTime,EndTime,BusType,countUnit,MtName)VALUES('" + marketPriceModel.Purname + "','" + marketPriceModel.MtGroupname + "','" + marketPriceModel.PriceUnit + "','" + marketPriceModel.PriceNum + "','" + marketPriceModel.StartTime + "','" + marketPriceModel.EndTime + "','" + marketPriceModel.BusType + "','" + marketPriceModel.CountUnit + "','" + marketPriceModel.Mtname + "')";
                    DBHelper.ExecuteNonQuery(sql);

                    
                    

                }


                if (!erroInfo.Equals(""))
                {
                    if (MessageUtil.ShowYesNoAndError(erroInfo + "\n其他数据导入成功!是否继续") == DialogResult.No)
                    {

                        this.Close();

                    }
                }
                else
                {
                    if (MessageUtil.ShowYesNoAndTips("保存成功！是否继续?") == DialogResult.No)
                    {


                        this.Close();

                    }
                }
            }
           
        }

        private void imporMarketPriceApplicationForm_Load(object sender, EventArgs e)
        {

        }
    }
}
