﻿
using Lib.Bll.MDBll.General;
using Lib.SqlServerDAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;

namespace MMClient.MD.NewMMarketPrice.createRecordInfo
{
    public partial class saveAddInfoPUrRecordForm1 : Form
    {
        private string supplierName;
        private string purName;
        private string mtGroupName;
        private string mtName;
        private string recordCode;
        private string factory;
        private string recordClass;
        private string supplierId;
        private GeneralBLL gn = new GeneralBLL();
        public saveAddInfoPUrRecordForm1(string supplierId,string supplierName, string purName, string mtGroupName, string mtName,string recordCode, string factory, string recordClass)
        {
            this.supplierName = supplierName;
            this.purName = purName;
            this.mtGroupName = mtGroupName;
            this.mtName = mtName;
            this.recordCode = recordCode;
            this.factory = factory;
            this.recordClass = recordClass;
            this.supplierId = supplierId;
            InitializeComponent();
            
           
            init();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (isNullOrEmpty())
            {
                MessageBox.Show("录入信息不完整或非法，请检查");
                return;
            }
            MessageBoxButtons messButton = MessageBoxButtons.OKCancel;
            DialogResult dr = MessageBox.Show("是否保存当前信息并创建采购信息记录？", "警  告", messButton);
            
            if ("保准信息记录".Equals(recordClass))
            {
                recordClass = "1";
            }
            else if("委托加工记录".Equals(recordClass))
            {
                recordClass = "2";
            }
            else if("寄售信息记录".Equals(recordClass))
            {
                recordClass = "3";
            }
            else
            {
                recordClass = "4";
            }
            if (dr == DialogResult.OK)
            {
                //save info
                DataTable dt = new DataTable();
                String mtgroupid = "select Material_Group from Material_Group where Description='"+this.txtMtGroupName.Text.ToString()+"'";
                dt = DBHelper.ExecuteQueryDT(mtgroupid);
                string mtgrid = dt.Rows[0]["Material_Group"].ToString();
                string mtid = "select Material_ID from Material where Material_Name='"+mtName+"'";
                dt = DBHelper.ExecuteQueryDT(mtid);
                string mtId = dt.Rows[0]["Material_ID"].ToString();
                string sql = @"INSERT INTO RecordInfo (
	                                        RecordId,
	                                        SupplierId,
	                                        SupplierName,
	                                        PurchaseOrg,
                                            mtGroupName,
                                            mtGroupId,
                                            MaterialId,
                                            MaterialName,
                                            recordClass,
                                            texCode,
                                            enterRule,
                                            discountClass,
	                                        NetPrice,
                                            validationPrice,
	                                        PaymentClause,
                                            unit,
                                            cashDiscount,
	                                        StartTime,
	                                        EndTime,
                                            DemandCount,
                                            delFlag,
                                            CreateTime
                                        )VALUES('" + recordCode+"','"+supplierId+"','"+supplierName+"','"+purName+"','"+this.txtMtGroupName.Text.ToString()+ "','"+mtgrid+"','"+mtId+"','"+mtName+"','"+this.txtRecordClass.Text.ToString()+"','"+this.texCode.Text.ToString()+"','"+this.cbRule.Text.ToString()+"','"+this.cbDiscountClass.Text.ToString()+ "','" + this.txtPrice.Text.ToString() + "','" + this.txtValidatePrice.Text.ToString() + "','"+this.cbCurClass.Text.ToString()+"','"+this.cbUnit.Text.ToString()+ "','" + this.txtcashDiscount.Text.ToString() + "','" + this.dateTimePicker1.Value.ToString()+"','"+this.dateTimePicker2.Value.ToString()+"',10,0,'"+this.dateTimePicker3.Value.ToString()+"')";
                //采购数量字段DemandCount 不知其来源无可考证  为计算抱怨水平这里默认写入10
                string sql_1 = @"INSERT INTO PurcharseInfoRecord (
	                                        [recordCode],
	                                        [supplierID],
	                                        [supplierName],
	                                        [PurName],
	                                        [mtGroupName],
                                            [mtName],
	                                        [startTime],
	                                        [endTime],
	                                        [factoryID],
	                                        [enterRule],
	                                        [texCode],
	                                        [discountClass],
	                                        [Price],
	                                        [validationPrice],
	                                        [CurrencyClass],
	                                        [unit],
	                                        [cashDiscount],
                                            [recordClass]
                                        )VALUES('" + recordCode + "','" + supplierId + "','" + supplierName + "','" + purName + "','" + mtGroupName + "','" + mtName + "','" + this.dateTimePicker1.Value.ToString() + "','" + this.dateTimePicker2.Value.ToString() + "','" + factory + "','" + this.cbRule.Text.ToString() + "','" + this.texCode.Text.ToString() + "','" + this.cbDiscountClass.Text.ToString() + "','" + this.txtPrice.Text.ToString() + "','" + this.txtValidatePrice.Text.ToString() + "','" + this.cbCurClass.Text.ToString() + "','" + this.cbUnit.Text.ToString() + "','" + this.txtcashDiscount.Text.ToString() + "','" + recordClass + "')";

                try
                {
                    DBHelper.ExecuteNonQuery(sql);
                    DBHelper.ExecuteNonQuery(sql_1);
                    MessageBox.Show("信息保存成功");
                    this.Close();
                }
                catch
                {
                    MessageBox.Show("信息保存失败");
                    return;
                }
            }
            else
            {
                return;
            }
        }
        private  void init()
        {
            this.txtCode.Text = recordCode;
            this.txtRecordClass.Text = recordClass;
            this.txtSupplierName.Text = supplierName;
            this.txtPurName.Text = purName;
            this.txtMtGroupName.Text = mtGroupName;
            this.txtFactory.Text = factory;
            this.txtMtName.Text = mtName;
            List<string> list1 = gn.GetAllMeasurement();
            gn.IDInTheList(list1, cbUnit.Text);
            string u = gn.getUnitByMateriID(mtName);
            if(!"err".Equals(u)) cbUnit.Text = u;
        }
        private bool isNullOrEmpty()
        {
            if(String.IsNullOrEmpty(this.txtMtName.Text.ToString()) ||String.IsNullOrEmpty(texCode.Text.ToString())||String.IsNullOrEmpty(cbRule.Text.ToString())||String.IsNullOrEmpty(cbDiscountClass.Text.ToString())
                ||String.IsNullOrEmpty(txtPrice.Text.ToString())|| String.IsNullOrEmpty(txtValidatePrice.Text.ToString()) || String.IsNullOrEmpty(txtcashDiscount.Text.ToString())
                || String.IsNullOrEmpty(cbCurClass.Text.ToString())||String.IsNullOrEmpty(cbUnit.Text.ToString()))
            {
                try
                {
                    float.Parse(txtPrice.Text.ToString());
                    float.Parse(txtValidatePrice.Text.ToString());
                }
                catch
                {
                    return true;
                }
               
                return true;
            }
            return false;
        }

        private void txtMtName_TextChanged(object sender, EventArgs e)
        {
            
            string sql = "select countUnit from New_MarketPrice where MtName=(select Material_Name from Material where Material_ID='" + this.txtMtName.Text.ToString() + "')";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            try
            {
                this.cbUnit.Text = dt.Rows[0][0].ToString();
            }
            catch
            {
                this.cbUnit.Text = "";
            }

            string sql_1 = "select PriceNum from New_MarketPrice where PurGroupName='" + this.txtPurName.Text.ToString() + "' and MtGroupName='" + this.txtMtGroupName.Text.ToString() + "' and MtName=(select Material_Name from Material where Material_ID='"+this.txtMtName.Text.ToString()+"') and BusType='" + this.txtRecordClass.Text.ToString() + "'";
            DataTable dt_1 = DBHelper.ExecuteQueryDT(sql_1);
            try
            {
                this.txtPrice.Text = dt_1.Rows[0][0].ToString();
            }
            catch
            {
                this.txtPrice.Text = "";
            }
        }

        private void txtCode_TextChanged(object sender, EventArgs e)
        {
            string sql_1 = "select Currency_Name as name from Currency";
            DataTable dt_1 = DBHelper.ExecuteQueryDT(sql_1);

            if (dt_1 != null && dt_1.Rows.Count > 0)
            {
                this.cbCurClass.DataSource = dt_1;
                this.cbCurClass.DisplayMember = "name";
                this.cbCurClass.ValueMember = "name";
            }

            
        }

        private void cbRule_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void cbCurClass_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void txtPrice_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
