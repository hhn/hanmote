﻿namespace MMClient.MD.NewMMarketPrice.createRecordInfo
{
    partial class PurchasesRecordInfoViewForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel1 = new System.Windows.Forms.Panel();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.cbmatrialName = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cbClassInfo = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.txtsupplierID = new System.Windows.Forms.TextBox();
            this.cbSupplierName = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.txtPurName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.PurPriceTable = new System.Windows.Forms.DataGridView();
            this.信息记录ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.物料ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.金额 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.币种 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.单位 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.修改 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.删除 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PurPriceTable)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.dateTimePicker2);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.dateTimePicker1);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.cbmatrialName);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.cbClassInfo);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.button2);
            this.panel1.Controls.Add(this.txtsupplierID);
            this.panel1.Controls.Add(this.cbSupplierName);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.txtPurName);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(-2, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(809, 144);
            this.panel1.TabIndex = 3;
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.Location = new System.Drawing.Point(518, 55);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(132, 21);
            this.dateTimePicker2.TabIndex = 28;
            this.dateTimePicker2.Value = new System.DateTime(2019, 2, 10, 0, 0, 0, 0);
            this.dateTimePicker2.ValueChanged += new System.EventHandler(this.dateTimePicker2_ValueChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(485, 61);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(17, 12);
            this.label5.TabIndex = 27;
            this.label5.Text = "至";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(348, 55);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(122, 21);
            this.dateTimePicker1.TabIndex = 26;
            this.dateTimePicker1.Value = new System.DateTime(2018, 10, 17, 0, 0, 0, 0);
            this.dateTimePicker1.ValueChanged += new System.EventHandler(this.dateTimePicker1_ValueChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(289, 61);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 12);
            this.label4.TabIndex = 25;
            this.label4.Text = "时间范围";
            // 
            // cbmatrialName
            // 
            this.cbmatrialName.FormattingEnabled = true;
            this.cbmatrialName.Location = new System.Drawing.Point(126, 58);
            this.cbmatrialName.Name = "cbmatrialName";
            this.cbmatrialName.Size = new System.Drawing.Size(121, 20);
            this.cbmatrialName.TabIndex = 24;
            this.cbmatrialName.SelectedIndexChanged += new System.EventHandler(this.cbmatrialName_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(66, 61);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 12);
            this.label3.TabIndex = 23;
            this.label3.Text = "物料名称";
            // 
            // cbClassInfo
            // 
            this.cbClassInfo.FormattingEnabled = true;
            this.cbClassInfo.Items.AddRange(new object[] {
            "标准信息记录",
            "委托加工记录",
            "寄售信息记录",
            "管道信息记录"});
            this.cbClassInfo.Location = new System.Drawing.Point(126, 100);
            this.cbClassInfo.Name = "cbClassInfo";
            this.cbClassInfo.Size = new System.Drawing.Size(121, 20);
            this.cbClassInfo.TabIndex = 22;
            this.cbClassInfo.Text = "标准信息记录";
            this.cbClassInfo.SelectedIndexChanged += new System.EventHandler(this.cbClassInfo_SelectedIndexChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(66, 103);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 12);
            this.label6.TabIndex = 21;
            this.label6.Text = "信息类别";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(518, 100);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(121, 23);
            this.button2.TabIndex = 20;
            this.button2.Text = "查  询";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // txtsupplierID
            // 
            this.txtsupplierID.Location = new System.Drawing.Point(275, 21);
            this.txtsupplierID.Name = "txtsupplierID";
            this.txtsupplierID.ReadOnly = true;
            this.txtsupplierID.Size = new System.Drawing.Size(121, 21);
            this.txtsupplierID.TabIndex = 17;
            this.txtsupplierID.TextChanged += new System.EventHandler(this.txtsupplierID_TextChanged);
            // 
            // cbSupplierName
            // 
            this.cbSupplierName.FormattingEnabled = true;
            this.cbSupplierName.Location = new System.Drawing.Point(126, 21);
            this.cbSupplierName.Name = "cbSupplierName";
            this.cbSupplierName.Size = new System.Drawing.Size(121, 20);
            this.cbSupplierName.TabIndex = 16;
            this.cbSupplierName.SelectedIndexChanged += new System.EventHandler(this.cbSupplierName_SelectedIndexChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label7.Location = new System.Drawing.Point(0, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(67, 14);
            this.label7.TabIndex = 14;
            this.label7.Text = "基本信息";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(406, 309);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 12;
            this.button1.Text = "下一步";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // txtPurName
            // 
            this.txtPurName.Location = new System.Drawing.Point(518, 18);
            this.txtPurName.Name = "txtPurName";
            this.txtPurName.ReadOnly = true;
            this.txtPurName.Size = new System.Drawing.Size(121, 21);
            this.txtPurName.TabIndex = 3;
            this.txtPurName.TextChanged += new System.EventHandler(this.txtPurName_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(449, 24);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 2;
            this.label2.Text = "采购组织";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(66, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "供应商ID";
            // 
            // PurPriceTable
            // 
            this.PurPriceTable.AllowUserToAddRows = false;
            this.PurPriceTable.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.PurPriceTable.BackgroundColor = System.Drawing.SystemColors.ControlLight;
            this.PurPriceTable.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.PurPriceTable.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.PurPriceTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.PurPriceTable.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.信息记录ID,
            this.物料ID,
            this.金额,
            this.币种,
            this.单位,
            this.修改,
            this.删除});
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.PurPriceTable.DefaultCellStyle = dataGridViewCellStyle4;
            this.PurPriceTable.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.PurPriceTable.GridColor = System.Drawing.SystemColors.ControlLight;
            this.PurPriceTable.Location = new System.Drawing.Point(-2, 162);
            this.PurPriceTable.Name = "PurPriceTable";
            this.PurPriceTable.RowTemplate.Height = 23;
            this.PurPriceTable.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.PurPriceTable.Size = new System.Drawing.Size(809, 367);
            this.PurPriceTable.TabIndex = 17;
            this.PurPriceTable.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.PurPriceTable_CellContentClick);
            // 
            // 信息记录ID
            // 
            this.信息记录ID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.信息记录ID.DataPropertyName = "信息记录ID";
            this.信息记录ID.Frozen = true;
            this.信息记录ID.HeaderText = "信息记录ID";
            this.信息记录ID.Name = "信息记录ID";
            this.信息记录ID.Width = 160;
            // 
            // 物料ID
            // 
            this.物料ID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.物料ID.DataPropertyName = "物料ID";
            this.物料ID.Frozen = true;
            this.物料ID.HeaderText = "物料ID";
            this.物料ID.Name = "物料ID";
            this.物料ID.Width = 160;
            // 
            // 金额
            // 
            this.金额.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.金额.DataPropertyName = "金额";
            this.金额.HeaderText = "金额";
            this.金额.Name = "金额";
            this.金额.Width = 110;
            // 
            // 币种
            // 
            this.币种.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.币种.DataPropertyName = "币种";
            this.币种.HeaderText = "币种";
            this.币种.Name = "币种";
            this.币种.Width = 90;
            // 
            // 单位
            // 
            this.单位.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.单位.DataPropertyName = "单位";
            this.单位.HeaderText = "单位";
            this.单位.Name = "单位";
            this.单位.Width = 80;
            // 
            // 修改
            // 
            this.修改.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.NullValue = "修改";
            this.修改.DefaultCellStyle = dataGridViewCellStyle2;
            this.修改.HeaderText = "修改";
            this.修改.Name = "修改";
            this.修改.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.修改.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.修改.Text = "修改";
            this.修改.Width = 80;
            // 
            // 删除
            // 
            this.删除.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.NullValue = "删除";
            this.删除.DefaultCellStyle = dataGridViewCellStyle3;
            this.删除.HeaderText = "删除";
            this.删除.Name = "删除";
            this.删除.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.删除.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.删除.Text = "删除";
            this.删除.Width = 80;
            // 
            // PurchasesRecordInfoViewForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(808, 607);
            this.Controls.Add(this.PurPriceTable);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "PurchasesRecordInfoViewForm";
            this.Text = "采购信息记录查看";
            this.Load += new System.EventHandler(this.SupplierName_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PurPriceTable)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txtPurName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbSupplierName;
        public System.Windows.Forms.DataGridView PurPriceTable;
        private System.Windows.Forms.TextBox txtsupplierID;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.ComboBox cbClassInfo;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cbmatrialName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridViewTextBoxColumn 信息记录ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn 物料ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn 金额;
        private System.Windows.Forms.DataGridViewTextBoxColumn 币种;
        private System.Windows.Forms.DataGridViewTextBoxColumn 单位;
        private System.Windows.Forms.DataGridViewButtonColumn 修改;
        private System.Windows.Forms.DataGridViewButtonColumn 删除;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
    }
}