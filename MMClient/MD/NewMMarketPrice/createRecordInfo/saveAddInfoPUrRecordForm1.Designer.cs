﻿namespace MMClient.MD.NewMMarketPrice.createRecordInfo
{
    partial class saveAddInfoPUrRecordForm1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtMtName = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.txtRecordClass = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtSupplierName = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.txtCode = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtFactory = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtMtGroupName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtPurName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.cbUnit = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.txtcashDiscount = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.label14 = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.cbCurClass = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtValidatePrice = new System.Windows.Forms.TextBox();
            this.txtPrice = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.texCode = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this.cbDiscountClass = new System.Windows.Forms.ComboBox();
            this.cbRule = new System.Windows.Forms.ComboBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.button2 = new System.Windows.Forms.Button();
            this.label19 = new System.Windows.Forms.Label();
            this.dateTimePicker3 = new System.Windows.Forms.DateTimePicker();
            this.label24 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.txtMtName);
            this.panel1.Controls.Add(this.label23);
            this.panel1.Controls.Add(this.txtRecordClass);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.txtSupplierName);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.txtCode);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.txtFactory);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.txtMtGroupName);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.txtPurName);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(2, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(892, 181);
            this.panel1.TabIndex = 1;
            // 
            // txtMtName
            // 
            this.txtMtName.Location = new System.Drawing.Point(548, 100);
            this.txtMtName.Name = "txtMtName";
            this.txtMtName.ReadOnly = true;
            this.txtMtName.Size = new System.Drawing.Size(121, 21);
            this.txtMtName.TabIndex = 17;
            this.txtMtName.TextChanged += new System.EventHandler(this.txtMtName_TextChanged);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(513, 102);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(29, 12);
            this.label23.TabIndex = 16;
            this.label23.Text = "物料";
            // 
            // txtRecordClass
            // 
            this.txtRecordClass.Location = new System.Drawing.Point(126, 141);
            this.txtRecordClass.Name = "txtRecordClass";
            this.txtRecordClass.ReadOnly = true;
            this.txtRecordClass.Size = new System.Drawing.Size(121, 21);
            this.txtRecordClass.TabIndex = 15;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label7.Location = new System.Drawing.Point(0, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(67, 14);
            this.label7.TabIndex = 14;
            this.label7.Text = "基本信息";
            // 
            // txtSupplierName
            // 
            this.txtSupplierName.Location = new System.Drawing.Point(126, 62);
            this.txtSupplierName.Name = "txtSupplierName";
            this.txtSupplierName.ReadOnly = true;
            this.txtSupplierName.Size = new System.Drawing.Size(121, 21);
            this.txtSupplierName.TabIndex = 13;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(406, 309);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 12;
            this.button1.Text = "下一步";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(54, 141);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 12);
            this.label6.TabIndex = 10;
            this.label6.Text = "信息类别";
            // 
            // txtCode
            // 
            this.txtCode.Location = new System.Drawing.Point(126, 25);
            this.txtCode.Name = "txtCode";
            this.txtCode.ReadOnly = true;
            this.txtCode.Size = new System.Drawing.Size(121, 21);
            this.txtCode.TabIndex = 9;
            this.txtCode.TextChanged += new System.EventHandler(this.txtCode_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 28);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(101, 12);
            this.label5.TabIndex = 8;
            this.label5.Text = "采购信息记录编码";
            // 
            // txtFactory
            // 
            this.txtFactory.Location = new System.Drawing.Point(755, 99);
            this.txtFactory.Name = "txtFactory";
            this.txtFactory.ReadOnly = true;
            this.txtFactory.Size = new System.Drawing.Size(121, 21);
            this.txtFactory.TabIndex = 7;
            this.txtFactory.Visible = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(717, 102);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(29, 12);
            this.label4.TabIndex = 6;
            this.label4.Text = "工厂";
            this.label4.Visible = false;
            // 
            // txtMtGroupName
            // 
            this.txtMtGroupName.Location = new System.Drawing.Point(347, 100);
            this.txtMtGroupName.Name = "txtMtGroupName";
            this.txtMtGroupName.ReadOnly = true;
            this.txtMtGroupName.Size = new System.Drawing.Size(121, 21);
            this.txtMtGroupName.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(300, 103);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 12);
            this.label3.TabIndex = 4;
            this.label3.Text = "物料组";
            // 
            // txtPurName
            // 
            this.txtPurName.Location = new System.Drawing.Point(126, 100);
            this.txtPurName.Name = "txtPurName";
            this.txtPurName.ReadOnly = true;
            this.txtPurName.Size = new System.Drawing.Size(121, 21);
            this.txtPurName.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(54, 103);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 2;
            this.label2.Text = "采购组织";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(66, 62);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "供应商";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label24);
            this.panel2.Controls.Add(this.dateTimePicker3);
            this.panel2.Controls.Add(this.cbUnit);
            this.panel2.Controls.Add(this.label22);
            this.panel2.Controls.Add(this.label21);
            this.panel2.Controls.Add(this.txtcashDiscount);
            this.panel2.Controls.Add(this.label20);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.dateTimePicker2);
            this.panel2.Controls.Add(this.label14);
            this.panel2.Controls.Add(this.dateTimePicker1);
            this.panel2.Controls.Add(this.label13);
            this.panel2.Controls.Add(this.label12);
            this.panel2.Controls.Add(this.cbCurClass);
            this.panel2.Controls.Add(this.label11);
            this.panel2.Controls.Add(this.txtValidatePrice);
            this.panel2.Controls.Add(this.txtPrice);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Location = new System.Drawing.Point(2, 307);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(892, 186);
            this.panel2.TabIndex = 2;
            // 
            // cbUnit
            // 
            this.cbUnit.Location = new System.Drawing.Point(476, 59);
            this.cbUnit.Name = "cbUnit";
            this.cbUnit.Size = new System.Drawing.Size(121, 21);
            this.cbUnit.TabIndex = 20;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.ForeColor = System.Drawing.Color.Red;
            this.label22.Location = new System.Drawing.Point(265, 66);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(65, 12);
            this.label22.TabIndex = 19;
            this.label22.Text = "只能是数字";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.ForeColor = System.Drawing.Color.Red;
            this.label21.Location = new System.Drawing.Point(265, 24);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(65, 12);
            this.label21.TabIndex = 18;
            this.label21.Text = "只能是数字";
            // 
            // txtcashDiscount
            // 
            this.txtcashDiscount.Location = new System.Drawing.Point(709, 59);
            this.txtcashDiscount.Name = "txtcashDiscount";
            this.txtcashDiscount.ReadOnly = true;
            this.txtcashDiscount.Size = new System.Drawing.Size(121, 21);
            this.txtcashDiscount.TabIndex = 13;
            this.txtcashDiscount.Text = "0";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(650, 66);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(53, 12);
            this.label20.TabIndex = 12;
            this.label20.Text = "现金折扣";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label8.Location = new System.Drawing.Point(5, 1);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(37, 14);
            this.label8.TabIndex = 3;
            this.label8.Text = "条件";
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.Location = new System.Drawing.Point(302, 107);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(126, 21);
            this.dateTimePicker2.TabIndex = 11;
            this.dateTimePicker2.Value = new System.DateTime(2019, 1, 24, 0, 0, 0, 0);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(265, 112);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(17, 12);
            this.label14.TabIndex = 10;
            this.label14.Text = "至";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(123, 106);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(121, 21);
            this.dateTimePicker1.TabIndex = 9;
            this.dateTimePicker1.Value = new System.DateTime(2019, 1, 24, 10, 57, 31, 0);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(63, 112);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(41, 12);
            this.label13.TabIndex = 8;
            this.label13.Text = "有效期";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(430, 64);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(29, 12);
            this.label12.TabIndex = 7;
            this.label12.Text = "单位";
            // 
            // cbCurClass
            // 
            this.cbCurClass.FormattingEnabled = true;
            this.cbCurClass.Location = new System.Drawing.Point(476, 18);
            this.cbCurClass.Name = "cbCurClass";
            this.cbCurClass.Size = new System.Drawing.Size(121, 20);
            this.cbCurClass.TabIndex = 5;
            this.cbCurClass.SelectedIndexChanged += new System.EventHandler(this.cbCurClass_SelectedIndexChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(406, 24);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(53, 12);
            this.label11.TabIndex = 4;
            this.label11.Text = "货币类型";
            // 
            // txtValidatePrice
            // 
            this.txtValidatePrice.Location = new System.Drawing.Point(123, 59);
            this.txtValidatePrice.Name = "txtValidatePrice";
            this.txtValidatePrice.Size = new System.Drawing.Size(121, 21);
            this.txtValidatePrice.TabIndex = 3;
            // 
            // txtPrice
            // 
            this.txtPrice.Location = new System.Drawing.Point(123, 18);
            this.txtPrice.Name = "txtPrice";
            this.txtPrice.Size = new System.Drawing.Size(121, 21);
            this.txtPrice.TabIndex = 2;
            this.txtPrice.TextChanged += new System.EventHandler(this.txtPrice_TextChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(63, 64);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(41, 12);
            this.label10.TabIndex = 1;
            this.label10.Text = "有效价";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(63, 21);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(41, 12);
            this.label9.TabIndex = 0;
            this.label9.Text = "市场价";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.texCode);
            this.panel3.Controls.Add(this.label16);
            this.panel3.Controls.Add(this.cbDiscountClass);
            this.panel3.Controls.Add(this.cbRule);
            this.panel3.Controls.Add(this.label17);
            this.panel3.Controls.Add(this.label18);
            this.panel3.Controls.Add(this.label15);
            this.panel3.Location = new System.Drawing.Point(2, 199);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(892, 102);
            this.panel3.TabIndex = 3;
            // 
            // texCode
            // 
            this.texCode.FormattingEnabled = true;
            this.texCode.Items.AddRange(new object[] {
            "J1",
            "J2",
            "T2"});
            this.texCode.Location = new System.Drawing.Point(123, 42);
            this.texCode.Name = "texCode";
            this.texCode.Size = new System.Drawing.Size(121, 20);
            this.texCode.TabIndex = 16;
            this.texCode.Text = "J1";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(489, 45);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(53, 12);
            this.label16.TabIndex = 15;
            this.label16.Text = "折扣类型";
            // 
            // cbDiscountClass
            // 
            this.cbDiscountClass.FormattingEnabled = true;
            this.cbDiscountClass.Items.AddRange(new object[] {
            "比例",
            "定额"});
            this.cbDiscountClass.Location = new System.Drawing.Point(548, 40);
            this.cbDiscountClass.Name = "cbDiscountClass";
            this.cbDiscountClass.Size = new System.Drawing.Size(121, 20);
            this.cbDiscountClass.TabIndex = 14;
            this.cbDiscountClass.Text = "比例";
            // 
            // cbRule
            // 
            this.cbRule.FormattingEnabled = true;
            this.cbRule.Items.AddRange(new object[] {
            "商业",
            "向上取整",
            "向下取整"});
            this.cbRule.Location = new System.Drawing.Point(347, 42);
            this.cbRule.Name = "cbRule";
            this.cbRule.Size = new System.Drawing.Size(121, 20);
            this.cbRule.TabIndex = 13;
            this.cbRule.Text = "向下取整";
            this.cbRule.SelectedIndexChanged += new System.EventHandler(this.cbRule_SelectedIndexChanged);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(277, 48);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(53, 12);
            this.label17.TabIndex = 12;
            this.label17.Text = "舍入规则";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(75, 45);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(29, 12);
            this.label18.TabIndex = 9;
            this.label18.Text = "税码";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label15.Location = new System.Drawing.Point(3, 4);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(37, 14);
            this.label15.TabIndex = 1;
            this.label15.Text = "控制";
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.button2);
            this.panel4.Controls.Add(this.label19);
            this.panel4.Location = new System.Drawing.Point(2, 490);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(889, 133);
            this.panel4.TabIndex = 4;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(402, 97);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(97, 32);
            this.button2.TabIndex = 1;
            this.button2.Text = "保  存";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(0, 16);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(707, 24);
            this.label19.TabIndex = 0;
            this.label19.Text = "说明：\r\n  含入规则中，商业表示四舍五入；向上取整就是全部进入，例如13.145表示为13.15；向下取整就是舍掉，例如13.145表示为13.14.";
            // 
            // dateTimePicker3
            // 
            this.dateTimePicker3.Location = new System.Drawing.Point(123, 147);
            this.dateTimePicker3.Name = "dateTimePicker3";
            this.dateTimePicker3.Size = new System.Drawing.Size(116, 21);
            this.dateTimePicker3.TabIndex = 21;
            this.dateTimePicker3.Value = new System.DateTime(2019, 1, 29, 12, 1, 52, 0);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(54, 153);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(53, 12);
            this.label24.TabIndex = 22;
            this.label24.Text = "创建时间";
            // 
            // saveAddInfoPUrRecordForm1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(890, 658);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "saveAddInfoPUrRecordForm1";
            this.Text = "添加市场价格记录";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtCode;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtFactory;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtMtGroupName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtPurName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtSupplierName;
        private System.Windows.Forms.TextBox txtRecordClass;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox txtcashDiscount;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox cbCurClass;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtValidatePrice;
        private System.Windows.Forms.TextBox txtPrice;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ComboBox cbDiscountClass;
        private System.Windows.Forms.ComboBox cbRule;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txtMtName;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.ComboBox texCode;
        private System.Windows.Forms.TextBox cbUnit;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.DateTimePicker dateTimePicker3;
    }
}