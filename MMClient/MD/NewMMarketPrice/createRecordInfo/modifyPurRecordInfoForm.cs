﻿using Lib.Bll.MDBll.General;
using Lib.SqlServerDAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MMClient.MD.NewMMarketPrice.createRecordInfo
{
    public partial class modifyPurRecordInfoForm : Form
    {
        private string recordCode;
        private DataTable dt = null;
        private GeneralBLL gn = new GeneralBLL();
        public modifyPurRecordInfoForm(string recordCode,DataTable dt)
        {
            InitializeComponent();
            this.recordCode = recordCode;
            this.txtRecordID.Text = recordCode;
            this.dt = dt;
            
            initial();
        }

        private void initial()
        {
            texCode.Text = dt.Rows[0][0].ToString(); 
            cbRule.Text = dt.Rows[0][1].ToString();
            cbDiscountClass.Text = dt.Rows[0][2].ToString();
            txtPrice.Text = dt.Rows[0][3].ToString();
            txtValidatePrice.Text = dt.Rows[0][4].ToString();
            txtcashDiscount.Text = dt.Rows[0][5].ToString();
            cbCurClass.Text = dt.Rows[0][6].ToString();
            cbUnit.Text = dt.Rows[0][7].ToString();
            dateTimePicker1.Value = Convert.ToDateTime(dt.Rows[0][8].ToString());
            dateTimePicker2.Value = Convert.ToDateTime(dt.Rows[0][9].ToString());
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (isNullOrEmpty())
            {
                MessageBox.Show("信息不完整或不合法");
                return ;
            }
            string update = @"UPDATE RecordInfo
                                SET texCode = '" + texCode.Text.ToString() + @"',
                                 enterRule = '" + cbRule.Text.ToString() + @"',
                                 discountClass = '" + cbDiscountClass.Text.ToString() + @"',
                                 NetPrice = '" + txtPrice.Text.ToString() + @"',
                                 validationPrice = '" + txtValidatePrice.Text.ToString() + @"',
                                 cashDiscount = '" + txtcashDiscount.Text.ToString() + @"',
                                 PaymentClause ='" + cbCurClass.Text.ToString() + @"',
                                 StartTime='"+this.dateTimePicker1.Value.ToString()+ @"',
                                 EndTime='"+this.dateTimePicker2.Value.ToString()+"' WHERE  recordId='"+recordCode+"' ";
            MessageBoxButtons messButton = MessageBoxButtons.OKCancel;
            DialogResult dr = MessageBox.Show("是否确定修改该条记录？", "警  告", messButton);
            if (dr == DialogResult.OK)
            {
                DBHelper.ExecuteNonQuery(update);
                MessageBox.Show("修改成功");
                this.Close();
            }
        }
        private bool isNullOrEmpty()
        {
            if (String.IsNullOrEmpty(texCode.Text.ToString()) || String.IsNullOrEmpty(cbRule.Text.ToString()) || String.IsNullOrEmpty(cbDiscountClass.Text.ToString())
                || String.IsNullOrEmpty(txtPrice.Text.ToString()) || String.IsNullOrEmpty(txtValidatePrice.Text.ToString()) || String.IsNullOrEmpty(txtcashDiscount.Text.ToString())
                || String.IsNullOrEmpty(cbCurClass.Text.ToString()) || String.IsNullOrEmpty(cbUnit.Text.ToString()))
            {
                try
                {
                    float.Parse(txtPrice.Text.ToString());
                    float.Parse(txtValidatePrice.Text.ToString());
                }
                catch
                {
                    return true;
                }

                return true;
            }
            return false;
        }

        private void txtRecordID_TextChanged(object sender, EventArgs e)
        {
            this.cbUnit.Text = "";
            string sql = "select unit from RecordInfo where RecordId='" + this.txtRecordID.Text.ToString() + "'";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            try
            {
                this.cbUnit.Text = dt.Rows[0][0].ToString();
            }
            catch
            {
                this.cbUnit.Text = "";
            }

            string sql_1 = "select Currency_Name as name from Currency";
            DataTable dt_1 = DBHelper.ExecuteQueryDT(sql_1);

            if (dt_1 != null && dt_1.Rows.Count > 0)
            {
                this.cbCurClass.DataSource = dt_1;
                this.cbCurClass.DisplayMember = "name";
                this.cbCurClass.ValueMember = "name";
            }
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {

        }
    }
}
