﻿using System;

namespace MMClient.MD.NewMMarketPrice
{
    partial class AddPurRecordInfoForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtGroupName = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.txtMTID = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.supplierID = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.cbClassInfo = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtCode = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtFactory = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtPurName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cbSupplierName = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.txtGroupName);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.comboBox1);
            this.panel1.Controls.Add(this.txtMTID);
            this.panel1.Controls.Add(this.button2);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.supplierID);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.cbClassInfo);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.txtCode);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.txtFactory);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.txtPurName);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.cbSupplierName);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(0, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(877, 526);
            this.panel1.TabIndex = 0;
            // 
            // txtGroupName
            // 
            this.txtGroupName.FormattingEnabled = true;
            this.txtGroupName.Location = new System.Drawing.Point(141, 132);
            this.txtGroupName.Name = "txtGroupName";
            this.txtGroupName.Size = new System.Drawing.Size(121, 20);
            this.txtGroupName.TabIndex = 21;
            this.txtGroupName.SelectedIndexChanged += new System.EventHandler(this.txtGroupName_SelectedIndexChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(587, 240);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(89, 84);
            this.label8.TabIndex = 20;
            this.label8.Text = "1-保准信息记录\r\n\r\n2-委托加工记录\r\n\r\n3-寄售信息记录\r\n\r\n4-管道信息记录";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(141, 180);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(121, 20);
            this.comboBox1.TabIndex = 19;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged_1);
            // 
            // txtMTID
            // 
            this.txtMTID.Location = new System.Drawing.Point(289, 180);
            this.txtMTID.Name = "txtMTID";
            this.txtMTID.ReadOnly = true;
            this.txtMTID.Size = new System.Drawing.Size(204, 21);
            this.txtMTID.TabIndex = 18;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(13, 11);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 17;
            this.button2.Text = "重置";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(93, 184);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(29, 12);
            this.label7.TabIndex = 14;
            this.label7.Text = "物料";
            // 
            // supplierID
            // 
            this.supplierID.Location = new System.Drawing.Point(289, 50);
            this.supplierID.Name = "supplierID";
            this.supplierID.ReadOnly = true;
            this.supplierID.Size = new System.Drawing.Size(204, 21);
            this.supplierID.TabIndex = 13;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(371, 392);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 12;
            this.button1.Text = "下一步";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // cbClassInfo
            // 
            this.cbClassInfo.FormattingEnabled = true;
            this.cbClassInfo.Items.AddRange(new object[] {
            "标准信息记录",
            "委托加工记录",
            "寄售信息记录",
            "管道信息记录"});
            this.cbClassInfo.Location = new System.Drawing.Point(141, 267);
            this.cbClassInfo.Name = "cbClassInfo";
            this.cbClassInfo.Size = new System.Drawing.Size(121, 20);
            this.cbClassInfo.TabIndex = 11;
            this.cbClassInfo.Text = "标准信息记录";
            this.cbClassInfo.SelectedIndexChanged += new System.EventHandler(this.cbClassInfo_SelectedIndexChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(69, 267);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 12);
            this.label6.TabIndex = 10;
            this.label6.Text = "信息类别";
            // 
            // txtCode
            // 
            this.txtCode.Location = new System.Drawing.Point(141, 226);
            this.txtCode.Name = "txtCode";
            this.txtCode.ReadOnly = true;
            this.txtCode.Size = new System.Drawing.Size(121, 21);
            this.txtCode.TabIndex = 9;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(21, 229);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(101, 12);
            this.label5.TabIndex = 8;
            this.label5.Text = "采购信息记录编码";
            // 
            // txtFactory
            // 
            this.txtFactory.Location = new System.Drawing.Point(141, 226);
            this.txtFactory.Name = "txtFactory";
            this.txtFactory.ReadOnly = true;
            this.txtFactory.Size = new System.Drawing.Size(121, 21);
            this.txtFactory.TabIndex = 7;
            this.txtFactory.Text = "00001";
            this.txtFactory.Visible = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(93, 229);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(29, 12);
            this.label4.TabIndex = 6;
            this.label4.Text = "工厂";
            this.label4.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(81, 135);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 12);
            this.label3.TabIndex = 4;
            this.label3.Text = "物料组";
            // 
            // txtPurName
            // 
            this.txtPurName.Location = new System.Drawing.Point(141, 92);
            this.txtPurName.Name = "txtPurName";
            this.txtPurName.ReadOnly = true;
            this.txtPurName.Size = new System.Drawing.Size(121, 21);
            this.txtPurName.TabIndex = 3;
            this.txtPurName.TextChanged += new System.EventHandler(this.txtPurName_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(69, 95);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 2;
            this.label2.Text = "采购组织";
            // 
            // cbSupplierName
            // 
            this.cbSupplierName.FormattingEnabled = true;
            this.cbSupplierName.Location = new System.Drawing.Point(141, 51);
            this.cbSupplierName.Name = "cbSupplierName";
            this.cbSupplierName.Size = new System.Drawing.Size(121, 20);
            this.cbSupplierName.TabIndex = 1;
            this.cbSupplierName.SelectedIndexChanged += new System.EventHandler(this.cbSupplierName_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(57, 54);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "供应商编号";
            // 
            // AddPurRecordInfoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(876, 561);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "AddPurRecordInfoForm";
            this.Text = "采购信息选择";
            this.Load += new System.EventHandler(this.suppllierName_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ComboBox cbClassInfo;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtCode;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtFactory;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtPurName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cbSupplierName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox supplierID;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox txtMTID;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox txtGroupName;
    }
}