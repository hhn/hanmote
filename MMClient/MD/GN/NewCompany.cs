﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Bll.MDBll.General;
using Lib.Common.CommonUtils;

namespace MMClient.MD.GN
{
    public partial class NewCompany : Form
    {
        public NewCompany()
        {
            InitializeComponent();
        }
        GeneralBLL gn = new GeneralBLL();
        private void btn_确定_Click(object sender, EventArgs e)
        {
            if (cbb_公司代码.Text == "")
                MessageUtil.ShowError("请输入正确的公司代码");
            else
            {
                List<string> list = gn.GetAllCompany();
                if (gn.IDInTheList(list, cbb_公司代码.Text) == true)
                {
                    string cyid = cbb_公司代码.Text;
                    string cyname = cbb_公司名称.Text;
                    gn.NewCompany(cyid, cyname);
                    MessageUtil.ShowTips("新建公司信息成功");
                    this.Close();
                }
                else
                    MessageUtil.ShowError("公司代码重复，请重新输入");
            }
        }
    }
}
