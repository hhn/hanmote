﻿using Lib.Bll.MDBll.General;
using Lib.Common.CommonUtils;
using Lib.SqlServerDAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MMClient.MD.GN
{
    
    public partial class MDPurAndMtGroup : Form
    {
        GeneralBLL gen = new GeneralBLL();
        private string code_PurGroup1;
        private string name_PurGroup1;
        private string code_MtGroup1;
        private string name_MtGroup1;

        public MDPurAndMtGroup(string code_PurGroup1, string name_PurGroup1,String code_MtGroup1,String name_MtGroup1)
        {
            InitializeComponent();
            this.code_PurGroup1 = code_PurGroup1;
            this.name_PurGroup1 = name_PurGroup1;
            this.code_MtGroup1 = code_MtGroup1;
            this.name_MtGroup1 = name_MtGroup1;
            code_PurGroup.Text = code_PurGroup1;
            name_PurGroup.Text = name_PurGroup1;
            code_MtGroup.Text  = code_MtGroup1;
            name_MtGroup.Text  = name_MtGroup1;
        }
        ///
        public void GroupModify_Load(object sender, EventArgs e)
        {
            List<String> mtID = gen.getALLMtGroupID(code_PurGroup.Text);
            code_MtGroup.DataSource = mtID;
            code_MtGroup.Text = code_MtGroup1;   //设置当前选中
        }

        private void code_MtGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            String sql = "SELECT * FROM [Material_Group] WHERE Material_Group='" + code_MtGroup.Text + "'";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            if (dt.Rows.Count != 0)
            {
                name_MtGroup.Text = dt.Rows[0][2].ToString();
            }
        }

        private void save_MD_Click(object sender, EventArgs e)
        {

            if (gen.MDGroupInfo(code_PurGroup1, name_PurGroup1, code_MtGroup1, name_MtGroup1,code_PurGroup.Text, name_PurGroup.Text, code_MtGroup.Text, name_MtGroup.Text) == true) {
                MessageUtil.ShowTips("修改采购组-物料组信息成功");
                this.Close();
            } 
        }
    }
}
