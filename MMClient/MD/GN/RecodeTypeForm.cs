﻿using Lib.Common.CommonUtils;
using Lib.Common.MMCException.Bll;
using Lib.Common.MMCException.IDAL;
using Lib.SqlServerDAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MMClient.MD.GN
{
    public partial class RecodeTypeForm :  WeifenLuo.WinFormsUI.Docking.DockContent
    
    {
        public RecodeTypeForm()
        {
            InitializeComponent();
        }

        private void btn_刷新数据_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = DBHelper.ExecuteQueryDT("SELECT recodeCode as 记录类型编码,recodeType as 记录类型描述  FROM [RecodeType] ");
                dgv_记录类型.DataSource = dt;
                pageTool_Load(sender, e);
            }
            catch (DBException ex)
            {
                MessageBox.Show("刷新失败");
            }
        }

        private void btn_新条目_Click(object sender, EventArgs e)
        {
            NewRecordTypeForm typeForm = new NewRecordTypeForm();
            typeForm.Show();
        }

        private void btn_删除_Click(object sender, EventArgs e)
        {
            if (dgv_记录类型.CurrentRow == null)
            {
                MessageUtil.ShowError("请选择要删除的行");
            }
            else
            {
                if (MessageUtil.ShowYesNoAndTips("是否确定删除") == DialogResult.Yes)
                {
                    try
                    {
                        string recodeCode = dgv_记录类型.CurrentRow.Cells[0].Value.ToString();
                        DBHelper.ExecuteQueryDS("delete from RecodeType where recodeCode = '"+ recodeCode + "'");
                        dgv_记录类型.Rows.Remove(dgv_记录类型.CurrentRow);
                        MessageUtil.ShowTips("删除成功");
                        pageTool_Load(sender, e);
                    }
                    catch (BllException ex)
                    {
                        MessageUtil.ShowTips("删除失败");
                    }
                }
            }
        }

        private void pageTool_Load(object sender, EventArgs e)
        {
            string sql = "SELECT count(*) FROM [RecodeType]";
            try
            {
                int i = int.Parse(DBHelper.ExecuteQueryDT(sql).Rows[0][0].ToString());
                int totalNum = i;
                pageTool.DrawControl(totalNum);
                //事件改变调用函数
                pageTool.OnPageChanged += pageTool_OnPageChanged;
            }
            catch (Lib.Common.MMCException.Bll.BllException ex)
            {
                MessageUtil.ShowTips("数据库异常，请稍后重试");
                return;
            }
        }
        private void pageTool_OnPageChanged(object sender, EventArgs e)
        {
            LoadData();
        }
        private void LoadData()
        {
            try
            {
                dgv_记录类型.DataSource = findConditionalInforByPage(this.pageTool.PageSize, this.pageTool.PageIndex);
            }
            catch (BllException ex)
            {
                MessageUtil.ShowError(ex.Message);
            }
        }
        private object findConditionalInforByPage(int pageSize, int pageIndex)
        {
            try
            {
                string sql = @"SELECT top " + pageSize + " recodeCode as 记录类型编码,recodeType as 记录类型描述 FROM [RecodeType] where recodeCode not in(select top " + pageSize * (pageIndex - 1) + " recodeCode from RecodeType ORDER BY recodeCode ASC)ORDER BY recodeCode";
                return DBHelper.ExecuteQueryDT(sql);
            }
            catch (DBException ex)
            {
                throw new BllException("当前无数据", ex);
            };
        }

        private void RecodeTypeForm_Load(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = DBHelper.ExecuteQueryDT("SELECT recodeCode as 记录类型编码,recodeType as 记录类型描述  FROM [RecodeType] ");
                dgv_记录类型.DataSource = dt;
            }
            catch (DBException ex)
            {
                MessageBox.Show("加载失败");
            }
        }
    }
}
