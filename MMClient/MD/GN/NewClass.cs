﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Model.MD.MT;
using Lib.SqlServerDAL.MDDAL.MT;
using Lib.Common.CommonUtils;
using Lib.Common.MMCException.Bll;

namespace MMClient.MD.GN
{
    public partial class NewClass : Form
    {
        public NewClass()
        {
            InitializeComponent();
        }
        MaterialClassDAL mcdal = new MaterialClassDAL();
        private void NewClass_Load(object sender, EventArgs e)
        {
           
            dgv_新建特性.Rows.Add(100);
            DateTime dt = DateTime.Now;
            string ds = dt.ToString("yyyyMMddHHmmss");
            double dd = Convert.ToDouble(ds);
            tbx_分类编码.Text = Convert.ToString(dd);
            tbx_分类编码.Enabled = false;

        }

        private void btn_新建_Click(object sender, EventArgs e)
        {

            int countnumber = 0;
            List<Class_Feature> listcf = new List<Class_Feature>();

            for (int i = 0; i < dgv_新建特性.Rows.Count && dgv_新建特性.Rows[i].Cells["cln_特性名称"] != null ; i++)
            {
                
                if(dgv_新建特性.Rows[i].Cells["cln_特性名称"].Value == null)
                {
                    break;
                }
                String name = dgv_新建特性.Rows[i].Cells["cln_特性名称"].Value.ToString();
                string Select_Value = dgv_新建特性.Rows[i].Cells["cln_状态"].EditedFormattedValue.ToString();
                if (Select_Value == "True" && name != "")
                {
                    Class_Feature clfe = new Class_Feature();
                    clfe.Class_ID = tbx_分类编码.Text;
                    clfe.Class_Name = tbx_分类名称.Text;
                    clfe.Feature_ID =Convert.ToString( countnumber);
                    clfe.Feature_Name = name;
                    countnumber++;
                    listcf.Add(clfe);
                }
            }
            try
            {
                mcdal.InsertClassFeature(listcf);
                MessageUtil.ShowTips("新建成功");
                this.Close();
            }
            catch(BllException ex)
            {
                MessageUtil.ShowTips("数据库连接异常，请稍后重试");
            }
          
          


        }
    }
}
