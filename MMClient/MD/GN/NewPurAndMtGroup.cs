﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using Lib.SqlServerDAL;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Bll.MDBll.General;
using Lib.Common.CommonUtils;
using MMClient.SystemConfig.UserManage;
using MMClient.CertificationManagement.CertificationProcessing;

namespace MMClient.MD.GN
{

    public partial class NewPurAndMtGroup : Form
    {
        GeneralBLL gen = new GeneralBLL();
        private UserInfoListForm userInfoListForm;
        string purOrgId = "";
        string purOrgName = "";
        public NewPurAndMtGroup(string purOrgId, string purOrgName)
        {
            InitializeComponent();
            this.purOrgId = purOrgId;
            this.purOrgName = purOrgName;
        }

        private void fillMtGroupDiviedTable(String purName)
        {
            String sql = @"SELECT
                              Material_Group as mtCode,
	                            Description AS Mt_Group_Name
                            FROM
	                            Material_Group";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            this.MtGroupDiviedTable.AutoGenerateColumns = false;
            this.MtGroupDiviedTable.DataSource = dt;
            getHadSelected(dt,purName);
        }

        private void getHadSelected(DataTable dtble, String purName)
        {
            String sql = @"SELECT
	                            sur.Username as Username,
	                            mtsur.Mt_Group_Name as Mt_Group_Name
                            FROM
	                            Hanmote_User_MtGroupName mtsur,
	                            Hanmote_Base_User sur
                            WHERE
	                            mtsur.User_ID = sur.User_ID and mtsur.Type = 1 and Pur_Group_Name = '"+ purName + "'";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);

            sql = @"SELECT MtGroup_ID,isVisible FROM [dbo].[Porg_MtGroup_Relationship] where Porg_ID='" + code_Pur_Organic.Text.ToString()+"'";

            DataTable isVisiDt = DBHelper.ExecuteQueryDT(sql);


            if (dt.Rows.Count>0)
            {
                for(int j = 0; j < dtble.Rows.Count; j++)
                {

                    for (int k = 0; k < isVisiDt.Rows.Count; k++) {
                        if (isVisiDt.Rows[k]["MtGroup_ID"].ToString().Equals(dtble.Rows[j]["mtCode"].ToString())) {
                            MtGroupDiviedTable.Rows[j].Cells["isVisible"].Value = isVisiDt.Rows[k]["isVisible"].ToString();
                            break;
                        }


                    }
                    MtGroupDiviedTable.Rows[j].Cells["isDivied"].Value = "暂无(点击指定主审)";

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        if (dt.Rows[i][1].ToString().Equals(dtble.Rows[j][1].ToString()))
                        {
                            MtGroupDiviedTable.Rows[j].Cells["isDivied"].Value = "已指定负责人：" + dt.Rows[i][0].ToString();
                            MtGroupDiviedTable.Rows[j].Cells["isFinished"].Value = true;
                            break;
                        }
                    }
                }
            }
            else{
                for (int j = 0; j < MtGroupDiviedTable.Rows.Count; j++)
                {
                    MtGroupDiviedTable.Rows[j].Cells["isDivied"].Value = "暂无(点击指定主审)";
                }
            }
        }

        private void code_Pur_Organic_SelectedIndexChanged(object sender, EventArgs e)
        {
            String sql = "SELECT * FROM [Buyer_Org] WHERE Buyer_Org='" + code_Pur_Organic.Text + "'";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            if (dt.Rows.Count != 0) {
                name__Pur_Organic.Text = dt.Rows[0][1].ToString();
            }
            fillMtGroupDiviedTable(name__Pur_Organic.Text.ToString().Trim());
        }

        private void NewPurAndMtGroup_Load(object sender, EventArgs e)
        {
            /* List<String> purID = gen.getALLPurGroupID();
             code_Pur_Organic.DataSource = purID;*/
            code_Pur_Organic.Text = purOrgId;
            name__Pur_Organic.Text = purOrgName;
            fillMtGroupDiviedTable(name__Pur_Organic.Text.ToString().Trim());
        }
        /// <summary>
        /// dgv点击事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MtGroupDiviedTable_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {   //选择主审点击事件
            if (MtGroupDiviedTable.Columns[e.ColumnIndex].Name.Equals("isDivied"))
            {
                //得到当前行
                int currentIndex = getCurrentSelectedRowIndex();
                DataGridViewRow row = this.MtGroupDiviedTable.Rows[currentIndex];
                String isDivied = Convert.ToString(row.Cells["isDivied"].Value);
                //选择主审
                 
                        userInfoListForm = new UserInfoListForm(1,this, e.RowIndex);
                        userInfoListForm.ShowDialog();
            }

            //指定物料组是否显示勾选点击事件
            int isVisible = 0;
            if (MtGroupDiviedTable.Columns[e.ColumnIndex].Name.Equals("isVisible"))
            {

                if (MtGroupDiviedTable.CurrentRow.Cells["isDivied"].Value.ToString().Substring(0, 2).Equals("暂无"))
                {
                    MessageUtil.ShowTips("请先指定评审责任人！");
                    return;

                }
                isVisible = 0;
                if ((bool)MtGroupDiviedTable.CurrentCell.EditedFormattedValue == true)
                {
                    MtGroupDiviedTable.CurrentCell.Value = false;
                    isVisible = 1;
                }
                else {
                    MtGroupDiviedTable.CurrentCell.Value = true;
                    
                }

                if (MessageUtil.ShowOKCancelAndQuestion("是否保存更改！") == DialogResult.OK)
                {

                    DBHelper.ExecuteNonQuery("UPDATE Porg_MtGroup_Relationship set isVisible='" + isVisible + "' where Porg_ID='" + code_Pur_Organic.Text + "' and MtGroup_ID='" + MtGroupDiviedTable.CurrentRow.Cells["mtCode"].Value.ToString() + "'");
                }
                else {

                    if ((bool)MtGroupDiviedTable.CurrentCell.EditedFormattedValue == true)
                    {
                        MtGroupDiviedTable.CurrentCell.Value = false;
                     
                    }
                    else
                    {
                        MtGroupDiviedTable.CurrentCell.Value = true;

                    }

                }

            }
            //上传文件事件
            if (MtGroupDiviedTable.Columns[e.ColumnIndex].Name.Equals("btn_fileUpLoad")) {



                Fupload fuploadForm = new Fupload(this.name__Pur_Organic.Text,MtGroupDiviedTable.Rows[e.RowIndex].Cells["Mt_Group_Name"].Value.ToString());
                fuploadForm.Show();


            }

        }

        private int getCurrentSelectedRowIndex()
        {
            if (this.MtGroupDiviedTable.Rows.Count <= 0)
            {
                MessageUtil.ShowWarning("用户表为空，无法执行操作");
                return -1;
            }

            if (this.MtGroupDiviedTable.CurrentRow.Index < 0)
            {
                MessageUtil.ShowWarning("请选择一行记录，然后进行操作！");
                return -1;
            }

            return this.MtGroupDiviedTable.CurrentRow.Index;
        }

        private void save_Mt_group_Click(object sender, EventArgs e)
        {
            bool flag = false;
            int isVisible = 1;
            for (int j = 0; j < MtGroupDiviedTable.Rows.Count; j++)
            {
                
                DataGridViewRow row = this.MtGroupDiviedTable.Rows[j];
                String Username = Convert.ToString(row.Cells["isDivied"].Value);
                if (!Username.Equals("暂无(点击指定主审)"))
                {
                    flag = true;
                }
            }
                //添加物料组负责人
           if (!flag)
            {
                MessageUtil.ShowTips("请指定物料组负责人！");
                return;
            }
            bool isAdd = false;
            for (int j = 0; j < MtGroupDiviedTable.Rows.Count; j++)
            {
                isVisible = 1;
                DataGridViewRow row = this.MtGroupDiviedTable.Rows[j];
                String isDivied = Convert.ToString(row.Cells["isDivied"].Value);
                String Mt_Group_Name = Convert.ToString(row.Cells["Mt_Group_Name"].Value);
                String mtCode = Convert.ToString(row.Cells["mtCode"].Value);
                String isDivied2 = isDivied.Substring(0,1);
                if ((bool)row.Cells["isVisible"].EditedFormattedValue == true)
                {
                    isVisible = 0;

                }
                if (!isDivied.Equals("暂无(点击指定主审)")&&isDivied2.Equals("新"))
                {
                    isAdd =  gen.newPorgAndMtGroup(code_Pur_Organic.Text, name__Pur_Organic.Text, mtCode, Mt_Group_Name, textBox1.Text, isVisible);
                }


            }
            if (isAdd)
            {
                MessageUtil.ShowTips("新建采购组-物料组信息成功");
                this.Close();
            }
            else {
                if (MessageUtil.ShowOKCancelAndQuestion("没有任何更改！点击确认按钮退出") == DialogResult.OK) {

                    this.Close();

                }
                

            }

        }
        /// <summary>
        /// 添加表格行
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MtGroupDiviedTable_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            Rectangle rectangle = new Rectangle(e.RowBounds.Location.X,
             e.RowBounds.Location.Y,
             MtGroupDiviedTable.RowHeadersWidth - 4, e.RowBounds.Height);
            TextRenderer.DrawText(e.Graphics,
                  (e.RowIndex + 1).ToString(),
                   MtGroupDiviedTable.RowHeadersDefaultCellStyle.Font,
                   rectangle,
                   MtGroupDiviedTable.RowHeadersDefaultCellStyle.ForeColor,
                   TextFormatFlags.VerticalCenter | TextFormatFlags.Right);
        }

        private void 删除ToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }
    }
}
