﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.SqlServerDAL;
using Lib.Common.CommonUtils;

namespace MMClient.MD.GN
{
    public partial class NewIndustryCategory : Form
    {
        public NewIndustryCategory()
        {
            InitializeComponent();
        }

        private void btn_确定_Click(object sender, EventArgs e)
        {
            try
            {
                string sql = "INSERT INTO [Industry_Category] VALUES('" + cbb_工业标识代码.Text + "','" + cbb_工业标识名称.Text + "')";
                DBHelper.ExecuteNonQuery(sql);
                MessageUtil.ShowTips("新建工业标识成功");
                this.Close();
            }
            catch (Exception ex)
            {
                MessageUtil.ShowError("工业标识重复");
            }

        }
    }
}
