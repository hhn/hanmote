﻿namespace MMClient.MD.GN
{
    partial class MDPurAndMtGroup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.code_PurGroup = new System.Windows.Forms.TextBox();
            this.name_PurGroup = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.code_MtGroup = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.save_MD = new System.Windows.Forms.Button();
            this.name_MtGroup = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // code_PurGroup
            // 
            this.code_PurGroup.Location = new System.Drawing.Point(122, 76);
            this.code_PurGroup.Name = "code_PurGroup";
            this.code_PurGroup.ReadOnly = true;
            this.code_PurGroup.Size = new System.Drawing.Size(138, 21);
            this.code_PurGroup.TabIndex = 0;
           // this.code_PurGroup.TextChanged += new System.EventHandler(this.code_PurGroup_TextChanged);
            // 
            // name_PurGroup
            // 
            this.name_PurGroup.Location = new System.Drawing.Point(126, 130);
            this.name_PurGroup.Name = "name_PurGroup";
            this.name_PurGroup.ReadOnly = true;
            this.name_PurGroup.Size = new System.Drawing.Size(138, 21);
            this.name_PurGroup.TabIndex = 1;
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(51, 79);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(65, 12);
            this.label1.TabIndex = 2;
            this.label1.Text = "采购组ID：";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(43, 133);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 12);
            this.label2.TabIndex = 3;
            this.label2.Text = "采购组名称：";
            // 
            // code_MtGroup
            // 
            this.code_MtGroup.FormattingEnabled = true;
            this.code_MtGroup.Location = new System.Drawing.Point(397, 81);
            this.code_MtGroup.Name = "code_MtGroup";
            this.code_MtGroup.Size = new System.Drawing.Size(138, 20);
            this.code_MtGroup.TabIndex = 4;
            this.code_MtGroup.SelectedIndexChanged += new System.EventHandler(this.code_MtGroup_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(326, 84);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 12);
            this.label3.TabIndex = 6;
            this.label3.Text = "物料组ID：";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(315, 134);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(77, 12);
            this.label4.TabIndex = 7;
            this.label4.Text = "物料组名称：";
            // 
            // save_MD
            // 
            this.save_MD.Location = new System.Drawing.Point(244, 238);
            this.save_MD.Name = "save_MD";
            this.save_MD.Size = new System.Drawing.Size(117, 38);
            this.save_MD.TabIndex = 8;
            this.save_MD.Text = "保存";
            this.save_MD.UseVisualStyleBackColor = true;
            this.save_MD.Click += new System.EventHandler(this.save_MD_Click);
            // 
            // name_MtGroup
            // 
            this.name_MtGroup.Location = new System.Drawing.Point(397, 133);
            this.name_MtGroup.Name = "name_MtGroup";
            this.name_MtGroup.ReadOnly = true;
            this.name_MtGroup.Size = new System.Drawing.Size(138, 21);
            this.name_MtGroup.TabIndex = 9;
            // 
            // MDPurAndMtGroup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(607, 313);
            this.Controls.Add(this.name_MtGroup);
            this.Controls.Add(this.save_MD);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.code_MtGroup);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.name_PurGroup);
            this.Controls.Add(this.code_PurGroup);
            this.Name = "MDPurAndMtGroup";
            this.Text = "修改分配物料组信息";
            this.Load += new System.EventHandler(this.GroupModify_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private void MDPurAndMtGroup_Load(object sender, System.EventArgs e)
        {
            throw new System.NotImplementedException();
        }

        #endregion

        private System.Windows.Forms.TextBox code_PurGroup;
        private System.Windows.Forms.TextBox name_PurGroup;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox code_MtGroup;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button save_MD;
        private System.Windows.Forms.TextBox name_MtGroup;
    }
}