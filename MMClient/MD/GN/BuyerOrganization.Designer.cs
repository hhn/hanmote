﻿namespace MMClient.MD.GN
{
    partial class BuyerOrganization
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel1 = new System.Windows.Forms.Panel();
            this.LB_orgID = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.mtGpCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mtGpName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.评审负责人 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.button1 = new System.Windows.Forms.Button();
            this.btn_删除 = new System.Windows.Forms.Button();
            this.btn_新条目 = new System.Windows.Forms.Button();
            this.btn_刷新 = new System.Windows.Forms.Button();
            this.dgv_采购组织 = new System.Windows.Forms.DataGridView();
            this.purOrgId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.purOrgName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.分配物料组ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnShow = new System.Windows.Forms.DataGridViewButtonColumn();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_采购组织)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(13, 13);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1463, 600);
            this.panel1.TabIndex = 0;
            // 
            // LB_orgID
            // 
            this.LB_orgID.AutoSize = true;
            this.LB_orgID.Location = new System.Drawing.Point(635, 28);
            this.LB_orgID.Name = "LB_orgID";
            this.LB_orgID.Size = new System.Drawing.Size(79, 13);
            this.LB_orgID.TabIndex = 8;
            this.LB_orgID.Text = "采购组织代码";
            this.LB_orgID.Visible = false;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.mtGpCode,
            this.mtGpName,
            this.评审负责人});
            this.dataGridView1.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dataGridView1.Location = new System.Drawing.Point(493, 56);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.Size = new System.Drawing.Size(1082, 603);
            this.dataGridView1.TabIndex = 7;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // mtGpCode
            // 
            this.mtGpCode.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.mtGpCode.DataPropertyName = "物料组代码";
            this.mtGpCode.FillWeight = 44.43293F;
            this.mtGpCode.HeaderText = "物料组代码";
            this.mtGpCode.Name = "mtGpCode";
            this.mtGpCode.Width = 92;
            // 
            // mtGpName
            // 
            this.mtGpName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.mtGpName.DataPropertyName = "物料组名称";
            this.mtGpName.FillWeight = 6.475525F;
            this.mtGpName.HeaderText = "物料组名称";
            this.mtGpName.Name = "mtGpName";
            this.mtGpName.Width = 92;
            // 
            // 评审负责人
            // 
            this.评审负责人.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.评审负责人.DataPropertyName = "评审负责人";
            this.评审负责人.FillWeight = 13.11991F;
            this.评审负责人.HeaderText = "评审负责人";
            this.评审负责人.Name = "评审负责人";
            this.评审负责人.Width = 92;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(442, 21);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(92, 29);
            this.button1.TabIndex = 6;
            this.button1.Text = "分配物料组";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btn_删除
            // 
            this.btn_删除.Location = new System.Drawing.Point(171, 21);
            this.btn_删除.Name = "btn_删除";
            this.btn_删除.Size = new System.Drawing.Size(92, 27);
            this.btn_删除.TabIndex = 5;
            this.btn_删除.Text = "删除";
            this.btn_删除.UseVisualStyleBackColor = true;
            this.btn_删除.Visible = false;
            // 
            // btn_新条目
            // 
            this.btn_新条目.Location = new System.Drawing.Point(38, 21);
            this.btn_新条目.Name = "btn_新条目";
            this.btn_新条目.Size = new System.Drawing.Size(92, 28);
            this.btn_新条目.TabIndex = 4;
            this.btn_新条目.Text = "新条目";
            this.btn_新条目.UseVisualStyleBackColor = true;
            this.btn_新条目.Visible = false;
            // 
            // btn_刷新
            // 
            this.btn_刷新.Location = new System.Drawing.Point(306, 21);
            this.btn_刷新.Name = "btn_刷新";
            this.btn_刷新.Size = new System.Drawing.Size(92, 27);
            this.btn_刷新.TabIndex = 3;
            this.btn_刷新.Text = "刷新数据";
            this.btn_刷新.UseVisualStyleBackColor = true;
            this.btn_刷新.Visible = false;
            // 
            // dgv_采购组织
            // 
            this.dgv_采购组织.AllowUserToAddRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.dgv_采购组织.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv_采购组织.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.dgv_采购组织.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_采购组织.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dgv_采购组织.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_采购组织.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.purOrgId,
            this.purOrgName,
            this.btnShow});
            this.dgv_采购组织.Location = new System.Drawing.Point(38, 56);
            this.dgv_采购组织.Name = "dgv_采购组织";
            this.dgv_采购组织.RowTemplate.Height = 24;
            this.dgv_采购组织.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv_采购组织.Size = new System.Drawing.Size(449, 603);
            this.dgv_采购组织.TabIndex = 0;
            this.dgv_采购组织.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_采购组织_CellContentClick);
            // 
            // purOrgId
            // 
            this.purOrgId.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.purOrgId.DataPropertyName = "采购组织代码";
            this.purOrgId.FillWeight = 1F;
            this.purOrgId.HeaderText = "采购组织代码";
            this.purOrgId.Name = "purOrgId";
            this.purOrgId.Width = 150;
            // 
            // purOrgName
            // 
            this.purOrgName.ContextMenuStrip = this.contextMenuStrip1;
            this.purOrgName.DataPropertyName = "采购组织名称";
            this.purOrgName.FillWeight = 1F;
            this.purOrgName.HeaderText = "采购组织名称";
            this.purOrgName.Name = "purOrgName";
            this.purOrgName.ReadOnly = true;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.分配物料组ToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(137, 26);
            // 
            // 分配物料组ToolStripMenuItem
            // 
            this.分配物料组ToolStripMenuItem.Name = "分配物料组ToolStripMenuItem";
            this.分配物料组ToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
            this.分配物料组ToolStripMenuItem.Text = "分配物料组";
            // 
            // btnShow
            // 
            this.btnShow.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.btnShow.FillWeight = 1F;
            this.btnShow.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnShow.HeaderText = "详情查看";
            this.btnShow.Name = "btnShow";
            this.btnShow.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.btnShow.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.btnShow.Text = "查看详情";
            this.btnShow.ToolTipText = "查看详情";
            this.btnShow.UseColumnTextForButtonValue = true;
            this.btnShow.Width = 150;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.LB_orgID);
            this.groupBox1.Controls.Add(this.dataGridView1);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.btn_删除);
            this.groupBox1.Controls.Add(this.btn_新条目);
            this.groupBox1.Controls.Add(this.btn_刷新);
            this.groupBox1.Controls.Add(this.dgv_采购组织);
            this.groupBox1.Location = new System.Drawing.Point(12, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1581, 665);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "维护采购组织";
            // 
            // BuyerOrganization
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1605, 669);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "BuyerOrganization";
            this.Text = "维护采购组织";
            this.Load += new System.EventHandler(this.BuyerOrganization_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_采购组织)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label LB_orgID;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn mtGpCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn mtGpName;
        private System.Windows.Forms.DataGridViewTextBoxColumn 评审负责人;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btn_删除;
        private System.Windows.Forms.Button btn_新条目;
        private System.Windows.Forms.Button btn_刷新;
        private System.Windows.Forms.DataGridView dgv_采购组织;
        private System.Windows.Forms.DataGridViewTextBoxColumn purOrgId;
        private System.Windows.Forms.DataGridViewTextBoxColumn purOrgName;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 分配物料组ToolStripMenuItem;
        private System.Windows.Forms.DataGridViewButtonColumn btnShow;
        private System.Windows.Forms.GroupBox groupBox1;
    }
}