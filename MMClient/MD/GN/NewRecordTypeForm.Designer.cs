﻿namespace MMClient.MD.GN
{
    partial class NewRecordTypeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.btn_确定 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.typeCode = new System.Windows.Forms.TextBox();
            this.typeName = new System.Windows.Forms.TextBox();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.typeName);
            this.panel1.Controls.Add(this.typeCode);
            this.panel1.Controls.Add(this.btn_确定);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(26, 26);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(382, 242);
            this.panel1.TabIndex = 0;
            // 
            // btn_确定
            // 
            this.btn_确定.Location = new System.Drawing.Point(151, 163);
            this.btn_确定.Margin = new System.Windows.Forms.Padding(2);
            this.btn_确定.Name = "btn_确定";
            this.btn_确定.Size = new System.Drawing.Size(56, 34);
            this.btn_确定.TabIndex = 45;
            this.btn_确定.Text = "保存";
            this.btn_确定.UseVisualStyleBackColor = true;
            this.btn_确定.Click += new System.EventHandler(this.btn_确定_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(41, 98);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 12);
            this.label2.TabIndex = 42;
            this.label2.Text = "记录类型名称";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(41, 35);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 12);
            this.label1.TabIndex = 41;
            this.label1.Text = "记录类型编码";
            // 
            // typeCode
            // 
            this.typeCode.Location = new System.Drawing.Point(151, 32);
            this.typeCode.Name = "typeCode";
            this.typeCode.Size = new System.Drawing.Size(144, 21);
            this.typeCode.TabIndex = 46;
            // 
            // typeName
            // 
            this.typeName.Location = new System.Drawing.Point(151, 89);
            this.typeName.Name = "typeName";
            this.typeName.Size = new System.Drawing.Size(144, 21);
            this.typeName.TabIndex = 47;
            // 
            // NewRecordTypeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(452, 303);
            this.Controls.Add(this.panel1);
            this.Name = "NewRecordTypeForm";
            this.Text = "NewRecordTypeForm";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox typeName;
        private System.Windows.Forms.TextBox typeCode;
        private System.Windows.Forms.Button btn_确定;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
    }
}