﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Common.CommonUtils;
using Lib.Bll.MDBll.General;
using MMClient.SystemConfig.UserManage;

namespace MMClient.MD.GN
{
    public partial class BuyerOrganization :WeifenLuo.WinFormsUI.Docking.DockContent
    {
        AddSysUserForm showUserInfo = null;
        public BuyerOrganization()
        {
            
            InitializeComponent();
        }
        GeneralBLL gn = new GeneralBLL();
        private void BuyerOrganization_Load(object sender, EventArgs e)
        {
            DataTable dt = gn.GetAllBuyerOrganizationInformation();
            dgv_采购组织.DataSource = null;
            dgv_采购组织.DataSource = dt;
        }

        private void btn_刷新_Click(object sender, EventArgs e)
        {
            DataTable dt = gn.GetAllBuyerOrganizationInformation();
            dgv_采购组织.DataSource = dt;
        }

        private void btn_新条目_Click(object sender, EventArgs e)
        {
            NewBuyerOrganization nbo = new NewBuyerOrganization();
            nbo.Show();
        }

        private void btn_删除_Click(object sender, EventArgs e)
        {
            if (dgv_采购组织.CurrentRow == null)
                MessageUtil.ShowError("请选择要删除的行");
            else
            {
                if (MessageUtil.ShowYesNoAndTips("是否确定删除") == DialogResult.Yes)
                {
                    string boid = dgv_采购组织.CurrentRow.Cells[0].Value.ToString();
                    gn.DeleteBuyerOrganization(boid);
                    dgv_采购组织.Rows.Remove(dgv_采购组织.CurrentRow);
                    MessageUtil.ShowTips("删除成功");
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //采购组织代码
           string purOrgId= dgv_采购组织.CurrentRow.Cells["purOrgId"].Value.ToString();
            //采购组织名称
            string purOrgName= dgv_采购组织.CurrentRow.Cells["purOrgName"].Value.ToString();
            NewPurAndMtGroup gd = new NewPurAndMtGroup(purOrgId, purOrgName);
            gd.Show();
           

        }

        private void dgv_采购组织_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgv_采购组织.Columns[e.ColumnIndex].Name.Equals("btnShow"))
            {
                //点击按钮操作
                
                string purOrgId =dgv_采购组织.CurrentRow.Cells["purOrgName"].Value.ToString();
                this.LB_orgID.Text = purOrgId;
                Dictionary<string, object> datas = gn.getMtGroupAndMasterRelation(purOrgId);
                if (datas == null) {
                    dataGridView1.DataSource = null;
                    MessageUtil.ShowTips("采购组织暂无数据");
                    return;
                }
                this.dataGridView1.AutoGenerateColumns = false;
                dataGridView1.Columns.Clear();
                dataGridView1.DataSource = null;
                dataGridView1.Columns.AddRange(this.mtGpCode);
                dataGridView1.Columns.AddRange(this.mtGpName);
                dataGridView1.Columns.AddRange(this.评审负责人);


                dataGridView1.DataSource = datas["dataTable"];
                Dictionary<string, Dictionary<string, List<string>>> person = (Dictionary<string, Dictionary<string, List<string>>>)datas["dc"];


                //评审员数据
                foreach (string key in person[dataGridView1.Rows[0].Cells[1].Value.ToString()].Keys)
                {
                    DataGridViewComboBoxColumn Column = new DataGridViewComboBoxColumn();

                    Dictionary<string, string> temp = new Dictionary<string, string>();

                    Column.HeaderText = key;
                    Column.Name = key;
                    Column.DefaultCellStyle.BackColor = System.Drawing.Color.White;
                    Column.DisplayStyle = DataGridViewComboBoxDisplayStyle.DropDownButton;
                    Column.Width = 150;
                    dataGridView1.Columns.AddRange(Column);
                }

                //直属领导
                Dictionary<string, string> leader = (Dictionary<string, string>)datas["leader"];
                foreach (string key in leader.Keys)
                {
                   
                    DataGridViewTextBoxColumn Column = new DataGridViewTextBoxColumn();
               
                    Column.HeaderText = key;
                    Column.Name = key;
                  
                    dataGridView1.Columns.AddRange(Column);

                }


                for (int i = 0; i < dataGridView1.Rows.Count; i++) {

                    Dictionary<string, List<string>> tempData = person[dataGridView1.Rows[i].Cells[1].Value.ToString()];
                    foreach (string key in tempData.Keys) {

                        DataTable dt = new DataTable();
                        DataColumn dcCombo = new DataColumn("item");
                        dt.Columns.Add(dcCombo);
                        for (int j = 0; j < tempData[key].Count; j++)
                        {
                            DataRow dr = dt.NewRow();
                            dt.Rows.Add(dr);
                            dt.Rows[j]["item"] = tempData[key][j].ToString();
                        }
                        ((DataGridViewComboBoxCell)dataGridView1.Rows[i].Cells[key]).DataSource = dt;
                        ((DataGridViewComboBoxCell)dataGridView1.Rows[i].Cells[key]).ValueMember = "item";
                        ((DataGridViewComboBoxCell)dataGridView1.Rows[i].Cells[key]).DisplayMember = "item";
                        ((DataGridViewComboBoxCell)dataGridView1.Rows[i].Cells[key]).Value = dt.Rows[0][0];

                    }


                    foreach (string key in leader.Keys) {

                        dataGridView1.Rows[i].Cells[key].Value=leader[key];

                    }
                }

            }


        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {


            DataGridViewComboBoxCell a = new DataGridViewComboBoxCell();
            string userid = "";
            if (dataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex].GetType() == a.GetType())
            {

                userid = ((DataGridViewComboBoxCell)dataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex]).FormattedValue.ToString().Trim();

            }
            else {
                userid = dataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString();
            }

            if (userid.Equals("暂无评审员")) {
                int indexNum = this.dataGridView1.CurrentCell.ColumnIndex;
                string ColumnText = this.dataGridView1.Columns[indexNum].HeaderText.ToString();

                if (MessageUtil.ShowOKCancelAndQuestion("是否指定评审员！") == DialogResult.OK) {
                    selectJudgePerson selectJudgePerson = new selectJudgePerson(this.LB_orgID.Text, dataGridView1.CurrentRow.Cells["mtGpName"].Value.ToString(), ColumnText);
                    selectJudgePerson.ShowDialog();
                }
                return;
            }

            if (e.ColumnIndex > 1) {
                showUserInfo = new AddSysUserForm(userid, 2);
                showUserInfo.ShowDialog();

            }


        }
        /// <summary>
        /// 分配物料组
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void 分配物料组ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            button1_Click( sender, e);
        }

        
    }
}
