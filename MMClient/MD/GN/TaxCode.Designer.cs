﻿namespace MMClient.MD.GN
{
    partial class TaxCode
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.pageTool = new pager.pagetool.pageNext();
            this.dgv_税码 = new System.Windows.Forms.DataGridView();
            this.税率代码 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.税率值 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.税率说明 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btn_删除 = new System.Windows.Forms.Button();
            this.btn_新条目 = new System.Windows.Forms.Button();
            this.btn_刷新 = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_税码)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.pageTool);
            this.groupBox1.Controls.Add(this.dgv_税码);
            this.groupBox1.Controls.Add(this.btn_删除);
            this.groupBox1.Controls.Add(this.btn_新条目);
            this.groupBox1.Controls.Add(this.btn_刷新);
            this.groupBox1.Location = new System.Drawing.Point(2, 9);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox1.Size = new System.Drawing.Size(1315, 583);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "维护税码";
            // 
            // pageTool
            // 
            this.pageTool.Location = new System.Drawing.Point(295, 507);
            this.pageTool.Name = "pageTool";
            this.pageTool.PageIndex = 1;
            this.pageTool.PageSize = 100;
            this.pageTool.RecordCount = 0;
            this.pageTool.Size = new System.Drawing.Size(696, 37);
            this.pageTool.TabIndex = 7;
            this.pageTool.OnPageChanged += new System.EventHandler(this.pageTool_OnPageChanged);
            this.pageTool.Load += new System.EventHandler(this.pageNext1_Load);
            // 
            // dgv_税码
            // 
            this.dgv_税码.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgv_税码.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.dgv_税码.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgv_税码.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_税码.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.税率代码,
            this.税率值,
            this.税率说明});
            this.dgv_税码.Location = new System.Drawing.Point(170, 18);
            this.dgv_税码.Name = "dgv_税码";
            this.dgv_税码.RowTemplate.Height = 23;
            this.dgv_税码.Size = new System.Drawing.Size(1067, 483);
            this.dgv_税码.TabIndex = 6;
            // 
            // 税率代码
            // 
            this.税率代码.DataPropertyName = "Tax_Code";
            this.税率代码.HeaderText = "税率代码";
            this.税率代码.Name = "税率代码";
            this.税率代码.Width = 200;
            // 
            // 税率值
            // 
            this.税率值.DataPropertyName = "Tax";
            this.税率值.HeaderText = "税率值";
            this.税率值.Name = "税率值";
            this.税率值.Width = 200;
            // 
            // 税率说明
            // 
            this.税率说明.DataPropertyName = "Description";
            this.税率说明.HeaderText = "税种";
            this.税率说明.Name = "税率说明";
            this.税率说明.Width = 250;
            // 
            // btn_删除
            // 
            this.btn_删除.Location = new System.Drawing.Point(27, 302);
            this.btn_删除.Margin = new System.Windows.Forms.Padding(2);
            this.btn_删除.Name = "btn_删除";
            this.btn_删除.Size = new System.Drawing.Size(109, 48);
            this.btn_删除.TabIndex = 5;
            this.btn_删除.Text = "删除";
            this.btn_删除.UseVisualStyleBackColor = true;
            this.btn_删除.Click += new System.EventHandler(this.btn_删除_Click);
            // 
            // btn_新条目
            // 
            this.btn_新条目.Location = new System.Drawing.Point(27, 217);
            this.btn_新条目.Margin = new System.Windows.Forms.Padding(2);
            this.btn_新条目.Name = "btn_新条目";
            this.btn_新条目.Size = new System.Drawing.Size(109, 48);
            this.btn_新条目.TabIndex = 4;
            this.btn_新条目.Text = "新条目";
            this.btn_新条目.UseVisualStyleBackColor = true;
            this.btn_新条目.Click += new System.EventHandler(this.btn_新条目_Click);
            // 
            // btn_刷新
            // 
            this.btn_刷新.Location = new System.Drawing.Point(27, 131);
            this.btn_刷新.Margin = new System.Windows.Forms.Padding(2);
            this.btn_刷新.Name = "btn_刷新";
            this.btn_刷新.Size = new System.Drawing.Size(109, 48);
            this.btn_刷新.TabIndex = 3;
            this.btn_刷新.Text = "刷新";
            this.btn_刷新.UseVisualStyleBackColor = true;
            this.btn_刷新.Click += new System.EventHandler(this.btn_刷新_Click);
            // 
            // TaxCode
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1346, 697);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "TaxCode";
            this.Text = "维护税码";
            this.Load += new System.EventHandler(this.TaxCode_Load);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_税码)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btn_删除;
        private System.Windows.Forms.Button btn_新条目;
        private System.Windows.Forms.Button btn_刷新;
        private System.Windows.Forms.DataGridView dgv_税码;
        private System.Windows.Forms.DataGridViewTextBoxColumn 税率代码;
        private System.Windows.Forms.DataGridViewTextBoxColumn 税率值;
        private System.Windows.Forms.DataGridViewTextBoxColumn 税率说明;
        private pager.pagetool.pageNext pageTool;
    }
}