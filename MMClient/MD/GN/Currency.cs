﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.SqlServerDAL;
using Lib.Bll;
using Lib.Common.MMCException.IDAL;
using Lib.Common.CommonUtils;
using Lib.Common.MMCException.Bll;

namespace MMClient.MD.GN
{
    public partial class Currency : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        public Currency()
        {
            InitializeComponent();
        }

        private void Currency_Load(object sender, EventArgs e)
        {
            try
            {
                string sql = " SELECT Currency_ID as '货币编码',Currency_Name as '货币名称',Currency_ENG as '英文缩写',Currency_LOGO as '货币标识' FROM [Currency] ";
                DataTable dt = DBHelper.ExecuteQueryDT(sql);
                dgv_货币信息.DataSource = dt;
            }catch(DBException ex)
            {
                MessageBox.Show("加载失败");
            }

        }

        private void btn_新条目_Click(object sender, EventArgs e)
        {
            NewCurrency ncy = new NewCurrency();
            ncy.Show();
        }

        private void btn_刷新_Click(object sender, EventArgs e)
        {
            try
            {
                string sql = " SELECT Currency_ID as '货币编码',Currency_Name as '货币名称',Currency_ENG as '英文缩写',Currency_LOGO as '货币标识' FROM [Currency] ";
                DataTable dt = DBHelper.ExecuteQueryDT(sql);
                dgv_货币信息.DataSource = dt;
                pageTool_Load(sender,e);
            }
            catch(DBException ex)
            {
                MessageBox.Show("刷新失败");
            }
        }

        private void btn_删除_Click(object sender, EventArgs e)
        {
            try
            {
                string code = dgv_货币信息.CurrentRow.Cells[0].Value.ToString();
                string sql = " DELETE FROM [Currency] WHERE Currency_ID='" + code + "'";
                DBHelper.ExecuteNonQuery(sql);
                LoadData();    
                MessageBox.Show("删除成功");
                pageTool_Load(sender, e);
            }
            catch(Exception ex)
            {
                MessageBox.Show("删除失败");
            }
        }

        private void pageTool_Load(object sender, EventArgs e)
        {
            string sql = "SELECT count(*) from [Currency]";
            try
            {
                int i = int.Parse(DBHelper.ExecuteQueryDT(sql).Rows[0][0].ToString());
                int totalNum = i;
                pageTool.DrawControl(totalNum);
                //事件改变调用函数
                pageTool.OnPageChanged += pageTool_OnPageChanged;
            }
            catch (Lib.Common.MMCException.Bll.BllException ex)
            {
                MessageUtil.ShowTips("数据库异常，请稍后重试");
                return;
            }
        }
        private void pageTool_OnPageChanged(object sender, EventArgs e)
        {
            LoadData();
        }
        private void LoadData()
        {
            try
            {
                dgv_货币信息.DataSource = findConditionalInforByPage(this.pageTool.PageSize, this.pageTool.PageIndex);
            }
            catch (BllException ex)
            {
                MessageUtil.ShowError(ex.Message);
            }
        }
        private object findConditionalInforByPage(int pageSize, int pageIndex)
        {
            try
            {
                string sql = @"SELECT top " + pageSize + " Currency_ID,Currency_Name,Currency_Eng,Currency_Logo from Currency  where Currency_ID not in(select top " + pageSize * (pageIndex - 1) + " Currency_ID from Currency ORDER BY Currency_ID ASC)ORDER BY Currency_ID";
                return DBHelper.ExecuteQueryDT(sql);
            }
            catch (DBException ex)
            {
                throw new BllException("当前无数据", ex);
            };
        }
    }
}
