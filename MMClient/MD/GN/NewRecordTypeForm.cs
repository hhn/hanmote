﻿using Lib.Common.MMCException.IDAL;
using Lib.SqlServerDAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MMClient.MD.GN
{
    public partial class NewRecordTypeForm : Form
    {
        public NewRecordTypeForm()
        {
            InitializeComponent();
        }

        private void btn_确定_Click(object sender, EventArgs e)
        {
            if (typeCode.Text.ToString().Trim() != "" && typeName.Text.ToString().Trim() != "")
            {
                try
                {
                    DBHelper.ExecuteQueryDS("INSERT INTO RecodeType VALUES ('"+ typeName.Text.ToString().Trim() + "', '"+ typeCode.Text.ToString().Trim()+ "')");
                    MessageBox.Show("保存成功");
                    this.Close();
                }catch(DBException ex)
                {
                    MessageBox.Show("保存失败");
                }
            }
            else
            {
                MessageBox.Show("请将信息填写完整后点击保存");
            }

        }
    }
}
