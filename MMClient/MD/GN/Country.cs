﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.SqlServerDAL;
using Lib.Bll;
using Lib.Common.CommonUtils;
using Lib.Common.MMCException.Bll;
using Lib.Common.MMCException.IDAL;

namespace MMClient.MD.GN
{
    public partial class Country : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        public Country()
        {
            InitializeComponent();
        }

        private void Country_Load(object sender, EventArgs e)
        {
            string sql = " SELECT Country_ID,Country_Name,Country_Eng from Country";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            dgv_国家信息.DataSource = dt;
        }

        private void btn_新条目_Click(object sender, EventArgs e)
        {
            NewCountry ncy = new NewCountry();
            ncy.Show();

        }

        private void btn_刷新_Click(object sender, EventArgs e)
        {
            string sql = "SELECT Country_ID,Country_Name,Country_Eng from Country";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            dgv_国家信息.DataSource = dt;
            pageTool_Load(sender, e);
        }

        private void btn_删除_Click(object sender, EventArgs e)
        {
            try
            {
                dgv_国家信息.Rows.Remove(dgv_国家信息.SelectedRows[0]);
            }
            catch (Exception ex)
            {
                MessageBox.Show("无删除项");
                return;
            }

            string code = dgv_国家信息.CurrentRow.Cells[0].Value.ToString();
            string sql = " DELETE FROM [Country] WHERE Country_ID='" + code + "'";
            DBHelper.ExecuteNonQuery(sql);
            pageTool_Load(sender, e);
        }

        private void pageTool_Load(object sender, EventArgs e)
        {
            string sql = "SELECT count(*) from Country";
            try
            {
                int i = int.Parse(DBHelper.ExecuteQueryDT(sql).Rows[0][0].ToString());
                int totalNum = i;
                pageTool.DrawControl(totalNum);
                //事件改变调用函数
                pageTool.OnPageChanged += pageTool_OnPageChanged;
            }
            catch (Lib.Common.MMCException.Bll.BllException ex)
            {
                MessageUtil.ShowTips("数据库异常，请稍后重试");
                return;
            }
        }

        private void pageTool_OnPageChanged(object sender, EventArgs e)
        {
            LoadData();
        }
        private void LoadData()
        {
            try
            {
                dgv_国家信息.DataSource = FindCountryInforByPage(this.pageTool.PageSize, this.pageTool.PageIndex);
            }
            catch (BllException ex)
            {
                MessageUtil.ShowError(ex.Message);
            }
        }

        private object FindCountryInforByPage(int pageSize, int pageIndex)
        {
            try
            {
                string sql = @"SELECT top " + pageSize + "  Country_ID,Country_Name,Country_Eng  FROM Country   where Country_ID not in(select top " + pageSize * (pageIndex - 1) + " Country_ID from Country ORDER BY Country_ID ASC)ORDER BY Country_ID";
                return DBHelper.ExecuteQueryDT(sql);
            }
            catch (DBException ex)
            {
                throw new BllException("当前无数据", ex);
            }
        }
    }
}
