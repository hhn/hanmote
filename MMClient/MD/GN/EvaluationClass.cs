﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Bll.MDBll.General;
using Lib.Common.CommonUtils;
using Lib.Common.MMCException.Bll;
using Lib.Common.MMCException.IDAL;
using Lib.SqlServerDAL;

namespace MMClient.MD.GN
{
    public partial class EvaluationClass : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        public EvaluationClass()
        {
            InitializeComponent();
        }
        GeneralBLL gn = new GeneralBLL();

        private void EvaluationClass_Load(object sender, EventArgs e)
        {
            DataTable dt = gn.GetAllEvaluationClassInformation();
            dgv_评估类.DataSource = dt;
        }

        private void btn_新条目_Click(object sender, EventArgs e)
        {
            NewEvaluationClass nec = new NewEvaluationClass();
            nec.Show();
        }

        private void btn_刷新_Click(object sender, EventArgs e)
        {
            DataTable dt = gn.GetAllEvaluationClassInformation();
            dgv_评估类.DataSource = dt;
            pageTool_Load(sender, e);
        }

        private void btn_删除_Click(object sender, EventArgs e)
        {
            if (dgv_评估类.CurrentRow == null)
            {
                MessageUtil.ShowError("请选择要删除的行");
            }
            else
            {
                if (MessageUtil.ShowYesNoAndTips("是否确定删除") == DialogResult.Yes)
                {
                    try
                    {
                        string ClassID = dgv_评估类.CurrentRow.Cells[0].Value.ToString();
                        gn.DeleteEvaluationClass(ClassID);
                        dgv_评估类.Rows.Remove(dgv_评估类.CurrentRow);
                        MessageUtil.ShowTips("删除成功");
                        pageTool_Load(sender, e);
                    }
                    catch(DBException ex)
                    {
                        MessageUtil.ShowTips("删除失败");
                    }
                }
            }
        }

        private void pageTool_Load(object sender, EventArgs e)
        {
            string sql = "SELECT count(*) FROM [Evaluation_Class]";
            try
            {
                int i = int.Parse(DBHelper.ExecuteQueryDT(sql).Rows[0][0].ToString());
                int totalNum = i;
                pageTool.DrawControl(totalNum);
                //事件改变调用函数
                pageTool.OnPageChanged += pageTool_OnPageChanged;
            }
            catch (Lib.Common.MMCException.Bll.BllException ex)
            {
                MessageUtil.ShowTips("数据库异常，请稍后重试");
                return;
            }
        }
        private void pageTool_OnPageChanged(object sender, EventArgs e)
        {
            LoadData();
        }
        private void LoadData()
        {
            try
            {
                dgv_评估类.DataSource = findConditionalInforByPage(this.pageTool.PageSize, this.pageTool.PageIndex);
            }
            catch (BllException ex)
            {
                MessageUtil.ShowError(ex.Message);
            }
        }
        private object findConditionalInforByPage(int pageSize, int pageIndex)
        {
            try
            {
                string sql = @"SELECT top " + pageSize + "Class_ID AS '评估类编码',Class_Name AS '评估类名称',Description AS '评估类描述' FROM [Evaluation_Class]  where Class_ID not in(select top " + pageSize * (pageIndex - 1) + " Class_ID from Evaluation_Class ORDER BY Class_ID ASC)ORDER BY Class_ID";
                return DBHelper.ExecuteQueryDT(sql);
            }
            catch (DBException ex)
            {
                throw new BllException("当前无数据", ex);
            };
        }
    }
}
