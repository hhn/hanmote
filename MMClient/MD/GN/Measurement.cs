﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Bll.MDBll.General;
using Lib.Common.CommonUtils;
using Lib.Common.MMCException.Bll;
using Lib.Common.MMCException.IDAL;
using Lib.SqlServerDAL;

namespace MMClient.MD.GN
{
    public partial class Measurement : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        public Measurement()
        {
            InitializeComponent();
        }
        GeneralBLL gn = new GeneralBLL();

        private void btn_新条目_Click(object sender, EventArgs e)
        {
            NewMeasurement nmt = new NewMeasurement();
            nmt.Show();
        }

        private void Measurement_Load(object sender, EventArgs e)
        {
            DataTable dt = gn.GetAllMeasurementInformation();
            dgv_计量单位.DataSource = dt;
        }

        private void btn_刷新_Click(object sender, EventArgs e)
        {
            DataTable dt = gn.GetAllMeasurementInformation();
            dgv_计量单位.DataSource = dt;
            pageTool_Load(sender, e);
        }

        private void pageTool_Load(object sender, EventArgs e)
        {
            string sql = "SELECT count(*) FROM [Measurement]";
            try
            {
                int i = int.Parse(DBHelper.ExecuteQueryDT(sql).Rows[0][0].ToString());
                int totalNum = i;
                pageTool.DrawControl(totalNum);
                //事件改变调用函数
                pageTool.OnPageChanged += pageTool_OnPageChanged;
            }
            catch (Lib.Common.MMCException.Bll.BllException ex)
            {
                MessageUtil.ShowTips("数据库异常，请稍后重试");
                return;
            }
        }

        private void pageTool_OnPageChanged(object sender, EventArgs e)
        {
            LoadData();
        }
        private void LoadData()
        {
            try
            {
                dgv_计量单位.DataSource = findCountryInforByPage(this.pageTool.PageSize, this.pageTool.PageIndex);
            }
            catch (BllException ex)
            {
                MessageUtil.ShowError(ex.Message);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="pageSize"></param>
        /// <param name="pageIndex"></param>
        /// <returns></returns>
        private object findCountryInforByPage(int pageSize, int pageIndex)
        {
            try
            {
                string sql = @"SELECT top " + pageSize + "Measurement_ID AS '计量单位代码',Measurement_Name AS '计量单位名称',Relevant_Measurement AS '相关计量单位',Ratio AS '比率' FROM [Measurement]  where Measurement_ID not in(select top " + pageSize * (pageIndex - 1) + " Measurement_ID from Measurement ORDER BY Measurement_ID ASC)ORDER BY Measurement_ID";
                return DBHelper.ExecuteQueryDT(sql);
            }
            catch (DBException ex)
            {
                throw new BllException("当前无数据", ex);
            }
        }
    }
}
