﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Bll.MDBll.General;
using Lib.Common.CommonUtils;
using Lib.Common.MMCException.Bll;
using Lib.Common.MMCException.IDAL;
using Lib.SqlServerDAL;

namespace MMClient.MD.GN
{
    public partial class Company : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        public Company()
        {
            InitializeComponent();
        }
        GeneralBLL gn = new GeneralBLL();

        private void btn_刷新数据_Click(object sender, EventArgs e)
        {
            DataTable dt = gn.GetAllCompanyInformation();
            dgv_公司代码.DataSource = dt;
            pageTool_Load(sender, e);
        }

        private void btn_新条目_Click(object sender, EventArgs e)
        {
            NewCompany ncy = new NewCompany();
            ncy.Show();
        }

        private void btn_删除_Click(object sender, EventArgs e)
        {
            if (dgv_公司代码.CurrentRow == null)
                MessageUtil.ShowError("请选择要删除的行");
            else
            {
                string CompanyID = dgv_公司代码.CurrentRow.Cells[0].Value.ToString();
                try
                {
                    gn.DeleteCompany(CompanyID);
                    MessageUtil.ShowTips("删除成功");
                    LoadData();
                    pageTool_Load(sender,e);

                }
                catch(BllException ex)
                {
                    MessageUtil.ShowTips("删除失败");
                }
            }
        }

        private void Company_Load(object sender, EventArgs e)
        {
            DataTable dt = gn.GetAllCompanyInformation();
            dgv_公司代码.DataSource = dt;
        }

        private void pageTool_Load(object sender, EventArgs e)
        {
            string sql = "SELECT count(*) FROM [Company]";
            try
            {
                int i = int.Parse(DBHelper.ExecuteQueryDT(sql).Rows[0][0].ToString());
                int totalNum = i;
                pageTool.DrawControl(totalNum);
                //事件改变调用函数
                pageTool.OnPageChanged += pageTool_OnPageChanged;
            }
            catch (Lib.Common.MMCException.Bll.BllException ex)
            {
                MessageUtil.ShowTips("数据库异常，请稍后重试");
                return;
            }
        }
        private void pageTool_OnPageChanged(object sender, EventArgs e)
        {
            LoadData();
        }
        private void LoadData()
        {
            try
            {
                dgv_公司代码.DataSource = findConditionalInforByPage(this.pageTool.PageSize, this.pageTool.PageIndex);
            }
            catch (BllException ex)
            {
                MessageUtil.ShowError(ex.Message);
            }
        }
        private object findConditionalInforByPage(int pageSize, int pageIndex)
        {
            try
            {
                string sql = @"SELECT top " + pageSize + " Company_ID AS '公司代码',Company_Name AS '公司名称' FROM [Company]  where Company_ID not in(select top " + pageSize * (pageIndex - 1) + " Company_ID from Company ORDER BY Company_ID ASC)ORDER BY Company_ID";
                return DBHelper.ExecuteQueryDT(sql);
            }
            catch (DBException ex)
            {
                throw new BllException("当前无数据", ex);
            };
        }
    }
}
