﻿namespace MMClient.MD.GN
{
    partial class selectJudgePerson
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel2 = new System.Windows.Forms.Panel();
            this.name__Pur_Organic = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.code_Pur_Organic = new System.Windows.Forms.ComboBox();
            this.dgv_judgeManInfo = new System.Windows.Forms.DataGridView();
            this.selectJudge = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.evalPersonCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.evalPersonName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.evalItem = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btn_save = new System.Windows.Forms.Button();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_judgeManInfo)).BeginInit();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.Controls.Add(this.name__Pur_Organic);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.code_Pur_Organic);
            this.panel2.Location = new System.Drawing.Point(12, 12);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(623, 61);
            this.panel2.TabIndex = 15;
            // 
            // name__Pur_Organic
            // 
            this.name__Pur_Organic.Location = new System.Drawing.Point(391, 23);
            this.name__Pur_Organic.Name = "name__Pur_Organic";
            this.name__Pur_Organic.ReadOnly = true;
            this.name__Pur_Organic.Size = new System.Drawing.Size(134, 21);
            this.name__Pur_Organic.TabIndex = 13;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(304, 27);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 12);
            this.label2.TabIndex = 12;
            this.label2.Text = "采购组织名称：";
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 32);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(89, 12);
            this.label1.TabIndex = 11;
            this.label1.Text = "采购组织编码：";
            // 
            // code_Pur_Organic
            // 
            this.code_Pur_Organic.FormattingEnabled = true;
            this.code_Pur_Organic.Location = new System.Drawing.Point(109, 24);
            this.code_Pur_Organic.Name = "code_Pur_Organic";
            this.code_Pur_Organic.Size = new System.Drawing.Size(134, 20);
            this.code_Pur_Organic.TabIndex = 10;
            // 
            // dgv_judgeManInfo
            // 
            this.dgv_judgeManInfo.AllowUserToAddRows = false;
            this.dgv_judgeManInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgv_judgeManInfo.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_judgeManInfo.BackgroundColor = System.Drawing.SystemColors.ControlLight;
            this.dgv_judgeManInfo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_judgeManInfo.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv_judgeManInfo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_judgeManInfo.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.selectJudge,
            this.evalPersonCode,
            this.evalPersonName,
            this.evalItem});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv_judgeManInfo.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgv_judgeManInfo.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgv_judgeManInfo.GridColor = System.Drawing.SystemColors.ControlLight;
            this.dgv_judgeManInfo.Location = new System.Drawing.Point(12, 79);
            this.dgv_judgeManInfo.Name = "dgv_judgeManInfo";
            this.dgv_judgeManInfo.RowTemplate.Height = 23;
            this.dgv_judgeManInfo.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv_judgeManInfo.Size = new System.Drawing.Size(624, 429);
            this.dgv_judgeManInfo.TabIndex = 16;
            this.dgv_judgeManInfo.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_judgeManInfo_CellContentClick);
            this.dgv_judgeManInfo.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.MtGroupDiviedTable_RowPostPaint);
            // 
            // selectJudge
            // 
            this.selectJudge.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.selectJudge.HeaderText = "勾选";
            this.selectJudge.Name = "selectJudge";
            this.selectJudge.Width = 38;
            // 
            // evalPersonCode
            // 
            this.evalPersonCode.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.evalPersonCode.DataPropertyName = "evalPersonCode";
            this.evalPersonCode.HeaderText = "评估员编号";
            this.evalPersonCode.Name = "evalPersonCode";
            this.evalPersonCode.ReadOnly = true;
            this.evalPersonCode.ToolTipText = "评估员的编号";
            this.evalPersonCode.Width = 150;
            // 
            // evalPersonName
            // 
            this.evalPersonName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.evalPersonName.DataPropertyName = "evalPersonName";
            this.evalPersonName.HeaderText = "评估员名称";
            this.evalPersonName.Name = "evalPersonName";
            this.evalPersonName.ToolTipText = "评估员名称";
            this.evalPersonName.Width = 150;
            // 
            // evalItem
            // 
            this.evalItem.DataPropertyName = "evalItem";
            this.evalItem.HeaderText = "评估方面";
            this.evalItem.Name = "evalItem";
            // 
            // btn_save
            // 
            this.btn_save.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_save.Location = new System.Drawing.Point(229, 514);
            this.btn_save.Name = "btn_save";
            this.btn_save.Size = new System.Drawing.Size(75, 23);
            this.btn_save.TabIndex = 17;
            this.btn_save.Text = "保存";
            this.btn_save.UseVisualStyleBackColor = true;
            this.btn_save.Click += new System.EventHandler(this.btn_save_Click);
            // 
            // selectJudgePerson
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(650, 546);
            this.Controls.Add(this.btn_save);
            this.Controls.Add(this.dgv_judgeManInfo);
            this.Controls.Add(this.panel2);
            this.Name = "selectJudgePerson";
            this.Text = "指定评审员";
            this.Load += new System.EventHandler(this.selectJudgePerson_Load);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_judgeManInfo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox name__Pur_Organic;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox code_Pur_Organic;
        public System.Windows.Forms.DataGridView dgv_judgeManInfo;
        private System.Windows.Forms.DataGridViewCheckBoxColumn selectJudge;
        private System.Windows.Forms.DataGridViewTextBoxColumn evalPersonCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn evalPersonName;
        private System.Windows.Forms.DataGridViewTextBoxColumn evalItem;
        private System.Windows.Forms.Button btn_save;
    }
}