﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Bll.MDBll.General;
using Lib.Common.CommonUtils;
using Lib.Common.MMCException.Bll;
using Lib.Common.MMCException.IDAL;
using Lib.SqlServerDAL;

namespace MMClient.MD.GN
{
    public partial class Division : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        public Division()
        {
            InitializeComponent();
        }
        GeneralBLL gn = new GeneralBLL();
        private void Division_Load(object sender, EventArgs e)
        {
           DataTable dt = gn.GetAllDivisionInformation();
           dgv_产品组.DataSource = dt;

        }

        private void btn_刷新_Click(object sender, EventArgs e)
        {
            DataTable dt = gn.GetAllDivisionInformation();
            dgv_产品组.DataSource = dt;
            pageTool_Load(sender, e);
        }

        private void btn_新条目_Click(object sender, EventArgs e)
        {
            NewDivision ndn = new NewDivision();
            ndn.Show();
        }

        private void btn_删除_Click(object sender, EventArgs e)
        {
            if (dgv_产品组.CurrentRow == null)
            {
                MessageUtil.ShowError("请选择要删除的行");
            }
            else
            {
                if (MessageUtil.ShowYesNoAndTips("是否确定删除") == DialogResult.Yes)
                {
                    string DivisionID = dgv_产品组.CurrentRow.Cells[0].Value.ToString();
                    try
                    {
                        gn.DeleteDivision(DivisionID);
                        LoadData();
                        MessageUtil.ShowTips("删除成功");
                        pageTool_Load(sender, e);
                    }
                    catch(BllException ex)
                    {
                        MessageUtil.ShowTips("删除失败");
                    }
                }
            }
        }

        private void pageTool_Load(object sender, EventArgs e)
        {
            string sql = "SELECT count(*) FROM [Division]";
            try
            {
                int i = int.Parse(DBHelper.ExecuteQueryDT(sql).Rows[0][0].ToString());
                int totalNum = i;
                pageTool.DrawControl(totalNum);
                //事件改变调用函数
                pageTool.OnPageChanged += pageTool_OnPageChanged;
            }
            catch (Lib.Common.MMCException.Bll.BllException ex)
            {
                MessageUtil.ShowTips("数据库异常，请稍后重试");
                return;
            }
        }
        private void pageTool_OnPageChanged(object sender, EventArgs e)
        {
            LoadData();
        }
        private void LoadData()
        {
            try
            {
                dgv_产品组.DataSource = findConditionalInforByPage(this.pageTool.PageSize, this.pageTool.PageIndex);
            }
            catch (BllException ex)
            {
                MessageUtil.ShowError(ex.Message);
            }
        }
        private object findConditionalInforByPage(int pageSize, int pageIndex)
        {
            try
            {
                string sql = @"SELECT top " + pageSize + " Division_ID AS '产品组编码',Division_Name AS '产品组名称',Description AS '描述' FROM [Division] where Division_ID not in(select top " + pageSize * (pageIndex - 1) + " Division_ID from Division ORDER BY Division_ID ASC)ORDER BY Division_ID";
                return DBHelper.ExecuteQueryDT(sql);
            }
            catch (DBException ex)
            {
                throw new BllException("当前无数据", ex);
            };
        }
    }
}
