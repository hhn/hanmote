﻿namespace MMClient.MD.GN
{
    partial class NewCountry
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_确定 = new System.Windows.Forms.Button();
            this.cbb_国家标志 = new System.Windows.Forms.ComboBox();
            this.cbb_英文缩写 = new System.Windows.Forms.ComboBox();
            this.cbb_国家名称 = new System.Windows.Forms.ComboBox();
            this.cbb_国家代码 = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btn_确定
            // 
            this.btn_确定.Location = new System.Drawing.Point(353, 200);
            this.btn_确定.Name = "btn_确定";
            this.btn_确定.Size = new System.Drawing.Size(75, 45);
            this.btn_确定.TabIndex = 17;
            this.btn_确定.Text = "确定";
            this.btn_确定.UseVisualStyleBackColor = true;
            this.btn_确定.Click += new System.EventHandler(this.btn_确定_Click);
            // 
            // cbb_国家标志
            // 
            this.cbb_国家标志.FormattingEnabled = true;
            this.cbb_国家标志.Location = new System.Drawing.Point(189, 149);
            this.cbb_国家标志.Name = "cbb_国家标志";
            this.cbb_国家标志.Size = new System.Drawing.Size(121, 24);
            this.cbb_国家标志.TabIndex = 16;
            // 
            // cbb_英文缩写
            // 
            this.cbb_英文缩写.FormattingEnabled = true;
            this.cbb_英文缩写.Location = new System.Drawing.Point(189, 113);
            this.cbb_英文缩写.Name = "cbb_英文缩写";
            this.cbb_英文缩写.Size = new System.Drawing.Size(121, 24);
            this.cbb_英文缩写.TabIndex = 15;
            // 
            // cbb_国家名称
            // 
            this.cbb_国家名称.FormattingEnabled = true;
            this.cbb_国家名称.Location = new System.Drawing.Point(189, 78);
            this.cbb_国家名称.Name = "cbb_国家名称";
            this.cbb_国家名称.Size = new System.Drawing.Size(121, 24);
            this.cbb_国家名称.TabIndex = 14;
            // 
            // cbb_国家代码
            // 
            this.cbb_国家代码.FormattingEnabled = true;
            this.cbb_国家代码.Location = new System.Drawing.Point(189, 45);
            this.cbb_国家代码.Name = "cbb_国家代码";
            this.cbb_国家代码.Size = new System.Drawing.Size(121, 24);
            this.cbb_国家代码.TabIndex = 13;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(23, 152);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(64, 17);
            this.label4.TabIndex = 12;
            this.label4.Text = "国家标志";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(23, 116);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 17);
            this.label3.TabIndex = 11;
            this.label3.Text = "英文缩写";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(23, 81);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 17);
            this.label2.TabIndex = 10;
            this.label2.Text = "国家名称";
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(23, 48);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(64, 17);
            this.label1.TabIndex = 9;
            this.label1.Text = "国家代码";
            // 
            // NewCountry
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(546, 253);
            this.Controls.Add(this.btn_确定);
            this.Controls.Add(this.cbb_国家标志);
            this.Controls.Add(this.cbb_英文缩写);
            this.Controls.Add(this.cbb_国家名称);
            this.Controls.Add(this.cbb_国家代码);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "NewCountry";
            this.Text = "新建国家";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_确定;
        private System.Windows.Forms.ComboBox cbb_国家标志;
        private System.Windows.Forms.ComboBox cbb_英文缩写;
        private System.Windows.Forms.ComboBox cbb_国家名称;
        private System.Windows.Forms.ComboBox cbb_国家代码;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
    }
}