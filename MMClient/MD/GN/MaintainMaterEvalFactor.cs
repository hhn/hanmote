﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;
using Lib.Bll.MDBll.MaintainMaterialFactor;
using Lib.Common.CommonUtils;
using Lib.Common;
using Lib.Model.MD;

namespace MMClient.MD.GN
{
    /// <summary>
    /// 维护主标准、次标准
    /// </summary>
    public partial class MaintainMaterEvalFactor : DockContent
    {
        MaintainMtFactor maintainMtFactorBll = new MaintainMtFactor();
        string updateCode;

        public MaintainMaterEvalFactor()
        {
            InitializeComponent();
        }

        private void MaintainMaterEvalFactor_Load(object sender, EventArgs e)
        {

            try
            {
                DataTable dt = maintainMtFactorBll.getMtEvalFactor();
                dgv_MainEvalFactorShow.DataSource = dt;
            }
            catch (Exception ex)
            {
                MessageUtil.ShowTips("加载失败！请重试");
                LogHelper.WriteExceptionLog(ex);
            }
 
            
        }
       
        /// <summary>
        /// 主标准表格添加行号
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgv_MainEvalFactorShow_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            RowPostPaint.paintNum(dgv_MainEvalFactorShow, sender, e); 
        }
        /// <summary>
        /// 次标准表格添加行号
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgv_SubEvalFactor_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            RowPostPaint.paintNum(dgv_SubEvalFactor, sender, e);
        }
     


        private void dgv_MainEvalFactorShow_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                if (dgv_MainEvalFactorShow.CurrentRow != null)
                {
                    string mainEvalFactor = dgv_MainEvalFactorShow.CurrentRow.Cells["代码"].Value.ToString();
                    dgv_SubEvalFactor.DataSource = maintainMtFactorBll.getSubEvalFactor(mainEvalFactor);
                }
            }
            catch (Exception)
            {

                throw;
            }
           
        }

        private void 删除ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            try
            {
                string mainEvalFactor = dgv_MainEvalFactorShow.CurrentRow.Cells["代码"].Value.ToString();
                if (MessageUtil.ShowYesNoAndTips("确定删除？") == DialogResult.Yes) {

                    if (maintainMtFactorBll.deleteMainEvalFactor(mainEvalFactor))
                    {
                        DataRowView drv = dgv_MainEvalFactorShow.SelectedRows[0].DataBoundItem as DataRowView;
                        drv.Row.Delete();
                        MessageUtil.ShowTips("删除成功！");

                    }

                }         
            }
            catch (Exception)
            {

                MessageUtil.ShowError("删除失败！");
            }
        }


        private void 修改ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            updateCode= dgv_MainEvalFactorShow.CurrentRow.Cells["代码"].Value.ToString();
            dgv_MainEvalFactorShow.CurrentCell.ReadOnly = false;
            dgv_MainEvalFactorShow.BeginEdit(false);
            this.dgv_MainEvalFactorShow.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_MainEvalFactorShow_CellValueChanged);

        }
        /// <summary>
        /// 保存主标准的修改
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgv_MainEvalFactorShow_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            MainEvalFactor mainEvalFactor = new MainEvalFactor();
            mainEvalFactor.Code = dgv_MainEvalFactorShow.CurrentRow.Cells["代码"].Value.ToString();
            mainEvalFactor.Desc = dgv_MainEvalFactorShow.CurrentRow.Cells["描述"].Value.ToString();
            if (MessageUtil.ShowYesNoAndTips("保存更改？") == DialogResult.Yes) {
                maintainMtFactorBll.updateMainEval(mainEvalFactor,updateCode);
                this.dgv_MainEvalFactorShow.CellValueChanged -= new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_MainEvalFactorShow_CellValueChanged);
                updateCode = "";
            }
        }

        private void 退出ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        /// <summary>
        /// 次标准删除
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void toolStripMenuItem3_Click(object sender, EventArgs e)
        {
            try
            {
                string subEvalCode = dgv_SubEvalFactor.CurrentRow.Cells["SubStandardCode"].Value.ToString();
                if (MessageUtil.ShowYesNoAndTips("确定删除？") == DialogResult.Yes)
                {
                    if (maintainMtFactorBll.deleteSubEvalFactor(subEvalCode))
                    {
                        DataRowView drv = dgv_SubEvalFactor.SelectedRows[0].DataBoundItem as DataRowView;
                        drv.Row.Delete();
                        MessageUtil.ShowTips("删除成功！");
                    }

                }
            }
            catch (Exception ex)
            {

                MessageUtil.ShowError("删除失败！");
            }
        }

        private void toolStripMenuItem4_Click(object sender, EventArgs e)
        {
            updateCode = dgv_SubEvalFactor.CurrentRow.Cells["SubStandardCode"].Value.ToString();
            dgv_SubEvalFactor.CurrentCell.ReadOnly = false;
            dgv_SubEvalFactor.BeginEdit(false);
            this.dgv_SubEvalFactor.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_SubEvalFactor_CellValueChanged);

        }
        /// <summary>
        /// 修改次标准
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgv_SubEvalFactor_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {

            SubEvalFactor subEvalFactor = new SubEvalFactor();
            string mainCode = dgv_MainEvalFactorShow.CurrentRow.Cells["代码"].Value.ToString();
            subEvalFactor.SubCode = dgv_SubEvalFactor.CurrentRow.Cells["SubStandardCode"].Value.ToString();
            subEvalFactor.Desc = dgv_SubEvalFactor.CurrentRow.Cells["SubStandardDes"].Value.ToString();
            if (MessageUtil.ShowYesNoAndTips("保存更改？") == DialogResult.Yes)
            {
                maintainMtFactorBll.updateSubEval(subEvalFactor, updateCode, mainCode);
                this.dgv_SubEvalFactor.CellValueChanged -= new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_MainEvalFactorShow_CellValueChanged);

            }


        }
        /// <summary>
        ///主标准单元格，失去焦点，不可编辑
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgv_MainEvalFactorShow_CellLeave(object sender, DataGridViewCellEventArgs e)
        {
            dgv_MainEvalFactorShow.CurrentCell.ReadOnly = true;
           
        }

        private void dgv_SubEvalFactor_CellLeave(object sender, DataGridViewCellEventArgs e)
        {
            dgv_SubEvalFactor.CurrentCell.ReadOnly = true;
            
        }

        private void 添加ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            DataTable dt = (DataTable)dgv_MainEvalFactorShow.DataSource;
            dt.Rows.Clear();
            dgv_MainEvalFactorShow.DataSource = dt;
            foreach (DataGridViewColumn item in dgv_MainEvalFactorShow.Columns)
            {
                item.ReadOnly = false;
            }
            this.dgv_MainEvalFactorShow.CellLeave -= new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_MainEvalFactorShow_CellLeave);
        }

        private void 保存ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            MainEvalFactor mainEvalFactor = new MainEvalFactor();
            try
            {
                for (int i = 0; i < dgv_MainEvalFactorShow.Rows.Count - 1; i++)
                {
                    DataGridViewRow item = dgv_MainEvalFactorShow.Rows[i];

                    if (!(item.Cells["代码"].Value.ToString().Equals("") || item.Cells["描述"].Value.ToString().Equals("")))
                    {
                        mainEvalFactor.Code = item.Cells["代码"].Value.ToString();
                        mainEvalFactor.Desc = item.Cells["描述"].Value.ToString();
                        maintainMtFactorBll.addMainEvalFactor(mainEvalFactor);
                    }
                    else
                    {
                        MessageUtil.ShowError("输入不能为空！");
                        return;
                    }
                }

            }
            catch (Exception ex)
            {
                LogHelper.WriteExceptionLog(ex);
                MessageUtil.ShowError(mainEvalFactor.Code + "添加失败！");
                return;
            }
            MessageUtil.ShowError("添加成功！");
            dgv_MainEvalFactorShow.ReadOnly = true;
            dgv_MainEvalFactorShow.DataSource = maintainMtFactorBll.getMtEvalFactor();
        }

        private void 重置ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            dgv_MainEvalFactorShow.DataSource = maintainMtFactorBll.getMtEvalFactor();
            dgv_MainEvalFactorShow.ReadOnly = true;
            //重置激活失去焦点事件
            this.dgv_MainEvalFactorShow.CellLeave += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_MainEvalFactorShow_CellLeave);
        }

        private void 添加ToolStripMenuItem_Click(object sender, EventArgs e)
        {

            DataTable dt = (DataTable)dgv_SubEvalFactor.DataSource;
            dt.Rows.Clear();
            dgv_SubEvalFactor.DataSource = dt;
            dgv_SubEvalFactor.ReadOnly = false;
            foreach (DataGridViewColumn item in dgv_SubEvalFactor.Columns)
            {
                item.ReadOnly = false;
            }
            this.dgv_SubEvalFactor.CellLeave -= new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_SubEvalFactor_CellLeave);
            this.dgv_SubEvalFactor.CellValueChanged -= new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_SubEvalFactor_CellValueChanged);
        }

        private void 保存ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SubEvalFactor subEvalFactor = new SubEvalFactor();
            subEvalFactor.MainEvalCode = dgv_MainEvalFactorShow.CurrentRow.Cells["代码"].Value.ToString();
            try
            {
                for (int i = 0; i < dgv_SubEvalFactor.Rows.Count - 1; i++)
                {
                    DataGridViewRow item = dgv_SubEvalFactor.Rows[i];

                    if (!item.Cells["SubStandardCode"].Value.ToString().Equals(""))
                    {
                        subEvalFactor.SubCode = item.Cells["SubStandardCode"].Value.ToString();
                        subEvalFactor.Desc = item.Cells["SubStandardDes"].Value.ToString();
                        maintainMtFactorBll.addSubEvalFactor(subEvalFactor);
                    }
                }

            }
            catch (Exception ex)
            {
                LogHelper.WriteExceptionLog(ex);
                MessageUtil.ShowError(subEvalFactor.SubCode + "添加失败！");
            }
        }
    }
}
