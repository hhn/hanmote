﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Bll.MDBll.General;
using Lib.Common.CommonUtils;
using Lib.Common.MMCException.Bll;
using Lib.Common.MMCException.IDAL;
using Lib.SqlServerDAL;

namespace MMClient.MD.GN
{
    public partial class CostCenter : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        public CostCenter()
        {
            InitializeComponent();
        }
        GeneralBLL gn = new GeneralBLL();

        private void CostCenter_Load(object sender, EventArgs e)
        {
            string sql = "SELECT Cost_Center_ID AS '成本中心代码',Cost_Center_Name AS '成本中心名称',Company_ID AS '公司代码' FROM [Cost_Center]";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            dgv_成本中心.DataSource = dt;
        }

        private void btn_刷新_Click(object sender, EventArgs e)
        {
            string sql = "SELECT Cost_Center_ID AS '成本中心代码',Cost_Center_Name AS '成本中心名称',Company_ID AS '公司代码' FROM [Cost_Center]";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            dgv_成本中心.DataSource = dt;
            pageTool_Load(sender, e);
        }

        private void btn_新条目_Click(object sender, EventArgs e)
        {
            NewCostCenter ncc = new NewCostCenter();
            ncc.Show();
        }

        private void pageTool_Load(object sender, EventArgs e)
        {
            string sql = "SELECT count(*) FROM [Cost_Center]";
            try
            {
                int i = int.Parse(DBHelper.ExecuteQueryDT(sql).Rows[0][0].ToString());
                int totalNum = i;
                pageTool.DrawControl(totalNum);
                //事件改变调用函数
                pageTool.OnPageChanged += pageTool_OnPageChanged;
            }
            catch (Lib.Common.MMCException.Bll.BllException ex)
            {
                MessageUtil.ShowTips("数据库异常，请稍后重试");
                return;
            }
        }
        private void pageTool_OnPageChanged(object sender, EventArgs e)
        {
            LoadData();
        }
        private void LoadData()
        {
            try
            {
                dgv_成本中心.DataSource = findConditionalInforByPage(this.pageTool.PageSize, this.pageTool.PageIndex);
            }
            catch (BllException ex)
            {
                MessageUtil.ShowError(ex.Message);
            }
        }
        private object findConditionalInforByPage(int pageSize, int pageIndex)
        {
            try
            {
                string sql = @"SELECT top " + pageSize + "Cost_Center_ID AS '成本中心代码',Cost_Center_Name AS '成本中心名称',Company_ID AS '公司代码' FROM [Cost_Center]  where Cost_Center_ID not in(select top " + pageSize * (pageIndex - 1) + " Cost_Center_ID from Cost_Center ORDER BY Cost_Center_ID ASC)ORDER BY Cost_Center_ID";
                return DBHelper.ExecuteQueryDT(sql);
            }
            catch (DBException ex)
            {
                throw new BllException("当前无数据", ex);
            };
        }

        private void btn_删除_Click(object sender, EventArgs e)
        {
            if (dgv_成本中心.CurrentRow == null)
            {
                MessageUtil.ShowError("请选择要删除的行");
            }
            else
            {
                if (MessageUtil.ShowYesNoAndTips("是否确定删除") == DialogResult.Yes)
                {
                    try
                    {
                        string Cost_Center_ID = dgv_成本中心.CurrentRow.Cells[0].Value.ToString();
                        string sql = "delete from  Cost_Center where Cost_Center_ID = '" + Cost_Center_ID + "'";
                        DBHelper.ExecuteQueryDT(sql);
                        dgv_成本中心.Rows.Remove(dgv_成本中心.CurrentRow);
                        MessageUtil.ShowTips("删除成功");
                        pageTool_Load(sender, e);
                    }catch(DBException ex)
                    {
                        MessageBox.Show("删除失败");
                    }
                }
            }
        }
    }
}
