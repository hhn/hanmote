﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Bll.MDBll.General;
using Lib.Common.CommonUtils;
using Lib.Common.MMCException.Bll;
using Lib.Common.MMCException.IDAL;
using Lib.SqlServerDAL;


namespace MMClient.MD.GN
{
    public partial class IndustryCategory : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        public IndustryCategory()
        {
            InitializeComponent();
        }
        GeneralBLL gn = new GeneralBLL();

        private void IndustryCategory_Load(object sender, EventArgs e)
        {
            string sql = "SELECT Industry_ID AS '工业标识编码',Industry_Category AS '工业标识名称' FROM [Industry_Category] ";
           DataTable dt = DBHelper.ExecuteQueryDT(sql);
           dgv_工业标识.DataSource = dt;
        }

        private void btn_新条目_Click(object sender, EventArgs e)
        {
            NewIndustryCategory nic = new NewIndustryCategory();
            nic.Show();
        }

        private void btn_刷新_Click(object sender, EventArgs e)
        {
            try
            {
                string sql = "SELECT Industry_ID AS '工业标识编码',Industry_Category AS '工业标识名称' FROM [Industry_Category] ";
                DataTable dt = DBHelper.ExecuteQueryDT(sql);
                dgv_工业标识.DataSource = dt;
                pageTool_Load(sender, e);
            }catch(DBException ex)
            {
                MessageUtil.ShowTips("刷新失败");
            }
        }

        private void pageTool_Load(object sender, EventArgs e)
        {
            string sql = "SELECT count(*) FROM [Industry_Category]";
            try
            {
                int i = int.Parse(DBHelper.ExecuteQueryDT(sql).Rows[0][0].ToString());
                int totalNum = i;
                pageTool.DrawControl(totalNum);
                //事件改变调用函数
                pageTool.OnPageChanged += pageTool_OnPageChanged;
            }
            catch (Lib.Common.MMCException.Bll.BllException ex)
            {
                MessageUtil.ShowTips("数据库异常，请稍后重试");
                return;
            }
        }
        private void pageTool_OnPageChanged(object sender, EventArgs e)
        {
            LoadData();
        }
        private void LoadData()
        {
            try
            {
                dgv_工业标识.DataSource = findConditionalInforByPage(this.pageTool.PageSize, this.pageTool.PageIndex);
            }
            catch (BllException ex)
            {
                MessageUtil.ShowError(ex.Message);
            }
        }
        private object findConditionalInforByPage(int pageSize, int pageIndex)
        {
            try
            {
                string sql = @"SELECT top " + pageSize + " Industry_ID AS '工业标识编码',Industry_Category AS '工业标识名称' FROM [Industry_Category]   where Industry_ID not in(select top " + pageSize * (pageIndex - 1) + " Industry_ID from Industry_Category ORDER BY Industry_ID ASC)ORDER BY Industry_ID";
                return DBHelper.ExecuteQueryDT(sql);
            }
            catch (DBException ex)
            {
                throw new BllException("当前无数据", ex);
            };
        }

        private void btn_删除_Click(object sender, EventArgs e)
        {
            if (dgv_工业标识.CurrentRow == null)
                MessageUtil.ShowError("请选择要删除的行");
            else
            {
                string Industry_ID = dgv_工业标识.CurrentRow.Cells[0].Value.ToString();
                try
                {
                    string sql = "delete from Industry_Category where Industry_ID = '"+ Industry_ID + "'";
                    DBHelper.ExecuteQueryDT(sql);
                    LoadData();
                    pageTool_Load(sender, e);

                }
                catch (BllException ex)
                {
                    MessageUtil.ShowTips("删除失败");
                }
            }
        }
    }
}
