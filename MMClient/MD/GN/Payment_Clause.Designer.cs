﻿namespace MMClient.MD.GN
{
    partial class Payment_Clause
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.pageTool = new pager.pagetool.pageNext();
            this.dgv_付款条件 = new System.Windows.Forms.DataGridView();
            this.btn_删除 = new System.Windows.Forms.Button();
            this.btn_新条目 = new System.Windows.Forms.Button();
            this.btn_刷新数据 = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_付款条件)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.pageTool);
            this.groupBox1.Controls.Add(this.dgv_付款条件);
            this.groupBox1.Controls.Add(this.btn_删除);
            this.groupBox1.Controls.Add(this.btn_新条目);
            this.groupBox1.Controls.Add(this.btn_刷新数据);
            this.groupBox1.Location = new System.Drawing.Point(0, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1000, 616);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "付款条件";
            // 
            // pageTool
            // 
            this.pageTool.Location = new System.Drawing.Point(161, 514);
            this.pageTool.Name = "pageTool";
            this.pageTool.PageIndex = 1;
            this.pageTool.PageSize = 100;
            this.pageTool.RecordCount = 0;
            this.pageTool.Size = new System.Drawing.Size(685, 43);
            this.pageTool.TabIndex = 10;
            this.pageTool.Load += new System.EventHandler(this.pageTool_Load);
            // 
            // dgv_付款条件
            // 
            this.dgv_付款条件.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgv_付款条件.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.dgv_付款条件.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgv_付款条件.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_付款条件.Location = new System.Drawing.Point(170, 18);
            this.dgv_付款条件.Name = "dgv_付款条件";
            this.dgv_付款条件.RowTemplate.Height = 23;
            this.dgv_付款条件.Size = new System.Drawing.Size(653, 490);
            this.dgv_付款条件.TabIndex = 9;
            this.dgv_付款条件.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_付款条件_CellDoubleClick_1);
            // 
            // btn_删除
            // 
            this.btn_删除.Location = new System.Drawing.Point(27, 308);
            this.btn_删除.Name = "btn_删除";
            this.btn_删除.Size = new System.Drawing.Size(109, 48);
            this.btn_删除.TabIndex = 6;
            this.btn_删除.Text = "删除";
            this.btn_删除.UseVisualStyleBackColor = true;
            this.btn_删除.Click += new System.EventHandler(this.btn_删除_Click);
            // 
            // btn_新条目
            // 
            this.btn_新条目.Location = new System.Drawing.Point(27, 219);
            this.btn_新条目.Name = "btn_新条目";
            this.btn_新条目.Size = new System.Drawing.Size(109, 48);
            this.btn_新条目.TabIndex = 5;
            this.btn_新条目.Text = "新条目";
            this.btn_新条目.UseVisualStyleBackColor = true;
            this.btn_新条目.Click += new System.EventHandler(this.btn_新条目_Click);
            // 
            // btn_刷新数据
            // 
            this.btn_刷新数据.Location = new System.Drawing.Point(27, 131);
            this.btn_刷新数据.Name = "btn_刷新数据";
            this.btn_刷新数据.Size = new System.Drawing.Size(109, 48);
            this.btn_刷新数据.TabIndex = 4;
            this.btn_刷新数据.Text = "刷新";
            this.btn_刷新数据.UseVisualStyleBackColor = true;
            this.btn_刷新数据.Click += new System.EventHandler(this.btn_刷新数据_Click);
            // 
            // Payment_Clause
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1012, 623);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "Payment_Clause";
            this.Text = "付款条件信息";
            this.Load += new System.EventHandler(this.Payment_Clause_Load);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_付款条件)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btn_刷新数据;
        private System.Windows.Forms.Button btn_新条目;
        private System.Windows.Forms.Button btn_删除;
        private pager.pagetool.pageNext pageTool;
        private System.Windows.Forms.DataGridView dgv_付款条件;
    }
}