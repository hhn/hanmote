﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Bll.MDBll.General;
using Lib.Common.CommonUtils;
using Lib.Common.MMCException.Bll;
using Lib.Common.MMCException.IDAL;
using Lib.SqlServerDAL;

namespace MMClient.MD.GN
{
    public partial class TradeClause : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        public TradeClause()
        {
            InitializeComponent();
        }
        GeneralBLL gn = new GeneralBLL();

        private void TradeClause_Load(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = gn.GetAllTradeClauseInformation();
                dgv_支付条件.DataSource = dt;
            }catch(DBException ex)
            {
                MessageBox.Show("数据库查询失败");
            }
        }

        private void btn_新条目_Click(object sender, EventArgs e)
        {
            NewTradeClause ntc = new NewTradeClause();
            ntc.Show();
        }

        private void btn_刷新_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = gn.GetAllTradeClauseInformation();
                dgv_支付条件.DataSource = dt;
                pageTool_Load(sender, e);
            }catch(DBException ex)
            {
                MessageBox.Show("刷新失败");
            }
        }

        private void btn_删除_Click(object sender, EventArgs e)
        {
            if (dgv_支付条件.CurrentRow == null)
            {
                MessageUtil.ShowError("请选择要删除的行");
            }
            else
            {
                if (MessageUtil.ShowYesNoAndTips("是否确定删除") == DialogResult.Yes)
                {
                    try
                    {
                        string ClauseID = dgv_支付条件.CurrentRow.Cells[0].Value.ToString();
                        gn.DeleteTradeClause(ClauseID);
                        dgv_支付条件.Rows.Remove(dgv_支付条件.CurrentRow);
                        MessageUtil.ShowTips("删除成功");
                        pageTool_Load(sender, e);
                    }
                    catch(BllException ex)
                    {
                        MessageUtil.ShowTips("删除失败");
                    }
                }
            }
        }

        private void pageTool_Load(object sender, EventArgs e)
        {
            string sql = "SELECT count(*) FROM [Trade_Clause]";
            try
            {
                int i = int.Parse(DBHelper.ExecuteQueryDT(sql).Rows[0][0].ToString());
                int totalNum = i;
                pageTool.DrawControl(totalNum);
                //事件改变调用函数
                pageTool.OnPageChanged += pageTool_OnPageChanged;
            }
            catch (Lib.Common.MMCException.Bll.BllException ex)
            {
                MessageUtil.ShowTips("数据库异常，请稍后重试");
                return;
            }
        }
        private void pageTool_OnPageChanged(object sender, EventArgs e)
        {
            LoadData();
        }
        private void LoadData()
        {
            try
            {
                dgv_支付条件.DataSource = findConditionalInforByPage(this.pageTool.PageSize, this.pageTool.PageIndex);
            }
            catch (BllException ex)
            {
                MessageUtil.ShowError(ex.Message);
            }
        }
        private object findConditionalInforByPage(int pageSize, int pageIndex)
        {
            try
            {
                string sql = @"SELECT top " + pageSize + " TradeClause_ID AS '交付条件编码',TradeClause_Name AS '交付条件名称',Description AS '交付条件描述' FROM [Trade_Clause] where TradeClause_ID not in(select top " + pageSize * (pageIndex - 1) + " TradeClause_ID from TradeClause ORDER BY TradeClause_ID ASC)ORDER BY TradeClause_ID";
                return DBHelper.ExecuteQueryDT(sql);
            }
            catch (DBException ex)
            {
                throw new BllException("当前无数据", ex);
            };
        }
    }
}
