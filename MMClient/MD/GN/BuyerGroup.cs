﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Bll.MDBll.General;
using Lib.Common.CommonUtils;
using Lib.Common.MMCException.Bll;
using Lib.Common.MMCException.IDAL;
using Lib.SqlServerDAL;

namespace MMClient.MD.GN
{
    public partial class BuyerGroup : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        public BuyerGroup()
        {
            InitializeComponent();
        }
        GeneralBLL gn = new GeneralBLL();
        private void Buyer_Group_Load(object sender, EventArgs e)
        {
            DataTable dt = gn.GetAllBuyerGroupInformation();
            dgv_采购组.DataSource = dt;

          
        }

        private void btn_新条目_Click(object sender, EventArgs e)
        {
            NewBuyerGroup ngp = new NewBuyerGroup();
            ngp.Show();
        }

        private void btn_刷新数据_Click(object sender, EventArgs e)
        {
            DataTable dt = gn.GetAllBuyerGroupInformation();
            dgv_采购组.DataSource = dt;
            pageTool_Load(sender, e);
        }

        private void btn_删除_Click(object sender, EventArgs e)
        {
            if (dgv_采购组.CurrentRow == null)
                MessageUtil.ShowError("请选择要删除的行");
            else
            {
                if (MessageUtil.ShowYesNoAndTips("是否确定删除") == DialogResult.Yes)
                {
                    string GroupID = dgv_采购组.CurrentRow.Cells[0].Value.ToString();
                    try
                    {
                        gn.DeleteBuyerGroup(GroupID);
                        LoadData();
                        MessageUtil.ShowTips("删除成功");
                        pageTool_Load(sender, e);
                    }
                    catch(BllException ex)
                    {
                        MessageUtil.ShowTips("删除失败");
                    }
                }

            }
        }

        private void pageTool_Load(object sender, EventArgs e)
        {
            string sql = "SELECT count(*) FROM [Buyer_Group]";
            try
            {
                int i = int.Parse(DBHelper.ExecuteQueryDT(sql).Rows[0][0].ToString());
                int totalNum = i;
                pageTool.DrawControl(totalNum);
                //事件改变调用函数
                pageTool.OnPageChanged += pageTool_OnPageChanged;
            }
            catch (Lib.Common.MMCException.Bll.BllException ex)
            {
                MessageUtil.ShowTips("数据库异常，请稍后重试");
                return;
            }
        }
        private void pageTool_OnPageChanged(object sender, EventArgs e)
        {
            LoadData();
        }
        private void LoadData()
        {
            try
            {
                dgv_采购组.DataSource = findConditionalInforByPage(this.pageTool.PageSize, this.pageTool.PageIndex);
            }
            catch (BllException ex)
            {
                MessageUtil.ShowError(ex.Message);
            }
        }
        private object findConditionalInforByPage(int pageSize, int pageIndex)
        {
            try
            {
                string sql = @"SELECT top " + pageSize + " Buyer_Group AS '采购组编码',Buyer_Group_Name AS '采购组名称',Material_Type AS '负责的物料类型' FROM [Buyer_Group] where Buyer_Group not in(select top " + pageSize * (pageIndex - 1) + " Buyer_Group from Buyer_Group ORDER BY Buyer_Group ASC)ORDER BY Buyer_Group";
                return DBHelper.ExecuteQueryDT(sql);
            }
            catch (DBException ex)
            {
                throw new BllException("当前无数据", ex);
            };
        }

    }
}
