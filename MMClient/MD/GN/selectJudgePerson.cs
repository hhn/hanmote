﻿using Lib.Bll.MDBll.General;
using Lib.Common.CommonUtils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MMClient.MD.GN
{
    public partial class selectJudgePerson : Form
    {
        GeneralBLL gn = new GeneralBLL();
        string orgName = "";
        string mtName = "";
        string evalItemName = "";
        public selectJudgePerson(string orgName,string mtName,string evalName)
        {
            InitializeComponent();
            //填充dgv数据
            dgv_judgeManInfo.DataSource = gn.getEvalPersonInfo(evalName);
            this.orgName = orgName;
            this.mtName = mtName;
            this.evalItemName = evalName;
        }

        private void MtGroupDiviedTable_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {

            Rectangle rectangle = new Rectangle(e.RowBounds.Location.X,
             e.RowBounds.Location.Y,
             dgv_judgeManInfo.RowHeadersWidth - 4, e.RowBounds.Height);
            TextRenderer.DrawText(e.Graphics,
                  (e.RowIndex + 1).ToString(),
                   dgv_judgeManInfo.RowHeadersDefaultCellStyle.Font,
                   rectangle,
                   dgv_judgeManInfo.RowHeadersDefaultCellStyle.ForeColor,
                   TextFormatFlags.VerticalCenter | TextFormatFlags.Right);
        }

        private void selectJudgePerson_Load(object sender, EventArgs e)
        {


        }

        private void dgv_judgeManInfo_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgv_judgeManInfo.Columns[e.ColumnIndex].Name == "selectJudge") {
                if ((bool)dgv_judgeManInfo.Rows[e.RowIndex].Cells[e.ColumnIndex].EditedFormattedValue==true) {

                    dgv_judgeManInfo.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = false;
                }
                else {
                    dgv_judgeManInfo.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = true;
                }
                

            }
        }

        private void btn_save_Click(object sender, EventArgs e)
        {
            string evaluaterId;
            try
            {
                foreach (DataGridViewRow item in dgv_judgeManInfo.Rows)
                {
                    if ((bool)item.Cells["selectJudge"].EditedFormattedValue == true)
                    {

                        evaluaterId = item.Cells["evalPersonCode"].Value.ToString();

                        gn.saveEvalInfo(this.mtName, this.orgName, evaluaterId);

                    }
                }
            }
            catch (Exception)
            {
                MessageUtil.ShowError("保存失败！");
                return;
            }
            MessageUtil.ShowTips("保存成功！");
            
           
            


        }

    }
}
