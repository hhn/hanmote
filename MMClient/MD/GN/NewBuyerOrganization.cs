﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Bll.MDBll.General;
using Lib.Common.CommonUtils;
using Lib.Bll.MDBll.General.Material_Type;

namespace MMClient.MD.GN
{
    public partial class NewBuyerOrganization : Form
    {
        public NewBuyerOrganization()
        {
            InitializeComponent();
        }
        GeneralBLL gn = new GeneralBLL();

        private void btn_确定_Click(object sender, EventArgs e)
        {
            if (cbb_采购组织代码.Text == "")

            {
                MessageUtil.ShowError("请输入正确的采购组织代码");
                cbb_采购组织代码.Focus();
            }
            else
            {
                List<string> list = gn.GetAllBuyerOrganization();
                if (gn.IDInTheList(list, cbb_采购组织代码.Text) == true)
                {
                    gn.NewBuyerOrganization(cbb_采购组织代码.Text, cbb_采购组织名称.Text);
                    MessageUtil.ShowTips("新建采购组织信息成功");
                    this.Close();
                }
                else
                {
                    MessageUtil.ShowError("采购组织代码重复，请重新输入");
                    cbb_采购组织代码.Focus();
                }
            }
        }
    }
}
