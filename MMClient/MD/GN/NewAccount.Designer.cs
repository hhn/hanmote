﻿namespace MMClient.MD.GN
{
    partial class NewAccount
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.cbb_科目类型 = new System.Windows.Forms.ComboBox();
            this.cbb_父科目名称 = new System.Windows.Forms.ComboBox();
            this.btn_确定 = new System.Windows.Forms.Button();
            this.cbb_科目名称 = new System.Windows.Forms.ComboBox();
            this.cbb_科目代码 = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(363, 121);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(134, 17);
            this.label5.TabIndex = 54;
            this.label5.Text = "（无父科目请不填）";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(70, 162);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(64, 17);
            this.label4.TabIndex = 53;
            this.label4.Text = "科目类型";
            // 
            // cbb_科目类型
            // 
            this.cbb_科目类型.FormattingEnabled = true;
            this.cbb_科目类型.Location = new System.Drawing.Point(236, 159);
            this.cbb_科目类型.Name = "cbb_科目类型";
            this.cbb_科目类型.Size = new System.Drawing.Size(121, 24);
            this.cbb_科目类型.TabIndex = 52;
            // 
            // cbb_父科目名称
            // 
            this.cbb_父科目名称.FormattingEnabled = true;
            this.cbb_父科目名称.Location = new System.Drawing.Point(236, 118);
            this.cbb_父科目名称.Name = "cbb_父科目名称";
            this.cbb_父科目名称.Size = new System.Drawing.Size(121, 24);
            this.cbb_父科目名称.TabIndex = 51;
            // 
            // btn_确定
            // 
            this.btn_确定.Location = new System.Drawing.Point(400, 205);
            this.btn_确定.Name = "btn_确定";
            this.btn_确定.Size = new System.Drawing.Size(75, 45);
            this.btn_确定.TabIndex = 50;
            this.btn_确定.Text = "确定";
            this.btn_确定.UseVisualStyleBackColor = true;
            this.btn_确定.Click += new System.EventHandler(this.btn_确定_Click);
            // 
            // cbb_科目名称
            // 
            this.cbb_科目名称.FormattingEnabled = true;
            this.cbb_科目名称.Location = new System.Drawing.Point(236, 83);
            this.cbb_科目名称.Name = "cbb_科目名称";
            this.cbb_科目名称.Size = new System.Drawing.Size(121, 24);
            this.cbb_科目名称.TabIndex = 49;
            // 
            // cbb_科目代码
            // 
            this.cbb_科目代码.FormattingEnabled = true;
            this.cbb_科目代码.Location = new System.Drawing.Point(236, 50);
            this.cbb_科目代码.Name = "cbb_科目代码";
            this.cbb_科目代码.Size = new System.Drawing.Size(121, 24);
            this.cbb_科目代码.TabIndex = 48;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(70, 121);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(78, 17);
            this.label3.TabIndex = 47;
            this.label3.Text = "父科目名称";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(70, 86);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 17);
            this.label2.TabIndex = 46;
            this.label2.Text = "科目名称";
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(70, 53);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(64, 17);
            this.label1.TabIndex = 45;
            this.label1.Text = "科目代码";
            // 
            // NewAccount
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(546, 253);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.cbb_科目类型);
            this.Controls.Add(this.cbb_父科目名称);
            this.Controls.Add(this.btn_确定);
            this.Controls.Add(this.cbb_科目名称);
            this.Controls.Add(this.cbb_科目代码);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "NewAccount";
            this.Text = "新建科目";
            this.Load += new System.EventHandler(this.NewAccount_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cbb_科目类型;
        private System.Windows.Forms.ComboBox cbb_父科目名称;
        private System.Windows.Forms.Button btn_确定;
        private System.Windows.Forms.ComboBox cbb_科目名称;
        private System.Windows.Forms.ComboBox cbb_科目代码;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
    }
}