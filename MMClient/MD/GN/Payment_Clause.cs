﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Bll.MDBll.General;
using Lib.Common.CommonUtils;
using Lib.Common.MMCException.Bll;
using Lib.Common.MMCException.IDAL;
using Lib.SqlServerDAL;

namespace MMClient.MD.GN
{
    public partial class Payment_Clause : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        public Payment_Clause()
        {
            InitializeComponent();
        }

        private void Payment_Clause_Load(object sender, EventArgs e)
        {
            try
            {
                GeneralBLL gn = new GeneralBLL();
                DataTable dt = gn.GetAllPaymentClause();
                dgv_付款条件.DataSource = dt;
            }catch(DBException ex)
            {
                MessageBox.Show("数据加载失败");
            }
        }

        private void btn_刷新数据_Click(object sender, EventArgs e)
        {
            GeneralBLL gn = new GeneralBLL();
            try
            {
                DataTable dt = gn.GetAllPaymentClause();
                dgv_付款条件.DataSource = dt;
                pageTool_Load(sender, e);
            }catch(DBException ex)
            {
                MessageUtil.ShowError("刷新失败");
            }
        }

        private void btn_新条目_Click(object sender, EventArgs e)
        {
            NewClause nclause = new NewClause();
            nclause.Show();
        }

        private void dgv_付款条件_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            string clauseID = this.dgv_付款条件.CurrentRow.Cells[0].Value.ToString();
            ClauseInformation cinfo = new ClauseInformation(clauseID);
            cinfo.Show();
        }

        private void pageTool_Load(object sender, EventArgs e)
        {
            string sql = "SELECT count(*) from [Payment_Clause]";
            try
            {
                int i = int.Parse(DBHelper.ExecuteQueryDT(sql).Rows[0][0].ToString());
                int totalNum = i;
                pageTool.DrawControl(totalNum);
                //事件改变调用函数
                pageTool.OnPageChanged += pageTool_OnPageChanged;
            }
            catch (Lib.Common.MMCException.Bll.BllException ex)
            {
                MessageUtil.ShowTips("数据库异常，请稍后重试");
                return;
            }
        }
        private void pageTool_OnPageChanged(object sender, EventArgs e)
        {
            LoadData();
        }
        private void LoadData()
        {
            try
            {
                dgv_付款条件.DataSource = findConditionalInforByPage(this.pageTool.PageSize, this.pageTool.PageIndex);
            }
            catch (BllException ex)
            {
                MessageUtil.ShowError(ex.Message);
            }
        }
        private object findConditionalInforByPage(int pageSize, int pageIndex)
        {
            try
            {
                string sql = @"SELECT top " + pageSize + "   Clause_ID  as 条件代码, Description as 描述   FROM [Payment_Clause]  where Clause_ID not in(select top " + pageSize * (pageIndex - 1) + " Clause_ID from Payment_Clause ORDER BY Clause_ID ASC)ORDER BY Clause_ID";
                return DBHelper.ExecuteQueryDT(sql);
            }
            catch (DBException ex)
            {
                throw new BllException("当前无数据", ex);
            };
        }

        private void btn_删除_Click(object sender, EventArgs e)
        {
            string code = dgv_付款条件.CurrentRow.Cells[0].Value.ToString();
            string sql = " DELETE FROM [Payment_Clause] WHERE Clause_ID='" + code + "'";
            try
            {
                DBHelper.ExecuteNonQuery(sql);
                LoadData();
                pageTool_Load(sender, e);
            }catch(BllException ex)
            {
                MessageBox.Show("数据删除失败");
            }
        }

        private void dgv_付款条件_CellDoubleClick_1(object sender, DataGridViewCellEventArgs e)
        {
            dgv_付款条件_CellDoubleClick(sender, e);
        }
    }
}
