﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Bll.MDBll.General;
using Lib.Common.CommonUtils;
using Lib.Common.MMCException.Bll;
using Lib.Common.MMCException.IDAL;
using Lib.SqlServerDAL;

namespace MMClient.MD.GN
{
    public partial class TaxCode : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        public TaxCode()
        {
            InitializeComponent();
        }
        GeneralBLL gn = new GeneralBLL();
        private void TaxCode_Load(object sender, EventArgs e)
        {
           DataTable dt = gn.GetAllTaxCodeInformation();
           dgv_税码.DataSource = dt;
        }

        private void btn_刷新_Click(object sender, EventArgs e)
        {
            DataTable dt = gn.GetAllTaxCodeInformation();
            dgv_税码.DataSource = dt;
        }

        private void btn_新条目_Click(object sender, EventArgs e)
        {
            NewTaxCode ntc = new NewTaxCode();
            ntc.Show(); 
        }

        private void btn_删除_Click(object sender, EventArgs e)
        {
            if (dgv_税码.CurrentRow == null)
            {
                MessageUtil.ShowError("请选择要删除的行");
            }
            else
            {
                if (MessageUtil.ShowYesNoAndTips("是否确定删除") == DialogResult.Yes)
                {
                    string taxcode = dgv_税码.CurrentRow.Cells[0].Value.ToString();
                    gn.DeleteTaxCode(taxcode);
                    dgv_税码.Rows.Remove(dgv_税码.CurrentRow);
                    MessageUtil.ShowTips("删除成功");
                    pageNext1_Load(sender,e);
                }
            }
        }

        private void pageNext1_Load(object sender, EventArgs e)
        {
            string sql = "select count(*) from Tax_Code";
            try
            {
                int i = int.Parse(Lib.SqlServerDAL.DBHelper.ExecuteQueryDT(sql).Rows[0][0].ToString());
                int totalNum = i;
                pageTool.DrawControl(totalNum);
                //事件改变调用函数
                pageTool.OnPageChanged += pageTool_OnPageChanged;
            }
            catch (BllException ex)
            {
                MessageUtil.ShowTips("数据库异常，请稍后重试");
                return;
            }
        }

        private void LoadData()
        {
            try
            {
                dgv_税码.DataSource = FindTaxCodeInforByPage(this.pageTool.PageSize, this.pageTool.PageIndex);
            }
            catch (BllException ex)
            {
                MessageUtil.ShowError(ex.Message);
            }
        }

        private object FindTaxCodeInforByPage(int pageSize, int pageIndex)
        {
            try
            {
                string sql = @"SELECT top " + pageSize + "Tax_Code,Tax,Description from Tax_Code  where Tax_Code not in(select top " + pageSize * (pageIndex - 1) + " Tax_Code from Tax_Code ORDER BY Tax_Code ASC)ORDER BY Tax_Code";
                return DBHelper.ExecuteQueryDT(sql);
            }
            catch (DBException ex)
            {
                throw new BllException("当前无数据", ex);
            }
        }

        private void pageTool_OnPageChanged(object sender, EventArgs e)
        {
            LoadData();
        }
    }
}
