﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Bll.MDBll.General;
using Lib.Common.CommonUtils;

namespace MMClient.MD.GN
{
    public partial class NewEvaluationClass : Form
    {
        public NewEvaluationClass()
        {
            InitializeComponent();
        }
        GeneralBLL gn = new GeneralBLL();
        private void btn_确定_Click(object sender, EventArgs e)
        {
            string ClassID = cbb_评估类代码.Text;
            string ClassName = cbb_评估类名称.Text;
            string Description = tbx_描述.Text;
            if (cbb_评估类代码.Text == "")
            {
                MessageUtil.ShowError("请输入正确的评估类代码");
            }
            else
            {
                if (cbb_评估类名称.Text == "")
                {
                    MessageUtil.ShowError("请输入正确的评估类名称");
                }
                else
                {
                    gn.NewEvaluationClass(ClassID, ClassName, Description);
                    MessageUtil.ShowTips("新建评估类成功");
                    this.Close();
                }

            }
        }
    }
}
