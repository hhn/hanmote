﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Bll.MDBll.General;
using Lib.Bll;
using Lib.SqlServerDAL;

namespace MMClient.MD.GN
{
    public partial class NewAccount : Form
    {
        public NewAccount()
        {
            InitializeComponent();
        }
        GeneralBLL gn = new GeneralBLL();
        AccountBLL act = new AccountBLL();
        ComboBoxItem cbm = new ComboBoxItem();
        private void NewAccount_Load(object sender, EventArgs e)
        {
            List<string> list = act.GetAllAccountName();
          cbm.FuzzyQury(cbb_父科目名称, list);
            
        }

        private void btn_确定_Click(object sender, EventArgs e)
        {
            string fid;
            string accountid;
            string accountname;
            string accounttype;
            if (cbb_父科目名称.Text == "")
            {
                fid = "RA";
                accountid = cbb_科目代码.Text;
            }
            else
            {
                string sql1 = "SELECT Account_ID FROM [Account] WHERE Account_Name='" + cbb_父科目名称.Text + "'";
                DataTable dt = DBHelper.ExecuteQueryDT(sql1);
                fid = dt.Rows[0][0].ToString();
                accountid = fid + cbb_科目代码.Text;
            }
            accountname = cbb_科目名称.Text;
            accounttype = cbb_科目类型.Text;
            string sql2 = "INSERT INTO [Account] (Account_ID,Account_Name,Account_Type,Parent_ID)VALUES('" + accountid + "','" + accountname + "','" + accounttype + "','" + fid + "')";
            DBHelper.ExecuteNonQuery(sql2);
            MessageBox.Show("新建成功");
            this.Close();
        }
    }
}
