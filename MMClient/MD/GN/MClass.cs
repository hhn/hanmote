﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.SqlServerDAL;

namespace MMClient.MD.GN
{
    public partial class MClass : Form
    {
        public MClass()
        {
            InitializeComponent();
        }
        string Class_ID;
        string Class_Name;
        public MClass(string ClassID,string ClassName)
        {


            InitializeComponent();
            Class_ID = ClassID;
            Class_Name = ClassName;
        }

        private void MClass_Load(object sender, EventArgs e)
        {
            tbx_分类编码.Text = Class_ID;
            tbx_分类名称.Text = Class_Name;
            tbx_分类编码.Enabled = false;
            tbx_分类名称.Enabled = false;
            dgv_新建特性.Rows.Add(100);
            string sql = "SELECT * FROM [Class_Feature] WHERE Class_ID='" + tbx_分类编码.Text + "'";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dgv_新建特性.Rows[i].Cells["cln_状态"].Value = true;
                dgv_新建特性.Rows[i].Cells["cln_特性编码"].Value = dt.Rows[i]["Feature_ID"].ToString();
                dgv_新建特性.Rows[i].Cells["cln_特性名称"].Value = dt.Rows[i]["Feature_Name"].ToString();
            }
            dgv_新建特性.Enabled = false;
        }

    }
}
