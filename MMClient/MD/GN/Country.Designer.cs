﻿namespace MMClient.MD.GN
{
    partial class Country
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.pageTool = new pager.pagetool.pageNext();
            this.dgv_国家信息 = new System.Windows.Forms.DataGridView();
            this.国家编码 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.国家名称 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.英文缩写 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btn_删除 = new System.Windows.Forms.Button();
            this.btn_刷新 = new System.Windows.Forms.Button();
            this.btn_新条目 = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_国家信息)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.pageTool);
            this.groupBox1.Controls.Add(this.dgv_国家信息);
            this.groupBox1.Controls.Add(this.btn_删除);
            this.groupBox1.Controls.Add(this.btn_刷新);
            this.groupBox1.Controls.Add(this.btn_新条目);
            this.groupBox1.Location = new System.Drawing.Point(0, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(921, 568);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "国家信息";
            // 
            // pageTool
            // 
            this.pageTool.Location = new System.Drawing.Point(170, 427);
            this.pageTool.Name = "pageTool";
            this.pageTool.PageIndex = 1;
            this.pageTool.PageSize = 100;
            this.pageTool.RecordCount = 0;
            this.pageTool.Size = new System.Drawing.Size(685, 40);
            this.pageTool.TabIndex = 8;
            this.pageTool.OnPageChanged += new System.EventHandler(this.pageTool_OnPageChanged);
            this.pageTool.Load += new System.EventHandler(this.pageTool_Load);
            // 
            // dgv_国家信息
            // 
            this.dgv_国家信息.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgv_国家信息.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.dgv_国家信息.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgv_国家信息.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_国家信息.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.国家编码,
            this.国家名称,
            this.英文缩写});
            this.dgv_国家信息.Location = new System.Drawing.Point(170, 18);
            this.dgv_国家信息.Name = "dgv_国家信息";
            this.dgv_国家信息.RowTemplate.Height = 23;
            this.dgv_国家信息.Size = new System.Drawing.Size(653, 403);
            this.dgv_国家信息.TabIndex = 7;
            // 
            // 国家编码
            // 
            this.国家编码.DataPropertyName = "Country_ID";
            this.国家编码.HeaderText = "国家编码";
            this.国家编码.Name = "国家编码";
            // 
            // 国家名称
            // 
            this.国家名称.DataPropertyName = "Country_Name";
            this.国家名称.HeaderText = "国家名称";
            this.国家名称.Name = "国家名称";
            // 
            // 英文缩写
            // 
            this.英文缩写.DataPropertyName = "Country_Eng";
            this.英文缩写.HeaderText = "英文缩写";
            this.英文缩写.Name = "英文缩写";
            // 
            // btn_删除
            // 
            this.btn_删除.Location = new System.Drawing.Point(27, 311);
            this.btn_删除.Name = "btn_删除";
            this.btn_删除.Size = new System.Drawing.Size(109, 48);
            this.btn_删除.TabIndex = 3;
            this.btn_删除.Text = "删除";
            this.btn_删除.UseVisualStyleBackColor = true;
            this.btn_删除.Click += new System.EventHandler(this.btn_删除_Click);
            // 
            // btn_刷新
            // 
            this.btn_刷新.Location = new System.Drawing.Point(27, 131);
            this.btn_刷新.Name = "btn_刷新";
            this.btn_刷新.Size = new System.Drawing.Size(109, 48);
            this.btn_刷新.TabIndex = 2;
            this.btn_刷新.Text = "刷新";
            this.btn_刷新.UseVisualStyleBackColor = true;
            this.btn_刷新.Click += new System.EventHandler(this.btn_刷新_Click);
            // 
            // btn_新条目
            // 
            this.btn_新条目.Location = new System.Drawing.Point(27, 216);
            this.btn_新条目.Name = "btn_新条目";
            this.btn_新条目.Size = new System.Drawing.Size(109, 48);
            this.btn_新条目.TabIndex = 1;
            this.btn_新条目.Text = "新条目";
            this.btn_新条目.UseVisualStyleBackColor = true;
            this.btn_新条目.Click += new System.EventHandler(this.btn_新条目_Click);
            // 
            // Country
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(921, 582);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "Country";
            this.Text = "维护国家信息";
            this.Load += new System.EventHandler(this.Country_Load);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_国家信息)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btn_删除;
        private System.Windows.Forms.Button btn_刷新;
        private System.Windows.Forms.Button btn_新条目;
        private pager.pagetool.pageNext pageTool;
        private System.Windows.Forms.DataGridView dgv_国家信息;
        private System.Windows.Forms.DataGridViewTextBoxColumn 国家编码;
        private System.Windows.Forms.DataGridViewTextBoxColumn 国家名称;
        private System.Windows.Forms.DataGridViewTextBoxColumn 英文缩写;
    }
}