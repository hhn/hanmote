﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Bll.MDBll.General;
using Lib.Common.MMCException.Bll;

namespace MMClient.MD.GN
{
    public partial class NewClause : Form
    {
        public NewClause()
        {
            InitializeComponent();
        }

        private void btn_取消_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_确定_Click(object sender, EventArgs e)
        {
            float dct1 = 0;
            float dct2 = 0;
            int day1 = 0;
            int day2 = 0;
            int day3 = 0;
            string description;
            string ID;
            description = tbx_描述.Text;
            ID = tbx_条件编码.Text;
            if (tbx_折扣1.Text == "" || tbx_折扣2.Text == "" || tbx_天数2.Text == "" || tbx_天数3.Text == "" || tbx_天数1.Text == "" || description == "" || ID == "")
            {
                MessageBox.Show("请保证数据填写完整");
                return;
            }
            try
            {
                dct1 = (float)Convert.ToDouble(tbx_折扣1.Text);
                dct2 = (float)Convert.ToDouble(tbx_折扣2.Text);
                day1 = Convert.ToInt16(tbx_天数1.Text);
                day2 = Convert.ToInt16(tbx_天数2.Text);
                day3 = Convert.ToInt16(tbx_天数3.Text);
                

            }
            catch (Exception ex)
            {
                MessageBox.Show("数据格式填写错误");
            }
              
            GeneralBLL gn = new GeneralBLL();
            try
            {
                gn.NewPaymentClause(ID, description, dct1, day1, dct2, day2, day3);
                MessageBox.Show("创建付款条件成功");
                this.Close();
            }
            catch(BllException ex)
            {
                MessageBox.Show("数据库连接异常，请稍后重试");
            }
          
        }

      
    }
}
