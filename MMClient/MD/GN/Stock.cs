﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Bll.MDBll.General;
using Lib.Common.CommonUtils;
using Lib.Common.MMCException.Bll;
using Lib.Common.MMCException.IDAL;
using Lib.SqlServerDAL;

namespace MMClient.MD.GN
{
    public partial class Stock : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        public Stock()
        {
            InitializeComponent();
        }
        GeneralBLL gn = new GeneralBLL();
        private void btn_新条目_Click(object sender, EventArgs e)
        {
            NewStock nsk = new NewStock();
            nsk.Show();
        }

        private void Stock_Load(object sender, EventArgs e)
        {
            DataTable dt = gn.GetAllStockInformation();
            dgv_仓库信息.DataSource = dt;
        }

        private void btn_刷新_Click(object sender, EventArgs e)
        {
            DataTable dt = gn.GetAllStockInformation();
            dgv_仓库信息.DataSource = dt;
            pageTool_Load(sender, e);
        }

        private void btn_删除_Click(object sender, EventArgs e)
        {
            if (dgv_仓库信息.CurrentRow == null)
                MessageUtil.ShowError("请选择要删除的行");
            else
            {
                string StockID = dgv_仓库信息.CurrentRow.Cells[0].Value.ToString();
                try
                {
                    gn.DeleteStock(StockID);
                    LoadData();
                    MessageUtil.ShowTips("删除成功");
                    pageTool_Load(sender, e);
                }
                catch(BllException ex)
                {
                    MessageUtil.ShowTips("删除失败");
                }
            }
        }

        private void dgv_仓库信息_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            string stockid;
            string factoryid;
            int rindex = dgv_仓库信息.CurrentCell.RowIndex;
            stockid = dgv_仓库信息[0, rindex].Value.ToString();
            factoryid = dgv_仓库信息[2, rindex].Value.ToString();
            StockInformation ski = new StockInformation(stockid, factoryid);
            ski.Show();
        }

        private void pageTool_Load(object sender, EventArgs e)
        {
            string sql = "SELECT count(*) FROM [Stock]";
            try
            {
                int i = int.Parse(DBHelper.ExecuteQueryDT(sql).Rows[0][0].ToString());
                int totalNum = i;
                pageTool.DrawControl(totalNum);
                //事件改变调用函数
                pageTool.OnPageChanged += pageTool_OnPageChanged;
            }
            catch (Lib.Common.MMCException.Bll.BllException ex)
            {
                MessageUtil.ShowTips("数据库异常，请稍后重试");
                return;
            }
        }
        private void pageTool_OnPageChanged(object sender, EventArgs e)
        {
            LoadData();
        }
        private void LoadData()
        {
            try
            {
                dgv_仓库信息.DataSource = findConditionalInforByPage(this.pageTool.PageSize, this.pageTool.PageIndex);
            }
            catch (BllException ex)
            {
                MessageUtil.ShowError(ex.Message);
            }
        }
        private object findConditionalInforByPage(int pageSize, int pageIndex)
        {
            try
            {
                string sql = @"SELECT top " + pageSize + " Stock_ID AS '仓库代码',Stock_Name AS '仓库名称',Factory_ID AS '所属工厂代码' FROM [Stock] where Stock_ID not in(select top " + pageSize * (pageIndex - 1) + " Stock_ID from Stock ORDER BY Stock_ID ASC)ORDER BY Stock_ID";
                return DBHelper.ExecuteQueryDT(sql);
            }
            catch (DBException ex)
            {
                throw new BllException("当前无数据", ex);
            };
        }
    }
}
