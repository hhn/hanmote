﻿namespace MMClient.MD.GN
{
    partial class MaintainPurOrg
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgv_采购组织 = new System.Windows.Forms.DataGridView();
            this.purOrgId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.purOrgName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pageNext1 = new pager.pagetool.pageNext();
            this.btn_删除 = new System.Windows.Forms.Button();
            this.btn_新条目 = new System.Windows.Forms.Button();
            this.btn_刷新 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_采购组织)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgv_采购组织
            // 
            this.dgv_采购组织.AllowUserToAddRows = false;
            this.dgv_采购组织.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.dgv_采购组织.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_采购组织.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dgv_采购组织.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgv_采购组织.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_采购组织.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.purOrgId,
            this.purOrgName});
            this.dgv_采购组织.Location = new System.Drawing.Point(170, 18);
            this.dgv_采购组织.Name = "dgv_采购组织";
            this.dgv_采购组织.RowTemplate.Height = 24;
            this.dgv_采购组织.Size = new System.Drawing.Size(937, 588);
            this.dgv_采购组织.TabIndex = 1;
            // 
            // purOrgId
            // 
            this.purOrgId.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.purOrgId.DataPropertyName = "采购组织代码";
            this.purOrgId.FillWeight = 1F;
            this.purOrgId.HeaderText = "采购组织代码";
            this.purOrgId.Name = "purOrgId";
            this.purOrgId.Width = 150;
            // 
            // purOrgName
            // 
            this.purOrgName.DataPropertyName = "采购组织名称";
            this.purOrgName.FillWeight = 1F;
            this.purOrgName.HeaderText = "采购组织名称";
            this.purOrgName.Name = "purOrgName";
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.pageNext1);
            this.panel1.Controls.Add(this.btn_删除);
            this.panel1.Controls.Add(this.btn_新条目);
            this.panel1.Controls.Add(this.btn_刷新);
            this.panel1.Controls.Add(this.dgv_采购组织);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1339, 680);
            this.panel1.TabIndex = 2;
            // 
            // pageNext1
            // 
            this.pageNext1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pageNext1.Location = new System.Drawing.Point(283, 612);
            this.pageNext1.Name = "pageNext1";
            this.pageNext1.PageIndex = 1;
            this.pageNext1.PageSize = 100;
            this.pageNext1.RecordCount = 0;
            this.pageNext1.Size = new System.Drawing.Size(681, 42);
            this.pageNext1.TabIndex = 9;
            // 
            // btn_删除
            // 
            this.btn_删除.Location = new System.Drawing.Point(27, 315);
            this.btn_删除.Name = "btn_删除";
            this.btn_删除.Size = new System.Drawing.Size(109, 48);
            this.btn_删除.TabIndex = 8;
            this.btn_删除.Text = "删除";
            this.btn_删除.UseVisualStyleBackColor = true;
            this.btn_删除.Click += new System.EventHandler(this.btn_删除_Click);
            // 
            // btn_新条目
            // 
            this.btn_新条目.Location = new System.Drawing.Point(27, 218);
            this.btn_新条目.Name = "btn_新条目";
            this.btn_新条目.Size = new System.Drawing.Size(109, 48);
            this.btn_新条目.TabIndex = 7;
            this.btn_新条目.Text = "新条目";
            this.btn_新条目.UseVisualStyleBackColor = true;
            this.btn_新条目.Click += new System.EventHandler(this.btn_新条目_Click);
            // 
            // btn_刷新
            // 
            this.btn_刷新.Location = new System.Drawing.Point(27, 131);
            this.btn_刷新.Name = "btn_刷新";
            this.btn_刷新.Size = new System.Drawing.Size(109, 48);
            this.btn_刷新.TabIndex = 6;
            this.btn_刷新.Text = "刷新";
            this.btn_刷新.UseVisualStyleBackColor = true;
            this.btn_刷新.Click += new System.EventHandler(this.btn_刷新_Click);
            // 
            // MaintainPurOrg
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1353, 694);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "MaintainPurOrg";
            this.Text = "维护采购组织";
            this.Load += new System.EventHandler(this.MaintainPurOrg_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_采购组织)).EndInit();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgv_采购组织;
        private System.Windows.Forms.Panel panel1;
        private pager.pagetool.pageNext pageNext1;
        private System.Windows.Forms.Button btn_删除;
        private System.Windows.Forms.Button btn_新条目;
        private System.Windows.Forms.Button btn_刷新;
        private System.Windows.Forms.DataGridViewTextBoxColumn purOrgId;
        private System.Windows.Forms.DataGridViewTextBoxColumn purOrgName;
    }
}