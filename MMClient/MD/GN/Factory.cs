﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Bll.MDBll.General;
using Lib.Common.CommonUtils;
using Lib.Common.MMCException.Bll;
using Lib.Common.MMCException.IDAL;
using Lib.SqlServerDAL;

namespace MMClient.MD.GN
{
    public partial class Factory : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        public Factory()
        {
            InitializeComponent();
        }
        GeneralBLL gn = new GeneralBLL();

        private void Factory_Load(object sender, EventArgs e)
        {
            
            DataTable dt = gn.GetAllFactoryInformation();
            dgv_工厂信息.DataSource = dt;
        }

        private void btn_新条目_Click(object sender, EventArgs e)
        {
            NewFactory nfy = new NewFactory();
            nfy.Show();
        }

        private void btn_刷新_Click(object sender, EventArgs e)
        {
            
            DataTable dt = gn.GetAllFactoryInformation();
            dgv_工厂信息.DataSource = dt;
            pageTool_Load(sender, e);
        }

        private void btn_删除_Click(object sender, EventArgs e)
        {
            if (dgv_工厂信息.CurrentRow == null)
                MessageUtil.ShowError("请选择要删除的行");
            else
            {
                if (MessageUtil.ShowYesNoAndTips("是否确定删除") == DialogResult.Yes)
                {
                    string FactoryID = dgv_工厂信息.CurrentRow.Cells[0].Value.ToString();
                    try
                    {
                        gn.DeleteFactory(FactoryID);
                        LoadData();
                        MessageUtil.ShowTips("删除成功");
                        pageTool_Load(sender, e);
                    }
                    catch(BllException ex)
                    {
                        MessageUtil.ShowTips("删除失败");
                    }
                }

            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FactoryCompany fc = new FactoryCompany();
            fc.Show();
        }

        private void pageTool_Load(object sender, EventArgs e)
        {
            string sql = "SELECT count(*) FROM [Factory]";
            try
            {
                int i = int.Parse(DBHelper.ExecuteQueryDT(sql).Rows[0][0].ToString());
                int totalNum = i;
                pageTool.DrawControl(totalNum);
                //事件改变调用函数
                pageTool.OnPageChanged += pageTool_OnPageChanged;
            }
            catch (Lib.Common.MMCException.Bll.BllException ex)
            {
                MessageUtil.ShowTips("数据库异常，请稍后重试");
                return;
            }
        }
        private void pageTool_OnPageChanged(object sender, EventArgs e)
        {
            LoadData();
        }
        private void LoadData()
        {
            try
            {
                dgv_工厂信息.DataSource = findConditionalInforByPage(this.pageTool.PageSize, this.pageTool.PageIndex);
            }
            catch (BllException ex)
            {
                MessageUtil.ShowError(ex.Message);
            }
        }
        private object findConditionalInforByPage(int pageSize, int pageIndex)
        {
            try
            {
                string sql = @"SELECT top " + pageSize + " Factory_ID AS '工厂代码',Factory_Name AS '工厂名称',Company_ID AS '公司代码' FROM [Factory] where Factory_ID not in(select top " + pageSize * (pageIndex - 1) + " Factory_ID from Factory ORDER BY Factory_ID ASC)ORDER BY Factory_ID";
                return DBHelper.ExecuteQueryDT(sql);
            }
            catch (DBException ex)
            {
                throw new BllException("当前无数据", ex);
            };
        }
    }
}
