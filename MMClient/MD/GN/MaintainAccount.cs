﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.SqlServerDAL;

namespace MMClient.MD.GN
{
    public partial class MaintainAccount : Form
    {
        public MaintainAccount()
        {
            InitializeComponent();
        }

        private void MaintainAccount_Load(object sender, EventArgs e)
        {
            string sql = "SELECT * FROM [Account] ";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            dgv_科目.DataSource = dt;
        }

        private void btn_刷新_Click(object sender, EventArgs e)
        {
            string sql = "SELECT * FROM [Account] ";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            dgv_科目.DataSource = dt;
        }

        private void btn_新条目_Click(object sender, EventArgs e)
        {
            NewAccount mta = new NewAccount();
            mta.Show();
        }
    }
}
