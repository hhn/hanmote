﻿namespace MMClient.MD.GN
{
    partial class NewStock
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_确定 = new System.Windows.Forms.Button();
            this.cbb_所属工厂 = new System.Windows.Forms.ComboBox();
            this.cbb_仓库名称 = new System.Windows.Forms.ComboBox();
            this.cbb_仓库代码 = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btn_确定
            // 
            this.btn_确定.Location = new System.Drawing.Point(384, 208);
            this.btn_确定.Name = "btn_确定";
            this.btn_确定.Size = new System.Drawing.Size(75, 45);
            this.btn_确定.TabIndex = 33;
            this.btn_确定.Text = "确定";
            this.btn_确定.UseVisualStyleBackColor = true;
            this.btn_确定.Click += new System.EventHandler(this.btn_确定_Click);
            // 
            // cbb_所属工厂
            // 
            this.cbb_所属工厂.FormattingEnabled = true;
            this.cbb_所属工厂.Location = new System.Drawing.Point(220, 121);
            this.cbb_所属工厂.Name = "cbb_所属工厂";
            this.cbb_所属工厂.Size = new System.Drawing.Size(121, 24);
            this.cbb_所属工厂.TabIndex = 32;
            // 
            // cbb_仓库名称
            // 
            this.cbb_仓库名称.FormattingEnabled = true;
            this.cbb_仓库名称.Location = new System.Drawing.Point(220, 86);
            this.cbb_仓库名称.Name = "cbb_仓库名称";
            this.cbb_仓库名称.Size = new System.Drawing.Size(121, 24);
            this.cbb_仓库名称.TabIndex = 31;
            // 
            // cbb_仓库代码
            // 
            this.cbb_仓库代码.FormattingEnabled = true;
            this.cbb_仓库代码.Location = new System.Drawing.Point(220, 53);
            this.cbb_仓库代码.Name = "cbb_仓库代码";
            this.cbb_仓库代码.Size = new System.Drawing.Size(121, 24);
            this.cbb_仓库代码.TabIndex = 30;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(54, 124);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 17);
            this.label3.TabIndex = 29;
            this.label3.Text = "所属工厂";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(54, 89);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 17);
            this.label2.TabIndex = 28;
            this.label2.Text = "仓库名称";
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(54, 56);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(64, 17);
            this.label1.TabIndex = 27;
            this.label1.Text = "仓库代码";
            // 
            // NewStock
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(546, 253);
            this.Controls.Add(this.btn_确定);
            this.Controls.Add(this.cbb_所属工厂);
            this.Controls.Add(this.cbb_仓库名称);
            this.Controls.Add(this.cbb_仓库代码);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "NewStock";
            this.Text = "新建仓库";
            this.Load += new System.EventHandler(this.NewStock_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_确定;
        private System.Windows.Forms.ComboBox cbb_所属工厂;
        private System.Windows.Forms.ComboBox cbb_仓库名称;
        private System.Windows.Forms.ComboBox cbb_仓库代码;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
    }
}