﻿namespace MMClient.MD.GN
{
    partial class NewFactory
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_确定 = new System.Windows.Forms.Button();
            this.cbb_所属公司 = new System.Windows.Forms.ComboBox();
            this.cbb_工厂名称 = new System.Windows.Forms.ComboBox();
            this.cbb_工厂代码 = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btn_确定
            // 
            this.btn_确定.Location = new System.Drawing.Point(380, 207);
            this.btn_确定.Name = "btn_确定";
            this.btn_确定.Size = new System.Drawing.Size(75, 45);
            this.btn_确定.TabIndex = 26;
            this.btn_确定.Text = "确定";
            this.btn_确定.UseVisualStyleBackColor = true;
            this.btn_确定.Click += new System.EventHandler(this.btn_确定_Click);
            // 
            // cbb_所属公司
            // 
            this.cbb_所属公司.FormattingEnabled = true;
            this.cbb_所属公司.Location = new System.Drawing.Point(216, 120);
            this.cbb_所属公司.Name = "cbb_所属公司";
            this.cbb_所属公司.Size = new System.Drawing.Size(121, 24);
            this.cbb_所属公司.TabIndex = 24;
            // 
            // cbb_工厂名称
            // 
            this.cbb_工厂名称.FormattingEnabled = true;
            this.cbb_工厂名称.Location = new System.Drawing.Point(216, 85);
            this.cbb_工厂名称.Name = "cbb_工厂名称";
            this.cbb_工厂名称.Size = new System.Drawing.Size(121, 24);
            this.cbb_工厂名称.TabIndex = 23;
            // 
            // cbb_工厂代码
            // 
            this.cbb_工厂代码.FormattingEnabled = true;
            this.cbb_工厂代码.Location = new System.Drawing.Point(216, 52);
            this.cbb_工厂代码.Name = "cbb_工厂代码";
            this.cbb_工厂代码.Size = new System.Drawing.Size(121, 24);
            this.cbb_工厂代码.TabIndex = 22;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(50, 123);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 17);
            this.label3.TabIndex = 20;
            this.label3.Text = "所属公司";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(50, 88);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 17);
            this.label2.TabIndex = 19;
            this.label2.Text = "工厂名称";
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(50, 55);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(64, 17);
            this.label1.TabIndex = 18;
            this.label1.Text = "工厂代码";
            // 
            // NewFactory
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(546, 253);
            this.Controls.Add(this.btn_确定);
            this.Controls.Add(this.cbb_所属公司);
            this.Controls.Add(this.cbb_工厂名称);
            this.Controls.Add(this.cbb_工厂代码);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "NewFactory";
            this.Text = "新建工厂";
            this.Load += new System.EventHandler(this.NewFactory_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_确定;
        private System.Windows.Forms.ComboBox cbb_所属公司;
        private System.Windows.Forms.ComboBox cbb_工厂名称;
        private System.Windows.Forms.ComboBox cbb_工厂代码;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;

    }
}