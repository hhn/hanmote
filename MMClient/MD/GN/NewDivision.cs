﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Bll.MDBll.General;
using Lib.Common.CommonUtils;

namespace MMClient.MD.GN
{
    public partial class NewDivision : Form
    {
        public NewDivision()
        {
            InitializeComponent();
        }
        GeneralBLL gn = new GeneralBLL();

        private void btn_确定_Click(object sender, EventArgs e)
        {
            string DivisionID = cbb_产品组代码.Text;
            string DivisionName = cbb_产品组名称.Text;
            string Description = tbx_描述.Text;
            if (cbb_产品组代码.Text == "")
            {
                MessageUtil.ShowError("请输入正确的产品组代码");
            }
            else
            {
                if (cbb_产品组名称.Text == "")
                {
                    MessageUtil.ShowError("请输入正确的产品组名称");
                }
                else
                {
                    gn.NewDivision(DivisionID, DivisionName, Description);
                    MessageUtil.ShowTips("新建产品组成功");
                    this.Close();
                }
            }
        }
    }
}
