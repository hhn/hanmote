﻿using Lib.IDAL.MDIDAL.SP;
using Lib.Model.MD.SP;
using Lib.SqlServerDAL.MDDAL.SP;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MMClient.MD.SP
{
    public partial class createAG : Form
    {
        Account_Group account_Group = new Account_Group();
        Account_GroupIDAL account_GroupIDAL = new Account_GroupDAL();
        NSIDAL NSIDAL = new NSDAL();
        DataTable dt1,dt2;
        Boolean AGIDflag, AGNSFlag;

        public createAG()
        {
            InitializeComponent();
            init();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            AGIDflag = account_GroupIDAL.CheckOnce(account_Group.Account_Group_ID);
            if (!AGIDflag)
            {
                MessageBox.Show("ID不唯一,请修改");
            }
            else {
                MessageBox.Show("ID唯一");
                button2.Enabled = false;
                button3.Enabled = true;
                textBox2.Enabled = true;
                textBox3.Enabled = true;
                comboBox1.Enabled = true;
                checkBox1.Enabled = true;
                checkBox1.Checked = true;
                getdt2();
            }
        }

        private void getdt2() {
            dt2 = NSIDAL.selectAllID();
            comboBox1.DataSource = dt2;
            comboBox1.DisplayMember = "Description";
            comboBox1.ValueMember = "Number_ID";
            label6.Text ="号码段ID:"+comboBox1.SelectedValue.ToString();
            account_Group.NS_id = comboBox1.SelectedValue.ToString();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked == true)
            {
                textBox2.Text = "一次性供应商";
                textBox2.Enabled = false;
                account_Group.Description = "一次性供应商";
                account_Group.SingleStatus = "是";
            }
            else {
                textBox2.Text = "";
                textBox2.Enabled = true;
                account_Group.SingleStatus = "否";
            }
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            account_Group.Description = textBox2.Text.ToString();
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            if (textBox3.Text.ToString().Trim().Equals("") || textBox3.Text == null)
            {
                button2.Enabled = false;
                AGNSFlag = false;
                MessageBox.Show("请输入号码段描述后再查找");
            }
            else {
                button2.Enabled = true;
            }
        }

        private void init() {
            button1.Enabled = false;
            button2.Enabled = false;
            button3.Enabled = false;
            textBox2.Enabled = false;
            textBox3.Enabled = false;
            comboBox1.Enabled = false;
            checkBox1.Enabled = false;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox1.SelectedValue != null)
            {
                label6.Text = "号码段ID:" + comboBox1.SelectedValue.ToString();
                account_Group.NS_id = comboBox1.SelectedValue.ToString();
                AGNSFlag = true;
            }
            else {
                AGNSFlag = false;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            dt1 = NSIDAL.selectbyname(textBox3.Text.ToString());
            comboBox1.DataSource = dt1;
            comboBox1.DisplayMember = "Description";
            comboBox1.ValueMember = "Number_ID";
            try {
                label6.Text = "号码段ID:"+comboBox1.SelectedValue.ToString();
                account_Group.NS_id = comboBox1.SelectedValue.ToString();
            } catch {
                MessageBox.Show("没有此号码段,已为您查找全部!");
                getdt2();
            }
            finally {
                AGNSFlag = true;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (AGNSFlag && AGIDflag) {
              string messsage =  account_GroupIDAL.insert(account_Group);
                MessageBox.Show(messsage);
            }
            this.Close();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            init();
            Boolean AGGFflag = true;
            if (textBox1.Text.ToString().Trim().Equals("") || textBox1.Text == null)
            {
                this.button1.Enabled = false;
                AGGFflag = false;
                MessageBox.Show("请输入正确的ID");
            }
            else if (!((textBox1.Text.ToString().CompareTo("aa") >= 0) && (textBox1.Text.ToString().CompareTo("ZZ") <= 0)&&(textBox1.Text.ToString().Trim().Length<3))) {
                label5.Text = "ID范围为:aa-ZZ";
                AGGFflag = false;
            }else if(AGGFflag)
            {
                label5.Text = "";
                account_Group.Account_Group_ID = textBox1.Text.ToString();
                this.button1.Enabled = true;
            }
        }
    }
}
