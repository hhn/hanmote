﻿namespace MMClient.MD.SP
{
    partial class SPORG
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SPORG));
            this.cbb_供应商名称 = new System.Windows.Forms.ComboBox();
            this.cbb_供应商编码 = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cbb_采购组织编码 = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.TextBox_Message = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.cbb_订单优化限制 = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txb_定价日期控制 = new System.Windows.Forms.TextBox();
            this.cbb_定价日期控制 = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txb_方案组 = new System.Windows.Forms.TextBox();
            this.cbb_方案组 = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.cbb_最小订单值 = new System.Windows.Forms.ComboBox();
            this.cbb_贸易条件 = new System.Windows.Forms.ComboBox();
            this.cbb_付款条件 = new System.Windows.Forms.ComboBox();
            this.cbb_订单货币 = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.cbb_供应商累计 = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.ckb_基于收货的发票验证 = new System.Windows.Forms.CheckBox();
            this.ckb_自动评估结算交货 = new System.Windows.Forms.CheckBox();
            this.ckb_自动评估结算保留 = new System.Windows.Forms.CheckBox();
            this.ckb_回执需求 = new System.Windows.Forms.CheckBox();
            this.ckb_自动建立采购订单 = new System.Windows.Forms.CheckBox();
            this.ckb_后续结算 = new System.Windows.Forms.CheckBox();
            this.ckb_后续结算索引 = new System.Windows.Forms.CheckBox();
            this.ckb_所需业务量比较 = new System.Windows.Forms.CheckBox();
            this.ckb_单据索引有效的 = new System.Windows.Forms.CheckBox();
            this.ckb_退货供应商 = new System.Windows.Forms.CheckBox();
            this.ckb_基于服务的开票校验 = new System.Windows.Forms.CheckBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label16 = new System.Windows.Forms.Label();
            this.cbb_装运条件 = new System.Windows.Forms.ComboBox();
            this.ckb_相关代理业务 = new System.Windows.Forms.CheckBox();
            this.ckb_相关价格确定 = new System.Windows.Forms.CheckBox();
            this.ckb_准许折扣 = new System.Windows.Forms.CheckBox();
            this.ckb_允许重新评估 = new System.Windows.Forms.CheckBox();
            this.cbb_控制参数文件 = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txb_排序标准 = new System.Windows.Forms.TextBox();
            this.cbb_排序标准 = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.cbb_输入管理处 = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.cbb_运输方式 = new System.Windows.Forms.ComboBox();
            this.cbb_ABC标识 = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.dtp_计划交货时间 = new System.Windows.Forms.DateTimePicker();
            this.cbb_舍入参数文件 = new System.Windows.Forms.ComboBox();
            this.label21 = new System.Windows.Forms.Label();
            this.cbb_计量单位组 = new System.Windows.Forms.ComboBox();
            this.label20 = new System.Windows.Forms.Label();
            this.cbb_确认控制 = new System.Windows.Forms.ComboBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.cbb_采购组 = new System.Windows.Forms.ComboBox();
            this.label17 = new System.Windows.Forms.Label();
            this.toolStripExecutionMonitorSetting = new System.Windows.Forms.ToolStrip();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton7 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton8 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton4 = new System.Windows.Forms.ToolStripButton();
            this.TextBox_采购组织名称 = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.toolStripExecutionMonitorSetting.SuspendLayout();
            this.SuspendLayout();
            // 
            // cbb_供应商名称
            // 
            this.cbb_供应商名称.Enabled = false;
            this.cbb_供应商名称.FormattingEnabled = true;
            this.cbb_供应商名称.Location = new System.Drawing.Point(360, 87);
            this.cbb_供应商名称.Name = "cbb_供应商名称";
            this.cbb_供应商名称.Size = new System.Drawing.Size(208, 21);
            this.cbb_供应商名称.TabIndex = 5;
            this.cbb_供应商名称.Tag = "2";
            this.cbb_供应商名称.TextChanged += new System.EventHandler(this.cbb_供应商名称_TextChanged);
            // 
            // cbb_供应商编码
            // 
            this.cbb_供应商编码.Enabled = false;
            this.cbb_供应商编码.FormattingEnabled = true;
            this.cbb_供应商编码.Location = new System.Drawing.Point(180, 87);
            this.cbb_供应商编码.Name = "cbb_供应商编码";
            this.cbb_供应商编码.Size = new System.Drawing.Size(174, 21);
            this.cbb_供应商编码.TabIndex = 4;
            this.cbb_供应商编码.Tag = "1";
            this.cbb_供应商编码.TextChanged += new System.EventHandler(this.cbb_供应商编码_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 94);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "供应商";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 134);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "采购组织";
            // 
            // cbb_采购组织编码
            // 
            this.cbb_采购组织编码.FormattingEnabled = true;
            this.cbb_采购组织编码.Location = new System.Drawing.Point(180, 124);
            this.cbb_采购组织编码.Name = "cbb_采购组织编码";
            this.cbb_采购组织编码.Size = new System.Drawing.Size(174, 21);
            this.cbb_采购组织编码.TabIndex = 7;
            this.cbb_采购组织编码.Tag = "1";
            this.cbb_采购组织编码.SelectedIndexChanged += new System.EventHandler(this.cbb_采购组织编码_SelectedIndexChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.TextBox_Message);
            this.groupBox1.Controls.Add(this.textBox1);
            this.groupBox1.Controls.Add(this.label22);
            this.groupBox1.Controls.Add(this.cbb_订单优化限制);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.txb_定价日期控制);
            this.groupBox1.Controls.Add(this.cbb_定价日期控制);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.txb_方案组);
            this.groupBox1.Controls.Add(this.cbb_方案组);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.cbb_最小订单值);
            this.groupBox1.Controls.Add(this.cbb_贸易条件);
            this.groupBox1.Controls.Add(this.cbb_付款条件);
            this.groupBox1.Controls.Add(this.cbb_订单货币);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.groupBox1.Location = new System.Drawing.Point(10, 154);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1183, 294);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "条件";
            // 
            // TextBox_Message
            // 
            this.TextBox_Message.BackColor = System.Drawing.SystemColors.Control;
            this.TextBox_Message.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TextBox_Message.Cursor = System.Windows.Forms.Cursors.No;
            this.TextBox_Message.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.TextBox_Message.ForeColor = System.Drawing.SystemColors.InfoText;
            this.TextBox_Message.Location = new System.Drawing.Point(297, 70);
            this.TextBox_Message.Name = "TextBox_Message";
            this.TextBox_Message.ReadOnly = true;
            this.TextBox_Message.Size = new System.Drawing.Size(614, 19);
            this.TextBox_Message.TabIndex = 18;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(758, 146);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(153, 19);
            this.textBox1.TabIndex = 17;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(597, 149);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(79, 13);
            this.label22.TabIndex = 16;
            this.label22.Text = "最小订单批量";
            // 
            // cbb_订单优化限制
            // 
            this.cbb_订单优化限制.FormattingEnabled = true;
            this.cbb_订单优化限制.Location = new System.Drawing.Point(174, 266);
            this.cbb_订单优化限制.Name = "cbb_订单优化限制";
            this.cbb_订单优化限制.Size = new System.Drawing.Size(58, 21);
            this.cbb_订单优化限制.TabIndex = 15;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(10, 269);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(79, 13);
            this.label9.TabIndex = 14;
            this.label9.Text = "订单优化限制";
            // 
            // txb_定价日期控制
            // 
            this.txb_定价日期控制.Location = new System.Drawing.Point(248, 227);
            this.txb_定价日期控制.Name = "txb_定价日期控制";
            this.txb_定价日期控制.Size = new System.Drawing.Size(202, 19);
            this.txb_定价日期控制.TabIndex = 13;
            // 
            // cbb_定价日期控制
            // 
            this.cbb_定价日期控制.FormattingEnabled = true;
            this.cbb_定价日期控制.Location = new System.Drawing.Point(174, 224);
            this.cbb_定价日期控制.Name = "cbb_定价日期控制";
            this.cbb_定价日期控制.Size = new System.Drawing.Size(58, 21);
            this.cbb_定价日期控制.TabIndex = 12;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(10, 227);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(79, 13);
            this.label8.TabIndex = 11;
            this.label8.Text = "定价日期控制";
            // 
            // txb_方案组
            // 
            this.txb_方案组.Location = new System.Drawing.Point(248, 186);
            this.txb_方案组.Name = "txb_方案组";
            this.txb_方案组.Size = new System.Drawing.Size(202, 19);
            this.txb_方案组.TabIndex = 10;
            // 
            // cbb_方案组
            // 
            this.cbb_方案组.FormattingEnabled = true;
            this.cbb_方案组.Location = new System.Drawing.Point(174, 183);
            this.cbb_方案组.Name = "cbb_方案组";
            this.cbb_方案组.Size = new System.Drawing.Size(58, 21);
            this.cbb_方案组.TabIndex = 9;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(10, 186);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(91, 13);
            this.label7.TabIndex = 8;
            this.label7.Text = "方案组，供应商";
            // 
            // cbb_最小订单值
            // 
            this.cbb_最小订单值.FormattingEnabled = true;
            this.cbb_最小订单值.Location = new System.Drawing.Point(174, 143);
            this.cbb_最小订单值.Name = "cbb_最小订单值";
            this.cbb_最小订单值.Size = new System.Drawing.Size(174, 21);
            this.cbb_最小订单值.TabIndex = 7;
            // 
            // cbb_贸易条件
            // 
            this.cbb_贸易条件.FormattingEnabled = true;
            this.cbb_贸易条件.Location = new System.Drawing.Point(174, 106);
            this.cbb_贸易条件.Name = "cbb_贸易条件";
            this.cbb_贸易条件.Size = new System.Drawing.Size(58, 21);
            this.cbb_贸易条件.TabIndex = 6;
            // 
            // cbb_付款条件
            // 
            this.cbb_付款条件.FormattingEnabled = true;
            this.cbb_付款条件.Location = new System.Drawing.Point(174, 70);
            this.cbb_付款条件.Name = "cbb_付款条件";
            this.cbb_付款条件.Size = new System.Drawing.Size(121, 21);
            this.cbb_付款条件.TabIndex = 5;
            this.cbb_付款条件.SelectedIndexChanged += new System.EventHandler(this.cbb_付款条件_SelectedIndexChanged);
            // 
            // cbb_订单货币
            // 
            this.cbb_订单货币.FormattingEnabled = true;
            this.cbb_订单货币.Location = new System.Drawing.Point(174, 34);
            this.cbb_订单货币.Name = "cbb_订单货币";
            this.cbb_订单货币.Size = new System.Drawing.Size(121, 21);
            this.cbb_订单货币.TabIndex = 4;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(10, 146);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(67, 13);
            this.label6.TabIndex = 3;
            this.label6.Text = "最小订单值";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(10, 109);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(79, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "国际贸易条件";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(10, 73);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(55, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "付款条件";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(10, 37);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "订单货币";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.cbb_供应商累计);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Location = new System.Drawing.Point(10, 454);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1168, 71);
            this.groupBox2.TabIndex = 10;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "销售数据";
            // 
            // cbb_供应商累计
            // 
            this.cbb_供应商累计.FormattingEnabled = true;
            this.cbb_供应商累计.Location = new System.Drawing.Point(173, 36);
            this.cbb_供应商累计.Name = "cbb_供应商累计";
            this.cbb_供应商累计.Size = new System.Drawing.Size(276, 21);
            this.cbb_供应商累计.TabIndex = 1;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(9, 39);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(67, 13);
            this.label10.TabIndex = 0;
            this.label10.Text = "供应商累计";
            // 
            // ckb_基于收货的发票验证
            // 
            this.ckb_基于收货的发票验证.AutoSize = true;
            this.ckb_基于收货的发票验证.Location = new System.Drawing.Point(9, 34);
            this.ckb_基于收货的发票验证.Name = "ckb_基于收货的发票验证";
            this.ckb_基于收货的发票验证.Size = new System.Drawing.Size(134, 17);
            this.ckb_基于收货的发票验证.TabIndex = 0;
            this.ckb_基于收货的发票验证.Text = "基于收货的发票验证";
            this.ckb_基于收货的发票验证.UseVisualStyleBackColor = true;
            // 
            // ckb_自动评估结算交货
            // 
            this.ckb_自动评估结算交货.AutoSize = true;
            this.ckb_自动评估结算交货.Location = new System.Drawing.Point(9, 61);
            this.ckb_自动评估结算交货.Name = "ckb_自动评估结算交货";
            this.ckb_自动评估结算交货.Size = new System.Drawing.Size(138, 17);
            this.ckb_自动评估结算交货.TabIndex = 1;
            this.ckb_自动评估结算交货.Text = "自动评估GR结算交货";
            this.ckb_自动评估结算交货.UseVisualStyleBackColor = true;
            // 
            // ckb_自动评估结算保留
            // 
            this.ckb_自动评估结算保留.AutoSize = true;
            this.ckb_自动评估结算保留.Location = new System.Drawing.Point(9, 88);
            this.ckb_自动评估结算保留.Name = "ckb_自动评估结算保留";
            this.ckb_自动评估结算保留.Size = new System.Drawing.Size(138, 17);
            this.ckb_自动评估结算保留.TabIndex = 2;
            this.ckb_自动评估结算保留.Text = "自动评估GR结算保留";
            this.ckb_自动评估结算保留.UseVisualStyleBackColor = true;
            // 
            // ckb_回执需求
            // 
            this.ckb_回执需求.AutoSize = true;
            this.ckb_回执需求.Location = new System.Drawing.Point(9, 115);
            this.ckb_回执需求.Name = "ckb_回执需求";
            this.ckb_回执需求.Size = new System.Drawing.Size(74, 17);
            this.ckb_回执需求.TabIndex = 3;
            this.ckb_回执需求.Text = "回执需求";
            this.ckb_回执需求.UseVisualStyleBackColor = true;
            // 
            // ckb_自动建立采购订单
            // 
            this.ckb_自动建立采购订单.AutoSize = true;
            this.ckb_自动建立采购订单.Location = new System.Drawing.Point(9, 142);
            this.ckb_自动建立采购订单.Name = "ckb_自动建立采购订单";
            this.ckb_自动建立采购订单.Size = new System.Drawing.Size(122, 17);
            this.ckb_自动建立采购订单.TabIndex = 4;
            this.ckb_自动建立采购订单.Text = "自动建立采购订单";
            this.ckb_自动建立采购订单.UseVisualStyleBackColor = true;
            // 
            // ckb_后续结算
            // 
            this.ckb_后续结算.AutoSize = true;
            this.ckb_后续结算.Location = new System.Drawing.Point(9, 169);
            this.ckb_后续结算.Name = "ckb_后续结算";
            this.ckb_后续结算.Size = new System.Drawing.Size(74, 17);
            this.ckb_后续结算.TabIndex = 5;
            this.ckb_后续结算.Text = "后续结算";
            this.ckb_后续结算.UseVisualStyleBackColor = true;
            // 
            // ckb_后续结算索引
            // 
            this.ckb_后续结算索引.AutoSize = true;
            this.ckb_后续结算索引.Location = new System.Drawing.Point(9, 196);
            this.ckb_后续结算索引.Name = "ckb_后续结算索引";
            this.ckb_后续结算索引.Size = new System.Drawing.Size(98, 17);
            this.ckb_后续结算索引.TabIndex = 6;
            this.ckb_后续结算索引.Text = "后续结算索引";
            this.ckb_后续结算索引.UseVisualStyleBackColor = true;
            // 
            // ckb_所需业务量比较
            // 
            this.ckb_所需业务量比较.AutoSize = true;
            this.ckb_所需业务量比较.Location = new System.Drawing.Point(9, 223);
            this.ckb_所需业务量比较.Name = "ckb_所需业务量比较";
            this.ckb_所需业务量比较.Size = new System.Drawing.Size(139, 17);
            this.ckb_所需业务量比较.TabIndex = 7;
            this.ckb_所需业务量比较.Text = "所需业务量比较/协议";
            this.ckb_所需业务量比较.UseVisualStyleBackColor = true;
            // 
            // ckb_单据索引有效的
            // 
            this.ckb_单据索引有效的.AutoSize = true;
            this.ckb_单据索引有效的.Location = new System.Drawing.Point(9, 250);
            this.ckb_单据索引有效的.Name = "ckb_单据索引有效的";
            this.ckb_单据索引有效的.Size = new System.Drawing.Size(110, 17);
            this.ckb_单据索引有效的.TabIndex = 8;
            this.ckb_单据索引有效的.Text = "单据索引有效的";
            this.ckb_单据索引有效的.UseVisualStyleBackColor = true;
            // 
            // ckb_退货供应商
            // 
            this.ckb_退货供应商.AutoSize = true;
            this.ckb_退货供应商.Location = new System.Drawing.Point(9, 277);
            this.ckb_退货供应商.Name = "ckb_退货供应商";
            this.ckb_退货供应商.Size = new System.Drawing.Size(86, 17);
            this.ckb_退货供应商.TabIndex = 9;
            this.ckb_退货供应商.Text = "退货供应商";
            this.ckb_退货供应商.UseVisualStyleBackColor = true;
            // 
            // ckb_基于服务的开票校验
            // 
            this.ckb_基于服务的开票校验.AutoSize = true;
            this.ckb_基于服务的开票校验.Location = new System.Drawing.Point(9, 304);
            this.ckb_基于服务的开票校验.Name = "ckb_基于服务的开票校验";
            this.ckb_基于服务的开票校验.Size = new System.Drawing.Size(134, 17);
            this.ckb_基于服务的开票校验.TabIndex = 10;
            this.ckb_基于服务的开票校验.Text = "基于服务的开票校验";
            this.ckb_基于服务的开票校验.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.groupBox4);
            this.groupBox3.Controls.Add(this.label16);
            this.groupBox3.Controls.Add(this.cbb_装运条件);
            this.groupBox3.Controls.Add(this.ckb_相关代理业务);
            this.groupBox3.Controls.Add(this.ckb_相关价格确定);
            this.groupBox3.Controls.Add(this.ckb_准许折扣);
            this.groupBox3.Controls.Add(this.ckb_允许重新评估);
            this.groupBox3.Controls.Add(this.cbb_控制参数文件);
            this.groupBox3.Controls.Add(this.label15);
            this.groupBox3.Controls.Add(this.txb_排序标准);
            this.groupBox3.Controls.Add(this.cbb_排序标准);
            this.groupBox3.Controls.Add(this.label14);
            this.groupBox3.Controls.Add(this.cbb_输入管理处);
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Controls.Add(this.cbb_运输方式);
            this.groupBox3.Controls.Add(this.cbb_ABC标识);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.ckb_基于服务的开票校验);
            this.groupBox3.Controls.Add(this.ckb_退货供应商);
            this.groupBox3.Controls.Add(this.ckb_单据索引有效的);
            this.groupBox3.Controls.Add(this.ckb_所需业务量比较);
            this.groupBox3.Controls.Add(this.ckb_后续结算索引);
            this.groupBox3.Controls.Add(this.ckb_后续结算);
            this.groupBox3.Controls.Add(this.ckb_自动建立采购订单);
            this.groupBox3.Controls.Add(this.ckb_回执需求);
            this.groupBox3.Controls.Add(this.ckb_自动评估结算保留);
            this.groupBox3.Controls.Add(this.ckb_自动评估结算交货);
            this.groupBox3.Controls.Add(this.ckb_基于收货的发票验证);
            this.groupBox3.Location = new System.Drawing.Point(10, 531);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(1164, 328);
            this.groupBox3.TabIndex = 11;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "控制数据";
            // 
            // groupBox4
            // 
            this.groupBox4.Location = new System.Drawing.Point(4, 327);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(1174, 214);
            this.groupBox4.TabIndex = 29;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "groupBox4";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(714, 274);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(55, 13);
            this.label16.TabIndex = 28;
            this.label16.Text = "装运条件";
            // 
            // cbb_装运条件
            // 
            this.cbb_装运条件.FormattingEnabled = true;
            this.cbb_装运条件.Location = new System.Drawing.Point(858, 271);
            this.cbb_装运条件.Name = "cbb_装运条件";
            this.cbb_装运条件.Size = new System.Drawing.Size(121, 21);
            this.cbb_装运条件.TabIndex = 27;
            // 
            // ckb_相关代理业务
            // 
            this.ckb_相关代理业务.AutoSize = true;
            this.ckb_相关代理业务.Location = new System.Drawing.Point(426, 277);
            this.ckb_相关代理业务.Name = "ckb_相关代理业务";
            this.ckb_相关代理业务.Size = new System.Drawing.Size(98, 17);
            this.ckb_相关代理业务.TabIndex = 26;
            this.ckb_相关代理业务.Text = "相关代理业务";
            this.ckb_相关代理业务.UseVisualStyleBackColor = true;
            // 
            // ckb_相关价格确定
            // 
            this.ckb_相关价格确定.AutoSize = true;
            this.ckb_相关价格确定.Location = new System.Drawing.Point(426, 250);
            this.ckb_相关价格确定.Name = "ckb_相关价格确定";
            this.ckb_相关价格确定.Size = new System.Drawing.Size(194, 17);
            this.ckb_相关价格确定.TabIndex = 25;
            this.ckb_相关价格确定.Text = "相关价格确定（删除层次结构）";
            this.ckb_相关价格确定.UseVisualStyleBackColor = true;
            // 
            // ckb_准许折扣
            // 
            this.ckb_准许折扣.AutoSize = true;
            this.ckb_准许折扣.Location = new System.Drawing.Point(426, 223);
            this.ckb_准许折扣.Name = "ckb_准许折扣";
            this.ckb_准许折扣.Size = new System.Drawing.Size(74, 17);
            this.ckb_准许折扣.TabIndex = 24;
            this.ckb_准许折扣.Text = "准许折扣";
            this.ckb_准许折扣.UseVisualStyleBackColor = true;
            // 
            // ckb_允许重新评估
            // 
            this.ckb_允许重新评估.AutoSize = true;
            this.ckb_允许重新评估.Location = new System.Drawing.Point(426, 196);
            this.ckb_允许重新评估.Name = "ckb_允许重新评估";
            this.ckb_允许重新评估.Size = new System.Drawing.Size(98, 17);
            this.ckb_允许重新评估.TabIndex = 23;
            this.ckb_允许重新评估.Text = "允许重新评估";
            this.ckb_允许重新评估.UseVisualStyleBackColor = true;
            // 
            // cbb_控制参数文件
            // 
            this.cbb_控制参数文件.FormattingEnabled = true;
            this.cbb_控制参数文件.Location = new System.Drawing.Point(717, 170);
            this.cbb_控制参数文件.Name = "cbb_控制参数文件";
            this.cbb_控制参数文件.Size = new System.Drawing.Size(121, 21);
            this.cbb_控制参数文件.TabIndex = 22;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(423, 173);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(123, 13);
            this.label15.TabIndex = 21;
            this.label15.Text = "PROACT控制参数文件";
            // 
            // txb_排序标准
            // 
            this.txb_排序标准.Location = new System.Drawing.Point(777, 136);
            this.txb_排序标准.Name = "txb_排序标准";
            this.txb_排序标准.Size = new System.Drawing.Size(202, 19);
            this.txb_排序标准.TabIndex = 20;
            // 
            // cbb_排序标准
            // 
            this.cbb_排序标准.FormattingEnabled = true;
            this.cbb_排序标准.Location = new System.Drawing.Point(717, 136);
            this.cbb_排序标准.Name = "cbb_排序标准";
            this.cbb_排序标准.Size = new System.Drawing.Size(43, 21);
            this.cbb_排序标准.TabIndex = 19;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(423, 139);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(55, 13);
            this.label14.TabIndex = 18;
            this.label14.Text = "排序标准";
            // 
            // cbb_输入管理处
            // 
            this.cbb_输入管理处.FormattingEnabled = true;
            this.cbb_输入管理处.Location = new System.Drawing.Point(717, 99);
            this.cbb_输入管理处.Name = "cbb_输入管理处";
            this.cbb_输入管理处.Size = new System.Drawing.Size(121, 21);
            this.cbb_输入管理处.TabIndex = 17;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(423, 102);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(67, 13);
            this.label13.TabIndex = 16;
            this.label13.Text = "输入管理处";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(423, 68);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(55, 13);
            this.label12.TabIndex = 15;
            this.label12.Text = "运输方式";
            // 
            // cbb_运输方式
            // 
            this.cbb_运输方式.FormattingEnabled = true;
            this.cbb_运输方式.Location = new System.Drawing.Point(717, 65);
            this.cbb_运输方式.Name = "cbb_运输方式";
            this.cbb_运输方式.Size = new System.Drawing.Size(43, 21);
            this.cbb_运输方式.TabIndex = 14;
            // 
            // cbb_ABC标识
            // 
            this.cbb_ABC标识.FormattingEnabled = true;
            this.cbb_ABC标识.Location = new System.Drawing.Point(717, 32);
            this.cbb_ABC标识.Name = "cbb_ABC标识";
            this.cbb_ABC标识.Size = new System.Drawing.Size(43, 21);
            this.cbb_ABC标识.TabIndex = 13;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(423, 38);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(52, 13);
            this.label11.TabIndex = 12;
            this.label11.Text = "ABC标识";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.dtp_计划交货时间);
            this.groupBox5.Controls.Add(this.cbb_舍入参数文件);
            this.groupBox5.Controls.Add(this.label21);
            this.groupBox5.Controls.Add(this.cbb_计量单位组);
            this.groupBox5.Controls.Add(this.label20);
            this.groupBox5.Controls.Add(this.cbb_确认控制);
            this.groupBox5.Controls.Add(this.label19);
            this.groupBox5.Controls.Add(this.label18);
            this.groupBox5.Controls.Add(this.cbb_采购组);
            this.groupBox5.Controls.Add(this.label17);
            this.groupBox5.Location = new System.Drawing.Point(10, 865);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(1165, 139);
            this.groupBox5.TabIndex = 12;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "缺省数据物料";
            // 
            // dtp_计划交货时间
            // 
            this.dtp_计划交货时间.Location = new System.Drawing.Point(170, 72);
            this.dtp_计划交货时间.Name = "dtp_计划交货时间";
            this.dtp_计划交货时间.Size = new System.Drawing.Size(200, 19);
            this.dtp_计划交货时间.TabIndex = 10;
            // 
            // cbb_舍入参数文件
            // 
            this.cbb_舍入参数文件.FormattingEnabled = true;
            this.cbb_舍入参数文件.Location = new System.Drawing.Point(657, 70);
            this.cbb_舍入参数文件.Name = "cbb_舍入参数文件";
            this.cbb_舍入参数文件.Size = new System.Drawing.Size(121, 21);
            this.cbb_舍入参数文件.TabIndex = 9;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(423, 70);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(79, 13);
            this.label21.TabIndex = 8;
            this.label21.Text = "舍入参数文件";
            // 
            // cbb_计量单位组
            // 
            this.cbb_计量单位组.FormattingEnabled = true;
            this.cbb_计量单位组.Location = new System.Drawing.Point(657, 30);
            this.cbb_计量单位组.Name = "cbb_计量单位组";
            this.cbb_计量单位组.Size = new System.Drawing.Size(121, 21);
            this.cbb_计量单位组.TabIndex = 7;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(423, 33);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(67, 13);
            this.label20.TabIndex = 6;
            this.label20.Text = "计量单位组";
            // 
            // cbb_确认控制
            // 
            this.cbb_确认控制.FormattingEnabled = true;
            this.cbb_确认控制.Location = new System.Drawing.Point(170, 110);
            this.cbb_确认控制.Name = "cbb_确认控制";
            this.cbb_确认控制.Size = new System.Drawing.Size(121, 21);
            this.cbb_确认控制.TabIndex = 5;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(10, 110);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(55, 13);
            this.label19.TabIndex = 4;
            this.label19.Text = "确认控制";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(6, 70);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(79, 13);
            this.label18.TabIndex = 2;
            this.label18.Text = "计划交货时间";
            // 
            // cbb_采购组
            // 
            this.cbb_采购组.FormattingEnabled = true;
            this.cbb_采购组.Location = new System.Drawing.Point(170, 30);
            this.cbb_采购组.Name = "cbb_采购组";
            this.cbb_采购组.Size = new System.Drawing.Size(121, 21);
            this.cbb_采购组.TabIndex = 1;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(10, 33);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(43, 13);
            this.label17.TabIndex = 0;
            this.label17.Text = "采购组";
            // 
            // toolStripExecutionMonitorSetting
            // 
            this.toolStripExecutionMonitorSetting.ImageScalingSize = new System.Drawing.Size(40, 40);
            this.toolStripExecutionMonitorSetting.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton1,
            this.toolStripButton7,
            this.toolStripButton8,
            this.toolStripButton4});
            this.toolStripExecutionMonitorSetting.Location = new System.Drawing.Point(0, 0);
            this.toolStripExecutionMonitorSetting.Name = "toolStripExecutionMonitorSetting";
            this.toolStripExecutionMonitorSetting.Size = new System.Drawing.Size(1177, 64);
            this.toolStripExecutionMonitorSetting.TabIndex = 24;
            this.toolStripExecutionMonitorSetting.Text = "toolStrip1";
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(72, 61);
            this.toolStripButton1.Text = "选择供应商";
            this.toolStripButton1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // toolStripButton7
            // 
            this.toolStripButton7.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton7.Image")));
            this.toolStripButton7.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton7.Name = "toolStripButton7";
            this.toolStripButton7.Size = new System.Drawing.Size(44, 61);
            this.toolStripButton7.Text = "刷新";
            this.toolStripButton7.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton7.Click += new System.EventHandler(this.toolStripButton7_Click);
            // 
            // toolStripButton8
            // 
            this.toolStripButton8.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton8.Name = "toolStripButton8";
            this.toolStripButton8.Size = new System.Drawing.Size(36, 61);
            this.toolStripButton8.Text = "关闭";
            this.toolStripButton8.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton8.Click += new System.EventHandler(this.toolStripButton8_Click);
            // 
            // toolStripButton4
            // 
            this.toolStripButton4.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton4.Name = "toolStripButton4";
            this.toolStripButton4.Size = new System.Drawing.Size(36, 61);
            this.toolStripButton4.Text = "确定";
            this.toolStripButton4.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton4.Click += new System.EventHandler(this.toolStripButton4_Click);
            // 
            // TextBox_采购组织名称
            // 
            this.TextBox_采购组织名称.Location = new System.Drawing.Point(360, 124);
            this.TextBox_采购组织名称.Name = "TextBox_采购组织名称";
            this.TextBox_采购组织名称.Size = new System.Drawing.Size(208, 19);
            this.TextBox_采购组织名称.TabIndex = 25;
            // 
            // SPORG
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1194, 750);
            this.Controls.Add(this.TextBox_采购组织名称);
            this.Controls.Add(this.toolStripExecutionMonitorSetting);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.cbb_采购组织编码);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cbb_供应商名称);
            this.Controls.Add(this.cbb_供应商编码);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "SPORG";
            this.Text = "采购组织视图";
            this.Load += new System.EventHandler(this.SPORG_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.toolStripExecutionMonitorSetting.ResumeLayout(false);
            this.toolStripExecutionMonitorSetting.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.ComboBox cbb_供应商名称;
        public System.Windows.Forms.ComboBox cbb_供应商编码;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        public System.Windows.Forms.ComboBox cbb_采购组织编码;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txb_定价日期控制;
        private System.Windows.Forms.ComboBox cbb_定价日期控制;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txb_方案组;
        private System.Windows.Forms.ComboBox cbb_方案组;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cbb_最小订单值;
        private System.Windows.Forms.ComboBox cbb_贸易条件;
        private System.Windows.Forms.ComboBox cbb_付款条件;
        private System.Windows.Forms.ComboBox cbb_订单货币;
        private System.Windows.Forms.ComboBox cbb_订单优化限制;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox cbb_供应商累计;
        private System.Windows.Forms.CheckBox ckb_基于收货的发票验证;
        private System.Windows.Forms.CheckBox ckb_自动评估结算交货;
        private System.Windows.Forms.CheckBox ckb_自动评估结算保留;
        private System.Windows.Forms.CheckBox ckb_回执需求;
        private System.Windows.Forms.CheckBox ckb_自动建立采购订单;
        private System.Windows.Forms.CheckBox ckb_后续结算;
        private System.Windows.Forms.CheckBox ckb_后续结算索引;
        private System.Windows.Forms.CheckBox ckb_所需业务量比较;
        private System.Windows.Forms.CheckBox ckb_单据索引有效的;
        private System.Windows.Forms.CheckBox ckb_退货供应商;
        private System.Windows.Forms.CheckBox ckb_基于服务的开票校验;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ComboBox cbb_装运条件;
        private System.Windows.Forms.CheckBox ckb_相关代理业务;
        private System.Windows.Forms.CheckBox ckb_相关价格确定;
        private System.Windows.Forms.CheckBox ckb_准许折扣;
        private System.Windows.Forms.CheckBox ckb_允许重新评估;
        private System.Windows.Forms.ComboBox cbb_控制参数文件;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txb_排序标准;
        private System.Windows.Forms.ComboBox cbb_排序标准;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox cbb_输入管理处;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox cbb_运输方式;
        private System.Windows.Forms.ComboBox cbb_ABC标识;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.ComboBox cbb_计量单位组;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.ComboBox cbb_确认控制;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.ComboBox cbb_采购组;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.ComboBox cbb_舍入参数文件;
        private System.Windows.Forms.DateTimePicker dtp_计划交货时间;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.ToolStrip toolStripExecutionMonitorSetting;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripButton toolStripButton7;
        private System.Windows.Forms.ToolStripButton toolStripButton8;
        private System.Windows.Forms.ToolStripButton toolStripButton4;
        private System.Windows.Forms.TextBox TextBox_采购组织名称;
        private System.Windows.Forms.TextBox TextBox_Message;
    }
}