﻿using Lib.IDAL.MDIDAL.SP;
using Lib.Model.MD.SP;
using Lib.SqlServerDAL.MDDAL.SP;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MMClient.MD.SP
{
    public partial class UpdateNS : Form
    {
        NSIDAL updateNSIDAL = new NSDAL();
        Number_segement number_Segement = new Number_segement();
        Boolean finalflag, beginflag, endflag,statusflag;
        public UpdateNS(string mNID)
        {
            number_Segement = updateNSIDAL.selectbyID(mNID);
            InitializeComponent();
            textBox1.Text = number_Segement.Number_ID;
            textBox2.Text = number_Segement.Description;
            if (number_Segement.Status.ToString().Trim().Equals("内部"))
            {
                radioButton1.Checked = true;
                textBox3.Text = number_Segement.NS_Begin.ToString();
                textBox4.Text = number_Segement.NS_End.ToString();
                textBox5.Text = number_Segement.NS_status.ToString();
            }
            else
            {
                radioButton2.Checked = true;
                textBox3.Enabled = false;
                textBox4.Enabled = false;
                textBox5.Enabled = false;
            }

            button1.Enabled = false;
            textBox1.Enabled = false;
            radioButton1.Enabled = false;
            radioButton2.Enabled = false;
        }

        

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            number_Segement.Description = textBox2.Text.ToString();
        }

        private bool finalcheck() {
            if (number_Segement.Status.Equals("内部"))
            {
                if (!beginflag)
                {
                    MessageBox.Show("开始号码段格式错误");
                    return false;
                }

                if (!endflag)
                {
                    MessageBox.Show("结束号码段格式错误");
                    return false;
                }

                if (!(int.Parse(number_Segement.NS_End.ToString()) >= int.Parse(number_Segement.NS_Begin)))
                {
                    MessageBox.Show("错误:结束号码段<开始号码段");
                    return false;
                }

                if ((int.Parse(number_Segement.NS_End.ToString()) > 999 || (int.Parse(number_Segement.NS_Begin) < 0)))
                {
                    MessageBox.Show("错误:请输入0-999");
                    return false;
                }

                if (!statusflag) {
                    MessageBox.Show("错误：号码段状态格式错误");
                    return false;
                }

                if ((int.Parse(number_Segement.NS_status.ToString()) > int.Parse(number_Segement.NS_End.ToString())) || (int.Parse(number_Segement.NS_status.ToString()) < int.Parse(number_Segement.NS_Begin.ToString())))
                {
                    MessageBox.Show("错误:号码段状态小于开始断或者大于结束断");
                    return false;
                }

            }

            return true;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            finalflag = finalcheck();
            if(finalflag)
            {
                string message = updateNSIDAL.update(number_Segement);
                MessageBox.Show(message);
                this.Close();
            }
        }

        private void textBox5_TextChanged(object sender, EventArgs e)
        {
            int NS_status;
            this.statusflag = int.TryParse(textBox5.Text.ToString().Trim(), out NS_status);
            number_Segement.NS_status = NS_status.ToString();
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            int begin;
            this.beginflag = int.TryParse(textBox3.Text.ToString().Trim(), out begin);
            number_Segement.NS_Begin = begin.ToString();
            if(int.Parse(number_Segement.NS_Begin)>int.Parse(number_Segement.NS_status))
            number_Segement.NS_status = begin.ToString();
        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {
            int end;
            this.endflag = int.TryParse(textBox4.Text.ToString().Trim(), out end);
            number_Segement.NS_End = end.ToString();
        }
    }
}
