﻿using Lib.IDAL.MDIDAL.SP;
using Lib.Model.MD.SP;
using Lib.SqlServerDAL.MDDAL.SP;
using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Data;
using Lib.Common.CommonUtils;
using Lib.SqlServerDAL;

namespace MMClient.MD.SP
{
    public partial class CreateCode : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        string Supplier_loginaccount_type,Supplier_name,AccountGroupSinglestatus,AccountGroupName,Floginaccount,lwstatus;
        string Companyname,Buyername;
        string fCompanyname,fCompanyCode,fBuyername,fBuyerID;
        int llstatus;
        CreateCodeIDAL createCodeIDAL = new CreateCodeDAL();
        Account_GroupIDAL account_groupIDAL = new Account_GroupDAL();
        NSIDAL nSIDAL = new NSDAL();
        Boolean onceflag,buyerflag,companyflag;

        public CreateCode()
        {
            InitializeComponent();
            comboBox筛选供应商.SelectedItem="已有账号供应商";
            comboBox筛选账户组.SelectedItem ="一次性供应商";
            Supplier_name = "null";
            AccountGroupName = "null";
            Companyname = "null";
            Buyername = "null";
            button创建账户.Enabled = false;
        }

        private void comboBox筛选供应商_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox筛选供应商.SelectedItem.Equals("已有账号供应商"))
            {
                Supplier_loginaccount_type = "not null";
            }
            else if (comboBox筛选供应商.SelectedItem.Equals("暂无账号供应商"))
            {
                Supplier_loginaccount_type = "null";
            }
        }

        private void comboBox筛选账户组_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox筛选账户组.SelectedItem.Equals("一次性供应商"))
            {
                AccountGroupSinglestatus = "是";
            }
            else if (comboBox筛选账户组.SelectedItem.Equals("非一次性供应商")) {
                AccountGroupSinglestatus = "否";
            }
        }

        private void 查询供应商Button_Click(object sender, EventArgs e)
        {
            DataTable supplierinfodt = createCodeIDAL.getsupplierinfo(Supplier_name, Supplier_loginaccount_type);
            dataGridView_选择供应商.DataSource = supplierinfodt;
        }

        private void button查询账户组_Click(object sender, EventArgs e)
        {
            DataTable AccountGroupdt = account_groupIDAL.getAccountGroup(AccountGroupName, AccountGroupSinglestatus);
            dataGridView_选择账户组.DataSource = AccountGroupdt;
        }

        private void textbox查询供应商_TextChanged(object sender, EventArgs e)
        {
            if (textbox查询供应商.Text.ToString().Trim().Equals("") || textbox查询供应商.Text == null)
            {
                Supplier_name = "null";
            }
            else {
                Supplier_name = textbox查询供应商.Text.ToString().Trim();
            }
        }

        private void textBox5_TextChanged(object sender, EventArgs e)
        {
            if (textBox5.Text.ToString().Trim().Equals("") || textBox5.Text == null)
            {
                Buyername = "null";
            }
            else
            {
                Buyername = textBox5.Text.ToString().Trim();
            }
        }

        private void button查询采购组织_Click(object sender, EventArgs e)
        {
            DataTable Buyerdt = createCodeIDAL.getbuyernamecode(Buyername);
            dataGridView_选择采购组织.DataSource = Buyerdt;
        }

        private void button查询公司代码_Click(object sender, EventArgs e)
        {
            DataTable companydt = createCodeIDAL.getcompanynamecode(Companyname);
            dataGridView_选择公司代码.DataSource = companydt;
        }

        private void dataGridView_选择公司代码_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                button创建账户.Enabled = false;
                int currentIndex = getCurrentSelectedRowIndex("选择公司代码");
                DataGridViewRow row = this.dataGridView_选择公司代码.Rows[currentIndex];
                fCompanyCode= textBox公司代码结果.Text = Convert.ToString(row.Cells["Company_ID"].Value);
                fCompanyname= TextBox公司名称结果.Text = Convert.ToString(row.Cells["Company_Name"].Value);
                companyflag = true;
            }
            catch
            {

            }
        }

        private void dataGridView_选择采购组织_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                button创建账户.Enabled = false;
                int currentIndex = getCurrentSelectedRowIndex("选择采购组织");
                DataGridViewRow row = this.dataGridView_选择采购组织.Rows[currentIndex];
                fBuyerID = textBox采购组织结果.Text = Convert.ToString(row.Cells["Buyer_Org"].Value);
                fBuyername = textBox采购组织名称结果.Text = Convert.ToString(row.Cells["Buyer_Org_Name"].Value);
                buyerflag = true;
            }
            catch
            {

            }
        }

        private void dataGridView_选择账户组_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                button创建账户.Enabled = false;
                int currentIndex = getCurrentSelectedRowIndex("选择账户组");
                DataGridViewRow row = this.dataGridView_选择账户组.Rows[currentIndex];
                TextBox账户组ID.Text = Convert.ToString(row.Cells["Account_Group_ID"].Value);
                textBox账户组描述.Text = Convert.ToString(row.Cells["Description"].Value);
                TextBox一次性供应商标识.Text = Convert.ToString(row.Cells["SingleStatus"].Value);
                textBox号码段ID.Text = Convert.ToString(row.Cells["NS_id"].Value);
                lwstatus = Convert.ToString(row.Cells["Status"].Value);
                if (lwstatus.Equals("内部"))
                {
                    llstatus = Convert.ToInt16(row.Cells["NS_status"].Value);
                    Floginaccount = TextBox账户组ID.Text.ToString() + llstatus.ToString();
                    Textbox供应商账号.Text = Floginaccount;
                    textBox外部输入框.Enabled = false;
                    textBox外部输入框.Text = "";
                    label外部.Text = "";
                }
                else if (lwstatus.Equals("外部")) {
                    textBox外部输入框.Enabled = true;
                    label外部.Text = "";
                }
            }
            catch
            {

            }
        }

        private void textBox公司名称_TextChanged(object sender, EventArgs e)
        {
            if (textBox公司名称.Text.ToString().Trim().Equals("") || textBox公司名称.Text == null)
            {
                Companyname = "null";
            }
            else
            {
                Companyname = textBox公司名称.Text.ToString().Trim();
            }
        }

        private void button创建账户_Click(object sender, EventArgs e)
        {
            string message, message1, message2, message3;


            try
            {
                string sql1 = " UPDATE Supplier_MainAccount SET LoginAccount = '" + Floginaccount + "' WHERE Supplier_Id='" + textbox供应商流水号.Text.ToString() + "'";
                string sql2 = " UPDATE Supplier_Base set Supplier_AccountGroup = '" +textBox账户组描述.Text.ToString()+ "' where Supplier_ID='" +textbox供应商流水号.Text.ToString()+ "'";
                string sql3 = "";
                string sql4 = "";
                string sql5 = "";
                string addTrans = "";
                int count = 2;
                if (lwstatus.Equals("内部"))
                {
                    llstatus = llstatus + 1;
                     sql3 = " UPDATE Number_Segment SET NS_status = '" + llstatus.ToString() + "' where Number_ID = '" +textBox号码段ID.Text.ToString()+ "'";
                    addTrans+= " begin " + sql3 + " if(@@error<>0) rollback tran else ";
                    count++;
                }
                if(companyflag)
                {
                    sql4 =  " INSERT INTO Supplier_CompanyCode (Company_ID,Company_Name,Supplier_ID,Supplier_Name) VALUES('" + fCompanyCode + "','" + fCompanyname + "','" + textbox供应商流水号.Text.ToString() + "','" + textbox供应商名称结果.Text.ToString() + "')";
                    addTrans += " begin " + sql4 +" if(@@error<>0) rollback tran else ";
                    count++;
                }               
                if (buyerflag)
                {                 
                    sql5 = " INSERT INTO Supplier_Purchasing_Org (PurchasingORG_ID,PurchasingORG_Name,Supplier_ID,Supplier_Name) VALUES('" + fBuyerID + "','" + fBuyername + "','" + textbox供应商流水号.Text.ToString() + "','" + textbox供应商名称结果.Text.ToString() + "')";
                    addTrans += " begin " + sql5+ " if(@@error<>0) rollback tran else  ";
                    count++;
                }
                string end ="" ;
                for(int i = 0;i<count-1;i++)
                {
                    end += "  end   ";
                }
                string transSQL = " begin tran" + sql1 + " if(@@error<>0) rollback tran else begin" + sql2 + " if(@@error<>0) rollback tran else"+ addTrans + "commit tran " + end;
                DBHelper.ExecuteNonQuery(transSQL);
                MessageBox.Show("更新成功");
            }
            catch(Exception ex)
            {
                MessageBox.Show("更新失败,公司代码或采购组织已存在相关信息,请转到具体页面进行修改!");
                companyflag = false;
                buyerflag = false;
            }


        }

        private void button检测账户唯一性_Click(object sender, EventArgs e)
        {
            onceflag = createCodeIDAL.checkonce(Floginaccount);
            if (onceflag)
            {
                MessageBox.Show("账号唯一");
                button创建账户.Enabled = true;
            }
            else
            {
                MessageBox.Show("账号不唯一");
                button创建账户.Enabled = false;
            }
        }

        private void textBox外部输入框_TextChanged(object sender, EventArgs e)
        {
            if (textBox外部输入框.Text.ToString().Trim().Length != 3)
            {
                label外部.Text = "请输入长度为3的字符";
                button创建账户.Enabled = false;
            }
            else
            {
                Floginaccount = TextBox账户组ID.Text.ToString() + textBox外部输入框.Text.ToString();
                Textbox供应商账号.Text = Floginaccount;
                label外部.Text = "";
                button检测账户唯一性.Enabled = true;
            }
        }

        private void textBox账户组名称_TextChanged(object sender, EventArgs e)
        {
            if (textBox账户组名称.Text.ToString().Trim().Equals("") || textBox账户组名称.Text == null)
            {
                AccountGroupName = "null";
            }
            else
            {
                AccountGroupName = textBox账户组名称.Text.ToString().Trim();
            }
        }

        private void dataGridView_选择供应商_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                button创建账户.Enabled = false;
                int currentIndex = getCurrentSelectedRowIndex("选择供应商");
                DataGridViewRow row = this.dataGridView_选择供应商.Rows[currentIndex];
                textbox供应商流水号.Text = Convert.ToString(row.Cells["Supplier_Id"].Value);
                textbox供应商名称结果.Text = Convert.ToString(row.Cells["SupplierName"].Value);
                if (row.Cells["LoginAccount"].Value != null)
                {
                    Floginaccount=Textbox供应商账号.Text = Convert.ToString(row.Cells["LoginAccount"].Value);
                }
                button查询账户组_Click(sender, e);
            }
            catch {

            }
        }

        

        private int getCurrentSelectedRowIndex(string type)
        {
            if (type.Equals("选择供应商"))
            {
                if (this.dataGridView_选择供应商.Rows.Count <= 0)
                {
                    MessageUtil.ShowWarning("供应商为空，无法执行操作");
                    return -1;
                }

                if (this.dataGridView_选择供应商.CurrentRow.Index < 0)
                {
                    MessageUtil.ShowWarning("请选择一行记录，然后进行操作！");
                    return -1;
                }
                return this.dataGridView_选择供应商.CurrentRow.Index;
            }

            if (type.Equals("选择账户组"))
            {
                if (this.dataGridView_选择账户组.Rows.Count <= 0)
                {
                    MessageUtil.ShowWarning("账户组为空，无法执行操作");
                    return -1;
                }

                if (this.dataGridView_选择账户组.CurrentRow.Index < 0)
                {
                    MessageUtil.ShowWarning("请选择一行记录，然后进行操作！");
                    return -1;
                }
                return this.dataGridView_选择账户组.CurrentRow.Index;
            }

            if (type.Equals("选择公司代码"))
            {
                if (this.dataGridView_选择公司代码.Rows.Count <= 0)
                {
                    MessageUtil.ShowWarning("公司代码为空，无法执行操作");
                    return -1;
                }

                if (this.dataGridView_选择公司代码.CurrentRow.Index < 0)
                {
                    MessageUtil.ShowWarning("请选择一行记录，然后进行操作！");
                    return -1;
                }
                return this.dataGridView_选择公司代码.CurrentRow.Index;
            }

            if (type.Equals("选择采购组织"))
            {
                if (this.dataGridView_选择采购组织.Rows.Count <= 0)
                {
                    MessageUtil.ShowWarning("采购组织为空，无法执行操作");
                    return -1;
                }

                if (this.dataGridView_选择采购组织.CurrentRow.Index < 0)
                {
                    MessageUtil.ShowWarning("请选择一行记录，然后进行操作！");
                    return -1;
                }
                return this.dataGridView_选择采购组织.CurrentRow.Index;
            }

            return 0;
        }



    }

       
}
