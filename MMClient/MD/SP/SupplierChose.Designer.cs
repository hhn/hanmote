﻿namespace MMClient.MD.SP
{
    partial class SupplierChose
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.supplierInforGroupBox = new System.Windows.Forms.GroupBox();
            this.supplierChoseToolStrip = new System.Windows.Forms.ToolStrip();
            this.closeButton = new System.Windows.Forms.ToolStripButton();
            this.certainButton = new System.Windows.Forms.ToolStripButton();
            this.supplierLabel = new System.Windows.Forms.Label();
            this.supplierTextBox = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.querySupplierGroupBox = new System.Windows.Forms.GroupBox();
            this.pageTool = new pager.pagetool.pageNext();
            this.supplierInforDataGridView = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.supplierInforGroupBox.SuspendLayout();
            this.supplierChoseToolStrip.SuspendLayout();
            this.querySupplierGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.supplierInforDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // supplierInforGroupBox
            // 
            this.supplierInforGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.supplierInforGroupBox.Controls.Add(this.supplierInforDataGridView);
            this.supplierInforGroupBox.Location = new System.Drawing.Point(2, 139);
            this.supplierInforGroupBox.Margin = new System.Windows.Forms.Padding(2);
            this.supplierInforGroupBox.Name = "supplierInforGroupBox";
            this.supplierInforGroupBox.Padding = new System.Windows.Forms.Padding(2);
            this.supplierInforGroupBox.Size = new System.Drawing.Size(951, 502);
            this.supplierInforGroupBox.TabIndex = 0;
            this.supplierInforGroupBox.TabStop = false;
            this.supplierInforGroupBox.Text = "供应商信息";
            // 
            // supplierChoseToolStrip
            // 
            this.supplierChoseToolStrip.ImageScalingSize = new System.Drawing.Size(40, 40);
            this.supplierChoseToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.closeButton,
            this.certainButton});
            this.supplierChoseToolStrip.Location = new System.Drawing.Point(0, 0);
            this.supplierChoseToolStrip.Name = "supplierChoseToolStrip";
            this.supplierChoseToolStrip.Size = new System.Drawing.Size(962, 25);
            this.supplierChoseToolStrip.TabIndex = 20;
            this.supplierChoseToolStrip.Text = "toolStrip1";
            // 
            // closeButton
            // 
            this.closeButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.closeButton.Name = "closeButton";
            this.closeButton.Size = new System.Drawing.Size(36, 22);
            this.closeButton.Text = "关闭";
            this.closeButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.closeButton.Click += new System.EventHandler(this.toolStripButton8_Click);
            // 
            // certainButton
            // 
            this.certainButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.certainButton.Name = "certainButton";
            this.certainButton.Size = new System.Drawing.Size(36, 22);
            this.certainButton.Text = "确定";
            this.certainButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.certainButton.Click += new System.EventHandler(this.toolStripButton4_Click);
            // 
            // supplierLabel
            // 
            this.supplierLabel.AutoSize = true;
            this.supplierLabel.Location = new System.Drawing.Point(4, 26);
            this.supplierLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.supplierLabel.Name = "supplierLabel";
            this.supplierLabel.Size = new System.Drawing.Size(41, 12);
            this.supplierLabel.TabIndex = 0;
            this.supplierLabel.Text = "供应商";
            // 
            // supplierTextBox
            // 
            this.supplierTextBox.Location = new System.Drawing.Point(65, 23);
            this.supplierTextBox.Margin = new System.Windows.Forms.Padding(2);
            this.supplierTextBox.Name = "supplierTextBox";
            this.supplierTextBox.Size = new System.Drawing.Size(109, 21);
            this.supplierTextBox.TabIndex = 1;
            this.supplierTextBox.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(734, 76);
            this.button1.Margin = new System.Windows.Forms.Padding(2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(92, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "选择供应商";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // querySupplierGroupBox
            // 
            this.querySupplierGroupBox.Controls.Add(this.button1);
            this.querySupplierGroupBox.Controls.Add(this.supplierTextBox);
            this.querySupplierGroupBox.Controls.Add(this.supplierLabel);
            this.querySupplierGroupBox.Location = new System.Drawing.Point(2, 79);
            this.querySupplierGroupBox.Margin = new System.Windows.Forms.Padding(2);
            this.querySupplierGroupBox.Name = "querySupplierGroupBox";
            this.querySupplierGroupBox.Padding = new System.Windows.Forms.Padding(2);
            this.querySupplierGroupBox.Size = new System.Drawing.Size(946, 55);
            this.querySupplierGroupBox.TabIndex = 1;
            this.querySupplierGroupBox.TabStop = false;
            this.querySupplierGroupBox.Text = "查询供应商";
            // 
            // pageTool
            // 
            this.pageTool.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.pageTool.Location = new System.Drawing.Point(104, 646);
            this.pageTool.Name = "pageTool";
            this.pageTool.PageIndex = 1;
            this.pageTool.PageSize = 10;
            this.pageTool.RecordCount = 0;
            this.pageTool.Size = new System.Drawing.Size(664, 37);
            this.pageTool.TabIndex = 21;
            this.pageTool.Load += new System.EventHandler(this.PageTool_Load);
            // 
            // supplierInforDataGridView
            // 
            this.supplierInforDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.supplierInforDataGridView.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.supplierInforDataGridView.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.supplierInforDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.supplierInforDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column5,
            this.Column6,
            this.Column7,
            this.Column8,
            this.Column9});
            this.supplierInforDataGridView.Location = new System.Drawing.Point(6, 19);
            this.supplierInforDataGridView.Name = "supplierInforDataGridView";
            this.supplierInforDataGridView.RowTemplate.Height = 23;
            this.supplierInforDataGridView.Size = new System.Drawing.Size(940, 478);
            this.supplierInforDataGridView.TabIndex = 0;
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "Supplier_ID";
            this.Column1.HeaderText = "供应商编码";
            this.Column1.Name = "Column1";
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "Supplier_AccountGroup";
            this.Column2.HeaderText = "供应商账户组";
            this.Column2.Name = "Column2";
            // 
            // Column3
            // 
            this.Column3.DataPropertyName = "Supplier_Name";
            this.Column3.HeaderText = "供应商名称";
            this.Column3.Name = "Column3";
            // 
            // Column4
            // 
            this.Column4.DataPropertyName = "Supplier_Title";
            this.Column4.HeaderText = "供应商标题";
            this.Column4.Name = "Column4";
            // 
            // Column5
            // 
            this.Column5.DataPropertyName = "Nation";
            this.Column5.HeaderText = "所在国家";
            this.Column5.Name = "Column5";
            // 
            // Column6
            // 
            this.Column6.DataPropertyName = "Supplier_OName";
            this.Column6.HeaderText = "供应商原名称";
            this.Column6.Name = "Column6";
            // 
            // Column7
            // 
            this.Column7.DataPropertyName = "Client";
            this.Column7.HeaderText = "客户";
            this.Column7.Name = "Column7";
            // 
            // Column8
            // 
            this.Column8.DataPropertyName = "Trade_Partner";
            this.Column8.HeaderText = "贸易伙伴";
            this.Column8.Name = "Column8";
            // 
            // Column9
            // 
            this.Column9.DataPropertyName = "Group_Code";
            this.Column9.HeaderText = "组代码";
            this.Column9.Name = "Column9";
            // 
            // SupplierChose
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(962, 695);
            this.Controls.Add(this.pageTool);
            this.Controls.Add(this.supplierChoseToolStrip);
            this.Controls.Add(this.querySupplierGroupBox);
            this.Controls.Add(this.supplierInforGroupBox);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "SupplierChose";
            this.Text = "选择供应商";
            this.Load += new System.EventHandler(this.SupplierChose_Load);
            this.supplierInforGroupBox.ResumeLayout(false);
            this.supplierChoseToolStrip.ResumeLayout(false);
            this.supplierChoseToolStrip.PerformLayout();
            this.querySupplierGroupBox.ResumeLayout(false);
            this.querySupplierGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.supplierInforDataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox supplierInforGroupBox;
        private System.Windows.Forms.ToolStrip supplierChoseToolStrip;
        private System.Windows.Forms.ToolStripButton closeButton;
        private System.Windows.Forms.ToolStripButton certainButton;
        private System.Windows.Forms.Label supplierLabel;
        private System.Windows.Forms.TextBox supplierTextBox;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.GroupBox querySupplierGroupBox;
        private pager.pagetool.pageNext pageTool;
        private System.Windows.Forms.DataGridView supplierInforDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
    }
}