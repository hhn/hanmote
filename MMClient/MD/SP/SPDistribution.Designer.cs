﻿namespace MMClient.MD
{
    partial class SPDistribution
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox18 = new System.Windows.Forms.GroupBox();
            this.textBox37 = new System.Windows.Forms.TextBox();
            this.label108 = new System.Windows.Forms.Label();
            this.button34 = new System.Windows.Forms.Button();
            this.button35 = new System.Windows.Forms.Button();
            this.listBox59 = new System.Windows.Forms.ListBox();
            this.label109 = new System.Windows.Forms.Label();
            this.listBox60 = new System.Windows.Forms.ListBox();
            this.label110 = new System.Windows.Forms.Label();
            this.listBox61 = new System.Windows.Forms.ListBox();
            this.label111 = new System.Windows.Forms.Label();
            this.listBox62 = new System.Windows.Forms.ListBox();
            this.label112 = new System.Windows.Forms.Label();
            this.listBox63 = new System.Windows.Forms.ListBox();
            this.label113 = new System.Windows.Forms.Label();
            this.listBox64 = new System.Windows.Forms.ListBox();
            this.label114 = new System.Windows.Forms.Label();
            this.listBox65 = new System.Windows.Forms.ListBox();
            this.label115 = new System.Windows.Forms.Label();
            this.groupBox20 = new System.Windows.Forms.GroupBox();
            this.dataGridView10 = new System.Windows.Forms.DataGridView();
            this.groupBox19 = new System.Windows.Forms.GroupBox();
            this.dataGridView9 = new System.Windows.Forms.DataGridView();
            this.groupBox18.SuspendLayout();
            this.groupBox20.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView10)).BeginInit();
            this.groupBox19.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView9)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox18
            // 
            this.groupBox18.Controls.Add(this.textBox37);
            this.groupBox18.Controls.Add(this.label108);
            this.groupBox18.Controls.Add(this.button34);
            this.groupBox18.Controls.Add(this.button35);
            this.groupBox18.Controls.Add(this.listBox59);
            this.groupBox18.Controls.Add(this.label109);
            this.groupBox18.Controls.Add(this.listBox60);
            this.groupBox18.Controls.Add(this.label110);
            this.groupBox18.Controls.Add(this.listBox61);
            this.groupBox18.Controls.Add(this.label111);
            this.groupBox18.Controls.Add(this.listBox62);
            this.groupBox18.Controls.Add(this.label112);
            this.groupBox18.Controls.Add(this.listBox63);
            this.groupBox18.Controls.Add(this.label113);
            this.groupBox18.Controls.Add(this.listBox64);
            this.groupBox18.Controls.Add(this.label114);
            this.groupBox18.Controls.Add(this.listBox65);
            this.groupBox18.Controls.Add(this.label115);
            this.groupBox18.Location = new System.Drawing.Point(38, 12);
            this.groupBox18.Name = "groupBox18";
            this.groupBox18.Size = new System.Drawing.Size(631, 219);
            this.groupBox18.TabIndex = 21;
            this.groupBox18.TabStop = false;
            this.groupBox18.Text = "条件选择";
            // 
            // textBox37
            // 
            this.textBox37.Location = new System.Drawing.Point(459, 170);
            this.textBox37.Name = "textBox37";
            this.textBox37.Size = new System.Drawing.Size(117, 21);
            this.textBox37.TabIndex = 35;
            // 
            // label108
            // 
            this.label108.AutoSize = true;
            this.label108.Location = new System.Drawing.Point(344, 170);
            this.label108.Name = "label108";
            this.label108.Size = new System.Drawing.Size(65, 12);
            this.label108.TabIndex = 34;
            this.label108.Text = "最大命中数";
            // 
            // button34
            // 
            this.button34.Location = new System.Drawing.Point(231, 170);
            this.button34.Name = "button34";
            this.button34.Size = new System.Drawing.Size(75, 23);
            this.button34.TabIndex = 33;
            this.button34.Text = "清除";
            this.button34.UseVisualStyleBackColor = true;
            // 
            // button35
            // 
            this.button35.Location = new System.Drawing.Point(96, 170);
            this.button35.Name = "button35";
            this.button35.Size = new System.Drawing.Size(75, 23);
            this.button35.TabIndex = 32;
            this.button35.Text = "查询";
            this.button35.UseVisualStyleBackColor = true;
            // 
            // listBox59
            // 
            this.listBox59.FormattingEnabled = true;
            this.listBox59.ItemHeight = 12;
            this.listBox59.Location = new System.Drawing.Point(154, 148);
            this.listBox59.Name = "listBox59";
            this.listBox59.Size = new System.Drawing.Size(120, 16);
            this.listBox59.TabIndex = 31;
            // 
            // label109
            // 
            this.label109.AutoSize = true;
            this.label109.Location = new System.Drawing.Point(40, 148);
            this.label109.Name = "label109";
            this.label109.Size = new System.Drawing.Size(29, 12);
            this.label109.TabIndex = 30;
            this.label109.Text = "其他";
            // 
            // listBox60
            // 
            this.listBox60.FormattingEnabled = true;
            this.listBox60.ItemHeight = 12;
            this.listBox60.Location = new System.Drawing.Point(459, 105);
            this.listBox60.Name = "listBox60";
            this.listBox60.Size = new System.Drawing.Size(120, 16);
            this.listBox60.TabIndex = 29;
            // 
            // label110
            // 
            this.label110.AutoSize = true;
            this.label110.Location = new System.Drawing.Point(345, 105);
            this.label110.Name = "label110";
            this.label110.Size = new System.Drawing.Size(17, 12);
            this.label110.TabIndex = 28;
            this.label110.Text = "至";
            // 
            // listBox61
            // 
            this.listBox61.FormattingEnabled = true;
            this.listBox61.ItemHeight = 12;
            this.listBox61.Location = new System.Drawing.Point(154, 105);
            this.listBox61.Name = "listBox61";
            this.listBox61.Size = new System.Drawing.Size(120, 16);
            this.listBox61.TabIndex = 27;
            // 
            // label111
            // 
            this.label111.AutoSize = true;
            this.label111.Location = new System.Drawing.Point(40, 105);
            this.label111.Name = "label111";
            this.label111.Size = new System.Drawing.Size(65, 12);
            this.label111.TabIndex = 26;
            this.label111.Text = "供应商级别";
            // 
            // listBox62
            // 
            this.listBox62.FormattingEnabled = true;
            this.listBox62.ItemHeight = 12;
            this.listBox62.Location = new System.Drawing.Point(459, 60);
            this.listBox62.Name = "listBox62";
            this.listBox62.Size = new System.Drawing.Size(120, 16);
            this.listBox62.TabIndex = 25;
            // 
            // label112
            // 
            this.label112.AutoSize = true;
            this.label112.Location = new System.Drawing.Point(345, 60);
            this.label112.Name = "label112";
            this.label112.Size = new System.Drawing.Size(17, 12);
            this.label112.TabIndex = 24;
            this.label112.Text = "至";
            // 
            // listBox63
            // 
            this.listBox63.FormattingEnabled = true;
            this.listBox63.ItemHeight = 12;
            this.listBox63.Location = new System.Drawing.Point(154, 60);
            this.listBox63.Name = "listBox63";
            this.listBox63.Size = new System.Drawing.Size(120, 16);
            this.listBox63.TabIndex = 23;
            // 
            // label113
            // 
            this.label113.AutoSize = true;
            this.label113.Location = new System.Drawing.Point(40, 60);
            this.label113.Name = "label113";
            this.label113.Size = new System.Drawing.Size(65, 12);
            this.label113.TabIndex = 22;
            this.label113.Text = "供应商名称";
            // 
            // listBox64
            // 
            this.listBox64.FormattingEnabled = true;
            this.listBox64.ItemHeight = 12;
            this.listBox64.Location = new System.Drawing.Point(459, 20);
            this.listBox64.Name = "listBox64";
            this.listBox64.Size = new System.Drawing.Size(120, 16);
            this.listBox64.TabIndex = 21;
            // 
            // label114
            // 
            this.label114.AutoSize = true;
            this.label114.Location = new System.Drawing.Point(345, 20);
            this.label114.Name = "label114";
            this.label114.Size = new System.Drawing.Size(17, 12);
            this.label114.TabIndex = 20;
            this.label114.Text = "至";
            // 
            // listBox65
            // 
            this.listBox65.FormattingEnabled = true;
            this.listBox65.ItemHeight = 12;
            this.listBox65.Location = new System.Drawing.Point(154, 20);
            this.listBox65.Name = "listBox65";
            this.listBox65.Size = new System.Drawing.Size(120, 16);
            this.listBox65.TabIndex = 19;
            // 
            // label115
            // 
            this.label115.AutoSize = true;
            this.label115.Location = new System.Drawing.Point(40, 20);
            this.label115.Name = "label115";
            this.label115.Size = new System.Drawing.Size(65, 12);
            this.label115.TabIndex = 18;
            this.label115.Text = "供应商编码";
            // 
            // groupBox20
            // 
            this.groupBox20.Controls.Add(this.dataGridView10);
            this.groupBox20.Location = new System.Drawing.Point(34, 480);
            this.groupBox20.Name = "groupBox20";
            this.groupBox20.Size = new System.Drawing.Size(634, 163);
            this.groupBox20.TabIndex = 20;
            this.groupBox20.TabStop = false;
            this.groupBox20.Text = "分发的子系统或者单位";
            // 
            // dataGridView10
            // 
            this.dataGridView10.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView10.Location = new System.Drawing.Point(0, 12);
            this.dataGridView10.Name = "dataGridView10";
            this.dataGridView10.RowTemplate.Height = 23;
            this.dataGridView10.Size = new System.Drawing.Size(628, 150);
            this.dataGridView10.TabIndex = 0;
            // 
            // groupBox19
            // 
            this.groupBox19.Controls.Add(this.dataGridView9);
            this.groupBox19.Location = new System.Drawing.Point(34, 243);
            this.groupBox19.Name = "groupBox19";
            this.groupBox19.Size = new System.Drawing.Size(636, 231);
            this.groupBox19.TabIndex = 19;
            this.groupBox19.TabStop = false;
            this.groupBox19.Text = "查询结果";
            // 
            // dataGridView9
            // 
            this.dataGridView9.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView9.Location = new System.Drawing.Point(7, 21);
            this.dataGridView9.Name = "dataGridView9";
            this.dataGridView9.RowTemplate.Height = 23;
            this.dataGridView9.Size = new System.Drawing.Size(623, 204);
            this.dataGridView9.TabIndex = 0;
            // 
            // SPDistribution
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(937, 750);
            this.Controls.Add(this.groupBox18);
            this.Controls.Add(this.groupBox20);
            this.Controls.Add(this.groupBox19);
            this.Name = "SPDistribution";
            this.Text = "SPDistribution";
            this.groupBox18.ResumeLayout(false);
            this.groupBox18.PerformLayout();
            this.groupBox20.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView10)).EndInit();
            this.groupBox19.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView9)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox18;
        private System.Windows.Forms.TextBox textBox37;
        private System.Windows.Forms.Label label108;
        private System.Windows.Forms.Button button34;
        private System.Windows.Forms.Button button35;
        private System.Windows.Forms.ListBox listBox59;
        private System.Windows.Forms.Label label109;
        private System.Windows.Forms.ListBox listBox60;
        private System.Windows.Forms.Label label110;
        private System.Windows.Forms.ListBox listBox61;
        private System.Windows.Forms.Label label111;
        private System.Windows.Forms.ListBox listBox62;
        private System.Windows.Forms.Label label112;
        private System.Windows.Forms.ListBox listBox63;
        private System.Windows.Forms.Label label113;
        private System.Windows.Forms.ListBox listBox64;
        private System.Windows.Forms.Label label114;
        private System.Windows.Forms.ListBox listBox65;
        private System.Windows.Forms.Label label115;
        private System.Windows.Forms.GroupBox groupBox20;
        private System.Windows.Forms.DataGridView dataGridView10;
        private System.Windows.Forms.GroupBox groupBox19;
        private System.Windows.Forms.DataGridView dataGridView9;
    }
}