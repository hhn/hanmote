﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.SqlServerDAL;
using Lib.Bll;
using MMClient.MD.SP;
using Lib.Bll.MDBll.General;
using Lib.Bll.MDBll.SP;
using Lib.Model.MD.SP;
using Lib.Common.CommonUtils;
using Lib.Common.MMCException.IDAL;

namespace MMClient.MD.SP
{
    public partial class SPORG : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        public SPORG()
        {
            InitializeComponent();
        }
        GeneralBLL gn = new GeneralBLL();
        ComboBoxItem cbm = new ComboBoxItem();
        SupplierPurchaseORGBLL spo = new SupplierPurchaseORGBLL();
        SupplierBaseBLL spb = new SupplierBaseBLL();
        string supplier_id, supplier_name;

        private void cbb_采购组织编码_SelectedIndexChanged(object sender, EventArgs e)
        {
            string Buyer_Org = this.cbb_采购组织编码.Text;
            string Buyer_OrgName = gn.GetBuyer_OrgNameByBuyer_Org(Buyer_Org);
            this.TextBox_采购组织名称.Text = Buyer_OrgName;
            List<string> list = spb.GetAllSupplierID();
            if (cbb_供应商编码.Text == "" || gn.IDInTheList(list, cbb_供应商编码.Text) == true)
            {
                MessageUtil.ShowError("请选择正确的供应商编码");
            }


            //填充采购组下拉列表
            List<string> BuyerGroupList = gn.GetAllBuyerGroupByBuyer_Org(Buyer_Org);
            FormUtils.FillCombox(this.cbb_采购组, BuyerGroupList);
        }
    
        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            SupplierChose spc = new SupplierChose(this);
            spc.Show();
        }

        private void toolStripButton4_Click(object sender, EventArgs e)
        {
            //确认按钮
            try
            {
                string sql = "select * from Supplier_Purchasing_Org where Supplier_ID = '" + supplier_id + "'";
                DataTable dt = DBHelper.ExecuteQueryDT(sql);
                if (dt != null && dt.Rows.Count > 0)
                {
                    sql = " UPDATE [Supplier_Purchasing_Org] SET Order_Units='" + cbb_订单货币.Text + "',Payment_Clause='" + cbb_付款条件.Text + "',Trade_Clause='" + cbb_贸易条件.Text + "',MIN_OrderValue='" + cbb_最小订单值.Text + "',Supplier_Group='" + cbb_方案组.Text + "',PricingDate_Control='" + cbb_定价日期控制.Text + "',Order_Limit='" + cbb_订单优化限制.Text + "',Supplier_Has='" + cbb_供应商累计.Text + "',Invoice_Verification='" + ckb_基于收货的发票验证.Checked + "',Delivery_Settlement='" + ckb_自动评估结算交货.Checked + "',Settlement_Reserves='" + ckb_自动评估结算保留.Checked + "',ReturnReceipt_Requirements='" + ckb_回执需求.Checked + "',Automatic_PurchaseOrder='" + ckb_自动建立采购订单.Checked + "',Subsequent_Settlement='" + ckb_后续结算.Checked + "',Settlement_Index='" + ckb_后续结算索引.Checked + "',Business_Comparison='" + ckb_所需业务量比较.Checked + "',Document_Indexin='" + ckb_单据索引有效的.Checked + "',Return_Supplier='" + ckb_退货供应商.Checked + "',Service_Verification='" + ckb_基于服务的开票校验.Checked + "',Allow_Reevaluation='" + ckb_允许重新评估.Checked + "',Allow_Discount='" + ckb_准许折扣.Checked + "',Price_Determination='" + ckb_相关价格确定.Checked + "',Agent_Service='" + ckb_相关代理业务.Checked + "',Shipment_Terms='" + cbb_装运条件.Text + "',ABC_Identify='" + cbb_ABC标识.Text + "',Transportation_Mode='" + cbb_运输方式.Text + "',Input_ManagementOffice='" + cbb_输入管理处.Text + "',Sort_Criteria='" + cbb_排序标准.Text + "',PRO_ControlData='" + cbb_控制参数文件.Text + "',Buyer_Group='" + cbb_采购组.Text + "',Delivery_Time='" + dtp_计划交货时间.Value + "',Control_Confirm='" + cbb_确认控制.Text + "',Measurment_Group='" + cbb_计量单位组.Text + "',Rounding_Parameters='" + cbb_舍入参数文件.Text + "' WHERE PurchasingORG_ID ='" + cbb_采购组织编码.Text + "'";
                    DBHelper.ExecuteNonQuery(sql);
                    MessageBox.Show("维护成功");
                }
                else
                {
                    sql = " INSERT INTO [Supplier_Purchasing_Org](PurchasingORG_ID,PurchasingORG_Name,Order_Units,Payment_Clause,Trade_Clause,MIN_OrderValue,Supplier_Group,PricingDate_Control,Order_Limit,Supplier_Has,Invoice_Verification,Delivery_Settlement,Settlement_Reserves,ReturnReceipt_Requirements,Automatic_PurchaseOrder,Subsequent_Settlement,Settlement_Index,Business_Comparison,Document_Indexin,Return_Supplier,Service_Verification,Allow_Reevaluation,Allow_Discount,Price_Determination,Agent_Service,Shipment_Terms,ABC_Identify,Transportation_Mode,Input_ManagementOffice,Sort_Criteria,PRO_ControlData,Buyer_Group,Delivery_Time,Control_Confirm,Measurment_Group,Rounding_Parameters,Supplier_ID,Supplier_Name) Values ('" + cbb_采购组织编码.Text + "','" + TextBox_采购组织名称.Text + "','" + cbb_订单货币.Text + "','" + cbb_付款条件.Text + "','" + cbb_贸易条件.Text + "','" + cbb_最小订单值.Text + "','" + cbb_方案组.Text + "','" + cbb_定价日期控制.Text + "','" + cbb_订单优化限制.Text + "','" + cbb_供应商累计.Text + "','" + ckb_基于收货的发票验证.Checked + "','" + ckb_自动评估结算交货.Checked + "','" + ckb_自动评估结算保留.Checked + "','" + ckb_回执需求.Checked + "','" + ckb_自动建立采购订单.Checked + "','" + ckb_后续结算.Checked + "','" + ckb_后续结算索引.Checked + "','" + ckb_所需业务量比较.Checked + "','" + ckb_单据索引有效的.Checked + "','" + ckb_退货供应商.Checked + "','" + ckb_基于服务的开票校验.Checked + "','" + ckb_允许重新评估.Checked + "','" + ckb_准许折扣.Checked + "','" + ckb_相关价格确定.Checked + "','" + ckb_相关代理业务.Checked + "','" + cbb_装运条件.Text + "','" + cbb_ABC标识.Text + "','" + cbb_运输方式.Text + "','" + cbb_输入管理处.Text + "','" + cbb_排序标准.Text + "','" + cbb_控制参数文件.Text + "','" + cbb_采购组.Text + "','" + dtp_计划交货时间.Value + "','" + cbb_确认控制.Text + "','" + cbb_计量单位组.Text + "','" + cbb_舍入参数文件.Text + "','"+supplier_id+"','"+supplier_name+"')";
                    DBHelper.ExecuteNonQuery(sql);
                    MessageBox.Show("创建成功");
                }
            }
            catch (Lib.Common.MMCException.IDAL.DBException ex)
            {
                Console.WriteLine(ex.StackTrace);
                MessageUtil.ShowError(ex.Message);
            }
               
        }

        private void toolStripButton8_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void toolStripButton7_Click(object sender, EventArgs e)
        {
            foreach (Control col in this.Controls)
            {
                if (col.GetType().Name.Equals("ComboBox"))
                {
                    ((ComboBox)col).Text = string.Empty;
                }
                if (col.GetType().Name.Equals("CheckBox"))
                {
                    ((CheckBox)col).Text = string.Empty;
                }
            }
            cbb_订单货币.Text = "";
            cbb_订单货币.Text = "";
            cbb_供应商名称.Text = "";
            cbb_订单货币.Text = "";
            TextBox_采购组织名称.Text = "";
            cbb_付款条件.Text = "";
            cbb_贸易条件.Text = "";
            cbb_最小订单值.Text = "";
            cbb_方案组.Text = "";
            cbb_定价日期控制.Text = "";
            cbb_订单优化限制.Text = "";
            cbb_供应商累计.Text = "";


            ckb_基于收货的发票验证.Checked = false;


            ckb_自动评估结算交货.Checked = false;

            ckb_自动评估结算保留.Checked = false;

            ckb_回执需求.Checked = false;

            ckb_自动建立采购订单.Checked = false;

            ckb_后续结算.Checked = false;

            ckb_后续结算索引.Checked = false;

            ckb_所需业务量比较.Checked = false;

            ckb_单据索引有效的.Checked = false;

            ckb_退货供应商.Checked = false;

            ckb_基于服务的开票校验.Checked = false;

            ckb_允许重新评估.Checked = false;

            ckb_准许折扣.Checked = false;

            ckb_相关价格确定.Checked = false;

            ckb_相关代理业务.Checked = false;

            cbb_装运条件.Text = "";
            cbb_ABC标识.Text = "";
            cbb_运输方式.Text = "";
            cbb_输入管理处.Text = "";
            cbb_排序标准.Text = "";
            cbb_控制参数文件.Text = "";
            cbb_采购组.Text = "";
            dtp_计划交货时间.Text = "";
            cbb_确认控制.Text = "";
            cbb_计量单位组.Text = "";
            cbb_舍入参数文件.Text = "";
        }
        private void cbb_付款条件_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.TextBox_Message.Text = null;
                string sql = "select Description from Payment_Clause where Clause_ID = '" + this.cbb_付款条件.Text + "'";
                DataTable dt = DBHelper.ExecuteQueryDT(sql);
                if (dt != null && dt.Rows.Count > 0)
                {
                    this.TextBox_Message.Text = dt.Rows[0][0].ToString();
                }
            }
            catch (DBException ex)
            {
                Console.WriteLine(ex.StackTrace);
                MessageUtil.ShowError(ex.Message);
            }
        }

        private void cbb_供应商编码_TextChanged(object sender, EventArgs e)
        {
            supplier_id = cbb_供应商编码.Text.ToString();
        }

        private void cbb_供应商名称_TextChanged(object sender, EventArgs e)
        {
            supplier_name = cbb_供应商名称.Text.ToString();
        }

        private void SPORG_Load(object sender, EventArgs e)
        {
            //Supplier_Purchasing_Org表
            //加载采购组织编码列表
            string sql = "SELECT Buyer_Org AS 'id', Buyer_Org_Name AS 'name'  FROM [Buyer_Org]";
            List<string> BuyerOrganizationList = gn.GetAllBuyerOrganization();
            if(BuyerOrganizationList != null)
                FormUtils.FillCombox(this.cbb_采购组织编码, BuyerOrganizationList);
            //this.cbb_采购组织编码.DataSource = DBHelper.ExecuteQueryDT(sql);
            //this.cbb_采购组织编码.DisplayMember = "name";
            //this.cbb_采购组织编码.ValueMember = "id";
            
            //加载货币列表
            List<string> currencyList = gn.GetAllCurrency();
            FormUtils.FillCombox(cbb_订单货币, currencyList);


            //加载付款条件列表
            List<string> paymentClauseNameList = gn.GetAllPaymentClauseName();
            FormUtils.FillCombox(cbb_付款条件, paymentClauseNameList);

            //加载贸易条件列表
            List<string> TradeClauseList = gn.GetAllTradeClause();
            FormUtils.FillCombox(cbb_贸易条件, TradeClauseList);
        }


    }
}
