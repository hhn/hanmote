﻿using Lib.Common.CommonUtils;
using Lib.Common.MMCException.Bll;
using Lib.Common.MMCException.IDAL;
using Lib.SqlServerDAL;
using MMClient.MD.SP;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MMClient.MD
{
    public partial class MaintenAg : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        private UpdateAG updateAG = null;

        public MaintenAg()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            createAG createAG = new createAG();
            createAG.Show();
            LoadData();
        }

        private void pageTool_Load(object sender, EventArgs e)
        {
            string sql = "select count(*) from Account_Group";
            try
            {
                int i = int.Parse(DBHelper.ExecuteQueryDT(sql).Rows[0][0].ToString());
                int totalNum = i;
                pageTool.DrawControl(totalNum);
                //事件改变调用函数
                pageTool.OnPageChanged += pageTool_OnPageChanged;
            }
            catch (Lib.Common.MMCException.Bll.BllException ex)
            {
                MessageUtil.ShowTips("数据库异常，请稍后重试");
                return;
            }
        }

        private void pageTool_OnPageChanged(object sender, EventArgs e)
        {
            LoadData();
        }

        private void LoadData()
        {
            try
            {
                dataGridView1.DataSource = FindBigclassfyInforByPage(this.pageTool.PageSize, this.pageTool.PageIndex);
            }
            catch (BllException ex)
            {
                MessageUtil.ShowError(ex.Message);
            }
        }

        private DataTable FindBigclassfyInforByPage(int pageSize, int pageIndex)
        {
            try
            {
                string sql = @"SELECT top " + pageSize + " Account_Group_ID,Description,SingleStatus,NS_id FROM Account_Group  where Account_Group_ID not in(select top " + pageSize * (pageIndex - 1) + " Account_Group_ID from Account_Group ORDER BY Account_Group_ID ASC)ORDER BY Account_Group_ID";
                return DBHelper.ExecuteQueryDT(sql);
            }
            catch (DBException ex)
            {
                throw new BllException("当前无数据", ex);
            };
        }

        private void dataGridView1_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            int currentIndex = getCurrentSelectedRowIndex();
            DataGridViewRow currentRow = this.dataGridView1.Rows[currentIndex];
            string Account_Group_ID = currentRow.Cells["AG_ID"].Value.ToString();
            if (Account_Group_ID == "" || Account_Group_ID.Equals(""))
            {
                MessageBox.Show("账户组为空，无法进入页面");
                return;
            }
            if (updateAG == null)
            {
                updateAG = new UpdateAG(Account_Group_ID);
                updateAG.Show();
                updateAG = null;
            }
            LoadData();
        }

        private void MaintenAg_Load(object sender, EventArgs e)
        {
            LoadData();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            LoadData();
            pageTool_Load(sender, e);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            int currentIndex = getCurrentSelectedRowIndex();
            DataGridViewRow row = this.dataGridView1.Rows[currentIndex];
            String Account_Group_ID = Convert.ToString(row.Cells["AG_ID"].Value);
            string sql = "DELETE from Account_Group WHERE Account_Group_ID = '" + Account_Group_ID + "' ";
            try
            {
                DBHelper.ExecuteNonQuery(sql);
                MessageBox.Show("删除成功");
                LoadData();
            }
            catch (BllException ex)
            {
                MessageUtil.ShowTips("数据库异，请稍后重试");
            }
            pageTool_Load(sender, e);
        }

        private int getCurrentSelectedRowIndex()
        {
            if (this.dataGridView1.Rows.Count <= 0)
            {
                MessageUtil.ShowWarning("账号组为空，无法执行操作");
                return -1;
            }

            if (this.dataGridView1.CurrentRow.Index < 0)
            {
                MessageUtil.ShowWarning("请选择一行记录，然后进行操作！");
                return -1;
            }
            return this.dataGridView1.CurrentRow.Index;
        }

       
    }
}
