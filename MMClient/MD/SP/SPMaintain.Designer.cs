﻿namespace MMClient.MD
{
    partial class SPMaintain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SPMaintain));
            this.label1 = new System.Windows.Forms.Label();
            this.cbb_供应商编码 = new System.Windows.Forms.ComboBox();
            this.cbb_供应商名称 = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cbb_账户组 = new System.Windows.Forms.ComboBox();
            this.label21 = new System.Windows.Forms.Label();
            this.cbb_名称 = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cbb_标题 = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.cbb_地区 = new System.Windows.Forms.ComboBox();
            this.地区 = new System.Windows.Forms.Label();
            this.cbb_国家 = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.cbb_城市 = new System.Windows.Forms.ComboBox();
            this.cbb_邮政编码 = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.cbb_门牌号 = new System.Windows.Forms.ComboBox();
            this.cbb_街道 = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label25 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.tbx_电子邮箱 = new System.Windows.Forms.TextBox();
            this.cbb_公司邮政编码 = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.cbb_邮政编码信箱 = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.cbb_邮政信箱 = new System.Windows.Forms.ComboBox();
            this.通讯 = new System.Windows.Forms.GroupBox();
            this.cbb_移动电话分机号 = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this.cbb_电话分机号 = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            this.cbb_传真 = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.cbb_移动电话 = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.cbb_电话 = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.cbb_语言 = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.tbx_所属分类 = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.cbb_行业标识 = new System.Windows.Forms.ComboBox();
            this.label22 = new System.Windows.Forms.Label();
            this.cbb_组代码 = new System.Windows.Forms.ComboBox();
            this.cbb_权限组 = new System.Windows.Forms.ComboBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.cbb_贸易伙伴 = new System.Windows.Forms.ComboBox();
            this.label18 = new System.Windows.Forms.Label();
            this.cbb_客户 = new System.Windows.Forms.ComboBox();
            this.label17 = new System.Windows.Forms.Label();
            this.toolStripExecutionMonitorSetting = new System.Windows.Forms.ToolStrip();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton7 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton8 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton4 = new System.Windows.Forms.ToolStripButton();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.通讯.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.toolStripExecutionMonitorSetting.SuspendLayout();
            this.SuspendLayout();
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 67);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(50, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "供应商";
            // 
            // cbb_供应商编码
            // 
            this.cbb_供应商编码.Enabled = false;
            this.cbb_供应商编码.FormattingEnabled = true;
            this.cbb_供应商编码.Location = new System.Drawing.Point(174, 70);
            this.cbb_供应商编码.Name = "cbb_供应商编码";
            this.cbb_供应商编码.Size = new System.Drawing.Size(174, 24);
            this.cbb_供应商编码.TabIndex = 1;
            this.cbb_供应商编码.Tag = "1";
            this.cbb_供应商编码.SelectedIndexChanged += new System.EventHandler(this.cbb_供应商编码_SelectedIndexChanged);
            // 
            // cbb_供应商名称
            // 
            this.cbb_供应商名称.Enabled = false;
            this.cbb_供应商名称.FormattingEnabled = true;
            this.cbb_供应商名称.Location = new System.Drawing.Point(354, 70);
            this.cbb_供应商名称.Name = "cbb_供应商名称";
            this.cbb_供应商名称.Size = new System.Drawing.Size(208, 24);
            this.cbb_供应商名称.TabIndex = 2;
            this.cbb_供应商名称.Tag = "2";
            // 
            // supplierInforGroupBox
            // 
            this.groupBox1.Controls.Add(this.cbb_账户组);
            this.groupBox1.Controls.Add(this.label21);
            this.groupBox1.Controls.Add(this.cbb_名称);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.cbb_标题);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(9, 100);
            this.groupBox1.Name = "supplierInforGroupBox";
            this.groupBox1.Size = new System.Drawing.Size(1171, 130);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "名称";
            // 
            // cbb_账户组
            // 
            this.cbb_账户组.FormattingEnabled = true;
            this.cbb_账户组.Items.AddRange(new object[] {
            "原材料供应商",
            "贸易产品供应商",
            "服务供应商"});
            this.cbb_账户组.Location = new System.Drawing.Point(779, 34);
            this.cbb_账户组.Name = "cbb_账户组";
            this.cbb_账户组.Size = new System.Drawing.Size(267, 24);
            this.cbb_账户组.TabIndex = 5;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(636, 41);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(50, 17);
            this.label21.TabIndex = 4;
            this.label21.Text = "账户组";
            // 
            // cbb_名称
            // 
            this.cbb_名称.FormattingEnabled = true;
            this.cbb_名称.Location = new System.Drawing.Point(174, 85);
            this.cbb_名称.Name = "cbb_名称";
            this.cbb_名称.Size = new System.Drawing.Size(267, 24);
            this.cbb_名称.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 88);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(40, 17);
            this.label3.TabIndex = 2;
            this.label3.Text = " 名称";
            // 
            // cbb_标题
            // 
            this.cbb_标题.FormattingEnabled = true;
            this.cbb_标题.Location = new System.Drawing.Point(174, 34);
            this.cbb_标题.Name = "cbb_标题";
            this.cbb_标题.Size = new System.Drawing.Size(267, 24);
            this.cbb_标题.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(10, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(36, 17);
            this.label2.TabIndex = 0;
            this.label2.Text = "标题";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.cbb_地区);
            this.groupBox2.Controls.Add(this.地区);
            this.groupBox2.Controls.Add(this.cbb_国家);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.cbb_城市);
            this.groupBox2.Controls.Add(this.cbb_邮政编码);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.cbb_门牌号);
            this.groupBox2.Controls.Add(this.cbb_街道);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Location = new System.Drawing.Point(9, 236);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1172, 178);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "街道地址";
            // 
            // cbb_地区
            // 
            this.cbb_地区.FormattingEnabled = true;
            this.cbb_地区.Location = new System.Drawing.Point(508, 141);
            this.cbb_地区.Name = "cbb_地区";
            this.cbb_地区.Size = new System.Drawing.Size(121, 24);
            this.cbb_地区.TabIndex = 11;
            // 
            // 地区
            // 
            this.地区.AutoSize = true;
            this.地区.Location = new System.Drawing.Point(407, 144);
            this.地区.Name = "地区";
            this.地区.Size = new System.Drawing.Size(36, 17);
            this.地区.TabIndex = 10;
            this.地区.Text = "地区";
            // 
            // cbb_国家
            // 
            this.cbb_国家.FormattingEnabled = true;
            this.cbb_国家.Location = new System.Drawing.Point(176, 141);
            this.cbb_国家.Name = "cbb_国家";
            this.cbb_国家.Size = new System.Drawing.Size(121, 24);
            this.cbb_国家.TabIndex = 9;
            this.cbb_国家.SelectedIndexChanged += new System.EventHandler(this.cbb_国家_SelectedIndexChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(8, 144);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(36, 17);
            this.label7.TabIndex = 8;
            this.label7.Text = "国家";
            // 
            // cbb_城市
            // 
            this.cbb_城市.FormattingEnabled = true;
            this.cbb_城市.Location = new System.Drawing.Point(356, 92);
            this.cbb_城市.Name = "cbb_城市";
            this.cbb_城市.Size = new System.Drawing.Size(274, 24);
            this.cbb_城市.TabIndex = 7;
            // 
            // cbb_邮政编码
            // 
            this.cbb_邮政编码.FormattingEnabled = true;
            this.cbb_邮政编码.Location = new System.Drawing.Point(176, 92);
            this.cbb_邮政编码.Name = "cbb_邮政编码";
            this.cbb_邮政编码.Size = new System.Drawing.Size(174, 24);
            this.cbb_邮政编码.TabIndex = 6;
            this.cbb_邮政编码.TextChanged += new System.EventHandler(this.cbb_邮政编码_TextChanged);
            this.cbb_邮政编码.Leave += new System.EventHandler(this.cbb_邮政编码_Leave);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(8, 95);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(96, 17);
            this.label6.TabIndex = 5;
            this.label6.Text = "邮政编码/城市";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(576, 47);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(22, 17);
            this.label5.TabIndex = 4;
            this.label5.Text = "号";
            // 
            // cbb_门牌号
            // 
            this.cbb_门牌号.FormattingEnabled = true;
            this.cbb_门牌号.Location = new System.Drawing.Point(449, 44);
            this.cbb_门牌号.Name = "cbb_门牌号";
            this.cbb_门牌号.Size = new System.Drawing.Size(121, 24);
            this.cbb_门牌号.TabIndex = 3;
            // 
            // cbb_街道
            // 
            this.cbb_街道.FormattingEnabled = true;
            this.cbb_街道.Location = new System.Drawing.Point(176, 44);
            this.cbb_街道.Name = "cbb_街道";
            this.cbb_街道.Size = new System.Drawing.Size(267, 24);
            this.cbb_街道.TabIndex = 2;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 47);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(82, 17);
            this.label4.TabIndex = 0;
            this.label4.Text = "街道/门牌号";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label25);
            this.groupBox3.Controls.Add(this.label23);
            this.groupBox3.Controls.Add(this.tbx_电子邮箱);
            this.groupBox3.Controls.Add(this.cbb_公司邮政编码);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this.cbb_邮政编码信箱);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.cbb_邮政信箱);
            this.groupBox3.Location = new System.Drawing.Point(9, 420);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(1171, 175);
            this.groupBox3.TabIndex = 5;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "邮政信箱地址";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(407, 45);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(64, 17);
            this.label25.TabIndex = 13;
            this.label25.Text = "电子邮箱";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(1212, 41);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(54, 17);
            this.label23.TabIndex = 7;
            this.label23.Text = "label23";
            // 
            // tbx_电子邮箱
            // 
            this.tbx_电子邮箱.Location = new System.Drawing.Point(520, 40);
            this.tbx_电子邮箱.Name = "tbx_电子邮箱";
            this.tbx_电子邮箱.Size = new System.Drawing.Size(216, 22);
            this.tbx_电子邮箱.TabIndex = 6;
            this.tbx_电子邮箱.Leave += new System.EventHandler(this.tbx_电子邮箱_Leave);
            // 
            // cbb_公司邮政编码
            // 
            this.cbb_公司邮政编码.FormattingEnabled = true;
            this.cbb_公司邮政编码.Location = new System.Drawing.Point(176, 128);
            this.cbb_公司邮政编码.Name = "cbb_公司邮政编码";
            this.cbb_公司邮政编码.Size = new System.Drawing.Size(174, 24);
            this.cbb_公司邮政编码.TabIndex = 5;
            this.cbb_公司邮政编码.TextChanged += new System.EventHandler(this.cbb_公司邮政编码_TextChanged);
            this.cbb_公司邮政编码.Leave += new System.EventHandler(this.cbb_公司邮政编码_Leave);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(12, 131);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(92, 17);
            this.label10.TabIndex = 4;
            this.label10.Text = "公司邮政编码";
            // 
            // cbb_邮政编码信箱
            // 
            this.cbb_邮政编码信箱.FormattingEnabled = true;
            this.cbb_邮政编码信箱.Location = new System.Drawing.Point(176, 82);
            this.cbb_邮政编码信箱.Name = "cbb_邮政编码信箱";
            this.cbb_邮政编码信箱.Size = new System.Drawing.Size(174, 24);
            this.cbb_邮政编码信箱.TabIndex = 3;
            this.cbb_邮政编码信箱.TextChanged += new System.EventHandler(this.cbb_邮政编码信箱_TextChanged);
            this.cbb_邮政编码信箱.Leave += new System.EventHandler(this.cbb_邮政编码信箱_Leave);
            this.cbb_邮政编码信箱.MouseLeave += new System.EventHandler(this.cbb_邮政编码信箱_MouseLeave);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(12, 85);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(64, 17);
            this.label9.TabIndex = 2;
            this.label9.Text = "邮政编码";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(12, 41);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(64, 17);
            this.label8.TabIndex = 1;
            this.label8.Text = "邮政信箱";
            // 
            // cbb_邮政信箱
            // 
            this.cbb_邮政信箱.FormattingEnabled = true;
            this.cbb_邮政信箱.Location = new System.Drawing.Point(176, 38);
            this.cbb_邮政信箱.Name = "cbb_邮政信箱";
            this.cbb_邮政信箱.Size = new System.Drawing.Size(174, 24);
            this.cbb_邮政信箱.TabIndex = 0;
            // 
            // 通讯
            // 
            this.通讯.Controls.Add(this.cbb_移动电话分机号);
            this.通讯.Controls.Add(this.label16);
            this.通讯.Controls.Add(this.cbb_电话分机号);
            this.通讯.Controls.Add(this.label15);
            this.通讯.Controls.Add(this.cbb_传真);
            this.通讯.Controls.Add(this.label14);
            this.通讯.Controls.Add(this.cbb_移动电话);
            this.通讯.Controls.Add(this.label13);
            this.通讯.Controls.Add(this.cbb_电话);
            this.通讯.Controls.Add(this.label12);
            this.通讯.Controls.Add(this.cbb_语言);
            this.通讯.Controls.Add(this.label11);
            this.通讯.Location = new System.Drawing.Point(9, 601);
            this.通讯.Name = "通讯";
            this.通讯.Size = new System.Drawing.Size(1168, 218);
            this.通讯.TabIndex = 6;
            this.通讯.TabStop = false;
            this.通讯.Text = "通讯";
            // 
            // cbb_移动电话分机号
            // 
            this.cbb_移动电话分机号.FormattingEnabled = true;
            this.cbb_移动电话分机号.Location = new System.Drawing.Point(537, 137);
            this.cbb_移动电话分机号.Name = "cbb_移动电话分机号";
            this.cbb_移动电话分机号.Size = new System.Drawing.Size(174, 24);
            this.cbb_移动电话分机号.TabIndex = 16;
            this.cbb_移动电话分机号.TextChanged += new System.EventHandler(this.cbb_移动电话分机号_TextChanged);
            this.cbb_移动电话分机号.Leave += new System.EventHandler(this.cbb_移动电话分机号_Leave);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(404, 141);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(50, 17);
            this.label16.TabIndex = 15;
            this.label16.Text = "分机号";
            // 
            // cbb_电话分机号
            // 
            this.cbb_电话分机号.FormattingEnabled = true;
            this.cbb_电话分机号.Location = new System.Drawing.Point(537, 92);
            this.cbb_电话分机号.Name = "cbb_电话分机号";
            this.cbb_电话分机号.Size = new System.Drawing.Size(174, 24);
            this.cbb_电话分机号.TabIndex = 14;
            this.cbb_电话分机号.TextChanged += new System.EventHandler(this.cbb_电话分机号_TextChanged);
            this.cbb_电话分机号.Leave += new System.EventHandler(this.cbb_电话分机号_Leave);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(404, 95);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(50, 17);
            this.label15.TabIndex = 13;
            this.label15.Text = "分机号";
            // 
            // cbb_传真
            // 
            this.cbb_传真.FormattingEnabled = true;
            this.cbb_传真.Location = new System.Drawing.Point(173, 178);
            this.cbb_传真.Name = "cbb_传真";
            this.cbb_传真.Size = new System.Drawing.Size(174, 24);
            this.cbb_传真.TabIndex = 12;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(9, 181);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(36, 17);
            this.label14.TabIndex = 11;
            this.label14.Text = "传真";
            // 
            // cbb_移动电话
            // 
            this.cbb_移动电话.FormattingEnabled = true;
            this.cbb_移动电话.Location = new System.Drawing.Point(173, 134);
            this.cbb_移动电话.Name = "cbb_移动电话";
            this.cbb_移动电话.Size = new System.Drawing.Size(174, 24);
            this.cbb_移动电话.TabIndex = 10;
            this.cbb_移动电话.TextChanged += new System.EventHandler(this.cbb_移动电话_TextChanged);
            this.cbb_移动电话.Leave += new System.EventHandler(this.cbb_移动电话_Leave);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(9, 137);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(64, 17);
            this.label13.TabIndex = 9;
            this.label13.Text = "移动电话";
            // 
            // cbb_电话
            // 
            this.cbb_电话.FormattingEnabled = true;
            this.cbb_电话.Location = new System.Drawing.Point(173, 88);
            this.cbb_电话.Name = "cbb_电话";
            this.cbb_电话.Size = new System.Drawing.Size(174, 24);
            this.cbb_电话.TabIndex = 8;
            this.cbb_电话.TextChanged += new System.EventHandler(this.cbb_电话_TextChanged);
            this.cbb_电话.Leave += new System.EventHandler(this.cbb_电话_Leave);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(9, 95);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(36, 17);
            this.label12.TabIndex = 7;
            this.label12.Text = "电话";
            // 
            // cbb_语言
            // 
            this.cbb_语言.FormattingEnabled = true;
            this.cbb_语言.Location = new System.Drawing.Point(173, 44);
            this.cbb_语言.Name = "cbb_语言";
            this.cbb_语言.Size = new System.Drawing.Size(174, 24);
            this.cbb_语言.TabIndex = 6;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(9, 47);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(36, 17);
            this.label11.TabIndex = 0;
            this.label11.Text = "语言";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.tbx_所属分类);
            this.groupBox4.Controls.Add(this.label24);
            this.groupBox4.Controls.Add(this.cbb_行业标识);
            this.groupBox4.Controls.Add(this.label22);
            this.groupBox4.Controls.Add(this.cbb_组代码);
            this.groupBox4.Controls.Add(this.cbb_权限组);
            this.groupBox4.Controls.Add(this.label20);
            this.groupBox4.Controls.Add(this.label19);
            this.groupBox4.Controls.Add(this.cbb_贸易伙伴);
            this.groupBox4.Controls.Add(this.label18);
            this.groupBox4.Controls.Add(this.cbb_客户);
            this.groupBox4.Controls.Add(this.label17);
            this.groupBox4.Location = new System.Drawing.Point(9, 825);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(1165, 122);
            this.groupBox4.TabIndex = 7;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "科目控制";
            // 
            // tbx_所属分类
            // 
            this.tbx_所属分类.Enabled = false;
            this.tbx_所属分类.Location = new System.Drawing.Point(972, 93);
            this.tbx_所属分类.Name = "tbx_所属分类";
            this.tbx_所属分类.Size = new System.Drawing.Size(121, 22);
            this.tbx_所属分类.TabIndex = 23;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(868, 96);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(64, 17);
            this.label24.TabIndex = 22;
            this.label24.Text = "所属分类";
            // 
            // cbb_行业标识
            // 
            this.cbb_行业标识.FormattingEnabled = true;
            this.cbb_行业标识.Location = new System.Drawing.Point(972, 42);
            this.cbb_行业标识.Name = "cbb_行业标识";
            this.cbb_行业标识.Size = new System.Drawing.Size(121, 24);
            this.cbb_行业标识.TabIndex = 21;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(868, 45);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(64, 17);
            this.label22.TabIndex = 20;
            this.label22.Text = "行业标示";
            // 
            // cbb_组代码
            // 
            this.cbb_组代码.FormattingEnabled = true;
            this.cbb_组代码.Location = new System.Drawing.Point(618, 93);
            this.cbb_组代码.Name = "cbb_组代码";
            this.cbb_组代码.Size = new System.Drawing.Size(174, 24);
            this.cbb_组代码.TabIndex = 19;
            // 
            // cbb_权限组
            // 
            this.cbb_权限组.FormattingEnabled = true;
            this.cbb_权限组.Location = new System.Drawing.Point(618, 42);
            this.cbb_权限组.Name = "cbb_权限组";
            this.cbb_权限组.Size = new System.Drawing.Size(174, 24);
            this.cbb_权限组.TabIndex = 18;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(471, 96);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(50, 17);
            this.label20.TabIndex = 17;
            this.label20.Text = "组代码";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(471, 45);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(50, 17);
            this.label19.TabIndex = 16;
            this.label19.Text = "权限组";
            // 
            // cbb_贸易伙伴
            // 
            this.cbb_贸易伙伴.FormattingEnabled = true;
            this.cbb_贸易伙伴.Location = new System.Drawing.Point(170, 93);
            this.cbb_贸易伙伴.Name = "cbb_贸易伙伴";
            this.cbb_贸易伙伴.Size = new System.Drawing.Size(174, 24);
            this.cbb_贸易伙伴.TabIndex = 15;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(6, 96);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(64, 17);
            this.label18.TabIndex = 14;
            this.label18.Text = "贸易伙伴";
            // 
            // cbb_客户
            // 
            this.cbb_客户.FormattingEnabled = true;
            this.cbb_客户.Location = new System.Drawing.Point(170, 42);
            this.cbb_客户.Name = "cbb_客户";
            this.cbb_客户.Size = new System.Drawing.Size(174, 24);
            this.cbb_客户.TabIndex = 13;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(6, 45);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(36, 17);
            this.label17.TabIndex = 0;
            this.label17.Text = "客户";
            // 
            // toolStripExecutionMonitorSetting
            // 
            this.toolStripExecutionMonitorSetting.ImageScalingSize = new System.Drawing.Size(40, 40);
            this.toolStripExecutionMonitorSetting.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton1,
            this.toolStripButton7,
            this.toolStripButton8,
            this.toolStripButton4});
            this.toolStripExecutionMonitorSetting.Location = new System.Drawing.Point(0, 0);
            this.toolStripExecutionMonitorSetting.Name = "toolStripExecutionMonitorSetting";
            this.toolStripExecutionMonitorSetting.Size = new System.Drawing.Size(1223, 67);
            this.toolStripExecutionMonitorSetting.TabIndex = 23;
            this.toolStripExecutionMonitorSetting.Text = "toolStrip1";
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(88, 64);
            this.toolStripButton1.Text = "选择供应商";
            this.toolStripButton1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // toolStripButton7
            // 
            this.toolStripButton7.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton7.Image")));
            this.toolStripButton7.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton7.Name = "toolStripButton7";
            this.toolStripButton7.Size = new System.Drawing.Size(44, 64);
            this.toolStripButton7.Text = "刷新";
            this.toolStripButton7.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton7.Click += new System.EventHandler(this.toolStripButton7_Click);
            // 
            // closeButton
            // 
            this.toolStripButton8.Image = ((System.Drawing.Image)(resources.GetObject("closeButton.Image")));
            this.toolStripButton8.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton8.Name = "closeButton";
            this.toolStripButton8.Size = new System.Drawing.Size(44, 64);
            this.toolStripButton8.Text = "关闭";
            this.toolStripButton8.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton8.Click += new System.EventHandler(this.toolStripButton8_Click);
            // 
            // certainButton
            // 
            this.toolStripButton4.Image = ((System.Drawing.Image)(resources.GetObject("certainButton.Image")));
            this.toolStripButton4.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton4.Name = "certainButton";
            this.toolStripButton4.Size = new System.Drawing.Size(44, 64);
            this.toolStripButton4.Text = "确定";
            this.toolStripButton4.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton4.Click += new System.EventHandler(this.toolStripButton4_Click);
            // 
            // SPMaintain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1244, 601);
            this.Controls.Add(this.toolStripExecutionMonitorSetting);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.通讯);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.cbb_供应商名称);
            this.Controls.Add(this.cbb_供应商编码);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "SPMaintain";
            this.Text = "基本视图";
            this.Load += new System.EventHandler(this.SPMaintain_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.通讯.ResumeLayout(false);
            this.通讯.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.toolStripExecutionMonitorSetting.ResumeLayout(false);
            this.toolStripExecutionMonitorSetting.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox cbb_名称;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cbb_标题;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cbb_门牌号;
        private System.Windows.Forms.ComboBox cbb_街道;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cbb_地区;
        private System.Windows.Forms.Label 地区;
        private System.Windows.Forms.ComboBox cbb_国家;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cbb_城市;
        private System.Windows.Forms.ComboBox cbb_邮政编码;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ComboBox cbb_公司邮政编码;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox cbb_邮政编码信箱;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cbb_邮政信箱;
        private System.Windows.Forms.GroupBox 通讯;
        private System.Windows.Forms.ComboBox cbb_传真;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox cbb_移动电话;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox cbb_电话;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox cbb_语言;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox cbb_移动电话分机号;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ComboBox cbb_电话分机号;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.ComboBox cbb_组代码;
        private System.Windows.Forms.ComboBox cbb_权限组;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.ComboBox cbb_贸易伙伴;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.ComboBox cbb_客户;
        private System.Windows.Forms.Label label17;
        public System.Windows.Forms.ComboBox cbb_供应商编码;
        public System.Windows.Forms.ComboBox cbb_供应商名称;
        public System.Windows.Forms.ComboBox cbb_账户组;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.ComboBox cbb_行业标识;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox tbx_电子邮箱;
        private System.Windows.Forms.TextBox tbx_所属分类;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.ToolStrip toolStripExecutionMonitorSetting;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripButton toolStripButton7;
        private System.Windows.Forms.ToolStripButton toolStripButton8;
        private System.Windows.Forms.ToolStripButton toolStripButton4;
    }
}