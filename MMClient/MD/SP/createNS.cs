﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Lib.IDAL.MDIDAL.SP;
using Lib.Model.MD.SP;
using Lib.SqlServerDAL.MDDAL.SP;

namespace MMClient.MD.SP
{
    public partial class createNS : Form
    {
        Number_segement number_Segement = new Number_segement();
        NSIDAL createNSIDAL = new NSDAL();
        Boolean finalflag,onceflag,beginflag,endflag;

        public createNS()
        {
            InitializeComponent();
            init();
            this.button1.Enabled = false;
        }

        public void init() {
            this.textBox2.Enabled = false;
            this.textBox3.Enabled = false;
            this.textBox4.Enabled = false;
            this.radioButton1.Enabled = false;
            this.radioButton2.Enabled = false;
            this.button2.Enabled = false;
        }


        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            init();
            if (textBox1.Text.ToString().Trim().Equals("") || textBox1.Text == null)
            {
                this.button1.Enabled = false;
                MessageBox.Show("请输入正确的ID");
            } else if(textBox1.Text.ToString().Trim().Length>3){
                label10.Text = "请输入不大于3位的ID";
                button1.Enabled = false;
            }
            else {
                label10.Text = "";
                number_Segement.Number_ID = textBox1.Text.ToString();
                this.button1.Enabled = true;
            }
            
        }


        private void button1_Click(object sender, EventArgs e)
        {
            onceflag = createNSIDAL.CheckID(number_Segement.Number_ID.ToString());
            if (onceflag)
            {
                this.textBox2.Enabled = true;
                this.textBox3.Enabled = true;
                this.textBox4.Enabled = true;
                this.radioButton1.Enabled = true;
                this.radioButton2.Enabled = true;
                this.button2.Enabled = true;
                MessageBox.Show("ID唯一");
            }
            else {
                MessageBox.Show("ID冲突,请修改ID");
            }
            
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            this.number_Segement.Status = "外部";
            textBox3.Enabled = false;
            textBox4.Enabled = false;
            this.beginflag = true;
            this.endflag = true;
            this.number_Segement.NS_Begin ="null";
            this.number_Segement.NS_End = "null";
            this.number_Segement.NS_status = "null";
            
            
        }


        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            this.number_Segement.Status = "内部";
            textBox3.Enabled = true;
            textBox4.Enabled = true;
            this.beginflag = false;
            this.endflag = false;
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            this.number_Segement.Description = textBox2.Text.ToString();
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            int begin;
            this.beginflag = int.TryParse(textBox3.Text.ToString().Trim(), out begin);
            number_Segement.NS_Begin = begin.ToString();
            number_Segement.NS_status = begin.ToString();
        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {
            int end;
            this.endflag = int.TryParse(textBox4.Text.ToString().Trim(), out end);
            number_Segement.NS_End = end.ToString();
        }

        private bool checkfinal() {

            if (!onceflag)
            {
                return false;
            }
            
            if (number_Segement.Status.Equals("内部"))
            {
                if (!beginflag)
                {
                    MessageBox.Show("开始号码段格式错误");
                    return false;
                }
                
                if (!endflag)
                {
                    MessageBox.Show("结束号码段格式错误");
                    return false;
                }
                
                if (!(int.Parse(number_Segement.NS_End.ToString()) >= int.Parse(number_Segement.NS_Begin)))
                {
                    MessageBox.Show("错误:结束号码段<开始号码段");
                    return false;
                }

                if ((int.Parse(number_Segement.NS_End.ToString()) > 999 || (int.Parse(number_Segement.NS_Begin) < 0))){
                    MessageBox.Show("错误:请输入0-999");
                    return false;
                }
                
            }
           
            return true;

        }

        private void button2_Click(object sender, EventArgs e)
        {

            finalflag = checkfinal();
            if (finalflag)
            {
                string message = createNSIDAL.insert(number_Segement);
                MessageBox.Show(message);
                this.Close();
            }
            


        }



    }
}
