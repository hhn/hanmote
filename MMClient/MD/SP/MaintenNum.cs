﻿using Lib.Common.CommonUtils;
using Lib.Common.MMCException.Bll;
using Lib.Common.MMCException.IDAL;
using Lib.SqlServerDAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MMClient.MD.SP
{
    public partial class MaintenNum : WeifenLuo.WinFormsUI.Docking.DockContent
    {
       private UpdateNS updateNS = null;

        public MaintenNum()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            createNS createNS = new createNS();
            createNS.Show();
            LoadData();
        }

        private void pageTool_Load(object sender, EventArgs e)
        {
            string sql = "select count(*) from Number_Segment";
            try
            {
                int i = int.Parse(DBHelper.ExecuteQueryDT(sql).Rows[0][0].ToString());
                int totalNum = i;
                pageTool.DrawControl(totalNum);
                //事件改变调用函数
                pageTool.OnPageChanged += pageTool_OnPageChanged;
            }
            catch (Lib.Common.MMCException.Bll.BllException ex)
            {
                MessageUtil.ShowTips("数据库异常，请稍后重试");
                return;
            }
        }

        private void pageTool_OnPageChanged(object sender, EventArgs e)
        {
            LoadData();
        }

        private void MaintenNum_Load(object sender, EventArgs e)
        {
            LoadData();
        }

        private void LoadData()
        {
            try
            {
                dataGridView1.DataSource = FindBigclassfyInforByPage(this.pageTool.PageSize, this.pageTool.PageIndex);
            }
            catch (BllException ex)
            {
                MessageUtil.ShowError(ex.Message);
            }
        }

        private DataTable FindBigclassfyInforByPage(int pageSize, int pageIndex)
        {
            try
            {
                string sql = @"SELECT top " + pageSize + " Number_ID,Description,Status,NS_Begin,NS_End,NS_status FROM Number_Segment  where Number_ID not in(select top " + pageSize * (pageIndex - 1) + " Number_ID from Number_Segment ORDER BY Number_ID ASC)ORDER BY Number_ID";
                return DBHelper.ExecuteQueryDT(sql);
            }
            catch (DBException ex)
            {
                throw new BllException("当前无数据", ex);
            };
        }

        private void button1_Click(object sender, EventArgs e)
        {
            LoadData();
            pageTool_Load(sender, e);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            int currentIndex = getCurrentSelectedRowIndex();
            DataGridViewRow row = this.dataGridView1.Rows[currentIndex];
            String Number_ID = Convert.ToString(row.Cells["MNID"].Value);
            string sql = "DELETE from Number_Segment WHERE Number_ID = '" + Number_ID + "' ";
            try
            {
                DBHelper.ExecuteNonQuery(sql);
                MessageBox.Show("删除成功");
                LoadData();
            }
            catch (Exception ex)
            {
                MessageUtil.ShowTips("此号码段正在被使用,无法删除!");
            }
            pageTool_Load(sender, e);
        }

        private int getCurrentSelectedRowIndex()
        {
            if (this.dataGridView1.Rows.Count <= 0)
            {
                MessageUtil.ShowWarning("号码段为空，无法执行操作");
                return -1;
            }

            if (this.dataGridView1.CurrentRow.Index < 0)
            {
                MessageUtil.ShowWarning("请选择一行记录，然后进行操作！");
                return -1;
            }

            return this.dataGridView1.CurrentRow.Index;
        }

        private void dataGridView1_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            int currentIndex = getCurrentSelectedRowIndex();
            DataGridViewRow currentRow = this.dataGridView1.Rows[currentIndex];
            string MNID = currentRow.Cells["MNID"].Value.ToString();
            if (MNID == "" || MNID.Equals(""))
            {
                MessageBox.Show("号码段为空，无法进入页面");
                return;
            }
            if (updateNS == null)
            {
                updateNS = new UpdateNS(MNID);
                updateNS.Show();
                updateNS = null;
            }
            LoadData();
        }
    }
}
