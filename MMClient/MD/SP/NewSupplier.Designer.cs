﻿namespace MMClient.MD.SP
{
    partial class NewSupplier
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cbb_供应商账户组 = new System.Windows.Forms.ComboBox();
            this.btn_确定 = new System.Windows.Forms.Button();
            this.cbb_供应商名称 = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cbb_供应商编码 = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // supplierInforGroupBox
            // 
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.cbb_供应商账户组);
            this.groupBox1.Controls.Add(this.btn_确定);
            this.groupBox1.Controls.Add(this.cbb_供应商名称);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.cbb_供应商编码);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(0, 6);
            this.groupBox1.Name = "supplierInforGroupBox";
            this.groupBox1.Size = new System.Drawing.Size(748, 293);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "新建供应商";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(72, 184);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(92, 17);
            this.label3.TabIndex = 6;
            this.label3.Text = "供应商账户组";
            // 
            // cbb_供应商账户组
            // 
            this.cbb_供应商账户组.FormattingEnabled = true;
            this.cbb_供应商账户组.Items.AddRange(new object[] {
            "原材料供应商",
            "贸易产品供应商",
            "服务供应商"});
            this.cbb_供应商账户组.Location = new System.Drawing.Point(252, 181);
            this.cbb_供应商账户组.Name = "cbb_供应商账户组";
            this.cbb_供应商账户组.Size = new System.Drawing.Size(121, 24);
            this.cbb_供应商账户组.TabIndex = 5;
            // 
            // btn_确定
            // 
            this.btn_确定.Location = new System.Drawing.Point(499, 242);
            this.btn_确定.Name = "btn_确定";
            this.btn_确定.Size = new System.Drawing.Size(75, 35);
            this.btn_确定.TabIndex = 4;
            this.btn_确定.Text = "确定";
            this.btn_确定.UseVisualStyleBackColor = true;
            this.btn_确定.Click += new System.EventHandler(this.btn_确定_Click);
            // 
            // cbb_供应商名称
            // 
            this.cbb_供应商名称.FormattingEnabled = true;
            this.cbb_供应商名称.Location = new System.Drawing.Point(252, 122);
            this.cbb_供应商名称.Name = "cbb_供应商名称";
            this.cbb_供应商名称.Size = new System.Drawing.Size(121, 24);
            this.cbb_供应商名称.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(72, 125);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(78, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "供应商名称";
            // 
            // cbb_供应商编码
            // 
            this.cbb_供应商编码.FormattingEnabled = true;
            this.cbb_供应商编码.Location = new System.Drawing.Point(252, 64);
            this.cbb_供应商编码.Name = "cbb_供应商编码";
            this.cbb_供应商编码.Size = new System.Drawing.Size(121, 24);
            this.cbb_供应商编码.TabIndex = 1;
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(72, 67);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(78, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "供应商编码";
            // 
            // NewSupplier
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(748, 304);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "NewSupplier";
            this.Text = "新建供应商";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btn_确定;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.ComboBox cbb_供应商名称;
        public System.Windows.Forms.ComboBox cbb_供应商编码;
        private System.Windows.Forms.Label label3;
        public System.Windows.Forms.ComboBox cbb_供应商账户组;
    }
}