﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Common.CommonUtils;
using Lib.SqlServerDAL;
using Lib.Bll;
using MMClient.MD.SP;
using Lib.Model.MD.SP;
using Lib.Bll.MDBll.SP;
using Lib.Bll.MDBll.General;
using Lib.Common.MMCException.IDAL;
using Lib.Common.MMCException.Bll;

namespace MMClient.MD
{
    public partial class SPMaintain : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        
        public SPMaintain()
        {
            InitializeComponent();   
        }
        GeneralBLL gn = new GeneralBLL();
        SupplierBaseBLL sbb = new SupplierBaseBLL();
        ComboBoxItem cbm = new ComboBoxItem();
        FormHelper formh = new FormHelper();
        private void SPMaintain_Load(object sender, EventArgs e)
        {
            List<string> list1 = sbb.GetAllSupplierID();
            cbm.FuzzyQury(cbb_供应商编码, list1);
            List<string> list2 = gn.GetAllCountryName();
            cbm.FuzzyQury(cbb_国家, list2);

            string sql = "SELECT * FROM [Industry_Category]";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            List<string> list3 = new List<string>();
            if (dt != null)
            {
               for (int i = 0; i < dt.Rows.Count; i++)
               {
                   list3.Add(dt.Rows[i][1].ToString());
               }
            }
            cbb_行业标识.DataSource = list3;

            
        }

        private void cbb_供应商编码_SelectedIndexChanged(object sender, EventArgs e)
        {
            ///重置文本框
            cbb_标题.Text = "";
            cbb_城市.Text = "";
            cbb_传真.Text = "";
            cbb_移动电话分机号.Text = "";
            cbb_地区.Text = "";
            cbb_电话.Text = "";
            cbb_电话分机号.Text = "";
            cbb_公司邮政编码.Text = "";
            //cbb_供应商编码.Text = "";
            cbb_供应商名称.Text = "";
            cbb_国家.Text = "";
            cbb_街道.Text = "";
            cbb_客户.Text = "";
            cbb_贸易伙伴.Text = "";
            cbb_门牌号.Text = "";
            cbb_名称.Text = "";
            cbb_权限组.Text = "";
            cbb_移动电话.Text = "";
            cbb_邮政编码.Text = "";
            cbb_邮政编码信箱.Text = "";
            cbb_邮政信箱.Text = "";
            cbb_语言.Text = "";
            cbb_账户组.Text = "";
            cbb_组代码.Text = "";
            string spid = cbb_供应商编码.Text;
                SupplierBase smodel = new SupplierBase();
             smodel = sbb.GetSupplierBasicInformation(spid);
             cbb_供应商名称.Text = smodel.Supplier_Name;
             cbb_账户组.Text = smodel.Supplier_Account;
             cbb_标题.Text = smodel.Supplier_Title;
             cbb_国家.Text = smodel.Nation;
             cbb_街道.Text = smodel.Address;
             cbb_门牌号.Text = smodel.House_Number;
             cbb_邮政编码.Text = smodel.Street_postalCode;
             cbb_城市.Text = smodel.City;
             cbb_地区.Text = smodel.Region;
             cbb_邮政信箱.Text = smodel.Post_OfficeBox;
             cbb_公司邮政编码.Text = smodel.Company_ZipCode;
             cbb_语言.Text = smodel.Language;
             cbb_邮政编码信箱.Text = smodel.Zip_Code;
             cbb_传真.Text = smodel.Fax;
             cbb_电话.Text = smodel.Contact_phone1;
             cbb_移动电话.Text = smodel.Mobile_Phone1;
             cbb_电话分机号.Text = smodel.Contact_phone2;
             cbb_移动电话分机号.Text = smodel.Mobile_Phone2;
             tbx_电子邮箱.Text = smodel.Email;
             tbx_所属分类.Text = smodel.Supplier_Class;
            
            //string spid = cbb_供应商编码.Text;
            //string sql2 = "SELECT * FROM [Supplier_Base] where Supplier_ID='" +spid+ "'";
            //DataTable dt = DBHelper.ExecuteQueryDT(sql2);
            //foreach (Control col in this.Controls)
            //{
            //    if (col.GetType().Name.Equals("ComboBox")&&col.Name.Equals("cbb_供应商编码"))
            //    {
            //        ((ComboBox)col).Text = string.Empty;
            //    }
            //}

           
            //cbb_供应商名称.Text = dt.Rows[0][3].ToString();
            //cbb_账户组.Text = dt.Rows[0][2].ToString();
            //cbb_标题.Text = dt.Rows[0][4].ToString();
            //cbb_国家.Text = dt.Rows[0][5].ToString();
            //cbb_街道.Text = dt.Rows[0][6].ToString();
            //cbb_门牌号.Text = dt.Rows[0][7].ToString();
            //cbb_邮政编码.Text = dt.Rows[0][8].ToString();
            //cbb_城市.Text = dt.Rows[0][9].ToString();
            //cbb_地区.Text = dt.Rows[0][10].ToString();
            //cbb_邮政信箱.Text = dt.Rows[0][11].ToString();
            //cbb_公司邮政编码.Text = dt.Rows[0][12].ToString();
            //cbb_语言.Text = dt.Rows[0][13].ToString();
            //cbb_邮政编码信箱.Text = dt.Rows[0][15].ToString();
            //cbb_传真.Text = dt.Rows[0][16].ToString();
            //cbb_电话.Text = dt.Rows[0][17].ToString();
            //cbb_移动电话.Text = dt.Rows[0][18].ToString();

        }

        private void btn_确定_Click(object sender, EventArgs e)
        {
           
            //string sql3 = " UPDATE [Supplier_Base] SET Supplier_Name='" + cbb_供应商名称.Text + "',Supplier_Tiile='" + cbb_标题.Text + "',Nation='" + cbb_国家.Text + "',Address='" + cbb_街道.Text + "',House_Number='" + cbb_门牌号.Text + "',Street_postalCode='" + cbb_邮政编码.Text + "',City='" + cbb_城市.Text + "',Region='" + cbb_地区.Text + "',Post_OfficeBox='" + cbb_邮政信箱.Text + "',Company_ZipCode='" + cbb_公司邮政编码.Text + "',Language='" + cbb_语言.Text + "',Zip_Code='" + cbb_邮政编码信箱.Text + "',Fax='" + cbb_传真.Text + "',Mobile_Phone1='" + cbb_移动电话.Text + "',Mobile_Phone2='" + cbb_电话分机号.Text + "',Supplier_AccountGroup='" + cbb_账户组.Text +"' WHERE Supplier_ID= '" + cbb_供应商编码.Text + "'";
            //DBHelper.ExecuteNonQuery(sql3);
            //MessageBox.Show("修改成功");
        }

        private void btn_重置_Click(object sender, EventArgs e)
        {
           
        
        }

        private void btn_新建_Click(object sender, EventArgs e)
        {
            NewSupplier nsp = new NewSupplier(this);
            nsp.Show();
        }

        private void cbb_邮政编码_TextChanged(object sender, EventArgs e)
        {
            Dictionary<string, string> dict_temp = new Dictionary<string, string>();
            string key = "邮政编码";
            dict_temp.Add(key, "");
            if(cbb_邮政编码.Text!="")
            formh.onlyNumber(cbb_邮政编码, key, dict_temp);
        }

        private void cbb_邮政编码信箱_TextChanged(object sender, EventArgs e)
        {
            Dictionary<string, string> dict_temp = new Dictionary<string, string>();
            string key = "邮政编码信箱";
            dict_temp.Add(key, "");
            if (cbb_邮政编码信箱.Text != "")
            formh.onlyNumber(cbb_邮政编码信箱, key, dict_temp);
        }

        private void cbb_公司邮政编码_TextChanged(object sender, EventArgs e)
        {
            Dictionary<string, string> dict_temp = new Dictionary<string, string>();
            string key = "公司邮政编码";
            dict_temp.Add(key, "");
            if (cbb_公司邮政编码.Text != "")
            formh.onlyNumber(cbb_公司邮政编码, key, dict_temp);
        }

        private void cbb_电话_TextChanged(object sender, EventArgs e)
        {
            Dictionary<string, string> dict_temp = new Dictionary<string, string>();
            string key = "电话";
            dict_temp.Add(key, "");
            if (cbb_电话.Text != "")
            formh.onlyNumber(cbb_电话, key, dict_temp);
        }

        private void cbb_电话分机号_TextChanged(object sender, EventArgs e)
        {
            Dictionary<string, string> dict_temp = new Dictionary<string, string>();
            string key = "电话分机号";
            dict_temp.Add(key, "");
            if (cbb_电话分机号.Text != "")
            formh.onlyNumber(cbb_电话分机号, key, dict_temp);
        }

        private void cbb_移动电话_TextChanged(object sender, EventArgs e)
        {
            Dictionary<string, string> dict_temp = new Dictionary<string, string>();
            string key = "移动电话";
            dict_temp.Add(key, "");
            if (cbb_移动电话.Text != "")
            formh.onlyNumber(cbb_移动电话, key, dict_temp);
        }

        private void cbb_移动电话分机号_TextChanged(object sender, EventArgs e)
        {
            Dictionary<string, string> dict_temp = new Dictionary<string, string>();
            string key = "移动电话分机号";
            dict_temp.Add(key, "");
            if (cbb_移动电话分机号.Text != "")
            formh.onlyNumber(cbb_移动电话分机号, key, dict_temp);
        }

        private void cbb_国家_SelectedIndexChanged(object sender, EventArgs e)
        {
             List<string> list = new List<string>();
             if (cbb_国家.Text != "")
             {
                 list = gn.GetAllCity(cbb_国家.Text);
                 if (list == null)
                 {
                     MessageUtil.ShowError("没有城市信息");
                     cbb_城市.DataSource = null;
                 }
                 else
                 {
                     cbb_城市.DataSource = list;
                     cbb_城市.Text = "";
                 }
             }
           
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            SupplierChose spc = new SupplierChose(this);
            spc.Show();
        }

        private void toolStripButton4_Click(object sender, EventArgs e)
        {
            SupplierBase smodel = new SupplierBase();
            smodel.Supplier_ID = cbb_供应商编码.Text;
            smodel.Supplier_Name = cbb_供应商名称.Text;
            smodel.Supplier_Account = cbb_账户组.Text;
            smodel.Supplier_Title = cbb_标题.Text;
            smodel.Nation = cbb_国家.Text;
            smodel.Address = cbb_街道.Text;
            smodel.House_Number = cbb_门牌号.Text;
            smodel.Street_postalCode = cbb_邮政编码.Text;
            smodel.City = cbb_城市.Text;
            smodel.Region = cbb_地区.Text;
            smodel.Post_OfficeBox = cbb_邮政信箱.Text;
            smodel.Company_ZipCode = cbb_公司邮政编码.Text;
            smodel.Language = cbb_语言.Text;
            smodel.Zip_Code = cbb_邮政编码信箱.Text;
            smodel.Fax = cbb_传真.Text;
            smodel.Contact_phone1 = cbb_电话.Text;
            smodel.Contact_phone2 = cbb_电话分机号.Text;
            smodel.Mobile_Phone1 = cbb_移动电话.Text;
            smodel.Mobile_Phone2 = cbb_移动电话分机号.Text;
            smodel.Email = tbx_电子邮箱.Text;
            try
            {
                sbb.UpdateSupplierBasicInformation(smodel);
                MessageUtil.ShowTips("修改成功");
            }
            catch (BllException ex)
            {
                MessageUtil.ShowTips(ex.Msg);
            }
        }

        private void toolStripButton8_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void toolStripButton7_Click(object sender, EventArgs e)
        {
            cbb_标题.Text = "";
            cbb_城市.Text = "";
            cbb_传真.Text = "";
            cbb_移动电话分机号.Text = "";
            cbb_地区.Text = "";
            cbb_电话.Text = "";
            cbb_电话分机号.Text = "";
            cbb_公司邮政编码.Text = "";
            cbb_供应商编码.Text = "";
            cbb_供应商名称.Text = "";
            cbb_国家.Text = "";
            cbb_街道.Text = "";
            cbb_客户.Text = "";
            cbb_贸易伙伴.Text = "";
            cbb_门牌号.Text = "";
            cbb_名称.Text = "";
            cbb_权限组.Text = "";
            cbb_移动电话.Text = "";
            cbb_邮政编码.Text = "";
            cbb_邮政编码信箱.Text = "";
            cbb_邮政信箱.Text = "";
            cbb_语言.Text = "";
            cbb_账户组.Text = "";
            cbb_组代码.Text = "";
            tbx_电子邮箱.Text = "";
        }

        private void cbb_邮政编码信箱_MouseLeave(object sender, EventArgs e)
        {
            
        }

        private void cbb_邮政编码信箱_Leave(object sender, EventArgs e)
        {
            formh.ZipCode(cbb_邮政编码信箱);
        }

        private void cbb_邮政编码_Leave(object sender, EventArgs e)
        {
            formh.ZipCode(cbb_邮政编码);
        }

        private void cbb_公司邮政编码_Leave(object sender, EventArgs e)
        {
            formh.ZipCode(cbb_公司邮政编码);
        }

        private void cbb_电话_Leave(object sender, EventArgs e)
        {
            formh.telephone(cbb_电话);
        }

        private void cbb_移动电话_Leave(object sender, EventArgs e)
        {
            formh.telephone(cbb_移动电话);
        }

        private void cbb_电话分机号_Leave(object sender, EventArgs e)
        {
            formh.telephone(cbb_电话分机号);
        }

        private void cbb_移动电话分机号_Leave(object sender, EventArgs e)
        {
            formh.telephone(cbb_移动电话分机号);
        }

        private void tbx_电子邮箱_Leave(object sender, EventArgs e)
        {
            formh.Email(tbx_电子邮箱);
        }


    }
}
