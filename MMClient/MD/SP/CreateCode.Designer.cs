﻿namespace MMClient.MD.SP
{
    partial class CreateCode
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.供应商名称label = new System.Windows.Forms.Label();
            this.textbox查询供应商 = new System.Windows.Forms.TextBox();
            this.查询供应商Button = new System.Windows.Forms.Button();
            this.comboBox筛选供应商 = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.dataGridView_选择供应商 = new System.Windows.Forms.DataGridView();
            this.Supplier_Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SupplierName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ManagerName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LoginAccount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label外部 = new System.Windows.Forms.Label();
            this.textBox外部输入框 = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.Textbox供应商账号 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textbox供应商流水号 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textbox供应商名称结果 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.textBox号码段ID = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.TextBox一次性供应商标识 = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.textBox账户组描述 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.TextBox账户组ID = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.dataGridView_选择账户组 = new System.Windows.Forms.DataGridView();
            this.Account_Group_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Description = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SingleStatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NS_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Status = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NS_Begin = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NS_End = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NS_status = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.comboBox筛选账户组 = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.button查询账户组 = new System.Windows.Forms.Button();
            this.textBox账户组名称 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.TextBox公司名称结果 = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.textBox公司代码结果 = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.button查询公司代码 = new System.Windows.Forms.Button();
            this.textBox公司名称 = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.dataGridView_选择公司代码 = new System.Windows.Forms.DataGridView();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.textBox采购组织名称结果 = new System.Windows.Forms.TextBox();
            this.textBox采购组织结果 = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.dataGridView_选择采购组织 = new System.Windows.Forms.DataGridView();
            this.button查询采购组织 = new System.Windows.Forms.Button();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.button创建账户 = new System.Windows.Forms.Button();
            this.button检测账户唯一性 = new System.Windows.Forms.Button();
            this.Company_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Company_Name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Buyer_Org = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Buyer_Org_Name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_选择供应商)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_选择账户组)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_选择公司代码)).BeginInit();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_选择采购组织)).BeginInit();
            this.SuspendLayout();
            // 
            // 供应商名称label
            // 
            this.供应商名称label.AutoSize = true;
            this.供应商名称label.Location = new System.Drawing.Point(28, 22);
            this.供应商名称label.Name = "供应商名称label";
            this.供应商名称label.Size = new System.Drawing.Size(71, 12);
            this.供应商名称label.TabIndex = 27;
            this.供应商名称label.Text = "供应商名称:";
            // 
            // textbox查询供应商
            // 
            this.textbox查询供应商.Location = new System.Drawing.Point(105, 19);
            this.textbox查询供应商.Name = "textbox查询供应商";
            this.textbox查询供应商.Size = new System.Drawing.Size(154, 21);
            this.textbox查询供应商.TabIndex = 28;
            this.textbox查询供应商.TextChanged += new System.EventHandler(this.textbox查询供应商_TextChanged);
            // 
            // 查询供应商Button
            // 
            this.查询供应商Button.Location = new System.Drawing.Point(274, 17);
            this.查询供应商Button.Name = "查询供应商Button";
            this.查询供应商Button.Size = new System.Drawing.Size(94, 23);
            this.查询供应商Button.TabIndex = 29;
            this.查询供应商Button.Text = "查询供应商";
            this.查询供应商Button.UseVisualStyleBackColor = true;
            this.查询供应商Button.Click += new System.EventHandler(this.查询供应商Button_Click);
            // 
            // comboBox筛选供应商
            // 
            this.comboBox筛选供应商.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox筛选供应商.FormattingEnabled = true;
            this.comboBox筛选供应商.Items.AddRange(new object[] {
            "已有账号供应商",
            "暂无账号供应商"});
            this.comboBox筛选供应商.Location = new System.Drawing.Point(482, 19);
            this.comboBox筛选供应商.Name = "comboBox筛选供应商";
            this.comboBox筛选供应商.Size = new System.Drawing.Size(143, 20);
            this.comboBox筛选供应商.TabIndex = 30;
            this.comboBox筛选供应商.SelectedIndexChanged += new System.EventHandler(this.comboBox筛选供应商_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(399, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 12);
            this.label1.TabIndex = 31;
            this.label1.Text = "筛选供应商：";
            // 
            // dataGridView_选择供应商
            // 
            this.dataGridView_选择供应商.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dataGridView_选择供应商.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_选择供应商.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Supplier_Id,
            this.SupplierName,
            this.ManagerName,
            this.LoginAccount});
            this.dataGridView_选择供应商.Location = new System.Drawing.Point(0, 60);
            this.dataGridView_选择供应商.Name = "dataGridView_选择供应商";
            this.dataGridView_选择供应商.RowTemplate.Height = 23;
            this.dataGridView_选择供应商.Size = new System.Drawing.Size(759, 172);
            this.dataGridView_选择供应商.TabIndex = 32;
            this.dataGridView_选择供应商.SelectionChanged += new System.EventHandler(this.dataGridView_选择供应商_SelectionChanged);
            // 
            // Supplier_Id
            // 
            this.Supplier_Id.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Supplier_Id.DataPropertyName = "Supplier_Id";
            this.Supplier_Id.HeaderText = "供应商流水号";
            this.Supplier_Id.Name = "Supplier_Id";
            this.Supplier_Id.ReadOnly = true;
            // 
            // SupplierName
            // 
            this.SupplierName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.SupplierName.DataPropertyName = "Supplier_Name";
            this.SupplierName.HeaderText = "供应商名称";
            this.SupplierName.Name = "SupplierName";
            this.SupplierName.ReadOnly = true;
            // 
            // ManagerName
            // 
            this.ManagerName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ManagerName.DataPropertyName = "ManagerName";
            this.ManagerName.HeaderText = "经理姓名";
            this.ManagerName.Name = "ManagerName";
            this.ManagerName.ReadOnly = true;
            // 
            // LoginAccount
            // 
            this.LoginAccount.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.LoginAccount.DataPropertyName = "LoginAccount";
            this.LoginAccount.HeaderText = "供应商账号";
            this.LoginAccount.Name = "LoginAccount";
            this.LoginAccount.ReadOnly = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label外部);
            this.groupBox1.Controls.Add(this.textBox外部输入框);
            this.groupBox1.Controls.Add(this.label17);
            this.groupBox1.Controls.Add(this.Textbox供应商账号);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.textbox供应商流水号);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.textbox供应商名称结果);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.comboBox筛选供应商);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.dataGridView_选择供应商);
            this.groupBox1.Controls.Add(this.查询供应商Button);
            this.groupBox1.Controls.Add(this.供应商名称label);
            this.groupBox1.Controls.Add(this.textbox查询供应商);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(773, 296);
            this.groupBox1.TabIndex = 230;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "选择供应商";
            // 
            // label外部
            // 
            this.label外部.AutoSize = true;
            this.label外部.ForeColor = System.Drawing.Color.Red;
            this.label外部.Location = new System.Drawing.Point(497, 281);
            this.label外部.Name = "label外部";
            this.label外部.Size = new System.Drawing.Size(0, 12);
            this.label外部.TabIndex = 238;
            // 
            // textBox外部输入框
            // 
            this.textBox外部输入框.Enabled = false;
            this.textBox外部输入框.Location = new System.Drawing.Point(486, 253);
            this.textBox外部输入框.Name = "textBox外部输入框";
            this.textBox外部输入框.Size = new System.Drawing.Size(75, 21);
            this.textBox外部输入框.TabIndex = 237;
            this.textBox外部输入框.TextChanged += new System.EventHandler(this.textBox外部输入框_TextChanged);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(413, 256);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(77, 12);
            this.label17.TabIndex = 236;
            this.label17.Text = "外部输入框：";
            // 
            // Textbox供应商账号
            // 
            this.Textbox供应商账号.Enabled = false;
            this.Textbox供应商账号.Location = new System.Drawing.Point(656, 253);
            this.Textbox供应商账号.Name = "Textbox供应商账号";
            this.Textbox供应商账号.Size = new System.Drawing.Size(103, 21);
            this.Textbox供应商账号.TabIndex = 235;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(573, 256);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(77, 12);
            this.label4.TabIndex = 234;
            this.label4.Text = "供应商账号：";
            // 
            // textbox供应商流水号
            // 
            this.textbox供应商流水号.Enabled = false;
            this.textbox供应商流水号.Location = new System.Drawing.Point(85, 253);
            this.textbox供应商流水号.Name = "textbox供应商流水号";
            this.textbox供应商流水号.Size = new System.Drawing.Size(142, 21);
            this.textbox供应商流水号.TabIndex = 233;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(2, 256);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(89, 12);
            this.label3.TabIndex = 232;
            this.label3.Text = "供应商流水号：";
            // 
            // textbox供应商名称结果
            // 
            this.textbox供应商名称结果.Enabled = false;
            this.textbox供应商名称结果.Location = new System.Drawing.Point(305, 253);
            this.textbox供应商名称结果.Name = "textbox供应商名称结果";
            this.textbox供应商名称结果.Size = new System.Drawing.Size(100, 21);
            this.textbox供应商名称结果.TabIndex = 231;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(233, 256);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 12);
            this.label2.TabIndex = 230;
            this.label2.Text = "供应商名称：";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.textBox号码段ID);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.TextBox一次性供应商标识);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.textBox账户组描述);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.TextBox账户组ID);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.dataGridView_选择账户组);
            this.groupBox2.Controls.Add(this.comboBox筛选账户组);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.button查询账户组);
            this.groupBox2.Controls.Add(this.textBox账户组名称);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Location = new System.Drawing.Point(12, 314);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(773, 279);
            this.groupBox2.TabIndex = 231;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "选择账户组";
            // 
            // textBox号码段ID
            // 
            this.textBox号码段ID.Enabled = false;
            this.textBox号码段ID.Location = new System.Drawing.Point(656, 237);
            this.textBox号码段ID.Name = "textBox号码段ID";
            this.textBox号码段ID.Size = new System.Drawing.Size(58, 21);
            this.textBox号码段ID.TabIndex = 240;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(591, 240);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(59, 12);
            this.label10.TabIndex = 239;
            this.label10.Text = "号码段ID:";
            // 
            // TextBox一次性供应商标识
            // 
            this.TextBox一次性供应商标识.Enabled = false;
            this.TextBox一次性供应商标识.Location = new System.Drawing.Point(499, 237);
            this.TextBox一次性供应商标识.Name = "TextBox一次性供应商标识";
            this.TextBox一次性供应商标识.Size = new System.Drawing.Size(58, 21);
            this.TextBox一次性供应商标识.TabIndex = 238;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(390, 240);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(113, 12);
            this.label9.TabIndex = 237;
            this.label9.Text = "一次性供应商标识：";
            // 
            // textBox账户组描述
            // 
            this.textBox账户组描述.Enabled = false;
            this.textBox账户组描述.Location = new System.Drawing.Point(248, 237);
            this.textBox账户组描述.Name = "textBox账户组描述";
            this.textBox账户组描述.Size = new System.Drawing.Size(120, 21);
            this.textBox账户组描述.TabIndex = 236;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(171, 240);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(71, 12);
            this.label8.TabIndex = 235;
            this.label8.Text = "账户组描述:";
            // 
            // TextBox账户组ID
            // 
            this.TextBox账户组ID.Enabled = false;
            this.TextBox账户组ID.Location = new System.Drawing.Point(79, 237);
            this.TextBox账户组ID.Name = "TextBox账户组ID";
            this.TextBox账户组ID.Size = new System.Drawing.Size(55, 21);
            this.TextBox账户组ID.TabIndex = 234;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(8, 240);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(65, 12);
            this.label7.TabIndex = 231;
            this.label7.Text = "账户组ID：";
            // 
            // dataGridView_选择账户组
            // 
            this.dataGridView_选择账户组.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dataGridView_选择账户组.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_选择账户组.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Account_Group_ID,
            this.Description,
            this.SingleStatus,
            this.NS_id,
            this.Status,
            this.NS_Begin,
            this.NS_End,
            this.NS_status});
            this.dataGridView_选择账户组.Location = new System.Drawing.Point(8, 69);
            this.dataGridView_选择账户组.Name = "dataGridView_选择账户组";
            this.dataGridView_选择账户组.RowTemplate.Height = 23;
            this.dataGridView_选择账户组.Size = new System.Drawing.Size(747, 150);
            this.dataGridView_选择账户组.TabIndex = 33;
            this.dataGridView_选择账户组.SelectionChanged += new System.EventHandler(this.dataGridView_选择账户组_SelectionChanged);
            // 
            // Account_Group_ID
            // 
            this.Account_Group_ID.DataPropertyName = "Account_Group_ID";
            this.Account_Group_ID.HeaderText = "账户组ID";
            this.Account_Group_ID.Name = "Account_Group_ID";
            this.Account_Group_ID.ReadOnly = true;
            this.Account_Group_ID.Width = 80;
            // 
            // Description
            // 
            this.Description.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Description.DataPropertyName = "Description";
            this.Description.HeaderText = "账户组描述";
            this.Description.Name = "Description";
            this.Description.ReadOnly = true;
            // 
            // SingleStatus
            // 
            this.SingleStatus.DataPropertyName = "SingleStatus";
            this.SingleStatus.HeaderText = "一次性";
            this.SingleStatus.Name = "SingleStatus";
            this.SingleStatus.ReadOnly = true;
            this.SingleStatus.Width = 70;
            // 
            // NS_id
            // 
            this.NS_id.DataPropertyName = "NS_id";
            this.NS_id.HeaderText = "号码段ID";
            this.NS_id.Name = "NS_id";
            this.NS_id.ReadOnly = true;
            this.NS_id.Width = 80;
            // 
            // Status
            // 
            this.Status.DataPropertyName = "Status";
            this.Status.HeaderText = "输入方式";
            this.Status.Name = "Status";
            this.Status.ReadOnly = true;
            this.Status.Width = 80;
            // 
            // NS_Begin
            // 
            this.NS_Begin.DataPropertyName = "NS_Begin";
            this.NS_Begin.HeaderText = "开始号码";
            this.NS_Begin.Name = "NS_Begin";
            this.NS_Begin.ReadOnly = true;
            this.NS_Begin.Width = 80;
            // 
            // NS_End
            // 
            this.NS_End.DataPropertyName = "NS_End";
            this.NS_End.HeaderText = "结束号码";
            this.NS_End.Name = "NS_End";
            this.NS_End.ReadOnly = true;
            this.NS_End.Width = 80;
            // 
            // NS_status
            // 
            this.NS_status.DataPropertyName = "NS_status";
            this.NS_status.HeaderText = "号码状态";
            this.NS_status.Name = "NS_status";
            this.NS_status.ReadOnly = true;
            this.NS_status.Width = 80;
            // 
            // comboBox筛选账户组
            // 
            this.comboBox筛选账户组.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox筛选账户组.FormattingEnabled = true;
            this.comboBox筛选账户组.Items.AddRange(new object[] {
            "一次性供应商",
            "非一次性供应商"});
            this.comboBox筛选账户组.Location = new System.Drawing.Point(482, 31);
            this.comboBox筛选账户组.Name = "comboBox筛选账户组";
            this.comboBox筛选账户组.Size = new System.Drawing.Size(143, 20);
            this.comboBox筛选账户组.TabIndex = 32;
            this.comboBox筛选账户组.SelectedIndexChanged += new System.EventHandler(this.comboBox筛选账户组_SelectedIndexChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(399, 34);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(77, 12);
            this.label6.TabIndex = 31;
            this.label6.Text = "筛选账户组：";
            // 
            // button查询账户组
            // 
            this.button查询账户组.Location = new System.Drawing.Point(274, 29);
            this.button查询账户组.Name = "button查询账户组";
            this.button查询账户组.Size = new System.Drawing.Size(94, 23);
            this.button查询账户组.TabIndex = 30;
            this.button查询账户组.Text = "查询账户组";
            this.button查询账户组.UseVisualStyleBackColor = true;
            this.button查询账户组.Click += new System.EventHandler(this.button查询账户组_Click);
            // 
            // textBox账户组名称
            // 
            this.textBox账户组名称.Location = new System.Drawing.Point(105, 31);
            this.textBox账户组名称.Name = "textBox账户组名称";
            this.textBox账户组名称.Size = new System.Drawing.Size(154, 21);
            this.textBox账户组名称.TabIndex = 1;
            this.textBox账户组名称.TextChanged += new System.EventHandler(this.textBox账户组名称_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(30, 34);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(77, 12);
            this.label5.TabIndex = 0;
            this.label5.Text = "账户组名称：";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.TextBox公司名称结果);
            this.groupBox3.Controls.Add(this.label14);
            this.groupBox3.Controls.Add(this.textBox公司代码结果);
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this.button查询公司代码);
            this.groupBox3.Controls.Add(this.textBox公司名称);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.dataGridView_选择公司代码);
            this.groupBox3.Location = new System.Drawing.Point(12, 600);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(382, 222);
            this.groupBox3.TabIndex = 232;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "选择公司代码";
            // 
            // TextBox公司名称结果
            // 
            this.TextBox公司名称结果.Enabled = false;
            this.TextBox公司名称结果.Location = new System.Drawing.Point(255, 191);
            this.TextBox公司名称结果.Name = "TextBox公司名称结果";
            this.TextBox公司名称结果.Size = new System.Drawing.Size(100, 21);
            this.TextBox公司名称结果.TabIndex = 235;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(194, 194);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(65, 12);
            this.label14.TabIndex = 234;
            this.label14.Text = "公司名称：";
            // 
            // textBox公司代码结果
            // 
            this.textBox公司代码结果.Enabled = false;
            this.textBox公司代码结果.Location = new System.Drawing.Point(79, 191);
            this.textBox公司代码结果.Name = "textBox公司代码结果";
            this.textBox公司代码结果.Size = new System.Drawing.Size(100, 21);
            this.textBox公司代码结果.TabIndex = 233;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(8, 194);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(65, 12);
            this.label13.TabIndex = 232;
            this.label13.Text = "公司代码：";
            // 
            // button查询公司代码
            // 
            this.button查询公司代码.Location = new System.Drawing.Point(210, 22);
            this.button查询公司代码.Name = "button查询公司代码";
            this.button查询公司代码.Size = new System.Drawing.Size(91, 23);
            this.button查询公司代码.TabIndex = 3;
            this.button查询公司代码.Text = "查询公司代码";
            this.button查询公司代码.UseVisualStyleBackColor = true;
            this.button查询公司代码.Click += new System.EventHandler(this.button查询公司代码_Click);
            // 
            // textBox公司名称
            // 
            this.textBox公司名称.Location = new System.Drawing.Point(93, 23);
            this.textBox公司名称.Name = "textBox公司名称";
            this.textBox公司名称.Size = new System.Drawing.Size(100, 21);
            this.textBox公司名称.TabIndex = 2;
            this.textBox公司名称.TextChanged += new System.EventHandler(this.textBox公司名称_TextChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(22, 26);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(65, 12);
            this.label11.TabIndex = 1;
            this.label11.Text = "公司名称：";
            // 
            // dataGridView_选择公司代码
            // 
            this.dataGridView_选择公司代码.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dataGridView_选择公司代码.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_选择公司代码.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Company_ID,
            this.Company_Name});
            this.dataGridView_选择公司代码.Location = new System.Drawing.Point(8, 51);
            this.dataGridView_选择公司代码.Name = "dataGridView_选择公司代码";
            this.dataGridView_选择公司代码.RowTemplate.Height = 23;
            this.dataGridView_选择公司代码.Size = new System.Drawing.Size(362, 130);
            this.dataGridView_选择公司代码.TabIndex = 0;
            this.dataGridView_选择公司代码.SelectionChanged += new System.EventHandler(this.dataGridView_选择公司代码_SelectionChanged);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.textBox采购组织名称结果);
            this.groupBox4.Controls.Add(this.textBox采购组织结果);
            this.groupBox4.Controls.Add(this.label16);
            this.groupBox4.Controls.Add(this.label15);
            this.groupBox4.Controls.Add(this.dataGridView_选择采购组织);
            this.groupBox4.Controls.Add(this.button查询采购组织);
            this.groupBox4.Controls.Add(this.textBox5);
            this.groupBox4.Controls.Add(this.label12);
            this.groupBox4.Location = new System.Drawing.Point(408, 600);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(377, 222);
            this.groupBox4.TabIndex = 233;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "选择采购组织";
            // 
            // textBox采购组织名称结果
            // 
            this.textBox采购组织名称结果.Enabled = false;
            this.textBox采购组织名称结果.Location = new System.Drawing.Point(274, 188);
            this.textBox采购组织名称结果.Name = "textBox采购组织名称结果";
            this.textBox采购组织名称结果.Size = new System.Drawing.Size(89, 21);
            this.textBox采购组织名称结果.TabIndex = 9;
            // 
            // textBox采购组织结果
            // 
            this.textBox采购组织结果.Enabled = false;
            this.textBox采购组织结果.Location = new System.Drawing.Point(90, 188);
            this.textBox采购组织结果.Name = "textBox采购组织结果";
            this.textBox采购组织结果.Size = new System.Drawing.Size(71, 21);
            this.textBox采购组织结果.TabIndex = 8;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(17, 194);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(77, 12);
            this.label16.TabIndex = 7;
            this.label16.Text = "采购组织ID：";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(183, 194);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(89, 12);
            this.label15.TabIndex = 6;
            this.label15.Text = "采购组织名称：";
            // 
            // dataGridView_选择采购组织
            // 
            this.dataGridView_选择采购组织.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dataGridView_选择采购组织.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_选择采购组织.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Buyer_Org,
            this.Buyer_Org_Name});
            this.dataGridView_选择采购组织.Location = new System.Drawing.Point(9, 51);
            this.dataGridView_选择采购组织.Name = "dataGridView_选择采购组织";
            this.dataGridView_选择采购组织.RowTemplate.Height = 23;
            this.dataGridView_选择采购组织.Size = new System.Drawing.Size(354, 130);
            this.dataGridView_选择采购组织.TabIndex = 5;
            this.dataGridView_选择采购组织.SelectionChanged += new System.EventHandler(this.dataGridView_选择采购组织_SelectionChanged);
            // 
            // button查询采购组织
            // 
            this.button查询采购组织.Location = new System.Drawing.Point(254, 20);
            this.button查询采购组织.Name = "button查询采购组织";
            this.button查询采购组织.Size = new System.Drawing.Size(91, 23);
            this.button查询采购组织.TabIndex = 4;
            this.button查询采购组织.Text = "查询采购组织";
            this.button查询采购组织.UseVisualStyleBackColor = true;
            this.button查询采购组织.Click += new System.EventHandler(this.button查询采购组织_Click);
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(129, 22);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(100, 21);
            this.textBox5.TabIndex = 1;
            this.textBox5.TextChanged += new System.EventHandler(this.textBox5_TextChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(33, 27);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(89, 12);
            this.label12.TabIndex = 0;
            this.label12.Text = "采购组织名称：";
            // 
            // button创建账户
            // 
            this.button创建账户.Location = new System.Drawing.Point(537, 837);
            this.button创建账户.Name = "button创建账户";
            this.button创建账户.Size = new System.Drawing.Size(100, 23);
            this.button创建账户.TabIndex = 234;
            this.button创建账户.Text = "创建账户";
            this.button创建账户.UseVisualStyleBackColor = true;
            this.button创建账户.Click += new System.EventHandler(this.button创建账户_Click);
            // 
            // button检测账户唯一性
            // 
            this.button检测账户唯一性.Location = new System.Drawing.Point(142, 837);
            this.button检测账户唯一性.Name = "button检测账户唯一性";
            this.button检测账户唯一性.Size = new System.Drawing.Size(97, 23);
            this.button检测账户唯一性.TabIndex = 235;
            this.button检测账户唯一性.Text = "检测账户唯一性";
            this.button检测账户唯一性.UseVisualStyleBackColor = true;
            this.button检测账户唯一性.Click += new System.EventHandler(this.button检测账户唯一性_Click);
            // 
            // Company_ID
            // 
            this.Company_ID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Company_ID.DataPropertyName = "Company_ID";
            this.Company_ID.HeaderText = "公司代码";
            this.Company_ID.Name = "Company_ID";
            this.Company_ID.ReadOnly = true;
            // 
            // Company_Name
            // 
            this.Company_Name.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Company_Name.DataPropertyName = "Company_Name";
            this.Company_Name.HeaderText = "公司名称";
            this.Company_Name.Name = "Company_Name";
            this.Company_Name.ReadOnly = true;
            // 
            // Buyer_Org
            // 
            this.Buyer_Org.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Buyer_Org.DataPropertyName = "Buyer_Org";
            this.Buyer_Org.HeaderText = "采购组织ID";
            this.Buyer_Org.Name = "Buyer_Org";
            this.Buyer_Org.ReadOnly = true;
            // 
            // Buyer_Org_Name
            // 
            this.Buyer_Org_Name.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Buyer_Org_Name.DataPropertyName = "Buyer_Org_Name";
            this.Buyer_Org_Name.HeaderText = "采购组织名称";
            this.Buyer_Org_Name.Name = "Buyer_Org_Name";
            this.Buyer_Org_Name.ReadOnly = true;
            // 
            // CreateCode
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(817, 872);
            this.Controls.Add(this.button检测账户唯一性);
            this.Controls.Add(this.button创建账户);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "CreateCode";
            this.Text = "创建供应商编码";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_选择供应商)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_选择账户组)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_选择公司代码)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_选择采购组织)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label 供应商名称label;
        private System.Windows.Forms.TextBox textbox查询供应商;
        private System.Windows.Forms.Button 查询供应商Button;
        private System.Windows.Forms.ComboBox comboBox筛选供应商;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dataGridView_选择供应商;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox textbox供应商名称结果;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox Textbox供应商账号;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textbox供应商流水号;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox textBox账户组名称;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button button查询账户组;
        private System.Windows.Forms.TextBox textBox号码段ID;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox TextBox一次性供应商标识;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textBox账户组描述;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox TextBox账户组ID;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DataGridView dataGridView_选择账户组;
        private System.Windows.Forms.ComboBox comboBox筛选账户组;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button button查询公司代码;
        private System.Windows.Forms.TextBox textBox公司名称;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.DataGridView dataGridView_选择公司代码;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button button查询采购组织;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox TextBox公司名称结果;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox textBox公司代码结果;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.DataGridView dataGridView_选择采购组织;
        private System.Windows.Forms.TextBox textBox采购组织名称结果;
        private System.Windows.Forms.TextBox textBox采购组织结果;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Button button创建账户;
        private System.Windows.Forms.DataGridViewTextBoxColumn Supplier_Id;
        private System.Windows.Forms.DataGridViewTextBoxColumn SupplierName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ManagerName;
        private System.Windows.Forms.DataGridViewTextBoxColumn LoginAccount;
        private System.Windows.Forms.DataGridViewTextBoxColumn Account_Group_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Description;
        private System.Windows.Forms.DataGridViewTextBoxColumn SingleStatus;
        private System.Windows.Forms.DataGridViewTextBoxColumn NS_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn Status;
        private System.Windows.Forms.DataGridViewTextBoxColumn NS_Begin;
        private System.Windows.Forms.DataGridViewTextBoxColumn NS_End;
        private System.Windows.Forms.DataGridViewTextBoxColumn NS_status;
        private System.Windows.Forms.TextBox textBox外部输入框;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Button button检测账户唯一性;
        private System.Windows.Forms.Label label外部;
        private System.Windows.Forms.DataGridViewTextBoxColumn Company_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Company_Name;
        private System.Windows.Forms.DataGridViewTextBoxColumn Buyer_Org;
        private System.Windows.Forms.DataGridViewTextBoxColumn Buyer_Org_Name;
    }
}