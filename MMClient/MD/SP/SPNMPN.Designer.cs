﻿namespace MMClient.MD.SP
{
    partial class SPNMPN
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox21 = new System.Windows.Forms.GroupBox();
            this.textBox38 = new System.Windows.Forms.TextBox();
            this.label116 = new System.Windows.Forms.Label();
            this.button36 = new System.Windows.Forms.Button();
            this.button37 = new System.Windows.Forms.Button();
            this.listBox66 = new System.Windows.Forms.ListBox();
            this.label117 = new System.Windows.Forms.Label();
            this.listBox69 = new System.Windows.Forms.ListBox();
            this.label120 = new System.Windows.Forms.Label();
            this.listBox70 = new System.Windows.Forms.ListBox();
            this.label121 = new System.Windows.Forms.Label();
            this.listBox71 = new System.Windows.Forms.ListBox();
            this.label122 = new System.Windows.Forms.Label();
            this.listBox72 = new System.Windows.Forms.ListBox();
            this.label123 = new System.Windows.Forms.Label();
            this.groupBox23 = new System.Windows.Forms.GroupBox();
            this.listBox67 = new System.Windows.Forms.ListBox();
            this.label118 = new System.Windows.Forms.Label();
            this.listBox68 = new System.Windows.Forms.ListBox();
            this.label119 = new System.Windows.Forms.Label();
            this.listBox73 = new System.Windows.Forms.ListBox();
            this.label124 = new System.Windows.Forms.Label();
            this.listBox74 = new System.Windows.Forms.ListBox();
            this.label125 = new System.Windows.Forms.Label();
            this.listBox75 = new System.Windows.Forms.ListBox();
            this.label126 = new System.Windows.Forms.Label();
            this.listBox76 = new System.Windows.Forms.ListBox();
            this.label127 = new System.Windows.Forms.Label();
            this.button38 = new System.Windows.Forms.Button();
            this.button39 = new System.Windows.Forms.Button();
            this.groupBox22 = new System.Windows.Forms.GroupBox();
            this.dataGridView11 = new System.Windows.Forms.DataGridView();
            this.groupBox21.SuspendLayout();
            this.groupBox23.SuspendLayout();
            this.groupBox22.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView11)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox21
            // 
            this.groupBox21.Controls.Add(this.textBox38);
            this.groupBox21.Controls.Add(this.label116);
            this.groupBox21.Controls.Add(this.button36);
            this.groupBox21.Controls.Add(this.button37);
            this.groupBox21.Controls.Add(this.listBox66);
            this.groupBox21.Controls.Add(this.label117);
            this.groupBox21.Controls.Add(this.listBox69);
            this.groupBox21.Controls.Add(this.label120);
            this.groupBox21.Controls.Add(this.listBox70);
            this.groupBox21.Controls.Add(this.label121);
            this.groupBox21.Controls.Add(this.listBox71);
            this.groupBox21.Controls.Add(this.label122);
            this.groupBox21.Controls.Add(this.listBox72);
            this.groupBox21.Controls.Add(this.label123);
            this.groupBox21.Location = new System.Drawing.Point(105, 44);
            this.groupBox21.Name = "groupBox21";
            this.groupBox21.Size = new System.Drawing.Size(631, 219);
            this.groupBox21.TabIndex = 21;
            this.groupBox21.TabStop = false;
            this.groupBox21.Text = "条件选择";
            // 
            // textBox38
            // 
            this.textBox38.Location = new System.Drawing.Point(459, 170);
            this.textBox38.Name = "textBox38";
            this.textBox38.Size = new System.Drawing.Size(117, 21);
            this.textBox38.TabIndex = 35;
            // 
            // label116
            // 
            this.label116.AutoSize = true;
            this.label116.Location = new System.Drawing.Point(344, 170);
            this.label116.Name = "label116";
            this.label116.Size = new System.Drawing.Size(65, 12);
            this.label116.TabIndex = 34;
            this.label116.Text = "最大命中数";
            // 
            // button36
            // 
            this.button36.Location = new System.Drawing.Point(231, 170);
            this.button36.Name = "button36";
            this.button36.Size = new System.Drawing.Size(75, 23);
            this.button36.TabIndex = 33;
            this.button36.Text = "清除";
            this.button36.UseVisualStyleBackColor = true;
            // 
            // button37
            // 
            this.button37.Location = new System.Drawing.Point(96, 170);
            this.button37.Name = "button37";
            this.button37.Size = new System.Drawing.Size(75, 23);
            this.button37.TabIndex = 32;
            this.button37.Text = "查询";
            this.button37.UseVisualStyleBackColor = true;
            // 
            // listBox66
            // 
            this.listBox66.FormattingEnabled = true;
            this.listBox66.ItemHeight = 12;
            this.listBox66.Location = new System.Drawing.Point(154, 115);
            this.listBox66.Name = "listBox66";
            this.listBox66.Size = new System.Drawing.Size(120, 16);
            this.listBox66.TabIndex = 31;
            // 
            // label117
            // 
            this.label117.AutoSize = true;
            this.label117.Location = new System.Drawing.Point(40, 115);
            this.label117.Name = "label117";
            this.label117.Size = new System.Drawing.Size(29, 12);
            this.label117.TabIndex = 30;
            this.label117.Text = "其他";
            // 
            // listBox69
            // 
            this.listBox69.FormattingEnabled = true;
            this.listBox69.ItemHeight = 12;
            this.listBox69.Location = new System.Drawing.Point(459, 60);
            this.listBox69.Name = "listBox69";
            this.listBox69.Size = new System.Drawing.Size(120, 16);
            this.listBox69.TabIndex = 25;
            // 
            // label120
            // 
            this.label120.AutoSize = true;
            this.label120.Location = new System.Drawing.Point(345, 60);
            this.label120.Name = "label120";
            this.label120.Size = new System.Drawing.Size(17, 12);
            this.label120.TabIndex = 24;
            this.label120.Text = "至";
            // 
            // listBox70
            // 
            this.listBox70.FormattingEnabled = true;
            this.listBox70.ItemHeight = 12;
            this.listBox70.Location = new System.Drawing.Point(154, 60);
            this.listBox70.Name = "listBox70";
            this.listBox70.Size = new System.Drawing.Size(120, 16);
            this.listBox70.TabIndex = 23;
            // 
            // label121
            // 
            this.label121.AutoSize = true;
            this.label121.Location = new System.Drawing.Point(40, 60);
            this.label121.Name = "label121";
            this.label121.Size = new System.Drawing.Size(65, 12);
            this.label121.TabIndex = 22;
            this.label121.Text = "制造商名称";
            // 
            // listBox71
            // 
            this.listBox71.FormattingEnabled = true;
            this.listBox71.ItemHeight = 12;
            this.listBox71.Location = new System.Drawing.Point(459, 20);
            this.listBox71.Name = "listBox71";
            this.listBox71.Size = new System.Drawing.Size(120, 16);
            this.listBox71.TabIndex = 21;
            // 
            // label122
            // 
            this.label122.AutoSize = true;
            this.label122.Location = new System.Drawing.Point(345, 20);
            this.label122.Name = "label122";
            this.label122.Size = new System.Drawing.Size(17, 12);
            this.label122.TabIndex = 20;
            this.label122.Text = "至";
            // 
            // listBox72
            // 
            this.listBox72.FormattingEnabled = true;
            this.listBox72.ItemHeight = 12;
            this.listBox72.Location = new System.Drawing.Point(154, 20);
            this.listBox72.Name = "listBox72";
            this.listBox72.Size = new System.Drawing.Size(120, 16);
            this.listBox72.TabIndex = 19;
            // 
            // label123
            // 
            this.label123.AutoSize = true;
            this.label123.Location = new System.Drawing.Point(40, 20);
            this.label123.Name = "label123";
            this.label123.Size = new System.Drawing.Size(65, 12);
            this.label123.TabIndex = 18;
            this.label123.Text = "制造商编码";
            // 
            // groupBox23
            // 
            this.groupBox23.Controls.Add(this.listBox67);
            this.groupBox23.Controls.Add(this.label118);
            this.groupBox23.Controls.Add(this.listBox68);
            this.groupBox23.Controls.Add(this.label119);
            this.groupBox23.Controls.Add(this.listBox73);
            this.groupBox23.Controls.Add(this.label124);
            this.groupBox23.Controls.Add(this.listBox74);
            this.groupBox23.Controls.Add(this.label125);
            this.groupBox23.Controls.Add(this.listBox75);
            this.groupBox23.Controls.Add(this.label126);
            this.groupBox23.Controls.Add(this.listBox76);
            this.groupBox23.Controls.Add(this.label127);
            this.groupBox23.Controls.Add(this.button38);
            this.groupBox23.Controls.Add(this.button39);
            this.groupBox23.Location = new System.Drawing.Point(103, 512);
            this.groupBox23.Name = "groupBox23";
            this.groupBox23.Size = new System.Drawing.Size(622, 194);
            this.groupBox23.TabIndex = 20;
            this.groupBox23.TabStop = false;
            this.groupBox23.Text = "修改查询结果";
            // 
            // listBox67
            // 
            this.listBox67.DisplayMember = "Material_Name";
            this.listBox67.FormattingEnabled = true;
            this.listBox67.ItemHeight = 12;
            this.listBox67.Location = new System.Drawing.Point(436, 113);
            this.listBox67.Name = "listBox67";
            this.listBox67.Size = new System.Drawing.Size(120, 16);
            this.listBox67.TabIndex = 35;
            // 
            // label118
            // 
            this.label118.AutoSize = true;
            this.label118.Location = new System.Drawing.Point(322, 113);
            this.label118.Name = "label118";
            this.label118.Size = new System.Drawing.Size(41, 12);
            this.label118.TabIndex = 34;
            this.label118.Text = "修改至";
            // 
            // listBox68
            // 
            this.listBox68.DisplayMember = "Material_Name";
            this.listBox68.FormattingEnabled = true;
            this.listBox68.ItemHeight = 12;
            this.listBox68.Location = new System.Drawing.Point(131, 113);
            this.listBox68.Name = "listBox68";
            this.listBox68.Size = new System.Drawing.Size(120, 16);
            this.listBox68.TabIndex = 33;
            // 
            // label119
            // 
            this.label119.AutoSize = true;
            this.label119.Location = new System.Drawing.Point(17, 113);
            this.label119.Name = "label119";
            this.label119.Size = new System.Drawing.Size(65, 12);
            this.label119.TabIndex = 32;
            this.label119.Text = "供应商编码";
            // 
            // listBox73
            // 
            this.listBox73.DisplayMember = "Material_Name";
            this.listBox73.FormattingEnabled = true;
            this.listBox73.ItemHeight = 12;
            this.listBox73.Location = new System.Drawing.Point(436, 68);
            this.listBox73.Name = "listBox73";
            this.listBox73.Size = new System.Drawing.Size(120, 16);
            this.listBox73.TabIndex = 31;
            // 
            // label124
            // 
            this.label124.AutoSize = true;
            this.label124.Location = new System.Drawing.Point(322, 68);
            this.label124.Name = "label124";
            this.label124.Size = new System.Drawing.Size(41, 12);
            this.label124.TabIndex = 30;
            this.label124.Text = "修改至";
            // 
            // listBox74
            // 
            this.listBox74.DisplayMember = "Material_Name";
            this.listBox74.FormattingEnabled = true;
            this.listBox74.ItemHeight = 12;
            this.listBox74.Location = new System.Drawing.Point(131, 68);
            this.listBox74.Name = "listBox74";
            this.listBox74.Size = new System.Drawing.Size(120, 16);
            this.listBox74.TabIndex = 29;
            // 
            // label125
            // 
            this.label125.AutoSize = true;
            this.label125.Location = new System.Drawing.Point(17, 68);
            this.label125.Name = "label125";
            this.label125.Size = new System.Drawing.Size(65, 12);
            this.label125.TabIndex = 28;
            this.label125.Text = "制造商名称";
            // 
            // listBox75
            // 
            this.listBox75.DisplayMember = "Material_Name";
            this.listBox75.FormattingEnabled = true;
            this.listBox75.ItemHeight = 12;
            this.listBox75.Location = new System.Drawing.Point(436, 28);
            this.listBox75.Name = "listBox75";
            this.listBox75.Size = new System.Drawing.Size(120, 16);
            this.listBox75.TabIndex = 27;
            // 
            // label126
            // 
            this.label126.AutoSize = true;
            this.label126.Location = new System.Drawing.Point(322, 28);
            this.label126.Name = "label126";
            this.label126.Size = new System.Drawing.Size(41, 12);
            this.label126.TabIndex = 26;
            this.label126.Text = "修改至";
            // 
            // listBox76
            // 
            this.listBox76.DisplayMember = "Material_Name";
            this.listBox76.FormattingEnabled = true;
            this.listBox76.ItemHeight = 12;
            this.listBox76.Location = new System.Drawing.Point(131, 28);
            this.listBox76.Name = "listBox76";
            this.listBox76.Size = new System.Drawing.Size(120, 16);
            this.listBox76.TabIndex = 25;
            // 
            // label127
            // 
            this.label127.AutoSize = true;
            this.label127.Location = new System.Drawing.Point(17, 28);
            this.label127.Name = "label127";
            this.label127.Size = new System.Drawing.Size(65, 12);
            this.label127.TabIndex = 24;
            this.label127.Text = "制造商编码";
            // 
            // button38
            // 
            this.button38.Location = new System.Drawing.Point(382, 149);
            this.button38.Name = "button38";
            this.button38.Size = new System.Drawing.Size(75, 23);
            this.button38.TabIndex = 23;
            this.button38.Text = "重置";
            this.button38.UseVisualStyleBackColor = true;
            // 
            // button39
            // 
            this.button39.Location = new System.Drawing.Point(165, 149);
            this.button39.Name = "button39";
            this.button39.Size = new System.Drawing.Size(75, 23);
            this.button39.TabIndex = 22;
            this.button39.Text = "确定";
            this.button39.UseVisualStyleBackColor = true;
            // 
            // groupBox22
            // 
            this.groupBox22.Controls.Add(this.dataGridView11);
            this.groupBox22.Location = new System.Drawing.Point(103, 275);
            this.groupBox22.Name = "groupBox22";
            this.groupBox22.Size = new System.Drawing.Size(636, 231);
            this.groupBox22.TabIndex = 19;
            this.groupBox22.TabStop = false;
            this.groupBox22.Text = "查询结果";
            // 
            // dataGridView11
            // 
            this.dataGridView11.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView11.Location = new System.Drawing.Point(7, 21);
            this.dataGridView11.Name = "dataGridView11";
            this.dataGridView11.RowTemplate.Height = 23;
            this.dataGridView11.Size = new System.Drawing.Size(623, 204);
            this.dataGridView11.TabIndex = 0;
            // 
            // SPNMPN
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 750);
            this.Controls.Add(this.groupBox21);
            this.Controls.Add(this.groupBox23);
            this.Controls.Add(this.groupBox22);
            this.Name = "SPNMPN";
            this.Text = "SPNMPN";
            this.groupBox21.ResumeLayout(false);
            this.groupBox21.PerformLayout();
            this.groupBox23.ResumeLayout(false);
            this.groupBox23.PerformLayout();
            this.groupBox22.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView11)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox21;
        private System.Windows.Forms.TextBox textBox38;
        private System.Windows.Forms.Label label116;
        private System.Windows.Forms.Button button36;
        private System.Windows.Forms.Button button37;
        private System.Windows.Forms.ListBox listBox66;
        private System.Windows.Forms.Label label117;
        private System.Windows.Forms.ListBox listBox69;
        private System.Windows.Forms.Label label120;
        private System.Windows.Forms.ListBox listBox70;
        private System.Windows.Forms.Label label121;
        private System.Windows.Forms.ListBox listBox71;
        private System.Windows.Forms.Label label122;
        private System.Windows.Forms.ListBox listBox72;
        private System.Windows.Forms.Label label123;
        private System.Windows.Forms.GroupBox groupBox23;
        private System.Windows.Forms.ListBox listBox67;
        private System.Windows.Forms.Label label118;
        private System.Windows.Forms.ListBox listBox68;
        private System.Windows.Forms.Label label119;
        private System.Windows.Forms.ListBox listBox73;
        private System.Windows.Forms.Label label124;
        private System.Windows.Forms.ListBox listBox74;
        private System.Windows.Forms.Label label125;
        private System.Windows.Forms.ListBox listBox75;
        private System.Windows.Forms.Label label126;
        private System.Windows.Forms.ListBox listBox76;
        private System.Windows.Forms.Label label127;
        private System.Windows.Forms.Button button38;
        private System.Windows.Forms.Button button39;
        private System.Windows.Forms.GroupBox groupBox22;
        private System.Windows.Forms.DataGridView dataGridView11;
    }
}