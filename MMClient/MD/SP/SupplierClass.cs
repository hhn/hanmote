﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.SqlServerDAL;
using Lib.SqlServerDAL.MDDAL.MT;
using Lib.Model.MD.SP;
using Lib.Model.MD.MT;
using Lib.Common.CommonUtils;

namespace MMClient.MD.SP
{
    public partial class SupplierClass : Form
    {
        MaterialClassDAL mcdal = new MaterialClassDAL();
        int flag = 0;
        /// <summary>
        /// 新建时候用
        /// </summary>
        string classid;
        /// <summary>
        /// 维护时候用
        /// </summary>
        string classid1;
        int rowcount = 0;

        public SupplierClass()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SupplierChose spc = new SupplierChose(this);
            spc.Show();
        }

        private void tbx_供应商编码_TextChanged(object sender, EventArgs e)
        {
            string sql = "SELECT * FROM [Supplier_Feature] WHERE Supplier_ID = '" + tbx_供应商编码.Text + "'";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            if (dt.Rows.Count == 0)
            {
                flag = 0;
                DataTable dt1 = mcdal.GetDistinctClassInfo();
                List<string> list = new List<string>();
                for (int i = 0; i < dt1.Rows.Count; i++)
                {
                    string mtid = dt1.Rows[i]["Class_Name"].ToString();
                    list.Add(mtid);
                }
                cbb_特性类.DataSource = list;
                cbb_特性类.Text = "";
            }
            else
            {
                classid1 = dt.Rows[0]["Class_ID"].ToString();
                string sql2 = "SELECT DISTINCT Class_Name FROM [Class_Feature] WHERE Class_ID = '" + classid1 + "'";
                DataTable dt2 = DBHelper.ExecuteQueryDT(sql2);
                string classname = dt2.Rows[0][0].ToString();
                cbb_特性类.Text = classname;
                cbb_特性类.Enabled = false;
                List<Supplier_Feature> listfe = new List<Supplier_Feature>();
                listfe = mcdal.GetFeatureInfoByClassIDSPID(classid1, tbx_供应商编码.Text);
                for (int i = 0; i < listfe.Count; i++)
                {
                    dgv_类特性.Rows[i].Cells["cln_特征描述"].Value = listfe[i].Feature_Name;
                    dgv_类特性.Rows[i].Cells["cln_特性编码"].Value = listfe[i].Feature_ID;
                    dgv_类特性.Rows[i].Cells["cln_值"].Value = listfe[i].Feature_Value;
                    rowcount++;
                }
                btn_维护特征.Visible = false;
                flag = 1;
            }

        }

        private void btn_维护特征_Click(object sender, EventArgs e)
        {
            ///读取分类编码
            string sql = "SELECT DISTINCT Class_ID FROM [Class_Feature] WHERE Class_Name='" + cbb_特性类.Text + "'";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            classid = dt.Rows[0][0].ToString();
            List<Class_Feature> list = mcdal.GetFeatureInfoByClassID(classid);
            for (int i = 0; i < list.Count; i++)
            {
                dgv_类特性.Rows[i].Cells["cln_特征描述"].Value = list[i].Feature_Name;
                dgv_类特性.Rows[i].Cells["cln_特性编码"].Value = list[i].Feature_ID;
                rowcount++;
            }
        }

        private void btn_维护值_Click(object sender, EventArgs e)
        {
            if (flag == 0)
            {
                List<Supplier_Feature> listsf = new List<Supplier_Feature>();
                string spid = tbx_供应商编码.Text;
                for (int i = 0; i < rowcount; i++)
                {
                    Supplier_Feature spfe = new Supplier_Feature();
                    spfe.Class_ID = classid;
                    spfe.Class_Name = cbb_特性类.Text;
                    spfe.Feature_ID = dgv_类特性.Rows[i].Cells["cln_特性编码"].Value.ToString();
                    spfe.Feature_Name = dgv_类特性.Rows[i].Cells["cln_特征描述"].Value.ToString();
                    spfe.Feature_Value = dgv_类特性.Rows[i].Cells["cln_值"].Value.ToString();
                    spfe.Supplier_ID = spid;
                    listsf.Add(spfe);
                }
                mcdal.InsertFeatureInfosp(listsf);
            }
            else
            {
                List<Supplier_Feature> listsf = new List<Supplier_Feature>();
                string spid = tbx_供应商编码.Text;
                for (int i = 0; i < rowcount; i++)
                {
                    Supplier_Feature spfe = new Supplier_Feature();
                    spfe.Class_ID = classid;
                    spfe.Class_Name = cbb_特性类.Text;
                    spfe.Feature_ID = dgv_类特性.Rows[i].Cells["cln_特性编码"].Value.ToString();
                    spfe.Feature_Name = dgv_类特性.Rows[i].Cells["cln_特征描述"].Value.ToString();
                    spfe.Feature_Value = dgv_类特性.Rows[i].Cells["cln_值"].Value.ToString();
                    spfe.Supplier_ID = spid;
                    listsf.Add(spfe);
                }
                
                mcdal.UpdateFeatureInfosp(listsf);
            }
            MessageUtil.ShowTips("维护成功");
        }

        private void SupplierClass_Load(object sender, EventArgs e)
        {
            dgv_类特性.Rows.Add(100);
        }
    }
}
