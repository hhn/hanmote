﻿using Lib.IDAL.MDIDAL.SP;
using Lib.Model.MD.SP;
using Lib.SqlServerDAL.MDDAL.SP;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MMClient.MD.SP
{
    public partial class UpdateAG : Form
    {
        Account_Group account_Group = new Account_Group();
        Account_GroupIDAL account_GroupIDAL = new Account_GroupDAL();
        NSIDAL NSIDAL = new NSDAL();
        DataTable dt1,dt2;
        Boolean AGNSFlag;

        public UpdateAG(string account_Group_ID)
        {
            InitializeComponent();
            account_Group = account_GroupIDAL.selectbyid(account_Group_ID);
            init();
        }

        public void init() {
            textBox1.Text = account_Group.Account_Group_ID.ToString();
            textBox1.Enabled = false;
            button1.Enabled = false;

            if (account_Group.SingleStatus.Equals("是"))
            {
                checkBox1.Checked = true;
                textBox2.Text = account_Group.Description;
                textBox2.Enabled = false;
            }
            else {
                checkBox1.Checked = false;
                textBox2.Text = account_Group.Description;
                textBox2.Enabled = true;
            }

            button2.Enabled = false;
            comboBox1.Text = account_Group.NS_id;
            comboBox1.Enabled = false;
            AGNSFlag = true;

        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked == true)
            {
                textBox2.Text = "一次性供应商";
                textBox2.Enabled = false;
                account_Group.Description = "一次性供应商";
                account_Group.SingleStatus = "是";
            }
            else {
                textBox2.Text = "";
                textBox2.Enabled = true;
                account_Group.SingleStatus = "否";
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            dt1 = NSIDAL.selectbyname(textBox3.Text.ToString());
            comboBox1.DataSource = dt1;
            comboBox1.DisplayMember = "Description";
            comboBox1.ValueMember = "Number_ID";
            try
            {
                label6.Text = "号码段ID:" + comboBox1.SelectedValue.ToString();
                account_Group.NS_id = comboBox1.SelectedValue.ToString();
            }
            catch
            {
                MessageBox.Show("没有此号码段,已为您查找全部!");
                getdt2();
            }
            finally
            {
                AGNSFlag = true;
                comboBox1.Enabled = true;
            }
        }

        private void getdt2()
        {
            dt2 = NSIDAL.selectAllID();
            comboBox1.DataSource = dt2;
            comboBox1.DisplayMember = "Description";
            comboBox1.ValueMember = "Number_ID";
            label6.Text = "号码段ID:" + comboBox1.SelectedValue.ToString();
            account_Group.NS_id = comboBox1.SelectedValue.ToString();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox1.SelectedValue != null)
            {
                account_Group.NS_id = label6.Text = comboBox1.SelectedValue.ToString();
                AGNSFlag = true;
            }
            else
            {
                AGNSFlag = false;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (AGNSFlag)
            {
                string messsage = account_GroupIDAL.update(account_Group);
                MessageBox.Show(messsage);
            }
            this.Close();
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            account_Group.Description = textBox2.Text.ToString().Trim();
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            if (textBox3.Text.ToString().Trim().Equals("") || textBox3.Text == null)
            {
                button2.Enabled = false;
                AGNSFlag = false;
                MessageBox.Show("请输入号码段描述后再查找");
            }
            else
            {
                button2.Enabled = true;
            }
        }
    }
}
