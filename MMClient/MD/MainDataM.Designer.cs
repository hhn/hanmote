﻿namespace MMClient
{
    partial class MainDataM
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.TreeNode treeNode1 = new System.Windows.Forms.TreeNode("查询");
            System.Windows.Forms.TreeNode treeNode2 = new System.Windows.Forms.TreeNode("创建");
            System.Windows.Forms.TreeNode treeNode3 = new System.Windows.Forms.TreeNode("修改");
            System.Windows.Forms.TreeNode treeNode4 = new System.Windows.Forms.TreeNode("基础数据");
            System.Windows.Forms.TreeNode treeNode5 = new System.Windows.Forms.TreeNode("采购");
            System.Windows.Forms.TreeNode treeNode6 = new System.Windows.Forms.TreeNode("会计");
            System.Windows.Forms.TreeNode treeNode7 = new System.Windows.Forms.TreeNode("存储");
            System.Windows.Forms.TreeNode treeNode8 = new System.Windows.Forms.TreeNode("MRP");
            System.Windows.Forms.TreeNode treeNode9 = new System.Windows.Forms.TreeNode("维护", new System.Windows.Forms.TreeNode[] {
            treeNode4,
            treeNode5,
            treeNode6,
            treeNode7,
            treeNode8});
            System.Windows.Forms.TreeNode treeNode10 = new System.Windows.Forms.TreeNode("分发");
            System.Windows.Forms.TreeNode treeNode11 = new System.Windows.Forms.TreeNode("数据录入");
            System.Windows.Forms.TreeNode treeNode12 = new System.Windows.Forms.TreeNode("数据修改");
            System.Windows.Forms.TreeNode treeNode13 = new System.Windows.Forms.TreeNode("数据查询");
            System.Windows.Forms.TreeNode treeNode14 = new System.Windows.Forms.TreeNode("数据出入库");
            System.Windows.Forms.TreeNode treeNode15 = new System.Windows.Forms.TreeNode("批次管理", new System.Windows.Forms.TreeNode[] {
            treeNode11,
            treeNode12,
            treeNode13,
            treeNode14});
            System.Windows.Forms.TreeNode treeNode16 = new System.Windows.Forms.TreeNode("记录变更");
            System.Windows.Forms.TreeNode treeNode17 = new System.Windows.Forms.TreeNode("工程型变更");
            System.Windows.Forms.TreeNode treeNode18 = new System.Windows.Forms.TreeNode("节点1");
            System.Windows.Forms.TreeNode treeNode19 = new System.Windows.Forms.TreeNode("物料组", new System.Windows.Forms.TreeNode[] {
            treeNode18});
            System.Windows.Forms.TreeNode treeNode20 = new System.Windows.Forms.TreeNode("物料类型");
            System.Windows.Forms.TreeNode treeNode21 = new System.Windows.Forms.TreeNode("物料主数据", new System.Windows.Forms.TreeNode[] {
            treeNode1,
            treeNode2,
            treeNode3,
            treeNode9,
            treeNode10,
            treeNode15,
            treeNode16,
            treeNode17,
            treeNode19,
            treeNode20});
            System.Windows.Forms.TreeNode treeNode22 = new System.Windows.Forms.TreeNode("查询");
            System.Windows.Forms.TreeNode treeNode23 = new System.Windows.Forms.TreeNode("创建");
            System.Windows.Forms.TreeNode treeNode24 = new System.Windows.Forms.TreeNode("修改");
            System.Windows.Forms.TreeNode treeNode25 = new System.Windows.Forms.TreeNode("分发");
            System.Windows.Forms.TreeNode treeNode26 = new System.Windows.Forms.TreeNode("基本视图");
            System.Windows.Forms.TreeNode treeNode27 = new System.Windows.Forms.TreeNode("采购组织视图");
            System.Windows.Forms.TreeNode treeNode28 = new System.Windows.Forms.TreeNode("公司代码视图");
            System.Windows.Forms.TreeNode treeNode29 = new System.Windows.Forms.TreeNode("维护", new System.Windows.Forms.TreeNode[] {
            treeNode26,
            treeNode27,
            treeNode28});
            System.Windows.Forms.TreeNode treeNode30 = new System.Windows.Forms.TreeNode("MPN");
            System.Windows.Forms.TreeNode treeNode31 = new System.Windows.Forms.TreeNode("非MPN");
            System.Windows.Forms.TreeNode treeNode32 = new System.Windows.Forms.TreeNode("制造商管理", new System.Windows.Forms.TreeNode[] {
            treeNode30,
            treeNode31});
            System.Windows.Forms.TreeNode treeNode33 = new System.Windows.Forms.TreeNode("供应商主数据", new System.Windows.Forms.TreeNode[] {
            treeNode22,
            treeNode23,
            treeNode24,
            treeNode25,
            treeNode29,
            treeNode32});
            System.Windows.Forms.TreeNode treeNode34 = new System.Windows.Forms.TreeNode("查询");
            System.Windows.Forms.TreeNode treeNode35 = new System.Windows.Forms.TreeNode("创建");
            System.Windows.Forms.TreeNode treeNode36 = new System.Windows.Forms.TreeNode("修改");
            System.Windows.Forms.TreeNode treeNode37 = new System.Windows.Forms.TreeNode("分发");
            System.Windows.Forms.TreeNode treeNode38 = new System.Windows.Forms.TreeNode("维护");
            System.Windows.Forms.TreeNode treeNode39 = new System.Windows.Forms.TreeNode("专家库", new System.Windows.Forms.TreeNode[] {
            treeNode34,
            treeNode35,
            treeNode36,
            treeNode37,
            treeNode38});
            System.Windows.Forms.TreeNode treeNode40 = new System.Windows.Forms.TreeNode("查询");
            System.Windows.Forms.TreeNode treeNode41 = new System.Windows.Forms.TreeNode("创建");
            System.Windows.Forms.TreeNode treeNode42 = new System.Windows.Forms.TreeNode("修改");
            System.Windows.Forms.TreeNode treeNode43 = new System.Windows.Forms.TreeNode("分发");
            System.Windows.Forms.TreeNode treeNode44 = new System.Windows.Forms.TreeNode("维护");
            System.Windows.Forms.TreeNode treeNode45 = new System.Windows.Forms.TreeNode("价格库", new System.Windows.Forms.TreeNode[] {
            treeNode40,
            treeNode41,
            treeNode42,
            treeNode43,
            treeNode44});
            System.Windows.Forms.TreeNode treeNode46 = new System.Windows.Forms.TreeNode("计量单位");
            System.Windows.Forms.TreeNode treeNode47 = new System.Windows.Forms.TreeNode("货币");
            System.Windows.Forms.TreeNode treeNode48 = new System.Windows.Forms.TreeNode("国家");
            System.Windows.Forms.TreeNode treeNode49 = new System.Windows.Forms.TreeNode("节点2");
            System.Windows.Forms.TreeNode treeNode50 = new System.Windows.Forms.TreeNode("节点3");
            System.Windows.Forms.TreeNode treeNode51 = new System.Windows.Forms.TreeNode("一般设置", new System.Windows.Forms.TreeNode[] {
            treeNode46,
            treeNode47,
            treeNode48,
            treeNode49,
            treeNode50});
            System.Windows.Forms.TreeNode treeNode52 = new System.Windows.Forms.TreeNode("公司");
            System.Windows.Forms.TreeNode treeNode53 = new System.Windows.Forms.TreeNode("工厂");
            System.Windows.Forms.TreeNode treeNode54 = new System.Windows.Forms.TreeNode("仓库");
            System.Windows.Forms.TreeNode treeNode55 = new System.Windows.Forms.TreeNode("采购组织");
            System.Windows.Forms.TreeNode treeNode56 = new System.Windows.Forms.TreeNode("组织结构", new System.Windows.Forms.TreeNode[] {
            treeNode52,
            treeNode53,
            treeNode54,
            treeNode55});
            System.Windows.Forms.TreeNode treeNode57 = new System.Windows.Forms.TreeNode("生成会计凭证");
            System.Windows.Forms.TreeNode treeNode58 = new System.Windows.Forms.TreeNode("查询会计凭证");
            System.Windows.Forms.TreeNode treeNode59 = new System.Windows.Forms.TreeNode("查询科目明细");
            System.Windows.Forms.TreeNode treeNode60 = new System.Windows.Forms.TreeNode("财务主数据", new System.Windows.Forms.TreeNode[] {
            treeNode57,
            treeNode58,
            treeNode59});
            WeifenLuo.WinFormsUI.Docking.DockPanelSkin dockPanelSkin1 = new WeifenLuo.WinFormsUI.Docking.DockPanelSkin();
            WeifenLuo.WinFormsUI.Docking.AutoHideStripSkin autoHideStripSkin1 = new WeifenLuo.WinFormsUI.Docking.AutoHideStripSkin();
            WeifenLuo.WinFormsUI.Docking.DockPanelGradient dockPanelGradient1 = new WeifenLuo.WinFormsUI.Docking.DockPanelGradient();
            WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient1 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
            WeifenLuo.WinFormsUI.Docking.DockPaneStripSkin dockPaneStripSkin1 = new WeifenLuo.WinFormsUI.Docking.DockPaneStripSkin();
            WeifenLuo.WinFormsUI.Docking.DockPaneStripGradient dockPaneStripGradient1 = new WeifenLuo.WinFormsUI.Docking.DockPaneStripGradient();
            WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient2 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
            WeifenLuo.WinFormsUI.Docking.DockPanelGradient dockPanelGradient2 = new WeifenLuo.WinFormsUI.Docking.DockPanelGradient();
            WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient3 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
            WeifenLuo.WinFormsUI.Docking.DockPaneStripToolWindowGradient dockPaneStripToolWindowGradient1 = new WeifenLuo.WinFormsUI.Docking.DockPaneStripToolWindowGradient();
            WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient4 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
            WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient5 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
            WeifenLuo.WinFormsUI.Docking.DockPanelGradient dockPanelGradient3 = new WeifenLuo.WinFormsUI.Docking.DockPanelGradient();
            WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient6 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
            WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient7 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tV_md = new System.Windows.Forms.TreeView();
            this.dockPanel1 = new WeifenLuo.WinFormsUI.Docking.DockPanel();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 19.13289F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 80.8671F));
            this.tableLayoutPanel1.Controls.Add(this.tV_md, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.dockPanel1, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(4);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1274, 925);
            this.tableLayoutPanel1.TabIndex = 14;
            // 
            // tV_md
            // 
            this.tV_md.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tV_md.Location = new System.Drawing.Point(4, 4);
            this.tV_md.Margin = new System.Windows.Forms.Padding(4);
            this.tV_md.Name = "tV_md";
            treeNode1.Name = "查询";
            treeNode1.Tag = "10";
            treeNode1.Text = "查询";
            treeNode1.ToolTipText = "查询";
            treeNode2.Name = "创建";
            treeNode2.Tag = "11";
            treeNode2.Text = "创建";
            treeNode2.ToolTipText = "创建";
            treeNode3.Name = "MT_修改";
            treeNode3.Tag = "12";
            treeNode3.Text = "修改";
            treeNode3.ToolTipText = "修改";
            treeNode4.Name = "节点0";
            treeNode4.Tag = "130";
            treeNode4.Text = "基础数据";
            treeNode5.Name = "节点2";
            treeNode5.Tag = "131";
            treeNode5.Text = "采购";
            treeNode6.Name = "节点3";
            treeNode6.Tag = "132";
            treeNode6.Text = "会计";
            treeNode7.Name = "存储";
            treeNode7.Tag = "133";
            treeNode7.Text = "存储";
            treeNode8.Name = "节点1";
            treeNode8.Tag = "134";
            treeNode8.Text = "MRP";
            treeNode9.Name = "MT_维护";
            treeNode9.Tag = "13";
            treeNode9.Text = "维护";
            treeNode9.ToolTipText = "维护";
            treeNode10.Name = "MT_分发";
            treeNode10.Tag = "14";
            treeNode10.Text = "分发";
            treeNode10.ToolTipText = "分发";
            treeNode11.Name = "数据录入";
            treeNode11.Tag = "150";
            treeNode11.Text = "数据录入";
            treeNode11.ToolTipText = "数据录入";
            treeNode12.Name = "数据修改";
            treeNode12.Tag = "151";
            treeNode12.Text = "数据修改";
            treeNode12.ToolTipText = "数据修改";
            treeNode13.Name = "数据查询";
            treeNode13.Tag = "152";
            treeNode13.Text = "数据查询";
            treeNode13.ToolTipText = "数据查询";
            treeNode14.Name = "数据出入库";
            treeNode14.Tag = "153";
            treeNode14.Text = "数据出入库";
            treeNode14.ToolTipText = "数据出入库";
            treeNode15.Name = "MT_批次管理";
            treeNode15.Tag = "150";
            treeNode15.Text = "批次管理";
            treeNode15.ToolTipText = "批次管理";
            treeNode16.Name = "MT_记录变更";
            treeNode16.Tag = "16";
            treeNode16.Text = "记录变更";
            treeNode16.ToolTipText = "记录变更";
            treeNode17.Name = "MT_工程型变更";
            treeNode17.Tag = "17";
            treeNode17.Text = "工程型变更";
            treeNode17.ToolTipText = "工程型变更";
            treeNode18.Name = "节点1";
            treeNode18.Text = "节点1";
            treeNode19.Name = "物料组";
            treeNode19.Text = "物料组";
            treeNode19.ToolTipText = "物料组";
            treeNode20.Name = "物料类型";
            treeNode20.Text = "物料类型";
            treeNode20.ToolTipText = "物料类型";
            treeNode21.BackColor = System.Drawing.Color.Transparent;
            treeNode21.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            treeNode21.Name = "物料主数据";
            treeNode21.NodeFont = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            treeNode21.Tag = "1";
            treeNode21.Text = "物料主数据";
            treeNode21.ToolTipText = "物料主数据";
            treeNode22.Name = "SP_查询";
            treeNode22.Tag = "20";
            treeNode22.Text = "查询";
            treeNode22.ToolTipText = "查询";
            treeNode23.Name = "SP_创建";
            treeNode23.Tag = "21";
            treeNode23.Text = "创建";
            treeNode23.ToolTipText = "创建";
            treeNode24.Name = "SP_修改";
            treeNode24.Tag = "22";
            treeNode24.Text = "修改";
            treeNode24.ToolTipText = "修改";
            treeNode25.Name = "SP_分发";
            treeNode25.Tag = "23";
            treeNode25.Text = "分发";
            treeNode25.ToolTipText = "分发";
            treeNode26.Name = "节点0";
            treeNode26.Tag = "240";
            treeNode26.Text = "基本视图";
            treeNode27.Name = "节点1";
            treeNode27.Tag = "241";
            treeNode27.Text = "采购组织视图";
            treeNode28.Name = "节点2";
            treeNode28.Tag = "242";
            treeNode28.Text = "公司代码视图";
            treeNode29.Name = "SP_维护";
            treeNode29.Tag = "24";
            treeNode29.Text = "维护";
            treeNode29.ToolTipText = "维护";
            treeNode30.Name = "MPN";
            treeNode30.Tag = "250";
            treeNode30.Text = "MPN";
            treeNode30.ToolTipText = "MPN";
            treeNode31.Name = "非MPN";
            treeNode31.Tag = "251";
            treeNode31.Text = "非MPN";
            treeNode31.ToolTipText = "非MPN";
            treeNode32.Name = "制造商管理";
            treeNode32.Tag = "250";
            treeNode32.Text = "制造商管理";
            treeNode32.ToolTipText = "制造商管理";
            treeNode33.Name = "供应商主数据";
            treeNode33.NodeFont = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold);
            treeNode33.Tag = "2";
            treeNode33.Text = "供应商主数据";
            treeNode33.ToolTipText = "供应商主数据";
            treeNode34.Name = "SE_查询";
            treeNode34.Tag = "30";
            treeNode34.Text = "查询";
            treeNode34.ToolTipText = "查询";
            treeNode35.Name = "SE_创建";
            treeNode35.Tag = "31";
            treeNode35.Text = "创建";
            treeNode35.ToolTipText = "创建";
            treeNode36.Name = "SE_修改";
            treeNode36.Tag = "32";
            treeNode36.Text = "修改";
            treeNode36.ToolTipText = "修改";
            treeNode37.Name = "SE_分发";
            treeNode37.Tag = "33";
            treeNode37.Text = "分发";
            treeNode37.ToolTipText = "分发";
            treeNode38.Name = "SE_维护";
            treeNode38.Tag = "34";
            treeNode38.Text = "维护";
            treeNode38.ToolTipText = "维护";
            treeNode39.Name = "专家库";
            treeNode39.NodeFont = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold);
            treeNode39.Tag = "3";
            treeNode39.Text = "专家库";
            treeNode39.ToolTipText = "专家库";
            treeNode40.Name = "SE_查询";
            treeNode40.Tag = "40";
            treeNode40.Text = "查询";
            treeNode40.ToolTipText = "查询";
            treeNode41.Name = "SE_创建";
            treeNode41.Tag = "41";
            treeNode41.Text = "创建";
            treeNode41.ToolTipText = "创建";
            treeNode42.Name = "SE_修改";
            treeNode42.Tag = "42";
            treeNode42.Text = "修改";
            treeNode42.ToolTipText = "修改";
            treeNode43.Name = "SE_分发";
            treeNode43.Tag = "43";
            treeNode43.Text = "分发";
            treeNode43.ToolTipText = "分发";
            treeNode44.Name = "SE_维护";
            treeNode44.Tag = "44";
            treeNode44.Text = "维护";
            treeNode44.ToolTipText = "维护";
            treeNode45.Name = "价格库";
            treeNode45.NodeFont = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold);
            treeNode45.Tag = "4";
            treeNode45.Text = "价格库";
            treeNode46.Name = "计量单位";
            treeNode46.Tag = "50";
            treeNode46.Text = "计量单位";
            treeNode46.ToolTipText = "计量单位";
            treeNode47.Name = "货币";
            treeNode47.Tag = "51";
            treeNode47.Text = "货币";
            treeNode48.Name = "节点1";
            treeNode48.Tag = "52";
            treeNode48.Text = "国家";
            treeNode49.Name = "节点2";
            treeNode49.Text = "节点2";
            treeNode50.Name = "节点3";
            treeNode50.Text = "节点3";
            treeNode51.Name = "一般设置";
            treeNode51.NodeFont = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold);
            treeNode51.Tag = "5";
            treeNode51.Text = "一般设置";
            treeNode51.ToolTipText = "一般设置";
            treeNode52.Name = "公司";
            treeNode52.Tag = "60";
            treeNode52.Text = "公司";
            treeNode52.ToolTipText = "公司";
            treeNode53.Name = "工厂";
            treeNode53.Tag = "61";
            treeNode53.Text = "工厂";
            treeNode53.ToolTipText = "工厂";
            treeNode54.Name = "仓库";
            treeNode54.Tag = "62";
            treeNode54.Text = "仓库";
            treeNode54.ToolTipText = "仓库";
            treeNode55.Name = "采购组织";
            treeNode55.Tag = "63";
            treeNode55.Text = "采购组织";
            treeNode55.ToolTipText = "采购组织";
            treeNode56.Name = "组织结构";
            treeNode56.NodeFont = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold);
            treeNode56.Tag = "6";
            treeNode56.Text = "组织结构";
            treeNode56.ToolTipText = "组织结构";
            treeNode57.Name = "节点2";
            treeNode57.Tag = "70";
            treeNode57.Text = "生成会计凭证";
            treeNode58.Name = "节点3";
            treeNode58.Tag = "71";
            treeNode58.Text = "查询会计凭证";
            treeNode59.Name = "节点1";
            treeNode59.Tag = "72";
            treeNode59.Text = "查询科目明细";
            treeNode60.Name = "节点1";
            treeNode60.Text = "财务主数据";
            this.tV_md.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode21,
            treeNode33,
            treeNode39,
            treeNode45,
            treeNode51,
            treeNode56,
            treeNode60});
            this.tV_md.Size = new System.Drawing.Size(235, 917);
            this.tV_md.TabIndex = 15;
            this.tV_md.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.tV_md_AfterSelect);
            // 
            // dockPanel1
            // 
            this.dockPanel1.ActiveAutoHideContent = null;
            this.dockPanel1.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.dockPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dockPanel1.DockBackColor = System.Drawing.SystemColors.ButtonFace;
            this.dockPanel1.DocumentStyle = WeifenLuo.WinFormsUI.Docking.DocumentStyle.DockingWindow;
            this.dockPanel1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.dockPanel1.Location = new System.Drawing.Point(247, 4);
            this.dockPanel1.Margin = new System.Windows.Forms.Padding(4);
            this.dockPanel1.Name = "dockPanel1";
            this.dockPanel1.RightToLeftLayout = true;
            this.dockPanel1.Size = new System.Drawing.Size(1023, 917);
            dockPanelGradient1.EndColor = System.Drawing.SystemColors.ControlLight;
            dockPanelGradient1.StartColor = System.Drawing.SystemColors.ControlLight;
            autoHideStripSkin1.DockStripGradient = dockPanelGradient1;
            tabGradient1.EndColor = System.Drawing.SystemColors.Control;
            tabGradient1.StartColor = System.Drawing.SystemColors.Control;
            tabGradient1.TextColor = System.Drawing.SystemColors.ControlDarkDark;
            autoHideStripSkin1.TabGradient = tabGradient1;
            dockPanelSkin1.AutoHideStripSkin = autoHideStripSkin1;
            tabGradient2.EndColor = System.Drawing.SystemColors.ControlLightLight;
            tabGradient2.StartColor = System.Drawing.SystemColors.ControlLightLight;
            tabGradient2.TextColor = System.Drawing.SystemColors.ControlText;
            dockPaneStripGradient1.ActiveTabGradient = tabGradient2;
            dockPanelGradient2.EndColor = System.Drawing.SystemColors.Control;
            dockPanelGradient2.StartColor = System.Drawing.SystemColors.Control;
            dockPaneStripGradient1.DockStripGradient = dockPanelGradient2;
            tabGradient3.EndColor = System.Drawing.SystemColors.ControlLight;
            tabGradient3.StartColor = System.Drawing.SystemColors.ControlLight;
            tabGradient3.TextColor = System.Drawing.SystemColors.ControlText;
            dockPaneStripGradient1.InactiveTabGradient = tabGradient3;
            dockPaneStripSkin1.DocumentGradient = dockPaneStripGradient1;
            tabGradient4.EndColor = System.Drawing.SystemColors.ActiveCaption;
            tabGradient4.LinearGradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            tabGradient4.StartColor = System.Drawing.SystemColors.GradientActiveCaption;
            tabGradient4.TextColor = System.Drawing.SystemColors.ActiveCaptionText;
            dockPaneStripToolWindowGradient1.ActiveCaptionGradient = tabGradient4;
            tabGradient5.EndColor = System.Drawing.SystemColors.Control;
            tabGradient5.StartColor = System.Drawing.SystemColors.Control;
            tabGradient5.TextColor = System.Drawing.SystemColors.ControlText;
            dockPaneStripToolWindowGradient1.ActiveTabGradient = tabGradient5;
            dockPanelGradient3.EndColor = System.Drawing.SystemColors.ControlLight;
            dockPanelGradient3.StartColor = System.Drawing.SystemColors.ControlLight;
            dockPaneStripToolWindowGradient1.DockStripGradient = dockPanelGradient3;
            tabGradient6.EndColor = System.Drawing.SystemColors.GradientInactiveCaption;
            tabGradient6.LinearGradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            tabGradient6.StartColor = System.Drawing.SystemColors.GradientInactiveCaption;
            tabGradient6.TextColor = System.Drawing.SystemColors.ControlText;
            dockPaneStripToolWindowGradient1.InactiveCaptionGradient = tabGradient6;
            tabGradient7.EndColor = System.Drawing.Color.Transparent;
            tabGradient7.StartColor = System.Drawing.Color.Transparent;
            tabGradient7.TextColor = System.Drawing.SystemColors.ControlDarkDark;
            dockPaneStripToolWindowGradient1.InactiveTabGradient = tabGradient7;
            dockPaneStripSkin1.ToolWindowGradient = dockPaneStripToolWindowGradient1;
            dockPanelSkin1.DockPaneStripSkin = dockPaneStripSkin1;
            this.dockPanel1.Skin = dockPanelSkin1;
            this.dockPanel1.TabIndex = 16;
            // 
            // MainDataM
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.ClientSize = new System.Drawing.Size(1274, 925);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "MainDataM";
            this.Text = "MainDataM";
            this.Load += new System.EventHandler(this.MainDataM_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion


        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        public System.Windows.Forms.TreeView tV_md;
        private WeifenLuo.WinFormsUI.Docking.DockPanel dockPanel1;
    }
}