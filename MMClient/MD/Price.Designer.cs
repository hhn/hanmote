﻿namespace MMClient.MD
{
    partial class Price
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.tabControl5 = new System.Windows.Forms.TabControl();
            this.tabPage22 = new System.Windows.Forms.TabPage();
            this.groupBox24 = new System.Windows.Forms.GroupBox();
            this.chart3 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.groupBox25 = new System.Windows.Forms.GroupBox();
            this.tabControl9 = new System.Windows.Forms.TabControl();
            this.tabPage23 = new System.Windows.Forms.TabPage();
            this.dataGridView12 = new System.Windows.Forms.DataGridView();
            this.tabPage24 = new System.Windows.Forms.TabPage();
            this.panel22 = new System.Windows.Forms.Panel();
            this.button40 = new System.Windows.Forms.Button();
            this.button41 = new System.Windows.Forms.Button();
            this.button42 = new System.Windows.Forms.Button();
            this.dataGridView13 = new System.Windows.Forms.DataGridView();
            this.panel23 = new System.Windows.Forms.Panel();
            this.groupBox26 = new System.Windows.Forms.GroupBox();
            this.textBox39 = new System.Windows.Forms.TextBox();
            this.label128 = new System.Windows.Forms.Label();
            this.button43 = new System.Windows.Forms.Button();
            this.button44 = new System.Windows.Forms.Button();
            this.listBox77 = new System.Windows.Forms.ListBox();
            this.label129 = new System.Windows.Forms.Label();
            this.listBox78 = new System.Windows.Forms.ListBox();
            this.label130 = new System.Windows.Forms.Label();
            this.listBox79 = new System.Windows.Forms.ListBox();
            this.label131 = new System.Windows.Forms.Label();
            this.listBox80 = new System.Windows.Forms.ListBox();
            this.label132 = new System.Windows.Forms.Label();
            this.listBox81 = new System.Windows.Forms.ListBox();
            this.label133 = new System.Windows.Forms.Label();
            this.listBox82 = new System.Windows.Forms.ListBox();
            this.label134 = new System.Windows.Forms.Label();
            this.listBox83 = new System.Windows.Forms.ListBox();
            this.label135 = new System.Windows.Forms.Label();
            this.tabPage25 = new System.Windows.Forms.TabPage();
            this.groupBox27 = new System.Windows.Forms.GroupBox();
            this.button45 = new System.Windows.Forms.Button();
            this.button46 = new System.Windows.Forms.Button();
            this.textBox40 = new System.Windows.Forms.TextBox();
            this.label136 = new System.Windows.Forms.Label();
            this.textBox41 = new System.Windows.Forms.TextBox();
            this.label137 = new System.Windows.Forms.Label();
            this.textBox42 = new System.Windows.Forms.TextBox();
            this.label138 = new System.Windows.Forms.Label();
            this.textBox43 = new System.Windows.Forms.TextBox();
            this.label139 = new System.Windows.Forms.Label();
            this.textBox44 = new System.Windows.Forms.TextBox();
            this.label140 = new System.Windows.Forms.Label();
            this.textBox45 = new System.Windows.Forms.TextBox();
            this.label141 = new System.Windows.Forms.Label();
            this.textBox46 = new System.Windows.Forms.TextBox();
            this.label142 = new System.Windows.Forms.Label();
            this.textBox47 = new System.Windows.Forms.TextBox();
            this.label143 = new System.Windows.Forms.Label();
            this.tabPage35 = new System.Windows.Forms.TabPage();
            this.panel24 = new System.Windows.Forms.Panel();
            this.groupBox28 = new System.Windows.Forms.GroupBox();
            this.listBox84 = new System.Windows.Forms.ListBox();
            this.label144 = new System.Windows.Forms.Label();
            this.listBox85 = new System.Windows.Forms.ListBox();
            this.label145 = new System.Windows.Forms.Label();
            this.listBox86 = new System.Windows.Forms.ListBox();
            this.label146 = new System.Windows.Forms.Label();
            this.listBox87 = new System.Windows.Forms.ListBox();
            this.label147 = new System.Windows.Forms.Label();
            this.listBox88 = new System.Windows.Forms.ListBox();
            this.label148 = new System.Windows.Forms.Label();
            this.listBox89 = new System.Windows.Forms.ListBox();
            this.label149 = new System.Windows.Forms.Label();
            this.button47 = new System.Windows.Forms.Button();
            this.button48 = new System.Windows.Forms.Button();
            this.tabPage36 = new System.Windows.Forms.TabPage();
            this.groupBox29 = new System.Windows.Forms.GroupBox();
            this.dataGridView14 = new System.Windows.Forms.DataGridView();
            this.groupBox30 = new System.Windows.Forms.GroupBox();
            this.dataGridView15 = new System.Windows.Forms.DataGridView();
            this.panel25 = new System.Windows.Forms.Panel();
            this.groupBox31 = new System.Windows.Forms.GroupBox();
            this.textBox48 = new System.Windows.Forms.TextBox();
            this.label150 = new System.Windows.Forms.Label();
            this.button49 = new System.Windows.Forms.Button();
            this.button50 = new System.Windows.Forms.Button();
            this.listBox90 = new System.Windows.Forms.ListBox();
            this.label151 = new System.Windows.Forms.Label();
            this.listBox91 = new System.Windows.Forms.ListBox();
            this.label152 = new System.Windows.Forms.Label();
            this.listBox92 = new System.Windows.Forms.ListBox();
            this.label153 = new System.Windows.Forms.Label();
            this.listBox93 = new System.Windows.Forms.ListBox();
            this.label154 = new System.Windows.Forms.Label();
            this.listBox94 = new System.Windows.Forms.ListBox();
            this.label155 = new System.Windows.Forms.Label();
            this.listBox95 = new System.Windows.Forms.ListBox();
            this.label156 = new System.Windows.Forms.Label();
            this.listBox96 = new System.Windows.Forms.ListBox();
            this.label157 = new System.Windows.Forms.Label();
            this.tabPage37 = new System.Windows.Forms.TabPage();
            this.tabControl5.SuspendLayout();
            this.tabPage22.SuspendLayout();
            this.groupBox24.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart3)).BeginInit();
            this.groupBox25.SuspendLayout();
            this.tabControl9.SuspendLayout();
            this.tabPage23.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView12)).BeginInit();
            this.panel22.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView13)).BeginInit();
            this.panel23.SuspendLayout();
            this.groupBox26.SuspendLayout();
            this.tabPage25.SuspendLayout();
            this.groupBox27.SuspendLayout();
            this.tabPage35.SuspendLayout();
            this.panel24.SuspendLayout();
            this.groupBox28.SuspendLayout();
            this.tabPage36.SuspendLayout();
            this.groupBox29.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView14)).BeginInit();
            this.groupBox30.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView15)).BeginInit();
            this.panel25.SuspendLayout();
            this.groupBox31.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl5
            // 
            this.tabControl5.Controls.Add(this.tabPage22);
            this.tabControl5.Controls.Add(this.tabPage25);
            this.tabControl5.Controls.Add(this.tabPage35);
            this.tabControl5.Controls.Add(this.tabPage36);
            this.tabControl5.Controls.Add(this.tabPage37);
            this.tabControl5.Location = new System.Drawing.Point(12, 2);
            this.tabControl5.Name = "tabControl5";
            this.tabControl5.SelectedIndex = 0;
            this.tabControl5.Size = new System.Drawing.Size(916, 668);
            this.tabControl5.TabIndex = 3;
            // 
            // tabPage22
            // 
            this.tabPage22.Controls.Add(this.groupBox24);
            this.tabPage22.Controls.Add(this.groupBox25);
            this.tabPage22.Controls.Add(this.panel22);
            this.tabPage22.Controls.Add(this.panel23);
            this.tabPage22.Location = new System.Drawing.Point(4, 22);
            this.tabPage22.Name = "tabPage22";
            this.tabPage22.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage22.Size = new System.Drawing.Size(908, 642);
            this.tabPage22.TabIndex = 0;
            this.tabPage22.Text = "查询";
            this.tabPage22.UseVisualStyleBackColor = true;
            // 
            // groupBox24
            // 
            this.groupBox24.Controls.Add(this.chart3);
            this.groupBox24.Location = new System.Drawing.Point(564, 238);
            this.groupBox24.Name = "groupBox24";
            this.groupBox24.Size = new System.Drawing.Size(341, 378);
            this.groupBox24.TabIndex = 6;
            this.groupBox24.TabStop = false;
            this.groupBox24.Text = "数据分析";
            // 
            // chart3
            // 
            chartArea1.Name = "ChartArea1";
            this.chart3.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.chart3.Legends.Add(legend1);
            this.chart3.Location = new System.Drawing.Point(6, 20);
            this.chart3.Name = "chart3";
            series1.ChartArea = "ChartArea1";
            series1.Legend = "Legend1";
            series1.Name = "Series1";
            this.chart3.Series.Add(series1);
            this.chart3.Size = new System.Drawing.Size(315, 352);
            this.chart3.TabIndex = 0;
            this.chart3.Text = "chart3";
            // 
            // groupBox25
            // 
            this.groupBox25.Controls.Add(this.tabControl9);
            this.groupBox25.Location = new System.Drawing.Point(3, 428);
            this.groupBox25.Name = "groupBox25";
            this.groupBox25.Size = new System.Drawing.Size(554, 192);
            this.groupBox25.TabIndex = 5;
            this.groupBox25.TabStop = false;
            this.groupBox25.Text = "详细信息";
            // 
            // tabControl9
            // 
            this.tabControl9.Controls.Add(this.tabPage23);
            this.tabControl9.Controls.Add(this.tabPage24);
            this.tabControl9.Location = new System.Drawing.Point(1, 20);
            this.tabControl9.Name = "tabControl9";
            this.tabControl9.SelectedIndex = 0;
            this.tabControl9.Size = new System.Drawing.Size(590, 172);
            this.tabControl9.TabIndex = 0;
            // 
            // tabPage23
            // 
            this.tabPage23.Controls.Add(this.dataGridView12);
            this.tabPage23.Location = new System.Drawing.Point(4, 22);
            this.tabPage23.Name = "tabPage23";
            this.tabPage23.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage23.Size = new System.Drawing.Size(582, 146);
            this.tabPage23.TabIndex = 0;
            this.tabPage23.Text = "详细记录";
            this.tabPage23.UseVisualStyleBackColor = true;
            // 
            // dataGridView12
            // 
            this.dataGridView12.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView12.Location = new System.Drawing.Point(1, 3);
            this.dataGridView12.Name = "dataGridView12";
            this.dataGridView12.RowTemplate.Height = 23;
            this.dataGridView12.Size = new System.Drawing.Size(539, 137);
            this.dataGridView12.TabIndex = 0;
            // 
            // tabPage24
            // 
            this.tabPage24.Location = new System.Drawing.Point(4, 22);
            this.tabPage24.Name = "tabPage24";
            this.tabPage24.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage24.Size = new System.Drawing.Size(582, 146);
            this.tabPage24.TabIndex = 1;
            this.tabPage24.Text = "其他项";
            this.tabPage24.UseVisualStyleBackColor = true;
            // 
            // panel22
            // 
            this.panel22.Controls.Add(this.button40);
            this.panel22.Controls.Add(this.button41);
            this.panel22.Controls.Add(this.button42);
            this.panel22.Controls.Add(this.dataGridView13);
            this.panel22.Location = new System.Drawing.Point(3, 237);
            this.panel22.Name = "panel22";
            this.panel22.Size = new System.Drawing.Size(554, 185);
            this.panel22.TabIndex = 4;
            // 
            // button40
            // 
            this.button40.Location = new System.Drawing.Point(377, 159);
            this.button40.Name = "button40";
            this.button40.Size = new System.Drawing.Size(75, 23);
            this.button40.TabIndex = 3;
            this.button40.Text = "导出数据";
            this.button40.UseVisualStyleBackColor = true;
            // 
            // button41
            // 
            this.button41.Location = new System.Drawing.Point(206, 159);
            this.button41.Name = "button41";
            this.button41.Size = new System.Drawing.Size(75, 23);
            this.button41.TabIndex = 2;
            this.button41.Text = "数据分析";
            this.button41.UseVisualStyleBackColor = true;
            // 
            // button42
            // 
            this.button42.Location = new System.Drawing.Point(50, 159);
            this.button42.Name = "button42";
            this.button42.Size = new System.Drawing.Size(75, 23);
            this.button42.TabIndex = 1;
            this.button42.Text = "删除";
            this.button42.UseVisualStyleBackColor = true;
            // 
            // dataGridView13
            // 
            this.dataGridView13.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView13.Location = new System.Drawing.Point(3, 4);
            this.dataGridView13.Name = "dataGridView13";
            this.dataGridView13.RowTemplate.Height = 23;
            this.dataGridView13.Size = new System.Drawing.Size(548, 150);
            this.dataGridView13.TabIndex = 0;
            // 
            // panel23
            // 
            this.panel23.Controls.Add(this.groupBox26);
            this.panel23.Location = new System.Drawing.Point(3, 6);
            this.panel23.Name = "panel23";
            this.panel23.Size = new System.Drawing.Size(906, 225);
            this.panel23.TabIndex = 3;
            // 
            // groupBox26
            // 
            this.groupBox26.Controls.Add(this.textBox39);
            this.groupBox26.Controls.Add(this.label128);
            this.groupBox26.Controls.Add(this.button43);
            this.groupBox26.Controls.Add(this.button44);
            this.groupBox26.Controls.Add(this.listBox77);
            this.groupBox26.Controls.Add(this.label129);
            this.groupBox26.Controls.Add(this.listBox78);
            this.groupBox26.Controls.Add(this.label130);
            this.groupBox26.Controls.Add(this.listBox79);
            this.groupBox26.Controls.Add(this.label131);
            this.groupBox26.Controls.Add(this.listBox80);
            this.groupBox26.Controls.Add(this.label132);
            this.groupBox26.Controls.Add(this.listBox81);
            this.groupBox26.Controls.Add(this.label133);
            this.groupBox26.Controls.Add(this.listBox82);
            this.groupBox26.Controls.Add(this.label134);
            this.groupBox26.Controls.Add(this.listBox83);
            this.groupBox26.Controls.Add(this.label135);
            this.groupBox26.Location = new System.Drawing.Point(7, 3);
            this.groupBox26.Name = "groupBox26";
            this.groupBox26.Size = new System.Drawing.Size(631, 219);
            this.groupBox26.TabIndex = 18;
            this.groupBox26.TabStop = false;
            this.groupBox26.Text = "条件选择";
            // 
            // textBox39
            // 
            this.textBox39.Location = new System.Drawing.Point(459, 170);
            this.textBox39.Name = "textBox39";
            this.textBox39.Size = new System.Drawing.Size(117, 21);
            this.textBox39.TabIndex = 35;
            // 
            // label128
            // 
            this.label128.AutoSize = true;
            this.label128.Location = new System.Drawing.Point(344, 170);
            this.label128.Name = "label128";
            this.label128.Size = new System.Drawing.Size(65, 12);
            this.label128.TabIndex = 34;
            this.label128.Text = "最大命中数";
            // 
            // button43
            // 
            this.button43.Location = new System.Drawing.Point(231, 170);
            this.button43.Name = "button43";
            this.button43.Size = new System.Drawing.Size(75, 23);
            this.button43.TabIndex = 33;
            this.button43.Text = "清除";
            this.button43.UseVisualStyleBackColor = true;
            // 
            // button44
            // 
            this.button44.Location = new System.Drawing.Point(96, 170);
            this.button44.Name = "button44";
            this.button44.Size = new System.Drawing.Size(75, 23);
            this.button44.TabIndex = 32;
            this.button44.Text = "查询";
            this.button44.UseVisualStyleBackColor = true;
            // 
            // listBox77
            // 
            this.listBox77.FormattingEnabled = true;
            this.listBox77.ItemHeight = 12;
            this.listBox77.Location = new System.Drawing.Point(154, 148);
            this.listBox77.Name = "listBox77";
            this.listBox77.Size = new System.Drawing.Size(120, 16);
            this.listBox77.TabIndex = 31;
            // 
            // label129
            // 
            this.label129.AutoSize = true;
            this.label129.Location = new System.Drawing.Point(40, 148);
            this.label129.Name = "label129";
            this.label129.Size = new System.Drawing.Size(29, 12);
            this.label129.TabIndex = 30;
            this.label129.Text = "其他";
            // 
            // listBox78
            // 
            this.listBox78.FormattingEnabled = true;
            this.listBox78.ItemHeight = 12;
            this.listBox78.Location = new System.Drawing.Point(459, 105);
            this.listBox78.Name = "listBox78";
            this.listBox78.Size = new System.Drawing.Size(120, 16);
            this.listBox78.TabIndex = 29;
            // 
            // label130
            // 
            this.label130.AutoSize = true;
            this.label130.Location = new System.Drawing.Point(345, 105);
            this.label130.Name = "label130";
            this.label130.Size = new System.Drawing.Size(17, 12);
            this.label130.TabIndex = 28;
            this.label130.Text = "至";
            // 
            // listBox79
            // 
            this.listBox79.FormattingEnabled = true;
            this.listBox79.ItemHeight = 12;
            this.listBox79.Location = new System.Drawing.Point(154, 105);
            this.listBox79.Name = "listBox79";
            this.listBox79.Size = new System.Drawing.Size(120, 16);
            this.listBox79.TabIndex = 27;
            // 
            // label131
            // 
            this.label131.AutoSize = true;
            this.label131.Location = new System.Drawing.Point(40, 105);
            this.label131.Name = "label131";
            this.label131.Size = new System.Drawing.Size(53, 12);
            this.label131.TabIndex = 26;
            this.label131.Text = "当前价格";
            // 
            // listBox80
            // 
            this.listBox80.FormattingEnabled = true;
            this.listBox80.ItemHeight = 12;
            this.listBox80.Location = new System.Drawing.Point(459, 60);
            this.listBox80.Name = "listBox80";
            this.listBox80.Size = new System.Drawing.Size(120, 16);
            this.listBox80.TabIndex = 25;
            // 
            // label132
            // 
            this.label132.AutoSize = true;
            this.label132.Location = new System.Drawing.Point(345, 60);
            this.label132.Name = "label132";
            this.label132.Size = new System.Drawing.Size(17, 12);
            this.label132.TabIndex = 24;
            this.label132.Text = "至";
            // 
            // listBox81
            // 
            this.listBox81.FormattingEnabled = true;
            this.listBox81.ItemHeight = 12;
            this.listBox81.Location = new System.Drawing.Point(154, 60);
            this.listBox81.Name = "listBox81";
            this.listBox81.Size = new System.Drawing.Size(120, 16);
            this.listBox81.TabIndex = 23;
            // 
            // label133
            // 
            this.label133.AutoSize = true;
            this.label133.Location = new System.Drawing.Point(40, 60);
            this.label133.Name = "label133";
            this.label133.Size = new System.Drawing.Size(65, 12);
            this.label133.TabIndex = 22;
            this.label133.Text = "供应商编码";
            // 
            // listBox82
            // 
            this.listBox82.FormattingEnabled = true;
            this.listBox82.ItemHeight = 12;
            this.listBox82.Location = new System.Drawing.Point(459, 20);
            this.listBox82.Name = "listBox82";
            this.listBox82.Size = new System.Drawing.Size(120, 16);
            this.listBox82.TabIndex = 21;
            // 
            // label134
            // 
            this.label134.AutoSize = true;
            this.label134.Location = new System.Drawing.Point(345, 20);
            this.label134.Name = "label134";
            this.label134.Size = new System.Drawing.Size(17, 12);
            this.label134.TabIndex = 20;
            this.label134.Text = "至";
            // 
            // listBox83
            // 
            this.listBox83.FormattingEnabled = true;
            this.listBox83.ItemHeight = 12;
            this.listBox83.Location = new System.Drawing.Point(154, 20);
            this.listBox83.Name = "listBox83";
            this.listBox83.Size = new System.Drawing.Size(120, 16);
            this.listBox83.TabIndex = 19;
            // 
            // label135
            // 
            this.label135.AutoSize = true;
            this.label135.Location = new System.Drawing.Point(40, 20);
            this.label135.Name = "label135";
            this.label135.Size = new System.Drawing.Size(53, 12);
            this.label135.TabIndex = 18;
            this.label135.Text = "物料编码";
            // 
            // tabPage25
            // 
            this.tabPage25.Controls.Add(this.groupBox27);
            this.tabPage25.Location = new System.Drawing.Point(4, 22);
            this.tabPage25.Name = "tabPage25";
            this.tabPage25.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage25.Size = new System.Drawing.Size(908, 642);
            this.tabPage25.TabIndex = 1;
            this.tabPage25.Text = "创建";
            this.tabPage25.UseVisualStyleBackColor = true;
            // 
            // groupBox27
            // 
            this.groupBox27.Controls.Add(this.button45);
            this.groupBox27.Controls.Add(this.button46);
            this.groupBox27.Controls.Add(this.textBox40);
            this.groupBox27.Controls.Add(this.label136);
            this.groupBox27.Controls.Add(this.textBox41);
            this.groupBox27.Controls.Add(this.label137);
            this.groupBox27.Controls.Add(this.textBox42);
            this.groupBox27.Controls.Add(this.label138);
            this.groupBox27.Controls.Add(this.textBox43);
            this.groupBox27.Controls.Add(this.label139);
            this.groupBox27.Controls.Add(this.textBox44);
            this.groupBox27.Controls.Add(this.label140);
            this.groupBox27.Controls.Add(this.textBox45);
            this.groupBox27.Controls.Add(this.label141);
            this.groupBox27.Controls.Add(this.textBox46);
            this.groupBox27.Controls.Add(this.label142);
            this.groupBox27.Controls.Add(this.textBox47);
            this.groupBox27.Controls.Add(this.label143);
            this.groupBox27.Location = new System.Drawing.Point(6, 6);
            this.groupBox27.Name = "groupBox27";
            this.groupBox27.Size = new System.Drawing.Size(656, 619);
            this.groupBox27.TabIndex = 1;
            this.groupBox27.TabStop = false;
            this.groupBox27.Text = "创建新数据";
            // 
            // button45
            // 
            this.button45.Location = new System.Drawing.Point(340, 556);
            this.button45.Name = "button45";
            this.button45.Size = new System.Drawing.Size(75, 23);
            this.button45.TabIndex = 21;
            this.button45.Text = "重置";
            this.button45.UseVisualStyleBackColor = true;
            // 
            // button46
            // 
            this.button46.Location = new System.Drawing.Point(78, 556);
            this.button46.Name = "button46";
            this.button46.Size = new System.Drawing.Size(75, 23);
            this.button46.TabIndex = 20;
            this.button46.Text = "确定";
            this.button46.UseVisualStyleBackColor = true;
            // 
            // textBox40
            // 
            this.textBox40.Location = new System.Drawing.Point(409, 213);
            this.textBox40.Name = "textBox40";
            this.textBox40.Size = new System.Drawing.Size(128, 21);
            this.textBox40.TabIndex = 15;
            // 
            // label136
            // 
            this.label136.AutoSize = true;
            this.label136.Location = new System.Drawing.Point(338, 218);
            this.label136.Name = "label136";
            this.label136.Size = new System.Drawing.Size(53, 12);
            this.label136.TabIndex = 14;
            this.label136.Text = "计量单位";
            // 
            // textBox41
            // 
            this.textBox41.Location = new System.Drawing.Point(78, 213);
            this.textBox41.Name = "textBox41";
            this.textBox41.Size = new System.Drawing.Size(128, 21);
            this.textBox41.TabIndex = 13;
            // 
            // label137
            // 
            this.label137.AutoSize = true;
            this.label137.Location = new System.Drawing.Point(7, 218);
            this.label137.Name = "label137";
            this.label137.Size = new System.Drawing.Size(53, 12);
            this.label137.TabIndex = 12;
            this.label137.Text = "当前价格";
            // 
            // textBox42
            // 
            this.textBox42.Location = new System.Drawing.Point(409, 151);
            this.textBox42.Name = "textBox42";
            this.textBox42.Size = new System.Drawing.Size(128, 21);
            this.textBox42.TabIndex = 11;
            // 
            // label138
            // 
            this.label138.AutoSize = true;
            this.label138.Location = new System.Drawing.Point(338, 156);
            this.label138.Name = "label138";
            this.label138.Size = new System.Drawing.Size(53, 12);
            this.label138.TabIndex = 10;
            this.label138.Text = "网络价格";
            // 
            // textBox43
            // 
            this.textBox43.Location = new System.Drawing.Point(78, 147);
            this.textBox43.Name = "textBox43";
            this.textBox43.Size = new System.Drawing.Size(128, 21);
            this.textBox43.TabIndex = 9;
            // 
            // label139
            // 
            this.label139.AutoSize = true;
            this.label139.Location = new System.Drawing.Point(7, 152);
            this.label139.Name = "label139";
            this.label139.Size = new System.Drawing.Size(53, 12);
            this.label139.TabIndex = 8;
            this.label139.Text = "上期价格";
            // 
            // textBox44
            // 
            this.textBox44.Location = new System.Drawing.Point(409, 80);
            this.textBox44.Name = "textBox44";
            this.textBox44.Size = new System.Drawing.Size(128, 21);
            this.textBox44.TabIndex = 7;
            // 
            // label140
            // 
            this.label140.AutoSize = true;
            this.label140.Location = new System.Drawing.Point(338, 85);
            this.label140.Name = "label140";
            this.label140.Size = new System.Drawing.Size(65, 12);
            this.label140.TabIndex = 6;
            this.label140.Text = "供应商名称";
            // 
            // textBox45
            // 
            this.textBox45.Location = new System.Drawing.Point(77, 82);
            this.textBox45.Name = "textBox45";
            this.textBox45.Size = new System.Drawing.Size(128, 21);
            this.textBox45.TabIndex = 5;
            // 
            // label141
            // 
            this.label141.AutoSize = true;
            this.label141.Location = new System.Drawing.Point(6, 87);
            this.label141.Name = "label141";
            this.label141.Size = new System.Drawing.Size(65, 12);
            this.label141.TabIndex = 4;
            this.label141.Text = "供应商编码";
            // 
            // textBox46
            // 
            this.textBox46.Location = new System.Drawing.Point(409, 19);
            this.textBox46.Name = "textBox46";
            this.textBox46.Size = new System.Drawing.Size(128, 21);
            this.textBox46.TabIndex = 3;
            // 
            // label142
            // 
            this.label142.AutoSize = true;
            this.label142.Location = new System.Drawing.Point(338, 24);
            this.label142.Name = "label142";
            this.label142.Size = new System.Drawing.Size(53, 12);
            this.label142.TabIndex = 2;
            this.label142.Text = "物料名称";
            // 
            // textBox47
            // 
            this.textBox47.Location = new System.Drawing.Point(78, 21);
            this.textBox47.Name = "textBox47";
            this.textBox47.Size = new System.Drawing.Size(128, 21);
            this.textBox47.TabIndex = 1;
            // 
            // label143
            // 
            this.label143.AutoSize = true;
            this.label143.Location = new System.Drawing.Point(7, 26);
            this.label143.Name = "label143";
            this.label143.Size = new System.Drawing.Size(53, 12);
            this.label143.TabIndex = 0;
            this.label143.Text = "物料编码";
            // 
            // tabPage35
            // 
            this.tabPage35.Controls.Add(this.panel24);
            this.tabPage35.Location = new System.Drawing.Point(4, 22);
            this.tabPage35.Name = "tabPage35";
            this.tabPage35.Size = new System.Drawing.Size(908, 642);
            this.tabPage35.TabIndex = 2;
            this.tabPage35.Text = "修改";
            this.tabPage35.UseVisualStyleBackColor = true;
            // 
            // panel24
            // 
            this.panel24.Controls.Add(this.groupBox28);
            this.panel24.Location = new System.Drawing.Point(3, 6);
            this.panel24.Name = "panel24";
            this.panel24.Size = new System.Drawing.Size(651, 640);
            this.panel24.TabIndex = 1;
            // 
            // groupBox28
            // 
            this.groupBox28.Controls.Add(this.listBox84);
            this.groupBox28.Controls.Add(this.label144);
            this.groupBox28.Controls.Add(this.listBox85);
            this.groupBox28.Controls.Add(this.label145);
            this.groupBox28.Controls.Add(this.listBox86);
            this.groupBox28.Controls.Add(this.label146);
            this.groupBox28.Controls.Add(this.listBox87);
            this.groupBox28.Controls.Add(this.label147);
            this.groupBox28.Controls.Add(this.listBox88);
            this.groupBox28.Controls.Add(this.label148);
            this.groupBox28.Controls.Add(this.listBox89);
            this.groupBox28.Controls.Add(this.label149);
            this.groupBox28.Controls.Add(this.button47);
            this.groupBox28.Controls.Add(this.button48);
            this.groupBox28.Location = new System.Drawing.Point(3, 3);
            this.groupBox28.Name = "groupBox28";
            this.groupBox28.Size = new System.Drawing.Size(640, 616);
            this.groupBox28.TabIndex = 1;
            this.groupBox28.TabStop = false;
            this.groupBox28.Text = "修改新数据";
            // 
            // listBox84
            // 
            this.listBox84.FormattingEnabled = true;
            this.listBox84.ItemHeight = 12;
            this.listBox84.Location = new System.Drawing.Point(436, 113);
            this.listBox84.Name = "listBox84";
            this.listBox84.Size = new System.Drawing.Size(120, 16);
            this.listBox84.TabIndex = 35;
            // 
            // label144
            // 
            this.label144.AutoSize = true;
            this.label144.Location = new System.Drawing.Point(322, 113);
            this.label144.Name = "label144";
            this.label144.Size = new System.Drawing.Size(41, 12);
            this.label144.TabIndex = 34;
            this.label144.Text = "修改至";
            // 
            // listBox85
            // 
            this.listBox85.FormattingEnabled = true;
            this.listBox85.ItemHeight = 12;
            this.listBox85.Location = new System.Drawing.Point(131, 113);
            this.listBox85.Name = "listBox85";
            this.listBox85.Size = new System.Drawing.Size(120, 16);
            this.listBox85.TabIndex = 33;
            // 
            // label145
            // 
            this.label145.AutoSize = true;
            this.label145.Location = new System.Drawing.Point(17, 113);
            this.label145.Name = "label145";
            this.label145.Size = new System.Drawing.Size(53, 12);
            this.label145.TabIndex = 32;
            this.label145.Text = "价格信息";
            // 
            // listBox86
            // 
            this.listBox86.FormattingEnabled = true;
            this.listBox86.ItemHeight = 12;
            this.listBox86.Location = new System.Drawing.Point(436, 68);
            this.listBox86.Name = "listBox86";
            this.listBox86.Size = new System.Drawing.Size(120, 16);
            this.listBox86.TabIndex = 31;
            // 
            // label146
            // 
            this.label146.AutoSize = true;
            this.label146.Location = new System.Drawing.Point(322, 68);
            this.label146.Name = "label146";
            this.label146.Size = new System.Drawing.Size(41, 12);
            this.label146.TabIndex = 30;
            this.label146.Text = "修改至";
            // 
            // listBox87
            // 
            this.listBox87.FormattingEnabled = true;
            this.listBox87.ItemHeight = 12;
            this.listBox87.Location = new System.Drawing.Point(131, 68);
            this.listBox87.Name = "listBox87";
            this.listBox87.Size = new System.Drawing.Size(120, 16);
            this.listBox87.TabIndex = 29;
            // 
            // label147
            // 
            this.label147.AutoSize = true;
            this.label147.Location = new System.Drawing.Point(17, 68);
            this.label147.Name = "label147";
            this.label147.Size = new System.Drawing.Size(65, 12);
            this.label147.TabIndex = 28;
            this.label147.Text = "供应商编码";
            // 
            // listBox88
            // 
            this.listBox88.FormattingEnabled = true;
            this.listBox88.ItemHeight = 12;
            this.listBox88.Location = new System.Drawing.Point(436, 28);
            this.listBox88.Name = "listBox88";
            this.listBox88.Size = new System.Drawing.Size(120, 16);
            this.listBox88.TabIndex = 27;
            // 
            // label148
            // 
            this.label148.AutoSize = true;
            this.label148.Location = new System.Drawing.Point(322, 28);
            this.label148.Name = "label148";
            this.label148.Size = new System.Drawing.Size(41, 12);
            this.label148.TabIndex = 26;
            this.label148.Text = "修改至";
            // 
            // listBox89
            // 
            this.listBox89.FormattingEnabled = true;
            this.listBox89.ItemHeight = 12;
            this.listBox89.Location = new System.Drawing.Point(131, 28);
            this.listBox89.Name = "listBox89";
            this.listBox89.Size = new System.Drawing.Size(120, 16);
            this.listBox89.TabIndex = 25;
            // 
            // label149
            // 
            this.label149.AutoSize = true;
            this.label149.Location = new System.Drawing.Point(17, 28);
            this.label149.Name = "label149";
            this.label149.Size = new System.Drawing.Size(53, 12);
            this.label149.TabIndex = 24;
            this.label149.Text = "物料编码";
            // 
            // button47
            // 
            this.button47.Location = new System.Drawing.Point(392, 558);
            this.button47.Name = "button47";
            this.button47.Size = new System.Drawing.Size(75, 23);
            this.button47.TabIndex = 23;
            this.button47.Text = "重置";
            this.button47.UseVisualStyleBackColor = true;
            // 
            // button48
            // 
            this.button48.Location = new System.Drawing.Point(130, 558);
            this.button48.Name = "button48";
            this.button48.Size = new System.Drawing.Size(75, 23);
            this.button48.TabIndex = 22;
            this.button48.Text = "确定";
            this.button48.UseVisualStyleBackColor = true;
            // 
            // tabPage36
            // 
            this.tabPage36.Controls.Add(this.groupBox29);
            this.tabPage36.Controls.Add(this.groupBox30);
            this.tabPage36.Controls.Add(this.panel25);
            this.tabPage36.Location = new System.Drawing.Point(4, 22);
            this.tabPage36.Name = "tabPage36";
            this.tabPage36.Size = new System.Drawing.Size(908, 642);
            this.tabPage36.TabIndex = 3;
            this.tabPage36.Text = "分发";
            this.tabPage36.UseVisualStyleBackColor = true;
            // 
            // groupBox29
            // 
            this.groupBox29.Controls.Add(this.dataGridView14);
            this.groupBox29.Location = new System.Drawing.Point(3, 471);
            this.groupBox29.Name = "groupBox29";
            this.groupBox29.Size = new System.Drawing.Size(634, 163);
            this.groupBox29.TabIndex = 6;
            this.groupBox29.TabStop = false;
            this.groupBox29.Text = "分发的子系统或者单位";
            // 
            // dataGridView14
            // 
            this.dataGridView14.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView14.Location = new System.Drawing.Point(0, 12);
            this.dataGridView14.Name = "dataGridView14";
            this.dataGridView14.RowTemplate.Height = 23;
            this.dataGridView14.Size = new System.Drawing.Size(628, 150);
            this.dataGridView14.TabIndex = 0;
            // 
            // groupBox30
            // 
            this.groupBox30.Controls.Add(this.dataGridView15);
            this.groupBox30.Location = new System.Drawing.Point(3, 234);
            this.groupBox30.Name = "groupBox30";
            this.groupBox30.Size = new System.Drawing.Size(636, 231);
            this.groupBox30.TabIndex = 5;
            this.groupBox30.TabStop = false;
            this.groupBox30.Text = "查询结果";
            // 
            // dataGridView15
            // 
            this.dataGridView15.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView15.Location = new System.Drawing.Point(7, 21);
            this.dataGridView15.Name = "dataGridView15";
            this.dataGridView15.RowTemplate.Height = 23;
            this.dataGridView15.Size = new System.Drawing.Size(623, 204);
            this.dataGridView15.TabIndex = 0;
            // 
            // panel25
            // 
            this.panel25.Controls.Add(this.groupBox31);
            this.panel25.Location = new System.Drawing.Point(3, 3);
            this.panel25.Name = "panel25";
            this.panel25.Size = new System.Drawing.Size(906, 225);
            this.panel25.TabIndex = 4;
            // 
            // groupBox31
            // 
            this.groupBox31.Controls.Add(this.textBox48);
            this.groupBox31.Controls.Add(this.label150);
            this.groupBox31.Controls.Add(this.button49);
            this.groupBox31.Controls.Add(this.button50);
            this.groupBox31.Controls.Add(this.listBox90);
            this.groupBox31.Controls.Add(this.label151);
            this.groupBox31.Controls.Add(this.listBox91);
            this.groupBox31.Controls.Add(this.label152);
            this.groupBox31.Controls.Add(this.listBox92);
            this.groupBox31.Controls.Add(this.label153);
            this.groupBox31.Controls.Add(this.listBox93);
            this.groupBox31.Controls.Add(this.label154);
            this.groupBox31.Controls.Add(this.listBox94);
            this.groupBox31.Controls.Add(this.label155);
            this.groupBox31.Controls.Add(this.listBox95);
            this.groupBox31.Controls.Add(this.label156);
            this.groupBox31.Controls.Add(this.listBox96);
            this.groupBox31.Controls.Add(this.label157);
            this.groupBox31.Location = new System.Drawing.Point(7, 3);
            this.groupBox31.Name = "groupBox31";
            this.groupBox31.Size = new System.Drawing.Size(631, 219);
            this.groupBox31.TabIndex = 18;
            this.groupBox31.TabStop = false;
            this.groupBox31.Text = "条件选择";
            // 
            // textBox48
            // 
            this.textBox48.Location = new System.Drawing.Point(459, 170);
            this.textBox48.Name = "textBox48";
            this.textBox48.Size = new System.Drawing.Size(117, 21);
            this.textBox48.TabIndex = 35;
            // 
            // label150
            // 
            this.label150.AutoSize = true;
            this.label150.Location = new System.Drawing.Point(344, 170);
            this.label150.Name = "label150";
            this.label150.Size = new System.Drawing.Size(65, 12);
            this.label150.TabIndex = 34;
            this.label150.Text = "最大命中数";
            // 
            // button49
            // 
            this.button49.Location = new System.Drawing.Point(231, 170);
            this.button49.Name = "button49";
            this.button49.Size = new System.Drawing.Size(75, 23);
            this.button49.TabIndex = 33;
            this.button49.Text = "清除";
            this.button49.UseVisualStyleBackColor = true;
            // 
            // button50
            // 
            this.button50.Location = new System.Drawing.Point(96, 170);
            this.button50.Name = "button50";
            this.button50.Size = new System.Drawing.Size(75, 23);
            this.button50.TabIndex = 32;
            this.button50.Text = "查询";
            this.button50.UseVisualStyleBackColor = true;
            // 
            // listBox90
            // 
            this.listBox90.FormattingEnabled = true;
            this.listBox90.ItemHeight = 12;
            this.listBox90.Location = new System.Drawing.Point(154, 148);
            this.listBox90.Name = "listBox90";
            this.listBox90.Size = new System.Drawing.Size(120, 16);
            this.listBox90.TabIndex = 31;
            // 
            // label151
            // 
            this.label151.AutoSize = true;
            this.label151.Location = new System.Drawing.Point(40, 148);
            this.label151.Name = "label151";
            this.label151.Size = new System.Drawing.Size(29, 12);
            this.label151.TabIndex = 30;
            this.label151.Text = "其他";
            // 
            // listBox91
            // 
            this.listBox91.FormattingEnabled = true;
            this.listBox91.ItemHeight = 12;
            this.listBox91.Location = new System.Drawing.Point(459, 105);
            this.listBox91.Name = "listBox91";
            this.listBox91.Size = new System.Drawing.Size(120, 16);
            this.listBox91.TabIndex = 29;
            // 
            // label152
            // 
            this.label152.AutoSize = true;
            this.label152.Location = new System.Drawing.Point(345, 105);
            this.label152.Name = "label152";
            this.label152.Size = new System.Drawing.Size(17, 12);
            this.label152.TabIndex = 28;
            this.label152.Text = "至";
            // 
            // listBox92
            // 
            this.listBox92.FormattingEnabled = true;
            this.listBox92.ItemHeight = 12;
            this.listBox92.Location = new System.Drawing.Point(154, 105);
            this.listBox92.Name = "listBox92";
            this.listBox92.Size = new System.Drawing.Size(120, 16);
            this.listBox92.TabIndex = 27;
            // 
            // label153
            // 
            this.label153.AutoSize = true;
            this.label153.Location = new System.Drawing.Point(40, 105);
            this.label153.Name = "label153";
            this.label153.Size = new System.Drawing.Size(29, 12);
            this.label153.TabIndex = 26;
            this.label153.Text = "价格";
            // 
            // listBox93
            // 
            this.listBox93.FormattingEnabled = true;
            this.listBox93.ItemHeight = 12;
            this.listBox93.Location = new System.Drawing.Point(459, 60);
            this.listBox93.Name = "listBox93";
            this.listBox93.Size = new System.Drawing.Size(120, 16);
            this.listBox93.TabIndex = 25;
            // 
            // label154
            // 
            this.label154.AutoSize = true;
            this.label154.Location = new System.Drawing.Point(345, 60);
            this.label154.Name = "label154";
            this.label154.Size = new System.Drawing.Size(17, 12);
            this.label154.TabIndex = 24;
            this.label154.Text = "至";
            // 
            // listBox94
            // 
            this.listBox94.FormattingEnabled = true;
            this.listBox94.ItemHeight = 12;
            this.listBox94.Location = new System.Drawing.Point(154, 60);
            this.listBox94.Name = "listBox94";
            this.listBox94.Size = new System.Drawing.Size(120, 16);
            this.listBox94.TabIndex = 23;
            // 
            // label155
            // 
            this.label155.AutoSize = true;
            this.label155.Location = new System.Drawing.Point(40, 60);
            this.label155.Name = "label155";
            this.label155.Size = new System.Drawing.Size(65, 12);
            this.label155.TabIndex = 22;
            this.label155.Text = "供应商编码";
            // 
            // listBox95
            // 
            this.listBox95.FormattingEnabled = true;
            this.listBox95.ItemHeight = 12;
            this.listBox95.Location = new System.Drawing.Point(459, 20);
            this.listBox95.Name = "listBox95";
            this.listBox95.Size = new System.Drawing.Size(120, 16);
            this.listBox95.TabIndex = 21;
            // 
            // label156
            // 
            this.label156.AutoSize = true;
            this.label156.Location = new System.Drawing.Point(345, 20);
            this.label156.Name = "label156";
            this.label156.Size = new System.Drawing.Size(17, 12);
            this.label156.TabIndex = 20;
            this.label156.Text = "至";
            // 
            // listBox96
            // 
            this.listBox96.FormattingEnabled = true;
            this.listBox96.ItemHeight = 12;
            this.listBox96.Location = new System.Drawing.Point(154, 20);
            this.listBox96.Name = "listBox96";
            this.listBox96.Size = new System.Drawing.Size(120, 16);
            this.listBox96.TabIndex = 19;
            // 
            // label157
            // 
            this.label157.AutoSize = true;
            this.label157.Location = new System.Drawing.Point(40, 20);
            this.label157.Name = "label157";
            this.label157.Size = new System.Drawing.Size(53, 12);
            this.label157.TabIndex = 18;
            this.label157.Text = "物料编码";
            // 
            // tabPage37
            // 
            this.tabPage37.Location = new System.Drawing.Point(4, 22);
            this.tabPage37.Name = "tabPage37";
            this.tabPage37.Size = new System.Drawing.Size(908, 642);
            this.tabPage37.TabIndex = 4;
            this.tabPage37.Text = "维护";
            this.tabPage37.UseVisualStyleBackColor = true;
            // 
            // Price
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1145, 742);
            this.Controls.Add(this.tabControl5);
            this.Name = "Price";
            this.Text = "Price";
            this.tabControl5.ResumeLayout(false);
            this.tabPage22.ResumeLayout(false);
            this.groupBox24.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chart3)).EndInit();
            this.groupBox25.ResumeLayout(false);
            this.tabControl9.ResumeLayout(false);
            this.tabPage23.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView12)).EndInit();
            this.panel22.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView13)).EndInit();
            this.panel23.ResumeLayout(false);
            this.groupBox26.ResumeLayout(false);
            this.groupBox26.PerformLayout();
            this.tabPage25.ResumeLayout(false);
            this.groupBox27.ResumeLayout(false);
            this.groupBox27.PerformLayout();
            this.tabPage35.ResumeLayout(false);
            this.panel24.ResumeLayout(false);
            this.groupBox28.ResumeLayout(false);
            this.groupBox28.PerformLayout();
            this.tabPage36.ResumeLayout(false);
            this.groupBox29.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView14)).EndInit();
            this.groupBox30.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView15)).EndInit();
            this.panel25.ResumeLayout(false);
            this.groupBox31.ResumeLayout(false);
            this.groupBox31.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl5;
        private System.Windows.Forms.TabPage tabPage22;
        private System.Windows.Forms.GroupBox groupBox24;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart3;
        private System.Windows.Forms.GroupBox groupBox25;
        private System.Windows.Forms.TabControl tabControl9;
        private System.Windows.Forms.TabPage tabPage23;
        private System.Windows.Forms.DataGridView dataGridView12;
        private System.Windows.Forms.TabPage tabPage24;
        private System.Windows.Forms.Panel panel22;
        private System.Windows.Forms.Button button40;
        private System.Windows.Forms.Button button41;
        private System.Windows.Forms.Button button42;
        private System.Windows.Forms.DataGridView dataGridView13;
        private System.Windows.Forms.Panel panel23;
        private System.Windows.Forms.GroupBox groupBox26;
        private System.Windows.Forms.TextBox textBox39;
        private System.Windows.Forms.Label label128;
        private System.Windows.Forms.Button button43;
        private System.Windows.Forms.Button button44;
        private System.Windows.Forms.ListBox listBox77;
        private System.Windows.Forms.Label label129;
        private System.Windows.Forms.ListBox listBox78;
        private System.Windows.Forms.Label label130;
        private System.Windows.Forms.ListBox listBox79;
        private System.Windows.Forms.Label label131;
        private System.Windows.Forms.ListBox listBox80;
        private System.Windows.Forms.Label label132;
        private System.Windows.Forms.ListBox listBox81;
        private System.Windows.Forms.Label label133;
        private System.Windows.Forms.ListBox listBox82;
        private System.Windows.Forms.Label label134;
        private System.Windows.Forms.ListBox listBox83;
        private System.Windows.Forms.Label label135;
        private System.Windows.Forms.TabPage tabPage25;
        private System.Windows.Forms.GroupBox groupBox27;
        private System.Windows.Forms.Button button45;
        private System.Windows.Forms.Button button46;
        private System.Windows.Forms.TextBox textBox40;
        private System.Windows.Forms.Label label136;
        private System.Windows.Forms.TextBox textBox41;
        private System.Windows.Forms.Label label137;
        private System.Windows.Forms.TextBox textBox42;
        private System.Windows.Forms.Label label138;
        private System.Windows.Forms.TextBox textBox43;
        private System.Windows.Forms.Label label139;
        private System.Windows.Forms.TextBox textBox44;
        private System.Windows.Forms.Label label140;
        private System.Windows.Forms.TextBox textBox45;
        private System.Windows.Forms.Label label141;
        private System.Windows.Forms.TextBox textBox46;
        private System.Windows.Forms.Label label142;
        private System.Windows.Forms.TextBox textBox47;
        private System.Windows.Forms.Label label143;
        private System.Windows.Forms.TabPage tabPage35;
        private System.Windows.Forms.Panel panel24;
        private System.Windows.Forms.GroupBox groupBox28;
        private System.Windows.Forms.ListBox listBox84;
        private System.Windows.Forms.Label label144;
        private System.Windows.Forms.ListBox listBox85;
        private System.Windows.Forms.Label label145;
        private System.Windows.Forms.ListBox listBox86;
        private System.Windows.Forms.Label label146;
        private System.Windows.Forms.ListBox listBox87;
        private System.Windows.Forms.Label label147;
        private System.Windows.Forms.ListBox listBox88;
        private System.Windows.Forms.Label label148;
        private System.Windows.Forms.ListBox listBox89;
        private System.Windows.Forms.Label label149;
        private System.Windows.Forms.Button button47;
        private System.Windows.Forms.Button button48;
        private System.Windows.Forms.TabPage tabPage36;
        private System.Windows.Forms.GroupBox groupBox29;
        private System.Windows.Forms.DataGridView dataGridView14;
        private System.Windows.Forms.GroupBox groupBox30;
        private System.Windows.Forms.DataGridView dataGridView15;
        private System.Windows.Forms.Panel panel25;
        private System.Windows.Forms.GroupBox groupBox31;
        private System.Windows.Forms.TextBox textBox48;
        private System.Windows.Forms.Label label150;
        private System.Windows.Forms.Button button49;
        private System.Windows.Forms.Button button50;
        private System.Windows.Forms.ListBox listBox90;
        private System.Windows.Forms.Label label151;
        private System.Windows.Forms.ListBox listBox91;
        private System.Windows.Forms.Label label152;
        private System.Windows.Forms.ListBox listBox92;
        private System.Windows.Forms.Label label153;
        private System.Windows.Forms.ListBox listBox93;
        private System.Windows.Forms.Label label154;
        private System.Windows.Forms.ListBox listBox94;
        private System.Windows.Forms.Label label155;
        private System.Windows.Forms.ListBox listBox95;
        private System.Windows.Forms.Label label156;
        private System.Windows.Forms.ListBox listBox96;
        private System.Windows.Forms.Label label157;
        private System.Windows.Forms.TabPage tabPage37;
    }
}