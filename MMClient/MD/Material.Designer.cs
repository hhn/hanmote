﻿namespace MMClient.MD
{
    partial class Material
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.panel4 = new System.Windows.Forms.Panel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.panel3 = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tabControl6 = new System.Windows.Forms.TabControl();
            this.tabPage26 = new System.Windows.Forms.TabPage();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.tabPage27 = new System.Windows.Forms.TabPage();
            this.panel2 = new System.Windows.Forms.Panel();
            this.button5 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.listBox7 = new System.Windows.Forms.ListBox();
            this.label9 = new System.Windows.Forms.Label();
            this.listBox6 = new System.Windows.Forms.ListBox();
            this.label8 = new System.Windows.Forms.Label();
            this.listBox5 = new System.Windows.Forms.ListBox();
            this.label7 = new System.Windows.Forms.Label();
            this.listBox4 = new System.Windows.Forms.ListBox();
            this.label6 = new System.Windows.Forms.Label();
            this.listBox3 = new System.Windows.Forms.ListBox();
            this.label5 = new System.Windows.Forms.Label();
            this.listBox2 = new System.Windows.Forms.ListBox();
            this.label4 = new System.Windows.Forms.Label();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.panel5 = new System.Windows.Forms.Panel();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.button7 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.panel6 = new System.Windows.Forms.Panel();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.listBox8 = new System.Windows.Forms.ListBox();
            this.label21 = new System.Windows.Forms.Label();
            this.listBox9 = new System.Windows.Forms.ListBox();
            this.label22 = new System.Windows.Forms.Label();
            this.listBox10 = new System.Windows.Forms.ListBox();
            this.label23 = new System.Windows.Forms.Label();
            this.listBox11 = new System.Windows.Forms.ListBox();
            this.label24 = new System.Windows.Forms.Label();
            this.listBox12 = new System.Windows.Forms.ListBox();
            this.label25 = new System.Windows.Forms.Label();
            this.listBox13 = new System.Windows.Forms.ListBox();
            this.label26 = new System.Windows.Forms.Label();
            this.button8 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.tabPage8 = new System.Windows.Forms.TabPage();
            this.tabPage9 = new System.Windows.Forms.TabPage();
            this.tabControl7 = new System.Windows.Forms.TabControl();
            this.tabPage29 = new System.Windows.Forms.TabPage();
            this.panel10 = new System.Windows.Forms.Panel();
            this.button12 = new System.Windows.Forms.Button();
            this.dateTimePicker3 = new System.Windows.Forms.DateTimePicker();
            this.label39 = new System.Windows.Forms.Label();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.label38 = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.label37 = new System.Windows.Forms.Label();
            this.textBox15 = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.textBox14 = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.textBox13 = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.tabPage30 = new System.Windows.Forms.TabPage();
            this.panel11 = new System.Windows.Forms.Panel();
            this.dataGridView5 = new System.Windows.Forms.DataGridView();
            this.button13 = new System.Windows.Forms.Button();
            this.dateTimePicker4 = new System.Windows.Forms.DateTimePicker();
            this.label40 = new System.Windows.Forms.Label();
            this.dateTimePicker5 = new System.Windows.Forms.DateTimePicker();
            this.label41 = new System.Windows.Forms.Label();
            this.dateTimePicker6 = new System.Windows.Forms.DateTimePicker();
            this.label42 = new System.Windows.Forms.Label();
            this.textBox16 = new System.Windows.Forms.TextBox();
            this.label43 = new System.Windows.Forms.Label();
            this.textBox17 = new System.Windows.Forms.TextBox();
            this.label44 = new System.Windows.Forms.Label();
            this.textBox18 = new System.Windows.Forms.TextBox();
            this.label45 = new System.Windows.Forms.Label();
            this.tabPage31 = new System.Windows.Forms.TabPage();
            this.panel12 = new System.Windows.Forms.Panel();
            this.dateTimePicker12 = new System.Windows.Forms.DateTimePicker();
            this.label57 = new System.Windows.Forms.Label();
            this.dateTimePicker11 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker10 = new System.Windows.Forms.DateTimePicker();
            this.label56 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.textBox24 = new System.Windows.Forms.TextBox();
            this.textBox23 = new System.Windows.Forms.TextBox();
            this.textBox22 = new System.Windows.Forms.TextBox();
            this.label52 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.button14 = new System.Windows.Forms.Button();
            this.dateTimePicker7 = new System.Windows.Forms.DateTimePicker();
            this.label46 = new System.Windows.Forms.Label();
            this.dateTimePicker8 = new System.Windows.Forms.DateTimePicker();
            this.label47 = new System.Windows.Forms.Label();
            this.dateTimePicker9 = new System.Windows.Forms.DateTimePicker();
            this.label48 = new System.Windows.Forms.Label();
            this.textBox19 = new System.Windows.Forms.TextBox();
            this.label49 = new System.Windows.Forms.Label();
            this.textBox20 = new System.Windows.Forms.TextBox();
            this.label50 = new System.Windows.Forms.Label();
            this.textBox21 = new System.Windows.Forms.TextBox();
            this.label51 = new System.Windows.Forms.Label();
            this.tabPage32 = new System.Windows.Forms.TabPage();
            this.panel13 = new System.Windows.Forms.Panel();
            this.button16 = new System.Windows.Forms.Button();
            this.listBox22 = new System.Windows.Forms.ListBox();
            this.label59 = new System.Windows.Forms.Label();
            this.button15 = new System.Windows.Forms.Button();
            this.listBox21 = new System.Windows.Forms.ListBox();
            this.label58 = new System.Windows.Forms.Label();
            this.tabPage10 = new System.Windows.Forms.TabPage();
            this.panel15 = new System.Windows.Forms.Panel();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.listBox30 = new System.Windows.Forms.ListBox();
            this.label68 = new System.Windows.Forms.Label();
            this.listBox31 = new System.Windows.Forms.ListBox();
            this.label69 = new System.Windows.Forms.Label();
            this.listBox32 = new System.Windows.Forms.ListBox();
            this.label70 = new System.Windows.Forms.Label();
            this.listBox33 = new System.Windows.Forms.ListBox();
            this.label71 = new System.Windows.Forms.Label();
            this.listBox34 = new System.Windows.Forms.ListBox();
            this.label72 = new System.Windows.Forms.Label();
            this.listBox35 = new System.Windows.Forms.ListBox();
            this.label73 = new System.Windows.Forms.Label();
            this.button19 = new System.Windows.Forms.Button();
            this.button20 = new System.Windows.Forms.Button();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.dataGridView6 = new System.Windows.Forms.DataGridView();
            this.panel14 = new System.Windows.Forms.Panel();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.textBox25 = new System.Windows.Forms.TextBox();
            this.label60 = new System.Windows.Forms.Label();
            this.button17 = new System.Windows.Forms.Button();
            this.button18 = new System.Windows.Forms.Button();
            this.listBox23 = new System.Windows.Forms.ListBox();
            this.label61 = new System.Windows.Forms.Label();
            this.listBox24 = new System.Windows.Forms.ListBox();
            this.label62 = new System.Windows.Forms.Label();
            this.listBox25 = new System.Windows.Forms.ListBox();
            this.label63 = new System.Windows.Forms.Label();
            this.listBox26 = new System.Windows.Forms.ListBox();
            this.label64 = new System.Windows.Forms.Label();
            this.listBox27 = new System.Windows.Forms.ListBox();
            this.label65 = new System.Windows.Forms.Label();
            this.listBox28 = new System.Windows.Forms.ListBox();
            this.label66 = new System.Windows.Forms.Label();
            this.listBox29 = new System.Windows.Forms.ListBox();
            this.label67 = new System.Windows.Forms.Label();
            this.tabPage11 = new System.Windows.Forms.TabPage();
            this.panel16 = new System.Windows.Forms.Panel();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.button24 = new System.Windows.Forms.Button();
            this.button23 = new System.Windows.Forms.Button();
            this.button22 = new System.Windows.Forms.Button();
            this.button21 = new System.Windows.Forms.Button();
            this.listBox44 = new System.Windows.Forms.ListBox();
            this.label82 = new System.Windows.Forms.Label();
            this.listBox45 = new System.Windows.Forms.ListBox();
            this.label83 = new System.Windows.Forms.Label();
            this.listBox42 = new System.Windows.Forms.ListBox();
            this.label74 = new System.Windows.Forms.Label();
            this.listBox43 = new System.Windows.Forms.ListBox();
            this.label81 = new System.Windows.Forms.Label();
            this.listBox36 = new System.Windows.Forms.ListBox();
            this.label75 = new System.Windows.Forms.Label();
            this.listBox37 = new System.Windows.Forms.ListBox();
            this.label76 = new System.Windows.Forms.Label();
            this.listBox38 = new System.Windows.Forms.ListBox();
            this.label77 = new System.Windows.Forms.Label();
            this.listBox39 = new System.Windows.Forms.ListBox();
            this.label78 = new System.Windows.Forms.Label();
            this.listBox40 = new System.Windows.Forms.ListBox();
            this.label79 = new System.Windows.Forms.Label();
            this.listBox41 = new System.Windows.Forms.ListBox();
            this.label80 = new System.Windows.Forms.Label();
            this.tabPage28 = new System.Windows.Forms.TabPage();
            this.panel9 = new System.Windows.Forms.Panel();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.dataGridView4 = new System.Windows.Forms.DataGridView();
            this.panel8 = new System.Windows.Forms.Panel();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.dataGridView3 = new System.Windows.Forms.DataGridView();
            this.panel7 = new System.Windows.Forms.Panel();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.textBox12 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.button10 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.listBox14 = new System.Windows.Forms.ListBox();
            this.label27 = new System.Windows.Forms.Label();
            this.listBox15 = new System.Windows.Forms.ListBox();
            this.label28 = new System.Windows.Forms.Label();
            this.listBox16 = new System.Windows.Forms.ListBox();
            this.label29 = new System.Windows.Forms.Label();
            this.listBox17 = new System.Windows.Forms.ListBox();
            this.label30 = new System.Windows.Forms.Label();
            this.listBox18 = new System.Windows.Forms.ListBox();
            this.label31 = new System.Windows.Forms.Label();
            this.listBox19 = new System.Windows.Forms.ListBox();
            this.label32 = new System.Windows.Forms.Label();
            this.listBox20 = new System.Windows.Forms.ListBox();
            this.label33 = new System.Windows.Forms.Label();
            this.tabControl2.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.panel4.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            this.panel3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabControl6.SuspendLayout();
            this.tabPage26.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.panel1.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.tabPage6.SuspendLayout();
            this.panel5.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.tabPage7.SuspendLayout();
            this.panel6.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.tabPage9.SuspendLayout();
            this.tabControl7.SuspendLayout();
            this.tabPage29.SuspendLayout();
            this.panel10.SuspendLayout();
            this.tabPage30.SuspendLayout();
            this.panel11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView5)).BeginInit();
            this.tabPage31.SuspendLayout();
            this.panel12.SuspendLayout();
            this.tabPage32.SuspendLayout();
            this.panel13.SuspendLayout();
            this.tabPage10.SuspendLayout();
            this.panel15.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.groupBox10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView6)).BeginInit();
            this.panel14.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.tabPage11.SuspendLayout();
            this.panel16.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this.tabPage28.SuspendLayout();
            this.panel9.SuspendLayout();
            this.groupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView4)).BeginInit();
            this.panel8.SuspendLayout();
            this.groupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).BeginInit();
            this.panel7.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl2
            // 
            this.tabControl2.Controls.Add(this.tabPage5);
            this.tabControl2.Controls.Add(this.tabPage6);
            this.tabControl2.Controls.Add(this.tabPage7);
            this.tabControl2.Controls.Add(this.tabPage28);
            this.tabControl2.Controls.Add(this.tabPage8);
            this.tabControl2.Controls.Add(this.tabPage9);
            this.tabControl2.Controls.Add(this.tabPage10);
            this.tabControl2.Controls.Add(this.tabPage11);
            this.tabControl2.Location = new System.Drawing.Point(1, 1);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(917, 671);
            this.tabControl2.TabIndex = 1;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.panel4);
            this.tabPage5.Controls.Add(this.panel3);
            this.tabPage5.Controls.Add(this.panel2);
            this.tabPage5.Controls.Add(this.panel1);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(909, 645);
            this.tabPage5.TabIndex = 0;
            this.tabPage5.Text = "查询";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.groupBox2);
            this.panel4.Location = new System.Drawing.Point(561, 238);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(347, 389);
            this.panel4.TabIndex = 5;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.chart1);
            this.groupBox2.Location = new System.Drawing.Point(3, 4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(341, 378);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "数据分析";
            // 
            // chart1
            // 
            chartArea1.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.chart1.Legends.Add(legend1);
            this.chart1.Location = new System.Drawing.Point(6, 20);
            this.chart1.Name = "chart1";
            series1.ChartArea = "ChartArea1";
            series1.Legend = "Legend1";
            series1.Name = "Series1";
            this.chart1.Series.Add(series1);
            this.chart1.Size = new System.Drawing.Size(315, 352);
            this.chart1.TabIndex = 0;
            this.chart1.Text = "chart1";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.groupBox1);
            this.panel3.Location = new System.Drawing.Point(0, 429);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(554, 198);
            this.panel3.TabIndex = 4;
            // 
            // supplierInforGroupBox
            // 
            this.groupBox1.Controls.Add(this.tabControl6);
            this.groupBox1.Location = new System.Drawing.Point(6, 3);
            this.groupBox1.Name = "supplierInforGroupBox";
            this.groupBox1.Size = new System.Drawing.Size(591, 192);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "详细信息";
            // 
            // tabControl6
            // 
            this.tabControl6.Controls.Add(this.tabPage26);
            this.tabControl6.Controls.Add(this.tabPage27);
            this.tabControl6.Location = new System.Drawing.Point(1, 20);
            this.tabControl6.Name = "tabControl6";
            this.tabControl6.SelectedIndex = 0;
            this.tabControl6.Size = new System.Drawing.Size(590, 172);
            this.tabControl6.TabIndex = 0;
            // 
            // tabPage26
            // 
            this.tabPage26.Controls.Add(this.dataGridView2);
            this.tabPage26.Location = new System.Drawing.Point(4, 22);
            this.tabPage26.Name = "tabPage26";
            this.tabPage26.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage26.Size = new System.Drawing.Size(582, 146);
            this.tabPage26.TabIndex = 0;
            this.tabPage26.Text = "详细记录";
            this.tabPage26.UseVisualStyleBackColor = true;
            // 
            // dataGridView2
            // 
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Location = new System.Drawing.Point(1, 3);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.RowTemplate.Height = 23;
            this.dataGridView2.Size = new System.Drawing.Size(539, 137);
            this.dataGridView2.TabIndex = 0;
            // 
            // tabPage27
            // 
            this.tabPage27.Location = new System.Drawing.Point(4, 22);
            this.tabPage27.Name = "tabPage27";
            this.tabPage27.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage27.Size = new System.Drawing.Size(582, 146);
            this.tabPage27.TabIndex = 1;
            this.tabPage27.Text = "其他项";
            this.tabPage27.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.button5);
            this.panel2.Controls.Add(this.button4);
            this.panel2.Controls.Add(this.button3);
            this.panel2.Controls.Add(this.dataGridView1);
            this.panel2.Location = new System.Drawing.Point(0, 238);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(554, 185);
            this.panel2.TabIndex = 3;
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(377, 159);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(75, 23);
            this.button5.TabIndex = 3;
            this.button5.Text = "导出数据";
            this.button5.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(206, 159);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 2;
            this.button4.Text = "数据分析";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(50, 159);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 1;
            this.button3.Text = "删除";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(3, 4);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.Size = new System.Drawing.Size(548, 150);
            this.dataGridView1.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.groupBox5);
            this.panel1.Location = new System.Drawing.Point(0, 6);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(906, 225);
            this.panel1.TabIndex = 2;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.textBox1);
            this.groupBox5.Controls.Add(this.label10);
            this.groupBox5.Controls.Add(this.button2);
            this.groupBox5.Controls.Add(this.button1);
            this.groupBox5.Controls.Add(this.listBox7);
            this.groupBox5.Controls.Add(this.label9);
            this.groupBox5.Controls.Add(this.listBox6);
            this.groupBox5.Controls.Add(this.label8);
            this.groupBox5.Controls.Add(this.listBox5);
            this.groupBox5.Controls.Add(this.label7);
            this.groupBox5.Controls.Add(this.listBox4);
            this.groupBox5.Controls.Add(this.label6);
            this.groupBox5.Controls.Add(this.listBox3);
            this.groupBox5.Controls.Add(this.label5);
            this.groupBox5.Controls.Add(this.listBox2);
            this.groupBox5.Controls.Add(this.label4);
            this.groupBox5.Controls.Add(this.listBox1);
            this.groupBox5.Controls.Add(this.label3);
            this.groupBox5.Location = new System.Drawing.Point(7, 3);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(631, 219);
            this.groupBox5.TabIndex = 18;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "条件选择";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(459, 170);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(117, 21);
            this.textBox1.TabIndex = 35;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(344, 170);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(65, 12);
            this.label10.TabIndex = 34;
            this.label10.Text = "最大命中数";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(231, 170);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 33;
            this.button2.Text = "清除";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(96, 170);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 32;
            this.button1.Text = "查询";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // listBox7
            // 
            this.listBox7.FormattingEnabled = true;
            this.listBox7.ItemHeight = 12;
            this.listBox7.Location = new System.Drawing.Point(154, 148);
            this.listBox7.Name = "listBox7";
            this.listBox7.Size = new System.Drawing.Size(120, 16);
            this.listBox7.TabIndex = 31;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(40, 148);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(29, 12);
            this.label9.TabIndex = 30;
            this.label9.Text = "其他";
            // 
            // listBox6
            // 
            this.listBox6.FormattingEnabled = true;
            this.listBox6.ItemHeight = 12;
            this.listBox6.Location = new System.Drawing.Point(459, 105);
            this.listBox6.Name = "listBox6";
            this.listBox6.Size = new System.Drawing.Size(120, 16);
            this.listBox6.TabIndex = 29;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(345, 105);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(17, 12);
            this.label8.TabIndex = 28;
            this.label8.Text = "至";
            // 
            // listBox5
            // 
            this.listBox5.FormattingEnabled = true;
            this.listBox5.ItemHeight = 12;
            this.listBox5.Location = new System.Drawing.Point(154, 105);
            this.listBox5.Name = "listBox5";
            this.listBox5.Size = new System.Drawing.Size(120, 16);
            this.listBox5.TabIndex = 27;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(40, 105);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(41, 12);
            this.label7.TabIndex = 26;
            this.label7.Text = "权限组";
            // 
            // listBox4
            // 
            this.listBox4.FormattingEnabled = true;
            this.listBox4.ItemHeight = 12;
            this.listBox4.Location = new System.Drawing.Point(459, 60);
            this.listBox4.Name = "listBox4";
            this.listBox4.Size = new System.Drawing.Size(120, 16);
            this.listBox4.TabIndex = 25;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(345, 60);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(17, 12);
            this.label6.TabIndex = 24;
            this.label6.Text = "至";
            // 
            // listBox3
            // 
            this.listBox3.FormattingEnabled = true;
            this.listBox3.ItemHeight = 12;
            this.listBox3.Location = new System.Drawing.Point(154, 60);
            this.listBox3.Name = "listBox3";
            this.listBox3.Size = new System.Drawing.Size(120, 16);
            this.listBox3.TabIndex = 23;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(40, 60);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 12);
            this.label5.TabIndex = 22;
            this.label5.Text = "物资级别";
            // 
            // listBox2
            // 
            this.listBox2.FormattingEnabled = true;
            this.listBox2.ItemHeight = 12;
            this.listBox2.Location = new System.Drawing.Point(459, 20);
            this.listBox2.Name = "listBox2";
            this.listBox2.Size = new System.Drawing.Size(120, 16);
            this.listBox2.TabIndex = 21;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(345, 20);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(17, 12);
            this.label4.TabIndex = 20;
            this.label4.Text = "至";
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.ItemHeight = 12;
            this.listBox1.Location = new System.Drawing.Point(154, 20);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(120, 16);
            this.listBox1.TabIndex = 19;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(40, 20);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 12);
            this.label3.TabIndex = 18;
            this.label3.Text = "物资类别";
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.panel5);
            this.tabPage6.Location = new System.Drawing.Point(4, 22);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage6.Size = new System.Drawing.Size(909, 645);
            this.tabPage6.TabIndex = 1;
            this.tabPage6.Text = "创建";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.groupBox3);
            this.panel5.Location = new System.Drawing.Point(7, 7);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(663, 635);
            this.panel5.TabIndex = 0;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.button7);
            this.groupBox3.Controls.Add(this.button6);
            this.groupBox3.Controls.Add(this.textBox11);
            this.groupBox3.Controls.Add(this.label20);
            this.groupBox3.Controls.Add(this.textBox10);
            this.groupBox3.Controls.Add(this.label19);
            this.groupBox3.Controls.Add(this.textBox9);
            this.groupBox3.Controls.Add(this.label18);
            this.groupBox3.Controls.Add(this.textBox8);
            this.groupBox3.Controls.Add(this.label17);
            this.groupBox3.Controls.Add(this.textBox7);
            this.groupBox3.Controls.Add(this.label16);
            this.groupBox3.Controls.Add(this.textBox6);
            this.groupBox3.Controls.Add(this.label15);
            this.groupBox3.Controls.Add(this.textBox5);
            this.groupBox3.Controls.Add(this.label14);
            this.groupBox3.Controls.Add(this.textBox4);
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this.textBox3);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Controls.Add(this.textBox2);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Location = new System.Drawing.Point(4, 3);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(656, 619);
            this.groupBox3.TabIndex = 0;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "创建新数据";
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(340, 556);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(75, 23);
            this.button7.TabIndex = 21;
            this.button7.Text = "重置";
            this.button7.UseVisualStyleBackColor = true;
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(78, 556);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(75, 23);
            this.button6.TabIndex = 20;
            this.button6.Text = "确定";
            this.button6.UseVisualStyleBackColor = true;
            // 
            // textBox11
            // 
            this.textBox11.Location = new System.Drawing.Point(409, 283);
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new System.Drawing.Size(128, 21);
            this.textBox11.TabIndex = 19;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(338, 288);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(29, 12);
            this.label20.TabIndex = 18;
            this.label20.Text = "其他";
            // 
            // textBox10
            // 
            this.textBox10.Location = new System.Drawing.Point(78, 283);
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new System.Drawing.Size(128, 21);
            this.textBox10.TabIndex = 17;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(7, 288);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(65, 12);
            this.label19.TabIndex = 16;
            this.label19.Text = "供应商编码";
            // 
            // textBox9
            // 
            this.textBox9.Location = new System.Drawing.Point(409, 213);
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new System.Drawing.Size(128, 21);
            this.textBox9.TabIndex = 15;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(338, 218);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(53, 12);
            this.label18.TabIndex = 14;
            this.label18.Text = "批次管理";
            // 
            // textBox8
            // 
            this.textBox8.Location = new System.Drawing.Point(78, 213);
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(128, 21);
            this.textBox8.TabIndex = 13;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(7, 218);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(53, 12);
            this.label17.TabIndex = 12;
            this.label17.Text = "计量单位";
            // 
            // textBox7
            // 
            this.textBox7.Location = new System.Drawing.Point(409, 151);
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(128, 21);
            this.textBox7.TabIndex = 11;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(338, 156);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(53, 12);
            this.label16.TabIndex = 10;
            this.label16.Text = "物资名称";
            // 
            // textBox6
            // 
            this.textBox6.Location = new System.Drawing.Point(78, 147);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(128, 21);
            this.textBox6.TabIndex = 9;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(7, 152);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(53, 12);
            this.label15.TabIndex = 8;
            this.label15.Text = "用户编码";
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(409, 80);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(128, 21);
            this.textBox5.TabIndex = 7;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(338, 85);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(41, 12);
            this.label14.TabIndex = 6;
            this.label14.Text = "权限组";
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(77, 82);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(128, 21);
            this.textBox4.TabIndex = 5;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(6, 87);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(53, 12);
            this.label13.TabIndex = 4;
            this.label13.Text = "物资级别";
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(409, 19);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(128, 21);
            this.textBox3.TabIndex = 3;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(338, 24);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(53, 12);
            this.label12.TabIndex = 2;
            this.label12.Text = "物资分类";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(78, 21);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(128, 21);
            this.textBox2.TabIndex = 1;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(7, 26);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(41, 12);
            this.label11.TabIndex = 0;
            this.label11.Text = "物资ID";
            // 
            // tabPage7
            // 
            this.tabPage7.Controls.Add(this.panel6);
            this.tabPage7.Location = new System.Drawing.Point(4, 22);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Size = new System.Drawing.Size(909, 645);
            this.tabPage7.TabIndex = 2;
            this.tabPage7.Text = "修改";
            this.tabPage7.UseVisualStyleBackColor = true;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.groupBox4);
            this.panel6.Location = new System.Drawing.Point(4, 4);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(651, 640);
            this.panel6.TabIndex = 0;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.listBox8);
            this.groupBox4.Controls.Add(this.label21);
            this.groupBox4.Controls.Add(this.listBox9);
            this.groupBox4.Controls.Add(this.label22);
            this.groupBox4.Controls.Add(this.listBox10);
            this.groupBox4.Controls.Add(this.label23);
            this.groupBox4.Controls.Add(this.listBox11);
            this.groupBox4.Controls.Add(this.label24);
            this.groupBox4.Controls.Add(this.listBox12);
            this.groupBox4.Controls.Add(this.label25);
            this.groupBox4.Controls.Add(this.listBox13);
            this.groupBox4.Controls.Add(this.label26);
            this.groupBox4.Controls.Add(this.button8);
            this.groupBox4.Controls.Add(this.button9);
            this.groupBox4.Location = new System.Drawing.Point(3, 3);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(640, 616);
            this.groupBox4.TabIndex = 1;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "修改新数据";
            // 
            // listBox8
            // 
            this.listBox8.FormattingEnabled = true;
            this.listBox8.ItemHeight = 12;
            this.listBox8.Location = new System.Drawing.Point(436, 113);
            this.listBox8.Name = "listBox8";
            this.listBox8.Size = new System.Drawing.Size(120, 16);
            this.listBox8.TabIndex = 35;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(322, 113);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(41, 12);
            this.label21.TabIndex = 34;
            this.label21.Text = "修改至";
            // 
            // listBox9
            // 
            this.listBox9.FormattingEnabled = true;
            this.listBox9.ItemHeight = 12;
            this.listBox9.Location = new System.Drawing.Point(131, 113);
            this.listBox9.Name = "listBox9";
            this.listBox9.Size = new System.Drawing.Size(120, 16);
            this.listBox9.TabIndex = 33;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(17, 113);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(41, 12);
            this.label22.TabIndex = 32;
            this.label22.Text = "权限组";
            // 
            // listBox10
            // 
            this.listBox10.FormattingEnabled = true;
            this.listBox10.ItemHeight = 12;
            this.listBox10.Location = new System.Drawing.Point(436, 68);
            this.listBox10.Name = "listBox10";
            this.listBox10.Size = new System.Drawing.Size(120, 16);
            this.listBox10.TabIndex = 31;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(322, 68);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(41, 12);
            this.label23.TabIndex = 30;
            this.label23.Text = "修改至";
            // 
            // listBox11
            // 
            this.listBox11.FormattingEnabled = true;
            this.listBox11.ItemHeight = 12;
            this.listBox11.Location = new System.Drawing.Point(131, 68);
            this.listBox11.Name = "listBox11";
            this.listBox11.Size = new System.Drawing.Size(120, 16);
            this.listBox11.TabIndex = 29;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(17, 68);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(53, 12);
            this.label24.TabIndex = 28;
            this.label24.Text = "物资级别";
            // 
            // listBox12
            // 
            this.listBox12.FormattingEnabled = true;
            this.listBox12.ItemHeight = 12;
            this.listBox12.Location = new System.Drawing.Point(436, 28);
            this.listBox12.Name = "listBox12";
            this.listBox12.Size = new System.Drawing.Size(120, 16);
            this.listBox12.TabIndex = 27;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(322, 28);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(41, 12);
            this.label25.TabIndex = 26;
            this.label25.Text = "修改至";
            // 
            // listBox13
            // 
            this.listBox13.FormattingEnabled = true;
            this.listBox13.ItemHeight = 12;
            this.listBox13.Location = new System.Drawing.Point(131, 28);
            this.listBox13.Name = "listBox13";
            this.listBox13.Size = new System.Drawing.Size(120, 16);
            this.listBox13.TabIndex = 25;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(17, 28);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(53, 12);
            this.label26.TabIndex = 24;
            this.label26.Text = "物资类别";
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(392, 558);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(75, 23);
            this.button8.TabIndex = 23;
            this.button8.Text = "重置";
            this.button8.UseVisualStyleBackColor = true;
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(130, 558);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(75, 23);
            this.button9.TabIndex = 22;
            this.button9.Text = "确定";
            this.button9.UseVisualStyleBackColor = true;
            // 
            // tabPage8
            // 
            this.tabPage8.Location = new System.Drawing.Point(4, 22);
            this.tabPage8.Name = "tabPage8";
            this.tabPage8.Size = new System.Drawing.Size(909, 645);
            this.tabPage8.TabIndex = 3;
            this.tabPage8.Text = "维护";
            this.tabPage8.UseVisualStyleBackColor = true;
            // 
            // tabPage9
            // 
            this.tabPage9.Controls.Add(this.tabControl7);
            this.tabPage9.Location = new System.Drawing.Point(4, 22);
            this.tabPage9.Name = "tabPage9";
            this.tabPage9.Size = new System.Drawing.Size(909, 645);
            this.tabPage9.TabIndex = 4;
            this.tabPage9.Text = "批次管理";
            this.tabPage9.UseVisualStyleBackColor = true;
            // 
            // tabControl7
            // 
            this.tabControl7.Controls.Add(this.tabPage29);
            this.tabControl7.Controls.Add(this.tabPage30);
            this.tabControl7.Controls.Add(this.tabPage31);
            this.tabControl7.Controls.Add(this.tabPage32);
            this.tabControl7.Location = new System.Drawing.Point(4, 4);
            this.tabControl7.Name = "tabControl7";
            this.tabControl7.SelectedIndex = 0;
            this.tabControl7.Size = new System.Drawing.Size(619, 644);
            this.tabControl7.TabIndex = 0;
            // 
            // tabPage29
            // 
            this.tabPage29.Controls.Add(this.panel10);
            this.tabPage29.Location = new System.Drawing.Point(4, 22);
            this.tabPage29.Name = "tabPage29";
            this.tabPage29.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage29.Size = new System.Drawing.Size(611, 618);
            this.tabPage29.TabIndex = 0;
            this.tabPage29.Text = "数据录入";
            this.tabPage29.UseVisualStyleBackColor = true;
            // 
            // panel10
            // 
            this.panel10.Controls.Add(this.button12);
            this.panel10.Controls.Add(this.dateTimePicker3);
            this.panel10.Controls.Add(this.label39);
            this.panel10.Controls.Add(this.dateTimePicker2);
            this.panel10.Controls.Add(this.label38);
            this.panel10.Controls.Add(this.dateTimePicker1);
            this.panel10.Controls.Add(this.label37);
            this.panel10.Controls.Add(this.textBox15);
            this.panel10.Controls.Add(this.label36);
            this.panel10.Controls.Add(this.textBox14);
            this.panel10.Controls.Add(this.label35);
            this.panel10.Controls.Add(this.textBox13);
            this.panel10.Controls.Add(this.label34);
            this.panel10.Location = new System.Drawing.Point(4, 4);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(511, 608);
            this.panel10.TabIndex = 0;
            // 
            // button12
            // 
            this.button12.Location = new System.Drawing.Point(156, 488);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(75, 23);
            this.button12.TabIndex = 12;
            this.button12.Text = "确定";
            this.button12.UseVisualStyleBackColor = true;
            // 
            // dateTimePicker3
            // 
            this.dateTimePicker3.Location = new System.Drawing.Point(129, 397);
            this.dateTimePicker3.Name = "dateTimePicker3";
            this.dateTimePicker3.Size = new System.Drawing.Size(175, 21);
            this.dateTimePicker3.TabIndex = 11;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(22, 397);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(53, 12);
            this.label39.TabIndex = 10;
            this.label39.Text = "出货日期";
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.Location = new System.Drawing.Point(129, 326);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(175, 21);
            this.dateTimePicker2.TabIndex = 9;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(22, 326);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(53, 12);
            this.label38.TabIndex = 8;
            this.label38.Text = "收货日期";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(129, 250);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(175, 21);
            this.dateTimePicker1.TabIndex = 7;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(22, 250);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(53, 12);
            this.label37.TabIndex = 6;
            this.label37.Text = "生产日期";
            // 
            // textBox15
            // 
            this.textBox15.Location = new System.Drawing.Point(129, 167);
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new System.Drawing.Size(175, 21);
            this.textBox15.TabIndex = 5;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(22, 167);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(53, 12);
            this.label36.TabIndex = 4;
            this.label36.Text = "批次级别";
            // 
            // textBox14
            // 
            this.textBox14.Location = new System.Drawing.Point(129, 93);
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new System.Drawing.Size(175, 21);
            this.textBox14.TabIndex = 3;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(22, 93);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(53, 12);
            this.label35.TabIndex = 2;
            this.label35.Text = "批次名称";
            // 
            // textBox13
            // 
            this.textBox13.Location = new System.Drawing.Point(129, 24);
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new System.Drawing.Size(175, 21);
            this.textBox13.TabIndex = 1;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(22, 24);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(41, 12);
            this.label34.TabIndex = 0;
            this.label34.Text = "批次ID";
            // 
            // tabPage30
            // 
            this.tabPage30.Controls.Add(this.panel11);
            this.tabPage30.Location = new System.Drawing.Point(4, 22);
            this.tabPage30.Name = "tabPage30";
            this.tabPage30.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage30.Size = new System.Drawing.Size(611, 618);
            this.tabPage30.TabIndex = 1;
            this.tabPage30.Text = "数据查询";
            this.tabPage30.UseVisualStyleBackColor = true;
            // 
            // panel11
            // 
            this.panel11.Controls.Add(this.dataGridView5);
            this.panel11.Controls.Add(this.button13);
            this.panel11.Controls.Add(this.dateTimePicker4);
            this.panel11.Controls.Add(this.label40);
            this.panel11.Controls.Add(this.dateTimePicker5);
            this.panel11.Controls.Add(this.label41);
            this.panel11.Controls.Add(this.dateTimePicker6);
            this.panel11.Controls.Add(this.label42);
            this.panel11.Controls.Add(this.textBox16);
            this.panel11.Controls.Add(this.label43);
            this.panel11.Controls.Add(this.textBox17);
            this.panel11.Controls.Add(this.label44);
            this.panel11.Controls.Add(this.textBox18);
            this.panel11.Controls.Add(this.label45);
            this.panel11.Location = new System.Drawing.Point(3, 3);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(511, 608);
            this.panel11.TabIndex = 1;
            // 
            // dataGridView5
            // 
            this.dataGridView5.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView5.Location = new System.Drawing.Point(3, 427);
            this.dataGridView5.Name = "dataGridView5";
            this.dataGridView5.RowTemplate.Height = 23;
            this.dataGridView5.Size = new System.Drawing.Size(505, 178);
            this.dataGridView5.TabIndex = 13;
            // 
            // button13
            // 
            this.button13.Location = new System.Drawing.Point(156, 398);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(75, 23);
            this.button13.TabIndex = 12;
            this.button13.Text = "确定";
            this.button13.UseVisualStyleBackColor = true;
            // 
            // dateTimePicker4
            // 
            this.dateTimePicker4.Location = new System.Drawing.Point(129, 343);
            this.dateTimePicker4.Name = "dateTimePicker4";
            this.dateTimePicker4.Size = new System.Drawing.Size(175, 21);
            this.dateTimePicker4.TabIndex = 11;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(22, 343);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(53, 12);
            this.label40.TabIndex = 10;
            this.label40.Text = "出货日期";
            // 
            // dateTimePicker5
            // 
            this.dateTimePicker5.Location = new System.Drawing.Point(129, 273);
            this.dateTimePicker5.Name = "dateTimePicker5";
            this.dateTimePicker5.Size = new System.Drawing.Size(175, 21);
            this.dateTimePicker5.TabIndex = 9;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(22, 273);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(53, 12);
            this.label41.TabIndex = 8;
            this.label41.Text = "收货日期";
            // 
            // dateTimePicker6
            // 
            this.dateTimePicker6.Location = new System.Drawing.Point(129, 196);
            this.dateTimePicker6.Name = "dateTimePicker6";
            this.dateTimePicker6.Size = new System.Drawing.Size(175, 21);
            this.dateTimePicker6.TabIndex = 7;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(22, 196);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(53, 12);
            this.label42.TabIndex = 6;
            this.label42.Text = "生产日期";
            // 
            // textBox16
            // 
            this.textBox16.Location = new System.Drawing.Point(129, 129);
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new System.Drawing.Size(175, 21);
            this.textBox16.TabIndex = 5;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(22, 129);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(53, 12);
            this.label43.TabIndex = 4;
            this.label43.Text = "批次级别";
            // 
            // textBox17
            // 
            this.textBox17.Location = new System.Drawing.Point(129, 72);
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new System.Drawing.Size(175, 21);
            this.textBox17.TabIndex = 3;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(22, 72);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(53, 12);
            this.label44.TabIndex = 2;
            this.label44.Text = "批次名称";
            // 
            // textBox18
            // 
            this.textBox18.Location = new System.Drawing.Point(129, 24);
            this.textBox18.Name = "textBox18";
            this.textBox18.Size = new System.Drawing.Size(175, 21);
            this.textBox18.TabIndex = 1;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(22, 24);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(41, 12);
            this.label45.TabIndex = 0;
            this.label45.Text = "批次ID";
            // 
            // tabPage31
            // 
            this.tabPage31.Controls.Add(this.panel12);
            this.tabPage31.Location = new System.Drawing.Point(4, 22);
            this.tabPage31.Name = "tabPage31";
            this.tabPage31.Size = new System.Drawing.Size(611, 618);
            this.tabPage31.TabIndex = 2;
            this.tabPage31.Text = "数据修改";
            this.tabPage31.UseVisualStyleBackColor = true;
            // 
            // panel12
            // 
            this.panel12.Controls.Add(this.dateTimePicker12);
            this.panel12.Controls.Add(this.label57);
            this.panel12.Controls.Add(this.dateTimePicker11);
            this.panel12.Controls.Add(this.dateTimePicker10);
            this.panel12.Controls.Add(this.label56);
            this.panel12.Controls.Add(this.label55);
            this.panel12.Controls.Add(this.textBox24);
            this.panel12.Controls.Add(this.textBox23);
            this.panel12.Controls.Add(this.textBox22);
            this.panel12.Controls.Add(this.label52);
            this.panel12.Controls.Add(this.label53);
            this.panel12.Controls.Add(this.label54);
            this.panel12.Controls.Add(this.button14);
            this.panel12.Controls.Add(this.dateTimePicker7);
            this.panel12.Controls.Add(this.label46);
            this.panel12.Controls.Add(this.dateTimePicker8);
            this.panel12.Controls.Add(this.label47);
            this.panel12.Controls.Add(this.dateTimePicker9);
            this.panel12.Controls.Add(this.label48);
            this.panel12.Controls.Add(this.textBox19);
            this.panel12.Controls.Add(this.label49);
            this.panel12.Controls.Add(this.textBox20);
            this.panel12.Controls.Add(this.label50);
            this.panel12.Controls.Add(this.textBox21);
            this.panel12.Controls.Add(this.label51);
            this.panel12.Location = new System.Drawing.Point(3, 3);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(605, 608);
            this.panel12.TabIndex = 1;
            // 
            // dateTimePicker12
            // 
            this.dateTimePicker12.Location = new System.Drawing.Point(382, 397);
            this.dateTimePicker12.Name = "dateTimePicker12";
            this.dateTimePicker12.Size = new System.Drawing.Size(175, 21);
            this.dateTimePicker12.TabIndex = 51;
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Location = new System.Drawing.Point(297, 397);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(41, 12);
            this.label57.TabIndex = 50;
            this.label57.Text = "修改至";
            // 
            // dateTimePicker11
            // 
            this.dateTimePicker11.Location = new System.Drawing.Point(382, 325);
            this.dateTimePicker11.Name = "dateTimePicker11";
            this.dateTimePicker11.Size = new System.Drawing.Size(175, 21);
            this.dateTimePicker11.TabIndex = 49;
            // 
            // dateTimePicker10
            // 
            this.dateTimePicker10.Location = new System.Drawing.Point(382, 252);
            this.dateTimePicker10.Name = "dateTimePicker10";
            this.dateTimePicker10.Size = new System.Drawing.Size(175, 21);
            this.dateTimePicker10.TabIndex = 48;
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(297, 325);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(41, 12);
            this.label56.TabIndex = 47;
            this.label56.Text = "修改至";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(297, 252);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(41, 12);
            this.label55.TabIndex = 45;
            this.label55.Text = "修改至";
            // 
            // textBox24
            // 
            this.textBox24.Location = new System.Drawing.Point(382, 167);
            this.textBox24.Name = "textBox24";
            this.textBox24.Size = new System.Drawing.Size(175, 21);
            this.textBox24.TabIndex = 44;
            // 
            // textBox23
            // 
            this.textBox23.Location = new System.Drawing.Point(382, 90);
            this.textBox23.Name = "textBox23";
            this.textBox23.Size = new System.Drawing.Size(175, 21);
            this.textBox23.TabIndex = 43;
            // 
            // textBox22
            // 
            this.textBox22.Location = new System.Drawing.Point(382, 24);
            this.textBox22.Name = "textBox22";
            this.textBox22.Size = new System.Drawing.Size(175, 21);
            this.textBox22.TabIndex = 42;
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(297, 166);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(41, 12);
            this.label52.TabIndex = 40;
            this.label52.Text = "修改至";
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(297, 93);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(41, 12);
            this.label53.TabIndex = 38;
            this.label53.Text = "修改至";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(297, 24);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(41, 12);
            this.label54.TabIndex = 36;
            this.label54.Text = "修改至";
            // 
            // button14
            // 
            this.button14.Location = new System.Drawing.Point(282, 468);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(75, 23);
            this.button14.TabIndex = 12;
            this.button14.Text = "确定修改";
            this.button14.UseVisualStyleBackColor = true;
            // 
            // dateTimePicker7
            // 
            this.dateTimePicker7.Location = new System.Drawing.Point(99, 397);
            this.dateTimePicker7.Name = "dateTimePicker7";
            this.dateTimePicker7.Size = new System.Drawing.Size(175, 21);
            this.dateTimePicker7.TabIndex = 11;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(22, 397);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(53, 12);
            this.label46.TabIndex = 10;
            this.label46.Text = "出货日期";
            // 
            // dateTimePicker8
            // 
            this.dateTimePicker8.Location = new System.Drawing.Point(99, 326);
            this.dateTimePicker8.Name = "dateTimePicker8";
            this.dateTimePicker8.Size = new System.Drawing.Size(175, 21);
            this.dateTimePicker8.TabIndex = 9;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(22, 326);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(53, 12);
            this.label47.TabIndex = 8;
            this.label47.Text = "收货日期";
            // 
            // dateTimePicker9
            // 
            this.dateTimePicker9.Location = new System.Drawing.Point(99, 250);
            this.dateTimePicker9.Name = "dateTimePicker9";
            this.dateTimePicker9.Size = new System.Drawing.Size(175, 21);
            this.dateTimePicker9.TabIndex = 7;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(22, 250);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(53, 12);
            this.label48.TabIndex = 6;
            this.label48.Text = "生产日期";
            // 
            // textBox19
            // 
            this.textBox19.Location = new System.Drawing.Point(99, 164);
            this.textBox19.Name = "textBox19";
            this.textBox19.Size = new System.Drawing.Size(175, 21);
            this.textBox19.TabIndex = 5;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(22, 167);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(53, 12);
            this.label49.TabIndex = 4;
            this.label49.Text = "批次级别";
            // 
            // textBox20
            // 
            this.textBox20.Location = new System.Drawing.Point(99, 90);
            this.textBox20.Name = "textBox20";
            this.textBox20.Size = new System.Drawing.Size(175, 21);
            this.textBox20.TabIndex = 3;
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(22, 93);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(53, 12);
            this.label50.TabIndex = 2;
            this.label50.Text = "批次名称";
            // 
            // textBox21
            // 
            this.textBox21.Location = new System.Drawing.Point(99, 24);
            this.textBox21.Name = "textBox21";
            this.textBox21.Size = new System.Drawing.Size(175, 21);
            this.textBox21.TabIndex = 1;
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(22, 24);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(41, 12);
            this.label51.TabIndex = 0;
            this.label51.Text = "批次ID";
            // 
            // tabPage32
            // 
            this.tabPage32.Controls.Add(this.panel13);
            this.tabPage32.Location = new System.Drawing.Point(4, 22);
            this.tabPage32.Name = "tabPage32";
            this.tabPage32.Size = new System.Drawing.Size(611, 618);
            this.tabPage32.TabIndex = 3;
            this.tabPage32.Text = "数据入库出库确定";
            this.tabPage32.UseVisualStyleBackColor = true;
            // 
            // panel13
            // 
            this.panel13.Controls.Add(this.button16);
            this.panel13.Controls.Add(this.listBox22);
            this.panel13.Controls.Add(this.label59);
            this.panel13.Controls.Add(this.button15);
            this.panel13.Controls.Add(this.listBox21);
            this.panel13.Controls.Add(this.label58);
            this.panel13.Location = new System.Drawing.Point(0, 4);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(497, 611);
            this.panel13.TabIndex = 0;
            // 
            // button16
            // 
            this.button16.Location = new System.Drawing.Point(107, 247);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(75, 23);
            this.button16.TabIndex = 16;
            this.button16.Text = "确定入库";
            this.button16.UseVisualStyleBackColor = true;
            // 
            // listBox22
            // 
            this.listBox22.FormattingEnabled = true;
            this.listBox22.ItemHeight = 12;
            this.listBox22.Location = new System.Drawing.Point(107, 179);
            this.listBox22.Name = "listBox22";
            this.listBox22.Size = new System.Drawing.Size(120, 16);
            this.listBox22.TabIndex = 15;
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Location = new System.Drawing.Point(16, 179);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(41, 12);
            this.label59.TabIndex = 14;
            this.label59.Text = "批次ID";
            // 
            // button15
            // 
            this.button15.Location = new System.Drawing.Point(107, 81);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(75, 23);
            this.button15.TabIndex = 13;
            this.button15.Text = "确定出库";
            this.button15.UseVisualStyleBackColor = true;
            // 
            // listBox21
            // 
            this.listBox21.FormattingEnabled = true;
            this.listBox21.ItemHeight = 12;
            this.listBox21.Location = new System.Drawing.Point(107, 13);
            this.listBox21.Name = "listBox21";
            this.listBox21.Size = new System.Drawing.Size(120, 16);
            this.listBox21.TabIndex = 3;
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Location = new System.Drawing.Point(16, 13);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(41, 12);
            this.label58.TabIndex = 2;
            this.label58.Text = "批次ID";
            // 
            // tabPage10
            // 
            this.tabPage10.Controls.Add(this.panel15);
            this.tabPage10.Controls.Add(this.groupBox10);
            this.tabPage10.Controls.Add(this.panel14);
            this.tabPage10.Location = new System.Drawing.Point(4, 22);
            this.tabPage10.Name = "tabPage10";
            this.tabPage10.Size = new System.Drawing.Size(909, 645);
            this.tabPage10.TabIndex = 5;
            this.tabPage10.Text = "记录变更";
            this.tabPage10.UseVisualStyleBackColor = true;
            // 
            // panel15
            // 
            this.panel15.Controls.Add(this.groupBox11);
            this.panel15.Location = new System.Drawing.Point(10, 444);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(631, 208);
            this.panel15.TabIndex = 5;
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.listBox30);
            this.groupBox11.Controls.Add(this.label68);
            this.groupBox11.Controls.Add(this.listBox31);
            this.groupBox11.Controls.Add(this.label69);
            this.groupBox11.Controls.Add(this.listBox32);
            this.groupBox11.Controls.Add(this.label70);
            this.groupBox11.Controls.Add(this.listBox33);
            this.groupBox11.Controls.Add(this.label71);
            this.groupBox11.Controls.Add(this.listBox34);
            this.groupBox11.Controls.Add(this.label72);
            this.groupBox11.Controls.Add(this.listBox35);
            this.groupBox11.Controls.Add(this.label73);
            this.groupBox11.Controls.Add(this.button19);
            this.groupBox11.Controls.Add(this.button20);
            this.groupBox11.Location = new System.Drawing.Point(3, 3);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(622, 194);
            this.groupBox11.TabIndex = 1;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "修改查询结果";
            // 
            // listBox30
            // 
            this.listBox30.DisplayMember = "Material_Name";
            this.listBox30.FormattingEnabled = true;
            this.listBox30.ItemHeight = 12;
            this.listBox30.Location = new System.Drawing.Point(436, 113);
            this.listBox30.Name = "listBox30";
            this.listBox30.Size = new System.Drawing.Size(120, 16);
            this.listBox30.TabIndex = 35;
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Location = new System.Drawing.Point(322, 113);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(41, 12);
            this.label68.TabIndex = 34;
            this.label68.Text = "修改至";
            // 
            // listBox31
            // 
            this.listBox31.DisplayMember = "Material_Name";
            this.listBox31.FormattingEnabled = true;
            this.listBox31.ItemHeight = 12;
            this.listBox31.Location = new System.Drawing.Point(131, 113);
            this.listBox31.Name = "listBox31";
            this.listBox31.Size = new System.Drawing.Size(120, 16);
            this.listBox31.TabIndex = 33;
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Location = new System.Drawing.Point(17, 113);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(41, 12);
            this.label69.TabIndex = 32;
            this.label69.Text = "权限组";
            // 
            // listBox32
            // 
            this.listBox32.DisplayMember = "Material_Name";
            this.listBox32.FormattingEnabled = true;
            this.listBox32.ItemHeight = 12;
            this.listBox32.Location = new System.Drawing.Point(436, 68);
            this.listBox32.Name = "listBox32";
            this.listBox32.Size = new System.Drawing.Size(120, 16);
            this.listBox32.TabIndex = 31;
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Location = new System.Drawing.Point(322, 68);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(41, 12);
            this.label70.TabIndex = 30;
            this.label70.Text = "修改至";
            // 
            // listBox33
            // 
            this.listBox33.DisplayMember = "Material_Name";
            this.listBox33.FormattingEnabled = true;
            this.listBox33.ItemHeight = 12;
            this.listBox33.Location = new System.Drawing.Point(131, 68);
            this.listBox33.Name = "listBox33";
            this.listBox33.Size = new System.Drawing.Size(120, 16);
            this.listBox33.TabIndex = 29;
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Location = new System.Drawing.Point(17, 68);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(53, 12);
            this.label71.TabIndex = 28;
            this.label71.Text = "物资级别";
            // 
            // listBox34
            // 
            this.listBox34.DisplayMember = "Material_Name";
            this.listBox34.FormattingEnabled = true;
            this.listBox34.ItemHeight = 12;
            this.listBox34.Location = new System.Drawing.Point(436, 28);
            this.listBox34.Name = "listBox34";
            this.listBox34.Size = new System.Drawing.Size(120, 16);
            this.listBox34.TabIndex = 27;
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Location = new System.Drawing.Point(322, 28);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(41, 12);
            this.label72.TabIndex = 26;
            this.label72.Text = "修改至";
            // 
            // listBox35
            // 
            this.listBox35.DisplayMember = "Material_Name";
            this.listBox35.FormattingEnabled = true;
            this.listBox35.ItemHeight = 12;
            this.listBox35.Location = new System.Drawing.Point(131, 28);
            this.listBox35.Name = "listBox35";
            this.listBox35.Size = new System.Drawing.Size(120, 16);
            this.listBox35.TabIndex = 25;
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Location = new System.Drawing.Point(17, 28);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(53, 12);
            this.label73.TabIndex = 24;
            this.label73.Text = "物资类别";
            // 
            // button19
            // 
            this.button19.Location = new System.Drawing.Point(382, 149);
            this.button19.Name = "button19";
            this.button19.Size = new System.Drawing.Size(75, 23);
            this.button19.TabIndex = 23;
            this.button19.Text = "重置";
            this.button19.UseVisualStyleBackColor = true;
            // 
            // button20
            // 
            this.button20.Location = new System.Drawing.Point(165, 149);
            this.button20.Name = "button20";
            this.button20.Size = new System.Drawing.Size(75, 23);
            this.button20.TabIndex = 22;
            this.button20.Text = "确定";
            this.button20.UseVisualStyleBackColor = true;
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.dataGridView6);
            this.groupBox10.Location = new System.Drawing.Point(5, 234);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(636, 204);
            this.groupBox10.TabIndex = 4;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "查询结果";
            // 
            // dataGridView6
            // 
            this.dataGridView6.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView6.Location = new System.Drawing.Point(7, 20);
            this.dataGridView6.Name = "dataGridView6";
            this.dataGridView6.RowTemplate.Height = 23;
            this.dataGridView6.Size = new System.Drawing.Size(623, 161);
            this.dataGridView6.TabIndex = 0;
            // 
            // panel14
            // 
            this.panel14.Controls.Add(this.groupBox9);
            this.panel14.Location = new System.Drawing.Point(3, 3);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(906, 225);
            this.panel14.TabIndex = 3;
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.textBox25);
            this.groupBox9.Controls.Add(this.label60);
            this.groupBox9.Controls.Add(this.button17);
            this.groupBox9.Controls.Add(this.button18);
            this.groupBox9.Controls.Add(this.listBox23);
            this.groupBox9.Controls.Add(this.label61);
            this.groupBox9.Controls.Add(this.listBox24);
            this.groupBox9.Controls.Add(this.label62);
            this.groupBox9.Controls.Add(this.listBox25);
            this.groupBox9.Controls.Add(this.label63);
            this.groupBox9.Controls.Add(this.listBox26);
            this.groupBox9.Controls.Add(this.label64);
            this.groupBox9.Controls.Add(this.listBox27);
            this.groupBox9.Controls.Add(this.label65);
            this.groupBox9.Controls.Add(this.listBox28);
            this.groupBox9.Controls.Add(this.label66);
            this.groupBox9.Controls.Add(this.listBox29);
            this.groupBox9.Controls.Add(this.label67);
            this.groupBox9.Location = new System.Drawing.Point(7, 3);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(631, 219);
            this.groupBox9.TabIndex = 18;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "条件选择";
            // 
            // textBox25
            // 
            this.textBox25.Location = new System.Drawing.Point(459, 170);
            this.textBox25.Name = "textBox25";
            this.textBox25.Size = new System.Drawing.Size(117, 21);
            this.textBox25.TabIndex = 35;
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Location = new System.Drawing.Point(344, 170);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(65, 12);
            this.label60.TabIndex = 34;
            this.label60.Text = "最大命中数";
            // 
            // button17
            // 
            this.button17.Location = new System.Drawing.Point(231, 170);
            this.button17.Name = "button17";
            this.button17.Size = new System.Drawing.Size(75, 23);
            this.button17.TabIndex = 33;
            this.button17.Text = "清除";
            this.button17.UseVisualStyleBackColor = true;
            // 
            // button18
            // 
            this.button18.Location = new System.Drawing.Point(96, 170);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(75, 23);
            this.button18.TabIndex = 32;
            this.button18.Text = "查询";
            this.button18.UseVisualStyleBackColor = true;
            // 
            // listBox23
            // 
            this.listBox23.FormattingEnabled = true;
            this.listBox23.ItemHeight = 12;
            this.listBox23.Location = new System.Drawing.Point(154, 148);
            this.listBox23.Name = "listBox23";
            this.listBox23.Size = new System.Drawing.Size(120, 16);
            this.listBox23.TabIndex = 31;
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Location = new System.Drawing.Point(40, 148);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(29, 12);
            this.label61.TabIndex = 30;
            this.label61.Text = "其他";
            // 
            // listBox24
            // 
            this.listBox24.FormattingEnabled = true;
            this.listBox24.ItemHeight = 12;
            this.listBox24.Location = new System.Drawing.Point(459, 105);
            this.listBox24.Name = "listBox24";
            this.listBox24.Size = new System.Drawing.Size(120, 16);
            this.listBox24.TabIndex = 29;
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Location = new System.Drawing.Point(345, 105);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(17, 12);
            this.label62.TabIndex = 28;
            this.label62.Text = "至";
            // 
            // listBox25
            // 
            this.listBox25.FormattingEnabled = true;
            this.listBox25.ItemHeight = 12;
            this.listBox25.Location = new System.Drawing.Point(154, 105);
            this.listBox25.Name = "listBox25";
            this.listBox25.Size = new System.Drawing.Size(120, 16);
            this.listBox25.TabIndex = 27;
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Location = new System.Drawing.Point(40, 105);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(41, 12);
            this.label63.TabIndex = 26;
            this.label63.Text = "权限组";
            // 
            // listBox26
            // 
            this.listBox26.FormattingEnabled = true;
            this.listBox26.ItemHeight = 12;
            this.listBox26.Location = new System.Drawing.Point(459, 60);
            this.listBox26.Name = "listBox26";
            this.listBox26.Size = new System.Drawing.Size(120, 16);
            this.listBox26.TabIndex = 25;
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Location = new System.Drawing.Point(345, 60);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(17, 12);
            this.label64.TabIndex = 24;
            this.label64.Text = "至";
            // 
            // listBox27
            // 
            this.listBox27.FormattingEnabled = true;
            this.listBox27.ItemHeight = 12;
            this.listBox27.Location = new System.Drawing.Point(154, 60);
            this.listBox27.Name = "listBox27";
            this.listBox27.Size = new System.Drawing.Size(120, 16);
            this.listBox27.TabIndex = 23;
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Location = new System.Drawing.Point(40, 60);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(53, 12);
            this.label65.TabIndex = 22;
            this.label65.Text = "物资级别";
            // 
            // listBox28
            // 
            this.listBox28.FormattingEnabled = true;
            this.listBox28.ItemHeight = 12;
            this.listBox28.Location = new System.Drawing.Point(459, 20);
            this.listBox28.Name = "listBox28";
            this.listBox28.Size = new System.Drawing.Size(120, 16);
            this.listBox28.TabIndex = 21;
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Location = new System.Drawing.Point(345, 20);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(17, 12);
            this.label66.TabIndex = 20;
            this.label66.Text = "至";
            // 
            // listBox29
            // 
            this.listBox29.FormattingEnabled = true;
            this.listBox29.ItemHeight = 12;
            this.listBox29.Location = new System.Drawing.Point(154, 20);
            this.listBox29.Name = "listBox29";
            this.listBox29.Size = new System.Drawing.Size(120, 16);
            this.listBox29.TabIndex = 19;
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Location = new System.Drawing.Point(40, 20);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(53, 12);
            this.label67.TabIndex = 18;
            this.label67.Text = "物资类别";
            // 
            // tabPage11
            // 
            this.tabPage11.Controls.Add(this.panel16);
            this.tabPage11.Location = new System.Drawing.Point(4, 22);
            this.tabPage11.Name = "tabPage11";
            this.tabPage11.Size = new System.Drawing.Size(909, 645);
            this.tabPage11.TabIndex = 6;
            this.tabPage11.Text = "工程型变更";
            this.tabPage11.UseVisualStyleBackColor = true;
            // 
            // panel16
            // 
            this.panel16.Controls.Add(this.groupBox12);
            this.panel16.Location = new System.Drawing.Point(4, 4);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(634, 339);
            this.panel16.TabIndex = 0;
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this.button24);
            this.groupBox12.Controls.Add(this.button23);
            this.groupBox12.Controls.Add(this.button22);
            this.groupBox12.Controls.Add(this.button21);
            this.groupBox12.Controls.Add(this.listBox44);
            this.groupBox12.Controls.Add(this.label82);
            this.groupBox12.Controls.Add(this.listBox45);
            this.groupBox12.Controls.Add(this.label83);
            this.groupBox12.Controls.Add(this.listBox42);
            this.groupBox12.Controls.Add(this.label74);
            this.groupBox12.Controls.Add(this.listBox43);
            this.groupBox12.Controls.Add(this.label81);
            this.groupBox12.Controls.Add(this.listBox36);
            this.groupBox12.Controls.Add(this.label75);
            this.groupBox12.Controls.Add(this.listBox37);
            this.groupBox12.Controls.Add(this.label76);
            this.groupBox12.Controls.Add(this.listBox38);
            this.groupBox12.Controls.Add(this.label77);
            this.groupBox12.Controls.Add(this.listBox39);
            this.groupBox12.Controls.Add(this.label78);
            this.groupBox12.Controls.Add(this.listBox40);
            this.groupBox12.Controls.Add(this.label79);
            this.groupBox12.Controls.Add(this.listBox41);
            this.groupBox12.Controls.Add(this.label80);
            this.groupBox12.Location = new System.Drawing.Point(4, 4);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(627, 332);
            this.groupBox12.TabIndex = 0;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "工程型变更";
            // 
            // button24
            // 
            this.button24.Location = new System.Drawing.Point(479, 158);
            this.button24.Name = "button24";
            this.button24.Size = new System.Drawing.Size(75, 23);
            this.button24.TabIndex = 53;
            this.button24.Text = "更新维度表";
            this.button24.UseVisualStyleBackColor = true;
            // 
            // button23
            // 
            this.button23.Location = new System.Drawing.Point(479, 111);
            this.button23.Name = "button23";
            this.button23.Size = new System.Drawing.Size(75, 23);
            this.button23.TabIndex = 52;
            this.button23.Text = "更新维度表";
            this.button23.UseVisualStyleBackColor = true;
            // 
            // button22
            // 
            this.button22.Location = new System.Drawing.Point(479, 66);
            this.button22.Name = "button22";
            this.button22.Size = new System.Drawing.Size(75, 23);
            this.button22.TabIndex = 51;
            this.button22.Text = "更新维度表";
            this.button22.UseVisualStyleBackColor = true;
            // 
            // button21
            // 
            this.button21.Location = new System.Drawing.Point(479, 26);
            this.button21.Name = "button21";
            this.button21.Size = new System.Drawing.Size(75, 23);
            this.button21.TabIndex = 50;
            this.button21.Text = "更新维度表";
            this.button21.UseVisualStyleBackColor = true;
            // 
            // listBox44
            // 
            this.listBox44.FormattingEnabled = true;
            this.listBox44.ItemHeight = 12;
            this.listBox44.Location = new System.Drawing.Point(324, 217);
            this.listBox44.Name = "listBox44";
            this.listBox44.Size = new System.Drawing.Size(120, 16);
            this.listBox44.TabIndex = 49;
            // 
            // label82
            // 
            this.label82.AutoSize = true;
            this.label82.Location = new System.Drawing.Point(280, 217);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(17, 12);
            this.label82.TabIndex = 48;
            this.label82.Text = "至";
            // 
            // listBox45
            // 
            this.listBox45.FormattingEnabled = true;
            this.listBox45.ItemHeight = 12;
            this.listBox45.Location = new System.Drawing.Point(131, 217);
            this.listBox45.Name = "listBox45";
            this.listBox45.Size = new System.Drawing.Size(120, 16);
            this.listBox45.TabIndex = 47;
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.Location = new System.Drawing.Point(17, 217);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(53, 12);
            this.label83.TabIndex = 46;
            this.label83.Text = "环境变更";
            // 
            // listBox42
            // 
            this.listBox42.FormattingEnabled = true;
            this.listBox42.ItemHeight = 12;
            this.listBox42.Location = new System.Drawing.Point(324, 165);
            this.listBox42.Name = "listBox42";
            this.listBox42.Size = new System.Drawing.Size(120, 16);
            this.listBox42.TabIndex = 45;
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Location = new System.Drawing.Point(280, 165);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(17, 12);
            this.label74.TabIndex = 44;
            this.label74.Text = "至";
            // 
            // listBox43
            // 
            this.listBox43.FormattingEnabled = true;
            this.listBox43.ItemHeight = 12;
            this.listBox43.Location = new System.Drawing.Point(131, 165);
            this.listBox43.Name = "listBox43";
            this.listBox43.Size = new System.Drawing.Size(120, 16);
            this.listBox43.TabIndex = 43;
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.Location = new System.Drawing.Point(17, 165);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(53, 12);
            this.label81.TabIndex = 42;
            this.label81.Text = "材料变更";
            // 
            // listBox36
            // 
            this.listBox36.FormattingEnabled = true;
            this.listBox36.ItemHeight = 12;
            this.listBox36.Location = new System.Drawing.Point(324, 111);
            this.listBox36.Name = "listBox36";
            this.listBox36.Size = new System.Drawing.Size(120, 16);
            this.listBox36.TabIndex = 41;
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Location = new System.Drawing.Point(280, 111);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(17, 12);
            this.label75.TabIndex = 40;
            this.label75.Text = "至";
            // 
            // listBox37
            // 
            this.listBox37.FormattingEnabled = true;
            this.listBox37.ItemHeight = 12;
            this.listBox37.Location = new System.Drawing.Point(131, 111);
            this.listBox37.Name = "listBox37";
            this.listBox37.Size = new System.Drawing.Size(120, 16);
            this.listBox37.TabIndex = 39;
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Location = new System.Drawing.Point(17, 111);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(53, 12);
            this.label76.TabIndex = 38;
            this.label76.Text = "机器变更";
            // 
            // listBox38
            // 
            this.listBox38.FormattingEnabled = true;
            this.listBox38.ItemHeight = 12;
            this.listBox38.Location = new System.Drawing.Point(324, 66);
            this.listBox38.Name = "listBox38";
            this.listBox38.Size = new System.Drawing.Size(120, 16);
            this.listBox38.TabIndex = 37;
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Location = new System.Drawing.Point(280, 66);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(17, 12);
            this.label77.TabIndex = 36;
            this.label77.Text = "至";
            // 
            // listBox39
            // 
            this.listBox39.FormattingEnabled = true;
            this.listBox39.ItemHeight = 12;
            this.listBox39.Location = new System.Drawing.Point(131, 66);
            this.listBox39.Name = "listBox39";
            this.listBox39.Size = new System.Drawing.Size(120, 16);
            this.listBox39.TabIndex = 35;
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Location = new System.Drawing.Point(17, 66);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(53, 12);
            this.label78.TabIndex = 34;
            this.label78.Text = "人员变更";
            // 
            // listBox40
            // 
            this.listBox40.FormattingEnabled = true;
            this.listBox40.ItemHeight = 12;
            this.listBox40.Location = new System.Drawing.Point(324, 26);
            this.listBox40.Name = "listBox40";
            this.listBox40.Size = new System.Drawing.Size(120, 16);
            this.listBox40.TabIndex = 33;
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Location = new System.Drawing.Point(280, 26);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(17, 12);
            this.label79.TabIndex = 32;
            this.label79.Text = "至";
            // 
            // listBox41
            // 
            this.listBox41.FormattingEnabled = true;
            this.listBox41.ItemHeight = 12;
            this.listBox41.Location = new System.Drawing.Point(131, 26);
            this.listBox41.Name = "listBox41";
            this.listBox41.Size = new System.Drawing.Size(120, 16);
            this.listBox41.TabIndex = 31;
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Location = new System.Drawing.Point(17, 26);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(53, 12);
            this.label80.TabIndex = 30;
            this.label80.Text = "生产基地";
            // 
            // tabPage28
            // 
            this.tabPage28.Controls.Add(this.panel9);
            this.tabPage28.Controls.Add(this.panel8);
            this.tabPage28.Controls.Add(this.panel7);
            this.tabPage28.Location = new System.Drawing.Point(4, 22);
            this.tabPage28.Name = "tabPage28";
            this.tabPage28.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage28.Size = new System.Drawing.Size(909, 645);
            this.tabPage28.TabIndex = 7;
            this.tabPage28.Text = "分发";
            this.tabPage28.UseVisualStyleBackColor = true;
            // 
            // panel9
            // 
            this.panel9.Controls.Add(this.groupBox8);
            this.panel9.Location = new System.Drawing.Point(7, 476);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(645, 172);
            this.panel9.TabIndex = 5;
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.dataGridView4);
            this.groupBox8.Location = new System.Drawing.Point(3, 3);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(634, 163);
            this.groupBox8.TabIndex = 0;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "分发的子系统或者单位";
            // 
            // dataGridView4
            // 
            this.dataGridView4.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView4.Location = new System.Drawing.Point(0, 12);
            this.dataGridView4.Name = "dataGridView4";
            this.dataGridView4.RowTemplate.Height = 23;
            this.dataGridView4.Size = new System.Drawing.Size(628, 150);
            this.dataGridView4.TabIndex = 0;
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.groupBox7);
            this.panel8.Location = new System.Drawing.Point(7, 235);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(645, 235);
            this.panel8.TabIndex = 4;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.dataGridView3);
            this.groupBox7.Location = new System.Drawing.Point(6, 4);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(636, 231);
            this.groupBox7.TabIndex = 0;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "查询结果";
            // 
            // dataGridView3
            // 
            this.dataGridView3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView3.Location = new System.Drawing.Point(7, 21);
            this.dataGridView3.Name = "dataGridView3";
            this.dataGridView3.RowTemplate.Height = 23;
            this.dataGridView3.Size = new System.Drawing.Size(623, 204);
            this.dataGridView3.TabIndex = 0;
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.groupBox6);
            this.panel7.Location = new System.Drawing.Point(6, 6);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(646, 225);
            this.panel7.TabIndex = 3;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.textBox12);
            this.groupBox6.Controls.Add(this.label2);
            this.groupBox6.Controls.Add(this.button10);
            this.groupBox6.Controls.Add(this.button11);
            this.groupBox6.Controls.Add(this.listBox14);
            this.groupBox6.Controls.Add(this.label27);
            this.groupBox6.Controls.Add(this.listBox15);
            this.groupBox6.Controls.Add(this.label28);
            this.groupBox6.Controls.Add(this.listBox16);
            this.groupBox6.Controls.Add(this.label29);
            this.groupBox6.Controls.Add(this.listBox17);
            this.groupBox6.Controls.Add(this.label30);
            this.groupBox6.Controls.Add(this.listBox18);
            this.groupBox6.Controls.Add(this.label31);
            this.groupBox6.Controls.Add(this.listBox19);
            this.groupBox6.Controls.Add(this.label32);
            this.groupBox6.Controls.Add(this.listBox20);
            this.groupBox6.Controls.Add(this.label33);
            this.groupBox6.Location = new System.Drawing.Point(7, 3);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(631, 219);
            this.groupBox6.TabIndex = 18;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "条件选择";
            // 
            // textBox12
            // 
            this.textBox12.Location = new System.Drawing.Point(459, 170);
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new System.Drawing.Size(117, 21);
            this.textBox12.TabIndex = 35;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(344, 170);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 12);
            this.label2.TabIndex = 34;
            this.label2.Text = "最大命中数";
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(231, 170);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(75, 23);
            this.button10.TabIndex = 33;
            this.button10.Text = "清除";
            this.button10.UseVisualStyleBackColor = true;
            // 
            // button11
            // 
            this.button11.Location = new System.Drawing.Point(96, 170);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(75, 23);
            this.button11.TabIndex = 32;
            this.button11.Text = "查询";
            this.button11.UseVisualStyleBackColor = true;
            // 
            // listBox14
            // 
            this.listBox14.FormattingEnabled = true;
            this.listBox14.ItemHeight = 12;
            this.listBox14.Location = new System.Drawing.Point(154, 148);
            this.listBox14.Name = "listBox14";
            this.listBox14.Size = new System.Drawing.Size(120, 16);
            this.listBox14.TabIndex = 31;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(40, 148);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(29, 12);
            this.label27.TabIndex = 30;
            this.label27.Text = "其他";
            // 
            // listBox15
            // 
            this.listBox15.FormattingEnabled = true;
            this.listBox15.ItemHeight = 12;
            this.listBox15.Location = new System.Drawing.Point(459, 105);
            this.listBox15.Name = "listBox15";
            this.listBox15.Size = new System.Drawing.Size(120, 16);
            this.listBox15.TabIndex = 29;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(345, 105);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(17, 12);
            this.label28.TabIndex = 28;
            this.label28.Text = "至";
            // 
            // listBox16
            // 
            this.listBox16.FormattingEnabled = true;
            this.listBox16.ItemHeight = 12;
            this.listBox16.Location = new System.Drawing.Point(154, 105);
            this.listBox16.Name = "listBox16";
            this.listBox16.Size = new System.Drawing.Size(120, 16);
            this.listBox16.TabIndex = 27;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(40, 105);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(41, 12);
            this.label29.TabIndex = 26;
            this.label29.Text = "权限组";
            // 
            // listBox17
            // 
            this.listBox17.FormattingEnabled = true;
            this.listBox17.ItemHeight = 12;
            this.listBox17.Location = new System.Drawing.Point(459, 60);
            this.listBox17.Name = "listBox17";
            this.listBox17.Size = new System.Drawing.Size(120, 16);
            this.listBox17.TabIndex = 25;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(345, 60);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(17, 12);
            this.label30.TabIndex = 24;
            this.label30.Text = "至";
            // 
            // listBox18
            // 
            this.listBox18.DisplayMember = "Material_Name";
            this.listBox18.FormattingEnabled = true;
            this.listBox18.ItemHeight = 12;
            this.listBox18.Location = new System.Drawing.Point(154, 60);
            this.listBox18.Name = "listBox18";
            this.listBox18.Size = new System.Drawing.Size(120, 16);
            this.listBox18.TabIndex = 23;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(40, 60);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(53, 12);
            this.label31.TabIndex = 22;
            this.label31.Text = "物资级别";
            // 
            // listBox19
            // 
            this.listBox19.FormattingEnabled = true;
            this.listBox19.ItemHeight = 12;
            this.listBox19.Location = new System.Drawing.Point(459, 20);
            this.listBox19.Name = "listBox19";
            this.listBox19.Size = new System.Drawing.Size(120, 16);
            this.listBox19.TabIndex = 21;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(345, 20);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(17, 12);
            this.label32.TabIndex = 20;
            this.label32.Text = "至";
            // 
            // listBox20
            // 
            this.listBox20.FormattingEnabled = true;
            this.listBox20.ItemHeight = 12;
            this.listBox20.Location = new System.Drawing.Point(154, 20);
            this.listBox20.Name = "listBox20";
            this.listBox20.Size = new System.Drawing.Size(120, 16);
            this.listBox20.TabIndex = 19;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(40, 20);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(53, 12);
            this.label33.TabIndex = 18;
            this.label33.Text = "物资类别";
            // 
            // MaterialSelect
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1077, 713);
            this.Controls.Add(this.tabControl2);
            this.Name = "MaterialSelect";
            this.Text = "MaterialSelect";
            this.tabControl2.ResumeLayout(false);
            this.tabPage5.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            this.panel3.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.tabControl6.ResumeLayout(false);
            this.tabPage26.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.tabPage6.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.tabPage7.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.tabPage9.ResumeLayout(false);
            this.tabControl7.ResumeLayout(false);
            this.tabPage29.ResumeLayout(false);
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            this.tabPage30.ResumeLayout(false);
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView5)).EndInit();
            this.tabPage31.ResumeLayout(false);
            this.panel12.ResumeLayout(false);
            this.panel12.PerformLayout();
            this.tabPage32.ResumeLayout(false);
            this.panel13.ResumeLayout(false);
            this.panel13.PerformLayout();
            this.tabPage10.ResumeLayout(false);
            this.panel15.ResumeLayout(false);
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView6)).EndInit();
            this.panel14.ResumeLayout(false);
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.tabPage11.ResumeLayout(false);
            this.panel16.ResumeLayout(false);
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            this.tabPage28.ResumeLayout(false);
            this.panel9.ResumeLayout(false);
            this.groupBox8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView4)).EndInit();
            this.panel8.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).EndInit();
            this.panel7.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl2;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TabControl tabControl6;
        private System.Windows.Forms.TabPage tabPage26;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.TabPage tabPage27;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ListBox listBox7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ListBox listBox6;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ListBox listBox5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ListBox listBox4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ListBox listBox3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ListBox listBox2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.TextBox textBox11;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TabPage tabPage7;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.ListBox listBox8;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.ListBox listBox9;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.ListBox listBox10;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.ListBox listBox11;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.ListBox listBox12;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.ListBox listBox13;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.TabPage tabPage28;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.DataGridView dataGridView4;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.DataGridView dataGridView3;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.TextBox textBox12;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.ListBox listBox14;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.ListBox listBox15;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.ListBox listBox16;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.ListBox listBox17;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.ListBox listBox18;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.ListBox listBox19;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.ListBox listBox20;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TabPage tabPage8;
        private System.Windows.Forms.TabPage tabPage9;
        private System.Windows.Forms.TabControl tabControl7;
        private System.Windows.Forms.TabPage tabPage29;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.DateTimePicker dateTimePicker3;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.TextBox textBox15;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.TextBox textBox14;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.TextBox textBox13;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TabPage tabPage30;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.DataGridView dataGridView5;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.DateTimePicker dateTimePicker4;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.DateTimePicker dateTimePicker5;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.DateTimePicker dateTimePicker6;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.TextBox textBox16;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.TextBox textBox17;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.TextBox textBox18;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.TabPage tabPage31;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.DateTimePicker dateTimePicker12;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.DateTimePicker dateTimePicker11;
        private System.Windows.Forms.DateTimePicker dateTimePicker10;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.TextBox textBox24;
        private System.Windows.Forms.TextBox textBox23;
        private System.Windows.Forms.TextBox textBox22;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.DateTimePicker dateTimePicker7;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.DateTimePicker dateTimePicker8;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.DateTimePicker dateTimePicker9;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.TextBox textBox19;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.TextBox textBox20;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.TextBox textBox21;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.TabPage tabPage32;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Button button16;
        private System.Windows.Forms.ListBox listBox22;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.ListBox listBox21;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.TabPage tabPage10;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.ListBox listBox30;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.ListBox listBox31;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.ListBox listBox32;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.ListBox listBox33;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.ListBox listBox34;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.ListBox listBox35;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.Button button19;
        private System.Windows.Forms.Button button20;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.DataGridView dataGridView6;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.TextBox textBox25;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Button button17;
        private System.Windows.Forms.Button button18;
        private System.Windows.Forms.ListBox listBox23;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.ListBox listBox24;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.ListBox listBox25;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.ListBox listBox26;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.ListBox listBox27;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.ListBox listBox28;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.ListBox listBox29;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.TabPage tabPage11;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.Button button24;
        private System.Windows.Forms.Button button23;
        private System.Windows.Forms.Button button22;
        private System.Windows.Forms.Button button21;
        private System.Windows.Forms.ListBox listBox44;
        private System.Windows.Forms.Label label82;
        private System.Windows.Forms.ListBox listBox45;
        private System.Windows.Forms.Label label83;
        private System.Windows.Forms.ListBox listBox42;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.ListBox listBox43;
        private System.Windows.Forms.Label label81;
        private System.Windows.Forms.ListBox listBox36;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.ListBox listBox37;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.ListBox listBox38;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.ListBox listBox39;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.ListBox listBox40;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.ListBox listBox41;
        private System.Windows.Forms.Label label80;

    }
}