﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;
using Lib.Bll.FileManageBLL;
using Lib.Model.FileManage;
using Lib.Common.CommonUtils;

namespace MMClient.FileManage
{
    public partial class NewFileTypeForm : DockContent
    {
        private FileTypeBLL fileTypeTool = new FileTypeBLL();               //文件类型管理工具
        List<string> fileTypeNameList = null;                               //FileTypeName链表
        
        public NewFileTypeForm()
        {
            InitializeComponent();
            refreshCbxList();
        }

        /// <summary>
        /// 初始化Form
        /// </summary>
        private void refreshCbxList() {
            //cbxFileType添加数据源
            fileTypeNameList = fileTypeTool.getAllFileTypeName();
            this.cbxFileType.DataSource = fileTypeNameList;
            this.cbxFileType.SelectedIndex = -1;
            clearForm();
        }

        /// <summary>
        /// 选择项发生变化
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cbxFileType_SelectedIndexChanged(object sender, EventArgs e)
        {
            //已经存在的信息
            if (this.cbxFileType.SelectedIndex > -1)
            {
                string fileTypeName = this.cbxFileType.Text;
                File_Type fileType = fileTypeTool.getFileType(fileTypeName);
                fillForm(fileType);
            }
        }

        /// <summary>
        /// 填充Form
        /// </summary>
        /// <param name="fileType"></param>
        private void fillForm(File_Type fileType) {
            this.rtbDetail.Text = fileType.Description;
            this.dtpCreateTime.Value = fileType.Create_Time;
        }

        /// <summary>
        /// 情况Form
        /// </summary>
        private void clearForm() {
            this.rtbDetail.Text = "";
            this.dtpCreateTime.Value = System.DateTime.Now;
        }

        /// <summary>
        /// 点击删除
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDelete_Click(object sender, EventArgs e)
        {
            string fileTypeName = this.cbxFileType.Text;
            int result = fileTypeTool.deleteFileType(fileTypeName);
            if (result > 0)
                MessageBox.Show("删除成功!");
            refreshCbxList();
        }

        /// <summary>
        /// 保存
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSave_Click(object sender, EventArgs e)
        {
            string fileTypeName = this.cbxFileType.Text;
            File_Type fileType = fileTypeTool.getFileType(fileTypeName);
            int num = 0;
            //新的
            if (fileType == null)
            {
                fileType = new File_Type();
                fileType.Create_Time = System.DateTime.Now;
                fileType.Creator_ID = "2015001";
                fileType.Description = this.rtbDetail.Text.Trim();
                fileType.File_Type_Name = fileTypeName;

                num = fileTypeTool.addFileType(fileType);
            }
            else {
                fileType.Description = this.rtbDetail.Text.Trim();

                num = fileTypeTool.updateFileType(fileType);
            }
            if (num > 0)
                MessageBox.Show("保存成功！");
            else
                MessageBox.Show("保存失败！");

            refreshCbxList();
        }
    }
}
