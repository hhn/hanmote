﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;
using Lib.Model.FileManage;
using System.IO;
using Lib.Bll.FileManageBLL;
using Lib.Common.CommonUtils;
using MMClient.CommonForms;
using MMClient.CommFileShow;

namespace MMClient.FileManage
{
    public partial class NewFileForm : DockContent
    {
        private string opType = "";         //操作类型
        private File_Info fileInfo;         //文件信息
        private FileTypeBLL fileTypeTool = new FileTypeBLL();   //文件类型操作工具
        private FileBLL fileTool = new FileBLL();               //文件操作工具
        private List<File_Attachment_Info> fileAttachmentList =
            new List<File_Attachment_Info>();       //文件附件列表(和dgvAttachmentInfo保存同步)

        public NewFileForm()
        {
            InitializeComponent();
            opType = "new";
            initialForm();
        }

        /// <summary>
        /// 有参构造函数
        /// </summary>
        /// <param name="_fileInfo">文件信息</param>
        /// <param name="_opType">操作类型</param>
        public NewFileForm(File_Info _fileInfo, string _opType)
        {
            InitializeComponent();
            this.fileInfo = _fileInfo;
            this.opType = _opType;
            initialForm();
        }

        /// <summary>
        /// 初始化Form
        /// </summary>
        private void initialForm()
        {
            //文件类型ComboBox
            List<string> fileTypeList = fileTypeTool.getAllFileTypeName();
            this.cbxFileType.DataSource = fileTypeList;
            //供应商可见范围
            this.cbxSupplierVisibleLevel.SelectedIndex = 0;

            if (opType.Equals("new"))
            {
                string defaultID = System.DateTime.Now.ToString("yyyyMMddHHmmss")
                    + "2016001";
                this.tbFileID.Text = defaultID;
            }
            else
            {
                //读取文件抬头信息
                this.tbFileID.Text = fileInfo.File_ID;
                this.rtbDetail.Text = fileInfo.Description;
                this.dtpCreateTime.Value = fileInfo.Create_Time;
                //读取附件信息
                fileAttachmentList = fileTool.getFileAttachmentList(fileInfo.File_ID);
                displayFileAttachmentList(fileAttachmentList);

                //界面标题
                if (opType.Equals("display"))
                    this.Text = "展示文档";
                else if (opType.Equals("edit"))
                    this.Text = "编辑文档";
            }

            disableControls(opType);
        }

        /// <summary>
        /// 禁止相应控件
        /// </summary>
        /// <param name="opType"></param>
        private void disableControls(string opType)
        {
            if (opType.Equals("new"))
            {
                this.btnDownload.Enabled = false;
            }
            else if (opType.Equals("display"))
            {
                this.tbFileID.Enabled = false;
                this.cbxFileType.Enabled = false;
                this.cbxSupplierVisibleLevel.Enabled = false;
                this.rtbDetail.Enabled = false;
                this.btnAdd.Enabled = false;
                this.btnDelete.Enabled = false;
                this.btnSave.Enabled = false;
                this.btnDownload.Enabled = false;
                this.dtpCreateTime.Enabled = false;
                //移除用户权限页
                this.tabControl1.TabPages.Remove(this.tpUserInfo);
            }
            else if (opType.Equals("edit")) {
                this.tbFileID.Enabled = false;
                this.cbxFileType.Enabled = false;
                this.dtpCreateTime.Enabled = false;
            }
        }

        /// <summary>
        /// 展示附件信息
        /// </summary>
        /// <param name="fileAttachmentList"></param>
        private void displayFileAttachmentList(List<File_Attachment_Info> fileAttachmentList)
        {
            this.dgvAttachmentInfo.Rows.Clear();

            foreach (File_Attachment_Info fileAttachment in fileAttachmentList)
            {
                this.dgvAttachmentInfo.Rows.Add();
                DataGridViewRow row = this.dgvAttachmentInfo.Rows[this.dgvAttachmentInfo.Rows.Count - 1];
                row.Cells[1].Value = fileAttachment.Attachment_Name;
                row.Cells[3].Value = StringUtil.
                    convertFileSizeToString(fileAttachment.Attachment_Size);
            }
        }

        /// <summary>
        /// 点击增加
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAdd_Click(object sender, EventArgs e)
        {
            //添加文件
            if (this.tabControl1.SelectedTab == this.tpAttachmentInfo)
            {
                OpenFileDialog fileDialog = new OpenFileDialog();
                fileDialog.Title = "请选择文件";
                var result = fileDialog.ShowDialog();
                if (result == DialogResult.Cancel)
                    return;
                string fileName = fileDialog.FileName;
                if (fileName.Equals(String.Empty))
                    return;
                //防止文件名重复
                string safeFileName = fileDialog.SafeFileName;
                if (safeFileName.Length > 150) {
                    MessageUtil.ShowError("文件名过长!");
                    return;
                }
                foreach (File_Attachment_Info tmp in fileAttachmentList)
                {
                    if (tmp.Attachment_Name.Equals(safeFileName))
                    {
                        MessageUtil.ShowError("同名文件已存在！");
                        return;
                    }
                }

                //加入到DataGridView中
                this.dgvAttachmentInfo.Rows.Add();
                int rowCount = this.dgvAttachmentInfo.Rows.Count;
                DataGridViewRow row = this.dgvAttachmentInfo.Rows[rowCount - 1];
                row.Cells[1].Value = safeFileName;
                row.Cells[2].Value = fileName;
                var size = new FileInfo(fileName).Length;
                row.Cells[3].Value = StringUtil.convertFileSizeToString((double)size);
                //加入到fileAttachmentList中(这里不记录文件信息，暂时记录附件信息)
                File_Attachment_Info fileAttachment = new File_Attachment_Info();
                fileAttachment.Attachment_Name = safeFileName;
                //服务器端为"", 表示未上传到服务器
                fileAttachment.Attachment_Location = "";
                fileAttachment.Attachment_Local_Location = fileName;
                fileAttachment.Attachment_Download_Times = 0;
                fileAttachment.Attachment_Size = size;

                fileAttachmentList.Add(fileAttachment);
            }
            else if (this.tabControl1.SelectedTab == this.tpUserInfo)
            {
                //添加用户权限
                this.dgvUserInfo.Rows.Add();

            }
        }

        /// <summary>
        /// 删除一行
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDelete_Click(object sender, EventArgs e)
        {
            //删除文件附件项
            if (this.tabControl1.SelectedTab == this.tpAttachmentInfo) { 
                //查看勾选的项
                List<DataGridViewRow> selectedRows = new List<DataGridViewRow>();
                foreach (DataGridViewRow row in this.dgvAttachmentInfo.Rows) { 
                    string isSelected = row.Cells[0].EditedFormattedValue.ToString();
                    if (isSelected.Equals("True")) {
                        selectedRows.Add(row);
                    }
                }
                if (selectedRows.Count == 0)
                {
                    MessageUtil.ShowError("请选择删除项!");
                    return;
                }
                DialogResult result = MessageUtil.ShowYesNoAndWarning("确定删除文档中的附件吗？\n(可能包括已上传的附件)");
                if (result == DialogResult.No)
                    return;

                FTPTool ftp = FTPTool.getInstance();
                bool connectable = ftp.serverIsConnect();

                foreach (DataGridViewRow row in selectedRows) {
                    string fileLocation = DataGridViewCellTool.
                        getDataGridViewCellValueString(row.Cells[2]);
                    //文件已经上传到服务器中
                    string attachmentName = DataGridViewCellTool.
                            getDataGridViewCellValueString(row.Cells[1]);
                    for (int i = 0; i < fileAttachmentList.Count; i++) {
                        File_Attachment_Info info = fileAttachmentList[i];
                        if (info.Attachment_Name.Equals(attachmentName)) {
                            fileAttachmentList.RemoveAt(i);
                            break;
                        }
                    }
                    if (fileLocation.Equals("") && connectable)
                    {
                        string remoteFileLocation = "";
                        foreach (File_Attachment_Info attachment in fileAttachmentList)
                        {
                            if (attachment.Attachment_Name.Equals(attachmentName))
                            {
                                remoteFileLocation = attachment.Attachment_Location;
                                break;
                            }
                        }
                        //删除数据库记录
                        fileTool.deleteFileAttachment(this.tbFileID.Text, attachmentName);
                        //删除服务器文件
                        ftp.delete(remoteFileLocation);
                        //删除DataGridview记录
                        this.dgvAttachmentInfo.Rows.Remove(row);
                    }
                    else
                    {
                        //删除DataGridview记录
                        this.dgvAttachmentInfo.Rows.Remove(row);
                    }
                }
            }
            else if (this.tabControl1.SelectedTab == this.tpUserInfo) {
                //删除用户信息项
                if (this.dgvUserInfo.Rows.Count == 0)
                {
                    MessageUtil.ShowError("没有可删除内容");
                    return;
                }

                int rowIndex = this.dgvUserInfo.CurrentCell.RowIndex;
                if (rowIndex >= 0 && rowIndex < this.dgvUserInfo.Rows.Count)
                {
                    this.dgvUserInfo.Rows.RemoveAt(rowIndex);
                }
            }
        }

        /// <summary>
        /// 保存
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSave_Click(object sender, EventArgs e)
        {
            if (this.dgvAttachmentInfo.Rows.Count == 0)
            {
                MessageUtil.ShowError("请上传附件！");
                return;
            }

            FTPTool ftp = FTPTool.getInstance();
            //测试FTP服务器是否可连
            bool isConnect = ftp.serverIsConnect();
            if (!isConnect) {
                MessageUtil.ShowError("无法连接服务器!");
                return;
            }

            Dictionary<string, string> uploadRLDic =
                new Dictionary<string, string>();        //待上传文件词典
            double allFilesSize = 0.0;                   //待上传文件大小总和

            #region 读取文件抬头信息

            File_Info fileInfo = new File_Info();
            fileInfo.File_ID = this.tbFileID.Text.Trim();
            if (fileInfo.File_ID.Equals(""))
            {
                MessageUtil.ShowError("请填写文件编号!");
                return;
            }
            fileInfo.File_Type = this.cbxFileType.Text;
            fileInfo.Create_Time = this.dtpCreateTime.Value;
            fileInfo.Creator_ID = "2015001";
            fileInfo.Creator_Type = "企业用户";
            fileInfo.Supplier_Visible_Level = this.cbxSupplierVisibleLevel.Text;
            fileInfo.Modify_Time = this.dtpCreateTime.Value;
            fileInfo.Description = this.rtbDetail.Text;

            #endregion

            #region 读取附件信息

            foreach (File_Attachment_Info fileAttachment in fileAttachmentList)
            {
                fileAttachment.File_ID = fileInfo.File_ID;
                //未上传
                if (fileAttachment.Attachment_Location.Equals(""))
                {
                    fileAttachment.Attachment_Location =
                        fileInfo.File_Type + "/" + fileInfo.File_ID + "/"
                        + fileAttachment.Attachment_Name;
                    fileAttachment.Upload_Time = System.DateTime.Now;

                    uploadRLDic.Add(fileAttachment.Attachment_Location, fileAttachment.Attachment_Local_Location);
                    allFilesSize += fileAttachment.Attachment_Size;
                }
            }

            #endregion

            int resultNum = 0;
            if (opType.Equals("new"))
            {
                resultNum = fileTool.addFileInfo(fileInfo, fileAttachmentList);
                // 可以继续操作，继续操作变为更新
                this.opType = "edit";
            }
            else if(opType.Equals("edit")){
                resultNum = fileTool.updateFileInfo(fileInfo, fileAttachmentList);
            }

            if (resultNum > 0)
            {
                //上传
                /*
                LinkedList<string> uploadFailList = new LinkedList<string>();
                ProgressForm progress = new ProgressForm(uploadFailList);
                progress.RLFileDic = uploadRLDic;
                progress.AllFilesSize = allFilesSize;
                progress.OpType = "upload";
                progress.ShowDialog();
                //ftp.upload(uploadRLDic, allFilesSize);
                if (uploadFailList.Count == 0)
                {
                    foreach (DataGridViewRow curRow in this.dgvAttachmentInfo.Rows) {
                        curRow.Cells[2].Value = "";
                    }
                    MessageBox.Show("保存成功！");
                }
                */
                LinkedList<string> uploadFailList = new LinkedList<string>();
                foreach(KeyValuePair<string,string> kvpair in uploadRLDic)
                {
                    string localFile = kvpair.Value;
                    string remoteFile = kvpair.Key;
                    int indexOfLast = localFile.LastIndexOf('/');
                    // 文件名(不包括路径)
                    string fileSafeName = localFile.Substring(indexOfLast + 1,
                        localFile.Length - indexOfLast - 1);
                    try
                    {
                        ftp.upload(localFile,remoteFile);
                    }
                    catch(Exception ex)
                    {
                        uploadFailList.AddFirst(fileSafeName);
                    }
                }
                if (uploadFailList.Count == 0)
                {
                    foreach (DataGridViewRow row in this.dgvAttachmentInfo.Rows)
                    {
                        row.Cells[2].Value = "";
                    }
                    MessageBox.Show("保存成功！");
                }
                else
                {
                    //删除上传失败的数据库记录
                    foreach (DataGridViewRow row in this.dgvAttachmentInfo.Rows)
                    {
                        string fileSafeName = DataGridViewCellTool.
                            getDataGridViewCellValueString(row.Cells[1]);
                        if (uploadFailList.Contains(fileSafeName))
                        {
                            fileTool.deleteFileAttachment(fileInfo.File_ID,
                                fileSafeName);
                        }
                        else
                        {
                            // 清空 表示上传成功
                            row.Cells[2].Value = "";
                        }
                    }
                    MessageUtil.ShowError(uploadFailList.Count + "个文件上传失败");
                }
            }
            else
            {
                MessageUtil.ShowError("保存失败!");
            }
        }

        /// <summary>
        /// 下载操作
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDownload_Click(object sender, EventArgs e)
        {
            if (this.tabControl1.SelectedTab == this.tpUserInfo)
            {
                MessageUtil.ShowError("请正确操作！");
                return;
            }

            //待下载文件词典
            Dictionary<string, string> downloadRLDic = new Dictionary<string, string>();
            //待下载文件大小总和
            double allFilesSize = 0.0;
            List<string> fileNameList = new List<string>();
            foreach (DataGridViewRow row in this.dgvAttachmentInfo.Rows)
            {
                string isSelected = row.Cells[0].EditedFormattedValue.ToString();
                if (isSelected.Equals("True"))
                {
                    //附件位置不为""，说明未上传，不能下载
                    if (!DataGridViewCellTool.getDataGridViewCellValueString(
                        row.Cells[2]).Equals(""))
                    {
                        MessageUtil.ShowError("请正确选择下载文件");
                    }
                    string fileName = DataGridViewCellTool.getDataGridViewCellValueString(row.Cells[1]);
                    fileNameList.Add(fileName);
                }
            }
            if (fileNameList.Count == 0)
            {
                MessageUtil.ShowError("请至少选择一个下载文件！");
                return;
            }

            //判断服务器是否可连接
            FTPTool ftp = FTPTool.getInstance();
            if (!ftp.serverIsConnect()) {
                MessageUtil.ShowError("无法连接服务器");
                return;
            }

            //选择保存的文件夹
            FolderBrowserDialog downloadFileFolder = new FolderBrowserDialog();
            downloadFileFolder.Description = "请选择文件夹";
            //允许用户在浏览的时候create new folder
            downloadFileFolder.ShowNewFolderButton = false;

            DialogResult result = downloadFileFolder.ShowDialog();
            if (result == DialogResult.OK)
            {
                //格式: E://oil
                string selectedPath = downloadFileFolder.SelectedPath;
                selectedPath = selectedPath.Replace("\\", "/") + "/";
                foreach (string fileName in fileNameList)
                {
                    string remoteName = "";
                    foreach (File_Attachment_Info fileAttachment in fileAttachmentList)
                    {
                        if (fileAttachment.Attachment_Name.Equals(fileName))
                        {
                            remoteName = fileAttachment.Attachment_Location;
                            string localName = selectedPath + fileName;
                            downloadRLDic.Add(remoteName, localName);
                            allFilesSize += fileAttachment.Attachment_Size;

                            break;
                        }
                    }
                }

                //下载
                LinkedList<string> downloadFailList = new LinkedList<string>();
                ProgressForm progress = new ProgressForm(downloadFailList);
                progress.RLFileDic = downloadRLDic;
                progress.AllFilesSize = allFilesSize;
                progress.OpType = "download";
                progress.ShowDialog();
                
                if (downloadFailList.Count == 0)
                {
                    MessageBox.Show("下载成功！");
                    //更新下载成功数据库(下载次数)
                }
                else
                {
                    MessageUtil.ShowError("文件不存在或网络异常！请稍后重试！");
                }
            }
        }

        private void FileView_Click(object sender, EventArgs e)
        {

            if (this.tabControl1.SelectedTab == this.tpUserInfo)
            {
                MessageUtil.ShowError("请正确操作！");
                return;
            }

            //待下载文件词典
            Dictionary<string, string> downloadRLDic = new Dictionary<string, string>();
            //待下载文件大小总和
            double allFilesSize = 0.0;
            List<string> fileNameList = new List<string>();
            foreach (DataGridViewRow row in this.dgvAttachmentInfo.Rows)
            {
                string isSelected = row.Cells[0].EditedFormattedValue.ToString();
                if (isSelected.Equals("True"))
                {
                    //附件位置不为""，说明未上传，不能下载
                    if (!DataGridViewCellTool.getDataGridViewCellValueString(
                        row.Cells[2]).Equals(""))
                    {
                        MessageUtil.ShowError("请正确选择下载文件");
                    }
                    string fileName = DataGridViewCellTool.getDataGridViewCellValueString(row.Cells[1]);
                    fileNameList.Add(fileName);
                }
            }
            if (fileNameList.Count == 0)
            {
                MessageUtil.ShowError("请至少选择一个下载文件！");
                return;
            }

            //判断服务器是否可连接
            FTPTool ftp = FTPTool.getInstance();
            if (!ftp.serverIsConnect())
            {
                MessageUtil.ShowError("无法连接服务器");
                return;
            }

            //选择保存的文件夹
      

            string selectedPath = System.Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            
            selectedPath = selectedPath.Replace("\\", "/") + "/";
            
         
            
                //格式: E://oil
                
                foreach (string fileName in fileNameList)
                {
                    string remoteName = "";
                    foreach (File_Attachment_Info fileAttachment in fileAttachmentList)
                    {
                        if (fileAttachment.Attachment_Name.Equals(fileName))
                        {
                            remoteName = fileAttachment.Attachment_Location;
                            string localName = selectedPath + fileName;
                            downloadRLDic.Add(remoteName, localName);
                            allFilesSize += fileAttachment.Attachment_Size;

                            break;
                        }
                    }
                }

                //下载
                LinkedList<string> downloadFailList = new LinkedList<string>();
                ProgressForm progress = new ProgressForm(downloadFailList);
                progress.RLFileDic = downloadRLDic;
                progress.AllFilesSize = allFilesSize;
                progress.OpType = "download";
                progress.ShowDialog();

                if (downloadFailList.Count == 0)
                {
                   
                    //更新下载成功数据库(下载次数)
                    
                    FileShowForm fileShowForm = new FileShowForm(selectedPath+"\\"+ fileNameList[0]);
                    SingletonUserUI.addToUserUI(fileShowForm);
                }
                else
                {
                    MessageUtil.ShowError("文件不存在或网络异常！请稍后重试！");
                }
           

           


        }
    }
}    