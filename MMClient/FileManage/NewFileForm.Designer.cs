﻿namespace MMClient.FileManage
{
    partial class NewFileForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.tbFileID = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cbxFileType = new System.Windows.Forms.ComboBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tpAttachmentInfo = new System.Windows.Forms.TabPage();
            this.dgvAttachmentInfo = new System.Windows.Forms.DataGridView();
            this.Selected = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Attachment_Name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Attachment_Location = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Attachment_Size = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tpUserInfo = new System.Windows.Forms.TabPage();
            this.dgvUserInfo = new System.Windows.Forms.DataGridView();
            this.User_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.User_Type = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Permission_Type = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.dtpCreateTime = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.rtbDetail = new System.Windows.Forms.RichTextBox();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label5 = new System.Windows.Forms.Label();
            this.cbxSupplierVisibleLevel = new System.Windows.Forms.ComboBox();
            this.btnDownload = new System.Windows.Forms.Button();
            this.bgwFileOperate = new System.ComponentModel.BackgroundWorker();
            this.FileView = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.tpAttachmentInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAttachmentInfo)).BeginInit();
            this.tpUserInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvUserInfo)).BeginInit();
            this.SuspendLayout();
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(53, 36);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(65, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "文档编号：";
            // 
            // tbFileID
            // 
            this.tbFileID.Location = new System.Drawing.Point(149, 33);
            this.tbFileID.Name = "tbFileID";
            this.tbFileID.Size = new System.Drawing.Size(351, 21);
            this.tbFileID.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(53, 85);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 12);
            this.label2.TabIndex = 2;
            this.label2.Text = "文档类型：";
            // 
            // cbxFileType
            // 
            this.cbxFileType.FormattingEnabled = true;
            this.cbxFileType.Location = new System.Drawing.Point(149, 82);
            this.cbxFileType.Name = "cbxFileType";
            this.cbxFileType.Size = new System.Drawing.Size(351, 20);
            this.cbxFileType.TabIndex = 3;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tpAttachmentInfo);
            this.tabControl1.Controls.Add(this.tpUserInfo);
            this.tabControl1.Location = new System.Drawing.Point(12, 328);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(571, 353);
            this.tabControl1.TabIndex = 4;
            // 
            // tpAttachmentInfo
            // 
            this.tpAttachmentInfo.Controls.Add(this.dgvAttachmentInfo);
            this.tpAttachmentInfo.Location = new System.Drawing.Point(4, 22);
            this.tpAttachmentInfo.Name = "tpAttachmentInfo";
            this.tpAttachmentInfo.Padding = new System.Windows.Forms.Padding(3);
            this.tpAttachmentInfo.Size = new System.Drawing.Size(563, 327);
            this.tpAttachmentInfo.TabIndex = 1;
            this.tpAttachmentInfo.Text = "文档附件";
            this.tpAttachmentInfo.UseVisualStyleBackColor = true;
            // 
            // dgvAttachmentInfo
            // 
            this.dgvAttachmentInfo.AllowUserToAddRows = false;
            this.dgvAttachmentInfo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAttachmentInfo.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Selected,
            this.Attachment_Name,
            this.Attachment_Location,
            this.Attachment_Size});
            this.dgvAttachmentInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvAttachmentInfo.Location = new System.Drawing.Point(3, 3);
            this.dgvAttachmentInfo.Name = "dgvAttachmentInfo";
            this.dgvAttachmentInfo.RowHeadersVisible = false;
            this.dgvAttachmentInfo.RowTemplate.Height = 23;
            this.dgvAttachmentInfo.Size = new System.Drawing.Size(557, 321);
            this.dgvAttachmentInfo.TabIndex = 0;
            // 
            // Selected
            // 
            this.Selected.HeaderText = "选中";
            this.Selected.Name = "Selected";
            this.Selected.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Selected.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Selected.Width = 60;
            // 
            // Attachment_Name
            // 
            this.Attachment_Name.HeaderText = "附件名称";
            this.Attachment_Name.Name = "Attachment_Name";
            this.Attachment_Name.ReadOnly = true;
            this.Attachment_Name.Width = 200;
            // 
            // Attachment_Location
            // 
            this.Attachment_Location.HeaderText = "附件位置";
            this.Attachment_Location.Name = "Attachment_Location";
            this.Attachment_Location.ReadOnly = true;
            this.Attachment_Location.Width = 194;
            // 
            // Attachment_Size
            // 
            this.Attachment_Size.HeaderText = "附件大小";
            this.Attachment_Size.Name = "Attachment_Size";
            this.Attachment_Size.ReadOnly = true;
            // 
            // tpUserInfo
            // 
            this.tpUserInfo.Controls.Add(this.dgvUserInfo);
            this.tpUserInfo.Location = new System.Drawing.Point(4, 22);
            this.tpUserInfo.Name = "tpUserInfo";
            this.tpUserInfo.Padding = new System.Windows.Forms.Padding(3);
            this.tpUserInfo.Size = new System.Drawing.Size(563, 327);
            this.tpUserInfo.TabIndex = 0;
            this.tpUserInfo.Text = "权限设置";
            this.tpUserInfo.UseVisualStyleBackColor = true;
            // 
            // dgvUserInfo
            // 
            this.dgvUserInfo.AllowUserToAddRows = false;
            this.dgvUserInfo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvUserInfo.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.User_ID,
            this.User_Type,
            this.Permission_Type});
            this.dgvUserInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvUserInfo.Location = new System.Drawing.Point(3, 3);
            this.dgvUserInfo.Name = "dgvUserInfo";
            this.dgvUserInfo.RowHeadersVisible = false;
            this.dgvUserInfo.RowTemplate.Height = 23;
            this.dgvUserInfo.Size = new System.Drawing.Size(557, 321);
            this.dgvUserInfo.TabIndex = 0;
            // 
            // User_ID
            // 
            this.User_ID.HeaderText = "用户编号";
            this.User_ID.Name = "User_ID";
            this.User_ID.Width = 200;
            // 
            // User_Type
            // 
            this.User_Type.HeaderText = "用户类别";
            this.User_Type.Name = "User_Type";
            this.User_Type.Width = 150;
            // 
            // Permission_Type
            // 
            this.Permission_Type.HeaderText = "权限类型";
            this.Permission_Type.Items.AddRange(new object[] {
            "读取",
            "读取、编辑",
            "读取、编辑、删除"});
            this.Permission_Type.Name = "Permission_Type";
            this.Permission_Type.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Permission_Type.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Permission_Type.Width = 200;
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(228, 313);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(75, 23);
            this.btnAdd.TabIndex = 5;
            this.btnAdd.Text = "增加";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(317, 313);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(75, 23);
            this.btnDelete.TabIndex = 6;
            this.btnDelete.Text = "删除";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(501, 313);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 7;
            this.btnSave.Text = "保存";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(53, 166);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 12);
            this.label3.TabIndex = 8;
            this.label3.Text = "创建时间：";
            // 
            // dtpCreateTime
            // 
            this.dtpCreateTime.Location = new System.Drawing.Point(149, 160);
            this.dtpCreateTime.Name = "dtpCreateTime";
            this.dtpCreateTime.Size = new System.Drawing.Size(351, 21);
            this.dtpCreateTime.TabIndex = 9;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(53, 235);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 12);
            this.label4.TabIndex = 10;
            this.label4.Text = "详细说明：";
            // 
            // rtbDetail
            // 
            this.rtbDetail.Location = new System.Drawing.Point(149, 206);
            this.rtbDetail.Name = "rtbDetail";
            this.rtbDetail.Size = new System.Drawing.Size(351, 68);
            this.rtbDetail.TabIndex = 11;
            this.rtbDetail.Text = "";
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "用户编号";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Width = 150;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "用户类别";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.Width = 150;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "权限类型";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.HeaderText = "项目";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.Width = 60;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.HeaderText = "附件名称";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.Width = 200;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.HeaderText = "附件位置";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.Width = 194;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.HeaderText = "附件大小";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(17, 125);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(101, 12);
            this.label5.TabIndex = 12;
            this.label5.Text = "供应商可见范围：";
            // 
            // cbxSupplierVisibleLevel
            // 
            this.cbxSupplierVisibleLevel.FormattingEnabled = true;
            this.cbxSupplierVisibleLevel.Items.AddRange(new object[] {
            "不可查看",
            "指定供应商",
            "全部供应商"});
            this.cbxSupplierVisibleLevel.Location = new System.Drawing.Point(149, 122);
            this.cbxSupplierVisibleLevel.Name = "cbxSupplierVisibleLevel";
            this.cbxSupplierVisibleLevel.Size = new System.Drawing.Size(351, 20);
            this.cbxSupplierVisibleLevel.TabIndex = 13;
            // 
            // btnDownload
            // 
            this.btnDownload.Location = new System.Drawing.Point(408, 313);
            this.btnDownload.Name = "btnDownload";
            this.btnDownload.Size = new System.Drawing.Size(75, 23);
            this.btnDownload.TabIndex = 14;
            this.btnDownload.Text = "下载";
            this.btnDownload.UseVisualStyleBackColor = true;
            this.btnDownload.Click += new System.EventHandler(this.btnDownload_Click);
            // 
            // bgwFileOperate
            // 
            this.bgwFileOperate.WorkerReportsProgress = true;
            this.bgwFileOperate.WorkerSupportsCancellation = true;
            // 
            // FileView
            // 
            this.FileView.Location = new System.Drawing.Point(141, 313);
            this.FileView.Name = "FileView";
            this.FileView.Size = new System.Drawing.Size(75, 23);
            this.FileView.TabIndex = 15;
            this.FileView.Text = "查看";
            this.FileView.UseVisualStyleBackColor = true;
            this.FileView.Click += new System.EventHandler(this.FileView_Click);
            // 
            // NewFileForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(595, 688);
            this.Controls.Add(this.FileView);
            this.Controls.Add(this.btnDownload);
            this.Controls.Add(this.cbxSupplierVisibleLevel);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.rtbDetail);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.dtpCreateTime);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.cbxFileType);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tbFileID);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "NewFileForm";
            this.Text = "新建文档";
            this.tabControl1.ResumeLayout(false);
            this.tpAttachmentInfo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvAttachmentInfo)).EndInit();
            this.tpUserInfo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvUserInfo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbFileID;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cbxFileType;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tpUserInfo;
        private System.Windows.Forms.TabPage tpAttachmentInfo;
        private System.Windows.Forms.DataGridView dgvAttachmentInfo;
        private System.Windows.Forms.DataGridView dgvUserInfo;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker dtpCreateTime;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.RichTextBox rtbDetail;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cbxSupplierVisibleLevel;
        private System.Windows.Forms.Button btnDownload;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Selected;
        private System.Windows.Forms.DataGridViewTextBoxColumn Attachment_Name;
        private System.Windows.Forms.DataGridViewTextBoxColumn Attachment_Location;
        private System.Windows.Forms.DataGridViewTextBoxColumn Attachment_Size;
        private System.Windows.Forms.DataGridViewTextBoxColumn User_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn User_Type;
        private System.Windows.Forms.DataGridViewComboBoxColumn Permission_Type;
        private System.ComponentModel.BackgroundWorker bgwFileOperate;
        private System.Windows.Forms.Button FileView;
    }
}