﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using ChartDirector;

namespace MMClient.RA.ChartClass
{
    public class PM_SupplierABC
    {
        public void createChart(DataTable dt, WinChartViewer viewer)
        {
            int n = dt.Rows.Count;
            double[] xlable = new double[n];
            double[] ylable = new double[n];
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < n; i++)
                    {
                        //从数据库中取得横纵轴坐标值
                        xlable[i] = Convert.ToDouble(dt.Rows[i]["QCumShare"]) * 100;
                        ylable[i] = Convert.ToDouble(dt.Rows[i]["CumShare"]) * 100;
                    }
                }
            }
            XYChart c = new XYChart(630, 400);
            c.setPlotArea(55, 40, 550, 300, 0xADD8E6);
            //c.addTitle("供应商的ABC类分析", "宋体 Bold", 12);
            c.xAxis().setTitle("品种数量比例%");
            c.yAxis().setTitle("金额比例%");
            LineLayer line = c.addLineLayer(ylable);
            line.setXData(xlable);
            line.getDataSet(0).setDataSymbol(Chart.SquareSymbol, 6);
            //line.getDataSet(0).setDataSymbol(Chart.SquareSymbol, 9);
            viewer.Chart = c;
            viewer.ImageMap = c.getHTMLImageMap("clickable", "",
                "title='[{dataSetName}] Pressure = {x} Pa, Temperature = {value} C'");
        }
    }
}
