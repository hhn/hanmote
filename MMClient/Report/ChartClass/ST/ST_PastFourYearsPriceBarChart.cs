﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChartDirector;

namespace MMClient.RA.ChartClass
{
    public class ST_PastFourYearsPriceBarChart
    {
        public void createChart(WinChartViewer viewer)
        {
            // The data for the bar chart
            double[] data = { 16, 19.5, 23, 27};
            double[] data2 = { 21.375, 21.375, 21.375, 21.375 };

            // The labels for the bar chart
            string[] labels = {
                "2012", "2013", "2014", "2015"};

            // Create a XYChart object of size 600 x 360 pixels
            XYChart c = new XYChart(570, 420);

            // Add a title to the chart using 18pts Times Bold Italic font
            c.addTitle("过去4年单位价格分析", "宋体 Bold ",14);

            // Set the plotarea at (60, 40) and of size 500 x 280 pixels. Use a
            // vertical gradient color from light blue (eeeeff) to deep blue (0000cc)
            // as background. Set border and grid lines to white (ffffff).
            c.setPlotArea(60, 40, 370, 300, -1, 0xffffff, 0xffffff);

            c.addLegend(50, 30, false, "Arial Bold", 9).setBackground( Chart.Transparent);


            //增加一条平均单价线
            LineLayer lineLayer = c.addLineLayer();
            lineLayer.addDataSet(data2, 0xB20100, "平均单价").setDataSymbol(Chart.GlassSphere2Shape, 6);

            // Add a multi-color bar chart layer using the supplied data. Use soft
            // lighting effect with light direction from left.
            c.addBarLayer(data,0x01B556);

            // Set x axis labels using the given labels
            c.xAxis().setLabels(labels);

            // Draw the ticks between label positions (instead of at label positions)
            c.xAxis().setTickOffset(0.5);

            // Add a title to the y axis with 10pts Arial Bold font
            c.yAxis().setTitle("单价 (元)", "Arial Bold", 10);
            c.xAxis().setTitle("年份","Arial Bold",10);

            // Set axis label style to 8pts Arial Bold
            c.xAxis().setLabelStyle("Arial Bold", 8);
            c.yAxis().setLabelStyle("Arial Bold", 8);

            // Set axis line width to 2 pixels
            c.xAxis().setWidth(2);
            c.yAxis().setWidth(2);

            // Output the chart
            viewer.Chart = c;

            //include tool tip for the chart
            viewer.ImageMap = c.getHTMLImageMap("clickable", "",
                "title='年份：{xLabel}； 价格：{value}元'");
        }
    }
}
