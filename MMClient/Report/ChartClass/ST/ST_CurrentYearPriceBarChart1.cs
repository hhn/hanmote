﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChartDirector;

namespace MMClient.RA.ChartClass
{
    //寻源分析中物料当年价格条形图
    public class ST_CurrentYearPriceBarChart1
    {
        public void createChart(WinChartViewer viewer)
        {
            // The data for the bar chart
            double[] data = { 45, 56 };

            // The labels for the bar chart
            string[] labels = {"合同价格", "市场价格"};

            // Create a XYChart object of size 600 x 360 pixels
            XYChart c = new XYChart(490, 420);

            // Add a title to the chart using 18pts Times Bold Italic font
            c.addTitle("当年单位价格分析", "宋体 Bold ",14);

            // Set the plotarea at (60, 40) and of size 500 x 280 pixels. Use a
            // vertical gradient color from light blue (eeeeff) to deep blue (0000cc)
            // as background. Set border and grid lines to white (ffffff).
            //c.setPlotArea(60, 40, 370,300, c.linearGradientColor(60, 40, 60, 280,
            //    0xeeeeff, 0x0000cc), -1, 0xffffff, 0xffffff);

            c.setPlotArea(60, 40, 370, 300, -1, 0xffffff, 0xffffff);

            //将XY轴互换
            c.swapXY();

            // Add a multi-color bar chart layer using the supplied data. Use soft
            // lighting effect with light direction from left.
            //c.addBarLayer3(data).setBorderColor(Chart.Transparent,
           //     Chart.softLighting(Chart.Left));

            c.addBarLayer(data, 0x2953A7);

            // Set x axis labels using the given labels
            c.xAxis().setLabels(labels);

            // Draw the ticks between label positions (instead of at label positions)
            c.xAxis().setTickOffset(0.5);

            // Add a title to the y axis with 10pts Arial Bold font
            c.yAxis().setTitle("单价 (元)", "Arial Bold", 10);

            // Set axis label style to 8pts Arial Bold
            c.xAxis().setLabelStyle("Arial Bold", 8);
            c.yAxis().setLabelStyle("Arial Bold", 8);

            // Set axis line width to 2 pixels
            c.xAxis().setWidth(2);
            c.yAxis().setWidth(2);

            // Output the chart
            viewer.Chart = c;

            //include tool tip for the chart
            viewer.ImageMap = c.getHTMLImageMap("clickable", "",
                "title='类别 ：{xLabel}; 价格：{value}元'");
        }
    }
}
