﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Bll;
using Lib.Common.CommonUtils;

namespace MMClient.RA.SupplierPerformance
{
    public partial class SupplierScoreRanking : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        BuyerOrganizationBLL buyerOrganizationBLL = new BuyerOrganizationBLL();
        SupplierPerformanceBLL sPerformanceBLL = new SupplierPerformanceBLL();
        SupplierPerformanceTotalBLL sPerformanceTotalBLL = new SupplierPerformanceTotalBLL();
        DataTable dt1 = new DataTable();

        public SupplierScoreRanking()
        {
            //窗体初始化
            InitializeComponent();

            //为列标题栏添加一个“行号”
            this.dgv_ComparativeData.TopLeftHeaderCell.Value = "行号";
            this.dgv_SupplierTotalStandard.TopLeftHeaderCell.Value = "行号";

            //初始化窗体时默认选择第一个值
            this.cbb_PurchaseGroup.SelectedIndex = 0;
            this.cbb_MaterialGroup.SelectedIndex = 0;

            dt1 = buyerOrganizationBLL.GetAllBuyerOrgName();
            this.cbb_PurchaseGroup.DataSource = dt1;

            this.cbb_MaterialGroup.SelectedIndex = 0;
            this.cbb_MaterialGroup.SelectedIndex = 0;

            this.cbb_PurchaseGroup.DisplayMember = "Buyer_Org_Name";
            this.cbb_PurchaseGroup.ValueMember = "Buyer_Org_Name";
        }

        /// <summary>
        /// 生成报表
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_Query_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dt1 = sPerformanceBLL.GetSupplierPerformance(this.cbb_PurchaseGroup.SelectedValue.ToString(), "123");
                DataTable dt2 = sPerformanceTotalBLL.GetSupplierPerformanceTotal(this.cbb_PurchaseGroup.SelectedValue.ToString(), "123");
                this.DataGridShow(dt1, this.dgv_ComparativeData);
                this.DataGridShow(dt2, this.dgv_SupplierTotalStandard);
            }
            catch (Exception ex)
            {
                MessageUtil.ShowError(ex.Message);
            }
        }

        /// <summary>
        /// 将数据绑定到datagridview上
        /// </summary>
        /// <param name="dt"></param>
        private void DataGridShow(DataTable dt, DataGridView dgv)
        {
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    dgv.DataSource = dt;
                }
            }
        }

        /// <summary>
        /// 画行号
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgv_ComparativeData_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            var dgv = sender as DataGridView;
            if (dgv != null)
            {
                Rectangle rect = new Rectangle(e.RowBounds.Location.X, e.RowBounds.Location.Y, dgv.RowHeadersWidth - 4, e.RowBounds.Height);
                TextRenderer.DrawText(e.Graphics, (e.RowIndex + 1).ToString(), dgv.RowHeadersDefaultCellStyle.Font, rect, dgv.RowHeadersDefaultCellStyle.ForeColor, TextFormatFlags.VerticalCenter | TextFormatFlags.Right);
            }
        }

        /// <summary>
        /// 当鼠标进入单元格时发生
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgv_ComparativeData_CellMouseEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
            {
                this.dgv_ComparativeData.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.LightBlue;
                DataGridViewCellStyle style = new DataGridViewCellStyle();
                style.BackColor = Color.CornflowerBlue;
                this.dgv_ComparativeData.Rows[e.RowIndex].HeaderCell.Style = style;
            }
        }

        /// <summary>
        /// 当鼠标移出单元格时发生
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgv_ComparativeData_CellMouseLeave(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
            {
                this.dgv_ComparativeData.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.White;
                DataGridViewCellStyle style = new DataGridViewCellStyle();
                style.BackColor = Color.WhiteSmoke;
                this.dgv_ComparativeData.Rows[e.RowIndex].HeaderCell.Style = style;
            }
        }

        /// <summary>
        /// 当点击单元格内容时触发
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgv_ComparativeData_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            //当点击第一列单元格（公司名称）中内容时触发事件
            if (e.ColumnIndex == 0)
            {
                //MessageBox.Show(this.dgv_SupplierDependentFactor.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString());
                new SupplierDetails(this.dgv_ComparativeData.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString()).ShowDialog();
            }
        }

        private void dgv_SupplierTotalStandard_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            //当点击第一列单元格（公司名称）中内容时触发事件
            if (e.ColumnIndex == 0)
            {
                //MessageBox.Show(this.dgv_SupplierDependentFactor.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString());
                new SupplierDetails(this.dgv_SupplierTotalStandard.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString()).ShowDialog();
            }
        }

        private void dgv_SupplierTotalStandard_CellMouseEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
            {
                this.dgv_SupplierTotalStandard.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.LightBlue;
                DataGridViewCellStyle style = new DataGridViewCellStyle();
                style.BackColor = Color.CornflowerBlue;
                this.dgv_SupplierTotalStandard.Rows[e.RowIndex].HeaderCell.Style = style;
            }
        }

        private void dgv_SupplierTotalStandard_CellMouseLeave(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
            {
                this.dgv_SupplierTotalStandard.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.White;
                DataGridViewCellStyle style = new DataGridViewCellStyle();
                style.BackColor = Color.WhiteSmoke;
                this.dgv_SupplierTotalStandard.Rows[e.RowIndex].HeaderCell.Style = style;
            }
        }

        private void dgv_SupplierTotalStandard_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            var dgv = sender as DataGridView;
            if (dgv != null)
            {
                Rectangle rect = new Rectangle(e.RowBounds.Location.X, e.RowBounds.Location.Y, dgv.RowHeadersWidth * 2 / 3, e.RowBounds.Height);
                TextRenderer.DrawText(e.Graphics, (e.RowIndex + 1).ToString(), dgv.RowHeadersDefaultCellStyle.Font, rect, dgv.RowHeadersDefaultCellStyle.ForeColor, TextFormatFlags.VerticalCenter | TextFormatFlags.Right);
            }
        }
    }
}
