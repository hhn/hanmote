﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ChartDirector;
using MMClient.RA;
using System.Drawing.Printing;

namespace MMClient.RA.SupplierPerformance
{
    public partial class SupplierTrendAnalysis : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        #region 与打印相关类声明
        //ChartPrinter MyChartPrinter;
        #endregion

        #region 窗体初试化
        public SupplierTrendAnalysis()
        {
            InitializeComponent();
            this.cbb_PurchaseGroup.SelectedIndex = 0;
            this.cbb_MaterialGroup.SelectedIndex = 0;
        }
        #endregion

        #region 生成报表按钮事件
        /// <summary>
        /// 生成报表
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_Query_Click(object sender, EventArgs e)
        {
            WinChartViewer viewerAll = this.ALLChartViewer;
            this.createChartAll(viewerAll);
            WinChartViewer viewerQuality = this.QualityChartViewer;
            this.createChartAll(viewerQuality);
            WinChartViewer viewerDelivery = this.DeliveryChartViewer;
            this.createChartAll(viewerDelivery);
            WinChartViewer viewerCost = this.CostChartViewer;
            this.createChartAll(viewerCost);
        }
        #endregion

        #region 绘制图表方法
        private void createChartAll(WinChartViewer viewer)
        {
            double[] data = new double[7];
            double[] data1 = { 75, 80, 85, 70, 50, 90, 85 };
            double[] data2 = { 80, 75, 80, 90, 95, 60, 70 };
            double[] data3 = { 90, 80, 98, 83, 70, 60, 95 };
            double[] data4 = { 70, 90, 85, 93, 65, 90, 98 }; 
            if(viewer.Equals(this.ALLChartViewer))
            {
                data = data1;           
            }
            else if (viewer.Equals(this.QualityChartViewer))
            {
                data = data2; 
            }
            else if (viewer.Equals(this.DeliveryChartViewer))
            {
                data = data3; 
            }
            else
            {
                data = data4; 
            }
            // The data for the line chart
            //double[] data = {30, 28, 40, 55, 75, 68, 54};

            // The labels for the line chart
            string[] labels = { "一月", "二月", "三月", "四月", "五月", "六月", "七月" };

            // Create a XYChart object of size 250 x 250 pixels
            XYChart c = new XYChart(570, 265);

            // Set the plotarea at (30, 20) and of size 200 x 200 pixels
            c.setPlotArea(50, 20, 500,180);

            c.xAxis().setTitle("时间");

            c.yAxis().setTitle("得分");

            // Add a line chart layer using the given data
            c.addLineLayer(data);

            // Set the labels on the x axis.
            c.xAxis().setLabels(labels);

            // Display 1 out of 3 labels on the x-axis.
            c.xAxis().setLabelStep(1);

            // Output the chart
            viewer.Chart = c;

            //include tool tip for the chart
            viewer.ImageMap = c.getHTMLImageMap("clickable", "",
                "title='Hour {xLabel}: Traffic {value} GBytes'");
        }
        #endregion

        #region 图表钻取事件
        private void ALLChartViewer_ClickHotSpot(object sender, WinHotSpotEventArgs e)
        {
            new ParamViewer().Display(sender, e);
        }

        private void QualityChartViewer_ClickHotSpot(object sender, WinHotSpotEventArgs e)
        {
            new ParamViewer().Display(sender, e);
        }

        private void DeliveryChartViewer_ClickHotSpot(object sender, WinHotSpotEventArgs e)
        {
            new ParamViewer().Display(sender, e);
        }

        private void CostChartViewer_ClickHotSpot(object sender, WinHotSpotEventArgs e)
        {
            new ParamViewer().Display(sender, e);
        }
        #endregion

        /*

        #region 综合得分趋势打印
        private void btn_PrintAll_Click(object sender, EventArgs e)
        {
            if (this.TransforToImage(AllPageSetupDialog, lb_All, AllPrintDocument, panel_All))
            {
                this.AllPrintDocument.Print();
            }
        }

        private void btn_PreviewAll_Click(object sender, EventArgs e)
        {
            if (this.TransforToImage(AllPageSetupDialog, lb_All, AllPrintDocument, panel_All))
            {
                PrintPreviewDialog MyPrintPreviewDialog = new PrintPreviewDialog();
                MyPrintPreviewDialog.Document = AllPrintDocument;
                MyPrintPreviewDialog.ShowDialog();
            }
        }
       
        private void AllPrintDocument_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            MyChartPrinter.DrawChart(e.Graphics); 
        }
        #endregion

        #region 质量得分趋势打印
        private void btn_PrintQuality_Click(object sender, EventArgs e)
        {
            if (this.TransforToImage(QualityPageSetupDialog, lb_Quality, QualityPrintDocument, panel_Quality))
            {
                this.QualityPrintDocument.Print();
            }
        }

        private void btn_PreviewQuality_Click(object sender, EventArgs e)
        {
            if (this.TransforToImage(QualityPageSetupDialog, lb_Quality, QualityPrintDocument, panel_Quality))
            {
                PrintPreviewDialog MyPrintPreviewDialog = new PrintPreviewDialog();
                MyPrintPreviewDialog.Document = QualityPrintDocument;
                MyPrintPreviewDialog.ShowDialog();
            }
        }

        private void QualityPrintDocument_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            MyChartPrinter.DrawChart(e.Graphics); 
        }
        #endregion

        #region 交付得分趋势打印
        private void btn_PrintDelivery_Click(object sender, EventArgs e)
        {
            if (this.TransforToImage(DeliveryPageSetupDialog, lb_Delivery, DeliveryPrintDocument, panel_Delivery))
            {
                this.DeliveryPrintDocument.Print();
            }
        }

        private void btn_PreviewDelivery_Click(object sender, EventArgs e)
        {
            if (this.TransforToImage(DeliveryPageSetupDialog, lb_Delivery, DeliveryPrintDocument, panel_Delivery))
            {
                PrintPreviewDialog MyPrintPreviewDialog = new PrintPreviewDialog();
                MyPrintPreviewDialog.Document = DeliveryPrintDocument;
                MyPrintPreviewDialog.ShowDialog();
            }
        }

        private void DeliveryPrintDocument_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            MyChartPrinter.DrawChart(e.Graphics); 
        }
        #endregion

        #region 成本得分趋势打印
        private void btn_PrintCost_Click(object sender, EventArgs e)
        {
            if (this.TransforToImage(CostPageSetupDialog, lb_Cost, CostPrintDocument, panel_Cost))
            {
                this.CostPrintDocument.Print();
            }
        }

        private void btn_PrivierCost_Click(object sender, EventArgs e)
        {
            if (this.TransforToImage(CostPageSetupDialog, lb_Cost, CostPrintDocument, panel_Cost))
            {
                PrintPreviewDialog MyPrintPreviewDialog = new PrintPreviewDialog();
                MyPrintPreviewDialog.Document = CostPrintDocument;
                MyPrintPreviewDialog.ShowDialog();
            }
        }

        private void CostPrintDocument_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            MyChartPrinter.DrawChart(e.Graphics); 
        }
        #endregion

        #region 转换图表方法
        /// <summary>
        /// 将图表转换成一张图片
        /// </summary>
        /// <returns></returns>
        private bool TransforToImage(PageSetupDialog chartPageSetupDialog, Label lb_ChartTitle, PrintDocument chartPrintDocument, Panel panel_Chart)
        {
            chartPageSetupDialog.ShowDialog();
            //允许用户选择打印机
            PrintDialog chartPrintDialog = new PrintDialog();
            chartPrintDialog.AllowCurrentPage = true;
            chartPrintDialog.AllowPrintToFile = true;
            chartPrintDialog.AllowSelection = true;
            chartPrintDialog.AllowSomePages = true;
            chartPrintDialog.PrintToFile = false;
            chartPrintDialog.ShowHelp = true;
            chartPrintDialog.ShowHelp = true;
            if (chartPrintDialog.ShowDialog() != DialogResult.OK)
            {
                return false;
            }
            //获取打印文档名称
            string titleName = lb_ChartTitle.Text.Trim();
            chartPrintDocument.DocumentName = titleName;
            chartPrintDocument.PrinterSettings = chartPrintDialog.PrinterSettings;
            chartPrintDocument.DefaultPageSettings = chartPageSetupDialog.PageSettings;

            //this.panel_Chart.BackColor = Color.White;
            Bitmap bitmap = new Bitmap(panel_Chart.Width, panel_Chart.Height);
            panel_Chart.DrawToBitmap(bitmap, new System.Drawing.Rectangle(0, 0, bitmap.Width, bitmap.Height));
            MyChartPrinter = new ChartPrinter(bitmap, chartPrintDocument, true, true, titleName, new Font("宋体", 14, FontStyle.Bold, GraphicsUnit.Point), Color.Black);
            return true;
        }
       #endregion
         * */
    }
}
