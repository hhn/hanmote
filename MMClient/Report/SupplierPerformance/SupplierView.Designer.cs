﻿namespace MMClient.RA.SupplierPerformance
{
    partial class SupplierView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lb_Unit = new System.Windows.Forms.Label();
            this.lb_Number = new System.Windows.Forms.Label();
            this.lb_SearchResult = new System.Windows.Forms.Label();
            this.btn_Search = new System.Windows.Forms.Button();
            this.cbb_Country = new System.Windows.Forms.ComboBox();
            this.lb_Country = new System.Windows.Forms.Label();
            this.txt_ChooseSupplier = new System.Windows.Forms.TextBox();
            this.lb_ChooseSupplier = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.dgv_SupplierDetails = new System.Windows.Forms.DataGridView();
            this.Column_Choose = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Column_SupplierID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column_SupplierName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column_Address = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btn_Confirm = new System.Windows.Forms.Button();
            this.btn_Cancel = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_SupplierDetails)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.Control;
            this.panel1.Controls.Add(this.lb_Unit);
            this.panel1.Controls.Add(this.lb_Number);
            this.panel1.Controls.Add(this.lb_SearchResult);
            this.panel1.Controls.Add(this.btn_Search);
            this.panel1.Controls.Add(this.cbb_Country);
            this.panel1.Controls.Add(this.lb_Country);
            this.panel1.Controls.Add(this.txt_ChooseSupplier);
            this.panel1.Controls.Add(this.lb_ChooseSupplier);
            this.panel1.Location = new System.Drawing.Point(13, 13);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(912, 65);
            this.panel1.TabIndex = 0;
            // 
            // lb_Unit
            // 
            this.lb_Unit.Location = new System.Drawing.Point(794, 28);
            this.lb_Unit.Name = "lb_Unit";
            this.lb_Unit.Size = new System.Drawing.Size(20, 12);
            this.lb_Unit.TabIndex = 7;
            this.lb_Unit.Text = "条";
            // 
            // lb_Number
            // 
            this.lb_Number.Location = new System.Drawing.Point(779, 29);
            this.lb_Number.Name = "lb_Number";
            this.lb_Number.Size = new System.Drawing.Size(15, 12);
            this.lb_Number.TabIndex = 6;
            this.lb_Number.Text = "0";
            // 
            // lb_SearchResult
            // 
            this.lb_SearchResult.AutoSize = true;
            this.lb_SearchResult.Location = new System.Drawing.Point(718, 28);
            this.lb_SearchResult.Name = "lb_SearchResult";
            this.lb_SearchResult.Size = new System.Drawing.Size(65, 12);
            this.lb_SearchResult.TabIndex = 5;
            this.lb_SearchResult.Text = "搜索结果：";
            // 
            // btn_Search
            // 
            this.btn_Search.BackColor = System.Drawing.SystemColors.Control;
            this.btn_Search.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.btn_Search.Location = new System.Drawing.Point(601, 21);
            this.btn_Search.Name = "btn_Search";
            this.btn_Search.Size = new System.Drawing.Size(89, 23);
            this.btn_Search.TabIndex = 4;
            this.btn_Search.Text = "搜索";
            this.btn_Search.UseVisualStyleBackColor = true;
            this.btn_Search.Click += new System.EventHandler(this.btn_Search_Click);
            // 
            // cbb_Country
            // 
            this.cbb_Country.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbb_Country.FormattingEnabled = true;
            this.cbb_Country.Items.AddRange(new object[] {
            "中国",
            "美国",
            "英国"});
            this.cbb_Country.Location = new System.Drawing.Point(366, 20);
            this.cbb_Country.Name = "cbb_Country";
            this.cbb_Country.Size = new System.Drawing.Size(182, 20);
            this.cbb_Country.TabIndex = 3;
            // 
            // lb_Country
            // 
            this.lb_Country.AutoSize = true;
            this.lb_Country.Location = new System.Drawing.Point(319, 23);
            this.lb_Country.Name = "lb_Country";
            this.lb_Country.Size = new System.Drawing.Size(41, 12);
            this.lb_Country.TabIndex = 2;
            this.lb_Country.Text = "国家：";
            // 
            // txt_ChooseSupplier
            // 
            this.txt_ChooseSupplier.Location = new System.Drawing.Point(72, 20);
            this.txt_ChooseSupplier.Name = "txt_ChooseSupplier";
            this.txt_ChooseSupplier.Size = new System.Drawing.Size(209, 21);
            this.txt_ChooseSupplier.TabIndex = 1;
            this.txt_ChooseSupplier.Enter += new System.EventHandler(this.txt_ChooseSupplier_Enter);
            this.txt_ChooseSupplier.Leave += new System.EventHandler(this.txt_ChooseSupplier_Leave);
            // 
            // lb_ChooseSupplier
            // 
            this.lb_ChooseSupplier.AutoSize = true;
            this.lb_ChooseSupplier.Location = new System.Drawing.Point(13, 23);
            this.lb_ChooseSupplier.Name = "lb_ChooseSupplier";
            this.lb_ChooseSupplier.Size = new System.Drawing.Size(53, 12);
            this.lb_ChooseSupplier.TabIndex = 0;
            this.lb_ChooseSupplier.Text = "供应商：";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.panel3.Controls.Add(this.dgv_SupplierDetails);
            this.panel3.Location = new System.Drawing.Point(13, 84);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(912, 313);
            this.panel3.TabIndex = 2;
            // 
            // dgv_SupplierDetails
            // 
            this.dgv_SupplierDetails.AllowUserToAddRows = false;
            this.dgv_SupplierDetails.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgv_SupplierDetails.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_SupplierDetails.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv_SupplierDetails.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column_Choose,
            this.Column_SupplierID,
            this.Column_SupplierName,
            this.Column_Address});
            this.dgv_SupplierDetails.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_SupplierDetails.EnableHeadersVisualStyles = false;
            this.dgv_SupplierDetails.Location = new System.Drawing.Point(0, 0);
            this.dgv_SupplierDetails.Name = "dgv_SupplierDetails";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_SupplierDetails.RowHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.dgv_SupplierDetails.RowTemplate.DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dgv_SupplierDetails.RowTemplate.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.LightBlue;
            this.dgv_SupplierDetails.RowTemplate.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.Black;
            this.dgv_SupplierDetails.RowTemplate.Height = 23;
            this.dgv_SupplierDetails.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv_SupplierDetails.Size = new System.Drawing.Size(912, 313);
            this.dgv_SupplierDetails.TabIndex = 0;
            this.dgv_SupplierDetails.CellMouseEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_SupplierDetails_CellMouseEnter);
            this.dgv_SupplierDetails.CellMouseLeave += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_SupplierDetails_CellMouseLeave);
            this.dgv_SupplierDetails.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.dgv_SupplierDetails_RowPostPaint);
            // 
            // Column_Choose
            // 
            this.Column_Choose.HeaderText = "勾选";
            this.Column_Choose.Name = "Column_Choose";
            this.Column_Choose.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Column_Choose.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Column_Choose.Width = 50;
            // 
            // Column_SupplierID
            // 
            this.Column_SupplierID.DataPropertyName = "Supplier_ID";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.Column_SupplierID.DefaultCellStyle = dataGridViewCellStyle2;
            this.Column_SupplierID.HeaderText = "供应商编号";
            this.Column_SupplierID.Name = "Column_SupplierID";
            this.Column_SupplierID.Width = 150;
            // 
            // Column_SupplierName
            // 
            this.Column_SupplierName.DataPropertyName = "Supplier_Name";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.Column_SupplierName.DefaultCellStyle = dataGridViewCellStyle3;
            this.Column_SupplierName.HeaderText = "供应商名称";
            this.Column_SupplierName.Name = "Column_SupplierName";
            this.Column_SupplierName.Width = 272;
            // 
            // Column_Address
            // 
            this.Column_Address.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column_Address.DataPropertyName = "Address";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.Column_Address.DefaultCellStyle = dataGridViewCellStyle4;
            this.Column_Address.HeaderText = "所在地址";
            this.Column_Address.Name = "Column_Address";
            // 
            // btn_Confirm
            // 
            this.btn_Confirm.BackColor = System.Drawing.SystemColors.Control;
            this.btn_Confirm.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btn_Confirm.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Lime;
            this.btn_Confirm.Location = new System.Drawing.Point(661, 423);
            this.btn_Confirm.Name = "btn_Confirm";
            this.btn_Confirm.Size = new System.Drawing.Size(100, 32);
            this.btn_Confirm.TabIndex = 3;
            this.btn_Confirm.Text = "确定";
            this.btn_Confirm.UseVisualStyleBackColor = true;
            this.btn_Confirm.Click += new System.EventHandler(this.btn_Confirm_Click);
            // 
            // btn_Cancel
            // 
            this.btn_Cancel.BackColor = System.Drawing.SystemColors.Control;
            this.btn_Cancel.FlatAppearance.BorderColor = System.Drawing.Color.Gainsboro;
            this.btn_Cancel.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Red;
            this.btn_Cancel.Location = new System.Drawing.Point(767, 423);
            this.btn_Cancel.Name = "btn_Cancel";
            this.btn_Cancel.Size = new System.Drawing.Size(100, 32);
            this.btn_Cancel.TabIndex = 4;
            this.btn_Cancel.Text = "取消";
            this.btn_Cancel.UseVisualStyleBackColor = true;
            this.btn_Cancel.Click += new System.EventHandler(this.btn_Cancel_Click);
            // 
            // SupplierView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(937, 473);
            this.Controls.Add(this.btn_Cancel);
            this.Controls.Add(this.btn_Confirm);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SupplierView";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "请选择供应商";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_SupplierDetails)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button btn_Confirm;
        private System.Windows.Forms.Button btn_Cancel;
        private System.Windows.Forms.Label lb_Country;
        private System.Windows.Forms.TextBox txt_ChooseSupplier;
        private System.Windows.Forms.Label lb_ChooseSupplier;
        private System.Windows.Forms.Button btn_Search;
        private System.Windows.Forms.ComboBox cbb_Country;
        private System.Windows.Forms.DataGridView dgv_SupplierDetails;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column_Choose;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column_SupplierID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column_SupplierName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column_Address;
        private System.Windows.Forms.Label lb_Unit;
        private System.Windows.Forms.Label lb_Number;
        private System.Windows.Forms.Label lb_SearchResult;
    }
}