﻿namespace MMClient.RA.SupplierAnalysis
{
    partial class SupplierAnalysis
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel_query = new System.Windows.Forms.Panel();
            this.label_To = new System.Windows.Forms.Label();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.label_Time = new System.Windows.Forms.Label();
            this.btn_Query = new System.Windows.Forms.Button();
            this.panel_dataGridView = new System.Windows.Forms.Panel();
            this.chartViewer1 = new ChartDirector.WinChartViewer();
            this.panel_query.SuspendLayout();
            this.panel_dataGridView.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartViewer1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel_query
            // 
            this.panel_query.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panel_query.BackColor = System.Drawing.SystemColors.Control;
            this.panel_query.Controls.Add(this.label_To);
            this.panel_query.Controls.Add(this.dateTimePicker2);
            this.panel_query.Controls.Add(this.dateTimePicker1);
            this.panel_query.Controls.Add(this.label_Time);
            this.panel_query.Controls.Add(this.btn_Query);
            this.panel_query.Location = new System.Drawing.Point(13, 13);
            this.panel_query.Name = "panel_query";
            this.panel_query.Size = new System.Drawing.Size(994, 92);
            this.panel_query.TabIndex = 0;
            // 
            // label_To
            // 
            this.label_To.Location = new System.Drawing.Point(205, 32);
            this.label_To.Name = "label_To";
            this.label_To.Size = new System.Drawing.Size(20, 12);
            this.label_To.TabIndex = 4;
            this.label_To.Text = "--";
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.Location = new System.Drawing.Point(228, 28);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(129, 21);
            this.dateTimePicker2.TabIndex = 3;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(78, 28);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(121, 21);
            this.dateTimePicker1.TabIndex = 2;
            // 
            // label_Time
            // 
            this.label_Time.AutoSize = true;
            this.label_Time.Location = new System.Drawing.Point(38, 33);
            this.label_Time.Name = "label_Time";
            this.label_Time.Size = new System.Drawing.Size(41, 12);
            this.label_Time.TabIndex = 1;
            this.label_Time.Text = "时间：";
            // 
            // btn_Query
            // 
            this.btn_Query.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn_Query.Location = new System.Drawing.Point(413, 25);
            this.btn_Query.Name = "btn_Query";
            this.btn_Query.Size = new System.Drawing.Size(88, 27);
            this.btn_Query.TabIndex = 0;
            this.btn_Query.Text = "生成报表";
            this.btn_Query.UseVisualStyleBackColor = true;
            this.btn_Query.Click += new System.EventHandler(this.btn_Query_Click);
            // 
            // panel_dataGridView
            // 
            this.panel_dataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panel_dataGridView.BackColor = System.Drawing.SystemColors.Control;
            this.panel_dataGridView.Controls.Add(this.chartViewer1);
            this.panel_dataGridView.Location = new System.Drawing.Point(13, 108);
            this.panel_dataGridView.Name = "panel_dataGridView";
            this.panel_dataGridView.Size = new System.Drawing.Size(994, 390);
            this.panel_dataGridView.TabIndex = 1;
            // 
            // chartViewer1
            // 
            this.chartViewer1.HotSpotCursor = System.Windows.Forms.Cursors.Hand;
            this.chartViewer1.Location = new System.Drawing.Point(3, 3);
            this.chartViewer1.Name = "chartViewer1";
            this.chartViewer1.Size = new System.Drawing.Size(168, 126);
            this.chartViewer1.TabIndex = 3;
            this.chartViewer1.TabStop = false;
            this.chartViewer1.ClickHotSpot += new ChartDirector.WinHotSpotEventHandler(this.chartViewer1_ClickHotSpot);
            // 
            // SupplierAnalysis
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1019, 500);
            this.Controls.Add(this.panel_dataGridView);
            this.Controls.Add(this.panel_query);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "SupplierAnalysis";
            this.Text = "供应商分析";
            this.panel_query.ResumeLayout(false);
            this.panel_query.PerformLayout();
            this.panel_dataGridView.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chartViewer1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel_query;
        private System.Windows.Forms.Panel panel_dataGridView;
        private System.Windows.Forms.Button btn_Query;
        private ChartDirector.WinChartViewer chartViewer1;
        private System.Windows.Forms.Label label_To;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Label label_Time;
    }
}