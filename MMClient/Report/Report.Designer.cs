﻿namespace MMClient
{
    partial class Report
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            WeifenLuo.WinFormsUI.Docking.DockPanelSkin dockPanelSkin1 = new WeifenLuo.WinFormsUI.Docking.DockPanelSkin();
            WeifenLuo.WinFormsUI.Docking.AutoHideStripSkin autoHideStripSkin1 = new WeifenLuo.WinFormsUI.Docking.AutoHideStripSkin();
            WeifenLuo.WinFormsUI.Docking.DockPanelGradient dockPanelGradient1 = new WeifenLuo.WinFormsUI.Docking.DockPanelGradient();
            WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient1 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
            WeifenLuo.WinFormsUI.Docking.DockPaneStripSkin dockPaneStripSkin1 = new WeifenLuo.WinFormsUI.Docking.DockPaneStripSkin();
            WeifenLuo.WinFormsUI.Docking.DockPaneStripGradient dockPaneStripGradient1 = new WeifenLuo.WinFormsUI.Docking.DockPaneStripGradient();
            WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient2 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
            WeifenLuo.WinFormsUI.Docking.DockPanelGradient dockPanelGradient2 = new WeifenLuo.WinFormsUI.Docking.DockPanelGradient();
            WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient3 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
            WeifenLuo.WinFormsUI.Docking.DockPaneStripToolWindowGradient dockPaneStripToolWindowGradient1 = new WeifenLuo.WinFormsUI.Docking.DockPaneStripToolWindowGradient();
            WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient4 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
            WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient5 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
            WeifenLuo.WinFormsUI.Docking.DockPanelGradient dockPanelGradient3 = new WeifenLuo.WinFormsUI.Docking.DockPanelGradient();
            WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient6 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
            WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient7 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
            System.Windows.Forms.TreeNode treeNode1 = new System.Windows.Forms.TreeNode("物料价格变化分析", 1, 1);
            System.Windows.Forms.TreeNode treeNode2 = new System.Windows.Forms.TreeNode("物料的ABC类分析", 1, 1);
            System.Windows.Forms.TreeNode treeNode3 = new System.Windows.Forms.TreeNode("供应商的ABC类分析", 1, 1);
            System.Windows.Forms.TreeNode treeNode4 = new System.Windows.Forms.TreeNode("物料报价分析", 1, 1);
            System.Windows.Forms.TreeNode treeNode5 = new System.Windows.Forms.TreeNode("采购监控报表", 1, 1);
            System.Windows.Forms.TreeNode treeNode6 = new System.Windows.Forms.TreeNode("采购监控分析", new System.Windows.Forms.TreeNode[] {
            treeNode1,
            treeNode2,
            treeNode3,
            treeNode4,
            treeNode5});
            System.Windows.Forms.TreeNode treeNode7 = new System.Windows.Forms.TreeNode("物料采购花费分布", 1, 1);
            System.Windows.Forms.TreeNode treeNode8 = new System.Windows.Forms.TreeNode("成本结构分析", 1, 1);
            System.Windows.Forms.TreeNode treeNode9 = new System.Windows.Forms.TreeNode("成本比较分析", 1, 1);
            System.Windows.Forms.TreeNode treeNode10 = new System.Windows.Forms.TreeNode("成本趋势分析", 1, 1);
            System.Windows.Forms.TreeNode treeNode11 = new System.Windows.Forms.TreeNode("采购花费报表", 1, 1);
            System.Windows.Forms.TreeNode treeNode12 = new System.Windows.Forms.TreeNode("采购历史清单", 1, 1);
            System.Windows.Forms.TreeNode treeNode13 = new System.Windows.Forms.TreeNode("采购花费分析", new System.Windows.Forms.TreeNode[] {
            treeNode7,
            treeNode8,
            treeNode9,
            treeNode10,
            treeNode11,
            treeNode12});
            System.Windows.Forms.TreeNode treeNode14 = new System.Windows.Forms.TreeNode("供应商历史价格", 1, 1);
            System.Windows.Forms.TreeNode treeNode15 = new System.Windows.Forms.TreeNode("价格分析", 1, 1);
            System.Windows.Forms.TreeNode treeNode16 = new System.Windows.Forms.TreeNode("供应商竞价分析", 1, 1);
            System.Windows.Forms.TreeNode treeNode17 = new System.Windows.Forms.TreeNode("寻源分析", new System.Windows.Forms.TreeNode[] {
            treeNode14,
            treeNode15,
            treeNode16});
            System.Windows.Forms.TreeNode treeNode18 = new System.Windows.Forms.TreeNode("合同执行进度", 1, 1);
            System.Windows.Forms.TreeNode treeNode19 = new System.Windows.Forms.TreeNode("采购合同会签明细", 1, 1);
            System.Windows.Forms.TreeNode treeNode20 = new System.Windows.Forms.TreeNode("合同管理分析", 1, 1);
            System.Windows.Forms.TreeNode treeNode21 = new System.Windows.Forms.TreeNode("合同执行分析", new System.Windows.Forms.TreeNode[] {
            treeNode18,
            treeNode19,
            treeNode20});
            System.Windows.Forms.TreeNode treeNode22 = new System.Windows.Forms.TreeNode("供应商对比雷达图", 1, 1);
            System.Windows.Forms.TreeNode treeNode23 = new System.Windows.Forms.TreeNode("供应商趋势报表", 1, 1);
            System.Windows.Forms.TreeNode treeNode24 = new System.Windows.Forms.TreeNode("供应商得分排名", 1, 1);
            System.Windows.Forms.TreeNode treeNode25 = new System.Windows.Forms.TreeNode("供应商绩效分析", new System.Windows.Forms.TreeNode[] {
            treeNode22,
            treeNode23,
            treeNode24});
            System.Windows.Forms.TreeNode treeNode26 = new System.Windows.Forms.TreeNode("库龄分析明细表", 1, 1);
            System.Windows.Forms.TreeNode treeNode27 = new System.Windows.Forms.TreeNode("库存物料ABC类分析", 1, 1);
            System.Windows.Forms.TreeNode treeNode28 = new System.Windows.Forms.TreeNode("库存分析", new System.Windows.Forms.TreeNode[] {
            treeNode26,
            treeNode27});
            System.Windows.Forms.TreeNode treeNode29 = new System.Windows.Forms.TreeNode("供应分析", 1, 1);
            System.Windows.Forms.TreeNode treeNode30 = new System.Windows.Forms.TreeNode("供应商依赖因素分析", 1, 1);
            System.Windows.Forms.TreeNode treeNode31 = new System.Windows.Forms.TreeNode("定制", 1, 1);
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Report));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.dockPanel1 = new WeifenLuo.WinFormsUI.Docking.DockPanel();
            this.tv_Rt = new System.Windows.Forms.TreeView();
            this.treeView_imageList = new System.Windows.Forms.ImageList(this.components);
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 13.85542F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 86.14458F));
            this.tableLayoutPanel1.Controls.Add(this.dockPanel1, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.tv_Rt, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 575F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 575F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 575F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 575F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1162, 575);
            this.tableLayoutPanel1.TabIndex = 0;
            this.tableLayoutPanel1.Paint += new System.Windows.Forms.PaintEventHandler(this.tableLayoutPanel1_Paint);
            // 
            // dockPanel1
            // 
            this.dockPanel1.ActiveAutoHideContent = null;
            this.dockPanel1.AutoScroll = true;
            this.dockPanel1.AutoSize = true;
            this.dockPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dockPanel1.DockBackColor = System.Drawing.SystemColors.AppWorkspace;
            this.dockPanel1.DocumentStyle = WeifenLuo.WinFormsUI.Docking.DocumentStyle.DockingWindow;
            this.dockPanel1.Location = new System.Drawing.Point(163, 3);
            this.dockPanel1.Name = "dockPanel1";
            this.dockPanel1.Size = new System.Drawing.Size(996, 569);
            dockPanelGradient1.EndColor = System.Drawing.SystemColors.ControlLight;
            dockPanelGradient1.StartColor = System.Drawing.SystemColors.ControlLight;
            autoHideStripSkin1.DockStripGradient = dockPanelGradient1;
            tabGradient1.EndColor = System.Drawing.SystemColors.Control;
            tabGradient1.StartColor = System.Drawing.SystemColors.Control;
            tabGradient1.TextColor = System.Drawing.SystemColors.ControlDarkDark;
            autoHideStripSkin1.TabGradient = tabGradient1;
            dockPanelSkin1.AutoHideStripSkin = autoHideStripSkin1;
            tabGradient2.EndColor = System.Drawing.SystemColors.ControlLightLight;
            tabGradient2.StartColor = System.Drawing.SystemColors.ControlLightLight;
            tabGradient2.TextColor = System.Drawing.SystemColors.ControlText;
            dockPaneStripGradient1.ActiveTabGradient = tabGradient2;
            dockPanelGradient2.EndColor = System.Drawing.SystemColors.Control;
            dockPanelGradient2.StartColor = System.Drawing.SystemColors.Control;
            dockPaneStripGradient1.DockStripGradient = dockPanelGradient2;
            tabGradient3.EndColor = System.Drawing.SystemColors.ControlLight;
            tabGradient3.StartColor = System.Drawing.SystemColors.ControlLight;
            tabGradient3.TextColor = System.Drawing.SystemColors.ControlText;
            dockPaneStripGradient1.InactiveTabGradient = tabGradient3;
            dockPaneStripSkin1.DocumentGradient = dockPaneStripGradient1;
            tabGradient4.EndColor = System.Drawing.SystemColors.ActiveCaption;
            tabGradient4.LinearGradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            tabGradient4.StartColor = System.Drawing.SystemColors.GradientActiveCaption;
            tabGradient4.TextColor = System.Drawing.SystemColors.ActiveCaptionText;
            dockPaneStripToolWindowGradient1.ActiveCaptionGradient = tabGradient4;
            tabGradient5.EndColor = System.Drawing.SystemColors.Control;
            tabGradient5.StartColor = System.Drawing.SystemColors.Control;
            tabGradient5.TextColor = System.Drawing.SystemColors.ControlText;
            dockPaneStripToolWindowGradient1.ActiveTabGradient = tabGradient5;
            dockPanelGradient3.EndColor = System.Drawing.SystemColors.ControlLight;
            dockPanelGradient3.StartColor = System.Drawing.SystemColors.ControlLight;
            dockPaneStripToolWindowGradient1.DockStripGradient = dockPanelGradient3;
            tabGradient6.EndColor = System.Drawing.SystemColors.GradientInactiveCaption;
            tabGradient6.LinearGradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            tabGradient6.StartColor = System.Drawing.SystemColors.GradientInactiveCaption;
            tabGradient6.TextColor = System.Drawing.SystemColors.ControlText;
            dockPaneStripToolWindowGradient1.InactiveCaptionGradient = tabGradient6;
            tabGradient7.EndColor = System.Drawing.Color.Transparent;
            tabGradient7.StartColor = System.Drawing.Color.Transparent;
            tabGradient7.TextColor = System.Drawing.SystemColors.ControlDarkDark;
            dockPaneStripToolWindowGradient1.InactiveTabGradient = tabGradient7;
            dockPaneStripSkin1.ToolWindowGradient = dockPaneStripToolWindowGradient1;
            dockPanelSkin1.DockPaneStripSkin = dockPaneStripSkin1;
            this.dockPanel1.Skin = dockPanelSkin1;
            this.dockPanel1.TabIndex = 1;
            // 
            // tv_Rt
            // 
            this.tv_Rt.BackColor = System.Drawing.SystemColors.Window;
            this.tv_Rt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tv_Rt.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tv_Rt.ImageIndex = 0;
            this.tv_Rt.ImageList = this.treeView_imageList;
            this.tv_Rt.Location = new System.Drawing.Point(3, 3);
            this.tv_Rt.Name = "tv_Rt";
            treeNode1.ImageIndex = 1;
            treeNode1.Name = "物料价格变化分析";
            treeNode1.SelectedImageIndex = 1;
            treeNode1.Tag = "111";
            treeNode1.Text = "物料价格变化分析";
            treeNode2.ImageIndex = 1;
            treeNode2.Name = "物料的ABC类分析";
            treeNode2.SelectedImageIndex = 1;
            treeNode2.Tag = "14";
            treeNode2.Text = "物料的ABC类分析";
            treeNode3.ImageIndex = 1;
            treeNode3.Name = "供应商的ABC类分析";
            treeNode3.SelectedImageIndex = 1;
            treeNode3.Tag = "15";
            treeNode3.Text = "供应商的ABC类分析";
            treeNode4.ImageIndex = 1;
            treeNode4.Name = "物料报价分析";
            treeNode4.SelectedImageIndex = 1;
            treeNode4.Tag = "112";
            treeNode4.Text = "物料报价分析";
            treeNode5.ImageIndex = 1;
            treeNode5.Name = "节点0";
            treeNode5.SelectedImageIndex = 1;
            treeNode5.Tag = "121";
            treeNode5.Text = "采购监控报表";
            treeNode6.ImageIndex = 0;
            treeNode6.Name = "采购监控分析";
            treeNode6.Tag = "14";
            treeNode6.Text = "采购监控分析";
            treeNode6.ToolTipText = "采购监控分析";
            treeNode7.ImageIndex = 1;
            treeNode7.Name = "物料采购花费分布";
            treeNode7.SelectedImageIndex = 1;
            treeNode7.Tag = "211";
            treeNode7.Text = "物料采购花费分布";
            treeNode8.ImageIndex = 1;
            treeNode8.Name = "CostStructureAnalysis";
            treeNode8.SelectedImageIndex = 1;
            treeNode8.Tag = "212";
            treeNode8.Text = "成本结构分析";
            treeNode9.ImageIndex = 1;
            treeNode9.Name = "CostComparativeAnalysis";
            treeNode9.SelectedImageIndex = 1;
            treeNode9.Tag = "213";
            treeNode9.Text = "成本比较分析";
            treeNode10.ImageIndex = 1;
            treeNode10.Name = "CostTrendAnalysis";
            treeNode10.SelectedImageIndex = 1;
            treeNode10.Tag = "214";
            treeNode10.Text = "成本趋势分析";
            treeNode11.ImageIndex = 1;
            treeNode11.Name = "节点1";
            treeNode11.SelectedImageIndex = 1;
            treeNode11.Tag = "221";
            treeNode11.Text = "采购花费报表";
            treeNode12.ImageIndex = 1;
            treeNode12.Name = "采购历史清单";
            treeNode12.SelectedImageIndex = 1;
            treeNode12.Tag = "222";
            treeNode12.Text = "采购历史清单";
            treeNode13.ImageIndex = 0;
            treeNode13.Name = "采购花费分析";
            treeNode13.Tag = "214";
            treeNode13.Text = "采购花费分析";
            treeNode13.ToolTipText = "采购花费分析";
            treeNode14.ImageIndex = 1;
            treeNode14.Name = "供应商历史价格";
            treeNode14.SelectedImageIndex = 1;
            treeNode14.Tag = "311";
            treeNode14.Text = "供应商历史价格";
            treeNode15.ImageIndex = 1;
            treeNode15.Name = "PriceAnaysis";
            treeNode15.SelectedImageIndex = 1;
            treeNode15.Tag = "312";
            treeNode15.Text = "价格分析";
            treeNode16.ImageIndex = 1;
            treeNode16.Name = "SupplierBidingAnalysis";
            treeNode16.SelectedImageIndex = 1;
            treeNode16.Tag = "313";
            treeNode16.Text = "供应商竞价分析";
            treeNode17.ImageIndex = 0;
            treeNode17.Name = "寻源分析";
            treeNode17.Tag = "311";
            treeNode17.Text = "寻源分析";
            treeNode17.ToolTipText = "寻源分析";
            treeNode18.ImageIndex = 1;
            treeNode18.Name = "合同执行进度";
            treeNode18.SelectedImageIndex = 1;
            treeNode18.Tag = "411";
            treeNode18.Text = "合同执行进度";
            treeNode19.ImageIndex = 1;
            treeNode19.Name = "采购合同会签明细";
            treeNode19.SelectedImageIndex = 1;
            treeNode19.Tag = "421";
            treeNode19.Text = "采购合同会签明细";
            treeNode20.ImageIndex = 1;
            treeNode20.Name = "ConstractionManagement";
            treeNode20.SelectedImageIndex = 1;
            treeNode20.Tag = "413";
            treeNode20.Text = "合同管理分析";
            treeNode21.ImageIndex = 0;
            treeNode21.Name = "合同执行分析";
            treeNode21.Tag = "411";
            treeNode21.Text = "合同执行分析";
            treeNode21.ToolTipText = "合同执行分析";
            treeNode22.ImageIndex = 1;
            treeNode22.Name = "供应商对比雷达图";
            treeNode22.SelectedImageIndex = 1;
            treeNode22.Tag = "51";
            treeNode22.Text = "供应商对比雷达图";
            treeNode23.ImageIndex = 1;
            treeNode23.Name = "供应商趋势报表";
            treeNode23.SelectedImageIndex = 1;
            treeNode23.Tag = "52";
            treeNode23.Text = "供应商趋势报表";
            treeNode24.ImageIndex = 1;
            treeNode24.Name = "供应商得分排名";
            treeNode24.SelectedImageIndex = 1;
            treeNode24.Tag = "53";
            treeNode24.Text = "供应商得分排名";
            treeNode25.ImageIndex = 0;
            treeNode25.Name = "供应商绩效分析";
            treeNode25.Tag = "51";
            treeNode25.Text = "供应商绩效分析";
            treeNode25.ToolTipText = "供应商绩效分析";
            treeNode26.ImageIndex = 1;
            treeNode26.Name = "库龄分析明细表";
            treeNode26.SelectedImageIndex = 1;
            treeNode26.Tag = "61";
            treeNode26.Text = "库龄分析明细表";
            treeNode27.ImageIndex = 1;
            treeNode27.Name = "库存物料ABC类分析";
            treeNode27.SelectedImageIndex = 1;
            treeNode27.Tag = "62";
            treeNode27.Text = "库存物料ABC类分析";
            treeNode28.ImageIndex = 0;
            treeNode28.Name = "库存分析";
            treeNode28.Tag = "61";
            treeNode28.Text = "库存分析";
            treeNode28.ToolTipText = "库存分析";
            treeNode29.ImageIndex = 1;
            treeNode29.Name = "供应分析";
            treeNode29.SelectedImageIndex = 1;
            treeNode29.Tag = "7";
            treeNode29.Text = "供应分析";
            treeNode30.ImageIndex = 1;
            treeNode30.Name = "供应商依赖因素分析";
            treeNode30.SelectedImageIndex = 1;
            treeNode30.Tag = "8";
            treeNode30.Text = "供应商依赖因素分析";
            treeNode31.ImageIndex = 1;
            treeNode31.Name = "定制";
            treeNode31.SelectedImageIndex = 1;
            treeNode31.Tag = "9";
            treeNode31.Text = "定制";
            treeNode31.ToolTipText = "定制";
            this.tv_Rt.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode6,
            treeNode13,
            treeNode17,
            treeNode21,
            treeNode25,
            treeNode28,
            treeNode29,
            treeNode30,
            treeNode31});
            this.tv_Rt.SelectedImageIndex = 0;
            this.tv_Rt.ShowLines = false;
            this.tv_Rt.ShowNodeToolTips = true;
            this.tv_Rt.Size = new System.Drawing.Size(154, 569);
            this.tv_Rt.StateImageList = this.treeView_imageList;
            this.tv_Rt.TabIndex = 0;
            this.tv_Rt.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.tv_Rt_AfterSelect);
            // 
            // treeView_imageList
            // 
            this.treeView_imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("treeView_imageList.ImageStream")));
            this.treeView_imageList.TransparentColor = System.Drawing.Color.Transparent;
            this.treeView_imageList.Images.SetKeyName(0, "Folder2.png");
            this.treeView_imageList.Images.SetKeyName(1, "Table4.png");
            // 
            // Report
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1162, 575);
            this.Controls.Add(this.tableLayoutPanel1);
            this.IsMdiContainer = true;
            this.Name = "Report";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "报表分析";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

//        private System.Windows.Forms.BindingSource DemandBindingSource;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TreeView tv_Rt;
        private WeifenLuo.WinFormsUI.Docking.DockPanel dockPanel1;
        private System.Windows.Forms.ImageList treeView_imageList;
        //private MMDataSet MMDataSet;
        //private MMDataSetTableAdapters.DemandTableAdapter DemandTableAdapter;
    }
}