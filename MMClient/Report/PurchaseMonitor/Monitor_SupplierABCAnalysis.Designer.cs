﻿namespace MMClient.RA.PurchaseMonitor
{
    partial class Monitor_SupplierABCAnalysis
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.gb_Query = new System.Windows.Forms.GroupBox();
            this.cbb_Type = new System.Windows.Forms.ComboBox();
            this.lb_Type = new System.Windows.Forms.Label();
            this.btn_Query = new System.Windows.Forms.Button();
            this.dtp_EndTime = new System.Windows.Forms.DateTimePicker();
            this.lb_To = new System.Windows.Forms.Label();
            this.dtp_StartTime = new System.Windows.Forms.DateTimePicker();
            this.lb_Time = new System.Windows.Forms.Label();
            this.panel_Chart = new System.Windows.Forms.Panel();
            this.chartViewer1 = new ChartDirector.WinChartViewer();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Column_SupplierName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column_Purchase = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column_Share = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column_CumShare = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column_CumQShare = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ChartPageSetupDialog = new System.Windows.Forms.PageSetupDialog();
            this.ChartPrintDocument = new System.Drawing.Printing.PrintDocument();
            this.DetailChartPageSetupDialog = new System.Windows.Forms.PageSetupDialog();
            this.DetailChartPrintDocument = new System.Drawing.Printing.PrintDocument();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btn_DetailChartPreview = new System.Windows.Forms.Button();
            this.label_DetaiChart = new System.Windows.Forms.Label();
            this.btn_DetailChartPrint = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btn_CSupplier = new System.Windows.Forms.Button();
            this.btnBASupplier = new System.Windows.Forms.Button();
            this.btn_ASupplier = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btn_Preview = new System.Windows.Forms.Button();
            this.lb_ChartTitle = new System.Windows.Forms.Label();
            this.btn_Print = new System.Windows.Forms.Button();
            this.gb_Query.SuspendLayout();
            this.panel_Chart.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartViewer1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // gb_Query
            // 
            this.gb_Query.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.gb_Query.BackColor = System.Drawing.SystemColors.Control;
            this.gb_Query.Controls.Add(this.cbb_Type);
            this.gb_Query.Controls.Add(this.lb_Type);
            this.gb_Query.Controls.Add(this.btn_Query);
            this.gb_Query.Controls.Add(this.dtp_EndTime);
            this.gb_Query.Controls.Add(this.lb_To);
            this.gb_Query.Controls.Add(this.dtp_StartTime);
            this.gb_Query.Controls.Add(this.lb_Time);
            this.gb_Query.Location = new System.Drawing.Point(12, 12);
            this.gb_Query.Name = "gb_Query";
            this.gb_Query.Size = new System.Drawing.Size(1153, 92);
            this.gb_Query.TabIndex = 1;
            this.gb_Query.TabStop = false;
            this.gb_Query.Text = "查询条件";
            // 
            // cbb_Type
            // 
            this.cbb_Type.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbb_Type.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cbb_Type.FormattingEnabled = true;
            this.cbb_Type.Items.AddRange(new object[] {
            "所有",
            "A类供应商",
            "B类供应商",
            "C类供应商"});
            this.cbb_Type.Location = new System.Drawing.Point(387, 33);
            this.cbb_Type.Name = "cbb_Type";
            this.cbb_Type.Size = new System.Drawing.Size(125, 20);
            this.cbb_Type.TabIndex = 6;
            // 
            // lb_Type
            // 
            this.lb_Type.AutoSize = true;
            this.lb_Type.Location = new System.Drawing.Point(350, 37);
            this.lb_Type.Name = "lb_Type";
            this.lb_Type.Size = new System.Drawing.Size(41, 12);
            this.lb_Type.TabIndex = 5;
            this.lb_Type.Text = "类别：";
            // 
            // btn_Query
            // 
            this.btn_Query.Location = new System.Drawing.Point(587, 29);
            this.btn_Query.Name = "btn_Query";
            this.btn_Query.Size = new System.Drawing.Size(107, 29);
            this.btn_Query.TabIndex = 4;
            this.btn_Query.Text = "查看";
            this.btn_Query.UseVisualStyleBackColor = true;
            this.btn_Query.Click += new System.EventHandler(this.btn_Query_Click);
            // 
            // dtp_EndTime
            // 
            this.dtp_EndTime.Location = new System.Drawing.Point(204, 33);
            this.dtp_EndTime.Name = "dtp_EndTime";
            this.dtp_EndTime.Size = new System.Drawing.Size(122, 21);
            this.dtp_EndTime.TabIndex = 3;
            // 
            // lb_To
            // 
            this.lb_To.AutoSize = true;
            this.lb_To.Location = new System.Drawing.Point(181, 37);
            this.lb_To.Name = "lb_To";
            this.lb_To.Size = new System.Drawing.Size(17, 12);
            this.lb_To.TabIndex = 2;
            this.lb_To.Text = "--";
            // 
            // dtp_StartTime
            // 
            this.dtp_StartTime.Location = new System.Drawing.Point(53, 33);
            this.dtp_StartTime.Name = "dtp_StartTime";
            this.dtp_StartTime.Size = new System.Drawing.Size(122, 21);
            this.dtp_StartTime.TabIndex = 1;
            // 
            // lb_Time
            // 
            this.lb_Time.AutoSize = true;
            this.lb_Time.Location = new System.Drawing.Point(15, 37);
            this.lb_Time.Name = "lb_Time";
            this.lb_Time.Size = new System.Drawing.Size(41, 12);
            this.lb_Time.TabIndex = 0;
            this.lb_Time.Text = "时间：";
            // 
            // panel_Chart
            // 
            this.panel_Chart.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panel_Chart.BackColor = System.Drawing.SystemColors.Control;
            this.panel_Chart.Controls.Add(this.chartViewer1);
            this.panel_Chart.Location = new System.Drawing.Point(509, 150);
            this.panel_Chart.Name = "panel_Chart";
            this.panel_Chart.Size = new System.Drawing.Size(656, 476);
            this.panel_Chart.TabIndex = 4;
            // 
            // chartViewer1
            // 
            this.chartViewer1.HotSpotCursor = System.Windows.Forms.Cursors.Hand;
            this.chartViewer1.Location = new System.Drawing.Point(17, 14);
            this.chartViewer1.Name = "chartViewer1";
            this.chartViewer1.Size = new System.Drawing.Size(168, 126);
            this.chartViewer1.TabIndex = 2;
            this.chartViewer1.TabStop = false;
            this.chartViewer1.ClickHotSpot += new ChartDirector.WinHotSpotEventHandler(this.chartViewer1_ClickHotSpot);
            // 
            // dataGridView1
            // 
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.ColumnHeadersHeight = 30;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column_SupplierName,
            this.Column_Purchase,
            this.Column_Share,
            this.Column_CumShare,
            this.Column_CumQShare});
            this.dataGridView1.EnableHeadersVisualStyles = false;
            this.dataGridView1.Location = new System.Drawing.Point(12, 206);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.LightBlue;
            this.dataGridView1.RowTemplate.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.Black;
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.RowTemplate.ReadOnly = true;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(491, 421);
            this.dataGridView1.TabIndex = 5;
            this.dataGridView1.CellMouseEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellMouseEnter);
            this.dataGridView1.CellMouseLeave += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellMouseLeave);
            this.dataGridView1.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.dataGridView1_RowPostPaint);
            // 
            // Column_SupplierName
            // 
            this.Column_SupplierName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column_SupplierName.DataPropertyName = "Name";
            this.Column_SupplierName.HeaderText = "供应商名称";
            this.Column_SupplierName.Name = "Column_SupplierName";
            this.Column_SupplierName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Column_Purchase
            // 
            this.Column_Purchase.DataPropertyName = "TotalPrice";
            this.Column_Purchase.HeaderText = "金额";
            this.Column_Purchase.Name = "Column_Purchase";
            this.Column_Purchase.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Column_Share
            // 
            this.Column_Share.DataPropertyName = "Share";
            dataGridViewCellStyle2.Format = "0.00%";
            dataGridViewCellStyle2.NullValue = null;
            this.Column_Share.DefaultCellStyle = dataGridViewCellStyle2;
            this.Column_Share.HeaderText = "占比";
            this.Column_Share.Name = "Column_Share";
            this.Column_Share.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Column_CumShare
            // 
            this.Column_CumShare.DataPropertyName = "CumShare";
            dataGridViewCellStyle3.Format = "0.00%";
            dataGridViewCellStyle3.NullValue = null;
            this.Column_CumShare.DefaultCellStyle = dataGridViewCellStyle3;
            this.Column_CumShare.HeaderText = "累计占比";
            this.Column_CumShare.Name = "Column_CumShare";
            this.Column_CumShare.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Column_CumQShare
            // 
            this.Column_CumQShare.DataPropertyName = "QCumShare";
            this.Column_CumQShare.HeaderText = "品种累计占比";
            this.Column_CumQShare.Name = "Column_CumQShare";
            this.Column_CumQShare.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column_CumQShare.Visible = false;
            // 
            // ChartPageSetupDialog
            // 
            this.ChartPageSetupDialog.Document = this.ChartPrintDocument;
            // 
            // ChartPrintDocument
            // 
            this.ChartPrintDocument.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.ChartPrintDocument_PrintPage);
            // 
            // DetailChartPageSetupDialog
            // 
            this.DetailChartPageSetupDialog.Document = this.DetailChartPrintDocument;
            // 
            // DetailChartPrintDocument
            // 
            this.DetailChartPrintDocument.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.DetailChartPrintDocument_PrintPage);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn1.DataPropertyName = "Name";
            this.dataGridViewTextBoxColumn1.HeaderText = "供应商名称";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "TotalPrice";
            this.dataGridViewTextBoxColumn2.HeaderText = "金额";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "Share";
            dataGridViewCellStyle4.Format = "0.00%";
            dataGridViewCellStyle4.NullValue = null;
            this.dataGridViewTextBoxColumn3.DefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridViewTextBoxColumn3.HeaderText = "占比";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "CumShare";
            dataGridViewCellStyle5.Format = "0.00%";
            dataGridViewCellStyle5.NullValue = null;
            this.dataGridViewTextBoxColumn4.DefaultCellStyle = dataGridViewCellStyle5;
            this.dataGridViewTextBoxColumn4.HeaderText = "累计占比";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "QCumShare";
            this.dataGridViewTextBoxColumn5.HeaderText = "品种累计占比";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn5.Visible = false;
            // 
            // supplierInforGroupBox
            // 
            this.groupBox1.Controls.Add(this.btn_DetailChartPreview);
            this.groupBox1.Controls.Add(this.label_DetaiChart);
            this.groupBox1.Controls.Add(this.btn_DetailChartPrint);
            this.groupBox1.Location = new System.Drawing.Point(12, 161);
            this.groupBox1.Name = "supplierInforGroupBox";
            this.groupBox1.Size = new System.Drawing.Size(491, 41);
            this.groupBox1.TabIndex = 12;
            this.groupBox1.TabStop = false;
            // 
            // btn_DetailChartPreview
            // 
            this.btn_DetailChartPreview.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btn_DetailChartPreview.Location = new System.Drawing.Point(432, 13);
            this.btn_DetailChartPreview.Name = "btn_DetailChartPreview";
            this.btn_DetailChartPreview.Size = new System.Drawing.Size(53, 23);
            this.btn_DetailChartPreview.TabIndex = 2;
            this.btn_DetailChartPreview.Text = "预览";
            this.btn_DetailChartPreview.UseVisualStyleBackColor = true;
            // 
            // label_DetaiChart
            // 
            this.label_DetaiChart.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label_DetaiChart.Location = new System.Drawing.Point(4, 15);
            this.label_DetaiChart.Name = "label_DetaiChart";
            this.label_DetaiChart.Size = new System.Drawing.Size(220, 19);
            this.label_DetaiChart.TabIndex = 0;
            this.label_DetaiChart.Text = "供应商ABC类分析明细表";
            // 
            // btn_DetailChartPrint
            // 
            this.btn_DetailChartPrint.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btn_DetailChartPrint.Location = new System.Drawing.Point(374, 13);
            this.btn_DetailChartPrint.Name = "btn_DetailChartPrint";
            this.btn_DetailChartPrint.Size = new System.Drawing.Size(52, 23);
            this.btn_DetailChartPrint.TabIndex = 1;
            this.btn_DetailChartPrint.Text = "打印";
            this.btn_DetailChartPrint.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btn_CSupplier);
            this.groupBox3.Controls.Add(this.btnBASupplier);
            this.groupBox3.Controls.Add(this.btn_ASupplier);
            this.groupBox3.Location = new System.Drawing.Point(12, 106);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(491, 59);
            this.groupBox3.TabIndex = 13;
            this.groupBox3.TabStop = false;
            // 
            // btn_CSupplier
            // 
            this.btn_CSupplier.Location = new System.Drawing.Point(319, 17);
            this.btn_CSupplier.Name = "btn_CSupplier";
            this.btn_CSupplier.Size = new System.Drawing.Size(99, 31);
            this.btn_CSupplier.TabIndex = 2;
            this.btn_CSupplier.Text = "C类供应商";
            this.btn_CSupplier.UseVisualStyleBackColor = true;
            this.btn_CSupplier.Click += new System.EventHandler(this.btn_CSupplier_Click);
            // 
            // btnBASupplier
            // 
            this.btnBASupplier.Location = new System.Drawing.Point(176, 17);
            this.btnBASupplier.Name = "btnBASupplier";
            this.btnBASupplier.Size = new System.Drawing.Size(99, 31);
            this.btnBASupplier.TabIndex = 1;
            this.btnBASupplier.Text = "B类供应商";
            this.btnBASupplier.UseVisualStyleBackColor = true;
            this.btnBASupplier.Click += new System.EventHandler(this.btnBASupplier_Click);
            // 
            // btn_ASupplier
            // 
            this.btn_ASupplier.Location = new System.Drawing.Point(33, 17);
            this.btn_ASupplier.Name = "btn_ASupplier";
            this.btn_ASupplier.Size = new System.Drawing.Size(99, 31);
            this.btn_ASupplier.TabIndex = 0;
            this.btn_ASupplier.Text = "A类供应商";
            this.btn_ASupplier.UseVisualStyleBackColor = true;
            this.btn_ASupplier.Click += new System.EventHandler(this.btn_ASupplier_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.btn_Preview);
            this.groupBox2.Controls.Add(this.lb_ChartTitle);
            this.groupBox2.Controls.Add(this.btn_Print);
            this.groupBox2.Location = new System.Drawing.Point(509, 106);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(656, 41);
            this.groupBox2.TabIndex = 14;
            this.groupBox2.TabStop = false;
            // 
            // btn_Preview
            // 
            this.btn_Preview.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btn_Preview.Location = new System.Drawing.Point(599, 12);
            this.btn_Preview.Name = "btn_Preview";
            this.btn_Preview.Size = new System.Drawing.Size(53, 23);
            this.btn_Preview.TabIndex = 2;
            this.btn_Preview.Text = "预览";
            this.btn_Preview.UseVisualStyleBackColor = true;
            // 
            // lb_ChartTitle
            // 
            this.lb_ChartTitle.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lb_ChartTitle.Location = new System.Drawing.Point(3, 16);
            this.lb_ChartTitle.Name = "lb_ChartTitle";
            this.lb_ChartTitle.Size = new System.Drawing.Size(156, 19);
            this.lb_ChartTitle.TabIndex = 0;
            this.lb_ChartTitle.Text = "供应商ABC类分析图";
            // 
            // btn_Print
            // 
            this.btn_Print.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btn_Print.Location = new System.Drawing.Point(541, 12);
            this.btn_Print.Name = "btn_Print";
            this.btn_Print.Size = new System.Drawing.Size(52, 23);
            this.btn_Print.TabIndex = 1;
            this.btn_Print.Text = "打印";
            this.btn_Print.UseVisualStyleBackColor = true;
            // 
            // Monitor_SupplierABCAnalysis
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1169, 639);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.panel_Chart);
            this.Controls.Add(this.gb_Query);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "Monitor_SupplierABCAnalysis";
            this.Text = "供应商的ABC类分析";
            this.gb_Query.ResumeLayout(false);
            this.gb_Query.PerformLayout();
            this.panel_Chart.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chartViewer1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gb_Query;
        private System.Windows.Forms.ComboBox cbb_Type;
        private System.Windows.Forms.Label lb_Type;
        private System.Windows.Forms.Button btn_Query;
        private System.Windows.Forms.DateTimePicker dtp_EndTime;
        private System.Windows.Forms.Label lb_To;
        private System.Windows.Forms.DateTimePicker dtp_StartTime;
        private System.Windows.Forms.Label lb_Time;
        private System.Windows.Forms.Panel panel_Chart;
        private ChartDirector.WinChartViewer chartViewer1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column_SupplierName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column_Purchase;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column_Share;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column_CumShare;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column_CumQShare;
        private System.Windows.Forms.PageSetupDialog ChartPageSetupDialog;
        private System.Drawing.Printing.PrintDocument ChartPrintDocument;
        private System.Windows.Forms.PageSetupDialog DetailChartPageSetupDialog;
        private System.Drawing.Printing.PrintDocument DetailChartPrintDocument;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btn_DetailChartPreview;
        private System.Windows.Forms.Label label_DetaiChart;
        private System.Windows.Forms.Button btn_DetailChartPrint;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btn_CSupplier;
        private System.Windows.Forms.Button btnBASupplier;
        private System.Windows.Forms.Button btn_ASupplier;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btn_Preview;
        private System.Windows.Forms.Label lb_ChartTitle;
        private System.Windows.Forms.Button btn_Print;
    }
}