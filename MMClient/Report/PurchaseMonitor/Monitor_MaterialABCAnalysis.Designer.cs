﻿namespace MMClient.RA.PurchaseMonitor
{
    partial class Monitor_MaterialABCAnalysis
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            this.gb_Query = new System.Windows.Forms.GroupBox();
            this.cbb_Type = new System.Windows.Forms.ComboBox();
            this.lb_Type = new System.Windows.Forms.Label();
            this.btn_Query = new System.Windows.Forms.Button();
            this.dtp_EndTime = new System.Windows.Forms.DateTimePicker();
            this.lb_To = new System.Windows.Forms.Label();
            this.dtp_StartTime = new System.Windows.Forms.DateTimePicker();
            this.lb_Time = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Column_ProductCategory = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column_InvoiceValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column_Share = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column_CumShare = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column_CumQShare = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel_Chart = new System.Windows.Forms.Panel();
            this.chartViewer1 = new ChartDirector.WinChartViewer();
            this.btn_Preview = new System.Windows.Forms.Button();
            this.btn_Print = new System.Windows.Forms.Button();
            this.lb_ChartTitle = new System.Windows.Forms.Label();
            this.chartPrintDocument = new System.Drawing.Printing.PrintDocument();
            this.chartPageSetupDialog = new System.Windows.Forms.PageSetupDialog();
            this.btn_DetailChartPreview = new System.Windows.Forms.Button();
            this.btn_DetailChartPrint = new System.Windows.Forms.Button();
            this.label_DetaiChart = new System.Windows.Forms.Label();
            this.detailPageSetupDialog = new System.Windows.Forms.PageSetupDialog();
            this.detailPrintDocument = new System.Drawing.Printing.PrintDocument();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btn_CMaterial = new System.Windows.Forms.Button();
            this.btn_BMaterial = new System.Windows.Forms.Button();
            this.btn_AMaterial = new System.Windows.Forms.Button();
            this.gb_Query.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.panel_Chart.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartViewer1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // gb_Query
            // 
            this.gb_Query.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.gb_Query.BackColor = System.Drawing.SystemColors.Control;
            this.gb_Query.Controls.Add(this.cbb_Type);
            this.gb_Query.Controls.Add(this.lb_Type);
            this.gb_Query.Controls.Add(this.btn_Query);
            this.gb_Query.Controls.Add(this.dtp_EndTime);
            this.gb_Query.Controls.Add(this.lb_To);
            this.gb_Query.Controls.Add(this.dtp_StartTime);
            this.gb_Query.Controls.Add(this.lb_Time);
            this.gb_Query.Location = new System.Drawing.Point(12, 12);
            this.gb_Query.Name = "gb_Query";
            this.gb_Query.Size = new System.Drawing.Size(1153, 92);
            this.gb_Query.TabIndex = 0;
            this.gb_Query.TabStop = false;
            this.gb_Query.Text = "查询条件";
            // 
            // cbb_Type
            // 
            this.cbb_Type.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbb_Type.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cbb_Type.FormattingEnabled = true;
            this.cbb_Type.Items.AddRange(new object[] {
            "所有",
            "A物料",
            "B物料",
            "C物料"});
            this.cbb_Type.Location = new System.Drawing.Point(387, 33);
            this.cbb_Type.Name = "cbb_Type";
            this.cbb_Type.Size = new System.Drawing.Size(125, 20);
            this.cbb_Type.TabIndex = 6;
            // 
            // lb_Type
            // 
            this.lb_Type.AutoSize = true;
            this.lb_Type.Location = new System.Drawing.Point(350, 37);
            this.lb_Type.Name = "lb_Type";
            this.lb_Type.Size = new System.Drawing.Size(41, 12);
            this.lb_Type.TabIndex = 5;
            this.lb_Type.Text = "类别：";
            // 
            // btn_Query
            // 
            this.btn_Query.Location = new System.Drawing.Point(583, 27);
            this.btn_Query.Name = "btn_Query";
            this.btn_Query.Size = new System.Drawing.Size(91, 31);
            this.btn_Query.TabIndex = 4;
            this.btn_Query.Text = "查看";
            this.btn_Query.UseVisualStyleBackColor = true;
            this.btn_Query.Click += new System.EventHandler(this.btn_Query_Click);
            // 
            // dtp_EndTime
            // 
            this.dtp_EndTime.Location = new System.Drawing.Point(204, 33);
            this.dtp_EndTime.Name = "dtp_EndTime";
            this.dtp_EndTime.Size = new System.Drawing.Size(122, 21);
            this.dtp_EndTime.TabIndex = 3;
            // 
            // lb_To
            // 
            this.lb_To.AutoSize = true;
            this.lb_To.Location = new System.Drawing.Point(181, 37);
            this.lb_To.Name = "lb_To";
            this.lb_To.Size = new System.Drawing.Size(17, 12);
            this.lb_To.TabIndex = 2;
            this.lb_To.Text = "--";
            // 
            // dtp_StartTime
            // 
            this.dtp_StartTime.Location = new System.Drawing.Point(53, 33);
            this.dtp_StartTime.Name = "dtp_StartTime";
            this.dtp_StartTime.Size = new System.Drawing.Size(122, 21);
            this.dtp_StartTime.TabIndex = 1;
            // 
            // lb_Time
            // 
            this.lb_Time.AutoSize = true;
            this.lb_Time.Location = new System.Drawing.Point(15, 37);
            this.lb_Time.Name = "lb_Time";
            this.lb_Time.Size = new System.Drawing.Size(41, 12);
            this.lb_Time.TabIndex = 0;
            this.lb_Time.Text = "时间：";
            // 
            // dataGridView1
            // 
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.ColumnHeadersHeight = 30;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column_ProductCategory,
            this.Column_InvoiceValue,
            this.Column_Share,
            this.Column_CumShare,
            this.Column_CumQShare});
            this.dataGridView1.EnableHeadersVisualStyles = false;
            this.dataGridView1.Location = new System.Drawing.Point(12, 203);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.LightBlue;
            this.dataGridView1.RowTemplate.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.Black;
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.RowTemplate.ReadOnly = true;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(486, 424);
            this.dataGridView1.TabIndex = 1;
            this.dataGridView1.CellMouseEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellMouseEnter);
            this.dataGridView1.CellMouseLeave += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellMouseLeave);
            this.dataGridView1.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.dataGridView1_RowPostPaint);
            // 
            // Column_ProductCategory
            // 
            this.Column_ProductCategory.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column_ProductCategory.DataPropertyName = "Name";
            dataGridViewCellStyle2.Padding = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Column_ProductCategory.DefaultCellStyle = dataGridViewCellStyle2;
            this.Column_ProductCategory.HeaderText = "产品类别";
            this.Column_ProductCategory.Name = "Column_ProductCategory";
            this.Column_ProductCategory.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Column_InvoiceValue
            // 
            this.Column_InvoiceValue.DataPropertyName = "TotalPrice";
            dataGridViewCellStyle3.Format = "N2";
            dataGridViewCellStyle3.NullValue = null;
            this.Column_InvoiceValue.DefaultCellStyle = dataGridViewCellStyle3;
            this.Column_InvoiceValue.HeaderText = "发票金额";
            this.Column_InvoiceValue.Name = "Column_InvoiceValue";
            this.Column_InvoiceValue.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Column_Share
            // 
            this.Column_Share.DataPropertyName = "Share";
            dataGridViewCellStyle4.Format = "0.00%";
            dataGridViewCellStyle4.NullValue = null;
            this.Column_Share.DefaultCellStyle = dataGridViewCellStyle4;
            this.Column_Share.HeaderText = "占比";
            this.Column_Share.Name = "Column_Share";
            this.Column_Share.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Column_CumShare
            // 
            this.Column_CumShare.DataPropertyName = "CumShare";
            dataGridViewCellStyle5.Format = "0.00%";
            dataGridViewCellStyle5.NullValue = null;
            this.Column_CumShare.DefaultCellStyle = dataGridViewCellStyle5;
            this.Column_CumShare.HeaderText = "累计占比";
            this.Column_CumShare.Name = "Column_CumShare";
            this.Column_CumShare.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Column_CumQShare
            // 
            this.Column_CumQShare.DataPropertyName = "CumQShare";
            this.Column_CumQShare.HeaderText = "品种累计占比";
            this.Column_CumQShare.Name = "Column_CumQShare";
            this.Column_CumQShare.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column_CumQShare.Visible = false;
            // 
            // panel_Chart
            // 
            this.panel_Chart.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panel_Chart.BackColor = System.Drawing.SystemColors.Control;
            this.panel_Chart.Controls.Add(this.chartViewer1);
            this.panel_Chart.Location = new System.Drawing.Point(504, 148);
            this.panel_Chart.Name = "panel_Chart";
            this.panel_Chart.Size = new System.Drawing.Size(661, 479);
            this.panel_Chart.TabIndex = 3;
            // 
            // chartViewer1
            // 
            this.chartViewer1.HotSpotCursor = System.Windows.Forms.Cursors.Hand;
            this.chartViewer1.Location = new System.Drawing.Point(14, 15);
            this.chartViewer1.Name = "chartViewer1";
            this.chartViewer1.Size = new System.Drawing.Size(168, 126);
            this.chartViewer1.TabIndex = 2;
            this.chartViewer1.TabStop = false;
            this.chartViewer1.ClickHotSpot += new ChartDirector.WinHotSpotEventHandler(this.chartViewer1_ClickHotSpot);
            // 
            // btn_Preview
            // 
            this.btn_Preview.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btn_Preview.Location = new System.Drawing.Point(599, 12);
            this.btn_Preview.Name = "btn_Preview";
            this.btn_Preview.Size = new System.Drawing.Size(53, 23);
            this.btn_Preview.TabIndex = 2;
            this.btn_Preview.Text = "预览";
            this.btn_Preview.UseVisualStyleBackColor = true;
            this.btn_Preview.Click += new System.EventHandler(this.btn_Preview_Click);
            // 
            // btn_Print
            // 
            this.btn_Print.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btn_Print.Location = new System.Drawing.Point(541, 12);
            this.btn_Print.Name = "btn_Print";
            this.btn_Print.Size = new System.Drawing.Size(52, 23);
            this.btn_Print.TabIndex = 1;
            this.btn_Print.Text = "打印";
            this.btn_Print.UseVisualStyleBackColor = true;
            this.btn_Print.Click += new System.EventHandler(this.btn_Print_Click_1);
            // 
            // lb_ChartTitle
            // 
            this.lb_ChartTitle.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lb_ChartTitle.Location = new System.Drawing.Point(3, 16);
            this.lb_ChartTitle.Name = "lb_ChartTitle";
            this.lb_ChartTitle.Size = new System.Drawing.Size(156, 19);
            this.lb_ChartTitle.TabIndex = 0;
            this.lb_ChartTitle.Text = "物料ABC类分析图";
            // 
            // chartPrintDocument
            // 
            this.chartPrintDocument.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.MyPrintDocument_PrintPage);
            // 
            // chartPageSetupDialog
            // 
            this.chartPageSetupDialog.Document = this.chartPrintDocument;
            // 
            // btn_DetailChartPreview
            // 
            this.btn_DetailChartPreview.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btn_DetailChartPreview.Location = new System.Drawing.Point(424, 11);
            this.btn_DetailChartPreview.Name = "btn_DetailChartPreview";
            this.btn_DetailChartPreview.Size = new System.Drawing.Size(53, 23);
            this.btn_DetailChartPreview.TabIndex = 2;
            this.btn_DetailChartPreview.Text = "预览";
            this.btn_DetailChartPreview.UseVisualStyleBackColor = true;
            this.btn_DetailChartPreview.Click += new System.EventHandler(this.btn_DetailChartPreview_Click);
            // 
            // btn_DetailChartPrint
            // 
            this.btn_DetailChartPrint.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btn_DetailChartPrint.Location = new System.Drawing.Point(367, 11);
            this.btn_DetailChartPrint.Name = "btn_DetailChartPrint";
            this.btn_DetailChartPrint.Size = new System.Drawing.Size(52, 23);
            this.btn_DetailChartPrint.TabIndex = 1;
            this.btn_DetailChartPrint.Text = "打印";
            this.btn_DetailChartPrint.UseVisualStyleBackColor = true;
            this.btn_DetailChartPrint.Click += new System.EventHandler(this.btn_DetailChartPrint_Click);
            // 
            // label_DetaiChart
            // 
            this.label_DetaiChart.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label_DetaiChart.Location = new System.Drawing.Point(4, 15);
            this.label_DetaiChart.Name = "label_DetaiChart";
            this.label_DetaiChart.Size = new System.Drawing.Size(220, 19);
            this.label_DetaiChart.TabIndex = 0;
            this.label_DetaiChart.Text = "物料ABC类分析明细表";
            // 
            // detailPageSetupDialog
            // 
            this.detailPageSetupDialog.Document = this.detailPrintDocument;
            // 
            // detailPrintDocument
            // 
            this.detailPrintDocument.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.detailPrintDocument_PrintPage);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn1.DataPropertyName = "Name";
            dataGridViewCellStyle6.Padding = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.dataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle6;
            this.dataGridViewTextBoxColumn1.HeaderText = "产品类别";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "TotalPrice";
            dataGridViewCellStyle7.Format = "N2";
            dataGridViewCellStyle7.NullValue = null;
            this.dataGridViewTextBoxColumn2.DefaultCellStyle = dataGridViewCellStyle7;
            this.dataGridViewTextBoxColumn2.HeaderText = "发票金额";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "Share";
            dataGridViewCellStyle8.Format = "0.00%";
            dataGridViewCellStyle8.NullValue = null;
            this.dataGridViewTextBoxColumn3.DefaultCellStyle = dataGridViewCellStyle8;
            this.dataGridViewTextBoxColumn3.HeaderText = "占比";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "CumShare";
            dataGridViewCellStyle9.Format = "0.00%";
            dataGridViewCellStyle9.NullValue = null;
            this.dataGridViewTextBoxColumn4.DefaultCellStyle = dataGridViewCellStyle9;
            this.dataGridViewTextBoxColumn4.HeaderText = "累计占比";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "CumQShare";
            this.dataGridViewTextBoxColumn5.HeaderText = "品种累计占比";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn5.Visible = false;
            // 
            // supplierInforGroupBox
            // 
            this.groupBox1.Controls.Add(this.btn_DetailChartPreview);
            this.groupBox1.Controls.Add(this.label_DetaiChart);
            this.groupBox1.Controls.Add(this.btn_DetailChartPrint);
            this.groupBox1.Location = new System.Drawing.Point(12, 158);
            this.groupBox1.Name = "supplierInforGroupBox";
            this.groupBox1.Size = new System.Drawing.Size(483, 39);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.btn_Preview);
            this.groupBox2.Controls.Add(this.lb_ChartTitle);
            this.groupBox2.Controls.Add(this.btn_Print);
            this.groupBox2.Location = new System.Drawing.Point(505, 104);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(660, 41);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btn_CMaterial);
            this.groupBox3.Controls.Add(this.btn_BMaterial);
            this.groupBox3.Controls.Add(this.btn_AMaterial);
            this.groupBox3.Location = new System.Drawing.Point(13, 104);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(482, 59);
            this.groupBox3.TabIndex = 7;
            this.groupBox3.TabStop = false;
            // 
            // btn_CMaterial
            // 
            this.btn_CMaterial.Location = new System.Drawing.Point(319, 17);
            this.btn_CMaterial.Name = "btn_CMaterial";
            this.btn_CMaterial.Size = new System.Drawing.Size(99, 31);
            this.btn_CMaterial.TabIndex = 2;
            this.btn_CMaterial.Text = "C类物料";
            this.btn_CMaterial.UseVisualStyleBackColor = true;
            this.btn_CMaterial.Click += new System.EventHandler(this.btn_CMaterial_Click);
            // 
            // btn_BMaterial
            // 
            this.btn_BMaterial.Location = new System.Drawing.Point(176, 17);
            this.btn_BMaterial.Name = "btn_BMaterial";
            this.btn_BMaterial.Size = new System.Drawing.Size(99, 31);
            this.btn_BMaterial.TabIndex = 1;
            this.btn_BMaterial.Text = "B类物料";
            this.btn_BMaterial.UseVisualStyleBackColor = true;
            this.btn_BMaterial.Click += new System.EventHandler(this.btn_BMaterial_Click);
            // 
            // btn_AMaterial
            // 
            this.btn_AMaterial.Location = new System.Drawing.Point(33, 17);
            this.btn_AMaterial.Name = "btn_AMaterial";
            this.btn_AMaterial.Size = new System.Drawing.Size(99, 31);
            this.btn_AMaterial.TabIndex = 0;
            this.btn_AMaterial.Text = "A类物料";
            this.btn_AMaterial.UseVisualStyleBackColor = true;
            this.btn_AMaterial.Click += new System.EventHandler(this.btn_AMaterial_Click);
            // 
            // Monitor_MaterialABCAnalysis
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1169, 639);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.panel_Chart);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.gb_Query);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "Monitor_MaterialABCAnalysis";
            this.Text = "物料的ABC类分析";
            this.gb_Query.ResumeLayout(false);
            this.gb_Query.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.panel_Chart.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chartViewer1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gb_Query;
        private System.Windows.Forms.ComboBox cbb_Type;
        private System.Windows.Forms.Label lb_Type;
        private System.Windows.Forms.Button btn_Query;
        private System.Windows.Forms.DateTimePicker dtp_EndTime;
        private System.Windows.Forms.Label lb_To;
        private System.Windows.Forms.DateTimePicker dtp_StartTime;
        private System.Windows.Forms.Label lb_Time;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Panel panel_Chart;
        private ChartDirector.WinChartViewer chartViewer1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column_ProductCategory;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column_InvoiceValue;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column_Share;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column_CumShare;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column_CumQShare;
        private System.Windows.Forms.Label lb_ChartTitle;
        private System.Windows.Forms.Button btn_Preview;
        private System.Windows.Forms.Button btn_Print;
        private System.Drawing.Printing.PrintDocument chartPrintDocument;
        private System.Windows.Forms.PageSetupDialog chartPageSetupDialog;
        private System.Windows.Forms.Button btn_DetailChartPreview;
        private System.Windows.Forms.Button btn_DetailChartPrint;
        private System.Windows.Forms.Label label_DetaiChart;
        private System.Windows.Forms.PageSetupDialog detailPageSetupDialog;
        private System.Drawing.Printing.PrintDocument detailPrintDocument;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btn_CMaterial;
        private System.Windows.Forms.Button btn_BMaterial;
        private System.Windows.Forms.Button btn_AMaterial;
    }
}