﻿namespace MMClient.RA.SourseTracing
{
    partial class MaterialPriceOverview
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel_condition = new System.Windows.Forms.Panel();
            this.dtp_EndTime = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.dtp_StartTime = new System.Windows.Forms.DateTimePicker();
            this.lb_Date = new System.Windows.Forms.Label();
            this.cbb_MaterialGroup = new System.Windows.Forms.ComboBox();
            this.lb_MaterialGroup = new System.Windows.Forms.Label();
            this.txt_ChooseSupplier = new System.Windows.Forms.TextBox();
            this.btn_genarateReport = new System.Windows.Forms.Button();
            this.lb_ChooseSupplier = new System.Windows.Forms.Label();
            this.cbb_PurchaseGroup = new System.Windows.Forms.ComboBox();
            this.lb_purchaseGroup = new System.Windows.Forms.Label();
            this.lb_title = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.dvg_MaterialPriceOverview = new System.Windows.Forms.DataGridView();
            this.SupplierID = new System.Windows.Forms.DataGridViewLinkColumn();
            this.SupplierName = new System.Windows.Forms.DataGridViewLinkColumn();
            this.panel_dgv = new System.Windows.Forms.Panel();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CategoryName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MaterialID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MaterialName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Netprice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MarketPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MeasureUnit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ValidityDateStart = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ValidityDateEnd = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel_condition.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dvg_MaterialPriceOverview)).BeginInit();
            this.panel_dgv.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel_condition
            // 
            this.panel_condition.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panel_condition.BackColor = System.Drawing.SystemColors.Control;
            this.panel_condition.Controls.Add(this.dtp_EndTime);
            this.panel_condition.Controls.Add(this.label1);
            this.panel_condition.Controls.Add(this.dtp_StartTime);
            this.panel_condition.Controls.Add(this.lb_Date);
            this.panel_condition.Controls.Add(this.cbb_MaterialGroup);
            this.panel_condition.Controls.Add(this.lb_MaterialGroup);
            this.panel_condition.Controls.Add(this.txt_ChooseSupplier);
            this.panel_condition.Controls.Add(this.btn_genarateReport);
            this.panel_condition.Controls.Add(this.lb_ChooseSupplier);
            this.panel_condition.Controls.Add(this.cbb_PurchaseGroup);
            this.panel_condition.Controls.Add(this.lb_purchaseGroup);
            this.panel_condition.Location = new System.Drawing.Point(3, 4);
            this.panel_condition.Name = "panel_condition";
            this.panel_condition.Size = new System.Drawing.Size(1144, 87);
            this.panel_condition.TabIndex = 0;
            // 
            // dtp_EndTime
            // 
            this.dtp_EndTime.Location = new System.Drawing.Point(237, 12);
            this.dtp_EndTime.Name = "dtp_EndTime";
            this.dtp_EndTime.Size = new System.Drawing.Size(121, 21);
            this.dtp_EndTime.TabIndex = 11;
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(217, 18);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(17, 12);
            this.label1.TabIndex = 10;
            this.label1.Text = "--";
            // 
            // dtp_StartTime
            // 
            this.dtp_StartTime.Location = new System.Drawing.Point(94, 12);
            this.dtp_StartTime.Name = "dtp_StartTime";
            this.dtp_StartTime.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.dtp_StartTime.Size = new System.Drawing.Size(121, 21);
            this.dtp_StartTime.TabIndex = 9;
            // 
            // lb_Date
            // 
            this.lb_Date.AutoSize = true;
            this.lb_Date.Location = new System.Drawing.Point(51, 17);
            this.lb_Date.Name = "lb_Date";
            this.lb_Date.Size = new System.Drawing.Size(41, 12);
            this.lb_Date.TabIndex = 8;
            this.lb_Date.Text = "日期：";
            // 
            // cbb_MaterialGroup
            // 
            this.cbb_MaterialGroup.FormattingEnabled = true;
            this.cbb_MaterialGroup.Items.AddRange(new object[] {
            "全部",
            "钢板",
            "轴承"});
            this.cbb_MaterialGroup.Location = new System.Drawing.Point(94, 53);
            this.cbb_MaterialGroup.Name = "cbb_MaterialGroup";
            this.cbb_MaterialGroup.Size = new System.Drawing.Size(264, 20);
            this.cbb_MaterialGroup.TabIndex = 7;
            // 
            // lb_MaterialGroup
            // 
            this.lb_MaterialGroup.AutoSize = true;
            this.lb_MaterialGroup.Location = new System.Drawing.Point(38, 58);
            this.lb_MaterialGroup.Name = "lb_MaterialGroup";
            this.lb_MaterialGroup.Size = new System.Drawing.Size(53, 12);
            this.lb_MaterialGroup.TabIndex = 6;
            this.lb_MaterialGroup.Text = "物料组：";
            // 
            // txt_ChooseSupplier
            // 
            this.txt_ChooseSupplier.Location = new System.Drawing.Point(471, 55);
            this.txt_ChooseSupplier.Name = "txt_ChooseSupplier";
            this.txt_ChooseSupplier.Size = new System.Drawing.Size(257, 21);
            this.txt_ChooseSupplier.TabIndex = 5;
            this.txt_ChooseSupplier.Enter += new System.EventHandler(this.txt_ChooseSupplier_Enter);
            this.txt_ChooseSupplier.Leave += new System.EventHandler(this.txt_ChooseSupplier_Leave);
            // 
            // btn_genarateReport
            // 
            this.btn_genarateReport.BackColor = System.Drawing.SystemColors.Control;
            this.btn_genarateReport.Location = new System.Drawing.Point(800, 29);
            this.btn_genarateReport.Name = "btn_genarateReport";
            this.btn_genarateReport.Size = new System.Drawing.Size(114, 30);
            this.btn_genarateReport.TabIndex = 4;
            this.btn_genarateReport.Text = "生成报表";
            this.btn_genarateReport.UseVisualStyleBackColor = true;
            this.btn_genarateReport.Click += new System.EventHandler(this.btn_genarateReport_Click);
            // 
            // lb_ChooseSupplier
            // 
            this.lb_ChooseSupplier.AutoSize = true;
            this.lb_ChooseSupplier.Location = new System.Drawing.Point(416, 61);
            this.lb_ChooseSupplier.Name = "lb_ChooseSupplier";
            this.lb_ChooseSupplier.Size = new System.Drawing.Size(53, 12);
            this.lb_ChooseSupplier.TabIndex = 2;
            this.lb_ChooseSupplier.Text = "供应商：";
            // 
            // cbb_PurchaseGroup
            // 
            this.cbb_PurchaseGroup.FormattingEnabled = true;
            this.cbb_PurchaseGroup.Items.AddRange(new object[] {
            "全部",
            "原材料采购组",
            "半成品采购组"});
            this.cbb_PurchaseGroup.Location = new System.Drawing.Point(471, 12);
            this.cbb_PurchaseGroup.Name = "cbb_PurchaseGroup";
            this.cbb_PurchaseGroup.Size = new System.Drawing.Size(257, 20);
            this.cbb_PurchaseGroup.TabIndex = 1;
            // 
            // lb_purchaseGroup
            // 
            this.lb_purchaseGroup.AutoSize = true;
            this.lb_purchaseGroup.Location = new System.Drawing.Point(404, 17);
            this.lb_purchaseGroup.Name = "lb_purchaseGroup";
            this.lb_purchaseGroup.Size = new System.Drawing.Size(65, 12);
            this.lb_purchaseGroup.TabIndex = 0;
            this.lb_purchaseGroup.Text = "采购组织：";
            // 
            // lb_title
            // 
            this.lb_title.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lb_title.Location = new System.Drawing.Point(4, 6);
            this.lb_title.Name = "lb_title";
            this.lb_title.Size = new System.Drawing.Size(160, 22);
            this.lb_title.TabIndex = 1;
            this.lb_title.Text = "物料价格总览";
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.SystemColors.Control;
            this.panel1.Controls.Add(this.lb_title);
            this.panel1.Location = new System.Drawing.Point(3, 93);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1144, 31);
            this.panel1.TabIndex = 2;
            // 
            // dvg_MaterialPriceOverview
            // 
            this.dvg_MaterialPriceOverview.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dvg_MaterialPriceOverview.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dvg_MaterialPriceOverview.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dvg_MaterialPriceOverview.ColumnHeadersHeight = 32;
            this.dvg_MaterialPriceOverview.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CategoryName,
            this.MaterialID,
            this.MaterialName,
            this.Netprice,
            this.MarketPrice,
            this.MeasureUnit,
            this.SupplierID,
            this.SupplierName,
            this.ValidityDateStart,
            this.ValidityDateEnd});
            this.dvg_MaterialPriceOverview.EnableHeadersVisualStyles = false;
            this.dvg_MaterialPriceOverview.Location = new System.Drawing.Point(1, 2);
            this.dvg_MaterialPriceOverview.Name = "dvg_MaterialPriceOverview";
            this.dvg_MaterialPriceOverview.RowTemplate.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.LightBlue;
            this.dvg_MaterialPriceOverview.RowTemplate.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.Black;
            this.dvg_MaterialPriceOverview.RowTemplate.Height = 28;
            this.dvg_MaterialPriceOverview.RowTemplate.ReadOnly = true;
            this.dvg_MaterialPriceOverview.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dvg_MaterialPriceOverview.Size = new System.Drawing.Size(1142, 499);
            this.dvg_MaterialPriceOverview.TabIndex = 3;
            this.dvg_MaterialPriceOverview.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dvg_MaterialPrice_CellContentClick);
            this.dvg_MaterialPriceOverview.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dvg_MaterialPrice_CellDoubleClick);
            this.dvg_MaterialPriceOverview.CellMouseEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dvg_MaterialPrice_CellMouseEnter);
            this.dvg_MaterialPriceOverview.CellMouseLeave += new System.Windows.Forms.DataGridViewCellEventHandler(this.dvg_MaterialPrice_CellMouseLeave);
            this.dvg_MaterialPriceOverview.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.dvg_MaterialPrice_RowPostPaint);
            // 
            // SupplierID
            // 
            this.SupplierID.DataPropertyName = "Supplier_ID";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.SupplierID.DefaultCellStyle = dataGridViewCellStyle8;
            this.SupplierID.HeaderText = "供应商编码";
            this.SupplierID.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline;
            this.SupplierID.Name = "SupplierID";
            this.SupplierID.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.SupplierID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // SupplierName
            // 
            this.SupplierName.DataPropertyName = "Supplier_Name";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.SupplierName.DefaultCellStyle = dataGridViewCellStyle9;
            this.SupplierName.HeaderText = "供应商名称";
            this.SupplierName.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline;
            this.SupplierName.Name = "SupplierName";
            this.SupplierName.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.SupplierName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.SupplierName.Width = 150;
            // 
            // panel_dgv
            // 
            this.panel_dgv.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panel_dgv.BackColor = System.Drawing.SystemColors.Control;
            this.panel_dgv.Controls.Add(this.dvg_MaterialPriceOverview);
            this.panel_dgv.Location = new System.Drawing.Point(3, 127);
            this.panel_dgv.Name = "panel_dgv";
            this.panel_dgv.Size = new System.Drawing.Size(1144, 505);
            this.panel_dgv.TabIndex = 4;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "Material_Group";
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle12;
            this.dataGridViewTextBoxColumn1.HeaderText = "类别名称";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "Material_ID";
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewTextBoxColumn2.DefaultCellStyle = dataGridViewCellStyle13;
            this.dataGridViewTextBoxColumn2.HeaderText = "物料编码";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.Width = 150;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "Material_Name";
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewTextBoxColumn3.DefaultCellStyle = dataGridViewCellStyle14;
            this.dataGridViewTextBoxColumn3.HeaderText = "物料名称";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "Price";
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewTextBoxColumn4.DefaultCellStyle = dataGridViewCellStyle15;
            this.dataGridViewTextBoxColumn4.HeaderText = "净价";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            // 
            // dataGridViewTextBoxColumn5
            // 
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewTextBoxColumn5.DefaultCellStyle = dataGridViewCellStyle16;
            this.dataGridViewTextBoxColumn5.HeaderText = "市场价格";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "Measurement";
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewTextBoxColumn6.DefaultCellStyle = dataGridViewCellStyle17;
            this.dataGridViewTextBoxColumn6.HeaderText = "单位";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "StartTime";
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewTextBoxColumn7.DefaultCellStyle = dataGridViewCellStyle18;
            this.dataGridViewTextBoxColumn7.HeaderText = "有效期从";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.DataPropertyName = "EndTime";
            dataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewTextBoxColumn8.DefaultCellStyle = dataGridViewCellStyle19;
            this.dataGridViewTextBoxColumn8.HeaderText = "有效期到";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            // 
            // CategoryName
            // 
            this.CategoryName.DataPropertyName = "Material_Group";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.CategoryName.DefaultCellStyle = dataGridViewCellStyle2;
            this.CategoryName.HeaderText = "类别名称";
            this.CategoryName.Name = "CategoryName";
            // 
            // MaterialID
            // 
            this.MaterialID.DataPropertyName = "Material_ID";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.MaterialID.DefaultCellStyle = dataGridViewCellStyle3;
            this.MaterialID.HeaderText = "物料编码";
            this.MaterialID.Name = "MaterialID";
            this.MaterialID.Width = 150;
            // 
            // MaterialName
            // 
            this.MaterialName.DataPropertyName = "Material_Name";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.MaterialName.DefaultCellStyle = dataGridViewCellStyle4;
            this.MaterialName.HeaderText = "物料名称";
            this.MaterialName.Name = "MaterialName";
            // 
            // Netprice
            // 
            this.Netprice.DataPropertyName = "Price";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.Netprice.DefaultCellStyle = dataGridViewCellStyle5;
            this.Netprice.HeaderText = "净价";
            this.Netprice.Name = "Netprice";
            // 
            // MarketPrice
            // 
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.MarketPrice.DefaultCellStyle = dataGridViewCellStyle6;
            this.MarketPrice.HeaderText = "市场价格";
            this.MarketPrice.Name = "MarketPrice";
            // 
            // MeasureUnit
            // 
            this.MeasureUnit.DataPropertyName = "Measurement";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.MeasureUnit.DefaultCellStyle = dataGridViewCellStyle7;
            this.MeasureUnit.HeaderText = "单位";
            this.MeasureUnit.Name = "MeasureUnit";
            // 
            // ValidityDateStart
            // 
            this.ValidityDateStart.DataPropertyName = "StartTime";
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.ValidityDateStart.DefaultCellStyle = dataGridViewCellStyle10;
            this.ValidityDateStart.HeaderText = "有效期从";
            this.ValidityDateStart.Name = "ValidityDateStart";
            // 
            // ValidityDateEnd
            // 
            this.ValidityDateEnd.DataPropertyName = "EndTime";
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.ValidityDateEnd.DefaultCellStyle = dataGridViewCellStyle11;
            this.ValidityDateEnd.HeaderText = "有效期到";
            this.ValidityDateEnd.Name = "ValidityDateEnd";
            // 
            // MaterialPriceOverview
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1150, 637);
            this.Controls.Add(this.panel_dgv);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel_condition);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "MaterialPriceOverview";
            this.Text = "价格分析";
            this.panel_condition.ResumeLayout(false);
            this.panel_condition.PerformLayout();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dvg_MaterialPriceOverview)).EndInit();
            this.panel_dgv.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel_condition;
        private System.Windows.Forms.Label lb_title;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridView dvg_MaterialPriceOverview;
        private System.Windows.Forms.TextBox txt_ChooseSupplier;
        private System.Windows.Forms.Button btn_genarateReport;
        private System.Windows.Forms.Label lb_ChooseSupplier;
        private System.Windows.Forms.ComboBox cbb_PurchaseGroup;
        private System.Windows.Forms.Label lb_purchaseGroup;
        private System.Windows.Forms.Panel panel_dgv;
        private System.Windows.Forms.DataGridViewTextBoxColumn CategoryName;
        private System.Windows.Forms.DataGridViewTextBoxColumn MaterialID;
        private System.Windows.Forms.DataGridViewTextBoxColumn MaterialName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Netprice;
        private System.Windows.Forms.DataGridViewTextBoxColumn MarketPrice;
        private System.Windows.Forms.DataGridViewTextBoxColumn MeasureUnit;
        private System.Windows.Forms.DataGridViewLinkColumn SupplierID;
        private System.Windows.Forms.DataGridViewLinkColumn SupplierName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ValidityDateStart;
        private System.Windows.Forms.DataGridViewTextBoxColumn ValidityDateEnd;
        private System.Windows.Forms.DateTimePicker dtp_EndTime;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dtp_StartTime;
        private System.Windows.Forms.Label lb_Date;
        private System.Windows.Forms.ComboBox cbb_MaterialGroup;
        private System.Windows.Forms.Label lb_MaterialGroup;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
    }
}