﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ChartDirector;
using Lib.SqlServerDAL;

namespace MMClient.RA.SourseTracing
{
    public partial class Source_SupplierQuote : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        public Source_SupplierQuote()
        {
            InitializeComponent();
        }

        private void btn_query_Click(object sender, EventArgs e)
        {
            WinChartViewer viewer = chartViewer1;
            this.createChart(viewer, "1");
        }

        //Main code for creating chart.
        //Note: the argument img is unused because this demo only has 1 chart.
        public void createChart(WinChartViewer viewer, string img)
        {
            // The data for the line chart
            double[] data0 = { 60.2, 51.7, 81.3, 48.6, 56.2, 68.9, 52.8 };
            double[] data1 = { 30.0, 32.7, 33.9, 29.5, 32.2, 28.4, 29.8 };
            string[] labels = { "一月", "二月", "三月", "四月", "五月", "六月", "七月" };

            /*
            if(this.txt_MaterialID.Text.Trim() == "")
            {
                MessageBox.Show("请输入物料编码！","软件提示");
                return;
            }
            if (this.txt_SupplierID.Text.Trim() == "" && this.txt_SupplierName.Text.Trim() == "")
            {
                MessageBox.Show("请输入供应商编码或供应商名称！","软件提示");
                return;
            }
            //查找数据库
            string sql_Text = @"select  CONVERT(varchar(10),Begin_Time,112) as [Time],Price from [Quote] where Material_ID = '" + this.txt_MaterialID.Text + "'";
            sql_Text += @" and Supplier_ID = '" + this.txt_SupplierID.Text + "'";
            if (this.dtp_startTime.Value.Date > this.dtp_endTime.Value.Date)
            {
                MessageBox.Show("起始日期不能大于截止日期！", "软件提示");
                return;
            }
            if (this.dtp_startTime.ShowCheckBox == true)
            {
                if (this.dtp_startTime.Checked == true)  //表示该时间框被选中
                {
                    sql_Text += @" and Begin_Time >= '" + this.dtp_startTime.Value.ToString("yyyy-MM-dd") + "'";
                }
            }
            if (this.dtp_endTime.ShowCheckBox == true)
            {
                if (this.dtp_endTime.Checked == true)
                {
                    sql_Text += @" and Begin_Time <= '" + this.dtp_endTime.Value.ToString("yyyy-MM-dd") + "'";
                }
            }
            sql_Text += @"order by Begin_Time";
            DataTable data_Table = DBHelper.ExecuteQueryDT(sql_Text);
            int n = data_Table.Rows.Count;
            double[] data = new double[n];
            string[] labels = new string[n];
            for (int i = 0; i < data_Table.Rows.Count; i++)
            {
                data[i] = Convert.ToDouble(data_Table.Rows[i]["Price"]);
                labels[i] = Convert.ToString(data_Table.Rows[i]["Time"]).Substring(4,2) + "月";
            }

            string sql_Text1 = @"select  CONVERT(varchar(10),Begin_Time,112) as [Time],Price from [Order] where Material_ID = '" + this.txt_MaterialID.Text + "'";
            sql_Text1 += @" and Supplier_ID = '" + this.txt_SupplierID.Text + "'";
            if (this.dtp_startTime.Value.Date > this.dtp_endTime.Value.Date)
            {
                MessageBox.Show("起始日期不能大于截止日期！", "软件提示");
                return;
            }
            if (this.dtp_startTime.ShowCheckBox == true)
            {
                if (this.dtp_startTime.Checked == true)  //表示该时间框被选中
                {
                    sql_Text1 += @" and Begin_Time >= '" + this.dtp_startTime.Value.ToString("yyyy-MM-dd") + "'";
                }
            }
            if (this.dtp_endTime.ShowCheckBox == true)
            {
                if (this.dtp_endTime.Checked == true)
                {
                    sql_Text1 += @" and Begin_Time <= '" + this.dtp_endTime.Value.ToString("yyyy-MM-dd") + "'";
                }
            }
            sql_Text1 += @"order by Begin_Time";
            DataTable data_Table1 = DBHelper.ExecuteQueryDT(sql_Text1);
            int n1 = data_Table1.Rows.Count;
            double[] data1 = new double[n];
            //string[] labels = new string[n];
            for (int i = 0; i < data_Table1.Rows.Count; i++)
            {
                data1[i] = Convert.ToDouble(data_Table1.Rows[i]["Price"]);
                //labels[i] = Convert.ToString(data_Table.Rows[i]["Time"]).Substring(4, 2) + "月";
            }
            */
            // Create a XYChart object of size 300 x 180 pixels, with a pale yellow
            // (0xffffc0) background, a black border, and 1 pixel 3D border effect.
            XYChart c = new XYChart(1150, 580, 0xffffc0, 0x000000, 1);

            // Set the plotarea at (45, 35) and of size 240 x 120 pixels, with white
            // background. Turn on both horizontal and vertical grid lines with light
            // grey color (0xc0c0c0)
            c.setPlotArea(45, 40, 1040, 450, 0xffffff, -1, -1, 0xc0c0c0, -1);

            // Add a legend box at (45, 12) (top of the chart) using horizontal
            // layout and 8 pts Arial font Set the background and border color to
            // Transparent.
            c.addLegend(45, 14, false, "", 8).setBackground(Chart.Transparent);

            // Add a title to the chart using 9 pts Arial Bold/white font. Use a 1 x
            // 2 bitmap pattern as the background.
            c.addTitle("供应商-物料价格分析", "Arial Bold", 12, 0xffffff
                ).setBackground(c.patternColor(new int[] { 0x004000, 0x008000 }, 2));

            // Set the y axis label format to nn%
            //c.yAxis().setLabelFormat("{value}%");

            // Set the labels on the x axis
            c.xAxis().setLabels(labels);
            c.xAxis().setTitle("月份");
            c.yAxis().setTitle("单价(元)");
            // Add a line layer to the chart
            LineLayer layer = c.addLineLayer();

            // Add the first line. Plot the points with a 7 pixel square symbol
            layer.addDataSet(data0, 0xcf4040, "报价").setDataSymbol(
                Chart.SquareSymbol, 7);

            // Add the second line. Plot the points with a 9 pixel dismond symbol
            layer.addDataSet(data1, 0x40cf40, "定价").setDataSymbol(
                Chart.DiamondSymbol, 9);

            // Enable data label on the data points. Set the label format to nn%.
            layer.setDataLabelFormat("{value}");

            // Output the chart
            viewer.Chart = c;

            //include tool tip for the chart
            viewer.ImageMap = c.getHTMLImageMap("clickable", "",
                "title='{xLabel}: {dataSetName} {value}'");
        }


        //触发热点事件
        private void chartViewer1_ClickHotSpot(object sender, ChartDirector.WinHotSpotEventArgs e)
        {
            //new Viewer().Display(sender,e);
        }

    }
}
