﻿namespace MMClient.RA.SourseTracing
{
    partial class MaterialPriceAnalysis
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ts_navigation = new System.Windows.Forms.ToolStrip();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton3 = new System.Windows.Forms.ToolStripButton();
            this.panel_1 = new System.Windows.Forms.Panel();
            this.panel_pastFourYears = new System.Windows.Forms.Panel();
            this.wcv_PastFourYearsPrice = new ChartDirector.WinChartViewer();
            this.panel_currentYear = new System.Windows.Forms.Panel();
            this.wcv_CurrentYearPrice = new ChartDirector.WinChartViewer();
            this.ts_navigation.SuspendLayout();
            this.panel_1.SuspendLayout();
            this.panel_pastFourYears.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.wcv_PastFourYearsPrice)).BeginInit();
            this.panel_currentYear.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.wcv_CurrentYearPrice)).BeginInit();
            this.SuspendLayout();
            // 
            // ts_navigation
            // 
            this.ts_navigation.BackColor = System.Drawing.SystemColors.Control;
            this.ts_navigation.ImageScalingSize = new System.Drawing.Size(64, 64);
            this.ts_navigation.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton1,
            this.toolStripSeparator1,
            this.toolStripButton3});
            this.ts_navigation.Location = new System.Drawing.Point(0, 0);
            this.ts_navigation.Name = "ts_navigation";
            this.ts_navigation.Size = new System.Drawing.Size(1083, 29);
            this.ts_navigation.TabIndex = 0;
            this.ts_navigation.Text = "toolStrip1";
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.AutoSize = false;
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(56, 26);
            this.toolStripButton1.Text = "打印";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 29);
            // 
            // toolStripButton3
            // 
            this.toolStripButton3.AutoSize = false;
            this.toolStripButton3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton3.Name = "toolStripButton3";
            this.toolStripButton3.Size = new System.Drawing.Size(56, 26);
            this.toolStripButton3.Text = "导出";
            // 
            // panel_1
            // 
            this.panel_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panel_1.BackColor = System.Drawing.Color.White;
            this.panel_1.Controls.Add(this.panel_pastFourYears);
            this.panel_1.Controls.Add(this.panel_currentYear);
            this.panel_1.Location = new System.Drawing.Point(0, 32);
            this.panel_1.Name = "panel_1";
            this.panel_1.Size = new System.Drawing.Size(1083, 438);
            this.panel_1.TabIndex = 1;
            // 
            // panel_pastFourYears
            // 
            this.panel_pastFourYears.Controls.Add(this.wcv_PastFourYearsPrice);
            this.panel_pastFourYears.Location = new System.Drawing.Point(507, 3);
            this.panel_pastFourYears.Name = "panel_pastFourYears";
            this.panel_pastFourYears.Size = new System.Drawing.Size(576, 425);
            this.panel_pastFourYears.TabIndex = 0;
            // 
            // wcv_PastFourYearsPrice
            // 
            this.wcv_PastFourYearsPrice.HotSpotCursor = System.Windows.Forms.Cursors.Hand;
            this.wcv_PastFourYearsPrice.Location = new System.Drawing.Point(12, 11);
            this.wcv_PastFourYearsPrice.Name = "wcv_PastFourYearsPrice";
            this.wcv_PastFourYearsPrice.Size = new System.Drawing.Size(168, 126);
            this.wcv_PastFourYearsPrice.TabIndex = 4;
            this.wcv_PastFourYearsPrice.TabStop = false;
            // 
            // panel_currentYear
            // 
            this.panel_currentYear.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.panel_currentYear.Controls.Add(this.wcv_CurrentYearPrice);
            this.panel_currentYear.Location = new System.Drawing.Point(3, 3);
            this.panel_currentYear.Name = "panel_currentYear";
            this.panel_currentYear.Size = new System.Drawing.Size(498, 425);
            this.panel_currentYear.TabIndex = 0;
            // 
            // wcv_CurrentYearPrice
            // 
            this.wcv_CurrentYearPrice.HotSpotCursor = System.Windows.Forms.Cursors.Hand;
            this.wcv_CurrentYearPrice.Location = new System.Drawing.Point(9, 11);
            this.wcv_CurrentYearPrice.Name = "wcv_CurrentYearPrice";
            this.wcv_CurrentYearPrice.Size = new System.Drawing.Size(168, 126);
            this.wcv_CurrentYearPrice.TabIndex = 4;
            this.wcv_CurrentYearPrice.TabStop = false;
            // 
            // MaterialPriceAnalysis
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1083, 472);
            this.Controls.Add(this.panel_1);
            this.Controls.Add(this.ts_navigation);
            this.Name = "MaterialPriceAnalysis";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "物料价格分析";
            this.ts_navigation.ResumeLayout(false);
            this.ts_navigation.PerformLayout();
            this.panel_1.ResumeLayout(false);
            this.panel_pastFourYears.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.wcv_PastFourYearsPrice)).EndInit();
            this.panel_currentYear.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.wcv_CurrentYearPrice)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip ts_navigation;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton toolStripButton3;
        private System.Windows.Forms.Panel panel_1;
        private System.Windows.Forms.Panel panel_pastFourYears;
        private System.Windows.Forms.Panel panel_currentYear;
        private ChartDirector.WinChartViewer wcv_PastFourYearsPrice;
        private ChartDirector.WinChartViewer wcv_CurrentYearPrice;
    }
}