﻿namespace MMClient.RA.Customized
{
    partial class Customized
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox33 = new System.Windows.Forms.GroupBox();
            this.comboBox101 = new System.Windows.Forms.ComboBox();
            this.comboBox99 = new System.Windows.Forms.ComboBox();
            this.comboBox98 = new System.Windows.Forms.ComboBox();
            this.comboBox104 = new System.Windows.Forms.ComboBox();
            this.groupBox32 = new System.Windows.Forms.GroupBox();
            this.comboBox97 = new System.Windows.Forms.ComboBox();
            this.comboBox53 = new System.Windows.Forms.ComboBox();
            this.comboBox52 = new System.Windows.Forms.ComboBox();
            this.comboBox100 = new System.Windows.Forms.ComboBox();
            this.groupBox17 = new System.Windows.Forms.GroupBox();
            this.comboBox51 = new System.Windows.Forms.ComboBox();
            this.comboBox50 = new System.Windows.Forms.ComboBox();
            this.comboBox49 = new System.Windows.Forms.ComboBox();
            this.comboBox54 = new System.Windows.Forms.ComboBox();
            this.groupBox30 = new System.Windows.Forms.GroupBox();
            this.panel8 = new System.Windows.Forms.Panel();
            this.button15 = new System.Windows.Forms.Button();
            this.button16 = new System.Windows.Forms.Button();
            this.groupBox22 = new System.Windows.Forms.GroupBox();
            this.comboBox79 = new System.Windows.Forms.ComboBox();
            this.comboBox80 = new System.Windows.Forms.ComboBox();
            this.comboBox81 = new System.Windows.Forms.ComboBox();
            this.comboBox82 = new System.Windows.Forms.ComboBox();
            this.comboBox83 = new System.Windows.Forms.ComboBox();
            this.comboBox84 = new System.Windows.Forms.ComboBox();
            this.groupBox33.SuspendLayout();
            this.groupBox32.SuspendLayout();
            this.groupBox17.SuspendLayout();
            this.groupBox30.SuspendLayout();
            this.panel8.SuspendLayout();
            this.groupBox22.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox33
            // 
            this.groupBox33.BackColor = System.Drawing.SystemColors.Control;
            this.groupBox33.Controls.Add(this.comboBox101);
            this.groupBox33.Controls.Add(this.comboBox99);
            this.groupBox33.Controls.Add(this.comboBox98);
            this.groupBox33.Controls.Add(this.comboBox104);
            this.groupBox33.Location = new System.Drawing.Point(12, 190);
            this.groupBox33.Name = "groupBox33";
            this.groupBox33.Size = new System.Drawing.Size(939, 83);
            this.groupBox33.TabIndex = 17;
            this.groupBox33.TabStop = false;
            this.groupBox33.Text = "选择组合方式";
            // 
            // comboBox101
            // 
            this.comboBox101.FormattingEnabled = true;
            this.comboBox101.Items.AddRange(new object[] {
            "方式1",
            "方式2",
            "方式3"});
            this.comboBox101.Location = new System.Drawing.Point(515, 37);
            this.comboBox101.Name = "comboBox101";
            this.comboBox101.Size = new System.Drawing.Size(107, 20);
            this.comboBox101.TabIndex = 3;
            // 
            // comboBox99
            // 
            this.comboBox99.FormattingEnabled = true;
            this.comboBox99.Items.AddRange(new object[] {
            "方式1",
            "方式2",
            "方式3"});
            this.comboBox99.Location = new System.Drawing.Point(353, 37);
            this.comboBox99.Name = "comboBox99";
            this.comboBox99.Size = new System.Drawing.Size(107, 20);
            this.comboBox99.TabIndex = 2;
            // 
            // comboBox98
            // 
            this.comboBox98.FormattingEnabled = true;
            this.comboBox98.Items.AddRange(new object[] {
            "方式1",
            "方式2",
            "方式3"});
            this.comboBox98.Location = new System.Drawing.Point(181, 37);
            this.comboBox98.Name = "comboBox98";
            this.comboBox98.Size = new System.Drawing.Size(107, 20);
            this.comboBox98.TabIndex = 1;
            // 
            // comboBox104
            // 
            this.comboBox104.FormattingEnabled = true;
            this.comboBox104.Items.AddRange(new object[] {
            "方式1",
            "方式2",
            "方式3"});
            this.comboBox104.Location = new System.Drawing.Point(17, 37);
            this.comboBox104.Name = "comboBox104";
            this.comboBox104.Size = new System.Drawing.Size(107, 20);
            this.comboBox104.TabIndex = 0;
            // 
            // groupBox32
            // 
            this.groupBox32.BackColor = System.Drawing.SystemColors.Control;
            this.groupBox32.Controls.Add(this.comboBox97);
            this.groupBox32.Controls.Add(this.comboBox53);
            this.groupBox32.Controls.Add(this.comboBox52);
            this.groupBox32.Controls.Add(this.comboBox100);
            this.groupBox32.Location = new System.Drawing.Point(12, 101);
            this.groupBox32.Name = "groupBox32";
            this.groupBox32.Size = new System.Drawing.Size(939, 83);
            this.groupBox32.TabIndex = 18;
            this.groupBox32.TabStop = false;
            this.groupBox32.Text = "选择分析维度";
            // 
            // comboBox97
            // 
            this.comboBox97.FormattingEnabled = true;
            this.comboBox97.Items.AddRange(new object[] {
            "维度1",
            "维度2",
            "维度3",
            "维度4"});
            this.comboBox97.Location = new System.Drawing.Point(515, 37);
            this.comboBox97.Name = "comboBox97";
            this.comboBox97.Size = new System.Drawing.Size(107, 20);
            this.comboBox97.TabIndex = 3;
            // 
            // comboBox53
            // 
            this.comboBox53.FormattingEnabled = true;
            this.comboBox53.Items.AddRange(new object[] {
            "维度1",
            "维度2",
            "维度3",
            "维度4"});
            this.comboBox53.Location = new System.Drawing.Point(353, 37);
            this.comboBox53.Name = "comboBox53";
            this.comboBox53.Size = new System.Drawing.Size(107, 20);
            this.comboBox53.TabIndex = 2;
            // 
            // comboBox52
            // 
            this.comboBox52.FormattingEnabled = true;
            this.comboBox52.Items.AddRange(new object[] {
            "维度1",
            "维度2",
            "维度3",
            "维度4"});
            this.comboBox52.Location = new System.Drawing.Point(181, 37);
            this.comboBox52.Name = "comboBox52";
            this.comboBox52.Size = new System.Drawing.Size(107, 20);
            this.comboBox52.TabIndex = 1;
            // 
            // comboBox100
            // 
            this.comboBox100.FormattingEnabled = true;
            this.comboBox100.Items.AddRange(new object[] {
            "维度1",
            "维度2",
            "维度3",
            "维度4"});
            this.comboBox100.Location = new System.Drawing.Point(17, 37);
            this.comboBox100.Name = "comboBox100";
            this.comboBox100.Size = new System.Drawing.Size(107, 20);
            this.comboBox100.TabIndex = 0;
            // 
            // groupBox17
            // 
            this.groupBox17.BackColor = System.Drawing.SystemColors.Control;
            this.groupBox17.Controls.Add(this.comboBox51);
            this.groupBox17.Controls.Add(this.comboBox50);
            this.groupBox17.Controls.Add(this.comboBox49);
            this.groupBox17.Controls.Add(this.comboBox54);
            this.groupBox17.Location = new System.Drawing.Point(12, 12);
            this.groupBox17.Name = "groupBox17";
            this.groupBox17.Size = new System.Drawing.Size(939, 83);
            this.groupBox17.TabIndex = 19;
            this.groupBox17.TabStop = false;
            this.groupBox17.Text = "请选择数据源";
            // 
            // comboBox51
            // 
            this.comboBox51.FormattingEnabled = true;
            this.comboBox51.Items.AddRange(new object[] {
            "数据源1",
            "数据源2",
            "数据源3"});
            this.comboBox51.Location = new System.Drawing.Point(515, 37);
            this.comboBox51.Name = "comboBox51";
            this.comboBox51.Size = new System.Drawing.Size(107, 20);
            this.comboBox51.TabIndex = 3;
            // 
            // comboBox50
            // 
            this.comboBox50.FormattingEnabled = true;
            this.comboBox50.Items.AddRange(new object[] {
            "数据源1",
            "数据源2",
            "数据源3"});
            this.comboBox50.Location = new System.Drawing.Point(353, 37);
            this.comboBox50.Name = "comboBox50";
            this.comboBox50.Size = new System.Drawing.Size(107, 20);
            this.comboBox50.TabIndex = 2;
            // 
            // comboBox49
            // 
            this.comboBox49.FormattingEnabled = true;
            this.comboBox49.Items.AddRange(new object[] {
            "数据源1",
            "数据源2",
            "数据源3"});
            this.comboBox49.Location = new System.Drawing.Point(181, 37);
            this.comboBox49.Name = "comboBox49";
            this.comboBox49.Size = new System.Drawing.Size(107, 20);
            this.comboBox49.TabIndex = 1;
            // 
            // comboBox54
            // 
            this.comboBox54.FormattingEnabled = true;
            this.comboBox54.Items.AddRange(new object[] {
            "数据源1",
            "数据源2",
            "数据源3"});
            this.comboBox54.Location = new System.Drawing.Point(17, 37);
            this.comboBox54.Name = "comboBox54";
            this.comboBox54.Size = new System.Drawing.Size(107, 20);
            this.comboBox54.TabIndex = 0;
            // 
            // groupBox30
            // 
            this.groupBox30.Controls.Add(this.panel8);
            this.groupBox30.Location = new System.Drawing.Point(12, 411);
            this.groupBox30.Name = "groupBox30";
            this.groupBox30.Size = new System.Drawing.Size(938, 338);
            this.groupBox30.TabIndex = 16;
            this.groupBox30.TabStop = false;
            this.groupBox30.Text = "报表展示";
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.button15);
            this.panel8.Controls.Add(this.button16);
            this.panel8.Location = new System.Drawing.Point(6, 20);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(926, 414);
            this.panel8.TabIndex = 3;
            // 
            // button15
            // 
            this.button15.Location = new System.Drawing.Point(207, 323);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(75, 23);
            this.button15.TabIndex = 1;
            this.button15.Text = "上一组";
            this.button15.UseVisualStyleBackColor = true;
            // 
            // button16
            // 
            this.button16.Location = new System.Drawing.Point(596, 323);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(75, 23);
            this.button16.TabIndex = 2;
            this.button16.Text = "下一组";
            this.button16.UseVisualStyleBackColor = true;
            // 
            // groupBox22
            // 
            this.groupBox22.BackColor = System.Drawing.SystemColors.Control;
            this.groupBox22.Controls.Add(this.comboBox79);
            this.groupBox22.Controls.Add(this.comboBox80);
            this.groupBox22.Controls.Add(this.comboBox81);
            this.groupBox22.Controls.Add(this.comboBox82);
            this.groupBox22.Controls.Add(this.comboBox83);
            this.groupBox22.Controls.Add(this.comboBox84);
            this.groupBox22.Location = new System.Drawing.Point(12, 290);
            this.groupBox22.Name = "groupBox22";
            this.groupBox22.Size = new System.Drawing.Size(939, 83);
            this.groupBox22.TabIndex = 15;
            this.groupBox22.TabStop = false;
            this.groupBox22.Text = "表示方式";
            // 
            // comboBox79
            // 
            this.comboBox79.FormattingEnabled = true;
            this.comboBox79.Items.AddRange(new object[] {
            "柱状",
            "条形",
            "网状",
            "环状",
            "圆形"});
            this.comboBox79.Location = new System.Drawing.Point(787, 37);
            this.comboBox79.Name = "comboBox79";
            this.comboBox79.Size = new System.Drawing.Size(107, 20);
            this.comboBox79.TabIndex = 5;
            // 
            // comboBox80
            // 
            this.comboBox80.FormattingEnabled = true;
            this.comboBox80.Items.AddRange(new object[] {
            "柱状",
            "条形",
            "网状",
            "环状",
            "圆形"});
            this.comboBox80.Location = new System.Drawing.Point(626, 37);
            this.comboBox80.Name = "comboBox80";
            this.comboBox80.Size = new System.Drawing.Size(107, 20);
            this.comboBox80.TabIndex = 4;
            // 
            // comboBox81
            // 
            this.comboBox81.FormattingEnabled = true;
            this.comboBox81.Items.AddRange(new object[] {
            "柱状",
            "条形",
            "网状",
            "环状",
            "圆形"});
            this.comboBox81.Location = new System.Drawing.Point(472, 37);
            this.comboBox81.Name = "comboBox81";
            this.comboBox81.Size = new System.Drawing.Size(107, 20);
            this.comboBox81.TabIndex = 3;
            // 
            // comboBox82
            // 
            this.comboBox82.FormattingEnabled = true;
            this.comboBox82.Items.AddRange(new object[] {
            "柱状",
            "条形",
            "网状",
            "环状",
            "圆形"});
            this.comboBox82.Location = new System.Drawing.Point(321, 37);
            this.comboBox82.Name = "comboBox82";
            this.comboBox82.Size = new System.Drawing.Size(107, 20);
            this.comboBox82.TabIndex = 2;
            // 
            // comboBox83
            // 
            this.comboBox83.FormattingEnabled = true;
            this.comboBox83.Items.AddRange(new object[] {
            "柱状",
            "条形",
            "网状",
            "环状",
            "圆形"});
            this.comboBox83.Location = new System.Drawing.Point(167, 37);
            this.comboBox83.Name = "comboBox83";
            this.comboBox83.Size = new System.Drawing.Size(107, 20);
            this.comboBox83.TabIndex = 1;
            // 
            // comboBox84
            // 
            this.comboBox84.FormattingEnabled = true;
            this.comboBox84.Items.AddRange(new object[] {
            "柱状",
            "条形",
            "网状",
            "环状",
            "圆形"});
            this.comboBox84.Location = new System.Drawing.Point(17, 37);
            this.comboBox84.Name = "comboBox84";
            this.comboBox84.Size = new System.Drawing.Size(107, 20);
            this.comboBox84.TabIndex = 0;
            // 
            // Customized
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1222, 750);
            this.Controls.Add(this.groupBox33);
            this.Controls.Add(this.groupBox32);
            this.Controls.Add(this.groupBox17);
            this.Controls.Add(this.groupBox30);
            this.Controls.Add(this.groupBox22);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "Customized";
            this.Text = "定制";
            this.groupBox33.ResumeLayout(false);
            this.groupBox32.ResumeLayout(false);
            this.groupBox17.ResumeLayout(false);
            this.groupBox30.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.groupBox22.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox33;
        private System.Windows.Forms.ComboBox comboBox101;
        private System.Windows.Forms.ComboBox comboBox99;
        private System.Windows.Forms.ComboBox comboBox98;
        private System.Windows.Forms.ComboBox comboBox104;
        private System.Windows.Forms.GroupBox groupBox32;
        private System.Windows.Forms.ComboBox comboBox97;
        private System.Windows.Forms.ComboBox comboBox53;
        private System.Windows.Forms.ComboBox comboBox52;
        private System.Windows.Forms.ComboBox comboBox100;
        private System.Windows.Forms.GroupBox groupBox17;
        private System.Windows.Forms.ComboBox comboBox51;
        private System.Windows.Forms.ComboBox comboBox50;
        private System.Windows.Forms.ComboBox comboBox49;
        private System.Windows.Forms.ComboBox comboBox54;
        private System.Windows.Forms.GroupBox groupBox30;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.Button button16;
        private System.Windows.Forms.GroupBox groupBox22;
        private System.Windows.Forms.ComboBox comboBox79;
        private System.Windows.Forms.ComboBox comboBox80;
        private System.Windows.Forms.ComboBox comboBox81;
        private System.Windows.Forms.ComboBox comboBox82;
        private System.Windows.Forms.ComboBox comboBox83;
        private System.Windows.Forms.ComboBox comboBox84;

    }
}