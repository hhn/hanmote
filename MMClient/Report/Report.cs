﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MMClient.RA;
using WeifenLuo.WinFormsUI.Docking;
using MMClient.RA.PurchaseExpense;
using MMClient.RA.PurchaseMonitor;
using MMClient.RA.SourseTracing;
using MMClient.RA.ConstractExecution;
using MMClient.RA.SupplierPerformance;
using MMClient.RA.SupplierAnalysis;
using MMClient.RA.SupplierDependentFactors;
using MMClient.RA.Customized;
using MMClient.RA.StockAnalysis;

namespace MMClient
{
    public partial class Report : Form
    {
        #region 1.采购监控分析
        Form_QuoteAnalysis fqa = null;//供应商的报价分析
        Monitor_MaterialABCAnalysis mmABC = null;//物料的ABC类分析
        Monitor_SupplierABCAnalysis msABC = null;//供应商的ABC类分析
        #endregion

        #region 2.采购花费分析
        Cost_PurchaseCostDistribution cpcd = null; //采购花费分布
        CostStructureAnalysis costSA = null;   //成本结构分析
        CostComparativeAnalysis costCA = null;   //成本比较分析
        CostTrendAnalysis costTA = null;          //成本趋势分析
        #endregion

        #region 3.寻源
        Source_SupplierQuote ssq = null;  //供应商历史报价
        MaterialPriceOverview pao = null; //价格分析
        SupplierBiddingAnalysis sba = null; //供应商给竞价分析
        #endregion

        #region 4.合同执行
        Contract_ExecutionAnalysis cea = null; //合同执行进度
        ConstractionManagement cm = null; //合同管理分析
        #endregion

        #region 5.供应商绩效
        SupplierComparativeAnalysis ssr = null;//供应商对比分析
        SupplierTrendAnalysis sta = null;//供应商趋势报表
        SupplierScoreRanking sScoreRanking = null;
        #endregion

        #region 6.库存分析
        //CrystalReport_StockAgeAutoAnalysis crt_saaa = new CrystalReport_StockAgeAutoAnalysis(); //库龄自动分析表
        StockAgeAnalysis saa = null;//库龄分析
        StockABCAnalysis sABCa = null;//库存物料ABC类分析
        #endregion

        #region 7.供应商分析
        SupplierAnalysis sas = null;    //7.供应商分析
        #endregion

        #region 8.供应商依赖因素分析
        SupplierDependentFactors sdf = null; //供应商依赖因素分析
        #endregion

        #region 9.定制
        Customized ct = null;   //8.定制
        #endregion

        public Report()
        {
            InitializeComponent();
            //初始化时就展开所有的节点
            this.tv_Rt.ExpandAll();
        }

        private void tv_Rt_AfterSelect(object sender, TreeViewEventArgs e)
        {
            //string ss = null;
            //ss = tv_Rt.SelectedNode.Text;
            int node = -1;
            node = Convert.ToInt32(e.Node.Tag);//用事件e来获取选中的节点
            switch (node)
            {
                //case "物料主数据":
                //    {
                //        mtq.TopLevel = false;
                //        mtq.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                //        mtq.Show(this.dockPanel1, DockState.Document);


                //        break;
                //    }

                #region 1.采购监控分析
                
              
                case 112:
                    {
                        if(fqa == null || fqa.IsDisposed)
                        {
                            fqa = new Form_QuoteAnalysis();
                        }
                        fqa.TopLevel = false;
                        fqa.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                        fqa.Show(this.dockPanel1, DockState.Document);
                        break;
                    }
               
                case 14:
                    {
                        if(mmABC == null || mmABC.IsDisposed)
                        {
                            mmABC = new Monitor_MaterialABCAnalysis();
                        }
                        mmABC.TopLevel = false;
                        mmABC.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                        mmABC.Show(this.dockPanel1,DockState.Document);
                        break;
                    }
                case 15:
                    {
                        if(msABC == null || msABC.IsDisposed)
                        {
                            msABC = new Monitor_SupplierABCAnalysis();
                        }
                        msABC.TopLevel = false;
                        msABC.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                        msABC.Show(this.dockPanel1,DockState.Document);
                        break;
                    }
                #endregion

                #region 2.采购花费分析
                
                case 211:
                    {
                        if (cpcd == null || cpcd.IsDisposed)
                        {
                            cpcd = new Cost_PurchaseCostDistribution ();
                        }
                        cpcd.TopLevel = false;
                        cpcd.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                        cpcd.Show(this.dockPanel1, DockState.Document);
                        break;
                    }

                case 212:
                    {
                        if (costSA == null || costSA.IsDisposed)
                        {
                            costSA = new CostStructureAnalysis();
                        }
                        costSA.TopLevel = false;
                        costSA.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                        costSA.Show(this.dockPanel1, DockState.Document);
                        break;
                    }

                case 213:
                    {
                        if (costCA == null || costCA.IsDisposed)
                        {
                            costCA = new CostComparativeAnalysis();
                        }
                        costCA.TopLevel = false;
                        costCA.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                        costCA.Show(this.dockPanel1, DockState.Document);
                        break;
                    }

                case 214:
                    {
                        if (costTA == null || costTA.IsDisposed)
                        {
                            costTA = new CostTrendAnalysis();
                        }
                        costTA.TopLevel = false;
                        costTA.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                        costTA.Show(this.dockPanel1, DockState.Document);
                        break;
                    }

                #endregion

                #region 3.寻源分析
                
                case 311:
                    {
                        if (ssq == null || ssq.IsDisposed)
                        {
                            ssq = new Source_SupplierQuote();
                        }
                        ssq.TopLevel = false;
                        ssq.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                        ssq.Show(this.dockPanel1, DockState.Document);
                        break;
                    }
                case 312:
                    {
                        if (pao == null || pao.IsDisposed)
                        {
                            pao = new MaterialPriceOverview();                        
                        }
                        pao.TopLevel = false;
                        pao.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                        pao.Show(this.dockPanel1, DockState.Document);
                        break;
                    }
                case 313:
                    {
                        if (sba == null || sba.IsDisposed)
                        {
                            sba = new SupplierBiddingAnalysis();
                        }
                        sba.TopLevel = false;
                        sba.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                        sba.Show(this.dockPanel1, DockState.Document);
                        break;
                    }
                #endregion

                #region 4.合同执行分析
               

                case 411:
                    {
                        if (cea== null || cea.IsDisposed)
                        {
                            cea = new Contract_ExecutionAnalysis();
                        }
                        cea.TopLevel = false;
                        cea.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                        cea.Show(this.dockPanel1, DockState.Document);
                        break;
                    }

                case 413:
                    { 
                        if(cm ==null || cm.IsDisposed)
                        {
                            cm = new ConstractionManagement();
                        }
                        cm.TopLevel = false;
                        cm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                        cm.Show(this.dockPanel1, DockState.Document);
                        break;
                    }

                #endregion

                #region 5.供应商绩效分析
                
                case 51:
                    {
                        if(ssr == null || ssr.IsDisposed)
                        {
                            ssr = new SupplierComparativeAnalysis ();
                        }
                        ssr.TopLevel = false;
                        ssr.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                        ssr.Show(this.dockPanel1, DockState.Document);
                        break;
                    }

                case 52:
                    {
                        if (sta == null || sta.IsDisposed)
                        {
                            sta = new SupplierTrendAnalysis();
                        }
                        sta.TopLevel = false;
                        sta.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                        sta.Show(this.dockPanel1, DockState.Document);
                        break;
                    }

                case 53:
                    {
                        if (sScoreRanking == null || sScoreRanking.IsDisposed)
                        {
                            sScoreRanking = new SupplierScoreRanking();
                        }
                        sScoreRanking.TopLevel = false;
                        sScoreRanking.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                        sScoreRanking.Show(this.dockPanel1,DockState.Document);
                        break;
                    }

                #endregion

                #region 6.库存分析
               
                case 61:
                    {
                        if (saa == null || saa.IsDisposed)
                        {
                            saa = new StockAgeAnalysis();
                        }
                        saa.TopLevel = false;
                        saa.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                        saa.Show(this.dockPanel1, DockState.Document);
                        break;
                    }
                case 62:
                    {
                        if (sABCa == null || sABCa.IsDisposed)
                        {
                            sABCa = new StockABCAnalysis();
                        }
                        sABCa.TopLevel = false;
                        sABCa.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                        sABCa.Show(this.dockPanel1, DockState.Document);
                        break;
                    }
                #endregion

                #region 7.供应商分析
                case 7:
                    {
                        if(sas == null || sas.IsDisposed)
                        {
                            sas = new SupplierAnalysis();
                        }
                        sas.TopLevel = false;
                        sas.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                        sas.Show(this.dockPanel1, DockState.Document);
                        break;
                    }
                #endregion

                #region 8、供应商依赖因素分析
                case 8:
                    {
                        if (sdf == null || sdf.IsDisposed)
                        {
                            sdf = new SupplierDependentFactors();
                        }
                        sdf.TopLevel = false;
                        sdf.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                        sdf.Show(this.dockPanel1, DockState.Document);
                        break;
                    }
                #endregion

                #region 9.定制
                case 9:
                    {
                        if(ct == null || ct.IsDisposed)
                        {
                            ct = new Customized();
                        }
                        ct.TopLevel = false;
                        ct.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                        ct.Show(this.dockPanel1, DockState.Document);
                        break;
                    }
                #endregion
               
            }
            //this.tv_Rt.SelectedNode = null;
         }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
