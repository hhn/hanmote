﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MMClient.RA;
using Lib.SqlServerDAL;
using ChartDirector;
using MMClient.RA.ChartClass;
using Lib.Common.CommonUtils;

namespace MMClient.RA.StockAnalysis
{
    public partial class StockABCAnalysis : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        #region 相关打印类
        ChartPrinter MyChartPrinter;
        DataGridViewPrinter MyDataGridViewPrinter;
        #endregion

        SA_StockMaterialABC sasmabc = new SA_StockMaterialABC();

        #region 窗体初始化程序
        public StockABCAnalysis()
        {
            InitializeComponent();

            //为列标题栏添加一个“行号”
            this.dgv_DetailChart.TopLeftHeaderCell.Value = "行号";

            this.cbb_Type.SelectedIndex = 0;
        }
        #endregion

        #region 生成报表按钮
        /// <summary>
        /// 生成报表
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_Query_Click(object sender, EventArgs e)
        {
            string sqlText = null;
            if (this.cbb_Type.SelectedIndex == 0)
            {
                sqlText = @"WITH T AS (SELECT [Order].Material_ID AS ID, Material_Type.Material_ID AS Name, Price, Material_Count, Price*Material_Count AS TotalPrice, SUM(Price*Material_Count) OVER() AS Total, Price*Material_Count/SUM(Price*Material_Count) OVER() AS Share, CAST((CAST(Material_Count AS float))/(SUM(CAST(Material_Count AS int)) OVER()) AS decimal(5,4))AS QShare,ROW_NUMBER() OVER(ORDER BY Price*Material_Count DESC) AS ROW  FROM [Order] join Material_Type on [Order].Material_ID=Material_Type.Material_ID  WHERE [Order].Begin_Time >= '" + this.dtp_StartTime.Value.ToString("yyyy-MM-dd") + "' and [Order].Begin_Time <= '" + this.dtp_EndTime.Value.ToString("yyyy-MM-dd") + "') SELECT  Name, TotalPrice, Share,(select SUM(Share) from T b where b.ROW<=a.ROW) as CumShare,(select SUM(QShare) from T b where b.ROW<=a.ROW) as CumQShare FROM T a  ";
                //             dt1 = DBHelper.ExecuteQueryDT(sqlText);
            }
            else if (this.cbb_Type.SelectedIndex == 1)
            {
                sqlText = @"WITH T AS (SELECT [Order].Material_ID AS ID, Material_Type.Material_ID AS Name, Price, Material_Count, Price*Material_Count AS TotalPrice, SUM(Price*Material_Count) OVER() AS Total, Price*Material_Count/SUM(Price*Material_Count) OVER() AS Share, CAST((CAST(Material_Count AS float))/(SUM(CAST(Material_Count AS int)) OVER()) AS decimal(5,4))AS QShare,ROW_NUMBER() OVER(ORDER BY Price*Material_Count DESC) AS ROW  FROM [Order] join Material_Type on [Order].Material_ID=Material_Type.Material_ID  WHERE [Order].Begin_Time >= '" + this.dtp_StartTime.Value.ToString("yyyy-MM-dd") + "' and [Order].Begin_Time <= '" + this.dtp_EndTime.Value.ToString("yyyy-MM-dd") + "') SELECT  Name, TotalPrice, Share,(select SUM(Share) from T b where b.ROW<=a.ROW) as CumShare,(select SUM(QShare) from T b where b.ROW<=a.ROW) as CumQShare FROM T a  WHERE  (select SUM(Share) from T b where b.ROW<=a.ROW) < 0.75";

            }
            else if (this.cbb_Type.SelectedIndex == 2)
            {
                sqlText = @"WITH T AS (SELECT [Order].Material_ID AS ID, Material_Type.Material_ID AS Name, Price, Material_Count, Price*Material_Count AS TotalPrice, SUM(Price*Material_Count) OVER() AS Total, Price*Material_Count/SUM(Price*Material_Count) OVER() AS Share, CAST((CAST(Material_Count AS float))/(SUM(CAST(Material_Count AS int)) OVER()) AS decimal(5,4))AS QShare,ROW_NUMBER() OVER(ORDER BY Price*Material_Count DESC) AS ROW  FROM [Order] join Material_Type on [Order].Material_ID=Material_Type.Material_ID  WHERE [Order].Begin_Time >= '" + this.dtp_StartTime.Value.ToString("yyyy-MM-dd") + "' and [Order].Begin_Time <= '" + this.dtp_EndTime.Value.ToString("yyyy-MM-dd") + "') SELECT  Name, TotalPrice, Share,(select SUM(Share) from T b where b.ROW<=a.ROW) as CumShare,(select SUM(QShare) from T b where b.ROW<=a.ROW) as CumQShare FROM T a  WHERE  (select SUM(Share) from T b where b.ROW<=a.ROW) >= 0.75 and (select SUM(Share) from T b where b.ROW<=a.ROW) < 0.92";
            }
            else
            {
                sqlText = @"WITH T AS (SELECT [Order].Material_ID AS ID, Material_Type.Material_ID AS Name, Price, Material_Count, Price*Material_Count AS TotalPrice, SUM(Price*Material_Count) OVER() AS Total, Price*Material_Count/SUM(Price*Material_Count) OVER() AS Share, CAST((CAST(Material_Count AS float))/(SUM(CAST(Material_Count AS int)) OVER()) AS decimal(5,4))AS QShare,ROW_NUMBER() OVER(ORDER BY Price*Material_Count DESC) AS ROW  FROM [Order] join Material_Type on [Order].Material_ID=Material_Type.Material_ID  WHERE [Order].Begin_Time >= '" + this.dtp_StartTime.Value.ToString("yyyy-MM-dd") + "' and [Order].Begin_Time <= '" + this.dtp_EndTime.Value.ToString("yyyy-MM-dd") + "') SELECT  Name, TotalPrice, Share,(select SUM(Share) from T b where b.ROW<=a.ROW) as CumShare,(select SUM(QShare) from T b where b.ROW<=a.ROW) as CumQShare FROM T a  WHERE  (select SUM(Share) from T b where b.ROW<=a.ROW) >= 0.92 and (select SUM(Share) from T b where b.ROW<=a.ROW) < 1.0";
            }
            try
            {
                DataTable dt = DBHelper.ExecuteQueryDT(sqlText);
                this.DataGridShow(dt);
                WinChartViewer viewer = this.chartViewer1;
                this.StockABCAnalysisChart(dt, viewer);
            }
            catch (Exception ex)
            {
                MessageUtil.ShowError(ex.Message);
            }
        }
        #endregion

        #region 与DataGridView操作相关的方法
        /// <summary>
        /// 根据不同条件，datagridview某列显示不同颜色
        /// </summary>
        private void DataGridColor()
        {
            double temp = 0;
            for (int i = 0; i < dgv_DetailChart.Rows.Count; i++)
            {
                if (dgv_DetailChart.Rows[i].Cells[3].Value != null)
                {
                    temp = Convert.ToDouble(dgv_DetailChart.Rows[i].Cells[3].Value);
                    if (temp < 0.75)
                    {
                        dgv_DetailChart.Rows[i].Cells[3].Style.BackColor = System.Drawing.Color.GreenYellow;
                    }
                    else if (temp >= 0.75 && temp < 0.92)
                    {
                        dgv_DetailChart.Rows[i].Cells[3].Style.BackColor = System.Drawing.Color.Yellow;
                    }
                    else
                    {
                        dgv_DetailChart.Rows[i].Cells[3].Style.BackColor = System.Drawing.Color.Pink;
                    }
                }
            }
        }
        /// <summary>
        /// 将数据绑定到datagridview上
        /// </summary>
        /// <param name="dt"></param>
        private void DataGridShow(DataTable dt)
        {
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    this.dgv_DetailChart.DataSource = dt;
                    this.DataGridColor();
                }
            }
        }
        #endregion

        #region 绘制ABC类分析图
        /// <summary>
        /// 绘制ABC类分析图
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="viewer"></param>
        private void StockABCAnalysisChart(DataTable dt, WinChartViewer viewer)
        {
            this.sasmabc.createChart(dt, viewer);
        }
        #endregion

        #region 库存物料ABC类分析图打印程序
        /// <summary>
        /// 库存物料ABC类分析图打印
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_Print_Click(object sender, EventArgs e)
        {
            if (this.TransforToImage())
            {
                this.chartPrintDocument.Print();
            }
        }
        /// <summary>
        /// 库存物料ABC类分析图打印预览
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_Preview_Click(object sender, EventArgs e)
        {
            if (this.TransforToImage())
            {
                PrintPreviewDialog MyPrintPreviewDialog = new PrintPreviewDialog();
                MyPrintPreviewDialog.Document = chartPrintDocument;
                MyPrintPreviewDialog.ShowDialog();
            }
        }

        /// <summary>
        /// 库存物料ABC类分析图打印进程
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void chartPrintDocument_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            MyChartPrinter.DrawChart(e.Graphics); 
        }

        /// <summary>
        /// 将图表转换成一张图片
        /// </summary>
        /// <returns></returns>
        private bool TransforToImage()
        {
            chartPageSetupDialog.ShowDialog();
            //允许用户选择打印机
            PrintDialog chartPrintDialog = new PrintDialog();
            chartPrintDialog.AllowCurrentPage = true;
            chartPrintDialog.AllowPrintToFile = true;
            chartPrintDialog.AllowSelection = true;
            chartPrintDialog.AllowSomePages = true;
            chartPrintDialog.PrintToFile = false;
            chartPrintDialog.ShowHelp = true;
            chartPrintDialog.ShowHelp = true;
            if (chartPrintDialog.ShowDialog() != DialogResult.OK)
            {
                return false;
            }
            //获取打印文档名称
            string titleName = this.lb_ChartTitle.Text.Trim();
            chartPrintDocument.DocumentName = titleName;
            chartPrintDocument.PrinterSettings = chartPrintDialog.PrinterSettings;
            chartPrintDocument.DefaultPageSettings = chartPageSetupDialog.PageSettings;

            //this.panel_Chart.BackColor = Color.White;
            Bitmap bitmap = new Bitmap(this.panel_Chart.Width, this.panel_Chart.Height);
            this.panel_Chart.DrawToBitmap(bitmap, new System.Drawing.Rectangle(0, 0, bitmap.Width, bitmap.Height));
            MyChartPrinter = new ChartPrinter(bitmap, chartPrintDocument, true, true, titleName, new Font("宋体", 14, FontStyle.Bold, GraphicsUnit.Point), Color.Black);
            return true;
        }
        #endregion

        #region 库存物料ABC类分析明细表打印程序
        /// <summary>
        /// 库存物料ABC类分析明细表打印
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_DetailChartPrint_Click(object sender, EventArgs e)
        {
            if (SetupThePrinting())
            {
                detailChartPrintDocument.Print();
            }
        }
        /// <summary>
        /// 库存物料ABC类分析明细表打印预览
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_DetailChartPreview_Click(object sender, EventArgs e)
        {
            if (SetupThePrinting())
            {
                PrintPreviewDialog MyPrintPreviewDialog = new PrintPreviewDialog();
                MyPrintPreviewDialog.Document = detailChartPrintDocument;
                MyPrintPreviewDialog.ShowDialog();
            }
        }
        /// <summary>
        /// 库存物料ABC类分析明细表打印进程
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void detailChartPrintDocument_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            bool more = MyDataGridViewPrinter.DrawDataGridView(e.Graphics);
            if (more == true)
                e.HasMorePages = true;
        }

        private bool SetupThePrinting()
        {
            detailChartPageSetupDialog.ShowDialog();
            //允许用户从 Windows 窗体应用程序中选择一台打印机，并选择文档中要打印的部分。
            PrintDialog MyPrintDialog = new PrintDialog();
            //下面均是PrintDialog的页面属性设置，true为可编辑，false为不可编辑;根据不同要求来进行设置
            MyPrintDialog.AllowCurrentPage = true;
            MyPrintDialog.AllowPrintToFile = true;
            MyPrintDialog.AllowSelection = true;
            MyPrintDialog.AllowSomePages = true;
            MyPrintDialog.PrintToFile = false;
            MyPrintDialog.ShowHelp = false;
            MyPrintDialog.ShowNetwork = false;
            //判断是否在PrintDialog中选择了"确定按钮"
            if (MyPrintDialog.ShowDialog() != DialogResult.OK)
            {
                return false;
            }
            //要打印的文档名称,默认保存文档名称
            string titleName = this.label_DetaiChart.Text.Trim();
            detailChartPrintDocument.DocumentName = titleName;
            detailChartPrintDocument.PrinterSettings = MyPrintDialog.PrinterSettings;
            //MyPrintDocument.DefaultPageSettings = MyPrintDialog.PrinterSettings.DefaultPageSettings;
            detailChartPrintDocument.DefaultPageSettings = detailChartPageSetupDialog.PageSettings;
            //MyPrintDocument.DefaultPageSettings.Margins = new Margins(40, 40, 40, 40);
            MyDataGridViewPrinter = new DataGridViewPrinter(dgv_DetailChart, detailChartPrintDocument, true, true, titleName, new Font("宋体", 18, FontStyle.Bold, GraphicsUnit.Point), Color.Black, true);
            return true;
        }
        #endregion

        #region 绘制DataGridView行号
        /// <summary>
        /// 绘制行号
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgv_DetailChart_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            var dgv = sender as DataGridView;
            if (dgv != null)
            {
                Rectangle rect = new Rectangle(e.RowBounds.Location.X, e.RowBounds.Location.Y, dgv.RowHeadersWidth - 4, e.RowBounds.Height);
                TextRenderer.DrawText(e.Graphics, (e.RowIndex + 1).ToString(), dgv.RowHeadersDefaultCellStyle.Font, rect, dgv.RowHeadersDefaultCellStyle.ForeColor, TextFormatFlags.VerticalCenter | TextFormatFlags.Right);
            }
        }
        #endregion

        /*
        #region 库存物料A、B、C类按钮
        private void lb_AMaterial_Click(object sender, EventArgs e)
        {
            double temp = 0;
            CurrencyManager cm = (CurrencyManager)BindingContext[dgv_DetailChart.DataSource];
            cm.SuspendBinding();   //挂起数据绑定，以防止所做的更改对绑定数据源进行更新
            for (int i = 0; i < dgv_DetailChart.Rows.Count; i++)
            {
                if (dgv_DetailChart.Rows[i].Cells[3].Value != null)
                {
                    temp = Convert.ToDouble(dgv_DetailChart.Rows[i].Cells[3].Value);
                    if (temp < 0.75)
                    {
                        dgv_DetailChart.Rows[i].Visible = true;
                    }
                    else if (temp >= 0.75 && temp < 0.92)
                    {
                        dgv_DetailChart.Rows[i].Visible = false;
                    }
                    else
                    {
                        dgv_DetailChart.Rows[i].Visible = false;
                    }
                }
            }
            cm.ResumeBinding();  //恢复datagridview数据绑定
        }

        private void lb_BMaterial_Click(object sender, EventArgs e)
        {
            double temp = 0;
            CurrencyManager cm = (CurrencyManager)BindingContext[dgv_DetailChart.DataSource];
            cm.SuspendBinding();   //挂起数据绑定，以防止所做的更改对绑定数据源进行更新
            for (int i = 0; i < dgv_DetailChart.Rows.Count; i++)
            {

                if (dgv_DetailChart.Rows[i].Cells[3].Value != null)
                {
                    temp = Convert.ToDouble(dgv_DetailChart.Rows[i].Cells[3].Value);
                    if (temp < 0.75)
                    {
                        dgv_DetailChart.Rows[i].Visible = false;
                    }
                    else if (temp >= 0.75 && temp < 0.92)
                    {
                        dgv_DetailChart.Rows[i].Visible = true;
                    }
                    else
                    {
                        dgv_DetailChart.Rows[i].Visible = false;
                    }
                }
            }
            cm.ResumeBinding();  //恢复datagridview数据绑定
        }

        private void lb_CMaterial_Click(object sender, EventArgs e)
        {
            double temp = 0;
            CurrencyManager cm = (CurrencyManager)BindingContext[dgv_DetailChart.DataSource];
            cm.SuspendBinding();   //挂起数据绑定，以防止所做的更改对绑定数据源进行更新
            for (int i = 0; i < dgv_DetailChart.Rows.Count; i++)
            {
                if (dgv_DetailChart.Rows[i].Cells[3].Value != null)
                {
                    temp = Convert.ToDouble(dgv_DetailChart.Rows[i].Cells[3].Value);
                    if (temp < 0.75)
                    {
                        dgv_DetailChart.Rows[i].Visible = false;
                    }
                    else if (temp >= 0.75 && temp < 0.92)
                    {
                        dgv_DetailChart.Rows[i].Visible = false;
                    }
                    else
                    {
                        dgv_DetailChart.Rows[i].Visible = true;
                    }
                }
            }
            cm.ResumeBinding();  //恢复datagridview数据绑定
        }
        #endregion
         * */

        #region 钻取图表
        /// <summary>
        /// 显示钻取窗口
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void chartViewer1_ClickHotSpot_1(object sender, WinHotSpotEventArgs e)
        {
            //new ParamViewer().Display(sender, e);
            
            MessageBox.Show(e.GetAttrValues().ToString());
            //new ComprehensiveScoreDetail().ShowDialog();
        }
        #endregion

        private void btn_AMaterial_Click(object sender, EventArgs e)
        {
            try
            {
                double temp = 0;
                CurrencyManager cm = (CurrencyManager)BindingContext[dgv_DetailChart.DataSource];
                cm.SuspendBinding();   //挂起数据绑定，以防止所做的更改对绑定数据源进行更新
                for (int i = 0; i < dgv_DetailChart.Rows.Count; i++)
                {
                    if (dgv_DetailChart.Rows[i].Cells[3].Value != null)
                    {
                        temp = Convert.ToDouble(dgv_DetailChart.Rows[i].Cells[3].Value);
                        if (temp < 0.75)
                        {
                            dgv_DetailChart.Rows[i].Visible = true;
                        }
                        else if (temp >= 0.75 && temp < 0.92)
                        {
                            dgv_DetailChart.Rows[i].Visible = false;
                        }
                        else
                        {
                            dgv_DetailChart.Rows[i].Visible = false;
                        }
                    }
                }
                cm.ResumeBinding();  //恢复datagridview数据绑定
            }
            catch (Exception ex)
            {
                MessageUtil.ShowError(ex.Message);
            }
        }

        private void btn_BMaterial_Click(object sender, EventArgs e)
        {
            try
            {
                double temp = 0;
                CurrencyManager cm = (CurrencyManager)BindingContext[dgv_DetailChart.DataSource];
                cm.SuspendBinding();   //挂起数据绑定，以防止所做的更改对绑定数据源进行更新
                for (int i = 0; i < dgv_DetailChart.Rows.Count; i++)
                {

                    if (dgv_DetailChart.Rows[i].Cells[3].Value != null)
                    {
                        temp = Convert.ToDouble(dgv_DetailChart.Rows[i].Cells[3].Value);
                        if (temp < 0.75)
                        {
                            dgv_DetailChart.Rows[i].Visible = false;
                        }
                        else if (temp >= 0.75 && temp < 0.92)
                        {
                            dgv_DetailChart.Rows[i].Visible = true;
                        }
                        else
                        {
                            dgv_DetailChart.Rows[i].Visible = false;
                        }
                    }
                }
                cm.ResumeBinding();  //恢复datagridview数据绑定
            }
            catch (Exception ex)
            {
                MessageUtil.ShowError(ex.Message);
            }
        }

        private void btn_CMaterial_Click(object sender, EventArgs e)
        {
            try
            {
                double temp = 0;
                CurrencyManager cm = (CurrencyManager)BindingContext[dgv_DetailChart.DataSource];
                cm.SuspendBinding();   //挂起数据绑定，以防止所做的更改对绑定数据源进行更新
                for (int i = 0; i < dgv_DetailChart.Rows.Count; i++)
                {
                    if (dgv_DetailChart.Rows[i].Cells[3].Value != null)
                    {
                        temp = Convert.ToDouble(dgv_DetailChart.Rows[i].Cells[3].Value);
                        if (temp < 0.75)
                        {
                            dgv_DetailChart.Rows[i].Visible = false;
                        }
                        else if (temp >= 0.75 && temp < 0.92)
                        {
                            dgv_DetailChart.Rows[i].Visible = false;
                        }
                        else
                        {
                            dgv_DetailChart.Rows[i].Visible = true;
                        }
                    }
                }
                cm.ResumeBinding();  //恢复datagridview数据绑定
            }
            catch (Exception ex)
            {
                MessageUtil.ShowError(ex.Message);
            }
        }
    }
}

