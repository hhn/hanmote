﻿namespace MMClient.RA.PurchaseExpense
{
    partial class Cost_PurchaseCostDistribution
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gb_Query = new System.Windows.Forms.GroupBox();
            this.btn_query = new System.Windows.Forms.Button();
            this.dtp_endTime = new System.Windows.Forms.DateTimePicker();
            this.lb_endTime = new System.Windows.Forms.Label();
            this.lb_stratTime = new System.Windows.Forms.Label();
            this.dtp_startTime = new System.Windows.Forms.DateTimePicker();
            this.panel1 = new System.Windows.Forms.Panel();
            this.chartViewer1 = new ChartDirector.WinChartViewer();
            this.gb_Query.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartViewer1)).BeginInit();
            this.SuspendLayout();
            // 
            // gb_Query
            // 
            this.gb_Query.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.gb_Query.BackColor = System.Drawing.SystemColors.Control;
            this.gb_Query.Controls.Add(this.btn_query);
            this.gb_Query.Controls.Add(this.dtp_endTime);
            this.gb_Query.Controls.Add(this.lb_endTime);
            this.gb_Query.Controls.Add(this.lb_stratTime);
            this.gb_Query.Controls.Add(this.dtp_startTime);
            this.gb_Query.Location = new System.Drawing.Point(12, 12);
            this.gb_Query.Name = "gb_Query";
            this.gb_Query.Size = new System.Drawing.Size(979, 100);
            this.gb_Query.TabIndex = 0;
            this.gb_Query.TabStop = false;
            this.gb_Query.Text = "查询条件";
            // 
            // btn_query
            // 
            this.btn_query.Location = new System.Drawing.Point(560, 35);
            this.btn_query.Name = "btn_query";
            this.btn_query.Size = new System.Drawing.Size(88, 29);
            this.btn_query.TabIndex = 8;
            this.btn_query.Text = "报表查询";
            this.btn_query.UseVisualStyleBackColor = true;
            this.btn_query.Click += new System.EventHandler(this.btn_query_Click);
            // 
            // dtp_endTime
            // 
            this.dtp_endTime.Checked = false;
            this.dtp_endTime.Location = new System.Drawing.Point(343, 39);
            this.dtp_endTime.Name = "dtp_endTime";
            this.dtp_endTime.ShowCheckBox = true;
            this.dtp_endTime.Size = new System.Drawing.Size(130, 21);
            this.dtp_endTime.TabIndex = 1;
            // 
            // lb_endTime
            // 
            this.lb_endTime.AutoSize = true;
            this.lb_endTime.Location = new System.Drawing.Point(272, 43);
            this.lb_endTime.Name = "lb_endTime";
            this.lb_endTime.Size = new System.Drawing.Size(65, 12);
            this.lb_endTime.TabIndex = 3;
            this.lb_endTime.Text = "截止日期：";
            // 
            // lb_stratTime
            // 
            this.lb_stratTime.AutoSize = true;
            this.lb_stratTime.Location = new System.Drawing.Point(39, 43);
            this.lb_stratTime.Name = "lb_stratTime";
            this.lb_stratTime.Size = new System.Drawing.Size(65, 12);
            this.lb_stratTime.TabIndex = 2;
            this.lb_stratTime.Text = "起始时间：";
            // 
            // dtp_startTime
            // 
            this.dtp_startTime.Checked = false;
            this.dtp_startTime.Location = new System.Drawing.Point(110, 39);
            this.dtp_startTime.Name = "dtp_startTime";
            this.dtp_startTime.ShowCheckBox = true;
            this.dtp_startTime.Size = new System.Drawing.Size(130, 21);
            this.dtp_startTime.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.chartViewer1);
            this.panel1.Location = new System.Drawing.Point(13, 119);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(978, 335);
            this.panel1.TabIndex = 1;
            // 
            // chartViewer1
            // 
            this.chartViewer1.HotSpotCursor = System.Windows.Forms.Cursors.Hand;
            this.chartViewer1.Location = new System.Drawing.Point(3, 3);
            this.chartViewer1.Name = "chartViewer1";
            this.chartViewer1.Size = new System.Drawing.Size(168, 126);
            this.chartViewer1.TabIndex = 4;
            this.chartViewer1.TabStop = false;
            this.chartViewer1.ClickHotSpot += new ChartDirector.WinHotSpotEventHandler(this.chartViewer1_ClickHotSpot);
            // 
            // Cost_PurchaseCostDistribution
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1003, 466);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.gb_Query);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "Cost_PurchaseCostDistribution";
            this.Text = "物料采购花费分布";
            this.gb_Query.ResumeLayout(false);
            this.gb_Query.PerformLayout();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chartViewer1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gb_Query;
        private System.Windows.Forms.DateTimePicker dtp_endTime;
        private System.Windows.Forms.Label lb_endTime;
        private System.Windows.Forms.Label lb_stratTime;
        private System.Windows.Forms.DateTimePicker dtp_startTime;
        private System.Windows.Forms.Button btn_query;
        private System.Windows.Forms.Panel panel1;
        private ChartDirector.WinChartViewer chartViewer1;
    }
}