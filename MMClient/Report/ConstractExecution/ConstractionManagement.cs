﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Bll;
using Lib.Common.CommonUtils;

namespace MMClient.RA
{
    public partial class ConstractionManagement : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        //打印DataGridView类
        DataGridViewPrinter MyDataGridViewPrinter;

        ConstractManagementBLL cmBLL = new ConstractManagementBLL();
        //只读型常量
        private readonly string str_txt_ChooseSupplier = "供应商编码/供应商名称";
        private readonly string str_txt_Constact = "合同编号/合同名称";

        public ConstractionManagement()
        {
            InitializeComponent();

            //为列标题栏添加一个“行号”
            this.dgv_ConstractManagement.TopLeftHeaderCell.Value = "行号";

            this.cbb_PurchaseGroup.SelectedIndex = 0;
            this.txt_ChooseSupplier.Text = str_txt_ChooseSupplier;
            this.txt_Constract.Text = str_txt_Constact;
            //使用系统颜色要带系统声明即System.Drawing.Color
            this.txt_ChooseSupplier.ForeColor = System.Drawing.Color.LightGray;
            this.txt_Constract.ForeColor = System.Drawing.Color.LightGray;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = cmBLL.GetConstractInfo("aaa", "aaa", "aaa");
                this.DataGridShow(dt);
            }
            catch (Exception ex)
            {
                MessageUtil.ShowError(ex.Message);
            }
        }

        /// <summary>
        /// 将数据显示在datagridview中
        /// </summary>
        /// <param name="dt"></param>
        private void DataGridShow(DataTable dt)
        {
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    this.dgv_ConstractManagement.DataSource = dt;
                }
            }
        }

        /// <summary>
        /// 当鼠标进入textbox触发该事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_ChooseSupplier_Enter(object sender, EventArgs e)
        {
            if (this.txt_ChooseSupplier.Text.Trim().Equals(str_txt_ChooseSupplier))
            {
                //将textbox内容置空
                this.txt_ChooseSupplier.Text = "";
                //再次输入值时，颜色呈黑色
                this.txt_ChooseSupplier.ForeColor = System.Drawing.Color.Black;
            }
        }

        /// <summary>
        /// 当鼠标离开textbox时触发该时间
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_ChooseSupplier_Leave(object sender, EventArgs e)
        {
            if (this.txt_ChooseSupplier.Text.Trim() == "")
            {
                this.txt_ChooseSupplier.Text = str_txt_ChooseSupplier;
                this.txt_ChooseSupplier.ForeColor = System.Drawing.Color.LightGray;
            }
        }

        private void txt_Constract_Enter(object sender, EventArgs e)
        {
            if (this.txt_Constract.Text.Trim().Equals(str_txt_Constact))
            {
                //将textbox内容置空
                this.txt_Constract.Text = "";
                //再次输入值时，颜色呈黑色
                this.txt_Constract.ForeColor = System.Drawing.Color.Black;
            }
        }

        private void txt_Constract_Leave(object sender, EventArgs e)
        {
            if (this.txt_Constract.Text.Trim() == "")
            {
                this.txt_Constract.Text = str_txt_Constact;
                this.txt_Constract.ForeColor = System.Drawing.Color.LightGray;
            }
        }

        /// <summary>
        /// 画行号
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgv_ConstractManagement_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            var dgv = sender as DataGridView;
            if (dgv != null)
            {
                Rectangle rect = new Rectangle(e.RowBounds.Location.X, e.RowBounds.Location.Y, dgv.RowHeadersWidth / 2 + 4, e.RowBounds.Height);
                TextRenderer.DrawText(e.Graphics, (e.RowIndex + 1).ToString(), dgv.RowHeadersDefaultCellStyle.Font, rect, dgv.RowHeadersDefaultCellStyle.ForeColor, TextFormatFlags.VerticalCenter | TextFormatFlags.Right);
            }
        }

        /// <summary>
        /// 当鼠标进入单元格时发生
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgv_ConstractManagement_CellMouseEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
            {
                this.dgv_ConstractManagement.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.LightBlue;
                DataGridViewCellStyle style = new DataGridViewCellStyle();
                style.BackColor = Color.CornflowerBlue;
                this.dgv_ConstractManagement.Rows[e.RowIndex].HeaderCell.Style = style;
            }
        }

        /// <summary>
        /// 当鼠标移出单元格时发生
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgv_ConstractManagement_CellMouseLeave(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
            {
                this.dgv_ConstractManagement.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.White;
                DataGridViewCellStyle style = new DataGridViewCellStyle();
                style.BackColor = Color.WhiteSmoke;
                this.dgv_ConstractManagement.Rows[e.RowIndex].HeaderCell.Style = style;
            }
        }

        /// <summary>
        /// 点击供应商名称可以钻取供应商详细信息
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgv_ConstractManagement_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            //点击供应商名称时触发
            if (e.ColumnIndex == 0)
            {
                new SupplierDetails(this.dgv_ConstractManagement.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString(), "").ShowDialog();
            }
        }

        /// <summary>
        /// 打印
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_Print_Click(object sender, EventArgs e)
        {
            if (SetupThePrinting())
            {
                contractPrintDocument.Print();
            }
        }

        /// <summary>
        /// 打印进程
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void contractPrintDocument_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            bool more = MyDataGridViewPrinter.DrawDataGridView(e.Graphics);
            if (more == true)
                e.HasMorePages = true;
        }

        /// <summary>
        /// 打印预览
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_Preview_Click(object sender, EventArgs e)
        {
            if (SetupThePrinting())
            {
                PrintPreviewDialog MyPrintPreviewDialog = new PrintPreviewDialog();
                MyPrintPreviewDialog.Document = contractPrintDocument;
                MyPrintPreviewDialog.ShowDialog();
            }
        }


        private bool SetupThePrinting()
        {
            contractPageSetupDialog.ShowDialog();
            //允许用户从 Windows 窗体应用程序中选择一台打印机，并选择文档中要打印的部分。
            PrintDialog MyPrintDialog = new PrintDialog();
            //下面均是PrintDialog的页面属性设置，true为可编辑，false为不可编辑;根据不同要求来进行设置
            MyPrintDialog.AllowCurrentPage = true;
            MyPrintDialog.AllowPrintToFile = true;
            MyPrintDialog.AllowSelection = true;
            MyPrintDialog.AllowSomePages = true;
            MyPrintDialog.PrintToFile = false;
            MyPrintDialog.ShowHelp = false;
            MyPrintDialog.ShowNetwork = false;
            //判断是否在PrintDialog中选择了"确定按钮"
            if (MyPrintDialog.ShowDialog() != DialogResult.OK)
            {
                return false;
            }
            //要打印的文档名称,默认保存文档名称
            string titleName = this.lb_Title.Text.Trim();
            contractPrintDocument.DocumentName = titleName;
            contractPrintDocument.PrinterSettings = MyPrintDialog.PrinterSettings;
            //MyPrintDocument.DefaultPageSettings = MyPrintDialog.PrinterSettings.DefaultPageSettings;
            contractPrintDocument.DefaultPageSettings = contractPageSetupDialog.PageSettings;
            //MyPrintDocument.DefaultPageSettings.Margins = new Margins(40, 40, 40, 40);
            MyDataGridViewPrinter = new DataGridViewPrinter(dgv_ConstractManagement, contractPrintDocument, true, true, titleName, new Font("宋体", 18, FontStyle.Bold, GraphicsUnit.Point), Color.Black, true);
            return true;
        }
    }
}
