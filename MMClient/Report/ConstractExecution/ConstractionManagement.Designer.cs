﻿namespace MMClient.RA
{
    partial class ConstractionManagement
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btn_query = new System.Windows.Forms.Panel();
            this.lb_Constract = new System.Windows.Forms.Label();
            this.txt_Constract = new System.Windows.Forms.TextBox();
            this.txt_ChooseSupplier = new System.Windows.Forms.TextBox();
            this.lb_ChooseSupplier = new System.Windows.Forms.Label();
            this.cbb_PurchaseGroup = new System.Windows.Forms.ComboBox();
            this.lb_purchaseGroup = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.lb_To = new System.Windows.Forms.Label();
            this.lb_Date = new System.Windows.Forms.Label();
            this.panel_Navigation = new System.Windows.Forms.Panel();
            this.btn_Preview = new System.Windows.Forms.Button();
            this.btn_Print = new System.Windows.Forms.Button();
            this.lb_Title = new System.Windows.Forms.Label();
            this.dgv_ConstractManagement = new System.Windows.Forms.DataGridView();
            this.Column_SupplierName = new System.Windows.Forms.DataGridViewLinkColumn();
            this.Column_SupplierNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column_AnnualSales = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column_InvoicValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column_DependencyRatio = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column_ShareCategory = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column_ReceiptInAdvance = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.contractPrintDocument = new System.Drawing.Printing.PrintDocument();
            this.contractPageSetupDialog = new System.Windows.Forms.PageSetupDialog();
            this.btn_query.SuspendLayout();
            this.panel_Navigation.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_ConstractManagement)).BeginInit();
            this.SuspendLayout();
            // 
            // btn_query
            // 
            this.btn_query.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_query.BackColor = System.Drawing.SystemColors.Control;
            this.btn_query.Controls.Add(this.lb_Constract);
            this.btn_query.Controls.Add(this.txt_Constract);
            this.btn_query.Controls.Add(this.txt_ChooseSupplier);
            this.btn_query.Controls.Add(this.lb_ChooseSupplier);
            this.btn_query.Controls.Add(this.cbb_PurchaseGroup);
            this.btn_query.Controls.Add(this.lb_purchaseGroup);
            this.btn_query.Controls.Add(this.button1);
            this.btn_query.Controls.Add(this.dateTimePicker2);
            this.btn_query.Controls.Add(this.dateTimePicker1);
            this.btn_query.Controls.Add(this.lb_To);
            this.btn_query.Controls.Add(this.lb_Date);
            this.btn_query.Location = new System.Drawing.Point(3, 3);
            this.btn_query.Name = "btn_query";
            this.btn_query.Size = new System.Drawing.Size(1142, 126);
            this.btn_query.TabIndex = 0;
            // 
            // lb_Constract
            // 
            this.lb_Constract.AutoSize = true;
            this.lb_Constract.Location = new System.Drawing.Point(36, 82);
            this.lb_Constract.Name = "lb_Constract";
            this.lb_Constract.Size = new System.Drawing.Size(41, 12);
            this.lb_Constract.TabIndex = 10;
            this.lb_Constract.Text = "合同：";
            // 
            // txt_Constract
            // 
            this.txt_Constract.Location = new System.Drawing.Point(78, 78);
            this.txt_Constract.Name = "txt_Constract";
            this.txt_Constract.Size = new System.Drawing.Size(285, 21);
            this.txt_Constract.TabIndex = 9;
            this.txt_Constract.Enter += new System.EventHandler(this.txt_Constract_Enter);
            this.txt_Constract.Leave += new System.EventHandler(this.txt_Constract_Leave);
            // 
            // txt_ChooseSupplier
            // 
            this.txt_ChooseSupplier.Location = new System.Drawing.Point(494, 79);
            this.txt_ChooseSupplier.Name = "txt_ChooseSupplier";
            this.txt_ChooseSupplier.Size = new System.Drawing.Size(274, 21);
            this.txt_ChooseSupplier.TabIndex = 9;
            this.txt_ChooseSupplier.Enter += new System.EventHandler(this.txt_ChooseSupplier_Enter);
            this.txt_ChooseSupplier.Leave += new System.EventHandler(this.txt_ChooseSupplier_Leave);
            // 
            // lb_ChooseSupplier
            // 
            this.lb_ChooseSupplier.AutoSize = true;
            this.lb_ChooseSupplier.Location = new System.Drawing.Point(442, 84);
            this.lb_ChooseSupplier.Name = "lb_ChooseSupplier";
            this.lb_ChooseSupplier.Size = new System.Drawing.Size(53, 12);
            this.lb_ChooseSupplier.TabIndex = 8;
            this.lb_ChooseSupplier.Text = "供应商：";
            // 
            // cbb_PurchaseGroup
            // 
            this.cbb_PurchaseGroup.FormattingEnabled = true;
            this.cbb_PurchaseGroup.Items.AddRange(new object[] {
            "全部",
            "原材料采购组",
            "半成品采购组"});
            this.cbb_PurchaseGroup.Location = new System.Drawing.Point(494, 31);
            this.cbb_PurchaseGroup.Name = "cbb_PurchaseGroup";
            this.cbb_PurchaseGroup.Size = new System.Drawing.Size(274, 20);
            this.cbb_PurchaseGroup.TabIndex = 7;
            // 
            // lb_purchaseGroup
            // 
            this.lb_purchaseGroup.AutoSize = true;
            this.lb_purchaseGroup.Location = new System.Drawing.Point(430, 36);
            this.lb_purchaseGroup.Name = "lb_purchaseGroup";
            this.lb_purchaseGroup.Size = new System.Drawing.Size(65, 12);
            this.lb_purchaseGroup.TabIndex = 6;
            this.lb_purchaseGroup.Text = "采购组织：";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(849, 47);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(113, 33);
            this.button1.TabIndex = 4;
            this.button1.Text = "生成报表";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.Location = new System.Drawing.Point(232, 31);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(131, 21);
            this.dateTimePicker2.TabIndex = 3;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(78, 31);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(131, 21);
            this.dateTimePicker1.TabIndex = 2;
            // 
            // lb_To
            // 
            this.lb_To.AutoSize = true;
            this.lb_To.Location = new System.Drawing.Point(212, 35);
            this.lb_To.Name = "lb_To";
            this.lb_To.Size = new System.Drawing.Size(17, 12);
            this.lb_To.TabIndex = 1;
            this.lb_To.Text = "--";
            // 
            // lb_Date
            // 
            this.lb_Date.AutoSize = true;
            this.lb_Date.Location = new System.Drawing.Point(36, 35);
            this.lb_Date.Name = "lb_Date";
            this.lb_Date.Size = new System.Drawing.Size(41, 12);
            this.lb_Date.TabIndex = 0;
            this.lb_Date.Text = "日期：";
            // 
            // panel_Navigation
            // 
            this.panel_Navigation.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panel_Navigation.BackColor = System.Drawing.SystemColors.Control;
            this.panel_Navigation.Controls.Add(this.btn_Preview);
            this.panel_Navigation.Controls.Add(this.btn_Print);
            this.panel_Navigation.Controls.Add(this.lb_Title);
            this.panel_Navigation.Location = new System.Drawing.Point(3, 131);
            this.panel_Navigation.Name = "panel_Navigation";
            this.panel_Navigation.Size = new System.Drawing.Size(1142, 32);
            this.panel_Navigation.TabIndex = 2;
            // 
            // btn_Preview
            // 
            this.btn_Preview.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btn_Preview.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btn_Preview.Location = new System.Drawing.Point(1082, 4);
            this.btn_Preview.Name = "btn_Preview";
            this.btn_Preview.Size = new System.Drawing.Size(53, 23);
            this.btn_Preview.TabIndex = 3;
            this.btn_Preview.Text = "预览";
            this.btn_Preview.UseVisualStyleBackColor = true;
            this.btn_Preview.Click += new System.EventHandler(this.btn_Preview_Click);
            // 
            // btn_Print
            // 
            this.btn_Print.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btn_Print.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btn_Print.Location = new System.Drawing.Point(1020, 4);
            this.btn_Print.Name = "btn_Print";
            this.btn_Print.Size = new System.Drawing.Size(56, 23);
            this.btn_Print.TabIndex = 2;
            this.btn_Print.Text = "打印";
            this.btn_Print.UseVisualStyleBackColor = true;
            this.btn_Print.Click += new System.EventHandler(this.btn_Print_Click);
            // 
            // lb_Title
            // 
            this.lb_Title.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lb_Title.Location = new System.Drawing.Point(6, 7);
            this.lb_Title.Name = "lb_Title";
            this.lb_Title.Size = new System.Drawing.Size(250, 22);
            this.lb_Title.TabIndex = 0;
            this.lb_Title.Text = "合同管理分析";
            // 
            // dgv_ConstractManagement
            // 
            this.dgv_ConstractManagement.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgv_ConstractManagement.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgv_ConstractManagement.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_ConstractManagement.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv_ConstractManagement.ColumnHeadersHeight = 32;
            this.dgv_ConstractManagement.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgv_ConstractManagement.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column_SupplierName,
            this.Column_SupplierNo,
            this.Column_AnnualSales,
            this.Column_InvoicValue,
            this.Column_DependencyRatio,
            this.Column_ShareCategory,
            this.Column_ReceiptInAdvance});
            this.dgv_ConstractManagement.EnableHeadersVisualStyles = false;
            this.dgv_ConstractManagement.GridColor = System.Drawing.SystemColors.ScrollBar;
            this.dgv_ConstractManagement.Location = new System.Drawing.Point(3, 164);
            this.dgv_ConstractManagement.MultiSelect = false;
            this.dgv_ConstractManagement.Name = "dgv_ConstractManagement";
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.CornflowerBlue;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_ConstractManagement.RowHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.dgv_ConstractManagement.RowTemplate.DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dgv_ConstractManagement.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.dgv_ConstractManagement.RowTemplate.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.LightBlue;
            this.dgv_ConstractManagement.RowTemplate.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.Black;
            this.dgv_ConstractManagement.RowTemplate.Height = 26;
            this.dgv_ConstractManagement.RowTemplate.ReadOnly = true;
            this.dgv_ConstractManagement.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv_ConstractManagement.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv_ConstractManagement.Size = new System.Drawing.Size(1142, 382);
            this.dgv_ConstractManagement.TabIndex = 3;
            this.dgv_ConstractManagement.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_ConstractManagement_CellContentClick);
            this.dgv_ConstractManagement.CellMouseEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_ConstractManagement_CellMouseEnter);
            this.dgv_ConstractManagement.CellMouseLeave += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_ConstractManagement_CellMouseLeave);
            this.dgv_ConstractManagement.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.dgv_ConstractManagement_RowPostPaint);
            // 
            // Column_SupplierName
            // 
            this.Column_SupplierName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column_SupplierName.DataPropertyName = "Supplier_Name";
            this.Column_SupplierName.HeaderText = "供应商名称";
            this.Column_SupplierName.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline;
            this.Column_SupplierName.LinkColor = System.Drawing.Color.Blue;
            this.Column_SupplierName.Name = "Column_SupplierName";
            this.Column_SupplierName.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Column_SupplierName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Column_SupplierName.ToolTipText = "ddwd";
            this.Column_SupplierName.VisitedLinkColor = System.Drawing.Color.Purple;
            // 
            // Column_SupplierNo
            // 
            this.Column_SupplierNo.DataPropertyName = "ConstractID";
            this.Column_SupplierNo.HeaderText = "合同编号";
            this.Column_SupplierNo.Name = "Column_SupplierNo";
            this.Column_SupplierNo.ReadOnly = true;
            this.Column_SupplierNo.Width = 200;
            // 
            // Column_AnnualSales
            // 
            this.Column_AnnualSales.DataPropertyName = "OrderNum";
            this.Column_AnnualSales.HeaderText = "订单数量";
            this.Column_AnnualSales.Name = "Column_AnnualSales";
            this.Column_AnnualSales.ReadOnly = true;
            this.Column_AnnualSales.Width = 200;
            // 
            // Column_InvoicValue
            // 
            this.Column_InvoicValue.DataPropertyName = "RecieveNum";
            this.Column_InvoicValue.HeaderText = "接收数量";
            this.Column_InvoicValue.Name = "Column_InvoicValue";
            this.Column_InvoicValue.ReadOnly = true;
            this.Column_InvoicValue.Width = 200;
            // 
            // Column_DependencyRatio
            // 
            this.Column_DependencyRatio.DataPropertyName = "OnTimeRecieve";
            dataGridViewCellStyle2.Format = "0.00%";
            dataGridViewCellStyle2.NullValue = null;
            this.Column_DependencyRatio.DefaultCellStyle = dataGridViewCellStyle2;
            this.Column_DependencyRatio.HeaderText = "准时接收%";
            this.Column_DependencyRatio.Name = "Column_DependencyRatio";
            this.Column_DependencyRatio.ReadOnly = true;
            this.Column_DependencyRatio.Width = 190;
            // 
            // Column_ShareCategory
            // 
            this.Column_ShareCategory.DataPropertyName = "RelayRecieve";
            dataGridViewCellStyle3.Format = "0.00%";
            dataGridViewCellStyle3.NullValue = null;
            this.Column_ShareCategory.DefaultCellStyle = dataGridViewCellStyle3;
            this.Column_ShareCategory.HeaderText = "延迟收到%";
            this.Column_ShareCategory.Name = "Column_ShareCategory";
            this.Column_ShareCategory.ReadOnly = true;
            // 
            // Column_ReceiptInAdvance
            // 
            this.Column_ReceiptInAdvance.DataPropertyName = "AdvanceRecieve";
            dataGridViewCellStyle4.Format = "0.00%";
            dataGridViewCellStyle4.NullValue = null;
            this.Column_ReceiptInAdvance.DefaultCellStyle = dataGridViewCellStyle4;
            this.Column_ReceiptInAdvance.HeaderText = "提前收到%";
            this.Column_ReceiptInAdvance.Name = "Column_ReceiptInAdvance";
            // 
            // contractPrintDocument
            // 
            this.contractPrintDocument.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.contractPrintDocument_PrintPage);
            // 
            // contractPageSetupDialog
            // 
            this.contractPageSetupDialog.Document = this.contractPrintDocument;
            // 
            // ConstractionManagement
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1150, 520);
            this.Controls.Add(this.dgv_ConstractManagement);
            this.Controls.Add(this.panel_Navigation);
            this.Controls.Add(this.btn_query);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "ConstractionManagement";
            this.Text = "合同管理分析";
            this.btn_query.ResumeLayout(false);
            this.btn_query.PerformLayout();
            this.panel_Navigation.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_ConstractManagement)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel btn_query;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Label lb_To;
        private System.Windows.Forms.Label lb_Date;
        private System.Windows.Forms.Panel panel_Navigation;
        private System.Windows.Forms.Button btn_Preview;
        private System.Windows.Forms.Button btn_Print;
        private System.Windows.Forms.Label lb_Title;
        private System.Windows.Forms.DataGridView dgv_ConstractManagement;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txt_ChooseSupplier;
        private System.Windows.Forms.Label lb_ChooseSupplier;
        private System.Windows.Forms.ComboBox cbb_PurchaseGroup;
        private System.Windows.Forms.Label lb_purchaseGroup;
        private System.Windows.Forms.Label lb_Constract;
        private System.Windows.Forms.TextBox txt_Constract;
        private System.Windows.Forms.DataGridViewLinkColumn Column_SupplierName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column_SupplierNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column_AnnualSales;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column_InvoicValue;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column_DependencyRatio;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column_ShareCategory;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column_ReceiptInAdvance;
        private System.Drawing.Printing.PrintDocument contractPrintDocument;
        private System.Windows.Forms.PageSetupDialog contractPageSetupDialog;
    }
}