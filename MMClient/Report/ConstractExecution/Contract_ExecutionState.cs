﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ChartDirector;

namespace MMClient.RA.ConstractExecution
{
    public partial class Contract_ExecutionState : Form
    {
        public Contract_ExecutionState()
        {
            InitializeComponent();
        }

        private void btn_query_Click(object sender, EventArgs e)
        {
            WinChartViewer viewer = this.chartViewer1;
            this.createChart(viewer);
        }

        //开始画图表
        public void createChart(WinChartViewer viewer)
        {
            double[] data = { 1,2,3,2.5,1.5};
            string[] labels = { "Mon","Tue","Wen","Thu","Fri"};
            //创建一个大小为900*500像素大小的XYChart对象，
            XYChart chart = new XYChart(1050, 500, Chart.goldColor());
            //将图表画在x、y轴分别为20、20的位置，图表区域大小为800*400；
            chart.setPlotArea(48, 45, 950, 400);
            //给该图表添加标题
            chart.addTitle("价格走势", "宋体 Bold", 22).setBackground(Chart.metalColor(0x9999ff), -1, -1);
            chart.addBarLayer3(data);
            chart.swapXY();
            chart.xAxis().setLabels(labels);
            chart.xAxis().setTitle("年份", "宋体 bold Italic", 12, 3);
            chart.yAxis().setTitle("单价（元）", "宋体 bold Italic", 12, 3);
            //输出图表,将该图表在控件WinChartViewer中显示
            viewer.Chart = chart;
            viewer.ImageMap = chart.getHTMLImageMap("clickable", "",
                "title='Hour {xLabel}: Traffic {value} GBytes'");
        }

        //热点事件
        private void chartViewer1_ClickHotSpot(object sender, ChartDirector.WinHotSpotEventArgs e)
        {
            new ParamViewer().Display(sender, e);
        }
    }
}
