﻿using System;
using System.Windows.Forms;
using System.Collections;

namespace MMClient.RA
{
    public partial class ParamViewer : Form
    {
        /// <summary>
        /// ParamViewer Constructor
        /// </summary>
        public ParamViewer()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
        }

        /// <summary>
        /// ParamViewer Constructor
        /// </summary>
        public void Display(object sender, ChartDirector.WinHotSpotEventArgs e)
        {
            // Add the name of the ChartViewer control that is being clicked
            //增加被点击的chartviewer控件的名称
            listView.Items.Add(new ListViewItem(new string[] {"source", 
				((ChartDirector.WinChartViewer)sender).Name}));

            // List out the parameters of the hot spot
            //列出的参数热点
            foreach (DictionaryEntry key in e.GetAttrValues())
            {
                listView.Items.Add(new ListViewItem(
                    new string[] { (string)key.Key, (string)key.Value }));
            }

            // Display the form
            ShowDialog();
        }

        /// <summary>
        /// Handler for the OK button
        /// </summary>
        private void OKPB_Click(object sender, System.EventArgs e)
        {
            // Just close the Form
            Close();
        }
    }
}
