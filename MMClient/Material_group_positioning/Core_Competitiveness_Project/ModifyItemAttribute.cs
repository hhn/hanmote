﻿using Lib.Bll.Material_group_positioning.General;
using Lib.Common.CommonUtils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MMClient.Material_group_positioning.Core_Competitiveness_Project
{
    public partial class ModifyItemAttribute : Form
    {
        private string G;
        private string Z;
        public ModifyItemAttribute(string code_ORG, string name_ORG, String code_MTG1, String name_MTG1, String G11, String Z11)
        {
            InitializeComponent();
            ORGID.Text = code_ORG;
            ORGName.Text = name_ORG;
            MTGName.Text = name_MTG1;
            MTGID.Text = code_MTG1;
            G1.Text = G11;
            Z1.Text = Z11;
            this.G = G11;
            this.Z = Z11;
        }

        private void button1_Click(object sender, EventArgs e)//修改
        {
            GeneralBLL gn = new GeneralBLL();
            
            gn.UpdateitemMtGroup(this.G, this.Z, G1.Text, Z1.Text);
            MessageUtil.ShowWarning("修改成功！");

        }

        private void ModifyItemAttribute_Load(object sender, EventArgs e)
        {

        }
    }
}
