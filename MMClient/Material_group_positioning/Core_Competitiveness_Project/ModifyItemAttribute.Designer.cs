﻿namespace MMClient.Material_group_positioning.Core_Competitiveness_Project
{
    partial class ModifyItemAttribute
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ORGName = new System.Windows.Forms.TextBox();
            this.ORGID = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.MTGName = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.Z1 = new System.Windows.Forms.RichTextBox();
            this.L11 = new System.Windows.Forms.Label();
            this.G1 = new System.Windows.Forms.RichTextBox();
            this.L1 = new System.Windows.Forms.Label();
            this.MTGID = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // ORGName
            // 
            this.ORGName.Location = new System.Drawing.Point(102, 27);
            this.ORGName.Name = "ORGName";
            this.ORGName.ReadOnly = true;
            this.ORGName.Size = new System.Drawing.Size(100, 21);
            this.ORGName.TabIndex = 65;
            // 
            // ORGID
            // 
            this.ORGID.Location = new System.Drawing.Point(334, 27);
            this.ORGID.Name = "ORGID";
            this.ORGID.ReadOnly = true;
            this.ORGID.Size = new System.Drawing.Size(100, 21);
            this.ORGID.TabIndex = 64;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(248, 30);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 12);
            this.label3.TabIndex = 63;
            this.label3.Text = "采购组织编号";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(16, 30);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(77, 12);
            this.label4.TabIndex = 62;
            this.label4.Text = "采购组织名称";
            // 
            // MTGName
            // 
            this.MTGName.Location = new System.Drawing.Point(102, 64);
            this.MTGName.Name = "MTGName";
            this.MTGName.ReadOnly = true;
            this.MTGName.Size = new System.Drawing.Size(100, 21);
            this.MTGName.TabIndex = 61;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(238, 161);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 60;
            this.button1.Text = "修改";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Z1
            // 
            this.Z1.Location = new System.Drawing.Point(370, 103);
            this.Z1.Name = "Z1";
            this.Z1.Size = new System.Drawing.Size(160, 40);
            this.Z1.TabIndex = 59;
            this.Z1.Text = "";
            // 
            // L11
            // 
            this.L11.AutoSize = true;
            this.L11.Location = new System.Drawing.Point(334, 106);
            this.L11.Name = "L11";
            this.L11.Size = new System.Drawing.Size(29, 12);
            this.L11.TabIndex = 58;
            this.L11.Text = "指标";
            // 
            // G1
            // 
            this.G1.Location = new System.Drawing.Point(104, 106);
            this.G1.Name = "G1";
            this.G1.Size = new System.Drawing.Size(160, 37);
            this.G1.TabIndex = 57;
            this.G1.Text = "";
            // 
            // L1
            // 
            this.L1.AutoSize = true;
            this.L1.Location = new System.Drawing.Point(20, 106);
            this.L1.Name = "L1";
            this.L1.Size = new System.Drawing.Size(53, 12);
            this.L1.TabIndex = 56;
            this.L1.Text = "供应目标";
            // 
            // MTGID
            // 
            this.MTGID.Location = new System.Drawing.Point(334, 64);
            this.MTGID.Name = "MTGID";
            this.MTGID.ReadOnly = true;
            this.MTGID.Size = new System.Drawing.Size(100, 21);
            this.MTGID.TabIndex = 55;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(248, 67);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 12);
            this.label2.TabIndex = 54;
            this.label2.Text = "物料组编号";
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 67);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(65, 12);
            this.label1.TabIndex = 53;
            this.label1.Text = "物料组名称";
            // 
            // ModifyItemAttribute
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(547, 234);
            this.Controls.Add(this.ORGName);
            this.Controls.Add(this.ORGID);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.MTGName);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.Z1);
            this.Controls.Add(this.L11);
            this.Controls.Add(this.G1);
            this.Controls.Add(this.L1);
            this.Controls.Add(this.MTGID);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "ModifyItemAttribute";
            this.Text = "修改";
            this.Load += new System.EventHandler(this.ModifyItemAttribute_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox ORGName;
        private System.Windows.Forms.TextBox ORGID;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox MTGName;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.RichTextBox Z1;
        private System.Windows.Forms.Label L11;
        private System.Windows.Forms.RichTextBox G1;
        private System.Windows.Forms.Label L1;
        private System.Windows.Forms.TextBox MTGID;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
    }
}