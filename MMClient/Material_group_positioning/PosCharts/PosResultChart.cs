﻿using Lib.Bll.MT_GroupBll;
using Lib.Common.CommonUtils;
using Lib.Model.MT_GroupModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using WeifenLuo.WinFormsUI.Docking;

namespace MMClient.Material_group_positioning.PosCharts
{
    public partial class PosResultChart : DockContent
    {
        String mtId, purOrgName;
        N_RiskAssessmentBll n_RiskAssessmentBll = new N_RiskAssessmentBll();
        MtItemScoreInfoModle mtItemScoreInfoModle = new MtItemScoreInfoModle();
        public PosResultChart(String purOrgName, string purOrgId)
        {
            this.purOrgName = purOrgName;
            InitializeComponent();
            this.TB_purOrgName.Text = purOrgName;
            this.TB_purOrgId.Text = purOrgId;
           // double[] r = { 2, 8, 18 };
           // DrawCircl(r.Length,this.chart1,r);
            initalChart();

        }
        private void initalChart()
        {
            double x=0, y=0,total=0;
            String Level = "";
            string classResult;
            List<double> listY = new List<double>();
            List<double> listX = new List<double>();
            double r = 0;
            DataTable dt = null;
            try
            {
                 dt = n_RiskAssessmentBll.getMtGroupImportedData(this.TB_purOrgId.Text);
            }
            catch (Exception ex)
            {
                MessageUtil.ShowError("加载数据出错！");
                return;
            }

           
            if (dt.Rows.Count == 0) {
                MessageUtil.ShowTips("尚无物料组已评估，请先完成影响/供应风险评估！");
            }
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                listX.Clear();
                listY.Clear();
                if (!dt.Rows[i]["评分"].ToString().Equals(""))
                {
                    total = float.Parse(dt.Rows[i]["累计支出"].ToString().TrimEnd('%'));
                    x = float.Parse(dt.Rows[i]["支出金额"].ToString().TrimEnd('%'));
                    y = Math.Round(float.Parse(dt.Rows[i]["评分"].ToString()),2);
                    if (total <=80)
                    {
                        x = Math.Round(2 + 2 * (x) / 100,2);
                    }
                    else {
                        x = Math.Round(x * 2 / 100,2);
                    }

                    listY.Add(y);
                    listX.Add(x);
                    r = Math.Pow(listX[0], 2) + Math.Pow(listY[0], 2);
                }
                else {

                    MessageUtil.ShowError("数据不完整！");
                }

                if (r < 2)
                {
                    Level = "N";
                    classResult = "一般";

                }
                else if (r < 8)
                {

                    Level = "L";
                    classResult = "杠杆";

                }
                else if (r < 18)
                {

                    Level = "M";
                    classResult = "瓶颈";


                }
                else
                {
                    Level = "H";
                    classResult = "关键";

                }

                double cost = 0;
                if (total <= 80)
                {
                    cost = (x - 2) / 2 * 100;
                }
                else
                {
                    cost = x*50;

                }

                this.dgv_PositionInfo.Rows.Add(false, dt.Rows[i]["Material_ID"],dt.Rows[i]["MtGroupName"].ToString(),y, Math.Round(cost,2) + "%",Level, classResult);

            }
        }

        private void backBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void saveBtn_Click(object sender, EventArgs e)
        {
            //保存评估结果

            PosResultInfoModel posResultInfoModel = new PosResultInfoModel();
            int flag = 0,c=0;
            posResultInfoModel.PurOrgId = TB_purOrgId.Text;
            int n = dgv_PositionInfo.Rows.Count;
            for (int i = 0; i < n; i++) {
                posResultInfoModel.MtName = dgv_PositionInfo.Rows[i].Cells["物料"].Value.ToString();
                posResultInfoModel.InfluAndRiskScore = float.Parse(dgv_PositionInfo.Rows[i].Cells["影响风险评分"].Value.ToString());
                posResultInfoModel.Cost =dgv_PositionInfo.Rows[i].Cells["支出金额"].Value.ToString();
                posResultInfoModel.PosResult = dgv_PositionInfo.Rows[i].Cells["优先级"].Value.ToString();
                posResultInfoModel.ClassifyType = dgv_PositionInfo.Rows[i].Cells["classifyType"].Value.ToString();
                posResultInfoModel.MtId=dgv_PositionInfo.Rows[i].Cells["Material_ID"].Value.ToString();
                try
                {
                    flag = n_RiskAssessmentBll.insertLastPosResultInfo(posResultInfoModel);
                }
                catch (Exception ex)
                {

                    MessageUtil.ShowError("保存失败！"+ex.Message);
                }
                

                if (flag > 0) {
                    c++;
                }
            }
            if (c == n) { MessageUtil.ShowTips("保存成功！"); } else { MessageUtil.ShowError("保存失败，请重试！"); }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string purOrg = TB_purOrgId.Text;
            string mtName = this.mtName.Text;
           dgv_PositionInfo.DataSource = null;
            dgv_PositionInfo.AutoGenerateColumns = false;
            dgv_PositionInfo.DataSource = n_RiskAssessmentBll.SearchMtGroupByName(mtName, purOrg);
        }

        /// <summary>
        /// 添加序号
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridView1_RowStateChanged(object sender, DataGridViewRowStateChangedEventArgs e)
        {
            //显示在HeaderCell上
            for (int i = 0; i < this.dgv_PositionInfo.Rows.Count; i++)
            {
                DataGridViewRow r = this.dgv_PositionInfo.Rows[i];
                r.HeaderCell.Value = string.Format("{0}", i + 1);
            }
            this.dgv_PositionInfo.Refresh();
        }

    



        /// <summary>
        /// 绘制参考线
        /// </summary>
        /// <param name="n"></param>
        /// <param name="chart"></param>
        /// <param name="r"></param>
        private void DrawCircl(int n, Chart chart, double[] r)
        {
            List<Double> y1 = new List<Double>();
            List<Double> x1 = new List<Double>();

            for (int m = 0; m < n; m++)
            {
                for (double i = 0.00; i <= r[m]; i = i + 0.01)
                {
                    for (double j = 0.00; j <= r[m]; j = j + 0.01)
                    {
                        i = Math.Round(i, 2);
                        j = Math.Round(j, 2);
                        if (r[m] - (i * i + j * j) <= 10e-5)
                        {
                            x1.Add(i);
                            y1.Add(j);
                            break;

                        }


                    }


                }
                this.chart1.Series[m].Points.DataBindXY(x1, y1);
                x1.Clear();
                y1.Clear();


            }


        }


    }
  




}
