﻿using Lib.Bll.MT_GroupBll;
using Lib.Common.CommonUtils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MMClient.Material_group_positioning.PosCharts
{
    public partial class PosCost : Form
    {
        N_RiskAssessmentBll n_RiskAssessmentBll = new N_RiskAssessmentBll();
        String PurOrgName = "";
        public PosCost(String PurOrgName)
        {
            InitializeComponent();
            this.PurOrgName = PurOrgName;
        }

        //填充采购支出金额图
        public void fillPurCostChart(string PurName)
        {

            DataTable dt = n_RiskAssessmentBll.getPurCostAndItem(PurName);
            if (dt.Rows.Count == 0) {
                MessageUtil.ShowWarning("还未导入数据");
                return;

            }
            List<double> listX = new List<double>();
            List<double> listY = new List<double> ();
            listX.Add(0);
            listY.Add(0);
            double x = 0;
            double y = 0;
            double n = dt.Rows.Count;
            for (int i = 0; i < dt.Rows.Count; i++) {
                y= float.Parse( dt.Rows[i]["cost"].ToString().TrimEnd('%'));
                x = (i + 1)/n*100;
                listX.Add(x);
                listY.Add(y);
            }
            this.chart1.Series[0].ToolTip = "采购累计支出金额：#VALY%\n品项占比：#VALX%";
            this.chart1.Series[0].Points.DataBindXY(listX, listY);

        }
        //填充定位图
        public void fillPosChart()
        {



        }

        private void PosCost_Load(object sender, EventArgs e)
        {
            fillPurCostChart(PurOrgName);
        }
    }
}
