﻿using Lib.Bll.Material_group_positioning;
using Lib.Bll.MT_GroupBll;
using Lib.Common.CommonUtils;
using Lib.Model.MT_GroupModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using WeifenLuo.WinFormsUI.Docking;

namespace MMClient.Material_group_positioning
{
    public partial class N_MT_GroupPosition : DockContent
    {
        Boolean complete = false;
        //物料ID
        string id = "";
        //物料名称
        string name = "";
        //采购组织名称
        string purOrgName = "";
        //采购组织编号
        string purOrgId = "";
        //定位分数
        double resultPosScore = 0;
        //定位等级
        String influenceScore = "";
        String riskScore = "";
        String resultLevel = "";
        string type = "";
        N_RiskAssessmentBll n_RiskAssessmentBll = new N_RiskAssessmentBll();
        MtItemScoreInfoModle mtItemScoreInfoModle = new MtItemScoreInfoModle();
        public N_MT_GroupPosition(String id, String name, String purOrgId, string purOrgName, string type)
        {
            InitializeComponent();
            this.id = id;
            this.name = name;
            this.purOrgName = purOrgName;
            this.purOrgId = purOrgId;
            this.type = type;
        }


        /// <summary>
        /// 初始化数据
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void N_MT_GroupPosition_Load(object sender, EventArgs e)
        {

            dgv_itemInfo.DataSource = null;
            dgv_itemInfo.AutoGenerateColumns = false;
            dgv_itemInfo.DataSource = n_RiskAssessmentBll.getMtItemInfo(id, this.purOrgId, this.type);
            this.mtName.Text = name;
            this.mtID.Text = id;
            this.purchOrgName.Text = purOrgName;
            this.LB_purOrgId.Text = purOrgId;
            getTotalScoreInfo();//获取评分信息
            this.dgv_itemInfo.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellValueChanged);
        }

        /// <summary>
        /// 添加序号
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridView1_RowStateChanged(object sender, DataGridViewRowStateChangedEventArgs e)
        {
            //显示在HeaderCell上
            for (int i = 0; i < this.dgv_itemInfo.Rows.Count; i++)
            {
                DataGridViewRow r = this.dgv_itemInfo.Rows[i];
                r.HeaderCell.Value = string.Format("{0}", i + 1);
            }
            // this.dgv_itemInfo.Refresh();
        }

        /// <summary>
        /// 获取评分信息
        /// </summary>
        public void getTotalScoreInfo()
        {

            //查询评估结果
            try
            {

                DataTable dt = n_RiskAssessmentBll.getMtGroupResultData(id, this.purOrgId);
                if (dt.Rows.Count == 0)
                {
                    MessageUtil.ShowWarning("请先评估！");
                    return;

                }
                //平均值开平方
                //todo:有问题
                resultPosScore = Math.Sqrt((Math.Pow(float.Parse(dt.Rows[0]["AimsScore"].ToString().Trim()), 2) + Math.Pow(float.Parse(dt.Rows[0]["RisKScore"].ToString().Trim()), 2) + Math.Pow(float.Parse(dt.Rows[0]["chanceScore"].ToString().Trim()), 2)) / 3);
                if (resultPosScore < 1)
                {
                    resultLevel = "N";
                }
                else if (resultPosScore < 2)
                {
                    resultLevel = "L";
                }
                else if (resultPosScore < 3)
                {
                    resultLevel = "M";
                }
                else
                {
                    resultLevel = "H";
                }

                resultPosScore = Math.Round(resultPosScore, 2);
                //获取物料特性
                getMaterialqualityClass(this.mtID.Text);



                this.tb_InfluScore.Text = dt.Rows[0]["AimsScore"].ToString();
                this.TBinfluScore.Text = dt.Rows[0]["aimLevel"].ToString();
                this.TB_RiskScore.Text = dt.Rows[0]["RisKScore"].ToString();
                this.TB_RiskLevel.Text = dt.Rows[0]["RiskLevel"].ToString();
                this.TB_ChanceScore.Text = dt.Rows[0]["chanceScore"].ToString();
                this.TB_chanceLevel.Text = dt.Rows[0]["chanceLeve"].ToString();
                
                this.TB_TotalScore.Text = resultPosScore.ToString();
                this.TB_TotalLevel.Text = resultLevel.ToString();

            }
            catch (Exception)
            {

                MessageUtil.ShowError("数据异常");
            }

        }

        private void getMaterialqualityClass(string mtId)
        {
            string str = n_RiskAssessmentBll.getQualityClass(mtId);
            if (!str.Equals("")) {
                this.LB_qualityClass.Text = str;
                LB_qualityClass.Visible = true;
                LB_qualityClass.Visible = true;
            }
        }




        /// <summary>
        /// 打分界面
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int CIndex = e.ColumnIndex;
            if (dgv_itemInfo.Columns[e.ColumnIndex].Name.Equals("评估"))
            {
                string aim = dgv_itemInfo.CurrentRow.Cells["供应目标"].Value.ToString();
                string gloab = dgv_itemInfo.CurrentRow.Cells["指标"].Value.ToString();
                mtItemScoreInfoModle.Aim = dgv_itemInfo.CurrentRow.Cells["供应目标"].Value.ToString();
                mtItemScoreInfoModle.Global = dgv_itemInfo.CurrentRow.Cells["指标"].Value.ToString();
                mtItemScoreInfoModle.InfluenLevel = dgv_itemInfo.CurrentRow.Cells["影响力PIP等级"].Value.ToString();
                mtItemScoreInfoModle.InfluenScore = dgv_itemInfo.CurrentRow.Cells["影响力得分"].Value.ToString().Trim();
                mtItemScoreInfoModle.RiskLevel = dgv_itemInfo.CurrentRow.Cells["供应风险等级"].Value.ToString();
                mtItemScoreInfoModle.RiskScore = dgv_itemInfo.CurrentRow.Cells["供应风险得分"].Value.ToString().Trim();
                mtItemScoreInfoModle.RiskText = dgv_itemInfo.CurrentRow.Cells["风险"].Value.ToString().Trim();
                mtItemScoreInfoModle.ChanceText = dgv_itemInfo.CurrentRow.Cells["chance"].Value.ToString().Trim();
                mtItemScoreInfoModle.ChanceLevel = dgv_itemInfo.CurrentRow.Cells["chanceLevel"].Value.ToString().Trim();
                mtItemScoreInfoModle.ChanceScore = dgv_itemInfo.CurrentRow.Cells["chanceScore"].Value.ToString().Trim();
                mtItemScoreInfoModle.Id = id;
                mtItemScoreInfoModle.PurOrgId = this.purOrgId;
                mtItemScoreInfoModle.PurOrgName = this.purOrgName;
                mtItemScoreInfoModle.AimId = dgv_itemInfo.CurrentRow.Cells["goalId"].Value.ToString().Trim();
                mtItemScoreInfoModle.MtName = this.name;
                mtItemScoreInfoModle.Category1 = "非生产性物料";
                mtItemScoreInfoModle.AutoId1 = dgv_itemInfo.CurrentRow.Cells["GoalId"].Value.ToString().Trim();

                if (mtItemScoreInfoModle.InfluenScore.Equals("待评估") || mtItemScoreInfoModle.InfluenScore.Equals("") || mtItemScoreInfoModle.RiskScore.Equals("") || mtItemScoreInfoModle.RiskScore.Equals("待评估"))
                {
                    MessageUtil.ShowError("请输入分数信息");
                    return;
                }
                if (mtItemScoreInfoModle.RiskText.Equals(""))
                {
                    MessageUtil.ShowError("请输入风险！");
                    return;
                }

                //插入分数信息
                Boolean flag = n_RiskAssessmentBll.updateSingleMtSubScoreInfo(mtItemScoreInfoModle);
                if (flag)
                {
                    MessageUtil.ShowTips("评估完成！");
                    dgv_itemInfo.DataSource = n_RiskAssessmentBll.getMtItemInfo(id, this.purOrgId, this.type);
                    getTotalScoreInfo();
                }
                else
                {
                    MessageUtil.ShowError("评估失败");
                }


            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridView1_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            string level = "";
            int Cindex = e.ColumnIndex;
            string scoreStr = "";
            string levelStr = "";

            if (Cindex <= 0) { return; }


            if (dgv_itemInfo.Columns[Cindex].Name.Equals("影响力得分"))
            {
                scoreStr = "影响力得分";
                levelStr = "影响力PIP等级";


            }
            else if (dgv_itemInfo.Columns[Cindex].Name.Equals("供应风险得分"))
            {
                scoreStr = "供应风险得分";
                levelStr = "供应风险等级";
            }
            else if (dgv_itemInfo.Columns[Cindex].Name.Equals("chanceScore"))
            {

                scoreStr = "chanceScore";
                levelStr = "chanceLevel";
            }
            else {
                return;
            }



            level = "";

            if (dgv_itemInfo.CurrentRow.Cells[scoreStr].Value.Equals("待评估") || dgv_itemInfo.CurrentRow.Cells[scoreStr].Value.Equals(""))
            {
                return;
            }

            try
            {
                float score = float.Parse(dgv_itemInfo.CurrentRow.Cells[scoreStr].Value.ToString());
                if (score > 4 || score < 0)
                {
                    MessageUtil.ShowError("打分范围为：0-4");
                    dgv_itemInfo.CurrentRow.Cells[scoreStr].Value = "";
                    return;

                }
                if (score >= 0 && score < 1)
                {
                    level = "N";
                }
                else if (score < 2)
                {
                    level = "L";
                }
                else if (score < 3)
                {
                    level = "M";
                }
                else
                {
                    level = "H";
                }
                dgv_itemInfo.CurrentRow.Cells[levelStr].Value = level;

            }
            catch (Exception)
            {
                MessageUtil.ShowError("打分只能为数字！");
            }


        }





        private void backBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        //将评估结果写入数据库

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {



                int flag = n_RiskAssessmentBll.insertIntoPosResInfo(float.Parse(this.TB_TotalScore.Text), TB_TotalLevel.Text.ToString(), id, purOrgId, TB_RiskScore.Text.Trim(), this.tb_InfluScore.Text, mtName.Text, float.Parse(TB_ChanceScore.Text));
                string info = "保存失败！";
                if (flag > 0)
                {
                    info = "保存成功！";
                }

                MessageUtil.ShowTips(info);
            }
            catch (Exception ex)
            {
                MessageUtil.ShowError("保存失败！" + ex.Message);
            }




        }

        private void mutEvlBtn_Click(object sender, EventArgs e)
        {
            int n = 0;
            int count = this.dgv_itemInfo.Rows.Count;
            try
            {
                for (int i = 0; i < count; i++)
                {

                    string aim = dgv_itemInfo.Rows[i].Cells["供应目标"].Value.ToString();
                    string gloab = dgv_itemInfo.Rows[i].Cells["指标"].Value.ToString();
                    mtItemScoreInfoModle.Aim = dgv_itemInfo.CurrentRow.Cells["供应目标"].Value.ToString();
                    mtItemScoreInfoModle.Global = dgv_itemInfo.CurrentRow.Cells["指标"].Value.ToString();
                    mtItemScoreInfoModle.InfluenLevel = dgv_itemInfo.CurrentRow.Cells["影响力PIP等级"].Value.ToString();
                    mtItemScoreInfoModle.InfluenScore = dgv_itemInfo.CurrentRow.Cells["影响力得分"].Value.ToString().Trim();
                    mtItemScoreInfoModle.RiskLevel = dgv_itemInfo.CurrentRow.Cells["供应风险等级"].Value.ToString();
                    mtItemScoreInfoModle.RiskScore = dgv_itemInfo.CurrentRow.Cells["供应风险得分"].Value.ToString().Trim();
                    mtItemScoreInfoModle.RiskText = dgv_itemInfo.CurrentRow.Cells["风险"].Value.ToString().Trim();
                    mtItemScoreInfoModle.ChanceText = dgv_itemInfo.CurrentRow.Cells["chance"].Value.ToString().Trim();
                    mtItemScoreInfoModle.ChanceLevel = dgv_itemInfo.CurrentRow.Cells["chanceLevel"].Value.ToString().Trim();
                    mtItemScoreInfoModle.ChanceScore = dgv_itemInfo.CurrentRow.Cells["chanceScore"].Value.ToString().Trim();
                    mtItemScoreInfoModle.Id = id;
                    mtItemScoreInfoModle.PurOrgId = this.purOrgId;
                    mtItemScoreInfoModle.PurOrgName = this.purOrgName;
                    mtItemScoreInfoModle.AimId = dgv_itemInfo.CurrentRow.Cells["goalId"].Value.ToString().Trim();
                    mtItemScoreInfoModle.MtName = this.name;
                    mtItemScoreInfoModle.Category1 = "非生产性物料";
                    mtItemScoreInfoModle.AutoId1 = dgv_itemInfo.CurrentRow.Cells["GoalId"].Value.ToString().Trim();

                    if (mtItemScoreInfoModle.RiskText.Equals("") || mtItemScoreInfoModle.InfluenScore.Equals("待评估") || mtItemScoreInfoModle.InfluenScore.Equals("") || mtItemScoreInfoModle.RiskScore.Equals("") || mtItemScoreInfoModle.RiskScore.Equals("待评估"))
                    {
                        MessageUtil.ShowError("还未评估完成");
                        return;
                    }

                    //插入分数信息
                    Boolean flag = n_RiskAssessmentBll.updateSingleMtSubScoreInfo(mtItemScoreInfoModle);
                    if (flag)
                    {

                        n++;

                    }
                    else
                    {
                        MessageUtil.ShowError("评估失败，请重试");
                        return;
                    }


                }
                if (n == count)
                {
                    MessageUtil.ShowTips("评估成功！");

                }
                dgv_itemInfo.DataSource = n_RiskAssessmentBll.getMtItemInfo(id, this.purOrgId, this.type);
                getTotalScoreInfo();
            }
            catch (Exception ex)
            {

                MessageUtil.ShowError("批量评估失败！" + ex.Message);
            }
        }

        /// <summary>
        /// 翻页事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pageNext1_OnPageChanged(object sender, EventArgs e)
        {
            loadData();
        }
        /// <summary>
        /// 加载数据
        /// </summary>
        private void loadData()
        {

        }
        /// <summary>
        /// 加载分页数据
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pageNext1_Load(object sender, EventArgs e)
        {

            pageNext1.DrawControl(n_RiskAssessmentBll.getIndicateCount(mtID.Text.ToString()));
            pageNext1.OnPageChanged += pageNext1_OnPageChanged;
            //加载数据
            loadData();
        }

    }
}
