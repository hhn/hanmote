﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;

using Lib.Bll.Material_group_positioning.General;
using Lib.Common.CommonUtils;
using MMClient.Material_group_positioning;
using Lib.SqlServerDAL;

namespace MMClient.CertificationManagement.Material_group_positioning
{
    public partial class AddMTGgoals : DockContent
    {
        public AddMTGgoals()
        {
            InitializeComponent();
        }

        private void AddMTGgoals_Load(object sender, EventArgs e)//加载数据
        {
            GeneralBLL gn = new GeneralBLL();
           /* DataTable dt = gn.loaddata(SingleUserInstance.getCurrentUserInstance().User_ID);
            dataGridView1.DataSource = dt;*/
            List<String> orgID = gn.getALLorg();//获取采购组织ID下拉框数据源
            comboBox1.DataSource = orgID;
            
        }
        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)//使采购组织名称与采购组织ID联动
        {
            List<String> MtGroup_ID = new List<string>();

            //String sql = " select Buyer_Org_Name  from  Buyer_Org ";
            /* String sql = "SELECT distinct id FROM [MTGAttributeDefinition] WHERE Category='生产性物料' and  PurOrgID='" + comboBox1.Text + "'";*/
            String sql = @"SELECT distinct p.DateTime AS '创建时间',p.MtGroup_ID as '物料组编号' ,p.MtGroup_Name as '物料组名称' ,case    
                                when p.Status='1' then '已创建'
                                when p.Status='0' then '未创建'
                                end as '状态'
                                FROM 
                                [Porg_MtGroup_Relationship] as p , 
                                Material_Group as m  
                                WHERE p.Porg_ID='" + comboBox1.Text + "' AND p.MtGroup_Name=m.Description and m.Category='生产性物料' ;";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            /*if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    MtGroup_ID.Add(dt.Rows[i][0].ToString());
                }
            }*/
           dataGridView1.DataSource = dt;

            DataGridViewButtonColumn button1 = new DataGridViewButtonColumn();
            button1.Name = "button1";
            button1.Text = "处理";//加上这两个就能显示
            button1.UseColumnTextForButtonValue = true;//
            button1.Width = 50;
            button1.HeaderText = "处理";
            //this.dataGridView1.Columns.AddRange(button1);
            if (dataGridView1.Columns.Count>=5)
            {
                dataGridView1.Columns.Remove("button1");
            }
            dataGridView1.Columns.Add(button1);
            this.dataGridView1.Columns["物料组名称"].FillWeight = 100;//设置某一列的宽度，数值为比例值


            //String sql1 = "SELECT distinct PurOrgName FROM [MTGAttributeDefinition] WHERE Category='生产性物料' and   PurOrgID='" + comboBox1.Text + "'";
            String sql1 = "select Buyer_Org_Name  from  Buyer_Org where Buyer_Org='"+ comboBox1.Text + "';";
            DataTable dt1 = DBHelper.ExecuteQueryDT(sql1);
            if (dt1.Rows.Count != 0)
            {
                textBox1.Text = dt1.Rows[0][0].ToString();
            }
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)//使物料组名称与物料组ID关联
        {
            
            String sql1 = "SELECT distinct MTGName FROM [MTGAttributeDefinition] WHERE Category='生产性物料' and id='" + comboBox2.Text + "'";
            DataTable dt1 = DBHelper.ExecuteQueryDT(sql1);
            if (dt1.Rows.Count != 0)
            {
                textBox2.Text = dt1.Rows[0][0].ToString();
            }
        }
        private void button1_Click(object sender, EventArgs e)//搜索
        {
            string ORGID = comboBox1.Text;
            string ORGName = textBox1.Text;
            string MTGID = textBox4.Text;
            string MTGName = textBox2.Text;
            string status = comboBox3.Text;
            string status1 ;
            /* GeneralBLL gn = new GeneralBLL();
             DataTable dt = gn.searchdata(SingleUserInstance.getCurrentUserInstance().User_ID, ORGID, ORGName, MTGID, MTGName);*/

            String sql = @"SELECT distinct p.DateTime AS '创建时间',p.MtGroup_ID as '物料组编号' ,p.MtGroup_Name as '物料组名称' ,
                                case    
                                when p.Status='1' then '已创建'
                                when p.Status='0' then '未创建'
                                end as '状态'
                                FROM 
                                [Porg_MtGroup_Relationship] as p , 
                                Material_Group as m  
                                WHERE p.Porg_ID ='" + comboBox1.Text + "' AND p.MtGroup_Name=m.Description and m.Category='生产性物料' ";
            if (ORGID != null && ORGID != "")
                sql += " and p.Porg_ID= '" + ORGID + "'";
            if (ORGName != null && ORGName != "")
                sql += " and p.Porg_Name like '%" + ORGName + "%'";
            if (MTGID != null && MTGID != "")
                sql += " and p.MtGroup_ID = '" + MTGID + "'";
            if (MTGName != null && MTGName != "")
                sql += " and p.MtGroup_Name like '%" + MTGName + "%'";
            if (status != null && status != "")
            {
                if (status.Equals("已创建"))
                    status1 ="1";
                else
                    status1 = "0";
                sql += " and p.Status = '" + status1 + "'";
            }
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            
            dataGridView1.DataSource = dt;


            DataGridViewButtonColumn button1 = new DataGridViewButtonColumn();
            button1.Name = "button1";
            button1.Text = "处理";//加上这两个就能显示
            button1.UseColumnTextForButtonValue = true;//
            button1.Width = 50;
            button1.HeaderText = "处理";
            if (dataGridView1.Columns.Count >= 5)
            {
                dataGridView1.Columns.Remove("button1");
            }
            dataGridView1.Columns.Add(button1);
            //this.dataGridView1.Columns.AddRange(button1);
            this.dataGridView1.Columns["物料组名称"].FillWeight = 100;

        }

        private void ThylxBtn_CellContentClick(object sender, DataGridViewCellEventArgs e)//三个操作之一对应的事件跳转代码
        {

            if (e.RowIndex >= 0)
            {


                if (dataGridView1.Columns[e.ColumnIndex].Name == "button1")//单击处理对应事件
                {
                    Handel a = new Handel(comboBox1.Text, textBox1.Text, this.dataGridView1.CurrentRow.Cells["物料组编号"].Value.ToString(), this.dataGridView1.CurrentRow.Cells["物料组名称"].Value.ToString());
                    a.Show();
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)//刷新
        {
            GeneralBLL gn = new GeneralBLL();
            DataTable dt = gn.loaddata(SingleUserInstance.getCurrentUserInstance().User_ID);
            dataGridView1.DataSource = dt;

            List<String> orgID = gn.getALLorgID();//获取采购组织ID下拉框数据源
            comboBox1.DataSource = orgID;
        }

        private void button3_Click(object sender, EventArgs e)//添加
        {
            AddNewAttribute a = new AddNewAttribute();
            a.Show();
        }

        private void button4_Click(object sender, EventArgs e)//修改
        {
            if (dataGridView1.CurrentRow.Cells.Count < 6)
            {
                string a = comboBox1.Text;
                string b = textBox1.Text;
                string c = comboBox2.Text;
                string d = textBox2.Text;
                string h = dataGridView1.CurrentRow.Cells["供应目标"].Value.ToString();
                string f = dataGridView1.CurrentRow.Cells["指标"].Value.ToString();
               // modifyAtribute B = new modifyAtribute(a, b, c, d, h, f);
               // B.Show();
            }
            else
            {
                string a = dataGridView1.CurrentRow.Cells["采购组织编号"].Value.ToString();
                string b = dataGridView1.CurrentRow.Cells["采购组织名称"].Value.ToString();
                string c = dataGridView1.CurrentRow.Cells["物料组编号"].Value.ToString();
                string d = dataGridView1.CurrentRow.Cells["物料组名称"].Value.ToString();
                string h = dataGridView1.CurrentRow.Cells["供应目标"].Value.ToString();
                string f = dataGridView1.CurrentRow.Cells["指标"].Value.ToString();
                //modifyAtribute B = new modifyAtribute(a, b, c, d, h, f);
               // B.Show();
            }
        }

        private void button5_Click(object sender, EventArgs e)//删除
        {
            string c = dataGridView1.CurrentRow.Cells["供应目标"].Value.ToString();
            string d = dataGridView1.CurrentRow.Cells["指标"].Value.ToString();
            GeneralBLL gn = new GeneralBLL();
            //gn.deletedata(c,d);//删除数据
            MessageUtil.ShowWarning("删除成功！");

         
            DataTable dt = gn.loaddata(SingleUserInstance.getCurrentUserInstance().User_ID);
            dataGridView1.DataSource = dt;

        }
    }
}
