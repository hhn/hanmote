﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;

using Lib.Bll.Material_group_positioning.General;
using Lib.Common.CommonUtils;
using Lib.SqlServerDAL;
using System.Text.RegularExpressions;

namespace MMClient.Material_group_positioning
{
    public partial class AddNewAttribute : Form
    {
        public AddNewAttribute()
        {
            InitializeComponent();
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void AddNewAttribute_Load(object sender, EventArgs e)
        {
            GeneralBLL gn = new GeneralBLL();
           /* List<String> mtID = gn.getALLMtGroup();
            code_MtGroup.DataSource = mtID;*/

            List<String> orgName = gn.getALLorg();//获取采购组织下拉框数据源
            comboBox1.DataSource = orgName;

            List<String> model = gn.getModel();//获取属性模板绑定源
            comboBox21.DataSource = model;

            // code_MtGroup.Text = code_MtGroup1;   //设置当前选中
        }
        private void comboBox21_SelectedIndexChanged(object sender, EventArgs e)//获取属性模板
        {
            GeneralBLL gn = new GeneralBLL();
           /* List<String> model = gn.getModel();//获取属性模板绑定源
            comboBox21.DataSource = model;*/
            String MtGroup_ID ="";
            String M= "";
            //String sql = " select Buyer_Org_Name  from  Buyer_Org ";
            String sql = "SELECT AimModel FROM [MTGroupAttributeModel] WHERE Type='" + comboBox21.Text + "'";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            if (dt.Rows.Count != 0)
            {
                MtGroup_ID = dt.Rows[0][0].ToString();
            }


            string[] sArray = Regex.Split(MtGroup_ID, "&", RegexOptions.IgnoreCase);
            
                GO1.Text = sArray[0];
            GO2.Text = sArray[1];
            GO3.Text = sArray[2];
            GO4.Text = sArray[3];


            String sql1 = "SELECT GoalModel FROM [MTGroupAttributeModel] WHERE Type='" + comboBox21.Text + "'";
            DataTable dt1 = DBHelper.ExecuteQueryDT(sql1);
            if (dt1.Rows.Count != 0)
            {
                M = dt1.Rows[0][0].ToString();
            }
            string[] sArray1 = Regex.Split(M, "&", RegexOptions.IgnoreCase);
            ZH1.Text = sArray1[0];
            ZH2.Text = sArray1[1];
            ZH3.Text = sArray1[2];
            ZH4.Text = sArray1[3];

        }
        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)//使采购组织与物料组联动
        {
            List<String> MtGroup_ID = new List<string>();

            //String sql = " select Buyer_Org_Name  from  Buyer_Org ";
            //String sql = "SELECT MtGroup_Name FROM [Porg_MtGroup_Relationship] WHERE Porg_Name='" + comboBox1.Text + "'";
            String sql = @"SELECT distinct p.MtGroup_Name 
                                FROM 
                                [Porg_MtGroup_Relationship] as p , 
                                Material_Group as m  
                                WHERE p.Porg_Name='" + comboBox1.Text + "' AND p.MtGroup_Name=m.Description and m.Category='生产性物料' ;";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    MtGroup_ID.Add(dt.Rows[i][0].ToString());
                }
            }
            code_MtGroup.DataSource = MtGroup_ID;

            String sql1 = "SELECT distinct Porg_ID FROM [Porg_MtGroup_Relationship] WHERE Porg_Name='" + comboBox1.Text + "'";
            DataTable dt1 = DBHelper.ExecuteQueryDT(sql1);
            if (dt1.Rows.Count != 0)
            {
                textBox1.Text = dt1.Rows[0][0].ToString();
            }
        }
        private void code_MtGroup_SelectedIndexChanged(object sender, EventArgs e)//使编号与名称联动
        {
            String sql = "SELECT * FROM [Material_Group] WHERE Description='" + code_MtGroup.Text + "'";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            if (dt.Rows.Count != 0)
            {
                name_MtGroup.Text = dt.Rows[0][1].ToString();
            }
        }
    
        private void button1_Click(object sender, EventArgs e)//保存
        {
            GeneralBLL gn = new GeneralBLL();
            /*gn.SaveGoal(textBox1.Text,comboBox1.Text, name_MtGroup.Text, code_MtGroup.Text, G1.Text,Z1.Text);
            if(G2.Text!=null&& G2.Text !="")
                gn.SaveGoal(textBox1.Text, comboBox1.Text, name_MtGroup.Text, code_MtGroup.Text, G2.Text, Z2.Text);
            if (G3.Text != null && G3.Text != "")
                gn.SaveGoal(textBox1.Text, comboBox1.Text, name_MtGroup.Text, code_MtGroup.Text, G3.Text, Z3.Text);
            if (G4.Text != null && G4.Text != "")
                gn.SaveGoal(textBox1.Text, comboBox1.Text, name_MtGroup.Text, code_MtGroup.Text, G4.Text, Z4.Text);*/
            gn.SaveGoal(textBox1.Text, comboBox1.Text, name_MtGroup.Text, code_MtGroup.Text, GO1.Text, ZH1.Text);
            if (GO2.Text != null && GO2.Text != "")
                gn.SaveGoal(textBox1.Text, comboBox1.Text, name_MtGroup.Text, code_MtGroup.Text, GO2.Text, ZH2.Text);
            if (GO3.Text != null && GO3.Text != "")
                gn.SaveGoal(textBox1.Text, comboBox1.Text, name_MtGroup.Text, code_MtGroup.Text, GO3.Text, ZH3.Text);
            if (GO4.Text != null && GO4.Text != "")
                gn.SaveGoal(textBox1.Text, comboBox1.Text, name_MtGroup.Text, code_MtGroup.Text, GO4.Text, ZH4.Text);
            MessageUtil.ShowWarning("保存成功！");
            this.Close();

        }

        private void B1_Click(object sender, EventArgs e)
        {
            L2.Visible = true;
            L12.Visible = true;
            G2.Visible = true;
            Z2.Visible = true;
            B2.Visible = true;
        }

        private void B2_Click(object sender, EventArgs e)
        {
            L3.Visible = true;
            L13.Visible = true;
            G3.Visible = true;
            Z3.Visible = true;
            B3.Visible = true;
        }

        private void B3_Click(object sender, EventArgs e)
        {
            L4.Visible = true;
            L14.Visible = true;
            G4.Visible = true;
            Z4.Visible = true;
            B4.Visible = true;
        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)//保存为模板
        {
            GeneralBLL gn = new GeneralBLL();
            string aim = "";
            string goal = "";
            aim = GO1.Text + "&" + GO2.Text + "&" + GO3.Text + "&" + GO4.Text;
            goal = ZH1.Text + "&" + ZH2.Text + "&" + ZH3.Text + "&" + ZH4.Text;
            //Popup a = new Popup(aim,goal);

            gn.SaveGoal(textBox1.Text, comboBox1.Text, name_MtGroup.Text, code_MtGroup.Text, GO1.Text, ZH1.Text);
            if (GO2.Text != null && GO2.Text != "")
                gn.SaveGoal(textBox1.Text, comboBox1.Text, name_MtGroup.Text, code_MtGroup.Text, GO2.Text, ZH2.Text);
            if (GO3.Text != null && GO3.Text != "")
                gn.SaveGoal(textBox1.Text, comboBox1.Text, name_MtGroup.Text, code_MtGroup.Text, GO3.Text, ZH3.Text);
            if (GO4.Text != null && GO4.Text != "")
                gn.SaveGoal(textBox1.Text, comboBox1.Text, name_MtGroup.Text, code_MtGroup.Text, GO4.Text, ZH4.Text);
            MessageUtil.ShowWarning("保存成功！");
           // a.Show();
            this.Close();
        }
    }
}
