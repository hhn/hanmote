﻿using Lib.Bll.SystemConfig.UserManage;
using Lib.Common.CommonUtils;
using Lib.SqlServerDAL;
using MMClient.Material_group_positioning.PosCharts;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Lib.Bll.MT_GroupBll;
using WeifenLuo.WinFormsUI.Docking;

namespace MMClient.Material_group_positioning
{
    public partial class importDataBtn : DockContent
    {
        private SystemUserOrganicRelationshipBLL systemUserManagerBLL = new SystemUserOrganicRelationshipBLL();
        N_RiskAssessmentBll n_RiskAssessmentBll = new N_RiskAssessmentBll();
        private string OledbConnString = "Provider = Microsoft.Jet.OLEDB.4.0 ; Data Source = {0};Extended Properties='Excel 8.0;HDR=Yes;IMEX=1;'";
        public importDataBtn()
        {
            InitializeComponent();
            
        }

        private void loadData()
        {
            ExcelFileTable.DataSource = fillDataTable();
            fillPurCostChart(this.PurGroupName.Text);
        }

        private void DataImportBtn_Click(object sender, EventArgs e)
        {

            DataTable table = fillDataTable();
            if (table.Rows.Count <= 0)
            {

                MessageBoxButtons messButton = MessageBoxButtons.OKCancel;
                DialogResult dr = MessageBox.Show("未检测到数据，请先导入文件", "警  告", messButton);
                if (dr == DialogResult.OK)
                {
                    DialogResult dr1 = this.openFileDialog1.ShowDialog();
                    if (DialogResult.OK == dr1) //判断是否选择文件  
                    {
                        string path = this.openFileDialog1.FileName.Trim();
                        if (string.IsNullOrEmpty(path))
                        {
                            MessageBox.Show("请选择要导入的EXCEL文件。", "信息");
                            return;
                        }
                        if (!File.Exists(path))  //判断文件是否存在  
                        {
                            MessageBox.Show("信息", "找不到对应的Excel文件，请重新选择。");
                            this.DataImportBtn.Focus();
                            return;
                        }
                        DataTable excelTbl = this.GetExcelTable(path);  //调用函数获取Excel中的信息  
                        if (excelTbl == null)
                        {
                            return;
                        }
                        ///遍历DataTable将数据列写入
                        for (int i = 0; i < excelTbl.Rows.Count; i++)
                        {
                            for (int j = 0; j < excelTbl.Columns.Count;)
                            {
                                string MtGroupName = excelTbl.Rows[i][j].ToString().Trim();
                                j++;
                                string MtGroupCode = excelTbl.Rows[i][j].ToString().Trim();
                                j++;
                                string rank = "H";
                                string SpRiskRank = rank.Equals("") ? "未评估" : rank;//根据采购组织物料组编码得到评估等级
                                j++;
                                string PurMoneyCount = excelTbl.Rows[i][j].ToString().Trim();
                                j++;
                                string Proportion = excelTbl.Rows[i][j].ToString().Trim();
                                j++;
                                string AddProportion = excelTbl.Rows[i][j].ToString().Trim();
                                j++;
                                string ExpenditureClass = excelTbl.Rows[i][j].ToString().Trim();
                                j++;
                                string MtPosition = excelTbl.Rows[i][j].ToString().Trim();
                                j++;
                                string PurGroupName = this.PurGroupName.Text.ToString().Trim();
                                j++;
                                string PurGroupCode = this.PurGroupCode.Text.ToString().Trim();
                                j++;
                                if (String.IsNullOrEmpty(MtGroupName)
                                    || String.IsNullOrEmpty(MtGroupCode)
                                    || String.IsNullOrEmpty(PurMoneyCount)
                                    || String.IsNullOrEmpty(Proportion)
                                    || String.IsNullOrEmpty(AddProportion)
                                    || String.IsNullOrEmpty(ExpenditureClass)
                                    || String.IsNullOrEmpty(MtPosition)
                                    || String.IsNullOrEmpty(PurGroupName)
                                    || String.IsNullOrEmpty(PurGroupCode))
                                {
                                    MessageBox.Show("数据录入失败，其检查导入数据表格的格式是否正确", "告知");
                                    return;
                                }
                                //插入到数据库
                                string sql = @"INSERT INTO  Hanmote_MtGroupPositionSheet (
	                                    [MtGroupName],
	                                    [MtGroupCode],
	                                    [SpRiskRank],
	                                    [PurMoneyCount],
	                                    [Proportion],
	                                    [AddProportionCount],
	                                    [CostMoneyCount],
	                                    [MtPosition],
	                                    [PurGroupName],
	                                    [PurGroupCode]
                                    )
                                    VALUES
	                                    (
                                        '" + MtGroupName + "','" + MtGroupCode + "','" + SpRiskRank + "','" + PurMoneyCount + "','" + Proportion + "','" + AddProportion + "','" + ExpenditureClass + "','" + MtPosition + "','" + PurGroupName + "','" + PurGroupCode + "')";

                                string sql1 = sql;
                                DBHelper.ExecuteQueryDT(sql);
                                if (j >= excelTbl.Columns.Count) break;
                            }
                        }
                    }
                }
            }
            ExcelFileTable.DataSource = fillDataTable();
            //绘制金额图
            fillPurCostChart(this.PurGroupName.Text);
        }

        private DataTable fillDataTable()
        {
            string info = @"SELECT
	                                MtGroupName AS 物料组名称,
	                                MtGroupCode AS 物料组编号,
	                                SpRiskRank AS 供应风险等级,
	                                PurMoneyCount AS 采购金额,
	                                Proportion AS 比例,
	                                AddProportionCount AS 累计比例,
	                                CostMoneyCount AS 支出水平,
	                                MtPosition AS 定位
                                FROM
	                                Hanmote_MtGroupPositionSheet
                                WHERE
                                PurGroupName='" + this.PurGroupName.Text + @"'
                                and PurGroupCode= '" + this.PurGroupCode.Text + "'";
            return DBHelper.ExecuteQueryDT(info);
        }

        private DataTable GetExcelTable(string path)
        {
            try
            {
                //获取excel数据  
                DataTable dt1 = new DataTable();
                if (!(path.ToLower().IndexOf(".xlsx") < 0))
                {
                    OledbConnString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source='{0}';Extended Properties='Excel 12.0;HDR=YES'";
                }
                string strConn = string.Format(OledbConnString, path);
                OleDbConnection conn = new OleDbConnection(strConn);
                conn.Open();
                DataTable dt = conn.GetSchema("Tables");
                //判断excel的sheet页数量，查询第1页  
                if (dt.Rows.Count > 0)
                {
                    string selSqlStr = string.Format("select * from [{0}]", dt.Rows[0]["TABLE_NAME"]);
                    OleDbDataAdapter oleDa = new OleDbDataAdapter(selSqlStr, conn);
                    oleDa.Fill(dt1);
                }
                conn.Close();

                return dt1;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Excel转换DataTable出错：" + ex.Message);
                return null;
            }
        }

        private void PurGroupName_SelectedIndexChanged(object sender, EventArgs e)
        {
            String sql = "SELECT * FROM [Buyer_Org] WHERE Buyer_Org_Name='" + this.PurGroupName.Text + "'";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            if (dt.Rows.Count != 0)
            {
                this.PurGroupCode.Text = dt.Rows[0][0].ToString();
            }

            loadData();
        }
        public void initMtGroupName_Load(object sender, EventArgs e)
        {
            DataTable table = systemUserManagerBLL.findAllPurGroupName();
            this.PurGroupName.DataSource = table;
            this.PurGroupName.DisplayMember = "name";
            this.PurGroupName.ValueMember = "id";
           


        }

        private void button3_Click(object sender, EventArgs e)
        {

            String sql = "delete  FROM [Hanmote_MtGroupPositionSheet] WHERE  PurGroupName='" + this.PurGroupName.Text + @"'
                                and PurGroupCode= '" + this.PurGroupCode.Text + "'";
            int count = DBHelper.ExecuteNonQuery(sql);
            DataImportBtn_Click(sender,e);
            if ( count> 0)
            {
                MessageUtil.ShowTips("更新成功！");
            }

        }

        //填充采购支出金额图
        public void fillPurCostChart(string PurName)
        {

            DataTable dt = n_RiskAssessmentBll.getPurCostAndItem(PurName);
            if (dt.Rows.Count == 0)
            {
                return;

            }
            List<double> listX = new List<double>();
            List<double> listY = new List<double>();
            listX.Add(0);
            listY.Add(0);
            double x = 0;
            double y = 0;
            double n = dt.Rows.Count;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                y = float.Parse(dt.Rows[i]["cost"].ToString().TrimEnd('%'));
                x = (i + 1) / n * 100;
                listX.Add(x);
                listY.Add(y);
            }
            listX.Sort();
            listY.Sort();
            this.chart1.Series[0].ToolTip = "采购累计支出金额：#VALY%\n品项占比：#VALX%";
            this.chart1.Series[0].Points.DataBindXY(listX, listY);

        }
        //填充定位图


        private void button1_Click(object sender, EventArgs e)
        {
            PosResultChart posResultChart = new PosResultChart(this.PurGroupName.Text,this.PurGroupCode.Text);
            SingletonUserUI.addToUserUI(posResultChart);
        }


    }
}
