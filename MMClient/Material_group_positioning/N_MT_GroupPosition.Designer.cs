﻿namespace MMClient.Material_group_positioning
{
    partial class N_MT_GroupPosition
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.TB_TotalLevel = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.TB_TotalScore = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.TB_chanceLevel = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.TB_ChanceScore = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.TB_RiskLevel = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.TB_RiskScore = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.TBinfluScore = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tb_InfluScore = new System.Windows.Forms.TextBox();
            this.LB_purOrgId = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.pageNext1 = new pager.pagetool.pageNext();
            this.mutEvlBtn = new System.Windows.Forms.Button();
            this.purchOrgName = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.mtID = new System.Windows.Forms.Label();
            this.mtName = new System.Windows.Forms.Label();
            this.dgv_itemInfo = new System.Windows.Forms.DataGridView();
            this.选择 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.领域 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.供应目标 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.goalId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.指标 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.影响力得分 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.影响力pip等级 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.风险 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.供应风险得分 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.供应风险等级 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.chance = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.chanceScore = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.chanceLevel = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.评估 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.result = new System.Windows.Forms.Label();
            this.label = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LB_qualityClass = new System.Windows.Forms.Label();
            this.LB_classText = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_itemInfo)).BeginInit();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Controls.Add(this.LB_purOrgId);
            this.panel1.Controls.Add(this.label17);
            this.panel1.Controls.Add(this.pageNext1);
            this.panel1.Controls.Add(this.mutEvlBtn);
            this.panel1.Controls.Add(this.purchOrgName);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.mtID);
            this.panel1.Controls.Add(this.mtName);
            this.panel1.Controls.Add(this.dgv_itemInfo);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.label);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(22, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1177, 774);
            this.panel1.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox1.Controls.Add(this.LB_qualityClass);
            this.groupBox1.Controls.Add(this.LB_classText);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.TB_TotalLevel);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.TB_TotalScore);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.TB_chanceLevel);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.TB_ChanceScore);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.TB_RiskLevel);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.TB_RiskScore);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.TBinfluScore);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.tb_InfluScore);
            this.groupBox1.Location = new System.Drawing.Point(19, 633);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1148, 132);
            this.groupBox1.TabIndex = 19;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "评估结果";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(655, 47);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 38;
            this.button1.Text = "保存";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(487, 51);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(53, 12);
            this.label10.TabIndex = 37;
            this.label10.Text = "综合等级";
            // 
            // TB_TotalLevel
            // 
            this.TB_TotalLevel.Enabled = false;
            this.TB_TotalLevel.Location = new System.Drawing.Point(558, 48);
            this.TB_TotalLevel.Name = "TB_TotalLevel";
            this.TB_TotalLevel.Size = new System.Drawing.Size(39, 21);
            this.TB_TotalLevel.TabIndex = 36;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(487, 26);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(53, 12);
            this.label11.TabIndex = 35;
            this.label11.Text = "综合得分";
            // 
            // TB_TotalScore
            // 
            this.TB_TotalScore.Enabled = false;
            this.TB_TotalScore.Location = new System.Drawing.Point(558, 21);
            this.TB_TotalScore.Name = "TB_TotalScore";
            this.TB_TotalScore.Size = new System.Drawing.Size(39, 21);
            this.TB_TotalScore.TabIndex = 34;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(321, 47);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 12);
            this.label5.TabIndex = 33;
            this.label5.Text = "机遇等级";
            // 
            // TB_chanceLevel
            // 
            this.TB_chanceLevel.Enabled = false;
            this.TB_chanceLevel.Location = new System.Drawing.Point(392, 44);
            this.TB_chanceLevel.Name = "TB_chanceLevel";
            this.TB_chanceLevel.Size = new System.Drawing.Size(39, 21);
            this.TB_chanceLevel.TabIndex = 32;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(321, 22);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 12);
            this.label6.TabIndex = 31;
            this.label6.Text = "机遇得分";
            // 
            // TB_ChanceScore
            // 
            this.TB_ChanceScore.Enabled = false;
            this.TB_ChanceScore.Location = new System.Drawing.Point(392, 17);
            this.TB_ChanceScore.Name = "TB_ChanceScore";
            this.TB_ChanceScore.Size = new System.Drawing.Size(39, 21);
            this.TB_ChanceScore.TabIndex = 30;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(170, 47);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(53, 12);
            this.label8.TabIndex = 29;
            this.label8.Text = "风险等级";
            // 
            // TB_RiskLevel
            // 
            this.TB_RiskLevel.Enabled = false;
            this.TB_RiskLevel.Location = new System.Drawing.Point(241, 44);
            this.TB_RiskLevel.Name = "TB_RiskLevel";
            this.TB_RiskLevel.Size = new System.Drawing.Size(39, 21);
            this.TB_RiskLevel.TabIndex = 28;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(170, 22);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(53, 12);
            this.label9.TabIndex = 27;
            this.label9.Text = "风险得分";
            // 
            // TB_RiskScore
            // 
            this.TB_RiskScore.Enabled = false;
            this.TB_RiskScore.Location = new System.Drawing.Point(241, 17);
            this.TB_RiskScore.Name = "TB_RiskScore";
            this.TB_RiskScore.Size = new System.Drawing.Size(39, 21);
            this.TB_RiskScore.TabIndex = 26;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(24, 50);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(65, 12);
            this.label7.TabIndex = 25;
            this.label7.Text = "影响力等级";
            // 
            // TBinfluScore
            // 
            this.TBinfluScore.Enabled = false;
            this.TBinfluScore.Location = new System.Drawing.Point(95, 47);
            this.TBinfluScore.Name = "TBinfluScore";
            this.TBinfluScore.Size = new System.Drawing.Size(39, 21);
            this.TBinfluScore.TabIndex = 24;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(24, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 12);
            this.label2.TabIndex = 19;
            this.label2.Text = "影响力得分";
            // 
            // tb_InfluScore
            // 
            this.tb_InfluScore.Enabled = false;
            this.tb_InfluScore.Location = new System.Drawing.Point(95, 20);
            this.tb_InfluScore.Name = "tb_InfluScore";
            this.tb_InfluScore.Size = new System.Drawing.Size(39, 21);
            this.tb_InfluScore.TabIndex = 18;
            // 
            // LB_purOrgId
            // 
            this.LB_purOrgId.AutoSize = true;
            this.LB_purOrgId.Location = new System.Drawing.Point(418, 57);
            this.LB_purOrgId.Name = "LB_purOrgId";
            this.LB_purOrgId.Size = new System.Drawing.Size(29, 12);
            this.LB_purOrgId.TabIndex = 17;
            this.LB_purOrgId.Text = "编号";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(323, 57);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(89, 12);
            this.label17.TabIndex = 16;
            this.label17.Text = "采购组织编号：";
            // 
            // pageNext1
            // 
            this.pageNext1.Location = new System.Drawing.Point(26, 580);
            this.pageNext1.Name = "pageNext1";
            this.pageNext1.PageIndex = 1;
            this.pageNext1.PageSize = 100;
            this.pageNext1.RecordCount = 0;
            this.pageNext1.Size = new System.Drawing.Size(660, 37);
            this.pageNext1.TabIndex = 15;
            this.pageNext1.OnPageChanged += new System.EventHandler(this.pageNext1_OnPageChanged);
            this.pageNext1.Load += new System.EventHandler(this.pageNext1_Load);
            // 
            // mutEvlBtn
            // 
            this.mutEvlBtn.Location = new System.Drawing.Point(636, 52);
            this.mutEvlBtn.Name = "mutEvlBtn";
            this.mutEvlBtn.Size = new System.Drawing.Size(75, 23);
            this.mutEvlBtn.TabIndex = 13;
            this.mutEvlBtn.Text = "批量评估";
            this.mutEvlBtn.UseVisualStyleBackColor = true;
            this.mutEvlBtn.Click += new System.EventHandler(this.mutEvlBtn_Click);
            // 
            // purchOrgName
            // 
            this.purchOrgName.AutoSize = true;
            this.purchOrgName.Location = new System.Drawing.Point(418, 21);
            this.purchOrgName.Name = "purchOrgName";
            this.purchOrgName.Size = new System.Drawing.Size(29, 12);
            this.purchOrgName.TabIndex = 12;
            this.purchOrgName.Text = "编号";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(323, 21);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(89, 12);
            this.label4.TabIndex = 11;
            this.label4.Text = "采购组织名称：";
            // 
            // mtID
            // 
            this.mtID.AutoSize = true;
            this.mtID.Location = new System.Drawing.Point(86, 57);
            this.mtID.Name = "mtID";
            this.mtID.Size = new System.Drawing.Size(29, 12);
            this.mtID.TabIndex = 10;
            this.mtID.Text = "编号";
            // 
            // mtName
            // 
            this.mtName.AutoSize = true;
            this.mtName.Location = new System.Drawing.Point(86, 21);
            this.mtName.Name = "mtName";
            this.mtName.Size = new System.Drawing.Size(53, 12);
            this.mtName.TabIndex = 9;
            this.mtName.Text = "物料名称";
            // 
            // dgv_itemInfo
            // 
            this.dgv_itemInfo.AllowUserToAddRows = false;
            this.dgv_itemInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgv_itemInfo.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_itemInfo.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgv_itemInfo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgv_itemInfo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_itemInfo.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.选择,
            this.领域,
            this.供应目标,
            this.goalId,
            this.指标,
            this.影响力得分,
            this.影响力pip等级,
            this.风险,
            this.供应风险得分,
            this.供应风险等级,
            this.chance,
            this.chanceScore,
            this.chanceLevel,
            this.评估});
            this.dgv_itemInfo.GridColor = System.Drawing.SystemColors.Control;
            this.dgv_itemInfo.Location = new System.Drawing.Point(19, 83);
            this.dgv_itemInfo.Name = "dgv_itemInfo";
            this.dgv_itemInfo.RowTemplate.Height = 23;
            this.dgv_itemInfo.Size = new System.Drawing.Size(1148, 549);
            this.dgv_itemInfo.TabIndex = 8;
            this.dgv_itemInfo.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            this.dgv_itemInfo.RowStateChanged += new System.Windows.Forms.DataGridViewRowStateChangedEventHandler(this.dataGridView1_RowStateChanged);
            // 
            // 选择
            // 
            this.选择.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.选择.FillWeight = 22.84264F;
            this.选择.HeaderText = "选择";
            this.选择.Name = "选择";
            this.选择.Width = 50;
            // 
            // 领域
            // 
            this.领域.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.领域.DataPropertyName = "领域";
            this.领域.HeaderText = "领域";
            this.领域.Name = "领域";
            this.领域.Width = 70;
            // 
            // 供应目标
            // 
            this.供应目标.DataPropertyName = "供应目标";
            this.供应目标.FillWeight = 49.83849F;
            this.供应目标.HeaderText = "供应目标";
            this.供应目标.Name = "供应目标";
            this.供应目标.ReadOnly = true;
            this.供应目标.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // goalId
            // 
            this.goalId.DataPropertyName = "供应指标编号";
            this.goalId.HeaderText = "供应指标编号";
            this.goalId.Name = "goalId";
            this.goalId.ReadOnly = true;
            this.goalId.Visible = false;
            // 
            // 指标
            // 
            this.指标.DataPropertyName = "指标";
            this.指标.FillWeight = 49.83849F;
            this.指标.HeaderText = "指标";
            this.指标.Name = "指标";
            this.指标.ReadOnly = true;
            this.指标.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // 影响力得分
            // 
            this.影响力得分.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.影响力得分.DataPropertyName = "影响力得分";
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.影响力得分.DefaultCellStyle = dataGridViewCellStyle1;
            this.影响力得分.FillWeight = 109.6447F;
            this.影响力得分.HeaderText = "影响力得分";
            this.影响力得分.Name = "影响力得分";
            this.影响力得分.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.影响力得分.ToolTipText = "输入分数";
            this.影响力得分.Width = 80;
            // 
            // 影响力pip等级
            // 
            this.影响力pip等级.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.影响力pip等级.DataPropertyName = "影响力PIP等级";
            this.影响力pip等级.FillWeight = 49.83849F;
            this.影响力pip等级.HeaderText = "影响力PIP等级";
            this.影响力pip等级.Name = "影响力pip等级";
            this.影响力pip等级.ReadOnly = true;
            this.影响力pip等级.Width = 80;
            // 
            // 风险
            // 
            this.风险.DataPropertyName = "风险";
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.风险.DefaultCellStyle = dataGridViewCellStyle2;
            this.风险.FillWeight = 49.83849F;
            this.风险.HeaderText = "风险";
            this.风险.Name = "风险";
            this.风险.ToolTipText = "填写风险";
            // 
            // 供应风险得分
            // 
            this.供应风险得分.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.供应风险得分.DataPropertyName = "供应风险得分";
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.供应风险得分.DefaultCellStyle = dataGridViewCellStyle3;
            this.供应风险得分.FillWeight = 468.4818F;
            this.供应风险得分.HeaderText = "供应风险得分";
            this.供应风险得分.Name = "供应风险得分";
            this.供应风险得分.ToolTipText = "输入分数";
            this.供应风险得分.Width = 80;
            // 
            // 供应风险等级
            // 
            this.供应风险等级.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.供应风险等级.DataPropertyName = "供应风险等级";
            this.供应风险等级.FillWeight = 49.83849F;
            this.供应风险等级.HeaderText = "供应风险等级";
            this.供应风险等级.Name = "供应风险等级";
            this.供应风险等级.ReadOnly = true;
            this.供应风险等级.Width = 80;
            // 
            // chance
            // 
            this.chance.DataPropertyName = "chanceText";
            this.chance.HeaderText = "机遇";
            this.chance.Name = "chance";
            // 
            // chanceScore
            // 
            this.chanceScore.DataPropertyName = "chanceScore";
            this.chanceScore.HeaderText = "机遇得分";
            this.chanceScore.Name = "chanceScore";
            // 
            // chanceLevel
            // 
            this.chanceLevel.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.chanceLevel.DataPropertyName = "chanceLeve";
            this.chanceLevel.HeaderText = "机遇等级";
            this.chanceLevel.Name = "chanceLevel";
            this.chanceLevel.Width = 61;
            // 
            // 评估
            // 
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.NullValue = "评估";
            this.评估.DefaultCellStyle = dataGridViewCellStyle4;
            this.评估.FillWeight = 49.83849F;
            this.评估.HeaderText = "评估";
            this.评估.Name = "评估";
            this.评估.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // panel3
            // 
            this.panel3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.panel3.Controls.Add(this.label3);
            this.panel3.Controls.Add(this.result);
            this.panel3.Location = new System.Drawing.Point(903, 415);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(244, 158);
            this.panel3.TabIndex = 17;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label3.Location = new System.Drawing.Point(3, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(72, 16);
            this.label3.TabIndex = 9;
            this.label3.Text = "评估结果";
            // 
            // result
            // 
            this.result.AutoSize = true;
            this.result.BackColor = System.Drawing.SystemColors.Control;
            this.result.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.result.ForeColor = System.Drawing.Color.Red;
            this.result.Location = new System.Drawing.Point(7, 38);
            this.result.Name = "result";
            this.result.Size = new System.Drawing.Size(63, 14);
            this.result.TabIndex = 10;
            this.result.Text = "尚未评估";
            // 
            // label
            // 
            this.label.AutoSize = true;
            this.label.Location = new System.Drawing.Point(24, 57);
            this.label.Name = "label";
            this.label.Size = new System.Drawing.Size(53, 12);
            this.label.TabIndex = 5;
            this.label.Text = "编  号：";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(24, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 12);
            this.label1.TabIndex = 1;
            this.label1.Text = "物料名称:";
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.dataGridViewTextBoxColumn1.DataPropertyName = "领域";
            this.dataGridViewTextBoxColumn1.HeaderText = "领域";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Width = 70;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "供应目标";
            this.dataGridViewTextBoxColumn2.FillWeight = 49.83849F;
            this.dataGridViewTextBoxColumn2.HeaderText = "供应目标";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn2.Width = 127;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "指标";
            this.dataGridViewTextBoxColumn3.FillWeight = 49.83849F;
            this.dataGridViewTextBoxColumn3.HeaderText = "指标";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn3.Width = 128;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.dataGridViewTextBoxColumn4.DataPropertyName = "影响力得分";
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dataGridViewTextBoxColumn4.DefaultCellStyle = dataGridViewCellStyle5;
            this.dataGridViewTextBoxColumn4.FillWeight = 109.6447F;
            this.dataGridViewTextBoxColumn4.HeaderText = "影响力得分";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn4.ToolTipText = "输入分数";
            this.dataGridViewTextBoxColumn4.Width = 80;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.dataGridViewTextBoxColumn5.DataPropertyName = "影响力PIP等级";
            this.dataGridViewTextBoxColumn5.FillWeight = 49.83849F;
            this.dataGridViewTextBoxColumn5.HeaderText = "影响力PIP等级";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            this.dataGridViewTextBoxColumn5.Width = 80;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "风险";
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dataGridViewTextBoxColumn6.DefaultCellStyle = dataGridViewCellStyle6;
            this.dataGridViewTextBoxColumn6.FillWeight = 49.83849F;
            this.dataGridViewTextBoxColumn6.HeaderText = "风险";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ToolTipText = "填写风险";
            this.dataGridViewTextBoxColumn6.Width = 127;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.dataGridViewTextBoxColumn7.DataPropertyName = "供应风险得分";
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dataGridViewTextBoxColumn7.DefaultCellStyle = dataGridViewCellStyle7;
            this.dataGridViewTextBoxColumn7.FillWeight = 468.4818F;
            this.dataGridViewTextBoxColumn7.HeaderText = "供应风险得分";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ToolTipText = "输入分数";
            this.dataGridViewTextBoxColumn7.Width = 80;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.dataGridViewTextBoxColumn8.DataPropertyName = "供应风险等级";
            this.dataGridViewTextBoxColumn8.FillWeight = 49.83849F;
            this.dataGridViewTextBoxColumn8.HeaderText = "供应风险等级";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            this.dataGridViewTextBoxColumn8.Width = 80;
            // 
            // LB_qualityClass
            // 
            this.LB_qualityClass.AutoSize = true;
            this.LB_qualityClass.Location = new System.Drawing.Point(119, 90);
            this.LB_qualityClass.Name = "LB_qualityClass";
            this.LB_qualityClass.Size = new System.Drawing.Size(29, 12);
            this.LB_qualityClass.TabIndex = 40;
            this.LB_qualityClass.Text = "编号";
            // 
            // LB_classText
            // 
            this.LB_classText.AutoSize = true;
            this.LB_classText.Location = new System.Drawing.Point(24, 90);
            this.LB_classText.Name = "LB_classText";
            this.LB_classText.Size = new System.Drawing.Size(65, 12);
            this.LB_classText.TabIndex = 39;
            this.LB_classText.Text = "物料特性：";
            // 
            // N_MT_GroupPosition
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1201, 789);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "N_MT_GroupPosition";
            this.Text = "等级定位";
            this.Load += new System.EventHandler(this.N_MT_GroupPosition_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_itemInfo)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridView dgv_itemInfo;
        private System.Windows.Forms.Label label;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label mtName;
        private System.Windows.Forms.Label mtID;
        private System.Windows.Forms.Label purchOrgName;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label result;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button mutEvlBtn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private pager.pagetool.pageNext pageNext1;
        private System.Windows.Forms.Label LB_purOrgId;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox TB_TotalLevel;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox TB_TotalScore;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox TB_chanceLevel;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox TB_ChanceScore;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox TB_RiskLevel;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox TB_RiskScore;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox TBinfluScore;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tb_InfluScore;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn 选择;
        private System.Windows.Forms.DataGridViewTextBoxColumn 领域;
        private System.Windows.Forms.DataGridViewTextBoxColumn 供应目标;
        private System.Windows.Forms.DataGridViewTextBoxColumn goalId;
        private System.Windows.Forms.DataGridViewTextBoxColumn 指标;
        private System.Windows.Forms.DataGridViewTextBoxColumn 影响力得分;
        private System.Windows.Forms.DataGridViewTextBoxColumn 影响力pip等级;
        private System.Windows.Forms.DataGridViewTextBoxColumn 风险;
        private System.Windows.Forms.DataGridViewTextBoxColumn 供应风险得分;
        private System.Windows.Forms.DataGridViewTextBoxColumn 供应风险等级;
        private System.Windows.Forms.DataGridViewTextBoxColumn chance;
        private System.Windows.Forms.DataGridViewTextBoxColumn chanceScore;
        private System.Windows.Forms.DataGridViewTextBoxColumn chanceLevel;
        private System.Windows.Forms.DataGridViewButtonColumn 评估;
        private System.Windows.Forms.Label LB_qualityClass;
        private System.Windows.Forms.Label LB_classText;
    }
}