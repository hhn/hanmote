﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;

using Lib.Bll.Material_group_positioning.General;
using Lib.Common.CommonUtils;
using Lib.SqlServerDAL;
using System.Text.RegularExpressions;

namespace MMClient.Material_group_positioning
{
    public partial class Handel : Form
    {
        private string Orgid;
        private string Orgname;
        private string Mtgid;
        private string Mtgname;
        public Handel(string orgid, string orgname, string mtgid, string mtgname)
        {
            this.Orgid = orgid;
            this.Orgname = orgname;
            this.Mtgid = mtgid;
            this.Mtgname = mtgname;

            InitializeComponent();
        }

        private void View_Load(object sender, EventArgs e)
        {
            //button5.Visible = true;


            String F = null;
            String T = null;
            textBox1.Text = this.Orgid;
            textBox3.Text = this.Orgname;
            name_MtGroup.Text = this.Mtgid;
            textBox4.Text = this.Mtgname;






            List<String> MtGroup_ID = new List<string>();//领域绑定
            String sql = " select distinct Area  from  MTGAttributeDefinition AS M  WHERE M.Category='生产性物料' and M.PurOrgID='" + textBox1.Text + "' AND M.id='" + name_MtGroup.Text + "'";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    MtGroup_ID.Add(dt.Rows[i][0].ToString());
                }
            }
            comboBox2.DataSource = MtGroup_ID;






            //创建人和创建日期
            String sql1 = "SELECT Founder,DateTime FROM Porg_MtGroup_Relationship WHERE Porg_ID='" + textBox1.Text + "'and MtGroup_ID='" + name_MtGroup.Text + "';";
            DataTable dt1 = DBHelper.ExecuteQueryDT(sql1);
            if (dt1.Rows.Count != 0)
            {
                F = dt1.Rows[0][0].ToString();
                T = dt1.Rows[0][1].ToString();
            }
            textBox6.Text = F;
            textBox5.Text = T;

            GeneralBLL gn = new GeneralBLL();
            List<String> model = gn.getModel();//获取属性模板绑定源
            comboBox21.DataSource = model;
            dataGridView1.DataSource = null;
            dataGridView1.Columns.Clear();
            dataGridView1.Rows.Clear();

            //数据表绑定
            string sql2 = @"select 
                                    M.Area as '领域',
                                    M.Aims as '供应目标', 
                                    M.Goals as '指标' 
                                    from MTGAttributeDefinition as M
                                    WHERE M.Category='生产性物料' and M.PurOrgID='" + textBox1.Text + "' AND M.id='" + name_MtGroup.Text + "';";

            DataTable dt2 = DBHelper.ExecuteQueryDT(sql2);
            dataGridView1.DataSource = dt2;




            button5.Visible = true;
            button6.Visible = true;
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)//使领域和数据表联动
        {
            string sql2 = @"select 
                                    M.Area as '领域',
                                    M.Aims as '供应目标', 
                                    M.Goals as '指标' 
                                    from MTGAttributeDefinition as M
                                    WHERE M.Category='生产性物料' and M.PurOrgID='" + textBox1.Text + "' AND M.id='" + name_MtGroup.Text + "'AND M.Area='" + comboBox2.Text + "'";

            DataTable dt2 = DBHelper.ExecuteQueryDT(sql2);
            dataGridView1.DataSource = dt2;
        }

        private void button1_Click(object sender, EventArgs e)//add
        {
            HandelAdd a = new HandelAdd(textBox1.Text, textBox3.Text, name_MtGroup.Text, textBox4.Text);
            a.Show();
        }

        private void button5_Click(object sender, EventArgs e)//update
        {
            string a = textBox1.Text;
            string b = textBox3.Text;
            string c = name_MtGroup.Text;
            string d = textBox4.Text;
            string g = dataGridView1.CurrentRow.Cells["领域"].Value.ToString();
            string h = dataGridView1.CurrentRow.Cells["供应目标"].Value.ToString();
            string f = dataGridView1.CurrentRow.Cells["指标"].Value.ToString();
            modifyAtribute B = new modifyAtribute(a, b, c, d, g, h, f);
            B.Show();
        }

        private void button6_Click(object sender, EventArgs e)//delete
        {
            string b = dataGridView1.CurrentRow.Cells["领域"].Value.ToString();
            string c = dataGridView1.CurrentRow.Cells["供应目标"].Value.ToString();
            string d = dataGridView1.CurrentRow.Cells["指标"].Value.ToString();
            GeneralBLL gn = new GeneralBLL();
            //gn.deletedata(c, d);//删除数据
            gn.deletedata(b, c, d);//删除数据

            string sql = @"delete from MTGAttributeDefinition WHERE Category='生产性物料' and PurOrgID='" + textBox1.Text + "' and id='" + name_MtGroup.Text + "' and Aims='" + c + "' AND Goals='" + d + "' AND Area='" + b + "'";
            DBHelper.ExecuteQueryDT(sql);

            MessageUtil.ShowWarning("删除成功！");


            string sql2 = @"select 
                                    M.Area as '领域',
                                    M.Aims as '供应目标', 
                                    M.Goals as '指标' 
                                    from MTGAttributeDefinition as M
                                    WHERE M.Category='生产性物料' and M.PurOrgID='" + textBox1.Text + "' AND M.id='" + name_MtGroup.Text + "';";

            DataTable dt2 = DBHelper.ExecuteQueryDT(sql2);
            dataGridView1.DataSource = dt2;
        }

        private void button2_Click(object sender, EventArgs e)//refresh
        {
            button5.Visible = true;
            button6.Visible = true;


            dataGridView1.DataSource = null;
            dataGridView1.Columns.Clear();
            dataGridView1.Rows.Clear();


            String F = null;
            String T = null;


            //领域刷新
            List<String> MtGroup_ID = new List<string>();//领域绑定
            String sql = " select distinct Area  from  MTGAttributeDefinition AS M  WHERE M.Category='生产性物料' and M.PurOrgID='" + textBox1.Text + "' AND M.id='" + name_MtGroup.Text + "'";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    MtGroup_ID.Add(dt.Rows[i][0].ToString());
                }
            }
            comboBox2.DataSource = MtGroup_ID;

            //数据表刷新

            textBox1.Text = this.Orgid;
            textBox3.Text = this.Orgname;
            name_MtGroup.Text = this.Mtgid;
            textBox4.Text = this.Mtgname;

            string sql3 = @"select 
                                    M.Area as '领域',
                                    M.Aims as '供应目标', 
                                    M.Goals as '指标' 
                                    from MTGAttributeDefinition as M
                                    WHERE M.Category='生产性物料' and M.PurOrgID='" + textBox1.Text + "' AND M.id='" + name_MtGroup.Text + "';";

            DataTable dt3 = DBHelper.ExecuteQueryDT(sql3);
            dataGridView1.DataSource = dt3;




            //创建人创建时间刷新
            String sql1 = "SELECT Founder,DateTime FROM Porg_MtGroup_Relationship WHERE Porg_ID='" + textBox1.Text + "'and MtGroup_ID='" + name_MtGroup.Text + "';";
            DataTable dt1 = DBHelper.ExecuteQueryDT(sql1);
            if (dt1.Rows.Count != 0)
            {
                F = dt1.Rows[0][0].ToString();
                T = dt1.Rows[0][1].ToString();
            }
            textBox6.Text = F;
            textBox5.Text = T;

        }

        private void button3_Click(object sender, EventArgs e)//继续添加
        {
            dataGridView1.DataSource = null;
            dataGridView1.Columns.Clear();
            dataGridView1.Rows.Clear();


            DataGridViewTextBoxColumn co = new DataGridViewTextBoxColumn();
            co.Name = "Column";
            co.Width = 50;
            co.HeaderText = "领域";

            DataGridViewTextBoxColumn co1 = new DataGridViewTextBoxColumn();
            co1.Name = "Column1";
            co1.Width = 500;
            co1.HeaderText = "供应目标";

            DataGridViewTextBoxColumn co2 = new DataGridViewTextBoxColumn();
            co2.Name = "Column2";
            co2.Width = 5000;
            co2.HeaderText = "指标";
            dataGridView1.Columns.AddRange(co);
            dataGridView1.Columns.AddRange(co1);
            dataGridView1.Columns.AddRange(co2);


            //this.dataGridView1.Columns["供应目标"].FillWeight = 300;//设置某一列的宽度，数值为比例值
            //this.dataGridView1.Columns["指标"].FillWeight = 300;//设置某一列的宽度，数值为比例值
            //this.dataGridView1.Rows.Add(5);






            this.dataGridView1.Rows.Add(10);
        }

        private void button8_Click(object sender, EventArgs e)//保存为模板
        {
            GeneralBLL gn = new GeneralBLL();
            string aim = "";
            string goal = "";
            string area = "";

            for (int i = 0; i < dataGridView1.RowCount - 1; i++)//保存创建数据
            {
                area = area + dataGridView1.Rows[i].Cells[0].Value + "&";
                aim = aim + dataGridView1.Rows[i].Cells[1].Value + "&";
                goal = goal + dataGridView1.Rows[i].Cells[2].Value + "&";

            }

            Popup a = new Popup(area, aim, goal);

            a.Show();
        }

        private void button7_Click(object sender, EventArgs e)//保存
        {
            GeneralBLL gn = new GeneralBLL();

            textBox1.Text = this.Orgid;
            textBox3.Text = this.Orgname;
            name_MtGroup.Text = this.Mtgid;
            textBox4.Text = this.Mtgname;


            string sql = null;
            string sql1 = null;
            for (int i = 0; i < dataGridView1.RowCount - 1; i++)//保存创建数据
            {
                if ((dataGridView1.Rows[i].Cells[0].Value != null) && (dataGridView1.Rows[i].Cells[1].Value != null) && (dataGridView1.Rows[i].Cells[2].Value != null))
                {
                    if ((!dataGridView1.Rows[i].Cells[0].Value.Equals("")) && (!dataGridView1.Rows[i].Cells[1].Value.Equals("")) && (!dataGridView1.Rows[i].Cells[2].Value.Equals("")))
                    {
                        sql = @"
IF NOT EXISTS(SELECT * FROM MTGAttributeDefinition WHERE Category='生产性物料' and Aims='" + dataGridView1.Rows[i].Cells[1].Value + "' and PurOrgID='" + textBox1.Text + "'and id='" + name_MtGroup.Text + "' and Area='" + dataGridView1.Rows[i].Cells[0].Value + "' and Goals= '" + dataGridView1.Rows[i].Cells[2].Value + "') BEGIN insert into   MTGAttributeDefinition (PurOrgID,PurOrgName,id,MTGName,Category,Aims,Goals,Area,Founder,DateTime)  values ('" + textBox1.Text + "','" + textBox3.Text + "','" + name_MtGroup.Text + "','" + textBox4.Text + "','生产性物料','" + dataGridView1.Rows[i].Cells[1].Value + "','" + dataGridView1.Rows[i].Cells[2].Value + "','" + dataGridView1.Rows[i].Cells[0].Value + "','" + textBox6.Text + "','" + dateTimePicker1.Text + "');  END";


                        DBHelper.ExecuteQueryDT(sql);
                    }
                }
            }
            //将创建状态改为1
            sql1 = @"UPDATE Porg_MtGroup_Relationship
        SET	Status='1',DateTime='" + dateTimePicker1.Text + "' ,Founder ='" + textBox6.Text + "' WHERE Porg_ID='" + textBox1.Text + "' AND MtGroup_ID='" + name_MtGroup.Text + "'";
            DBHelper.ExecuteQueryDT(sql1);





            MessageUtil.ShowWarning("保存成功！");
            //this.Close();
        }

        private void button4_Click(object sender, EventArgs e)//模板刷新
        {
            GeneralBLL gn = new GeneralBLL();
            List<String> model = gn.getModel();//获取属性模板绑定源
            comboBox21.DataSource = model;
        }
        
        private void comboBox21_SelectedIndexChanged(object sender, EventArgs e)//获取属性模板
        {
            button5.Visible = false;
            button6.Visible = false;


            dataGridView1.DataSource = null;
            dataGridView1.Columns.Clear();
            dataGridView1.Rows.Clear();


            DataGridViewTextBoxColumn co = new DataGridViewTextBoxColumn();
            co.Name = "Column";
            co.Width = 50;
            co.HeaderText = "领域";

            DataGridViewTextBoxColumn co1 = new DataGridViewTextBoxColumn();
            co1.Name = "Column1";
            co1.Width = 500;
            co1.HeaderText = "供应目标";

            DataGridViewTextBoxColumn co2 = new DataGridViewTextBoxColumn();
            co2.Name = "Column2";
            co2.Width = 5000;
            co2.HeaderText = "指标";
            dataGridView1.Columns.AddRange(co);
            dataGridView1.Columns.AddRange(co1);
            dataGridView1.Columns.AddRange(co2);


            //this.dataGridView1.Columns["供应目标"].FillWeight = 300;//设置某一列的宽度，数值为比例值
            //this.dataGridView1.Columns["指标"].FillWeight = 300;//设置某一列的宽度，数值为比例值
            this.dataGridView1.Rows.Add(10);











            GeneralBLL gn = new GeneralBLL();
            /* List<String> model = gn.getModel();//获取属性模板绑定源
             comboBox21.DataSource = model;*/
            String MtGroup_ID = "";
            String M = "";
            //String sql = " select Buyer_Org_Name  from  Buyer_Org ";
            String sql = "SELECT AimModel FROM [MTGroupAttributeModel] WHERE Type='" + comboBox21.Text + "'";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            if (dt.Rows.Count != 0)
            {
                MtGroup_ID = dt.Rows[0][0].ToString();
            }
            string[] sArray = Regex.Split(MtGroup_ID, "&", RegexOptions.IgnoreCase);
            if (dataGridView1.RowCount < sArray.Length)
            {
                this.dataGridView1.Rows.Add(sArray.Length - dataGridView1.RowCount);
            }
            for (int i = 0; i < sArray.Length; i++)
            {
                dataGridView1.Rows[i].Cells[1].Value = sArray[i];

            }


            String sql1 = "SELECT GoalModel FROM [MTGroupAttributeModel] WHERE Type='" + comboBox21.Text + "'";
            DataTable dt1 = DBHelper.ExecuteQueryDT(sql1);
            if (dt1.Rows.Count != 0)
            {
                M = dt1.Rows[0][0].ToString();
            }
            string[] sArray1 = Regex.Split(M, "&", RegexOptions.IgnoreCase);

            if (dataGridView1.RowCount < sArray1.Length)
            {
                this.dataGridView1.Rows.Add(sArray1.Length - dataGridView1.RowCount);
            }
            for (int i = 0; i < sArray1.Length; i++)
            {
                dataGridView1.Rows[i].Cells[2].Value = sArray1[i];

            }

            String sql2 = "SELECT AreaModel FROM [MTGroupAttributeModel] WHERE Type='" + comboBox21.Text + "'";
            DataTable dt2 = DBHelper.ExecuteQueryDT(sql2);
            if (dt2.Rows.Count != 0)
            {
                M = dt2.Rows[0][0].ToString();
            }
            string[] sArray2 = Regex.Split(M, "&", RegexOptions.IgnoreCase);

            if (dataGridView1.RowCount < sArray2.Length)
            {
                this.dataGridView1.Rows.Add(sArray2.Length - dataGridView1.RowCount);
            }
            for (int i = 0; i < sArray2.Length; i++)
            {
                dataGridView1.Rows[i].Cells[0].Value = sArray2[i];

            }

        }

    }
}
