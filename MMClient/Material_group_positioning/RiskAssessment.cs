﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;
using Lib.Bll.MT_GroupBll;
using Lib.Common.CommonUtils;
using Lib.Bll.MDBll.General;
using System.Windows.Forms.DataVisualization.Charting;

namespace MMClient.Material_group_positioning
{
    public partial class RiskAssessment : DockContent
    {
        GeneralBLL gn = new GeneralBLL();
        N_RiskAssessmentBll riskAssessmentBll = new N_RiskAssessmentBll();

        public RiskAssessment()
        {
            InitializeComponent();
        }

        private void RiskAssessment_Load(object sender, EventArgs e)
        {
            dgv_mtInfo.AutoGenerateColumns = false;
            //查询MTGAttributeDefinition表数据
            //dgv_mtInfo.DataSource = riskAssessmentBll.GetDefinedMt_Group(this.purOrgName.Text);

            this.purOrgName.DisplayMember = "Buyer_Org_Name";
            this.purOrgName.ValueMember = "Buyer_Org";
            this.purOrgName.DataSource = gn.GetAllBuyerOrganizationName();

           // double[] r = { 2, 8, 18 };
           // DrawCircl(r.Length, this.chart1, r);
            //initalChart();//初始化图表

        }

        private void dataGridView1_RowStateChanged(object sender, DataGridViewRowStateChangedEventArgs e)
        {
            //显示在HeaderCell上
            for (int i = 0; i < this.dgv_mtInfo.Rows.Count; i++)
            {
                DataGridViewRow r = this.dgv_mtInfo.Rows[i];
                r.HeaderCell.Value = string.Format("{0}", i + 1);
            }
            this.dgv_mtInfo.Refresh();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgv_mtInfo.Columns[e.ColumnIndex].Name.Equals("opreate")) {
                string id = dgv_mtInfo.CurrentRow.Cells["物料编号"].Value.ToString();
                string name = dgv_mtInfo.CurrentRow.Cells["物料名称"].Value.ToString();
                string type = dgv_mtInfo.CurrentRow.Cells["type"].Value.ToString();

                //获取物料组信息
                N_MT_GroupPosition n_MT_GroupPosition = new N_MT_GroupPosition(id, name, this.purOrgName.SelectedValue.ToString().Trim(), this.purOrgName.Text.ToString().Trim(),type);
                SingletonUserUI.addToUserUI(n_MT_GroupPosition);
            }
        }


        //初始化图表
        private void initalChart()
        {
           
            List<float> listY = new List<float>();
            List<float> listX = new List<float>();
            double r = 0;
            DataTable dt = riskAssessmentBll.getMtGroupFinished(purOrgName.Text);
            Series series1 = this.chart1.Series["无影响"];
            Series series2 = this.chart1.Series["低影响"];
            Series series3 = this.chart1.Series["中影响"];
            this.chart1.Series.Clear();

            chart1.Series.Add(series1);
            chart1.Series.Add(series2);
            chart1.Series.Add(series3);

            for (int i = 0; i < dt.Rows.Count; i++)
            {

                listX.Clear();
                listY.Clear();
                if (!dt.Rows[i]["riskScore"].ToString().Equals(""))
                {

                    listY.Add(float.Parse(dt.Rows[i]["influenceScore"].ToString()));
                    listX.Add(float.Parse(dt.Rows[i]["riskScore"].ToString()));
                    r = Math.Pow(listX[0], 2) + Math.Pow(listY[0], 2);
                }


                //求欧式距离


                Series series = new Series();
                series.ChartArea = "ChartArea1";
                series.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Bubble;
                series.IsValueShownAsLabel = false;
                //series.Label = dt.Rows[i]["Aims"].ToString();
                series.LabelToolTip = dt.Rows[i]["mtName"].ToString();
                series.Legend = "Legend1";
                if (dt.Rows[i]["mtName"].ToString().Length > 4)
                {
                    series.LegendText = dt.Rows[i]["mtName"].ToString().Substring(0, 4) + "..";
                }
                series.LegendToolTip = dt.Rows[i]["mtName"].ToString();
                //设置颜色

                if (r < 2)
                {
                    series.MarkerColor = System.Drawing.Color.LightSkyBlue;
                }
                else if (r < 8)
                {
                    series.MarkerColor = System.Drawing.Color.Orange;
                }
                else if (r < 18)
                { series.MarkerColor = System.Drawing.Color.Red; }
                else
                { series.MarkerColor = System.Drawing.Color.Black; }

                series.MarkerSize = 10;
                series.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Circle;
                series.Name = dt.Rows[i]["mtName"].ToString();
                series.ToolTip = dt.Rows[i]["mtName"].ToString();
                series.ToolTip = "指标：#SERIESNAME\n影响力指数：#VALY\n供应风险：#VALX";
                series.YValuesPerPoint = 2;
                series.Points.DataBindXY(listX, listY);

                try
                {
                    this.chart1.Series.Add(series);

                }
                catch (Exception e)
                {
                    MessageUtil.ShowError("有重复的数据，请正确定义评估项！");
                }

            }
 
        }




        /// <summary>
        /// 初始化已经评估的图表
        /// </summary>
        private void chart_Load()
        {

            List<double> listY = new List<double>();
            List<double> listX = new List<double>();


            double r = 0;
            DataTable dt = riskAssessmentBll.getMtGroupFinished(purOrgName.Text);

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                Series series = this.chart1.Series[dt.Rows[i]["mtName"].ToString()];
                listX.Clear();
                listY.Clear();
                if (!dt.Rows[i]["riskScore"].ToString().Equals(""))
                {

                    listY.Add(float.Parse(dt.Rows[i]["influenceScore"].ToString()));
                    listX.Add(float.Parse(dt.Rows[i]["riskScore"].ToString()));
                    r = Math.Pow(listX[0], 2) + Math.Pow(listY[0], 2);
                }
                //设置颜色

                if (r < 2)
                {
                    series.MarkerColor = System.Drawing.Color.LightSkyBlue;
                }
                else if (r < 8)
                {
                    series.MarkerColor = System.Drawing.Color.Orange;
                }
                else if (r < 18)
                { series.MarkerColor = System.Drawing.Color.Red; }
                else
                { series.MarkerColor = System.Drawing.Color.Black; }
                //绑定数据
                series.Points.DataBindXY(listX, listY);
            }

        }

        /// <summary>
        /// 绘制参考线
        /// </summary>
        /// <param name="n"></param>
        /// <param name="chart"></param>
        /// <param name="r"></param>
        private void DrawCircl(int n, Chart chart, double[] r)
        {
            List<Double> y1 = new List<Double>();
            List<Double> x1 = new List<Double>();

            for (int m = 0; m < n; m++)
            {
                for (double i = 0.00; i <= r[m]; i = i + 0.01)
                {
                    for (double j = 0.00; j <= r[m]; j = j + 0.01)
                    {
                        i = Math.Round(i, 2);
                        j = Math.Round(j, 2);
                        if (r[m] - (i * i + j * j) <= 10e-5)
                        {
                            x1.Add(i);
                            y1.Add(j);
                            break;

                        }


                    }


                }
                this.chart1.Series[m].Points.DataBindXY(x1, y1);
                x1.Clear();
                y1.Clear();


            }


        }

        private void dataGridView1_RowStateChanged_1(object sender, DataGridViewRowStateChangedEventArgs e)
        {
            for (int i = 0; i < this.dgv_mtInfo.Rows.Count; i++)
            {
                DataGridViewRow r = this.dgv_mtInfo.Rows[i];
                r.HeaderCell.Value = string.Format("{0}", i + 1);
            }
           // this.dgv_mtInfo.Refresh();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            dgv_mtInfo.DataSource =riskAssessmentBll.mtGroupSearch(this.mtName.Text,this.purOrgName.Text);
        }

        private void button2_Click(object sender, EventArgs e)
        {
           /* List<float> listY = new List<float>();
            List<float> listX = new List<float>();
            double r = 0;
            int i=0;
            bool flag = false;
            DataTable dt = riskAssessmentBll.getMtGroupFinished(purOrgName.Text);
            Series series1 = this.chart1.Series["无影响"];
            Series series2 = this.chart1.Series["低影响"];
            Series series3 = this.chart1.Series["中影响"];
            this.chart1.Series.Clear();

            chart1.Series.Add(series1);
            chart1.Series.Add(series2);
            chart1.Series.Add(series3);

            for (int n = 0; n < dgv_mtInfo.Rows.Count; n++)
            {
                listX.Clear();
                listY.Clear();
                if ((bool)dgv_mtInfo.Rows[n].Cells["选择"].EditedFormattedValue == true)
                {
                    flag = false;
                    for (int m = 0; m < dt.Rows.Count; m++) {
                        if (dgv_mtInfo.Rows[n].Cells["物料组名称"].Value.ToString().Equals(dt.Rows[m]["mtName"].ToString())) {
                            i = m;
                            flag = true;
                            break;
                        }

                    }

                    if (flag)
                    {

                        if (!dt.Rows[i]["riskScore"].ToString().Equals(""))
                        {

                            listY.Add(float.Parse(dt.Rows[i]["influenceScore"].ToString()));
                            listX.Add(float.Parse(dt.Rows[i]["riskScore"].ToString()));
                            r = Math.Pow(listX[0], 2) + Math.Pow(listY[0], 2);
                        }


                        //求欧式距离


                        Series series = new Series();
                        series.ChartArea = "ChartArea1";
                        series.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Bubble;
                        series.IsValueShownAsLabel = false;
                        //series.Label = dt.Rows[i]["Aims"].ToString();
                        series.LabelToolTip = dt.Rows[i]["mtName"].ToString();
                        series.Legend = "Legend1";
                        if (dt.Rows[i]["mtName"].ToString().Length > 4)
                        {
                            series.LegendText = dt.Rows[i]["mtName"].ToString().Substring(0, 4) + "..";
                        }
                        series.LegendToolTip = dt.Rows[i]["mtName"].ToString();
                        //设置颜色

                        if (r < 2)
                        {
                            series.MarkerColor = System.Drawing.Color.LightSkyBlue;
                        }
                        else if (r < 8)
                        {
                            series.MarkerColor = System.Drawing.Color.Orange;
                        }
                        else if (r < 18)
                        { series.MarkerColor = System.Drawing.Color.Red; }
                        else
                        { series.MarkerColor = System.Drawing.Color.Black; }

                        series.MarkerSize = 10;
                        series.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Circle;
                        series.Name = dt.Rows[i]["mtName"].ToString();
                        series.ToolTip = dt.Rows[i]["mtName"].ToString();
                        series.ToolTip = "指标：#SERIESNAME\n影响力指数：#VALY\n供应风险：#VALX";
                        series.YValuesPerPoint = 2;
                        series.Points.DataBindXY(listX, listY);

                        try
                        {
                            this.chart1.Series.Add(series);

                        }
                        catch (Exception)
                        {
                            MessageUtil.ShowError("有重复的数据，请正确定义评估项！");
                        }

                    }
                    else {
                        MessageUtil.ShowError(dgv_mtInfo.Rows[n].Cells["物料组名称"].Value.ToString() + "尚未评估，请重新选择！");

                    }
                    


                   
                }
                
              

            }*/
        }

        private void button3_Click(object sender, EventArgs e)
        {
            RiskAssessment_Load(sender, e);
        }
        /// <summary>
        /// 采购组织下拉框改变，加载物料组数据
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void purOrgName_SelectedIndexChanged(object sender, EventArgs e)
        {
            mtName.DisplayMember = "MtGroup_Name";
            mtName.ValueMember = "MtGroup_ID";
            mtName.DataSource = riskAssessmentBll.getMtGroupInfo(purOrgName.SelectedValue.ToString());
        }

        private void mtName_SelectedIndexChanged(object sender, EventArgs e)
        {
            //查询所有物料的信息
            dgv_mtInfo.DataSource = riskAssessmentBll.getMtInfo(purOrgName.SelectedValue.ToString(), mtName.SelectedValue.ToString());

        }
    }
}
