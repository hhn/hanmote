﻿namespace MMClient.Material_group_positioning.GoalAndAim
{
    partial class AssertMtGroupGoal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgv_goal = new System.Windows.Forms.DataGridView();
            this.RowNum = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.domain = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.goalId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.supplierGoal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.goalValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Unit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.levelValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.level = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.createTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CB_MtGroupTypeName = new System.Windows.Forms.ComboBox();
            this.btn_Save = new System.Windows.Forms.Button();
            this.MtgroupName = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.pageNext1 = new pager.pagetool.pageNext();
            this.dgv_MtGroupInfo = new System.Windows.Forms.DataGridView();
            this.RowNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mtId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.pageNext2 = new pager.pagetool.pageNext();
            this.btnImportData = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_goal)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_MtGroupInfo)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgv_goal
            // 
            this.dgv_goal.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.dgv_goal.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_goal.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllHeaders;
            this.dgv_goal.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.dgv_goal.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgv_goal.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_goal.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.RowNum,
            this.domain,
            this.goalId,
            this.supplierGoal,
            this.goalValue,
            this.Unit,
            this.levelValue,
            this.level,
            this.createTime});
            this.dgv_goal.Location = new System.Drawing.Point(6, 20);
            this.dgv_goal.Name = "dgv_goal";
            this.dgv_goal.RowTemplate.Height = 23;
            this.dgv_goal.Size = new System.Drawing.Size(802, 730);
            this.dgv_goal.TabIndex = 0;
            this.dgv_goal.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_goal_CellValueChanged);
            // 
            // RowNum
            // 
            this.RowNum.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.RowNum.DataPropertyName = "RowNumber";
            this.RowNum.HeaderText = "序号";
            this.RowNum.Name = "RowNum";
            this.RowNum.ReadOnly = true;
            this.RowNum.Width = 54;
            // 
            // domain
            // 
            this.domain.DataPropertyName = "domain";
            this.domain.HeaderText = "领域";
            this.domain.Name = "domain";
            // 
            // goalId
            // 
            this.goalId.DataPropertyName = "GoalId";
            this.goalId.HeaderText = "供应目标编号";
            this.goalId.Name = "goalId";
            this.goalId.Visible = false;
            // 
            // supplierGoal
            // 
            this.supplierGoal.DataPropertyName = "GoalText";
            this.supplierGoal.HeaderText = "供应目标";
            this.supplierGoal.Name = "supplierGoal";
            // 
            // goalValue
            // 
            this.goalValue.DataPropertyName = "GoalValue";
            this.goalValue.HeaderText = "目标值";
            this.goalValue.Name = "goalValue";
            // 
            // Unit
            // 
            this.Unit.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Unit.DataPropertyName = "Unit";
            this.Unit.HeaderText = "单位";
            this.Unit.Name = "Unit";
            this.Unit.Width = 54;
            // 
            // levelValue
            // 
            this.levelValue.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.levelValue.DataPropertyName = "GoalLevelValue";
            this.levelValue.HeaderText = "等级值";
            this.levelValue.Name = "levelValue";
            this.levelValue.Width = 66;
            // 
            // level
            // 
            this.level.DataPropertyName = "GoalPipLevel";
            this.level.HeaderText = "pip等级";
            this.level.Name = "level";
            // 
            // createTime
            // 
            this.createTime.DataPropertyName = "createTime";
            this.createTime.HeaderText = "创建时间";
            this.createTime.Name = "createTime";
            this.createTime.ReadOnly = true;
            // 
            // CB_MtGroupTypeName
            // 
            this.CB_MtGroupTypeName.FormattingEnabled = true;
            this.CB_MtGroupTypeName.Location = new System.Drawing.Point(95, 12);
            this.CB_MtGroupTypeName.Name = "CB_MtGroupTypeName";
            this.CB_MtGroupTypeName.Size = new System.Drawing.Size(156, 20);
            this.CB_MtGroupTypeName.TabIndex = 2;
            // 
            // btn_Save
            // 
            this.btn_Save.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_Save.Location = new System.Drawing.Point(699, 760);
            this.btn_Save.Name = "btn_Save";
            this.btn_Save.Size = new System.Drawing.Size(75, 23);
            this.btn_Save.TabIndex = 3;
            this.btn_Save.Text = "保存";
            this.btn_Save.UseVisualStyleBackColor = true;
            this.btn_Save.Click += new System.EventHandler(this.btn_Save_Click);
            // 
            // MtgroupName
            // 
            this.MtgroupName.AutoSize = true;
            this.MtgroupName.Location = new System.Drawing.Point(12, 15);
            this.MtgroupName.Name = "MtgroupName";
            this.MtgroupName.Size = new System.Drawing.Size(77, 12);
            this.MtgroupName.TabIndex = 4;
            this.MtgroupName.Text = "物料组名称：";
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox1.Controls.Add(this.pageNext1);
            this.groupBox1.Controls.Add(this.dgv_MtGroupInfo);
            this.groupBox1.Location = new System.Drawing.Point(12, 38);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(661, 795);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "物料信息";
            // 
            // pageNext1
            // 
            this.pageNext1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.pageNext1.Location = new System.Drawing.Point(6, 752);
            this.pageNext1.Name = "pageNext1";
            this.pageNext1.PageIndex = 1;
            this.pageNext1.PageSize = 100;
            this.pageNext1.RecordCount = 0;
            this.pageNext1.Size = new System.Drawing.Size(644, 37);
            this.pageNext1.TabIndex = 1;
            this.pageNext1.OnPageChanged += new System.EventHandler(this.pageNext1_OnPageChanged);
            // 
            // dgv_MtGroupInfo
            // 
            this.dgv_MtGroupInfo.AllowUserToAddRows = false;
            this.dgv_MtGroupInfo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.dgv_MtGroupInfo.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_MtGroupInfo.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dgv_MtGroupInfo.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.dgv_MtGroupInfo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgv_MtGroupInfo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_MtGroupInfo.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.RowNumber,
            this.mtId,
            this.Column4,
            this.Column1});
            this.dgv_MtGroupInfo.Location = new System.Drawing.Point(7, 20);
            this.dgv_MtGroupInfo.Name = "dgv_MtGroupInfo";
            this.dgv_MtGroupInfo.RowTemplate.Height = 23;
            this.dgv_MtGroupInfo.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv_MtGroupInfo.Size = new System.Drawing.Size(643, 730);
            this.dgv_MtGroupInfo.TabIndex = 0;
            this.dgv_MtGroupInfo.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_MtGroupInof_CellClick);
            // 
            // RowNumber
            // 
            this.RowNumber.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.RowNumber.DataPropertyName = "RowNumber";
            this.RowNumber.HeaderText = "序号";
            this.RowNumber.Name = "RowNumber";
            this.RowNumber.ReadOnly = true;
            this.RowNumber.Width = 54;
            // 
            // mtId
            // 
            this.mtId.DataPropertyName = "Material_Group";
            this.mtId.HeaderText = "物料编号";
            this.mtId.Name = "mtId";
            this.mtId.ReadOnly = true;
            // 
            // Column4
            // 
            this.Column4.DataPropertyName = "Description";
            this.Column4.HeaderText = "物料名称";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "MaterialGroupType";
            this.Column1.HeaderText = "物料类型";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Visible = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox2.Controls.Add(this.pageNext2);
            this.groupBox2.Controls.Add(this.dgv_goal);
            this.groupBox2.Controls.Add(this.btn_Save);
            this.groupBox2.Location = new System.Drawing.Point(679, 38);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(814, 789);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "供应目标信息";
            // 
            // pageNext2
            // 
            this.pageNext2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.pageNext2.Location = new System.Drawing.Point(6, 754);
            this.pageNext2.Name = "pageNext2";
            this.pageNext2.PageIndex = 1;
            this.pageNext2.PageSize = 100;
            this.pageNext2.RecordCount = 0;
            this.pageNext2.Size = new System.Drawing.Size(644, 29);
            this.pageNext2.TabIndex = 2;
            this.pageNext2.OnPageChanged += new System.EventHandler(this.pageNext2_OnPageChanged);
            // 
            // btnImportData
            // 
            this.btnImportData.Location = new System.Drawing.Point(273, 10);
            this.btnImportData.Name = "btnImportData";
            this.btnImportData.Size = new System.Drawing.Size(113, 23);
            this.btnImportData.TabIndex = 7;
            this.btnImportData.Text = "导入数据";
            this.btnImportData.UseVisualStyleBackColor = true;
            this.btnImportData.Click += new System.EventHandler(this.btnImportData_Click);
            // 
            // AssertMtGroupGoal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1496, 845);
            this.Controls.Add(this.btnImportData);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.MtgroupName);
            this.Controls.Add(this.CB_MtGroupTypeName);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "AssertMtGroupGoal";
            this.Text = "非生产性物料";
            this.Load += new System.EventHandler(this.AssertMtGroupGoal_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_goal)).EndInit();
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_MtGroupInfo)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgv_goal;
        private System.Windows.Forms.ComboBox CB_MtGroupTypeName;
        private System.Windows.Forms.Button btn_Save;
        private System.Windows.Forms.Label MtgroupName;
        private System.Windows.Forms.GroupBox groupBox1;
        private pager.pagetool.pageNext pageNext1;
        private System.Windows.Forms.DataGridView dgv_MtGroupInfo;
        private System.Windows.Forms.GroupBox groupBox2;
        private pager.pagetool.pageNext pageNext2;
        private System.Windows.Forms.DataGridViewTextBoxColumn RowNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn mtId;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn RowNum;
        private System.Windows.Forms.DataGridViewTextBoxColumn domain;
        private System.Windows.Forms.DataGridViewTextBoxColumn goalId;
        private System.Windows.Forms.DataGridViewTextBoxColumn supplierGoal;
        private System.Windows.Forms.DataGridViewTextBoxColumn goalValue;
        private System.Windows.Forms.DataGridViewTextBoxColumn Unit;
        private System.Windows.Forms.DataGridViewTextBoxColumn levelValue;
        private System.Windows.Forms.DataGridViewTextBoxColumn level;
        private System.Windows.Forms.DataGridViewTextBoxColumn createTime;
        private System.Windows.Forms.Button btnImportData;
    }
}