﻿using Lib.Common.CommonUtils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MMClient.Material_group_positioning.GoalAndAim
{
    public partial class importExcleData : Form
    {

        public DataTable excelTbl;
        public importExcleData()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult dr1 = this.openFileDialog1.ShowDialog();
                if (DialogResult.OK == dr1) //判断是否选择文件  
                {
                    string path = this.openFileDialog1.FileName.Trim();
                    if (string.IsNullOrEmpty(path))
                    {
                        MessageBox.Show("请选择要导入的EXCEL文件！！！", "信息");
                        return;
                    }
                    if (!File.Exists(path))  //判断文件是否存在  
                    {
                        MessageBox.Show("信息", "找不到对应的Excel文件，请重新选择。");
                        return;
                    }
                    excelTbl = FileImportOrExport.ExcelToTable(path);

                    if (excelTbl.Rows.Count == 0)
                    {
                        MessageUtil.ShowError("文件中无数据");
                    }
                    dgv_dataShow.DataSource = excelTbl;
                }
            }
            catch (Exception ex)
            {

                MessageUtil.ShowError("读取文件失败！");
            }
           
        }
        private void loadData(DataTable excelTbl)
        {
            MessageUtil.ShowTips("父类数据");
        }

        private void saveBtn_Click(object sender, EventArgs e)
        {
            try
            {
                if (checkDgvData())
                {
                    loadData();
                }
            }
            catch (Exception ex)
            {

               MessageBox.Show("文件格式不符合要求");
            }
           
           
        }

        public virtual void loadData() {
            MessageUtil.ShowTips("父类数据");
        }

        public virtual void getModel()
        {
            MessageUtil.ShowTips("生成模板");
        }

        /// <summary>
        /// 生成模板代码
        /// </summary>
        /// <param name="cols">列名</param>
        /// <param name="fileName">文件名,带后缀</param>
        public void produceModel(List<string> cols,string fileName) {

            string path = choseFileStorePath()+"\\"+ fileName;
            DataTable table = new DataTable();
            string erroInfo = "模板生成成功!文件位置:"+path;
            foreach (string item in cols)
            {
                table.Columns.Add(item);
            }
            try
                {
                    FileImportOrExport.TableToExcel(table, path);
                    System.Diagnostics.Process.Start(path);
            }
                catch (Exception ex)
                {
                erroInfo = "模板生成失败!";
                }
            MessageUtil.ShowTips(erroInfo);

        }



        private bool checkDgvData()
        {
            if (this.excelTbl == null || this.excelTbl.Rows.Count == 0)
            {
                MessageUtil.ShowError("尚未导入数据！请先导入数据");
                return false;
            }
            else
            {

                return true;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                getModel();
            }
            catch (Exception ex)
            {

                MessageUtil.ShowError(ex.Message);
            }
            
        }


        public string choseFileStorePath() {
            FolderBrowserDialog dialog = new FolderBrowserDialog();
            dialog.Description = "请选择文件路径";
            string foldPath="C:\\";
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                 foldPath = dialog.SelectedPath;
            }

            return foldPath;

        }


    }
}
