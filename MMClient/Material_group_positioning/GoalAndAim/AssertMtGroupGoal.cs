﻿using Lib.Bll.Material_group_positioning;
using Lib.Common.CommonUtils;
using Lib.Model.MT_GroupModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MMClient.Material_group_positioning.GoalAndAim
{
    public partial class AssertMtGroupGoal : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        GoalAndAimBll goalAndAimBll = new GoalAndAimBll();
        ImportNoPrdData importNoPrdData;
        private int toalNum;

        public AssertMtGroupGoal()
        {
            InitializeComponent();
            this.pageNext1.PageSize = 20;
            this.pageNext2.PageSize = 20;
        }
        //下拉框改变
        private void CB_MtGroupName_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.pageNext2.PageIndex = 1;
            this.pageNext1.PageIndex = 1;
            //加载物料组数据
            pageNext1_Load();
            //加载目标数据
            pageNext2_Load();

        }
        /// <summary>
        /// 初始化数据
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AssertMtGroupGoal_Load(object sender, EventArgs e)
        {
            //加载物料组类型
           // CB_MtGroupTypeName.DataSource = goalAndAimBll.getMtGroupTypeId();
           //获取物料组信息
            CB_MtGroupTypeName.DataSource = goalAndAimBll.getMtGroupInfo();
            CB_MtGroupTypeName.ValueMember = "Id";
            CB_MtGroupTypeName.DisplayMember = "Value";
            //加载物料组数据
            pageNext1_Load();
            //加载目标数据
            pageNext2_Load();
            //激活下拉框change事件
            this.CB_MtGroupTypeName.SelectedIndexChanged += new System.EventHandler(this.CB_MtGroupName_SelectedIndexChanged);

        }
        /// <summary>
        /// 保存按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_Save_Click(object sender, EventArgs e)
        {
            //GroupGoalModel groupGoalModel = new GroupGoalModel();
            String mtGroupId = dgv_MtGroupInfo.CurrentRow.Cells["mtId"].Value.ToString();
            bool flag= goalAndAimBll.insertAssertAndMainInfo(dgv_goal,mtGroupId);
            string tips = "保存失败!";
            if (flag)
            {
                 tips = "保存成功！";
            }
            MessageUtil.ShowTips(tips);
            
        }
        //选中一行数据，更新右侧的数据
        private void dgv_MtGroupInof_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            //加载供应目标信息
            pageNext2_Load();
        }

        /// <summary>
        /// 加载物料组数据
        /// </summary>
        private void loadMaterialData() {
            //刷新物料组数据
            try
            {
                //dgv_MtGroupInfo.DataSource = goalAndAimBll.getAssertMaterial(CB_MtGroupTypeName.SelectedValue.ToString(),this.pageNext1.PageSize,this.pageNext1.PageIndex);
                //获取物料数据
                dgv_MtGroupInfo.DataSource = goalAndAimBll.getMaterialInfo(CB_MtGroupTypeName.SelectedValue.ToString(), this.pageNext1.PageSize, this.pageNext1.PageIndex);
                //默认选中第一行
                if(dgv_MtGroupInfo.Rows.Count!=0)
                     dgv_MtGroupInfo.Rows[0].Selected = true;
            }
            catch (Exception ex)
            {
                MessageUtil.ShowError("数据加载错误!");
            }
            
        }
        /// <summary>
        /// 加载指标信息
        /// </summary>
        private void loadIndicatorData()
        {
            string mtId = "";
            //如果选中为空，则不执行操作
            if (dgv_MtGroupInfo.CurrentRow == null)
            {
                return;
            }
            else
            {
                mtId = dgv_MtGroupInfo.CurrentRow.Cells["mtId"].Value.ToString();
                dgv_goal.AutoGenerateColumns = false;
                dgv_goal.DataSource = goalAndAimBll.getAssertGoal(mtId,this.pageNext2.PageSize,pageNext2.PageIndex);



            }
         
        }
        /// <summary>
        /// 加载物料组分页控件数据
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pageNext1_Load()
        {
            //toalNum = goalAndAimBll.getMtGroupCount(this.CB_MtGroupTypeName.SelectedValue.ToString());
            if (this.CB_MtGroupTypeName.SelectedValue == null||this.CB_MtGroupTypeName.Equals("")) {

                return;
            }
            toalNum = goalAndAimBll.getMtInfoCount(this.CB_MtGroupTypeName.SelectedValue.ToString());
            pageNext1.DrawControl(toalNum);
            pageNext1.OnPageChanged += pageNext1_OnPageChanged;
            loadMaterialData();


        }
        /// <summary>
        /// 加载分页控件数据
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pageNext2_Load()
        {
            string mtId = "";
            //如果选中为空，则不执行操作
            if (dgv_MtGroupInfo.CurrentRow == null)
            {
                return;
            }
            else {
                mtId = dgv_MtGroupInfo.CurrentRow.Cells["mtId"].Value.ToString();

            }
            pageNext2.DrawControl(goalAndAimBll.getIndicateCount(mtId));
            pageNext2.OnPageChanged += pageNext2_OnPageChanged;
            //加载数据
            loadIndicatorData();

        }
        /// <summary>
        /// 物料组列表翻页事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pageNext1_OnPageChanged(object sender, EventArgs e)
        {
            loadMaterialData();
        }
        /// <summary>
        /// 翻页事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pageNext2_OnPageChanged(object sender, EventArgs e)
        {
            loadIndicatorData();
        }

  
        private void dgv_goal_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {


            if (dgv_goal.Columns[e.ColumnIndex].Name.Equals("levelValue"))
            {
                DataGridViewCell current = dgv_goal.Rows[e.RowIndex].Cells[e.ColumnIndex];
                try
                {
                    string level = "N";
                    double value = Math.Floor(float.Parse(dgv_goal.CurrentCell.Value.ToString()));

                    switch (value)
                    {
                        case 0:level = "N";break;
                        case 1: level = "L"; break;
                        case 2: level = "M"; break;
                        case 3:level = "H";break;
                        default: { MessageUtil.ShowError("请输入0-4的数！");
                                level = "";
                                 };
                            break;
                    }
                    
                    dgv_goal.Rows[e.RowIndex].Cells["level"].Value = level;
                }
                catch (Exception)
                {

                    MessageUtil.ShowError("只能输入数值！");
                    
                }

            }
        }
        /// <summary>
        /// 导入excel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnImportData_Click(object sender, EventArgs e)
        {
            if (importNoPrdData == null|| importNoPrdData.IsDisposed){

                importNoPrdData = new ImportNoPrdData();
            }
            importNoPrdData.ShowDialog();
        }
    }
}
