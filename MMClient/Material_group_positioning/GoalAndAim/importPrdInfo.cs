﻿using Lib.Bll.Material_group_positioning;
using Lib.Common.CommonUtils;
using Lib.Model.PrdModel;
using Lib.SqlServerDAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MMClient.Material_group_positioning.GoalAndAim
{
    public partial class importPrdInfo : importExcleData
    {
        private PrdModel prdModel;
        private GoalAndAimBll goalAndAimBll=new GoalAndAimBll(); 
        public importPrdInfo()
        {
            InitializeComponent();
        }

        public override void getModel()
        {
            List<string> list = new List<string> { "产品编号", "产品名称","物料编号","物料名称" };
            string fileName = "产品数据导入模板.xlsx";
            base.produceModel(list,fileName);
        }


        /// <summary>
        ///重写数据
        /// </summary>
        public override void loadData()
        {
       
                string erroInfo = "";
                bool flag = true;
                if (prdModel == null)
                {
                    prdModel = new PrdModel();

                }
                foreach (DataRow row in this.excelTbl.Rows)
                {
                    prdModel.PrdId = row["产品编号"].ToString();
                    prdModel.PrdName = row["产品名称"].ToString();
                    prdModel.MaterialId = row["物料编号"].ToString();
                    prdModel.MaterialName = row["物料名称"].ToString();
                    //添加产品信息
                    if (!goalAndAimBll.insertPrdInfo(prdModel))
                    {
                        erroInfo += prdModel.PrdName + "保存失败\n";
                    }
                    //建立产品与物料的关系
                    if (!goalAndAimBll.insertPrdInfoAndMaterial(prdModel))
                    {
                        erroInfo += prdModel.MaterialId + "--" + prdModel.MaterialName + "不存在\n";
                    }
                }

                if (!erroInfo.Equals(""))
                {
                    if (MessageUtil.ShowYesNoAndError(erroInfo + "\n其他数据导入成功!是否继续") == DialogResult.No)
                    {

                        this.Close();

                    }
                   
                }
                else
                {
                    if (MessageUtil.ShowYesNoAndTips("保存成功！是否继续?") == DialogResult.No)
                    {

                        this.Close();

                    }
                }

      
        }
    }
}
