﻿using Lib.Bll.Material_group_positioning;
using Lib.Common.CommonUtils;
using Lib.Model.MT_GroupModel;
using Lib.Model.PrdModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MMClient.Material_group_positioning.GoalAndAim
{
    public partial class ImportNoPrdData : importExcleData
    {
        private GroupGoalModel prdIndicatorModel;
        private GoalAndAimBll goalAndAimBll=new GoalAndAimBll();

        public ImportNoPrdData()
        {
            InitializeComponent();
        }

        public override void getModel()
        {
            List<string> list = new List<string> { "目标编号", "目标内容", "目标值", "单位", "目标等级值", "目标等级", "物料编号", "领域" };
            string fileName = "非生产物料目标数据导入模板.xlsx";
            base.produceModel(list, fileName);
        }

        /// <summary>
        ///重写数据
        /// </summary>
        public override void loadData()
        {
            //todo:重写
            string erroInfo = "";
            bool flag = true;
            string tempPrdId = "";
            if (prdIndicatorModel == null)
            {
                prdIndicatorModel = new GroupGoalModel();

            }

            foreach (DataRow row in this.excelTbl.Rows)
            {
                prdIndicatorModel.GoalId1 = row[0].ToString();
                prdIndicatorModel.GoalText1 = row[1].ToString();
                prdIndicatorModel.GoalValue1 = float.Parse(row[2].ToString());
                prdIndicatorModel.Unit1 = row[3].ToString();
                prdIndicatorModel.GoalLevelValue1 = float.Parse(row[4].ToString());
                prdIndicatorModel.GoalPipLevel1 = row[5].ToString();
                prdIndicatorModel.MtGroupId1 = row[6].ToString();
                prdIndicatorModel.Domain = row[7].ToString();



                if (tempPrdId != prdIndicatorModel.GoalId1)
                {
                    if (goalAndAimBll.getMaterials(prdIndicatorModel.MtGroupId1))
                    {
                        tempPrdId = "";
                        //建立物料与目标关系
                        if (goalAndAimBll.insertAssertAndMainInfo(prdIndicatorModel) != 1)
                        {
                            erroInfo += prdIndicatorModel.GoalId1 + "保存失败\n";
                        }
                    }
                    else
                    {
                        erroInfo += prdIndicatorModel.MtGroupId1 + "物料不存在,请先录入该物料\n";
                        tempPrdId = prdIndicatorModel.MtGroupId1;
                    }

                }
            }
            if (!erroInfo.Equals(""))
            {
                if (MessageUtil.ShowYesNoAndError(erroInfo + "\n其他数据导入成功!是否继续") == DialogResult.No)
                {

                    this.Close();

                }
            }
            else
            {
                if (MessageUtil.ShowYesNoAndTips("保存成功！是否继续?") == DialogResult.No)
                {

                    this.Close();

                }
            }
        }


    }
}
