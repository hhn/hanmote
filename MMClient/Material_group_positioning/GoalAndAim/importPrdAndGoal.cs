﻿using Lib.Bll.Material_group_positioning;
using Lib.Common.CommonUtils;
using Lib.Model.PrdModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MMClient.Material_group_positioning.GoalAndAim
{
    public partial class importPrdAndGoal : importExcleData
    {
        private prdGoalModel prdGoalModel;
        private GoalAndAimBll goalAndAimBll = new GoalAndAimBll();

        public importPrdAndGoal()
        {
            InitializeComponent();
        }


        public override void getModel()
        {
            List<string> list = new List<string> { "产品编号", "产品目标", "产品目标等级", "产品目标值", "领域" };
            string fileName = "产品目标数据导入模板.xlsx";
            base.produceModel(list, fileName);
        }

        /// <summary>
        ///重写数据
        /// </summary>
        public override void loadData()
        {
            //todo:重写
            string erroInfo = "";
            bool flag = true;
            string tempPrdId = "";
            if (prdGoalModel == null)
            {
                prdGoalModel = new prdGoalModel();

            }

            foreach (DataRow row in this.excelTbl.Rows)
            {
                prdGoalModel.PrdId = row["产品编号"].ToString();
                prdGoalModel.GoalText = row["产品目标"].ToString();
                prdGoalModel.GoalPipLevel = row["产品目标等级"].ToString();
                prdGoalModel.GoalSocre = row["产品目标值"].ToString();
                prdGoalModel.Domain = row["领域"].ToString();
                prdGoalModel.CreateTime = DateTime.Now;

                if (tempPrdId != prdGoalModel.PrdId) {
                    if (goalAndAimBll.getPrdId(prdGoalModel.PrdId))
                    {
                        tempPrdId = "";
                        //建立产品与产品目标的关系
                        if (goalAndAimBll.insertGoalInfo(prdGoalModel) != 1)
                        {
                            erroInfo += prdGoalModel.GoalText + "保存失败\n";
                        }
                    }
                    else
                    {
                        erroInfo += prdGoalModel.PrdId + "产品不存在,请先录入该产品\n";
                        tempPrdId = prdGoalModel.PrdId;
                    }

                }
            }
            if (!erroInfo.Equals(""))
            {
                if (MessageUtil.ShowYesNoAndError(erroInfo + "\n其他数据导入成功!是否继续") == DialogResult.No)
                {

                    this.Close();

                }
            }
            else
            {
                if (MessageUtil.ShowYesNoAndTips("保存成功！是否继续?") == DialogResult.No)
                {

                    this.Close();

                }
            }
        }
    }
}
