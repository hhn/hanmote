﻿using Lib.Bll.Material_group_positioning;
using Lib.Common.CommonUtils;
using Lib.Common.MMCException.Bll;
using Lib.Model.PrdModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MMClient.Material_group_positioning.GoalAndAim
{
    public partial class PrdMtGroupGoals : WeifenLuo.WinFormsUI.Docking.DockContent
    {

        GoalAndAimBll goalAndAimBll = new GoalAndAimBll();
        private int toalNum;
        private int toalNum1;
        importPrdInfo importExcleData;
        private importPrdAndGoal importGoalAndIndicator;
        private importIndicatorData importIndicatorData;
        public PrdMtGroupGoals()
        {
            InitializeComponent();
            this.pageNext2.PageSize = 10;
            this.pageNext1.PageSize = 10;
        }
        //初始化数据
        private void PrdMtGroupGoals_Load(object sender, EventArgs e)
        {
            //获取产品信息
            CB_Prd.DataSource = goalAndAimBll.getPrdId();
            pageNext2_Load(sender, e);
        }
        //下拉框数据改变，列表数据改变
        private void CB_Prd_SelectedIndexChanged(object sender, EventArgs e)
        {
            //加载目标信息
            pageNext2_Load(sender, e);
            //加载产品名称
            LB_prdName.Text = goalAndAimBll.getPrdName(CB_Prd.Text);
            //加载物料数据
            pageNext1_Load(sender, e);

        }

        //读取产品目标信息
        private void LoadprdData(string prdId, int pageSize, int pageIndex) {

            try
            {
                if (pageIndex == 0) pageIndex = 1;
                DataTable dt = goalAndAimBll.getPrdAim(prdId, pageSize, pageIndex);
                dgv_Aim.DataSource = dt;
                //默认选中第一行
                this.dgv_Aim.Rows[0].Selected = true;
            }
            catch (BllException ex)
            {

                MessageUtil.ShowError(ex.Message);
            }
 
        }

        private void btnSaveGoals_Click(object sender, EventArgs e)
        {
            prdGoalModel prdGoalModel = new prdGoalModel();
            prdGoalModel.PrdId = CB_Prd.Text.Trim();
            
            int flag = 0;
            int n = 0;
            for (int i = 0; i < dgv_Aim.Rows.Count - 1; i++) {
        
                prdGoalModel.GoalId = dgv_Aim.Rows[i].Cells["supplierGoal"].Value.ToString().Trim();
                prdGoalModel.GoalText = dgv_Aim.Rows[i].Cells["goalText"].Value.ToString().Trim();
                prdGoalModel.GoalSocre = dgv_Aim.Rows[i].Cells["goalScore"].Value.ToString().Trim();
                prdGoalModel.GoalPipLevel = dgv_Aim.Rows[i].Cells["goalPipLevel"].Value.ToString().Trim();
                prdGoalModel.Domain = dgv_Aim.Rows[i].Cells["domain"].Value.ToString().Trim();

                //保存供应目标信息
                try
                {
                    flag = goalAndAimBll.insertGoalInfo(prdGoalModel);
                    n += flag;
                   if (flag ==0) {
                        MessageUtil.ShowTips(prdGoalModel.GoalId+"保存失败！");

                    }

                }
                catch (BllException ex)
                {
                    MessageUtil.ShowTips("保存失败！"+ex.Message);
                }
            }

            if (n == dgv_Aim.Rows.Count - 1)
            {
                MessageUtil.ShowTips("保存成功！");
                pageNext2_Load( sender,  e);
            }
            else
            {

                MessageUtil.ShowTips(prdGoalModel.GoalId + "部分保存成功！");
                pageNext2_Load(sender, e);
            }

        }
        /// <summary>
        /// 供应目标行点击事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgv_Aim_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            string goalId = dgv_Aim.CurrentRow.Cells["supplierGoal"].Value.ToString();
            //获取组成产品的物料信息及指标信息
            pageNext1_Load(sender, e);

           
        }
        /// <summary>
        /// 供应指标保存事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void indicatorSave_Click(object sender, EventArgs e)
        {

            PrdIndicatorModel prdIndicatorModel = new PrdIndicatorModel();
            int flag = 0;
            int n = 0;
            prdIndicatorModel.GoalId = dgv_Aim.CurrentRow.Cells["supplierGoal"].Value.ToString();
            string erroStr = "";
            if (prdIndicatorModel.GoalId.Equals("")) {
                MessageUtil.ShowTips("请选中要分解的供应目标！");

            }
            prdIndicatorModel.Domain = dgv_Aim.CurrentRow.Cells["domain"].Value.ToString();

            for (int i = 0; i < dgv_Material.Rows.Count - 1; i++) {
               prdIndicatorModel.IndicatorId = dgv_Material.Rows[i].Cells["indicatorId"].Value.ToString().Trim();
                prdIndicatorModel.IndicatorText = dgv_Material.Rows[i].Cells["indicatorText"].Value.ToString().Trim();
                prdIndicatorModel.IndicatorScore = float.Parse(dgv_Material.Rows[i].Cells["indicatorScore"].Value.ToString().Trim());
                prdIndicatorModel.MaterialId = dgv_Material.Rows[i].Cells["mtId"].Value.ToString().Trim();
                prdIndicatorModel.PipLevel = dgv_Material.Rows[i].Cells["mtPipLevel"].Value.ToString().Trim();
                prdIndicatorModel.Unit1 = dgv_Material.Rows[i].Cells["Unit"].Value.ToString().Trim();
                prdIndicatorModel.LevelValue = dgv_Material.Rows[i].Cells["levelValue"].Value.ToString().Trim();

                if (prdIndicatorModel.IndicatorText.Equals("")) {
                    continue;
                }

                //插入数据
                try
                {
                    
                    flag = goalAndAimBll.insertIndicatorInfo(prdIndicatorModel);
                    n += flag;
                    if (flag == 0) {
                        erroStr += " "+erroStr;
                    }
                   
                }
                catch (BllException ex)
                {

                    MessageUtil.ShowError("保存失败！");
                }
               
            }
            //如果更新数据条数与dgv行数相等，则全部更新成功，否在部分成功，或者失败
            if (n == dgv_Material.Rows.Count - 1)
            {
                MessageUtil.ShowTips("保存成功！");
                if (dgv_Aim.CurrentRow != null)
                {
                   string  goalId = dgv_Aim.CurrentRow.Cells["supplierGoal"].Value.ToString();
                    pageNext1_Load(sender,e);

                }
                
            }
            else
            {
                if (dgv_Aim.CurrentRow != null)
                {
                    string goalId = dgv_Aim.CurrentRow.Cells["supplierGoal"].Value.ToString();
                    pageNext1_Load(sender, e);

                }
                MessageUtil.ShowTips(erroStr+"保存失败！");
            }
        }
        /// <summary>
        /// 分页
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pageNext2_Load(object sender, EventArgs e)
        {
            toalNum = goalAndAimBll.getGoalCount(CB_Prd.Text);
            pageNext2.DrawControl(toalNum);
            pageNext2.OnPageChanged += pageNext2_OnPageChanged;
            LoadprdData(CB_Prd.Text, this.pageNext2.PageSize, this.pageNext2.PageIndex);

        }
        /// <summary>
        /// 分页
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pageNext1_Load(object sender, EventArgs e)
        {
            toalNum1 = goalAndAimBll.getIndicationCount(CB_Prd.Text);
            pageNext1.DrawControl(toalNum1);
            pageNext1.OnPageChanged += pageNext1_OnPageChanged;
            //加载数据
            loadIndicate();
        }
        /// <summary>
        /// 激活事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pageNext2_OnPageChanged(object sender, EventArgs e)
        {
            LoadprdData(CB_Prd.Text, this.pageNext2.PageSize, this.pageNext2.PageIndex); 
        }
     

        /// <summary>
        /// 激活事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pageNext1_OnPageChanged(object sender, EventArgs e)
        {
            loadIndicate();
            
           
        }

        /// <summary>
        /// 加载指标信息
        /// </summary>
        private void loadIndicate() {
            string goalId = "";

            if (!(dgv_Aim.CurrentRow==null))
                goalId = dgv_Aim.CurrentRow.Cells["supplierGoal"].Value.ToString();
            //获取组成产品的物料信息及指标信息
            if (this.pageNext1.PageIndex == 0)
            {
                this.pageNext1.PageIndex = 1;

            }
            try
            {
                dgv_Material.AutoGenerateColumns = false;
                dgv_Material.DataSource = goalAndAimBll.getMaterialInfo(CB_Prd.Text, goalId, this.pageNext1.PageSize, this.pageNext1.PageIndex);
            }
            catch (Exception ex)
            {

                MessageUtil.ShowError(ex.Message);
            }
            
        }

        private void dgv_Material_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
          
            if (dgv_Material.Columns[e.ColumnIndex].Name.Equals("levelValue")) {
                try
                {
                    float levelValue = float.Parse(dgv_Material.CurrentCell.Value.ToString());
                    if (levelValue > 4 || levelValue < 0)
                    {
                        MessageUtil.ShowError("请输入0-4之间的数"); return;
                    }
                    else if (levelValue >= 3)
                    {
                        dgv_Material.CurrentRow.Cells["mtPipLevel"].Value = "H";

                    }
                    else if (levelValue >= 2)
                    {
                        dgv_Material.CurrentRow.Cells["mtPipLevel"].Value = "M";

                    }
                    else if (levelValue >= 1)
                    {
                        dgv_Material.CurrentRow.Cells["mtPipLevel"].Value = "L";

                    }
                    else if (levelValue >= 0)
                    {
                        dgv_Material.CurrentRow.Cells["mtPipLevel"].Value = "N";

                    }
                }
                catch (Exception)
                {


                    MessageUtil.ShowError("打分只能为数字！");
                }
         
            }
        }

        private void 导入产品数据ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (importExcleData == null || importExcleData.IsDisposed)
            {
                importExcleData = new importPrdInfo();
            }
            importExcleData.ShowDialog();
        }

        private void 导入目标数据ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (importGoalAndIndicator == null || importGoalAndIndicator.IsDisposed)
            {
                importGoalAndIndicator = new importPrdAndGoal();
            }

            importGoalAndIndicator.ShowDialog();
        }

        private void 刷新ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PrdMtGroupGoals_Load( sender,e);
        }

        private void 导入指标信息ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (importIndicatorData == null || importIndicatorData.IsDisposed)
            {
                importIndicatorData = new importIndicatorData();
            }
            importIndicatorData.ShowDialog();
        }
    }
}
