﻿using Lib.Bll.Material_group_positioning;
using Lib.Common.CommonUtils;
using Lib.Model.PrdModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MMClient.Material_group_positioning.GoalAndAim
{
    public partial class importIndicatorData : importExcleData
    {
        private GoalAndAimBll goalAndAimBll = new GoalAndAimBll();
        private PrdIndicatorModel prdIndicatorModel;

        public importIndicatorData()
        {
            InitializeComponent();
        }

        public override void getModel()
        {
            List<string> list = new List<string> { "指标编号", "指标名称", "指标值", "指标单位", "等级值", "影响等级", "所属物料编号", "所属目标编号", "领域" };
            string fileName = "产品指标数据导入模板.xlsx";
            base.produceModel(list, fileName);
        }


        /// <summary>
        ///重写数据
        /// </summary>
        public override void loadData()
        {
            //todo:重写
            string erroInfo = "";
            bool flag = true;
            string tempPrdId = "";
            if (prdIndicatorModel == null)
            {
                prdIndicatorModel = new PrdIndicatorModel();

            }

            foreach (DataRow row in this.excelTbl.Rows)
            {
                prdIndicatorModel.IndicatorId = row["指标编号"].ToString();
                prdIndicatorModel.IndicatorText = row["指标名称"].ToString();
                prdIndicatorModel.PipLevel = row["影响等级"].ToString();
                prdIndicatorModel.IndicatorScore = float.Parse(row["指标值"].ToString());
                prdIndicatorModel.Domain = row["领域"].ToString();
                prdIndicatorModel.Unit1 = row["指标单位"].ToString();
                prdIndicatorModel.MaterialId= row["所属物料编号"].ToString();
                prdIndicatorModel.GoalId = row["所属目标编号"].ToString();
                prdIndicatorModel.LevelValue = row["等级值"].ToString();



                if (tempPrdId != prdIndicatorModel.GoalId)
                {
                    if (goalAndAimBll.getGoalId(prdIndicatorModel.GoalId))
                    {
                        tempPrdId = "";
                        //建立产品指标与产品目标的关系
                        if (goalAndAimBll.insertIndicatorInfo(prdIndicatorModel) != 1)
                        {
                            erroInfo += prdIndicatorModel.IndicatorText + "保存失败\n";
                        }
                    }
                    else
                    {
                        erroInfo += prdIndicatorModel.GoalId + "产品目标不存在,请先录入该目标\n";
                        tempPrdId = prdIndicatorModel.GoalId;
                    }

                }
            }
            if (!erroInfo.Equals(""))
            {
                if (MessageUtil.ShowYesNoAndError(erroInfo + "\n其他数据导入成功!是否继续") == DialogResult.No)
                {

                    this.Close();

                }
            }
            else
            {
                if (MessageUtil.ShowYesNoAndTips("保存成功！是否继续?") == DialogResult.No)
                {

                    this.Close();

                }
                else {
                    excelTbl.Clear();

                }
            }
        }


    }
}
