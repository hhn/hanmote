﻿namespace MMClient.Material_group_positioning.GoalAndAim
{
    partial class PrdMtGroupGoals
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dgv_Aim = new System.Windows.Forms.DataGridView();
            this.supplierGoal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.domain = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.goalText = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.goalScore = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.time = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.goalPipLevel = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pageNext2 = new pager.pagetool.pageNext();
            this.btnSaveGoals = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.pageNext1 = new pager.pagetool.pageNext();
            this.indicatorSave = new System.Windows.Forms.Button();
            this.dgv_Material = new System.Windows.Forms.DataGridView();
            this.mtId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mtName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.indicatorId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.indicatorText = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.indicatorScore = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Unit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.levelValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mtPipLevel = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CB_Prd = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.LB_prdName = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.数据导入ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.导入产品数据ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.导入目标数据ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.导入指标信息ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.刷新ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_Aim)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_Material)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox1.Controls.Add(this.dgv_Aim);
            this.groupBox1.Controls.Add(this.pageNext2);
            this.groupBox1.Controls.Add(this.btnSaveGoals);
            this.groupBox1.Location = new System.Drawing.Point(10, 70);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(746, 696);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "供应目标";
            // 
            // dgv_Aim
            // 
            this.dgv_Aim.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.dgv_Aim.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_Aim.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.dgv_Aim.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgv_Aim.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_Aim.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.supplierGoal,
            this.domain,
            this.goalText,
            this.goalScore,
            this.time,
            this.goalPipLevel});
            this.dgv_Aim.Location = new System.Drawing.Point(6, 20);
            this.dgv_Aim.Name = "dgv_Aim";
            this.dgv_Aim.RowTemplate.Height = 23;
            this.dgv_Aim.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv_Aim.Size = new System.Drawing.Size(734, 627);
            this.dgv_Aim.TabIndex = 0;
            this.dgv_Aim.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_Aim_CellClick);
            // 
            // supplierGoal
            // 
            this.supplierGoal.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.supplierGoal.DataPropertyName = "goalId";
            this.supplierGoal.FillWeight = 60.571F;
            this.supplierGoal.HeaderText = "供应目标编号";
            this.supplierGoal.Name = "supplierGoal";
            this.supplierGoal.Visible = false;
            // 
            // domain
            // 
            this.domain.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.domain.DataPropertyName = "domain";
            this.domain.HeaderText = "领域";
            this.domain.Name = "domain";
            this.domain.Width = 54;
            // 
            // goalText
            // 
            this.goalText.DataPropertyName = "goalText";
            this.goalText.FillWeight = 47.90615F;
            this.goalText.HeaderText = "供应目标";
            this.goalText.Name = "goalText";
            // 
            // goalScore
            // 
            this.goalScore.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.goalScore.DataPropertyName = "goalSocre";
            this.goalScore.HeaderText = "目标值";
            this.goalScore.Name = "goalScore";
            this.goalScore.Width = 66;
            // 
            // time
            // 
            this.time.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.time.DataPropertyName = "createTime";
            this.time.HeaderText = "创建时间";
            this.time.Name = "time";
            this.time.Width = 78;
            // 
            // goalPipLevel
            // 
            this.goalPipLevel.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.goalPipLevel.DataPropertyName = "pipLevel";
            this.goalPipLevel.HeaderText = "pip等级";
            this.goalPipLevel.Name = "goalPipLevel";
            this.goalPipLevel.Width = 72;
            // 
            // pageNext2
            // 
            this.pageNext2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.pageNext2.Location = new System.Drawing.Point(6, 653);
            this.pageNext2.Name = "pageNext2";
            this.pageNext2.PageIndex = 1;
            this.pageNext2.PageSize = 100;
            this.pageNext2.RecordCount = 0;
            this.pageNext2.Size = new System.Drawing.Size(653, 29);
            this.pageNext2.TabIndex = 3;
            this.pageNext2.OnPageChanged += new System.EventHandler(this.pageNext2_OnPageChanged);
            // 
            // btnSaveGoals
            // 
            this.btnSaveGoals.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSaveGoals.Location = new System.Drawing.Point(665, 659);
            this.btnSaveGoals.Name = "btnSaveGoals";
            this.btnSaveGoals.Size = new System.Drawing.Size(75, 23);
            this.btnSaveGoals.TabIndex = 1;
            this.btnSaveGoals.Text = "保存";
            this.btnSaveGoals.UseVisualStyleBackColor = true;
            this.btnSaveGoals.Click += new System.EventHandler(this.btnSaveGoals_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox2.Controls.Add(this.pageNext1);
            this.groupBox2.Controls.Add(this.indicatorSave);
            this.groupBox2.Controls.Add(this.dgv_Material);
            this.groupBox2.Location = new System.Drawing.Point(762, 70);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(734, 696);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "物料";
            // 
            // pageNext1
            // 
            this.pageNext1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.pageNext1.Location = new System.Drawing.Point(11, 653);
            this.pageNext1.Name = "pageNext1";
            this.pageNext1.PageIndex = 1;
            this.pageNext1.PageSize = 100;
            this.pageNext1.RecordCount = 0;
            this.pageNext1.Size = new System.Drawing.Size(655, 37);
            this.pageNext1.TabIndex = 2;
            this.pageNext1.OnPageChanged += new System.EventHandler(this.pageNext1_OnPageChanged);
            // 
            // indicatorSave
            // 
            this.indicatorSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.indicatorSave.Location = new System.Drawing.Point(672, 659);
            this.indicatorSave.Name = "indicatorSave";
            this.indicatorSave.Size = new System.Drawing.Size(56, 23);
            this.indicatorSave.TabIndex = 2;
            this.indicatorSave.Text = "保存";
            this.indicatorSave.UseVisualStyleBackColor = true;
            this.indicatorSave.Click += new System.EventHandler(this.indicatorSave_Click);
            // 
            // dgv_Material
            // 
            this.dgv_Material.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.dgv_Material.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_Material.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.dgv_Material.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgv_Material.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_Material.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.mtId,
            this.mtName,
            this.indicatorId,
            this.indicatorText,
            this.indicatorScore,
            this.Unit,
            this.levelValue,
            this.mtPipLevel});
            this.dgv_Material.Location = new System.Drawing.Point(11, 20);
            this.dgv_Material.Name = "dgv_Material";
            this.dgv_Material.RowTemplate.Height = 23;
            this.dgv_Material.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv_Material.Size = new System.Drawing.Size(717, 627);
            this.dgv_Material.TabIndex = 0;
            this.dgv_Material.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_Material_CellValueChanged);
            // 
            // mtId
            // 
            this.mtId.DataPropertyName = "Material_ID";
            this.mtId.HeaderText = "物料编号";
            this.mtId.Name = "mtId";
            this.mtId.ReadOnly = true;
            // 
            // mtName
            // 
            this.mtName.DataPropertyName = "Material_Name";
            this.mtName.HeaderText = "物料名称";
            this.mtName.Name = "mtName";
            this.mtName.ReadOnly = true;
            // 
            // indicatorId
            // 
            this.indicatorId.DataPropertyName = "indicatorId";
            this.indicatorId.HeaderText = "指标编号";
            this.indicatorId.Name = "indicatorId";
            this.indicatorId.Visible = false;
            // 
            // indicatorText
            // 
            this.indicatorText.DataPropertyName = "indicatorText";
            this.indicatorText.HeaderText = "供应指标";
            this.indicatorText.Name = "indicatorText";
            // 
            // indicatorScore
            // 
            this.indicatorScore.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.indicatorScore.DataPropertyName = "indicatorScore";
            this.indicatorScore.HeaderText = "目标值";
            this.indicatorScore.Name = "indicatorScore";
            this.indicatorScore.Width = 66;
            // 
            // Unit
            // 
            this.Unit.DataPropertyName = "Unit";
            this.Unit.HeaderText = "单位";
            this.Unit.Name = "Unit";
            // 
            // levelValue
            // 
            this.levelValue.DataPropertyName = "levelValue";
            this.levelValue.HeaderText = "等级值";
            this.levelValue.Name = "levelValue";
            // 
            // mtPipLevel
            // 
            this.mtPipLevel.DataPropertyName = "pipLevel";
            this.mtPipLevel.HeaderText = "pip等级";
            this.mtPipLevel.Name = "mtPipLevel";
            // 
            // CB_Prd
            // 
            this.CB_Prd.FormattingEnabled = true;
            this.CB_Prd.Items.AddRange(new object[] {
            "汽车",
            "轮船",
            "飞机",
            "火箭"});
            this.CB_Prd.Location = new System.Drawing.Point(73, 38);
            this.CB_Prd.Name = "CB_Prd";
            this.CB_Prd.Size = new System.Drawing.Size(140, 20);
            this.CB_Prd.TabIndex = 7;
            this.CB_Prd.SelectedIndexChanged += new System.EventHandler(this.CB_Prd_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 41);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 6;
            this.label1.Text = "产品名称";
            // 
            // LB_prdName
            // 
            this.LB_prdName.AutoSize = true;
            this.LB_prdName.Location = new System.Drawing.Point(231, 41);
            this.LB_prdName.Name = "LB_prdName";
            this.LB_prdName.Size = new System.Drawing.Size(41, 12);
            this.LB_prdName.TabIndex = 10;
            this.LB_prdName.Text = "label2";
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.数据导入ToolStripMenuItem,
            this.刷新ToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1508, 25);
            this.menuStrip1.TabIndex = 12;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // 数据导入ToolStripMenuItem
            // 
            this.数据导入ToolStripMenuItem.BackColor = System.Drawing.SystemColors.ControlDark;
            this.数据导入ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.导入产品数据ToolStripMenuItem,
            this.导入目标数据ToolStripMenuItem,
            this.导入指标信息ToolStripMenuItem});
            this.数据导入ToolStripMenuItem.Name = "数据导入ToolStripMenuItem";
            this.数据导入ToolStripMenuItem.Size = new System.Drawing.Size(68, 21);
            this.数据导入ToolStripMenuItem.Text = "数据导入";
            // 
            // 导入产品数据ToolStripMenuItem
            // 
            this.导入产品数据ToolStripMenuItem.Name = "导入产品数据ToolStripMenuItem";
            this.导入产品数据ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.导入产品数据ToolStripMenuItem.Text = "导入产品数据";
            this.导入产品数据ToolStripMenuItem.Click += new System.EventHandler(this.导入产品数据ToolStripMenuItem_Click);
            // 
            // 导入目标数据ToolStripMenuItem
            // 
            this.导入目标数据ToolStripMenuItem.Name = "导入目标数据ToolStripMenuItem";
            this.导入目标数据ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.导入目标数据ToolStripMenuItem.Text = "导入目标数据";
            this.导入目标数据ToolStripMenuItem.Click += new System.EventHandler(this.导入目标数据ToolStripMenuItem_Click);
            // 
            // 导入指标信息ToolStripMenuItem
            // 
            this.导入指标信息ToolStripMenuItem.Name = "导入指标信息ToolStripMenuItem";
            this.导入指标信息ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.导入指标信息ToolStripMenuItem.Text = "导入指标信息";
            this.导入指标信息ToolStripMenuItem.Click += new System.EventHandler(this.导入指标信息ToolStripMenuItem_Click);
            // 
            // 刷新ToolStripMenuItem
            // 
            this.刷新ToolStripMenuItem.BackColor = System.Drawing.SystemColors.ControlDark;
            this.刷新ToolStripMenuItem.Name = "刷新ToolStripMenuItem";
            this.刷新ToolStripMenuItem.Size = new System.Drawing.Size(44, 21);
            this.刷新ToolStripMenuItem.Text = "刷新";
            this.刷新ToolStripMenuItem.Click += new System.EventHandler(this.刷新ToolStripMenuItem_Click);
            // 
            // PrdMtGroupGoals
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1508, 768);
            this.Controls.Add(this.LB_prdName);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.CB_Prd);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.menuStrip1);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "PrdMtGroupGoals";
            this.Text = "生产性物料";
            this.Load += new System.EventHandler(this.PrdMtGroupGoals_Load);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_Aim)).EndInit();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_Material)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView dgv_Aim;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView dgv_Material;
        private System.Windows.Forms.ComboBox CB_Prd;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label LB_prdName;
        private System.Windows.Forms.Button btnSaveGoals;
        private System.Windows.Forms.Button indicatorSave;
        private pager.pagetool.pageNext pageNext2;
        private pager.pagetool.pageNext pageNext1;
        private System.Windows.Forms.DataGridViewTextBoxColumn mtId;
        private System.Windows.Forms.DataGridViewTextBoxColumn mtName;
        private System.Windows.Forms.DataGridViewTextBoxColumn indicatorId;
        private System.Windows.Forms.DataGridViewTextBoxColumn indicatorText;
        private System.Windows.Forms.DataGridViewTextBoxColumn indicatorScore;
        private System.Windows.Forms.DataGridViewTextBoxColumn Unit;
        private System.Windows.Forms.DataGridViewTextBoxColumn levelValue;
        private System.Windows.Forms.DataGridViewTextBoxColumn mtPipLevel;
        private System.Windows.Forms.DataGridViewTextBoxColumn supplierGoal;
        private System.Windows.Forms.DataGridViewTextBoxColumn domain;
        private System.Windows.Forms.DataGridViewTextBoxColumn goalText;
        private System.Windows.Forms.DataGridViewTextBoxColumn goalScore;
        private System.Windows.Forms.DataGridViewTextBoxColumn time;
        private System.Windows.Forms.DataGridViewTextBoxColumn goalPipLevel;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 数据导入ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 导入产品数据ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 导入目标数据ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 导入指标信息ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 刷新ToolStripMenuItem;
    }
}