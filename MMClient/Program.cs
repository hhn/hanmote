﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using MMClient.SystemConfig.UserManage;
using Lib.Bll.SourcingManage.SourcingManagement;
using MMClient.SystemConfig;
using MMClient.SourcingManage.SourcingManagement;
namespace MMClient
{

    static class Program
    {
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            //Application.Run(SingletonUserUI.getUserUI());
            
            LoginForm loginForm = new LoginForm();
            if (loginForm.ShowDialog() == DialogResult.OK)
            {
                Application.Run(SingletonUserUI.getUserUI());
            }
             
        }

    }
}
