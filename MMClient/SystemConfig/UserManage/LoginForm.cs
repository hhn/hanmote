﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
///116.196.99.18
using Lib.Common.CommonUtils;
using Lib.Bll.SystemConfig.UserManage;
using Lib.Model.SystemConfig;
using System.Media;
using Microsoft.VisualBasic.Devices;

namespace MMClient.SystemConfig.UserManage
{
    public partial class LoginForm : Form
    {
        public LoginForm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 检查登录信息是否填写完整
        /// </summary>
        /// <returns></returns>
        private bool checkIntegrity()
        {
            if (String.IsNullOrEmpty(this.user_name.Text))
            {
                MessageUtil.ShowWarning("用户名称不能为空，请填写！");
                return false;
            }

            if (String.IsNullOrEmpty(this.user_password.Text))
            {
                MessageUtil.ShowWarning("用户密码不能为空，请填写！");
                return false;
            }
/*
            if (this.user_type.SelectedIndex < 0 || String.IsNullOrEmpty(this.user_type.Text))
            {
                MessageUtil.ShowWarning("用户类型不能为空，请选择！");
                return false;
            }*/

            return true;
        }

        #region 按钮点击事件
        /// <summary>
        /// 登录按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void loginBtn_Click(object sender, EventArgs e)
        {
            //检查用户信息是否填写完整
            if (checkIntegrity() != true)
            {
                return;
            }

            //初始化用户登录信息
            BaseUserModel userModel = SingleUserInstance.getCurrentUserInstance();
            userModel.Login_Name = this.user_name.Text;
            userModel.Login_Password = this.user_password.Text;
            string a = this.user_name.Text.ToString().Trim()==""?"admin":this.user_name.Text.ToString().Trim().Substring(0, 2);
            if (a.Equals("ad")) userModel.Manager_Flag = 1;
            else userModel.Manager_Flag = 0;
           // userModel.Manager_Flag = this.user_type.SelectedIndex == 0 ? 0 : 1;
            //  0  -- "一般用户", 对应表Hanmote_Base_user Manager_Flag=0
            //  1  -- "管理员", 对应表Hanmote_Base_user Manager_Flag=1
            //验证用户身份信息
            LoginBLL loginBll = new LoginBLL();
            try
            {
                //用户登录成功
                if (loginBll.validateLoginInfor(userModel))
                {
                    this.DialogResult = DialogResult.OK;
                   
                }
                else
                {
                    //登录失败，重置用户信息
                    SingleUserInstance.clearBaseUserModel();
                    //提示登录失败
                    MessageUtil.ShowWarning("登录失败，请检查登录信息是否填写正确！");
                }
            }
            catch (Exception)
            {
                //重置用户信息
                SingleUserInstance.clearBaseUserModel();
                //显示提示错误
                MessageUtil.ShowError("连接服务器失败，请检查网络设置！");
            }
        }

        /// <summary>
        /// 重置按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void resetBtn_Click(object sender, EventArgs e)
        {
            resetLoginInfor();
        }

        /// <summary>
        /// 清除已填写、选择信息
        /// </summary>
        private void resetLoginInfor()
        {
            this.user_name.Clear();
            this.user_password.Clear();
      //      this.user_type.SelectedIndex = -1;
       //     this.user_type.Text = "";
        }
        #endregion

        private void user_name_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
