﻿using Lib.Bll.SystemConfig.UserManage;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MMClient.SystemConfig.UserManage
{
    public partial class DefinedUserInfoSelectMtGroup : Form
    {
        private SystemUserOrganicRelationshipBLL systemUserManagerBLL = new SystemUserOrganicRelationshipBLL();
        private int [] flag ;
        private string userID;
        public string MTname;//物料组
        private int type;// 1 评估员 其他：主审
        private AddSysUserForm addSysUserForm;
        string purGroupName;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="addSysUserForm"></param>
        /// <param name="userID"></param>
        /// <param name="Type"> 1 主审 2 评估员 </param>
        public DefinedUserInfoSelectMtGroup(AddSysUserForm addSysUserForm, string userID,int type)
        {
            this.addSysUserForm = addSysUserForm;
            flag = new int[10000000];
            this.userID = userID;
            this.MTname = "";
            this.type = type;
            InitializeComponent();
        }

        /// <summary>
        /// 加载物料组
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LoadPurGroupName(object sender, EventArgs e)
        {
            DataTable table = systemUserManagerBLL.findAllPurGroupName();
            this.PurGroupName.DataSource = table;
            this.PurGroupName.DisplayMember = "name";
            this.PurGroupName.ValueMember = "id";
            table.Rows.Add("all", "全部");
        }
        /// <summary>
        /// 改变值时刷新列表物料
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PurGroupName_SelectedIndexChanged(object sender, EventArgs e)
        {
            for (int i = 0; i < 100; i++) { flag[i] = 0; }
            // addSysUserForm.buttonMtGroup.Text = "";
            this.MtGroupTable.DataSource = null;
            purGroupName = this.PurGroupName.Text.ToString();
            if (purGroupName != null || !purGroupName.Equals(""))
            {
                DataTable table = systemUserManagerBLL.findAllMtGroupNameByPurGroupName(purGroupName,userID,type);
                this.MtGroupTable.AutoGenerateColumns = false;
                this.MtGroupTable.DataSource = table;
                    getFlagSelectedMtGroup(table);
            }
        }

        //根据当前用户已经负责的物料组、采购组织勾选出来
        private void getFlagSelectedMtGroup(DataTable table)
        {
            if(table!=null && table.Rows.Count > 0)
            {
                //根据当前用户取得它所负责的采购组织和物料组
                DataTable dt =  systemUserManagerBLL.getMainEvaulatorSelectdMtGroupAndPurGroupByUserID(userID,purGroupName);
                if (dt!=null && dt.Rows.Count > 0)
                {
                    string MtInfo ="物料组：";
                    for (int i = 0;i< dt.Rows.Count;i++)
                    {
                        //根据用户信息得到用户已选择的采购组织下的物料组填入
                        MtInfo += dt.Rows[i][0].ToString()+ " 、";
                        for (int j = 0; j < table.Rows.Count; j++)
                        {
                            string q = dt.Rows[i][1].ToString();
                            string r = table.Rows[j][2].ToString();
                            if (dt.Rows[i][1].ToString().Equals(table.Rows[j][2].ToString()) && dt.Rows[i][0].ToString().Equals(table.Rows[j][1].ToString()))
                            {
                                MtGroupTable.Rows[j].Cells[0].Value = true;
                            }
                        }
                    }
                    this.PurMtInfo.Text = MtInfo;
                }
                else
                {
                    this.PurMtInfo.Text = "该用户没有负责的物料组";
                }
            }
        }

        /// <summary>
        /// 选择
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MtGroupTable_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.RowIndex != -1 && !this.MtGroupTable.Rows[e.RowIndex].IsNewRow)
            {
                if (e.ColumnIndex == 0)
                {
                    if (flag[e.RowIndex] == 0)
                    {
                        this.MtGroupTable.Rows[e.RowIndex].Cells[0].Value = true;
                        flag[e.RowIndex] = 1;
                    }
                    else
                    {
                        this.MtGroupTable.Rows[e.RowIndex].Cells[0].Value = false;
                        flag[e.RowIndex] = 0;
                    }

                }

            }
            else
            {
                MessageBox.Show("当前无数据可操作");
            }
        }
        private bool isHave(int [] a)
        {
            for (int k = 0; k < this.MtGroupTable.Rows.Count; k++)
            {
                if ((bool)this.MtGroupTable.Rows[k].Cells[0].EditedFormattedValue == true)
                {
                    return true;
                }
            }
            return false;
        }
        private void button1_Click(object sender, EventArgs e)
        {
            MTname = "";
            String mtGroup="";
            if (isHave(flag))
            {
                for(int k = 0;k<this.MtGroupTable.Rows.Count;k++)
                {
                    if ((bool)this.MtGroupTable.Rows[k].Cells[0].EditedFormattedValue == true)
                    {
                        String name = this.MtGroupTable.Rows[k].Cells[2].Value.ToString();
                        String nameGroup = this.MtGroupTable.Rows[k].Cells[4].Value.ToString();
                        MTname += name + "、";
                        mtGroup += nameGroup + "、";
                    }
                }
                MessageBoxButtons messButton = MessageBoxButtons.OKCancel;
                DialogResult dr = MessageBox.Show("是否确认设置负责物料组为：" + MTname +"?", "警  告", messButton);
                if (dr == DialogResult.OK)
                {
                     this.PurMtInfo.Text = MTname;
                     this.label2.Text = "修改后：";
                     addSysUserForm.buttonMtGroup.Text = MTname;
                     addSysUserForm.PurName.Text = mtGroup;
                     addSysUserForm.MethodMtGroup.Text = MTname;
                }
               
            }
            else
            {
                MessageBox.Show("没有选择物料组");
            }
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
