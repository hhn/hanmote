﻿using System;

namespace MMClient.SystemConfig.UserManage
{
    partial class SystemUserManagerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SystemUserManagerForm));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.toolStripLabel3 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripLabel4 = new System.Windows.Forms.ToolStripLabel();
            this.topPanel = new System.Windows.Forms.Panel();
            this.footerPanel = new System.Windows.Forms.Panel();
            this.bindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorMovePreviousPage = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveNextPage = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.sumRecord = new System.Windows.Forms.ToolStripLabel();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.eachPageNum = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripLabel2 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.jumpToPageNum = new System.Windows.Forms.ToolStripTextBox();
            this.contentPanel = new System.Windows.Forms.Panel();
            this.SystemUserManager = new System.Windows.Forms.DataGridView();
            this.companyClass = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sysUserID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sysUserName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.JobPosition = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DepartName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PhoneNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Email = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sysUserStatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.操作 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.headerPanel = new System.Windows.Forms.Panel();
            this.delbtn = new System.Windows.Forms.Button();
            this.editUser = new System.Windows.Forms.Button();
            this.addUser = new System.Windows.Forms.Button();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.AuthoritedBtn = new System.Windows.Forms.Button();
            this.OrganicNameQe = new System.Windows.Forms.ComboBox();
            this.RoleNameQe = new System.Windows.Forms.ComboBox();
            this.companyClassQe = new System.Windows.Forms.ComboBox();
            this.ResetInfoList = new System.Windows.Forms.Button();
            this.UserNAmeQe = new System.Windows.Forms.TextBox();
            this.ReseachByCondition = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.Reset = new System.Windows.Forms.Button();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.topPanel.SuspendLayout();
            this.footerPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator)).BeginInit();
            this.bindingNavigator.SuspendLayout();
            this.contentPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SystemUserManager)).BeginInit();
            this.headerPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStripLabel3
            // 
            this.toolStripLabel3.Name = "toolStripLabel3";
            this.toolStripLabel3.Size = new System.Drawing.Size(56, 22);
            this.toolStripLabel3.Text = "跳转到：";
            // 
            // toolStripLabel4
            // 
            this.toolStripLabel4.Name = "toolStripLabel4";
            this.toolStripLabel4.Size = new System.Drawing.Size(20, 22);
            this.toolStripLabel4.Text = "页";
            // 
            // topPanel
            // 
            this.topPanel.Controls.Add(this.footerPanel);
            this.topPanel.Controls.Add(this.contentPanel);
            this.topPanel.Controls.Add(this.headerPanel);
            this.topPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.topPanel.Location = new System.Drawing.Point(0, 0);
            this.topPanel.Name = "topPanel";
            this.topPanel.Size = new System.Drawing.Size(1095, 506);
            this.topPanel.TabIndex = 45;
            // 
            // footerPanel
            // 
            this.footerPanel.Controls.Add(this.bindingNavigator);
            this.footerPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.footerPanel.Location = new System.Drawing.Point(0, 478);
            this.footerPanel.Name = "footerPanel";
            this.footerPanel.Size = new System.Drawing.Size(1095, 28);
            this.footerPanel.TabIndex = 2;
            // 
            // bindingNavigator
            // 
            this.bindingNavigator.AddNewItem = null;
            this.bindingNavigator.CountItem = this.bindingNavigatorCountItem;
            this.bindingNavigator.DeleteItem = null;
            this.bindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMovePreviousPage,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveNextPage,
            this.bindingNavigatorSeparator2,
            this.sumRecord,
            this.toolStripLabel1,
            this.eachPageNum,
            this.toolStripLabel2,
            this.toolStripSeparator1,
            this.toolStripLabel3,
            this.jumpToPageNum,
            this.toolStripLabel4});
            this.bindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.bindingNavigator.MoveFirstItem = this.bindingNavigatorMovePreviousPage;
            this.bindingNavigator.MoveLastItem = this.bindingNavigatorMoveNextPage;
            this.bindingNavigator.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.bindingNavigator.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.bindingNavigator.Name = "bindingNavigator";
            this.bindingNavigator.PositionItem = this.bindingNavigatorPositionItem;
            this.bindingNavigator.Size = new System.Drawing.Size(1095, 25);
            this.bindingNavigator.TabIndex = 2;
            this.bindingNavigator.Text = "bindingNavigator1";
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(32, 22);
            this.bindingNavigatorCountItem.Text = "/ {0}";
            this.bindingNavigatorCountItem.ToolTipText = "总项数";
            // 
            // bindingNavigatorMovePreviousPage
            // 
            this.bindingNavigatorMovePreviousPage.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousPage.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousPage.Image")));
            this.bindingNavigatorMovePreviousPage.Name = "bindingNavigatorMovePreviousPage";
            this.bindingNavigatorMovePreviousPage.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousPage.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMovePreviousPage.Text = "移到第一条记录";
            this.bindingNavigatorMovePreviousPage.ToolTipText = "移到前一页";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMovePreviousItem.Text = "移到上一条记录";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "位置";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "当前位置";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveNextItem.Text = "移到下一条记录";
            // 
            // bindingNavigatorMoveNextPage
            // 
            this.bindingNavigatorMoveNextPage.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextPage.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextPage.Image")));
            this.bindingNavigatorMoveNextPage.Name = "bindingNavigatorMoveNextPage";
            this.bindingNavigatorMoveNextPage.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextPage.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveNextPage.Text = "移到最后一条记录";
            this.bindingNavigatorMoveNextPage.ToolTipText = "移到下一页";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // sumRecord
            // 
            this.sumRecord.Name = "sumRecord";
            this.sumRecord.Size = new System.Drawing.Size(71, 22);
            this.sumRecord.Text = "共 0 条记录";
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(37, 22);
            this.toolStripLabel1.Text = "/每页";
            // 
            // eachPageNum
            // 
            this.eachPageNum.Name = "eachPageNum";
            this.eachPageNum.Size = new System.Drawing.Size(40, 25);
            this.eachPageNum.Text = "20";
            // 
            // toolStripLabel2
            // 
            this.toolStripLabel2.Name = "toolStripLabel2";
            this.toolStripLabel2.Size = new System.Drawing.Size(44, 22);
            this.toolStripLabel2.Text = "条记录";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // jumpToPageNum
            // 
            this.jumpToPageNum.Name = "jumpToPageNum";
            this.jumpToPageNum.Size = new System.Drawing.Size(50, 25);
            // 
            // contentPanel
            // 
            this.contentPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.contentPanel.Controls.Add(this.SystemUserManager);
            this.contentPanel.Location = new System.Drawing.Point(0, 124);
            this.contentPanel.Name = "contentPanel";
            this.contentPanel.Size = new System.Drawing.Size(1095, 351);
            this.contentPanel.TabIndex = 1;
            // 
            // SystemUserManager
            // 
            this.SystemUserManager.AllowUserToAddRows = false;
            this.SystemUserManager.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.SystemUserManager.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.SystemUserManager.BackgroundColor = System.Drawing.SystemColors.ControlLight;
            this.SystemUserManager.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.SystemUserManager.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.SystemUserManager.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.SystemUserManager.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.companyClass,
            this.sysUserID,
            this.sysUserName,
            this.JobPosition,
            this.DepartName,
            this.PhoneNumber,
            this.Email,
            this.sysUserStatus,
            this.操作});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.SystemUserManager.DefaultCellStyle = dataGridViewCellStyle3;
            this.SystemUserManager.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SystemUserManager.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.SystemUserManager.GridColor = System.Drawing.SystemColors.ControlLight;
            this.SystemUserManager.Location = new System.Drawing.Point(0, 0);
            this.SystemUserManager.Name = "SystemUserManager";
            this.SystemUserManager.RowTemplate.Height = 23;
            this.SystemUserManager.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.SystemUserManager.Size = new System.Drawing.Size(1095, 351);
            this.SystemUserManager.TabIndex = 3;
            this.SystemUserManager.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.SystemUserManager_CellContentClick_1);
            // 
            // companyClass
            // 
            this.companyClass.DataPropertyName = "所属公司";
            this.companyClass.HeaderText = "所属公司";
            this.companyClass.Name = "companyClass";
            // 
            // sysUserID
            // 
            this.sysUserID.DataPropertyName = "员工编号";
            this.sysUserID.HeaderText = "员工编号";
            this.sysUserID.Name = "sysUserID";
            // 
            // sysUserName
            // 
            this.sysUserName.DataPropertyName = "姓名";
            this.sysUserName.HeaderText = "姓名";
            this.sysUserName.Name = "sysUserName";
            // 
            // JobPosition
            // 
            this.JobPosition.DataPropertyName = "职务";
            this.JobPosition.HeaderText = "职务";
            this.JobPosition.Name = "JobPosition";
            // 
            // DepartName
            // 
            this.DepartName.DataPropertyName = "所属部门";
            this.DepartName.HeaderText = "所属部门";
            this.DepartName.Name = "DepartName";
            // 
            // PhoneNumber
            // 
            this.PhoneNumber.DataPropertyName = "联系方式";
            this.PhoneNumber.HeaderText = "联系方式";
            this.PhoneNumber.Name = "PhoneNumber";
            // 
            // Email
            // 
            this.Email.DataPropertyName = "邮箱";
            this.Email.HeaderText = "邮箱";
            this.Email.Name = "Email";
            // 
            // sysUserStatus
            // 
            this.sysUserStatus.DataPropertyName = "状态";
            this.sysUserStatus.HeaderText = "状态";
            this.sysUserStatus.Name = "sysUserStatus";
            // 
            // 操作
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.NullValue = "注销/激活";
            this.操作.DefaultCellStyle = dataGridViewCellStyle2;
            this.操作.HeaderText = "操作";
            this.操作.Name = "操作";
            // 
            // headerPanel
            // 
            this.headerPanel.Controls.Add(this.delbtn);
            this.headerPanel.Controls.Add(this.editUser);
            this.headerPanel.Controls.Add(this.addUser);
            this.headerPanel.Controls.Add(this.linkLabel1);
            this.headerPanel.Controls.Add(this.AuthoritedBtn);
            this.headerPanel.Controls.Add(this.OrganicNameQe);
            this.headerPanel.Controls.Add(this.RoleNameQe);
            this.headerPanel.Controls.Add(this.companyClassQe);
            this.headerPanel.Controls.Add(this.ResetInfoList);
            this.headerPanel.Controls.Add(this.UserNAmeQe);
            this.headerPanel.Controls.Add(this.ReseachByCondition);
            this.headerPanel.Controls.Add(this.label3);
            this.headerPanel.Controls.Add(this.label4);
            this.headerPanel.Controls.Add(this.label6);
            this.headerPanel.Controls.Add(this.label7);
            this.headerPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.headerPanel.Location = new System.Drawing.Point(0, 0);
            this.headerPanel.Name = "headerPanel";
            this.headerPanel.Size = new System.Drawing.Size(1095, 118);
            this.headerPanel.TabIndex = 0;
            // 
            // delbtn
            // 
            this.delbtn.Location = new System.Drawing.Point(517, 55);
            this.delbtn.Name = "delbtn";
            this.delbtn.Size = new System.Drawing.Size(91, 23);
            this.delbtn.TabIndex = 69;
            this.delbtn.Text = "删  除";
            this.delbtn.UseVisualStyleBackColor = true;
            this.delbtn.Click += new System.EventHandler(this.delbtn_Click);
            // 
            // editUser
            // 
            this.editUser.Location = new System.Drawing.Point(298, 55);
            this.editUser.Name = "editUser";
            this.editUser.Size = new System.Drawing.Size(91, 23);
            this.editUser.TabIndex = 68;
            this.editUser.Text = "查看/编辑";
            this.editUser.UseVisualStyleBackColor = true;
            this.editUser.Click += new System.EventHandler(this.editUser_Click);
            // 
            // addUser
            // 
            this.addUser.Location = new System.Drawing.Point(73, 55);
            this.addUser.Name = "addUser";
            this.addUser.Size = new System.Drawing.Size(91, 23);
            this.addUser.TabIndex = 67;
            this.addUser.Text = "新  建";
            this.addUser.UseVisualStyleBackColor = true;
            this.addUser.Click += new System.EventHandler(this.addUser_Click);
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Location = new System.Drawing.Point(171, 99);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(41, 12);
            this.linkLabel1.TabIndex = 66;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "点  我";
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // AuthoritedBtn
            // 
            this.AuthoritedBtn.Location = new System.Drawing.Point(74, 94);
            this.AuthoritedBtn.Name = "AuthoritedBtn";
            this.AuthoritedBtn.Size = new System.Drawing.Size(91, 23);
            this.AuthoritedBtn.TabIndex = 65;
            this.AuthoritedBtn.Text = "授  权";
            this.AuthoritedBtn.UseVisualStyleBackColor = true;
            this.AuthoritedBtn.Click += new System.EventHandler(this.AuthoritedBtn_Click);
            // 
            // OrganicNameQe
            // 
            this.OrganicNameQe.FormattingEnabled = true;
            this.OrganicNameQe.Location = new System.Drawing.Point(517, 19);
            this.OrganicNameQe.Name = "OrganicNameQe";
            this.OrganicNameQe.Size = new System.Drawing.Size(157, 20);
            this.OrganicNameQe.TabIndex = 64;
            // 
            // RoleNameQe
            // 
            this.RoleNameQe.FormattingEnabled = true;
            this.RoleNameQe.Location = new System.Drawing.Point(298, 18);
            this.RoleNameQe.Name = "RoleNameQe";
            this.RoleNameQe.Size = new System.Drawing.Size(137, 20);
            this.RoleNameQe.TabIndex = 63;
            // 
            // companyClassQe
            // 
            this.companyClassQe.FormattingEnabled = true;
            this.companyClassQe.Location = new System.Drawing.Point(74, 17);
            this.companyClassQe.Name = "companyClassQe";
            this.companyClassQe.Size = new System.Drawing.Size(139, 20);
            this.companyClassQe.TabIndex = 62;
            this.companyClassQe.SelectedIndexChanged += new System.EventHandler(this.companyClassQe_SelectedIndexChanged);
            // 
            // ResetInfoList
            // 
            this.ResetInfoList.Location = new System.Drawing.Point(961, 55);
            this.ResetInfoList.Name = "ResetInfoList";
            this.ResetInfoList.Size = new System.Drawing.Size(91, 23);
            this.ResetInfoList.TabIndex = 61;
            this.ResetInfoList.Text = "重  置";
            this.ResetInfoList.UseVisualStyleBackColor = true;
            this.ResetInfoList.Click += new System.EventHandler(this.ResetInfoList_Click);
            // 
            // UserNAmeQe
            // 
            this.UserNAmeQe.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.UserNAmeQe.Location = new System.Drawing.Point(733, 16);
            this.UserNAmeQe.Name = "UserNAmeQe";
            this.UserNAmeQe.Size = new System.Drawing.Size(104, 23);
            this.UserNAmeQe.TabIndex = 59;
            // 
            // ReseachByCondition
            // 
            this.ReseachByCondition.Location = new System.Drawing.Point(833, 55);
            this.ReseachByCondition.Name = "ReseachByCondition";
            this.ReseachByCondition.Size = new System.Drawing.Size(91, 23);
            this.ReseachByCondition.TabIndex = 57;
            this.ReseachByCondition.Text = "查  询";
            this.ReseachByCondition.UseVisualStyleBackColor = true;
            this.ReseachByCondition.Click += new System.EventHandler(this.ReseachByCondition_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.label3.Location = new System.Drawing.Point(453, 20);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 17);
            this.label3.TabIndex = 56;
            this.label3.Text = "所属部门：";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.label4.Location = new System.Drawing.Point(680, 21);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 17);
            this.label4.TabIndex = 55;
            this.label4.Text = "姓  名：";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.label6.Location = new System.Drawing.Point(245, 19);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(52, 17);
            this.label6.TabIndex = 54;
            this.label6.Text = "职  务：";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.label7.Location = new System.Drawing.Point(5, 19);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(68, 17);
            this.label7.TabIndex = 52;
            this.label7.Text = "公司名称：";
            // 
            // Reset
            // 
            this.Reset.Location = new System.Drawing.Point(613, 25);
            this.Reset.Name = "Reset";
            this.Reset.Size = new System.Drawing.Size(83, 23);
            this.Reset.TabIndex = 44;
            this.Reset.Text = "重  置";
            this.Reset.UseVisualStyleBackColor = true;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "SupplierId";
            this.dataGridViewTextBoxColumn1.HeaderText = "供应商编号";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Width = 102;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "companyName";
            this.dataGridViewTextBoxColumn2.HeaderText = "公司名称";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.Width = 102;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "Country";
            this.dataGridViewTextBoxColumn3.HeaderText = "区  域";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.Width = 102;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "ContactName";
            this.dataGridViewTextBoxColumn4.HeaderText = "联系人";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.Width = 102;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "PhoneNumber";
            this.dataGridViewTextBoxColumn5.HeaderText = "联系方式";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.Width = 102;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "ContactMail";
            this.dataGridViewTextBoxColumn6.HeaderText = "电子邮箱";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.Width = 102;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "MtGroupName";
            this.dataGridViewTextBoxColumn7.HeaderText = "物料组名称";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.Width = 102;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.HeaderText = "操    作";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.Width = 103;
            // 
            // SystemUserManagerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1095, 506);
            this.Controls.Add(this.topPanel);
            this.Controls.Add(this.Reset);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "SystemUserManagerForm";
            this.Text = "员工信息列表";
            this.Load += new System.EventHandler(this.SysManager_Load);
            this.topPanel.ResumeLayout(false);
            this.footerPanel.ResumeLayout(false);
            this.footerPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator)).EndInit();
            this.bindingNavigator.ResumeLayout(false);
            this.bindingNavigator.PerformLayout();
            this.contentPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SystemUserManager)).EndInit();
            this.headerPanel.ResumeLayout(false);
            this.headerPanel.PerformLayout();
            this.ResumeLayout(false);

        }
        #endregion

        private System.Windows.Forms.ToolStripLabel toolStripLabel3;
        private System.Windows.Forms.ToolStripLabel toolStripLabel4;
        private System.Windows.Forms.Panel topPanel;
        private System.Windows.Forms.Panel footerPanel;
        private System.Windows.Forms.BindingNavigator bindingNavigator;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousPage;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextPage;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.ToolStripLabel sumRecord;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripTextBox eachPageNum;
        private System.Windows.Forms.ToolStripLabel toolStripLabel2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripTextBox jumpToPageNum;
        private System.Windows.Forms.Panel contentPanel;
        private System.Windows.Forms.Panel headerPanel;
        private System.Windows.Forms.Button ResetInfoList;
        private System.Windows.Forms.TextBox UserNAmeQe;
        private System.Windows.Forms.Button ReseachByCondition;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button Reset;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.ComboBox RoleNameQe;
        private System.Windows.Forms.ComboBox companyClassQe;
        private System.Windows.Forms.ComboBox OrganicNameQe;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.Button AuthoritedBtn;
        private System.Windows.Forms.DataGridView SystemUserManager;
        private System.Windows.Forms.Button delbtn;
        private System.Windows.Forms.Button editUser;
        private System.Windows.Forms.Button addUser;
        private System.Windows.Forms.DataGridViewTextBoxColumn companyClass;
        private System.Windows.Forms.DataGridViewTextBoxColumn sysUserID;
        private System.Windows.Forms.DataGridViewTextBoxColumn sysUserName;
        private System.Windows.Forms.DataGridViewTextBoxColumn JobPosition;
        private System.Windows.Forms.DataGridViewTextBoxColumn DepartName;
        private System.Windows.Forms.DataGridViewTextBoxColumn PhoneNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn Email;
        private System.Windows.Forms.DataGridViewTextBoxColumn sysUserStatus;
        private System.Windows.Forms.DataGridViewButtonColumn 操作;
    }
}