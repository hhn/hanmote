﻿namespace MMClient.SystemConfig.UserManage
{
    partial class UserSearchConditionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserSearchConditionForm));
            this.userID = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.userName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.birthPlace = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.gender = new System.Windows.Forms.ComboBox();
            this.idNumber = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.telephone = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.okBtn = new System.Windows.Forms.Button();
            this.cancelBtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // userID
            // 
            this.userID.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.userID.Location = new System.Drawing.Point(114, 45);
            this.userID.Name = "userID";
            this.userID.Size = new System.Drawing.Size(164, 23);
            this.userID.TabIndex = 3;
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.label1.Location = new System.Drawing.Point(47, 47);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(68, 17);
            this.label1.TabIndex = 2;
            this.label1.Text = "用户编号：";
            // 
            // userName
            // 
            this.userName.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.userName.Location = new System.Drawing.Point(400, 45);
            this.userName.Name = "userName";
            this.userName.Size = new System.Drawing.Size(164, 23);
            this.userName.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.label2.Location = new System.Drawing.Point(333, 47);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(68, 17);
            this.label2.TabIndex = 4;
            this.label2.Text = "用户姓名：";
            // 
            // birthPlace
            // 
            this.birthPlace.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.birthPlace.Location = new System.Drawing.Point(114, 127);
            this.birthPlace.Name = "birthPlace";
            this.birthPlace.Size = new System.Drawing.Size(164, 23);
            this.birthPlace.TabIndex = 21;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.label13.Location = new System.Drawing.Point(47, 130);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(44, 17);
            this.label13.TabIndex = 27;
            this.label13.Text = "籍贯：";
            // 
            // gender
            // 
            this.gender.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.gender.FormattingEnabled = true;
            this.gender.Items.AddRange(new object[] {
            "男",
            "女"});
            this.gender.Location = new System.Drawing.Point(114, 86);
            this.gender.Name = "gender";
            this.gender.Size = new System.Drawing.Size(164, 25);
            this.gender.TabIndex = 18;
            // 
            // idNumber
            // 
            this.idNumber.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.idNumber.Location = new System.Drawing.Point(400, 127);
            this.idNumber.Name = "idNumber";
            this.idNumber.Size = new System.Drawing.Size(164, 23);
            this.idNumber.TabIndex = 22;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.label9.Location = new System.Drawing.Point(333, 130);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(68, 17);
            this.label9.TabIndex = 25;
            this.label9.Text = "身份证号：";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.label10.Location = new System.Drawing.Point(47, 89);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(44, 17);
            this.label10.TabIndex = 24;
            this.label10.Text = "性别：";
            // 
            // telephone
            // 
            this.telephone.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.telephone.Location = new System.Drawing.Point(400, 86);
            this.telephone.Name = "telephone";
            this.telephone.Size = new System.Drawing.Size(164, 23);
            this.telephone.TabIndex = 23;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.label5.Location = new System.Drawing.Point(333, 89);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(68, 17);
            this.label5.TabIndex = 19;
            this.label5.Text = "联系方式：";
            // 
            // okBtn
            // 
            this.okBtn.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.okBtn.Location = new System.Drawing.Point(223, 183);
            this.okBtn.Name = "okBtn";
            this.okBtn.Size = new System.Drawing.Size(75, 26);
            this.okBtn.TabIndex = 28;
            this.okBtn.Text = "确定";
            this.okBtn.UseVisualStyleBackColor = true;
            this.okBtn.Click += new System.EventHandler(this.okBtn_Click);
            // 
            // cancelBtn
            // 
            this.cancelBtn.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.cancelBtn.Location = new System.Drawing.Point(322, 183);
            this.cancelBtn.Name = "cancelBtn";
            this.cancelBtn.Size = new System.Drawing.Size(75, 26);
            this.cancelBtn.TabIndex = 29;
            this.cancelBtn.Text = "取消";
            this.cancelBtn.UseVisualStyleBackColor = true;
            this.cancelBtn.Click += new System.EventHandler(this.cancelBtn_Click);
            // 
            // UserSearchConditionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(636, 244);
            this.Controls.Add(this.cancelBtn);
            this.Controls.Add(this.okBtn);
            this.Controls.Add(this.birthPlace);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.gender);
            this.Controls.Add(this.idNumber);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.telephone);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.userName);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.userID);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "UserSearchConditionForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "查询条件";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox userID;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox userName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox birthPlace;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox gender;
        private System.Windows.Forms.TextBox idNumber;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox telephone;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button okBtn;
        private System.Windows.Forms.Button cancelBtn;
    }
}