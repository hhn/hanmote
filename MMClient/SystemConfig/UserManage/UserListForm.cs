﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Lib.Model.SystemConfig;
using Lib.Bll.SystemConfig.UserManage;
using Lib.Common.CommonUtils;
using MMClient.SystemConfig.PermissionManage;
using WeifenLuo.WinFormsUI.Docking;

namespace MMClient.SystemConfig.UserManage
{
    public partial class UserListForm :DockContent
    {
        private const int DEFAULT_PAGE_SIZE = 20;

        //每页显示的记录条数
        private int pageSize;

        //总记录条数
        private int sumSize;

        //总页数
        private int pageCount;

        //当前页号
        private int currentPage;

        //当前页最后一条记录在总记录中位置
        private int currentRow;

        //存储查询结果
        private DataTable userInforTable;

        //条件查询窗口对象
        private UserSearchConditionForm searchCondition;

        public UserListForm()
        {
            InitializeComponent();
            init();
            fillUserTable();
        }

        /// <summary>
        /// 初始化成员默认值
        /// </summary>
        private void init()
        {
            this.pageSize = DEFAULT_PAGE_SIZE;
            this.sumSize = 0;
            this.pageCount = 0;
            this.currentPage = 0;
            this.currentRow = 0;
        }

        /// <summary>
        /// 从数据库获取数据，填充用户表格
        /// </summary>
        public void fillUserTable()
        {
            UserListBLL userListBLL = new UserListBLL();
            //查询当前页大小的记录
            userInforTable = userListBLL.getUserList(this.pageSize,0);

            this.userTable.AutoGenerateColumns = false;
            this.userTable.DataSource = userInforTable;

            //记录总数
            this.sumSize = userListBLL.calculateUserNumber();
        }
        /// <summary>
        /// 从条件获取数据，填充用户表格
        /// </summary>
        public void fillCompanyTableByCompanyName(String companyName)
        {
            CompanyUserBLL userListBLL = new CompanyUserBLL();
            //查询当前页大小的记录
            userInforTable = userListBLL.getCompanyListByCondition(companyName, this.pageSize, 0);

            this.userTable.AutoGenerateColumns = false;
            this.userTable.DataSource = userInforTable;
            //记录总数
            this.sumSize = userListBLL.calculateUserNumber();
        }
        /// <summary>
        /// 查询当前选中的行号
        /// </summary>
        /// <returns></returns>
        private int getCurrentSelectedRowIndex()
        {
            if (this.userTable.Rows.Count <= 0)
            {
                MessageUtil.ShowWarning("用户表为空，无法执行操作");
                return -1;
            }

            if (this.userTable.CurrentRow.Index < 0)
            {
                MessageUtil.ShowWarning("请选择一行记录，然后进行操作！");
                return -1;
            }

            return this.userTable.CurrentRow.Index;
        }

        /// <summary>
        /// 根据查询条件查询用户
        /// </summary>
        /// <param name="userModel"></param>
        public void selectUserBySearchConditions(BaseUserModel userModel)
        {
            UserListBLL userListBll = new UserListBLL();

            try
            {
                this.userTable.AutoGenerateColumns = false;
                this.userTable.DataSource = userListBll.getUserListBySearchCondition(userModel);
            }
            catch(Exception e)
            {
                MessageUtil.ShowError(e.Message);
            }
        }

        #region 按钮点击事件
        /// <summary>
        /// 条件查询
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void searchOnConditionBtn_Click(object sender, EventArgs e)
        {
            ///条件查询
            String companyName = this.companyName.Text;
            fillCompanyTableByCompanyName(companyName);

        }
       
        /// <summary>
        /// 用户授权
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void authorizeBtn_Click(object sender, EventArgs e)
        {
            int currentIndex = getCurrentSelectedRowIndex();
            if (currentIndex == -1)
            {
                return;
            }

            //被授权用户Model
            DataGridViewRow row  = this.userTable.Rows[currentIndex];
            BaseUserModel.Supplier_Id = Convert.ToString(row.Cells["Supplier_Id"].Value);
            BaseUserModel.Supplier_Name = Convert.ToString(row.Cells["Supplier_Name"].Value);
            CompanyUserModel.Supplier_Id = Convert.ToString(row.Cells["Supplier_Id"].Value);
            CompanyUserModel.Supplier_Name = Convert.ToString(row.Cells["Supplier_Name"].Value);
            CompanyUserForm companyUserForm = new CompanyUserForm(CompanyUserModel.Supplier_Id, CompanyUserModel.Supplier_Name);
            companyUserForm.fillCompanyTable(CompanyUserModel.Supplier_Id);
            companyUserForm.ShowDialog(this);
           
        }
        #endregion

        #region bindingNavigator导航事件
        /// <summary>
        /// 前一页
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bindingNavigatorMovePreviousPage_Click(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// 前一条记录
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bindingNavigatorMovePreviousItem_Click(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// 后一条记录
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bindingNavigatorMoveNextItem_Click(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// 后一页
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bindingNavigatorMoveNextPage_Click(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// 检查所按下的键是否为 回车键
        /// </summary>
        /// <param name="e"></param>
        /// <returns></returns>
        private bool checkEnterKeyPress(KeyPressEventArgs e)
        {
            return (e.KeyChar == 13);
        }

        /// <summary>
        /// 当前第多少条记录
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bindingNavigatorPositionItem_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!checkEnterKeyPress(e))
            {
                return;
            }

            
        }

        /// <summary>
        /// 每页显示多少条记录
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void eachPageNum_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!checkEnterKeyPress(e))
            {
                return;
            }

            
        }

        /// <summary>
        /// 跳转到第多少页
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void jumpToPageNum_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!checkEnterKeyPress(e))
            {
                return;
            }

            
        }
        #endregion

        private void Reset_Click(object sender, EventArgs e)
        {
            this.companyName.Text = "";
            fillUserTable();
        }
    }
}
