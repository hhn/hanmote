﻿using Lib.Bll.SystemConfig.UserManage;
using Lib.Common.CommonUtils;
using MMClient.MD.GN;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MMClient.SystemConfig.UserManage
{
    public partial class UserInfoListForm : Form
    {
         private const int DEFAULT_PAGE_SIZE = 20;

        //每页显示的记录条数
        private int pageSize;

        //总记录条数
        private int sumSize;

        //总页数
        private int pageCount;

        //当前页号
        private int currentPage;

        //当前页最后一条记录在总记录中位置
        private int currentRow;

        //存储查询结果
        private DataTable userInforTable;
        private SystemUserOrganicRelationshipBLL systemUserOrganicRelationshipBLL = new SystemUserOrganicRelationshipBLL();
        private string userName;
        private string departName;
        public string name;
        public string id;
        private int index;
        private AddSysUserForm addSysUserForm;
        private NewPurAndMtGroup newPurAndMtGroup;
        private int type;
        public UserInfoListForm(AddSysUserForm addSysUserForm)
        {
            InitializeComponent();
            fillUserSelectedTable();
            this.addSysUserForm = addSysUserForm;
        }
        public UserInfoListForm(int type, NewPurAndMtGroup newPurAndMtGroup,int index)
        {
            InitializeComponent();
            this.type = type;
            this.index = index;
            this.newPurAndMtGroup = newPurAndMtGroup;
            fillUserSelectedTable();
        }

        /// <summary>
        /// 确认选择
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button3_Click(object sender, EventArgs e)
        {
            int currentIndex = getCurrentSelectedRowIndex();
            if(currentIndex>=0)
            {
                DataGridViewRow row = this.UserSelectedTable.Rows[currentIndex];
                String ID = Convert.ToString(row.Cells["sysUserID"].Value);
                String namestr = Convert.ToString(row.Cells["sysUserName"].Value);
                this.id = ID;
                this.name = namestr;
                if(type==1)
                {
                    newPurAndMtGroup.textBox1.Text = ID;
                    newPurAndMtGroup.textBox2.Text = namestr;
                    newPurAndMtGroup.MtGroupDiviedTable.Rows[index].Cells["isDivied"].Value = "新指定负责人：[" + namestr + "]";
                }

                else
                {
                    addSysUserForm.buttonUser.Text = namestr;
                    addSysUserForm.upUserID.Text = ID;
                }
                this.Close();
            }
            else
            {
                MessageBox.Show("无直接上级可选");
            }
        }

        private void fillUserSelectedTable()
        {
            userInforTable = systemUserOrganicRelationshipBLL.findAllSystemUserOrganicRelationshipInfo(15, 0,type);
            this.UserSelectedTable.AutoGenerateColumns = false;
            this.UserSelectedTable.DataSource = userInforTable;
            //记录总数
            this.sumSize = systemUserOrganicRelationshipBLL.calculateUserNumber();
        }
        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            if (this.textBoxDepart.Text == null || this.textBoxDepart.Text.ToString().Equals(""))
            {
                if(this.textBoxName.Text==null || this.textBoxName.Text.ToString().Equals(""))
                {
                    //fillUserSelectedTable();
                    return;
                }
            }
            this.userName = this.textBoxName.Text.ToString();
            this.departName = this.textBoxDepart.Text.ToString();
            userInforTable  =  systemUserOrganicRelationshipBLL.findAllSystemUserOrganicRelationshipInfoByCondition(this.userName,this.departName,type); 
            this.UserSelectedTable.AutoGenerateColumns = false;
            this.UserSelectedTable.DataSource = userInforTable;
            //记录总数
            this.sumSize = systemUserOrganicRelationshipBLL.calculateUserNumber();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.textBoxName.Text = "";
            this.textBoxDepart.Text = "";
            fillUserSelectedTable();
        }
        /// <summary>
        /// 查询当前选中的行号
        /// </summary>
        /// <returns></returns>
        private int getCurrentSelectedRowIndex()
        {
            if (this.UserSelectedTable.Rows.Count <= 0)
            {
                MessageUtil.ShowWarning("用户表为空，无法执行操作");
                return -1;
            }

            if (this.UserSelectedTable.CurrentRow.Index < 0)
            {
                MessageUtil.ShowWarning("请选择一行记录，然后进行操作！");
                return -1;
            }

            return this.UserSelectedTable.CurrentRow.Index;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
