﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Lib.Common.CommonUtils;
using Lib.Model.SystemConfig;
using Lib.Bll.SystemConfig.UserManage;

namespace MMClient.SystemConfig.UserManage
{
    public partial class GroupListForm : Form
    {
        public GroupListForm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 获取用户所有的组信息
        /// </summary>
        /// <param name="userId"></param>
        public void setGroupByUserId(String userId)
        {
            GroupListBLL groupListBLL = new GroupListBLL();
            JoinUserGroupModel joinUserGroupModel = new JoinUserGroupModel();
            joinUserGroupModel.User_ID = userId;

            try
            {
                this.groupTable.AutoGenerateColumns = false;
                this.groupTable.DataSource = groupListBLL.queryGroupByUserId(joinUserGroupModel);
            }
            catch (Exception)
            {
                MessageUtil.ShowError("获取用户组信息失败！");
                this.Close();
            }
        }
    }
}
