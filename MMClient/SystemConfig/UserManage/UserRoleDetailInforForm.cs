﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Lib.Bll.SystemConfig.UserManage;
using Lib.Common.CommonUtils;
using Lib.Model.SystemConfig;

namespace MMClient.SystemConfig.UserManage
{
    public partial class UserRoleDetailInforForm : Form
    {
        private int operateFlag;    //操作标志，0表示新建；1表示编辑

        public UserRoleDetailInforForm(int operateFlag,String roleId="")
        {
            InitializeComponent();

            if (operateFlag == 0)
            {
                initUserRoleId();
            }
            else if (operateFlag == 1)
            {
                initUserRoleInfor(roleId);
            }
        }

        /// <summary>
        /// 初始化角色编号
        /// </summary>
        private void initUserRoleId()
        {
            try
            {
                UserRoleDetailInforBLL userRoleDetail = new UserRoleDetailInforBLL();
                this.userRoleId.Text = userRoleDetail.getBiggestRoleId();
            }
            catch (Exception e)
            {
                MessageUtil.ShowError(e.Message);
                this.Close();
            }
        }

        /// <summary>
        /// 根据数据库信息初始化角色
        /// </summary>
        private void initUserRoleInfor(String roleId)
        {
            try
            {
                BaseRoleModel baseRoleModel = new BaseRoleModel();
                baseRoleModel.Role_ID = roleId;

                UserRoleDetailInforBLL userRoleDetail = new UserRoleDetailInforBLL();
                userRoleDetail.queryRoleInforById(baseRoleModel);

                this.userRoleId.Text = baseRoleModel.Role_ID;
                this.userRoleName.Text = baseRoleModel.Role_Name;
                this.userRoleDescription.Text = baseRoleModel.Description;
            }
            catch (Exception e)
            {
                MessageUtil.ShowError(e.Message);
                this.Close();
            }
        }

        /// <summary>
        /// 检查填写信息的完整性
        /// </summary>
        /// <returns></returns>
        private bool checkIntegrity()
        {
            if (String.IsNullOrEmpty(this.userRoleId.Text))
            {
                MessageUtil.ShowWarning("角色编号不能为空，无法提交！");
                return false;
            }

            if (String.IsNullOrEmpty(this.userRoleName.Text))
            {
                MessageUtil.ShowWarning("角色名称不能为空，请填写！");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 确定
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void okBtn_Click(object sender, EventArgs e)
        {
            if (!checkIntegrity())
            {
                return;
            }
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        /// <summary>
        /// 取消
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cancelBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #region 外部调用接口
        /// <summary>
        /// 获取角色编号
        /// </summary>
        /// <returns></returns>
        public String getUserRoleId()
        {
            return this.userRoleId.Text;
        }

        /// <summary>
        /// 获取角色名称
        /// </summary>
        /// <returns></returns>
        public String getUserRoleName()
        {
            return this.userRoleName.Text;
        }

        /// <summary>
        /// 获取角色描述信息
        /// </summary>
        /// <returns></returns>
        public String getUserDescription()
        {
            return this.userRoleDescription.Text;
        }
        #endregion

    }
}
