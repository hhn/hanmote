﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Lib.Model.SystemConfig;

namespace MMClient.SystemConfig.UserManage
{
    public partial class UserSearchConditionForm : Form
    {
        private UserListForm parentForm;
        private CompanyUserForm parentCompanyForm;
        public UserSearchConditionForm(UserListForm parentForm)
        {
            InitializeComponent();

            this.parentForm = parentForm;
        }

        public UserSearchConditionForm(CompanyUserForm companyUserForm)
        {
            InitializeComponent();

            this.parentCompanyForm = companyUserForm;
        }

        #region 按钮点击事件
        /// <summary>
        /// 确定查询
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void okBtn_Click(object sender, EventArgs e)
        {
            if (parentForm != null && !parentForm.IsDisposed)
            {
                BaseUserModel userModel = new BaseUserModel();
                userModel.User_ID = this.userID.Text;
                userModel.Username = this.userName.Text;
                userModel.Gender = this.gender.SelectedIndex;
               
                userModel.Birth_Place = this.birthPlace.Text;
                userModel.ID_Number = this.idNumber.Text;
                userModel.Mobile = this.telephone.Text;

                parentForm.selectUserBySearchConditions(userModel);
            }
            if (parentCompanyForm != null && !parentCompanyForm.IsDisposed)
            {
                BaseUserModel userModel = new BaseUserModel();
                userModel.User_ID = this.userID.Text;
                userModel.Username = this.userName.Text;
                userModel.Gender = this.gender.SelectedIndex;
               
                userModel.Birth_Place = this.birthPlace.Text;
                userModel.ID_Number = this.idNumber.Text;
                userModel.Mobile = this.telephone.Text;
                parentCompanyForm.selectUserBySearchConditions(userModel);
            }
        }

        /// <summary>
        /// 取消
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cancelBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion
    }
}
