﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Lib.Model.SystemConfig;
using Lib.Bll.SystemConfig.UserManage;
using Lib.Common.CommonUtils;
using MMClient.SystemConfig.PermissionManage;

namespace MMClient.SystemConfig.UserManage
{
    public partial class CompanyUserForm : Form
    {
        private const int DEFAULT_PAGE_SIZE = 20;

        //每页显示的记录条数
        private int pageSize;

        //总记录条数
        private int sumSize;

        //总页数
        private int pageCount;

        //当前页号
        private int currentPage;

        //当前页最后一条记录在总记录中位置
        private int currentRow;

        //存储查询结果
        private DataTable userInforTable;

        //条件查询窗口对象
        private UserSearchConditionForm searchCondition;

        private String Supplier_Id; //公司编号

        private String Supplier_Name;//公司名称


        public CompanyUserForm(String Supplier_Id,String Supplier_Name)
        {
            this.Supplier_Name = Supplier_Name;
            this.Supplier_Id = Supplier_Id;
            InitializeComponent();
            this.Text = Supplier_Name + "---员工信息列表";

        }

        public CompanyUserForm()
        {
        }

        /// <summary>
        /// 初始化成员默认值
        /// </summary>
        private void init()
        {
            this.pageSize = DEFAULT_PAGE_SIZE;
            this.sumSize = 0;
            this.pageCount = 0;
            this.currentPage = 0;
            this.currentRow = 0;
        }

        /// <summary>
        /// 从数据库获取数据，填充用户表格
        /// </summary>
        public void fillCompanyTable(String companyID)
        {
            this.Supplier_Id = companyID;
            CompanyUserBLL userListBLL = new CompanyUserBLL();
            //查询当前页大小的记录
            userInforTable = userListBLL.getCompanyUserList(companyID,this.pageSize, 0);

            this.userTableCompanyUser.AutoGenerateColumns = false;
            this.userTableCompanyUser.DataSource = userInforTable;
            //记录总数
            this.sumSize = userListBLL.calculateUserNumber();
        }

       

        /// <summary>
        /// 从数据库获取数据，填充用户表格
        /// </summary>
        public void fillCompanyUserTable()
        {
            CompanyUserBLL userListBLL = new CompanyUserBLL();
            //查询当前页大小的记录
            userInforTable = userListBLL.getCompanyUserList(Supplier_Id, this.pageSize, 0);

            this.userTableCompanyUser.AutoGenerateColumns = false;
            this.userTableCompanyUser.DataSource = userInforTable;

            //记录总数
            this.sumSize = userListBLL.calculateUserNumber();
        }

        internal void selectUserBySearchConditions(BaseUserModel userModel)
        {
            UserListBLL userListBll = new UserListBLL();

            try
            {
                this.userTableCompanyUser.AutoGenerateColumns = false;
                this.userTableCompanyUser.DataSource = userListBll.getUserListBySearchCondition(userModel);
            }
            catch (Exception e)
            {
                MessageUtil.ShowError(e.Message);
            }
        }

        /// <summary>
        /// 查询当前选中的行号
        /// </summary>
        /// <returns></returns>
        private int getCurrentSelectedRowIndex()
        {
            if (this.userTableCompanyUser.Rows.Count <= 0)
            {
                MessageUtil.ShowWarning("用户表为空，无法执行操作");
                return -1;
            }

            if (this.userTableCompanyUser.CurrentRow.Index < 0)
            {
                MessageUtil.ShowWarning("请选择一行记录，然后进行操作！");
                return -1;
            }

            return this.userTableCompanyUser.CurrentRow.Index;
        }

        private void authorizeBtnCompanyUser_Click(object sender, EventArgs e)
        {
            int currentIndex = getCurrentSelectedRowIndex();
            if (currentIndex == -1)
            {
                return;
            }

            //被授权用户Model
            DataGridViewRow row = this.userTableCompanyUser.Rows[currentIndex];
            BaseUserModel baseUserModel = new BaseUserModel();
            baseUserModel.User_ID = Convert.ToString(row.Cells["User_ID"].Value);
            baseUserModel.Username = Convert.ToString(row.Cells["Username"].Value);
            FunctionLevelPermissionManageForm functionLevelPermissionManage = new FunctionLevelPermissionManageForm(baseUserModel, 1);
            functionLevelPermissionManage.ShowDialog(this);

        }

        private void removeUserInforBtnCompanyUser_Click(object sender, EventArgs e)
        {
            int currentIndex = getCurrentSelectedRowIndex();
            if (currentIndex == -1)
            {
                return;
            }

            if (DialogResult.OK == MessageUtil.ShowOKCancelAndQuestion("确定删除当前用户？"))
            {
                //后续操作
                String User_ID = Convert.ToString(this.userTableCompanyUser.Rows[currentIndex].Cells["User_ID"].Value);
                CompanyUserBLL userListBLL = new CompanyUserBLL();
                userListBLL.delCompanyUserInfo(Supplier_Id, User_ID);
                fillCompanyUserTable();
            }
        }
        /// <summary>
        /// 
        /// 新建员工信息
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void createUserBtnCompanyUser_Click(object sender, EventArgs e)
        {
            UserDetailInforForm userDetailInfor = new UserDetailInforForm(Supplier_Id,Supplier_Name,1);

            if (userDetailInfor.ShowDialog(this) == DialogResult.OK)
            {
                fillCompanyUserTable();
            }
        }
        /// <summary>
        /// 编辑员工信息
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void editUserInforBtnCompanyUser_Click(object sender, EventArgs e)
        {
            int currentIndex = getCurrentSelectedRowIndex();
            if (currentIndex == -1)
            {
                return;
            }

            String userId = Convert.ToString(this.userTableCompanyUser.Rows[currentIndex].Cells["User_ID"].Value);
         //   String username = Convert.ToString(this.userTableCompanyUser.Rows[currentIndex].Cells["UserName"].Value);
            UserDetailInforForm userDetailInfor = new UserDetailInforForm(userId, this.Supplier_Id,2);
            userDetailInfor.initEditingUserinfor(userId, Supplier_Id);
            if (userDetailInfor.ShowDialog(this) == DialogResult.OK)
            {
                fillCompanyUserTable();
            }
        }

        private void searchOnConditionBtnCompanyUser_Click(object sender, EventArgs e)
        {
            if (searchCondition == null || searchCondition.IsDisposed)
            {
                searchCondition = new UserSearchConditionForm(this);
            }
            searchCondition.Show();
        }

        private void Reset_Click(object sender, EventArgs e)
        {
            fillCompanyUserTable();
        }
    }
}
