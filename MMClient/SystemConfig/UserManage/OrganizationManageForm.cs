﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Lib.Model.SystemConfig;
using Lib.Bll.SystemConfig.UserManage;
using Lib.Common.CommonUtils;

namespace MMClient.SystemConfig.UserManage
{
    public partial class OrganizationManageForm : Form
    {
        class NodeStruct
        {
            public String organizationID;
            public String organizationName;
            public int leftValue;
            public int rightValue;
            public int layer;
        }

        //点击的节点
        private TreeNode clickedNode;

        private int rowIndex;

        public OrganizationManageForm()
        {
            InitializeComponent();
            initOrganizationTreeView();
        }

        /// <summary>
        /// 查找树根节点
        /// </summary>
        /// <param name="organizationInforTable"></param>
        /// <returns></returns>
        private NodeStruct findRootStruct(DataTable organizationInforTable)
        {
            NodeStruct rootNode = null;
            foreach (DataRow row in organizationInforTable.Rows)
            {
                int layer = Convert.ToInt32(row["Level"]);
                if (layer == 0)
                {
                    rootNode = new NodeStruct();
                    rootNode.organizationID = Convert.ToString(row["Organization_ID"]);
                    rootNode.organizationName = Convert.ToString(row["Organization_Name"]);
                    rootNode.leftValue = Convert.ToInt32(row["Left_Value"]);
                    rootNode.rightValue = Convert.ToInt32(row["Right_Value"]);
                    rootNode.layer = Convert.ToInt32(row["Level"]);
                    break;
                }
            }
            return rootNode;
        }

        /// <summary>
        /// 查询某节点的孩子节点
        /// </summary>
        /// <param name="organizationInforTable"></param>
        /// <param name="childCacheNodeList"></param>
        /// <param name="parentNode"></param>
        private void selectChildStruct(DataTable organizationInforTable,
                                                              LinkedList<NodeStruct> childCacheNodeList,
                                                              NodeStruct parentNode)
        {
            //清空孩子节点缓存队列
            childCacheNodeList.Clear();

            //遍历DataTable，查找parentNode的孩子节点
            foreach (DataRow row in organizationInforTable.Rows)
            {
                int leftValue = Convert.ToInt32(row["Left_Value"]);
                int rightValue = Convert.ToInt32(row["Right_Value"]);
                int layer = Convert.ToInt32(row["Level"]);
                if (parentNode.layer == layer - 1 && parentNode.leftValue < leftValue && parentNode.rightValue > rightValue)
                {
                    bool addedFlag = false;

                    LinkedListNode<NodeStruct> firstNode = childCacheNodeList.First;
                    LinkedListNode<NodeStruct> lastNode = childCacheNodeList.Last;
                    LinkedListNode<NodeStruct> tmpNode = firstNode;

                    while (tmpNode != lastNode)
                    {
                        NodeStruct nodeValue = tmpNode.Value;
                        if (leftValue < nodeValue.leftValue && rightValue < nodeValue.rightValue)
                        {
                            NodeStruct newNode = new NodeStruct();
                            newNode.organizationID = Convert.ToString(row["Organization_ID"]);
                            newNode.organizationName = Convert.ToString(row["Organization_Name"]);
                            newNode.leftValue = leftValue;
                            newNode.rightValue = rightValue;
                            newNode.layer = layer;
                            childCacheNodeList.AddBefore(tmpNode, newNode);
                            addedFlag = true;
                            break;
                        }
                        tmpNode = tmpNode.Next;
                    }
                    //判断末端节点
                    if (!addedFlag && tmpNode != null)
                    {
                        NodeStruct nodeValue = tmpNode.Value;
                        if (leftValue < nodeValue.leftValue && rightValue < nodeValue.rightValue)
                        {
                            NodeStruct newNode = new NodeStruct();
                            newNode.organizationID = Convert.ToString(row["Organization_ID"]);
                            newNode.organizationName = Convert.ToString(row["Organization_Name"]);
                            newNode.leftValue = leftValue;
                            newNode.rightValue = rightValue;
                            newNode.layer = layer;
                            childCacheNodeList.AddBefore(tmpNode, newNode);
                            addedFlag = true;
                        }
                    }
                    //前面都不满足则插入链表尾部
                    if (!addedFlag)
                    {
                        NodeStruct newNode = new NodeStruct();
                        newNode.organizationID = Convert.ToString(row["Organization_ID"]);
                        newNode.organizationName = Convert.ToString(row["Organization_Name"]);
                        newNode.leftValue = leftValue;
                        newNode.rightValue = rightValue;
                        newNode.layer = layer;
                        childCacheNodeList.AddLast(newNode);
                    }
                }
            }
        }

        /// <summary>
        /// 创建树节点
        /// </summary>
        /// <returns></returns>
        private TreeNode createTreeNode(NodeStruct nodeStruct)
        {
            TreeNode treeNode = new TreeNode();
            treeNode.Name = nodeStruct.organizationID;
            treeNode.Text = nodeStruct.organizationName;
            treeNode.Tag = nodeStruct;
            return treeNode;
        }

        /// <summary>
        /// 插入节点parentNode的子节点来构建树，并将这些子节点保存到队列中
        /// </summary>
        /// <param name="parentNode"></param>
        /// <param name="childCacheNodeList"></param>
        private void insertAndSaveTreeNode(TreeNode parentNode, LinkedList<NodeStruct> childCacheNodeList,LinkedList<TreeNode> sumCacheNodeList)
        {
            foreach (NodeStruct nodeStruct in childCacheNodeList)
            {
                TreeNode treeNode = createTreeNode(nodeStruct);
                parentNode.Nodes.Add(treeNode);
                sumCacheNodeList.AddLast(treeNode);
            }
        }

        /// <summary>
        /// 初始化组织机构图
        /// </summary>
        private void initOrganizationTreeView()
        {
            //总的节点缓存队列
            LinkedList<TreeNode> sumCacheNodeList = new LinkedList<TreeNode>();
            //某节点孩子节点缓存队列
            LinkedList<NodeStruct> childCacheNodeList = new LinkedList<NodeStruct>();

            try
            {
                OrganizationManageBLL organizationManageBLL = new OrganizationManageBLL();
                DataTable organizationInforTable = organizationManageBLL.queryAllOrganizationInfor();
                if (organizationInforTable == null || organizationInforTable.Rows.Count <= 0)
                {
                    return;
                }
                
                //找到树根节点
                NodeStruct rootStruct = findRootStruct(organizationInforTable);
                //treeView插入根节点
                this.organizationTreeView.Nodes.Add(createTreeNode(rootStruct));
                //获取TreeView根节点treeNode
                TreeNode rootNode = this.organizationTreeView.Nodes[0];
                //插入rootNode到总节点缓存队列
                sumCacheNodeList.AddLast(rootNode);

                while (sumCacheNodeList.Count > 0)
                {
                    //队头元素出队
                    TreeNode tmpNode = sumCacheNodeList.First.Value;
                    sumCacheNodeList.RemoveFirst();
                    //查找tmpNode的孩子节点
                    selectChildStruct(organizationInforTable, childCacheNodeList, (NodeStruct)tmpNode.Tag);
                    //构建树并保存节点到队列
                    insertAndSaveTreeNode(tmpNode, childCacheNodeList,sumCacheNodeList);
                }
            }
            catch (Exception e)
            {
                MessageUtil.ShowError(e.Message);
                this.Close();
            }
        }

        /// <summary>
        /// TreeView为空，只显示新建菜单
        /// </summary>
        private void setMenuItemBlank()
        {
            this.createMenuItem.Enabled = true;
            this.editMenuItem.Enabled = false;
            this.deleteMenuItem.Enabled = false;
            this.addStaffMenuItem.Enabled = false;
        }

        /// <summary>
        /// TreeView不为空，点击节点时显示所有菜单
        /// </summary>
        private void setMenuItemNode()
        {
            this.createMenuItem.Enabled = true;
            this.editMenuItem.Enabled = true;
            this.deleteMenuItem.Enabled = true;
            this.addStaffMenuItem.Enabled = true;
        }

        /// <summary>
        /// treeView右键菜单
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void organizationTreeView_MouseUp(object sender, MouseEventArgs e)
        {
            //TreeView为空时，右键
            if (this.organizationTreeView.Nodes.Count <= 0 && e.Button == MouseButtons.Right)
            {
                setMenuItemBlank();
                this.organizationTreeViewMenu.Show(MousePosition.X, MousePosition.Y);
                return;
            }

            //TreeView不为空时，右键在某个节点上
            clickedNode = this.organizationTreeView.GetNodeAt(e.X,e.Y);
            if (clickedNode != null)
            {
                this.organizationTreeView.SelectedNode = clickedNode;

                if (e.Button == MouseButtons.Right)
                {
                    setMenuItemNode();
                    this.organizationTreeViewMenu.Show(MousePosition.X, MousePosition.Y);
                }
                else if (e.Button == MouseButtons.Left)
                {
                    fillUserDataGridView();
                }
            }
        }

        /// <summary>
        /// 创建新树节点
        /// </summary>
        /// <param name="nodeInfor"></param>
        /// <returns></returns>
        private TreeNode createTreeNode(String organizationID,String organizationName)
        {
            TreeNode newNode = new TreeNode();
            newNode.Name = organizationID;
            newNode.Text = organizationName;
            return newNode;
        }

        /// <summary>
        /// 插入新的组织机构信息
        /// </summary>
        private void insertOrganization(String organizationID,String organizationName,String organizationDescription)
        {
            OrganizationManageBLL orgManageBLL = new OrganizationManageBLL();
            BaseOrganizationModel orgModel = new BaseOrganizationModel();
            orgModel.Organization_ID = organizationID;
            orgModel.Organization_Name = organizationName;
            orgModel.Description = organizationDescription;

            if (clickedNode == null)//treeView为空，插入根节点
            {
                orgModel.Left_Value = 1;
                orgModel.Right_Value = 2;
                orgModel.Level = 0;
            }
            else//treeView不为空，插入clickedNode的子节点
            {
                orgModel.Left_Value = orgManageBLL.queryParentRightValue(clickedNode.Name);
                orgModel.Right_Value = orgModel.Left_Value + 1;
                orgModel.Level = clickedNode.Level + 1;
            }

            try
            {
                //插入组织机构信息到数据库
                orgManageBLL.insertOrganization(orgModel);

                //在TreeView上建立插入的节点
                TreeNode newNode = createTreeNode(organizationID, organizationName);
                if (clickedNode == null)
                {
                    this.organizationTreeView.Nodes.Add(newNode);
                }
                else
                {
                    clickedNode.Nodes.Add(newNode);
                }
            }
            catch (Exception e)
            {
                MessageUtil.ShowError(e.Message);
            }
        }

        /// <summary>
        /// 新建TreeView节点
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void createMenuItem_Click(object sender, EventArgs e)
        {
            OrganizationDetailForm organizationDetail = new OrganizationDetailForm(0);
            DialogResult rest = organizationDetail.ShowDialog(this);
            if (rest == DialogResult.OK)
            {
                String organizationID = organizationDetail.getOrganizationID();
                String organizationName = organizationDetail.getOrganizationName();
                String organizationDescription = organizationDetail.getOrganizationDescription();
                insertOrganization(organizationID,organizationName,organizationDescription);
            }
        }

        /// <summary>
        /// 编辑TreeView节点
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void editMenuItem_Click(object sender, EventArgs e)
        {
            if (clickedNode != null)
            {
                OrganizationDetailForm orgDetail = new OrganizationDetailForm(1,clickedNode.Name);
                DialogResult rest = orgDetail.ShowDialog(this);
                if (rest == DialogResult.OK)
                {
                    BaseOrganizationModel orgModel = new BaseOrganizationModel();
                    orgModel.Organization_ID = orgDetail.getOrganizationID();
                    orgModel.Organization_Name = orgDetail.getOrganizationName();
                    orgModel.Description = orgDetail.getOrganizationDescription();

                    try
                    {
                        //更新数据库记录
                        OrganizationManageBLL orgManageBLL = new OrganizationManageBLL();
                        orgManageBLL.updateOrganizationInfor(orgModel);

                        //更新TreeView节点
                        clickedNode.Text = orgDetail.getOrganizationName();
                    }
                    catch (Exception exp)
                    {
                        MessageUtil.ShowError(exp.Message);
                    }
                }
            }
        }

        /// <summary>
        /// 删除TreeView节点
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void deleteMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult rest = MessageUtil.ShowOKCancelAndQuestion("确定删除当前组织机构节点及所有子节点？");
            if (rest == DialogResult.OK)
            {
                try
                {
                    BaseOrganizationModel orgModel = new BaseOrganizationModel();
                    orgModel.Organization_ID = this.clickedNode.Name;

                    OrganizationManageBLL orgManageBLL = new OrganizationManageBLL();
                    //获取当前节点左右值
                    orgManageBLL.queryOrganizationInfor(orgModel);
                    //获取当前节点及所有子孙节点编号
                    DataTable offspringOrgTable = orgManageBLL.queryOffspringOrganization(orgModel);

                    //解除当前节点及子节点和员工的关联
                    JoinUserOrganizationModel joinUserOrgModel = new JoinUserOrganizationModel();
                    foreach (DataRow row in offspringOrgTable.Rows)
                    {
                        joinUserOrgModel.Organization_ID = Convert.ToString(row["Organization_ID"]);
                        orgManageBLL.removeUserOrganizationLinkByOrgId(joinUserOrgModel);
                    }
                    
                    //删除数据库中当前节点及所有子节点
                    //更新受影响的节点的左右值
                    orgManageBLL.deleteOffspringOrganizationAndUpdateNodes(orgModel);

                    //从界面treeView中删除选中节点及所有子节点
                    this.clickedNode.Nodes.Clear();
                    this.clickedNode.Remove();

                    //刷新员工列表
                    TreeNode selectedNode = this.organizationTreeView.SelectedNode;
                    this.clickedNode = selectedNode != null ? selectedNode : this.clickedNode;
                    fillUserDataGridView();
                }
                catch (Exception exp)
                {
                    MessageUtil.ShowError(exp.Message);
                }
            }
        }

        /// <summary>
        /// 关联员工到当前节点
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void addStaffMenuItem_Click(object sender, EventArgs e)
        {
            UserListForSelectionForm userListForSelectionForm = new UserListForSelectionForm();
            DialogResult rest = userListForSelectionForm.ShowDialog(this) ;
            if (rest == DialogResult.OK)
            {
                Dictionary<String, String> userDict = userListForSelectionForm.getSelectedUserDict();
                if (userDict.Count > 0)
                {
                    try
                    {
                        //将选择的员工和当前节点建立联系
                        OrganizationManageBLL orgManage = new OrganizationManageBLL();
                        JoinUserOrganizationModel model = new JoinUserOrganizationModel();
                        model.Organization_ID = this.clickedNode.Name;
                        foreach (var item in userDict)
                        {
                            model.User_ID = item.Key;
                            try
                            {
                                orgManage.insertUserOrganizationLink(model);
                            }
                            catch (Exception exp)
                            {
                                Console.Write(exp.Message);
                            }
                        }

                        //刷新员工列表
                        fillUserDataGridView();
                    }
                    catch (Exception exp)
                    {
                        MessageUtil.ShowError(exp.Message);
                    }
                }
            }
        }

        /// <summary>
        /// 根据所选择的组织机构级别，查询员工并填充列表
        /// </summary>
        private void fillUserDataGridView()
        {
            if (clickedNode != null)
            {
                //刷新员工列表
                OrganizationManageBLL orgManage = new OrganizationManageBLL();
                this.userDataGridView.AutoGenerateColumns = false;
                this.userDataGridView.DataSource = orgManage.queryUserInforByOrganizationID(clickedNode.Name);
            }
        }

        /// <summary>
        /// 将员工从某组织机构移除
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void unbondMenuItem_Click(object sender, EventArgs e)
        {
            if (this.clickedNode != null && this.rowIndex >= 0)
            {
                try
                {
                    DialogResult rest = MessageUtil.ShowOKCancelAndQuestion("确定从当前组织机构级别移除此员工？");
                    if (rest == DialogResult.OK)
                    {
                        JoinUserOrganizationModel orgModel = new JoinUserOrganizationModel();
                        orgModel.Organization_ID = this.clickedNode.Name;
                        orgModel.User_ID = Convert.ToString(this.userDataGridView.Rows[this.rowIndex].Cells[0].Value);

                        OrganizationManageBLL orgManageBLL = new OrganizationManageBLL();
                        orgManageBLL.removeUserOrganizationLink(orgModel);

                        fillUserDataGridView();
                    }
                }
                catch (Exception exp)
                {
                    MessageUtil.ShowError(exp.Message);
                }
            }
        }

        /// <summary>
        /// 右击员工列表显示菜单
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void userDataGridView_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right && e.RowIndex >= 0)
            {
                this.rowIndex = e.RowIndex;
                this.userDataGridViewMenu.Show(MousePosition.X,MousePosition.Y);
            }
        }

    }
}
