﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Configuration;
using System.Threading;

using Lib.Common.CommonUtils;
using Lib.Bll.SystemConfig.DataBackupRestoreManage;
using Lib.Model.SystemConfig;

using Microsoft.SqlServer.Management.Common;
using Microsoft.SqlServer.Management.Smo;

namespace MMClient.SystemConfig.DataBackupRestoreManage
{
    public partial class BackupDatabaseForm : Form
    {
        private String backupDirectory = "D:\\";                //备份目录
        private String backupAbsolutePath;                      //备份文件绝对路径
        private Server server;                                  //数据库服务器对象
        private String selectedDatabase;                        //要备份的数据库名

        private ProgressForm progressForm = null;               //进度条窗口
        private delegate void closeProressBarFormDelegate();    //关闭进度条窗口委托

        public BackupDatabaseForm()
        {
            InitializeComponent();
            initServerName();
        }

        /// <summary>
        /// 初始化服务器名
        /// </summary>
        public void initServerName()
        {
            String serverAddress = ConfigurationManager.AppSettings["serverAddress"];
            String []childItem = serverAddress.Split(new char[]{';'});
            foreach (String item in childItem)
            {
                if (item.StartsWith("Data Source"))
                {
                    String[] ipAddrss = item.Split(new char[]{'='});
                    if (ipAddrss.Length == 2)
                    {
                        this.serverName.Text = ipAddrss[1];
                    }
                }
            }
        }

        /// <summary>
        /// 连接服务器
        /// </summary>
        /// <param name="serverAndInstanceName">服务器名</param>
        /// <param name="userName">用户名</param>
        /// <param name="password">密码</param>
        /// <param name="useWindowAuthentication">是否使用windows身份验证</param>
        private void connect(String serverAndInstanceName,String userName,String password,bool useWindowAuthentication)
        {
            if (server == null)
            {
                server = new Server();
            }

            server.ConnectionContext.ServerInstance = serverAndInstanceName;
            server.ConnectionContext.LoginSecure = useWindowAuthentication; //是否启用windows身份验证

            if (!useWindowAuthentication)
            {
                server.ConnectionContext.Login = userName;
                server.ConnectionContext.Password = password;
            }
            
            server.ConnectionContext.Connect();
        }

        /// <summary>
        /// 获取服务器数据库名列表
        /// </summary>
        /// <returns></returns>
        private List<String> getDatabaseNameList()
        {
            List<String> dbList = new List<String>();
            foreach (Database db in server.Databases)
            {
                dbList.Add(db.Name);
            }
            return dbList;
        }

        /// <summary>
        /// 查询备份历史记录
        /// </summary>
        private void buildBackupHistory()
        {
            BackupDatabaseBLL backupDatabase = new BackupDatabaseBLL();
            this.backupHistory.Columns["backupTime"].DefaultCellStyle.Format = "yyyy-MM-dd hh:MM:ss";
            this.backupHistory.AutoGenerateColumns = false;
            this.backupHistory.DataSource = backupDatabase.queryAllBackupRecords();
        }

        /// <summary>
        /// 插入新的备份记录到数据库
        /// </summary>
        private void insertBackupRecord()
        {
            DatabaseBackupRecordModel recordModel = new DatabaseBackupRecordModel();
            recordModel.DB_Name = this.selectedDatabase;
            recordModel.Backup_Time = DateTime.Now.ToString("yyyy-MM-dd hh:MM:ss");
            recordModel.Backup_Operator = SingleUserInstance.getCurrentUserInstance().User_ID;
            recordModel.Backup_Path = backupAbsolutePath;

            BackupDatabaseBLL backupDatabase = new BackupDatabaseBLL();
            try
            {
                backupDatabase.insertBackupRecord(recordModel);
            }
            catch (Exception)
            {
                MessageUtil.ShowError("插入备份记录到数据库失败！");
            }
        }

        /// <summary>
        /// 数据库备份
        /// </summary>
        /// <param name="databaseName"></param>
        private void backupDatabase(String databaseName)
        {
            Backup backup = new Backup();

            backup.Action = BackupActionType.Database;//完全备份数据库
            backup.Database = databaseName;
            backup.Incremental = false; //不进行增量备份
            backup.Initialize = true;

            backupAbsolutePath = string.Format("{0}\\{1}.bak", backupDirectory, databaseName+DateTime.Now.ToString("yyyyMMddhhMMss"));
            BackupDeviceItem backupDeviceItem = new BackupDeviceItem(backupAbsolutePath, DeviceType.File);
            backup.Devices.Add(backupDeviceItem);
            backup.Complete += new ServerMessageEventHandler(backup_Complete);//备份完毕回调事件
            
            //异步备份数据库
            try
            {
                backup.SqlBackupAsync(server);
            }
            catch (Exception)
            {
                MessageUtil.ShowError("备份数据库出错！");
            }
            
        }

        /// <summary>
        /// 备份完成事件处理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void backup_Complete(object sender, ServerMessageEventArgs e)
        {
            //委托给控件所在的线程对控件进行操作，不能在此线程中直接ProgressFrom
            this.Invoke(new closeProressBarFormDelegate(closeProgressBar));
        }

        #region 按钮事件

        /// <summary>
        /// 检查连接必要的信息是否填写完整
        /// </summary>
        /// <returns></returns>
        private bool checkLinkInforIntegrity()
        {
            if (String.IsNullOrEmpty(this.serverName.Text))
            {
                MessageUtil.ShowWarning("服务器名称为空，请填写！");
                return false;
            }

            if (String.IsNullOrEmpty(this.loginId.Text))
            {
                MessageUtil.ShowWarning("登录名为空，请填写！");
                return false;
            }

            if (String.IsNullOrEmpty(this.loginPassword.Text))
            {
                MessageUtil.ShowWarning("登录密码为空，请填写！");
                return false;
            }

            return true;
        }

        /// <summary>
        /// //初始化数据库列表
        /// </summary>
        private void initDatabaseList()
        {
            List<String> databaseNameList = getDatabaseNameList();
            foreach (String databaseName in databaseNameList)
            {
                this.databaseList.Items.Add(databaseName);
            }
        }

        /// <summary>
        /// 建立数据库连接
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void createLinkBtn_Click(object sender, EventArgs e)
        {
            if (!checkLinkInforIntegrity())
            {
                return;
            }

            if (this.server != null && this.server.ConnectionContext.IsOpen)
            {
                MessageUtil.ShowWarning("连接已建立并打开！");
                return;
            }

            try
            {
                //连接数据库
                connect(this.serverName.Text, this.loginId.Text, this.loginPassword.Text, false);
                
                //初始化数据库列表
                initDatabaseList();
                
                //初始化备份历史表
                buildBackupHistory();
            }
            catch (Exception exp)
            {
                MessageUtil.ShowError(exp.Message);
            }
        }

        /// <summary>
        /// 备份数据库
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void backupBtn_Click(object sender, EventArgs e)
        {
            if (server == null)
            {
                MessageUtil.ShowWarning("数据库未连接，无法备份！");
                return;
            }
            if (!server.ConnectionContext.IsOpen)
            {
                MessageUtil.ShowWarning("数据库连接断开，无法备份！");
                return;
            }

            if (this.databaseList.SelectedIndex < 0)
            {
                MessageUtil.ShowWarning("请选择要备份的数据库！");
                return;
            }

            //保存所选的数据库名，在backgroundWorker中使用
            this.selectedDatabase = this.databaseList.SelectedItem.ToString();
            
            //开始执行backgroundWorker
            this.backgroundWorker.RunWorkerAsync(this);
        }

        /// <summary>
        /// 退出窗口
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void exitBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// 刷新备份历史
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void refreshBtn_Click(object sender, EventArgs e)
        {
            buildBackupHistory();
        }
        #endregion

        /// <summary>
        /// 释放连接资源
        /// </summary>
        private void releaseConnection()
        {
            if (server != null && server.ConnectionContext.IsOpen)
            {
                server.ConnectionContext.Disconnect();
            }
        }

        /// <summary>
        /// 窗口将关闭事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BackupDatabaseForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            releaseConnection();
        }

        /// <summary>
        /// BackgroundWorker后台执行
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void backgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            MethodInvoker methodInvoker = new MethodInvoker(showProgressBar);
            this.BeginInvoke(methodInvoker);
            
            //延时1s等待进度窗口显示
            //Thread.Sleep(1000);
            
            //开始备份数据库
            backupDatabase(this.selectedDatabase);
        }

        /// <summary>
        /// 显示进度条窗体
        /// </summary>
        private void showProgressBar()
        {
            progressForm = new ProgressForm();
            progressForm.ShowDialog();
        }

        /// <summary>
        /// 关闭进度条窗口
        /// </summary>
        private void closeProgressBar()
        {
            //关闭进度窗口
            if (progressForm != null)
            {
                progressForm.Close();
                progressForm = null;
            }

            //提示备份完毕
            MessageUtil.ShowTips("备份完毕！");

            //将备份的相关信息记录到数据库中
            insertBackupRecord();

            //刷新当前窗口备份历史记录
            buildBackupHistory();
        }


    }
}
