﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Lib.Bll.SystemConfig.UserManage;
using Lib.Bll.SystemConfig.PermissionManage;
using Lib.Common.CommonUtils;
using Lib.Model.SystemConfig;

namespace MMClient.SystemConfig.PermissionManage
{
    public partial class FunctionLevelPermissionManageForm : Form
    {
        private BaseUserModel userModel;           //被授权的用户Model
        private BaseGroupModel groupModel;     //被授权的组Model
        private BaseRoleModel roleModel;            //被授权的角色Model
        private int type;                                            //1:对用户；2：对组；3：对角色

        private Dictionary<Int32, Boolean> currentPagePermissionDict;   //存储当前页的所有权限项<权限对象ID,是否具有>

        public FunctionLevelPermissionManageForm(Object model,int type)
        {
            InitializeComponent();

            this.permissionTable.AutoGenerateColumns = false;
            this.type = type;
            switch (type)
            {
                case 1:
                    userModel = (BaseUserModel)model;
                    initBaseUser();
                    break;
                case 2:
                    groupModel = (BaseGroupModel)model;
                    initBaseGroup();
                    break;
                case 3:
                    roleModel = (BaseRoleModel)model;
                    initBaseRole();
                    break;
            }
        }

        /// <summary>
        /// 用户对象授权
        /// </summary>
        private void initBaseUser()
        {
        }

        private void initBaseGroup()
        {
            hiddenPermissionTableColumnsGroup();
        }

        private void initBaseRole()
        {
            hiddenPermissionTableColumnsRole();
        }

        /// <summary>
        /// 给组授权时隐藏2列
        /// </summary>
        private void hiddenPermissionTableColumnsGroup()
        {
            this.permissionTable.Columns["groupPermission"].Visible = false;
            this.permissionTable.Columns["rolePermission"].Visible = false;
            this.permissionTable.Columns["personalPermission"].HeaderText = "有否授权";
        }

        /// <summary>
        /// 给角色授权时隐藏2列
        /// </summary>
        private void hiddenPermissionTableColumnsRole()
        {
            this.permissionTable.Columns["groupPermission"].Visible = false;
            this.permissionTable.Columns["rolePermission"].Visible = false;
            this.permissionTable.Columns["personalPermission"].HeaderText = "有否授权";
        }

        #region 按钮点击事件处理
        /// <summary>
        /// 授予全部权限
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void authorizeAllBtn_Click(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// 取消所有权限
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cancelAllBtn_Click(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// 浏览权限列表
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void permissionListBtn_Click(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// 设置选择性
        /// </summary>
        /// <param name="flag"></param>
        private void setSelection(bool flag)
        {
            foreach (DataGridViewRow row in this.permissionTable.Rows)
            {
                row.Cells["personalPermission"].Value = flag;
            }
        }

        /// <summary>
        /// 全选当前菜单子项所有权限
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void selectCurrentAllBtn_Click(object sender, EventArgs e)
        {
            setSelection(true);
        }

        /// <summary>
        /// 取消当前菜单子项所有权限
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cancelCurrentAllBtn_Click(object sender, EventArgs e)
        {
            setSelection(false);
        }

        /// <summary>
        /// 获取改变了的权限项
        /// </summary>
        /// <param name="cancelldList">原来有现在取消了的</param>
        /// <param name="newAddedList">原来没有现在新授权的</param>
        private void getChangedPermissionItem(List<int> cancelledList,List<int> newAddedList)
        {
            foreach (DataGridViewRow row in this.permissionTable.Rows)
            {
                int curPermissionId = Convert.ToInt32(row.Cells["Permission_ID"].Value);
                bool curHasPermission = Convert.ToBoolean(row.Cells["personalPermission"].EditedFormattedValue);
                if (curHasPermission == true && currentPagePermissionDict[curPermissionId] == false)
                {
                    newAddedList.Add(curPermissionId);
                }
                else if (curHasPermission == false && currentPagePermissionDict[curPermissionId] == true)
                {
                    cancelledList.Add(curPermissionId);
                }
            }
        }

        /// <summary>
        /// 给单个用户授权
        /// </summary>
        private void authorizeUser()
        {
            List<int> newAddedList = new List<int>();
            List<int> cancelledList = new List<int>();
            getChangedPermissionItem(cancelledList, newAddedList);

            FunctionLevelPermissionManageBLL flpManageBLL = new FunctionLevelPermissionManageBLL();
            JoinUserFunctionPermissionObjectModel objModel = new JoinUserFunctionPermissionObjectModel();
            objModel.User_ID = this.userModel.User_ID;
            
            //是否发生过异常
            bool exceptionFlag = false; 

            //新增的授权项写入DB
            foreach (int permissionId in newAddedList)
            {
                try
                {
                    objModel.Permission_ID = permissionId;
                    flpManageBLL.authorizePermission(objModel);
                }
                catch (Exception)
                {
                    exceptionFlag = true;
                    MessageUtil.ShowError("授权发生异常！");
                }
            }

            //撤销的授权项从DB中删除
            foreach (int permissionId in cancelledList)
            {
                try
                {
                    objModel.Permission_ID = permissionId;
                    flpManageBLL.revokePermission(objModel);
                }
                catch (Exception)
                {
                    exceptionFlag = true;
                    MessageUtil.ShowError("授权发生异常！");
                }
            }

            if ((newAddedList.Count > 0 || cancelledList.Count > 0) && !exceptionFlag)
            {
                MessageUtil.ShowTips("授权操作完成！");
            }
        }

        /// <summary>
        /// 给组授权
        /// </summary>
        private void authorizeGroup()
        {
            List<int> newAddedList = new List<int>();
            List<int> cancelledList = new List<int>();
            getChangedPermissionItem(cancelledList, newAddedList);

            FunctionLevelPermissionManageBLL flpManageBLL = new FunctionLevelPermissionManageBLL();
            JoinGroupFunctionPermissionObjectModel objModel = new JoinGroupFunctionPermissionObjectModel();
            objModel.Group_ID = this.groupModel.Group_ID;

            //是否发生过异常
            bool exceptionFlag = false;

            //新增的授权项写入DB
            foreach (int permissionId in newAddedList)
            {
                try
                {
                    objModel.Permission_ID = permissionId;
                    flpManageBLL.authorizeGroupPermission(objModel);
                }
                catch (Exception)
                {
                    exceptionFlag = true;
                    MessageUtil.ShowError("授权发生异常！");
                }
            }

            //撤销的授权项从DB中删除
            foreach (int permissionId in cancelledList)
            {
                try
                {
                    objModel.Permission_ID = permissionId;
                    flpManageBLL.revokeGroupPermission(objModel);
                }
                catch (Exception)
                {
                    exceptionFlag = true;
                    MessageUtil.ShowError("授权发生异常！");
                }
            }

            if ((newAddedList.Count > 0 || cancelledList.Count > 0) && !exceptionFlag)
            {
                MessageUtil.ShowTips("授权操作完成！");
            }
        }

        /// <summary>
        /// 给角色授权
        /// </summary>
        private void authorizeRole()
        {
            List<int> newAddedList = new List<int>();
            List<int> cancelledList = new List<int>();
            getChangedPermissionItem(cancelledList, newAddedList);

            FunctionLevelPermissionManageBLL flpManageBLL = new FunctionLevelPermissionManageBLL();
            JoinRoleFunctionPermissionObjectModel objModel = new JoinRoleFunctionPermissionObjectModel();
            objModel.Role_ID = this.roleModel.Role_ID;

            //是否发生过异常
            bool exceptionFlag = false;

            //新增的授权项写入DB
            foreach (int permissionId in newAddedList)
            {
                try
                {
                    objModel.Permission_ID = permissionId;
                    flpManageBLL.authorizeRolePermission(objModel);
                }
                catch (Exception)
                {
                    exceptionFlag = true;
                    MessageUtil.ShowError("授权发生异常！");
                }
            }

            //撤销的授权项从DB中删除
            foreach (int permissionId in cancelledList)
            {
                try
                {
                    objModel.Permission_ID = permissionId;
                    flpManageBLL.revokeRolePermission(objModel);
                }
                catch (Exception)
                {
                    exceptionFlag = true;
                    MessageUtil.ShowError("授权发生异常！");
                }
            }

            if ((newAddedList.Count > 0 || cancelledList.Count > 0) && !exceptionFlag)
            {
                MessageUtil.ShowTips("授权操作完成！");
            }
        }

        /// <summary>
        /// 授权
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void authorizeBtn_Click(object sender, EventArgs e)
        {
            if (MessageUtil.ShowOKCancelAndQuestion("确定保存当前修改？") == DialogResult.Cancel)
            {
                return;
            }

            switch (this.type)
            {
                case 1:
                    authorizeUser();
                    break;
                case 2:
                    authorizeGroup();
                    break;
                case 3:
                    authorizeRole();
                    break;
            }
            setOldPermissionDict();
        }

        /// <summary>
        /// 退出
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void exitBtn_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }
        #endregion

        /// <summary>
        /// 将当页的权限对象存储到Map中,用于比对是否有改动
        /// </summary>
        private void setOldPermissionDict()
        {
            DataTable sourceTable = (DataTable)this.permissionTable.DataSource;
            if (sourceTable != null)
            {
                if (currentPagePermissionDict == null)
                {
                    currentPagePermissionDict = new Dictionary<Int32, Boolean>();
                }
                else
                {
                    currentPagePermissionDict.Clear();
                }
                foreach (DataRow row in sourceTable.Rows)
                {
                    int permissionId = Convert.ToInt32(row["Permission_ID"]);
                    bool flag = Convert.ToBoolean(row["hasPermission"]);
                    currentPagePermissionDict.Add(permissionId, flag);
                }
            }
        }

        /// <summary>
        /// 树节点选中
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void permissionObject_AfterSelect(object sender, TreeViewEventArgs e)
        {
            //点击非叶子节点，清空权限表
            if (e.Node.Nodes.Count > 0)
            {
                this.permissionTable.DataSource = null;
            }
            else
            {
                BaseFunctionPermissionObjectModel baseFunctionModel = new BaseFunctionPermissionObjectModel();
                baseFunctionModel.Menu_Name = e.Node.Name;
                
                FunctionLevelPermissionManageBLL functionPermissionBll = new FunctionLevelPermissionManageBLL();
                switch (this.type)
                {
                    case 1:
                        this.permissionTable.DataSource = functionPermissionBll.queryBaseFunctionPermission(this.userModel, baseFunctionModel);
                        break;
                    case 2:
                        this.permissionTable.DataSource = functionPermissionBll.queryBaseGroupPermission(this.groupModel,baseFunctionModel);
                        break;
                    case 3:
                        this.permissionTable.DataSource = functionPermissionBll.queryBaseRolePermission(this.roleModel, baseFunctionModel);
                        break;
                }
                setOldPermissionDict();
            }

            //状态栏显示当前选中的节点文本
            this.statusBar.Text = e.Node.Text;
        }

        /// <summary>
        /// 比对是否有权限项的选择发生变化
        /// </summary>
        /// <returns></returns>
        private bool isPermissionSelectionChanged()
        {
            foreach(DataGridViewRow row in this.permissionTable.Rows)
            {
                int permissionId = Convert.ToInt32(row.Cells["Permission_ID"].Value);
                bool hasPermission = Convert.ToBoolean(row.Cells["personalPermission"].EditedFormattedValue);    //必须使用编辑值
                if (this.currentPagePermissionDict.ContainsKey(permissionId))
                {
                    if (this.currentPagePermissionDict[permissionId] != hasPermission)
                    {
                        return true;
                    }
                }
                else
                {
                    throw new Exception("比对权限对象发生异常！");
                }
            }

            return false;
        }

        /// <summary>
        /// 窗口将关闭事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FunctionLevelPermissionManageForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            //退出时检查是否有修改未保存，给出提示
            if (isPermissionSelectionChanged())
            {
                DialogResult rest = MessageUtil.ShowOKCancelAndQuestion("仍有修改未保存，确定退出？");
                if (rest == DialogResult.Cancel)
                {
                    e.Cancel = true;
                }
                else
                {
                    e.Cancel = false;
                }
            }
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            authorExplain author= null;
            if (author == null || author.IsDisposed)
            {
                author = new authorExplain(1);
            }
            author.Show();
        }
    }
}
