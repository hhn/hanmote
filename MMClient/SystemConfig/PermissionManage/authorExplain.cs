﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MMClient.SystemConfig.PermissionManage
{
    public partial class authorExplain : Form
    {
        public authorExplain(int flag)//1 权限解释 2 权限授权方式介绍
        {
            InitializeComponent();
            if(flag==1)
            {
                this.textBox1.Text = @" 通识解释
用户：个人 角色 用户组
个人：单个员工
角色：根据业务分工的不同，所划分的角色不一样。
用户组：具有相同职能的一类员工。
权限设置则是根据用户的划分来分配权限
个人权限：单个公司员工所具有的权限，具有独特性，可以与他人不同。
角色权限：某类角色所具有的权限。
组权限：具有相同职能的一类员工，对他们授权时可直接将员工设置成隶属于某个用户组，该员工即可获得这个用户组的所有权限";
             }
            if (flag == 2)
            {
                this.Text = "操作说明";
                this.textBox1.Text = @"这里针对的是单个用户的授权，
您也可以通过将用户添加到特定的用户组，以获得用户组的所有权限";
            }
        }
    }
}
