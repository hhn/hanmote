﻿namespace MMClient.SystemConfig.PermissionManage
{
    partial class FunctionLevelPermissionManageForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.TreeNode treeNode1 = new System.Windows.Forms.TreeNode("新建物料数据");
            System.Windows.Forms.TreeNode treeNode2 = new System.Windows.Forms.TreeNode("基本视图");
            System.Windows.Forms.TreeNode treeNode3 = new System.Windows.Forms.TreeNode("会计视图");
            System.Windows.Forms.TreeNode treeNode4 = new System.Windows.Forms.TreeNode("采购视图");
            System.Windows.Forms.TreeNode treeNode5 = new System.Windows.Forms.TreeNode("存储视图");
            System.Windows.Forms.TreeNode treeNode6 = new System.Windows.Forms.TreeNode("建立批次");
            System.Windows.Forms.TreeNode treeNode7 = new System.Windows.Forms.TreeNode("查询批次");
            System.Windows.Forms.TreeNode treeNode8 = new System.Windows.Forms.TreeNode("批次管理", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode6,
            treeNode7});
            System.Windows.Forms.TreeNode treeNode9 = new System.Windows.Forms.TreeNode("库存管理MPN");
            System.Windows.Forms.TreeNode treeNode10 = new System.Windows.Forms.TreeNode("非库存管理MPN");
            System.Windows.Forms.TreeNode treeNode11 = new System.Windows.Forms.TreeNode("制造商物料管理", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode9,
            treeNode10});
            System.Windows.Forms.TreeNode treeNode12 = new System.Windows.Forms.TreeNode("物料主数据", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode1,
            treeNode2,
            treeNode3,
            treeNode4,
            treeNode5,
            treeNode8,
            treeNode11});
            System.Windows.Forms.TreeNode treeNode13 = new System.Windows.Forms.TreeNode("新建供应商");
            System.Windows.Forms.TreeNode treeNode14 = new System.Windows.Forms.TreeNode("基本视图");
            System.Windows.Forms.TreeNode treeNode15 = new System.Windows.Forms.TreeNode("采购组织视图");
            System.Windows.Forms.TreeNode treeNode16 = new System.Windows.Forms.TreeNode("公司代码视图");
            System.Windows.Forms.TreeNode treeNode17 = new System.Windows.Forms.TreeNode("供应商主数据", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode13,
            treeNode14,
            treeNode15,
            treeNode16});
            System.Windows.Forms.TreeNode treeNode18 = new System.Windows.Forms.TreeNode("生成会计凭证");
            System.Windows.Forms.TreeNode treeNode19 = new System.Windows.Forms.TreeNode("查询会计凭证");
            System.Windows.Forms.TreeNode treeNode20 = new System.Windows.Forms.TreeNode("查询科目信息");
            System.Windows.Forms.TreeNode treeNode21 = new System.Windows.Forms.TreeNode("财务主数据", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode18,
            treeNode19,
            treeNode20});
            System.Windows.Forms.TreeNode treeNode22 = new System.Windows.Forms.TreeNode("维护物料组");
            System.Windows.Forms.TreeNode treeNode23 = new System.Windows.Forms.TreeNode("物料大分类");
            System.Windows.Forms.TreeNode treeNode24 = new System.Windows.Forms.TreeNode("物料小分类");
            System.Windows.Forms.TreeNode treeNode25 = new System.Windows.Forms.TreeNode("物料精确分类");
            System.Windows.Forms.TreeNode treeNode26 = new System.Windows.Forms.TreeNode("维护物料类型", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode23,
            treeNode24,
            treeNode25});
            System.Windows.Forms.TreeNode treeNode27 = new System.Windows.Forms.TreeNode("维护货币");
            System.Windows.Forms.TreeNode treeNode28 = new System.Windows.Forms.TreeNode("维护国家信息");
            System.Windows.Forms.TreeNode treeNode29 = new System.Windows.Forms.TreeNode("维护付款条件");
            System.Windows.Forms.TreeNode treeNode30 = new System.Windows.Forms.TreeNode("维护计量单位");
            System.Windows.Forms.TreeNode treeNode31 = new System.Windows.Forms.TreeNode("维护评估类");
            System.Windows.Forms.TreeNode treeNode32 = new System.Windows.Forms.TreeNode("维护公司代码");
            System.Windows.Forms.TreeNode treeNode33 = new System.Windows.Forms.TreeNode("维护工厂");
            System.Windows.Forms.TreeNode treeNode34 = new System.Windows.Forms.TreeNode("维护库存地");
            System.Windows.Forms.TreeNode treeNode35 = new System.Windows.Forms.TreeNode("维护采购组");
            System.Windows.Forms.TreeNode treeNode36 = new System.Windows.Forms.TreeNode("维护采购组织");
            System.Windows.Forms.TreeNode treeNode37 = new System.Windows.Forms.TreeNode("维护产品组");
            System.Windows.Forms.TreeNode treeNode38 = new System.Windows.Forms.TreeNode("维护交付条件");
            System.Windows.Forms.TreeNode treeNode39 = new System.Windows.Forms.TreeNode("一般设置", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode22,
            treeNode26,
            treeNode27,
            treeNode28,
            treeNode29,
            treeNode30,
            treeNode31,
            treeNode32,
            treeNode33,
            treeNode34,
            treeNode35,
            treeNode36,
            treeNode37,
            treeNode38});
            System.Windows.Forms.TreeNode treeNode40 = new System.Windows.Forms.TreeNode("数据管理", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode12,
            treeNode17,
            treeNode21,
            treeNode39});
            System.Windows.Forms.TreeNode treeNode41 = new System.Windows.Forms.TreeNode("查看计划");
            System.Windows.Forms.TreeNode treeNode42 = new System.Windows.Forms.TreeNode("计划列表", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode41});
            System.Windows.Forms.TreeNode treeNode43 = new System.Windows.Forms.TreeNode("新建");
            System.Windows.Forms.TreeNode treeNode44 = new System.Windows.Forms.TreeNode("修改");
            System.Windows.Forms.TreeNode treeNode45 = new System.Windows.Forms.TreeNode("查看");
            System.Windows.Forms.TreeNode treeNode46 = new System.Windows.Forms.TreeNode("删除");
            System.Windows.Forms.TreeNode treeNode47 = new System.Windows.Forms.TreeNode("申请部门", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode43,
            treeNode44,
            treeNode45,
            treeNode46});
            System.Windows.Forms.TreeNode treeNode48 = new System.Windows.Forms.TreeNode("审核");
            System.Windows.Forms.TreeNode treeNode49 = new System.Windows.Forms.TreeNode("汇总");
            System.Windows.Forms.TreeNode treeNode50 = new System.Windows.Forms.TreeNode("生成");
            System.Windows.Forms.TreeNode treeNode51 = new System.Windows.Forms.TreeNode("寻源");
            System.Windows.Forms.TreeNode treeNode52 = new System.Windows.Forms.TreeNode("采购部门", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode48,
            treeNode49,
            treeNode50,
            treeNode51});
            System.Windows.Forms.TreeNode treeNode53 = new System.Windows.Forms.TreeNode("计划管理", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode42,
            treeNode47,
            treeNode52});
            System.Windows.Forms.TreeNode treeNode54 = new System.Windows.Forms.TreeNode("创建源清单");
            System.Windows.Forms.TreeNode treeNode55 = new System.Windows.Forms.TreeNode("维护源清单");
            System.Windows.Forms.TreeNode treeNode56 = new System.Windows.Forms.TreeNode("源清单", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode54,
            treeNode55});
            System.Windows.Forms.TreeNode treeNode57 = new System.Windows.Forms.TreeNode("创建寻源");
            System.Windows.Forms.TreeNode treeNode58 = new System.Windows.Forms.TreeNode("维护寻源");
            System.Windows.Forms.TreeNode treeNode59 = new System.Windows.Forms.TreeNode("查看询价");
            System.Windows.Forms.TreeNode treeNode60 = new System.Windows.Forms.TreeNode("创建询价");
            System.Windows.Forms.TreeNode treeNode61 = new System.Windows.Forms.TreeNode("更改询价");
            System.Windows.Forms.TreeNode treeNode62 = new System.Windows.Forms.TreeNode("询价", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode59,
            treeNode60,
            treeNode61});
            System.Windows.Forms.TreeNode treeNode63 = new System.Windows.Forms.TreeNode("查看报价");
            System.Windows.Forms.TreeNode treeNode64 = new System.Windows.Forms.TreeNode("执行报价");
            System.Windows.Forms.TreeNode treeNode65 = new System.Windows.Forms.TreeNode("报价", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode63,
            treeNode64});
            System.Windows.Forms.TreeNode treeNode66 = new System.Windows.Forms.TreeNode("查看比价");
            System.Windows.Forms.TreeNode treeNode67 = new System.Windows.Forms.TreeNode("执行比价");
            System.Windows.Forms.TreeNode treeNode68 = new System.Windows.Forms.TreeNode("比价", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode66,
            treeNode67});
            System.Windows.Forms.TreeNode treeNode69 = new System.Windows.Forms.TreeNode("询价", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode62,
            treeNode65,
            treeNode68});
            System.Windows.Forms.TreeNode treeNode70 = new System.Windows.Forms.TreeNode("查看招标");
            System.Windows.Forms.TreeNode treeNode71 = new System.Windows.Forms.TreeNode("创建招标");
            System.Windows.Forms.TreeNode treeNode72 = new System.Windows.Forms.TreeNode("修改招标");
            System.Windows.Forms.TreeNode treeNode73 = new System.Windows.Forms.TreeNode("处理投标");
            System.Windows.Forms.TreeNode treeNode74 = new System.Windows.Forms.TreeNode("进行评标");
            System.Windows.Forms.TreeNode treeNode75 = new System.Windows.Forms.TreeNode("审批招标");
            System.Windows.Forms.TreeNode treeNode76 = new System.Windows.Forms.TreeNode("招标", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode70,
            treeNode71,
            treeNode72,
            treeNode73,
            treeNode74,
            treeNode75});
            System.Windows.Forms.TreeNode treeNode77 = new System.Windows.Forms.TreeNode("竞价");
            System.Windows.Forms.TreeNode treeNode78 = new System.Windows.Forms.TreeNode("竞拍");
            System.Windows.Forms.TreeNode treeNode79 = new System.Windows.Forms.TreeNode("拍卖", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode77,
            treeNode78});
            System.Windows.Forms.TreeNode treeNode80 = new System.Windows.Forms.TreeNode("采购信息维护");
            System.Windows.Forms.TreeNode treeNode81 = new System.Windows.Forms.TreeNode("招标技术提案");
            System.Windows.Forms.TreeNode treeNode82 = new System.Windows.Forms.TreeNode("一次招标");
            System.Windows.Forms.TreeNode treeNode83 = new System.Windows.Forms.TreeNode("供应商回复信息");
            System.Windows.Forms.TreeNode treeNode84 = new System.Windows.Forms.TreeNode("净现值回复");
            System.Windows.Forms.TreeNode treeNode85 = new System.Windows.Forms.TreeNode("两段式招标", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode81,
            treeNode82,
            treeNode83,
            treeNode84});
            System.Windows.Forms.TreeNode treeNode86 = new System.Windows.Forms.TreeNode("普通招标", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode85});
            System.Windows.Forms.TreeNode treeNode87 = new System.Windows.Forms.TreeNode("寻源管理", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode56,
            treeNode57,
            treeNode58,
            treeNode69,
            treeNode76,
            treeNode79,
            treeNode80,
            treeNode86});
            System.Windows.Forms.TreeNode treeNode88 = new System.Windows.Forms.TreeNode("管理");
            System.Windows.Forms.TreeNode treeNode89 = new System.Windows.Forms.TreeNode("合同模板", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode88});
            System.Windows.Forms.TreeNode treeNode90 = new System.Windows.Forms.TreeNode("管理");
            System.Windows.Forms.TreeNode treeNode91 = new System.Windows.Forms.TreeNode("合同文本", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode90});
            System.Windows.Forms.TreeNode treeNode92 = new System.Windows.Forms.TreeNode("合同管理", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode89,
            treeNode91});
            System.Windows.Forms.TreeNode treeNode93 = new System.Windows.Forms.TreeNode("管理");
            System.Windows.Forms.TreeNode treeNode94 = new System.Windows.Forms.TreeNode("合同", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode93});
            System.Windows.Forms.TreeNode treeNode95 = new System.Windows.Forms.TreeNode("管理");
            System.Windows.Forms.TreeNode treeNode96 = new System.Windows.Forms.TreeNode("计划协议", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode95});
            System.Windows.Forms.TreeNode treeNode97 = new System.Windows.Forms.TreeNode("框架协议", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode94,
            treeNode96});
            System.Windows.Forms.TreeNode treeNode98 = new System.Windows.Forms.TreeNode("管理");
            System.Windows.Forms.TreeNode treeNode99 = new System.Windows.Forms.TreeNode("配额协议", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode98});
            System.Windows.Forms.TreeNode treeNode100 = new System.Windows.Forms.TreeNode("自助采购", 1, 1);
            System.Windows.Forms.TreeNode treeNode101 = new System.Windows.Forms.TreeNode("订单管理");
            System.Windows.Forms.TreeNode treeNode102 = new System.Windows.Forms.TreeNode("发票管理");
            System.Windows.Forms.TreeNode treeNode103 = new System.Windows.Forms.TreeNode("订单执行", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode101,
            treeNode102});
            System.Windows.Forms.TreeNode treeNode104 = new System.Windows.Forms.TreeNode("手工维护");
            System.Windows.Forms.TreeNode treeNode105 = new System.Windows.Forms.TreeNode("自动评估");
            System.Windows.Forms.TreeNode treeNode106 = new System.Windows.Forms.TreeNode("批量评估");
            System.Windows.Forms.TreeNode treeNode107 = new System.Windows.Forms.TreeNode("结果显示");
            System.Windows.Forms.TreeNode treeNode108 = new System.Windows.Forms.TreeNode("清单显示");
            System.Windows.Forms.TreeNode treeNode109 = new System.Windows.Forms.TreeNode("一般服务");
            System.Windows.Forms.TreeNode treeNode110 = new System.Windows.Forms.TreeNode("外部服务");
            System.Windows.Forms.TreeNode treeNode111 = new System.Windows.Forms.TreeNode("维护服务占比");
            System.Windows.Forms.TreeNode treeNode112 = new System.Windows.Forms.TreeNode("服务评估", new System.Windows.Forms.TreeNode[] {
            treeNode109,
            treeNode110,
            treeNode111});
            System.Windows.Forms.TreeNode treeNode113 = new System.Windows.Forms.TreeNode("绩效评估", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode104,
            treeNode105,
            treeNode106,
            treeNode107,
            treeNode108,
            treeNode112});
            System.Windows.Forms.TreeNode treeNode114 = new System.Windows.Forms.TreeNode("体系/过程审核");
            System.Windows.Forms.TreeNode treeNode115 = new System.Windows.Forms.TreeNode("产品审核");
            System.Windows.Forms.TreeNode treeNode116 = new System.Windows.Forms.TreeNode("质量审核", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode114,
            treeNode115});
            System.Windows.Forms.TreeNode treeNode117 = new System.Windows.Forms.TreeNode("抱怨/拒绝情况录入");
            System.Windows.Forms.TreeNode treeNode118 = new System.Windows.Forms.TreeNode("服务情况录入");
            System.Windows.Forms.TreeNode treeNode119 = new System.Windows.Forms.TreeNode("收货问卷调查");
            System.Windows.Forms.TreeNode treeNode120 = new System.Windows.Forms.TreeNode("绩效评估数据维护", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode116,
            treeNode117,
            treeNode118,
            treeNode119});
            System.Windows.Forms.TreeNode treeNode121 = new System.Windows.Forms.TreeNode("执行分级");
            System.Windows.Forms.TreeNode treeNode122 = new System.Windows.Forms.TreeNode("分级查看");
            System.Windows.Forms.TreeNode treeNode123 = new System.Windows.Forms.TreeNode("供应商分级", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode121,
            treeNode122});
            System.Windows.Forms.TreeNode treeNode124 = new System.Windows.Forms.TreeNode("低效能供应商警告");
            System.Windows.Forms.TreeNode treeNode125 = new System.Windows.Forms.TreeNode("低效能供应商管理三级流程");
            System.Windows.Forms.TreeNode treeNode126 = new System.Windows.Forms.TreeNode("低效能供应商淘汰");
            System.Windows.Forms.TreeNode treeNode127 = new System.Windows.Forms.TreeNode("低效能供应商管理", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode124,
            treeNode125,
            treeNode126});
            System.Windows.Forms.TreeNode treeNode128 = new System.Windows.Forms.TreeNode("降成本计算");
            System.Windows.Forms.TreeNode treeNode129 = new System.Windows.Forms.TreeNode("降成本结果查询");
            System.Windows.Forms.TreeNode treeNode130 = new System.Windows.Forms.TreeNode("采购降成本", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode128,
            treeNode129});
            System.Windows.Forms.TreeNode treeNode131 = new System.Windows.Forms.TreeNode("供应商区分");
            System.Windows.Forms.TreeNode treeNode132 = new System.Windows.Forms.TreeNode("供应商清单");
            System.Windows.Forms.TreeNode treeNode133 = new System.Windows.Forms.TreeNode("供应商价值", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode131,
            treeNode132});
            System.Windows.Forms.TreeNode treeNode134 = new System.Windows.Forms.TreeNode("供应商前期准备");
            System.Windows.Forms.TreeNode treeNode135 = new System.Windows.Forms.TreeNode("初步筛选");
            System.Windows.Forms.TreeNode treeNode136 = new System.Windows.Forms.TreeNode("协议与承诺");
            System.Windows.Forms.TreeNode treeNode137 = new System.Windows.Forms.TreeNode("供应商能力评估");
            System.Windows.Forms.TreeNode treeNode138 = new System.Windows.Forms.TreeNode("SWOT分析");
            System.Windows.Forms.TreeNode treeNode139 = new System.Windows.Forms.TreeNode("改进/验收");
            System.Windows.Forms.TreeNode treeNode140 = new System.Windows.Forms.TreeNode("筛选");
            System.Windows.Forms.TreeNode treeNode141 = new System.Windows.Forms.TreeNode("主审");
            System.Windows.Forms.TreeNode treeNode142 = new System.Windows.Forms.TreeNode("评审员");
            System.Windows.Forms.TreeNode treeNode143 = new System.Windows.Forms.TreeNode("预评", new System.Windows.Forms.TreeNode[] {
            treeNode141,
            treeNode142});
            System.Windows.Forms.TreeNode treeNode144 = new System.Windows.Forms.TreeNode("主审");
            System.Windows.Forms.TreeNode treeNode145 = new System.Windows.Forms.TreeNode("评审员");
            System.Windows.Forms.TreeNode treeNode146 = new System.Windows.Forms.TreeNode("现场评估", new System.Windows.Forms.TreeNode[] {
            treeNode144,
            treeNode145});
            System.Windows.Forms.TreeNode treeNode147 = new System.Windows.Forms.TreeNode("决策");
            System.Windows.Forms.TreeNode treeNode148 = new System.Windows.Forms.TreeNode("准入");
            System.Windows.Forms.TreeNode treeNode149 = new System.Windows.Forms.TreeNode("已准入供应商", 1, 1);
            System.Windows.Forms.TreeNode treeNode150 = new System.Windows.Forms.TreeNode("供应山改善");
            System.Windows.Forms.TreeNode treeNode151 = new System.Windows.Forms.TreeNode("文件上传");
            System.Windows.Forms.TreeNode treeNode152 = new System.Windows.Forms.TreeNode("供应商认证", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode134,
            treeNode135,
            treeNode136,
            treeNode137,
            treeNode138,
            treeNode139,
            treeNode140,
            treeNode143,
            treeNode146,
            treeNode147,
            treeNode148,
            treeNode149,
            treeNode150,
            treeNode151});
            System.Windows.Forms.TreeNode treeNode153 = new System.Windows.Forms.TreeNode("供应商用户管理");
            System.Windows.Forms.TreeNode treeNode154 = new System.Windows.Forms.TreeNode("供应商管理", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode113,
            treeNode120,
            treeNode123,
            treeNode127,
            treeNode130,
            treeNode133,
            treeNode152,
            treeNode153});
            System.Windows.Forms.TreeNode treeNode155 = new System.Windows.Forms.TreeNode("物料的ABC类分析");
            System.Windows.Forms.TreeNode treeNode156 = new System.Windows.Forms.TreeNode("供应商的ABC类分析");
            System.Windows.Forms.TreeNode treeNode157 = new System.Windows.Forms.TreeNode("采购监控分析", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode155,
            treeNode156});
            System.Windows.Forms.TreeNode treeNode158 = new System.Windows.Forms.TreeNode("采购花费分布");
            System.Windows.Forms.TreeNode treeNode159 = new System.Windows.Forms.TreeNode("成本结构分析");
            System.Windows.Forms.TreeNode treeNode160 = new System.Windows.Forms.TreeNode("成本比较分析");
            System.Windows.Forms.TreeNode treeNode161 = new System.Windows.Forms.TreeNode("成本趋势分析");
            System.Windows.Forms.TreeNode treeNode162 = new System.Windows.Forms.TreeNode("采购花费分析", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode158,
            treeNode159,
            treeNode160,
            treeNode161});
            System.Windows.Forms.TreeNode treeNode163 = new System.Windows.Forms.TreeNode("价格分析");
            System.Windows.Forms.TreeNode treeNode164 = new System.Windows.Forms.TreeNode("供应商竞价分析");
            System.Windows.Forms.TreeNode treeNode165 = new System.Windows.Forms.TreeNode("寻源分析", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode163,
            treeNode164});
            System.Windows.Forms.TreeNode treeNode166 = new System.Windows.Forms.TreeNode("合同管理分析");
            System.Windows.Forms.TreeNode treeNode167 = new System.Windows.Forms.TreeNode("合同执行分析", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode166});
            System.Windows.Forms.TreeNode treeNode168 = new System.Windows.Forms.TreeNode("供应商对比雷达图");
            System.Windows.Forms.TreeNode treeNode169 = new System.Windows.Forms.TreeNode("供应商趋势报表");
            System.Windows.Forms.TreeNode treeNode170 = new System.Windows.Forms.TreeNode("供应商得分排名");
            System.Windows.Forms.TreeNode treeNode171 = new System.Windows.Forms.TreeNode("供应商绩效分析", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode168,
            treeNode169,
            treeNode170});
            System.Windows.Forms.TreeNode treeNode172 = new System.Windows.Forms.TreeNode("库龄分析明细表");
            System.Windows.Forms.TreeNode treeNode173 = new System.Windows.Forms.TreeNode("库存物料ABC类分析");
            System.Windows.Forms.TreeNode treeNode174 = new System.Windows.Forms.TreeNode("库存分析", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode172,
            treeNode173});
            System.Windows.Forms.TreeNode treeNode175 = new System.Windows.Forms.TreeNode("供应商分析");
            System.Windows.Forms.TreeNode treeNode176 = new System.Windows.Forms.TreeNode("供应商依赖因素分析");
            System.Windows.Forms.TreeNode treeNode177 = new System.Windows.Forms.TreeNode("定制");
            System.Windows.Forms.TreeNode treeNode178 = new System.Windows.Forms.TreeNode("报表分析", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode157,
            treeNode162,
            treeNode165,
            treeNode167,
            treeNode171,
            treeNode174,
            treeNode175,
            treeNode176,
            treeNode177});
            System.Windows.Forms.TreeNode treeNode179 = new System.Windows.Forms.TreeNode("库存类型");
            System.Windows.Forms.TreeNode treeNode180 = new System.Windows.Forms.TreeNode("库存状态");
            System.Windows.Forms.TreeNode treeNode181 = new System.Windows.Forms.TreeNode("查看");
            System.Windows.Forms.TreeNode treeNode182 = new System.Windows.Forms.TreeNode("事务/事件");
            System.Windows.Forms.TreeNode treeNode183 = new System.Windows.Forms.TreeNode("移动类型", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode181,
            treeNode182});
            System.Windows.Forms.TreeNode treeNode184 = new System.Windows.Forms.TreeNode("基础配置", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode179,
            treeNode180,
            treeNode183});
            System.Windows.Forms.TreeNode treeNode185 = new System.Windows.Forms.TreeNode("库存", 0, 0);
            System.Windows.Forms.TreeNode treeNode186 = new System.Windows.Forms.TreeNode("货物移动", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode185});
            System.Windows.Forms.TreeNode treeNode187 = new System.Windows.Forms.TreeNode("显示");
            System.Windows.Forms.TreeNode treeNode188 = new System.Windows.Forms.TreeNode("更改");
            System.Windows.Forms.TreeNode treeNode189 = new System.Windows.Forms.TreeNode("取消");
            System.Windows.Forms.TreeNode treeNode190 = new System.Windows.Forms.TreeNode("查询");
            System.Windows.Forms.TreeNode treeNode191 = new System.Windows.Forms.TreeNode("物料凭证", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode187,
            treeNode188,
            treeNode189,
            treeNode190});
            System.Windows.Forms.TreeNode treeNode192 = new System.Windows.Forms.TreeNode("库存总览");
            System.Windows.Forms.TreeNode treeNode193 = new System.Windows.Forms.TreeNode("工厂库存");
            System.Windows.Forms.TreeNode treeNode194 = new System.Windows.Forms.TreeNode("仓库库存");
            System.Windows.Forms.TreeNode treeNode195 = new System.Windows.Forms.TreeNode("寄售库存");
            System.Windows.Forms.TreeNode treeNode196 = new System.Windows.Forms.TreeNode("在途库存");
            System.Windows.Forms.TreeNode treeNode197 = new System.Windows.Forms.TreeNode("收发存报表", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode192,
            treeNode193,
            treeNode194,
            treeNode195,
            treeNode196});
            System.Windows.Forms.TreeNode treeNode198 = new System.Windows.Forms.TreeNode("手动设置安全库存");
            System.Windows.Forms.TreeNode treeNode199 = new System.Windows.Forms.TreeNode("查看安全库存");
            System.Windows.Forms.TreeNode treeNode200 = new System.Windows.Forms.TreeNode("更新安全库存");
            System.Windows.Forms.TreeNode treeNode201 = new System.Windows.Forms.TreeNode("计算安全库存");
            System.Windows.Forms.TreeNode treeNode202 = new System.Windows.Forms.TreeNode("输入需求量");
            System.Windows.Forms.TreeNode treeNode203 = new System.Windows.Forms.TreeNode("系统计算安全库存");
            System.Windows.Forms.TreeNode treeNode204 = new System.Windows.Forms.TreeNode("安全库存", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode198,
            treeNode199,
            treeNode200,
            treeNode201,
            treeNode202,
            treeNode203});
            System.Windows.Forms.TreeNode treeNode205 = new System.Windows.Forms.TreeNode("集中创建盘点凭证");
            System.Windows.Forms.TreeNode treeNode206 = new System.Windows.Forms.TreeNode("单个创建盘点凭证");
            System.Windows.Forms.TreeNode treeNode207 = new System.Windows.Forms.TreeNode("盘点冻结与解冻");
            System.Windows.Forms.TreeNode treeNode208 = new System.Windows.Forms.TreeNode("输入盘点结果");
            System.Windows.Forms.TreeNode treeNode209 = new System.Windows.Forms.TreeNode("差异清单一览");
            System.Windows.Forms.TreeNode treeNode210 = new System.Windows.Forms.TreeNode("盘点数据更新");
            System.Windows.Forms.TreeNode treeNode211 = new System.Windows.Forms.TreeNode("年度盘点", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode205,
            treeNode206,
            treeNode207,
            treeNode208,
            treeNode209,
            treeNode210});
            System.Windows.Forms.TreeNode treeNode212 = new System.Windows.Forms.TreeNode("显示物料清单");
            System.Windows.Forms.TreeNode treeNode213 = new System.Windows.Forms.TreeNode("差异调整");
            System.Windows.Forms.TreeNode treeNode214 = new System.Windows.Forms.TreeNode("显示差异调整");
            System.Windows.Forms.TreeNode treeNode215 = new System.Windows.Forms.TreeNode("取消差异调整");
            System.Windows.Forms.TreeNode treeNode216 = new System.Windows.Forms.TreeNode("循环盘点", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode212,
            treeNode213,
            treeNode214,
            treeNode215});
            System.Windows.Forms.TreeNode treeNode217 = new System.Windows.Forms.TreeNode("库存盘点", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode211,
            treeNode216});
            System.Windows.Forms.TreeNode treeNode218 = new System.Windows.Forms.TreeNode("质检评分");
            System.Windows.Forms.TreeNode treeNode219 = new System.Windows.Forms.TreeNode("装运评分");
            System.Windows.Forms.TreeNode treeNode220 = new System.Windows.Forms.TreeNode("非原材料拒收数量");
            System.Windows.Forms.TreeNode treeNode221 = new System.Windows.Forms.TreeNode("收货评分", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode218,
            treeNode219,
            treeNode220});
            System.Windows.Forms.TreeNode treeNode222 = new System.Windows.Forms.TreeNode("设置提前提醒天数");
            System.Windows.Forms.TreeNode treeNode223 = new System.Windows.Forms.TreeNode("到期物料提醒");
            System.Windows.Forms.TreeNode treeNode224 = new System.Windows.Forms.TreeNode("到期提醒", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode222,
            treeNode223});
            System.Windows.Forms.TreeNode treeNode225 = new System.Windows.Forms.TreeNode("库存管理", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode184,
            treeNode186,
            treeNode191,
            treeNode197,
            treeNode204,
            treeNode217,
            treeNode221,
            treeNode224});
            System.Windows.Forms.TreeNode treeNode226 = new System.Windows.Forms.TreeNode("文档管理", 1, 1);
            System.Windows.Forms.TreeNode treeNode227 = new System.Windows.Forms.TreeNode("订单");
            System.Windows.Forms.TreeNode treeNode228 = new System.Windows.Forms.TreeNode("询价单");
            System.Windows.Forms.TreeNode treeNode229 = new System.Windows.Forms.TreeNode("招标");
            System.Windows.Forms.TreeNode treeNode230 = new System.Windows.Forms.TreeNode("谈判");
            System.Windows.Forms.TreeNode treeNode231 = new System.Windows.Forms.TreeNode("合同管理");
            System.Windows.Forms.TreeNode treeNode232 = new System.Windows.Forms.TreeNode("商务", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode227,
            treeNode228,
            treeNode229,
            treeNode230,
            treeNode231});
            System.Windows.Forms.TreeNode treeNode233 = new System.Windows.Forms.TreeNode("质检报告");
            System.Windows.Forms.TreeNode treeNode234 = new System.Windows.Forms.TreeNode("质量", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode233});
            System.Windows.Forms.TreeNode treeNode235 = new System.Windows.Forms.TreeNode("我的业务", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode232,
            treeNode234});
            System.Windows.Forms.TreeNode treeNode236 = new System.Windows.Forms.TreeNode("供应商基本信息");
            System.Windows.Forms.TreeNode treeNode237 = new System.Windows.Forms.TreeNode("协议");
            System.Windows.Forms.TreeNode treeNode238 = new System.Windows.Forms.TreeNode("付款条件");
            System.Windows.Forms.TreeNode treeNode239 = new System.Windows.Forms.TreeNode("平衡积分卡");
            System.Windows.Forms.TreeNode treeNode240 = new System.Windows.Forms.TreeNode("花费情况");
            System.Windows.Forms.TreeNode treeNode241 = new System.Windows.Forms.TreeNode("会议纪要");
            System.Windows.Forms.TreeNode treeNode242 = new System.Windows.Forms.TreeNode("查看", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode236,
            treeNode237,
            treeNode238,
            treeNode239,
            treeNode240,
            treeNode241});
            System.Windows.Forms.TreeNode treeNode243 = new System.Windows.Forms.TreeNode("供应商联系方式");
            System.Windows.Forms.TreeNode treeNode244 = new System.Windows.Forms.TreeNode("年营业额");
            System.Windows.Forms.TreeNode treeNode245 = new System.Windows.Forms.TreeNode("供应山评估");
            System.Windows.Forms.TreeNode treeNode246 = new System.Windows.Forms.TreeNode("供应商网址");
            System.Windows.Forms.TreeNode treeNode247 = new System.Windows.Forms.TreeNode("CSR评估");
            System.Windows.Forms.TreeNode treeNode248 = new System.Windows.Forms.TreeNode("更新", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode243,
            treeNode244,
            treeNode245,
            treeNode246,
            treeNode247});
            System.Windows.Forms.TreeNode treeNode249 = new System.Windows.Forms.TreeNode("我的表现", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode242,
            treeNode248});
            System.Windows.Forms.TreeNode treeNode250 = new System.Windows.Forms.TreeNode("供应商门户权限管理", 1, 0, new System.Windows.Forms.TreeNode[] {
            treeNode235,
            treeNode249});
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FunctionLevelPermissionManageForm));
            this.topPanel = new System.Windows.Forms.Panel();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.permissionObject = new System.Windows.Forms.TreeView();
            this.exitBtn = new System.Windows.Forms.Button();
            this.authorizeBtn = new System.Windows.Forms.Button();
            this.cancelCurrentAllBtn = new System.Windows.Forms.Button();
            this.selectCurrentAllBtn = new System.Windows.Forms.Button();
            this.permissionListBtn = new System.Windows.Forms.Button();
            this.cancelAllBtn = new System.Windows.Forms.Button();
            this.authorizeAllBtn = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.permissionTable = new System.Windows.Forms.DataGridView();
            this.Permission_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Permission_Name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.personalPermission = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.groupPermission = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.rolePermission = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.statusBar = new System.Windows.Forms.TextBox();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.topPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.permissionTable)).BeginInit();
            this.SuspendLayout();
            // 
            // topPanel
            // 
            this.topPanel.Controls.Add(this.linkLabel1);
            this.topPanel.Controls.Add(this.permissionObject);
            this.topPanel.Controls.Add(this.exitBtn);
            this.topPanel.Controls.Add(this.authorizeBtn);
            this.topPanel.Controls.Add(this.cancelCurrentAllBtn);
            this.topPanel.Controls.Add(this.selectCurrentAllBtn);
            this.topPanel.Controls.Add(this.permissionListBtn);
            this.topPanel.Controls.Add(this.cancelAllBtn);
            this.topPanel.Controls.Add(this.authorizeAllBtn);
            this.topPanel.Controls.Add(this.label2);
            this.topPanel.Controls.Add(this.label1);
            this.topPanel.Controls.Add(this.permissionTable);
            this.topPanel.Controls.Add(this.statusBar);
            this.topPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.topPanel.Location = new System.Drawing.Point(0, 0);
            this.topPanel.Name = "topPanel";
            this.topPanel.Size = new System.Drawing.Size(780, 468);
            this.topPanel.TabIndex = 0;
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Location = new System.Drawing.Point(722, 9);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(32, 17);
            this.linkLabel1.TabIndex = 14;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "帮助";
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // permissionObject
            // 
            this.permissionObject.Location = new System.Drawing.Point(14, 34);
            this.permissionObject.Name = "permissionObject";
            treeNode1.Name = "NewMaterialData";
            treeNode1.Text = "新建物料数据";
            treeNode2.Name = "MaterialBasicView";
            treeNode2.Text = "基本视图";
            treeNode3.Name = "MaterialAccountView";
            treeNode3.Text = "会计视图";
            treeNode4.Name = "MaterialBuyerView";
            treeNode4.Text = "采购视图";
            treeNode5.Name = "MaterialStockView";
            treeNode5.Text = "存储视图";
            treeNode6.Name = "NewBatch";
            treeNode6.Text = "建立批次";
            treeNode7.Name = "QuryBatch";
            treeNode7.Text = "查询批次";
            treeNode8.ImageIndex = 1;
            treeNode8.Name = "Batch";
            treeNode8.SelectedImageIndex = 1;
            treeNode8.Text = "批次管理";
            treeNode9.Name = "StockMPN";
            treeNode9.Text = "库存管理MPN";
            treeNode10.Name = "NonStockMPN";
            treeNode10.Text = "非库存管理MPN";
            treeNode11.ImageIndex = 1;
            treeNode11.Name = "MPN";
            treeNode11.SelectedImageIndex = 1;
            treeNode11.Text = "制造商物料管理";
            treeNode12.ImageIndex = 1;
            treeNode12.Name = "MaterialData";
            treeNode12.SelectedImageIndex = 1;
            treeNode12.Text = "物料主数据";
            treeNode13.Name = "NewSupplier";
            treeNode13.Text = "新建供应商";
            treeNode14.Name = "SupplierBasicView";
            treeNode14.Text = "基本视图";
            treeNode15.Name = "SupplierBuyerOrgView";
            treeNode15.Text = "采购组织视图";
            treeNode16.Name = "SupplierCompanyCodeView";
            treeNode16.Text = "公司代码视图";
            treeNode17.ImageIndex = 1;
            treeNode17.Name = "SupplierData";
            treeNode17.SelectedImageIndex = 1;
            treeNode17.Text = "供应商主数据";
            treeNode18.Name = "NewVocher";
            treeNode18.Text = "生成会计凭证";
            treeNode19.Name = "QuryVocher";
            treeNode19.Text = "查询会计凭证";
            treeNode20.Name = "QuryAccount";
            treeNode20.Text = "查询科目信息";
            treeNode21.ImageIndex = 1;
            treeNode21.Name = "AccountData";
            treeNode21.SelectedImageIndex = 1;
            treeNode21.Text = "财务主数据";
            treeNode22.Name = "MaintainMaterialGroup";
            treeNode22.Text = "维护物料组";
            treeNode23.Name = "MaterialTypeA";
            treeNode23.Text = "物料大分类";
            treeNode24.Name = "MaterialTypeB";
            treeNode24.Text = "物料小分类";
            treeNode25.Name = "MaterialTypeC";
            treeNode25.Text = "物料精确分类";
            treeNode26.ImageIndex = 1;
            treeNode26.Name = "MaintainMaterialType";
            treeNode26.SelectedImageIndex = 1;
            treeNode26.Text = "维护物料类型";
            treeNode27.Name = "MaintainCurrency";
            treeNode27.Text = "维护货币";
            treeNode28.Name = "MaintainCountry";
            treeNode28.Text = "维护国家信息";
            treeNode29.Name = "MaintainPaymentClause";
            treeNode29.Text = "维护付款条件";
            treeNode30.Name = "MaintainMeasurement";
            treeNode30.Text = "维护计量单位";
            treeNode31.Name = "MaintainEvaluationClass";
            treeNode31.Text = "维护评估类";
            treeNode32.Name = "MaintainCompanyCode";
            treeNode32.Text = "维护公司代码";
            treeNode33.Name = "MaintainFactory";
            treeNode33.Text = "维护工厂";
            treeNode34.Name = "MaintainStock";
            treeNode34.Text = "维护库存地";
            treeNode35.Name = "MaintainBuyerGroup";
            treeNode35.Text = "维护采购组";
            treeNode36.Name = "MaintainBuyerOrganization";
            treeNode36.Text = "维护采购组织";
            treeNode37.Name = "MaintainDivision";
            treeNode37.Text = "维护产品组";
            treeNode38.Name = "MaintainTradeClause";
            treeNode38.Text = "维护交付条件";
            treeNode39.ImageIndex = 1;
            treeNode39.Name = "GeneralSettings";
            treeNode39.SelectedImageIndex = 1;
            treeNode39.Text = "一般设置";
            treeNode40.ImageIndex = 1;
            treeNode40.Name = "MainData";
            treeNode40.SelectedImageIndex = 1;
            treeNode40.Text = "数据管理";
            treeNode41.Name = "showDemand";
            treeNode41.Text = "查看计划";
            treeNode42.ImageIndex = 1;
            treeNode42.Name = "demandList";
            treeNode42.SelectedImageIndex = 1;
            treeNode42.Text = "计划列表";
            treeNode43.Name = "newStandardDemand";
            treeNode43.Text = "新建";
            treeNode44.Name = "maintenancePlan";
            treeNode44.Text = "修改";
            treeNode45.Name = "more_Info";
            treeNode45.Text = "查看";
            treeNode46.Name = "delete_Info";
            treeNode46.Text = "删除";
            treeNode47.ImageIndex = 1;
            treeNode47.Name = "apply_Dep";
            treeNode47.SelectedImageIndex = 1;
            treeNode47.Text = "申请部门";
            treeNode48.Name = "auditSubmitStandard";
            treeNode48.Text = "审核";
            treeNode49.Name = "aggregate_Info";
            treeNode49.Text = "汇总";
            treeNode50.Name = "createPurchasing";
            treeNode50.Text = "生成";
            treeNode51.Name = "source";
            treeNode51.Text = "寻源";
            treeNode52.ImageIndex = 1;
            treeNode52.Name = "buy_Dep";
            treeNode52.SelectedImageIndex = 1;
            treeNode52.Text = "采购部门";
            treeNode53.ImageIndex = 1;
            treeNode53.Name = "procurementMM";
            treeNode53.SelectedImageIndex = 1;
            treeNode53.Text = "计划管理";
            treeNode54.Name = "newSourceList";
            treeNode54.Text = "创建源清单";
            treeNode55.Name = "maintenanceSourceList";
            treeNode55.Text = "维护源清单";
            treeNode56.ImageIndex = 1;
            treeNode56.Name = "sourceList";
            treeNode56.SelectedImageIndex = 1;
            treeNode56.Text = "源清单";
            treeNode57.Name = "createSource";
            treeNode57.Text = "创建寻源";
            treeNode58.Name = "sourceMaintain";
            treeNode58.Text = "维护寻源";
            treeNode59.Name = "viewInquiry";
            treeNode59.Text = "查看询价";
            treeNode60.Name = "newInquiry";
            treeNode60.Text = "创建询价";
            treeNode61.Name = "editInquiry";
            treeNode61.Text = "更改询价";
            treeNode62.ImageIndex = 1;
            treeNode62.Name = "allInquiries";
            treeNode62.SelectedImageIndex = 1;
            treeNode62.Text = "询价";
            treeNode63.Name = "offerInfo";
            treeNode63.Text = "查看报价";
            treeNode64.Name = "executeInqueryPrice";
            treeNode64.Text = "执行报价";
            treeNode65.ImageIndex = 1;
            treeNode65.Name = "offerList";
            treeNode65.SelectedImageIndex = 1;
            treeNode65.Text = "报价";
            treeNode66.Name = "compareInfo";
            treeNode66.Text = "查看比价";
            treeNode67.Name = "executeCompare";
            treeNode67.Text = "执行比价";
            treeNode68.ImageIndex = 1;
            treeNode68.Name = "compareList";
            treeNode68.SelectedImageIndex = 1;
            treeNode68.Text = "比价";
            treeNode69.ImageIndex = 1;
            treeNode69.Name = "inquiry";
            treeNode69.SelectedImageIndex = 1;
            treeNode69.Text = "询价";
            treeNode70.Name = "showBids";
            treeNode70.Text = "查看招标";
            treeNode71.Name = "newBidding";
            treeNode71.Text = "创建招标";
            treeNode72.Name = "changeTender";
            treeNode72.Text = "修改招标";
            treeNode73.Name = "offerBidding";
            treeNode73.Text = "处理投标";
            treeNode74.Name = "compareBidding";
            treeNode74.Text = "进行评标";
            treeNode75.Name = "checkBid";
            treeNode75.Text = "审批招标";
            treeNode76.ImageIndex = 1;
            treeNode76.Name = "bidding";
            treeNode76.SelectedImageIndex = 1;
            treeNode76.Text = "招标";
            treeNode77.Name = "newAuction";
            treeNode77.Text = "竞价";
            treeNode78.Name = "offerAuction";
            treeNode78.Text = "竞拍";
            treeNode79.ImageIndex = 1;
            treeNode79.Name = "auction";
            treeNode79.SelectedImageIndex = 1;
            treeNode79.Text = "拍卖";
            treeNode80.Name = "sourceRecord";
            treeNode80.Text = "采购信息维护";
            treeNode81.Name = "submitTechnical";
            treeNode81.Text = "招标技术提案";
            treeNode82.Name = "firstBid";
            treeNode82.Text = "一次招标";
            treeNode83.Name = "supplierReplyInformation";
            treeNode83.Text = "供应商回复信息";
            treeNode84.Name = "supplierReply";
            treeNode84.Text = "净现值回复";
            treeNode85.ImageIndex = 1;
            treeNode85.Name = "twiceMethod";
            treeNode85.SelectedImageIndex = 1;
            treeNode85.Text = "两段式招标";
            treeNode86.ImageIndex = 1;
            treeNode86.Name = "generalMethod";
            treeNode86.SelectedImageIndex = 1;
            treeNode86.Text = "普通招标";
            treeNode87.ImageIndex = 1;
            treeNode87.Name = "sourceMM";
            treeNode87.SelectedImageIndex = 1;
            treeNode87.Text = "寻源管理";
            treeNode88.Name = "manageContractTemplateNode";
            treeNode88.Text = "管理";
            treeNode89.ImageIndex = 1;
            treeNode89.Name = "contractTemplateNode";
            treeNode89.SelectedImageIndex = 1;
            treeNode89.Text = "合同模板";
            treeNode90.Name = "manageContractTextNode";
            treeNode90.Text = "管理";
            treeNode91.ImageIndex = 1;
            treeNode91.Name = "contractTextNode";
            treeNode91.SelectedImageIndex = 1;
            treeNode91.Text = "合同文本";
            treeNode92.ImageIndex = 1;
            treeNode92.Name = "contractManageNode";
            treeNode92.SelectedImageIndex = 1;
            treeNode92.Text = "合同管理";
            treeNode93.ImageIndex = 0;
            treeNode93.Name = "manageAgreementContractNode";
            treeNode93.Text = "管理";
            treeNode94.ImageIndex = 1;
            treeNode94.Name = "agreementContractNode";
            treeNode94.SelectedImageIndex = 1;
            treeNode94.Text = "合同";
            treeNode95.ImageIndex = 0;
            treeNode95.Name = "manageSchedulingAgreementNode";
            treeNode95.Text = "管理";
            treeNode96.ImageIndex = 1;
            treeNode96.Name = "ScheduleAgreementNode";
            treeNode96.SelectedImageIndex = 1;
            treeNode96.Text = "计划协议";
            treeNode97.ImageIndex = 1;
            treeNode97.Name = "outlineAgreementNode";
            treeNode97.SelectedImageIndex = 1;
            treeNode97.Text = "框架协议";
            treeNode98.ImageKey = "Table4.png";
            treeNode98.Name = "quotaArrangementManageNode";
            treeNode98.Text = "管理";
            treeNode99.ImageIndex = 1;
            treeNode99.Name = "quotaArrangementNode";
            treeNode99.SelectedImageIndex = 1;
            treeNode99.Text = "配额协议";
            treeNode100.ImageIndex = 1;
            treeNode100.Name = "SelfServicePurchasingNode";
            treeNode100.SelectedImageIndex = 1;
            treeNode100.Text = "自助采购";
            treeNode101.ImageKey = "Table4.png";
            treeNode101.Name = "POManageNode";
            treeNode101.Text = "订单管理";
            treeNode102.Name = "InvoiceAndPaymentNode";
            treeNode102.Text = "发票管理";
            treeNode103.ImageIndex = 1;
            treeNode103.Name = "节点6";
            treeNode103.SelectedImageIndex = 1;
            treeNode103.Text = "订单执行";
            treeNode104.Name = "SPAddNode";
            treeNode104.Text = "手工维护";
            treeNode105.Name = "SPAutoNode";
            treeNode105.Text = "自动评估";
            treeNode106.Name = "SPBatchAddNode";
            treeNode106.Text = "批量评估";
            treeNode107.Name = "SPResultNode";
            treeNode107.Text = "结果显示";
            treeNode108.Name = "SPListNode";
            treeNode108.Text = "清单显示";
            treeNode109.Name = "InterForm";
            treeNode109.Text = "一般服务";
            treeNode110.Name = "ExService";
            treeNode110.Text = "外部服务";
            treeNode111.Name = "IntialServiceRate";
            treeNode111.Text = "维护服务占比";
            treeNode112.Name = "serivceEval";
            treeNode112.Text = "服务评估";
            treeNode113.ImageIndex = 1;
            treeNode113.Name = "SupplierPerformanceNode";
            treeNode113.SelectedImageIndex = 1;
            treeNode113.Text = "绩效评估";
            treeNode114.Name = "SystemProcessAuditNode";
            treeNode114.Text = "体系/过程审核";
            treeNode115.Name = "ProductAuditNode";
            treeNode115.Text = "产品审核";
            treeNode116.ImageIndex = 1;
            treeNode116.Name = "QualityAuditNode";
            treeNode116.SelectedImageIndex = 1;
            treeNode116.Text = "质量审核";
            treeNode117.Name = "ComplaintRejectAddNode";
            treeNode117.Text = "抱怨/拒绝情况录入";
            treeNode118.Name = "ServiceAddNode";
            treeNode118.Text = "服务情况录入";
            treeNode119.Name = "QuestionaireNode";
            treeNode119.Text = "收货问卷调查";
            treeNode120.ImageIndex = 1;
            treeNode120.Name = "SPDataAddNode";
            treeNode120.SelectedImageIndex = 1;
            treeNode120.Text = "绩效评估数据维护";
            treeNode121.Name = "SupplierClassifyNode";
            treeNode121.Text = "执行分级";
            treeNode122.Name = "ClassificationResultNode";
            treeNode122.Text = "分级查看";
            treeNode123.ImageIndex = 1;
            treeNode123.Name = "SupplierClassifyNode";
            treeNode123.SelectedImageIndex = 1;
            treeNode123.Text = "供应商分级";
            treeNode124.Name = "LPSWarningNode";
            treeNode124.Text = "低效能供应商警告";
            treeNode125.Name = "LPSProcessNode";
            treeNode125.Text = "低效能供应商管理三级流程";
            treeNode126.Name = "LPSOutNode";
            treeNode126.Text = "低效能供应商淘汰";
            treeNode127.ImageIndex = 1;
            treeNode127.Name = "LPSNode";
            treeNode127.SelectedImageIndex = 1;
            treeNode127.Text = "低效能供应商管理";
            treeNode128.Name = "CostReductionCalculateNode";
            treeNode128.Text = "降成本计算";
            treeNode129.Name = "CostReductionResultNode";
            treeNode129.Text = "降成本结果查询";
            treeNode130.ImageIndex = 1;
            treeNode130.Name = "CostReductionNode";
            treeNode130.SelectedImageIndex = 1;
            treeNode130.Text = "采购降成本";
            treeNode131.Name = "BusinessValueAssessNode";
            treeNode131.Text = "供应商区分";
            treeNode132.Name = "SupplierTrackNode";
            treeNode132.Text = "供应商清单";
            treeNode133.ImageIndex = 1;
            treeNode133.Name = "SupplierValueNode";
            treeNode133.SelectedImageIndex = 1;
            treeNode133.Text = "供应商价值";
            treeNode134.Name = "certificationPreparationNode";
            treeNode134.Text = "供应商前期准备";
            treeNode135.Name = "initialFilterNode";
            treeNode135.Text = "初步筛选";
            treeNode136.Name = "agreementAndCommitmentsNode";
            treeNode136.Text = "协议与承诺";
            treeNode137.Name = "supplierEvaluationNode";
            treeNode137.Text = "供应商能力评估";
            treeNode138.Name = "SWOTNode";
            treeNode138.Text = "SWOT分析";
            treeNode139.Name = "imporvementAndCheckNode";
            treeNode139.Text = "改进/验收";
            treeNode140.Name = "selectSupplier";
            treeNode140.Text = "筛选";
            treeNode141.Name = "PerMainEval";
            treeNode141.Text = "主审";
            treeNode142.Name = "PerEval";
            treeNode142.Text = "评审员";
            treeNode143.Name = "预评";
            treeNode143.Text = "预评";
            treeNode144.Name = "LocationEvaluateNode";
            treeNode144.Text = "主审";
            treeNode145.Name = "localEvaluationSingleNode";
            treeNode145.Text = "评审员";
            treeNode146.Name = "现场评估";
            treeNode146.Text = "现场评估";
            treeNode147.Name = "Decison";
            treeNode147.Text = "决策";
            treeNode148.Name = "certificationFinished";
            treeNode148.Text = "准入";
            treeNode149.ImageIndex = 1;
            treeNode149.Name = "passedSupplier";
            treeNode149.SelectedImageIndex = 1;
            treeNode149.Text = "已准入供应商";
            treeNode150.Name = "ImprovementReport";
            treeNode150.Text = "供应山改善";
            treeNode151.Name = "Fupload";
            treeNode151.Text = "文件上传";
            treeNode152.ImageIndex = 1;
            treeNode152.Name = "authenticationNode";
            treeNode152.SelectedImageIndex = 1;
            treeNode152.Text = "供应商认证";
            treeNode153.ImageKey = "(默认值)";
            treeNode153.Name = "userManageNode";
            treeNode153.Text = "供应商用户管理";
            treeNode154.ImageIndex = 1;
            treeNode154.Name = "节点8";
            treeNode154.SelectedImageIndex = 1;
            treeNode154.Text = "供应商管理";
            treeNode155.Name = "MaterialABCAnalysis";
            treeNode155.Tag = "100";
            treeNode155.Text = "物料的ABC类分析";
            treeNode155.ToolTipText = "物料的ABC类分析";
            treeNode156.Name = "SupplierABCAnalysis";
            treeNode156.Tag = "101";
            treeNode156.Text = "供应商的ABC类分析";
            treeNode157.ImageIndex = 1;
            treeNode157.Name = "PurchaseMonitor";
            treeNode157.SelectedImageIndex = 1;
            treeNode157.Tag = "100";
            treeNode157.Text = "采购监控分析";
            treeNode157.ToolTipText = "采购监控分析";
            treeNode158.Name = "PurchaseConstDistribution";
            treeNode158.Tag = "201";
            treeNode158.Text = "采购花费分布";
            treeNode158.ToolTipText = "采购花费分布";
            treeNode159.Name = "CostStructureAnalysis";
            treeNode159.Tag = "202";
            treeNode159.Text = "成本结构分析";
            treeNode159.ToolTipText = "成本结构分析";
            treeNode160.Name = "CostComparativeAnalysis";
            treeNode160.Tag = "203";
            treeNode160.Text = "成本比较分析";
            treeNode160.ToolTipText = "成本比较分析";
            treeNode161.Name = "CostTrenAnalysis";
            treeNode161.Tag = "204";
            treeNode161.Text = "成本趋势分析";
            treeNode161.ToolTipText = "成本趋势分析";
            treeNode162.ImageIndex = 1;
            treeNode162.Name = "PurchaseExpense";
            treeNode162.SelectedImageIndex = 1;
            treeNode162.Tag = "200";
            treeNode162.Text = "采购花费分析";
            treeNode162.ToolTipText = "采购花费分析";
            treeNode163.Name = "MaterialPriceAnalysis";
            treeNode163.Tag = "300";
            treeNode163.Text = "价格分析";
            treeNode163.ToolTipText = "价格分析";
            treeNode164.Name = "SupplierBiddingAnalysis";
            treeNode164.Tag = "301";
            treeNode164.Text = "供应商竞价分析";
            treeNode164.ToolTipText = "供应商竞价分析";
            treeNode165.ImageIndex = 1;
            treeNode165.Name = "SourseTracing";
            treeNode165.SelectedImageIndex = 1;
            treeNode165.Tag = "300";
            treeNode165.Text = "寻源分析";
            treeNode165.ToolTipText = "寻源分析";
            treeNode166.Name = "ConstractionManagement";
            treeNode166.Tag = "400";
            treeNode166.Text = "合同管理分析";
            treeNode166.ToolTipText = "合同管理分析";
            treeNode167.ImageIndex = 1;
            treeNode167.Name = "ConstractionExecutuon";
            treeNode167.SelectedImageIndex = 1;
            treeNode167.Tag = "400";
            treeNode167.Text = "合同执行分析";
            treeNode167.ToolTipText = "合同执行分析";
            treeNode168.Name = "SupplierComparativeAnalysis";
            treeNode168.Tag = "500";
            treeNode168.Text = "供应商对比雷达图";
            treeNode168.ToolTipText = "供应商对比雷达图";
            treeNode169.Name = "SupplierTrendAnalysis";
            treeNode169.Tag = "501";
            treeNode169.Text = "供应商趋势报表";
            treeNode169.ToolTipText = "供应商趋势报表";
            treeNode170.Name = "SupplierScoreRanking";
            treeNode170.Tag = "502";
            treeNode170.Text = "供应商得分排名";
            treeNode170.ToolTipText = "供应商得分排名";
            treeNode171.ImageIndex = 1;
            treeNode171.Name = "SupplierPerformance";
            treeNode171.SelectedImageIndex = 1;
            treeNode171.Tag = "500";
            treeNode171.Text = "供应商绩效分析";
            treeNode171.ToolTipText = "供应商绩效分析";
            treeNode172.Name = "StockAgeAnalysis";
            treeNode172.Tag = "600";
            treeNode172.Text = "库龄分析明细表";
            treeNode172.ToolTipText = "库龄分析明细表";
            treeNode173.Name = "StockABCAnalysis";
            treeNode173.Tag = "601";
            treeNode173.Text = "库存物料ABC类分析";
            treeNode173.ToolTipText = "库存物料ABC类分析";
            treeNode174.ImageIndex = 1;
            treeNode174.Name = "StockAnalysis";
            treeNode174.SelectedImageIndex = 1;
            treeNode174.Tag = "600";
            treeNode174.Text = "库存分析";
            treeNode174.ToolTipText = "库存分析";
            treeNode175.Name = "SupplierAnalysis";
            treeNode175.Tag = "700";
            treeNode175.Text = "供应商分析";
            treeNode175.ToolTipText = "供应商分析";
            treeNode176.Name = "SuppplierDependentFactors";
            treeNode176.Tag = "800";
            treeNode176.Text = "供应商依赖因素分析";
            treeNode176.ToolTipText = "供应商依赖因素分析";
            treeNode177.Name = "Customized";
            treeNode177.Tag = "900";
            treeNode177.Text = "定制";
            treeNode177.ToolTipText = "定制";
            treeNode178.ImageIndex = 1;
            treeNode178.Name = "节点9";
            treeNode178.SelectedImageIndex = 1;
            treeNode178.Text = "报表分析";
            treeNode179.Name = "node_StockType";
            treeNode179.Tag = "10101";
            treeNode179.Text = "库存类型";
            treeNode180.Name = "node_StockState";
            treeNode180.Tag = "10102";
            treeNode180.Text = "库存状态";
            treeNode181.Name = "FindMoveType";
            treeNode181.Text = "查看";
            treeNode181.ToolTipText = "查看";
            treeNode182.Name = "TransactionEvent";
            treeNode182.Text = "事务/事件";
            treeNode182.ToolTipText = "事务/事件";
            treeNode183.ImageIndex = 1;
            treeNode183.Name = "MoveType";
            treeNode183.SelectedImageIndex = 1;
            treeNode183.Text = "移动类型";
            treeNode183.ToolTipText = "移动类型";
            treeNode184.ImageIndex = 1;
            treeNode184.Name = "基础配置";
            treeNode184.SelectedImageIndex = 1;
            treeNode184.Text = "基础配置";
            treeNode185.ImageIndex = 0;
            treeNode185.Name = "Receiving";
            treeNode185.SelectedImageIndex = 0;
            treeNode185.Text = "库存";
            treeNode185.ToolTipText = "库存";
            treeNode186.ImageIndex = 1;
            treeNode186.Name = "MaterialMove";
            treeNode186.SelectedImageIndex = 1;
            treeNode186.Tag = "10";
            treeNode186.Text = "货物移动";
            treeNode186.ToolTipText = "货物移动";
            treeNode187.Name = "DisplayDocument";
            treeNode187.Text = "显示";
            treeNode187.ToolTipText = "显示";
            treeNode188.Name = "ChangeDocument";
            treeNode188.Text = "更改";
            treeNode188.ToolTipText = "更改";
            treeNode189.Name = "CancelDocument";
            treeNode189.Text = "取消";
            treeNode189.ToolTipText = "取消";
            treeNode190.Name = "QueryDocument";
            treeNode190.Text = "查询";
            treeNode190.ToolTipText = "查询";
            treeNode191.ImageIndex = 1;
            treeNode191.Name = "MaterialDoc";
            treeNode191.SelectedImageIndex = 1;
            treeNode191.Text = "物料凭证";
            treeNode191.ToolTipText = "物料凭证";
            treeNode192.Name = "StockOverview";
            treeNode192.Text = "库存总览";
            treeNode193.Name = "FactoryStock";
            treeNode193.Text = "工厂库存";
            treeNode194.Name = "StockStock";
            treeNode194.Text = "仓库库存";
            treeNode195.Name = "ConsignmentStock";
            treeNode195.Text = "寄售库存";
            treeNode196.Name = "TransitStock";
            treeNode196.Text = "在途库存";
            treeNode197.ImageIndex = 1;
            treeNode197.Name = "Report";
            treeNode197.SelectedImageIndex = 1;
            treeNode197.Text = "收发存报表";
            treeNode197.ToolTipText = "收发存报表";
            treeNode198.Name = "SaftyStockSet";
            treeNode198.Text = "手动设置安全库存";
            treeNode199.Name = "ShowSaftyInventory";
            treeNode199.Text = "查看安全库存";
            treeNode200.Name = "UpdateSaftyStock";
            treeNode200.Text = "更新安全库存";
            treeNode201.Name = "compute1";
            treeNode201.Text = "计算安全库存";
            treeNode202.Name = "InputDemand";
            treeNode202.Text = "输入需求量";
            treeNode203.Name = "SaftyStockComp";
            treeNode203.Text = "系统计算安全库存";
            treeNode204.ImageIndex = 1;
            treeNode204.Name = "SaveStock";
            treeNode204.SelectedImageIndex = 1;
            treeNode204.Text = "安全库存";
            treeNode204.ToolTipText = "安全库存";
            treeNode205.Name = "createDocument";
            treeNode205.Text = "集中创建盘点凭证";
            treeNode206.Name = "CreateDocument";
            treeNode206.Text = "单个创建盘点凭证";
            treeNode207.Name = "Freezing";
            treeNode207.Text = "盘点冻结与解冻";
            treeNode208.Name = "InputResult";
            treeNode208.Text = "输入盘点结果";
            treeNode209.Name = "ShowDifference";
            treeNode209.Text = "差异清单一览";
            treeNode210.Name = "CheckPost";
            treeNode210.Text = "盘点数据更新";
            treeNode211.ImageIndex = 1;
            treeNode211.Name = "节点0";
            treeNode211.SelectedImageIndex = 1;
            treeNode211.Text = "年度盘点";
            treeNode212.Name = "StockList";
            treeNode212.Text = "显示物料清单";
            treeNode213.Name = "AdjustDifference";
            treeNode213.Text = "差异调整";
            treeNode214.Name = "ShowDiffLoop";
            treeNode214.Text = "显示差异调整";
            treeNode215.Name = "CancelAdjust";
            treeNode215.Text = "取消差异调整";
            treeNode216.ImageIndex = 1;
            treeNode216.Name = "节点1";
            treeNode216.SelectedImageIndex = 1;
            treeNode216.Text = "循环盘点";
            treeNode217.ImageIndex = 1;
            treeNode217.Name = "StockCheck";
            treeNode217.SelectedImageIndex = 1;
            treeNode217.Text = "库存盘点";
            treeNode217.ToolTipText = "库存盘点";
            treeNode218.Name = "ReceiveScore";
            treeNode218.Text = "质检评分";
            treeNode219.Name = "ShipmentScore";
            treeNode219.Text = "装运评分";
            treeNode220.Name = "NonRawMaterialScore";
            treeNode220.Text = "非原材料拒收数量";
            treeNode221.ImageIndex = 1;
            treeNode221.Name = "ReceivingScore";
            treeNode221.SelectedImageIndex = 1;
            treeNode221.Text = "收货评分";
            treeNode221.ToolTipText = "收货评分";
            treeNode222.Name = "Remind";
            treeNode222.Text = "设置提前提醒天数";
            treeNode223.Name = "RemindHandle";
            treeNode223.Text = "到期物料提醒";
            treeNode224.ImageIndex = 1;
            treeNode224.Name = "ExpirationReminder";
            treeNode224.SelectedImageIndex = 1;
            treeNode224.Text = "到期提醒";
            treeNode224.ToolTipText = "到期提醒";
            treeNode225.ImageIndex = 1;
            treeNode225.Name = "节点12";
            treeNode225.SelectedImageIndex = 1;
            treeNode225.Text = "库存管理";
            treeNode226.ImageIndex = 1;
            treeNode226.Name = "fileManageNode";
            treeNode226.SelectedImageIndex = 1;
            treeNode226.Text = "文档管理";
            treeNode227.Name = "order";
            treeNode227.Tag = "900";
            treeNode227.Text = "订单";
            treeNode227.ToolTipText = "订单";
            treeNode228.Name = "queryOrder";
            treeNode228.Tag = "900";
            treeNode228.Text = "询价单";
            treeNode228.ToolTipText = "询价单";
            treeNode229.Name = "bid";
            treeNode229.Tag = "900";
            treeNode229.Text = "招标";
            treeNode229.ToolTipText = "招标";
            treeNode230.Name = "negotiation";
            treeNode230.Tag = "900";
            treeNode230.Text = "谈判";
            treeNode230.ToolTipText = "谈判";
            treeNode231.Name = "contractManager";
            treeNode231.Tag = "900";
            treeNode231.Text = "合同管理";
            treeNode231.ToolTipText = "合同管理";
            treeNode232.ImageIndex = 1;
            treeNode232.Name = "business";
            treeNode232.SelectedImageIndex = 1;
            treeNode232.Tag = "900";
            treeNode232.Text = "商务";
            treeNode232.ToolTipText = "商务";
            treeNode233.Name = "qualityReport";
            treeNode233.Tag = "900";
            treeNode233.Text = "质检报告";
            treeNode233.ToolTipText = "质检报告";
            treeNode234.ImageIndex = 1;
            treeNode234.Name = "quality";
            treeNode234.SelectedImageIndex = 1;
            treeNode234.Tag = "900";
            treeNode234.Text = "质量";
            treeNode234.ToolTipText = "质量";
            treeNode235.ImageIndex = 1;
            treeNode235.Name = "";
            treeNode235.SelectedImageIndex = 1;
            treeNode235.Text = "我的业务";
            treeNode236.Name = "BaseInfo";
            treeNode236.Tag = "900";
            treeNode236.Text = "供应商基本信息";
            treeNode236.ToolTipText = "供应商基本信息";
            treeNode237.Name = "protocol";
            treeNode237.Tag = "900";
            treeNode237.Text = "协议";
            treeNode237.ToolTipText = "协议";
            treeNode238.Name = "payCondition";
            treeNode238.Tag = "900";
            treeNode238.Text = "付款条件";
            treeNode238.ToolTipText = "付款条件";
            treeNode239.Name = "BalanceIntegralCard";
            treeNode239.Tag = "900";
            treeNode239.Text = "平衡积分卡";
            treeNode239.ToolTipText = "平衡积分卡";
            treeNode240.Name = "cost";
            treeNode240.Tag = "900";
            treeNode240.Text = "花费情况";
            treeNode240.ToolTipText = "花费情况";
            treeNode241.Name = "meeting";
            treeNode241.Tag = "900";
            treeNode241.Text = "会议纪要";
            treeNode241.ToolTipText = "会议纪要";
            treeNode242.ImageIndex = 1;
            treeNode242.Name = "view";
            treeNode242.SelectedImageIndex = 1;
            treeNode242.Tag = "900";
            treeNode242.Text = "查看";
            treeNode242.ToolTipText = "查看";
            treeNode243.Name = "methodByPhone";
            treeNode243.Tag = "900";
            treeNode243.Text = "供应商联系方式";
            treeNode243.ToolTipText = "供应商联系方式";
            treeNode244.Name = "yearPro";
            treeNode244.Tag = "900";
            treeNode244.Text = "年营业额";
            treeNode244.ToolTipText = "年营业额";
            treeNode245.Name = "evaluate";
            treeNode245.Tag = "900";
            treeNode245.Text = "供应山评估";
            treeNode245.ToolTipText = "供应山评估";
            treeNode246.Name = "addrUrl";
            treeNode246.Tag = "900";
            treeNode246.Text = "供应商网址";
            treeNode246.ToolTipText = "供应商网址";
            treeNode247.Name = "CSR";
            treeNode247.Tag = "900";
            treeNode247.Text = "CSR评估";
            treeNode247.ToolTipText = "CSR评估";
            treeNode248.ImageIndex = 1;
            treeNode248.Name = "update";
            treeNode248.SelectedImageIndex = 1;
            treeNode248.Tag = "900";
            treeNode248.Text = "更新";
            treeNode248.ToolTipText = "更新";
            treeNode249.ImageIndex = 1;
            treeNode249.Name = "";
            treeNode249.SelectedImageIndex = 1;
            treeNode249.Text = "我的表现";
            treeNode250.ImageIndex = 1;
            treeNode250.Name = "";
            treeNode250.SelectedImageIndex = 0;
            treeNode250.Text = "供应商门户权限管理";
            this.permissionObject.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode40,
            treeNode53,
            treeNode87,
            treeNode92,
            treeNode97,
            treeNode99,
            treeNode100,
            treeNode103,
            treeNode154,
            treeNode178,
            treeNode225,
            treeNode226,
            treeNode250});
            this.permissionObject.Size = new System.Drawing.Size(215, 373);
            this.permissionObject.TabIndex = 13;
            this.permissionObject.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.permissionObject_AfterSelect);
            // 
            // exitBtn
            // 
            this.exitBtn.Location = new System.Drawing.Point(665, 415);
            this.exitBtn.Name = "exitBtn";
            this.exitBtn.Size = new System.Drawing.Size(75, 23);
            this.exitBtn.TabIndex = 12;
            this.exitBtn.Text = "关 闭";
            this.exitBtn.UseVisualStyleBackColor = true;
            this.exitBtn.Click += new System.EventHandler(this.exitBtn_Click);
            // 
            // authorizeBtn
            // 
            this.authorizeBtn.Location = new System.Drawing.Point(577, 415);
            this.authorizeBtn.Name = "authorizeBtn";
            this.authorizeBtn.Size = new System.Drawing.Size(75, 23);
            this.authorizeBtn.TabIndex = 11;
            this.authorizeBtn.Text = "授 权";
            this.authorizeBtn.UseVisualStyleBackColor = true;
            this.authorizeBtn.Click += new System.EventHandler(this.authorizeBtn_Click);
            // 
            // cancelCurrentAllBtn
            // 
            this.cancelCurrentAllBtn.Location = new System.Drawing.Point(489, 415);
            this.cancelCurrentAllBtn.Name = "cancelCurrentAllBtn";
            this.cancelCurrentAllBtn.Size = new System.Drawing.Size(75, 23);
            this.cancelCurrentAllBtn.TabIndex = 10;
            this.cancelCurrentAllBtn.Text = "全部取消";
            this.cancelCurrentAllBtn.UseVisualStyleBackColor = true;
            this.cancelCurrentAllBtn.Click += new System.EventHandler(this.cancelCurrentAllBtn_Click);
            // 
            // selectCurrentAllBtn
            // 
            this.selectCurrentAllBtn.Location = new System.Drawing.Point(403, 415);
            this.selectCurrentAllBtn.Name = "selectCurrentAllBtn";
            this.selectCurrentAllBtn.Size = new System.Drawing.Size(75, 23);
            this.selectCurrentAllBtn.TabIndex = 9;
            this.selectCurrentAllBtn.Text = "全部选择";
            this.selectCurrentAllBtn.UseVisualStyleBackColor = true;
            this.selectCurrentAllBtn.Click += new System.EventHandler(this.selectCurrentAllBtn_Click);
            // 
            // permissionListBtn
            // 
            this.permissionListBtn.Location = new System.Drawing.Point(238, 415);
            this.permissionListBtn.Name = "permissionListBtn";
            this.permissionListBtn.Size = new System.Drawing.Size(75, 23);
            this.permissionListBtn.TabIndex = 8;
            this.permissionListBtn.Text = "权限列表";
            this.permissionListBtn.UseVisualStyleBackColor = true;
            this.permissionListBtn.Click += new System.EventHandler(this.permissionListBtn_Click);
            // 
            // cancelAllBtn
            // 
            this.cancelAllBtn.Location = new System.Drawing.Point(126, 415);
            this.cancelAllBtn.Name = "cancelAllBtn";
            this.cancelAllBtn.Size = new System.Drawing.Size(103, 23);
            this.cancelAllBtn.TabIndex = 7;
            this.cancelAllBtn.Text = "取消所有权限";
            this.cancelAllBtn.UseVisualStyleBackColor = true;
            this.cancelAllBtn.Click += new System.EventHandler(this.cancelAllBtn_Click);
            // 
            // authorizeAllBtn
            // 
            this.authorizeAllBtn.Location = new System.Drawing.Point(13, 415);
            this.authorizeAllBtn.Name = "authorizeAllBtn";
            this.authorizeAllBtn.Size = new System.Drawing.Size(103, 23);
            this.authorizeAllBtn.TabIndex = 6;
            this.authorizeAllBtn.Text = "授予所有权限";
            this.authorizeAllBtn.UseVisualStyleBackColor = true;
            this.authorizeAllBtn.Click += new System.EventHandler(this.authorizeAllBtn_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(245, 14);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 17);
            this.label2.TabIndex = 5;
            this.label2.Text = "权限：";
            // 
            // supplierLabel
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 14);
            this.label1.Name = "supplierLabel";
            this.label1.Size = new System.Drawing.Size(68, 17);
            this.label1.TabIndex = 4;
            this.label1.Text = "授权对象：";
            // 
            // permissionTable
            // 
            this.permissionTable.AllowUserToAddRows = false;
            this.permissionTable.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.permissionTable.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.permissionTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.permissionTable.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Permission_ID,
            this.Permission_Name,
            this.personalPermission,
            this.groupPermission,
            this.rolePermission});
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.permissionTable.DefaultCellStyle = dataGridViewCellStyle4;
            this.permissionTable.Location = new System.Drawing.Point(242, 34);
            this.permissionTable.Name = "permissionTable";
            this.permissionTable.RowHeadersVisible = false;
            this.permissionTable.RowTemplate.Height = 23;
            this.permissionTable.Size = new System.Drawing.Size(526, 373);
            this.permissionTable.TabIndex = 3;
            // 
            // Permission_ID
            // 
            this.Permission_ID.DataPropertyName = "Permission_ID";
            this.Permission_ID.HeaderText = "权限对象编码";
            this.Permission_ID.Name = "Permission_ID";
            this.Permission_ID.Visible = false;
            // 
            // Permission_Name
            // 
            this.Permission_Name.DataPropertyName = "Permission_Name";
            this.Permission_Name.FillWeight = 80F;
            this.Permission_Name.HeaderText = "权限功能说明";
            this.Permission_Name.MinimumWidth = 200;
            this.Permission_Name.Name = "Permission_Name";
            // 
            // personalPermission
            // 
            this.personalPermission.DataPropertyName = "hasPermission";
            this.personalPermission.HeaderText = "个人权限";
            this.personalPermission.MinimumWidth = 50;
            this.personalPermission.Name = "personalPermission";
            // 
            // groupPermission
            // 
            this.groupPermission.DataPropertyName = "groupPermission";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.NullValue = false;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Control;
            this.groupPermission.DefaultCellStyle = dataGridViewCellStyle2;
            this.groupPermission.HeaderText = "组权限";
            this.groupPermission.Name = "groupPermission";
            this.groupPermission.ReadOnly = true;
            this.groupPermission.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.groupPermission.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // rolePermission
            // 
            this.rolePermission.DataPropertyName = "rolePermission";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.NullValue = false;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Control;
            this.rolePermission.DefaultCellStyle = dataGridViewCellStyle3;
            this.rolePermission.HeaderText = "角色权限";
            this.rolePermission.Name = "rolePermission";
            this.rolePermission.ReadOnly = true;
            this.rolePermission.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.rolePermission.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // statusBar
            // 
            this.statusBar.BackColor = System.Drawing.SystemColors.Control;
            this.statusBar.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.statusBar.Location = new System.Drawing.Point(0, 445);
            this.statusBar.Name = "statusBar";
            this.statusBar.Size = new System.Drawing.Size(780, 23);
            this.statusBar.TabIndex = 1;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.FillWeight = 80F;
            this.dataGridViewTextBoxColumn1.HeaderText = "权限功能说明";
            this.dataGridViewTextBoxColumn1.MinimumWidth = 200;
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Width = 338;
            // 
            // FunctionLevelPermissionManageForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(780, 468);
            this.Controls.Add(this.topPanel);
            this.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(796, 507);
            this.MinimumSize = new System.Drawing.Size(796, 507);
            this.Name = "FunctionLevelPermissionManageForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "功能级权限管理";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FunctionLevelPermissionManageForm_FormClosing);
            this.topPanel.ResumeLayout(false);
            this.topPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.permissionTable)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel topPanel;
        private System.Windows.Forms.TextBox statusBar;
        private System.Windows.Forms.DataGridView permissionTable;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button permissionListBtn;
        private System.Windows.Forms.Button cancelAllBtn;
        private System.Windows.Forms.Button authorizeAllBtn;
        private System.Windows.Forms.Button exitBtn;
        private System.Windows.Forms.Button authorizeBtn;
        private System.Windows.Forms.Button cancelCurrentAllBtn;
        private System.Windows.Forms.Button selectCurrentAllBtn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Permission_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Permission_Name;
        private System.Windows.Forms.DataGridViewCheckBoxColumn personalPermission;
        private System.Windows.Forms.DataGridViewCheckBoxColumn groupPermission;
        private System.Windows.Forms.DataGridViewCheckBoxColumn rolePermission;
        private System.Windows.Forms.TreeView permissionObject;
        private System.Windows.Forms.LinkLabel linkLabel1;
    }
}