﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib.Common.CommonUtils;
using Lib.Bll.SystemConfig.PermissionManage;

namespace MMClient.SystemConfig.PermissionManage
{
    /// <summary>
    /// 检查功能级权限
    /// </summary>
    public class FunctionLevelPermissionValidate
    {
        /// <summary>
        /// 检查点击菜单进入窗口的权限
        /// </summary>
        /// <param name="menuName"></param>
        /// <returns></returns>
        public static bool checkEnterWindowPermission(string menuName)
        {
            //用户信息为空，拒绝访问
            if (SingleUserInstance.getCurrentUserInstance() == null)
            {
                displayAccessDenied();
                return false;
            }

            //管理员
            if (SingleUserInstance.getCurrentUserInstance().Manager_Flag == 1)
            {
                return true;
            }

            FunctionLevelPermissionValidateBLL permissionValidate = new FunctionLevelPermissionValidateBLL();
            bool flag = permissionValidate.canEnterWindow(SingleUserInstance.getCurrentUserInstance(), menuName);
            if (!flag)
            {
                displayAccessDenied();
            }

            return flag;
        }

        /// <summary>
        /// 检查窗口内操作的权限
        /// </summary>
        /// <param name="permissionId"></param>
        /// <returns></returns>
        public static bool checkOperationInWindowPermission(int permissionId)
        {
            //用户信息为空，拒绝访问
            if (SingleUserInstance.getCurrentUserInstance() == null)
            {
                displayAccessDenied();
                return false;
            }

            //管理员
            if (SingleUserInstance.getCurrentUserInstance().Manager_Flag == 1)
            {
                return true;
            }
            //一口答应和原图给我粉雾好噶viu额;
            FunctionLevelPermissionValidateBLL permissionValidate = new FunctionLevelPermissionValidateBLL();
            bool flag = permissionValidate.canOperateInWindow(SingleUserInstance.getCurrentUserInstance(), permissionId);
            if (!flag)
            {
                displayAccessDenied();
            }

            return flag;
        }

        /// <summary>
        /// 拒绝访问提示
        /// </summary>
        public static void displayAccessDenied()
        {
            MessageUtil.ShowWarning("用户未获授权，拒绝访问！");
        }

    }
}
