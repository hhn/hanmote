﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Bll.StockBLL;
using Lib.Bll.MDBll.General;

namespace MMClient.InventoryManagement
{
    public partial class Frm_ShowDifference : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        public Frm_ShowDifference()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (comboBox2.SelectedItem == null && comboBox3.SelectedItem == null && comboBox4.SelectedItem == null && textBox4.Text.Trim() == "" && textBox5.Text.Trim() == "" && textBox6.Text.Trim() == "")
            {
                MessageBox.Show("请在“数据选择”部分至少填写一项");
                return;
            }

            string material = comboBox2.SelectedItem == null ? "" : comboBox2.SelectedItem.ToString().Trim();
            string factory = comboBox3.SelectedItem == null ? "" : comboBox3.SelectedItem.ToString().Trim();
            string stock = comboBox4.SelectedItem == null ? "" : comboBox4.SelectedItem.ToString().Trim();
            string batch = textBox4.Text.Trim();
            string documentID = textBox5.Text.Trim();
            string checkID = textBox6.Text.Trim();

            string inventorystate = comboBox1.SelectedItem==null?"":comboBox1.SelectedItem.ToString();
            string accountyear = textBox7.Text.Trim();
            DateTime checkdate = dateTimePicker1.Value;
            DateTime plancheckdate = dateTimePicker2.Value;
            string referid = textBox8.Text.Trim();

            bool uncheck = checkBox1.Checked;
            bool partcheck = checkBox2.Checked;
            bool allchecked = checkBox3.Checked;

            bool uncount = checkBox4.Checked;
            bool count = checkBox5.Checked;
            bool post = checkBox6.Checked;
            bool recheck = checkBox7.Checked;


            DataTable dt = new DataTable("dt");

            dt.Columns.Add("material", typeof(string));
            dt.Columns.Add("factory", typeof(string));
            dt.Columns.Add("stock", typeof(string));
            dt.Columns.Add("batch", typeof(string));
            dt.Columns.Add("documentID", typeof(string));
            dt.Columns.Add("checkID", typeof(string));

            dt.Columns.Add("inventorystate", typeof(string));
            dt.Columns.Add("plancheckDate", typeof(string));
            dt.Columns.Add("accountyear", typeof(string));
            dt.Columns.Add("referid", typeof(string));
            dt.Columns.Add("checkdate", typeof(string));

            dt.Columns.Add("uncheck", typeof(bool));
            dt.Columns.Add("partcheck", typeof(bool));
            dt.Columns.Add("allchecked", typeof(bool));

            dt.Columns.Add("uncount", typeof(bool));
            dt.Columns.Add("count", typeof(bool));
            dt.Columns.Add("post", typeof(bool));
            dt.Columns.Add("recheck", typeof(bool));

            DataRow dr = dt.NewRow();
            dr["material"] = material;
            dr["factory"] = factory;
            dr["stock"] = stock;
            dr["batch"] = batch;
            dr["documentID"] = documentID;
            dr["checkID"] = checkID;
            dr["inventorystate"] = inventorystate;
            if (this.dateTimePicker2.Checked == false)
                dr["plancheckDate"] = "";
            else
            dr["plancheckDate"] = plancheckdate;
            dr["accountyear"] = accountyear;
            dr["referid"] = referid;
            if (this.dateTimePicker1.Checked == false)
                dr["checkdate"] = "";
            else
                dr["checkdate"] = checkdate;
            dr["uncheck"] = uncheck;
            dr["partcheck"] = partcheck;
            dr["allchecked"] = allchecked;
            dr["uncount"] = uncount;
            dr["count"] = count;
            dr["post"] = post;
            dr["recheck"] = recheck;
            
            dt.Rows.Add(dr);

            ShowDifferenceBLL sdb = new ShowDifferenceBLL();
            DataTable dt0 = sdb.selectInfo(dt);
            if (dt0.Rows.Count == 0)
            {
                MessageBox.Show("没有符合条件的物料");

            }
            else
            {
                Frm_ShowDifferenceShow frm_ShowDifferenceShownew = new Frm_ShowDifferenceShow(dt0);
                frm_ShowDifferenceShownew.Show();
            }

        }

        private void Frm_ShowDifference_Load(object sender, EventArgs e)
        {
            this.dateTimePicker1.Checked = false;
            this.dateTimePicker2.Checked = false;


            List<string> mlist = new List<string>();
            List<string> flist = new List<string>();
            List<string> slist = new List<string>();
            getMaterialInfoBLL gmb = new getMaterialInfoBLL();
            DataTable dt = gmb.getMaterialID();
            DataTable dt0 = gmb.getFactoryID();
            DataTable dt1 = gmb.getStockID();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                mlist.Insert(i, dt.Rows[i][0].ToString());
            }
            for (int i = 0; i < dt0.Rows.Count; i++)
            {
                flist.Insert(i, dt0.Rows[i][0].ToString());
            }
            for (int i = 0; i < dt1.Rows.Count; i++)
            {
                slist.Insert(i, dt1.Rows[i][0].ToString());
            }

            ComboBoxItem comb = new ComboBoxItem();
            comb.FuzzyQury(comboBox3, flist);
            comb.FuzzyQury(comboBox2, mlist);
            comb.FuzzyQury(comboBox4, slist);

        }

        
    }
}
