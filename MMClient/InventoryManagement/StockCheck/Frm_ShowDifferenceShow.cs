﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MMClient.InventoryManagement
{
    public partial class Frm_ShowDifferenceShow : Form
    {
        DataTable basicInfo;
        public Frm_ShowDifferenceShow()
        {
            InitializeComponent();
        }

        public Frm_ShowDifferenceShow(DataTable dt)
        {
            basicInfo = dt;
            InitializeComponent();
        }

        private void Frm_ShowDifferenceShow_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource = basicInfo;
        }
    }
}
