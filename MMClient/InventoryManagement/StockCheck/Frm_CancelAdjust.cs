﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using Lib.Bll.StockBLL;
using Lib.Bll.MDBll.General;

namespace MMClient.InventoryManagement
{
    public partial class Frm_CancelAdjust : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        public Frm_CancelAdjust()
        {
            InitializeComponent();
            this.dataGridView1.TopLeftHeaderCell.Value = "行号";
        }

        private void dataGridView1_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {

            var dgv = sender as DataGridView;
            if (dgv != null)
            {
                Rectangle rect = new Rectangle(e.RowBounds.Location.X, e.RowBounds.Location.Y, (dgv.RowHeadersWidth + 12) / 2, e.RowBounds.Height);
                TextRenderer.DrawText(e.Graphics, (e.RowIndex + 1).ToString(), dgv.RowHeadersDefaultCellStyle.Font, rect, dgv.RowHeadersDefaultCellStyle.ForeColor, TextFormatFlags.VerticalCenter | TextFormatFlags.Right);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int documentid;
            try
            {
                documentid = Convert.ToInt32(comboBox1.SelectedItem.ToString());
            }
            catch
            {
                MessageBox.Show("输入格式不正确，请检查", "提示");
                return;
            }
            AdjustDifferenceBLL adb = new AdjustDifferenceBLL();

            bool c = adb.canceled(documentid.ToString());
            if (!c)
            {
                MessageBox.Show("凭证不可冲销", "提示");
                return; 
            }
            
            DataTable dt1 = adb.ShowDifferenceAdjustDAL(documentid.ToString());

            if (dt1.Rows.Count < 1)
            {
                MessageBox.Show("暂无满足条件的结果，请检查输入是否正确", "提示");
                return;
            }

            this.dataGridView1.DataSource = dt1;
            this.dataGridView1.Visible = true;
            this.dataGridView1.Show();

        }

        private void button2_Click(object sender, EventArgs e)
        {
            DataTable dt = new DataTable("dt");
            dt.Columns.Add("documentdate", typeof(DateTime));
            dt.Columns.Add("postdate", typeof(DateTime));
            dt.Columns.Add("materialnote", typeof(string));
            dt.Columns.Add("materialid", typeof(string));
            dt.Columns.Add("materialname", typeof(string));
            dt.Columns.Add("materialcount", typeof(double));
            dt.Columns.Add("materialunit", typeof(string));
            dt.Columns.Add("factory", typeof(string));
            dt.Columns.Add("stock", typeof(string));
            dt.Columns.Add("batch", typeof(string));
            dt.Columns.Add("evaluatetype", typeof(string));
            dt.Columns.Add("inventorystate", typeof(string));
            dt.Columns.Add("materialgroup", typeof(string));
            dt.Columns.Add("movetype", typeof(string));
            dt.Columns.Add("inventorytype", typeof(string));

            if (this.dataGridView1.RowCount < 1)
            {
                MessageBox.Show("请输入数据", "提示");
                return;
            }

            for (int i = 0; i < this.dataGridView1.RowCount; i++)
            {

                DateTime documentdate = this.dateTimePicker1.Value.Date;
                DateTime postdate = this.dateTimePicker2.Value.Date;
                string materialnote = this.textBox2.Text.Trim();
                string materialid = this.dataGridView1.Rows[i].Cells[0].Value == null ? "" : this.dataGridView1.Rows[i].Cells[0].Value.ToString().Trim();
                string materialname = this.dataGridView1.Rows[i].Cells[1].Value == null ? "" : this.dataGridView1.Rows[i].Cells[1].Value.ToString().Trim();
                double materialcount = this.dataGridView1.Rows[i].Cells[2].Value == null ? 0 : Convert.ToDouble(this.dataGridView1.Rows[i].Cells[2].Value.ToString().Trim());
                string materialunit = this.dataGridView1.Rows[i].Cells[3].Value == null ? "" : this.dataGridView1.Rows[i].Cells[3].Value.ToString().Trim();
                string factory = this.dataGridView1.Rows[i].Cells[4].Value == null ? "" : this.dataGridView1.Rows[i].Cells[4].Value.ToString().Trim();
                string stock = this.dataGridView1.Rows[i].Cells[5].Value == null ? "" : this.dataGridView1.Rows[i].Cells[5].Value.ToString().Trim();
                string batch = this.dataGridView1.Rows[i].Cells[6].Value == null ? "" : this.dataGridView1.Rows[i].Cells[6].Value.ToString().Trim();
                string evaluatetype = this.dataGridView1.Rows[i].Cells[7].Value == null ? "" : this.dataGridView1.Rows[i].Cells[7].Value.ToString().Trim();
                string inventorystate = this.dataGridView1.Rows[i].Cells[8].Value == null ? "" : this.dataGridView1.Rows[i].Cells[8].Value.ToString().Trim();
                string materialgroup = this.dataGridView1.Rows[i].Cells[9].Value == null ? "" : this.dataGridView1.Rows[i].Cells[9].Value.ToString().Trim();
                string movetype;
                if (this.dataGridView1.Rows[i].Cells[10].Value.ToString().Trim()=="933")
                {
                    movetype = "935";
                }
                else
                    movetype = "936";
                string inventorytype = this.dataGridView1.Rows[i].Cells[11].Value == null ? "" : this.dataGridView1.Rows[i].Cells[11].Value.ToString().Trim();

                DataRow dr = dt.NewRow();
                dr["documentdate"] = documentdate;
                dr["postdate"] = postdate;
                dr["materialnote"] = materialnote;
                dr["materialid"] = materialid;
                dr["materialname"] = materialname;
                dr["materialcount"] = materialcount;
                dr["materialunit"] = materialunit;
                dr["factory"] = factory;
                dr["stock"] = stock;
                dr["batch"] = batch;
                dr["evaluatetype"] = evaluatetype;
                dr["inventorystate"] = inventorystate;
                dr["materialgroup"] = materialgroup;
                dr["movetype"] = movetype;
                dr["inventorytype"] = inventorytype;

                dt.Rows.Add(dr);

            }
            AdjustDifferenceBLL adb = new AdjustDifferenceBLL();
            string documentid = Convert.ToInt32(comboBox1.SelectedItem.ToString()).ToString();
            if (adb.updatecancel(documentid))
            {
                bool b = adb.genCancelDocument(dt);
                if (b)
                {
                    if(adb.updateInventory(dt))
                    MessageBox.Show("取消成功", "提示");
                }
                else
                {
                    MessageBox.Show("取消失败", "提示");

                }
            }
        }

        private void Frm_CancelAdjust_Load(object sender, EventArgs e)
        {
            List<string> doculist = new List<string>();
            
            getMaterialInfoBLL gmb = new getMaterialInfoBLL();
            DataTable dt = gmb.getloopStocktakingDocuid();
            
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                doculist.Insert(i, dt.Rows[i][0].ToString());
            }
            
            ComboBoxItem comb = new ComboBoxItem();
            comb.FuzzyQury(comboBox1, doculist);
        }
    }
}
