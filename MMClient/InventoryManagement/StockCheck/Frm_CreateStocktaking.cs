﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Bll.StockBLL;
using Lib.Bll.MDBll.General;

namespace MMClient.InventoryManagement
{
    public partial class Frm_CreateStocktaking : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        public Frm_CreateStocktaking()
        {
            InitializeComponent();
        }

        private void CreateStocktakingForm_Load(object sender, EventArgs e)
        {
            List<string> mlist = new List<string>();
            List<string> flist = new List<string>();
            List<string> slist = new List<string>();
            getMaterialInfoBLL gmb = new getMaterialInfoBLL();
            DataTable dt = gmb.getMaterialID();
            DataTable dt0 = gmb.getFactoryID();
            DataTable dt1 = gmb.getStockID();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                mlist.Insert(i, dt.Rows[i][0].ToString());
            }
            for (int i = 0; i < dt0.Rows.Count; i++)
            {
                flist.Insert(i, dt0.Rows[i][0].ToString());
            }
            for (int i = 0; i < dt1.Rows.Count; i++)
            {
                slist.Insert(i, dt1.Rows[i][0].ToString());
            }

            ComboBoxItem comb = new ComboBoxItem();
            comb.FuzzyQury(comboBox2, flist);
            comb.FuzzyQury(comboBox1, mlist);
            comb.FuzzyQury(comboBox3, slist);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (comboBox2.SelectedItem == null)
            {
                MessageBox.Show("请填写工厂信息");
                    return;
            }

            if (comboBox3.SelectedItem == null)
            {
                MessageBox.Show("请填写库存地信息");
                return;
            }

            string m = comboBox1.SelectedItem == null ? "" : comboBox1.SelectedItem.ToString().Trim();
            string f = comboBox2.SelectedItem == null ? "" : comboBox2.SelectedItem.ToString().Trim();
            string s = comboBox3.SelectedItem == null ? "" : comboBox3.SelectedItem.ToString().Trim();
            string et = comboBox4.SelectedItem == null ? "" : comboBox4.SelectedItem.ToString().Trim();
            string mg = textBox5.Text.Trim();
            bool b1 = checkBox1.Checked;
            bool b2 = checkBox2.Checked;
            bool b3 = checkBox3.Checked;
            DateTime dtime = dateTimePicker1.Value;
            CreateStocktakingBLL csd = new CreateStocktakingBLL();
            DataTable dt = csd.selectMaterial(m,f,s,et,mg,b1,b2,b3);
            if (dt.Rows.Count == 0)
            {
                MessageBox.Show("仓库不存在满足条件的物料记录","提示");
                return;
            }
            bool docuBuildSucc = csd.createDocument(dt,dtime, textBox6.Text, textBox7.Text);
            if (docuBuildSucc == true)
            {
                MessageBox.Show("创建成功");
            }

            

        }

        private void comboBox3_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
