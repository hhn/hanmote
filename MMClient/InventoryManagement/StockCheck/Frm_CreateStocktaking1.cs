﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using Lib.Bll.MDBll.General;
using Lib.Bll.StockBLL;

namespace MMClient.InventoryManagement
{
    public partial class Frm_CreateStocktaking1 : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        public Frm_CreateStocktaking1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (comboBox2.SelectedItem == null || comboBox1.SelectedItem == null || comboBox3.SelectedItem == null)
            {
                MessageBox.Show("请将盘点地点信息填写完整");
                return;
            }
            DataTable dt = new DataTable("stktk");
            dt.Columns.Add("docuDate", typeof(DateTime));
            dt.Columns.Add("CheckDate", typeof(DateTime));
            dt.Columns.Add("factory", typeof(string));
            dt.Columns.Add("stock", typeof(string));
            dt.Columns.Add("Inventorytype", typeof(string));
            dt.Columns.Add("stocktakeID", typeof(string));
            dt.Columns.Add("referID", typeof(string));
            DataRow dr = dt.NewRow();
            dr["docuDate"] = dateTimePicker1.Value;
            dr["CheckDate"] = dateTimePicker2.Value;
            dr["factory"] = comboBox2.SelectedItem == null ? "" : comboBox2.SelectedItem.ToString().Trim();
            dr["stock"] = comboBox3.SelectedItem == null ? "" : comboBox3.SelectedItem.ToString().Trim();
            dr["Inventorytype"] = comboBox1.SelectedItem == null ? "" : comboBox1.SelectedItem.ToString().Trim();
            dr["stocktakeID"] = textBox3.Text.Trim();
            dr["referID"] = textBox4.Text.Trim();

            dt.Rows.Add(dr);

            Frm_CreateStocktaking1_1 frm =  new Frm_CreateStocktaking1_1(dt);
            frm.Show();

        }

        private void Frm_CreateStocktaking1_Load(object sender, EventArgs e)
        {
           
            List<string> flist = new List<string>();
            List<string> slist = new List<string>();
            getMaterialInfoBLL gmb = new getMaterialInfoBLL();

            DataTable dt0 = gmb.getFactoryID();
            DataTable dt1 = gmb.getStockID();
            
            for (int i = 0; i < dt0.Rows.Count; i++)
            {
                flist.Insert(i, dt0.Rows[i][0].ToString());
            }
            for (int i = 0; i < dt1.Rows.Count; i++)
            {
                slist.Insert(i, dt1.Rows[i][0].ToString());
            }

            ComboBoxItem comb = new ComboBoxItem();
            comb.FuzzyQury(comboBox2, flist);
            comb.FuzzyQury(comboBox3, slist);
        }
    }
}
