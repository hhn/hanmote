﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Bll.StockBLL;

namespace MMClient.InventoryManagement
{
    public partial class Frm_AdjustDifference : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        public Frm_AdjustDifference()
        {
            InitializeComponent();
            this.dataGridView1.TopLeftHeaderCell.Value = "行号";
        }

        private void dataGridView1_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            var dgv = sender as DataGridView;
            if (dgv != null)
            {
                Rectangle rect = new Rectangle(e.RowBounds.Location.X, e.RowBounds.Location.Y, (dgv.RowHeadersWidth + 12) / 2, e.RowBounds.Height);
                TextRenderer.DrawText(e.Graphics, (e.RowIndex + 1).ToString(), dgv.RowHeadersDefaultCellStyle.Font, rect, dgv.RowHeadersDefaultCellStyle.ForeColor, TextFormatFlags.VerticalCenter | TextFormatFlags.Right);
            }
        }

        private void Frm_AdjustDifference_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

            DataTable dt = new DataTable("dt");
            dt.Columns.Add("documentdate", typeof(DateTime));
            dt.Columns.Add("postdate", typeof(DateTime));
            dt.Columns.Add("materialnote", typeof(string));
            dt.Columns.Add("materialid", typeof(string));
            dt.Columns.Add("materialname", typeof(string));
            dt.Columns.Add("materialcount", typeof(double));
            dt.Columns.Add("materialunit", typeof(string));
            dt.Columns.Add("factory", typeof(string));
            dt.Columns.Add("stock", typeof(string));
            dt.Columns.Add("batch", typeof(string));
            dt.Columns.Add("evaluatetype", typeof(string));
            dt.Columns.Add("inventorystate", typeof(string));
            dt.Columns.Add("materialgroup", typeof(string));
            dt.Columns.Add("movetype", typeof(string));
            dt.Columns.Add("inventorytype", typeof(string));

            if (this.dataGridView1.RowCount < 2)
            {
                MessageBox.Show("请输入数据", "提示");
                return;
            }

            for (int i = 0; i < this.dataGridView1.RowCount - 1; i++)
            {
               
                    DateTime documentdate = this.dateTimePicker1.Value.Date;
                    DateTime postdate = this.dateTimePicker2.Value.Date;
                    string materialnote = this.textBox1.Text.Trim();
                    string materialid = this.dataGridView1.Rows[i].Cells[0].Value==null ? "" : this.dataGridView1.Rows[i].Cells[0].Value.ToString().Trim();
                    string materialname = this.dataGridView1.Rows[i].Cells[1].Value == null ? "" : this.dataGridView1.Rows[i].Cells[1].Value.ToString().Trim();
                    double materialcount = this.dataGridView1.Rows[i].Cells[2].Value == null ? 0 : Convert.ToDouble(this.dataGridView1.Rows[i].Cells[2].Value.ToString().Trim());
                    string materialunit = this.dataGridView1.Rows[i].Cells[3].Value == null ? "" : this.dataGridView1.Rows[i].Cells[3].Value.ToString().Trim();
                    string factory = this.dataGridView1.Rows[i].Cells[4].Value == null ? "" : this.dataGridView1.Rows[i].Cells[4].Value.ToString().Trim();
                    string stock = this.dataGridView1.Rows[i].Cells[5].Value == null ? "" : this.dataGridView1.Rows[i].Cells[5].Value.ToString().Trim();
                    string batch = this.dataGridView1.Rows[i].Cells[6].Value == null ? "" : this.dataGridView1.Rows[i].Cells[6].Value.ToString().Trim();
                    string evaluatetype = this.dataGridView1.Rows[i].Cells[7].Value == null ? "" : this.dataGridView1.Rows[i].Cells[7].Value.ToString().Trim();
                    string inventorystate = this.dataGridView1.Rows[i].Cells[8].Value == null ? "" : this.dataGridView1.Rows[i].Cells[8].Value.ToString().Trim();
                    string materialgroup = this.dataGridView1.Rows[i].Cells[9].Value == null ? "" : this.dataGridView1.Rows[i].Cells[9].Value.ToString().Trim();
                    string movetype = this.dataGridView1.Rows[i].Cells[10].Value == null ? "" : this.dataGridView1.Rows[i].Cells[10].Value.ToString().Trim();
                    string inventorytype = this.dataGridView1.Rows[i].Cells[11].Value == null ? "" : this.dataGridView1.Rows[i].Cells[11].Value.ToString().Trim();

                    if (evaluatetype == "" || inventorystate == "" || materialid == "" || materialcount == 0 || factory == "" || stock == "" || inventorystate == "" || movetype == ""||inventorytype=="")
                    {
                        MessageBox.Show("请检查输入是否完整或者数量是否非零");
                        return;
                    }



                    DataRow dr = dt.NewRow();
                    dr["documentdate"] = documentdate;
                    dr["postdate"] = postdate;
                    dr["materialnote"] = materialnote;
                    dr["materialid"] = materialid;
                    dr["materialname"] = materialname;
                    dr["materialcount"] = materialcount;
                    dr["materialunit"] = materialunit;
                    dr["factory"] = factory;
                    dr["stock"] = stock;
                    dr["batch"] = batch;
                    dr["evaluatetype"] = evaluatetype;
                    dr["inventorystate"] = inventorystate;
                    dr["materialgroup"] = materialgroup;
                    dr["movetype"] = movetype;
                    dr["inventorytype"] = inventorytype;

                    dt.Rows.Add(dr);
                

            }
            AdjustDifferenceBLL adb = new AdjustDifferenceBLL();
            bool b = adb.genAdjustDocument(dt);
            if (b)
            {
                if (adb.updateInventory(dt))
                    MessageBox.Show("调整成功", "提示");
            }
            else
            {
                MessageBox.Show("调整失败", "提示");
 
            }
            
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int rowindex = e.RowIndex;
            try
            {
                if (dataGridView1.Rows[rowindex].Cells[0].Value.ToString() != "")
                {
                    string materialid = dataGridView1.Rows[rowindex].Cells[0].Value.ToString();

                    getMaterialInfoBLL gmib = new getMaterialInfoBLL();
                    string materialname = gmib.getMaterialName(materialid);
                    dataGridView1.Rows[rowindex].Cells[1].Value = materialname;
                    /*string materialtype = gmib.getMaterialType(materialid);
                    dataGridView1.Rows[rowindex].Cells[1].Value = materialname;*/
                    string measurement = gmib.getMeasurement(materialid);
                    dataGridView1.Rows[rowindex].Cells[3].Value = measurement;
                    string materialgroup = gmib.getMaterialGroup(materialid);
                    dataGridView1.Rows[rowindex].Cells[9].Value = materialgroup;
                }
            }
            catch
            {
                /*
                MessageBox.Show("请确认输入的物料编码是否在主数据中创建");
                return;*/
            }    
        }
    }
}
