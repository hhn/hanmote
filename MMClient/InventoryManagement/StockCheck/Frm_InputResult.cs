﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Bll.MDBll.General;
using Lib.Bll.StockBLL;

namespace MMClient.InventoryManagement
{
    public partial class Frm_InputResult : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        public Frm_InputResult()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (comboBox1.SelectedItem==null|| textBox2.Text.Trim() == "" )
            {
                MessageBox.Show("请将盘点地点信息填写完整");
                return;
            }
            try
            {
                Convert.ToInt32(textBox2.Text.Trim());
            }
            catch
            {
                MessageBox.Show("请输入2000-2500之间的年份");
                return; 
            }
            if (Convert.ToInt32(textBox2.Text.Trim()) < 2000 || Convert.ToInt32(textBox2.Text.Trim()) > 2500)
            {
                MessageBox.Show("请输入2000-2500之间的年份");
                return; 
            }
            DataTable dt = new DataTable("stktk");
            dt.Columns.Add("DocumentID", typeof(string));
            dt.Columns.Add("checkyear", typeof(int));
            dt.Columns.Add("checkdate", typeof(DateTime));
            dt.Columns.Add("dif", typeof(double));
            
            DataRow dr = dt.NewRow();
            dr["DocumentID"] = comboBox1.SelectedItem == null ? "" : comboBox1.SelectedItem.ToString().Trim();
            dr["checkyear"] = Convert.ToInt16(textBox2.Text.Trim());
            dr["checkdate"] = dateTimePicker1.Value;
            dr["dif"] = Convert.ToDouble(textBox2.Text.Trim());
            
            dt.Rows.Add(dr);

            Frm_InputResultDetail frm = new Frm_InputResultDetail(dt);
            frm.Show();

        }

        private void Frm_InputResult_Load(object sender, EventArgs e)
        {
            List<string> doculist = new List<string>();

            getMaterialInfoBLL gmb = new getMaterialInfoBLL();
            DataTable dt = gmb.getStocktakingDocuid();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                doculist.Insert(i, dt.Rows[i][0].ToString());
            }

            ComboBoxItem comb = new ComboBoxItem();
            comb.FuzzyQury(comboBox1, doculist);
        }
    }
}
