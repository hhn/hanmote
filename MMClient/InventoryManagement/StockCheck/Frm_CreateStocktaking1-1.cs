﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.SqlServerDAL;
using Lib.Bll.StockBLL;

namespace MMClient.InventoryManagement
{
    public partial class Frm_CreateStocktaking1_1 : Form
    {
        ComboBox cbbx = new ComboBox();
        Rectangle _Rectangle;

        DataTable basicInfo;
        public Frm_CreateStocktaking1_1(DataTable dt)
        {
            basicInfo = dt;
            InitializeComponent();
            this.dataGridView1.TopLeftHeaderCell.Value = "行号";
            this.textBox1.Text = dt.Rows[0][2].ToString();
            this.textBox2.Text = dt.Rows[0][3].ToString();

        }

        private void bindcomb()
        {
            getMaterialInfoBLL gmib = new getMaterialInfoBLL();
            DataTable dt = gmib.getMaterialID();
            cbbx.DataSource = dt;
            this.cbbx.DisplayMember = "Material_ID";//字段名
            this.cbbx.ValueMember = "Material_ID";//字段名
            cbbx.DropDownStyle = ComboBoxStyle.DropDownList;

        }

        private void cmb_Temp_SelectedIndexChanged(object sender, EventArgs e)
        {
            dataGridView1.CurrentCell.Value = cbbx == null ? "" : cbbx.SelectedValue.ToString();  //下拉框控件选择值时，就把选择的值赋给所在的单元格  
        }


        /****************单元格被单击，判断是否是放时间控件的那一列*******************/
        /*        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
                {
                    if (e.ColumnIndex == 0)
                    {

                        _Rectangle = dataGridView1.GetCellDisplayRectangle(e.ColumnIndex, e.RowIndex, true); //得到所在单元格位置和大小  
                        cbbx.Size = new Size(_Rectangle.Width, _Rectangle.Height); //把单元格大小赋给时间控件  
                        cbbx.Location = new Point(_Rectangle.X, _Rectangle.Y); //把单元格位置赋给时间控件  
                        cbbx.Visible = true;  //可以显示控件了  
                    }
                    else
                        cbbx.Visible = false;
                }
         * /

                /***********当列的宽度变化时，时间控件先隐藏起来，不然单元格变大时间控件无法跟着变大哦***********/
        private void dataGridView1_ColumnWidthChanged(object sender, DataGridViewColumnEventArgs e)
        {
            cbbx.Visible = false;

        }

        /***********滚动条滚动时，单元格位置发生变化，也得隐藏时间控件，不然时间控件位置不动就乱了********/
        private void dataGridView1_Scroll(object sender, ScrollEventArgs e)
        {
            cbbx.Visible = false;
        }


        private void label2_Click(object sender, EventArgs e)
        {

        }


        private void dataGridView1_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            var dgv = sender as DataGridView;
            if (dgv != null)
            {
                Rectangle rect = new Rectangle(e.RowBounds.Location.X, e.RowBounds.Location.Y, (dgv.RowHeadersWidth + 12) / 2, e.RowBounds.Height);
                TextRenderer.DrawText(e.Graphics, (e.RowIndex + 1).ToString(), dgv.RowHeadersDefaultCellStyle.Font, rect, dgv.RowHeadersDefaultCellStyle.ForeColor, TextFormatFlags.VerticalCenter | TextFormatFlags.Right);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {

            CreateStocktakingBLL csd = new CreateStocktakingBLL();
            int n = dataGridView1.RowCount;
            DataTable dt = new DataTable("dt");
            dt.Columns.Add("material", typeof(string));
            dt.Columns.Add("factory", typeof(string));
            dt.Columns.Add("stock", typeof(string));
            dt.Columns.Add("Inventorystate", typeof(string));
            dt.Columns.Add("batch", typeof(string));
            dt.Columns.Add("inventoryType", typeof(string));
            for (int i = 0; i < n - 1; i++)
            {
                if (dataGridView1.Rows[i].Cells[0].Value == null || dataGridView1.Rows[i].Cells[3].Value == null)
                {
                    MessageBox.Show("请将物料信息填写完整");
                    return;
                }
                if (Convert.ToBoolean(dataGridView1.Rows[i].Cells[4].Value) == false)
                {
                    string materialID = dataGridView1.Rows[i].Cells[0].Value.ToString();
                    string batchID = dataGridView1.Rows[i].Cells[2].Value == null ? "" : dataGridView1.Rows[i].Cells[2].Value.ToString();
                    string inventoryState = dataGridView1.Rows[i].Cells[3].Value.ToString();
                    string factoryID = basicInfo.Rows[0][2].ToString();
                    string stockID = basicInfo.Rows[0][3].ToString();
                    string inventoryType = basicInfo.Rows[0][4].ToString();

                    DataRow dr = dt.NewRow();
                    dr["material"] = materialID;
                    dr["factory"] = factoryID;
                    dr["stock"] = stockID;
                    dr["Inventorystate"] = inventoryState;
                    dr["batch"] = batchID;
                    dr["inventoryType"] = inventoryType;

                    dt.Rows.Add(dr);
                }
            }
            DataTable dt1 = csd.selectMaterialSingle(dt);
            if (dt1.Rows.Count == 0)
            {
                MessageBox.Show("暂无符合条件的物料记录,请确认工厂、物料等信息填写正确");
                return;
            }
            bool docuBuildSucc = csd.createDocument(dt1, Convert.ToDateTime(basicInfo.Rows[0][1]), basicInfo.Rows[0][6].ToString(), basicInfo.Rows[0][5].ToString());
            if (docuBuildSucc == true)
            {
                MessageBox.Show("创建成功");
            }
            this.Close();
        }



        private void Frm_CreateStocktaking1_1_Load(object sender, EventArgs e)
        {
            bindcomb();
            // dataGridView1.Controls.Add(cbbx);  //把时间控件加入DataGridView  
            cbbx.Visible = false;  //先不让它显示    
            cbbx.DropDownStyle = ComboBoxStyle.DropDownList;
            cbbx.SelectedIndexChanged += new EventHandler(cmb_Temp_SelectedIndexChanged);//为下拉框控件加入事件
            // 将下拉列表框加入到DataGridView控件中
            this.dataGridView1.Controls.Add(cbbx);
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {

                _Rectangle = dataGridView1.GetCellDisplayRectangle(e.ColumnIndex, e.RowIndex, true); //得到所在单元格位置和大小  
                cbbx.Size = new Size(_Rectangle.Width, _Rectangle.Height); //把单元格大小赋给时间控件  
                cbbx.Location = new Point(_Rectangle.X, _Rectangle.Y); //把单元格位置赋给时间控件  
                cbbx.Visible = true;  //可以显示控件了  
            }
            else
                cbbx.Visible = false;


            int rowindex = e.RowIndex;
            try
            {
                if (dataGridView1.Rows[rowindex].Cells[0].Value.ToString() != "")
                {
                    string materialid = dataGridView1.Rows[rowindex].Cells[0].Value.ToString();

                    CreateStocktakingBLL csd = new CreateStocktakingBLL();
                    string materialname = csd.getMaterialname(materialid);
                    dataGridView1.Rows[rowindex].Cells[1].Value = materialname;
                }
            }
            catch
            {
                /*
                MessageBox.Show("请确认输入的物料编码是否在主数据中创建");
                return;*/
            }
        }
    }


}


