﻿namespace MMClient.InventoryManagement
{
    partial class Frm_CancelDocument
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_Confirm = new System.Windows.Forms.Button();
            this.btn_Cancel = new System.Windows.Forms.Button();
            this.lb_VoucherDate = new System.Windows.Forms.Label();
            this.lb_MaterialDoc = new System.Windows.Forms.Label();
            this.lb_MaterialDocYear = new System.Windows.Forms.Label();
            this.dtp_VoucherDate = new System.Windows.Forms.DateTimePicker();
            this.txt_MaterialDoc = new System.Windows.Forms.TextBox();
            this.txt_MaterialDocYear = new System.Windows.Forms.TextBox();
            this.gb_DocDefault = new System.Windows.Forms.GroupBox();
            this.txt_MoveReason = new System.Windows.Forms.TextBox();
            this.lb_MoveReason = new System.Windows.Forms.Label();
            this.gb_Condition = new System.Windows.Forms.GroupBox();
            this.gb_DocDefault.SuspendLayout();
            this.gb_Condition.SuspendLayout();
            this.SuspendLayout();
            // 
            // btn_Confirm
            // 
            this.btn_Confirm.Location = new System.Drawing.Point(16, 45);
            this.btn_Confirm.Name = "btn_Confirm";
            this.btn_Confirm.Size = new System.Drawing.Size(61, 24);
            this.btn_Confirm.TabIndex = 0;
            this.btn_Confirm.Text = "确定";
            this.btn_Confirm.UseVisualStyleBackColor = true;
            this.btn_Confirm.Click += new System.EventHandler(this.btn_Confirm_Click);
            // 
            // btn_Cancel
            // 
            this.btn_Cancel.Location = new System.Drawing.Point(83, 45);
            this.btn_Cancel.Name = "btn_Cancel";
            this.btn_Cancel.Size = new System.Drawing.Size(61, 24);
            this.btn_Cancel.TabIndex = 1;
            this.btn_Cancel.Text = "取消";
            this.btn_Cancel.UseVisualStyleBackColor = true;
            this.btn_Cancel.Click += new System.EventHandler(this.btn_Cancel_Click);
            // 
            // lb_VoucherDate
            // 
            this.lb_VoucherDate.AutoSize = true;
            this.lb_VoucherDate.Location = new System.Drawing.Point(10, 28);
            this.lb_VoucherDate.Name = "lb_VoucherDate";
            this.lb_VoucherDate.Size = new System.Drawing.Size(53, 12);
            this.lb_VoucherDate.TabIndex = 2;
            this.lb_VoucherDate.Text = "过账日期";
            // 
            // lb_MaterialDoc
            // 
            this.lb_MaterialDoc.AutoSize = true;
            this.lb_MaterialDoc.Location = new System.Drawing.Point(10, 68);
            this.lb_MaterialDoc.Name = "lb_MaterialDoc";
            this.lb_MaterialDoc.Size = new System.Drawing.Size(53, 12);
            this.lb_MaterialDoc.TabIndex = 3;
            this.lb_MaterialDoc.Text = "物料凭证";
            // 
            // lb_MaterialDocYear
            // 
            this.lb_MaterialDocYear.AutoSize = true;
            this.lb_MaterialDocYear.Location = new System.Drawing.Point(10, 111);
            this.lb_MaterialDocYear.Name = "lb_MaterialDocYear";
            this.lb_MaterialDocYear.Size = new System.Drawing.Size(77, 12);
            this.lb_MaterialDocYear.TabIndex = 4;
            this.lb_MaterialDocYear.Text = "物料凭证年度";
            // 
            // dtp_VoucherDate
            // 
            this.dtp_VoucherDate.Location = new System.Drawing.Point(116, 22);
            this.dtp_VoucherDate.Name = "dtp_VoucherDate";
            this.dtp_VoucherDate.Size = new System.Drawing.Size(200, 21);
            this.dtp_VoucherDate.TabIndex = 5;
            // 
            // txt_MaterialDoc
            // 
            this.txt_MaterialDoc.Location = new System.Drawing.Point(116, 63);
            this.txt_MaterialDoc.Name = "txt_MaterialDoc";
            this.txt_MaterialDoc.Size = new System.Drawing.Size(200, 21);
            this.txt_MaterialDoc.TabIndex = 6;
            // 
            // txt_MaterialDocYear
            // 
            this.txt_MaterialDocYear.Location = new System.Drawing.Point(116, 104);
            this.txt_MaterialDocYear.Name = "txt_MaterialDocYear";
            this.txt_MaterialDocYear.Size = new System.Drawing.Size(106, 21);
            this.txt_MaterialDocYear.TabIndex = 7;
            // 
            // gb_DocDefault
            // 
            this.gb_DocDefault.Controls.Add(this.txt_MoveReason);
            this.gb_DocDefault.Controls.Add(this.lb_MoveReason);
            this.gb_DocDefault.Location = new System.Drawing.Point(16, 235);
            this.gb_DocDefault.Name = "gb_DocDefault";
            this.gb_DocDefault.Size = new System.Drawing.Size(783, 165);
            this.gb_DocDefault.TabIndex = 8;
            this.gb_DocDefault.TabStop = false;
            this.gb_DocDefault.Text = "凭证项目的缺省值";
            // 
            // txt_MoveReason
            // 
            this.txt_MoveReason.Location = new System.Drawing.Point(112, 45);
            this.txt_MoveReason.Name = "txt_MoveReason";
            this.txt_MoveReason.Size = new System.Drawing.Size(325, 21);
            this.txt_MoveReason.TabIndex = 1;
            // 
            // lb_MoveReason
            // 
            this.lb_MoveReason.AutoSize = true;
            this.lb_MoveReason.Location = new System.Drawing.Point(8, 50);
            this.lb_MoveReason.Name = "lb_MoveReason";
            this.lb_MoveReason.Size = new System.Drawing.Size(53, 12);
            this.lb_MoveReason.TabIndex = 0;
            this.lb_MoveReason.Text = "移动原因";
            // 
            // gb_Condition
            // 
            this.gb_Condition.Controls.Add(this.txt_MaterialDoc);
            this.gb_Condition.Controls.Add(this.lb_VoucherDate);
            this.gb_Condition.Controls.Add(this.txt_MaterialDocYear);
            this.gb_Condition.Controls.Add(this.lb_MaterialDoc);
            this.gb_Condition.Controls.Add(this.lb_MaterialDocYear);
            this.gb_Condition.Controls.Add(this.dtp_VoucherDate);
            this.gb_Condition.Location = new System.Drawing.Point(12, 78);
            this.gb_Condition.Name = "gb_Condition";
            this.gb_Condition.Size = new System.Drawing.Size(787, 145);
            this.gb_Condition.TabIndex = 9;
            this.gb_Condition.TabStop = false;
            // 
            // Frm_CancelDocument
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(860, 496);
            this.Controls.Add(this.gb_Condition);
            this.Controls.Add(this.gb_DocDefault);
            this.Controls.Add(this.btn_Cancel);
            this.Controls.Add(this.btn_Confirm);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Frm_CancelDocument";
            this.Text = "物料凭证->取消";
            this.gb_DocDefault.ResumeLayout(false);
            this.gb_DocDefault.PerformLayout();
            this.gb_Condition.ResumeLayout(false);
            this.gb_Condition.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn_Confirm;
        private System.Windows.Forms.Button btn_Cancel;
        private System.Windows.Forms.Label lb_VoucherDate;
        private System.Windows.Forms.Label lb_MaterialDoc;
        private System.Windows.Forms.Label lb_MaterialDocYear;
        private System.Windows.Forms.DateTimePicker dtp_VoucherDate;
        private System.Windows.Forms.TextBox txt_MaterialDoc;
        private System.Windows.Forms.TextBox txt_MaterialDocYear;
        private System.Windows.Forms.GroupBox gb_DocDefault;
        private System.Windows.Forms.GroupBox gb_Condition;
        private System.Windows.Forms.TextBox txt_MoveReason;
        private System.Windows.Forms.Label lb_MoveReason;
    }
}