﻿namespace MMClient.InventoryManagement
{
    partial class Frm_DisplayCancelDocument
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.lb_MoveType = new System.Windows.Forms.Label();
            this.btn_Cancel = new System.Windows.Forms.Button();
            this.btn_Confirm = new System.Windows.Forms.Button();
            this.gb_Condition = new System.Windows.Forms.GroupBox();
            this.lb_InterMoveType = new System.Windows.Forms.Label();
            this.txt_Factory = new System.Windows.Forms.TextBox();
            this.txt_Supplier = new System.Windows.Forms.TextBox();
            this.txt_MoveType = new System.Windows.Forms.TextBox();
            this.lb_Factory = new System.Windows.Forms.Label();
            this.lb_Supplier = new System.Windows.Forms.Label();
            this.gb_Project = new System.Windows.Forms.GroupBox();
            this.dgb_Project = new System.Windows.Forms.DataGridView();
            this.Column_Select = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Column_Material = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column_Count = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column_Unit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column_StockID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column_BatchID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column_FactoryId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gb_Condition.SuspendLayout();
            this.gb_Project.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgb_Project)).BeginInit();
            this.SuspendLayout();
            // 
            // lb_MoveType
            // 
            this.lb_MoveType.AutoSize = true;
            this.lb_MoveType.Location = new System.Drawing.Point(6, 25);
            this.lb_MoveType.Name = "lb_MoveType";
            this.lb_MoveType.Size = new System.Drawing.Size(53, 12);
            this.lb_MoveType.TabIndex = 0;
            this.lb_MoveType.Text = "移动类型";
            // 
            // btn_Cancel
            // 
            this.btn_Cancel.Location = new System.Drawing.Point(83, 45);
            this.btn_Cancel.Name = "btn_Cancel";
            this.btn_Cancel.Size = new System.Drawing.Size(61, 24);
            this.btn_Cancel.TabIndex = 3;
            this.btn_Cancel.Text = "取消";
            this.btn_Cancel.UseVisualStyleBackColor = true;
            this.btn_Cancel.Click += new System.EventHandler(this.btn_Cancel_Click);
            // 
            // btn_Confirm
            // 
            this.btn_Confirm.Location = new System.Drawing.Point(16, 45);
            this.btn_Confirm.Name = "btn_Confirm";
            this.btn_Confirm.Size = new System.Drawing.Size(61, 24);
            this.btn_Confirm.TabIndex = 2;
            this.btn_Confirm.Text = "确定";
            this.btn_Confirm.UseVisualStyleBackColor = true;
            // 
            // gb_Condition
            // 
            this.gb_Condition.Controls.Add(this.lb_InterMoveType);
            this.gb_Condition.Controls.Add(this.txt_Factory);
            this.gb_Condition.Controls.Add(this.txt_Supplier);
            this.gb_Condition.Controls.Add(this.txt_MoveType);
            this.gb_Condition.Controls.Add(this.lb_Factory);
            this.gb_Condition.Controls.Add(this.lb_Supplier);
            this.gb_Condition.Controls.Add(this.lb_MoveType);
            this.gb_Condition.Location = new System.Drawing.Point(16, 79);
            this.gb_Condition.Name = "gb_Condition";
            this.gb_Condition.Size = new System.Drawing.Size(787, 145);
            this.gb_Condition.TabIndex = 4;
            this.gb_Condition.TabStop = false;
            // 
            // lb_InterMoveType
            // 
            this.lb_InterMoveType.AutoSize = true;
            this.lb_InterMoveType.Location = new System.Drawing.Point(218, 24);
            this.lb_InterMoveType.Name = "lb_InterMoveType";
            this.lb_InterMoveType.Size = new System.Drawing.Size(77, 12);
            this.lb_InterMoveType.TabIndex = 6;
            this.lb_InterMoveType.Text = "解释移动类型";
            // 
            // txt_Factory
            // 
            this.txt_Factory.Location = new System.Drawing.Point(117, 100);
            this.txt_Factory.Name = "txt_Factory";
            this.txt_Factory.ReadOnly = true;
            this.txt_Factory.Size = new System.Drawing.Size(303, 21);
            this.txt_Factory.TabIndex = 5;
            // 
            // txt_Supplier
            // 
            this.txt_Supplier.Location = new System.Drawing.Point(117, 60);
            this.txt_Supplier.Name = "txt_Supplier";
            this.txt_Supplier.ReadOnly = true;
            this.txt_Supplier.Size = new System.Drawing.Size(303, 21);
            this.txt_Supplier.TabIndex = 4;
            // 
            // txt_MoveType
            // 
            this.txt_MoveType.Location = new System.Drawing.Point(117, 21);
            this.txt_MoveType.Name = "txt_MoveType";
            this.txt_MoveType.ReadOnly = true;
            this.txt_MoveType.Size = new System.Drawing.Size(95, 21);
            this.txt_MoveType.TabIndex = 3;
            // 
            // lb_Factory
            // 
            this.lb_Factory.AutoSize = true;
            this.lb_Factory.Location = new System.Drawing.Point(6, 105);
            this.lb_Factory.Name = "lb_Factory";
            this.lb_Factory.Size = new System.Drawing.Size(53, 12);
            this.lb_Factory.TabIndex = 2;
            this.lb_Factory.Text = "发货工厂";
            // 
            // lb_Supplier
            // 
            this.lb_Supplier.AutoSize = true;
            this.lb_Supplier.Location = new System.Drawing.Point(6, 63);
            this.lb_Supplier.Name = "lb_Supplier";
            this.lb_Supplier.Size = new System.Drawing.Size(41, 12);
            this.lb_Supplier.TabIndex = 1;
            this.lb_Supplier.Text = "供应商";
            // 
            // gb_Project
            // 
            this.gb_Project.Controls.Add(this.dgb_Project);
            this.gb_Project.Location = new System.Drawing.Point(16, 240);
            this.gb_Project.Name = "gb_Project";
            this.gb_Project.Size = new System.Drawing.Size(787, 244);
            this.gb_Project.TabIndex = 5;
            this.gb_Project.TabStop = false;
            this.gb_Project.Text = "项目";
            // 
            // dgb_Project
            // 
            this.dgb_Project.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgb_Project.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgb_Project.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgb_Project.ColumnHeadersHeight = 25;
            this.dgb_Project.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column_Select,
            this.Column_Material,
            this.Column_Count,
            this.Column_Unit,
            this.Column_StockID,
            this.Column_BatchID,
            this.Column_FactoryId});
            this.dgb_Project.EnableHeadersVisualStyles = false;
            this.dgb_Project.Location = new System.Drawing.Point(8, 28);
            this.dgb_Project.Name = "dgb_Project";
            this.dgb_Project.RowTemplate.Height = 23;
            this.dgb_Project.Size = new System.Drawing.Size(773, 210);
            this.dgb_Project.TabIndex = 0;
            // 
            // Column_Select
            // 
            this.Column_Select.FillWeight = 50F;
            this.Column_Select.HeaderText = "勾选";
            this.Column_Select.Name = "Column_Select";
            this.Column_Select.Width = 50;
            // 
            // Column_Material
            // 
            this.Column_Material.FillWeight = 200F;
            this.Column_Material.HeaderText = "物料";
            this.Column_Material.Name = "Column_Material";
            this.Column_Material.ReadOnly = true;
            this.Column_Material.Width = 150;
            // 
            // Column_Count
            // 
            this.Column_Count.FillWeight = 150F;
            this.Column_Count.HeaderText = "数量";
            this.Column_Count.Name = "Column_Count";
            this.Column_Count.ReadOnly = true;
            this.Column_Count.Width = 70;
            // 
            // Column_Unit
            // 
            this.Column_Unit.FillWeight = 150F;
            this.Column_Unit.HeaderText = "单位";
            this.Column_Unit.Name = "Column_Unit";
            this.Column_Unit.ReadOnly = true;
            this.Column_Unit.Width = 70;
            // 
            // Column_StockID
            // 
            this.Column_StockID.FillWeight = 150F;
            this.Column_StockID.HeaderText = "库存编码";
            this.Column_StockID.Name = "Column_StockID";
            this.Column_StockID.ReadOnly = true;
            this.Column_StockID.Width = 130;
            // 
            // Column_BatchID
            // 
            this.Column_BatchID.FillWeight = 150F;
            this.Column_BatchID.HeaderText = "批次";
            this.Column_BatchID.Name = "Column_BatchID";
            this.Column_BatchID.ReadOnly = true;
            this.Column_BatchID.Width = 150;
            // 
            // Column_FactoryId
            // 
            this.Column_FactoryId.FillWeight = 150F;
            this.Column_FactoryId.HeaderText = "工厂";
            this.Column_FactoryId.Name = "Column_FactoryId";
            this.Column_FactoryId.ReadOnly = true;
            // 
            // Frm_DisplayCancelDocument
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(860, 514);
            this.Controls.Add(this.gb_Project);
            this.Controls.Add(this.gb_Condition);
            this.Controls.Add(this.btn_Cancel);
            this.Controls.Add(this.btn_Confirm);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Frm_DisplayCancelDocument";
            this.Text = "取消物料凭证";
            this.gb_Condition.ResumeLayout(false);
            this.gb_Condition.PerformLayout();
            this.gb_Project.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgb_Project)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lb_MoveType;
        private System.Windows.Forms.Button btn_Cancel;
        private System.Windows.Forms.Button btn_Confirm;
        private System.Windows.Forms.GroupBox gb_Condition;
        private System.Windows.Forms.Label lb_Factory;
        private System.Windows.Forms.Label lb_Supplier;
        private System.Windows.Forms.TextBox txt_MoveType;
        private System.Windows.Forms.Label lb_InterMoveType;
        private System.Windows.Forms.TextBox txt_Factory;
        private System.Windows.Forms.TextBox txt_Supplier;
        private System.Windows.Forms.GroupBox gb_Project;
        private System.Windows.Forms.DataGridView dgb_Project;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column_Select;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column_Material;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column_Count;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column_Unit;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column_StockID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column_BatchID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column_FactoryId;
    }
}