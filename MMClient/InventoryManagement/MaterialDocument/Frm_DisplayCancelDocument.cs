﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MMClient.InventoryManagement
{
    public partial class Frm_DisplayCancelDocument : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        /// <summary>
        /// 无参构造函数
        /// </summary>
        /// <param name="userUI"></param>
        public Frm_DisplayCancelDocument()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 取消
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_Cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
