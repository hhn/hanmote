﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;

namespace MMClient.InventoryManagement
{
    public partial class Frm_CancelDocument : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        //主界面窗体
        UserUI frm_UserUI = null;

        public Frm_CancelDocument()
            : this(null)
        { 
        
        }

        public Frm_CancelDocument(UserUI userUI)
        {
            InitializeComponent();
            this.frm_UserUI = userUI;
        }

        /// <summary>
        /// 取消
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_Cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        ///确定 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_Confirm_Click(object sender, EventArgs e)
        {
            Frm_DisplayCancelDocument frm_DisplayCancelDocument = new Frm_DisplayCancelDocument();
            frm_DisplayCancelDocument.TopLevel = false;
            frm_DisplayCancelDocument.Dock = DockStyle.Fill;
            frm_DisplayCancelDocument.Location = new Point(0,40);
            frm_DisplayCancelDocument.Show(this.frm_UserUI.dockPnlForm, DockState.Document);
        }
    }
}
