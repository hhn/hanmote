﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Bll.StockBLL;

namespace MMClient.InventoryManagement
{
    public partial class Frm_ConsignmentStockShow : Form
    {
        DataTable basicinfo;
        public Frm_ConsignmentStockShow()
        {
            InitializeComponent();
        }

        public Frm_ConsignmentStockShow(DataTable dt)
        {
            //InitializeComponent();
            basicinfo = dt;
            InitializeComponent();
            this.dataGridView1.TopLeftHeaderCell.Value = "行号";
        }

        private void Frm_ConsignmentStockShow_Load(object sender, EventArgs e)
        {
            DataTable dt = new DataTable("dt");
            //Company_ID as 公司,Inventory.Factory_ID as 工厂,Stock_ID as 库存地,Material_ID as 物料,Batch_ID as 批次,Inventory_Type as 库存类型,Count as 数量,SKU as 单位

            dt.Columns.Add("公司", typeof(string));
            dt.Columns.Add("工厂", typeof(string));
            dt.Columns.Add("库存地", typeof(string));
            dt.Columns.Add("物料编码", typeof(string));
            dt.Columns.Add("物料名", typeof(string));
            dt.Columns.Add("批次", typeof(string));
            dt.Columns.Add("库存类型", typeof(string));
            dt.Columns.Add("数量", typeof(double));
            dt.Columns.Add("单位", typeof(string));

            for (int i = 0; i < basicinfo.Rows.Count; i++)
            {
                string s = basicinfo.Rows[i][3].ToString();
                getMaterialInfoBLL gmib = new getMaterialInfoBLL();
                string materialname = gmib.getMaterialName(basicinfo.Rows[i][3].ToString());

                DataRow dr = dt.NewRow();
                dr["公司"] = basicinfo.Rows[i][0].ToString();
                dr["工厂"] = basicinfo.Rows[i][1].ToString();
                dr["库存地"] = basicinfo.Rows[i][2].ToString();
                dr["物料编码"] = basicinfo.Rows[i][3].ToString();
                dr["物料名"] = materialname;
                dr["批次"] = basicinfo.Rows[i][4].ToString();
                dr["库存类型"] = basicinfo.Rows[i][5].ToString();
                dr["数量"] = Convert.ToDouble(basicinfo.Rows[i][6].ToString());
                dr["单位"] = basicinfo.Rows[i][7].ToString();

                dt.Rows.Add(dr);

            }

            this.dataGridView1.DataSource = dt;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dataGridView1_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            var dgv = sender as DataGridView;
            if (dgv != null)
            {
                Rectangle rect = new Rectangle(e.RowBounds.Location.X, e.RowBounds.Location.Y, (dgv.RowHeadersWidth + 12) / 2, e.RowBounds.Height);
                TextRenderer.DrawText(e.Graphics, (e.RowIndex + 1).ToString(), dgv.RowHeadersDefaultCellStyle.Font, rect, dgv.RowHeadersDefaultCellStyle.ForeColor, TextFormatFlags.VerticalCenter | TextFormatFlags.Right);
            }
        }
    }
}
