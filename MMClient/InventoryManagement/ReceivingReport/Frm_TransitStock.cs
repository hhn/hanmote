﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Bll.StockBLL;
using Lib.Bll.MDBll.General;

namespace MMClient.InventoryManagement
{
    public partial class Frm_TransitStock : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        public Frm_TransitStock()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string materialid = comboBox2.SelectedItem == null ? "" : comboBox2.SelectedItem.ToString().Trim();
            string factoryid = comboBox3.SelectedItem == null ? "" : comboBox3.SelectedItem.ToString().Trim();
            string stockid = comboBox4.SelectedItem == null ? "" : comboBox4.SelectedItem.ToString().Trim();
            string batchid = comboBox5.SelectedItem == null ? "" : comboBox5.SelectedItem.ToString().Trim();
            string evaluatetype = comboBox1.SelectedItem == null ? "" : comboBox1.SelectedItem.ToString().Trim();
            string materialgroup = textBox5.Text.Trim();

            if (stockid == "")
            {
                MessageBox.Show("请输入库存地编号");
                return;
            }
            if (factoryid == "")
            {
                MessageBox.Show("请输入工厂编号");
                return;
            }

            bool commonstock = checkBox1.Checked;
            bool specialtype1 = checkBox2.Checked;
            bool specialtype2 = checkBox3.Checked;
            bool negative = checkBox5.Checked;
            bool batch = checkBox6.Checked;
            bool nonzero = checkBox7.Checked;

            DataTable dt = new DataTable("dt");
            dt.Columns.Add("materialid", typeof(string));
            dt.Columns.Add("factoryid", typeof(string));
            dt.Columns.Add("stockid", typeof(string));
            dt.Columns.Add("batchid", typeof(string));
            dt.Columns.Add("evaluatetype", typeof(string));
            dt.Columns.Add("materialgroup", typeof(string));
            dt.Columns.Add("commonstock", typeof(bool));
            dt.Columns.Add("specialtype1", typeof(bool));
            dt.Columns.Add("specialtype2", typeof(bool));
            dt.Columns.Add("negative", typeof(bool));
            dt.Columns.Add("batch", typeof(bool));
            dt.Columns.Add("nonzero", typeof(bool));

            DataRow dr = dt.NewRow();
            dr["materialid"] = materialid;
            dr["factoryid"] = factoryid;
            dr["stockid"] = stockid;
            dr["batchid"] = batchid;
            dr["evaluatetype"] = evaluatetype;
            dr["materialgroup"] = materialgroup;
            dr["commonstock"] = commonstock;
            dr["specialtype1"] = specialtype1;
            dr["specialtype2"] = specialtype2;
            dr["negative"] = negative;
            dr["batch"] = batch;
            dr["nonzero"] = nonzero;
            dt.Rows.Add(dr);

            TransitStockBLL ssb = new TransitStockBLL();
            DataTable dt0 = ssb.showStock(dt);
            if (dt0 != null)
            {
                Frm_TransitStockShow fts = new Frm_TransitStockShow(dt0);
                fts.Show();

            }
            else
            {
                MessageBox.Show("暂无符合条件的物料，请检查输入是否正确");
                return;
            }
        }

        private void Frm_TransitStock_Load(object sender, EventArgs e)
        {
            List<string> mlist = new List<string>();
            List<string> flist = new List<string>();
            List<string> slist = new List<string>();
            List<string> blist = new List<string>();
            getMaterialInfoBLL gmb = new getMaterialInfoBLL();
            DataTable dt = gmb.getMaterialID();
            DataTable dt0 = gmb.getFactoryID();
            DataTable dt1 = gmb.getStockID();
            DataTable dt2 = gmb.getbatchid();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                mlist.Insert(i, dt.Rows[i][0].ToString());
            }
            for (int i = 0; i < dt0.Rows.Count; i++)
            {
                flist.Insert(i, dt0.Rows[i][0].ToString());
            }
            for (int i = 0; i < dt1.Rows.Count; i++)
            {
                slist.Insert(i, dt1.Rows[i][0].ToString());
            }
            for (int i = 0; i < dt2.Rows.Count; i++)
            {
                blist.Insert(i, dt2.Rows[i][0].ToString());
            }

            ComboBoxItem comb = new ComboBoxItem();
            comb.FuzzyQury(comboBox3, flist);
            comb.FuzzyQury(comboBox2, mlist);
            comb.FuzzyQury(comboBox4, slist);
            comb.FuzzyQury(comboBox5, blist);
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            textBox1.Text = null;
            string materialid = comboBox2.SelectedItem == null ? "" : comboBox2.SelectedItem.ToString().Trim();
            if (materialid != "")
            {
                string materialname;
                getMaterialInfoBLL gmib = new getMaterialInfoBLL();
                try
                {
                    materialname = gmib.getMaterialName(materialid);
                }
                catch
                {
                    materialname = "";
                }
                this.textBox1.Text = materialname;
            }
        }
    }
}
