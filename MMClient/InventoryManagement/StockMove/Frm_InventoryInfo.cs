﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Common.CommonUtils;
using Lib.Bll.StockBLL;
using Lib.Model.StockModel;


namespace MMClient.InventoryManagement
{
    public partial class Frm_InventoryInfo : Form
    {
        InventoryInfoBLL inventoryInfoBLL = new InventoryInfoBLL();
        Frm_Receiving frm_Receiving = new Frm_Receiving();
        String moveTypeId = "";
        int eventNo = -1;
        /// <summary>
        /// 只读型常量，提示信息
        /// </summary>
        private readonly string str_txt_Material = "物料编码/物料名称/双击选择";
        /// <summary>
        /// 只读型常量，提示信息
        /// </summary>
        private readonly string str_txt_Factory = "工厂编码/工厂名称/双击选择";
        /// <summary>
        /// 只读型常量，提示信息
        /// </summary>
        private readonly string str_txt_Stock = "库存编码/库存名称/双击选择";

        public Frm_InventoryInfo()
            :this(null,null,-1)
        { 
            //无参构造方法
        }

        public Frm_InventoryInfo(Frm_Receiving frm_Receiving,string moveTypeId,int eventNo)
        {
            InitializeComponent();
            this.frm_Receiving = frm_Receiving;
            this.moveTypeId = moveTypeId;
            this.eventNo = eventNo;
            this.dgv_Details.TopLeftHeaderCell.Value = "行号";
            this.cbb_InventoryState.SelectedIndex = 2;
            //当窗体加载时，让其焦点在标签上
            this.lb_Material.Select();
            this.txt_Material.Text = this.str_txt_Material;
            //使用系统颜色要带系统声明即System.Drawing.Color
            this.txt_Material.ForeColor = System.Drawing.Color.LightGray;
            this.txt_Factory.Text = this.str_txt_Factory;
            //使用系统颜色要带系统声明即System.Drawing.Color
            this.txt_Factory.ForeColor = System.Drawing.Color.LightGray;
            this.txt_Stock.Text = this.str_txt_Stock;
            //使用系统颜色要带系统声明即System.Drawing.Color
            this.txt_Stock.ForeColor = System.Drawing.Color.LightGray;
        }

        /// <summary>
        /// 搜索
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_Search_Click(object sender, EventArgs e)
        {
            //每次点击时，先清空原来的dgv
            this.dgv_Details.Rows.Clear();
            try
            {
                //清楚DataGridView控件中的数据
                dgv_Details.Rows.Clear();
                this.lb_Number.Text = "0";
                //取得查询集合
                List<InventoryExtendModel> inventoryExtendModelList = inventoryInfoBLL.findInventoryInfo(this.txt_Material.Text.Trim(), this.txt_Factory.Text.Trim(), this.cbb_InventoryState.Text.Trim(), this.txt_Stock.Text.Trim());
                if (inventoryExtendModelList != null)
                {
                    //取得查到的集合包含的条数
                    int count = inventoryExtendModelList.Count;
                    int i = 0;
                    foreach (InventoryExtendModel inventoryExtendModel in inventoryExtendModelList)
                    {
                        dgv_Details.Rows.Add(1);
                        dgv_Details.Rows[i].Cells["Col_MaterialId"].Value = inventoryExtendModel.Material_ID;
                        dgv_Details.Rows[i].Cells["Col_MaterialName"].Value = inventoryExtendModel.Material_Name;
                        dgv_Details.Rows[i].Cells["Col_Factory"].Value = inventoryExtendModel.Factory_ID;
                        dgv_Details.Rows[i].Cells["Col_Stock"].Value = inventoryExtendModel.Stock_ID;
                        dgv_Details.Rows[i].Cells["Col_Measurement"].Value = inventoryExtendModel.SKU;
                        dgv_Details.Rows[i].Cells["Col_Number"].Value = inventoryExtendModel.Count;
                        dgv_Details.Rows[i].Cells["Col_StockState"].Value = inventoryExtendModel.Inventory_State;
                        dgv_Details.Rows[i].Cells["Col_BatchId"].Value = inventoryExtendModel.Batch_ID;
                        i++;
                    }
                    this.lb_Number.Text = count.ToString();
                }
                else
                {
                    MessageBox.Show("没有查到相关数据","温馨提示",MessageBoxButtons.OK,MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageUtil.ShowError(ex.Message);
            }
        }

        /// <summary>
        /// 鼠标双击单元格时发生
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgv_Details_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            //MessageBox.Show("hello hanmote");
            string material_Id = this.dgv_Details.CurrentRow.Cells["Col_MaterialId"].Value.ToString();
            string inventoryState = this.dgv_Details.CurrentRow.Cells["Col_StockState"].Value.ToString();
            string stockId = this.dgv_Details.CurrentRow.Cells["Col_Stock"].Value.ToString();
            string batchId = this.dgv_Details.CurrentRow.Cells["Col_BatchId"].Value.ToString();
            string factoryId = this.dgv_Details.CurrentRow.Cells["Col_Factory"].Value.ToString();
            try
            {
                InventoryExtendModel inventoryExtendModel = inventoryInfoBLL.findInventoryInfoByMaterialId(material_Id, inventoryState, stockId, batchId, factoryId);
                if (inventoryExtendModel != null)
                {
                    frm_Receiving.stockTrans(inventoryExtendModel, moveTypeId);
                    //关闭窗口

                    this.Close();
                }
                else
                {
                    MessageBox.Show("请选择正确信息", "温馨提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageUtil.ShowError(ex.Message);
            }
        }

        /// <summary>
        /// 当双击物料输入框时弹出物料选择信息
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_Material_DoubleClick(object sender, EventArgs e)
        {
            new Frm_SelectMaterial().ShowDialog();
        }
        /// <summary>
        /// 当双击工厂输入框时弹出物料选择信息
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_Factory_DoubleClick(object sender, EventArgs e)
        {
            new Frm_SelectFactory().ShowDialog();
        }
        /// <summary>
        /// 当双击库存输入框时弹出物料选择信息
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_Stock_DoubleClick(object sender, EventArgs e)
        {
            new Frm_SelectStock().ShowDialog();
        }

        /// <summary>
        /// 画行号
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgv_Details_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            var dgv = sender as DataGridView;
            if (dgv != null)
            {
                Rectangle rect = new Rectangle(e.RowBounds.Location.X, e.RowBounds.Location.Y, (dgv.RowHeadersWidth + 12) / 2, e.RowBounds.Height);
                TextRenderer.DrawText(e.Graphics, (e.RowIndex + 1).ToString(), dgv.RowHeadersDefaultCellStyle.Font, rect, dgv.RowHeadersDefaultCellStyle.ForeColor, TextFormatFlags.VerticalCenter | TextFormatFlags.Right);
            }
        }

        /// <summary>
        /// 当鼠标进入单元格时发生
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgv_Details_CellMouseEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
            {
                this.dgv_Details.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.LightBlue;
                DataGridViewCellStyle style = new DataGridViewCellStyle();
                style.BackColor = Color.CornflowerBlue;
                this.dgv_Details.Rows[e.RowIndex].HeaderCell.Style = style;
            }
        }

        /// <summary>
        /// 当鼠标移出单元格时发生
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgv_Details_CellMouseLeave(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
            {
                this.dgv_Details.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.White;
                DataGridViewCellStyle style = new DataGridViewCellStyle();
                style.BackColor = Color.WhiteSmoke;
                this.dgv_Details.Rows[e.RowIndex].HeaderCell.Style = style;
            }
        }

        /// <summary>
        /// 当鼠标进入“物料”输入框时发生
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_Material_Enter(object sender, EventArgs e)
        {
            if (this.txt_Material.Text.Trim().Equals(str_txt_Material))
            {
                //将textbox内容置空
                this.txt_Material.Text = "";
                //再次输入值时，颜色呈黑色
                this.txt_Material.ForeColor = System.Drawing.Color.Black;
            }
        }
        /// <summary>
        /// 当鼠标离开“物料”输入框时发生
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_Material_Leave(object sender, EventArgs e)
        {
            if (this.txt_Material.Text.Trim() == "")
            {
                this.txt_Material.Text = str_txt_Material;
                this.txt_Material.ForeColor = System.Drawing.Color.LightGray;
            }
        }

        /// <summary>
        /// 当鼠标进入“工厂”输入框时发生
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_Factory_Enter(object sender, EventArgs e)
        {
            if (this.txt_Factory.Text.Trim().Equals(str_txt_Factory))
            {
                //将textbox内容置空
                this.txt_Factory.Text = "";
                //再次输入值时，颜色呈黑色
                this.txt_Factory.ForeColor = System.Drawing.Color.Black;
            }
        }
        /// <summary>
        /// 当鼠标离开“工厂”输入框时发生
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_Factory_Leave(object sender, EventArgs e)
        {
            if (this.txt_Factory.Text.Trim() == "")
            {
                this.txt_Factory.Text = str_txt_Factory;
                this.txt_Factory.ForeColor = System.Drawing.Color.LightGray;
            }
        }
        /// <summary>
        /// 当鼠标进入“库存”输入框时发生
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_Stock_Enter(object sender, EventArgs e)
        {
            if (this.txt_Stock.Text.Trim().Equals(str_txt_Stock))
            {
                //将textbox内容置空
                this.txt_Stock.Text = "";
                //再次输入值时，颜色呈黑色
                this.txt_Stock.ForeColor = System.Drawing.Color.Black;
            }
        }
        /// <summary>
        /// 当鼠标离开“库存”输入框时发生
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_Stock_Leave(object sender, EventArgs e)
        {
            if (this.txt_Stock.Text.Trim() == "")
            {
                this.txt_Stock.Text = str_txt_Stock;
                this.txt_Stock.ForeColor = System.Drawing.Color.LightGray;
            }
        }

        /// <summary>
        /// 取消按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_Cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// 确定按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_Confirm_Click(object sender, EventArgs e)
        {
                if (2 == this.eventNo)
                {
                    //new一个保存库存信息的list集合
                    List<InventoryExtendModel> inventoryExtendModelList = new List<InventoryExtendModel>();

                    foreach (DataGridViewRow dgvr in this.dgv_Details.Rows)
                    {
                        if (dgvr.Selected)
                        {
                            //new一个保存库存信息的模板
                            InventoryExtendModel inventoryExtendModel = new InventoryExtendModel();
                            //MessageBox.Show("you are selected");
                            inventoryExtendModel.Material_ID = dgvr.Cells["Col_MaterialId"].Value.ToString();
                            inventoryExtendModel.Material_Name = dgvr.Cells["Col_MaterialName"].Value.ToString();
                            inventoryExtendModel.Factory_ID = dgvr.Cells["Col_Factory"].Value.ToString();
                            inventoryExtendModel.Count = Convert.ToInt32(dgvr.Cells["Col_Number"].Value.ToString());
                            inventoryExtendModel.SKU = dgvr.Cells["Col_Measurement"].Value.ToString();
                            inventoryExtendModel.Stock_ID = dgvr.Cells["Col_Stock"].Value.ToString();
                            inventoryExtendModel.Batch_ID = dgvr.Cells["Col_BatchId"].Value.ToString();
                            inventoryExtendModel.Inventory_State = dgvr.Cells["Col_StockState"].Value.ToString();
                            inventoryExtendModelList.Add(inventoryExtendModel);
                        }
                    }
                    //将选中的行记录下来，然后传回父窗体中
                    frm_Receiving.multiAddBind(inventoryExtendModelList);
                    //关闭窗口
                    this.Close();
                }
        }
    }
}
