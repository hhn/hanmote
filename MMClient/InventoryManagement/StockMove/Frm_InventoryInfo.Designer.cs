﻿namespace MMClient.InventoryManagement
{
    partial class Frm_InventoryInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.gb_Query = new System.Windows.Forms.GroupBox();
            this.lb_Unit = new System.Windows.Forms.Label();
            this.lb_Number = new System.Windows.Forms.Label();
            this.lb_SearchResult = new System.Windows.Forms.Label();
            this.btn_Search = new System.Windows.Forms.Button();
            this.txt_Stock = new System.Windows.Forms.TextBox();
            this.txt_Factory = new System.Windows.Forms.TextBox();
            this.lb_Stock = new System.Windows.Forms.Label();
            this.lb_Factory = new System.Windows.Forms.Label();
            this.cbb_InventoryState = new System.Windows.Forms.ComboBox();
            this.lb_InventoryState = new System.Windows.Forms.Label();
            this.txt_Material = new System.Windows.Forms.TextBox();
            this.lb_Material = new System.Windows.Forms.Label();
            this.dgv_Details = new System.Windows.Forms.DataGridView();
            this.Col_MaterialId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Col_MaterialName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Col_Factory = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Col_Stock = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Col_Measurement = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Col_Number = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Col_StockState = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Col_BatchId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btn_Confirm = new System.Windows.Forms.Button();
            this.btn_Cancel = new System.Windows.Forms.Button();
            this.gb_Materials = new System.Windows.Forms.GroupBox();
            this.lb_Notes = new System.Windows.Forms.Label();
            this.gb_Query.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_Details)).BeginInit();
            this.gb_Materials.SuspendLayout();
            this.SuspendLayout();
            // 
            // gb_Query
            // 
            this.gb_Query.Controls.Add(this.lb_Unit);
            this.gb_Query.Controls.Add(this.lb_Number);
            this.gb_Query.Controls.Add(this.lb_SearchResult);
            this.gb_Query.Controls.Add(this.btn_Search);
            this.gb_Query.Controls.Add(this.txt_Stock);
            this.gb_Query.Controls.Add(this.txt_Factory);
            this.gb_Query.Controls.Add(this.lb_Stock);
            this.gb_Query.Controls.Add(this.lb_Factory);
            this.gb_Query.Controls.Add(this.cbb_InventoryState);
            this.gb_Query.Controls.Add(this.lb_InventoryState);
            this.gb_Query.Controls.Add(this.txt_Material);
            this.gb_Query.Controls.Add(this.lb_Material);
            this.gb_Query.Location = new System.Drawing.Point(5, 13);
            this.gb_Query.Name = "gb_Query";
            this.gb_Query.Size = new System.Drawing.Size(960, 115);
            this.gb_Query.TabIndex = 0;
            this.gb_Query.TabStop = false;
            // 
            // lb_Unit
            // 
            this.lb_Unit.Location = new System.Drawing.Point(880, 45);
            this.lb_Unit.Name = "lb_Unit";
            this.lb_Unit.Size = new System.Drawing.Size(20, 12);
            this.lb_Unit.TabIndex = 13;
            this.lb_Unit.Text = "条";
            // 
            // lb_Number
            // 
            this.lb_Number.Location = new System.Drawing.Point(861, 46);
            this.lb_Number.Name = "lb_Number";
            this.lb_Number.Size = new System.Drawing.Size(15, 12);
            this.lb_Number.TabIndex = 12;
            this.lb_Number.Text = "0";
            // 
            // lb_SearchResult
            // 
            this.lb_SearchResult.AutoSize = true;
            this.lb_SearchResult.Location = new System.Drawing.Point(800, 45);
            this.lb_SearchResult.Name = "lb_SearchResult";
            this.lb_SearchResult.Size = new System.Drawing.Size(65, 12);
            this.lb_SearchResult.TabIndex = 11;
            this.lb_SearchResult.Text = "搜索结果：";
            // 
            // btn_Search
            // 
            this.btn_Search.Location = new System.Drawing.Point(668, 36);
            this.btn_Search.Name = "btn_Search";
            this.btn_Search.Size = new System.Drawing.Size(110, 31);
            this.btn_Search.TabIndex = 8;
            this.btn_Search.Text = "搜索";
            this.btn_Search.UseVisualStyleBackColor = true;
            this.btn_Search.Click += new System.EventHandler(this.btn_Search_Click);
            // 
            // txt_Stock
            // 
            this.txt_Stock.Location = new System.Drawing.Point(424, 64);
            this.txt_Stock.Name = "txt_Stock";
            this.txt_Stock.Size = new System.Drawing.Size(198, 21);
            this.txt_Stock.TabIndex = 7;
            this.txt_Stock.DoubleClick += new System.EventHandler(this.txt_Stock_DoubleClick);
            this.txt_Stock.Enter += new System.EventHandler(this.txt_Stock_Enter);
            this.txt_Stock.Leave += new System.EventHandler(this.txt_Stock_Leave);
            // 
            // txt_Factory
            // 
            this.txt_Factory.Location = new System.Drawing.Point(424, 18);
            this.txt_Factory.Name = "txt_Factory";
            this.txt_Factory.Size = new System.Drawing.Size(198, 21);
            this.txt_Factory.TabIndex = 6;
            this.txt_Factory.DoubleClick += new System.EventHandler(this.txt_Factory_DoubleClick);
            this.txt_Factory.Enter += new System.EventHandler(this.txt_Factory_Enter);
            this.txt_Factory.Leave += new System.EventHandler(this.txt_Factory_Leave);
            // 
            // lb_Stock
            // 
            this.lb_Stock.AutoSize = true;
            this.lb_Stock.Location = new System.Drawing.Point(356, 68);
            this.lb_Stock.Name = "lb_Stock";
            this.lb_Stock.Size = new System.Drawing.Size(41, 12);
            this.lb_Stock.TabIndex = 5;
            this.lb_Stock.Text = "库存地";
            // 
            // lb_Factory
            // 
            this.lb_Factory.AutoSize = true;
            this.lb_Factory.Location = new System.Drawing.Point(356, 23);
            this.lb_Factory.Name = "lb_Factory";
            this.lb_Factory.Size = new System.Drawing.Size(29, 12);
            this.lb_Factory.TabIndex = 4;
            this.lb_Factory.Text = "工厂";
            // 
            // cbb_InventoryState
            // 
            this.cbb_InventoryState.FormattingEnabled = true;
            this.cbb_InventoryState.Items.AddRange(new object[] {
            "全部物料",
            "质检库存",
            "非限制库存",
            "冻结库存",
            "在途库存"});
            this.cbb_InventoryState.Location = new System.Drawing.Point(84, 64);
            this.cbb_InventoryState.Name = "cbb_InventoryState";
            this.cbb_InventoryState.Size = new System.Drawing.Size(217, 20);
            this.cbb_InventoryState.TabIndex = 3;
            // 
            // lb_InventoryState
            // 
            this.lb_InventoryState.AutoSize = true;
            this.lb_InventoryState.Location = new System.Drawing.Point(18, 68);
            this.lb_InventoryState.Name = "lb_InventoryState";
            this.lb_InventoryState.Size = new System.Drawing.Size(53, 12);
            this.lb_InventoryState.TabIndex = 2;
            this.lb_InventoryState.Text = "库存状态";
            // 
            // txt_Material
            // 
            this.txt_Material.Location = new System.Drawing.Point(84, 18);
            this.txt_Material.Name = "txt_Material";
            this.txt_Material.Size = new System.Drawing.Size(217, 21);
            this.txt_Material.TabIndex = 1;
            this.txt_Material.DoubleClick += new System.EventHandler(this.txt_Material_DoubleClick);
            this.txt_Material.Enter += new System.EventHandler(this.txt_Material_Enter);
            this.txt_Material.Leave += new System.EventHandler(this.txt_Material_Leave);
            // 
            // lb_Material
            // 
            this.lb_Material.AutoSize = true;
            this.lb_Material.Location = new System.Drawing.Point(16, 23);
            this.lb_Material.Name = "lb_Material";
            this.lb_Material.Size = new System.Drawing.Size(29, 12);
            this.lb_Material.TabIndex = 0;
            this.lb_Material.Text = "物料";
            // 
            // dgv_Details
            // 
            this.dgv_Details.AllowUserToAddRows = false;
            this.dgv_Details.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgv_Details.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_Details.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv_Details.ColumnHeadersHeight = 25;
            this.dgv_Details.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgv_Details.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Col_MaterialId,
            this.Col_MaterialName,
            this.Col_Factory,
            this.Col_Stock,
            this.Col_Measurement,
            this.Col_Number,
            this.Col_StockState,
            this.Col_BatchId});
            this.dgv_Details.EnableHeadersVisualStyles = false;
            this.dgv_Details.Location = new System.Drawing.Point(3, 13);
            this.dgv_Details.Name = "dgv_Details";
            this.dgv_Details.ReadOnly = true;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_Details.RowHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgv_Details.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.LightBlue;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black;
            this.dgv_Details.RowsDefaultCellStyle = dataGridViewCellStyle3;
            this.dgv_Details.RowTemplate.Height = 23;
            this.dgv_Details.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv_Details.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv_Details.ShowCellErrors = false;
            this.dgv_Details.Size = new System.Drawing.Size(945, 296);
            this.dgv_Details.TabIndex = 1;
            this.dgv_Details.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgv_Details_CellMouseDoubleClick);
            this.dgv_Details.CellMouseEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_Details_CellMouseEnter);
            this.dgv_Details.CellMouseLeave += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_Details_CellMouseLeave);
            this.dgv_Details.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.dgv_Details_RowPostPaint);
            // 
            // Col_MaterialId
            // 
            this.Col_MaterialId.HeaderText = "物品编号";
            this.Col_MaterialId.Name = "Col_MaterialId";
            this.Col_MaterialId.ReadOnly = true;
            this.Col_MaterialId.Width = 130;
            // 
            // Col_MaterialName
            // 
            this.Col_MaterialName.HeaderText = "物品名称";
            this.Col_MaterialName.Name = "Col_MaterialName";
            this.Col_MaterialName.ReadOnly = true;
            this.Col_MaterialName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Col_MaterialName.Width = 130;
            // 
            // Col_Factory
            // 
            this.Col_Factory.HeaderText = "工厂";
            this.Col_Factory.Name = "Col_Factory";
            this.Col_Factory.ReadOnly = true;
            this.Col_Factory.Width = 120;
            // 
            // Col_Stock
            // 
            this.Col_Stock.HeaderText = "库存地";
            this.Col_Stock.Name = "Col_Stock";
            this.Col_Stock.ReadOnly = true;
            this.Col_Stock.Width = 120;
            // 
            // Col_Measurement
            // 
            this.Col_Measurement.HeaderText = "单位";
            this.Col_Measurement.Name = "Col_Measurement";
            this.Col_Measurement.ReadOnly = true;
            this.Col_Measurement.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Col_Number
            // 
            this.Col_Number.HeaderText = "数量";
            this.Col_Number.Name = "Col_Number";
            this.Col_Number.ReadOnly = true;
            // 
            // Col_StockState
            // 
            this.Col_StockState.HeaderText = "库存状态";
            this.Col_StockState.Name = "Col_StockState";
            this.Col_StockState.ReadOnly = true;
            this.Col_StockState.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Col_BatchId
            // 
            this.Col_BatchId.HeaderText = "批次";
            this.Col_BatchId.Name = "Col_BatchId";
            this.Col_BatchId.ReadOnly = true;
            this.Col_BatchId.Width = 104;
            // 
            // btn_Confirm
            // 
            this.btn_Confirm.Location = new System.Drawing.Point(781, 449);
            this.btn_Confirm.Name = "btn_Confirm";
            this.btn_Confirm.Size = new System.Drawing.Size(89, 32);
            this.btn_Confirm.TabIndex = 2;
            this.btn_Confirm.Text = "确定";
            this.btn_Confirm.UseVisualStyleBackColor = true;
            this.btn_Confirm.Click += new System.EventHandler(this.btn_Confirm_Click);
            // 
            // btn_Cancel
            // 
            this.btn_Cancel.Location = new System.Drawing.Point(876, 449);
            this.btn_Cancel.Name = "btn_Cancel";
            this.btn_Cancel.Size = new System.Drawing.Size(89, 32);
            this.btn_Cancel.TabIndex = 3;
            this.btn_Cancel.Text = "取消";
            this.btn_Cancel.UseVisualStyleBackColor = true;
            this.btn_Cancel.Click += new System.EventHandler(this.btn_Cancel_Click);
            // 
            // gb_Materials
            // 
            this.gb_Materials.Controls.Add(this.dgv_Details);
            this.gb_Materials.Location = new System.Drawing.Point(5, 128);
            this.gb_Materials.Name = "gb_Materials";
            this.gb_Materials.Size = new System.Drawing.Size(960, 315);
            this.gb_Materials.TabIndex = 4;
            this.gb_Materials.TabStop = false;
            // 
            // lb_Notes
            // 
            this.lb_Notes.AutoSize = true;
            this.lb_Notes.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lb_Notes.Location = new System.Drawing.Point(8, 451);
            this.lb_Notes.Name = "lb_Notes";
            this.lb_Notes.Size = new System.Drawing.Size(338, 12);
            this.lb_Notes.TabIndex = 5;
            this.lb_Notes.Text = "多选方法：鼠标选中某行上下拖拉或按住ctr键点选多行。";
            // 
            // Frm_InventoryInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(970, 489);
            this.Controls.Add(this.lb_Notes);
            this.Controls.Add(this.gb_Materials);
            this.Controls.Add(this.btn_Cancel);
            this.Controls.Add(this.btn_Confirm);
            this.Controls.Add(this.gb_Query);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Frm_InventoryInfo";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "请选择物品";
            this.gb_Query.ResumeLayout(false);
            this.gb_Query.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_Details)).EndInit();
            this.gb_Materials.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox gb_Query;
        private System.Windows.Forms.DataGridView dgv_Details;
        private System.Windows.Forms.Button btn_Search;
        private System.Windows.Forms.TextBox txt_Stock;
        private System.Windows.Forms.TextBox txt_Factory;
        private System.Windows.Forms.Label lb_Stock;
        private System.Windows.Forms.Label lb_Factory;
        private System.Windows.Forms.ComboBox cbb_InventoryState;
        private System.Windows.Forms.Label lb_InventoryState;
        private System.Windows.Forms.TextBox txt_Material;
        private System.Windows.Forms.Label lb_Material;
        private System.Windows.Forms.Label lb_Unit;
        private System.Windows.Forms.Label lb_Number;
        private System.Windows.Forms.Label lb_SearchResult;
        private System.Windows.Forms.DataGridViewTextBoxColumn Col_MaterialId;
        private System.Windows.Forms.DataGridViewTextBoxColumn Col_MaterialName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Col_Factory;
        private System.Windows.Forms.DataGridViewTextBoxColumn Col_Stock;
        private System.Windows.Forms.DataGridViewTextBoxColumn Col_Measurement;
        private System.Windows.Forms.DataGridViewTextBoxColumn Col_Number;
        private System.Windows.Forms.DataGridViewTextBoxColumn Col_StockState;
        private System.Windows.Forms.DataGridViewTextBoxColumn Col_BatchId;
        private System.Windows.Forms.Button btn_Confirm;
        private System.Windows.Forms.Button btn_Cancel;
        private System.Windows.Forms.GroupBox gb_Materials;
        private System.Windows.Forms.Label lb_Notes;
    }
}