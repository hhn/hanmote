﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Windows.Forms;

namespace MMClient.InventoryManagement
{
    public class CommonMethod_AboutComboBox
    {
        /// <summary>
        /// 手动向combobox中添加内容
        /// </summary>
        /// <param name="list">要添加到comboBox的集合</param>
        /// <param name="cbb">指定具体的comboBox</param>
        public static void addItmesToComboBox(List<string> list, ComboBox cbb)
        {
            //增加一个空白行
            cbb.Items.Add("");

            if (list != null)
            {
                foreach (string str in list)
                {
                    cbb.Items.Add(str);
                }
            }
        }

    }
}
