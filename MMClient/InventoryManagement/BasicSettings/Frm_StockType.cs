﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Lib.Common.CommonUtils;
using Lib.Bll.StockBLL;
using Lib.Model.StockModel;

namespace MMClient.InventoryManagement
{
    public partial class Frm_StockType : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        /// <summary>
        /// 操作库存类型的业务逻辑层
        /// </summary>
        StockTypeBLL stockTypeBLL = new StockTypeBLL();

        /// <summary>
        /// 无参构造函数
        /// </summary>
        public Frm_StockType()
        {
            InitializeComponent();

            this.dgv_StockTypeDetails.TopLeftHeaderCell.Value = "序号";
        }

        #region 事件类操作
        /// <summary>
        /// 按钮->新增
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_New_Click(object sender, EventArgs e)
        {
            Frm_NewStockType frm_NewStockType = new Frm_NewStockType(1);
            DialogResult result =  frm_NewStockType.ShowDialog();
            if (DialogResult.OK == result) 
            {
                //存放库存类型的模板类
                StockTypeModel stockTypeModel = new StockTypeModel();
                stockTypeModel.Id = frm_NewStockType.TypeId;
                stockTypeModel.Type = frm_NewStockType.TypeName;
                stockTypeModel.Valid = 1; //初次创建的库存类型都是有效的
                stockTypeModel.Create_ts = Convert.ToDateTime(DateTime.Now.ToLongDateString()); //库存类型创建时间
                //取得插入数据库的行数
                int influnceRows = stockTypeBLL.insertStockType(stockTypeModel);
                if (influnceRows > 0)
                {
                    this.updateDGV(stockTypeModel);
                    MessageUtil.ShowTips("库存类型新增了一条");
                }
                else
                {
                    MessageUtil.ShowError("出错了!");
                }
            }
        }

        /// <summary>
        /// 按钮->修改
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_Update_Click(object sender, EventArgs e)
        {
            //获取当前行
            DataGridViewRow currentRow = this.dgv_StockTypeDetails.CurrentRow;
            if (currentRow == null)
            {
                MessageUtil.ShowTips("亲，没有可修改的数据哟!");
                return;
            }
            //获取库存类型ID
            string strTypeId = currentRow.Cells["Col_StockType_Id"].Value.ToString();
            int intTypeId = Convert.ToInt32(strTypeId);

            Frm_NewStockType frm_NewStockType = new Frm_NewStockType(2, intTypeId);
            DialogResult result = frm_NewStockType.ShowDialog();
            if (DialogResult.OK == result)
            {
                //存放库存类型的模板类
                StockTypeModel stockTypeModel = new StockTypeModel();
                stockTypeModel.Id = intTypeId;
                stockTypeModel.Type = frm_NewStockType.TypeName;
                stockTypeModel.Update_ts = Convert.ToDateTime(DateTime.Now.ToLongDateString()); //库存类型创建时间
                //取得插入数据库的行数
                int influnceRows = stockTypeBLL.updateStockTypeByTypeId(stockTypeModel);
                if (influnceRows > 0)
                {
                    this.updateDGV(stockTypeModel, currentRow);
                    MessageUtil.ShowTips("库存类型修改成功");
                }
                else
                {
                    MessageUtil.ShowError("出错了!");
                }
            }
        }

        /// <summary>
        /// 按钮->删除
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_Delete_Click(object sender, EventArgs e)
        {
            //获取当前行
            DataGridViewRow currentRow = this.dgv_StockTypeDetails.CurrentRow;
            if (currentRow == null)
            {
                MessageUtil.ShowTips("亲，没有可删除的数据哟!");
                return;
            }
            //获取有效性
            string strValid = currentRow.Cells["Col_Flag"].Value.ToString();
            if (strValid.Equals("有效"))
            {
                DialogResult result = MessageBox.Show(this, "您确定要删除库存类型吗？", "警告信息", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
                if (result == DialogResult.OK)
                {
                    //获取库存类型ID
                    string typeId = currentRow.Cells["Col_StockType_Id"].Value.ToString();
                    int influence = stockTypeBLL.deleteStockTypeByTypeId(Convert.ToInt32(typeId));
                    if (influence > 0)
                    {
                        currentRow.Cells["Col_Flag"].Value = "失效";
                    }
                    else
                    {
                        MessageUtil.ShowError("出错啦!");
                    }
                }
            }
            else
            {
                DialogResult result = MessageBox.Show(this, "您确定要恢复库存类型吗？", "警告信息", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
                if (result == DialogResult.OK)
                {
                    //获取复杂工单号
                    string typeId = currentRow.Cells["Col_StockType_Id"].Value.ToString();
                    int influence = stockTypeBLL.restoreStockTypeByTypeId(Convert.ToInt32(typeId));
                    if (influence > 0)
                    {
                        currentRow.Cells["Col_Flag"].Value = "有效";
                    }
                    else
                    {
                        MessageUtil.ShowError("出错啦!");
                    }
                }
            }
        }
        
        /// <summary>
        /// 当前行右键->新建
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ToolStripMenuItem_New_Click(object sender, EventArgs e)
        {
            this.btn_New_Click(sender, e);
        }

        /// <summary>
        /// 当前行右键->删除
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ToolStripMenuItem_Delete_Click(object sender, EventArgs e)
        {
            this.btn_Delete_Click(sender,e);
        }

        /// <summary>
        /// 当前行右键->修改
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ToolStripMenuItem_Update_Click(object sender, EventArgs e)
        {
            this.btn_Update_Click(sender, e);
        }

        /// <summary>
        /// 当鼠标右键时出现的操作菜单
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgv_StockTypeDetails_CellMouseDown(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right && e.ColumnIndex >= 0 && e.RowIndex >= 0)
            {
                DataGridViewRow currentRow = this.dgv_StockTypeDetails.CurrentRow;
                //获取有效性
                string strValid = currentRow.Cells["Col_Flag"].Value.ToString();
                if (strValid.Equals("有效"))
                {
                    this.ToolStripMenuItem_Delete.Text = "删除";
                    //this.toolStripMenuItem_Delete.Image = Properties.Resources.novalid;
                }
                else
                {
                    this.ToolStripMenuItem_Delete.Text = "恢复";
                    //this.toolStripMenuItem_Delete.Image = Properties.Resources.valid;
                }
                this.contextMenuStrip.Show(MousePosition.X, MousePosition.Y);
            }
        }

        /// <summary>
        /// 当这个窗体加载时触发
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Frm_StockType_Load(object sender, EventArgs e)
        {
            this.btn_DisplayALL_Click(sender,e);
        }

        /// <summary>
        /// 显示全部
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_DisplayALL_Click(object sender, EventArgs e)
        {
            this.iniDGVData();
        }

        /// <summary>
        /// 画行号
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgv_StockTypeDetails_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            var dgv = sender as DataGridView;
            if (dgv != null)
            {
                Rectangle rect = new Rectangle(e.RowBounds.Location.X, e.RowBounds.Location.Y, (dgv.RowHeadersWidth + 12) / 2, e.RowBounds.Height);
                TextRenderer.DrawText(e.Graphics, (e.RowIndex + 1).ToString(), dgv.RowHeadersDefaultCellStyle.Font, rect, dgv.RowHeadersDefaultCellStyle.ForeColor, TextFormatFlags.VerticalCenter | TextFormatFlags.Right);
            }
        }

        #endregion

        #region 公共调用方法
        /// <summary>
        /// 初始化datagridview中的数据
        /// </summary>
        private void iniDGVData() 
        {
            //先清空DGV中的数据
            this.dgv_StockTypeDetails.Rows.Clear();

            List<StockTypeModel> stockTypeModelList = stockTypeBLL.getAllStockType();
            if (stockTypeModelList != null)
            {
                int i = 0;
                foreach (StockTypeModel stockTypeModel in stockTypeModelList)
                {
                    this.dgv_StockTypeDetails.Rows.Add(1);
                    dgv_StockTypeDetails.Rows[i].Cells["Col_StockType_Id"].Value = stockTypeModel.Id;
                    if (1 == stockTypeModel.Valid)
                    {
                        dgv_StockTypeDetails.Rows[i].Cells["Col_Flag"].Value = "有效";
                    }
                    else
                    {
                        dgv_StockTypeDetails.Rows[i].Cells["Col_Flag"].Value = "无效";
                    }

                    dgv_StockTypeDetails.Rows[i].Cells["Col_StockType_Name"].Value = stockTypeModel.Type;
                    i++;
                }
            }
            else
            {
                //MessageBox.Show("没有查到相关数据", "温馨提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        /// <summary>
        /// 插入数据的同时更新DGV
        /// </summary>
        /// <param name="stockTypeModel"></param>
        private void updateDGV(StockTypeModel stockTypeModel)
        {
            //给dgv添加一行，添加在最后
            this.dgv_StockTypeDetails.Rows.Add(1);
            //获取dgv最后一行的位置，然后插入数据
            dgv_StockTypeDetails.Rows[this.dgv_StockTypeDetails.Rows.Count - 1].Cells["Col_StockType_Id"].Value = stockTypeModel.Id;
            dgv_StockTypeDetails.Rows[this.dgv_StockTypeDetails.Rows.Count - 1].Cells["Col_Flag"].Value = "有效";
            dgv_StockTypeDetails.Rows[this.dgv_StockTypeDetails.Rows.Count - 1].Cells["Col_StockType_Name"].Value = stockTypeModel.Type;
        }

        /// <summary>
        /// 修改操作,同时更新当前选中行的数据
        /// </summary>
        /// <param name="moveTypeModel"></param>
        /// <param name="currentRow"></param>
        public void updateDGV(StockTypeModel stockTypeModel, DataGridViewRow currentRow)
        {
            //修改当前行数据
            currentRow.Cells["Col_StockType_Name"].Value = stockTypeModel.Type;
        }
        #endregion

    }
}
