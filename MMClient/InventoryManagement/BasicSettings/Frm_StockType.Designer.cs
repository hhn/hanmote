﻿namespace MMClient.InventoryManagement
{
    partial class Frm_StockType 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btn_Update = new System.Windows.Forms.Button();
            this.btn_New = new System.Windows.Forms.Button();
            this.btn_Delete = new System.Windows.Forms.Button();
            this.dgv_StockTypeDetails = new System.Windows.Forms.DataGridView();
            this.contextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.ToolStripMenuItem_New = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripMenuItem_Delete = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripMenuItem_Update = new System.Windows.Forms.ToolStripMenuItem();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btn_DisplayALL = new System.Windows.Forms.Button();
            this.Col_StockType_Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Col_Flag = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Col_StockType_Name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_StockTypeDetails)).BeginInit();
            this.contextMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // btn_Update
            // 
            this.btn_Update.Location = new System.Drawing.Point(83, 45);
            this.btn_Update.Name = "btn_Update";
            this.btn_Update.Size = new System.Drawing.Size(61, 24);
            this.btn_Update.TabIndex = 10;
            this.btn_Update.Text = "修改";
            this.btn_Update.UseVisualStyleBackColor = true;
            this.btn_Update.Click += new System.EventHandler(this.btn_Update_Click);
            // 
            // btn_New
            // 
            this.btn_New.Location = new System.Drawing.Point(16, 45);
            this.btn_New.Name = "btn_New";
            this.btn_New.Size = new System.Drawing.Size(61, 24);
            this.btn_New.TabIndex = 8;
            this.btn_New.Text = "新增";
            this.btn_New.UseVisualStyleBackColor = true;
            this.btn_New.Click += new System.EventHandler(this.btn_New_Click);
            // 
            // btn_Delete
            // 
            this.btn_Delete.Location = new System.Drawing.Point(150, 45);
            this.btn_Delete.Name = "btn_Delete";
            this.btn_Delete.Size = new System.Drawing.Size(61, 24);
            this.btn_Delete.TabIndex = 9;
            this.btn_Delete.Text = "删除";
            this.btn_Delete.UseVisualStyleBackColor = true;
            this.btn_Delete.Click += new System.EventHandler(this.btn_Delete_Click);
            // 
            // dgv_StockTypeDetails
            // 
            this.dgv_StockTypeDetails.AllowUserToAddRows = false;
            this.dgv_StockTypeDetails.AllowUserToResizeColumns = false;
            this.dgv_StockTypeDetails.AllowUserToResizeRows = false;
            this.dgv_StockTypeDetails.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.dgv_StockTypeDetails.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgv_StockTypeDetails.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_StockTypeDetails.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.dgv_StockTypeDetails.ColumnHeadersHeight = 25;
            this.dgv_StockTypeDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgv_StockTypeDetails.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Col_StockType_Id,
            this.Col_Flag,
            this.Col_StockType_Name});
            this.dgv_StockTypeDetails.EnableHeadersVisualStyles = false;
            this.dgv_StockTypeDetails.Location = new System.Drawing.Point(17, 92);
            this.dgv_StockTypeDetails.MultiSelect = false;
            this.dgv_StockTypeDetails.Name = "dgv_StockTypeDetails";
            this.dgv_StockTypeDetails.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.LightBlue;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.Black;
            this.dgv_StockTypeDetails.RowsDefaultCellStyle = dataGridViewCellStyle7;
            this.dgv_StockTypeDetails.RowTemplate.Height = 23;
            this.dgv_StockTypeDetails.RowTemplate.ReadOnly = true;
            this.dgv_StockTypeDetails.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv_StockTypeDetails.Size = new System.Drawing.Size(549, 406);
            this.dgv_StockTypeDetails.TabIndex = 11;
            this.dgv_StockTypeDetails.CellMouseDown += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgv_StockTypeDetails_CellMouseDown);
            this.dgv_StockTypeDetails.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.dgv_StockTypeDetails_RowPostPaint);
            // 
            // contextMenuStrip
            // 
            this.contextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripMenuItem_New,
            this.ToolStripMenuItem_Delete,
            this.ToolStripMenuItem_Update});
            this.contextMenuStrip.Name = "contextMenuStrip";
            this.contextMenuStrip.Size = new System.Drawing.Size(101, 70);
            // 
            // ToolStripMenuItem_New
            // 
            this.ToolStripMenuItem_New.Name = "ToolStripMenuItem_New";
            this.ToolStripMenuItem_New.Size = new System.Drawing.Size(100, 22);
            this.ToolStripMenuItem_New.Text = "新增";
            this.ToolStripMenuItem_New.Click += new System.EventHandler(this.ToolStripMenuItem_New_Click);
            // 
            // ToolStripMenuItem_Delete
            // 
            this.ToolStripMenuItem_Delete.Name = "ToolStripMenuItem_Delete";
            this.ToolStripMenuItem_Delete.Size = new System.Drawing.Size(100, 22);
            this.ToolStripMenuItem_Delete.Text = "删除";
            this.ToolStripMenuItem_Delete.Click += new System.EventHandler(this.ToolStripMenuItem_Delete_Click);
            // 
            // ToolStripMenuItem_Update
            // 
            this.ToolStripMenuItem_Update.Name = "ToolStripMenuItem_Update";
            this.ToolStripMenuItem_Update.Size = new System.Drawing.Size(100, 22);
            this.ToolStripMenuItem_Update.Text = "修改";
            this.ToolStripMenuItem_Update.Click += new System.EventHandler(this.ToolStripMenuItem_Update_Click);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "编号";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn1.ToolTipText = "编号";
            this.dataGridViewTextBoxColumn1.Width = 120;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.dataGridViewTextBoxColumn2.DefaultCellStyle = dataGridViewCellStyle8;
            this.dataGridViewTextBoxColumn2.HeaderText = "名称";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn2.ToolTipText = "名称";
            // 
            // btn_DisplayALL
            // 
            this.btn_DisplayALL.Location = new System.Drawing.Point(217, 46);
            this.btn_DisplayALL.Name = "btn_DisplayALL";
            this.btn_DisplayALL.Size = new System.Drawing.Size(75, 23);
            this.btn_DisplayALL.TabIndex = 12;
            this.btn_DisplayALL.Text = "显示全部";
            this.btn_DisplayALL.UseVisualStyleBackColor = true;
            this.btn_DisplayALL.Click += new System.EventHandler(this.btn_DisplayALL_Click);
            // 
            // Col_StockType_Id
            // 
            this.Col_StockType_Id.DataPropertyName = "Id";
            this.Col_StockType_Id.HeaderText = "编号";
            this.Col_StockType_Id.Name = "Col_StockType_Id";
            this.Col_StockType_Id.ReadOnly = true;
            this.Col_StockType_Id.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Col_StockType_Id.ToolTipText = "编号";
            this.Col_StockType_Id.Width = 120;
            // 
            // Col_Flag
            // 
            this.Col_Flag.HeaderText = "是否有效";
            this.Col_Flag.Name = "Col_Flag";
            this.Col_Flag.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Col_StockType_Name
            // 
            this.Col_StockType_Name.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Col_StockType_Name.DataPropertyName = "Type";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.Col_StockType_Name.DefaultCellStyle = dataGridViewCellStyle6;
            this.Col_StockType_Name.HeaderText = "名称";
            this.Col_StockType_Name.Name = "Col_StockType_Name";
            this.Col_StockType_Name.ReadOnly = true;
            this.Col_StockType_Name.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Col_StockType_Name.ToolTipText = "名称";
            // 
            // Frm_StockType
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(915, 510);
            this.Controls.Add(this.btn_DisplayALL);
            this.Controls.Add(this.dgv_StockTypeDetails);
            this.Controls.Add(this.btn_Update);
            this.Controls.Add(this.btn_New);
            this.Controls.Add(this.btn_Delete);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Frm_StockType";
            this.Text = "库存类型设置";
            this.Load += new System.EventHandler(this.Frm_StockType_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_StockTypeDetails)).EndInit();
            this.contextMenuStrip.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn_Update;
        private System.Windows.Forms.Button btn_New;
        private System.Windows.Forms.Button btn_Delete;
        private System.Windows.Forms.DataGridView dgv_StockTypeDetails;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuItem_New;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuItem_Delete;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuItem_Update;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.Button btn_DisplayALL;
        private System.Windows.Forms.DataGridViewTextBoxColumn Col_StockType_Id;
        private System.Windows.Forms.DataGridViewTextBoxColumn Col_Flag;
        private System.Windows.Forms.DataGridViewTextBoxColumn Col_StockType_Name;
    }
}