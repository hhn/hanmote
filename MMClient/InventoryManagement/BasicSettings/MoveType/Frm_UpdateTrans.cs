﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Bll.StockBLL;
using Lib.Model.StockModel;
using Lib.Common.CommonUtils;

namespace MMClient.InventoryManagement
{
    public partial class Frm_UpdateTrans : Form
    {
        Frm_TransOrEvent frm_TransOrEvent = new Frm_TransOrEvent();
        StockTransInfoBLL stockTransInfoBLL = new StockTransInfoBLL();
        DataGridViewRow currentRow = new DataGridViewRow();
        public Frm_UpdateTrans()
            :this(null,null)
        {
            //无参构造函数
        }

        public Frm_UpdateTrans(Frm_TransOrEvent frm_TransOrEvent, DataGridViewRow currentRow)
        {
            InitializeComponent();
            //初始化参数
            this.frm_TransOrEvent = frm_TransOrEvent;
            this.currentRow = currentRow;
            this.initialData();
        }

        /// <summary>
        /// 初始化绑定数据
        /// </summary>
        private void initialData()
        {
            try
            {
                string stockTransId = currentRow.Cells["Col_TransId"].Value.ToString();
                //执行数据库查询操作，返回移动类型基本信息
                StockTransExtendModel stockTransExtendModel = stockTransInfoBLL.findStockTransInfoByID(stockTransId);
                if (stockTransExtendModel != null)
                {
                    this.txt_TransId.Text = stockTransExtendModel.StockTrans_ID;
                    this.txt_TransName.Text = stockTransExtendModel.StockTrans_Name;
                    if (stockTransExtendModel.StockTrans_Activate)
                    {
                        this.rb_Yes.Select();
                    }
                    else
                    {
                        this.rb_No.Select();
                    }
                }
                else
                {
                    MessageBox.Show("没有查到相关数据", "温馨提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageUtil.ShowError(ex.Message);
            }
        }

        /// <summary>
        /// 取消
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_Cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// 确定
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_Confirm_Click(object sender, EventArgs e)
        {
            //构造一个StockTransModel实例
            StockTransModel stockTransModel = new StockTransModel();
            //获取窗体中用户输入事务编号
            string StockTrans_ID = this.txt_TransId.Text.Trim();
            //获取窗体中用户输入事务名称
            string StockTrans_Name = this.txt_TransName.Text.Trim();
            //获取用户所选的激活状态
            Boolean StockTrans_Activate = false;
            if (this.rb_Yes.Checked)
            {
                StockTrans_Activate = true;
            }
            if (this.rb_No.Checked)
            {
                StockTrans_Activate = false;
            }
            if ("".Equals(StockTrans_Name))
            {
                MessageBox.Show("描述文本不能为空", "温馨提示", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                stockTransModel.StockTrans_ID = StockTrans_ID;
                stockTransModel.StockTrans_Name = StockTrans_Name;
                stockTransModel.StockTrans_Activate = StockTrans_Activate;
                try
                {
                    //执行插入语句，并返回结果
                    int queryResult = stockTransInfoBLL.updateStockTransInfo(stockTransModel);
                    if (queryResult > 0)
                    {
                        //数据插入成功显示消息
                        MessageBox.Show("数据修改成功", "信息提示");
                        //返回到父窗体的代码
                        //此处要修改
                        this.frm_TransOrEvent.updateDGV(stockTransModel, currentRow);
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("数据修改失败", "错误提示", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                catch (Exception ex)
                {
                    MessageUtil.ShowError(ex.Message);
                }
            }
        }
    }
}
