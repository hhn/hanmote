﻿namespace MMClient.InventoryManagement
{
    partial class Frm_NewMoveType
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.gb_NewMoveType = new System.Windows.Forms.GroupBox();
            this.lb_ToolTipText2 = new System.Windows.Forms.Label();
            this.lb_ToolTip2 = new System.Windows.Forms.Label();
            this.lb_ToolTipText = new System.Windows.Forms.Label();
            this.lb_ToolTip = new System.Windows.Forms.Label();
            this.cbb_TransactionDtb = new System.Windows.Forms.ComboBox();
            this.txt_MoveTypeReason = new System.Windows.Forms.TextBox();
            this.txt_MoveTypeMeaning = new System.Windows.Forms.TextBox();
            this.txt_MoveTypeId = new System.Windows.Forms.TextBox();
            this.lb_TransactionDtb = new System.Windows.Forms.Label();
            this.lb_MoveTypeReason = new System.Windows.Forms.Label();
            this.lb_MoveTypeMeaning = new System.Windows.Forms.Label();
            this.lb_MoveTypeId = new System.Windows.Forms.Label();
            this.btn_Confirm = new System.Windows.Forms.Button();
            this.btn_Cancel = new System.Windows.Forms.Button();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.gb_NewMoveType.SuspendLayout();
            this.SuspendLayout();
            // 
            // gb_NewMoveType
            // 
            this.gb_NewMoveType.Controls.Add(this.lb_ToolTipText2);
            this.gb_NewMoveType.Controls.Add(this.lb_ToolTip2);
            this.gb_NewMoveType.Controls.Add(this.lb_ToolTipText);
            this.gb_NewMoveType.Controls.Add(this.lb_ToolTip);
            this.gb_NewMoveType.Controls.Add(this.cbb_TransactionDtb);
            this.gb_NewMoveType.Controls.Add(this.txt_MoveTypeReason);
            this.gb_NewMoveType.Controls.Add(this.txt_MoveTypeMeaning);
            this.gb_NewMoveType.Controls.Add(this.txt_MoveTypeId);
            this.gb_NewMoveType.Controls.Add(this.lb_TransactionDtb);
            this.gb_NewMoveType.Controls.Add(this.lb_MoveTypeReason);
            this.gb_NewMoveType.Controls.Add(this.lb_MoveTypeMeaning);
            this.gb_NewMoveType.Controls.Add(this.lb_MoveTypeId);
            this.gb_NewMoveType.Location = new System.Drawing.Point(12, 12);
            this.gb_NewMoveType.Name = "gb_NewMoveType";
            this.gb_NewMoveType.Size = new System.Drawing.Size(612, 245);
            this.gb_NewMoveType.TabIndex = 0;
            this.gb_NewMoveType.TabStop = false;
            this.gb_NewMoveType.Text = "新建";
            // 
            // lb_ToolTipText2
            // 
            this.lb_ToolTipText2.AutoSize = true;
            this.lb_ToolTipText2.Location = new System.Drawing.Point(365, 69);
            this.lb_ToolTipText2.Name = "lb_ToolTipText2";
            this.lb_ToolTipText2.Size = new System.Drawing.Size(29, 12);
            this.lb_ToolTipText2.TabIndex = 11;
            this.lb_ToolTipText2.Text = "必填";
            // 
            // lb_ToolTip2
            // 
            this.lb_ToolTip2.AutoSize = true;
            this.lb_ToolTip2.ForeColor = System.Drawing.Color.Red;
            this.lb_ToolTip2.Location = new System.Drawing.Point(357, 69);
            this.lb_ToolTip2.Name = "lb_ToolTip2";
            this.lb_ToolTip2.Size = new System.Drawing.Size(11, 12);
            this.lb_ToolTip2.TabIndex = 10;
            this.lb_ToolTip2.Text = "*";
            // 
            // lb_ToolTipText
            // 
            this.lb_ToolTipText.AutoSize = true;
            this.lb_ToolTipText.Location = new System.Drawing.Point(366, 31);
            this.lb_ToolTipText.Name = "lb_ToolTipText";
            this.lb_ToolTipText.Size = new System.Drawing.Size(239, 12);
            this.lb_ToolTipText.TabIndex = 9;
            this.lb_ToolTipText.Text = "必填，建议采用\"Z\"开头的三位编码,如\"Z01\"";
            // 
            // lb_ToolTip
            // 
            this.lb_ToolTip.AutoSize = true;
            this.lb_ToolTip.ForeColor = System.Drawing.Color.Red;
            this.lb_ToolTip.Location = new System.Drawing.Point(357, 32);
            this.lb_ToolTip.Name = "lb_ToolTip";
            this.lb_ToolTip.Size = new System.Drawing.Size(11, 12);
            this.lb_ToolTip.TabIndex = 8;
            this.lb_ToolTip.Text = "*";
            // 
            // cbb_TransactionDtb
            // 
            this.cbb_TransactionDtb.FormattingEnabled = true;
            this.cbb_TransactionDtb.Location = new System.Drawing.Point(128, 189);
            this.cbb_TransactionDtb.Name = "cbb_TransactionDtb";
            this.cbb_TransactionDtb.Size = new System.Drawing.Size(223, 20);
            this.cbb_TransactionDtb.TabIndex = 7;
            // 
            // txt_MoveTypeReason
            // 
            this.txt_MoveTypeReason.Location = new System.Drawing.Point(128, 104);
            this.txt_MoveTypeReason.Multiline = true;
            this.txt_MoveTypeReason.Name = "txt_MoveTypeReason";
            this.txt_MoveTypeReason.Size = new System.Drawing.Size(223, 64);
            this.txt_MoveTypeReason.TabIndex = 6;
            // 
            // txt_MoveTypeMeaning
            // 
            this.txt_MoveTypeMeaning.Location = new System.Drawing.Point(128, 65);
            this.txt_MoveTypeMeaning.Name = "txt_MoveTypeMeaning";
            this.txt_MoveTypeMeaning.Size = new System.Drawing.Size(223, 21);
            this.txt_MoveTypeMeaning.TabIndex = 5;
            this.toolTip.SetToolTip(this.txt_MoveTypeMeaning, "必填");
            // 
            // txt_MoveTypeId
            // 
            this.txt_MoveTypeId.Location = new System.Drawing.Point(128, 27);
            this.txt_MoveTypeId.Name = "txt_MoveTypeId";
            this.txt_MoveTypeId.Size = new System.Drawing.Size(223, 21);
            this.txt_MoveTypeId.TabIndex = 4;
            this.toolTip.SetToolTip(this.txt_MoveTypeId, "必填，建议采用\"Z\"开头的三位编码,如\"Z01\"");
            // 
            // lb_TransactionDtb
            // 
            this.lb_TransactionDtb.AutoSize = true;
            this.lb_TransactionDtb.Location = new System.Drawing.Point(16, 193);
            this.lb_TransactionDtb.Name = "lb_TransactionDtb";
            this.lb_TransactionDtb.Size = new System.Drawing.Size(53, 12);
            this.lb_TransactionDtb.TabIndex = 3;
            this.lb_TransactionDtb.Text = "事务分配";
            // 
            // lb_MoveTypeReason
            // 
            this.lb_MoveTypeReason.AutoSize = true;
            this.lb_MoveTypeReason.Location = new System.Drawing.Point(16, 107);
            this.lb_MoveTypeReason.Name = "lb_MoveTypeReason";
            this.lb_MoveTypeReason.Size = new System.Drawing.Size(53, 12);
            this.lb_MoveTypeReason.TabIndex = 2;
            this.lb_MoveTypeReason.Text = "详细文本";
            // 
            // lb_MoveTypeMeaning
            // 
            this.lb_MoveTypeMeaning.AutoSize = true;
            this.lb_MoveTypeMeaning.Location = new System.Drawing.Point(15, 69);
            this.lb_MoveTypeMeaning.Name = "lb_MoveTypeMeaning";
            this.lb_MoveTypeMeaning.Size = new System.Drawing.Size(29, 12);
            this.lb_MoveTypeMeaning.TabIndex = 1;
            this.lb_MoveTypeMeaning.Text = "描述";
            // 
            // lb_MoveTypeId
            // 
            this.lb_MoveTypeId.AutoSize = true;
            this.lb_MoveTypeId.Location = new System.Drawing.Point(15, 32);
            this.lb_MoveTypeId.Name = "lb_MoveTypeId";
            this.lb_MoveTypeId.Size = new System.Drawing.Size(77, 12);
            this.lb_MoveTypeId.TabIndex = 0;
            this.lb_MoveTypeId.Text = "移动类型编码";
            // 
            // btn_Confirm
            // 
            this.btn_Confirm.Location = new System.Drawing.Point(468, 272);
            this.btn_Confirm.Name = "btn_Confirm";
            this.btn_Confirm.Size = new System.Drawing.Size(75, 30);
            this.btn_Confirm.TabIndex = 1;
            this.btn_Confirm.Text = "确定";
            this.btn_Confirm.UseVisualStyleBackColor = true;
            this.btn_Confirm.Click += new System.EventHandler(this.btn_Confirm_Click);
            // 
            // btn_Cancel
            // 
            this.btn_Cancel.Location = new System.Drawing.Point(549, 272);
            this.btn_Cancel.Name = "btn_Cancel";
            this.btn_Cancel.Size = new System.Drawing.Size(75, 30);
            this.btn_Cancel.TabIndex = 2;
            this.btn_Cancel.Text = "取消";
            this.btn_Cancel.UseVisualStyleBackColor = true;
            this.btn_Cancel.Click += new System.EventHandler(this.btn_Cancel_Click);
            // 
            // Frm_NewMoveType
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(636, 314);
            this.Controls.Add(this.btn_Cancel);
            this.Controls.Add(this.btn_Confirm);
            this.Controls.Add(this.gb_NewMoveType);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Frm_NewMoveType";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "新建移动类型";
            this.gb_NewMoveType.ResumeLayout(false);
            this.gb_NewMoveType.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gb_NewMoveType;
        private System.Windows.Forms.Button btn_Confirm;
        private System.Windows.Forms.Button btn_Cancel;
        private System.Windows.Forms.ComboBox cbb_TransactionDtb;
        private System.Windows.Forms.TextBox txt_MoveTypeReason;
        private System.Windows.Forms.TextBox txt_MoveTypeMeaning;
        private System.Windows.Forms.TextBox txt_MoveTypeId;
        private System.Windows.Forms.Label lb_TransactionDtb;
        private System.Windows.Forms.Label lb_MoveTypeReason;
        private System.Windows.Forms.Label lb_MoveTypeMeaning;
        private System.Windows.Forms.Label lb_MoveTypeId;
        private System.Windows.Forms.Label lb_ToolTipText;
        private System.Windows.Forms.Label lb_ToolTip;
        private System.Windows.Forms.Label lb_ToolTipText2;
        private System.Windows.Forms.Label lb_ToolTip2;
        private System.Windows.Forms.ToolTip toolTip;
    }
}