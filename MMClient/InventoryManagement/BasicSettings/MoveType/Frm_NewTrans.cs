﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Bll.StockBLL;
using Lib.Model.StockModel;
using Lib.Common.CommonUtils;

namespace MMClient.InventoryManagement
{
    public partial class Frm_NewTrans : Form
    {
        Frm_TransOrEvent frm_TransOrEvent = new Frm_TransOrEvent();
        StockTransInfoBLL stockTransInfoBLL = new StockTransInfoBLL();
        public Frm_NewTrans()
            :this(null)
        {
            //无参构造函数
        }

        public Frm_NewTrans(Frm_TransOrEvent frm_TransOrEvent)
        {
            InitializeComponent();
            this.frm_TransOrEvent = frm_TransOrEvent;
        }

        /// <summary>
        /// 取消
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_Cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// 确定
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_Confirm_Click(object sender, EventArgs e)
        {
            //构造一个StockTransModel实例
            StockTransModel stockTransModel = new StockTransModel();
            //获取窗体中用户输入事务编号
            string StockTrans_ID = this.txt_TransId.Text.Trim();
            //获取窗体中用户输入事务名称
            string StockTrans_Name = this.txt_TransName.Text.Trim();
            //获取用户所选的激活状态
            Boolean StockTrans_Activate = false;
            if (this.rb_Yes.Checked)
            {
                StockTrans_Activate = true;
            }
            if (this.rb_No.Checked)
            {
                StockTrans_Activate = false;
            }
            if ("".Equals(StockTrans_ID) || "".Equals(StockTrans_Name))
            {
                MessageBox.Show("请将必填信息填写完整", "温馨提示", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                stockTransModel.StockTrans_ID = StockTrans_ID;
                stockTransModel.StockTrans_Name = StockTrans_Name;
                stockTransModel.StockTrans_Activate = StockTrans_Activate;
                try
                {
                    //执行插入语句，并返回结果
                    int queryResult = stockTransInfoBLL.insertNewStockTransInfo(stockTransModel);
                    if (queryResult > 0)
                    {
                        //数据插入成功显示消息
                        MessageBox.Show("数据插入成功", "信息提示");
                        //返回到父窗体的代码
                        //this.frm_TransOrEvent.updateDGV(stockTransModel);
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("数据插入失败", "错误提示", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                catch (Exception ex)
                {
                    MessageUtil.ShowError(ex.Message);
                }
            }
        }
    }
}
