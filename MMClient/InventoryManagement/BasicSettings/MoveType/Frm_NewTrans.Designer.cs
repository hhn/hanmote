﻿namespace MMClient.InventoryManagement
{
    partial class Frm_NewTrans
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gb_NewTrans = new System.Windows.Forms.GroupBox();
            this.rb_No = new System.Windows.Forms.RadioButton();
            this.rb_Yes = new System.Windows.Forms.RadioButton();
            this.lb_ToolTipText2 = new System.Windows.Forms.Label();
            this.lb_ToolTipText1 = new System.Windows.Forms.Label();
            this.lb_ToolTip2 = new System.Windows.Forms.Label();
            this.lb_ToolTip1 = new System.Windows.Forms.Label();
            this.txt_TransName = new System.Windows.Forms.TextBox();
            this.txt_TransId = new System.Windows.Forms.TextBox();
            this.lb_TransActivate = new System.Windows.Forms.Label();
            this.lb_TransName = new System.Windows.Forms.Label();
            this.lb_TransId = new System.Windows.Forms.Label();
            this.btn_Cancel = new System.Windows.Forms.Button();
            this.btn_Confirm = new System.Windows.Forms.Button();
            this.gb_NewTrans.SuspendLayout();
            this.SuspendLayout();
            // 
            // gb_NewTrans
            // 
            this.gb_NewTrans.Controls.Add(this.rb_No);
            this.gb_NewTrans.Controls.Add(this.rb_Yes);
            this.gb_NewTrans.Controls.Add(this.lb_ToolTipText2);
            this.gb_NewTrans.Controls.Add(this.lb_ToolTipText1);
            this.gb_NewTrans.Controls.Add(this.lb_ToolTip2);
            this.gb_NewTrans.Controls.Add(this.lb_ToolTip1);
            this.gb_NewTrans.Controls.Add(this.txt_TransName);
            this.gb_NewTrans.Controls.Add(this.txt_TransId);
            this.gb_NewTrans.Controls.Add(this.lb_TransActivate);
            this.gb_NewTrans.Controls.Add(this.lb_TransName);
            this.gb_NewTrans.Controls.Add(this.lb_TransId);
            this.gb_NewTrans.Location = new System.Drawing.Point(12, 12);
            this.gb_NewTrans.Name = "gb_NewTrans";
            this.gb_NewTrans.Size = new System.Drawing.Size(576, 173);
            this.gb_NewTrans.TabIndex = 0;
            this.gb_NewTrans.TabStop = false;
            this.gb_NewTrans.Text = "新建";
            // 
            // rb_No
            // 
            this.rb_No.AutoSize = true;
            this.rb_No.Location = new System.Drawing.Point(170, 115);
            this.rb_No.Name = "rb_No";
            this.rb_No.Size = new System.Drawing.Size(35, 16);
            this.rb_No.TabIndex = 14;
            this.rb_No.TabStop = true;
            this.rb_No.Text = "否";
            this.rb_No.UseVisualStyleBackColor = true;
            // 
            // rb_Yes
            // 
            this.rb_Yes.AutoSize = true;
            this.rb_Yes.Checked = true;
            this.rb_Yes.Location = new System.Drawing.Point(116, 115);
            this.rb_Yes.Name = "rb_Yes";
            this.rb_Yes.Size = new System.Drawing.Size(35, 16);
            this.rb_Yes.TabIndex = 13;
            this.rb_Yes.TabStop = true;
            this.rb_Yes.Text = "是";
            this.rb_Yes.UseVisualStyleBackColor = true;
            // 
            // lb_ToolTipText2
            // 
            this.lb_ToolTipText2.AutoSize = true;
            this.lb_ToolTipText2.Location = new System.Drawing.Point(329, 73);
            this.lb_ToolTipText2.Name = "lb_ToolTipText2";
            this.lb_ToolTipText2.Size = new System.Drawing.Size(29, 12);
            this.lb_ToolTipText2.TabIndex = 12;
            this.lb_ToolTipText2.Text = "必填";
            // 
            // lb_ToolTipText1
            // 
            this.lb_ToolTipText1.AutoSize = true;
            this.lb_ToolTipText1.Location = new System.Drawing.Point(329, 31);
            this.lb_ToolTipText1.Name = "lb_ToolTipText1";
            this.lb_ToolTipText1.Size = new System.Drawing.Size(239, 12);
            this.lb_ToolTipText1.TabIndex = 10;
            this.lb_ToolTipText1.Text = "必填，建议采用\"B\"开头的三位编码,如\"B01\"";
            // 
            // lb_ToolTip2
            // 
            this.lb_ToolTip2.AutoSize = true;
            this.lb_ToolTip2.ForeColor = System.Drawing.Color.Red;
            this.lb_ToolTip2.Location = new System.Drawing.Point(319, 31);
            this.lb_ToolTip2.Name = "lb_ToolTip2";
            this.lb_ToolTip2.Size = new System.Drawing.Size(11, 12);
            this.lb_ToolTip2.TabIndex = 9;
            this.lb_ToolTip2.Text = "*";
            // 
            // lb_ToolTip1
            // 
            this.lb_ToolTip1.AutoSize = true;
            this.lb_ToolTip1.ForeColor = System.Drawing.Color.Red;
            this.lb_ToolTip1.Location = new System.Drawing.Point(318, 73);
            this.lb_ToolTip1.Name = "lb_ToolTip1";
            this.lb_ToolTip1.Size = new System.Drawing.Size(11, 12);
            this.lb_ToolTip1.TabIndex = 9;
            this.lb_ToolTip1.Text = "*";
            // 
            // txt_TransName
            // 
            this.txt_TransName.Location = new System.Drawing.Point(116, 70);
            this.txt_TransName.Name = "txt_TransName";
            this.txt_TransName.Size = new System.Drawing.Size(196, 21);
            this.txt_TransName.TabIndex = 4;
            // 
            // txt_TransId
            // 
            this.txt_TransId.Location = new System.Drawing.Point(116, 26);
            this.txt_TransId.Name = "txt_TransId";
            this.txt_TransId.Size = new System.Drawing.Size(197, 21);
            this.txt_TransId.TabIndex = 3;
            // 
            // lb_TransActivate
            // 
            this.lb_TransActivate.AutoSize = true;
            this.lb_TransActivate.Location = new System.Drawing.Point(16, 117);
            this.lb_TransActivate.Name = "lb_TransActivate";
            this.lb_TransActivate.Size = new System.Drawing.Size(53, 12);
            this.lb_TransActivate.TabIndex = 2;
            this.lb_TransActivate.Text = "是否激活";
            // 
            // lb_TransName
            // 
            this.lb_TransName.Location = new System.Drawing.Point(16, 73);
            this.lb_TransName.Name = "lb_TransName";
            this.lb_TransName.Size = new System.Drawing.Size(83, 12);
            this.lb_TransName.TabIndex = 1;
            this.lb_TransName.Text = "事务/事件名称     ";
            // 
            // lb_TransId
            // 
            this.lb_TransId.AutoSize = true;
            this.lb_TransId.Location = new System.Drawing.Point(16, 31);
            this.lb_TransId.Name = "lb_TransId";
            this.lb_TransId.Size = new System.Drawing.Size(83, 12);
            this.lb_TransId.TabIndex = 0;
            this.lb_TransId.Text = "事务/事件编码";
            // 
            // btn_Cancel
            // 
            this.btn_Cancel.Location = new System.Drawing.Point(514, 197);
            this.btn_Cancel.Name = "btn_Cancel";
            this.btn_Cancel.Size = new System.Drawing.Size(75, 30);
            this.btn_Cancel.TabIndex = 4;
            this.btn_Cancel.Text = "取消";
            this.btn_Cancel.UseVisualStyleBackColor = true;
            this.btn_Cancel.Click += new System.EventHandler(this.btn_Cancel_Click);
            // 
            // btn_Confirm
            // 
            this.btn_Confirm.Location = new System.Drawing.Point(433, 197);
            this.btn_Confirm.Name = "btn_Confirm";
            this.btn_Confirm.Size = new System.Drawing.Size(75, 30);
            this.btn_Confirm.TabIndex = 3;
            this.btn_Confirm.Text = "确定";
            this.btn_Confirm.UseVisualStyleBackColor = true;
            this.btn_Confirm.Click += new System.EventHandler(this.btn_Confirm_Click);
            // 
            // Frm_NewTrans
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(600, 240);
            this.Controls.Add(this.btn_Cancel);
            this.Controls.Add(this.btn_Confirm);
            this.Controls.Add(this.gb_NewTrans);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Frm_NewTrans";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "新建事务/事件";
            this.gb_NewTrans.ResumeLayout(false);
            this.gb_NewTrans.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gb_NewTrans;
        private System.Windows.Forms.Button btn_Cancel;
        private System.Windows.Forms.Button btn_Confirm;
        private System.Windows.Forms.Label lb_TransActivate;
        private System.Windows.Forms.Label lb_TransName;
        private System.Windows.Forms.Label lb_TransId;
        private System.Windows.Forms.TextBox txt_TransName;
        private System.Windows.Forms.TextBox txt_TransId;
        private System.Windows.Forms.Label lb_ToolTip2;
        private System.Windows.Forms.Label lb_ToolTip1;
        private System.Windows.Forms.Label lb_ToolTipText1;
        private System.Windows.Forms.Label lb_ToolTipText2;
        private System.Windows.Forms.RadioButton rb_No;
        private System.Windows.Forms.RadioButton rb_Yes;
    }
}