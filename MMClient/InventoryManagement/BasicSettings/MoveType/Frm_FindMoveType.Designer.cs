﻿namespace MMClient.InventoryManagement
{
    partial class Frm_FindMoveType
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.contextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.ToolStripMenuItem_New = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripMenuItem_Delete = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripMenuItem_Update = new System.Windows.Forms.ToolStripMenuItem();
            this.btn_Delete = new System.Windows.Forms.Button();
            this.btn_New = new System.Windows.Forms.Button();
            this.btn_Update = new System.Windows.Forms.Button();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgv_MoveTypeDetails = new System.Windows.Forms.DataGridView();
            this.Col_MT_TransactionDtb = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Col_MT_Name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Col_MT_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.contextMenuStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_MoveTypeDetails)).BeginInit();
            this.SuspendLayout();
            // 
            // contextMenuStrip
            // 
            this.contextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripMenuItem_New,
            this.ToolStripMenuItem_Delete,
            this.ToolStripMenuItem_Update});
            this.contextMenuStrip.Name = "contextMenuStrip";
            this.contextMenuStrip.Size = new System.Drawing.Size(101, 70);
            // 
            // ToolStripMenuItem_New
            // 
            this.ToolStripMenuItem_New.Name = "ToolStripMenuItem_New";
            this.ToolStripMenuItem_New.Size = new System.Drawing.Size(100, 22);
            this.ToolStripMenuItem_New.Text = "新增";
            this.ToolStripMenuItem_New.Click += new System.EventHandler(this.ToolStripMenuItem_New_Click);
            // 
            // ToolStripMenuItem_Delete
            // 
            this.ToolStripMenuItem_Delete.Name = "ToolStripMenuItem_Delete";
            this.ToolStripMenuItem_Delete.Size = new System.Drawing.Size(100, 22);
            this.ToolStripMenuItem_Delete.Text = "删除";
            this.ToolStripMenuItem_Delete.Click += new System.EventHandler(this.ToolStripMenuItem_Delete_Click);
            // 
            // ToolStripMenuItem_Update
            // 
            this.ToolStripMenuItem_Update.Name = "ToolStripMenuItem_Update";
            this.ToolStripMenuItem_Update.Size = new System.Drawing.Size(100, 22);
            this.ToolStripMenuItem_Update.Text = "修改";
            this.ToolStripMenuItem_Update.Click += new System.EventHandler(this.ToolStripMenuItem_Update_Click);
            // 
            // btn_Delete
            // 
            this.btn_Delete.Location = new System.Drawing.Point(148, 45);
            this.btn_Delete.Name = "btn_Delete";
            this.btn_Delete.Size = new System.Drawing.Size(61, 24);
            this.btn_Delete.TabIndex = 4;
            this.btn_Delete.Text = "删除";
            this.btn_Delete.UseVisualStyleBackColor = true;
            this.btn_Delete.Click += new System.EventHandler(this.btn_Delete_Click);
            // 
            // btn_New
            // 
            this.btn_New.Location = new System.Drawing.Point(16, 45);
            this.btn_New.Name = "btn_New";
            this.btn_New.Size = new System.Drawing.Size(61, 24);
            this.btn_New.TabIndex = 3;
            this.btn_New.Text = "新增";
            this.btn_New.UseVisualStyleBackColor = true;
            this.btn_New.Click += new System.EventHandler(this.btn_New_Click);
            // 
            // btn_Update
            // 
            this.btn_Update.Location = new System.Drawing.Point(82, 45);
            this.btn_Update.Name = "btn_Update";
            this.btn_Update.Size = new System.Drawing.Size(61, 24);
            this.btn_Update.TabIndex = 7;
            this.btn_Update.Text = "修改";
            this.btn_Update.UseVisualStyleBackColor = true;
            this.btn_Update.Click += new System.EventHandler(this.btn_Update_Click);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "移动类型编号";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn1.ToolTipText = "移动类型编号";
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "移动类型意义";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn2.ToolTipText = "移动类型意义";
            this.dataGridViewTextBoxColumn2.Width = 200;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "移动原因";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn3.ToolTipText = "移动原因";
            this.dataGridViewTextBoxColumn3.Width = 200;
            // 
            // dgv_MoveTypeDetails
            // 
            this.dgv_MoveTypeDetails.AllowUserToAddRows = false;
            this.dgv_MoveTypeDetails.AllowUserToResizeColumns = false;
            this.dgv_MoveTypeDetails.AllowUserToResizeRows = false;
            this.dgv_MoveTypeDetails.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.dgv_MoveTypeDetails.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgv_MoveTypeDetails.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_MoveTypeDetails.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv_MoveTypeDetails.ColumnHeadersHeight = 25;
            this.dgv_MoveTypeDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgv_MoveTypeDetails.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Col_MT_ID,
            this.Col_MT_Name,
            this.Col_MT_TransactionDtb});
            this.dgv_MoveTypeDetails.ContextMenuStrip = this.contextMenuStrip;
            this.dgv_MoveTypeDetails.EnableHeadersVisualStyles = false;
            this.dgv_MoveTypeDetails.Location = new System.Drawing.Point(16, 91);
            this.dgv_MoveTypeDetails.MultiSelect = false;
            this.dgv_MoveTypeDetails.Name = "dgv_MoveTypeDetails";
            this.dgv_MoveTypeDetails.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.LightBlue;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black;
            this.dgv_MoveTypeDetails.RowsDefaultCellStyle = dataGridViewCellStyle3;
            this.dgv_MoveTypeDetails.RowTemplate.Height = 23;
            this.dgv_MoveTypeDetails.RowTemplate.ReadOnly = true;
            this.dgv_MoveTypeDetails.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv_MoveTypeDetails.Size = new System.Drawing.Size(533, 395);
            this.dgv_MoveTypeDetails.TabIndex = 2;
            this.dgv_MoveTypeDetails.CellMouseEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_MoveTypeDetails_CellMouseEnter);
            this.dgv_MoveTypeDetails.CellMouseLeave += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_MoveTypeDetails_CellMouseLeave);
            this.dgv_MoveTypeDetails.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.dgv_MoveTypeDetails_RowPostPaint);
            // 
            // Col_MT_TransactionDtb
            // 
            this.Col_MT_TransactionDtb.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Col_MT_TransactionDtb.HeaderText = "事务分配";
            this.Col_MT_TransactionDtb.Name = "Col_MT_TransactionDtb";
            this.Col_MT_TransactionDtb.ReadOnly = true;
            this.Col_MT_TransactionDtb.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Col_MT_Name
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.Col_MT_Name.DefaultCellStyle = dataGridViewCellStyle2;
            this.Col_MT_Name.HeaderText = "描述";
            this.Col_MT_Name.Name = "Col_MT_Name";
            this.Col_MT_Name.ReadOnly = true;
            this.Col_MT_Name.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Col_MT_Name.ToolTipText = "描述";
            this.Col_MT_Name.Width = 260;
            // 
            // Col_MT_ID
            // 
            this.Col_MT_ID.HeaderText = "移动类型编号";
            this.Col_MT_ID.Name = "Col_MT_ID";
            this.Col_MT_ID.ReadOnly = true;
            this.Col_MT_ID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Col_MT_ID.ToolTipText = "移动类型编号";
            // 
            // Frm_FindMoveType
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(915, 510);
            this.Controls.Add(this.dgv_MoveTypeDetails);
            this.Controls.Add(this.btn_Update);
            this.Controls.Add(this.btn_New);
            this.Controls.Add(this.btn_Delete);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Frm_FindMoveType";
            this.Text = "移动类型->查看";
            this.contextMenuStrip.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_MoveTypeDetails)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn_Delete;
        private System.Windows.Forms.Button btn_New;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuItem_New;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuItem_Delete;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuItem_Update;
        private System.Windows.Forms.Button btn_Update;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridView dgv_MoveTypeDetails;
        private System.Windows.Forms.DataGridViewTextBoxColumn Col_MT_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Col_MT_Name;
        private System.Windows.Forms.DataGridViewTextBoxColumn Col_MT_TransactionDtb;
    }
}