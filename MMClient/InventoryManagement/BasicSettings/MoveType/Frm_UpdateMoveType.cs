﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Common.CommonUtils;
using Lib.Bll.StockBLL;
using Lib.Model.StockModel;

namespace MMClient.InventoryManagement
{
    public partial class Frm_UpdateMoveType : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        MoveTypeInfoBLL moveTypeInfoBLL = new MoveTypeInfoBLL();
        StockTransInfoBLL stockTransInfoBLL = new StockTransInfoBLL();
        Frm_FindMoveType frm_FindMoveType = new Frm_FindMoveType();
        //string moveTypeId = "";
        DataGridViewRow currentRow = new DataGridViewRow();
        public Frm_UpdateMoveType()
            : this(null,null)
        { 
            //无参构造函数
        }
        public Frm_UpdateMoveType(Frm_FindMoveType frm_FindMoveType, DataGridViewRow currentRow)
        {
            InitializeComponent();
            this.lb_MoveTypeId.Select();
            //初始化参数
            this.frm_FindMoveType = frm_FindMoveType;
            this.currentRow = currentRow;
            this.initialMoveTypeData();
            this.initialTransDisData();
        }

        /// <summary>
        ///  初始化绑定数据,事务/事件
        /// </summary>
        private void initialTransDisData()
        {
            try
            {
                List<StockTransExtendModel> stockTransExtendModelList = stockTransInfoBLL.findAllStockTransInfo();
                if (stockTransExtendModelList != null)
                {
                    foreach (StockTransExtendModel stockTransExtendModel in stockTransExtendModelList)
                    {
                        if (stockTransExtendModel.StockTrans_Activate)
                        {
                            string transOrEventName = stockTransExtendModel.StockTrans_ID + " " + stockTransExtendModel.StockTrans_Name;
                            this.cbb_TransactionDtb.Items.Add(transOrEventName);
                        }
                    }
                }
                else
                {
                    MessageBox.Show("没有查到相关数据", "温馨提示", MessageBoxButtons.OK, MessageBoxIcon.Information);

                }
            }
            catch (Exception ex)
            {
                MessageUtil.ShowError(ex.Message);
            }
        }

        /// <summary>
        /// 初始化绑定数据,移动类型数据
        /// </summary>
        private void initialMoveTypeData()
        {
            try
            {
                string moveTypeId = currentRow.Cells["Col_MT_ID"].Value.ToString();
                //执行数据库查询操作，返回移动类型基本信息
                MoveTypeExtendModel moveTypeExtendModel = moveTypeInfoBLL.findMoveTypeInfoByID(moveTypeId);
                if (moveTypeExtendModel != null)
                {
                    this.txt_MoveTypeId.Text = moveTypeExtendModel.MT_ID;
                    this.txt_MoveTypeMeaning.Text = moveTypeExtendModel.MT_Name;
                    this.txt_MoveTypeReason.Text = moveTypeExtendModel.MT_Reason;
                    this.cbb_TransactionDtb.Text = moveTypeExtendModel.MT_TransactionDtb;
                }
                else
                {
                    MessageBox.Show("没有查到相关数据", "温馨提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageUtil.ShowError(ex.Message);
            }
        }

        /// <summary>
        /// 关闭该窗口
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_Cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// 确定按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_Confirm_Click(object sender, EventArgs e)
        {
            MoveTypeModel moveTypeModel = new MoveTypeModel();
            string MT_ID = this.txt_MoveTypeId.Text.Trim();
            string MT_Name = this.txt_MoveTypeMeaning.Text.Trim();
            string MT_Reason = this.txt_MoveTypeReason.Text.Trim();
            string MT_TransactionDtb = this.cbb_TransactionDtb.Text.Trim().Substring(0,3);
            if ("".Equals(MT_Name))
            {
                MessageBox.Show("描述文本不能为空", "温馨提示", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                moveTypeModel.MT_ID = MT_ID;
                moveTypeModel.MT_Name = MT_Name;
                moveTypeModel.MT_Reason = MT_Reason;
                moveTypeModel.MT_TransactionDtb = MT_TransactionDtb;
                try
                {
                    //执行插入语句，并返回结果
                    int queryResult = moveTypeInfoBLL.updateMoveTypeInfo(moveTypeModel);
                    if (queryResult > 0)
                    {
                        //数据插入成功显示消息
                        MessageBox.Show("数据修改成功", "信息提示");
                        //返回到父窗体的代码
                        //此处要修改
                        this.frm_FindMoveType.updateDGV(moveTypeModel,currentRow);
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("数据修改失败", "错误提示", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                catch (Exception ex)
                {
                    MessageUtil.ShowError(ex.Message);
                }
            }
        }
    }
}
