﻿namespace MMClient.InventoryManagement
{
    partial class Frm_UpdateTrans
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gb_UpdateTrans = new System.Windows.Forms.GroupBox();
            this.rb_No = new System.Windows.Forms.RadioButton();
            this.rb_Yes = new System.Windows.Forms.RadioButton();
            this.txt_TransName = new System.Windows.Forms.TextBox();
            this.txt_TransId = new System.Windows.Forms.TextBox();
            this.lb_TransActivate = new System.Windows.Forms.Label();
            this.lb_TransName = new System.Windows.Forms.Label();
            this.lb_TransId = new System.Windows.Forms.Label();
            this.btn_Cancel = new System.Windows.Forms.Button();
            this.btn_Confirm = new System.Windows.Forms.Button();
            this.gb_UpdateTrans.SuspendLayout();
            this.SuspendLayout();
            // 
            // gb_UpdateTrans
            // 
            this.gb_UpdateTrans.Controls.Add(this.rb_No);
            this.gb_UpdateTrans.Controls.Add(this.rb_Yes);
            this.gb_UpdateTrans.Controls.Add(this.txt_TransName);
            this.gb_UpdateTrans.Controls.Add(this.txt_TransId);
            this.gb_UpdateTrans.Controls.Add(this.lb_TransActivate);
            this.gb_UpdateTrans.Controls.Add(this.lb_TransName);
            this.gb_UpdateTrans.Controls.Add(this.lb_TransId);
            this.gb_UpdateTrans.Location = new System.Drawing.Point(12, 12);
            this.gb_UpdateTrans.Name = "gb_UpdateTrans";
            this.gb_UpdateTrans.Size = new System.Drawing.Size(420, 169);
            this.gb_UpdateTrans.TabIndex = 0;
            this.gb_UpdateTrans.TabStop = false;
            this.gb_UpdateTrans.Text = "修改";
            // 
            // rb_No
            // 
            this.rb_No.AutoSize = true;
            this.rb_No.Location = new System.Drawing.Point(178, 117);
            this.rb_No.Name = "rb_No";
            this.rb_No.Size = new System.Drawing.Size(35, 16);
            this.rb_No.TabIndex = 21;
            this.rb_No.TabStop = true;
            this.rb_No.Text = "否";
            this.rb_No.UseVisualStyleBackColor = true;
            // 
            // rb_Yes
            // 
            this.rb_Yes.AutoSize = true;
            this.rb_Yes.Location = new System.Drawing.Point(124, 117);
            this.rb_Yes.Name = "rb_Yes";
            this.rb_Yes.Size = new System.Drawing.Size(35, 16);
            this.rb_Yes.TabIndex = 20;
            this.rb_Yes.TabStop = true;
            this.rb_Yes.Text = "是";
            this.rb_Yes.UseVisualStyleBackColor = true;
            // 
            // txt_TransName
            // 
            this.txt_TransName.Location = new System.Drawing.Point(124, 72);
            this.txt_TransName.Name = "txt_TransName";
            this.txt_TransName.ReadOnly = true;
            this.txt_TransName.Size = new System.Drawing.Size(269, 21);
            this.txt_TransName.TabIndex = 19;
            // 
            // txt_TransId
            // 
            this.txt_TransId.Location = new System.Drawing.Point(124, 28);
            this.txt_TransId.Name = "txt_TransId";
            this.txt_TransId.ReadOnly = true;
            this.txt_TransId.Size = new System.Drawing.Size(269, 21);
            this.txt_TransId.TabIndex = 18;
            // 
            // lb_TransActivate
            // 
            this.lb_TransActivate.AutoSize = true;
            this.lb_TransActivate.Location = new System.Drawing.Point(24, 119);
            this.lb_TransActivate.Name = "lb_TransActivate";
            this.lb_TransActivate.Size = new System.Drawing.Size(53, 12);
            this.lb_TransActivate.TabIndex = 17;
            this.lb_TransActivate.Text = "是否激活";
            // 
            // lb_TransName
            // 
            this.lb_TransName.Location = new System.Drawing.Point(24, 75);
            this.lb_TransName.Name = "lb_TransName";
            this.lb_TransName.Size = new System.Drawing.Size(83, 12);
            this.lb_TransName.TabIndex = 16;
            this.lb_TransName.Text = "事务/事件名称     ";
            // 
            // lb_TransId
            // 
            this.lb_TransId.AutoSize = true;
            this.lb_TransId.Location = new System.Drawing.Point(24, 33);
            this.lb_TransId.Name = "lb_TransId";
            this.lb_TransId.Size = new System.Drawing.Size(83, 12);
            this.lb_TransId.TabIndex = 15;
            this.lb_TransId.Text = "事务/事件编码";
            // 
            // btn_Cancel
            // 
            this.btn_Cancel.Location = new System.Drawing.Point(357, 192);
            this.btn_Cancel.Name = "btn_Cancel";
            this.btn_Cancel.Size = new System.Drawing.Size(75, 30);
            this.btn_Cancel.TabIndex = 6;
            this.btn_Cancel.Text = "取消";
            this.btn_Cancel.UseVisualStyleBackColor = true;
            this.btn_Cancel.Click += new System.EventHandler(this.btn_Cancel_Click);
            // 
            // btn_Confirm
            // 
            this.btn_Confirm.Location = new System.Drawing.Point(276, 192);
            this.btn_Confirm.Name = "btn_Confirm";
            this.btn_Confirm.Size = new System.Drawing.Size(75, 30);
            this.btn_Confirm.TabIndex = 5;
            this.btn_Confirm.Text = "确定";
            this.btn_Confirm.UseVisualStyleBackColor = true;
            this.btn_Confirm.Click += new System.EventHandler(this.btn_Confirm_Click);
            // 
            // Frm_UpdateTrans
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(443, 234);
            this.Controls.Add(this.btn_Cancel);
            this.Controls.Add(this.btn_Confirm);
            this.Controls.Add(this.gb_UpdateTrans);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Frm_UpdateTrans";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "修改事务/事件";
            this.gb_UpdateTrans.ResumeLayout(false);
            this.gb_UpdateTrans.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gb_UpdateTrans;
        private System.Windows.Forms.Button btn_Cancel;
        private System.Windows.Forms.Button btn_Confirm;
        private System.Windows.Forms.RadioButton rb_No;
        private System.Windows.Forms.RadioButton rb_Yes;
        private System.Windows.Forms.TextBox txt_TransName;
        private System.Windows.Forms.TextBox txt_TransId;
        private System.Windows.Forms.Label lb_TransActivate;
        private System.Windows.Forms.Label lb_TransName;
        private System.Windows.Forms.Label lb_TransId;
    }
}