﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Model.StockModel;
using Lib.Bll.StockBLL;
using Lib.Common.CommonUtils;

namespace MMClient.InventoryManagement
{
    public partial class Frm_TransOrEvent : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        StockTransInfoBLL stockTransInfoBLL = new StockTransInfoBLL();
        public Frm_TransOrEvent()
        {
            InitializeComponent();
            this.dgv_TransDetails.TopLeftHeaderCell.Value = "行号";
            this.initialData();
        }

        /// <summary>
        /// 初始化时绑定数据;
        /// </summary>
        private void initialData()
        {
            try
            {
                List<StockTransExtendModel> stockTransExtendModelList = stockTransInfoBLL.findAllStockTransInfo();
                if (stockTransExtendModelList != null)
                {
                    int i = 0;
                    foreach (StockTransExtendModel stockTransExtendModel in stockTransExtendModelList)
                    {
                        this.dgv_TransDetails.Rows.Add(1);
                        dgv_TransDetails.Rows[i].Cells["Col_TransId"].Value = stockTransExtendModel.StockTrans_ID;
                        dgv_TransDetails.Rows[i].Cells["Col_TransName"].Value = stockTransExtendModel.StockTrans_Name;
                        dgv_TransDetails.Rows[i].Cells["Col_TransActivate"].Value = stockTransExtendModel.StockTrans_Activate;
                        i++;
                    }
                }
                else
                {
                    MessageBox.Show("没有查到相关数据", "温馨提示", MessageBoxButtons.OK, MessageBoxIcon.Information);

                }
            }
            catch (Exception ex)
            {
                MessageUtil.ShowError(ex.Message);
            }
        }

        /// <summary>
        /// 画行号
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgv_TransDetails_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            var dgv = sender as DataGridView;
            if (dgv != null)
            {
                Rectangle rect = new Rectangle(e.RowBounds.Location.X, e.RowBounds.Location.Y, (dgv.RowHeadersWidth + 12) / 2, e.RowBounds.Height);
                TextRenderer.DrawText(e.Graphics, (e.RowIndex + 1).ToString(), dgv.RowHeadersDefaultCellStyle.Font, rect, dgv.RowHeadersDefaultCellStyle.ForeColor, TextFormatFlags.VerticalCenter | TextFormatFlags.Right);
            }
        }

        /// <summary>
        /// 当鼠标进入单元格时发生
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgv_TransDetails_CellMouseEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
            {
                this.dgv_TransDetails.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.LightBlue;
                DataGridViewCellStyle style = new DataGridViewCellStyle();
                style.BackColor = Color.CornflowerBlue;
                this.dgv_TransDetails.Rows[e.RowIndex].HeaderCell.Style = style;
            }
        }

        /// <summary>
        /// 当鼠标移出单元格时发生
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgv_TransDetails_CellMouseLeave(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
            {
                this.dgv_TransDetails.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.White;
                DataGridViewCellStyle style = new DataGridViewCellStyle();
                style.BackColor = Color.WhiteSmoke;
                this.dgv_TransDetails.Rows[e.RowIndex].HeaderCell.Style = style;
            }
        }

        /// <summary>
        /// 重载方法
        /// </summary>
        /// <param name="stockTransModel">事务信息类</param>
        /// <param name="currentRow"></param>
        public void updateDGV(StockTransModel stockTransModel, DataGridViewRow currentRow)
        {
            currentRow.Cells["Col_TransName"].Value = stockTransModel.StockTrans_Name;
            currentRow.Cells["Col_TransActivate"].Value = stockTransModel.StockTrans_Activate;
        }

        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_Update_Click(object sender, EventArgs e)
        {
            DataGridViewRow currentRow = this.dgv_TransDetails.CurrentRow;
            if (currentRow == null)
            {
                MessageUtil.ShowTips("亲，没有要修改的数据哟!");
                return;
            }
            new Frm_UpdateTrans(this, dgv_TransDetails.CurrentRow).ShowDialog();
        }


        /// <summary>
        /// 右击->修改
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ToolStripMenuItem_Update_Click(object sender, EventArgs e)
        {
            this.btn_Update_Click(sender, e);
        }

    }
}
