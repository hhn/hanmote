﻿namespace MMClient.InventoryManagement{
    partial class Frm_UpdateMoveType
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gb_UpdateMoveType = new System.Windows.Forms.GroupBox();
            this.cbb_TransactionDtb = new System.Windows.Forms.ComboBox();
            this.txt_MoveTypeReason = new System.Windows.Forms.TextBox();
            this.txt_MoveTypeMeaning = new System.Windows.Forms.TextBox();
            this.txt_MoveTypeId = new System.Windows.Forms.TextBox();
            this.lb_TransactionDtb = new System.Windows.Forms.Label();
            this.lb_MoveTypeReason = new System.Windows.Forms.Label();
            this.lb_MoveTypeMeaning = new System.Windows.Forms.Label();
            this.lb_MoveTypeId = new System.Windows.Forms.Label();
            this.btn_Cancel = new System.Windows.Forms.Button();
            this.btn_Confirm = new System.Windows.Forms.Button();
            this.gb_UpdateMoveType.SuspendLayout();
            this.SuspendLayout();
            // 
            // gb_UpdateMoveType
            // 
            this.gb_UpdateMoveType.Controls.Add(this.cbb_TransactionDtb);
            this.gb_UpdateMoveType.Controls.Add(this.txt_MoveTypeReason);
            this.gb_UpdateMoveType.Controls.Add(this.txt_MoveTypeMeaning);
            this.gb_UpdateMoveType.Controls.Add(this.txt_MoveTypeId);
            this.gb_UpdateMoveType.Controls.Add(this.lb_TransactionDtb);
            this.gb_UpdateMoveType.Controls.Add(this.lb_MoveTypeReason);
            this.gb_UpdateMoveType.Controls.Add(this.lb_MoveTypeMeaning);
            this.gb_UpdateMoveType.Controls.Add(this.lb_MoveTypeId);
            this.gb_UpdateMoveType.Location = new System.Drawing.Point(12, 12);
            this.gb_UpdateMoveType.Name = "gb_UpdateMoveType";
            this.gb_UpdateMoveType.Size = new System.Drawing.Size(446, 227);
            this.gb_UpdateMoveType.TabIndex = 1;
            this.gb_UpdateMoveType.TabStop = false;
            this.gb_UpdateMoveType.Text = "修改";
            // 
            // cbb_TransactionDtb
            // 
            this.cbb_TransactionDtb.FormattingEnabled = true;
            this.cbb_TransactionDtb.Location = new System.Drawing.Point(128, 189);
            this.cbb_TransactionDtb.Name = "cbb_TransactionDtb";
            this.cbb_TransactionDtb.Size = new System.Drawing.Size(280, 20);
            this.cbb_TransactionDtb.TabIndex = 7;
            // 
            // txt_MoveTypeReason
            // 
            this.txt_MoveTypeReason.Location = new System.Drawing.Point(128, 104);
            this.txt_MoveTypeReason.Multiline = true;
            this.txt_MoveTypeReason.Name = "txt_MoveTypeReason";
            this.txt_MoveTypeReason.Size = new System.Drawing.Size(280, 64);
            this.txt_MoveTypeReason.TabIndex = 6;
            // 
            // txt_MoveTypeMeaning
            // 
            this.txt_MoveTypeMeaning.Location = new System.Drawing.Point(128, 65);
            this.txt_MoveTypeMeaning.Name = "txt_MoveTypeMeaning";
            this.txt_MoveTypeMeaning.Size = new System.Drawing.Size(280, 21);
            this.txt_MoveTypeMeaning.TabIndex = 5;
            // 
            // txt_MoveTypeId
            // 
            this.txt_MoveTypeId.Location = new System.Drawing.Point(128, 27);
            this.txt_MoveTypeId.Name = "txt_MoveTypeId";
            this.txt_MoveTypeId.ReadOnly = true;
            this.txt_MoveTypeId.Size = new System.Drawing.Size(280, 21);
            this.txt_MoveTypeId.TabIndex = 4;
            // 
            // lb_TransactionDtb
            // 
            this.lb_TransactionDtb.AutoSize = true;
            this.lb_TransactionDtb.Location = new System.Drawing.Point(16, 193);
            this.lb_TransactionDtb.Name = "lb_TransactionDtb";
            this.lb_TransactionDtb.Size = new System.Drawing.Size(53, 12);
            this.lb_TransactionDtb.TabIndex = 3;
            this.lb_TransactionDtb.Text = "事务分配";
            // 
            // lb_MoveTypeReason
            // 
            this.lb_MoveTypeReason.AutoSize = true;
            this.lb_MoveTypeReason.Location = new System.Drawing.Point(16, 107);
            this.lb_MoveTypeReason.Name = "lb_MoveTypeReason";
            this.lb_MoveTypeReason.Size = new System.Drawing.Size(53, 12);
            this.lb_MoveTypeReason.TabIndex = 2;
            this.lb_MoveTypeReason.Text = "详细文本";
            // 
            // lb_MoveTypeMeaning
            // 
            this.lb_MoveTypeMeaning.AutoSize = true;
            this.lb_MoveTypeMeaning.Location = new System.Drawing.Point(15, 69);
            this.lb_MoveTypeMeaning.Name = "lb_MoveTypeMeaning";
            this.lb_MoveTypeMeaning.Size = new System.Drawing.Size(29, 12);
            this.lb_MoveTypeMeaning.TabIndex = 1;
            this.lb_MoveTypeMeaning.Text = "描述";
            // 
            // lb_MoveTypeId
            // 
            this.lb_MoveTypeId.AutoSize = true;
            this.lb_MoveTypeId.Location = new System.Drawing.Point(15, 32);
            this.lb_MoveTypeId.Name = "lb_MoveTypeId";
            this.lb_MoveTypeId.Size = new System.Drawing.Size(77, 12);
            this.lb_MoveTypeId.TabIndex = 0;
            this.lb_MoveTypeId.Text = "移动类型编码";
            // 
            // btn_Cancel
            // 
            this.btn_Cancel.Location = new System.Drawing.Point(384, 249);
            this.btn_Cancel.Name = "btn_Cancel";
            this.btn_Cancel.Size = new System.Drawing.Size(75, 30);
            this.btn_Cancel.TabIndex = 4;
            this.btn_Cancel.Text = "取消";
            this.btn_Cancel.UseVisualStyleBackColor = true;
            this.btn_Cancel.Click += new System.EventHandler(this.btn_Cancel_Click);
            // 
            // btn_Confirm
            // 
            this.btn_Confirm.Location = new System.Drawing.Point(303, 249);
            this.btn_Confirm.Name = "btn_Confirm";
            this.btn_Confirm.Size = new System.Drawing.Size(75, 30);
            this.btn_Confirm.TabIndex = 3;
            this.btn_Confirm.Text = "确定";
            this.btn_Confirm.UseVisualStyleBackColor = true;
            this.btn_Confirm.Click += new System.EventHandler(this.btn_Confirm_Click);
            // 
            // Frm_UpdateMoveType
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(466, 288);
            this.Controls.Add(this.btn_Cancel);
            this.Controls.Add(this.btn_Confirm);
            this.Controls.Add(this.gb_UpdateMoveType);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Frm_UpdateMoveType";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "修改移动类型";
            this.gb_UpdateMoveType.ResumeLayout(false);
            this.gb_UpdateMoveType.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gb_UpdateMoveType;
        private System.Windows.Forms.ComboBox cbb_TransactionDtb;
        private System.Windows.Forms.TextBox txt_MoveTypeReason;
        private System.Windows.Forms.TextBox txt_MoveTypeMeaning;
        private System.Windows.Forms.TextBox txt_MoveTypeId;
        private System.Windows.Forms.Label lb_TransactionDtb;
        private System.Windows.Forms.Label lb_MoveTypeReason;
        private System.Windows.Forms.Label lb_MoveTypeMeaning;
        private System.Windows.Forms.Label lb_MoveTypeId;
        private System.Windows.Forms.Button btn_Cancel;
        private System.Windows.Forms.Button btn_Confirm;
    }
}