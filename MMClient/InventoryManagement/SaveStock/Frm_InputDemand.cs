﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Bll.StockBLL;
using Lib.Bll.MDBll.General;
using Lib.SqlServerDAL.StockDAL;

namespace MMClient.InventoryManagement
{
    public partial class Frm_InputDemand : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        public Frm_InputDemand()
        {
            InitializeComponent();
        }

        private void Frm_InputDemand_Load(object sender, EventArgs e)
        {
            List<string> mlist = new List<string>();
            List<string> flist = new List<string>();
            getMaterialInfoBLL gmb = new getMaterialInfoBLL();
            DataTable dt = gmb.getMaterialID();
            DataTable dt0 = gmb.getFactoryID();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                mlist.Insert(i, dt.Rows[i][0].ToString());
            }
            for (int i = 0; i < dt0.Rows.Count; i++)
            {
                flist.Insert(i, dt0.Rows[i][0].ToString());
            }

            ComboBoxItem comb = new ComboBoxItem();
            comb.FuzzyQury(comboBox1, flist);
            comb.FuzzyQury(comboBox2, mlist);
           
        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            double d1;
            double d2;
            double d3;
            double d4;
            double d5;
            double d6;
            double d7;
            double d8;
            double d9;
            double d10;

            string factory;
            string material;

            try
            {
                factory = this.comboBox1.SelectedItem.ToString().Trim();
                material = this.comboBox2.SelectedItem.ToString().Trim();
            }
            catch {
                MessageBox.Show("工厂或物料输入有误");
                return;
 
            }

            DataTable dt = new DataTable("dt");
            dt.Columns.Add("factory", typeof(string));
            dt.Columns.Add("material", typeof(string));
            dt.Columns.Add("demand", typeof(double));
            
            #region
            if (textBox1.Text != "")
            {
                try
                {
                    d1 = Convert.ToDouble(textBox1.Text);
                    DataRow dr = dt.NewRow();
                    dr["factory"] = factory;
                    dr["material"] = material;
                    dr["demand"] = d1;
                    dt.Rows.Add(dr);
                }
                catch {
                    MessageBox.Show("第一阶段需求请输入数字");
                }
            }

            if (textBox2.Text != "")
            {
                try
                {
                    d2 = Convert.ToDouble(textBox2.Text.Trim());
                    DataRow dr = dt.NewRow();
                    dr["factory"] = factory;
                    dr["material"] = material;
                    dr["demand"] = d2;
                    dt.Rows.Add(dr);
                }
                catch
                {
                    MessageBox.Show("第二阶段需求请输入数字");
                }
            }

            if (textBox3.Text != "")
            {
                try
                {
                    d3 = Convert.ToDouble(textBox3.Text.Trim());
                    DataRow dr = dt.NewRow();
                    dr["factory"] = factory;
                    dr["material"] = material;
                    dr["demand"] = d3;
                    dt.Rows.Add(dr);
                }
                catch
                {
                    MessageBox.Show("第三阶段需求请输入数字");
                }
            }

            if (textBox4.Text != "")
            {
                try
                {
                    d4 = Convert.ToDouble(textBox4.Text.Trim());
                    DataRow dr = dt.NewRow();
                    dr["factory"] = factory;
                    dr["material"] = material;
                    dr["demand"] = d4;
                    dt.Rows.Add(dr);
                }
                catch
                {
                    MessageBox.Show("第一阶段需求请输入数字");
                }
            }
            if (textBox5.Text != "")
            {
                try
                {
                    d5 = Convert.ToDouble(textBox5.Text.Trim());
                    DataRow dr = dt.NewRow();
                    dr["factory"] = factory;
                    dr["material"] = material;
                    dr["demand"] = d5;
                    dt.Rows.Add(dr);
                }
                catch
                {
                    MessageBox.Show("第五阶段需求请输入数字");
                }
            }
            if (textBox6.Text.Trim() != "")
            {
                try
                {
                    d6 = Convert.ToDouble(textBox6.Text.Trim());
                    DataRow dr = dt.NewRow();
                    dr["factory"] = factory;
                    dr["material"] = material;
                    dr["demand"] = d6;
                    dt.Rows.Add(dr);
                }
                catch
                {
                    MessageBox.Show("第六阶段需求请输入数字");
                }
            }
            if (textBox7.Text != "")
            {
                try
                {
                    d7 = Convert.ToDouble(textBox7.Text.Trim());
                    DataRow dr = dt.NewRow();
                    dr["factory"] = factory;
                    dr["material"] = material;
                    dr["demand"] = d7;
                    dt.Rows.Add(dr);
                }
                catch
                {
                    MessageBox.Show("第七阶段需求请输入数字");
                }
            }
            if (textBox8.Text != "")
            {
                try
                {
                    d8 = Convert.ToDouble(textBox8.Text.Trim());
                    DataRow dr = dt.NewRow();
                    dr["factory"] = factory;
                    dr["material"] = material;
                    dr["demand"] = d8;
                    dt.Rows.Add(dr);
                }
                catch
                {
                    MessageBox.Show("第八阶段需求请输入数字");
                }
            }
            if (textBox9.Text != "")
            {
                try
                {
                    d9 = Convert.ToDouble(textBox9.Text.Trim());
                    DataRow dr = dt.NewRow();
                    dr["factory"] = factory;
                    dr["material"] = material;
                    dr["demand"] = d9;
                    dt.Rows.Add(dr);
                }
                catch
                {
                    MessageBox.Show("第九阶段需求请输入数字");
                }
            }
            if (textBox10.Text != "")
            {
                try
                {
                    d10 = Convert.ToDouble(textBox10.Text.Trim());
                    DataRow dr = dt.NewRow();
                    dr["factory"] = factory;
                    dr["material"] = material;
                    dr["demand"] = d10;
                    dt.Rows.Add(dr);
                }
                catch
                {
                    MessageBox.Show("第十阶段需求请输入数字");
                }
            }

                #endregion
            SaftyInventoryDAL sid = new SaftyInventoryDAL();
            int n = dt.Rows.Count;
            sid.inputdemand(dt);
            MessageBox.Show("保存成功", "提示");
        }
        
    }
}
