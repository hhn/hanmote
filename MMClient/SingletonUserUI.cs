﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WeifenLuo.WinFormsUI.Docking;

namespace MMClient
{
    /// <summary>
    /// 线程安全单例，确保所有form采用同一个UserUI
    /// </summary>
    public class SingletonUserUI
    {

        //test
        private static UserUI userUI = new UserUI();

        public static UserUI getUserUI()
        {
            return userUI;
        }

        /// <summary>
        /// 加入到UserUI中
        /// </summary>
        /// <param name="content"></param>
        public static void addToUserUI(DockContent content){
            userUI.displayOnDockPanel(content);
        }
    }
}
