﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Model.CatalogPurchase;
using Lib.Common.CommonUtils;
using Lib.Bll.CatalogManageBLL;

namespace MMClient.CatalogManage.CatalogAgreement
{
    public partial class AddShoppingCartForm : Form
    {
        //业务逻辑处理工具
        private ShoppingCartBLL shoppingCartTool = new ShoppingCartBLL();
        //加入购物车
        private CatalogMaterial catalogMaterial;

        public AddShoppingCartForm(CatalogMaterial _catalogMaterial)
        {
            InitializeComponent();
            this.catalogMaterial = _catalogMaterial;

            display();
        }

        private void display() {
            this.tbMaterialCatalogID.Text = catalogMaterial.ID;
            this.tbMaterialName.Text = catalogMaterial.Material_Name;
            this.tbSupplierID.Text = catalogMaterial.Supplier_ID;
            this.tbSupplierName.Text = catalogMaterial.Supplier_Name;
            this.tbPrice.Text = Convert.ToString(catalogMaterial.Price);
            this.tbCurrency.Text = catalogMaterial.Currency_Type;
            this.tbUnit.Text = catalogMaterial.Material_Unit;
            this.tbCurrency2.Text = catalogMaterial.Currency_Type;
            try
            {
                double price = catalogMaterial.Price;
                double quantity = (double)this.nudAmount.Value;

                double amount = price * quantity;
                this.tbTotalAmount.Text = Convert.ToString(amount);
            }
            catch (Exception)
            {
                this.tbTotalAmount.Text = "0.0";
            }

            //disable
            this.tbMaterialCatalogID.Enabled = false;
            this.tbMaterialName.Enabled = false;
            this.tbSupplierID.Enabled = false;
            this.tbSupplierName.Enabled = false;
            this.tbUnit.Enabled = false;
            this.tbPrice.Enabled = false;
            this.tbCurrency.Enabled = false;
            this.tbCurrency2.Enabled = false;
            this.tbTotalAmount.Enabled = false;
        }

        /// <summary>
        /// 点击确定，加入购物车
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnConfirm_Click(object sender, EventArgs e)
        {
            double amount = (double)this.nudAmount.Value;
            if (MathUtil.equalsZero(amount)) {
                MessageUtil.ShowError("采购数量不能为0！");
                return;
            }
            string shoppingCartID = "SC2016001";
            string materialCatalogID = catalogMaterial.ID;
            //购物车中是否已经存在该物料的采购
            ShoppingCartItem oldItem = shoppingCartTool.getShoppingCartItem(shoppingCartID, materialCatalogID);
            int result = 0;
            if (oldItem == null)
            {
                //未加入
                ShoppingCartItem newItem = new ShoppingCartItem();
                newItem.Shopping_Cart_ID = shoppingCartID;
                newItem.Material_Catalog_ID = materialCatalogID;
                newItem.Total_Quantity = amount;
                newItem.Net_Price = catalogMaterial.Price;
                newItem.Total_Amount = newItem.Total_Quantity * newItem.Net_Price;
                newItem.Material_Name = catalogMaterial.Material_Name;
                newItem.Supplier_ID = catalogMaterial.Supplier_ID;
                newItem.Is_Valid = "有效";
                newItem.Currency = catalogMaterial.Currency_Type;
                //加入
                result = shoppingCartTool.addToShoppingCart(newItem);
            }
            else { 
                //已存在
                oldItem.Total_Quantity += amount;
                oldItem.Total_Amount += (amount * catalogMaterial.Price);
                //更新
                result = shoppingCartTool.updateShoppingCart(oldItem);
            }

            if (result > 0)
            {
                MessageBox.Show("加入成功！");
                this.Close();
            }
            else {
                MessageBox.Show("加入失败！");
            }
        }

        /// <summary>
        /// 总金额自动变化
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void nudAmount_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                double price = catalogMaterial.Price;
                double quantity = (double)this.nudAmount.Value;

                double amount = price * quantity;
                this.tbTotalAmount.Text = Convert.ToString(amount);
                return;
            }
            catch (Exception) {
                this.tbTotalAmount.Text = "0.0";
                return;
            }
        }
    }
}
