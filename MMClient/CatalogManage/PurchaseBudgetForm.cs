﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Bll.MDBll.General;
using Lib.Bll.MDBll.MT;
using Lib.Model.MD.MT;
using Lib.SqlServerDAL;
using WeifenLuo.WinFormsUI.Docking;
using Lib.Common.CommonUtils;

namespace MMClient.CatalogManage
{
    public partial class PurchaseBudgetForm : DockContent
    {
        //业务逻辑处理工具
        private GeneralBLL generalTool = new GeneralBLL();
        private MaterialBLL materialTool = new MaterialBLL();
        private MaterialAccountBLL materialAccountTool = new MaterialAccountBLL();
        // 所有的物料ID
        private List<string> materialIDList = null;
        //货币类型
        private string currencyType = "";
        //工厂编号列表
        List<string> factoryIDList;
        //公司编号列表
        List<string> companyIDList;

        public PurchaseBudgetForm()
        {
            InitializeComponent();
            
            initialData();
        }

        /// <summary>
        /// 初始化数据
        /// </summary>
        private void initialData(){
            //设置编号
            string defaultID = "CGYS" + System.DateTime.Now.ToString("yyyyMMddHHmmss")
                    + "2016001";
            this.cbxBudgetType.Text = defaultID;
            //货币类型
            List<string> currencyList = generalTool.GetCurrencyType();
            this.cbxCurrencyType.DataSource = currencyList;
            cbxCurrencyType_SelectedIndexChanged(null, null);
            //物料编号
            materialIDList = materialTool.GetAllMaterialID();
            //是否制定详细预算
            cbDetail_CheckedChanged(null, null);
            //采购类型
            this.cbxBudgetType.SelectedIndex = 0;
            //公司编号
            companyIDList = generalTool.GetAllCompany();
            foreach (string companyID in this.companyIDList)
            {
                this.cbxCompanyID.Items.Add(companyID);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cbDetail_CheckedChanged(object sender, EventArgs e)
        {
            bool value = this.cbDetail.Checked;

            this.lblBudgetDetail.Enabled = value;
            this.btnAdd.Enabled = value;
            this.btnDelete.Enabled = value;
            this.dgvBudgetInfo.Enabled = value;
        }

        /// <summary>
        /// 选择变化
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cbxBudgetType_SelectedIndexChanged(object sender, EventArgs e)
        {
            string text = this.cbxBudgetType.Text;
            if (text.Equals("单次采购"))
            {
                this.dtpBeginTime.Enabled = false;
                this.dtpEndTime.Enabled = false;
            }
            else if(text.Equals("持续采购")){
                this.dtpBeginTime.Enabled = true;
                this.dtpEndTime.Enabled = true;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAdd_Click(object sender, EventArgs e)
        {
            this.dgvBudgetInfo.Rows.Add();
            DataGridViewRow row = this.dgvBudgetInfo.Rows[this.dgvBudgetInfo.Rows.Count - 1];
            DataGridViewComboBoxCell comboBox = row.Cells[0] as DataGridViewComboBoxCell;
            comboBox.DataSource = materialIDList;

            row.Cells[3].Value = currencyType;
        }

        private void materialID_SelectedIndexChanged(object sender, EventArgs e)
        {
            int rowIndex = this.dgvBudgetInfo.CurrentCell.RowIndex;
            int columnIndex = this.dgvBudgetInfo.CurrentCell.ColumnIndex;
            //可以转换
            if (rowIndex >= 0)
            {
                var sendingCB = sender as DataGridViewComboBoxEditingControl;
                string materialID = sendingCB.EditingControlFormattedValue.ToString();
                if (!materialID.Equals(""))
                {
                    //DataTable dt = generalTool.getMaterialInfo(materialID);
                    MaterialBase materialInfo = materialAccountTool
                        .GetBasicInformation(materialID);

                    //DataRow curRow = dt.Rows[0];
                    DataGridViewRow dgvRow = this.dgvBudgetInfo.Rows[rowIndex];
                    //短文本
                    dgvRow.Cells[1].Value = materialInfo.Material_Name;
                }
            }
        }

        private void dgvBudgetInfo_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            int columnIndex = this.dgvBudgetInfo.CurrentCell.ColumnIndex;
            if (columnIndex == 0 && e.Control is ComboBox)
            {
                ComboBox combo = e.Control as ComboBox;
                combo.SelectedIndexChanged += materialID_SelectedIndexChanged;
            }
        }

        private void cbxCurrencyType_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.currencyType = this.cbxCurrencyType.Text;
            foreach (DataGridViewRow row in this.dgvBudgetInfo.Rows)
                row.Cells[3].Value = currencyType;
        }

        /// <summary>
        /// 公司发生变化
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cbxCompanyID_SelectedIndexChanged(object sender, EventArgs e)
        {
            string companyID = this.cbxCompanyID.Text;
            if (!companyID.Trim().Equals("")) {
                this.factoryIDList = generalTool.GetAllFactory(companyID);
                //编辑工厂列表
                this.cbxFactoryID.Items.Clear();
                foreach (string factoryID in this.factoryIDList) {
                    this.cbxFactoryID.Items.Add(factoryID);
                }
            }
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (this.dgvBudgetInfo.Rows.Count == 0)
            {
                MessageBox.Show("没有可删除内容", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            int rowIndex = this.dgvBudgetInfo.CurrentCell.RowIndex;
            if (rowIndex >= 0 && rowIndex < this.dgvBudgetInfo.Rows.Count)
            {
                this.dgvBudgetInfo.Rows.RemoveAt(rowIndex);
            }
        }

        /// <summary>
        /// 保存
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSave_Click(object sender, EventArgs e)
        {
            string budgetID = this.cbxPurchaseBudgetID.Text;
            string companyID = this.cbxCompanyID.Text;
            string factoryID = this.cbxFactoryID.Text;
            string budgetType = this.cbxBudgetType.Text;
            DateTime beginTime = this.dtpBeginTime.Value;
            DateTime endTime = this.dtpEndTime.Value;
            double sumBudget;
            try
            {
                sumBudget = Convert.ToDouble(this.tbSumBudget.Text);
            }
            catch (Exception) {
                MessageUtil.ShowError("请正确填写！");
                return;
            }
            string isDetail = this.cbDetail.Checked ? "1" : "0";

            if (budgetID.Equals("") || (budgetType.Equals("持续采购") && beginTime >= endTime)) {
                MessageUtil.ShowError("请正确填写！");
                return;
            }
            LinkedList<string> sqlList = new LinkedList<string>();
            StringBuilder sql = new StringBuilder("insert into Purchase_Budget (")
                .Append("Budget_ID, Budget_Type, Company_ID, Factory_ID, Begin_Time, End_Time, Budget,")
                .Append("Currency_Type, Is_Detail) values('")
                .Append(budgetID).Append("', '")
                .Append(budgetType).Append("', '")
                .Append(companyID).Append("', '")
                .Append(factoryID).Append("', '")
                .Append(beginTime.ToString("yyyy-MM-dd hh:mm:ss.fff")).Append("', '")
                .Append(endTime.ToString("yyyy-MM-dd hh:mm:ss.fff")).Append("', ")
                .Append(Convert.ToString(sumBudget)).Append(", '")
                .Append(currencyType).Append("', ")
                .Append(isDetail).Append(")");

            sqlList.AddLast(sql.ToString());

            if (this.cbDetail.Checked && this.dgvBudgetInfo.Rows.Count == 0) {
                MessageUtil.ShowError("请增加详细项！");
                return;
            }
            try
            {
                double tmpSum = 0.0;
                foreach (DataGridViewRow row in this.dgvBudgetInfo.Rows)
                {
                    string materialID = row.Cells[0].Value.ToString();
                    string materialName = row.Cells[1].Value.ToString();
                    double budget = Convert.ToDouble(row.Cells[2].Value.ToString());
                    tmpSum += budget;

                    StringBuilder sql2 = new StringBuilder("insert into Purchase_Budget_Item (")
                        .Append("Budget_ID, Material_ID, Material_Name, Budget) values('")
                        .Append(budgetID).Append("', '")
                        .Append(materialID).Append("', '")
                        .Append(materialName).Append("', ")
                        .Append(Convert.ToString(budget)).Append(")");
                    
                    sqlList.AddLast(sql2.ToString());
                }

                if (!MathUtil.equalsZero(tmpSum - sumBudget)) {
                    MessageUtil.ShowError("预算总额不相等！");
                    return;
                }
            }
            catch (Exception) {
                MessageUtil.ShowError("请正确填写!");
                return;
            }

            int result = DBHelper.ExecuteNonQuery(sqlList);
            if (result > 0)
            {
                MessageBox.Show("保存成功！");
                this.Close();
            }
            else
                MessageBox.Show("保存失败！");
        }
    }
}
