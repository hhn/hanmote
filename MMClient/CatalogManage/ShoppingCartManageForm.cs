﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Model.CatalogPurchase;
using Lib.Common.CommonUtils;
using Lib.Bll.CatalogManageBLL;
using Lib.SqlServerDAL;

namespace MMClient.CatalogManage
{
    public partial class ShoppingCartManageForm : Form
    {
        //业务逻辑处理工具
        private ShoppingCartBLL shoppingCartTool = new ShoppingCartBLL();
        private CatalogMaterialBLL catalogMaterialTool = new CatalogMaterialBLL();
        //购物车编号
        private string supplierID = "";

        public ShoppingCartManageForm()
        {
            InitializeComponent();
            initialData();
            //initialForm();
        }

        /// <summary>
        /// 
        /// </summary>
        private void initialForm() {
            this.dgvPurchaseItem.Rows.Add(3);
            DataGridViewRow row = this.dgvPurchaseItem.Rows[0];
            row.Cells[1].Value = "0010001002133069";
            row.Cells[2].Value = "201634184";
            row.Cells[3].Value = "3M汽车贴膜";
            row.Cells[4].Value = "39.0";
            row.Cells[5].Value = "40.0";
            row.Cells[6].Value = "1560.0";

            row = this.dgvPurchaseItem.Rows[1];
            row.Cells[1].Value = "0010001002135798";
            row.Cells[2].Value = "201634184";
            row.Cells[3].Value = "3M车蜡";
            row.Cells[4].Value = "99.0";
            row.Cells[5].Value = "10.0";
            row.Cells[6].Value = "990.0";

            row = this.dgvPurchaseItem.Rows[2];
            row.Cells[1].Value = "0010001002133040";
            row.Cells[2].Value = "201634173";
            row.Cells[3].Value = "轮胎保护蜡";
            row.Cells[4].Value = "69.0";
            row.Cells[5].Value = "50.0";
            row.Cells[6].Value = "3450.0";
        }

        /// <summary>
        /// 初始化数据
        /// </summary>
        private void initialData(){
            string shoppingCartID = "SC2016001";
            List<ShoppingCartItem> items = shoppingCartTool.getShoppingCartItems(shoppingCartID);
            foreach (ShoppingCartItem item in items) {
                this.dgvPurchaseItem.Rows.Add();
                DataGridViewRow row = this.dgvPurchaseItem.Rows[this.dgvPurchaseItem.Rows.Count - 1];
                row.Cells[0].Value = true;
                //物料在自助采购目录中的编号(PK)
                row.Cells[1].Value = item.Material_Catalog_ID;
                row.Cells[2].Value = item.Supplier_ID;
                row.Cells[3].Value = item.Material_Name;
                row.Cells[4].Value = item.Net_Price;
                row.Cells[5].Value = item.Total_Quantity;
                row.Cells[6].Value = item.Total_Amount;

                this.tbCurrency.Text = item.Currency;
            }

            calAmount();
            //初始预算编号列表
            this.cbxPurchaseBudgetID.DataSource = shoppingCartTool.getPurchaseBudgetList();
        }

        /// <summary>
        /// 计算总金额
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvPurchaseItem_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int columnIndex = e.ColumnIndex;
            if (columnIndex == 0)
            {
                calAmount();
            }
        }

        /// <summary>
        /// 查看
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tsbPreview_Click(object sender, EventArgs e)
        {
            if (this.dgvPurchaseItem.Rows.Count == 0) {
                MessageUtil.ShowError("没有选中行！");
                return;
            }
            DataGridViewRow row = this.dgvPurchaseItem.SelectedRows[0];
            string materialCatalogID = row.Cells[1].Value.ToString();
            CatalogMaterial materialInfo = catalogMaterialTool.getCatalogMaterial(materialCatalogID);
            if (materialInfo != null)
            {
                CatalogForm previewForm = new CatalogForm(materialInfo, "view");
                previewForm.ShowDialog();
            }
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tsbDelete_Click(object sender, EventArgs e)
        {
            if (this.dgvPurchaseItem.Rows.Count == 0)
            {
                MessageUtil.ShowError("没有选中行！");
            }

            DialogResult re = MessageUtil.ShowOKCancelAndQuestion("确认删除商品？");
            if (re == DialogResult.Cancel)
                return;

            DataGridViewRow row = this.dgvPurchaseItem.SelectedRows[0];
            string materialCatalogID = row.Cells[1].Value.ToString();
            string shoppingCartID = "SC2016001";
            int result = shoppingCartTool.deleteShoppingCartItem(materialCatalogID, shoppingCartID);
            if (result != 0)
            {
                MessageBox.Show("删除成功！");
                this.dgvPurchaseItem.Rows.Remove(row);
                calAmount();
            }
            else {
                MessageBox.Show("删除失败！");
            }
        }

        /// <summary>
        /// 计算总额
        /// </summary>
        private void calAmount() {
            double sum = 0.0;
            foreach (DataGridViewRow row in this.dgvPurchaseItem.Rows)
            {
                bool isChecked = (bool)row.Cells[0].EditedFormattedValue;
                if (isChecked)
                {
                    sum += Convert.ToDouble(row.Cells[6].Value);
                }
            }
            this.tbShoppingCartAmount.Text = sum.ToString("0.0000");
        }

        /// <summary>
        /// 修改购物车
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tsbEdit_Click(object sender, EventArgs e)
        {
            if (this.dgvPurchaseItem.Rows.Count == 0)
            {
                MessageUtil.ShowError("没有选中行！");
            }

            DataGridViewRow row = this.dgvPurchaseItem.SelectedRows[0];
        }

        /// <summary>
        /// 生产采购订单
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tsbOrder_Click(object sender, EventArgs e)
        {
            //检测采购预算
            double shoppingCartAmount = Convert.ToDouble(this.tbShoppingCartAmount.Text);
            double budgetRemain = Convert.ToDouble(this.tbBudgetRemain.Text);
            if (shoppingCartAmount > budgetRemain) {
                MessageUtil.ShowError("预算不足！");
                return;
            }

            //生成订单

            //更新采购预算

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cbxPurchaseBudgetID_SelectedIndexChanged(object sender, EventArgs e)
        {
            string budgetID = this.cbxPurchaseBudgetID.Text;
            string sql = "select * from Purchase_Budget where Budget_ID = '" + budgetID + "'";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            if (dt.Rows.Count == 0)
            {
                //清空
                this.tbBudgetRemain.Text = "0.0";
                this.tbSumBudget.Text = "0.0";
                this.tbCurrency.Text = "";
            }
            else {
                DataRow row = dt.Rows[0];
                this.tbBudgetRemain.Text = row["Budget_Remain"].ToString();
                this.tbSumBudget.Text = row["Budget"].ToString();
                this.tbCurrency.Text = row["Currency_Type"].ToString();
                string type = row["Budget_Type"].ToString();
                if (type.Equals("单次采购"))
                {
                    this.lblEndTime.Visible = false;
                    this.dtpEndTime.Visible = false;
                }
                else {
                    this.lblEndTime.Visible = true;
                    this.dtpEndTime.Visible = true;
                    this.dtpEndTime.Value = Convert.ToDateTime(row["End_Time"].ToString());
                }
            }
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }
    }
}
