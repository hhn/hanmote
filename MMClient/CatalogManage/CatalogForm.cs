﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;
using Lib.Bll.CatalogManageBLL;
using Lib.Bll.MDBll.General;
using Lib.Bll.MDBll.MT;
using Lib.Model.MD.MT;
using Lib.SqlServerDAL;
using Lib.Model.CatalogPurchase;
using Lib.Common.CommonUtils;
using Lib.Bll.ContractManageBLL.OutlineAgreementBLL;
using Lib.Model.ContractManage.OutlineAgreement;


namespace MMClient.CatalogManage
{
    public partial class CatalogForm : DockContent
    {
        // 业务逻辑
        private CatalogMaterialBLL catalogMaterialTool = new CatalogMaterialBLL();
        private GeneralBLL generalTool = new GeneralBLL();
        private MaterialBLL materialTool = new MaterialBLL();
        private MaterialAccountBLL materialAccountTool = new MaterialAccountBLL();
        private AgreementContractBLL agreementContractTool = new AgreementContractBLL();

        // 物料信息
        private CatalogMaterial catalogMaterial = null;
        // 操作类型
        private string opType;
        // 所有的物料ID
        private List<string> materialIDList = null;
        // 所有的自助采购协议编号
        private List<string> agreementIDList = new List<string>();

        public CatalogForm()
        {
            InitializeComponent();

            this.opType = "new";
            initialData();
        }

        /// <summary>
        /// 有参构造函数
        /// </summary>
        /// <param name="_opType"></param>
        public CatalogForm(CatalogMaterial _catalogMaterial, string _opType)
        {
            InitializeComponent();

            this.catalogMaterial = _catalogMaterial;
            this.opType = _opType;
            //初始化数据
            initialData();
            //读取数据
            displayData(catalogMaterial);
            //加以限定
            disableControls();
        }

        /// <summary>
        /// 展示数据
        /// </summary>
        /// <param name="material"></param>
        private void displayData(CatalogMaterial material) {
            //编号
            this.cbxMaterialID.SelectedIndex =
                this.cbxMaterialID.Items.IndexOf(material.Material_ID);
            //物流类别
            this.cbxMaterialType.SelectedIndex = this.cbxMaterialType.Items.IndexOf(material.Material_Type);
            //规格
            this.tbMaterialStandard.Text = material.Material_Standard;
            //价格
            this.tbPrice.Text = Convert.ToString(material.Price);
            //单位
            this.tbUnit.Text = material.Material_Unit;
            //供应商编号
            this.cbxSupplierID.SelectedIndex =
                this.cbxSupplierID.Items.IndexOf(material.Supplier_ID);
            // 供应商名称
            // 详细描述
            this.rtbDetail.Text = material.Detail;
            // 图片
            this.pbImage.Image = material.Image;
        }

        private void disableControls() {
            if (opType.Equals("view")) {
                foreach (Control c in this.Controls) {
                    if (c is TextBox || c is RichTextBox || c is ComboBox || c is PictureBox) {
                        c.Enabled = false;
                    }
                }

                this.btnSave.Text = "关闭";
            }
            else if (opType.Equals("edit")) {
                this.cbxMaterialID.Enabled = false;
                this.cbxAgreementID.Enabled = false;
                this.cbxCurrencyType.Enabled = false;
                this.cbxSupplierID.Enabled = false;
                this.cbxSupplierName.Enabled = false;
                this.tbUnit.Enabled = false;
            }
        }

        /// <summary>
        /// 初始化数据
        /// </summary>
        private void initialData() {
            // 物料ID
            materialIDList = materialTool.GetAllMaterialID();
            this.cbxMaterialID.Items.Clear();
            foreach (string materialID in materialIDList) {
                this.cbxMaterialID.Items.Add(materialID);
            }

            //物料类型
            string typeSql = "SELECT DISTINCT(Material_Type) FROM Material_Catalog";
            DataTable typeDt = DBHelper.ExecuteQueryDT(typeSql);
            foreach (DataRow row in typeDt.Rows) {
                this.cbxMaterialType.Items.Add(row["Material_Type"].ToString());
            }
            
            // 货币类型
            List<string> currencyList = generalTool.GetCurrencyType();
            this.cbxCurrencyType.DataSource = currencyList;
            // 初始化采购协议编号、供应商编号、供应商名称
            string agreementSql = "SELECT a.ID, a.Supplier_ID, b.Supplier_Name FROM Agreement_Contract a, Supplier_Base b WHERE a.Type = '自助采购' AND a.End_Time > GETDATE() AND a.Status = '执行中' AND a.Supplier_ID = b.Supplier_ID";
            DataTable agDt = DBHelper.ExecuteQueryDT(agreementSql);
            // 三个combobox联动
            this.cbxAgreementID.DataSource = agDt;
            this.cbxAgreementID.ValueMember = "ID";
            this.cbxAgreementID.DisplayMember = "ID";
            this.cbxSupplierID.DataSource = agDt;
            this.cbxSupplierID.ValueMember = "Supplier_ID";
            this.cbxSupplierID.DisplayMember = "Supplier_ID";
            this.cbxSupplierName.DataSource = agDt;
            this.cbxSupplierName.ValueMember = "Supplier_Name";
            this.cbxSupplierName.DisplayMember = "Supplier_Name";

            //cbxAgreementID_SelectedIndexChanged(null, null);
        }

        /// <summary>
        /// 上传图片
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pbImage_Click(object sender, EventArgs e)
        {
            OpenFileDialog fileDialog = new OpenFileDialog();
            fileDialog.Title = "请选择图片";
            fileDialog.Filter = "Image Files(*.jpg;*.png;*.bmp;*.gif;*.ico;*.dib;*.tif)|*.jpg;*.png;*.bmp;*.gif;*.ico;*.dib;*.tif";
            var result = fileDialog.ShowDialog();
            if (result == DialogResult.Cancel)
                return;
            string fileName = fileDialog.FileName;
            if (fileName.Equals(String.Empty))
                return;
            Bitmap myImage = new Bitmap(fileName);
            this.pbImage.Image = myImage;
        }

        /// <summary>
        /// 保存
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSave_Click(object sender, EventArgs e)
        {
            if (this.opType.Equals("view")) {
                this.Close();
                return;
            }

            //读取数据
            if (this.opType.Equals("new"))
                this.catalogMaterial = new CatalogMaterial();

            this.catalogMaterial.ID = System.DateTime.Now.ToString("yyyyMMddHHmmssffff");
            this.catalogMaterial.Material_ID = this.cbxMaterialID.Text;
            this.catalogMaterial.Material_Name = this.lblMaterialName.Text;
            this.catalogMaterial.Material_Type = this.cbxMaterialType.Text;
            this.catalogMaterial.Material_Standard = this.tbMaterialStandard.Text;
            this.catalogMaterial.Price = DataGridViewCellTool.convertStringToDouble(
                this.tbPrice.Text);
            this.catalogMaterial.Material_Unit = this.tbUnit.Text;
            this.catalogMaterial.Currency_Type = this.cbxCurrencyType.Text;
            this.catalogMaterial.Supplier_ID = this.cbxSupplierID.Text;
            this.catalogMaterial.Supplier_Name = this.cbxSupplierName.Text;
            this.catalogMaterial.Catalog_Purchase_Contract_ID = this.cbxAgreementID.Text;
            this.catalogMaterial.Detail = this.rtbDetail.Text;
            this.catalogMaterial.Image = (Bitmap)(this.pbImage.Image);
            this.catalogMaterial.Create_Time = System.DateTime.Now;
            this.catalogMaterial.End_Time = System.DateTime.Now;
            this.catalogMaterial.Status = "有效";

            if (this.opType.Equals("new")) {
                int result = catalogMaterialTool.addNewCatalogMaterial(this.catalogMaterial);
                if (result > 0)
                    MessageBox.Show("保存成功！");
                else
                    MessageUtil.ShowError("保存失败！");
            }
        }

        /// <summary>
        /// 物料ID变化
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cbxMaterialID_SelectedIndexChanged(object sender, EventArgs e)
        {
            string agreementID = this.cbxAgreementID.Text;
            string materialID = this.cbxMaterialID.Text;
            string sql = "SELECT Agreement_Contract_Item.Material_ID, Agreement_Contract_Item.Material_Group, Agreement_Contract_Item.Material_Name, Agreement_Contract_Item.Net_Price, Agreement_Contract_Item.OPU, Material.Material_Standard FROM Agreement_Contract_Item LEFT JOIN Material ON Material.Material_ID = Agreement_Contract_Item.Material_ID WHERE Agreement_Contract_Item.Agreement_Contract_ID = '"
                + agreementID + "' AND Agreement_Contract_Item.Material_ID = '" + materialID + "'";
            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            if (dt.Rows.Count == 0)
            {
                this.lblMaterialName.Text = "";
                this.tbMaterialStandard.Text = "";
                this.tbUnit.Text = "";
            }
            else {
                DataRow row = dt.Rows[0];
                this.lblMaterialName.Text = row["Material_Name"].ToString();
                this.tbMaterialStandard.Text = row["Material_Standard"].ToString();
                this.tbUnit.Text = row["OPU"].ToString();
                this.tbPrice.Text = row["Net_Price"].ToString();
            }
        }

        /// <summary>
        /// 编号发生变化
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cbxAgreementID_SelectedIndexChanged(object sender, EventArgs e)
        {
            string agreementID = this.cbxAgreementID.Text;
            //不知道一开始为什么会出现这个
            if (agreementID.Equals("System.Data.DataRowView"))
                return;
            string sql = "SELECT Material_ID FROM Agreement_Contract_Item WHERE Agreement_Contract_ID = '"
                + agreementID + "'";

            DataTable dt = DBHelper.ExecuteQueryDT(sql);
            this.cbxMaterialID.Items.Clear();
            foreach (DataRow row in dt.Rows) {
                this.cbxMaterialID.Items.Add(row["Material_ID"].ToString());
            }
        }

        private void CatalogForm_Load(object sender, EventArgs e)
        {

        }

        private void tbMaterialStandard_TextChanged(object sender, EventArgs e)
        {

        }

        private void cbxSupplierID_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
